<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    /*'cloud' => 'google', // Optional: set Google Drive as default cloud storage

    'disks' => [

        // ...

        'google' => [
            'driver' => 'google',
            'clientId' => env('GOOGLE_DRIVE_CLIENT_ID'),
            'clientSecret' => env('GOOGLE_DRIVE_CLIENT_SECRET'),
            'refreshToken' => env('GOOGLE_DRIVE_REFRESH_TOKEN'),
            'folderId' => env('GOOGLE_DRIVE_FOLDER_ID'),
        ],*/

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [
        'local' => [ 
            'driver' => 'local', 
            'root' => public_path().'/storage', 
        ],

        'feriasleccionesarchivos' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias/lecciones',
        ],

        'feriassolicitudpatrocinio' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias/solicitud_patrocinio',
        ],

        'feriasinvitados' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias/invitados',
        ],

        'feriaspatrocinadores' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias/patrocinadores',
        ],

        'feriasproveedores' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias/proveedores',
        ],

        'conferencista' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias/conferencista',
        ],

        'ferias' => [
            'driver' => 'local',
            'root' => public_path().'/storage/ferias',
        ],

        'empresa' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/empresas/principal', 
        ],

        'archivosi' => [ 
            'driver' => 'local', 
            'root' => public_path().'/aimportant', 
        ],

        'pendientes' => [
            'driver' => 'local',
            'root' => public_path().'/storage/pendientes',
        ],

        'catearchivo' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/categoriaarchivo', 
        ],

        'empresasede' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/empresas/sede',
        ],

        'cliente' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/clientes', 
        ],

        'freelanceUser' => [
            'driver' => 'local',
            'root' => public_path().'/storage/Freelance/users',
        ],

        'freelanceEmpresa' => [
            'driver' => 'local',
            'root' => public_path().'/storage/Freelance/empresas',
        ],

        'freelanceCategoria' => [
            'driver' => 'local',
            'root' => public_path().'/storage/Freelance/categorias',
        ],

        'producto' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/productos', 
        ],
        'evidencias' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/evidencias', 
        ],
        'oportunidades' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/oportunidades', 
        ],
        'anexos' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/ventas/anexos', 
        ],
        'apu' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/ventas/apu', 
        ],
        'contrato' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/ventas/contrato', 
        ],
        'propuesta' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/ventas/propuesta', 
        ],
        'agenda' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/agenda', 
        ],
        'activity' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/file/activity', 
        ],
        'iconos' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/icons', 
        ],
        'porcentajes' => [ 
            'driver' => 'local', 
            'root' => public_path().'/images/porcentajes', 
        ],
        'competencias' => [
            'driver' => 'local',
            'root' => public_path()."/storage/competencias_files/profile_images",
        ],
        'nuevosmercados' =>[
            'driver' => 'local',
            'root' => public_path()."/storage/nuevos_mercados/profile_images"
        ],
        'nuevosmercadosattachments' =>[
            'driver' => 'local',
            'root' => public_path()."/storage/nuevos_mercados/attachment_files"
        ],
        'mapalechero' =>[
            'driver' => 'local',
            'root' => public_path()."/storage/MapaLechero"
        ],
        /*'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],*/

        /*'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],*/

    ],

];





