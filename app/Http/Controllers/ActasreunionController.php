<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Acta;
use App\Invitado;
use App\Actas_tema;
use App\Actas_accione;
use App\Actas_acciones_comentario;
use App\Venta;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
date_default_timezone_set("America/Bogota");

class ActasreunionController extends Controller
{
    public function action(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*Ver Acta*/
            case 0:
                $acta = Acta::find($request->id);
                $invitados = Invitado::where('id_acta',$acta->id)->get();
                $temas = Actas_tema::where('id_acta',$acta->id)->get();
                $pendientes = Actas_accione::where('id_acta',$acta->id)->get();
                $numero=strlen($acta->id);
                $numero_acta = '';
                for($i=1;$i<=4-$numero;$i++){
                    $numero_acta.='0';
                }
                $numero_acta='AC-'.$numero_acta.$acta->id;
                if($acta->estado == "citada"){
                    ob_start(); ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Acta <?=$numero_acta?> <em>(Citada)</em></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Titulo </p>
                                    <p class="text-secondary"><em><?=$acta->nombre?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Descripción </p>
                                    <p class="text-secondary"><em><?=$acta->descripcion?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Fecha y hora </p>
                                    <p class="text-secondary"><em><?=$this->fecha(date('Y-m-d',strtotime($acta->fecha))).' '.date('(h:i:s A)', strtotime($acta->fecha))?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Lugar </p>
                                    <p class="text-secondary"><em><?=$acta->lugar?></em></p>
                                </div>
                            </div>
                            <!--Invitados-->
                            <div class="col-md-12">
                                <div class="form-group">
                                  <p>Invitados </p>
                                </div>
                                <?php if(count($invitados) > 0){ ?>
                                    <div class="row">
                                    <?php $i=0; foreach($invitados as $invitado){ $i++; ?>
                                    <?php if($invitado->tipo == "usuario sistema"){
                                    $user = User::find($invitado->id_usuario);
                                    $nombre = explode(' ',$user->nombres);
                                    $apellido = explode(' ',$user->apellidos);
                                    ?>
                                        <div class="col-md-3 my-4">
                                            <img src="/images/file/clientes/<?=$user->foto?>" class="img-circle" style="width: 40px" alt="Avatar">
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$nombre[0].' '.$apellido[0]?> </strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario sistema)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?=$user->cargo?></span>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-md-3 my-4">
                                            <i class="icon ion-person"></i>
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$invitado->nombre?></strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario externo)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?php if(!empty($invitado->email)){ echo '('.$invitado->email.')'; }else{ echo 'No tiene correo'; } ?></span>
                                        </div>
                                    <?php } } ?>
                                    </div>
                                <?php }else{ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay invitados</em></p>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
<?php               $data['ver'] = ob_get_contents();
                    ob_end_clean();
                }else{
                    ob_start(); ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h2>Acta <?=$numero_acta?> <em>(Realizada)</em></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Titulo </p>
                                    <p class="text-secondary"><em><?=$acta->nombre?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Descripción </p>
                                    <p class="text-secondary"><em><?=$acta->descripcion?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Fecha y hora </p>
                                    <p class="text-secondary"><em><?=$this->fecha(date('Y-m-d',strtotime($acta->fecha))).' '.date('(h:i:s A)', strtotime($acta->fecha))?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Lugar </p>
                                    <p class="text-secondary"><em><?=$acta->lugar?></em></p>
                                </div>
                            </div>
                            <!--Invitados-->
                            <div class="col-md-12">
                                <div class="form-group">
                                  <p>Invitados </p>
                                </div>
                                <?php if(count($invitados) > 0){ ?>
                                    <div class="row">
                                    <?php $i=0; foreach($invitados as $invitado){ $i++; ?>
                                    <?php if($invitado->tipo == "usuario sistema"){
                                    $user = User::find($invitado->id_usuario);
                                    $nombre = explode(' ',$user->nombres);
                                    $apellido = explode(' ',$user->apellidos);
                                    $foto = $user->foto==''?'https://placeholdit.imgix.net/~text?txtsize=45&txt='.$nombre[0].'%20'.$apellido[0].'&w=200&h=200':'/images/file/clientes/'.$user->foto;
                                    ?>
                                        <div class="col-md-3 my-4">
                                            <img src="<?=$foto?>" class="img-circle" style="width: 40px" alt="Avatar">
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$nombre[0].' '.$apellido[0]?> </strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario sistema)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?=$user->cargo?></span>
                                        </div>
                                    <?php }else{
                                        $foto = 'https://placeholdit.imgix.net/~text?txtsize=45&txt='.$invitado->nombre.'&w=200&h=200';
                                        ?>
                                        <div class="col-md-3 my-4">
                                            <img src="<?=$foto?>" class="img-circle" style="width: 40px" alt="Avatar">
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$invitado->nombre?></strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario externo)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?php if(!empty($invitado->email)){ echo '('.$invitado->email.')'; }else{ echo 'No tiene correo'; } ?></span>
                                        </div>
                                    <?php } } ?>
                                    </div>
                                <?php }else{ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay invitados</em></p>
                                </div>
                                <?php } ?>
                            </div>
                            <!--Temas-->
                            <div class="col-md-12">
                                <div class="form-group" style="background-color: #03113a;">
                                  <p class="text-white">Temas </p>
                                </div>
                                <?php $i=100; foreach($temas as $tema){ $i++; ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title titulo-tema">
                                            <a class="collapsed titulo-tema0" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>"><b><?=ucwords(strtolower($tema->titulo))?></b><i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?=$i?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                   <div class="project-section general-info text-left">
								                       <h3>Titulo</h3>
                                                       <p><?=$tema->titulo?></p>
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="project-section general-info text-left">
                                                        <h3>Descripción </h3>
                                                        <p><?=$tema->descripcion?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(count($temas) == 0){ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay Temas</em></p>
                                </div>
                                <?php } ?>
                            </div>
                            <!--Pendientes-->
                            <div class="col-md-12">
                                <div class="form-group" style="background-color: #03113a;">
                                  <p class="text-white">Pendientes </p>
                                </div>
                                <?php $i=1000; foreach($pendientes as $pendiente){ $i++; ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title titulo-tema">
                                            <a class="collapsed titulo-tema<?=$i?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>"><b>Pendiente #<span><?=$i-1000?></span></b><i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?=$i?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                   <div class="project-section general-info text-left">
								                       <h3>Fecha Ejecución</h3>
                                                       <p><?=$this->fecha(date('Y-m-d',strtotime($pendiente->fecha_ejecucion))).' '.date('(h:i:s A)',strtotime($pendiente->fecha_ejecucion))?></p>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                   <div class="project-section general-info text-left">
								                       <h3>Responsable</h3>
                                                       <?php
                                                            if($pendiente->tipo_responsable == "usuario sistema"){
                                                                $responsable = User::find($pendiente->id_responsable);
                                                                $nombre = explode(' ',$responsable->nombres);
                                                                $apellido = explode(' ',$responsable->apellidos);
                                                                $nombre_completo = $nombre[0].' '.$apellido[0];
                                                            }else{
                                                                $responsable = Invitado::find($pendiente->id_responsable);
                                                                $nombre_completo = $responsable->nombre;
                                                            }
                                                       ?>
                                                       <p><?=$nombre_completo?></p>
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="project-section general-info text-left">
                                                        <h3>Descripción </h3>
                                                        <p><?=$pendiente->descripcion?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(count($pendientes)==0){ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay Pendientes</em></p>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
<?php               $data['ver'] = ob_get_contents();
                    ob_end_clean();
                }
                break;
            /*Nuevos inputs para invitado externo*/
            case 1:
                $data['invitados_externos1'] = DB::select('SELECT DISTINCT(nombre) FROM oportunidad_actas_invitados WHERE tipo LIKE "usuario externo" AND nombre IS NOT NULL');
                $data['invitados_externos2'] = DB::select('SELECT DISTINCT(nombre) FROM oportunidad_actas_invitados WHERE tipo LIKE "usuario externo" AND nombre IS NOT NULL');
                $data['invitados_externos'] = array_merge($data['invitados_externos1'], $data['invitados_externos2']);
                ob_start(); ?>
<div class="row" id="invitadoexterno<?=$request->numero?>">
    <div class="col-md-5">
        <div class="form-group">
            <p>Nombre </p>
            <label for="recipient-name" class="form-control-label"></label>
            <input type="text" class="form-control inv-externo" name="invitadoadicional[<?=$request->numero?>][nombre]" onchange="invitado_externo(this.value,'invitadoadicional_<?=$request->numero?>_correo')" requerid="requerid" list="invitados_externos<?=$request->numero?>">
            <datalist id="invitados_externos<?=$request->numero?>">
             <?php foreach($data['invitados_externos'] as $invitado){ ?>
              <option value="<?=$invitado->nombre?>">
             <?php } ?>
            </datalist>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <p>Correo </p>
            <label for="recipient-name" class="form-control-label"></label>
            <input type="email" class="form-control" id="invitadoadicional_<?=$request->numero?>_correo" name="invitadoadicional[<?=$request->numero?>][correo]" requerid="requerid">
        </div>
    </div>
    <div class="col-md-2 eliminar-inv-adi">
        <a onclick="eliminar_invitado_adicional(<?=$request->numero?>)"><i class="fa fa-trash fa-2 eliminar-inv" aria-hidden="true" title="Eliminar"></i></a>
    </div>
</div>
<?php               $data['html_invitado_externo'] = ob_get_contents();
                    ob_end_clean();
                break;
            case 2:
                $correo = DB::select('SELECT email FROM invitados WHERE nombre LIKE "'.$request->nombre.'"');
                if(isset($correo[0]->email)){
                    $data['correo'] = $correo[0]->email;
                }else{
                    $correo2 = DB::select('SELECT email FROM oportunidad_actas_invitados WHERE nombre LIKE "'.$request->nombre.'"');
                    if(isset($correo2[0]->email)){
                        $data['correo'] = $correo2[0]->email;
                    }else{
                        $data['correo'] = '';
                    }
                }
                break;
            /*Eliminar accion o tema*/
            case 3:
                if($request->tipo == "tema"){
                    $dato = Actas_tema::find($request->id);
                    if(isset($dato->id) && $dato->id != ""){
                        $dato->delete();
                        $data['msj'] = 'Eliminado el tema';
                    }else{
                        $data['msj'] = 'No se elimino el tema por que no existe en la base de datos';
                    }
                }else{
                    $dato = Actas_accione::find($request->id);
                    if(isset($dato->id) && $dato->id != ""){
                        $dato->delete();
                        $data['msj'] = 'Eliminado el pendiente';
                    }else{
                        $data['msj'] = 'No se elimino el pendiente por que no existe en la base de datos';
                    }
                }
                break;
            /*Eniar correo electronico a todos los invitados*/
            case 4:
                $reunion = Acta::find($request->id);
                $usuarios_sistema = DB::SELECT('SELECT (U.correo_corporativo) AS CORREO FROM invitados I INNER JOIN users U ON I.id_usuario = U.id WHERE I.id_acta = '.$request->id.' AND I.tipo = "usuario sistema" AND I.deleted_at IS NULL AND U.correo_corporativo IS NOT NULL');
                $usuarios_externos = DB::SELECT('SELECT (I.email) AS CORREO FROM invitados I WHERE I.id_acta = '.$request->id.' AND I.tipo = "usuario externo" AND I.deleted_at IS NULL AND I.email IS NOT NULL');
                $i=0;
                if(count($usuarios_sistema)>0){
                    foreach($usuarios_sistema as $usuario){
                        if($usuario->CORREO != ''){
                            $emails[$i] = $usuario->CORREO;
                            $i++;
                        }
                    }
                }
                if(count($usuarios_externos)>0){
                    foreach($usuarios_externos as $usuario){
                        if($usuario->CORREO != ''){
                            $emails[$i] = $usuario->CORREO;
                            $i++;
                        }
                    }
                }
                if(isset($emails)){
                    /*Envio de correos*/
                    setlocale(LC_ALL, "es_CO.UTF-8");
                    $fechareunion = strftime("%A %d %B %Y %I %M %p", strtotime($reunion->fecha));
                    $tituloAsunto = "Información de la reunión en ESSI el día ".$fechareunion;

                    if(isset($emails) && count($emails)>0){
                        $data = array('id' => $request->id, 'titulo_correo' => $tituloAsunto);
                        Mail::send('mail.mailactasreunionfinalizada', $data, function($message) use ($emails, $tituloAsunto){
                            $message->from('smart@bounces.smartessi.com', 'Smart Business ESSI');
                            $message->bcc($emails)->subject($tituloAsunto);
                        });
                    }
                    /*Fin Envio de correos*/
                    $data['msj'] = 'Enviado correctamente!';
                }else{
                    $data['msj'] = 'Error no hay correos!';
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }
    public function actasreunion($id){
        $data['venta'] = Venta::find($id);
        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72" AND deleted_at IS NULL');
        $actas = DB::select('SELECT *, IF(LENGTH(`nombre`)>17,CONCAT(SUBSTRING(`nombre`, 1, 14),"..."),`nombre`) AS nombre_abreviado FROM `actas` WHERE `id_oportunidad`= '.$id.' AND `deleted_at` IS NULL ORDER BY `id` ASC');
        $invitados = DB::select('SELECT * FROM `invitados` WHERE `id_acta` IN (SELECT `id` FROM `actas` WHERE `id_oportunidad`="'.$id.'")');
        $data['invitados_externos'] = DB::select('SELECT DISTINCT(nombre) FROM invitados WHERE tipo LIKE "usuario externo" AND nombre IS NOT NULL');
        /*$acciones['vigentes'] = DB::select('SELECT *, IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` IS NULL AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$id.'")');
        $acciones['realizado'] = DB::select('SELECT *, IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Realizado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$id.'")');
        $acciones['cancelado'] = DB::select('SELECT *, IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Cancelado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$id.'")');*/
        $actas2 = Acta::where('id_oportunidad',$id)->get();
        ob_start();
        $data['numero_vigentes']=0;
        foreach($actas2 as $acta){
            $query = 'IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido';
            $vigentes = DB::select('SELECT A.*, IFNULL(A.estado, "vigente") AS ESTADO , '.$query.' FROM actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($vigentes as $vigente){
                if(isset($vigente->ESTADO) && $vigente->ESTADO == "vigente"){
                    $data['numero_vigentes']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_vigentes']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($vigente->id_acta)>9){ if(($vigente->id_acta)>99){ if(($vigente->id_acta)>999){ $numero=$vigente->id_acta; }else{ $numero='0'.$vigente->id_acta; } }else{  $numero='00'.$vigente->id_acta; } }else{ $numero='000'.$vigente->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigente->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigente->fecha_ejecucion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($vigente->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$vigente->responsable_nombre?> <?=$vigente->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$vigente->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$vigente->descripcion?>" style="cursor:pointer;"><?=$vigente->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($vigente->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$vigente->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-2 encabezado">
        <a class="realizado" data-toggle="tooltip" data-placement="top" title="Realizado" onclick="marcar(<?=$vigente->id?>,'realizado')"><i class="fa fa-check-circle-o fa-2"></i></a>
        <a class="aplazar" data-toggle="tooltip" data-placement="top" title="Aplazar" onclick="marcar(<?=$vigente->id?>,'aplazar')"><i class="fa fa-repeat fa-2"></i></a>
        <a class="cancelar" data-toggle="tooltip" data-placement="top" title="Cancelar" onclick="marcar(<?=$vigente->id?>,'cancelar')"><i class="fa fa-times-circle-o fa-2"></i></a>
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$vigente->id?>,'<?=$numero?>')"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['vigentes'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_realizado']=0;
        foreach($actas as $acta){
            $realizados = DB::select('SELECT *, '.$query.' FROM actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($realizados as $realizado){
                if(isset($realizado->estado) && $realizado->estado == 'Realizado'){
                    $data['numero_realizado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_realizado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($realizado->id_acta)>9){ if(($realizado->id_acta)>99){ if(($realizado->id_acta)>999){ $numero=$realizado->id_acta; }else{ $numero='0'.$realizado->id_acta; } }else{  $numero='00'.$realizado->id_acta; } }else{ $numero='000'.$realizado->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizado->fecha_creacion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizado->fecha_ejecucion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizado->fecha_accion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($realizado->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$realizado->responsable_nombre?> <?=$realizado->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$realizado->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$realizado->descripcion?>" style="cursor:pointer;"><?=$realizado->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($realizado->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$realizado->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$realizado->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['realizados'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_cancelado']=0;
        foreach($actas as $acta){
            $cancelados = DB::select('SELECT A.*, '.$query.' FROM actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($cancelados as $cancelado){
                if(isset($cancelado->estado) && $cancelado->estado == 'Cancelado'){
                    $data['numero_cancelado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_cancelado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($cancelado->id_acta)>9){ if(($cancelado->id_acta)>99){ if(($cancelado->id_acta)>999){ $numero=$cancelado->id_acta; }else{ $numero='0'.$cancelado->id_acta; } }else{  $numero='00'.$cancelado->id_acta; } }else{ $numero='000'.$cancelado->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($cancelado->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($cancelado->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$cancelado->responsable_nombre?> <?=$cancelado->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$cancelado->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-4 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$cancelado->descripcion?>" style="cursor:pointer;"><?=$cancelado->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($cancelado->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$cancelado->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$cancelado->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['cancelados'] = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('actareunion.acta',['data' => $data, 'id' => $id, 'usuarios' => $usuarios, 'actas' => $actas, 'invitados' => $invitados]);
    }

    public function consultarinvitado($id){
        $usuario=User::findorfail($id);
        $nombre=explode(" ",$usuario->nombres);
        $apellido=explode(" ",$usuario->apellidos);
        $html='<li id="invitado'.$usuario->id.'">
                    <img src="/images/file/clientes/'.$usuario->foto.'" class="img-circle" alt="Avatar">
                    <p><a href="#"><strong>'.$nombre[0].' '.$apellido[0].'</strong></a></p>
                    <span class="text-muted">'.$usuario->cargo.' <i class="fa fa-times-circle quitar" onclick="eliminar('.$usuario->id.')" title="Eliminar"></i></span>
                </li>';
        return \Response::json(['html' => $html]);
    }

    public function guardarcita(Request $request){
        $dato = new Acta;
        $dato->nombre = $request->titulo;
        $dato->estado = "citada";
        $dato->descripcion = $request->descripcion;
        $dato->fecha = $request->fecha.':00';
        $dato->lugar = $request->lugar;
        $dato->id_oportunidad = $request->id_oportunidad;
        $dato->save();
        $id = $dato->id;

        $invitados=$request->invitados;
        $invitado=explode('%%',$invitados);
        $i=0;
        foreach($invitado as $inv){
            if($inv!=''){
                $dato = new Invitado;
                $dato->tipo = "usuario sistema";
                $dato->id_usuario = $inv;
                $dato->id_acta = $id;
                $dato->save();
                /*captura correo*/
                $user_invi = User::find($inv);
                if($user_invi->correo_corporativo != ''){
                    $emails[$i] = $user_invi->correo_corporativo;
                    $i++;
                }
            }
        }

        $invitados_externos=$request->invitadoadicional;
        if(count($invitados_externos)>0){
            foreach($invitados_externos as $ie){
                $dato = new Invitado;
                $dato->tipo = "usuario externo";
                $dato->nombre = $ie['nombre'];
                $emails[$i] = $ie['correo'];
                $dato->email = $ie['correo'];
                $dato->id_acta = $id;
                $dato->save();
                $i++;
            }
        }
        /*Envio de correos*/
        setlocale(LC_ALL, "es_CO.UTF-8");
        $fechareunion = strftime("%A %d %B %Y %I %M %p", strtotime($request->fecha.':00'));
        $tituloAsunto = "Reunión en ESSI el día ".$fechareunion;

        if(isset($emails) && count($emails)>0){
            $data = array('id' => $id);
            Mail::send('mail.mailactas', $data, function($message) use ($emails, $tituloAsunto){
                $message->from('smart@bounces.smartessi.com', 'Smart Business ESSI');
                $message->bcc($emails)->subject($tituloAsunto);
            });
        }
        /*Fin Envio de correos*/

        $datos=$this->listado($request->id_oportunidad);
        return view('actareunion.acta',['data' => $datos['data'], 'id' => $datos['id'], 'usuarios' => $datos['usuarios'], 'actas' =>$datos['actas'], 'invitados' => $datos['invitados']]);
        /*
        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72"');
        $actas = DB::select('SELECT * FROM `actas` WHERE `id_oportunidad`="'.$request->id_oportunidad.'" AND `deleted_at` IS NULL ORDER BY `id` ASC');
        $invitados = DB::select('SELECT * FROM `invitados` WHERE `id_acta` IN (SELECT `id` FROM `actas` WHERE `id_oportunidad`="'.$request->id_oportunidad.'")');
        $acciones['vigentes'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` IS NULL AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
        $acciones['realizado'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Realizado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
        $acciones['cancelado'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Cancelado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
        /*Consulta para activar el item del menu correspondiente*/
        /*$query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        /*return view('actareunion.acta',['id' => $request->id_oportunidad, 'usuarios' => $usuarios, 'actas' => $actas, 'invitados' => $invitados, 'acciones' => $acciones]);*/
    }

    public function asistencia(Request $request){
        $usuarios_sistema= DB::select('SELECT *, (SELECT `id` FROM `invitados` WHERE `id_acta`="'.$request->id.'" AND `tipo`="usuario sistema" AND U.id = id_usuario ) AS id_invitado FROM `users` AS U WHERE `id` IN (SELECT i.`id_usuario` FROM `invitados` AS i WHERE i.`id_acta`="'.$request->id.'" AND i.`tipo`="usuario sistema")');
        $usuarios_externos= DB::select('SELECT * FROM `invitados` WHERE `id_acta`="'.$request->id.'" AND `tipo`="usuario externo"');
        $listado_invitados= DB::select('SELECT * FROM `users` WHERE `id` IN (SELECT `id_usuario` FROM `invitados` WHERE `id_acta`="'.$request->id.'" AND  `tipo`="usuario sistema")');
        $html='';
        $i=0;
        foreach($usuarios_sistema as $user){
            $nombre=explode(' ',$user->nombres);
            $apellido=explode(' ',$user->apellidos);
           $html.='<div class="col-md-1 imagen-asis">
                    <img src="'.$request->url.'/images/file/clientes/'.$user->foto.'" class="img-circle img-asistencia" alt="Avatar">
                </div>
                <div class="col-md-3 nombre-asis">
                   <label class="control-inline fancy-checkbox">
                        <input type="hidden" id="asistenciavalor'.$user->id_invitado.'" name="asistencia['.$i.'][valor]" value="No">
                        <input type="hidden" id="asistenciauser'.$user->id_invitado.'" name="asistencia['.$i.'][user]" value="'.$user->id_invitado.'">
                        <input type="hidden" id="asistenciahorallegada'.$user->id_invitado.'" name="asistencia['.$i.'][horallegada]" value="00:00:00">
                        <input type="checkbox" class="lista-usuarios-asistencia" id="asistio'.$user->id_invitado.'">
                        <span>'.$nombre[0].' '.$apellido[0].'</span>
                   </label>
                </div>
                <div class="col-md-2 hora_llegada" id="hora_llegada'.$user->id_invitado.'">

                </div>';
            $i++;
        }
        foreach($usuarios_externos as $user){
          $html.='<div class="col-md-1 imagen-asis">
                    <img src="'.$request->url.'/images/externo.jpg" class="img-circle img-asistencia" alt="Avatar">
                </div>
                <div class="col-md-3 nombre-asis">
                   <label class="control-inline fancy-checkbox">
                        <input type="hidden" id="asistenciavalor'.$user->id.'" name="asistencia['.$i.'][valor]" value="No">
                        <input type="hidden" id="asistenciauser'.$user->id.'" name="asistencia['.$i.'][user]" value="'.$user->id.'">
                        <input type="hidden" id="asistenciahorallegada'.$user->id.'" name="asistencia['.$i.'][horallegada]" value="00:00:00">
                        <input type="checkbox" class="lista-usuarios-asistencia" id="asistio'.$user->id.'">
                        <span>'.$user->nombre.'</span>
                   </label>
                </div>
                <div class="col-md-2 hora_llegada" id="hora_llegada'.$user->id.'">

                </div>';
            $i++;
        }
        $html_responsable='';
        $select_responsable='<p>Responsable </p>
        <label for="message-text" class="form-control-label"></label>
        <select class="form-control responsable" name="pendiente[0][responsable]">
            <option value="">Seleccione un responsable</option>';
        foreach($listado_invitados as $usuario){
            $select_responsable.='<option value="usuario sistema¬'.$usuario->id.'">'.$usuario->name.'</option>';
            $html_responsable.='<option value="usuario sistema¬'.$usuario->id.'">'.$usuario->name.'</option>';
        }
        foreach($usuarios_externos as $usuario1){
            $select_responsable.='<option value="usuario externo¬'.$usuario1->id.'">'.$usuario1->nombre.'</option>';
            $html_responsable.='<option value="usuario externo¬'.$usuario1->id.'">'.$usuario1->nombre.'</option>';
        }
        $select_responsable.='</select>';
        return \Response::json(['html' => $html, 'select_responsable' => $select_responsable, 'html_responsable' => $html_responsable]);
    }

    public function savecontinuar(Request $request){
        $dato=Acta::findorfail($request->id);
        $dato->estado = "realizada";
        if(isset($request->fecha) && $request->fecha != ''){
        $dato->fecha_inicio_real = $request->fecha;
        }
        $dato->save();

        $asistencia=$request->asistencia;
        if($asistencia!=""){
            foreach($asistencia as $as){
                $dato=Invitado::findorfail($as['user']);
                $dato->asistio = $as['valor'];
                $dato->hora_llegada = date("H:i:s",strtotime($as['horallegada']));
                $dato->save();
            }
        }
        $data['id_temas'] = '';
        $temas=$request->temas;
        foreach($temas as $index => $tema){
            if($tema['titulo'] != '' && $tema['descripcion'] != "" && $request['id'] != ""){
                if($tema['id'] == ''){
                    $dato = new Actas_tema;
                }else{
                    $dato = Actas_tema::find($tema['id']);
                }
                $dato->titulo = $tema['titulo'];
                $dato->descripcion = $tema['descripcion'];
                $dato->id_acta = $request['id'];
                $dato->save();
                $data['id_temas'][$index] = $dato->id;
            }
        }

        $data['id_acciones'] = '';
        $acciones=$request->pendiente;
        foreach($acciones as $index => $accion){
            if($accion['fecha_creacion'] != '' && $accion['responsable'] != '' && $accion['descripcion'] != ''){
                if($accion['id'] == ''){
                    $dato = new Actas_accione;
                }else{
                    $dato = Actas_accione::find($accion['id']);
                }
                $dato->fecha_creacion = $accion['fecha_creacion'];
                $dato->fecha_ejecucion = $accion['fecha_ejecucion'];
                $responsable=explode('¬',$accion['responsable']);
                $dato->id_responsable = $responsable[1];
                $dato->tipo_responsable = $responsable[0];
                $dato->descripcion = $accion['descripcion'];
                $dato->id_acta = $request->id;
                $dato->save();
                $data['id_acciones'][$index] = $dato->id;
            }
        }
        $data['msj'] = 'Guardado correctamente!';
        return \Response::json(['data' => $data]);
    }

    public function iniciarreunion(Request $request){
        $dato=Acta::findorfail($request->id);
        $dato->estado = "realizada";
        $dato->fecha_inicio_real = $request->fecha;
        $dato->save();

        $asistencia=$request->asistencia;
        if($asistencia!=""){
            foreach($asistencia as $as){
                $dato=Invitado::findorfail($as['user']);
                $dato->asistio = $as['valor'];
                $dato->hora_llegada = date("H:i:s",strtotime($as['horallegada']));
                $dato->save();
            }
        }

        $temas=$request->temas;
        foreach($temas as $index => $tema){
            if($tema['titulo'] != '' && $tema['descripcion'] != "" && $request['id'] != ""){
                if($tema['id'] == ''){
                    $dato = new Actas_tema;
                }else{
                    $dato = Actas_tema::find($tema['id']);
                }
                $dato->titulo = $tema['titulo'];
                $dato->descripcion = $tema['descripcion'];
                $dato->id_acta = $request['id'];
                $dato->save();
            }
        }
        $acciones=$request->pendiente;
        foreach($acciones as $index => $accion){
            if($accion['fecha_creacion'] != '' && $accion['responsable'] != '' && $accion['descripcion'] != ''){
                if($accion['id'] == ''){
                    $dato = new Actas_accione;
                }else{
                    $dato = Actas_accione::find($accion['id']);
                }
                $dato->fecha_creacion = $accion['fecha_creacion'];
                $dato->fecha_ejecucion = $accion['fecha_ejecucion'];
                $responsable=explode('¬',$accion['responsable']);
                $dato->id_responsable = $responsable[1];
                $dato->tipo_responsable = $responsable[0];
                $dato->descripcion = $accion['descripcion'];
                $dato->id_acta = $request->id;
                $dato->save();
            }
        }
        $datos=$this->listado($request->id_oportunidad);
        return view('actareunion.acta',['data' => $datos['data'], 'id' => $datos['id'], 'usuarios' => $datos['usuarios'], 'actas' =>$datos['actas'], 'invitados' => $datos['invitados']]);
        /*$usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72"');
        $actas = DB::select('SELECT * FROM `actas` WHERE `id_oportunidad`="'.$request->id_oportunidad.'" AND `deleted_at` IS NULL ORDER BY `id` ASC');
        $invitados = DB::select('SELECT * FROM `invitados` WHERE `id_acta` IN (SELECT `id` FROM `actas` WHERE `id_oportunidad`="'.$request->id_oportunidad.'")');
        $acciones['vigentes'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` IS NULL AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
        $acciones['realizado'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Realizado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
        $acciones['cancelado'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Cancelado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');*/
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        /*return view('actareunion.acta',['id' => $request->id_oportunidad, 'usuarios' => $usuarios, 'actas' => $actas, 'invitados' => $invitados, 'acciones' => $acciones]);*/
    }

    public function marcarrealizada(Request $request){
        $dato=Actas_accione::findorfail($request->id);
        $dato->estado="Realizado";
        $dato->fecha_accion=date('Y-m-d H:i:s');
        $dato->save();

        $dato= New Actas_acciones_comentario;
        $dato->comentario=$request->comentario;
        $dato->estado="Realizado";
        $dato->fecha=date('Y-m-d H:i:s');
        $dato->id_user=Auth::user()->id;
        $dato->id_actas_acciones=$request->id;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function aplazar(Request $request){
        $dato=Actas_accione::findorfail($request->id);
        if($dato->aplazado > 0){
            $dato->aplazado=($dato->aplazado)+1;
        }else{
            $dato->aplazado=1;
        }
        $dato->fecha_ejecucion=$request->nueva;
        $dato->save();

        $dato= New Actas_acciones_comentario;
        $dato->comentario=$request->comentario;
        $dato->estado="Aplazado";
        $dato->fecha=date('Y-m-d H:i:s');
        $dato->id_user=Auth::user()->id;
        $dato->id_actas_acciones=$request->id;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function marcarcancelada(Request $request){
        $dato=Actas_accione::findorfail($request->id);
        $dato->estado="Cancelado";
        $dato->fecha_accion=date('Y-m-d H:i:s');
        $dato->save();

        $dato= New Actas_acciones_comentario;
        $dato->comentario=$request->comentario;
        $dato->estado="Cancelado";
        $dato->fecha=date('Y-m-d H:i:s');
        $dato->id_user=Auth::user()->id;
        $dato->id_actas_acciones=$request->id;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function eliminarcitada($id){
        $dato=Acta::findorfail($id);
        $dato->deleted_at=date('Y-m-d H:i:s');
        $data['id'] = $dato->id_oportunidad;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!','data' => $data]);
    }

    public function datos_acta($id){
        $dato=Acta::findorfail($id);
        $invitados = DB::select('SELECT *, (SELECT `foto` FROM `users` WHERE `id`=I.`id_usuario`) AS foto, (SELECT CONCAT(SUBSTRING_INDEX(UU.`nombres`, " ", 1 ), " ", SUBSTRING_INDEX(UU.`apellidos`, " ", 1 )) FROM `users` AS UU WHERE `id`=I.`id_usuario`) AS nombre_funcionario, (SELECT `cargo` FROM `users` WHERE `id`=I.`id_usuario`) AS cargo_funcionario FROM `invitados` AS I WHERE I.`id_acta` IN (SELECT A.`id` FROM `actas` AS A WHERE A.`id`="'.$id.'")');

        return \Response::json(['acta' => $dato, 'invitados' => $invitados]);
    }

    public function consultarinvitado_editar($id){
        $usuario=User::findorfail($id);
        $nombre=explode(" ",$usuario->nombres);
        $apellido=explode(" ",$usuario->apellidos);
        $html='<li id="invitado_editar'.$usuario->id.'">
                    <img src="/images/file/clientes/'.$usuario->foto.'" class="img-circle" alt="Avatar">
                    <p><a href="#"><strong>'.$nombre[0].' '.$apellido[0].'</strong></a></p>
                    <span class="text-muted">'.$usuario->cargo.' <i class="fa fa-times-circle quitar" onclick="eliminar_editar('.$usuario->id.')" title="Eliminar"></i></span>
                </li>';
        return \Response::json(['html' => $html]);
    }

    public function editarcita(Request $request){
        $dato = Acta::findorfail($request->id_cita);
        $dato->nombre = $request->titulo;
        $dato->estado = "citada";
        $dato->descripcion = $request->descripcion;
        $dato->fecha = date("Y-m-d H:i:s",strtotime($request->fecha));
        $dato->lugar = $request->lugar;
        $dato->id_oportunidad = $request->id_oportunidad;
        $dato->save();
        $id = $dato->id;

        /*eliminar invitados usuario sistema */
        $usuarios_sistemas = Invitado::where('id_acta', $id)->where('tipo', "usuario sistema")->get();
        foreach($usuarios_sistemas as $usuario){
            Invitado::destroy($usuario->id);
        }
        $invitados=$request->invitados;
        $invitado=explode('%%',$invitados);

        foreach($invitado as $inv){
            if($inv!=""){
                $dato = new Invitado;
                $dato->tipo = "usuario sistema";
                $dato->id_usuario = $inv;
                $dato->id_acta = $id;
                $dato->save();
            }
        }

        /*eliminar invitados usuario externo */
        $usuarios_externos = Invitado::where('id_acta', $id)->where('tipo', "usuario externo")->get();
        foreach($usuarios_externos as $usua){
            Invitado::destroy($usua->id);
        }
        $invitados_externos=$request->invitadoadicional;
        $i=0;
        if(isset($invitados_externos)){
            foreach($invitados_externos as $ie){
                $dato = new Invitado;
                $dato->tipo = "usuario externo";
                $dato->nombre = $ie['nombre'];
                $emails[$i] = $ie['correo'];
                $dato->email = $ie['correo'];
                $dato->id_acta = $id;
                $dato->save();
                $i++;
            }
        }
        $datos=$this->listado($request->id_oportunidad);
        return view('actareunion.acta',['data' => $datos['data'], 'id' => $datos['id'], 'usuarios' => $datos['usuarios'], 'actas' =>$datos['actas'], 'invitados' => $datos['invitados']]);
//        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72"');
//        $actas = DB::select('SELECT * FROM `actas` WHERE `id_oportunidad`="'.$request->id_oportunidad.'" AND `deleted_at` IS NULL ORDER BY `id` ASC');
//        $invitados = DB::select('SELECT * FROM `invitados` WHERE `id_acta` IN (SELECT `id` FROM `actas` WHERE `id_oportunidad`="'.$request->id_oportunidad.'")');
//        $acciones['vigentes'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` IS NULL AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
//        $acciones['realizado'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Realizado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
//        $acciones['cancelado'] = DB::select('SELECT *, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido FROM `actas_acciones` AS A WHERE A.`estado` = "Cancelado" AND A.`id_acta` IN (SELECT AC.`id` FROM `actas` AS AC WHERE AC.`id_oportunidad`="'.$request->id_oportunidad.'")');
//        /*Consulta para activar el item del menu correspondiente*/
//        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
//        $data['item_menu'] = DB::select($query);
//        if(count($data['item_menu']) > 0){
//            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
//        }else{
//            $data['item_menu']='';
//        }
//        /*Fin Consulta para activar el item del menu correspondiente*/
//        return view('actareunion.acta',['id' => $request->id_oportunidad, 'usuarios' => $usuarios, 'actas' => $actas, 'invitados' => $invitados, 'acciones' => $acciones]);
    }

    public function consultarcomentario($id){
        $comentarios= DB::select('SELECT AAC.* FROM actas_acciones_comentarios AAC INNER JOIN actas_acciones A ON AAC.id_actas_acciones = A.id WHERE A.id = '.$id.' ORDER BY AAC.fecha DESC LIMIT 0, 5');
        $html='';
        foreach($comentarios as $comentario){
            $user = User::findorfail($comentario->id_user);
            $nombres=explode(' ',$user->nombres);
            $apellidos=explode(' ',$user->apellidos);
            $nombre=$nombres[0].' '.$apellidos[0];
            $html.='<div class="row">
                        <div class="col-md-3">';
            if($comentario->estado == "Realizado"){
                $html.='<span class="label label-medium">Realizada</span>';
            }else if($comentario->estado == "Aplazado"){
                $html.='<span class="label label-emergency">Aplazada</span>';
            }else{
                $html.='<span class="label label-critical">Cancelada</span>';
            }
             $html.='</div>
                        <div class="col-md-6">
                            <p class="comentario-text"><a href="#">'.$nombre.':</a> '.$comentario->comentario.' <br><span class="timestamp hace-comentario">'.date("d/m/Y h:i:s A",strtotime($comentario->fecha)).'</span></p>
                        </div>
                        <div class="col-md-3">
                            <img src="/images/file/clientes/'.$user->foto.'" class="img-circle img-comentario" alt="Avatar">
                        </div>
                    </div>';
        }
        return \Response::json(['html' => $html]);
    }
    function fecha($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $f=explode('-',$fecha);
            $fecha_formato = $f[2].'/'.$meses[intval($f[1])].'/'.$f[0];
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }
    }

    protected function listado($id){
        $data['venta'] = Venta::find($id);
        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72" AND deleted_at IS NULL');
        $actas = DB::select('SELECT *, IF(LENGTH(`nombre`)>17,CONCAT(SUBSTRING(`nombre`, 1, 14),"..."),`nombre`) AS nombre_abreviado FROM `actas` WHERE `id_oportunidad`= '.$id.' AND `deleted_at` IS NULL ORDER BY `id` ASC');
        $invitados = DB::select('SELECT * FROM `invitados` WHERE `id_acta` IN (SELECT `id` FROM `actas` WHERE `id_oportunidad`="'.$id.'")');
        $data['invitados_externos'] = DB::select('SELECT DISTINCT(nombre) FROM invitados WHERE tipo LIKE "usuario externo" AND nombre IS NOT NULL');
        $actas2 = Acta::where('id_oportunidad',$id)->get();
        ob_start();
        $data['numero_vigentes']=0;
        $query = 'IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido';
        foreach($actas2 as $acta){
            $vigentes = DB::select('SELECT A.*, IFNULL(A.estado, "vigente") AS ESTADO , '.$query.' FROM actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC LIMIT 0,1');
            if(isset($vigentes[0]->ESTADO) && $vigentes[0]->ESTADO == "vigente"){
                $data['numero_vigentes']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_vigentes']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($vigentes[0]->id_acta)>9){ if(($vigentes[0]->id_acta)>99){ if(($vigentes[0]->id_acta)>999){ $numero=$vigentes[0]->id_acta; }else{ $numero='0'.$vigentes[0]->id_acta; } }else{  $numero='00'.$vigentes[0]->id_acta; } }else{ $numero='000'.$vigentes[0]->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigentes[0]->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigentes[0]->fecha_ejecucion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($vigentes[0]->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$vigentes[0]->responsable_nombre?> <?=$vigentes[0]->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$vigentes[0]->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$vigentes[0]->descripcion?>" style="cursor:pointer;"><?=$vigentes[0]->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($vigentes[0]->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$vigentes[0]->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-2 encabezado">
        <a class="realizado" data-toggle="tooltip" data-placement="top" title="Realizado" onclick="marcar(<?=$vigentes[0]->id?>,'realizado')"><i class="fa fa-check-circle-o fa-2"></i></a>
        <a class="aplazar" data-toggle="tooltip" data-placement="top" title="Aplazar" onclick="marcar(<?=$vigentes[0]->id?>,'aplazar')"><i class="fa fa-repeat fa-2"></i></a>
        <a class="cancelar" data-toggle="tooltip" data-placement="top" title="Cancelar" onclick="marcar(<?=$vigentes[0]->id?>,'cancelar')"><i class="fa fa-times-circle-o fa-2"></i></a>
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$acta->id?>,'<?=$numero?>')"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
            }
        }
        $data['vigentes'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_realizado']=0;
        foreach($actas as $acta){
            $realizados = DB::select('SELECT *, '.$query.' FROM actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC LIMIT 0,1');
            if(isset($realizados[0]->estado) && $realizados[0]->estado == 'Realizado'){
                $data['numero_realizado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_realizado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($realizados[0]->id_acta)>9){ if(($realizados[0]->id_acta)>99){ if(($realizados[0]->id_acta)>999){ $numero=$realizados[0]->id_acta; }else{ $numero='0'.$realizados[0]->id_acta; } }else{  $numero='00'.$realizados[0]->id_acta; } }else{ $numero='000'.$realizados[0]->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizados[0]->fecha_creacion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizados[0]->fecha_ejecucion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizados[0]->fecha_accion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($realizados[0]->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$realizados[0]->responsable_nombre?> <?=$realizados[0]->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$realizados[0]->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$realizados[0]->descripcion?>" style="cursor:pointer;"><?=$realizados[0]->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($realizados[0]->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$realizados[0]->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$acta->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
            }
        }
        $data['realizados'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_cancelado']=0;
        foreach($actas2 as $acta){
            $cancelados = DB::select('SELECT A.*, '.$query.' FROM actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($cancelados as $cancelado){
                if(isset($cancelado->estado) && $cancelado->estado == 'Cancelado'){
                    $data['numero_cancelado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_cancelado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($cancelado->id_acta)>9){ if(($cancelado->id_acta)>99){ if(($cancelado->id_acta)>999){ $numero=$cancelado->id_acta; }else{ $numero='0'.$cancelado->id_acta; } }else{  $numero='00'.$cancelado->id_acta; } }else{ $numero='000'.$cancelado->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($cancelado->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($cancelado->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$cancelado->responsable_nombre?> <?=$cancelado->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$cancelado->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-4 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$cancelado->descripcion?>" style="cursor:pointer;"><?=$cancelado->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($cancelado->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$cancelado->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$cancelado->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['cancelados'] = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        $datos['data'] = $data;
        $datos['id'] = $id;
        $datos['usuarios'] = $usuarios;
        $datos['actas'] = $actas;
        $datos['invitados'] = $invitados;
        return $datos;
    }
}
