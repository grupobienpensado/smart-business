<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\FeriasConference;
use App\Ferias_multimedias;
use App\Ferias_conferences_presupuesto_administrable;
use App\FeriasInvitado;
use App\FeriasConferencesComentario;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
date_default_timezone_set("America/Bogota");

class ConferenciaController extends Controller
{
    public function conferencia($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $dato=Feria::findOrFail($id);
        $conferencias=FeriasConference::where("id_feria",$id)->get()->sortBy('fecha');
        $i=0;
        $menu = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id, 4);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.conferencia',['data' => $data, 'dato' => $dato, 'conferencias' => $conferencias, 'i' => $i,'menu' => $menu]);
    }

    public function guardarconferencista(Request $request){
        $dato = FeriasConference::findOrFail($request->id);
        $foto_conferencista = "null";
        if (isset($request->fotoconferencista_values)) {
            if(!empty($request->fotoconferencista_values)){
                $imagen = json_decode($request->fotoconferencista_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('conferencista')->put($nombre,  $foto);
                $foto_conferencista = $nombre;
            }
        }
        if($dato->foto_conferencista == ''){
            $dato->foto_conferencista=$foto_conferencista;
        }else{
            $dato->foto_conferencista=$dato->foto_conferencista.'%%'.$foto_conferencista;
        }

        $conferencistas=$dato->conferencistas;
        if($dato->conferencistas == ''){
            $dato->conferencistas = $request->conferencista;
        }else{
            $dato->conferencistas = $conferencistas.'%%'.$request->conferencista;
        }

        $dato->save();
        $f=$request->f;
        $html='<div class="row fila-conferencista f-conferencista'.$request->id.'" id="fila_conferencista'.$f.'_'.$request->id.'">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">';
        if($foto_conferencista == 'null'){
            $html.='<img src="'.$request->direccion.'/storage/ferias/conferencista/noImagen.png" alt="Avatar" class="avatar img-circle img-lista">';
        }else{
            $html.='<img src="'.$request->direccion.'/storage/ferias/conferencista/'.$foto_conferencista.'" alt="Avatar" class="avatar img-circle img-lista">';
        }
        $html.='</div>
                    <div class="col-md-7 nombre-conferencista">
                        <a>'.$request->conferencista.'</a>
                    </div>
                    <div class="col-md-1">
                        <i class="fa fa-trash eliminar_conferencista" aria-hidden="true" title="Eliminar '.$request->conferencista.'" onclick="eliminar_conferencista(\''.$request->conferencista.'\',\''.$request->id.'\',\''.$f.'\')"></i>
                    </div>
                </div>';
        return \Response::json(['msj' => 'Guardado correctamente!', 'html' => $html]);
    }

    public function eliminarconferencista(Request $request){
        $dato = FeriasConference::findOrFail($request->id_conferencia);
        $conferencistas=explode('%%',$dato->conferencistas);
        $i=0;
        $interno=0;
        $nuevo='';
        foreach($conferencistas as $conferencista){
            if($conferencista == $request->conferencista){
                $f=$i;
            }else{
                if($interno==0){
                    $nuevo.=$conferencista;
                }else{
                    $nuevo.='%%'.$conferencista;
                }
                $interno++;
            }
            $i++;
        }
        $dato->conferencistas=$nuevo;

        $foto_conferencistas=explode('%%',$dato->foto_conferencista);
        $y=0;
        $nuevo_foto='';
        $interno=0;
        foreach($foto_conferencistas as $foto){
            if($y!=$f){
                if($interno==0){
                    $nuevo_foto.=$foto;
                }else{
                    $nuevo_foto.='%%'.$foto;
                }
                $interno++;
            }else{
                if($foto!='null'){
                     Storage::delete('ferias/conferencista/'.$foto);
                 }
            }
            $y++;
        }
        $dato->foto_conferencista=$nuevo_foto;

        $dato->save();
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }

    public function eliminartema(Request $request){
        $dato = FeriasConference::findOrFail($request->id_conferencia);
        $viejo=$dato->temas;
        $temas=explode('%%',$viejo);
        $nuevo='';
        $i=0;
        $eliminarseparador="no";
        if(count($temas)==2){
            $eliminarseparador="si";
        }
        foreach($temas as $tema){
            if($tema != $request->tema){
                if($i==0){
                    $nuevo.=$tema;
                }else{
                    $nuevo.='%%'.$tema;
                }
            }
            $i++;
        }
        if($eliminarseparador=="si"){
            $nuevo=str_replace("%%","",$nuevo);
        }
        $dato->temas=$nuevo;
        $dato->save();

        $temas=explode('%%',$nuevo);
        $t=0;
        $html='';
        if($nuevo != ''){
            foreach($temas as $tema){
                $numero=$t+1;
                $html.='<div class="row fila-conferencista" id="fila_tema_'.$t.'_'.$request->id_conferencia.'">
                            <div class="col-md-2"></div>
                            <div class="col-md-1 tema-conferencia">'.$numero.'.</div>
                            <div class="col-md-7 tema-conferencia">
                                <a>'.$tema.'</a>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash eliminar_temas" aria-hidden="true" title="Eliminar '.$tema.'" onclick="eliminar_tema(\''.$tema.'\',\''.$request->id_conferencia.'\',\'fila_tema_'.$t.'_'.$request->id_conferencia.'\')"></i>
                            </div>
                        </div>';
                $t++;
            }
        }
        if($nuevo!=""){
            $total=count($temas);
        }else{
            $total=0;
        }

        return \Response::json(['msj' => 'Eliminado correctamente!', 'html' => $html, 'total' => $total]);
    }

    public function guardartemas(Request $request){
        $dato = FeriasConference::findOrFail($request->id);
        $temas=$request->tema;
        $viejo=$dato->temas;
        $nuevo='';
        $i=0;
        foreach($temas as $tema){
            if($viejo==''){
                if($i==0){
                    $nuevo.=$tema;
                }else{
                    $nuevo.='%%'.$tema;
                }
            }else{
                $nuevo.='%%'.$tema;
            }
            $i++;
        }
        $dato->temas=$viejo.$nuevo;
        $dato->save();

        $tems=explode('%%',$viejo.$nuevo);
        $t=0;
        $html='';
        foreach($tems as $tem){
            $numero=$t+1;
            $html.='<div class="row fila-conferencista" id="fila_tema_'.$t.'_'.$request->id.'">
                        <div class="col-md-2"></div>
                        <div class="col-md-1 tema-conferencia">'.$numero.'.</div>
                        <div class="col-md-7 tema-conferencia">
                            <a>'.$tem.'</a>
                        </div>
                        <div class="col-md-1">
                            <i class="fa fa-trash eliminar_temas" aria-hidden="true" title="Eliminar '.$tem.'" onclick="eliminar_tema(\''.$tem.'\',\''.$request->id.'\',\'fila_tema_'.$t.'_'.$request->id.'\')"></i>
                        </div>
                    </div>';
            $t++;
        }
        return \Response::json(['msj' => 'Guardado correctamente!', 'html' => $html, 'total' => count($tems)]);
    }

    public function guardardatosconferencia(Request $request){
        $campo=$request->campo;
        $dato = FeriasConference::findOrFail($request->id_conferencia);
        $dato->$campo = $request->cambio;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function guardarduracionconferencia(Request $request){
        $campo=$request->campo;
        if($campo == "hora"){
            $dato = FeriasConference::findOrFail($request->id_conferencia);
            $duracion=$dato->duracion;
            $minuto=explode(':',$duracion);
            $dato->duracion=$request->cambio.':'.$minuto[1];
            $dato->save();
        }else{
            $dato = FeriasConference::findOrFail($request->id_conferencia);
            $duracion=$dato->duracion;
            $hora=explode(':',$duracion);
            $dato->duracion=$hora[0].':'.$request->cambio;
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function crearconferencia($id){
        $dato = new FeriasConference;
        $dato->id_feria = $id;
        $dato->save();
        return \Response::json(['msj' => 'Creado correctamente!']);
    }

    public function presupuestoconferencia($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $data['menu'] = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id,4);
        $data['datos'] = Ferias_conferences_presupuesto_administrable::where('id_feria',$id)->get();
        $data['i']=0;
        $data['id']=$id;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.confpresuadmin', ['data' => $data]);
    }

    public function guardarferiaconferenciapresupuestoadmin(Request $request){
        $presupuestos=$request->presupuesto;
        foreach($presupuestos as $presupuesto){
            $dato=Ferias_conferences_presupuesto_administrable::findOrFail($presupuesto['id']);
            $dato->concepto=$presupuesto['concepto'];
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function eliminaritempresupuestoconferencia($id){
        $dato=Ferias_conferences_presupuesto_administrable::findOrFail($id);
        $dato->delete();
        return \Response::json(['msj' => 'Eliminado Correctamente!']);
    }

    public function crearitempresupuestoconferencia($id){
        $dato= new Ferias_conferences_presupuesto_administrable;
        $dato->id_feria=$id;
        $dato->save();
        $id = $dato->id;
        return \Response::json(['id' => $id]);
    }

    public function presupuestoconferenciacomparar($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $data['menu'] = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id,4);
        $data['totales'] = DB::select('SELECT SUM(`presupuesto`) AS Presupuesto, SUM(`real`) AS vReal, (SUM(`presupuesto`)-SUM(`real`)) AS Ajuste FROM `ferias_conferences_presupuesto_administrables` WHERE `id_feria`="'.$id.'"');
        $data['items'] = DB::select('SELECT *, (`presupuesto` - `real`) AS ajuste FROM `ferias_conferences_presupuesto_administrables` WHERE `id_feria`="'.$id.'"');
        $data['id']=$id;
        $data['nombre'] = Auth::user()->nombres.' '.Auth::user()->apellidos;
        $feria = Feria::find($id);
        /*Fecha limite presupuesto*/
        $fehca_inicio = strtotime ( '+1 day' , strtotime ( $feria->fecha_inicio ) ) ;
        $fehca_inicio = date ( 'Y-m-d' , $fehca_inicio );
        $data['fecha_limite_presupuesto'] = date('d',strtotime($fehca_inicio)).' de '.$meses[intval(date('m',strtotime($fehca_inicio)))].' del '.date('Y',strtotime($fehca_inicio));
        if($fehca_inicio>=date('Y-m-d')){
            $data['permiso_presupuesto'] = "Si";
        }else{
            $data['permiso_presupuesto'] = "No";
            $data['vencio_presupuesto'] = (strtotime($fehca_inicio)-strtotime(date('Y-m-d')))/86400;
            $data['vencio_presupuesto'] = abs($data['vencio_presupuesto']);
            $data['vencio_presupuesto'] = floor($data['vencio_presupuesto']);
        }
        /*Fin Fecha limite presupuesto*/
        /*Fecha limite valor real*/
        $fehca_fin = strtotime ( '+10 day' , strtotime ( $feria->fecha_fin ) ) ;
        $fehca_fin = date ( 'Y-m-d' , $fehca_fin );
        $data['fecha_limite_real'] = date('d',strtotime($fehca_fin)).' de '.$meses[intval(date('m',strtotime($fehca_fin)))].' del '.date('Y',strtotime($fehca_fin));
        if($fehca_fin>=date('Y-m-d')){
            $data['permiso_real'] = "Si";
        }else{
            $data['permiso_real'] = "No";
            $data['vencio_real'] = (strtotime($fehca_fin)-strtotime(date('Y-m-d')))/86400;
            $data['vencio_real'] = abs($data['vencio_real']);
            $data['vencio_real'] = floor($data['vencio_real']);
        }
        /*Fin Fecha limite valor real*/
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.confpresucomparar', ['data' => $data]);
    }

    public function guardarajusteconferencia(Request $request){
        $ajustes = $request->ajuste;
        foreach($ajustes as $ajuste){
            $dato = Ferias_conferences_presupuesto_administrable::find($ajuste['id_concepto']);
            $dato->presupuesto=intval(str_replace("$ ", "", (str_replace(".", "", $ajuste['valor']))));
            $dato->real=intval(str_replace("$ ", "", (str_replace(".", "", $ajuste['real']))));
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function presupuestoconferenciacomentarios($id){
        $data['menu'] = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id,4);
        $data['id'] = $id;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.confcomentarios', ['data' => $data]);
    }

    public function listarcomentariosconferencia(Request $request){
        $comentarios = FeriasConferencesComentario::where('id_feria',$request->id)->get();
         ob_start(); ?>
            <?php foreach($comentarios as $comentario){
                $user=User::find($comentario->id_user);
            ?>
            <div class="comment-wrap">
                <div class="photo">
                    <div class="avatar" style="background-image: url('<?php echo $request->url_basico.'/images/file/clientes/'.$user->foto; ?>')"></div>
                </div>
                <div class="comment-block">
                    <p class="comment-text text-left">
                        <?=$comentario->comentario?>
                    </p>
                    <div class="bottom-comment">
                        <div class="comment-date">
                            <?=date("d/m/Y (h:i:s A)",strtotime($comentario->created_at))?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function comentarconferencia(Request $request){
        $dato = new FeriasConferencesComentario;
        $dato->comentario=$request->comentario;
        $dato->id_feria=$request->id_feria;
        $dato->id_user=$request->id_user;
        $dato->save();
        return \Response::json(['msj' => "Guardado correctamente"]);
    }

    public function listadoconferenciasferias(Request $request){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $conferencias = FeriasConference::where('id_feria',$request->id_feria)->get();
        $meses=['','ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC'];
        ob_start(); ?>
        <?php foreach($conferencias as $conferencia){
              $conferencistas=explode('%%',$conferencia->conferencistas);
              $foto_conferencista=explode('%%',$conferencia->foto_conferencista);
              $temas=explode('%%',$conferencia->temas);
        ?>
        <div class="col-md-12 text-left ml-5 item-conferen">
            <a class="h5">Conferencia: </a><a class="h4 name-conference"><?=$conferencia->titulo?></a>
        </div>
        <div class="col-md-12 ml-5 mt-3 mb-3 item-conferen">
            <div class="row">
                <div class="col-md-4 bg-white sombra-cuadro">
                    <div class="row">
                        <div class="col-md-10 title-conferencista text-left">
                            <h4 class="mt-4 mb-4"><img class="ml-5 mr-3" src="<?=$request->url_basico?>/images/iconosferias/conferencia/conferencia_icon.svg">CONFERENCISTA</h4>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>crear-conferencista<?php } ?>" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?> onclick="no_permiso('Usted no tiene permisos para crear un conferencista')" <?php } ?> id="conferencista<?=$conferencia->id?>" data-toggle="tooltip" data-placement="top" data-original-title="Crear conferencista"><img class="mr-3 mt-2" src="<?=$request->url_basico?>/images/iconosferias/conferencia/masconfere_icon.svg"></a>
                        </div>
                    </div>
                    <div class="row alto-fijo">
                        <div class="col-md-12">
                            <?php if($conferencia->conferencistas != ""){
                                  $f=0;
                                  foreach($conferencistas as $conferencista){
                            ?>
                            <div class="row R-conferncista">
                                <div class="col-md-3 mt-3 mb-3">
                                    <?php if($foto_conferencista[$f] == 'null'){ ?>
                                    <img src="<?=$request->url_basico?>/storage/ferias/conferencista/noImagen.png" class="img-circle photo-speaker">
                                    <?php }else{ ?>
                                    <img src="<?=$request->url_basico?>/storage/ferias/conferencista/<?=$foto_conferencista[$f]?>" class="img-circle photo-speaker">
                                    <?php } ?>
                                </div>
                                <div class="col-md-8 mt-5 mb-1">
                                    <a class="h4 name-conference"><?=$conferencista?></a>
                                </div>
                                <div class="col-md-1 mt-5 mb-1">
                                    <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar conferencista <?=$conferencista?>" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> onclick="eliminar_conferencista('<?=$conferencista?>','<?=$conferencia->id?>','<?=$f?>')" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para eliminar un conferencista')" <?php } ?> ><img class="eliminar-conferencista" src="<?=$request->url_basico?>/images/iconosferias/conferencia/eliminar_icon.svg"></a>
                                </div>
                            </div>
                            <?php $f++; } } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row ml-3 sombra-cuadro">
                        <div class="col-md-10 title-temas text-left">
                            <h4 class="mt-4 mb-4"><img class="ml-5 mr-3" src="<?=$request->url_basico?>/images/iconosferias/conferencia/temas_icon.svg">TEMAS</h4>
                        </div>
                        <div class="col-md-2 title-temas text-right">
                            <a class="cursor-mano <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>crear-tema<?php } ?>" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?> onclick="no_permiso('Usted no tiene permisos para crear temas')" <?php } ?> id="tema<?=$conferencia->id?>" data-toggle="tooltip" data-placement="top" data-original-title="Crear tema"><img class="mr-3 mt-2" src="<?=$request->url_basico?>/images/iconosferias/conferencia/mastemas_icon.svg"></a>
                        </div>
                    </div>
                    <div class="row ml-3 alto-fijo bg-white sombra-cuadro">
                        <div class="col-md-12">
                            <?php if($conferencia->temas != ''){
                                  $t=0;
                                  foreach($temas as $tema){
                                      $contenido = (strlen($tema) > 30)? substr($tema, 0, 28).'...': $tema;
                            ?>
                            <div class="row R-tema">
                                <div class="col-md-2 mt-5 mb-3">
                                    <img src="<?=$request->url_basico?>/images/iconosferias/conferencia/item.png" class="img-circle">
                                </div>
                                <div class="col-md-9 mt-5 mb-3">
                                    <a class="h4 name-conference" data-toggle="tooltip" data-placement="top" title="<?=$tema?>"><?=$contenido?></a>
                                </div>
                                <div class="col-md-1 mt-5 mb-3">
                                    <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Tema <?=$tema?>" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> onclick="eliminar_tema('<?=$tema?>','<?=$conferencia->id?>','fila_tema_<?=$t?>_<?=$conferencia->id?>')"<?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para eliminar temas')" <?php } ?>><img class="eliminar-conferencista" src="<?=$request->url_basico?>/images/iconosferias/conferencia/eliminar_icon.svg"></a>
                                </div>
                            </div>
                            <?php $t++; } } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row ml-3 sombra-cuadro">
                        <div class="col-md-10 title-ubicacion text-left py-4">
                            <a style="font-size: 13.8px;"><img class="ml-5 mr-3" src="<?=$request->url_basico?>/images/iconosferias/conferencia/ubicacion_icon.svg">UBICACIÓN & HORARIO</h4>
                        </div>
                        <div class="col-md-2 title-ubicacion text-right">
                            <a class="cursor-mano" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> onclick="edit_location(<?=$conferencia->id?>)" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para crear un proveedor')" <?php } ?> data-toggle="tooltip" data-placement="top" data-original-title="Editar Ubicación"><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row ml-3 alto-fijo bg-white sombra-cuadro">
                        <div class="col-md-12">
                            <div class="row R-tema text-center">
                                <div class="col-md-12">
                                   <h6 class="color-location">Ubicación: </h6>
                                    <?php $contenido = (strlen($conferencia->ubicacion) > 18)? substr($conferencia->ubicacion, 0, 18).'...' : $conferencia->ubicacion; ?>
                                   <h4 data-toggle="tooltip" data-placement="top" title="<?=$conferencia->ubicacion?>"><?=$contenido?></h4>
                                </div>
                            </div>
                            <div class="row R-tema text-center">
                                <div class="col-md-12">
                                   <h6 class="color-location">Fecha: </h6>
                                    <?php if(!empty($conferencia->fecha)){ ?>
                                   <h4><?=date('d',strtotime($conferencia->fecha)).' / '.$meses[intval(date('m',strtotime($conferencia->fecha)))].' / '.date('Y',strtotime($conferencia->fecha))?></h4>
                                   <?php }else{ ?>
                                   <h4> DD / MM / AAAA</h4>
                                   <?php } ?>
                                </div>
                            </div>
                            <div class="row R-tema text-center">
                                <div class="col-md-12">
                                   <h6 class="color-location">Hora Inicio: </h6>
                                    <?php if(!empty($conferencia->hora_inicio)){ ?>
                                   <h4><?=date("h:i:s A",strtotime($conferencia->hora_inicio))?></h4>
                                    <?php }else{ ?>
                                    <h4>00 : 00 AM</h4>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row R-tema text-center">
                                <div class="col-md-12">
                                   <h6 class="color-location">Hora Fin: </h6>
                                   <?php if(!empty($conferencia->hora_fin)){ ?>
                                   <h4><?=date("h:i:s A",strtotime($conferencia->hora_fin))?></h4>
                                    <?php }else{ ?>
                                    <h4>00 : 00 AM</h4>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Conferencia" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> onclick="eliminar_conferencia(<?=$conferencia->id?>,'<?=$conferencia->titulo?>')" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para eliminar una conferencia')" <?php } ?>><img src="<?=$request->url_basico?>/images/iconosferias/conferencia/eliminar_icon.svg"></a>
                </div>
            </div>
        </div>
         <?php
        }
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }
    public function crearconferenciasferias(Request $request){
        $dato = new FeriasConference;
        $dato->id_feria=$request->id;
        $dato->titulo=$request->nombre;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente']);
    }

    public function consultarubicacionconferencia($id){
        $dato = FeriasConference::find($id);
        ob_start(); ?>
            <input type="text" class="form-control" id="ubicacion_conference" value="<?=$dato->ubicacion?>" placeholder="Ubicación">
            <input class="form-control2 form-control datatime2" id="fecha_conference" value="<?=$dato->fecha?>" type="text" step="1800" placeholder="Fecha">
            <input type="time" class="form-control" id="hora_inicio_conference" value="<?=$dato->hora_inicio?>">
            <input type="time" class="form-control" id="hora_fin_conference" value="<?=$dato->hora_fin?>">
         <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function guardarubicacionconferencia(Request $request){
        $dato = FeriasConference::find($request->id_conferencia);
        $dato->ubicacion=$request->ubicacion;
        $dato->fecha=$request->fecha;
        $dato->hora_inicio=$request->hora_inicio;
        $dato->hora_fin=$request->hora_fin;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente']);
    }

    public function eliminarconferencia($id){
        $dato = FeriasConference::find($id);
        $foto_conferencistas=explode('%%',$dato->foto_conferencista);
        if(!empty($dato->foto_conferencista)){
            foreach($foto_conferencistas as $foto){
                 if($foto!='null'){
                     Storage::delete('ferias/conferencista/'.$foto);
                 }
            }
        }
        $dato->delete();
        return \Response::json(['msj' => 'Eliminado correctamente']);
    }
}
