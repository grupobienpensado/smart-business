<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Ciudades;
use App\Estados;
use App\Paises;
use App\FreelanceUser;
use App\ViewFreelanceLista;
use App\FreelancePaisSecundarios;
use App\FreelanceEstudioRealizados;
use App\FreelanceTipos;
use App\FreelanceEmpresas;
use App\FreelanceCategorias;
use App\FreelanceBitacoras;
use App\FreelanceBitacoraGastos;
use App\FreelanceTipoGastos;
use App\FreelanceUserArchivos;
use App\FreelanceComentarios;
use App\FreelancePresupuestos;
use App\FreelancePresupuestosConcepto;
use App\ViewFreelanceListaBitacoras;
use App\ViewOportunidades;
use App\Oportunidades;
use App\Empresa;
use App\Producto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
use Carbon\Carbon;
date_default_timezone_set("America/Bogota");

class FreelanceController extends Controller
{
    public function index(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 56 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('freelance.listadouser',['data' => $data]);
    }
    public function crear(){
        $data['paises'] = Paises::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 56 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('freelance.crearuser',['data' => $data]);
    }

    public function editar($id){
        $data['paises'] = Paises::all();
        $data['usuario'] = ViewFreelanceLista::find($id);
        /*Datos Adicionales*/
        $data['ciudades'] = Ciudades::where('state_id',$data['usuario']->IDESTADONACIMIENTO)->get();
        $data['estados'] = Estados::where('country_id',$data['usuario']->IDPAISNACIMIENTO)->get();
        $data['ciudades2'] = Ciudades::where('state_id',$data['usuario']->IDESTADOUBICACION)->get();
        $data['estados2'] = Estados::where('country_id',$data['usuario']->IDPAISUBICACION)->get();
        /*Fin Datos Adicionales*/
        $data['estudios'] = FreelanceEstudioRealizados::where('id_freelance_user',$id)->get();
        $data['paisesSecundarios'] = FreelancePaisSecundarios::where('id_freelance_user',$id)->get();
        ob_start();
        $i=0; foreach($data['estudios'] as $estudio){ ?>
        <div class="row mt-2 info-titulo" id="fila<?=$i?>">
            <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                <label for="titulo">Titulo Obtenido</label>
                <input class="form-control" type="text" id="educacion_titulo_<?=$i?>" onkeyup="max_campo(this.value,60,'#texto_educacion_titulo_<?=$i?>',this.id)" name="educacion[<?=$i?>][titulo_obtenido]" placeholder="Titulo Obtenido" value="<?=$estudio->titulo_obtenido?>">
                <p class="text-right invisible" id="texto_educacion_titulo_<?=$i?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/60</a></p>
            </div>
            <div class="col-sm-12 <?php if($i==0){ ?> col-md-4 col-lg-4 <?php }else{ ?> col-md-3 col-lg-3 <?php } ?> text-left mt-3">
                <label for="ano">Año culminación</label>
                <input class="form-control" type="number" id="educacion_ano_<?=$i?>" min="1900" max="<?=date('Y')?>" onkeyup="max_campo(this.value,4,'#texto_educacion_ano_<?=$i?>',this.id)" name="educacion[<?=$i?>][ano]" placeholder="Año culminación" value="<?=$estudio->ano?>">
                <p class="text-right invisible" id="texto_educacion_ano_<?=$i?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/4</a></p>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                <label for="institucion">Institución</label>
                <input class="form-control" type="text" id="educacion_institucion_<?=$i?>" onkeyup="max_campo(this.value,100,'#texto_educacion_institucion_<?=$i?>',this.id)" name="educacion[<?=$i?>][institucion]" placeholder="Institución" value="<?=$estudio->institucion?>">
                <p class="text-right invisible" id="texto_educacion_institucion_<?=$i?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
            </div>
            <?php if($i>0){ ?>
            <div class="col-sm-12 col-md-1 col-lg-1 text-center mt-3">
                <a class="text-danger" style="font-size:2rem; cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="EliminarEstudio(<?=$i?>)"><i class="fa fa-trash"></i></a>
            </div>
            <?php } ?>
        </div>
        <?php
            $i++;
        }
        if(count($data['estudios'])==0){ ?>
            <div class="row mt-2 info-titulo">
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="titulo">Titulo Obtenido</label>
                    <input class="form-control" type="text" id="educacion_titulo_0" onkeyup="max_campo(this.value,60,'#texto_educacion_titulo_0',this.id)" name="educacion[0][titulo_obtenido]" placeholder="Titulo Obtenido">
                    <p class="text-right invisible" id="texto_educacion_titulo_0"><a class="text-success"><i class="fa fa-pencil"></i>... 1/60</a></p>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="ano">Año culminación</label>
                    <input class="form-control" type="number" id="educacion_ano_0" min="1900" max="<?=date('Y')?>" onkeyup="max_campo(this.value,4,'#texto_educacion_ano_0',this.id)" name="educacion[0][ano]" placeholder="Año culminación">
                    <p class="text-right invisible" id="texto_educacion_ano_0"><a class="text-success"><i class="fa fa-pencil"></i>... 1/4</a></p>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="institucion">Institución</label>
                    <input class="form-control" type="text" id="educacion_institucion_0" onkeyup="max_campo(this.value,100,'#texto_educacion_institucion_0',this.id)" name="educacion[0][institucion]" placeholder="Institución" value="">
                    <p class="text-right invisible" id="texto_educacion_institucion_0"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                </div>
            </div>
        <?php
        }
        $data['html_estudios'] = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 56 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('freelance.editaruser',['data' => $data]);
    }

    public function action_crear(Request $request){
        $accion = $request->action;
        switch($accion){
            case 0:
                $pais = Paises::find($request->pais);
                ob_start(); ?>
                <img class="form-control pais-p" src="/images/icons/<?=$pais->imagen?>">
                <?php
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
            case 1:
                if(!empty($request->paises)){
                    ob_start(); ?>
                    <div class="row">
                    <?php
                    foreach($request->paises as $p){
                        $pais = Paises::find($p); ?>
                        <div class="col-sm-4 col-md-2 col-lg-2">
                            <img class="form-control pais-s" src="/images/icons/<?=$pais->imagen?>">
                        </div>
                        <?php
                    } ?>
                    </div>
                    <?php
                    $data['cuerpo'] = ob_get_contents();
                    ob_end_clean();
                }else{
                    $data['cuerpo'] = '';
                }
                break;
            case 2:
                //Guardar Foto
                $foto = $request->foto_values;
                if (!empty($foto)) {
                    $imagen = json_decode($foto);
                    $data = explode( ',', $imagen->data );
                    $fotografia = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $FOTOUSER =$antenombre.".jpg";
                    \Storage::disk('freelanceUser')->put($FOTOUSER,  $fotografia);
                }
                //Guardar foto completa
                $foto_completa = $request->foto_completa_values;
                if (!empty($foto_completa)) {
                    $imagen = json_decode($foto_completa);
                    $data = explode( ',', $imagen->data );
                    $fotografia_completa = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $FOTOCOMPLETAUSER =$antenombre.".jpg";
                    \Storage::disk('freelanceUser')->put($FOTOCOMPLETAUSER,  $fotografia_completa);
                }
                //Guardar datos en tabla freelance_users
                $formulario = $request->formulario;
                $dato = new FreelanceUser;
                foreach($formulario as $index=>$value){
                    $dato->$index=$value;
                }
                if(isset($FOTOUSER) && !empty($FOTOUSER)){
                    $dato->foto = $FOTOUSER;
                }
                if(isset($FOTOCOMPLETAUSER) && !empty($FOTOCOMPLETAUSER)){
                    $dato->foto_completa = $FOTOCOMPLETAUSER;
                }
                if($dato->save()){
                    $id = $dato->id;
                    foreach($request->paises_secundarios as $value){
                        $datopais = new FreelancePaisSecundarios;
                        $datopais->id_pais = $value;
                        $datopais->id_freelance_user = $id;
                        $datopais->save();
                    }

                    foreach($request->educacion as $educacion){
                        $datoestudio = new FreelanceEstudioRealizados;
                        foreach($educacion as $index=>$value){
                            $datoestudio->$index = $value;
                        }
                        $datoestudio->id_freelance_user = $id;
                        $datoestudio->save();
                    }
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar";
                }
                break;
            case 3:
                $estudio = $request->estudios;
                ob_start(); ?>
                <div class="row mt-2 info-titulo" id="fila<?=$estudio?>">
                    <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                        <label for="titulo">Titulo Obtenido</label>
                        <input class="form-control" type="text" id="educacion_titulo_<?=$estudio?>" onkeyup="max_campo(this.value,60,'#texto_educacion_titulo_<?=$estudio?>',this.id)" name="educacion[<?=$estudio?>][titulo_obtenido]" placeholder="Titulo Obtenido">
                        <p class="text-right invisible" id="texto_educacion_titulo_<?=$estudio?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/60</a></p>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 text-left mt-3">
                        <label for="ano">Año culminación</label>
                        <input class="form-control" type="number" id="educacion_ano_<?=$estudio?>" min="1900" max="<?=date('Y')?>" onkeyup="max_campo(this.value,4,'#texto_educacion_ano_<?=$estudio?>',this.id)" name="educacion[<?=$estudio?>][ano]" placeholder="Año culminación">
                        <p class="text-right invisible" id="texto_educacion_ano_<?=$estudio?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/4</a></p>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                        <label for="institucion">Institución</label>
                        <input class="form-control" type="text" id="educacion_institucion_<?=$estudio?>" onkeyup="max_campo(this.value,100,'#texto_educacion_institucion_<?=$estudio?>',this.id)" name="educacion[<?=$estudio?>][institucion]" placeholder="Institución" value="">
                        <p class="text-right invisible" id="texto_educacion_institucion_<?=$estudio?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                    </div>
                    <div class="col-sm-12 col-md-1 col-lg-1 text-center mt-3">
                        <a class="text-danger" style="font-size:2rem; cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="EliminarEstudio(<?=$estudio?>)"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
                <?php
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
            case 4:
                $dato = FreelanceUser::find($request->id);
                $foto_anterior = $dato->foto;
                //Guardar Foto
                $foto = $request->foto_values;
                if (!empty($foto)) {
                    $imagen = json_decode($foto);
                    $data = explode( ',', $imagen->data );
                    $fotografia = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $FOTOUSER =$antenombre.".jpg";
                    \Storage::disk('freelanceUser')->put($FOTOUSER,  $fotografia);
                    if($foto_anterior!=''){
                        Storage::delete('/Freelance/users/'.$foto_anterior);
                    }
                }
                $fotocompleta_anterior = $dato->foto_completa;
                //Guardar foto completa
                $foto_completa = $request->foto_completa_values;
                if (!empty($foto_completa)) {
                    $imagen = json_decode($foto_completa);
                    $data = explode( ',', $imagen->data );
                    $fotografia_completa = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $FOTOCOMPLETAUSER =$antenombre.".jpg";
                    \Storage::disk('freelanceUser')->put($FOTOCOMPLETAUSER,  $fotografia_completa);
                    if($fotocompleta_anterior!=''){
                        Storage::delete('/Freelance/users/'.$fotocompleta_anterior);
                    }
                }
                //Guardar datos en tabla freelance_users
                $formulario = $request->formulario;
                foreach($formulario as $index=>$value){
                    $dato->$index=$value;
                }
                if(isset($FOTOUSER) && !empty($FOTOUSER)){
                    $dato->foto = $FOTOUSER;
                }
                if(isset($FOTOCOMPLETAUSER) && !empty($FOTOCOMPLETAUSER)){
                    $dato->foto_completa = $FOTOCOMPLETAUSER;
                }
                if($dato->save()){
                    $id = $dato->id;
                    /*Eliminar los paises secundarios anteriores*/
                    $paisessecundarios = FreelancePaisSecundarios::where('id_freelance_user',$id)->get();
                    foreach($paisessecundarios as $p_s){
                        $p_s->delete();
                    }
                    /*FIN Eliminar los paises secundarios anteriores*/
                    if(isset($request->paises_secundarios) && !empty($request->paises_secundarios)){
                        foreach($request->paises_secundarios as $value){
                            $datopais = new FreelancePaisSecundarios;
                            $datopais->id_pais = $value;
                            $datopais->id_freelance_user = $id;
                            $datopais->save();
                        }
                    }
                    /*Eliminar los estudios realizados anteriores*/
                    $estudiosrealizados = FreelanceEstudioRealizados::where('id_freelance_user',$id)->get();
                    foreach($estudiosrealizados as $e_r){
                        $e_r->delete();
                    }
                    /*FIN Eliminar los estudios realizados anteriores*/
                    if(isset($request->educacion) && !empty($request->educacion)){
                        foreach($request->educacion as $educacion){
                            $datoestudio = new FreelanceEstudioRealizados;
                            $vacio = 'No';
                            foreach($educacion as $index=>$value){
                                $datoestudio->$index = $value;
                                if($value == ""){
                                    $vacio = 'Si';
                                }
                            }
                            $datoestudio->id_freelance_user = $id;
                            if($vacio == "No"){
                                $datoestudio->save();
                            }
                        }
                    }
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar";
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function red_mundial_coop(){
        return view('freelance.redmundialcooperacion');
    }

    public function dashboard_accion(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*Información de aliados*/
            case 0:
                $usuarios = ViewFreelanceLista::all();
                $query = 'SELECT DISTINCT(U.id_pais_principal) AS PaisPrincipal FROM freelance_users U WHERE U.deleted_at IS NULL';
                $principales = DB::select($query);
                $query = 'SELECT DISTINCT(S.id_pais) AS PaisSecundario FROM freelance_users U INNER JOIN freelance_pais_secundarios S ON U.id = S.id_freelance_user WHERE U.deleted_at IS NULL AND S.deleted_at IS NULL AND S.id_pais NOT IN (SELECT DISTINCT(FU.id_pais_principal) AS PaisPrincipal FROM freelance_users FU WHERE FU.deleted_at IS NULL)';
                $secundarios = DB::select($query);
                $query = 'SELECT (SELECT COUNT(F.id) FROM freelance_users F INNER JOIN freelance_categorias C ON F.id_freelance_categoria = C.id WHERE C.nombre="Gold") AS GOLD, (SELECT COUNT(F.id) FROM freelance_users F INNER JOIN freelance_categorias C ON F.id_freelance_categoria = C.id WHERE C.nombre="VIP") AS VIP, (SELECT COUNT(F.id) FROM freelance_users F INNER JOIN freelance_categorias C ON F.id_freelance_categoria = C.id WHERE C.nombre="A+") AS APOSITIVO, (SELECT COUNT(F.id) FROM freelance_users F INNER JOIN freelance_categorias C ON F.id_freelance_categoria = C.id WHERE C.nombre="A") AS A';
                $categorias = DB::select($query);
                $data['aliados'] = count($usuarios);
                $data['paises'] = count($principales)+count($secundarios);
                $data['categorias'] = $categorias;
                ob_start(); ?>
                <?php foreach($principales as $principal){
                    $pais = Paises::find($principal->PaisPrincipal);
                ?>
                <div class="swiper-slide"><img src="/images/icons/<?=$pais->imagen?>" alt="" width="50%"></div>
                <?php }
                foreach($secundarios as $secundario){
                    $pais = Paises::find($secundario->PaisSecundario);
                ?>
                <div class="swiper-slide"><img src="/images/icons/<?=$pais->imagen?>" alt="" width="50%"></div>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Información de oportunidades*/
            case 1:
                $data['oportunidades_perdidas'] = 0;
                $data['oportunidades_perdidas_valor'] = 0;
                $data['oportunidades_vendidas'] = 0;
                $data['oportunidades_vendidas_valor'] = 0;
                $data['oportunidades_vigentes'] = 0;
                $data['oportunidades_vigentes_valor'] = 0;
                $oportunidades = ViewOportunidades::where('id_user_freelance','!=',"")->get();
                foreach($oportunidades as $oportunidad){
                    switch($oportunidad->CICLO){
                        case ($oportunidad->CICLO == 0):
                            $data['oportunidades_perdidas']++;
                            $data['oportunidades_perdidas_valor']+=intval($oportunidad->VALOR);
                            break;
                        case ($oportunidad->CICLO == 90):
                            $data['oportunidades_vendidas']++;
                            $data['oportunidades_vendidas_valor']+=intval($oportunidad->VALOR);
                            break;
                        default:
                            $data['oportunidades_vigentes']++;
                            $data['oportunidades_vigentes_valor']+=intval($oportunidad->VALOR);
                            break;
                    }
                }
                $data['oportunidades_perdidas_valor'] = number_format($data['oportunidades_perdidas_valor']/1000000, 0, ',', '.').' Millones';
                $data['oportunidades_vendidas_valor'] = number_format($data['oportunidades_vendidas_valor']/1000000, 0, ',', '.').' Millones';
                $data['oportunidades_vigentes_valor'] = number_format($data['oportunidades_vigentes_valor']/1000000, 0, ',', '.').' Millones';
                break;
            /*Grafica Presupuesto vs Gastos*/
            case 2:
                for($mes=1;$mes<=12;$mes++){
                    $query = 'SELECT IF(IsNull(SUM(valor)), 0, SUM(valor)) AS Total FROM freelance_presupuestos WHERE mes = '.$mes.' AND ano = '.date('Y');
                    $presupuesto[$mes] = DB::select($query);
                    $presupuesto_total[$mes] = ($mes==1)? intval($presupuesto[$mes][0]->Total) : $presupuesto_total[$mes-1] + intval($presupuesto[$mes][0]->Total);
                }
                for($mes=1;$mes<=12;$mes++){
                    $fecha = $mes<10? date('Y').'-0'.$mes : date('Y').'-'.$mes;
                    $query = 'SELECT IF(IsNull(SUM(GASTO)), 0, SUM(GASTO)) AS Total FROM view_freelance_lista_bitacoras WHERE fecha_inicio LIKE "%'.$fecha.'%"';
                    $gasto[$mes] = DB::select($query);
                    $gasto_total[$mes] = ($mes==1)? intval($gasto[$mes][0]->Total) :  $gasto_total[$mes-1] + intval($gasto[$mes][0]->Total);
                }
                $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                 ob_start(); ?>
<script>
    AmCharts.makeChart("global_chart",
				{
					"type": "serial",
					"categoryField": "category",
					"columnWidth": 0,
					"startDuration": 1,
					"startEffect": "easeOutSine",
					"backgroundColor": "#1C2232",
					"color": "#FFFFFF",
					"theme": "light",
					"categoryAxis": {
						"gridPosition": "start",
						"axisAlpha": 1,
						"axisColor": "#FFFFFF",
						"fillColor": "#46C6FF"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonColor": "#009EB5",
							"balloonText": "[[title]] de [[category]]: $[[value]]",
							"bullet": "round",
							"bulletBorderColor": "undefined",
							"color": "#012D46",
							"fillColors": "#009EB5",
							"id": "AmGraph-1",
							"lineColor": "#009EB5",
							"title": "Presupuesto Anual",
							"valueField": "column-1"
						},
						{
							"balloonColor": "#88E006",
							"balloonText": "[[title]] de [[category]]: $[[value]]",
							"bullet": "square",
							"color": "#000000",
							"fillColors": "#FF8000",
							"id": "AmGraph-2",
							"lineColor": "#88E006",
							"periodSpan": 2,
							"precision": -2,
							"title": "Gasto Anual",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisAlpha": 1,
							"axisColor": "#FFFFFF",
							"color": "#FFFFFF",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {
						"showBullet": true
					},
					"legend": {
						"enabled": true,
						"color": "#FFFFFF",
						"rollOverColor": "#46C6FF",
						"useGraphSettings": true
					},
					"titles": [
						{
							"color": "#FFFFFF",
							"id": "Title-1",
							"size": 12,
							"text": "Presupuesto vs Gasto"
						}
					],
					"dataProvider": [
                        <?php for($mes=1;$mes<=12;$mes++){ ?>
						{
							"category": "<?=$meses[$mes]?>",
							"column-1": "<?=$presupuesto_total[$mes]?>",
							"column-2": "<?=$gasto_total[$mes]?>"
						}<?php if($mes<12){ ?>,<?php } ?>
                        <?php } ?>
					]
				}
			);
</script>
               <div id="global_chart" class="position-relative mx-auto"></div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Freelance con mayores gastos y mayores ventas*/
            case 3:
                if(isset($request->fecha_inicio) && isset($request->fecha_fin)){
                $query = 'SELECT U.NOMBRECORTO, (SELECT SUM(B.GASTO) FROM view_freelance_lista_bitacoras B WHERE B.id_freelance_user = U.id AND B.fecha_inicio BETWEEN "'.$request->fecha_inicio.'" AND "'.$request->fecha_fin.'") AS GASTO FROM view_freelance_listas U ORDER BY GASTO DESC LIMIT 0,6';
                }else{
                    $query = 'SELECT U.NOMBRECORTO, (SELECT SUM(B.GASTO) FROM view_freelance_lista_bitacoras B WHERE B.id_freelance_user = U.id) AS GASTO FROM view_freelance_listas U ORDER BY GASTO DESC LIMIT 0,6';
                }
                $usuarios_gastos = DB::select($query);
                $query = 'SELECT O.id AS ID_OPORTUNIDAD, O.id_user_freelance, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1), " ", SUBSTRING_INDEX(U.apellidos, " ", 1)) AS NOMBRECORTO, (SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) FROM historial_oportunidades H WHERE H.oportunidad_id = O.id AND H.key LIKE "val_pesos" ORDER BY H.id DESC LIMIT 0,1) AS VALOR, (SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) FROM historial_oportunidades H WHERE H.oportunidad_id = O.id AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1) AS CICLO  FROM oportunidades O INNER JOIN freelance_users U ON O.id_user_freelance = U.id WHERE O.id_user_freelance IS NOT NULL';
                $oportunidades = DB::select($query);
                foreach($oportunidades as $oportunidad){
                    if($oportunidad->CICLO == 90){
                        if(isset($request->fecha_inicio) && isset($request->fecha_fin)){
                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$oportunidad->ID_OPORTUNIDAD.' LIMIT 0,1';
                            $fecha_cierre = DB::select($query);
                            if(count($fecha_cierre)>0 && date('Y-m-d',strtotime($fecha_cierre[0]->value))>=date('Y-m-d',strtotime($request->fecha_inicio)) && date('Y-m-d',strtotime($fecha_cierre[0]->value))<=date('Y-m-d',strtotime($request->fecha_fin))){
                                if(isset($usuarios_ventas[$oportunidad->id_user_freelance]['valor'])){
                                    $usuarios_ventas[$oportunidad->id_user_freelance]['valor']+=intval($oportunidad->VALOR);
                                }else{
                                    $usuarios_ventas[$oportunidad->id_user_freelance]['valor']=intval($oportunidad->VALOR);
                                }
                                $usuarios_ventas[$oportunidad->id_user_freelance]['freelance'] = $oportunidad->NOMBRECORTO;
                            }
                        }else{
                            if(isset($usuarios_ventas[$oportunidad->id_user_freelance]['valor'])){
                                $usuarios_ventas[$oportunidad->id_user_freelance]['valor']+=intval($oportunidad->VALOR);
                            }else{
                                $usuarios_ventas[$oportunidad->id_user_freelance]['valor']=intval($oportunidad->VALOR);
                            }
                            $usuarios_ventas[$oportunidad->id_user_freelance]['freelance'] = $oportunidad->NOMBRECORTO;
                        }
                    }
                }
                ob_start(); ?>
                <div class="col-md-4 col-sm-12">
                   <h4 class="text-primary">Gastos</h4>
                    <ol class="text-left bg-light-2">
                        <div class="row no-gutters">
                            <div class="col-md-6">
                               <?php for($i=0;$i<3;$i++){
                                if(isset($usuarios_gastos[$i])){
                                ?>
                                <li class="my-2"><?=$usuarios_gastos[$i]->NOMBRECORTO?> <span class="ml-3 text-info"><?=number_format($usuarios_gastos[$i]->GASTO, 0, ',', '.')?></span></li>
                                <?php }else{ ?>
                                <li class="my-2">No hay un usuario <?=$i?> <span class="ml-3 text-info">0</span></li>
                                <?php } } ?>
                            </div>
                            <div class="col-md-6">
                               <?php for($i=3;$i<6;$i++){
                                if(isset($usuarios_gastos[$i])){
                                ?>
                                <li class="my-2"><?=$usuarios_gastos[$i]->NOMBRECORTO?> <span class="ml-3 text-info"><?=number_format($usuarios_gastos[$i]->GASTO, 0, ',', '.')?></span></li>
                                <?php }else{ ?>
                                <li class="my-2">No hay un usuario <?=$i?> <span class="ml-3 text-info">0</span></li>
                                <?php } } ?>
                            </div>
                        </div>
                    </ol>
                </div>
                <div class="col-md-4 col-sm-12">
                    <h4 class="text-primary">Ingresos</h4>
                    <ol class="text-left bg-light-2">
                       <div class="row no-gutters">
                            <div class="col-md-6">
                               <?php $i=0; if(isset($usuarios_ventas)){ foreach($usuarios_ventas as $venta){
                                if($i<3){ ?>
                                <li class="my-2"><?=$venta['freelance']?> <span class="ml-3 text-info"><?=number_format($venta['valor'], 0, ',', '.')?></span></li>
                                <?php }else if($i<6){
                                if($i==3){ ?>
                            </div>
                            <div class="col-md-6">
                                <?php } ?>
                                <li class="my-2"><?=$venta['freelance']?> <span class="ml-3 text-info"><?=number_format($venta['valor'], 0, ',', '.')?></span></li>
                                <?php if($i==5){ ?>
                            </div>
                                <?php } } $i++; } } ?>
                                <?php if($i<6){
                                        for($y=$i; $y<6; $y++){
                                            if($y<3){ ?>
                                <li class="my-2">No hay un funcionario <span class="ml-3 text-info">0</span></li>
                                <?php }else if($y<6){
                                if($y==3){ ?>
                            </div>
                            <div class="col-md-6">
                                <?php } ?>
                                <li class="my-2">No hay un funcionario <span class="ml-3 text-info">0</span></li>
                                <?php if($y==5){ ?>
                            </div>
                                <?php } } ?>
                                <?php } } ?>
                        </div>
                    </ol>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }


    public function action_listar(Request $request){
        $accion = $request->action;
        switch($accion){
            /*Listado de Usarios FREELANCE*/
            case 0:
                $usuarios = ViewFreelanceLista::orderby('updated_at','DESC')->get();
                ob_start();
                foreach($usuarios as $usuario){
?>
                <div class="col-md-4 mt-5">
                    <div class="card">
                        <div class="cardheader" style="background-image: url('http://subtlepatterns.com/patterns/geometry2.png')">
                            <div class="pull-right p-4" style="cursor: pointer;">
                                <div class="dropdown">
                                    <a class="fa fa-bars fa-2x dropdown-toggle text-white" aria-hidden="true" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #051d60 !important;"></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item action-see" href="/freelance/ver/<?=$usuario->id?>" data-cid=""><i class="fa fa-eye mr-3 text-info"></i>Ver</a>
                                        <a class="dropdown-item action-edit" href="/freelance/editar/<?=$usuario->id?>" data-cid=""><i class="fa fa-pencil mr-3 text-warning"></i>Editar</a>
                                        <a class="dropdown-item action-del" onclick="eliminar(<?=$usuario->id?>)" data-cid=""><i class="fa fa-trash mr-3 text-danger"></i>Eliminar</a>
                                        <a class="dropdown-item action-del" onclick="tipo_freelance(<?=$usuario->id?>,'<?=$usuario->NOMBRECORTO?>')" data-cid=""><i class="fa fa-arrow-circle-right"></i>Tipo Freelance</a>
                                        <a class="dropdown-item" onclick="categoria_freelance(<?=$usuario->id?>,'<?=$usuario->NOMBRECORTO?>')" data-cid=""><i class="fa fa-male"></i>Categoria Freelance</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="avatar">
                           <?php if($usuario->foto!=''){ ?>
                            <img alt="" src="<?=$usuario->FOTOPERFIL?>" class="img-avatar-card">
                            <?php }else{ ?>
                            <img alt="" src="http://via.placeholder.com/250x250" class="img-avatar-card">
                            <?php } ?>
                        </div>
                        <div class="container">
                            <h3 class="card-title mt-0">
                                <?=$usuario->NOMBRECORTO?>
                            </h3>
                            <h5 class="card-title mt-0">
                                <?php if($usuario->TIPOFREELANCE != ''){ echo $usuario->TIPOFREELANCE; }else{ echo '<em>No tipo Freelance</em>'; } ?>
                            </h5>
                            <h5 class="card-title mt-0">
                                <?php if($usuario->CATEGORIAFREELANCE != ''){ echo $usuario->CATEGORIAFREELANCE; }else{ echo '<em>No categoria Freelance</em>'; } ?>
                            </h5>
                            <h4 class="text-muted text-center"><?=$usuario->ESTADOUBICACION?> / <?=$usuario->PAISUBICACION?></h4>
                            <div class="row mt-4">
                                <div class="col-md-4"><span class="fa fa-id-card essi-color-text fa-lg"></span><br>
                                    <?=$usuario->TIPODOCUMENTO?><br>
                                    <?=$usuario->numero_documento?>
                                </div>
                                <div class="col-md-4"><span class="fa fa-phone mr-2 essi-color-text fa-lg fa-3" aria-hidden="true"></span><br>
                                    <?=$usuario->telefono_corporativo?>
                                </div>
                                <div class="col-md-4"><span class="fa fa-envelope mr-2 essi-color-text fa-lg" aria-hidden="true"></span><br>
                                    <?=$usuario->correo_corporativo?>
                                </div>
                            </div>
                        </div>
                        <hr class="comp-bottom-separator">
                    </div>
                </div>
                <?php
                }
                $data['html_listado'] = ob_get_contents();
                ob_end_clean();
                break;
            case 1:
                $dato = FreelanceUser::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            case 2:
                $tipos = FreelanceTipos::all();
                ob_start(); ?>
                <div class="content">
                  <div class="row">
                      <div class="col-md-12">
                          <a class="essi-color-text" style="font-size: 3rem; cursor: pointer;" title="Crear un tipo" onclick="acciontipo(0)"><i class="fa fa-plus"></i></a>
                      </div>
                  </div>
                  <?php if(count($tipos)==0){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><em>No hay registros de tipos de Freelance</em></p>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="row">
                        <div class="col-md-1">
                            <p class="h4"><em>Item</em></p>
                        </div>
                        <div class="col-md-3">
                            <p class="h4"><em>Nombre</em></p>
                        </div>
                        <div class="col-md-5">
                            <p class="h4"><em>Descripción</em></p>
                        </div>
                        <div class="col-md-3">
                            <p class="h4"><em>Empresa</em></p>
                        </div>
                    </div>
                   <?php } ?>
                   <div class="list-tipos">
                   <?php $i=1; foreach($tipos as $tipo){
                    if($tipo->empresa == 0){
                        $empresa = 'Si';
                        $empresa_f = FreelanceEmpresas::find($tipo->id_freelance_empresa);
                        $empresa.=isset($empresa_f->nombre)?', '.$empresa_f->nombre:'';

                    }else{
                        $empresa = 'No';
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-1">
                            <?=$i?>
                        </div>
                        <div class="col-md-3">
                            <p class="h4"><?=$tipo->nombre?></p>
                        </div>
                        <div class="col-md-5 text-justify">
                            <p class="h4"><?=$tipo->descripcion?></p>
                        </div>
                        <div class="col-md-2">
                            <p class="h4">Empresa: <?=$empresa?></p>
                        </div>
                        <div class="col-md-1">
                            <a class="essi-color-text" style="cursor: pointer;" title="Editar el tipo <?=$tipo->nombre?>" onclick="acciontipo(1,<?=$tipo->id?>)"><i class="fa fa-pencil"></i></a>
                            <a class="essi-color-text" style="cursor: pointer;" title="Eliminar el tipo <?=$tipo->nombre?>" onclick="acciontipo(2,<?=$tipo->id?>)"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    </div>
                </div>
                <?php
                $data['html_tipofreelance'] = ob_get_contents();
                ob_end_clean();
                break;
            case 3:
                $empresas = FreelanceEmpresas::all();
                ob_start(); ?>
                <div class="content">
                   <form method="POST" action="" id="formulario" enctype="multipart/form-data">
                   <input type="hidden" name="_token" value="<?=$request->_token?>">
                   <input type="hidden" value="10" name="action">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nombre_tipo">Nombre del tipo</label>
                            <input class="form-control" type="text" id="nombre_tipo" onkeyup="max_campo(this.value,70,'#texto_nombre_tipo',this.id)" name="formulario[nombre]" placeholder="Tipo Freelance" required>
                            <p class="text-right invisible" id="texto_nombre_tipo"><a class="text-success"><i class="fa fa-pencil"></i>... 1/70</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="descripcion_tipo">Descripción del tipo</label>
                            <textarea class="form-control" type="text" id="descripcion_tipo" name="formulario[descripcion]" rows="8" onkeyup="max_campo(this.value,350,'#texto_descripcion_tipo',this.id)" required="required"></textarea>
                            <p class="text-right invisible" id="texto_descripcion_tipo"><a class="text-success"><i class="fa fa-pencil"></i>... 1/350</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="empresa_tipo">Empresa</label>
                            <select class="form-control" name="formulario[empresa]" id="empresa_tipo" style="height:57%;" onchange="empresatipo(this.value)" required>
                                <option value="0">Si</option>
                                <option value="1" selected>No</option>
                            </select>
                        </div>
                        <div class="col-md-10 id_empresa_tipo mt-4" style="display: none;">
                            <label for="id_empresa_tipo">Seleccione la Empresa</label>
                            <select class="form-control" name="formulario[id_freelance_empresa]" style="height:57%;">
                                <option value="">Seleccione una opción</option>
                                <?php foreach($empresas as $empresa){ ?>
                                <option value="<?=$empresa->id?>"><?=$empresa->nombre?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2 id_empresa_tipo text-center mt-4" style="display: none;">
                            <p class="mt-5"><a class="essi-color-text" style="cursor: pointer" title="Crear empresa" onclick="acciontipo(3)"><i class="fa fa-plus"></i></a></p>
                        </div>
                    </div>
                    </form>
                </div>
                <?php
                $data['html_creartipo'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Listado de empresas*/
            case 4:
                $empresas = FreelanceEmpresas::all();
                ob_start(); ?>
                <div class="content">
                   <div class="row">
                       <div class="col-md-12">
                           <a class="essi-color-text" style="font-size: 3rem; cursor: pointer;" title="Crear una empresa" onclick="acciontipo(4)"><i class="fa fa-plus"></i></a>
                       </div>
                    </div>
                    <?php $i=1; foreach($empresas as $empresa){ ?>
                    <div class="row mt-3 b-b">
                        <div class="col-md-1">
                            <?=$i?>
                        </div>
                        <div class="col-md-2">
                           <?php if($empresa->logo!=''){ ?>
                            <img class="imagen-redonda" width="40px" src="/storage/Freelance/empresas/<?=$empresa->logo?>">
                            <?php }else{ ?>
                            <img class="imagen-redonda" src="http://via.placeholder.com/40x40">
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?=$empresa->nombre?>
                        </div>
                        <div class="col-md-4">
                            <?=$empresa->descripcion?>
                        </div>
                        <div class="col-md-2">
                            <a class="essi-color-text" style="cursor: pointer" title="Editar Empresa <?=$empresa->nombre?>" onclick="acciontipo(5,<?=$empresa->id?>)"><i class="fa fa-pencil"></i></a>
                            <a class="essi-color-text" style="cursor: pointer" title="Eliminar Empresa <?=$empresa->nombre?>" onclick="acciontipo(6,<?=$empresa->id?>)"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                    <?php $i++; }
                    if(count($empresas)==0){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><em>No hay registros de empresas de Freelance</em></p>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                $data['html_listaempresa'] = ob_get_contents();
                ob_end_clean();
                break;
            case 5:
                ob_start(); ?>
                <div class="content">
                   <form method="POST" action="" id="formulario" enctype="multipart/form-data">
                   <input type="hidden" name="_token" value="<?=$request->_token?>">
                   <input type="hidden" name="action" value="6">
                    <div class="row">
                       <div class="col-md-12 text-center">
                           <div class="dropzone logo-empresa" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                                <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                            </div>
                       </div>
                        <div class="col-md-12">
                            <label for="nombre_empresa">Nombre de la empresa</label>
                            <input class="form-control" type="text" id="nombre_empresa" onkeyup="max_campo(this.value,70,'#texto_nombre_empresa',this.id)" name="formulario[nombre]" placeholder="Nombre de la empresa" required>
                            <p class="text-right invisible" id="texto_nombre_empresa"><a class="text-success"><i class="fa fa-pencil"></i>... 1/70</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="descripcion_empresa">Descripción de la empresa</label>
                            <textarea class="form-control" type="text" id="descripcion_empresa" name="formulario[descripcion]" rows="8" onkeyup="max_campo(this.value,350,'#texto_descripcion_empresa',this.id)" required="required"></textarea>
                            <p class="text-right invisible" id="texto_descripcion_empresa"><a class="text-success"><i class="fa fa-pencil"></i>... 1/350</a></p>
                        </div>
                    </div>
                    </form>
                </div>
                <?php
                $data['html_crearempresa'] = ob_get_contents();
                ob_end_clean();
                break;
            case 6:
                $formulario = $request->formulario;
                $dato = new FreelanceEmpresas;
                //Guardar Foto
                $logo = $request->logo_values;
                if (!empty($logo)) {
                    $imagen = json_decode($logo);
                    $data = explode( ',', $imagen->data );
                    $fotografia = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $LOGOEMPRESA =$antenombre.".jpg";
                    $dato->logo = $LOGOEMPRESA;
                    \Storage::disk('freelanceEmpresa')->put($LOGOEMPRESA,  $fotografia);
                }
                foreach($formulario as $index=>$value){
                    $dato->$index = $value;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Editar Empresa*/
            case 7:
                $empresa = FreelanceEmpresas::find($request->id);
                ob_start(); ?>
                <div class="content">
                   <?php if($empresa->logo != ''){ ?>
                    <style>.logo-empresa{background-image: url('/storage/Freelance/empresas/<?=$empresa->logo?>');} .logo-empresa:after{content: '' !important;}</style>
                   <?php } ?>
                   <form method="POST" action="" id="formulario" enctype="multipart/form-data">
                   <input type="hidden" name="_token" value="<?=$request->_token?>">
                   <input type="hidden" name="action" value="8">
                   <input type="hidden" name="id" value="<?=$request->id?>">
                    <div class="row">
                       <div class="col-md-12 text-center">
                           <div class="dropzone logo-empresa" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                                <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                            </div>
                       </div>
                        <div class="col-md-12">
                            <label for="nombre_empresa">Nombre de la empresa</label>
                            <input class="form-control" type="text" id="nombre_empresa" onkeyup="max_campo(this.value,70,'#texto_nombre_empresa',this.id)" name="formulario[nombre]" placeholder="Nombre de la empresa" value="<?=$empresa->nombre?>" required>
                            <p class="text-right invisible" id="texto_nombre_empresa"><a class="text-success"><i class="fa fa-pencil"></i>... 1/70</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="descripcion_empresa">Descripción de la empresa</label>
                            <textarea class="form-control" type="text" id="descripcion_empresa" name="formulario[descripcion]" rows="8" onkeyup="max_campo(this.value,350,'#texto_descripcion_empresa',this.id)" required="required"><?=$empresa->descripcion?></textarea>
                            <p class="text-right invisible" id="texto_descripcion_empresa"><a class="text-success"><i class="fa fa-pencil"></i>... 1/350</a></p>
                        </div>
                    </div>
                    </form>
                </div>
                <?php
                $data['html_editarempresa'] = ob_get_contents();
                ob_end_clean();
                break;
            case 8:
                $formulario = $request->formulario;
                $dato = FreelanceEmpresas::find($request->id);
                $logo_anterior = $dato->logo;
                //Guardar Foto
                $logo = $request->logo_values;
                if (!empty($logo)) {
                    $imagen = json_decode($logo);
                    $data = explode( ',', $imagen->data );
                    $fotografia = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $LOGOEMPRESA =$antenombre.".jpg";
                    $dato->logo = $LOGOEMPRESA;
                    \Storage::disk('freelanceEmpresa')->put($LOGOEMPRESA,  $fotografia);
                    if($logo_anterior!=""){
                        Storage::delete('/Freelance/empresas/'.$logo_anterior);
                    }
                }
                foreach($formulario as $index=>$value){
                    $dato->$index = $value;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Eliminar Empresa*/
            case 9:
                $dato = FreelanceEmpresas::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            case 10:
                $dato = new FreelanceTipos;
                foreach($request->formulario as $index=>$value){
                    $dato->$index=$value;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            case 11:
                $empresas = FreelanceEmpresas::all();
                $tipo_f = FreelanceTipos::find($request->id);
                ob_start(); ?>
                <div class="content">
                   <form method="POST" action="" id="formulario" enctype="multipart/form-data">
                   <input type="hidden" name="_token" value="<?php echo $request->_token; ?>">
                   <input type="hidden" value="12" name="action">
                   <input type="hidden" value="<?php echo $tipo_f->id; ?>" name="id">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nombre_tipo_editar">Nombre del tipo</label>
                            <input class="form-control" type="text" value="<?php echo $tipo_f->nombre; ?>" id="nombre_tipo_editar" onkeyup="max_campo(this.value,70,'#nombre_tipo_editar',this.id)" name="formulario[nombre]" required>
                            <p class="text-right invisible" id="texto_nombre_tipo_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 1/70</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="descripcion_tipo_editar">Descripción del tipo</label>
                            <textarea class="form-control" type="text" id="descripcion_tipo_editar" name="formulario[descripcion]" rows="8" onkeyup="max_campo(this.value,350,'#texto_descripcion_tipo_editar',this.id)" required="required"><?php echo $tipo_f->descripcion; ?></textarea>
                            <p class="text-right invisible" id="texto_descripcion_tipo_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 1/350</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="empresa_tipo">Empresa</label>
                            <select class="form-control" name="formulario[empresa]" id="empresa_tipo" style="height:57%;" onchange="empresatipo(this.value)" required>
                                <option value="0" <?php if($tipo_f->empresa == 0){ echo 'Selected'; } ?>>Si</option>
                                <option value="1" <?php if($tipo_f->empresa == 1){ echo 'Selected'; } ?>>No</option>
                            </select>
                        </div>
                        <div class="col-md-10 id_empresa_tipo mt-4" <?php if($tipo_f->empresa == 1){ ?>style="display: none;" <?php } ?> >
                            <label for="id_empresa_tipo">Seleccione la Empresa</label>
                            <select class="form-control" name="formulario[id_freelance_empresa]" style="height:57%;">
                                <option value="">Seleccione una opción</option>
                                <?php foreach($empresas as $empresa){ ?>
                                <option value="<?php echo $empresa->id; ?>" <?php if($tipo_f->id_freelance_empresa == $empresa->id){ ?> Selected <?php } ?> ><?php echo $empresa->nombre; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2 id_empresa_tipo text-center mt-4" <?php if($tipo_f->empresa == 1){ ?>style="display: none;" <?php } ?>>
                            <p class="mt-5"><a class="essi-color-text" style="cursor: pointer" title="Crear empresa" onclick="acciontipo(3)"><i class="fa fa-plus"></i></a></p>
                        </div>
                    </div>
                    </form>
                </div>
                <?php
                $data['html_editartipo'] = ob_get_contents();
                ob_end_clean();
                break;
            case 12:
                $dato = FreelanceTipos::find($request->id);
                foreach($request->formulario as $index=>$value){
                    $dato->$index=$value;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Eliminar Tipo de Freelance*/
            case 13:
                $dato = FreelanceTipos::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            /*Modificar el tipo de freelance*/
            case 14:
                $tipos = FreelanceTipos::all();
                $usuario = FreelanceUser::find($request->id);
                ob_start(); ?>
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="tipo_freelance_modificar">Tipos de Freelance para <?=$request->nombre?></label>
                            <select class="form-control" id="tipo_freelance_modificar" style="height: 37px;">
                                <option value="">Seleccione una opción</option>
                                <?php foreach($tipos as $tipo){ ?>
                                <option value="<?=$tipo->id?>" <?php if($usuario->id_tipo_freelance == $tipo->id){ echo "Selected"; } ?> ><?=$tipo->nombre?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 15:
                $dato = FreelanceUser::find($request->id);
                $dato->id_tipo_freelance = $request->tipo;
                if($dato->save()){
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar!";
                }
                break;
            /*Listado de Categorias*/
            case 16:
                $categorias = FreelanceCategorias::all();
                ob_start(); ?>
                <div class="row">
                    <div class="col-md-12">
                      <a class="essi-color-text" style="font-size: 3rem; cursor: pointer;" title="Crear una categoria" onclick="accioncategoria(0)"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <?php if(count($categorias) == 0){ ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <em>No hay registros de categoria</em>
                    </div>
                </div>
                <?php }else{ ?>
                <div class="row">
                    <div class="col-md-1"><strong>Item</strong></div>
                    <div class="col-md-2"><strong>Logo</strong></div>
                    <div class="col-md-3"><strong>Nombre</strong></div>
                    <div class="col-md-6"><strong>Descripción</strong></div>
                </div>
                <div class="list-categorias">
                <?php $i=1; foreach($categorias as $categoria){ ?>
                <div class="row">
                    <div class="col-md-1"><?=$i?></div>
                    <div class="col-md-2 text-center">
                        <?php if($categoria->logo!=''){ ?>
                        <img class="imagen-redonda" width="40px" src="/storage/Freelance/categorias/<?=$categoria->logo?>">
                        <?php }else{ ?>
                        <img class="imagen-redonda" src="http://via.placeholder.com/40x40">
                        <?php } ?>
                    </div>
                    <div class="col-md-3 text-justify"><?=$categoria->nombre?></div>
                    <div class="col-md-4 text-justify"><?=$categoria->descripcion?></div>
                    <div class="col-md-2 text-center">
                        <a class="essi-color-text" style="cursor: pointer;" title="Editar el tipo <?=$categoria->nombre?>" onclick="accioncategoria(2,<?=$categoria->id?>)"><i class="fa fa-pencil"></i></a>
                        <a class="essi-color-text" style="cursor: pointer;" title="Eliminar el tipo <?=$categoria->nombre?>" onclick="accioncategoria(4,<?=$categoria->id?>)"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
                <?php
                $i++;
                } ?>
                </div>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 17:
                ob_start(); ?>
                <div class="content">
                   <form method="POST" action="" id="formulario" enctype="multipart/form-data">
                   <input type="hidden" name="_token" value="<?=$request->_token?>">
                   <input type="hidden" name="action" value="18">
                    <div class="row">
                       <div class="col-md-12 text-center">
                           <div class="dropzone logo-empresa" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                                <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                            </div>
                       </div>
                        <div class="col-md-12">
                            <label for="nombre_categoria">Nombre de la categoria</label>
                            <input class="form-control" type="text" id="nombre_categoria" onkeyup="max_campo(this.value,70,'#texto_nombre_categoria',this.id)" name="formulario[nombre]" placeholder="Nombre de la categoria" required>
                            <p class="text-right invisible" id="texto_nombre_categoria"><a class="text-success"><i class="fa fa-pencil"></i>... 1/70</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="descripcion_categoria">Descripción de la categoria</label>
                            <textarea class="form-control" type="text" id="descripcion_categoria" name="formulario[descripcion]" rows="8" onkeyup="max_campo(this.value,350,'#texto_descripcion_categoria',this.id)" required="required"></textarea>
                            <p class="text-right invisible" id="texto_descripcion_categoria"><a class="text-success"><i class="fa fa-pencil"></i>... 1/350</a></p>
                        </div>
                    </div>
                    </form>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 18:
                $formulario = $request->formulario;
                $dato = new FreelanceCategorias;
                //Guardar Foto
                $logo = $request->logo_values;
                if (!empty($logo)) {
                    $imagen = json_decode($logo);
                    $data = explode( ',', $imagen->data );
                    $fotografia = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $LOGOCATEGORIA =$antenombre.".jpg";
                    $dato->logo = $LOGOCATEGORIA;
                    \Storage::disk('freelanceCategoria')->put($LOGOCATEGORIA,  $fotografia);
                }
                foreach($formulario as $index=>$value){
                    $dato->$index = $value;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Editar Categoria*/
            case 19:
                $categoria = FreelanceCategorias::find($request->id);
                ob_start(); ?>
                <?php if($categoria->logo != ''){ ?>
                    <style>.logo-categoria{background-image: url('/storage/Freelance/categorias/<?=$categoria->logo?>');} .logo-categoria:after{content: '' !important;}</style>
                   <?php } ?>
                <div class="content">
                   <form method="POST" action="" id="formulario" enctype="multipart/form-data">
                   <input type="hidden" name="_token" value="<?=$request->_token?>">
                   <input type="hidden" name="action" value="20">
                   <input type="hidden" name="id" value="<?=$request->id?>">
                    <div class="row">
                       <div class="col-md-12 text-center">
                           <div class="dropzone logo-categoria" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                                <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                            </div>
                       </div>
                        <div class="col-md-12">
                            <label for="nombre_categoria_editar">Nombre de la categoria</label>
                            <input class="form-control" type="text" id="nombre_categoria_editar" onkeyup="max_campo(this.value,70,'#texto_nombre_categoria_editar',this.id)" name="formulario[nombre]" placeholder="Nombre de la categoria" value="<?=$categoria->nombre?>" required>
                            <p class="text-right invisible" id="texto_nombre_categoria_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 1/70</a></p>
                        </div>
                        <div class="col-md-12">
                            <label for="descripcion_categoria_editar">Descripción de la categoria</label>
                            <textarea class="form-control" type="text" id="descripcion_categoria_editar" name="formulario[descripcion]" rows="8" onkeyup="max_campo(this.value,350,'#texto_descripcion_categoria_editar',this.id)" required="required"><?=$categoria->descripcion?></textarea>
                            <p class="text-right invisible" id="texto_descripcion_categoria_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 1/350</a></p>
                        </div>
                    </div>
                    </form>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 20:
                $formulario = $request->formulario;
                $dato = FreelanceCategorias::find($request->id);
                $logo_anterior = $dato->logo;
                //Guardar Foto
                $logo = $request->logo_values;
                if (!empty($logo)) {
                    $imagen = json_decode($logo);
                    $data = explode( ',', $imagen->data );
                    $fotografia = base64_decode($data[1]);

                    $antenombre = uniqid();
                    $LOGOCATEGORIA =$antenombre.".jpg";
                    $dato->logo = $LOGOCATEGORIA;
                    \Storage::disk('freelanceCategoria')->put($LOGOCATEGORIA,  $fotografia);
                    if($logo_anterior!=""){
                        Storage::delete('/Freelance/categorias/'.$logo_anterior);
                    }
                }
                foreach($formulario as $index=>$value){
                    $dato->$index = $value;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Eliminar una categoria*/
            case 21:
                $dato = FreelanceCategorias::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            /*Modificar Categoria*/
            case 22:
                $categorias = FreelanceCategorias::all();
                $usuario = FreelanceUser::find($request->id);
                ob_start(); ?>
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="categoria_freelance_modificar">Categoria Freelance para <?=$request->nombre?></label>
                            <select class="form-control" id="categoria_freelance_modificar" style="height: 37px;">
                                <option value="">Seleccione una opción</option>
                                <?php foreach($categorias as $categoria){ ?>
                                <option value="<?=$categoria->id?>" <?php if($usuario->id_freelance_categoria == $categoria->id){ echo "Selected"; } ?> ><?=$categoria->nombre?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 23:
                $dato = FreelanceUser::find($request->id);
                $dato->id_freelance_categoria = $request->categoria;
                if($dato->save()){
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar!";
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function index_bitacora(){
        $query = 'SELECT O.id, O.titulo, O.id_user_freelance, (SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = O.id AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1) AS VALOR FROM oportunidades O WHERE O.id_user_freelance IS NOT NULL ORDER BY O.id ASC';
        $data['oportunidades'] = DB::select($query);
        $data['usuarios'] = FreelanceUser::all();
        $data['tipo_gasto'] = FreelanceTipoGastos::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 56 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('freelance.bitacora',['data' => $data]);
    }

    public function bitacora_accion(Request $request){
        switch($request->action){
            /*Lista de actividades*/
            case 0:
                $lista = ViewFreelanceListaBitacoras::all();
                ob_start();
                $i=1;
                foreach($lista as $actividad){
                ?>
                <tr onclick="ver_actividad(<?=$actividad->id?>)" style="cursor:pointer;">
                    <td><?=$i?></td>
                    <td><img class="img-redonda" src="<?=$actividad->RUTAFOTO?>" style="width:30px;background: none;cursor:pointer;" data-toggle="tooltip" data-placement="top" title="<?=$actividad->FREELANCE?>"><br><?=$actividad->NOMBRECORTOFREELANCE?></td>
                    <td><?=$actividad->TIPOBITACORA?></td>
                    <td><?php if($actividad->OPORTUNIDAD!=""){ echo $actividad->OPORTUNIDAD; }else{ echo '<em>No hay oportunidad</em>'; } ?></td>
                    <td><?=$this->fecha($actividad->FECHAINICIO)?></td>
                    <td><?=$this->fecha($actividad->FECHAFIN)?></td>
                    <td><?=$actividad->TIPOBITACORA?></td>
                    <td><?=$this->fecha($actividad->CREADO)?></td>
                </tr>
                <?php
                    $i++;
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Nombre de la oportunidad*/
            case 1:
                $dato = Oportunidades::find($request->id);
                if(isset($dato->titulo)){
                    $data['nombre_oportunidad'] = $dato->titulo;
                }else{
                    $data['nombre_oportunidad'] = 'No existe!';
                }

                break;
            /*Guardar la actividad de bitacora*/
            case 2:
                if($request->formulario['tipo_bitacora'] == 0 && $request->formulario['id_oportunidad'] == ""){
                    $data['msj'] = 'Debe elegir una oportunidad';
                }else if($request->formulario['descripcion'] == '<!DOCTYPE html>
<html>
<head>
</head>
<body>

</body>
</html>'){
                    $data['msj'] = 'Debe llenar la descripción';
                }else if($request->formulario['fecha_inicio'] > $request->formulario['fecha_final']){
                    /*Validación de fecha*/
                    $data['msj'] = 'La fecha de inicio debe ser menor a la fecha final';
                }else if($request->formulario['fecha_inicio'] == $request->formulario['fecha_final'] && $request->formulario['hora_inicio'] >= $request->formulario['hora_final']){
                    /*Validación de hora*/
                    $data['msj'] = 'La hora de inicio debe ser menor a la hora final';
                }else{
                    $dato = new FreelanceBitacoras;
                    foreach($request->formulario as $index=>$value){
                        $dato->$index=$value;
                    }
                    if($dato->save()){
                        $id=$dato->id;
                        foreach($request->gastos as $gastos){
                            $dato2 = new FreelanceBitacoraGastos;
                            $vacio = 0;
                            foreach($gastos as $index=>$value){
                                /*Si es vacio sumar en la variable para no guardar*/
                                if($value==''){
                                    $vacio++;
                                }
                                $dato2->$index = $index=="valor"? intval(str_replace("$ ", "", str_replace(",", "", $value))):$value;
                            }
                            $dato2->id_bitacora = $id;
                            if($vacio==0){
                                $dato2->save();
                            }
                        }
                        $data['msj'] = 'Guardado correctamente!';
                    }else{
                        $data['msj'] = 'Error al guardar!';
                    }
                }
                break;
            /*Elegir freelance*/
            case 3:
                $user = ViewFreelanceLista::find($request->id);
                $data['html'] = 'no existe';
                if(isset($user['id'])){
                    if($user['FOTOPERFIL'] != ""){
                        $data['FOTOPERFIL'] = $user['FOTOPERFIL'];
                    }else{
                        $data['FOTOPERFIL'] = 'http://via.placeholder.com/60x60';
                    }
                    ob_start(); ?>
                    <div class="row">
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="<?=$user['NOMBRECORTO']?>" style="cursor:no-drop" readonly>
                        </div>
                        <div class="col-md-2">
                            <a class="text-warning" style="font-size:2rem;cursor:pointer;" onclick="modificar_usuario()"><i class="fa fa-pencil"></i></a>
                        </div>
                    </div>
                    <?php
                    $data['html'] = ob_get_contents();
                    ob_end_clean();
                }
                break;
            case 4:
                $data['tipo_gasto'] = FreelanceTipoGastos::all();
                ob_start(); ?>
                <div class="col-md-5 mt-3 G-<?=$request->numero?>">
                    <select class="form-control" name="gastos[<?=$request->numero?>][id_tipo_gasto]" style="height:33px;">
                        <option value="">Selccione un tipo de gasto</option>
                        <?php foreach($data['tipo_gasto'] as $tipo){ ?>
                        <option value="<?=$tipo->id?>"><?=$tipo->nombre?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-5 mt-3 G-<?=$request->numero?>">
                    <input type="text" class="form-control" name="gastos[<?=$request->numero?>][valor]" id="valorG_<?=$request->numero?>" onkeyup="formato_numero(this.value,this.id)"  Placeholder="$ 0">
                </div>
                <div class="col-sm-12 mt-3 col-md-2 G-<?=$request->numero?>">
                    <a class="text-danger" style="font-size:2rem;" onclick="eliminar_item_gasto('.G-<?=$request->numero?>')"><i class="fa fa-trash"></i></a>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 5:
                $tipos = FreelanceTipoGastos::all();
                ob_start(); ?>
                <div class="container">
                <div class="row my-3">
                   <div class="col-md-12 text-center">
                       <a class="text-success" title="Crear un tipo de gasto" style="cursor:pointer;font-size:6rem;" onclick="accion(3)"><i class="fa fa-plus"></i></a>
                   </div>
                </div>
                <?php if(count($tipos)>0){ ?>
                <div class="row">
                   <div class="col-md-1">
                       <h4>Item</h4>
                   </div>
                    <div class="col-md-9 text-center">
                        <h4>Tipo de Gasto</h4>
                    </div>
                    <div class="col-md-2 text-center">
                        <h4>Acción</h4>
                    </div>
                </div>
                <?php $i=1; foreach($tipos as $tipo){ ?>
                <div class="row">
                   <div class="col-md-1">
                       <h5><strong><?=$i?></strong></h5>
                   </div>
                    <div class="col-md-9 text-center">
                        <h5><?=$tipo->nombre?></h5>
                    </div>
                    <div class="col-md-2 text-center">
                        <a class="text-warning" title="Editar <?=$tipo->nombre?>"  style="cursor:pointer;font-size:2rem;" onclick="accion(4,<?=$tipo->id?>)"><i class="fa fa-pencil"></i></a>
                        <a class="text-danger" title="Eliminar <?=$tipo->nombre?>"  style="cursor:pointer;font-size:2rem;" onclick="accion(5,<?=$tipo->id?>)"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
                <?php $i++; } }else{ ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="h5"><em>No hay registros</em></p>
                    </div>
                </div>
                <?php } ?>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 6:
                ob_start(); ?>
                <div class="container">
                    <div class="col-md-12">
                        <label for="nombre_gasto">Nombre</label><br>
                        <input type="form-control" style="width:100%;height:36px;" id="nombre_gasto" name="nombre" onkeyup="max_campo(this.value,100,'#texto_nombre_gasto',this.id)">
                        <p class="text-right invisible" id="texto_nombre_gasto"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 7:
                $dato = new freelanceTipoGastos;
                $dato->nombre = $request->nombre;
                if($dato->save()){
                    $data['msj']='Guardado correctamente!';
                }else{
                    $data['msj']='Error al guardar!';
                }
                break;
            case 8:
                $tipo = FreelanceTipoGastos::find($request->id);
                ob_start(); ?>
                <div class="container">
                    <div class="col-md-12">
                        <label for="editar_nombre_gasto">Nombre</label><br>
                        <input type="hidden" id="editar_id_gasto" value="<?=$tipo->id?>">
                        <input type="form-control" style="width:100%;height:36px;" id="editar_nombre_gasto" name="nombre" onkeyup="max_campo(this.value,100,'#texto_editar_nombre_gasto',this.id)" value="<?=$tipo->nombre?>">
                        <p class="text-right invisible" id="texto_editar_nombre_gasto"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 9:
                $dato = freelanceTipoGastos::find($request->id);
                $dato->nombre = $request->nombre;
                if($dato->save()){
                    $data['msj']='Guardado correctamente!';
                }else{
                    $data['msj']='Error al guardar!';
                }
                break;
            case 10:
                $dato = FreelanceTipoGastos::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            case 11:
                $query = 'SELECT O.id, O.titulo, O.id_user_freelance, (SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = O.id AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1) AS VALOR FROM oportunidades O WHERE O.id_user_freelance = '.$request->id.' ORDER BY O.id ASC';
                $oportunidades = DB::select($query);
                ob_start();
                if(count($oportunidades>0)){
                foreach($oportunidades as $oportunidad){ ?>
<option value="<?=$oportunidad->id?>"><?=$oportunidad->titulo?></option>
                <?php } }else{ ?>
<option value="">No hay oportunidades para este freelance</option>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function ver($id){
        $data['id'] = $id;
        $data['oportunidades'] = DB::select('SELECT id FROM oportunidades ORDER BY id DESC LIMIT 0,1');
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 56 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('freelance.veruser',['data' => $data]);
    }

    public function ver_accion(Request $request){
        $accion = $request->action;
        switch($accion){
            /*Ver General*/
            case 0:
                $user = ViewFreelanceLista::find($request->id);
                $estudios = FreelanceEstudioRealizados::where('id_freelance_user',$request->id)->get();
                $paisessecundarios = DB::select('SELECT P.name AS nombre, P.imagen AS imagen FROM freelance_pais_secundarios FP INNER JOIN paises P ON FP.id_pais = P.id WHERE FP.id_freelance_user = '.$request->id.' AND FP.deleted_at IS NULL ORDER BY FP.id ASC');
                $archivospdf = FreelanceUserArchivos::where('id_freelance_user',$request->id)->where('tipo',0)->get();
                $imagenes = FreelanceUserArchivos::where('id_freelance_user',$request->id)->where('tipo',1)->get();
                $comentarios = FreelanceComentarios::where('id_freelance_user',$request->id)->get();
                ob_start(); ?>
                <div class="col-sm-12 col-md-3">
                    <img class="imagen-redonda" src="<?=$user->FOTOPERFIL?>"><br>
                    <img class="mt-5" src="<?=$user->FOTOCOMPLETA?>">
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="row">
                        <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Información Personal</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="nombres">Nombres</label>
                            <input class="form-control" type="text" id="nombres" placeholder="Nombres" value="<?=$user->nombres?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="apellidos">Apellidos</label>
                            <input class="form-control" type="text" id="apellidos" placeholder="Apellidos" value="<?=$user->apellidos?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="tipo_documento">Tipo Documento</label>
                            <input class="form-control" type="text" id="tipo_documento" placeholder="Tipo documento" value="<?=$user->TIPODOCUMENTO?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="numero_documento">Numero Documento</label>
                            <input class="form-control" type="text" id="numero_documento" placeholder="Numero Documento" value="<?=$user->numero_documento?>" readonly>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="fecha_nacimiento">Fecha Nacimiento</label>
                            <input class="form-control" type="text" id="fecha_nacimiento" placeholder="Fecha Nacimiento" value="<?=$user->FECHANACIMIENTO?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="tipo_sangre">Tipo de sangre</label>
                            <input class="form-control" type="text" id="tipo_sangre" placeholder="Tipo de sangre" value="<?=$user->TIPOSANGRE?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="telefono_personal">Telefono personal</label>
                            <input class="form-control" type="text" id="telefono_personal" placeholder="Telefono Personal" value="<?=$user->telefono_personal?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="telefono_corporativo">Telefono Corporativo</label>
                            <input class="form-control" type="text" id="telefono_corporativo" placeholder="Telefono Corporativo" value="<?=$user->telefono_corporativo?>" readonly>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                            <label for="correo_personal">Correo Personal</label>
                            <input class="form-control" type="text" id="correo_personal" placeholder="Correo Personal" value="<?=$user->correo_personal?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                            <label for="correo_corporativo">Correo Corporativo</label>
                            <input class="form-control" type="text" id="correo_corporativo" placeholder="Correo Corporativo" value="<?=$user->correo_corporativo?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                            <label for="profesion">Profesión</label>
                            <input class="form-control" type="text" id="profesion" placeholder="Profesión" value="<?=$user->profesion?>" readonly>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 col-lg-12 text-left">
                            <label for="descripcion_perfil">Descripción del perfil</label>
                            <textarea class="form-control" id="descripcion_perfil" rows="8" readonly><?=$user->descripcion_perfil?></textarea>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                            <label for="pais_nacimiento">Pais Nacimiento</label>
                            <input class="form-control" type="text" id="pais_nacimiento" placeholder="Pais Nacimiento" value="<?=$user->PAISNACIMIENTO?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                            <label for="estado_nacimiento">Estado Nacimiento</label>
                            <input class="form-control" type="text" id="estado_nacimiento" placeholder="Estado Nacimiento" value="<?=$user->ESTADONACIMIENTO?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                            <label for="ciudad_nacimiento">Ciudad Nacimiento</label>
                            <input class="form-control" type="text" id="ciudad_nacimiento" placeholder="Ciudad Nacimiento" value="<?=$user->CIUDADNACIMIENTO?>" readonly>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Información Ubicación</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="pais_ubicacion">Pais</label>
                            <input class="form-control" type="text" id="pais_ubicacion" placeholder="Pais Ubicación" value="<?=$user->PAISUBICACION?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="estado_ubicacion">Estado</label>
                            <input class="form-control" type="text" id="estado_ubicacion" placeholder="Estado Ubicación" value="<?=$user->ESTADOUBICACION?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="ciudad_ubicacion">Ciudad</label>
                            <input class="form-control" type="text" id="ciudad_ubicacion" placeholder="Ciudad Ubicación" value="<?=$user->CIUDADUBICACION?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 text-left">
                            <label for="direccion_ubicacion">Dirección</label>
                            <input class="form-control" type="text" id="direccion_ubicacion" placeholder="Direccion Ubicación" value="<?=$user->direccion_ubicacion?>" readonly>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Estudios Realizados</h3>
                        </div>
                    </div>
                    <? foreach($estudios as $estudio){ ?>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                            <label for="educacion_titulo_0">Titulo Obtenido</label>
                            <input class="form-control" type="text" id="educacion_titulo_0" placeholder="Titulo Obtenido" value="<?=$estudio->titulo_obtenido?>" readonly>
                        </div>
                        <div class="col-sm-12  col-md-4 col-lg-4 text-left mt-3">
                            <label for="educacion_ano_0">Año culminación</label>
                            <input class="form-control" type="number" id="educacion_ano_0" min="1900" max="2018" placeholder="Año culminación" value="<?=$estudio->ano?>" readonly>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                            <label for="educacion_institucion_0">Institución</label>
                            <input class="form-control" type="text" id="educacion_institucion_0" placeholder="Institución" value="<?=$estudio->institucion?>" readonly>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(count($estudios)==0){ ?>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <p class="h4"><strong><em>No hay Registro de estudios</em></strong></p>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-sm-12 col-md-12">
                    <div class="row mt-3">
                        <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Vinculación ESSI</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6 text-left">
                            <label for="fecha_vinculacion_essi">Fecha Vinculación</label>
                            <input class="form-control" type="text" id="fecha_vinculacion_essi" placeholder="Fecha Vinculación" value="<?=$user->FECHAVINCULACION?>" readonly>
                        </div>
                        <div class="col-md-6 text-left">
                            <label for="cargo">Cargo</label>
                            <input class="form-control" type="text" id="cargo" placeholder="Cargo" value="Freelance" readonly>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-6 text-left text-white bg-ESSI shadow-2">
                            <h3>Pais Principal</h3>
                        </div>
                        <div class="col-sm-12 col-md-6 text-left text-white bg-ESSI shadow-2">
                            <h3>Paises Secundarios</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-6 text-center">
                            <?php if($user->FOTOPAISPRINCIPAL != ''){ ?>
                            <img class="form-control pais-p" src="<?=$user->FOTOPAISPRINCIPAL?>">
                            <?php }else{ ?>
                            <p class="h5 my-4"><strong><em>No tiene pais principal</em></strong></p>
                            <?php } ?>
                        </div>
                        <div class="col-sm-12 col-md-6 text-center">
                           <div class="row">
                               <?php foreach($paisessecundarios as $pais){ ?>
                                <div class="col-sm-6 col-md-3">
                                    <img class="form-control pais-s" src="/images/icons/<?=$pais->imagen?>">
                                </div>
                                <?php } ?>
                                <?php if(count($paisessecundarios)==0){ ?>
                                <p class="h5 my-4"><strong><em>No tiene paises secundarios</em></strong></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Calendario</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12">
                            <p class="h3 my-5 text-center"><strong><em>Calendario del usuario</em></strong></p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Archivos PDF</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12">
                          <p class="text-center my-4"><a class="text-success" style="font-size:5rem;cursor:pointer;" onclick="cargar_archivo(0)" data-toggle="tooltip" data-placement="top" title="Agregar un archivo PDF"><i class="fa fa-plus"></i></a></p>
                           <?php if(count($archivospdf)){ ?>
                            <div class="row">
                               <?php foreach($archivospdf as $archivo){ ?>
                                <div class="col-sm-6 col-md-2 my-3">
                                    <a class="text-danger" style="font-size:4rem;cursor:pointer;" onclick="abrir('<?=$archivo->archivo?>')"><i class="fa fa-file-pdf-o"></i></a>
                                    <p class="h5"><strong><?=$archivo->nombre?></strong></p>
                                    <p class="h6">Creado: <?=$this->fecha($archivo->created_at)?></p>
                                </div>
                                <?php } ?>
                            </div>
                            <?php }else{ ?>
                            <p class="h3 my-5 text-center"><strong><em>No hay Archivos PDF adjuntos</em></strong></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Imagenes</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12">
                          <p class="text-center my-4"><a class="text-success" style="font-size:5rem;cursor:pointer;" onclick="cargar_archivo(1)" data-toggle="tooltip" data-placement="top" title="Agregar una Imagen"><i class="fa fa-plus"></i></a></p>
                           <?php if(count($imagenes)){ ?>
                            <div class="row" style="margin-left: 10%">
                               <?php foreach($imagenes as $archivo){ ?>
                                <div class="col-md-2 col-archivo">
                                   <a data-gallery href="<?=$archivo->archivo?>"><img src="<?=$archivo->archivo?>" class="icono-video"  data-toggle="tooltip" data-placement="bottom" title="<?=$archivo->nombre?> <?=$this->fecha($archivo->created_at)?>"></a>
                                </div>
                                <?php } ?>
                            </div>
                            <?php }else{ ?>
                            <p class="h3 my-5 text-center"><strong><em>No hay Imagenes adjuntas</em></strong></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Comentarios</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="comments col-md-12 pt-5">
                            <?php foreach($comentarios as $comentario){
                                $user=User::find($comentario->id_user);
                            ?>
                            <div class="comment-wrap">
                                <div class="photo">
                                    <div class="avatar" style="background-image: url('/images/file/clientes/<?=$user->foto?>')"></div>
                                </div>
                                <div class="comment-block">
                                    <p class="comment-text text-left">
                                        <?=$comentario->comentario?>
                                    </p>
                                    <div class="bottom-comment">
                                        <div class="comment-date">
                                            <?=date("d/m/Y (h:i:s A)",strtotime($comentario->created_at))?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                        <div class="col-md-12 pt-4">
                            <div class="comment-wrap">
                                <div class="photo">
                                    <div class="avatar" style="background-image: url('/images/file/clientes/<?=Auth::user()->foto?>')"></div>
                                </div>
                                <div class="comment-block">
                                    <form autocomplete="off" id="messages_box">
                                       <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..." maxlength="600"></textarea>
                                    </form>
                                    <div class="pull-right">
                                    <button type="button" class="btn btn-success" onclick="comentar()" id="send_btn">Enviar <i class="fa fa-paper-plane"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Guardar una imagen*/
            case 1:
                $image = $request->file('archivo_att');
                $imageName = uniqid();
                $imegeExtension = $image->getClientOriginalExtension();
                $imageName = $imageName.'.'.$imegeExtension;
                $NameOriginal = $image->getClientOriginalName();
                $image->move(public_path('storage/Freelance/users/adjuntos'),$imageName);

                $dato = new FreelanceUserArchivos;
                $dato->nombre = $request->nombre;
                $dato->tipo = $request->tipo;
                $dato->archivo = "/storage/Freelance/users/adjuntos/".$imageName;
                $dato->id_freelance_user = $request->id_freelance_user;
                $dato->save();
                $data['msj'] = 'Guardado correctamente!';
                break;
            /*Listado de oportunidades*/
            case 2:
                $query = 'SELECT O.id AS id, O.titulo, (SELECT CONCAT(H.value," %") FROM historial_oportunidades as H WHERE H.oportunidad_id = O.id AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1) AS ciclo, (SELECT CONCAT("$ ",FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") FROM historial_oportunidades as H WHERE H.oportunidad_id = O.id AND H.key LIKE "val_pesos" ORDER BY H.id DESC LIMIT 0,1) AS valor, O.empresa_id FROM oportunidades O WHERE O.id_user_freelance = '.$request->id.' ORDER BY O.id DESC';
                $oportunidades = DB::select($query);
                //$oportunidades = Oportunidades::all();
                ob_start(); ?>
                <div class="col-md-12">
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Oportunidad Vigentes</h3>
                        </div>
                    </div>
                    <div class="row mt-3 mb-5">
                        <div class="col-sm-12 col-md-12">
                            <p class="h3 my-3"><a class="text-success" style="font-size:5rem;cursor:pointer;" data-toggle="tooltip" data-placement="top" title="añadrir oportunidad" onclick="anadir_oportunidad(0)"><i class="fa fa-plus"></i></a></p>
                        </div>
                       <?php $vigente=0; foreach($oportunidades as $oportunidad){
                        if(intval($oportunidad->ciclo)>0 && intval($oportunidad->ciclo)<90){
                        $empresa = Empresa::find($oportunidad->empresa_id);
                        ?>
                       <div class="col-sm-6 col-md-2 mt-5">
                          <?php if($empresa->logo != ""){ ?>
                           <img class="imagen-redonda" style="width: 80px;" src="/images/file/empresas/principal/<?=$empresa->logo?>" data-toggle="tooltip" data-placement="top" title="<?=$oportunidad->titulo?>">
                           <?php }else{ ?>
                           <img class="imagen-redonda" src="http://via.placeholder.com/80x80" data-toggle="tooltip" data-placement="top" title="<?=$oportunidad->titulo?>">
                           <?php } ?>
                           <p class="h5"><strong><?=$empresa->nombre?></strong></p>
                           <p class="h6"><?=$oportunidad->valor?></p>
                           <p class="h5"><strong><?=$oportunidad->ciclo?></strong></p>
                           <p><a class="text-danger" style="font-size:2rem;cursor:pointer;" onclick="eliminaroportunidad(<?=$oportunidad->id?>)" data-toggle="tooltip" data-placement="top" title="Eliminar <?=$oportunidad->titulo?> del listado"><i class="fa fa-trash"></i></a></p>
                       </div>
                       <?php $vigente++; } } ?>
                       <?php if($vigente==0){ ?>
                        <div class="col-sm-12 col-md-12">
                            <p class="h3 my-3"><strong><em>No hay oportunidades Vigentes</em></strong></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Oportunidad Vendidas</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                       <div class="col-sm-12 col-md-12">
                            <p class="h3 my-3"><a class="text-success" style="font-size:5rem;cursor:pointer;" data-toggle="tooltip" data-placement="top" title="añadrir oportunidad" onclick="anadir_oportunidad(1)"><i class="fa fa-plus"></i></a></p>
                        </div>
                       <?php $vendida=0; foreach($oportunidades as $oportunidad){
                        if(intval($oportunidad->ciclo)==90){
                        $empresa = Empresa::find($oportunidad->empresa_id);
                        ?>
                       <div class="col-sm-6 col-md-2 mt-5">
                          <?php if($empresa->logo != ""){ ?>
                           <img class="imagen-redonda" style="width: 80px;" src="/images/file/empresas/principal/<?=$empresa->logo?>" data-toggle="tooltip" data-placement="top" title="<?=$oportunidad->titulo?>">
                           <?php }else{ ?>
                           <img class="imagen-redonda" src="http://via.placeholder.com/80x80" data-toggle="tooltip" data-placement="top" title="<?=$oportunidad->titulo?>">
                           <?php } ?>
                           <p class="h5"><strong><?=$empresa->nombre?></strong></p>
                           <p class="h6"><?=$oportunidad->valor?></p>
                           <p class="h6"><strong><?=$oportunidad->ciclo?></strong></p>
                           <p><a class="text-danger" style="font-size:2rem;cursor:pointer;" onclick="eliminaroportunidad(<?=$oportunidad->id?>)" data-toggle="tooltip" data-placement="top" title="Eliminar <?=$oportunidad->titulo?> del listado"><i class="fa fa-trash"></i></a></p>
                       </div>
                       <?php $vendida++; } } ?>
                       <?php if($vendida==0){ ?>
                        <div class="col-sm-12 col-md-12">
                            <p class="h3 my-3"><strong><em>No hay oportunidades Vendidas</em></strong></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12 col-md-12 text-left text-white bg-ESSI shadow-2">
                            <h3>Oportunidad Perdidas</h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                       <div class="col-sm-12 col-md-12">
                            <p class="h3 my-3"><a class="text-success" style="font-size:5rem;cursor:pointer;" data-toggle="tooltip" data-placement="top" title="añadrir oportunidad" onclick="anadir_oportunidad(2)"><i class="fa fa-plus"></i></a></p>
                        </div>
                       <?php $perdida=0; foreach($oportunidades as $oportunidad){
                        if(intval($oportunidad->ciclo)==0){
                        $empresa = Empresa::find($oportunidad->empresa_id);
                        ?>
                       <div class="col-sm-6 col-md-2 mt-5">
                          <?php if($empresa->logo != ""){ ?>
                           <img class="imagen-redonda" style="width: 80px;" src="/images/file/empresas/principal/<?=$empresa->logo?>" data-toggle="tooltip" data-placement="top" title="<?=$oportunidad->titulo?>">
                           <?php }else{ ?>
                           <img class="imagen-redonda" src="http://via.placeholder.com/80x80" data-toggle="tooltip" data-placement="top" title="<?=$oportunidad->titulo?>">
                           <?php } ?>
                           <p class="h5"><strong><?=$empresa->nombre?></strong></p>
                           <p class="h6"><?=$oportunidad->valor?></p>
                           <p class="h6"><strong><?=$oportunidad->ciclo?></strong></p>
                           <p><a class="text-danger" style="font-size:2rem;cursor:pointer;" onclick="eliminaroportunidad(<?=$oportunidad->id?>)" data-toggle="tooltip" data-placement="top" title="Eliminar <?=$oportunidad->titulo?> del listado"><i class="fa fa-trash"></i></a></p>
                       </div>
                       <?php $perdida++; } } ?>
                       <?php if($perdida==0){ ?>
                        <div class="col-sm-12 col-md-12">
                            <p class="h3 my-3"><strong><em>No hay oportunidades Perdidas</em></strong></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Formulario para agregar una oportunidad Vigente-vendidas-perdidas*/
            case 3:
                switch($request->tipo){
                    /*Vigente*/
                    case 0:
                        $titulo = 'Vigente';
                        $query_condicion = 'WHERE CICLO > 0 AND CICLO < 90';
                        break;
                    /*Vendidas*/
                    case 1:
                        $titulo = 'Vendidas';
                        $query_condicion = 'WHERE CICLO = 90';
                        break;
                    /*Pedidas*/
                    case 2:
                        $titulo = 'Perdidas';
                        $query_condicion = 'WHERE CICLO = 0';
                        break;
                    default:
                        $titulo = '';
                        $query_condicion = '';
                        break;
                }
                $users = DB::select('SELECT DISTINCT(IDRESPONSABLE) FROM view_oportunidades '.$query_condicion.' ORDER BY IDRESPONSABLE ASC');
                ob_start(); ?>
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Oportunidades <?=$titulo?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
          <label for="recipient-name" class="col-form-label mb-4" style="font-size: 2rem !important;">Seleccione un Funcionario</label>
           <div class="row">
              <?php foreach($users as $user){
               $usuario = User::find($user->IDRESPONSABLE);
               $nombre = explode(' ',$usuario->nombres);
               $apellido = explode(' ',$usuario->apellidos);
               ?>
               <div class="col-sm-6 col-md-2">
                   <img src="/images/file/clientes/<?=$usuario->foto?>" class="imagen-redonda user-no" width="50px;" onclick="oportunidadesuser(<?=$usuario->id?>,<?=$request->tipo?>)">
                   <p class="h5"><strong><?=$nombre[0].' '.$apellido[0]?></strong></p>
               </div>
               <?php } ?>
           </div>
        </div>
        <div class="form-group list">

        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-primary" onclick="guardar_oportunidad(<?=$usuario->id?>)">Guardar</button>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Lista de oportunidades del usuario seleccionado*/
            case 4:
                switch($request->tipo){
                    /*Vigente*/
                    case 0:
                        $query_condicion = ' AND CICLO > 0 AND CICLO < 90';
                        break;
                    /*Vendidas*/
                    case 1:
                        $query_condicion = ' AND CICLO = 90';
                        break;
                    /*Pedidas*/
                    case 2:
                        $query_condicion = ' AND CICLO = 0';
                        break;
                    default:
                        $query_condicion = '';
                        break;
                }
                $oportunidades = DB::select('SELECT * FROM view_oportunidades WHERE IDRESPONSABLE = '.$request->id_user.$query_condicion.' AND id_user_freelance IS NULL OR id_user_freelance = "" ORDER BY id ASC');
                ob_start(); ?>
                <div class="row">
                    <div class="col-md-1 text-center">
                        <p class="h5"><strong>Item</strong></p>
                    </div>
                    <div class="col-md-9 text-center">
                        <p class="h5"><strong>Oportunidad</strong></p>
                    </div>
                    <div class="col-md-2 text-center">
                        <p class="h5"><strong>Agregar</strong></p>
                    </div>
                    <?php $i=1; foreach($oportunidades as $oportunidad){ ?>
                    <div class="col-md-1 text-center">
                        <p class="h5"><strong><?=$i?></strong></p>
                    </div>
                    <div class="col-md-2 text-center">
                        <?php if($oportunidad->EMPRESALOGO != ""){ ?>
                           <img class="imagen-redonda" style="width: 30px;" src="<?=$oportunidad->EMPRESALOGO?>">
                           <?php }else{ ?>
                           <img class="imagen-redonda" src="http://via.placeholder.com/30x30">
                           <?php } ?>
                    </div>
                    <div class="col-md-7 text-left">
                        <p class="h6"><?=$oportunidad->titulo?></p>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="control-inline onoffswitch oportunidad-user-freelance">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch<?=$oportunidad->id?>" onclick="oportunidad_select(<?=$oportunidad->id?>)">
                            <label class="onoffswitch-label" for="myonoffswitch<?=$oportunidad->id?>">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <?php if(count($oportunidades) == 0){ ?>
                    <div class="col-md-12 text-center">
                        <p class="h5"><em>No hay oportunidades</em></p>
                    </div>
                    <?php } ?>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Guardar las oportunidades del usuario freelance*/
            case 5:
                foreach($request->oportunidades as $index => $value){
                    if($value != ""){
                        $dato = Oportunidades::find($index);
                        $dato->id_user_freelance = $request->id;
                        if($dato->save()){
                            $data['msj'] = 'Guardado satisfactoriamente!';
                        }else{
                            $data['msj'] = 'Error al guardar!';
                        }
                    }
                }
                break;
            /*Eliminar una oportunidad del listado de freelance*/
            case 6:
                $dato = Oportunidades::find($request->id);
                $dato->id_user_freelance = null;
                if($dato->save()){
                    $data['msj'] = 'Eliminado satisfactoriamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            /*Guardar comentario al perfil*/
            case 7:
                $dato = new FreelanceComentarios;
                $dato->id_user = Auth::user()->id;
                $dato->comentario = $request->comentario;
                $dato->id_freelance_user = $request->id_user_freelance;
                if($dato->save()){
                    $data['msj'] = 'Guardado satisfactoriamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Flujo de caja*/
            case 8:
                if(isset($request->fecha_inicio) && isset($request->fecha_fin) && $request->fecha_inicio != "" && $request->fecha_fin != ""){
                    $query = 'SELECT G.valor, B.fecha_inicio AS fecha FROM freelance_bitacora_gastos G INNER JOIN freelance_bitacoras B ON G.id_bitacora = B.id WHERE B.fecha_inicio >= "'.$request->fecha_inicio.'" AND B.fecha_inicio <= "'.$request->fecha_fin.'" AND B.deleted_at IS NULL AND B.id_freelance_user = '.$request->id;
                }else{
                    $query = 'SELECT G.valor, B.fecha_inicio AS fecha FROM freelance_bitacora_gastos G INNER JOIN freelance_bitacoras B ON G.id_bitacora = B.id WHERE B.deleted_at IS NULL AND B.id_freelance_user = '.$request->id;
                }
                $actividades = DB::select($query);
                ob_start(); ?>
<script>
var chart = AmCharts.makeChart("chartdiv", {
        "type": "serial",
        "theme": "light",
        "dataProvider": [
            <?php foreach($actividades as $actividad){ ?>
            {
                "country": "<?=date('d/m/Y',strtotime($actividad->fecha))?>",
                "visits": <?=$actividad->valor?>
                      },
            <?php } ?>
                 ],
        "valueAxes": [{
            "gridColor": "#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0,
            "title": "Valor"
                  }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <br><b>$ [[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "visits"
                  }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "country",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "tickPosition": "start",
            "tickLength": 20,
            "labelRotation": 90,
            "fontSize": 10
        },
        "export": {
            "enabled": false
        }

    });

    AmCharts.makeChart("chartdiv1",
        {
            "type": "serial",
            "categoryField": "category",
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 90,
                "fontSize": 10
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[title]] de <br>[[category]]:<br><b>$ [[value]]</b>",
                    "bullet": "round",
                    "id": "AmGraph-1",
                    "title": "Flujo de caja acumulado",
                    "valueField": "column-1"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "title": "Valor"
                }
            ],
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "useGraphSettings": true
            },
            "titles": [
                {
                    "id": "Title-1",
                    "size": 15,
                    "text": ""
                }
            ],
            "dataProvider": [
                <?php
                $valoracumulado=0;
                foreach($actividades as $actividad){
                       $valoracumulado+=$actividad->valor;
                    ?>
                {
                    "category": "<?=date('d/m/Y',strtotime($actividad->fecha))?>",
                    "column-1": <?=$valoracumulado?>
                },
                <?php } ?>
            ]
        }
    );
</script>
 <!--Filtro-->
<div class="col-md-12">
    <div class="row filtro mt-5 margin-filtro-l margin-filtro-r">
        <div class="col-sm-12 col-md-4 filtro-new">
            <input class="form-control fecha-filtro" type="text" id="fecha_inicio" placeholder="fecha inicio" <?php if(isset($request->fecha_inicio) && $request->fecha_inicio != "") { ?> value="<?=$request->fecha_inicio?>" <?php } ?> required>
        </div>
        <div class="col-sm-12 col-md-4 filtro-new">
            <input class="form-control fecha-filtro" type="text" id="fecha_fin" placeholder="fecha fin" <?php if(isset($request->fecha_fin) && $request->fecha_fin != "") { ?> value="<?=$request->fecha_fin?>" <?php } ?> required>
        </div>
        <div class="col-sm-12 col-md-4 btn-filtrar" onclick="filtrar_flujo()">
            <div class="row">
                <div class="col-md-12">
                    <h5>Aplicar Filtros</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 text-center">
    <h3>Flujo de caja diario</h3>
    <?php if(count($actividades)>0){ ?>
    <div id="chartdiv"></div>
    <?php }else{ ?>
    <p class="h5 mt-5"><strong><em>No hay registro</em></strong></p>
    <?php } ?>
    <p class="h4 mt-3"><strong>Valor total: </strong>$ <?=number_format($valoracumulado)?></p>
</div>
<div class="col-md-12 text-center mt-5">
    <h3>Flujo de caja acumulado</h3>
    <?php if(count($actividades)>0){ ?>
    <div id="chartdiv1"></div>
    <?php }else{ ?>
    <p class="h5 mt-5"><strong><em>No hay registro</em></strong></p>
    <?php } ?>
    <p class="h4 mt-3"><strong>Valor total: </strong>$ <?=number_format($valoracumulado)?></p>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Presupuesto*/
            case 9:
                $usuario = ViewFreelanceLista::find($request->id);
                $meses_ano = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $ano = date('Y');
                $query = 'SELECT DISTINCT(P.id_concepto) AS CONCEPTO FROM freelance_presupuestos P WHERE P.deleted_at IS NULL AND P.ano = '.$ano.' AND P.id_freelance_user = '.$request->id;
                $conceptos = DB::SELECT($query);
                ob_start(); ?>
<div class="col-md-12">
    <img class="imagen-redonda" src="<?=$usuario->FOTOPERFIL?>">
    <p class="h3">Presupuesto año <?=date('Y')?></p>
</div>
<div class="col-md-12 text-center">
    <input type="hidden" name="id_user" value="<?=$request->id?>">
    <p><a class="text-success" style="font-size: 3rem;cursor:pointer;" data-toggle="tooltip" data-placement="right" title="Agregar concepto" onclick="presupuesto('crear')"><i class="fa fa-plus"></i></a></p>
</div>
<div class="col-md-6 text-right bg-secondary text-white">
    <p class="h4"><strong>Total</strong></p>
</div>
<div class="col-md-6 text-left bg-secondary text-white">
    <p class="h4"><strong id="total_presupuesto">$ 0</strong></p>
</div>
<!--<div class="col-md-6 text-right bg-dark text-white">
    <p class="h4"><strong>Mes</strong></p>
</div>
<div class="col-md-6 text-left bg-dark text-white">
    <p class="h4"><strong>Valor</strong></p>
</div>
<form method="POST" action="" id="formulario_presupuesto" enctype="multipart/form-data">
    <input type="hidden" name="action" value="10">
    <input type="hidden" name="id_user" value="<?=$request->id?>">
    <input type="hidden" name="_token" value="<?=$request->_token?>">
<?php
    $meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $data['valor_total'] = 0;
    for($i=1;$i<=12;$i++){
    $presupuesto = FreelancePresupuestos::where('id_freelance_user',$request->id)->where('mes',$i)->where('ano',date('Y'))->get();
    $valor = isset($presupuesto[0]->valor)?$presupuesto[0]->valor:0;
    $data['valor_total']+=$valor;
    ?>
<div class="col-md-6 text-right">
    <p class="h4"><strong><?=$meses[$i]?></strong></p>
</div>
<div class="col-md-6 text-left">
    <p class="h4"><input type="text" id="presupuesto_<?=$i?>_valor" class="form-control border-0 presupuesto-valor" name="presupuesto[<?=$i?>][valor]" style="font-size: 2.5rem !important;" placeholder="$ 0" value="<?='$ '.number_format($valor)?>" onkeyup="formato_numero(this.value,this.id)"></p>
</div>
<?php }
    $data['valor_total'] = '$ '.number_format($data['valor_total']);
    ?>
</form>-->
<div class="col-md-12 text-left bg-secondary text-white" id="encabezado_presupuesto">
    <div class="row">
        <div class="col-md-2">
            <p class="h4"><strong>Concepto</strong></p>
        </div>
        <div class="col-md-10">
            <div class="row">
               <?php for($i=0;$i<12;$i++){ ?>
                <div class="col-md-1"><p class="h6"><strong><?=$meses_ano[$i]?></strong></p></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php foreach($conceptos as $concepto){
$query = 'SELECT C.concepto, P.mes, P.valor FROM freelance_presupuestos P INNER JOIN freelance_presupuestos_concepto C ON P.id_concepto = C.id WHERE P.deleted_at IS NULL AND P.ano = '.$ano.' AND P.id_concepto = '.$concepto->CONCEPTO.' AND P.id_freelance_user = '.$request->id.' ORDER BY P.mes ASC';
$valores = DB::SELECT($query);
?>
<div class="col-md-12 text-left">
    <div class="row">
        <div class="col-md-2">
            <div class="row">
                <div class="col-md-3 text-center">
                    <a class="text-warning mt-4" style="cursor:pointer;" onclick="editar_presupuesto(<?=$concepto->CONCEPTO?>)"><i class="fa fa-pencil mt-4"></i></a>
                    <a class="text-danger" style="cursor:pointer;" onclick="eliminar_presupuesto(<?=$concepto->CONCEPTO?>)"><i class="fa fa-trash"></i></a>
                </div>
                <div class="col-md-9">
                    <p class="h4"><strong data-concepto="<?=$valores[0]->concepto?>"><?=$valores[0]->concepto?></strong></p>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
<?php foreach($valores as $valor){ ?>
                <div class="col-md-1"><p class="h6"><strong class="valor_presupuesto" data-valormes<?=$valor->mes?>>$ <?=number_format($valor->valor, 0, '.', ',')?></strong></p></div>
<?php } ?>
            </div>
        </div>
    </div>
</div>
<template id="formulario_editar_presupuesto<?=$concepto->CONCEPTO?>">
    <div class="row">
        <div class="col-md-12">
            <label>Concepto</label>
            <input type="text" class="form-control" id="concepto<?=$concepto->CONCEPTO?>" list="lista_conceptos" data-nombre="Campo: Concepto" value="<?=$valores[0]->concepto?>">
        </div>
    </div>
    <div class="row">
       <?php
        $meses_ano = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        for($i=0;$i<12;$i++){ ?>
        <div class="col-md-6">
            <label><?=$meses_ano[$i]?></label>
            <input type="text" class="form-control" id="mes_<?=$i+1?>_<?=$concepto->CONCEPTO?>" onkeyup="formato_numero(this.value,this.id)" data-nombre="Campo: <?=$meses_ano[$i]?>" value="$ <?=number_format($valores[$i]->valor, 0, '.', ',')?>">
        </div>
        <?php } ?>
    </div>
    <div class="row mt-5">
        <div class="col-md-12">
            <p class="bg-success" style="height:24px;cursor:pointer;" onclick="editar_presupuesto2(<?=$concepto->CONCEPTO?>)"><a class="text-white"><i class="fa fa-save"></i> Guardar</a></p>
        </div>
    </div>
</template>
<?php } ?>
<div class="col-md-12 bg-success text-center" style="height: 39px; cursor:pointer;">

</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 10:
                $ano = date('Y');

                $conceptos = FreelancePresupuestosConcepto::where('concepto',$request->concepto)->get();
                if(isset($conceptos[0]->concepto) && !empty($conceptos[0]->concepto)){
                    $id_concepto = $conceptos[0]->id;
                }else{
                    $dato = new FreelancePresupuestosConcepto;
                    $dato->concepto = $request->concepto;
                    $dato->save();
                    $id_concepto = $dato->id;
                }
                $presupuestos = FreelancePresupuestos::where('id_freelance_user',$request->id_user)->where('ano',$ano)->where('id_concepto',$id_concepto)->get();
                if(count($presupuestos)>0){
                    $data['msj'] = 'Ya existe registros para este concepto!';
                }else{
                    foreach($request->valor_mes as $index=>$value){
                        $dato1 = new FreelancePresupuestos;
                        $dato1->mes = $index+1;
                        $dato1->ano = $ano;
                        $dato1->valor = $value;
                        $dato1->id_concepto = $id_concepto;
                        $dato1->id_freelance_user = $request->id_user;
                        $dato1->save();
                    }
                    $data['msj'] = 'Guardado correctamente!';
                }
                /*
                $presupuestos = FreelancePresupuestos::where('id_freelance_user',$request->id_user)->where('ano',$ano)->get();
                if(count($presupuestos)>0){
                    foreach($presupuestos as $presupuesto){
                        $presupuesto->delete();
                    }
                }
                foreach($request->presupuesto as $index => $value){
                    $dato = new FreelancePresupuestos;
                    $dato->mes = $index;
                    $dato->ano = date('Y');
                    $dato->valor = intval(str_replace("$ ", "", str_replace(",", "", $value['valor'])));
                    $dato->id_freelance_user = $request->id_user;
                    if($dato->save()){
                        $data['msj'] = 'Guardado correctamente!';
                    }else{
                        $data['msj'] = 'Ocurrio un error no se guardo!';
                    }
                }*/
                break;
            /*Armar el datalist con los conceptos*/
            case 11:
                ob_start();
                $conceptos = FreelancePresupuestosConcepto::all();
                foreach($conceptos as $concepto){
                ?>
                <option value="<?=$concepto->concepto?>">
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Editar el presupuesto*/
            case 12:
                $ano = date('Y');
                $conceptos = FreelancePresupuestosConcepto::where('concepto',$request->concepto)->get();
                if(isset($conceptos[0]->concepto) && !empty($conceptos[0]->concepto)){
                    $id_concepto = $conceptos[0]->id;
                }else{
                    $dato = new FreelancePresupuestosConcepto;
                    $dato->concepto = $request->concepto;
                    $dato->save();
                    $id_concepto = $dato->id;
                }
                $presupuestos = FreelancePresupuestos::where('id_freelance_user',$request->id_user)->where('ano',$ano)->where('id_concepto',$id_concepto)->get();
                if(count($presupuestos)>0){
                    foreach($presupuestos as $presupuesto){
                        $presupuesto->delete();
                    }
                }
                foreach($request->valor_mes as $index=>$value){
                    $dato1 = new FreelancePresupuestos;
                    $dato1->mes = $index+1;
                    $dato1->ano = $ano;
                    $dato1->valor = $value;
                    $dato1->id_concepto = $id_concepto;
                    $dato1->id_freelance_user = $request->id_user;
                    $dato1->save();
                }
                $data['msj'] = 'Guardado correctamente!';

                break;
            /*Eliminar Presupuesto*/
            case 13:
                $ano = date('Y');
                $presupuestos = FreelancePresupuestos::where('id_freelance_user',$request->id_user)->where('ano',$ano)->where('id_concepto',$request->id_concepto)->get();
                if(count($presupuestos)>0){
                    foreach($presupuestos as $presupuesto){
                        $presupuesto->delete();
                    }
                    $data['msj'] = 'Eliminado satisfactoriamente!';
                }else{
                    $data['msj'] = 'No se elimino!';
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function oportunidades(){
        $data['paises'] = Paises::all();
        $query = 'SELECT DISTINCT(O.empresa) FROM view_oportunidades O WHERE O.id_user_freelance IS NOT NULL';
        $data['empresas'] = DB::select($query);
        $data['productos'] = Producto::all();
        $query = 'SELECT DISTINCT(O.NOMBRECORTOFREELANCE) FROM view_oportunidades O WHERE O.id_user_freelance IS NOT NULL';
        $data['freelance'] = DB::select($query);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 56 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('freelance.oportunidades',['data' => $data]);
    }

    public function oportunidad_accion(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*Listado de oportunidades*/
            case 0:
                $data['oportunidades'] = ViewOportunidades::where('id_user_freelance','!=','')->orderby('CICLO','DESC')->get();
                break;
        }
        return \Response::json(['data' => $data]);
    }

    function fecha($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $fecha_hora = explode(' ',$fecha);
            $f=explode('-',$fecha_hora[0]);
            $h=explode(':',$fecha_hora[1]);
            if($h[0]>12){
                $h[0]= intval($h[0]) - 12;
                $jornada = 'P.M';
            }else{
                $jornada = 'A.M';
            }
            $fecha_formato = $f[2].'/'.$meses[intval($f[1])].'/'.$f[0].' '.$h[0].':'.$h[1].':'.$h[2].' '.$jornada;
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }
    }
}
