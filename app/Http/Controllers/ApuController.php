<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;

use App\Apu;
use App\Apus_item;
use App\Apus_historico;
use Auth;
class ApuController extends Controller
{
    //
    public function save(Request $request){

    	$contador1=count($request->maquina);
    	$contador3=count($request->otros);
    	$contador2=count($request->gastos);
    	//unset($request->maquina[$contador1-1]);
    	//unset($request->otros[$contador2-1]);
    	$total_maquina=0;
    	$total=0;
    	foreach ($request->maquina as $key) {
    		$total_maquina=$total_maquina+$key["valor_total"];
    	}

    	foreach ($request->otros as $key) {
    		$total_maquina=$total_maquina+$key["valor_total"];
    	}

    	foreach ($request->gastos as $key) {
    		$total_maquina=$total_maquina+$key["valor_total"];
    	}
    	$datos=new Apu();
    	$datos->venta=$request->id;
    	$datos->version=$request->version;
    	$datos->costo_fabricacion=$total_maquina;
		$datos->user_id=Auth::user()->id;
		$datos->save();

		$version=new Apus_historico();
		$version->ventas=$request->id;
    	$version->version=$request->version;
    	$version->total_maquina=$request->total_maquina;
    	$version->total_equipos=$request->total_equipos;
    	$version->total_gastos=$request->total_gastos;
    	$version->costo_fabricacion=str_replace("$", "",$request->costo_fabricacion);
    	$version->valor_venta=$request->valor_venta;
    	$version->moneda=$request->moneda;
    	$version->negociacion_sugerida=$request->negociacion_sugerida;
    	$version->negociacion_real=$request->negociacion_real;
    	$version->precio_venta=$request->precio_venta;
    	$version->utilidad=str_replace("$", "",$request->utilidad);
    	$version->margen_bruto_venta=str_replace("%", "",$request->margen_bruto_venta);
    	$version->porcentaje_financiacion=$request->porcentaje_financiacion;
    	$version->meses_financiacion=$request->meses_financiacion;
    	$version->valor_financiacion=str_replace("$", "",$request->valor_financiacion);
    	$version->utilidad_final=str_replace("$", "",$request->utilidad_final);
    	$version->margen_bruto_final=str_replace("%", "",$request->margen_bruto_final);
    	$version->user_id=Auth::user()->id;
		$version->save();
		$id=$version->id;

		foreach ($request->maquina as $key) {
    		$item=new Apus_item();
			$item->ventas=$request->id;
	    	$item->version=$id;
	    	$item->concepto=$key["concepto"];
	    	$item->tipo=$key["tipo"];
	    	$item->cantidad=$key["cantidad"];
	    	$item->concepto=$key["concepto"];
	    	$item->valor_unitario=$key["valor_unitario"];
	    	$item->valor_total=$key["valor_total"];
	    	$item->user_id=Auth::user()->id;
			$item->save();
    	}

    	foreach ($request->otros as $key) {
    		$item=new Apus_item();
			$item->ventas=$request->id;
	    	$item->version=$id;
	    	$item->concepto=$key["concepto"];
	    	$item->tipo=$key["tipo"];
	    	$item->cantidad=$key["cantidad"];
	    	$item->concepto=$key["concepto"];
	    	$item->valor_unitario=$key["valor_unitario"];
	    	$item->valor_total=$key["valor_total"];
	    	$item->user_id=Auth::user()->id;
			$item->save();
    	}

    	foreach ($request->gastos as $key) {
    		$item=new Apus_item();
			$item->ventas=$request->id;
	    	$item->version=$id;
	    	$item->concepto=$key["concepto"];
	    	$item->tipo=$key["tipo"];
	    	$item->cantidad=$key["cantidad"];
	    	$item->concepto=$key["concepto"];
	    	$item->valor_unitario=$key["valor_unitario"];
	    	$item->valor_total=$key["valor_total"];
	    	$item->user_id=Auth::user()->id;
			$item->save();
    	}
    }
}
