<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Feria;
use App\Ferias_multimedias;
use App\FeriasLeccionesArchivo;
use App\FeriasCompetenciasArchivos;
use Auth;

use App\Http\Requests;
use Illuminate\Http\Request;

date_default_timezone_set("America/Bogota");
class ArchivoController extends Controller
{

    /**
     * Generate Image upload View
     *
     * @return void
     */
    public function dropzone()
    {
        return view('dropzone-view');
    }

    /**
     * Image Upload Code
     *
     * @return void
     */
    public function cargarmultimedia(Request $request)
    {
        $image = $request->file('file');
        $imageName = uniqid();
        $imegeExtension = $image->getClientOriginalExtension();
        $imageName = $imageName.'.'.$imegeExtension;
        $NameOriginal = $image->getClientOriginalName();
        $image->move(public_path('storage/ferias/multimedia'),$imageName);

        $dato = new Ferias_multimedias;
        $dato->tipo = "Archivo";
        $dato->contenido = $imageName;
        $dato->name = $NameOriginal;
        $dato->id_feria = $request->id;
        $dato->save();

        return response()->json(['success'=>$imageName]);
    }

    public function cargarmultimediacompetenciaferia(Request $request)
    {
        $image = $request->file('file');
        $imageName = uniqid();
        $imegeExtension = $image->getClientOriginalExtension();
        $imageName = $imageName.'.'.$imegeExtension;
        $NameOriginal = $image->getClientOriginalName();
        $image->move(public_path('storage/ferias/competencia'),$imageName);

        $dato = new FeriasCompetenciasArchivos;
        $dato->archivo = $imageName;
        $dato->nombre_archivo = $NameOriginal;
        $dato->id_feria_competencia = $request->id;
        $dato->save();

        return response()->json(['success'=>$imageName]);
    }

        public function cargarubicacion(Request $request)
    {
        $image = $request->file('file');
        $imageName = time().'__'.$image->getClientOriginalName();
        $image->move(public_path('storage/ferias'),$imageName);

        $dato = Feria::findOrFail($request->id);
        if($dato->ubicacion_foto!=''){
            Storage::delete('ferias/'.$dato->ubicacion_foto);
        }
        $dato->ubicacion_foto = $imageName;
        $dato->save();

        return response()->json(['success'=>$imageName]);
    }

    public function cargarlecciones(Request $request)
    {
        $image = $request->file('file');
        $imageName = uniqid();
        $imegeExtension = $image->getClientOriginalExtension();
        $imageName = $imageName.'.'.$imegeExtension;
        $NameOriginal = $image->getClientOriginalName();
        $image->move(public_path('storage/ferias/lecciones'),$imageName);

        $dato = new FeriasLeccionesArchivo;
        $dato->archivo = $imageName;
        $dato->nombre = $NameOriginal;
        $dato->id_feria = $request->id;
        $dato->id_user = Auth::user()->id;
        $dato->save();

        return response()->json(['success'=>$imageName]);
    }

}
