<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Principal;
use App\Menuses;
use App\Users_megaarchivo;
use App\Oportunidades;
use App\Oportunidad;
use App\Tiempomegaarchivos;
use App\Rutasusadas;
use App\Archivousados;
use Auth;

class MegaarchivoController extends Controller
{
    //
    public function megaarchivo(Request $request){
    	$us=$request->usuario;
        $pw=$request->contrasena;
        $usuario=Users_megaarchivo::where('name',$us)->get();
        $datos='';
        if(count($usuario)===0){
            $msj='No existe usuario';
            $ruta='megaarchivo.login';
        }else{
            foreach($usuario as $usua){
                $cont=$usua->contrasena;
            }
            if($cont === $pw){
                foreach($usuario as $usua){
                    $datos=$usua->id;
                }
                $ruta='megaarchivo.tipo';
                $msj='';
            }else{
                $msj='La contraseña es incorrecta';
                $ruta='megaarchivo.login';
            }
        }
        $datos2=Principal::all();
        $datos1=Menuses::all();
        $datos3=Oportunidades::all();
    	return view($ruta,['msj' => $msj, 'usuario' => $datos, 'datos' => $datos2, 'menu' => $datos1, 'oportunidades' => $datos3]);
    }

    public function tipoingreso(Request $request){
        $datos2=Principal::all();
        $datos1=Menuses::all();
        

        if(!isset($request->tiempoid)){
        $tiempo = new Tiempomegaarchivos;
        $tiempo->id_user = $request->usuario;
        $tiempo->tipo_ingreso = $request->tipo;
        $tiempo->oportunidad = $request->oportunidad_s2;
        $tiempo->fecha_ingreso = date("Y-m-d h:i:s");
        $tiempo->fecha_retiro = date("Y-m-d h:i:s");
        $tiempo->save();
        $tiempoid = $tiempo->id;
        }else{
            $tiempoid=$request->tiempoid;
        }

        $tiempo_megarchivo = Tiempomegaarchivos::findOrFail($tiempoid);
        $usuario_megarchivo=Users_megaarchivo::findOrFail($tiempo_megarchivo->id_user);
        $fun = Users_megaarchivo::findorfail($tiempo_megarchivo->id_user);

        return view('megaarchivo.menu',['func' => $fun,'datos' => $datos2, 'menu' => $datos1, 'tiempo' => $tiempoid, 'usuario_megarchivo' => $usuario_megarchivo]);
    }

    public function menu(){
    	$datos=Principal::all();
        $datos1=Menuses::all();
    	return view('megaarchivo.menu',['datos' => $datos, 'menu' => $datos1]);
    }

    public function menuguardar(Request $request){
        
        $dato = Menuses::findorfail(1);
        $dato->menu = $request->html;
        $dato->save();
        return \Response::json(['msg' => 'Guardado Satisfactoriamente']);
    }

    public function tiemponuevo(Request $request){
        $tiempo = Tiempomegaarchivos::findOrFail($request->id);
        $tiempo->fecha_retiro = date('Y-m-d h:i:s');
        $tiempo->save();
        return \Response::json(['msg' => 'Guardado Satisfactoriamente']);
    }

    public function documentos(Request $request){
        $tiempo_megarchivo = Tiempomegaarchivos::findOrFail($request->tiempo);
        $usuario_megarchivo=Users_megaarchivo::findOrFail($tiempo_megarchivo->id_user);

        return view('megaarchivo.contenido',['msg'=>'entro','carpeta' => $request->carpeta, 'tiempo' => $request->tiempo, 'usuario_megarchivo' => $usuario_megarchivo]);
        
    }

    public function rutausada(Request $request){
        $tiempo = new Rutasusadas;
        $tiempo->idtiempomegaarchivo = $request->id;
        $tiempo->archivo = $request->carpeta;
        $tiempo->fecha_ingreso = date('Y-m-d H:i:s');
        $tiempo->fecha_retiro = date('Y-m-d H:i:s');
        $tiempo->save();
        $tiempoid = $tiempo->id;
        return \Response::json(['msg' => 'Guardado Ruta Satisfactoriamente','tiemporuta' => $tiempoid]);
    }

    public function rutausadaeditar(Request $request){
        $tiempo = Rutasusadas::findOrFail($request->id);
        $tiempo->fecha_retiro = date('Y-m-d h:i:s');
        $tiempo->save();
        return \Response::json(['msg' => 'Guardado Ruta Satisfactoriamente']);
    }

    public function archivousado(Request $request){
        $tiempo = new Archivousados;
        $tiempo->idtiempomegaarchivo = $request->id;
        $tiempo->archivo = $request->nombre;
        $tiempo->ruta = $request->url;
        $tiempo->fecha_ingreso = date('Y-m-d H:i:s');
        $tiempo->fecha_retiro = date('Y-m-d H:i:s');
        $tiempo->save();
        return \Response::json(['msg' => 'Guardado apertura de archivo Satisfactoriamente']);
    }

    public function archivos_das_menu(Request $request)
    {          
        if($request->ajax()) {
            $public_path = public_path();      
            $ruta= $public_path.'/storage/'.$request->url;
            $mostrar = array();
            $archivos = array();
            $tamano=0;
            if (is_dir($ruta)) { 
                if ($dh = opendir($ruta)) { 
                    while (($file = readdir($dh)) !== false) { 
                    //esta línea la utilizaríamos si queremos listar todo lo que hay en el directorio 
                    //mostraría tanto archivos como directorios 
                    //echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file); 
                        if (is_dir($ruta . $file) && $file!="." && $file!=".."){ 
                            //solo si el archivo es un directorio, distinto que "." y ".
                            $mostrar = array_prepend($mostrar, $file);
                            //listar_directorios_ruta($ruta . $file . "/"); 
                        }elseif ($file!="." && $file!="..") {
                            $archivos = array_prepend($archivos, $file);
                            $tamano+=filesize($ruta.$file);
                            
                        }
                    } 
                    closedir($dh); 
                } 
            }
            

           return Response()->json(['status' => 'success','folder' => $mostrar,'archivos' => $archivos ,'titulo' => $request->input('url'), 'tamano' => $tamano]);
        }
        return Response::json(['status' => '402']);
    }

    public function consultar_funcionario(Request $request){
        function dateDiff($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            $diff = $end_ts - $start_ts;

            return round($diff / 3600);

        }

        function dateDiff2($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            $diff = $end_ts - $start_ts;

            return ($diff / 3600);

        }

        $f_inicio=($request->f_inicio).' 00:00:00';
        $f_fin=($request->f_fin).' 23:59:59';
        $tiempo = Tiempomegaarchivos::where('id_user',$request->id)->where('fecha_ingreso', '>=', $f_inicio)->where('fecha_retiro', '<=', $f_fin)->orderBy('created_at', 'desc')->get();
        $funcionario_consultado = Users_megaarchivo::findOrFail($request->id);

        $i=0;
        $html='<table class="table-bordered" style="width:100%"><thead><tr><td style="width:10%">#</td><td style="width:50%">Tipo Ingreso</td><td style="width:20%">Fecha</td><td style="width:20%">Duración</td></tr></thead></table>';
        if(count($tiempo)==0){
            $html.='No hay ingresos!';
        }else{
            foreach($tiempo as $t){
                    if(($t->tipo_ingreso)=='oportunidad'){

                        $oportunidad = Oportunidades::findOrFail($t->oportunidad);
                        $tiempo_duracion=number_format((dateDiff2($t->created_at, $t->updated_at)), 2, '.', '');
                        $tipo_ingreso='Ingreso por la '.($t->tipo_ingreso).' "'.$oportunidad->titulo.'" el día '.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).' tiempo de duración '.$tiempo_duracion.' Hora(s)';
                        $cabecero='<table style="width:100%; cursor:pointer;"><thead><tr><td style="width:10%">'.($i+1).'</td><td style="width:50%">'.$t->tipo_ingreso.' ('.$oportunidad->titulo.' )'.'</td><td style="width:20%">'.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).'</td><td style="width:20%">'.$tiempo_duracion.' Hora(s)'.'<i class="fa fa-angle-down rotate-icon"></i></td></tr></thead></table>';
                    }else{
                        $tiempo_duracion=number_format((dateDiff2($t->created_at, $t->updated_at)), 2, '.', '');
                        $tipo_ingreso='Ingreso por el '.($t->tipo_ingreso).' el día '.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).' tiempo de duración '.$tiempo_duracion.' Hora(s)';
                        $cabecero='<table style="width:100%; cursor:pointer;"><thead><tr><td style="width:10%">'.($i+1).'</td><td style="width:50%">'.$t->tipo_ingreso.'</td><td style="width:20%">'.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).'</td><td style="width:20%">'.$tiempo_duracion.' Hora(s)'.'<i class="fa fa-angle-down rotate-icon"></i></td></tr></thead></table>';
                    }            
                    $html=$html.'<div class="card" onclick="consultar_funcionario1('.$t->id.','.$i.')">
                            <div class="card-header" onclick="abrir_col(`collapse'.$i.'`)" role="tab" id="heading'.$i.'">
                                '.$cabecero.'
                            </div>
                            <div id="collapse'.$i.'" class="collapse" role="tabpanel" aria-labelledby="heading'.$i.'">
                                
                            </div>
                        </div>';
                    $i++;
            }
        }
        return (['html' => $html, 'funcionario_consultado' => $funcionario_consultado->nombre, 'foto' => $funcionario_consultado->foto]);
    }

    public function consultar_funcionario1(Request $request){
        $contenido='';
        function dateDiff($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            $diff = $end_ts - $start_ts;

            return round($diff / 3600);

        }

        function dateDiff2($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            $diff = $end_ts - $start_ts;

            return ($diff / 3600);

        }

        function tipoarchivo($archivo){
            $html='';
            $partes = explode('.', $archivo);
            $extension = end($partes); 
            if($extension == 'pdf' || $extension == 'PDF'){
            $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/pdf.png">';
            }else if($extension == 'psd' || $extension == 'PSD'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/photoshop.png">';
            }else if($extension == 'ai' || $extension == 'AI'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/illustrator.png">';
            }else if($extension == 'doc' || $extension == 'docx' || $extension == 'docm' || $extension == 'dotx' || $extension == 'dotm' || $extension == 'DOC' || $extension == 'DOCX' || $extension == 'DOCM' || $extension == 'DOTX' || $extension == 'DOTM'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/doc.png">';
            }else if($extension == 'html' || $extension == 'HTML'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/html.png">';
            }else if($extension == 'xlt' || $extension == 'xls' || $extension == 'xml' || $extension == 'xlsx' || $extension == 'xlsm' || $extension == 'xlsb' || $extension == 'xltx' || $extension == 'xltm' || $extension == 'xlam' || $extension == 'xla' || $extension == 'xlw' || $extension == 'xlr' || $extension == 'XLT' || $extension == 'XLS' || $extension == 'XML' || $extension == 'XLSX' || $extension == 'XLSM' || $extension == 'XLSB' || $extension == 'XLTX' || $extension == 'XLTM' || $extension == 'XLAM' || $extension == 'XLA' || $extension == 'XLW' || $extension == 'XLR'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/xls.png">';
              
            }else if($extension == 'pptx' || $extension == 'pptm' || $extension == 'ppt' || $extension == 'xps' || $extension == 'potx' || $extension == 'potm' || $extension == 'pot' || $extension == 'thmx' || $extension == 'ppsm' || $extension == 'pps' || $extension == 'ppam' || $extension == 'ppa' || $extension == 'PPTX' || $extension == 'PPTM' || $extension == 'PPT' || $extension == 'XPS' || $extension == 'POTX' || $extension == 'POTM' || $extension == 'POT' || $extension == 'THMX' || $extension == 'PPSM' || $extension == 'PPS' || $extension == 'PPAM' || $extension == 'PPA'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/ppt.png">';
            }else if($extension == 'js' || $extension == 'JS'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/javascript.png">';
            }else if($extension == 'jpg' || $extension == 'JPG'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/jpg.png">';
            }else if($extension == 'png' || $extension == 'PNG'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/png.png">';
            }else if($extension == 'mp3' || $extension == 'MP3'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/mp3.png">';
            }else if($extension == 'mp4' || $extension == 'MP4'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/mp4.png">'; 
            }else if($extension == 'mkv' || $extension == 'MKV'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/mkv.png">';
            }else if($extension == 'wmv' || $extension == 'WMV'){                 
              $html.='<im style="width: 7%;"g src='.url('/').'/images/megaarchivo/wmv.png">'; 
            }else if($extension == 'avi' || $extension == 'AVI'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/avi.png">';
            }else if($extension == 'txt' || $extension == 'TXT'){
              $html='<img style="width: 7%;" src="'.url('/').'/storage/archivos/txt.png">';
            }
            return $html;
        }
        $t = Tiempomegaarchivos::findorfail($request->id);

        $r_usadas = Rutasusadas::where('idtiempomegaarchivo',$t->id)->get();

        if(count($r_usadas)==0){
            $rutas='<storage>No se encontro aperturas en la navegación</storage>';
            $contenido='
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">'.$rutas.'</div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>';
        }else{
            $rutas='<table class="table">
                  <thead class="thead-default">
                    <tr>
                      <th style="background-color: #a0a0a0">#</th>
                      <th style="background-color: #a0a0a0">Carpeta</th>
                      <th style="background-color: #a0a0a0">Fecha y Hora</th>
                      <th style="background-color: #a0a0a0">Duración (Horas)</th>
                    </tr>
                  </thead>
                  <tbody>';
            $rutasusadas = Rutasusadas::where('idtiempomegaarchivo',$t->id)->get();
            $j=1;
            foreach($rutasusadas as $r){
               
                $rutas.=' <tr>
                          <th scope="row">'.$j.'</th>
                          <td>'.($r->archivo).'</td>
                          <td>'.date("d/m/Y (h:i:s A)",strtotime($r->fecha_ingreso)).'</td>
                          <td>'.number_format((dateDiff2(date("Y-m-d H:i:s",strtotime($r->fecha_ingreso)), date("Y-m-d H:i:s",strtotime($r->fecha_retiro)))), 2, '.', '').'</td>
                        </tr>';
                $a_usados = Archivousados::where('idtiempomegaarchivo',$t->id)->where('ruta',$r->archivo)->get();  
                if(count($a_usados)==0){

                }else{
                    $rutas.='<tr><td style="text-aling: center;"><i class="fa fa-arrow-right" aria-hidden="true"></i></td><td colspan="3"><table class="table">
                      <thead class="thead-default">
                        <tr>
                          <th>#</th>
                          <th>Archivo</th>
                          <th>Fecha y Hora</th>
                        </tr>
                      </thead>
                      <tbody>';
                    $archivousados = Archivousados::where('idtiempomegaarchivo',$t->id)->where('ruta',$r->archivo)->get();
                    $x=1;
                    foreach($archivousados as $a){
                        $rutas.='<tr>
                                  <th scope="row">'.$x.'</th>
                                  <td>'.tipoarchivo($a->archivo).($a->archivo).'</td>
                                  <td>'.date("d/m/Y (h:i:s A)",strtotime($a->fecha_ingreso)).'</td>
                                </tr>';
                        $x++;
                    }
                    $rutas.='</tbody>
                        </table><td></tr>';
                }
                $j++;
            }
            $rutas.='</tbody>
                </table>';
            
            $contenido='
                        <div class="card-block">
                            <center>
                                <h3>
                                    <strong>Carpetas</strong>
                                </h3>
                            </center>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">'.$rutas.'</div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>';
            
        }
        return (['html' => $contenido]);
    }

    public function consultar_funcionario_antigua(Request $request){
        function dateDiff($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            $diff = $end_ts - $start_ts;

            return round($diff / 3600);

        }
        function dateDiff2($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            $diff = $end_ts - $start_ts;

            return ($diff / 3600);

        }

        function tipoarchivo($archivo){
            $html='';
            $partes = explode('.', $archivo);
            $extension = end($partes); 
            if($extension == 'pdf' || $extension == 'PDF'){
            $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/pdf.png">';
            }else if($extension == 'psd' || $extension == 'PSD'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/photoshop.png">';
            }else if($extension == 'ai' || $extension == 'AI'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/illustrator.png">';
            }else if($extension == 'doc' || $extension == 'docx' || $extension == 'docm' || $extension == 'dotx' || $extension == 'dotm' || $extension == 'DOC' || $extension == 'DOCX' || $extension == 'DOCM' || $extension == 'DOTX' || $extension == 'DOTM'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/doc.png">';
            }else if($extension == 'html' || $extension == 'HTML'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/html.png">';
            }else if($extension == 'xlt' || $extension == 'xls' || $extension == 'xml' || $extension == 'xlsx' || $extension == 'xlsm' || $extension == 'xlsb' || $extension == 'xltx' || $extension == 'xltm' || $extension == 'xlam' || $extension == 'xla' || $extension == 'xlw' || $extension == 'xlr' || $extension == 'XLT' || $extension == 'XLS' || $extension == 'XML' || $extension == 'XLSX' || $extension == 'XLSM' || $extension == 'XLSB' || $extension == 'XLTX' || $extension == 'XLTM' || $extension == 'XLAM' || $extension == 'XLA' || $extension == 'XLW' || $extension == 'XLR'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/xls.png">';
              
            }else if($extension == 'pptx' || $extension == 'pptm' || $extension == 'ppt' || $extension == 'xps' || $extension == 'potx' || $extension == 'potm' || $extension == 'pot' || $extension == 'thmx' || $extension == 'ppsm' || $extension == 'pps' || $extension == 'ppam' || $extension == 'ppa' || $extension == 'PPTX' || $extension == 'PPTM' || $extension == 'PPT' || $extension == 'XPS' || $extension == 'POTX' || $extension == 'POTM' || $extension == 'POT' || $extension == 'THMX' || $extension == 'PPSM' || $extension == 'PPS' || $extension == 'PPAM' || $extension == 'PPA'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/ppt.png">';
            }else if($extension == 'js' || $extension == 'JS'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/javascript.png">';
            }else if($extension == 'jpg' || $extension == 'JPG'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/jpg.png">';
            }else if($extension == 'png' || $extension == 'PNG'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/png.png">';
            }else if($extension == 'mp3' || $extension == 'MP3'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/mp3.png">';
            }else if($extension == 'mp4' || $extension == 'MP4'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/mp4.png">'; 
            }else if($extension == 'mkv' || $extension == 'MKV'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/mkv.png">';
            }else if($extension == 'wmv' || $extension == 'WMV'){                 
              $html.='<im style="width: 7%;"g src='.url('/').'/images/megaarchivo/wmv.png">'; 
            }else if($extension == 'avi' || $extension == 'AVI'){
              $html='<img style="width: 7%;" src="'.url('/').'/images/megaarchivo/avi.png">';
            }else if($extension == 'txt' || $extension == 'TXT'){
              $html='<img style="width: 7%;" src="'.url('/').'/storage/archivos/txt.png">';
            }
            return $html;
        }

        $f_inicio=($request->f_inicio).' 00:00:00';
        $f_fin=($request->f_fin).' 23:59:59';
        $tiempo = Tiempomegaarchivos::where('id_user',$request->id)->where('fecha_ingreso', '>=', $f_inicio)->where('fecha_retiro', '<=', $f_fin)->orderBy('fecha_ingreso', 'desc')->get();
        $funcionario_consultado = Users_megaarchivo::findOrFail($request->id);

        $i=0;
        $html='<table class="table-bordered" style="width:100%"><thead><tr><td style="width:10%">#</td><td style="width:50%">Tipo Ingreso</td><td style="width:20%">Fecha</td><td style="width:20%">Duración</td></tr></thead></table>';

        foreach($tiempo as $t){
            $r_usadas = Rutasusadas::where('idtiempomegaarchivo',$t->id)->get();
            if(count($r_usadas)==0){
                $rutas=`<h1>No se encontro aperturas de </h1>`;
            }else{
                $rutas='<table class="table">
                      <thead class="thead-default">
                        <tr>
                          <th style="background-color: #a0a0a0">#</th>
                          <th style="background-color: #a0a0a0">Carpeta</th>
                          <th style="background-color: #a0a0a0">Fecha y Hora</th>
                          <th style="background-color: #a0a0a0">Duración (Horas)</th>
                        </tr>
                      </thead>
                      <tbody>';
                $rutasusadas = Rutasusadas::where('idtiempomegaarchivo',$t->id)->get();
                $j=1;
                foreach($rutasusadas as $r){
                    $rutas.=' <tr>
                              <th scope="row">'.$j.'</th>
                              <td>'.($r->archivo).'</td>
                              <td>'.date("d/m/Y (h:i:s A)",strtotime($r->fecha_ingreso)).'</td>
                              <td>'.number_format((dateDiff2(date("Y-m-d H:i:s",strtotime($r->fecha_ingreso)), date("Y-m-d H:i:s",strtotime($r->fecha_retiro)))), 2, '.', '').'</td>
                            </tr>';
                    $a_usados = Archivousados::where('idtiempomegaarchivo',$t->id)->where('ruta',$r->archivo)->get();  
                    if(count($a_usados)==0){

                    }else{
                        $rutas.='<tr><td style="text-aling: center;"><i class="fa fa-arrow-right" aria-hidden="true"></i></td><td colspan="3"><table class="table">
                          <thead class="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Archivo</th>
                              <th>Fecha y Hora</th>
                            </tr>
                          </thead>
                          <tbody>';
                        $archivousados = Archivousados::where('idtiempomegaarchivo',$t->id)->where('ruta',$r->archivo)->get();
                        $x=1;
                        foreach($archivousados as $a){
                            $rutas.='<tr>
                                      <th scope="row">'.$x.'</th>
                                      <td>'.tipoarchivo($a->archivo).($a->archivo).'</td>
                                      <td>'.date("d/m/Y (h:i:s A)",strtotime($a->fecha_ingreso)).'</td>
                                    </tr>';
                            $x++;
                        }
                        $rutas.='</tbody>
                            </table><td></tr>';
                    }
                    $j++;
                }
                $rutas.='</tbody>
                    </table>';
                
                if(($t->tipo_ingreso)=='oportunidad'){
                    $oportunidad = Oportunidades::findOrFail($t->oportunidad);
                    $tiempo_duracion=number_format((dateDiff2($t->fecha_ingreso, $t->fecha_retiro)), 2, '.', '');
                    $tipo_ingreso='Ingreso por la '.($t->tipo_ingreso).' "'.$oportunidad->titulo.'" el día '.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).' tiempo de duración '.$tiempo_duracion.' Hora(s)';
                    $cabecero='<table style="width:100%; cursor:pointer;"><thead><tr><td style="width:10%">'.($i+1).'</td><td style="width:50%">'.$t->tipo_ingreso.' ('.$oportunidad->titulo.' )'.'</td><td style="width:20%">'.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).'</td><td style="width:20%">'.$tiempo_duracion.' Hora(s)'.'<i class="fa fa-angle-down rotate-icon"></i></td></tr></thead></table>';
                }else{
                    $tiempo_duracion=number_format((dateDiff2($t->fecha_ingreso, $t->fecha_retiro)), 2, '.', '');
                    $tipo_ingreso='Ingreso por el '.($t->tipo_ingreso).' el día '.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).' tiempo de duración '.$tiempo_duracion.' Hora(s)';
                    $cabecero='<table style="width:100%; cursor:pointer;"><thead><tr><td style="width:10%">'.($i+1).'</td><td style="width:50%">'.$t->tipo_ingreso.'</td><td style="width:20%">'.date("d/m/Y (h:i:s A)",strtotime($t->fecha_ingreso)).'</td><td style="width:20%">'.$tiempo_duracion.' Hora(s)'.'<i class="fa fa-angle-down rotate-icon"></i></td></tr></thead></table>';
                }            
                $html=$html.'<div class="card">
                        <div class="card-header" onclick="abrir_col(`collapse'.$i.'`)" role="tab" id="heading'.$i.'">
                            '.$cabecero.'
                        </div>
                        <div id="collapse'.$i.'" class="collapse" role="tabpanel" aria-labelledby="heading'.$i.'">
                            <div class="card-block"><center><h3><strong>Carpetas</strong></h3></center><div class="row"><div class="col-md-1"></div><div class="col-md-10">'.$rutas.'</div><div class="col-md-1"></div></div>
                        </div>
                    </div>';
                $i++;
            }
            
        }

        return (['html' => $html, 'funcionario_consultado' => $funcionario_consultado->nombre, 'foto' => $funcionario_consultado->foto]);
    }

    public function dashboardmegarchivo(Request $request){
        $datos=Users_megaarchivo::all();
        $datos1=Tiempomegaarchivos::all();
        $datos2 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `rutasusadas` GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 5');
        $datos4 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `archivousados` GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 10');
        $i=0;
        
        foreach($datos4 as $d4){
            $diez[$i][0]=$d4->archivo;
            $diez[$i][1]=$d4->contador;
            $i++;
        }
        $tiempo_megarchivo = Tiempomegaarchivos::findOrFail($request->tiempo_dash);
        $usuario_megarchivo=Users_megaarchivo::findOrFail($tiempo_megarchivo->id_user);
        return view('megaarchivo.dashboard',['usuarios' => $datos, 'tiempomegaarchivos' => $datos1, 'rutasusadas' => $datos2, 'diez' => $diez, 'tiempo' => $request->tiempo_dash, 'usuario_megarchivo' => $usuario_megarchivo]);
    }

    public function configuracionmegarchivo(Request $request){
        $tiempo_megarchivo = Tiempomegaarchivos::findOrFail($request->tiempo_conf);
        $usuario_megarchivo=Users_megaarchivo::findOrFail($tiempo_megarchivo->id_user);
        return view('megaarchivo.configuracion',['tiempo' => $request->tiempo_conf, 'usuario_megarchivo' => $usuario_megarchivo]);
    }

    public function visualizador_imagenes($ru){
        $nombre=str_replace("¬", "/", $ru);
        $carpeta=explode("/",$nombre);
        
        $ruta_carpeta='';
        for($i=0;$i<((count($carpeta)) - 1);$i++){
            $ruta_carpeta.=$carpeta[$i].'/';
        }
        $ruta_carpeta=$ruta='storage/'.substr($ruta_carpeta, 0, -1);
        
        $html=obtener_estructura_directorios2($ruta_carpeta,$carpeta[((count($carpeta)) - 1)]);
        return view('megaarchivo.visualizadorimagenes',['ru' => $ru, 'html' => $html]);
    }

    public function carruselimagenes($nombre){
        $envio_ruta=$nombre;
        $nombre=str_replace("¬", "/", $nombre);

        $ruta='storage/'.$nombre;
        
        $html=obtener_estructura_directorios($ruta,$envio_ruta);
        
        return view('megaarchivo.carruselimagenes',['html' => $html]);
    }



}
/**
    * Funcion que muestra la estructura de carpetas a partir de la ruta dada.
    */
    function obtener_estructura_directorios($ruta,$envio_ruta){
        $html='';
        $con=0;
        // Se comprueba que realmente sea la ruta de un directorio
        if (file_exists($ruta)){
            
            // Abre un gestor de directorios para la ruta indicada
            $gestor = opendir($ruta);

            // Recorre todos los elementos del directorio
            while (($archivo = readdir($gestor)) !== false)  {
                
                $ruta_completa = $ruta . "/" . $archivo;

                // Se muestran todos los archivos y carpetas excepto "." y ".."
                if ($archivo != "." && $archivo != "..") {
                    // Si es un directorio se recorre recursivamente
                    if (is_dir($ruta_completa)){
                        
                    }else{

                        $imagen = explode(".", $archivo);//separo el nombre del archivo por el punto de forma que queda la extension en $imagen[1] 

                        //voy verificando todas las posibilidades... 
                       if($imagen[1]=="JPG" || $imagen[1]=="jpg"){
                            $con++;
                            $html.='<figure id="item'.$con.'" class="carouselItem trans3d"><div class="carouselItemInner trans3d"><img src="'.url('/').'/'.$ruta_completa.'" onclick="abrir_visor(\''.$envio_ruta.'¬'.$archivo.'\')" style="width:100%;height:100%; cursor: pointer;"></div></figure>';
                        }elseif ($imagen[1]=="gif" || $imagen[1]=="GIF") {
                           $con++;
                           $html.='<figure id="item'.$con.'" class="carouselItem trans3d"><div class="carouselItemInner trans3d"><img src="'.$ruta_completa.'" onclick="abrir_visor(\''.$envio_ruta.'¬'.$archivo.'\')" style="width:100%;height:100%;cursor:pointer;"></div></figure>';   
                        }elseif($imagen[1]=="png" || $imagen[1]=="PNG"){
                            $con++;
                            $html.='<figure id="item'.$con.'" class="carouselItem trans3d"><div class="carouselItemInner trans3d"><img src="'.$ruta_completa.'" onclick="abrir_visor(\''.$envio_ruta.'¬'.$archivo.'\')" style="width:100%;height:100%;cursor:pointer;"></div></figure>';
                        }                                                
                    }
                }
            }
            // Cierra el gestor de directorios
            closedir($gestor);
        }else {
            var_dump("No es una ruta de directorio valida<br/>");
        }

        return $html;
    }




function obtener_estructura_directorios2($ruta,$archivo_abierto){
        $html='';
        $con=0;
        // Se comprueba que realmente sea la ruta de un directorio
        if (file_exists($ruta)){
            
            // Abre un gestor de directorios para la ruta indicada
            $gestor = opendir($ruta);

            // Recorre todos los elementos del directorio
            while (($archivo = readdir($gestor)) !== false)  {
                
                $ruta_completa = $ruta . "/" . $archivo;

                // Se muestran todos los archivos y carpetas excepto "." y ".."
                if ($archivo != "." && $archivo != "..") {
                    // Si es un directorio se recorre recursivamente
                    if (is_dir($ruta_completa)){
                        
                    }else{
                        
                        if($archivo_abierto==$archivo){ //Preguntar si el archivo es diferente al que se abre
                            
                        }else{
                            $imagen = explode(".", $archivo);//separo el nombre del archivo por el punto de forma que queda la extension en $imagen[1] 
                            $ru=str_replace('storage/', '', $ruta);
                            //voy verificando todas las posibilidades... 
                           if($imagen[1]=="JPG" || $imagen[1]=="jpg"){
                                $con++;
                                $html.='<li><a href="#" onclick="abrir_imagen_visualizador(\''.$ru.'\',\''.$archivo.'\')"><img src="'.url('/').'/'.$ruta_completa.'" data-large="'.url('/').'/'.$ruta_completa.'" alt="image0'.$con.'" data-description="" /></a></li>';
                            }elseif ($imagen[1]=="gif" || $imagen[1]=="GIF") {
                               $con++;
                               $html.='<li><a href="#" onclick="abrir_imagen_visualizador(\''.$ru.'\',\''.$archivo.'\')"><img src="'.url('/').'/'.$ruta_completa.'" data-large="'.url('/').'/'.$ruta_completa.'" alt="image0'.$con.'1" data-description="" /></a></li>';   
                            }elseif($imagen[1]=="png" || $imagen[1]=="PNG"){
                                $con++;
                                $html.='<li><a href="#" onclick="abrir_imagen_visualizador(\''.$ru.'\',\''.$archivo.'\')"><img src="'.url('/').'/'.$ruta_completa.'" data-large="'.url('/').'/'.$ruta_completa.'" alt="image0'.$con.'" data-description="" /></a></li>';
                            }
                        }
                                                                        
                    }
                }
            }
            // Cierra el gestor de directorios
            closedir($gestor);
        }else {
            var_dump("No es una ruta de directorio valida<br/>");
        }

        return $html;
    }