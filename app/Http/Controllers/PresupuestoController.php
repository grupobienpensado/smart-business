<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Presupuesto;
use App\Actividades;
use App\Ajuste_gastos;
use App\Presupuesto_anual;
use App\User;
use App\Configuracion;
use Auth;

class PresupuestoController extends Controller
{
  public function index(){
        $modulo=13;
        $permiso="Ver Sumatoria";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="información de entrada, presupuestos, ajuste" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver propio"){ 
                $permiso="Ver propio";
              }
            }
          }          
        }

        $fun=User::findOrFail(Auth::user()->id);
        $datos=Presupuesto::All();
        /*Usuarios que aparecen en el SELECT*/
        $query = 'SELECT C.cargo FROM permiso_modulo_filtros PMF INNER JOIN permiso_cargos C ON PMF.id_cargo = C.id WHERE PMF.deleted_at IS NULL AND PMF.id_permiso_modulo = 13';
        $cargos = DB::SELECT($query);
        $where='';
        if(count($cargos)>0){
            foreach($cargos as $cargo){
                $where.=!empty($where)?' OR cargo = "'.$cargo->cargo.'"':' WHERE (cargo = "'.$cargo->cargo.'"';
            }
        }
        $where.=!empty($where)?")":"";
        /*fin Usuarios que aparecen en el SELECT*/
        $usuarios=DB::select('SELECT * FROM `users` '.$where.' AND `id` != "'.Auth::user()->id.'" AND `deleted_at` IS NULL ORDER BY `id` ASC');
        if(date('m')==12){
          $anio=date('Y')+1;
        }else{
          $anio=date("Y");
        }
        $total['alimentacion']=0;$total['transporte_interno']=0;$total['transporte_intermunicipal']=0;$total['tiquete_aereo']=0;$total['papeleria']=0;$total['invitacion_cliente']=0;$total['alquiler_vehiculo']=0;$total['gasolina_pasaje']=0;$total['hotel']=0;$total['otros']=0;$total['salario_propio']=0;$total['salario_tercero']=0;
        $lista=[];
        $i=0; 
        foreach ($datos as $key) {
          if($key->mes==(date("m")+1)&&$key->anio==$anio){
            if($permiso=="Ver propio"){
              if($key->user_id==Auth::user()->id){
                $lista[$i]=$key;
                $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
                $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
                $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
                $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
                $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
                $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
                $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
                $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
                $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
                $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
                $total['salario_propio']+=$key->salario_propio;
                $total['salario_tercero']+=$key->salario_tercero;
                $i++;
              }
            }else{
              $lista[$i]=$key;
              $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
              $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
              $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
              $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
              $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
              $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
              $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
              $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
              $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
              $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
              $total['salario_propio']+=$key->salario_propio;
              $total['salario_tercero']+=$key->salario_tercero;
              $i++;
            }         
          }
        }

        $total['total']=$total['alimentacion']+$total['transporte_interno']+$total['transporte_intermunicipal']+$total['tiquete_aereo']+$total['papeleria']+$total['invitacion_cliente']+$total['alquiler_vehiculo']+$total['gasolina_pasaje']+$total['hotel']+$total['otros']+$total['salario_propio']+$total['salario_tercero'];
        $total['total']=number_format($total['total'], 0, '.', ',');
        
        $permiso_filtro_usuario='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario, presupuesto" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "Si"){ 
                $permiso_filtro_usuario="Si";
              }else{
                $permiso_filtro_usuario="No";
              }
            }
          }          
        }

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('presupuesto.presupuesto',["data" => $data, "lista"=>$lista, "total"=>$total, 'usuarios'=>$usuarios, 'fun'=>$fun, 'permiso'=>$permiso, 'permiso_filtro_usuario'=>$permiso_filtro_usuario]);
    }

    public function mesanterior(){
      //Eduard: Dias de Plan de trabajo
      $dias = Configuracion::find(3);
      $data['diaslimite'] = $dias->valor;
      //Eduard: Fin Dias de Plan de trabajo
      if(((int)(date('d')))<=$data['diaslimite']){
          $mes=((int)(date('m')));
          $ano = date('Y');
          $total['alimentacion']=0;
          $total['transporte_interno']=0;
          $total['transporte_intermunicipal']=0;
          $total['tiquete_aereo']=0;
          $total['papeleria']=0;
          $total['invitacion_cliente']=0;
          $total['alquiler_vehiculo']=0;
          $total['gasolina_pasaje']=0;
          $total['hotel']=0;
          $total['otros']=0;
          $total['salario_propio']=0;
          $total['salario_tercero']=0;
        $datos=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.Auth::user()->id.'" AND `mes` = "'.$mes.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
        $lista=[];
        $i=0; 
        foreach ($datos as $key) {
            $lista[$i]=$key;
            $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
            $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
            $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
            $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
            $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
            $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
            $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
            $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
            $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
            $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
            $total['salario_propio']+=$key->salario_propio;
            $total['salario_tercero']+=$key->salario_tercero;
            $i++;
        }
          $total['total']=$total['alimentacion']+$total['transporte_interno']+$total['transporte_intermunicipal']+$total['tiquete_aereo']+$total['papeleria']+$total['invitacion_cliente']+$total['alquiler_vehiculo']+$total['gasolina_pasaje']+$total['hotel']+$total['otros']+$total['salario_propio']+$total['salario_tercero'];
          /*Consulta para activar el item del menu correspondiente*/
            $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
            $data['item_menu'] = DB::select($query);
            if(count($data['item_menu']) > 0){
                $data['item_menu']='menu_'.$data['item_menu'][0]->id;
            }else{
                $data['item_menu']='';
            }
            /*Fin Consulta para activar el item del menu correspondiente*/
        return view('presupuesto.presupuestoanterior2',["data" => $data, "lista"=>$lista, "total"=>$total]);
      }else{
        $index=PresupuestoController::index();
        return $index;
      }
        
    }

    public function filtrarpresupuesto(Request $request){
        $modulo=13;
        $permiso_usr="Ver Sumatoria";
        $usuario=$request->user;
        $mes=$request->mes;
        $html='';
        $permiso_editar_mes_anterior='no';
        if($usuario!=''&&$mes!=''){
            if($usuario=="sumatoria"){
                $datos=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.((int)(date('m' , strtotime ( $mes )))).'" AND `anio` = "'.date('Y' , strtotime ( $mes )).'" ORDER BY `id` ASC');
            }else{
                $fun=User::findOrFail($usuario);
                $datos=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$usuario.'" AND `mes` = "'.((int)(date('m' , strtotime ( $mes )))).'" AND `anio` = "'.date('Y' , strtotime ( $mes )).'" ORDER BY `id` ASC');    
            }
            

        }else if($usuario!=''){
            $mes=date('Y-m-d');
            $mes = strtotime ( '+1 month' , strtotime ( $mes ) ) ;
            $mes = date ( 'Y-m-j' , $mes );
            if($usuario=="sumatoria"){
                $datos=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.((int)(date('m' , strtotime ( $mes )))).'" AND `anio` = "'.date('Y' , strtotime ( $mes )).'" ORDER BY `id` ASC');
            }else{
                $fun=User::findOrFail($usuario);
                $datos=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$usuario.'" AND `mes` = "'.((int)(date('m' , strtotime ( $mes )))).'" AND `anio` = "'.date('Y' , strtotime ( $mes )).'" ORDER BY `id` ASC');   
            }                     
        }else if($mes!=''){
            $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="información de entrada, presupuestos, ajuste" AND `id_permisomodulo`="'.$modulo.'"');
            if(isset($acceso[0]->id)){
              $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
              if(isset($cargo[0]->id)){
                $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                if(isset($permiso[0]->permiso)){
                  if($permiso[0]->permiso == "Ver propio"){ 
                    $permiso_usr="Ver propio";
                    $datos=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.Auth::user()->id.'" AND `mes` = "'.((int)(date('m' , strtotime ( $mes )))).'" AND `anio` = "'.date('Y' , strtotime ( $mes )).'" ORDER BY `id` ASC');
                  }else{
                    $datos=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.((int)(date('m' , strtotime ( $mes )))).'" AND `anio` = "'.date('Y' , strtotime ( $mes )).'" ORDER BY `id` ASC');
                  }
                }
              }          
            }
            $fun=User::findOrFail(Auth::user()->id);

            //Eduard: Dias de Plan de trabajo
              $dias = Configuracion::find(3);
              $data['diaslimite'] = $dias->valor;
            //Eduard: Fin Dias de Plan de trabajo
            /*permiso editar mes anterior*/
            
            $mes_anterior=date ('m');
            if(((int)(date('m' , strtotime ( $mes ))))==$mes_anterior){
                if(((int)(date('d')))<=$data['diaslimite']){
                    $permiso_editar_mes_anterior="si";
                }
            }
            
        }
        
        $anio=date('Y' , strtotime($mes));
        $usuarios=DB::select('SELECT * FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution" ORDER BY `id` ASC');
        $dias=cal_days_in_month ( CAL_GREGORIAN, date("m", strtotime($mes)), $anio );
        $semana=["Dom","Lun","Mar","Mie","Jue","Vie","Sab"];
        $meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic"];
        
        $total['alimentacion']=0;$total['transporte_interno']=0;$total['transporte_intermunicipal']=0;$total['tiquete_aereo']=0;$total['papeleria']=0;$total['invitacion_cliente']=0;$total['alquiler_vehiculo']=0;$total['gasolina_pasaje']=0;$total['hotel']=0;$total['otros']=0;$total['salario_propio']=0;$total['salario_tercero']=0;
        $lista=[];
        $i=0; foreach ($datos as $key) {
            $lista[((int)($key->dia))]=$key;
            $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
            $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
            $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
            $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
            $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
            $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
            $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
            $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
            $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
            $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
            $total['salario_propio']+=$key->salario_propio;
            $total['salario_tercero']+=$key->salario_tercero;
            $i++;
        }
        $total['total']=$total['alimentacion']+$total['transporte_interno']+$total['transporte_intermunicipal']+$total['tiquete_aereo']+$total['papeleria']+$total['invitacion_cliente']+$total['alquiler_vehiculo']+$total['gasolina_pasaje']+$total['hotel']+$total['otros']+$total['salario_propio']+$total['salario_tercero'];
        $meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $html.='<div class="col-md-12 inline-block">
        <h3>Presupuesto de ';
        
        if($usuario=="sumatoria"){
            $quien="Todos los usuarios";
        }else if($usuario==""){
            if($permiso_usr=="Ver Sumatoria"){
                $quien="Todos los usuarios";
            }else{
                $quien=$fun->nombres.' '.$fun->apellidos;
            }           
        }else{
            $quien=$fun->nombres.' '.$fun->apellidos;    
        } 
        $html.=$quien.' Mes de '.$meses2[((int)(date('m' , strtotime($mes))))-1].' de '.date('Y' , strtotime($mes)).'</h3>
        <h3><strong>($ '.number_format($total['total']).')</strong></h3>';
        if($permiso_editar_mes_anterior=='si'){
          $html.='<a href="'.$request->url.'/presupuestoanterior"><i class="fa fa-money"></i> Editar Presupuesto</a>';
        }
        
        $html.='</div>
        <form method="POST" action="'.$request->url.'/presupuestosave" enctype="multipart/form-data">
        
        '.$request->csrf.'
         <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th width="100"></th>
                 <th class="text-center">Alimentación</th>
                 <th class="text-center">Transporte Interno</th>
                 <th class="text-center">Transporte Intermunicipal</th>
                 <th class="text-center">Tiquete Aereo</th>
                 <th class="text-center">Papeleria</th>
                 <th class="text-center">Invitación Cliente</th>
                 <th class="text-center">Alquiler Vehiculo</th>
                 <th class="text-center">Gasolina y Pasaje</th>
                 <th class="text-center">Hotel</th>
                 <th class="text-center">Otros</th>
                 <th class="text-center">Salario Propio</th>
                 <th class="text-center">Salario Tercero</th>
               </tr>
             </thead>
             <tbody>
              <tr>
                 <td class="dia">Total</td>
                 <td id="total_alimentacion">$ '.number_format($total['alimentacion']).'</td>
                 <td id="total_transporte_interno">$ '.number_format($total['transporte_interno']).'</td>
                 <td id="total_transporte_intermunicipal">$ '.number_format($total['transporte_intermunicipal']).'</td>
                 <td id="total_tiquete_aereo">$ '.number_format($total['tiquete_aereo']).'</td>
                 <td id="total_papeleria">$ '.number_format($total['papeleria']).'</td>
                 <td id="total_invitacion_cliente">$ '.number_format($total['invitacion_cliente']).'</td>
                 <td id="total_alquiler_vehiculo">$ '.number_format($total['alquiler_vehiculo']).'</td>
                 <td id="total_gasolina_pasaje">$ '.number_format($total['gasolina_pasaje']).'</td>
                 <td id="total_hotel">$ '.number_format($total['hotel']).'</td>
                 <td id="total_otros">$ '.number_format($total['otros']).'</td>
                 <td id="total_salario_propio">$ '.number_format($total['salario_propio']).'</td>
                 <td id="total_salario_tercero">$ '.number_format($total['salario_tercero']).'</td>
              </tr>';
             for($i=1;$i<=$dias;$i++){
                if($i<10){
                    $i="0".$i;
                }
             $nuevo=strtotime(date("Y-m", strtotime($mes))."-01");
             $nuevo = date ( 'Y-m' , $nuevo );
             $html.='<input type="hidden" name="datos['.($i-1).'][mes]" value="'.date("m", strtotime($mes)).'">
             <input type="hidden" name="datos['.($i-1).'][anio]" value="'.$anio.'">
             <input type="hidden" name="datos['.($i-1).'][dia]" value="'.$i.'">';
             if(!empty($lista[$i-1])){
             $html.='<input type="hidden" name="datos['.($i-1).'][id]" value="'.$lista[$i-1]->id.'">';
             }
               $html.='<tr>
                 <td class="dia">'.$semana[date("w",strtotime($nuevo."-".$i))].", ".$i." ".$meses[((int)date("m", strtotime($mes)))-1].'</td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero alimentacion" type="text" name="datos['.($i-1).'][alimentacion]" placeholder="$0"';  if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->alimentacion)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero transporte_interno" type="text" name="datos['.($i-1).'][transporte_interno]" placeholder="$0"'; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->transporte_interno)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero transporte_intermunicipal" type="text" name="datos['.($i-1).'][transporte_intermunicipal]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->transporte_intermunicipal)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero tiquete_aereo" type="text" name="datos['.($i-1).'][tiquete_aereo]" placeholder="$0"'; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->tiquete_aereo)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero papeleria" type="text" name="datos['.($i-1).'][papeleria]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->papeleria)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero invitacion_cliente" type="text" name="datos['.($i-1).'][invitacion_cliente]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->invitacion_cliente)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero alquiler_vehiculo" type="text" name="datos['.($i-1).'][alquiler_vehiculo]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->alquiler_vehiculo)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero gasolina_pasaje" type="text" name="datos['.($i-1).'][gasolina_pasaje]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->gasolina_pasaje)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero hotel" type="text" name="datos['.($i-1).'][hotel]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->hotel)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero otros" type="text" name="datos['.($i-1).'][otros]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->otros)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero salario_propio" type="text" name="datos['.($i-1).'][salario_propio]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->salario_propio)), 0, '.', ',').'" '; } $html.=' disabled></td>
                 <td><input style="cursor: no-drop" class="form-control2 form-control dinero salario_tercero" type="text" name="datos['.($i-1).'][salario_tercero]" placeholder="$0" '; if(!empty($lista[((int)$i)])){ $html.='value="'.number_format(((int)($lista[((int)$i)]->salario_tercero)), 0, '.', ',').'" '; } $html.=' disabled></td>
               </tr>';
            }
             $html.='</tbody>
           </table>
         </div>
      </form>';
        return \Response::json(['html' => $html]);
    }

    public function indexanterior(){
    	$datos=Presupuesto::All();
    	if(date('m')==1){
		  $anio=date('Y')-1;
		}else{
		  $anio=date("Y");
		}
		$lista=[];
    	$i=0; foreach ($datos as $key) {
    		# code...
    		if($key->user_id==Auth::user()->id&&$key->mes==(date("m")-1)&&$key->anio==$anio){
    			$lista[$i]=$key;
    			$i++;
    		}
    	}
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('presupuesto.presupuestoanterior',["data" => $data, "lista"=>$lista]);
    }

    public function indexanual(){
        $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.Auth::user()->id.'" AND `anio` = "'.date('Y').'" ORDER BY `id` ASC');
        $total['alimentacion']=0;$total['transporte_interno']=0;$total['transporte_intermunicipal']=0;$total['tiquete_aereo']=0;$total['papeleria']=0;$total['invitacion_cliente']=0;$total['alquiler_vehiculo']=0;$total['gasolina_pasaje']=0;$total['hotel']=0;$total['otros']=0;
    	$lista=[];
    	$i=0; foreach ($datos as $key) {
   			$lista[$i]=$key;
            $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
            $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
            $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
            $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
            $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
            $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
            $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
            $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
            $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
            $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
   			$i++;
    	}
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('presupuesto.presupuestoanual',["data" => $data, "lista"=>$lista, "total"=>$total]);
    }

    public function indexanualsiguiente(){

        $modulo=13;
        $permiso="Ver Sumatoria";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="información de entrada, presupuestos, ajuste" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver propio"){ 
                $permiso="Ver propio";
              }
            }
          }          
        }

        $fun=User::findOrFail(Auth::user()->id);
        /*Usuarios que aparecen en el SELECT*/
        $query = 'SELECT C.cargo FROM permiso_modulo_filtros PMF INNER JOIN permiso_cargos C ON PMF.id_cargo = C.id WHERE PMF.deleted_at IS NULL AND PMF.id_permiso_modulo = 13';
        $cargos = DB::SELECT($query);
        $where='';
        if(count($cargos)>0){
            foreach($cargos as $cargo){
                $where.=!empty($where)?' OR cargo = "'.$cargo->cargo.'"':' WHERE (cargo = "'.$cargo->cargo.'"';
            }
        }
        $where.=!empty($where)?")":"";
        /*fin Usuarios que aparecen en el SELECT*/
        $usuarios=DB::select('SELECT * FROM `users` '.$where.' ORDER BY `id` ASC');
        //Eduard: Permiso para llenar el presupuesto anual del año actual
        $query = 'SELECT valor FROM configuracions WHERE id = 4 AND deleted_at IS NULL';
        $permiso_ano_actual = DB::SELECT($query);
        if(isset($permiso_ano_actual[0]->valor) && $permiso_ano_actual[0]->valor == 'Año Actual'){
            $ano=((int)(date('Y')));
        }else{
            $ano=((int)(date('Y')+1));
        }
        $data["ano"] = $ano;
        if($permiso=="Ver propio"){
            $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.Auth::user()->id.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
        }else{
            $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.$ano.'" ORDER BY `id` ASC');    
        }
        
        $total['alimentacion']=0;$total['transporte_interno']=0;$total['transporte_intermunicipal']=0;$total['tiquete_aereo']=0;$total['papeleria']=0;$total['invitacion_cliente']=0;$total['alquiler_vehiculo']=0;$total['gasolina_pasaje']=0;$total['hotel']=0;$total['otros']=0;
        $lista=[];
        $i=0; 
        foreach ($datos as $key) {
            $lista[$i]=$key;
            $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
            $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
            $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
            $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
            $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
            $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
            $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
            $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
            $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
            $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
            $i++;
        }
        $total['total']=$total['alimentacion']+$total['transporte_interno']+$total['transporte_intermunicipal']+$total['tiquete_aereo']+$total['papeleria']+$total['invitacion_cliente']+$total['alquiler_vehiculo']+$total['gasolina_pasaje']+$total['hotel']+$total['otros'];
        $total['total']=number_format($total['total'], 0, '.', ',');
        
        $permiso_filtros="No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario y año, presupuesto anual" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
            $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
            if(isset($cargo1[0]->id)){
                $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
                if(isset($permiso1[0]->permiso)){
                    if($permiso1[0]->permiso == "Si"){ 
                        $permiso_filtros="Si";
                    }
                }
            }          
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('presupuesto.presupuestoanualsiguiente',["data" => $data, "lista"=>$lista, "total"=>$total, 'fun'=>$fun, 'usuarios'=>$usuarios, 'permiso_filtros'=>$permiso_filtros, 'permiso' => $permiso]);
    }

    public function filtrarpresupuestoanual(Request $request){
        $modulo=13;
        $permiso_usr="Ver Sumatoria";

        $usuario=$request->user;
        $ano=$request->ano;
        $html='';
        if($usuario!=''&&$ano!=''){
            if($usuario!='sumatoria'){
                $fun=User::findOrFail($usuario);
                $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.$usuario.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
            }else{
                $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.$ano.'" ORDER BY `id` ASC');
            }
        }else if($usuario!=''){
            if($usuario!='sumatoria'){
                $ano=date('Y')+1;
                $fun=User::findOrFail($usuario);
                $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.$usuario.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
            }else{
                $ano=date('Y')+1;
                $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.$ano.'" ORDER BY `id` ASC');
            }
                        
        }else if($ano!=''){
            
            $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="información de entrada, presupuestos, ajuste" AND `id_permisomodulo`="'.$modulo.'"');
            if(isset($acceso[0]->id)){
              $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
              if(isset($cargo[0]->id)){
                $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                if(isset($permiso[0]->permiso)){
                  if($permiso[0]->permiso == "Ver propio"){ 
                    $permiso_usr="Ver propio";
                    $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.Auth::user()->id.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
                  }else{
                    $datos=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.$ano.'" ORDER BY `id` ASC');
                  }
                }
              }          
            }
            $fun=User::findOrFail(Auth::user()->id);
            
        }

        if($usuario=='sumatoria'){
            $quien='de Todos los usuarios';
        }else if($usuario==''){
            if($permiso_usr=="Ver Sumatoria"){
                $quien='de Todos los usuarios';
            }else{
                $quien=$fun->nombres.' '.$fun->apellidos;
            }
        }else{            
            $quien=$fun->nombres.' '.$fun->apellidos;
        }
        $total['alimentacion']=0;$total['transporte_interno']=0;$total['transporte_intermunicipal']=0;$total['tiquete_aereo']=0;$total['papeleria']=0;$total['invitacion_cliente']=0;$total['alquiler_vehiculo']=0;$total['gasolina_pasaje']=0;$total['hotel']=0;$total['otros']=0;
        $meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic"];
        $lista=[];
        $i=0; 
        foreach ($datos as $key) {
            $lista[$i]=$key;
            $total['alimentacion']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alimentacion)))));
            $total['transporte_interno']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_interno)))));
            $total['transporte_intermunicipal']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->transporte_intermunicipal)))));
            $total['tiquete_aereo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->tiquete_aereo)))));
            $total['papeleria']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->papeleria)))));
            $total['invitacion_cliente']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->invitacion_cliente)))));
            $total['alquiler_vehiculo']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->alquiler_vehiculo)))));
            $total['gasolina_pasaje']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->gasolina_pasaje)))));
            $total['hotel']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->hotel)))));
            $total['otros']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', ($key->otros)))));
            $i++;
        }
        $total['total']=$total['alimentacion']+$total['transporte_interno']+$total['transporte_intermunicipal']+$total['tiquete_aereo']+$total['papeleria']+$total['invitacion_cliente']+$total['alquiler_vehiculo']+$total['gasolina_pasaje']+$total['hotel']+$total['otros'];
        $html.='<div class="col-md-12 inline-block">
          <h3>Presupuesto '.$quien.' de '.$ano.'</h3>
          <h3><strong>($ '.number_format($total['total']).')</strong></h3>
      </div>
        <form method="POST" action="'.$request->url.'/presupuestoanualsave" enctype="multipart/form-data">
        
        '.$request->csrf.'
         <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th width="100"></th>
                 <th class="text-center">Alimentación</th>
                 <th class="text-center">Transporte Interno</th>
                 <th class="text-center">Transporte Intermunicipal</th>
                 <th class="text-center">Tiquete Aereo</th>
                 <th class="text-center">Papeleria</th>
                 <th class="text-center">Invitación Cliente</th>
                 <th class="text-center">Alquiler Vehiculo</th>
                 <th class="text-center">Gasolina y Pasaje</th>
                 <th class="text-center">Hotel</th>
                 <th class="text-center">Otros</th>
               </tr>
             </thead>
             <tbody>
             <tr>
               <td class="dia">Total</td>
               <td id="total_alimentacion">$ '.number_format($total['alimentacion']).'</td>
               <td id="total_transporte_interno">$ '.number_format($total['transporte_interno']).'</td>
               <td id="total_transporte_intermunicipal">$ '.number_format($total['transporte_intermunicipal']).'</td>
               <td id="total_tiquete_aereo">$ '.number_format($total['tiquete_aereo']).'</td>
               <td id="total_papeleria">$ '.number_format($total['papeleria']).'</td>
               <td id="total_invitacion_cliente">$ '.number_format($total['invitacion_cliente']).'</td>
               <td id="total_alquiler_vehiculo">$ '.number_format($total['alquiler_vehiculo']).'</td>
               <td id="total_gasolina_pasaje">$ '.number_format($total['gasolina_pasaje']).'</td>
               <td id="total_hotel">$ '.number_format($total['hotel']).'</td>
               <td id="total_otros">$ '.number_format($total['otros']).'</td>
            </tr>';
             for($i=1;$i<=12;$i++){
             $html.='<input type="hidden" name="datos['.($i-1).'][mes]" value="'.$i.'">
             <input type="hidden" name="datos['.($i-1).'][anio]" value="'.($ano).'">';
             if(!empty($lista[$i-1])){
             $html.='<input type="hidden" name="datos['.($i-1).'][id]" value="'.$lista[($i-1)]->id.'">';
             }else{
             $html.='<input type="hidden" name="datos['.($i-1).'][id]">';
             }
             $html.='<tr>
                 <td>'.$meses[($i-1)]." de ".($ano).'</td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero alimentacion" type="text" name="datos['.($i-1).'][alimentacion]" placeholder="$0" ';  if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->alimentacion))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero transporte_interno" type="text" name="datos['.($i-1).'][transporte_interno]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->transporte_interno))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero transporte_intermunicipal" type="text" name="datos['.($i-1).'][transporte_intermunicipal]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->transporte_intermunicipal))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero tiquete_aereo" type="text" name="datos['.($i-1).'][tiquete_aereo]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->tiquete_aereo))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero papeleria" type="text" name="datos['.($i-1).'][papeleria]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->papeleria))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero invitacion_cliente" type="text" name="datos['.($i-1).'][invitacion_cliente]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->invitacion_cliente))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero alquiler_vehiculo" type="text" name="datos['.($i-1).'][alquiler_vehiculo]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->alquiler_vehiculo))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero gasolina_pasaje" type="text" name="datos['.($i-1).'][gasolina_pasaje]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->gasolina_pasaje))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero hotel" type="text" name="datos['.($i-1).'][hotel]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->hotel))), 0, '.', ',').'" '; } $html.='></td>
                 <td><input style="cursor: no-drop;" disabled class="form-control2 form-control dinero otros" type="text" name="datos['.($i-1).'][otros]" placeholder="$0" '; if(!empty($lista[((int)$i)-1])){ $html.='value="'.number_format(((int)(str_replace(',', '', $lista[$i-1]->otros))), 0, '.', ',').'" '; } $html.='></td>
               </tr>';
               }
             $html.='</tbody>
           </table>
         </div>
      </form>';
      return \Response::json(['html' => $html]);
    }

    public function savepresupuesto(Request $request){
    	foreach ($request->datos as $key) {
    		if(!isset($key["id"])){
    			$datos=new Presupuesto;
    		}else{
    			$datos = Presupuesto::findOrFail($key["id"]);
    		}
    		$datos->mes=$key["mes"];
    		$datos->anio=$key["anio"];
    		$datos->dia=$key["dia"];
    		$datos->alimentacion=str_replace(',', '', $key["alimentacion"]);
    		$datos->transporte_interno=str_replace(',', '', $key["transporte_interno"]);
    		$datos->transporte_intermunicipal=str_replace(',', '', $key["transporte_intermunicipal"]);
    		$datos->tiquete_aereo=str_replace(',', '', $key["tiquete_aereo"]);
    		$datos->papeleria=str_replace(',', '', $key["papeleria"]);
    		$datos->invitacion_cliente=str_replace(',', '', $key["invitacion_cliente"]);
    		$datos->alquiler_vehiculo=str_replace(',', '', $key["alquiler_vehiculo"]);
    		$datos->gasolina_pasaje=str_replace(',', '', $key["gasolina_pasaje"]);
    		$datos->hotel=str_replace(',', '', $key["hotel"]);
    		$datos->otros=str_replace(',', '', $key["otros"]);
            $datos->salario_propio=intval(str_replace(',', '', $key["salario_propio"]));
            $datos->salario_tercero=intval(str_replace(',', '', $key["salario_tercero"]));
    		$datos->user_id=Auth::user()->id;
    		$datos->save();
    	}
    	$model = Actividades::all();
    	$lista = Ajuste_gastos::all();
    	return redirect('presupuesto');
    }

    public function savepresupuestoanual(Request $request){
    	foreach ($request->datos as $key) {
    		if(empty($key["id"])){
    			$datos=new Presupuesto_anual;
    		}else{
    			$datos = Presupuesto_anual::findOrFail($key["id"]);
    		}
    		$datos->mes=$key["mes"];
    		$datos->anio=$key["anio"];
    		$datos->alimentacion=str_replace(',', '', $key["alimentacion"]);
            $datos->transporte_interno=str_replace(',', '', $key["transporte_interno"]);
            $datos->transporte_intermunicipal=str_replace(',', '', $key["transporte_intermunicipal"]);
            $datos->tiquete_aereo=str_replace(',', '', $key["tiquete_aereo"]);
            $datos->papeleria=str_replace(',', '', $key["papeleria"]);
            $datos->invitacion_cliente=str_replace(',', '', $key["invitacion_cliente"]);
            $datos->alquiler_vehiculo=str_replace(',', '', $key["alquiler_vehiculo"]);
            $datos->gasolina_pasaje=str_replace(',', '', $key["gasolina_pasaje"]);
            $datos->hotel=str_replace(',', '', $key["hotel"]);
            $datos->otros=str_replace(',', '', $key["otros"]);
    		$datos->user_id=Auth::user()->id;
    		$datos->save();
    	}
    	$model = Actividades::all();
    	$lista = Ajuste_gastos::all();
    	return redirect("/presupuestoanualsiguiente");
    	//return redirect('gastos.gastos',["actividades"=>$model,"id"=>Auth::user()->id,"ajustes"=>$lista]);
    }

    public function comparativo_accion(Request $request){
        switch($request->accion){
            /*Grafico Comparativo de Gastos*/
            case 0:
                $filtro_mes = ($request->mes != "")?(int)$request->mes:(int)(date('m'));
                $filtro_ano = ($request->ano != "")?$request->ano:date('Y');
                //Eduard: Hallar el ajuste del día 32
                if($request->mes!="" && $request->mes!=date('m')){
                    $filtro_tipo = ($request->tipo != "")?'(CONVERT(IF(IsNull(A.valor_'.$request->tipo.'), 0, A.valor_'.$request->tipo.'),UNSIGNED INTEGER))':'(CONVERT(IF(IsNull(A.valor_alimentacion), 0, A.valor_alimentacion),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_transporte_interno), 0, A.valor_transporte_interno),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_transporte_intermunicipal), 0, A.valor_transporte_intermunicipal),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_tiquete_aereo), 0, A.valor_tiquete_aereo),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_papeleria), 0, A.valor_papeleria),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_invitacion_cliente), 0, A.valor_invitacion_cliente),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_alquiler_vehiculo), 0, A.valor_alquiler_vehiculo),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_gasolina_pasaje), 0, A.valor_gasolina_pasaje),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_hotel), 0, A.valor_hotel),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.valor_otros), 0, A.valor_otros),UNSIGNED INTEGER))';
                    $query = 'SELECT IF(IsNull(SUM'.$filtro_tipo.'), 0,SUM'.$filtro_tipo.') AS TOTALVALORREAL FROM ajuste_gastos A WHERE A.anio = "'.$filtro_ano.'" AND A.mes = "'.$request->mes.'" AND A.tipo = "valorreal"';
                    $data['query_valor_real'] = $query;
                    $valor_real = DB::select($query);
                }
                $filtro_tipo = ($request->tipo != "")?'CONVERT(IF(IsNull(A.'.$request->tipo.'), 0, A.'.$request->tipo.'),UNSIGNED INTEGER)':'(CONVERT(IF(IsNull(A.alimentacion), 0, A.alimentacion),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.transporte_interno), 0, A.transporte_interno),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.transporte_intermunicipal), 0, A.transporte_intermunicipal),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.tiquete_aereo), 0, A.tiquete_aereo),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.papeleria), 0, A.papeleria),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.invitacion_cliente), 0, A.invitacion_cliente),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.alquiler_vehiculo), 0, A.alquiler_vehiculo),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.gasolina_pasaje), 0, A.gasolina_pasaje),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.hotel), 0, A.hotel),UNSIGNED INTEGER) + CONVERT(IF(IsNull(A.otros), 0, A.otros),UNSIGNED INTEGER))';
                $modulo=13;
                $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="detalle de los select USUARIO" AND `id_permisomodulo`="'.$modulo.'"');
                if(isset($acceso[0]->id)){
                  $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                  if(isset($cargo[0]->id)){
                    $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                    if(isset($permiso[0]->permiso)){
                      if($permiso[0]->permiso == "Ver propio"){
                        $filtro_user = ($request->usuario != "")?(($request->usuario != "sumatoria")?' AND A.user_id = '.$request->usuario:''):' AND A.user_id = '.Auth::user()->id;
                        $mi_presupuesto = DB::select('SELECT '.$filtro_tipo.' AS TOTAL, A.dia FROM presupuestos A WHERE A.mes="'.$filtro_mes.'" AND A.anio = "'.$filtro_ano.'"'.$filtro_user.' ORDER BY A.dia ASC');
                        $query_user = $filtro_user;
                      }else{
                        $filtro_user = ($request->usuario != "" && $request->usuario != "sumatoria")?' AND A.user_id = '.$request->usuario:'';
                        $mi_presupuesto = DB::select('SELECT '.$filtro_tipo.' AS TOTAL, A.dia FROM presupuestos A WHERE A.mes = "'.$filtro_mes.'" AND A.anio = "'.$filtro_ano.'"'.$filtro_user.' ORDER BY A.dia ASC');
                        $query_user = $filtro_user;
                      }
                    }
                  }
                }
                $fecha = ($request->mes != "" && $request->ano != "")?$request->ano.'-'.$request->mes.'-':(date('Y-m-'));
                $request->tipo = $request->tipo=='transporte_interno'?'transportes_internos':($request->tipo=='transporte_intermunicipal'?'transportes_intermunicipales':($request->tipo=='gasolina_pasaje'?'gasolina_pasajes':$request->tipo));
                $filtro_tipo = ($request->tipo != "")?'CONVERT(REPLACE(IF(IsNull(A.'.$request->tipo.'), 0, A.'.$request->tipo.'), ",", ""),UNSIGNED INTEGER)':'(CONVERT(REPLACE(IF(IsNull(A.alimentacion), 0, A.alimentacion), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.transportes_internos), 0, A.transportes_internos), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.transportes_intermunicipales), 0, A.transportes_intermunicipales), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.tiquete_aereo), 0, A.tiquete_aereo), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.papeleria), 0, A.papeleria), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.invitacion_cliente), 0, A.invitacion_cliente), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.alquiler_vehiculo), 0, A.alquiler_vehiculo), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.gasolina_pasajes), 0, A.gasolina_pasajes), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.hotel), 0, A.hotel), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.otros), 0, A.otros), ",", ""),UNSIGNED INTEGER))';
                $query = 'SELECT SUM('.$filtro_tipo.') AS TOTAL, A.fecha_actividad FROM actividades A WHERE A.fecha_actividad LIKE "%'.$fecha.'%"'.$query_user.' GROUP BY A.fecha_actividad ASC';
                $actividades = DB::select($query);
                $mes = ($request->mes != "")?$request->mes:(date('m'));
                $numero_dias = cal_days_in_month(CAL_GREGORIAN, $mes, $filtro_ano);
                $meses = ["","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
                $acumulado=0;
                $diario_presupuesto=0;
                ob_start();
?>
<div id="chartdiv-3"></div>
<script>
var chart = AmCharts.makeChart( "chartdiv-3", {
  "type": "serial",
  "legend": {
      "useGraphSettings": true
  },
  "addClassNames": true,
  "theme": "light",
  "autoMargins": false,
  "marginLeft": 90,
  "marginRight": 8,
  "marginTop": 10,
  "marginBottom": 26,
  "balloon": {
    "adjustBorderColor": false,
    "horizontalPadding": 10,
    "verticalPadding": 8,
    "color": "#ffffff"
  },

  "dataProvider": [
                <?php
                for($i=1;$i<=$numero_dias;$i++){
                    $diario=0;
                    if($i<10){
                        $i="0".$i;
                    }
                    foreach($actividades as $key){
                        if($key->fecha_actividad==$fecha.$i){
                            $diario=$key->TOTAL;
                            $acumulado=$acumulado+$diario;
                        }
                    }
                    foreach($mi_presupuesto as $m_p){
                        if($m_p->dia == $i){
                            $diario_presupuesto+=$m_p->TOTAL;
                        }
                    }
                    ?>
{
    "year": "<?=$i?>",
    "income": <?=$acumulado?>,
    "expenses": <?=$diario_presupuesto?>
  },
                    <?php }
                      if(isset($valor_real)){
                          $ajuste = $valor_real[0]->TOTALVALORREAL;
                          $i++;
      ?>
{
    "year": "<?=$i?>",
    "income": <?=$ajuste?>,
    "expenses": <?=$diario_presupuesto?>
  },
                    <?php } ?>
    ],
  "valueAxes": [ {
    "axisAlpha": 0,
    "position": "left"
  } ],
  "startDuration": 1,
  "graphs": [ {
    "alphaField": "alpha",
    "balloonText": "<span style='font-size:12px;'>[[title]] en <?=$meses[intval($mes)]?> [[category]]:<br><span style='font-size:20px;'>$ [[value]]</span> [[additional]]</span>",
    "fillAlphas": 1,
    "title": "Gastado según bitácora",
    "type": "column",
    "valueField": "income",
    "dashLengthField": "dashLengthColumn"
  }, {
    "id": "graph2",
    "balloonText": "<span style='font-size:12px;'>[[title]] en <?=$meses[intval($mes)]?> [[category]]:<br><span style='font-size:20px;'>$ [[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Presupuesto mensual de gastos",
    "valueField": "expenses",
    "dashLengthField": "dashLengthLine"
  } ],
  "categoryField": "year",
  "categoryAxis": {
    "gridPosition": "start",
    "axisAlpha": 0,
    "tickLength": 0
  },
  "export": {
    "enabled": false
  }
} );
</script>
                    <?php
                    $data['html'] = ob_get_contents();
                    ob_end_clean();
                    $data['titulo'] = 'Grafico Comparativo de Gastos '.$meses[$filtro_mes].' de '.$filtro_ano;
                break;
            /*Grafico Comparativo de Gastos VS presupuesto*/
            case 1:
                $filtro_ano = ($request->ano != "")?$request->ano:date('Y');
                $filtro_tipo = ($request->tipo != "")?'(CONVERT(REPLACE(IF(IsNull(A.'.$request->tipo.'), 0, A.'.$request->tipo.'), ",", ""),UNSIGNED INTEGER))':'(CONVERT(REPLACE(IF(IsNull(A.alimentacion), 0, A.alimentacion), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.transporte_interno), 0, A.transporte_interno), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.transporte_intermunicipal), 0, A.transporte_intermunicipal), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.tiquete_aereo), 0, A.tiquete_aereo), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.papeleria), 0, A.papeleria), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.invitacion_cliente), 0, A.invitacion_cliente), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.alquiler_vehiculo), 0, A.alquiler_vehiculo), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.gasolina_pasaje), 0, A.gasolina_pasaje), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.hotel), 0, A.hotel), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.otros), 0, A.otros), ",", ""),UNSIGNED INTEGER))';
                $modulo=13;
                $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="detalle de los select USUARIO" AND `id_permisomodulo`="'.$modulo.'"');
                if(isset($acceso[0]->id)){
                  $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                  if(isset($cargo[0]->id)){
                    $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                    if(isset($permiso[0]->permiso)){
                      if($permiso[0]->permiso == "Ver propio"){
                        $filtro_user = ($request->usuario != "")?(($request->usuario != "sumatoria")?' AND A.user_id = '.$request->usuario:''):' AND A.user_id = '.Auth::user()->id;
                        $presupuesto_anual = DB::select('SELECT SUM'.$filtro_tipo.' AS TOTAL, A.mes FROM presupuesto_anuals A WHERE A.anio = "'.$filtro_ano.'"'.$filtro_user.' GROUP BY A.mes ASC');
                        $query_user = $filtro_user;
                      }else{
                        $filtro_user = ($request->usuario != "" && $request->usuario != "sumatoria")?' AND A.user_id = '.$request->usuario:'';
                        $presupuesto_anual = DB::select('SELECT SUM'.$filtro_tipo.' AS TOTAL, A.mes FROM presupuesto_anuals A WHERE A.anio = "'.$filtro_ano.'"'.$filtro_user.' GROUP BY A.mes ASC');
                        $query_user = $filtro_user;
                      }
                    }
                  }
                }
                $request->tipo = $request->tipo=='transporte_interno'?'transportes_internos':($request->tipo=='transporte_intermunicipal'?'transportes_intermunicipales':($request->tipo=='gasolina_pasaje'?'gasolina_pasajes':$request->tipo));
                $filtro_tipo2 = ($request->tipo != "")?'CONVERT(REPLACE(IF(IsNull(A.'.$request->tipo.'), 0, A.'.$request->tipo.'), ",", ""),UNSIGNED INTEGER)':'(CONVERT(REPLACE(IF(IsNull(A.alimentacion), 0, A.alimentacion), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.transportes_internos), 0, A.transportes_internos), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.transportes_intermunicipales), 0, A.transportes_intermunicipales), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.tiquete_aereo), 0, A.tiquete_aereo), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.papeleria), 0, A.papeleria), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.invitacion_cliente), 0, A.invitacion_cliente), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.alquiler_vehiculo), 0, A.alquiler_vehiculo), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.gasolina_pasajes), 0, A.gasolina_pasajes), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.hotel), 0, A.hotel), ",", ""),UNSIGNED INTEGER) + CONVERT(REPLACE(IF(IsNull(A.otros), 0, A.otros), ",", ""),UNSIGNED INTEGER))';
                $query = 'SELECT SUM('.$filtro_tipo2.') AS TOTAL, A.fecha_actividad FROM actividades A WHERE A.fecha_actividad LIKE "%'.$filtro_ano.'%"'.$query_user.' GROUP BY A.fecha_actividad ASC';
                $actividades = DB::select($query);

                $query = 'SELECT SUM('.$filtro_tipo.') AS TOTAL, A.mes FROM presupuestos A WHERE A.anio = "'.$filtro_ano.'"'.$query_user.' GROUP BY A.mes ASC';
                $presupuesto = DB::select($query);

                $totalp=0;
                $totalpind=0;
                $totalg=0;
                ob_start(); ?>
<div id="chartdiv-4"></div>
<script>
AmCharts.makeChart("chartdiv-4",
        {
          "type": "serial",
          "legend": {
              "useGraphSettings": true
          },
          "categoryField": "category",
          "autoMarginOffset": 40,
          "marginRight": 60,
          "marginTop": 60,
          "startDuration": 1,
          "fontSize": 13,
          "theme": "default",
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-1",
              "lineThickness": 3,
              "title": "Presupuesto anual",
              "type": "smoothedLine",
              "valueField": "column-1"
            },
            {

              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-2",
              "lineThickness": 3,
              "title": "Gastos según bitácora",
              "type": "smoothedLine",
              "valueField": "column-2"
            },
            {

              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-3",
              "lineThickness": 3,
              "title": "Presupuesto mensual de gastos",
              "type": "smoothedLine",
              "valueField": "column-3"
            },

          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "titles": [],
          "dataProvider": [

                <?php
                for($i=1;$i<=12;$i++){
                    $o=$i<10?"0".$i:$i;
                    foreach ($presupuesto_anual as $key) {
                      if($i==$key->mes){
                        $totalp+=$key->TOTAL;
                      }
                    }
                    foreach ($actividades as $key) {
                        for($k=1;$k<=31;$k++){
                            $k=$k<10?"0".$k:$k;
                            if($filtro_ano."-".$o."-".$k==$key->fecha_actividad){
                                $totalg+=$key->TOTAL;
                            }
                        }
                    }
                    foreach ($presupuesto as $ind) {
                        if($i==$ind->mes){
                            $totalpind+=$ind->TOTAL;
                        }
                    }
                    ?>
                    {
                      "category": "<?=$filtro_ano.'-'.$o?>",
                      "column-1": <?=$totalp?>,
                      "column-2": <?=$totalg?>,
                      "column-3": <?=$totalpind?>
                    },
                    <?php
                }
                ?>
            ]
        }
      );
</script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }
}
