<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoriaArchivo;
use App\ArchivosImportante;
use App\ArchivoComentario;
use Illuminate\Support\Facades\Storage;
use App\User;
use Auth;

date_default_timezone_set("America/Bogota");

class ArchiImportantController extends Controller
{
    public function listAjax(){
        /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Archivos importantes" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){
          $data['item_menu']='menu_'.$data['item_menu'][0]->id;
      }else{
          $data['item_menu']='';
      }
      /*Fin Consulta para activar el item del menu correspondiente*/
        /*Permiso agregar archivo*/
        $modulo = 14;
        $data['permiso_agregar'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="agregar nuevo archivo" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_agregar']='Si';
			  }
            }
          }
        }
        /*Permiso Eliminar archivo*/
        $modulo = 14;
        $data['permiso_eliminar'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="eliminar archivo" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_eliminar']='Si';
			  }
            }
          }
        }
    	$datos = CategoriaArchivo::all();
    	$valid_tags = [];
		foreach ($datos as $tag) {
			$results = DB::select('SELECT COUNT(id) AS archivo FROM `archivos_importantes` WHERE cat_id = :id', ['id' => $tag->id]);
			$tag->archivo = $results[0]->archivo;
			$valid_tags[]=$tag;
		}	
    	return view('archimportant',['datos' => $valid_tags, 'data' => $data]);
    }

    public function eliminar($id){
        $model = ArchivosImportante::find($id);
        Storage::delete($model->file);
        if (isset($model) && !empty($model)) {
            $model->delete();
            return \Response::json(['success' => true]);
        }else{
            return \Response::json(['success' => false]);
        }
    }

    public function getCategoria(){
    	$datos = CategoriaArchivo::all();
    	$valid_tags = [];
		foreach ($datos as $tag) {
			$results = DB::select('SELECT COUNT(id) AS archivo FROM `archivos_importantes` WHERE cat_id = :id', ['id' => $tag->id]);
			$tag->archivo = $results[0]->archivo;
			$valid_tags[]=$tag;
		}
    	return \Response::json(['success' => true, 'datos' => $valid_tags]);
    }

    public function getArchivo($id){
        $cate = CategoriaArchivo::find($id);
    	$datos = ArchivosImportante::where('cat_id', $id)->get();
    	foreach ($datos as $tag) {
			$results = DB::select('SELECT COUNT(id) AS comentarios FROM `archivo_comentarios` WHERE arch_id = :id', ['id' => $tag->id]);
			$tag->comentarios = $results[0]->comentarios;
			$valid_tags[]=$tag;
		}
    	return \Response::json(['success' => true, 'datos' => $datos, 'cate' => $cate]);
    }

    public function getComentarios($id){
    	$datos = ArchivoComentario::where('arch_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		$user = User::findOrFail($value->user_id);
    		$value->user = $user; 
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function save(Request $request)
    {
		$logo = $request->foto_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombreprincipal =$antenombre.".png";
            \Storage::disk('catearchivo')->put($nombreprincipal,  $foto);
        }

	    $dato = new CategoriaArchivo;
		$dato->nombre = $request->nombre;
		if (isset($nombreprincipal)) {
			$dato->foto = $nombreprincipal;
		}
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

    public function saveArchivo(Request $request)
    {
    	$principal = $request->file('file');
		if ($principal) {			
			$antenombreprincipal = uniqid();
			$extensionprincipal = $principal->getClientOriginalExtension();
			$nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
			\Storage::disk('archivosi')->put($nombreprincipal,  \File::get($principal));
		}

	    $dato = new ArchivosImportante;
		$dato->nombre = $request->nombre;
		$dato->comentario = $request->comentario;
		$dato->cat_id = $request->cat_id;
		if (isset($nombreprincipal)) {
			$dato->file = $nombreprincipal;
		}
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

    public function saveComentario(Request $request)
    {
	    $dato = new ArchivoComentario;
		$dato->comentario = $request->comentario;
		$dato->arch_id = $request->arch_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }
}
