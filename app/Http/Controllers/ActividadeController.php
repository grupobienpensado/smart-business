<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\stdClass;
use App\Oportunidades;
use App\Actividades;
use App\Producto;
use App\Pendiente;
use App\Ajuste_gastos;
use App\Presupuesto;
use App\Presupuesto_anual;
use App\ActividadComentario;
use App\User;
use App\Cargos;
use App\HorasHombre;
use App\Tipo_actividades;
use App\Estado_actividade;
use App\Parafiscales;
use App\ActividadesArchivos;
use App\Agenda_clientes_actividade;
use App\Plan_trabajo_descripcion;
use App\AprobacionActividades;
use App\SubtipoActividad;
use App\Empresa;
use App\Configuracion;
use Auth;

date_default_timezone_set("America/Bogota");
class ActividadeController extends Controller
{
    public function action(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*Traer el plan de trabajo*/
            case 0:
                $plan = DB::select("SELECT P.id, P.tipo_actividad, S.concepto, P.observaciones FROM plan_trabajo_descripcions P INNER JOIN subtipo_actividads S ON P.subtipo = S.id WHERE P.id = ".$request->id);
                $data['msj']=$plan[0]->tipo_actividad.'/'.$plan[0]->concepto.'/'.substr($plan[0]->observaciones, 0, 100);
                break;
        }
        return \Response::json(['data' => $data]);
    }
    public function eliminar($id){
        $model = actividades::find($id);
        if (isset($model) && !empty($model)) {
            $model->delete();
            return \Response::json(['success' => true]);
        }else{
            return \Response::json(['success' => false]);
        }
    }

    public function indexCalendar(){
        $dt     = \Carbon\Carbon::now();
        $start  = $dt->startOfWeek('Y-m-d')->format('Y-m-d');
        $end    = $dt->endOfWeek('Y-m-d')->format('Y-m-d');
        $agendavisita = Agenda_clientes_actividade::where("fecha",">=",$start)->where("fecha","<=",$end)->where("user_id",Auth::user()->id)->get();
        $planestrabajo = DB::select("SELECT P.id, P.tipo_actividad, S.concepto, P.observaciones FROM plan_trabajo_descripcions P INNER JOIN subtipo_actividads S ON P.subtipo = S.id WHERE P.fecha BETWEEN :start AND :end AND P.user_id = :id ORDER BY P.oportunidad  DESC", ['id' => Auth::user()->id, 'end' => $end, 'start' => $start]);
        $data['consultaplanestrabajo'] = 'SELECT P.id, P.tipo_actividad, S.concepto, P.observaciones FROM plan_trabajo_descripcions P INNER JOIN subtipo_actividads S ON P.subtipo = S.id WHERE P.fecha BETWEEN '.$start.' AND '.$end.' AND P.user_id = '.Auth::user()->id.' ORDER BY P.oportunidad  DESC';
        ob_start(); ?>
<datalist id="planestrabajo">
        <?php
        foreach($planestrabajo as $plan){
        ?>
<option label="<?=$plan->tipo_actividad?>/<?=$plan->concepto?>/<?=substr($plan->observaciones, 0, 100)?>" value="<?=$plan->id?>"></option>
        <?php
        } ?>
</datalist>
        <?php
        $data['plantrabajoactual'] = ob_get_contents();
        ob_end_clean();
        /*Semana Anterior*/
        $fecha_spasada     = \Carbon\Carbon::now();
        $fecha_spasada  = $fecha_spasada->subDays(7);
        /*$fecha_spasada = date ( 'Y-m-j' , strtotime ( "-7 day" , strtotime ( date("Y-m-j") ) ) );*/
        $start_p  = $fecha_spasada->startOfWeek($fecha_spasada)->format('Y-m-d');
        $end_p    = $fecha_spasada->endOfWeek($fecha_spasada)->format('Y-m-d');
        $planestrabajo_p = DB::select("SELECT P.id, P.tipo_actividad, S.concepto, P.observaciones FROM plan_trabajo_descripcions P INNER JOIN subtipo_actividads S ON P.subtipo = S.id WHERE P.fecha BETWEEN :start_p AND :end_p AND P.user_id = :id ORDER BY P.oportunidad  DESC", ['id' => Auth::user()->id, 'end_p' => $end_p, 'start_p' => $start_p]);
        $data['consultaplanestrabajo_p'] = 'SELECT P.id, P.tipo_actividad, S.concepto, P.observaciones FROM plan_trabajo_descripcions P INNER JOIN subtipo_actividads S ON P.subtipo = S.id WHERE P.fecha BETWEEN '.$start_p.' AND '.$end_p.' AND P.user_id = '.Auth::user()->id.' ORDER BY P.oportunidad  DESC '.$fecha_spasada;
         ob_start(); ?>
<datalist id="planestrabajo">
        <?php
        foreach($planestrabajo_p as $plan){
        ?>
<option label="<?=$plan->tipo_actividad?>/<?=$plan->concepto?>/<?=substr($plan->observaciones, 0, 100)?>" value="<?=$plan->id?>"></option>
        <?php
        } ?>
</datalist>
        <?php
        $data['plantrabajoanterior'] = ob_get_contents();
        ob_end_clean();
        /*Fin Semana Anterior*/
        $modulo=10;
        $permiso_filtro='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $permiso_filtro="Si";
              }else{
                $permiso_filtro="No";
              }
            }
          }
        }

        $permiso_filtro_semana='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select semana" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "Si"){
                $permiso_filtro_semana="Si";
              }else{
                $permiso_filtro_semana="No";
              }
            }
          }
        }

        $permiso_crear_actividad='No';
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear actividad" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Si"){
                $permiso_crear_actividad="Si";
              }else{
                $permiso_crear_actividad="No";
              }
            }
          }
        }

        $permiso_aprobar='No';
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Aprobar actividad" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){
                $permiso_aprobar="Si";
              }else{
                $permiso_aprobar="No";
              }
            }
          }
        }


        $estado_actividades=Estado_actividade::all();
        $cargos=Cargos::all();
        $parafiscal = Parafiscales::findOrFail(1);
        $oportunidades = Oportunidades::where('estado','pendiente')->orWhere('estado','vendido')->get();
        $tipos_actividades=DB::select('SELECT * FROM `tipo_actividades` ORDER BY `orden` ASC');
        /*Eduard: Dias de Bitacora*/
        $dias = Configuracion::find(1);
        $data['dias_antes'] = $dias->valor;
        /*Fin dias de Bitacora*/
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Bitacora" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('bitacora.calendarnew',['data' => $data, 'tipos_actividades' => $tipos_actividades, 'estado_actividades' => $estado_actividades, 'cargos' => $cargos, 'parafiscal' => $parafiscal, 'oportunidades'=>$oportunidades, 'permiso_filtro' => $permiso_filtro, 'permiso_filtro_semana' => $permiso_filtro_semana, 'permiso_crear_actividad' => $permiso_crear_actividad, 'permiso_aprobar'=>$permiso_aprobar, 'agendavisita' => $agendavisita, 'planestrabajo' => $planestrabajo]);
    }

    public function listActividades($id, Request $request){
      $model=Actividades::where('fecha_actividad','>=',$request->start)
                        ->where('fecha_actividad','<=',$request->end)
                        ->when($id, function ($query) use ($id) {
                            return $query->where('user_id', $id)
                                         ->orWhere('responsable_gasto', $id);
                        })->get();
    	$valid_tags = [];
  		foreach ($model as $tag) {
  			try {
      			      $tiempo_inicio   = explode(":",$tag->tiempo_inicio);
                  $tiempo_fin      = explode(":",$tag->tiempo_fin);

                  $start = \Carbon\Carbon::createFromTime($tiempo_inicio[0], $tiempo_inicio[1], 0, 'America/Bogota');
                  $end   = \Carbon\Carbon::createFromTime($tiempo_fin[0], $tiempo_fin[1], 0, 'America/Bogota');


                  if (isset($tag->estado_aprobacion) && !empty($tag->estado_aprobacion)) {
                      if ($tag->estado_aprobacion == "SinRevisar"){
                          $tag->color = "#ea8900";
                      }elseif ($tag->estado_aprobacion == "Aprobado") {
                          $tag->color = "#016090";
                      }else{
                          $tag->color = "#D95350";
                      }
                  }else{
                    $tag->color = "#363C41";
                  }

                  $pendientes = DB::select("SELECT `tipo`, `fecha_ejecucion`, `detalle` FROM `pendientes` WHERE actividad_id = ".$tag->id);
                  $hhombre = DB::select("SELECT `id_user`, `cargo`,`cantidad`,`horas`,`valor` FROM `horas_hombres` WHERE `actividad_id` = ".$tag->id);
                  $archivos =ActividadesArchivos::where("actividad_id",$tag->id)->get();
                  $archivos2 =[];
                  foreach ($archivos as $archivo) {
                      $extension              = explode(".",$archivo->archivo);
                      $extension              = $extension[1];
                      $respaldata = new \stdClass();
                      $respaldata->extension  = $extension;
                      $respaldata->nombre     = $archivo->nombre;
                      $respaldata->archivo    = $archivo->archivo;
                      $archivos2[]            = $respaldata;
                  }

                  $aprobaciones = AprobacionActividades::where('actividad_id', $tag->id)->get();
                  $model = [];
                  foreach ($aprobaciones as $key => $aprobacion) {
                      $user = User::findOrFail($aprobacion->user_id);
                      $aprobacion->user = $user;
                      $model[] = $aprobacion;
                  }

                  if(!empty($tag->oportunidad_id)){
    	              $oportunidad = Oportunidades::find($tag->oportunidad_id);
    	              $tag->oportunidad = $oportunidad->empresa.", ".$oportunidad->pais;
    								$tag->oportunidad_id = $tag->oportunidad_id;
    	            }elseif (!empty($tag->empresa_id)) {
    	              $oportunidad = Empresa::find($tag->empresa_id);
    	              $tag->oportunidad = $oportunidad->nombre;
    								$tag->oportunidad_id = $tag->empresa_id;
    	            }else {
    	              $tag->oportunidad ="";
    								$tag->oportunidad_id = "";
    	            }
    	            // $tag->subtipo = isset($tag->subtipo) ? $tag->subtipo : "";

                  if (!empty($tag->subtipo)) {
                    $subActividad = SubtipoActividad::find($tag->subtipo);
                    $tag->subtipotexto = $subActividad->concepto;
                  }else {
                    $tag->subtipotexto = "";
                  }

      			$valid_tags[] = ['id' => $tag->id, 'subtipo' => $tag->subtipo, 'title' => $tag->tipo_actividad."\n".$tag->subtipotexto, 'start' => $tag->fecha_actividad."T".$start->format('H:i:s'), 'end' => $tag->fecha_actividad."T".$end->format('H:i:s'), 'overlap' => false, 'color' => $tag->color, 'oportunidad_id' => $tag->oportunidad_id, 'oportunidad' => $tag->oportunidad, 'estado_actividad' => $tag->estado_actividad, 'detalle_resultado' => $tag->detalle_resultado, 'detalle_actividad' => $tag->detalle_actividad, 'tipo_actividad' => $tag->tipo_actividad, 'allDay' => false, 'alimentacion'=> $tag->alimentacion, 'transportes_internos'=>$tag->transportes_internos, 'transportes_intermunicipales'=>$tag->transportes_intermunicipales, 'tiquete_aereo' => $tag->tiquete_aereo, 'papeleria' => $tag->papeleria, 'invitacion_cliente' =>$tag->invitacion_cliente, 'alquiler_vehiculo' => $tag->alquiler_vehiculo, 'gasolina_pasajes' => $tag->gasolina_pasajes, 'hotel'=>$tag->hotel, 'otros' => $tag->otros, 'pendientes' => $pendientes, 'hhombre' => $hhombre, 'archivos' => $archivos2, 'responsable_gasto' => $tag->responsable_gasto, "jornada" => $tag->jornada, "id_tipo" => $tag->id_tipo, "origen_datos" => $tag->origen_datos, "created_at" => $tag->created_at, "fuera_tiempo" => $tag->fuera_tiempo, "comentario" => $model];
  			} catch (Exception $e) {

  			}
  		}

  		return response()->json($valid_tags);
      	//return \Response::json(['success' => true, 'model' => $model]);
    }



    public function gastos(){
        $modulo=13;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="detalle de los select USUARIO" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver propio"){
                $mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.Auth::user()->id.'" AND `mes`="'.(int)(date('m')).'" AND `anio` = "'.date('Y').'" ORDER BY `id`');
                $presupuesto = DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.Auth::user()->id.'" AND `anio` = "'.date('Y').'" ORDER BY `id`');
              }else{
                $mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `mes`="'.(int)(date('m')).'" AND `anio` = "'.date('Y').'" ORDER BY `id`');
                $presupuesto = DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.date('Y').'" ORDER BY `id`');
              }
            }
          }
        }
    	$fun=User::findOrFail(Auth::user()->id);
        /*Usuarios que aparecen en el SELECT*/
        $query = 'SELECT C.cargo FROM permiso_modulo_filtros PMF INNER JOIN permiso_cargos C ON PMF.id_cargo = C.id WHERE PMF.deleted_at IS NULL AND PMF.id_permiso_modulo = 13';
        $cargos = DB::SELECT($query);
        $where='';
        if(count($cargos)>0){
            foreach($cargos as $cargo){
                $where.=!empty($where)?' OR cargo = "'.$cargo->cargo.'"':' WHERE cargo = "'.$cargo->cargo.'"';
            }
        }
        /*fin Usuarios que aparecen en el SELECT*/
    	$usuarios_fil=DB::select('SELECT * FROM users'.$where.' ORDER BY id ASC');
        $data['Query']='SELECT * FROM users'.$where.' ORDER BY id ASC';
    	$model = Actividades::all();
    	$lista = Ajuste_gastos::all();
    	$presupuesto_ind = Presupuesto::all();

    	$usuarios = User::all();
    	$ajuste=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.Auth::user()->id.'" AND `mes` = "'.date('m').'" AND `anio`="'.date("Y").'" AND `tipo`="ajuste"');
    	$suma_ajuste=0;
    	if(isset($ajuste[0]->id)){
    		$suma_ajuste=$ajuste[0]->valor_alimentacion+$ajuste[0]->valor_transporte_interno+$ajuste[0]->valor_transporte_intermunicipal+$ajuste[0]->valor_tiquete_aereo+$ajuste[0]->valor_papeleria+$ajuste[0]->valor_invitacion_cliente+$ajuste[0]->valor_alquiler_vehiculo+$ajuste[0]->valor_gasolina_pasaje+$ajuste[0]->valor_hotel+$ajuste[0]->valor_otros;
    	}

    	$ajusteano=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.Auth::user()->id.'" AND `anio`="'.date("Y").'" AND `tipo`="ajuste"');
    	$ajustemeses=[];
    	foreach ($ajusteano as $aa) {
    		for($i=1;$i<=12;$i++){
    			if($i<10){
    				$y='0'.$i;
    			}
    			if($aa->mes==$y){
    				$ajustemeses[$i]=$aa->valor_alimentacion+$aa->valor_transporte_interno+$aa->valor_transporte_intermunicipal+$aa->valor_tiquete_aereo+$aa->valor_papeleria+$aa->valor_invitacion_cliente+$aa->valor_alquiler_vehiculo+$aa->valor_gasolina_pasaje+$aa->valor_hotel+$aa->valor_otros;
    			}

    		}
    	}
        /*Eduard: Es la consulta que me devuelve los registros del ajuste del mes pasasdo*/
        $query = 'SELECT COUNT(*) AS NUMERO FROM ajuste_gastos A WHERE A.mes LIKE "'.date('m',strtotime("-1 month")).'" AND A.anio LIKE "'.date('Y',strtotime("-1 month")).'" AND A.user_id = '.Auth::user()->id;
        $ajuste_mes_pasado = DB::select($query);
        $meses = ["","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $dias_vencidos = strtotime(date('Y-m-')."10")-strtotime(date('Y-m-d'));
        $dias_vencidos = round($dias_vencidos/(60*60*24));
        ob_start();
        if($ajuste_mes_pasado[0]->NUMERO == 0){
            if(intval(date('d')) > 10){ ?>
<div class="alert alert-dark bpx" role="alert">
    <strong>Atención!</strong> Usted no lleno el presupuesto del mes de <?=$meses[intval(date('m',strtotime("-1 month")))]?> que vencio hace <?=$dias_vencidos*(-1)?> dia(s)
</div>
            <?php
            }else{ ?>
<div class="alert bg-warning text-white bpx" role="alert">
    <strong>Advertencia!</strong> Usted no ha llenado el presupuesto del mes de <?=$meses[intval(date('m',strtotime("-1 month")))]?> vence en <?=$dias_vencidos?> dia(s)
</div>
        <?php
            }
        }else{ ?>
<div class="alert bg-success text-white bpx" role="alert">
    <strong>Excelente!</strong> Usted ha llenado el presupuesto del mes de <?=$meses[intval(date('m',strtotime("-1 month")))]?> a tiempo.
</div>
    <?php
        }
        $data['mensaje_ajuste'] = ob_get_contents();
        ob_end_clean();
        /*Fin Comentario Eduard*/

        $permiso_filtro_usuario='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario, gastos" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "Si"){
                $permiso_filtro_usuario="Si";
              }else{
                $permiso_filtro_usuario="No";
              }
            }
          }
        }
        $mes_filtrado=date('m');
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('gastos.gastos2',["data" => $data, "actividades"=>$model,"id"=>Auth::user()->id,"individuales"=>$presupuesto_ind,"ajustes"=>$lista,"anuales"=>$presupuesto,"usuarios"=>$usuarios, "mi_presupuesto"=>$mi_presupuesto, "suma_ajuste"=>$suma_ajuste, 'ajustemeses'=>$ajustemeses, 'fun'=>$fun, 'usuarios'=>$usuarios, 'usuarios_fil'=>$usuarios_fil, 'permiso_filtro_usuario' => $permiso_filtro_usuario, 'mes_filtrado' => $mes_filtrado]);
    }

    public function filtrargastos(Request $request){
    	if($request->usuario!=''){
    		$usuario_f=$request->usuario;
    	}elseif($request->usuario=="sumatoria"){
            $usuario_f="sumatoria";
        }else{
    		$usuario_f=Auth::user()->id;
    	}
    	if($request->mes!=''){
    		$mes_completo_f=$request->mes;
    		$mes_f=date('m', strtotime($request->mes));
    		$ano_f=date('Y', strtotime($request->mes));
    	}else{
    		$mes_completo_f='';
    		$mes_f=date('m');
    		$ano_f=date('Y');
    	}
    	if($request->tipo!=''){
    		$tipo_f=$request->tipo;
    	}else{
    		$tipo_f="";
    	}
        $numero_dias_filtro = cal_days_in_month(CAL_GREGORIAN, $mes_f, $ano_f);
    	$filtros['usuario_f']=$usuario_f;
    	$filtros['mes_completo_f']=$mes_completo_f;
    	$filtros['tipo_f']=$tipo_f;
    	$fun=User::findOrFail(Auth::user()->id);
    	$usuarios_fil=DB::select('SELECT * FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution" OR `cargo` = "Gerente Comercial" ORDER BY `id` ASC');
    	$model = Actividades::all();
    	$lista = Ajuste_gastos::all();
    	$presupuesto_ind = Presupuesto::all();
        if($usuario_f=="sumatoria"){
        	$mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `mes`="'.(int)($mes_f).'" ORDER BY `id`');
        	$presupuesto = DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.$ano_f.'" ORDER BY `id`');
        	$ajuste=DB::select('SELECT * FROM `ajuste_gastos` WHERE `mes` = "'.$mes_f.'" AND `anio`="'.$ano_f.'" AND `tipo`="ajuste"');
        }else{
            $mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$usuario_f.'" AND `mes`="'.(int)($mes_f).'" ORDER BY `id`');
            $presupuesto = DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.$usuario_f.'" AND `anio` = "'.$ano_f.'" ORDER BY `id`');
            $ajuste=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.$usuario_f.'" AND `mes` = "'.$mes_f.'" AND `anio`="'.$ano_f.'" AND `tipo`="ajuste"');
        }
        $usuarios = User::all();
    	$suma_ajuste=0;
    	if(isset($ajuste[0]->id)){
    		if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
		      $suma_ajuste=$ajuste[0]->valor_alimentacion;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
		      $suma_ajuste=$ajuste[0]->valor_transporte_interno;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
		      $suma_ajuste=$ajuste[0]->valor_transporte_intermunicipal;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
		      $suma_ajuste=$ajuste[0]->valor_tiquete_aereo;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
		      $suma_ajuste=$ajuste[0]->valor_papeleria;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
		      $suma_ajuste=$ajuste[0]->valor_invitacion_cliente;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
		      $suma_ajuste=$ajuste[0]->valor_alquiler_vehiculo;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
		      $suma_ajuste=$ajuste[0]->valor_gasolina_pasaje;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
		      $suma_ajuste=$ajuste[0]->valor_hotel;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
		      $suma_ajuste=$ajuste[0]->valor_otros;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
		     $suma_ajuste=$ajuste[0]->valor_alimentacion+$ajuste[0]->valor_transporte_interno+$ajuste[0]->valor_transporte_intermunicipal+$ajuste[0]->valor_tiquete_aereo+$ajuste[0]->valor_papeleria+$ajuste[0]->valor_invitacion_cliente+$ajuste[0]->valor_alquiler_vehiculo+$ajuste[0]->valor_gasolina_pasaje+$ajuste[0]->valor_hotel+$ajuste[0]->valor_otros;
		    }

    	}

        if($usuario_f=="sumatoria"){
    	   $ajusteano=DB::select('SELECT * FROM `ajuste_gastos` WHERE `anio`="'.$ano_f.'" AND `tipo`="ajuste"');
        }else{
           $ajusteano=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.$usuario_f.'" AND `anio`="'.$ano_f.'" AND `tipo`="ajuste"');
        }
    	$ajustemeses=[];
    	foreach ($ajusteano as $aa) {
    		for($i=1;$i<=12;$i++){
    			if($i<10){
    				$y='0'.$i;
    			}
    			if($aa->mes==$y){
    				if(isset($ajuste[0]->id)){
			    		if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
					      $ajustemeses[$i]=$aa->valor_alimentacion;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
					      $ajustemeses[$i]=$aa->valor_transporte_interno;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
					      $ajustemeses[$i]=$aa->valor_transporte_intermunicipal;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
					      $ajustemeses[$i]=$aa->valor_tiquete_aereo;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
					      $ajustemeses[$i]=$aa->valor_papeleria;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
					      $ajustemeses[$i]=$aa->valor_invitacion_cliente;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
					      $ajustemeses[$i]=$aa->valor_alquiler_vehiculo;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
					      $ajustemeses[$i]=$aa->valor_gasolina_pasaje;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
					      $ajustemeses[$i]=$aa->valor_hotel;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
					      $ajustemeses[$i]=$aa->valor_otros;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
					     $ajustemeses[$i]=$aa->valor_alimentacion+$aa->valor_transporte_interno+$aa->valor_transporte_intermunicipal+$aa->valor_tiquete_aereo+$aa->valor_papeleria+$aa->valor_invitacion_cliente+$aa->valor_alquiler_vehiculo+$aa->valor_gasolina_pasaje+$aa->valor_hotel+$aa->valor_otros;
					    }
			    	}
    			}

    		}
    	}

        $modulo=13;
        $permiso_filtro_usuario='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario, gastos" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $permiso_filtro_usuario="Si";
              }else{
                $permiso_filtro_usuario="No";
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('gastos.gastos2',["data" => $data, "actividades"=>$model,"id"=>Auth::user()->id,"individuales"=>$presupuesto_ind,"ajustes"=>$lista,"anuales"=>$presupuesto,"usuarios"=>$usuarios, "mi_presupuesto"=>$mi_presupuesto, "suma_ajuste"=>$suma_ajuste, 'ajustemeses'=>$ajustemeses, 'fun'=>$fun, 'usuarios'=>$usuarios, 'usuarios_fil'=>$usuarios_fil, 'filtros'=>$filtros, 'numero_dias_filtro'=>$numero_dias_filtro, 'permiso_filtro_usuario'=>$permiso_filtro_usuario, 'mes_filtrado' => $mes_f]);
    }

    /**
  	* guarda un archivo en nuestro directorio local.
  	*
  	* @return Response
  	*/

    public function gesAprobar(Request $request)
    {
        $dato = Actividades::find($request->id);
        if (isset($dato) && !empty($dato)) {
            $dato->estado_aprobacion = $request->text;
            $dato->save();

            $aprobador = new AprobacionActividades;
            $aprobador->estado = $request->text;
            $aprobador->detalle = $request->detalleaprobar;
            $aprobador->actividad_id = $dato->id;
            $aprobador->user_id = Auth::user()->id;
            $aprobador->save();

            return \Response::json(['success' => true]);
        }else{
            return \Response::json(['success' => false]);
        }
    }

  	public function editsave(Request $request)
  	{
  		$dato 							= Actividades::find($request->id);
  	    $dato->fecha_actividad  		         = $request->fecha_actividad;
  	    $dato->tiempo_inicio  			         = $request->tiempo_inicio;
  	    $dato->tiempo_fin  				           = $request->tiempo_fin;
  	    $dato->tipo_actividad  			         = $request->tipo_actividad;
        $dato->subtipo                       =$request->subtipo;
    		$dato->empresa_id                    =$request->empresa_id;
  	    $dato->estado_actividad  		         = $request->estado_actividad;
  	    $dato->detalle_actividad  		       = $request->detalle_actividad;
  	    $dato->detalle_resultado  		       = $request->detalle_resultado;
  	    $dato->alimentacion  			           = $request->alimentacion;
  	    $dato->transportes_internos  	 	     = $request->transportes_internos;
  	    $dato->transportes_intermunicipales  = $request->transportes_intermunicipales;
  	    $dato->tiquete_aereo  			         = $request->tiquete_aereo;
  	    $dato->papeleria  				           = $request->papeleria;
  	    $dato->invitacion_cliente  		       = $request->invitacion_cliente;
  	    $dato->alquiler_vehiculo  		       = $request->alquiler_vehiculo;
  	    $dato->gasolina_pasajes  		         = $request->gasolina_pasajes;
  	    $dato->hotel  					             = $request->hotel;
  	    $dato->otros  					             = $request->otros;
  	    $dato->total  					             = $request->total;
        $dato->jornada                       = $request->jornada;
        $dato->id_tipo                       = $request->id_tipo;
        $dato->origen_datos                  = $request->origen_datos;
  	    $dato->oportunidad_id  			         = $request->oportunidad;
  	    $dato->total_h_hombre  			         = $request->total_h_hombre;
  	    $dato->total_general  			         = $request->total_general;
  	    $dato->fuera_tiempo				           = $request->fuera_tiempo;
          if (isset($request->responsable_gasto) && !empty($request->responsable_gasto)) {
              $dato->responsable_gasto    = $request->responsable_gasto;
          }

          $dato->estado_aprobacion = "SinRevisar";
  	    $dato->save();


        if ($request->origen_datos == "P") {
          $plantrabajo = Plan_trabajo_descripcion::find($request->id_tipo);
          if ($request->fecha_actividad != $plantrabajo->fecha) {
            $plantrabajo->plazo = "N";
          }else{
            if ($plantrabajo->jornada != $request->jornada) {
              $plantrabajo->plazo = "N";
            }else{
              $plantrabajo->plazo = "S";
            }
          }
          $plantrabajo->save();
        }

  	    if (isset($request->pendientes) && !empty($request->pendientes)) {

  	    	$models = Pendiente::where('actividad_id', $dato->id)->get();
  		    foreach ($models as $key => $value) {
  		    	$model = Pendiente::find($value->id);
  		      $model->delete();
  		    }

  		    foreach ($request->pendientes as $pendientes) {
  		    	if (isset($pendientes["detalle_pendiente"]) && !empty($pendientes["detalle_pendiente"])) {
              $detalle 					           = new Pendiente;
              $detalle->detalle            = $pendientes["detalle_pendiente"];
              $detalle->subtipo            = $request->subtipo;
              $detalle->empresa_id         = $request->empresa_id;
              $detalle->oportunidad_id  	 = $request->oportunidad;
              $detalle->tipo  	           = $pendientes["tipo_pendiente"];
              $detalle->fecha_ejecucion  	 = $pendientes["fecha_pendiente"];
              $detalle->actividad_id 		   = $dato->id;
              $detalle->estado 			       = "ejecutando";
              $detalle->responsable        = Auth::user()->id;
              $detalle->user_id 		       = Auth::user()->id;
              $detalle->save();
  			    }
  		    }
  	    }

  	    if (isset($request->hhombre) && !empty($request->hhombre)) {

  	    	$models = HorasHombre::where('actividad_id', $dato->id)->get();
  		    foreach ($models as $key => $value) {
  		    	$model = HorasHombre::find($value->id);
  		        $model->delete();
  		    }

  		    foreach ($request->hhombre as $hbre) {
  		    	if ((isset($hbre["cargo"]) && isset($hbre["id_user"])) && (!empty($hbre["cargo"])&& !empty($hbre["id_user"]))) {
  			    	$hhombre 					= new HorasHombre;
  				    $hhombre->cargo  			= $hbre["cargo"];
  				    $hhombre->id_user  			= $hbre["id_user"];
  				    $hhombre->cantidad  		= $hbre["cantidad"];
  				    $hhombre->horas  			= $hbre["horas"];
  				    $hhombre->valor 			= $hbre["valor"];
  				    $hhombre->oportunidad_id = $request->oportunidad;
  				    $hhombre->actividad_id 		= $dato->id;
  				    $hhombre->user_id 			= Auth::user()->id;
  				    $hhombre->save();
  				}else if (isset($hbre["cargo"]) && !empty($hbre["cargo"])) {
  			    	$hhombre 					= new HorasHombre;
  				    $hhombre->cargo  			= $hbre["cargo"];
  				    $hhombre->cantidad  		= $hbre["cantidad"];
  				    $hhombre->horas  			= $hbre["horas"];
  				    $hhombre->valor 			= $hbre["valor"];
  				    $hhombre->oportunidad_id = $request->oportunidad;
  				    $hhombre->actividad_id 		= $dato->id;
  				    $hhombre->user_id 			= Auth::user()->id;
  				    $hhombre->save();
  				}
  		    }
  		}

          $logo = $request->file('archivos');
          if ($logo) {
              for ($i=0;$i<count($logo);$i++) {
                  # code...
                  $antenombrelogo = uniqid();
                  $extensionlogo = $logo[$i]->getClientOriginalExtension();
                  $nombreoriginal = $logo[$i]->getClientOriginalName();
                  $nombrelogo =$antenombrelogo.".".$extensionlogo;
                  \Storage::disk('activity')->put($nombrelogo,  \File::get($logo[$i]));

                  $archi = new ActividadesArchivos;
                  $archi->actividad_id  = $dato->id;
                  $archi->archivo=$nombrelogo;
                  $archi->nombre=$nombreoriginal;
                  $archi->user_id = Auth::user()->id;
                  $archi->save();
              }

          }
  	    //return redirect('bitacora/ver/'.$request->id);
  	    return \Response::json(['success' => true]);
  	}

  	public function save(Request $request){
  	    $dato 							                 = new Actividades;
  	    $dato->fecha_actividad  		         = $request->fecha_actividad;
  	    $dato->tiempo_inicio  			         = $request->tiempo_inicio;
  	    $dato->tiempo_fin  				           = $request->tiempo_fin;
  	    $dato->tipo_actividad  			         = $request->tipo_actividad;
    		$dato->subtipo                       =$request->subtipo;
    		$dato->empresa_id                    =$request->empresa_id;
  	    $dato->estado_actividad  		         = $request->estado_actividad;
  	    $dato->detalle_actividad  	         = $request->detalle_actividad;
  	    $dato->detalle_resultado             = $request->detalle_resultado;
  	    $dato->alimentacion  			           = $request->alimentacion;
  	    $dato->transportes_internos  	       = $request->transportes_internos;
  	    $dato->transportes_intermunicipales  = $request->transportes_intermunicipales;
  	    $dato->tiquete_aereo  			         = $request->tiquete_aereo;
  	    $dato->papeleria  				           = $request->papeleria;
  	    $dato->invitacion_cliente  		       = $request->invitacion_cliente;
  	    $dato->alquiler_vehiculo  		       = $request->alquiler_vehiculo;
  	    $dato->gasolina_pasajes  		         = $request->gasolina_pasajes;
  	    $dato->hotel  					             = $request->hotel;
  	    $dato->otros  					             = $request->otros;
  	    $dato->total  					             = $request->total;
        $dato->jornada                       = $request->jornada;
        $dato->id_tipo                       = $request->id_tipo;
        $dato->origen_datos                  = $request->origen_datos;
  	    $dato->oportunidad_id  			         = $request->oportunidad;
  	    $dato->total_h_hombre  			         = $request->total_h_hombre;
  	    $dato->total_general  			         = $request->total_general;
  	    $dato->fuera_tiempo				           = $request->fuera_tiempo;
  	    $dato->user_id 					             = Auth::user()->id;
        if (isset($request->responsable_gasto) && !empty($request->responsable_gasto)) {
            $dato->responsable_gasto    = $request->responsable_gasto;
        }
  	    $dato->save();

        if ($request->origen_datos == "P") {
          $plantrabajo = Plan_trabajo_descripcion::find($request->id_tipo);
          if ($request->fecha_actividad != $plantrabajo->fecha) {
            $plantrabajo->plazo = "N";
          }else{
            if ($plantrabajo->jornada != $request->jornada) {
              $plantrabajo->plazo = "N";
            }else{
              $plantrabajo->plazo = "S";
            }
          }
          $plantrabajo->save();
        }



  	    foreach ($request->pendientes as $pendientes) {
  	    	if (isset($pendientes["detalle_pendiente"]) && !empty($pendientes["detalle_pendiente"])) {
  		    	$detalle 					           = new Pendiente;
            $detalle->detalle            = $pendientes["detalle_pendiente"];
        		$detalle->subtipo            = $request->subtipo;
            $detalle->empresa_id         = $request->empresa_id;
            $detalle->oportunidad_id  	 = $request->oportunidad;
            $detalle->tipo  	           = $pendientes["tipo_pendiente"];
            $detalle->fecha_ejecucion  	 = $pendientes["fecha_pendiente"];
            $detalle->estado 			       = "ejecutando";
            $detalle->actividad_id 		   = $dato->id;
            $detalle->responsable        = Auth::user()->id;
            $detalle->user_id 		       = Auth::user()->id;
            $detalle->save();
  		    }
  	    }

  	    foreach ($request->hhombre as $hbre) {
  	    	if ((isset($hbre["cargo"]) && isset($hbre["id_user"])) && (!empty($hbre["cargo"])&& !empty($hbre["id_user"]))) {
  		    	$hhombre 					= new HorasHombre;
  			    $hhombre->cargo  			= $hbre["cargo"];
  			    $hhombre->id_user  			= $hbre["id_user"];
  			    $hhombre->cantidad  		= $hbre["cantidad"];
  			    $hhombre->horas  			= $hbre["horas"];
  			    $hhombre->valor 			= $hbre["valor"];
  			    $hhombre->oportunidad_id = $request->oportunidad;
  			    $hhombre->actividad_id 		= $dato->id;
  			    $hhombre->user_id 			= Auth::user()->id;
  			    $hhombre->save();
  			}else if (isset($hbre["cargo"]) && !empty($hbre["cargo"])) {
  		    	$hhombre 					= new HorasHombre;
  			    $hhombre->cargo  			= $hbre["cargo"];
  			    $hhombre->cantidad  		= $hbre["cantidad"];
  			    $hhombre->horas  			= $hbre["horas"];
  			    $hhombre->valor 			= $hbre["valor"];
  			    $hhombre->oportunidad_id = $request->oportunidad;
  			    $hhombre->actividad_id 		= $dato->id;
  			    $hhombre->user_id 			= Auth::user()->id;
  			    $hhombre->save();
  			}
  	    }

          $logo = $request->file('archivos');
          if ($logo) {
              for ($i=0;$i<count($logo);$i++) {
                  # code...
                  $antenombrelogo = uniqid();
                  $extensionlogo = $logo[$i]->getClientOriginalExtension();
                  $nombreoriginal = $logo[$i]->getClientOriginalName();
                  $nombrelogo =$antenombrelogo.".".$extensionlogo;
                  \Storage::disk('activity')->put($nombrelogo,  \File::get($logo[$i]));

                  $archi = new ActividadesArchivos;
                  $archi->actividad_id  = $dato->id;
                  $archi->archivo=$nombrelogo;
                  $archi->nombre=$nombreoriginal;
                  $archi->user_id = Auth::user()->id;
                  $archi->save();
              }

          }

  	    return \Response::json(['success' => true]);
  	}


    public function getComentarios($id){
    	$datos = ActividadComentario::where('actividad_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		$user = User::findOrFail($value->user_id);
    		$value->user = $user;
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function saveComentario(Request $request)
    {
	    $dato = new ActividadComentario;
		$dato->comentario = $request->comentario;
		$dato->actividad_id = $request->actividad_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }
    public function listadogastos(){
    	$lista = Ajuste_gastos::all();
    	$modelo = Actividades::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('gastos.listadogastos',['data' => $data, 'listado' => $lista,'actividades'=>$modelo]);
    }


}
