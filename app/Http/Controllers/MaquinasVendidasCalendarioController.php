<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\OportunidadesAlertas;
use App\OportunidadesAlertasComentarios;
use App\ViewMaquinasvendidas;
use App\ViewMaquinasvendidasOportunidades;
class MaquinasVendidasCalendarioController extends Controller
{
	public function index(){
        /*Permiso para acceder Maquinas Vendidas*/
        $data['permiso_maquinasvendidas']='No';
        $modulo=8;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Acceso Mapa, Calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_maquinasvendidas']='Si';
			  }
            }
          }
        }

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Maquinas vendidas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        if($data['permiso_maquinasvendidas']=='Si'){
            return view('maquinasvendidas.calendarioalertas',['data' => $data]);
        }else{
            return view('oportunidades.nopermiso');
        }
    }

    public function action(Request $request){
        /*Permiso Obtener datos propios o ver toda la informacion de maquinas vendidas*/
        $permiso_maquinasvendidas_datos='Ver todas';
        $modulo=8;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Datos Propios Mapa, Calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Ver propio"){
				$permiso_maquinasvendidas_datos='Ver propio';
			  }
            }
          }
        }
        $data['permiso']=$permiso_maquinasvendidas_datos;
        switch($request->action){
            /*Listado de productos*/
            case 0:
                $query = 'SELECT PRODUCTO, PRODUCTOIMAGEN FROM view_maquinasvendidas UNION SELECT PRODUCTO, PRODUCTOIMAGEN FROM view_maquinasvendidas_oportunidades ORDER BY PRODUCTO ASC';
                $productos = DB::SELECT($query);
                $maquinasvendidas1 = DB::SELECT('SELECT * FROM view_maquinasvendidas');
                $maquinasvendidas2 = DB::SELECT('SELECT * FROM view_maquinasvendidas_oportunidades');
                $maquinasvendidas = array_merge($maquinasvendidas1, $maquinasvendidas2);
                ob_start();
                $data['total'] = 0;
                foreach($productos as $producto){
                    /*Aplicar permiso*/
                    //Where para sacar el total año actual MVD(Maquinas vendidas datos). Solo para "Ver propio"
                    $where_MVD = '';
                    //Where para sacar el total numero de alertas NA(NUMERO ALERTAS). Solo para "Ver propio"
                    $where_NA = '';
                    $where_NA2 = '';
                    if($permiso_maquinasvendidas_datos == "Ver propio"){
                        $query = 'SELECT IFNULL(SUM(OP.cantidad),0) FROM oportunidad_productos OP INNER JOIN productos P ON OP.producto = P.id WHERE OP.deleted_at IS NULL AND (SELECT HO.value FROM historial_oportunidades HO WHERE HO.oportunidad_id = OP.oportunidad_id AND HO.key LIKE "responsable" ORDER BY HO.id DESC LIMIT 0,1) = '.Auth::user()->id.' AND (SELECT HO.value FROM historial_oportunidades HO WHERE HO.oportunidad_id = OP.oportunidad_id AND HO.key LIKE "ciclo_venta" ORDER BY HO.id DESC LIMIT 0,1) = 90 AND P.name LIKE "'.$producto->PRODUCTO.'"';
                        $query1 = 'SELECT COUNT(MV.id) FROM maquinas_vendidas MV INNER JOIN productos P ON MV.producto = P.id WHERE MV.vendedor_id = '.Auth::user()->id.' AND P.name LIKE "'.$producto->PRODUCTO.'"';
                        $query2 = 'SELECT ('.$query.') AS CANTIDAD, ('.$query1.') AS CANTIDAD1';
                        $cantidad_total_vendedor = DB::SELECT($query2);
                        $numero = $cantidad_total_vendedor[0]->CANTIDAD + $cantidad_total_vendedor[0]->CANTIDAD1;
                        $where_MVD = ' AND (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "responsable" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) = '.Auth::user()->id;
                        $where_NA = ' AND (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "responsable" AND H.oportunidad_id = OP.oportunidad_id ORDER BY H.id DESC LIMIT 0,1) = '.Auth::user()->id;
                        $where_NA2 = ' AND MV.vendedor_id = '.Auth::user()->id;
                    }else{
                        $numero = 0;
                        foreach($maquinasvendidas as $venta){
                            $numero+=$producto->PRODUCTO==$venta->PRODUCTO?$venta->CANTIDAD_VENDIDA:0;
                        }
                    }
                    if($numero > 0){
                        $data['total']+=$numero;
                        /*Buscar el numero de vendedores*/
                        $query = 'SELECT COUNT(DISTINCT(V.IDRESPONSABLE)) AS VENDEDORES FROM oportunidad_productos OP INNER JOIN view_oportunidades V ON OP.oportunidad_id = V.id WHERE (SELECT P.id FROM productos P WHERE P.name LIKE "'.$producto->PRODUCTO.'" LIMIT 0,1) = OP.producto AND V.CICLO = 90';
                        $vendedores = DB::SELECT($query);
                        /*Numero de alertas*/
                        $query = 'SELECT COUNT(A.id) AS ALERTAS FROM oportunidades_alertas A INNER JOIN oportunidad_productos OP ON A.id_oportunidad_producto = OP.id WHERE OP.producto = (SELECT P.id FROM productos P WHERE P.name LIKE "'.$producto->PRODUCTO.'" LIMIT 0,1) AND A.deleted_at IS NULL'.$where_NA;
                        $alertas_oportunidades = DB::SELECT($query);
                        $query = 'SELECT COUNT(A.id) AS ALERTAS FROM oportunidades_alertas A INNER JOIN maquinas_vendidas MV ON A.id_maquinas_vendidas = MV.id WHERE MV.producto = (SELECT P.id FROM productos P WHERE P.name LIKE "'.$producto->PRODUCTO.'" LIMIT 0,1) AND A.deleted_at IS NULL'.$where_NA2;
                        $alertas_maquinavendidas = DB::SELECT($query);
                        $total_alertas = $alertas_oportunidades[0]->ALERTAS + $alertas_maquinavendidas[0]->ALERTAS;
                ?>
<div class="col-md-3 mt-3">
    <figure class="snip1477 bg-producto">
        <img src="<?=$producto->PRODUCTOIMAGEN?>" alt="sample38" />
        <div class="title">
            <div>
                <h2><?=$producto->PRODUCTO?></h2>
                <h4><?=$numero?> Equipos</h4>
            </div>
        </div>
        <figcaption>
           <?php if($permiso_maquinasvendidas_datos == "Ver todas"){ ?>
            <p class="text-white">Vendedores: (<?=$vendedores[0]->VENDEDORES?>) <br>Alertas: (<?=$total_alertas?>)</p>
            <?php }else{ ?>
            <p class="text-white">Alertas: (<?=$total_alertas?>)</p>
            <?php } ?>
        </figcaption>
        <a href="#" onclick="abrir_producto('<?=$producto->PRODUCTO?>')"></a>
    </figure>
</div>
                <?php
            }
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                $query = 'SELECT SUM(P.cantidad) AS TOTAL FROM oportunidades O INNER JOIN oportunidad_productos P ON O.id = P.oportunidad_id WHERE (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "ciclo_venta" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) = 90 AND (SELECT YEAR(H.value) FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) = '.date('Y').' AND P.deleted_at IS NULL'.$where_MVD;
                $total_ano_actual = DB::SELECT($query);
                $data['total_ano_actual'] = $total_ano_actual[0]->TOTAL;
                $data['total_ano_anterior'] = $data['total'] - $data['total_ano_actual'];
                break;
            /*Listado de productos vendidos*/
            case 1:
            /*Permiso crear alertas Equipos vendidos*/
            $data['permiso_crear_alerta']='No';
            $modulo=8;
            $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario - Crear Alerta" AND `id_permisomodulo`="'.$modulo.'"');
            if(isset($acceso1[0]->id)){
              $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
              if(isset($cargo1[0]->id)){
                $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
                if(isset($permiso1[0]->permiso)){
                  if($permiso1[0]->permiso == "Si"){
                    $data['permiso_crear_alerta']='Si';
                  }
                }
              }
            }
            /*Aplicar permiso*/
            $where = '';
            $Where2 = '';
            if($permiso_maquinasvendidas_datos == "Ver propio"){
                /*Condición Para filtrar por usuario*/
                $where = ' AND V.IDRESPONSABLE = '.Auth::user()->id;
                $Where2 = ' AND MV.vendedor_id = '.Auth::user()->id;
            }
                $query = 'SELECT OP.id AS ID, 0 AS TIPO, V.empresa AS EMPRESA, IFNULL(V.EMPRESALOGO,"https://placeholdit.imgix.net/~text?txtsize=8&txt=Sin%20Logo&w=50&h=50") AS EMPRESALOGO, V.CORTORESPONSABLE AS VENDEDOR, (SELECT U.foto FROM users U WHERE U.id = V.IDRESPONSABLE) AS FOTOVENDEDOR, (SELECT P.name FROM productos P WHERE P.id = OP.producto) AS PRODUCTO, (SELECT R.referencia FROM producto_referencias R WHERE R.id = OP.referencia) AS REFERENCIA, V.pais AS PAIS, YEAR(V.FECHACIERRE) AS ANO, (SELECT COUNT(A.id) FROM oportunidades_alertas A WHERE A.id_oportunidad_producto = OP.id AND A.tipo = 0 AND A.deleted_at IS NULL) AS ALERTAS FROM oportunidad_productos OP INNER JOIN view_oportunidades V ON V.id = OP.oportunidad_id WHERE V.CICLO = 90 AND OP.deleted_at IS NULL'.$where;
                $maquinas_vendidas_oportunidades = DB::SELECT($query);
                $query = 'SELECT MV.id AS ID, 1 AS TIPO, (SELECT E.nombre FROM empresas E WHERE E.id = MV.empresa) AS EMPRESA, (SELECT IF(E.logo IS NULL, CONCAT("/images/file/empresas/principal/",E.logo), "https://placeholdit.imgix.net/~text?txtsize=8&txt=Sin%20Logo&w=50&h=50") FROM empresas E WHERE E.id = MV.empresa) AS EMPRESALOGO, (SELECT CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) FROM users U WHERE U.id = MV.vendedor_id) AS VENDEDOR, (SELECT U.foto FROM users U WHERE U.id = MV.vendedor_id) AS FOTOVENDEDOR, (SELECT P.name FROM productos P WHERE P.id = MV.producto) AS PRODUCTO, PR.referencia AS REFERENCIA, IFNULL((SELECT S.pais FROM empresa_sedes S WHERE S.id = MV.sede),"Sin Pais") AS PAIS, MV.ano_venta AS ANO, (SELECT COUNT(A.id) FROM oportunidades_alertas A WHERE A.id_maquinas_vendidas = MV.id AND A.tipo = 1 AND A.deleted_at IS NULL) AS ALERTAS FROM maquinas_vendidas MV INNER JOIN producto_referencias PR ON MV.referencia = PR.id'.$Where2;
                $maquinas_vendidas = DB::SELECT($query);
                $data['maquinas_vendidas'] = array_merge($maquinas_vendidas_oportunidades, $maquinas_vendidas);
                break;
            /*Crear o Editar Alerta*/
            case 2:
                if($request->id_alerta == ''){
                    $dato = new OportunidadesAlertas;
                }else{
                    $dato = OportunidadesAlertas::find($request->id_alerta);
                }
                foreach($request->formulario as $index=>$value){
                    if($value != ""){
                        $dato->$index=$value;
                    }
                }
                if($dato->save()){
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar";
                }
                break;
            /*Listado de Alertas*/
            case 3:
                if($request->tipo == 0){
                    $where = 'A.tipo = 0 AND A.id_oportunidad_producto = '.$request->id.' AND A.deleted_at IS NULL';
                }else{
                    $where = 'A.tipo = 1 AND A.id_maquinas_vendidas = '.$request->id.' AND A.deleted_at IS NULL';
                }
                $query = 'SELECT A.id, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) AS CREADOR, IFNULL(CONCAT("/images/file/clientes/",U.foto),"https://placeholdit.imgix.net/~text?txtsize=8&txt=Sin%20Foto&w=50&h=50") AS FOTOCREADOR, A.created_at, A.titulo, A.descripcion, A.fecha_ejecucion FROM oportunidades_alertas A INNER JOIN users U ON A.id_user_creador = U.id WHERE '.$where;
                $alertas = DB::SELECT($query);
                ob_start();
                ?>
<div class="row d-flex justify-content-center">
    <?php foreach($alertas as $alerta){ ?>
    <div class="col-md-4 mt-3">
        <div class="content">
            <div class="row my-2 bg-white sombra" style="width:100%">
                <div class="col-md-12 mt-4">
                    <img src="/images/file/clientes/essi_admon.jpg" class="img-avatar">
                </div>
                <div class="col-md-12 mt-3">
                    <p class="h5">Creado por:<br><strong><?=$alerta->CREADOR?></strong></p>
                </div>
                <div class="col-md-12">
                    <hr class="my-1">
                    <p class="h6 text-success"><strong>Creado el dia:</strong></p>
                    <p class="h5 text-success"><?=$this->fecha_hora($alerta->created_at)?></p>
                    <hr class="my-1">
                </div>
                <div class="col-md-12">
                    <p class="h6"><strong>Titulo:</strong></p>
                    <p class="h5"><?=$alerta->titulo?></p>
                </div>
                <div class="col-md-12">
                    <p class="h6"><strong>Descripción:</strong></p>
                    <p class="h5"><?=$alerta->descripcion?></p>
                </div>
                <div class="col-md-12 mb-2">
                    <hr class="my-1">
                    <p class="h6 text-danger"><strong>El dia:</strong></p>
                    <p class="h5 text-danger"><i class="fa fa-calendar"></i> <?=$this->fecha($alerta->fecha_ejecucion)?></p>
                    <hr class="my-1">
                </div>
                <div class="col-md-12 btn-acciones mb-4">
                    <a class="text-warning" title="Editar Alerta" onclick="accion_alerta('editar',<?=$alerta->id?>)"><i class="fa fa-pencil"></i></a>
                    <a class="text-danger" title="Eliminar Alerta" onclick="accion_alerta('eliminar',<?=$alerta->id?>)"><i class="fa fa-trash"></i></a>
                    <a class="text-info" title="Ver Comentarios" onclick="accion_alerta('comentario',<?=$alerta->id?>)"><i class="fa fa-comment"></i></a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Datos de la alerta a Editar*/
            case 4:
                $data['alerta'] = OportunidadesAlertas::find($request->id);
                break;
            /*Eliminar alerta*/
            case 5:
                $dato = OportunidadesAlertas::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                    $data['tipo'] = $dato->tipo;
                    $data['id'] = $dato->tipo==0?$dato->id_oportunidad_producto:$dato->id_maquinas_vendidas;
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            /*Buscar alertas*/
            case 6:
                /*Aplicar permiso*/
                $where_NA ='';
                $where_NA2 = '';
                if($permiso_maquinasvendidas_datos == "Ver propio"){
                    $where_NA = ' AND (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "responsable" AND H.oportunidad_id = OP.oportunidad_id ORDER BY H.id DESC LIMIT 0,1) = '.Auth::user()->id;
                    $where_NA2 = 'AND MV.vendedor_id = '.Auth::user()->id;
                }
                $query = 'SELECT A.* FROM oportunidades_alertas A INNER JOIN oportunidad_productos OP ON A.id_oportunidad_producto = OP.id WHERE A.deleted_at IS NULL AND A.tipo = 0 AND OP.producto = (SELECT P.id FROM productos P WHERE P.name LIKE "'.$request->producto.'" LIMIT 0,1)'.$where_NA;
                $alertas_oportunidades = DB::SELECT($query);
                $query = 'SELECT A.* FROM oportunidades_alertas A INNER JOIN maquinas_vendidas MV ON A.id_maquinas_vendidas = MV.id WHERE A.deleted_at IS NULL AND A.tipo = 1 AND MV.producto = (SELECT P1.id FROM productos P1 WHERE P1.name LIKE "'.$request->producto.'" LIMIT 0,1)'.$where_NA2;
                $alertas_maquinasvendidas = DB::SELECT($query);
                $data['alertas'] = array_merge($alertas_oportunidades, $alertas_maquinasvendidas);
                break;
            /*Listar alertas con click en el calendario*/
            case 7:
                $query = 'SELECT A.id, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) AS CREADOR, IFNULL(CONCAT("/images/file/clientes/",U.foto),"https://placeholdit.imgix.net/~text?txtsize=8&txt=Sin%20Foto&w=50&h=50") AS FOTOCREADOR, A.created_at, A.titulo, A.descripcion, A.fecha_ejecucion FROM oportunidades_alertas A INNER JOIN users U ON A.id_user_creador = U.id WHERE A.deleted_at IS NULL AND A.fecha_ejecucion = "'.$request->fecha.'"';
                $alertas = DB::SELECT($query);
                ob_start();
                ?>
<div class="row d-flex justify-content-center">
    <?php foreach($alertas as $alerta){
                    /*Verificar si la alerta pertenece al producto seleccionado*/
                    $query = 'SELECT (SELECT COUNT(A.id) FROM oportunidades_alertas A INNER JOIN oportunidad_productos OP ON A.id_oportunidad_producto = OP.id WHERE A.id = '.$alerta->id.' AND OP.producto = (SELECT P.id FROM productos P WHERE P.name = "'.$request->producto.'" LIMIT 0,1)) AS CONTAR, (SELECT COUNT(A.id) FROM oportunidades_alertas A INNER JOIN maquinas_vendidas MV ON A.id_maquinas_vendidas = MV.id WHERE A.id = '.$alerta->id.' AND MV.producto = (SELECT P.id FROM productos P WHERE P.name = "'.$request->producto.'" LIMIT 0,1)) AS CONTAR1';
                    $contar = DB::SELECT($query);
                    if($contar[0]->CONTAR > 0 || $contar[0]->CONTAR1 > 0){
    ?>
    <div class="col-md-4 mt-3">
        <div class="content">
            <div class="row my-2 bg-white sombra" style="width:100%">
                <div class="col-md-12 mt-4">
                    <img src="/images/file/clientes/essi_admon.jpg" class="img-avatar">
                </div>
                <div class="col-md-12 mt-3">
                    <p class="h5">Creado por:<br><strong><?=$alerta->CREADOR?></strong></p>
                </div>
                <div class="col-md-12">
                    <hr class="my-1">
                    <p class="h6 text-success"><strong>Creado el dia:</strong></p>
                    <p class="h5 text-success"><?=$this->fecha_hora($alerta->created_at)?></p>
                    <hr class="my-1">
                </div>
                <div class="col-md-12">
                    <p class="h6"><strong>Titulo:</strong></p>
                    <p class="h5"><?=$alerta->titulo?></p>
                </div>
                <div class="col-md-12">
                    <p class="h6"><strong>Descripción:</strong></p>
                    <p class="h5"><?=$alerta->descripcion?></p>
                </div>
                <div class="col-md-12 mb-2">
                    <hr class="my-1">
                    <p class="h6 text-danger"><strong>El dia:</strong></p>
                    <p class="h5 text-danger"><i class="fa fa-calendar"></i> <?=$this->fecha($alerta->fecha_ejecucion)?></p>
                    <hr class="my-1">
                </div>
                <div class="col-md-12 btn-acciones mb-4">
                    <a class="text-warning" title="Editar Alerta" onclick="accion_alerta('editar',<?=$alerta->id?>)"><i class="fa fa-pencil"></i></a>
                    <a class="text-danger" title="Eliminar Alerta" onclick="accion_alerta('eliminar',<?=$alerta->id?>)"><i class="fa fa-trash"></i></a>
                    <a class="text-info" title="Ver Comentarios" onclick="accion_alerta('comentario',<?=$alerta->id?>)"><i class="fa fa-comment"></i></a>
                </div>
            </div>
        </div>
    </div>
    <?php } } ?>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Listado de Comentarios*/
            case 8:
                $query = 'SELECT C.*, (SELECT CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) FROM users U WHERE U.id = C.id_user_creador) AS CREADOR, (SELECT CONCAT("/images/file/clientes/",U.foto) FROM users U WHERE U.id = C.id_user_creador) AS FOTOCREADOR FROM oportunidades_alertas_comentarios C INNER JOIN oportunidades_alertas A ON C.id_oportunidades_alertas = A.id WHERE C.deleted_at IS NULL AND A.id = '.$request->id;
                $comentarios = DB::SELECT($query);
                ob_start();
                foreach($comentarios as $comentario){
                ?>
<div class="row">
    <div class="col-md-4"><img src="<?=$comentario->FOTOCREADOR?>" class="img-avatar">
        <p class="h6 bg-info text-white" style="border-radius: 10px 10px 10px 10px;-moz-border-radius: 10px 10px 10px 10px;-webkit-border-radius: 10px 10px 10px 10px;border: 0px solid #000000;"><a><?=$comentario->CREADOR?><br><?=$this->fecha_hora($comentario->created_at)?></a></p>
    </div>
    <div class="col-md-8">
        <p class="h4 text-left"><?=$comentario->comentario?></p>
    </div>
</div>
                <?php
                }
                if(count($comentarios)==0){
                    ?>
<div class="row">
    <div class="col-md-12">
        <p class="h5"><strong><em>No hay ningun comentario!</em></strong></p>
    </div>
</div>
                    <?php
                }
                ?>
<div class="row">
    <div class="col-md-12">
        <textarea class="form-control" id="comentario_alerta" rows="4">Comentario.....</textarea>
    </div>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Guardar comentario*/
            case 9:
                $dato = new OportunidadesAlertasComentarios;
                $dato->comentario = $request->comentario;
                $dato->id_oportunidades_alertas = $request->id;
                $dato->id_user_creador = Auth::user()->id;
                if($dato->save()){
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar";
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }
    protected function fecha($fechasinformato){
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $f = explode('-',$fechasinformato);
        $fechaconformato = $f[2].' de '.$meses[intval($f[1])].' del '.$f[0];
        return $fechaconformato;
    }
    function fecha_hora($f){
        $meses = array("","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $fecha_hora = explode(' ',$f);
        $fecha = explode('-',$fecha_hora[0]);
        $hora = explode(':',$fecha_hora[1]);
        if(intval($hora[0])>12){
           $hora[0] = intval($hora[0])-12;
           $jornada = "P.M";
       }else{
           $jornada = "A.M";
       }
        $fecha_final = $fecha[2].' de '.$meses[intval($fecha[1])].' '.$fecha[0].' '.$hora[0].':'.$hora[1].':'.$hora[2].' '.$jornada;
        return $fecha_final;
    }
}
