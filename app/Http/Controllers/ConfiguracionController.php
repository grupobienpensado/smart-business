<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use App\Metas_oportunidade;
use App\Participacion_probabilidade;
use Auth;
class ConfiguracionController extends Controller
{
    //
    public function save(Request $request){
    	if(isset($request->id)){
    		$datos=Metas_oportunidade::findOrFail($request->id);
    	}else{
    		$datos=new Metas_oportunidade();
    	}
    		$datos->corto_dias=$request->corto_dias;
    		$datos->corto_valor=$request->corto_valor;
    		$datos->medio_dias=$request->medio_dias;
    		$datos->medio_valor=$request->medio_valor;
    		$datos->largo_dias=$request->largo_dias;
    		$datos->largo_valor=$request->largo_valor;
    		$datos->user_id=Auth::user()->id;
    		$datos->save();
    	
    	if(isset($request->id_2)){
    		$dato=Participacion_probabilidade::findOrFail($request->id_2);
    	}else{
    		$dato=new Participacion_probabilidade();
    	}
    	$dato->alta=$request->alta;
    	$dato->media=$request->media;
    	$dato->baja=$request->baja;
    	$dato->user_id=Auth::user()->id;
    	$dato->save();

    	return redirect("ciclos");
    	
    }
}
