<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Competencia;
use App\RedesSociale;
use App\CompetenciasFuncionario;
use App\CompetenciasComentario;
use App\CompetenciasRedesSociale;
use App\CompetenciasSede;
use Auth;


class CompetenciasController extends Controller
{
    /**
     * Devuelve la vista inicial de competencias.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        /*Permiso para crear Competencia*/
        $modulo=23;
        $data['permiso_crear']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_crear']="Si";
              }
            }
          }
        }
        /*Permiso para Editar Competencia*/
        $modulo=23;
        $data['permiso_editar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Editar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_editar']="Si";
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Competencias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('competencias.index', ["data" => $data]);
    }

    public function VerCompetencia($id){
        $competencias = Competencia::find($id);
        $flag_principal =  CompetenciasSede::where(['id_competencia' => $id, 'sede_principal' => 1])->get();
        $data['id_competencia'] = $id;
        $data['perfil'] = $competencias;
        $data['flag'] = (isset($flag_principal[0]->sede_principal))? 1 : 0;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('competencias.competencia', ["data" => $data]);
    }

    /**
     * Listado de competencias de cards.
     *
     * @without parameters
     * @return cards wrappers
     */
    public function ListarCompetencias(){
        /*Permiso para Editar*/
        $modulo=23;
        $data['permiso_editar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Editar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_editar']="Si";
              }
            }
          }
        }
        /*Permiso para Eliminar*/
        $data['permiso_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_eliminar']="Si";
              }
            }
          }
        }
        $competencias = Competencia::all();

        ob_start();
        foreach ($competencias as $row) {
            $banner = ($row->banner)? url('/').'/storage/competencias_files/profile_images/'.$row->banner : "http://subtlepatterns.com/patterns/geometry2.png";
            $avatar = ($row->logo)? url('/').'/storage/competencias_files/profile_images/'.$row->logo : "http://via.placeholder.com/250x250";
    ?>
    <div class="col-md-4 mt-5">
        <div class="card">
            <div class="cardheader" style="background-image: url('<?php echo $banner; ?>')">
                <div class="pull-right p-4">
                    <div class="dropdown">
                        <a class="fa fa-bars fa-2x dropdown-toggle text-info" aria-hidden="true" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item action-see" href="<?php echo url("/")."/competencias/ver/{$row->id}"; ?>" data-cid="<?php echo $row->id; ?>"><i class="fa fa-eye mr-3 text-info"></i>Ver</a>
                            <a class="dropdown-item <?php if($data['permiso_editar']=="Si"){ ?>action-edit<?php } ?>" href="#" <?php if($data['permiso_editar']=="No"){ ?>onclick="no_permiso('Usted no tiene permisos para editar una competencia')"<?php } ?> data-cid="<?php echo $row->id; ?>"><i class="fa fa-pencil mr-3 text-warning"></i>Editar</a>
                            <a class="dropdown-item <?php if($data['permiso_eliminar']=="Si"){ ?>action-del<?php } ?>" href="#" <?php if($data['permiso_eliminar']=="No"){ ?>onclick="no_permiso('Usted no tiene permisos para eliminar una competencia')"<?php } ?> data-cid="<?php echo $row->id; ?>"><i class="fa fa-trash mr-3 text-danger"></i>Eliminar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="avatar">
                <img alt="" src="<?php echo $avatar; ?>" class="img-avatar-card">
            </div>
            <div class="container">
                <h3 class="card-title mt-0">
                    <?php echo $row->nombre; ?>
                </h3>
                <h4 class="text-muted text-center">Santander / Colombia</h4>
                <div class="row mt-4">
                    <div class="col-md-3"><span class="font-weight-bold essi-color-text fa-lg">NIT</span><br>
                        <?php echo $row->nit; ?>
                    </div>
                    <div class="col-md-4"><span class="fa fa-phone mr-2 essi-color-text fa-lg fa-3" aria-hidden="true"></span><br>
                        <?php echo $row->telefono_contacto; ?>
                    </div>
                    <div class="col-md-5"><span class="fa fa-envelope mr-2 essi-color-text fa-lg" aria-hidden="true"></span><br>
                        <?php echo $row->correo_contacto; ?>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h3>Descripción</h3>
                        <div class="container text-left p-2">
                            <?php echo $row->descripcion; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Redes sociales</h3>
                        <div class="container p-2">
                            <ul class="social-network social-circle">
                                <?php
                                 $social_query = "Select competencias_redes_sociales.*, competencias_redes_sociales.id_competencia, redes_sociales.nombre, redes_sociales.font_awesome From competencias_redes_sociales left Join redes_sociales On competencias_redes_sociales.id_red_social = redes_sociales.id Where competencias_redes_sociales.id_competencia = {$row->id}";
                                 $r_socials = DB::select($social_query);
                                 foreach($r_socials as $row2){ ?>
                                <li><a href="<?php echo $row2->perfil; ?>" class="red" title="<?php echo $row2->nombre; ?>"><?php echo $row2->font_awesome; ?></a></li>
                                <?php  } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="comp-bottom-separator">
        </div>
    </div>

    <?php }
        $comp_data = ob_get_contents();
        ob_end_clean();
        if($competencias){
            return \Response::json(['res' => 1, 'data' => $comp_data]);
        }else{
            return \Response::json(['res' => 0, 'msj' => "No hay datos de competencias"]);
        }
    }

      /**
     * Acciones para las competencias.
     *
     * @action int      $action[1-crear, 2-editar, 3-eliminar]
     * @id     int      $id_c
     * @return Response flag, msj, data
     */
    public function AccionesCompetencias(Request $request){
        if(isset($request->action) || isset($request->c_data)){
            $action = intval($request->action);
            $cid = $request->id;
            /*Crear*/
            if($action === 1 && !$cid){
                $compe = new Competencia;
                $compe->nombre = $request->nombre;
                $compe->descripcion = $request->detail;
                $compe->nit = $request->nit;
                $compe->fecha_fundacion = $request->fecha_fundacion;
                $compe->pagina_web = $request->pagina_web;
                $compe->correo_contacto = $request->correo_contacto;
                $compe->telefono_contacto = $request->telefono_contacto;

                if(isset($request->c_banner_values)){
                    $compe->banner = $this->ProcesarImagenes($request->c_banner_values);
                }
                if(isset($request->c_logo_values)){
                    $compe->logo = $this->ProcesarImagenes($request->c_logo_values);
                }
                if($compe->save()){
                    if(isset($request->socials) && isset($request->socials_link)){
                        $compentecia_id = $compe->id;
                    for($i = 0; $i < count($request->socials_link); $i++){
                        $social = new CompetenciasRedesSociale;
                        $social->id_competencia = $compentecia_id;
                        $social->id_red_social = $request->socials[$i];
                        $social->perfil = $request->socials_link[$i];
                        $social->save();
                    }
                        return \Response::json(['res' => 1, 'msj' => "Guardado correctamente"]);
                }
                }
            }
            /*Editar*/
            if($action === 2 && $cid){
                $compe = Competencia::findOrFail($cid);
                if(isset($request->nombre)){
                    $compe->nombre = $request->nombre;
                }
                if(isset($request->descripcion)){
                    $compe->descripcion = $request->detail;
                }

                if(isset($request->nit)){
                    $compe->nit = $request->nit;
                }
                if(isset($request->fecha_fundacion)){
                    $compe->fecha_fundacion = $request->fecha_fundacion;
                }
                if(isset($request->pagina_web)){
                    $compe->pagina_web = $request->pagina_web;
                }
                if(isset($request->correo_contacto)){
                    $compe->correo_contacto = $request->correo_contacto;
                }
                if(isset($request->telefono_contacto)){
                    $compe->telefono_contacto = $request->telefono_contacto;
                }
                if(isset($request->c_banner_values)){
                    $compe->banner = $this->ProcesarImagenes($request->c_banner_values);
                }
                if(isset($request->c_logo_values)){
                    $compe->logo = $this->ProcesarImagenes($request->c_logo_values);
                }
                if($compe->save()){
                    if(isset($request->socials) && isset($request->socials_link)){
                        $compentecia_id = $compe->id;
                    for($j = 0; $j < count($request->socials_link); $j++){
                        if(isset($request->id_red[$j])){
                            try{
                                //dd($request->id_red[$j],$request->socials_link);
                                $v=true;
                                try{
                                    $social_e = CompetenciasRedesSociale::find($request->id_red[$j]);
                                    if(is_null($social_e)){
                                        $v=false;
                                    }
                                }catch(\Exception $e){
                                    $v=false;
                                }
                                if($v){
                                    $social_e->id_competencia = $compentecia_id;
                                    $social_e->id_red_social = $request->socials[$j];
                                    $social_e->perfil = $request->socials_link[$j];
                                    $social_e->save();
                                }
                            }catch(\Exception $e){
                                dd($e);
                            }
                        }else{

                            $social_n = new CompetenciasRedesSociale;
                            $social_n->id_competencia = $compentecia_id;
                            $social_n->id_red_social = $request->socials[$j];
                            $social_n->perfil = $request->socials_link[$j];
                            $social_n->save();
                        }
                    }
                        return \Response::json(['res' => 1, 'msj' => "Guardado correctamente"]);
                }
                }
            }
            /*Eliminar*/
            if($action === 3 && $cid){
                $comp_del = Competencia::find($cid);
                Storage::delete("competencias/{$comp_del->logo}");
                if($comp_del->delete()){
                    return \Response::json(['res' => 1, 'msj' => "Eliminado correctamente"]);
                }else{
                    return \Response::json(['res' => 0, 'msj' => "No sepudo eliminar el elemento"]);
                }
            }
        }else{
            return \Response::json(['res' => 0, 'msj' => "Parámetros incorrectos"]);
        }
    }

    /**
     * Procesar imágenes en base 64 para guardarlas.
     *
     * @el char  $el[json_image_response]
     * @return nombre img para DB
     */
    public function ProcesarImagenes($el){
        $imagen = json_decode($el);
        $data = explode(',', $imagen->data);
        $foto = base64_decode($data[1]);
        $antenombre = sha1(time().$imagen->name);
        $nombre = $antenombre.".png";
        if(Storage::disk('competencias')->put($nombre,  $foto)){
            return $nombre;
        }else{
            return false;
        }
    }

    /**
     * Listar el perfil individual de una competencia
     * @param integer Request $request ID de la competencia
     */
    public function ListarCompetenciaIndividual(Request $request){
        if(isset($request->id)){
            $compe = Competencia::findOrFail($request->id);
            $query = "Select competencias_redes_sociales.*, redes_sociales.nombre, redes_sociales.font_awesome From competencias_redes_sociales Left Join redes_sociales On competencias_redes_sociales.id_red_social = redes_sociales.id Where competencias_redes_sociales.id_competencia = {$request->id}";
            $socials = DB::select($query);
            ob_start();
            foreach($socials as $row){
                if($socials){
    ?>
    <div class="clonedInput" id="socials_<?php echo $row->id; ?>">
        <select class="social-select form-control h-100" name="socials[]" readonly>
                <option selected value="<?php echo $row->id_red_social; ?>"><?php echo $row->nombre; ?></option>
                </select>
        <input class="form-control" name="socials_link[]" placeholder="Link del perfil" value="<?php echo $row->perfil; ?>">
        <input type="hidden" value="<?php echo $row->id; ?>" name="id_red">
        <div class="actions">
            <a class="btn clone fa fa-plus text-success" aria-hidden="true"></a>
            <a class="btn remove fa fa-minus text-danger" aria-hidden="true" data-inedit="true" data-redid="<?php echo $row->id; ?>"></a>
        </div>
    </div>
    <?php }} ?>
    <div class="clonedInput" id="socials_0">
        <select class="social-select form-control h-100" name="socials[]">
        </select>
        <input class="form-control" name="socials_link[]" placeholder="Link del perfil">
        <div class="actions">
            <a class="btn clone fa fa-plus text-success" aria-hidden="true"></a>
            <a class="btn remove fa fa-minus text-danger" aria-hidden="true"></a>
        </div>
    </div>
    <script>
        $(function(){
            ListarSocial($("#socials_0").children(".social-select"));
        });
    </script>
    <?php
            $redes = ob_get_contents();
            ob_end_clean();
            $data[0] = $compe;
            $data[1] = $redes;
            return \Response::json(['res' => 1, 'data' => $data]);
        }
    }

    public function ElminarSocialIndividual(Request $request){
        if(isset($request->red_id)){
            $social = CompetenciasRedesSociale::find($request->red_id);
            if($social->delete()){
                return \Response::json(['res' => 1, 'msj' => "Eliminado éxitosamente"]);
            }
        }
    }

    public function ListarComentariosCompetencia(Request $request){
     $query = "Select competencias_comentarios.comentario, competencias_comentarios.created_at, users.name, users.foto From competencias_comentarios left Join users On competencias_comentarios.id_usuario = users.id Where competencias_comentarios.id_competencia = {$request->id_comp}";

     $consulta = DB::select($query);
     ob_start();?>
            <?php foreach ($consulta as $row){?>
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url('<?php echo url('/').'/images/file/clientes/'.$row->foto; ?>')"></div>
                    </div>
                    <div class="comment-block">
                        <p class="comment-text text-left">
                            <?php echo $row->comentario; ?>
                        </p>
                        <div class="bottom-comment">
                            <div class="comment-date">
                                <?php echo date("d/m/Y (h:i A)", strtotime($row->created_at)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }

        $cuerpo = ob_get_contents();
        ob_end_clean();
		if($cuerpo){
            return \Response::json(['res' => 1, "data" => $cuerpo]);
		}else{
            return \Response::json(['res' => 0, "msj" => "No hay comentarios"]);
			}
    }

    public function ComentarCompetencia(Request $request){
        if(isset($request->id_comp)){
            $comp_comment = new CompetenciasComentario;
            $comp_comment->id_competencia = $request->id_comp;
            $comp_comment->id_usuario = Auth::user()->id;
            $comp_comment->comentario = $request->comentario;
           if($comp_comment->save()){
               return \Response::json(['res' => 1, "msj" => "Comentario exitoso."]);
           }
        }
    }

    public function AutocompleteFuncionarios(Request $request){
        if(isset($request->term)){
            $responsable = CompetenciasFuncionario::where('nombre','LIKE',"%{$request->term}%")->get();
            return \Response::json($responsable);
        }
    }

    public function AñadirSede(Request $request){
        if(isset($request->comp_id)){
            $sedes = new CompetenciasSede;
            $sedes->id_competencia = $request->comp_id;
            $sedes->ubicacion = $request->lat."%%".$request->lng;
            $sedes->cant_empleados = $request->cant_empleados;
            $sedes->id_responsable = $request->responsable;
            $sedes->correo = $request->correo;
            $sedes->foto_planta = $this->ProcesarImagenes($request->foto_planta_values);
            $sedes->sede_principal = $request->sede_principal;
            if(isset($request->sede_principal)){
                $sedes->sede_principal = $request->sede_principal;
            }
            if($sedes->save()){
                return \Response::json(['res' => 1, 'msj' => "Guardado correctamente"]);
            }
        }
    }

    public function AñadirFuncionario(Request $request){
        if(isset($request->comp_id)){
            $funcionario = new CompetenciasFuncionario;
            $funcionario->id_compentecia = $request->comp_id;
            $funcionario->foto = $this->ProcesarImagenes($request->foto_funcionario_values);
            $funcionario->nombre = $request->nom_fun;
            $funcionario->fecha_nacimiento = $request->fecha_nacimiento;
            $funcionario->cargo = $request->cargo;
            $funcionario->fecha_ingreso = $request->fecha_ingreso;
            $funcionario->salario = $request->salario;
            $funcionario->jefe_inmediato = $request->jefe_inmediato;
            $funcionario->estado_civil = $request->estado_civil;
            $funcionario->profesion = $request->profesion;
            $funcionario->perfil = $request->perfil;
            $funcionario->oportunidades = $request->oportunidades;
            if($funcionario->save()){
                return \Response::json(['res' => 1, 'msj' => "Guardado correctamente"]);
            }
        }
    }

    public function ListarSedes(Request $request){
        if(isset($request->id_comp)){
            $query = "SELECT * FROM competencias_sedes WHERE competencias_sedes.id_competencia = {$request->id_comp}";
            $sedes = DB::select($query);
            ob_start();?>
            <?php foreach($sedes as $row){ ?>
            <a>
            <img src="/storage/competencias_files/profile_images/<?php echo $row->foto_planta; ?>" alt="" class="rounded-circle m-4">
            </a>
            <?php }
            $sedes_html = ob_get_contents();
            ob_end_clean();
            return \Response::json(['res' => 1, 'data' => $sedes_html]);
        }
    }

    public function SedesMapa(Request $request){
        if(isset($request->id_comp)){
            $sedes = CompetenciasSede::where('id_competencia', $request->id_comp)->get();
            if($sedes){
                return \Response::json(['res' => 1, 'data' => $sedes]);
            }else{
                return \Response::json(['res' => 0, 'msj' => "No hay datos"]);
            }
        }
    }

    public function ListarFuncionarios(){
        if(isset($request->id_comp)){

        }
    }
}
