<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Users_megaarchivo;
//use Illuminate\Http\Response;

//use App\Http\Requests;
//use App\Http\Controllers\Controller;

class FolderController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*public function filintereses(Request $request)
    {
        $depaso = explode(",", $request->cadena);
        //$depaso = mb_str_split($request->cadena);
        //return response()->json($);
         $users = DB::table('intereses')
            ->distinct()
            ->join('int_users', 'int_users.id_interes', '=', 'intereses.id') 
            ->select('intereses.nombre', 'intereses.id')
            ->whereNotIn('intereses.id', $depaso)
            ->get();
        return response()->json($users);
    }*/

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $public_path = public_path();      
        $ruta= $public_path.'/storage/';
        $mostrar = array();
        $archivos = array();
        if (is_dir($ruta)) { 
            if ($dh = opendir($ruta)) { 
                while (($file = readdir($dh)) !== false) { 
                    if (is_dir($ruta . $file) && $file!="." && $file!=".."){ 
                        $mostrar = array_prepend($mostrar, $file);
                    }elseif ($file!="." && $file!="..") {
                        $archivos = array_prepend($archivos, $file);
                    }
                } 
                closedir($dh); 
            } 
        }
       return Response()->json(['status' => 'success','folder' => $mostrar,'archivos' => $archivos ,'titulo' => $request->input('url')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {          
        if($request->ajax()) {
            $public_path = public_path();      
            $ruta= $public_path.'/storage/'.$request->input('url').'/';
            $mostrar = array();
            $archivos = array();
            if (is_dir($ruta)) { 
                if ($dh = opendir($ruta)) { 
                    while (($file = readdir($dh)) !== false) { 
                    //esta línea la utilizaríamos si queremos listar todo lo que hay en el directorio 
                    //mostraría tanto archivos como directorios 
                    //echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file); 
                        if (is_dir($ruta . $file) && $file!="." && $file!=".."){ 
                            //solo si el archivo es un directorio, distinto que "." y ".
                            $mostrar = array_prepend($mostrar, $file);
                            //listar_directorios_ruta($ruta . $file . "/"); 
                        }elseif ($file!="." && $file!="..") {
                            $archivos = array_prepend($archivos, $file);
                        }
                    } 
                    closedir($dh); 
                } 
            }
            

           return Response()->json(['status' => 'success','folder' => $mostrar,'archivos' => $archivos ,'titulo' => $request->input('url')]);
        }
        return Response::json(['status' => '402']);
    }

    public function editar_nombre(Request $request){
        $viejo=$request->anterior;
        $nuevo=$request->nombre_nuevo;
        
        $anterior=$request->nombre_anterior;
        $contar=strlen($anterior);
        
        $viejo =substr($viejo, 0, -$contar);
        $nuevo=$viejo.$nuevo;

        $public_path = public_path();
        $url = $public_path.$request->anterior;
        if($request->extencion!=''){
            $url1 = $public_path.$nuevo.'.'.$request->extencion;
            $archivoAbierto = fopen($url, 'r');
            fclose($archivoAbierto);
            rename( $url, $url1 );
            return Response()->json(['msj' => "Realizado con exito archivo"]);
        }else{
            $url1 = $public_path.$nuevo;
            //$archivoAbierto = fopen($url, 'r');
            //fclose($archivoAbierto);
            rename( $url, $url1 );
            //El archivo se renombrará correctamente
            return Response()->json(['msj' => "Realizado con exito"]);
        }

        
    }

    public function crearmega(Request $request){
        $nombre=$request->nombre;
        $url=$request->url;

        $public_path = public_path();
        $carpeta = $public_path.'/storage/'.$url.'/'.$nombre;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
            return Response()->json(['msj' => "Creada exitosamente", 'type' => 'success']);
        }else{
            return Response()->json(['msj' => "Ya existe una carpeta", 'type' => 'error']);
        }
        
    }

    public function creararchivomega(Request $request){
        $url=$request->url;
        $usuario_megarchivo=Users_megaarchivo::findorfail($request->usuario);
        $public_path = public_path();
        $carpeta = $public_path.'/storage/'.$url;
        $archivo = $request->file('archivo');

        $antenombre = uniqid();
        $extension = $archivo->getClientOriginalExtension();
        $nombre =$antenombre.".".$extension;
        
        Storage::disk('local')->put($nombre,  \File::get($archivo));
        Storage::move($nombre, $url.'/'.$nombre);
        
        return view('megaarchivo.contenido',['carpeta' => $url, 're' => 'RENAME', 'tiempo' => $request->tiempo, 'html_cod' =>$request->codigo_html, 'titulo' =>$request->titulo, 'usuario_megarchivo' => $usuario_megarchivo]);
    }

    public function eliminarimega(request $request){
        $nombre=$request->nombre_anterior;
        $ruta=$request->recargar;
        Storage::delete($ruta.'/'.$nombre);
    }

    public function eliminarcarpeta(request $request){
        $nombre=$request->nombre_anterior;
        $ruta=$request->recargar;

        $public_path = public_path();
        //rmdir($public_path.'/storage/'.$ruta.'/'.$nombre);
        $carpeta=$public_path.'/storage/'.$ruta.'/'.$nombre;
        $carpeta = @scandir($carpeta);
        if (count($carpeta) > 2){
            echo 'No';
        }else{
            rmdir($public_path.'/storage/'.$ruta.'/'.$nombre);
            echo 'Eliminado correctamente';
        }

        

    }
    public function reemplazararchivomega(request $request){
        $usuario_megarchivo=Users_megaarchivo::findorfail($request->usuario);
        $nombre=$request->nombre_archivo;
        $ruta=$request->url3;
        Storage::delete($ruta.'/'.$nombre);


        $url=$request->url3;
        
        $public_path = public_path();
        $carpeta = $public_path.'/storage/'.$url;
        $archivo = $request->file('archivo');

        $ext=$request->extencion;
        $ext='.'.$ext;
        $antenombre = $request->nombre_archivo;
        $antenombre = str_replace($ext, "", $antenombre);
        $extension = $archivo->getClientOriginalExtension();
        $nombre =$antenombre.".".$extension;
        Storage::disk('local')->put($nombre,  \File::get($archivo));
        Storage::move($nombre, $url.'/'.$nombre);
        return view('megaarchivo.contenido',['carpeta' => $url, 're' => 'RENAME', 'tiempo' => $request->tiempo, 'usuario_megarchivo' => $usuario_megarchivo]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
