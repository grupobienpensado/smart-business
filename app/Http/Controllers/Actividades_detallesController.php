<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Actividades;
use App\Actividades_detalles;
use App\Actividades_detalles_canceladas;
use App\Actividades_detalles_imagenes;
use App\Oportunidades;
use App\Tipo_actividades;
use App\User;
use Auth;

date_default_timezone_set("America/Bogota");
class Actividades_detallesController extends Controller
{
    //
    public function indexactividades(Request $request)
    {	
    	$datos = Actividades_detalles::findOrFail($request->id);
    	$datos->observaciones = $request->observaciones;
    	//$datos->fecha_realizacion = date('Y-m-d');
    	$datos->fecha_finalizacion = date('Y-m-d');
		$datos->estado = $request->estado;
		$datos->user_id = Auth::user()->id;
	    $datos->save();
	    $id=$request->id;
    	$logo = $request->file('file-1');
		if ($logo) {
			for ($i=0;$i<count($logo);$i++) {
				# code...
				$antenombrelogo = uniqid();
				$extensionlogo = $logo[$i]->getClientOriginalExtension();
				$nombrelogo =$antenombrelogo.".".$extensionlogo;
				\Storage::disk('evidencias')->put($nombrelogo,  \File::get($logo[$i]));
				
				$dato = new Actividades_detalles_imagenes;
				$dato->actividades_detalles  = $id;
				$dato->imagen  = $nombrelogo;
				$dato->user_id = Auth::user()->id;
				$dato->save();
			}
			
		}

		return redirect('backlog/principal');
    }

    public function indexaplazar(Request $request)
    {	
    	$dato = new Actividades_detalles_canceladas;
		$dato->actividades_detalles  = $request->id;
		$dato->fecha_ejecucion  = $request->fecha_ejecucion;
		$dato->observaciones = $request->observaciones;
		$dato->save();

		$actividad = Actividades_detalles::find($request->id);
		$actividad->fecha_pendiente = $request->fecha_ejecucion;
		$actividad->save();			

		return redirect('backlog/principal');
    }

    public function save(Request $request)
    {	
    	$detalle 					= new Actividades_detalles;
	    $detalle->detalle  			= $request->detalle_pendiente;
	    $detalle->tipo_actividad  	= $request->tipo_pendiente;
	    $detalle->fecha_pendiente  	= $request->fecha_pendiente;
	    if ($request->oportunidad != "Trabajos Generales ESSI") {
	    	$detalle->oportunidad_id = $request->oportunidad;
	    }
	    $detalle->estado 			= "pendiente";
	    $detalle->responsable 		= Auth::user()->id;
	    $detalle->user_id 			= Auth::user()->id;
	    $detalle->save();			

		return redirect('backlog/principal');
    }

    public function indexcancelar(Request $request)
    {	
    	$datos = Actividades_detalles::findOrFail($request->id);
    	$datos->observacion_final = $request->observacion_final;
    	$datos->fecha_finalizacion=date("Y-m-d");
    	$datos->estado = "cancelado";
	    $datos->save();
	    
		return redirect('backlog/principal');
    }

    public function indexaprobar(Request $request)
    {	
    	$aprobarActiv = " in active";
    	$datos = Actividades_detalles::find($request->id);
    	$datos->observacion_final = $request->observaciones;
    	if($request->estado=="Aprobar"){
    		$datos->estado = "aprobado";
    	}else{
    		$datos->estado = "pendiente";
    	}
    	$datos->supervisor_final=Auth::user()->id;
    	$datos->fecha_final=date("Y-m-d");
    	$datos->fecha_pendiente = $request->fecha_ejecucion;
	    $datos->save();
	    
    	$dato = new Actividades_detalles_canceladas;
		$dato->actividades_detalles  = $request->id;
		$dato->fecha_ejecucion  = $request->fecha_ejecucion;
		$dato->observaciones="%%RECOMENDACION%%";
		$dato->save();

		$modulo=11;
        $permiso_crear='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón crear pendiente" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){ 
                $permiso_crear="Si";
              }else{
                $permiso_crear="No";
              }
            }
          }          
        }

        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="ver listado y calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "PROPIAS"){ 
                $valUseId = Auth::user()->id;
                $datos = Actividades_detalles::where('user_id', $valUseId)->get();
              }else{
                $datos=Actividades_detalles::all();
              }
            }
          }          
        }
        
        $permiso_botones="No";
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="1. pendientes... botones confirmar, aplazar, cancelar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Si"){ 
                $permiso_botones="Si";
              }else{
                $permiso_botones="No";
              }
            }
          }          
        } 

        $permiso_botones_aprobar="No";
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="2. aprobar... botones aceptar, rechazar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){ 
                $permiso_botones_aprobar="Si";
              }else{
                $permiso_botones_aprobar="No";
              }
            }
          }          
        }
        
    	$Oportunidades=Oportunidades::all();
    	$Actividades=Actividades::all();
    	$usuarios=User::all();
        $tipos_actividades=Tipo_actividades::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Listado de pendientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('backlog.backlog',['data' => $data, 'tipos_actividades' => $tipos_actividades,'datos' => $datos, 'oportunidades'=>$Oportunidades,'actividades'=>$Actividades,'usuarios'=>$usuarios, 'permiso_crear' => $permiso_crear, 'permiso_botones' => $permiso_botones, 'permiso_botones_aprobar' => $permiso_botones_aprobar, "aprobarActiv" => $aprobarActiv]);

    }

    public function lista(){
        /*Permiso para crear un pendiente*/
        $data['permiso_crear']="No";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón crear pendiente" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){
                $data['permiso_crear']="Si";
              }
            }
          }
        }

        /*Permiso para Exportar Excel y CSV*/
        $data['permiso_exportar']="No";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Exportar EXCEL y CSV" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){
                $data['permiso_exportar']="Si";
              }
            }
          }
        }

        /*Permiso para Aprobar y Rechazar*/
        $data['permiso_aprobar_rechazar']="No";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="2. aprobar... botones aceptar, rechazar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){
                $data['permiso_aprobar_rechazar']="Si";
              }
            }
          }
        }

        /*Permiso PENDIENTES para Confirmar, Aplazar y Cancelar*/
        $data['permiso_pendientes']="No";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="1. pendientes... botones confirmar, aplazar, cancelar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){
                $data['permiso_pendientes']="Si";
              }
            }
          }
        }

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Listado de pendientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('pendientes/list',['data' => $data]);
    }
}
