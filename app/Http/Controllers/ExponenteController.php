<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\Ferias_exponentes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
date_default_timezone_set("America/Bogota");

class ExponenteController extends Controller
{
    public function editarexponente($id){
        $feria=Feria::find($id);
        $datos=DB::select('SELECT * FROM `ferias_exponentes` WHERE `id_feria`="'.$id.'" ORDER BY `id` ASC');
        $responsables_essi=DB::select('SELECT * FROM `users` WHERE `cargo`="Partner Solution" OR `cargo`="Ejecutivo Comercial" OR `cargo`="Gerente Comercial" ORDER BY `id`');
        if(isset($datos[0]->equipos)){
            $maquinas=explode('%%',$datos[0]->equipos);
        }
        $i=0;
        ob_start();
        if(isset($maquinas)){
        foreach($maquinas as $maquina){
            if($i!=0){
            $equipo=DB::select('SELECT *, (SELECT P.`name` FROM `productos` AS P WHERE P.`id` = R.`producto_id`) AS Producto FROM `producto_referencias` AS R WHERE R.`id`="'.$maquina.'"');
         ?>
            <div class="col-md-3" id="img-maquina<?= $maquina ?>">
                <div class="row">
                    <div class="col-md-9 col-maquinas">
                        <img class="img-maquinas" src="/images/file/productos/<?= $equipo[0]->foto ?>">
                        <h6><?= $equipo[0]->Producto ?> - <?= $equipo[0]->referencia ?></h6>
                    </div>
                    <div class="col-md-3">
                        <i class="fa fa-times-circle eliminar-maqui" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick='eliminar_maquina("img-maquina<?= $maquina ?>","<?= $maquina ?>")'></i>
                    </div>
                </div>
            </div>
        <?php
            }
            $i++;
        }
        }
        $html['maquinas'] = ob_get_contents();
        ob_end_clean();
        $personas=explode('%%',$datos[0]->cantidad_personas);
        ob_start();
        if($datos[0]->cantidad_personas !=""){
        foreach($personas as $persona){
        $user=User::find($persona);
        $nombre=explode(' ',$user->nombres);
        $apellido=explode(' ',$user->apellidos);
         ?>
            <div class="col-md-1 text-center" id="funcionario_invitado<?= $user->id ?>">
                <img src="/images/file/clientes/<?= $user->foto ?>" class="img-circle foto-inv2" alt="Avatar">
                <p class="parra"><a href="#"><strong><?= $nombre[0].' '.$apellido[0] ?></strong></a></p>
                <span class="text-muted"><?= $user->cargo ?></span>
            </div>
        <?php
        }
        }
        $html['invitados'] = ob_get_contents();
        ob_end_clean();

        $objetivos=explode('%%%___%%%',$datos[0]->objetivos);
        ob_start();
        $i=0;
        if($datos[0]->objetivos != ""){
            foreach($objetivos as $objetivo){
                if($i==0){
                    ?>
                    <div class="row n_obj" id="n_obj<?= $i ?>">
                        <div class="col-md-1 num-objetivo">
                            <?= $i+1 ?>
                        </div>
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="objetivos[<?= $i ?>]" value="<?= $objetivo ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }else{
                    ?>
                    <div class="row n_obj" id="n_obj<?= $i ?>">
                        <div class="col-md-1 num-objetivo">
                            <?= $i+1 ?>
                        </div>
                        <div class="col-md-11 mt-1">
                            <div class="row">
                                <div class="col-md-11">
                                    <input type="text" class="form-control" name="objetivos[<?= $i ?>]" value="<?= $objetivo ?>" required>
                                </div>
                                <div class="col-md-1 text-center">
                                    <a class="eliminar" onclick="eliminarobjetivo(<?= $i ?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            $i++;
            }
        }
        $html['objetivos_total']=$i;
        $html['objetivos'] = ob_get_contents();
        ob_end_clean();

        $alcances=explode('%%%___%%%',$datos[0]->alcances);
        ob_start();
        $i=0;
        if($datos[0]->alcances != ""){
            foreach($alcances as $alcance){
                if($i==0){
                    ?>
                    <div class="row n_alc" id="n_alc<?= $i ?>">
                        <div class="col-md-1 num-alcance">
                            <?= $i+1 ?>
                        </div>
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="alcances[<?= $i ?>]" value="<?= $alcance ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }else{ ?>
                    <div class="row n_alc" id="n_alc<?= $i ?>">
                        <div class="col-md-1 num-alcance">
                            <?= $i+1 ?>
                        </div>
                        <div class="col-md-11 mt-1">
                            <div class="row">
                                <div class="col-md-11">
                                    <input type="text" class="form-control" name="alcances[<?= $i ?>]" value="<?= $alcance ?>" required>
                                </div>
                                <div class="col-md-1 text-center">
                                    <a class="eliminar" onclick="eliminaralcance(<?= $i ?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            $i++;
            }
        }
        $html['alcances_total']=$i;
        $html['alcances'] = ob_get_contents();
        ob_end_clean();

        $estrategias=explode('%%%___%%%',$datos[0]->estrategia);
        ob_start();
        $i=0;
        if($datos[0]->estrategia != ""){
            foreach($estrategias as $estrategia){
                if($i==0){
                    ?>
                    <div class="row n_est" id="n_est<?= $i ?>">
                        <div class="col-md-1 num-estrategia">
                            <?= $i+1 ?>
                        </div>
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="estrategia[<?= $i ?>]" value="<?= $estrategia ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }else{ ?>
                    <div class="row n_est" id="n_est<?= $i ?>">
                        <div class="col-md-1 num-estrategia">
                            <?= $i+1 ?>
                        </div>
                        <div class="col-md-11 mt-1">
                            <div class="row">
                                <div class="col-md-11">
                                    <input type="text" class="form-control" name="estrategia[<?= $i ?>]" value="<?= $estrategia ?>" required>
                                </div>
                                <div class="col-md-1 text-center">
                                    <a class="eliminar" onclick="eliminarestrategia(<?= $i ?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            $i++;
            }
        }
        $html['estrategias_total']=$i;
        $html['estrategias'] = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.editarexponente',['data' => $data, 'feria'=>$feria, 'datos' => $datos, 'responsables_essi' => $responsables_essi, 'html' => $html]);
    }

    public function editarexponentedos(Request $request){
        $objetivos=$request->objetivos;
        $obj='';
        $alcances=$request->alcances;
        $alc='';
        $estrategias=$request->estrategia;
        $est='';
        $dato= Feria::findOrFail($request->id_feria);
        $dato->estado="Activo";
        $dato->tipo="Exponente";
        $dato->save();

        $dato = Ferias_exponentes::findOrFail($request->id);

        $logo_anterior = $dato->baner;

        if (isset($request->baner_values)) {
            if(!empty($request->baner_values)){
                $imagen = json_decode($request->baner_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('ferias')->put($nombre,  $foto);
                $dato->baner = $nombre;
                if($logo_anterior!=''){
                    Storage::delete('ferias/'.$logo_anterior);
                }
            }
        }

        $dato->id_feria = $request->id_feria;
        $dato->equipos = $request->referencias;
        $dato->cantidad_personas = $request->cantidad_personas;

        $dato->responsable = $request->responsable;
        foreach($objetivos as $objetivo){
            if($obj==''){
                $obj=$objetivo;
            }else{
                $obj.='%%%___%%%'.$objetivo;
            }
        }
        $dato->objetivos = $obj;
        foreach($alcances as $alcance){
            if($alc==''){
                $alc=$alcance;
            }else{
                $alc.='%%%___%%%'.$alcance;
            }
        }
        $dato->alcances = $alc;
        foreach($estrategias as $estrategia){
            if($est==''){
                $est=$estrategia;
            }else{
                $est.='%%%___%%%'.$estrategia;
            }
        }
        $dato->estrategia = $est;
        $dato->atuendo = $request->atuendo;
        $dato->save();

        return \Response::json(['msj' => "Guardado Correctamente"]);
    }

}
