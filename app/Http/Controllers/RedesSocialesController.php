<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\RedesSociale;
use Auth;


class RedesSocialesController extends Controller
{

    public function ListarTodas(){
        $social = RedesSociale::all();
        ob_start();?>
        <?php foreach($social as $row){ ?>

        <option value="<?php echo $row->id;  ?>"><?php echo $row->nombre;  ?></option>

        <? }
        $redes = ob_get_contents();
        ob_end_clean();
        return \Response::json(['res' => 1, 'data' => $redes]);
    }

}
