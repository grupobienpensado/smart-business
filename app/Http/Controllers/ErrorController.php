<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\stdClass;
use App\User;
use Auth;

class ErrorController extends Controller
{
	public function error(){
		return view('error');
	}
}