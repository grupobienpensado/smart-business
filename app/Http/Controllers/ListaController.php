<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Actividades_detalles;
use App\Oportunidades;
use App\Actividades;
use App\Actividades_detalles_canceladas;
use App\Actividades_detalles_imagenes;
use App\User;
use App\Users_megaarchivo;
use App\Tiempomegaarchivos;
use App\Rutasusadas;
use App\Paises;
use App\Ciclo;
use App\Presupuesto;
use App\Presupuesto_anual;
use App\Metas_oportunidade;
use App\Participacion_probabilidade;
use App\Tipo_actividades;
use App\PermisoCargo;
use Auth;

class ListaController extends Controller
{
    //
    public function backlog(){
        $modulo=11;
        $permiso_crear='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón crear pendiente" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){ 
                $permiso_crear="Si";
              }else{
                $permiso_crear="No";
              }
            }
          }          
        }

        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="ver listado y calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "PROPIAS"){ 
                $valUseId = Auth::user()->id;
                $datos = Actividades_detalles::where('user_id', $valUseId)->get();
              }else{
                $datos=Actividades_detalles::all();
              }
            }
          }          
        }
        
        $permiso_botones="No";
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="1. pendientes... botones confirmar, aplazar, cancelar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Si"){ 
                $permiso_botones="Si";
              }else{
                $permiso_botones="No";
              }
            }
          }          
        } 

        $permiso_botones_aprobar="No";
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="2. aprobar... botones aceptar, rechazar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Si"){ 
                $permiso_botones_aprobar="Si";
              }else{
                $permiso_botones_aprobar="No";
              }
            }
          }          
        }
        
    	$Oportunidades=Oportunidades::all();
    	$Actividades=Actividades::all();
    	$usuarios=User::all();
        $tipos_actividades=Tipo_actividades::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Listado de pendientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('backlog.backlog',['data' => $data, 'tipos_actividades' => $tipos_actividades,'datos' => $datos, 'oportunidades'=>$Oportunidades,'actividades'=>$Actividades,'usuarios'=>$usuarios, 'permiso_crear' => $permiso_crear, 'permiso_botones' => $permiso_botones, 'permiso_botones_aprobar' => $permiso_botones_aprobar]);
    }
    public function calendario(){
        $modulo=11;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="ver listado y calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "PROPIAS"){ 
                $valUseId = Auth::user()->id;
                $datos = Actividades_detalles::where('user_id', $valUseId)->get();
              }else{
                $datos=Actividades_detalles::all();
              }
            }
          }          
        }
        //$datos=Actividades_detalles::all();
        $Oportunidades=Oportunidades::all();
        $Actividades=Actividades::all();
        $usuarios=User::all();
        $aplazadas=Actividades_detalles_canceladas::all();

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Listado de pendientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('backlog.calendario',['data' => $data, 'datos' => $datos, 'oportunidades'=>$Oportunidades,'actividades'=>$Actividades,'usuarios'=>$usuarios,'aplazadas'=>$aplazadas]);
    }

    public function veractividades($fecha_id){
        $dias=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
        $meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $list=explode("-", $fecha_id);
        $dia_letras=date("w",strtotime($fecha_id));
        $calendario=$dias[$dia_letras].", ".$list[2]." de ".$meses[(int)$list[1]-1]." de ".$list[0];

        $modulo=11;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="ver listado y calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "PROPIAS"){ 
                $valUseId = Auth::user()->id;
                $datos = Actividades_detalles::where('user_id', $valUseId)->where("fecha_pendiente", $fecha_id)->get();
              }else{
                $datos=Actividades_detalles::where("fecha_pendiente", $fecha_id)->get();
              }
            }
          }          
        }

        //$datos=Actividades_detalles::where("fecha_pendiente", $fecha_id)->get();
        $aplazadas=Actividades_detalles_canceladas::where("fecha_ejecucion", $fecha_id)->get();

        $pendiente=1;
        $finalizado=1;
        $cancelado=1;
        $pendientes="<table class='table table-sorting table-striped table-hover datatable dataTable no-footer'><thead><tr><th colspan='4' class='text-center titulo-inicial'>Actividades Pendientes</th></tr><tr><th>#</th><th>Oportunidad</th><th>Detalles</th><th>Responsable</th></tr></thead><tbody>";
        $canceladas="<table class='table table-sorting table-striped table-hover datatable dataTable no-footer'><thead><tr><th colspan='4' class='text-center titulo-inicial'>Actividades Canceladas</th></tr><tr><th>#</th><th>Oportunidad</th><th>Detalles</th><th>Responsable</th></tr></thead><tbody>";
        $realizadas="<table class='table table-sorting table-striped table-hover datatable dataTable no-footer'><thead><tr><th colspan='4' class='text-center titulo-inicial'>Actividades Realizadas</th></tr><tr><th>#</th><th>Oportunidad</th><th>Detalles</th><th>Responsable</th></tr></thead><tbody>";
        foreach ($datos as $dato) {
            $valida=false;
            $oportunidad=Oportunidades::find($dato->oportunidad_id);
            $usuario=User::find($dato->responsable);
            $nombre   = explode(" ",$usuario->nombres);
            $apellido = explode(" ",$usuario->apellidos); 
            $nameUser = $nombre[0] ." ". $apellido[0]; 
           /*foreach ($aplazadas as $key) {
                if($key->actividades_detalles==$dato->id){
                    $fecha=$key->fecha_ejecucion;
                    $valida=true;
                }
            }
            if($valida){
                if($fecha_id==$fecha){
                    if($dato->estado=="cancelado"){
                        if(isset($oportunidad) && !empty($oportunidad)){
                            $canceladas.="<tr><td>".$cancelado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>".$oportunidad->titulo."</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$usuario->nombres." ".$usuario->apellidos."</td></tr>";
                            $cancelado++;
                        }else{
                            $canceladas.="<tr><td>".$cancelado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>Trabajos generales ESSI</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$usuario->nombres." ".$usuario->apellidos."</td></tr>";
                            $cancelado++;
                        }

                    } 
                    if($dato->estado=="finalizado"){
                        if(isset($oportunidad) && !empty($oportunidad)){
                            $realizadas.="<tr><td>".$finalizado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>".$oportunidad->titulo."</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$usuario->nombres." ".$usuario->apellidos."</td></tr>";
                            $finalizado++;
                        }else{
                            $realizadas.="<tr><td>".$finalizado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>Trabajos generales ESSI</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$usuario->nombres." ".$usuario->apellidos."</td></tr>";
                            $finalizado++;
                        }
                    }
                    if($dato->estado=="pendiente"){
                        if (isset($oportunidad) && !empty($oportunidad)){
                            $pendientes.="<tr><td>".$pendiente."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>".$oportunidad->titulo."</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$usuario->nombres." ".$usuario->apellidos."</td></tr>";
                            $pendiente++;
                        }else{
                            $pendientes.="<tr><td>".$pendiente."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>Trabajos generales ESSI</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$usuario->nombres." ".$usuario->apellidos."</td></tr>";
                            $pendiente++;
                        }
                    }
                }
            }else{*/
                if($dato->estado=="cancelado"){
                    if($fecha_id==$dato->fecha_pendiente){
                        if(isset($oportunidad) && !empty($oportunidad)){
                            $canceladas.="<tr><td>".$cancelado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>".$oportunidad->titulo."</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$nameUser."</td></tr>";
                            $cancelado++;
                        }else{
                            $canceladas.="<tr><td>".$cancelado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>Trabajos generales ESSI</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$nameUser."</td></tr>";
                            $cancelado++;
                        }
                    }
                }
                if($dato->estado=="finalizado"){
                    if($fecha_id == $dato->fecha_pendiente){
                        if(isset($oportunidad) && !empty($oportunidad)){
                            $realizadas.="<tr><td>".$finalizado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>".$oportunidad->titulo."</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$nameUser."</td></tr>";
                            $finalizado++;
                        }else{
                            $realizadas.="<tr><td>".$finalizado."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>Trabajos generales ESSI</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$nameUser."</td></tr>";
                            $finalizado++;
                        }
                    }
                }
                if($dato->estado=="pendiente"){
                    if($fecha_id==$dato->fecha_pendiente){
                        if(isset($oportunidad) && !empty($oportunidad)){
                            $pendientes.="<tr><td>".$pendiente."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>".$oportunidad->titulo."</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$nameUser."</td></tr>";
                            $pendiente++;
                        }else{
                            $pendientes.="<tr><td>".$pendiente."</td><td style='text-align:left'><a href='../oportunidad/".$dato->oportunidad_id."' target='_blank'>Trabajos generales ESSI</a></td><td style='text-align:left'>".$dato->detalle."</td><td>".$nameUser."</td></tr>";
                            $pendiente++;
                        }
                    }
                }
            //} 
        }
            $canceladas.="</tbody></table>";
            $realizadas.="</tbody></table>";
            $pendientes.="</tbody></table>";
            $contenido=$pendientes."<hr>".$realizadas."<hr>".$canceladas;

            return ["contenido"=>$contenido,"fecha"=>$calendario];
    }
    public function comentarios($id){

    	$datos = Actividades_detalles::find($id);
    	$aplazadas=Actividades_detalles_canceladas::where("actividades_detalles",$datos->id)->get();
    	$imagenes=Actividades_detalles_imagenes::all();
    	$usuario=User::find($datos->responsable);
        $usuario2=User::find($datos->user_id);
        $supervisor=User::find($datos->supervisor_final);

        $nombre_usuario ="";
        if (isset($usuario->name) && !empty($usuario->name)) {
          $nombre   = explode(" ",$usuario->name);
          if (isset($nombre[2])) {
            $nombre_usuario = $nombre[0]." ".$nombre[2];
          }else{
            $nombre_usuario = $nombre[0]." ".$nombre[1];
          }
        }
        
        $nombre_supervisor ="";
        if (isset($supervisor->name) && !empty($supervisor->name)) {
          $nombre   = explode(" ",$supervisor->name);
          if (isset($nombre[2])) {
            $nombre_supervisor = $nombre[0]." ".$nombre[2];
          }else{
            $nombre_supervisor = $nombre[0]." ".$nombre[1];
          }
        }

        if (empty($usuario->foto)) {
            $foto_perfil = '<div class="media-left pull-left"><img src="http://via.placeholder.com/40x40?text=Foto" class="media-object img-circle"></div>';
        }else{
            $foto_perfil = '<div class="media-left pull-left"><img src="'.url("/").'/images/file/clientes/'.$usuario->foto.'" alt="'.$usuario->name.'" class="media-object img-circle" style="max-width:40px"></div>';
        }

        if (empty($supervisor->foto)) {
            $foto_perfilsup = '<div class="media-left pull-left"><img src="http://via.placeholder.com/40x40?text=Foto" class="media-object img-circle"></div>';
        }else{
            $foto_perfilsup = '<div class="media-left pull-left"><img src="'.url("/").'/images/file/clientes/'.$supervisor->foto.'" alt="'.$supervisor->name.'" class="media-object img-circle" style="max-width:40px"></div>';
        }

    	$content = '';
        $parrafosComent = '';
    	$valida=false;
    	$archivos = '';
        setlocale(LC_ALL, "es_CO.UTF-8");

        foreach($aplazadas as $aplazada){
            if($aplazada->observaciones!="%%RECOMENDACION%%"){
                $parrafosComent.=$foto_perfil.'
                <a href="#">'.$nombre_usuario.'</a><p class="text-muted username soloprimer normalp"> el día '.strftime("%d %B %Y", strtotime($aplazada->created_at)).' aplazo el pendiente:</p>
                <p class="text-muted username soloprimer normalp">'.ucfirst($aplazada->observaciones).'</p>';
            }else{
                $parrafosComent.=$foto_perfil.'
                <a href="#">'.$nombre_usuario.'</a><p class="text-muted username soloprimer normalp"> finalizó el pendiente el día '.strftime("%d %B %Y", strtotime($aplazada->updated_at)).'</p> 
                <p class="text-muted username soloprimer normalp">'.$datos->observaciones.'</p> 
                '.$foto_perfilsup.'<a href="#">'.$nombre_supervisor.'</a><p class="text-muted username soloprimer normalp"> reactivo el pendiente el día '.strftime("%d %B %Y", strtotime($aplazada->created_at)).'</p>
                <p class="text-muted username soloprimer normalp">Recomendando ejecutar el '.strftime("%d %B %Y", strtotime($aplazada->fecha_ejecucion)).'</p> 
                <p class="text-muted username soloprimer normalp">'.ucfirst($datos->observacion_final).'</p>';
            }           
        } 

        if($datos->observacion_final=="" && $datos->estado=="finalizado"){    
            $parrafosComent.=$foto_perfil.'
            <a href="#">'.$nombre_usuario.'</a><p class="text-muted username soloprimer normalp"> finalizó el pendiente el día '.strftime("%d %B %Y", strtotime($datos->updated_at)).'</p>
            <p class="text-muted username soloprimer normalp">'.$datos->observaciones.'</p>
            <p class="text-muted username soloprimer normalp">El pendiente se encuentra en espera de aprobación por parte del supervisor </p>';
        }

        if($datos->estado=="aprobado"){    
            $parrafosComent.=$foto_perfil.'
            <a href="#">'.$nombre_usuario.'</a><p class="text-muted username soloprimer normalp"> finalizó el pendiente el día '.strftime("%d %B %Y", strtotime($datos->updated_at)).'</p>
            <p class="text-muted username soloprimer normalp">'.$datos->observaciones.'</p>
            '.$foto_perfilsup.'<a href="#">'.$nombre_supervisor.'</a><p class="text-muted username soloprimer normalp">Pendiente aprobado el '.strftime("%d %B %Y", strtotime($datos->updated_at)).'</p> 
            <p class="text-muted username soloprimer normalp">'.ucfirst($datos->observacion_final).'</p>';
        }

        if($datos->estado=="cancelado"){
            $parrafosComent.=$foto_perfil.'
            <a href="#">'.$nombre_usuario.'</a><p class="text-muted username soloprimer normalp"> cancelo el pendiente el día '.strftime("%d %B %Y", strtotime($datos->updated_at)).'</p>
            <p class="text-muted username soloprimer normalp">'.ucfirst($datos->observaciones).' '.ucfirst($datos->observacion_final).'</p>';
        }
              

        

        $content = '
        <div class="media clearfix header-bottom">
            '.$foto_perfil.'
            <div class="media-body">
                <a href="#">'.$nombre_usuario.'</a><br>
                <p class="text-muted username soloprimer normalp">'.$datos->detalle.'</p>
                <p class="text-muted username soloprimer normalp">Creado el '.strftime("%d %B %Y", strtotime($datos->created_at)).'</p> 
                <p class="text-muted username soloprimer normalp">Se ejecutara el '.strftime("%d %B %Y", strtotime($datos->fecha_pendiente)).'</p>'.$parrafosComent.'                        
            </div>
        </div>';
        //aqui va lo otro 
        // todo antes de lo q esta

        if($datos->estado=="finalizado"){
            list($uno,$dos)=explode(" ",$datos->updated_at);
            foreach ($imagenes as $key) {
                if($key->actividades_detalles==$id){
                    list($nombre,$ext)=explode(".", $key->imagen);
                    if($ext=="jpg"||$ext=="JPG"){
                        $imagen="jpg.png";
                    }elseif($ext=="png"||$ext=="PNG"){
                        $imagen="png.png";
                    }elseif($ext=="DOC"||$ext=="doc"){
                        $imagen="doc.png";
                    }elseif($ext=="pdf"||$ext=="PDF"){
                        $imagen="pdf.png";
                    }elseif($ext=="ppt"||$ext=="PPT"){
                        $imagen="ppt.png";
                    }elseif($ext=="xls"||$ext=="XLS"){
                        $imagen="xls.png";
                    }else{
                        $imagen="file.png";
                    }
                    $archivos.='<div class="display-line">
                    <a href="/images/file/evidencias/'.$key->imagen.'" target="_blank"><img src="/images/icons/'.$imagen.'"/></a>
                    </div>';
                }
            }
        } 
        
    	
    	
    	return ['success'=>true, 'datos'=>$datos,'contenido'=>$content,'archivos'=>$archivos];
    }

    public function dashboardmega(){
        $datos=Users_megaarchivo::all();
        $datos1=Tiempomegaarchivos::all();
        $datos2 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `rutasusadas` GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 5');
        //$datos3 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `rutasusadas` GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 10');
        $datos4 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `archivousados` GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 10');
        $i=0;
        foreach($datos4 as $d4){
            $diez[$i][0]=$d4->archivo;
            $diez[$i][1]=$d4->contador;
            $i++;
        }
        return view('megaarchivo.dashboard',['usuarios' => $datos, 'tiempomegaarchivos' => $datos1, 'rutasusadas' => $datos2, 'diez' => $diez]);
    }

    public function usuarios(){
        $lista=Paises::all();
        $data['cargos'] = PermisoCargo::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Funcionarios" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('usuarios.crearusuario',["data" => $data, "paises"=>$lista]);
    }

    public function paises(){
        $lista=Paises::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Configuracion" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('configuracion.paises',["data" => $data, "paises"=>$lista]);
    }

    public function ciclos(){
        $lista=Ciclo::all();
        $lista=burbuja($lista);
        $metas=Metas_oportunidade::all();
        $probabilidades=Participacion_probabilidade::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Configuracion" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('configuracion.ciclos',["data" => $data, "ciclos"=>$lista,"metas"=>$metas,"probabilidades"=>$probabilidades]);
    }

    public function ciclosave(Request $request){
        $nombrelogo="";
        $ciclos=Ciclo::all();
        $valida=false;
        foreach ($ciclos as $key) {
            if($key->porcentaje==$request->porcentaje){
                $valida=true;
                break;
            }
        }
        if(!$valida){
            $logo = $request->foto_values;
            if (!empty($logo)) {
                $imagen = json_decode($logo);           
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);

                $antenombre = uniqid();
                $nombrelogo =$antenombre.".png";
                \Storage::disk('porcentajes')->put($nombrelogo,  $foto);
            }

            $datos=new Ciclo();
            $datos->nombre=$request->porcentaje."% - ".$request->nombre;
            $datos->porcentaje=$request->porcentaje;
            $datos->dias=$request->dias;
            $datos->imagen="images/porcentajes/".$nombrelogo;
            $datos->user_id=Auth::user()->id;
            $datos->save();
        }
        return redirect("ciclos");
    }
    public function findciclo($id){
        $ciclos=Ciclo::findOrFail($id);
        $list=explode(" ",$ciclos);
        $contenido='<input type="hidden" value="'.$id.'" name="id" />
                    <div class="col-md-4 form-group">
                        <label for="nombre" class="col-form-label">Nombre</label>
                        <input name="nombre" type="text" required="" value="'.$list[2].'" class="form-control">
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="dias" class="col-form-label">Dias de transición</label>
                        <input name="dias" type="number" required="" class="form-control" value="'.$ciclos->dias.'" min="0">
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="porcentaje" class="col-form-label">Porcentaje</label>
                        <input name="porcentaje" type="number" required="" class="form-control" value="'.$ciclos->porcentaje.'" min="0">
                    </div>
                    <div class="col-md-12" style="text-align: center; margin: 10px">
                      <div class="dropzone imagen-perfil" style="height: 100px; width:100px; max-height: 100px; min-height: 100px;" data-width="32" data-height="32" data-ajax="false" data-originalsave="true">
                        <input type="file" name="foto_2" required="required" accept="image/gif, image/jpeg, image/png">
                      </div>
                    </div>';
        return ["contenido"=>$contenido];
    }
    public function cicloedit(Request $request){
        $nombrelogo="";
        $ciclos=Ciclo::all();
        $valida=false;
        foreach ($ciclos as $key) {
            if($key->porcentaje==$request->porcentaje&&$key->id!=$request->id){
                $valida=true;
                break;
            }
        }
        if(!$valida){
            $logo = $request->foto_2_values;
            if (!empty($logo)) {
                $imagen = json_decode($logo);           
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);

                $antenombre = uniqid();
                $nombrelogo =$antenombre.".png";
                \Storage::disk('porcentajes')->put($nombrelogo,  $foto);
            }

            $datos=Ciclo::findOrFail($request->id);
            $datos->nombre=$request->porcentaje."% - ".$request->nombre;
            $datos->porcentaje=$request->porcentaje;
            $datos->dias=$request->dias;
            $datos->imagen="images/porcentajes/".$nombrelogo;
            $datos->user_id=Auth::user()->id;
            $datos->save();
        }
        return redirect("ciclos");
    }

    public function savepaises(Request $request){
        $nombrelogo="";
        $logo = $request->foto_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombrelogo =$antenombre.".png";
            \Storage::disk('iconos')->put($nombrelogo,  $foto);
        }
        $lista=Paises::all();
        foreach ($lista as $key) {
            if($request->pais==$key->name){
                $id=$key->id;
                break;
            }
        }
        $datos=Paises::findOrFail($id);
        $datos->imagen=$nombrelogo;
        $datos->user_id=Auth::user()->id;
        $datos->save();

        return redirect("paises");

    }

    public function dashboardmegafiltro($fi,$ff,$t){
        $datos=Users_megaarchivo::all();
        
        $datos1 = Tiempomegaarchivos::where('fecha_ingreso', '>=', ($fi.' 00:00:00'))->where('fecha_retiro', '<=', ($ff.' 23:59:59'))->get();
        $datos2 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `rutasusadas` WHERE `fecha_ingreso`>="'.$fi.' 00:00:00"'.' AND `fecha_retiro`<="'.$ff.' 23:59:59"'.' GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 5');
        //$datos3 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `rutasusadas` GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 10');
        $datos4 = DB::select('SELECT `archivo`, COUNT(`id`) AS `contador` FROM `archivousados` WHERE `fecha_ingreso`>="'.$fi.' 00:00:00"'.' AND `fecha_retiro`<="'.$ff.' 23:59:59"'.' GROUP BY(archivo) ORDER BY `contador` DESC LIMIT 10');
        $i=0;
        /*foreach($datos3 as $d3){
            $diez[$i][0]=$d3->archivo;
            $diez[$i][1]=$d3->contador;
            $i++;
        }*/
        
        foreach($datos4 as $d4){
            $diez[$i][0]=$d4->archivo;
            $diez[$i][1]=$d4->contador;
            $i++;
        }

        $tiempo_megarchivo = Tiempomegaarchivos::findOrFail($t);
        $usuario_megarchivo=Users_megaarchivo::findOrFail($tiempo_megarchivo->id_user);
        return view('megaarchivo.dashboard',['usuarios' => $datos, 'tiempomegaarchivos' => $datos1, 'rutasusadas' => $datos2, 'diez' => $diez, 'fi' => $fi, 'ff' => $ff, 'tiempo' => $t, 'usuario_megarchivo' => $usuario_megarchivo]);
    }
    public function filtrosgraficas($id){
        $meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $actividades = Actividades::all();
        $individuales = Presupuesto::all();
        $mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.Auth::user()->id.'" AND `mes`="'.(int)(date('m')).'" ORDER BY `id`');
        var_dump($id);
        $anuales = Presupuesto_anual::all();
        $numero_dias=$_GET["Calendario"];
        $acumulado=0;
        $presupuesto_acumulado=0;
        $diario_g=0;
        $contenido='';

    $contenido3='var chart = AmCharts.makeChart( "chartdiv-3", {
  "type": "serial",
  "addClassNames": true,
  "theme": "light",
  "autoMargins": false,
  "marginLeft": 30,
  "marginRight": 8,
  "marginTop": 10,
  "marginBottom": 26,
  "balloon": {
    "adjustBorderColor": false,
    "horizontalPadding": 10,
    "verticalPadding": 8,
    "color": "#ffffff"
  },

  "dataProvider": [';
    $ajust=0; 
    for($i=1;$i<=$numero_dias;$i++){
      $presu_diario=0; 
    if($i<10){
      $i="0".$i;
    }
    foreach($actividades as $key){
    if($key->fecha_actividad==date('Y-m-').$i&&$key->user_id==Auth::user()->id){
        if($id=="Alimentación"){
            $diario_g+=(int)(str_replace(",","",$key->alimentacion));
        }elseif($id=="Transportes Internos"){
            $diario_g+=(int)(str_replace(",","",$key->transportes_internos));
        }elseif($id=="Transportes Intermunicipales"){
            $diario_g+=(int)(str_replace(",","",$key->transportes_intermunicipales));
        }elseif($id=="Tiquetes Aereo"){
            $diario_g+=(int)(str_replace(",","",$key->tiquete_aereo));
        }elseif($id=="Papeleria"){
            $diario_g+=(int)(str_replace(",","",$key->papeleria));
        }elseif($id=="Invitación Cliente"){
            $diario_g+=(int)(str_replace(",","",$key->invitacion_cliente));
        }elseif($id=="Alquiler Vehiculo"){
            $diario_g+=(int)(str_replace(",","",$key->alquiler_vehiculo));
        }elseif($id=="Gasolina y Pasaje"){
            $diario_g+=(int)(str_replace(",","",$key->gasolina_pasaje));
        }elseif($id=="Hotel"){
            $diario_g+=(int)(str_replace(",","",$key->hotel));
        }elseif($id=="Otros"){
            $diario_g+=(int)(str_replace(",","",$key->otros));
        }elseif("Todos"){
            $diario_g+=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasaje)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
        }
        }
    }
    foreach($mi_presupuesto as $key){
        if($key->dia==$i){
            if($id=="Alimentación"){
                $presupuesto_acumulado+=str_replace(",","",$key->alimentacion);
                
            }elseif($id=="Transportes Internos"){
                $presupuesto_acumulado+=str_replace(",","",$key->transporte_interno);
                
            }elseif($id=="Transportes Intermunicipales"){
                $presupuesto_acumulado+=str_replace(",","",$key->transporte_intermunicipal);
                
            }elseif($id=="Tiquetes Aereo"){
                $presupuesto_acumulado+=str_replace(",","",$key->tiquete_aereo);
                
            }elseif($id=="Papeleria"){
                $presupuesto_acumulado+=str_replace(",","",$key->papeleria);
                
            }elseif($id=="Invitación Cliente"){
                $presupuesto_acumulado+=str_replace(",","",$key->invitacion_cliente);
                
            }elseif($id=="Alquiler Vehiculo"){
                $presupuesto_acumulado+=str_replace(",","",$key->alquiler_vehiculo);
                
            }elseif($id=="Gasolina y Pasaje"){
                $presupuesto_acumulado+=str_replace(",","",$key->gasolina_pasajes);
                
            }elseif($id=="Hotel"){
                $presupuesto_acumulado+=str_replace(",","",$key->hotel);
                
            }elseif($id=="Otros"){
                $presupuesto_acumulado+=str_replace(",","",$key->otros);
                
            }elseif("Todos"){
                $presupuesto_acumulado+=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
            }
        }
    
    }

    $contenido3.='{
    "year": "'.$i.'",
    "income": "'.$diario_g.'",
    "expenses": "'.$presupuesto_acumulado.'"
  },'; 
  }
    $contenido3.='],
  "valueAxes": [ {
    "axisAlpha": 0,
    "position": "left"
  } ],
  "startDuration": 1,
  "graphs": [ {
    "alphaField": "alpha",
    "balloonText": "<span>[[title]] en '.$meses[date("m")-1].' [[category]]:<br><span>$ [[value]]</span> [[additional]]</span>",
    "fillAlphas": 1,
    "title": "Gastado",
    "type": "column",
    "valueField": "income",
    "dashLengthField": "dashLengthColumn"
  }, {
    "id": "graph2",
    "balloonText": "<span>[[title]] en '.$meses[date("m")-1].' [[category]]:<br><span>$ [[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Presupuestado",
    "valueField": "expenses",
    "dashLengthField": "dashLengthLine"
  } ],
  "categoryField": "year",
  "categoryAxis": {
    "gridPosition": "start",
    "axisAlpha": 0,
    "tickLength": 0
  },
  "export": {
    "enabled": false
  }
} );';
    
    $contenido2='
    var chartData1 = generateChartData();
    var chart = AmCharts.makeChart("chartdiv-2", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "useGraphSettings": true
        },
        "dataProvider":chartData1,
        "synchronizeGrid":true,
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "axisAlpha": 1,
            "position": "right"
        }, {
            "id":"v3",
            "axisColor": "#B0DE09",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "position": "left"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "lineColor": "#FF6600",
            "bullet": "round",
            "bulletBorderThickness": 1,
            "hideBulletsCount": 30,
            "title": "Presupuesto Anual",
            "valueField": "visits",
        "fillAlphas": 0
        }, {
            "valueAxis": "v2",
            "lineColor": "#FCD202",
            "bullet": "square",
            "bulletBorderThickness": 1,
            "hideBulletsCount": 30,
            "title": "Gastado",
            "valueField": "hits",
        "fillAlphas": 0
        }, {
            "valueAxis": "v3",
            "lineColor": "#B0DE09",
            "bullet": "triangleUp",
            "bulletBorderThickness": 1,
            "hideBulletsCount": 30,
            "title": "Presupuesto Mensual",
            "valueField": "views",
        "fillAlphas": 0
        }],
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "axisColor": "#DADADA",
            "minorGridEnabled": false
        },
        "export": {
          "enabled": false,
            "position": "bottom-right"
         }
    });';
    $contenido2.='
    function generateChartData() {
    var chartData = [];';
    for($i=0;$i<12;$i++){ 
        $totalp=0;
        $totalg=0;
        $totalpind=0;
        foreach ($anuales as $key) {
          # code...
          $o=$i+1;
          if($o==$key->mes&&date("Y")==$key->anio){
            if($id=="Alimentación"){
                $totalp=str_replace(",","",$key->alimentacion);
            }elseif($id=="Transportes Internos"){
                $totalp=str_replace(",","",$key->transporte_interno);
            }elseif($id=="Transportes Intermunicipales"){
                $totalp=str_replace(",","",$key->transporte_intermunicipal);
            }elseif($id=="Tiquetes Aereo"){
                $totalp=str_replace(",","",$key->tiquete_aereo);
            }elseif($id=="Papeleria"){
                $totalp=str_replace(",","",$key->papeleria);
            }elseif($id=="Invitación Cliente"){
                $totalp=str_replace(",","",$key->invitacion_cliente);
            }elseif($id=="Alquiler Vehiculo"){
                $totalp=str_replace(",","",$key->alquiler_vehiculo);
            }elseif($id=="Gasolina y Pasaje"){
                $totalp=str_replace(",","",$key->gasolina_pasaje);
            }elseif($id=="Hotel"){
                $totalp=str_replace(",","",$key->hotel);
            }elseif($id=="Otros"){
                $totalp=str_replace(",","",$key->otros);
            }elseif("Todos"){
                $totalp=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transporte_interno)+str_replace(",","",$key->transporte_intermunicipal)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasaje)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
            }
            
          }
        }
        foreach ($actividades as $key) {
          # code...
          $o=$i+1;
          if($o<10){
            $o="0".$o;
          }
          for($k=1;$k<=31;$k++){
            if($k<10){
              $k="0".$k;
            }
            if(date("Y")."-".$o."-".$k==$key->fecha_actividad){
                if($id=="Alimentación"){
                    $totalg=$totalg+str_replace(",","",$key->alimentacion);
                }elseif($id=="Transportes Internos"){
                    $totalg=$totalg+str_replace(",","",$key->transportes_internos);
                }elseif($id=="Transportes Intermunicipales"){
                    $totalg=$totalg+str_replace(",","",$key->transportes_intermunicipales);
                }elseif($id=="Tiquetes Aereo"){
                    $totalg=$totalg+str_replace(",","",$key->tiquete_aereo);
                }elseif($id=="Papeleria"){
                    $totalg=$totalg+str_replace(",","",$key->papeleria);
                }elseif($id=="Invitación Cliente"){
                    $totalg=$totalg+str_replace(",","",$key->invitacion_cliente);
                }elseif($id=="Alquiler Vehiculo"){
                    $totalg=$totalg+str_replace(",","",$key->alquiler_vehiculo);
                }elseif($id=="Gasolina y Pasaje"){
                    $totalg=$totalg+str_replace(",","",$key->gasolina_pasajes);
                }elseif($id=="Hotel"){
                    $totalg=$totalg+str_replace(",","",$key->hotel);
                }elseif($id=="Otros"){
                    $totalg=$totalg+str_replace(",","",$key->otros);
                }elseif("Todos"){
                    $totalg=$totalg+str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
                }
            }
          }
        }
        foreach ($individuales as $ind) {
          # code...

           if(date("Y")==$ind->anio&&((int)$i+1)==$ind->mes){
            if($id=="Alimentación"){
                $totalpind=$totalpind+str_replace(",","",$ind->alimentacion);
            }elseif($id=="Transportes Internos"){
                $totalpind=$totalpind+str_replace(",","",$ind->transporte_interno);
            }elseif($id=="Transportes Intermunicipales"){
                $totalpind=$totalpind+str_replace(",","",$ind->transporte_intermunicipal);
            }elseif($id=="Tiquetes Aereo"){
                $totalpind=$totalpind+str_replace(",","",$ind->tiquete_aereo);
            }elseif($id=="Papeleria"){
                $totalpind=$totalpind+str_replace(",","",$ind->papeleria);
            }elseif($id=="Invitación Cliente"){
                $totalpind=$totalpind+str_replace(",","",$ind->invitacion_cliente);
            }elseif($id=="Alquiler Vehiculo"){
                $totalpind=$totalpind+str_replace(",","",$ind->alquiler_vehiculo);
            }elseif($id=="Gasolina y Pasaje"){
                $totalpind=$totalpind+str_replace(",","",$ind->gasolina_pasajes);
            }elseif($id=="Hotel"){
                $totalpind=$totalpind+str_replace(",","",$ind->hotel);
            }elseif($id=="Otros"){
                $totalpind=$totalpind+str_replace(",","",$ind->otros);
            }elseif("Todos"){
                $totalpind=$totalpind+str_replace(",","",$ind->alimentacion)+str_replace(",","",$ind->transporte_interno)+str_replace(",","",$ind->transporte_intermunicipal)+str_replace(",","",$ind->tiquete_aereo)+str_replace(",","",$ind->papeleria)+str_replace(",","",$ind->invitacion_cliente)+str_replace(",","",$ind->alquiler_vehiculo)+str_replace(",","",$ind->gasolina_pasajes)+str_replace(",","",$ind->hotel)+str_replace(",","",$ind->otros);
                }
            }
        }
        if($i<10){
          $o="0".($i+1);
        }else{
           $o=$i+1;
        }
        $dia=15;
        if($o==12){
          $dia=10;
        }

        $contenido2.='chartData.push({
            date: "'.date('Y-').$o."-".$dia.'",
            visits: "'.$totalp.'",
            hits: "'.$totalg.'",
            views: "'.$totalpind.'"
        });';
     }
     $contenido2.="return chartData; }";
        return ["gastos"=>$contenido,"anual"=>$contenido2,"diario"=>$contenido3];
    }


}

function burbuja($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["porcentaje"]>$array[$j+1]["porcentaje"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }
 
    return $array;
}
