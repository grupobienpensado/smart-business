<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Basedatosclientesgbp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
date_default_timezone_set("America/Bogota");

class ClientesgbpController extends Controller
{
    public function crearclientes(){
        return view('clientesgbp.crearclientes');
    }

    public function guardarclientes(Request $request){
        $clientes=$request->formulario;
        $dato = new Basedatosclientesgbp;
        foreach($clientes as $index=>$value){
            $dato->$index=$value;
        }
        $dato->save();
        return view('clientesgbp.crearclientes');
    }
}

?>
