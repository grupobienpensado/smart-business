<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\OportunidadProducto;
//require_once('/exportar/html2pdf.class.php');
use Auth;

class ReporteexcelController extends Controller
{
    public function reporte(){
        $oportunidades= DB::select('SELECT (SELECT (SELECT ES.`pais` FROM `empresa_sedes` AS ES WHERE ES.`id`=H3.`value`) FROM `historial_oportunidades` AS H3 WHERE H3.`key`="sede" AND H3.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1) AS pais, (SELECT H1.`value` FROM `historial_oportunidades` AS H1 WHERE H1.`key`="ciclo_venta" AND  H1.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1 ) AS ciclo_venta, (SELECT H2.`value` FROM `historial_oportunidades` AS H2 WHERE H2.`key`="fecha_oc" AND  H2.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1 ) AS fecha_cierre, O.`id` FROM `oportunidades` AS O WHERE (SELECT H1.`value` FROM `historial_oportunidades` AS H1 WHERE H1.`key`="ciclo_venta" AND H1.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1 )!=0 AND (SELECT H1.`value` FROM `historial_oportunidades` AS H1 WHERE H1.`key`="ciclo_venta" AND H1.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1 )!=90 AND (SELECT YEAR(H2.`value`) FROM `historial_oportunidades` AS H2 WHERE H2.`key`="fecha_oc" AND  H2.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1 )="2018" AND (SELECT (SELECT ES.`pais` FROM `empresa_sedes` AS ES WHERE ES.`id`=H3.`value`) FROM `historial_oportunidades` AS H3 WHERE H3.`key`="sede" AND H3.`oportunidad_id`=O.`id` ORDER BY `updated_at` DESC LIMIT 1)!="Colombia" ORDER BY `fecha_cierre` ASC');
        $consulta='';
        foreach($oportunidades as $oportunidad){
            $consulta.='O.`oportunidad_id`="'.$oportunidad->id.'" OR ';
            $oportunidades= DB::select('SELECT O.oportunidad_id, (SELECT P.`name` FROM `productos` AS P WHERE P.`id`=O.producto ) AS Producto, (SELECT R.`referencia` FROM `producto_referencias` AS R WHERE R.`id`=O.referencia ) AS Referencia, O.`Cantidad` FROM `oportunidad_productos` AS O WHERE O.`oportunidad_id`="'.$oportunidad->id.'" AND O.`deleted_at` IS NULL');

        }
        echo $consulta;
    }
}
?>
