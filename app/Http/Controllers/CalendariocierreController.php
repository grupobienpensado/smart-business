<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use APP\Producto;
use App\ViewOportunidades;
use Auth;

class CalendariocierreController extends Controller
{
    public function index()
    {
        /*Permiso para acceder Calendario de vendidas*/
        $data['permiso_calendariovendidos_acceder']='No';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre Vendidos - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_calendariovendidos_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Calendario de vendidas*/

        /*Permiso para acceder Calendario de Proyectadas*/
        $data['permiso_calendarioproyectados_acceder']='No';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre proyecctados - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_calendarioproyectados_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Calendario de Proyectadas*/

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('oportunidades.calendariocierremenu',['data' => $data]);
    }

    public function calendario_cierre(){
        /*Permiso para acceder Calendario de Proyectadas*/
        $data['permiso_calendarioproyectados_acceder']='No';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre proyecctados - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_calendarioproyectados_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Calendario de Proyectadas*/

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        if($data['permiso_calendarioproyectados_acceder']=='Si'){
            return view('oportunidades.calendariocierre',['data' => $data]);
        }else{
            return view('oportunidades.nopermiso');
        }

    }

    public function calendario_cierre_cerrado(){
        /*Permiso para acceder Calendario de vendidas*/
        $data['permiso_calendariovendidos_acceder']='No';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre Vendidos - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_calendariovendidos_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Calendario de vendidas*/
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        if($data['permiso_calendariovendidos_acceder'] == "Si"){
            return view('oportunidades.calendariocierrecerrado',['data' => $data]);
        }else{
            return view('oportunidades.nopermiso');
        }
    }

    public function action(Request $request){
        /*Permiso para acceder Calendario de Proyectadas*/
        $permiso_calendarioproyectados_datos='Ver todas';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre proyectados - datos" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Ver propio"){
				$permiso_calendarioproyectados_datos='Ver propio';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Calendario de Proyectadas*/

        $accion = $request->action;
        $filtro_maquina='';
        if(!empty($request->maquina)){
            $filtro_maquina=' AND OP.producto = '.$request->maquina;
        }
        switch($accion){
            case 0:
                /*lista de calendario anual*/
                if($request->medida == 1){
                    $ancho = "col-md-3";
                }else{
                    $ancho = "col-md-2";
                }
                $meses_letra=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $fecha_actual = date('Y-m');
                $cierres = DB::select('SELECT O.id AS ID_OPORTUNIDAD, (SELECT SUM(REPLACE(OP.total,",","")) FROM oportunidad_productos AS OP WHERE OP.oportunidad_id=O.id AND OP.deleted_at IS NULL'.$filtro_maquina.')*(SELECT REPLACE(H.value,",","") FROM historial_oportunidades AS H WHERE H.oportunidad_id = O.id AND H.key LIKE "tasa_cambio" ORDER BY H.created_at DESC LIMIT 0,1) AS TOTAL, (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.created_at DESC LIMIT 0,1) AS FECHA FROM oportunidades AS O WHERE (SELECT H1.value FROM historial_oportunidades AS H1 WHERE H1.oportunidad_id = O.id AND H1.key LIKE "ciclo_venta" ORDER BY H1.created_at DESC LIMIT 0,1) != "0" AND (SELECT H1.value FROM historial_oportunidades AS H1 WHERE H1.oportunidad_id = O.id AND H1.key LIKE "ciclo_venta" ORDER BY H1.created_at DESC LIMIT 0,1) != "90" AND (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.created_at DESC LIMIT 0,1) >= "'.$fecha_actual.'" ORDER BY FECHA ASC');
                $data['anos_aparece_cierre']='';
                $data['total']=0;
                foreach($cierres as $cierre){
                    /*aplicar permiso*/
                    $ejecutar = 1; //Si lo ejecute
                    if($permiso_calendarioproyectados_datos == 'Ver propio'){
                        $query = 'SELECT H.value AS IDRESPONSABLE FROM historial_oportunidades H WHERE H.key LIKE "responsable" AND H.oportunidad_id = '.$cierre->ID_OPORTUNIDAD.' ORDER BY H.id DESC LIMIT 0,1';
                        $responsable_opo = DB::SELECT($query);
                        if($responsable_opo[0]->IDRESPONSABLE == Auth::user()->id){
                            $ejecutar = 1; //Si lo ejecute
                        }else{
                            $ejecutar = 0; //No lo ejecute
                        }
                    }
                    if($ejecutar==1){
                        $data['total']+=$cierre->TOTAL;
                        /*Calcular total por año*/
                        if(!isset($data['total_ano'][date('Y',strtotime($cierre->FECHA))])){
                            $data['total_ano'][date('Y',strtotime($cierre->FECHA))] = $cierre->TOTAL;
                        }else{
                            $data['total_ano'][date('Y',strtotime($cierre->FECHA))]+=$cierre->TOTAL;
                        }
                        /*Fin Calcular total por año*/

                         if(!isset($condicion["'".date('Y-m',strtotime($cierre->FECHA))."'"])){
                            $condicion["'".date('Y-m',strtotime($cierre->FECHA))."'"]='OP.oportunidad_id = '.$cierre->ID_OPORTUNIDAD;
                             if($cierre->TOTAL > 0){
                                 $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"]='O.id = '.$cierre->ID_OPORTUNIDAD;
                             }else{
                                 $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"]='O.id = 0';
                             }
                        }else{
                            $condicion["'".date('Y-m',strtotime($cierre->FECHA))."'"].=' OR OP.oportunidad_id = '.$cierre->ID_OPORTUNIDAD;
                            if($cierre->TOTAL > 0){
                                $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"].=' OR O.id = '.$cierre->ID_OPORTUNIDAD;
                            }else{
                                $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"].=' OR O.id = 0';
                            }
                        }

                        if(isset($cuadro["'".date('Y-m',strtotime($cierre->FECHA))."'"])){
                            $cuadro["'".date('Y-m',strtotime($cierre->FECHA))."'"]['total_mes']+=$cierre->TOTAL;
                        }else{
                            $cuadro["'".date('Y-m',strtotime($cierre->FECHA))."'"]['total_mes']=$cierre->TOTAL;

                            /*mes año de la tarjeta*/
                            $cuadro[date("'".'Y-m',strtotime($cierre->FECHA))."'"]['mes']=intval(date('m',strtotime($cierre->FECHA)));
                            $cuadro[date("'".'Y-m',strtotime($cierre->FECHA))."'"]['ano']=intval(date('Y',strtotime($cierre->FECHA)));

                            /*Años de todos los cierres*/
                            if(substr_count($data['anos_aparece_cierre'], date('Y',strtotime($cierre->FECHA))) == 0 ){
                                $data['anos_aparece_cierre'].=date('Y',strtotime($cierre->FECHA)).' - ';
                            }
                        }
                    }
                }
                /*Numero de maquinas y empresas por mes*/
                if(isset($condicion)){
                    foreach($condicion as $index=>$c){
                        $numero_maquinas=DB::select('SELECT COUNT(DISTINCT(OP.referencia)) AS NUMERO_MAQUINAS FROM oportunidad_productos AS OP WHERE deleted_at IS NULL AND ('.$c.')'.$filtro_maquina);
                        $cuadro[$index]['numero_maquinas']=$numero_maquinas[0]->NUMERO_MAQUINAS;
                    }
                }
                if(isset($condicion2)){
                    foreach($condicion2 as $index2=>$c2){
                        $numero_empresas=DB::select('SELECT COUNT(DISTINCT(O.empresa_id)) AS NUMERO_EMPRESAS FROM oportunidades AS O WHERE '.$c2);
                        $cuadro[$index2]['numero_empresas']=$numero_empresas[0]->NUMERO_EMPRESAS;
                    }
                }

                /*Años de todos los cierres*/
                $data['anos_aparece_cierre']=substr($data['anos_aparece_cierre'], 0, -2);
                /*Total en Cierres*/
                $data['total'] = number_format(($data['total']/1000000), 0, '.', ',');
                /*Cierres por año*/
                if(isset($data['total_ano'])){
                    foreach($data['total_ano'] as $index=>$value){
                        $data['total_ano'][$index] = number_format(($value/1000000), 0, '.', ',');
                    }
                }
                $mes_mas_alto=0;
                $mes_mas_bajo=0;
                $data['mes_mas_alto']='';
                $data['mes_mas_bajo']='';
                $colores=array(0,1,2,3,4,5,5,5,5,4,3,2,1);
                ob_start();
                if(isset($cuadro)){
                foreach($cuadro as $c){
                        if($c['total_mes'] > $mes_mas_alto){
                            $mes_mas_alto=$c['total_mes'];
                            $data['mes_mas_alto'] = $meses_letra[$c['mes']].' del '.$c['ano'];
                        }
                    if($mes_mas_bajo==0){
                        $mes_mas_bajo=$c['total_mes'];
                        $data['mes_mas_bajo'] = $meses_letra[$c['mes']].' del '.$c['ano'];
                    }else if($c['total_mes'] < $mes_mas_bajo){
                        $mes_mas_bajo=$c['total_mes'];
                        $data['mes_mas_bajo'] = $meses_letra[$c['mes']].' del '.$c['ano'];
                    }
                ?>
                <div class="<?=$ancho?> col-sm-12">
                    <article class="material-card Blue-<?=$colores[$c['mes']]?>">
                        <h2 class="size-active">
                            <span>Total Cierre <?=$c['ano']?></span>
                            <strong>
                                <i class="fa fa-money"></i>
                                $ <?=number_format(($c['total_mes']/1000000), 0, '.', ',')?> Millon(es)
                            </strong>
                        </h2>
                        <div class="mc-content">
                            <div class="img-container">
                                <img class="img-responsive" src="<?=$request->url?>/images/mesesano/<?=$meses_letra[$c['mes']]?>.png">
                            </div>
                            <div class="mc-description">
                               Numero de máquinas: <?=$c['numero_maquinas']?>
                               Numero de empresas: <?=$c['numero_empresas']?>
                            </div>
                        </div>
                        <a class="mc-btn-action">
                            <i class="fa fa-bars"></i>
                        </a>
                        <div class="mc-footer">
                            <h4>
                                Acciones
                            </h4>
                            <a class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver Mes" onclick="ver_mes(<?=$c['ano']?>,<?=$c['mes']?>)"></a>
                        </div>
                    </article>
                </div>
                <?php }
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 1:
                /*Permiso para ver toda la informacion o ver propio*/
                $data['permiso_datos']='Ver todas';
                $modulo=3;
                $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre proyectados - datos" AND `id_permisomodulo`="'.$modulo.'"');
                if(isset($acceso1[0]->id)){
                  $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                  if(isset($cargo1[0]->id)){
                    $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
                    if(isset($permiso1[0]->permiso)){
                      if($permiso1[0]->permiso == "Ver propio"){
                        $data['permiso_datos']='Ver propio';
                      }
                    }
                  }
                }
                /*ver mes*/
                $m_s = $request->mes;
                if($m_s <= 9){
                    $m_s='0'.$m_s;
                }
                $fecha_inicio = $request->ano.'-'.$m_s.'-01';
                if($request->mes == 12){
                    $fecha_fin = (($request->ano)+1).'-01-01';
                }else{
                    $m_s=($request->mes)+1;
                    if($m_s <= 9){
                        $m_s='0'.$m_s;
                    }
                    $fecha_fin = $request->ano.'-'.$m_s.'-01';
                }
                $cierres = DB::select('SELECT O.id, (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.created_at DESC LIMIT 0,1) AS FECHA, (SELECT EM.nombre FROM empresas AS EM WHERE EM.id = O.empresa_id) AS EMPRESA, (SELECT EM.id FROM empresas AS EM WHERE EM.id = O.empresa_id) AS ID_EMPRESA, (SELECT EM.logo FROM empresas AS EM WHERE EM.id = O.empresa_id) AS EMPRESA_LOGO, O.pais AS EMPRESA_PAIS, (SELECT REPLACE(H3.value,",","") FROM historial_oportunidades AS H3 WHERE H3.key LIKE "tasa_cambio" AND H3.oportunidad_id = O.id ORDER BY H3.created_at DESC LIMIT 0,1) AS TASA FROM oportunidades AS O WHERE (SELECT H1.value FROM historial_oportunidades AS H1 WHERE H1.oportunidad_id = O.id AND H1.key LIKE "ciclo_venta" ORDER BY H1.created_at DESC LIMIT 0,1) != "0" AND (SELECT H1.value FROM historial_oportunidades AS H1 WHERE H1.oportunidad_id = O.id AND H1.key LIKE "ciclo_venta" ORDER BY H1.created_at DESC LIMIT 0,1) != "90" AND (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.created_at DESC LIMIT 0,1) >= "'.$fecha_inicio.'" AND (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.created_at DESC LIMIT 0,1) < "'.$fecha_fin.'"');
                $contador_margen=0;
                $data['margin-top']=0;
                foreach($cierres as $cierre){
                    /*aplicar permiso*/
                    $ejecutar = 1; //Si lo ejecute
                    if($data['permiso_datos'] == 'Ver propio'){
                        $query = 'SELECT HO.value AS IDRESPONSABLE FROM historial_oportunidades HO WHERE HO.key = "responsable" AND HO.oportunidad_id = '.$cierre->id.' ORDER BY HO.id DESC LIMIT 0,1';
                        $responsable = DB::SELECT($query);
                        if($responsable[0]->IDRESPONSABLE == Auth::user()->id){
                            $ejecutar = 1; //Si lo ejecute
                        }else{
                            $ejecutar = 0; //No lo ejecute
                        }
                    }
                    if($ejecutar==1){
                        $productos = DB::select('SELECT P.name, R.referencia, R.id, R.foto, (REPLACE(OP.total,",","") * '.$cierre->TASA.') AS TOTAL FROM oportunidad_productos OP INNER JOIN productos P ON OP.producto = P.id INNER JOIN producto_referencias R ON OP.referencia = R.id WHERE OP.oportunidad_id = '.$cierre->id.' AND OP.deleted_at IS NULL'.$filtro_maquina.' ORDER BY P.name ASC');
                        foreach($productos as $producto){
                            if(!isset($data['html'][$producto->id])){
                                $data['contador_maquina'][$producto->id]=1;
                                $data['total_maquina'][$producto->id]=$producto->TOTAL;
                                $contador_margen++;
                                if($contador_margen == 4 ){
                                    $data['margin-top']+=19.16;
                                    $contador_margen=1;
                                }

                                $data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA]=1;
                                ob_start();?>
                                    <div class="col-md-4 mt-5">
                                        <div class="row mr-2 border-card alto-igual">
                                            <div class="col-md-12">
                                                <div class="triangulo"></div>
                                                <div class="row banda-blue mt-5">
                                                    <div class="col-md-12 mt-2">
                                                      <h4 class="mt-4"><?=$producto->name.' - '.$producto->referencia?></h4>
                                                    </div>
                                                </div>
                                                <div class="row mt-5">
                                                    <div class="col-md-6 mt-5">
                                                        <img class="img-view" src="<?=$request->url?>/images/file/<?php if(!empty($producto->foto)){ echo 'productos/'.$producto->foto ; }else{ echo 'clientes/essi_admon.jpg'; } ?>">
                                                    </div>
                                                    <div class="col-md-6 mt-5">
                                                        <p class="color-view h1"><strong id="contador_<?=$producto->id?>">3</strong></p>
                                                        <p class="color-view h4"><i class="fa fa-database" aria-hidden="true"></i> Unidades</p>
                                                        <p class="color-view h1"><strong id="total_<?=$producto->id?>">230</strong></p>
                                                        <p class="color-view h4"><i class="fa fa-usd" aria-hidden="true"></i> Millones</p>
                                                    </div>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="row mb-5">
                                                            <div class="col-md-4">
                                                                <img class="img-view2" src="<?=$request->url?><?php if(empty($cierre->EMPRESA_LOGO)){ echo '/images/noimage.png'; }else{ echo '/images/file/empresas/principal/'.$cierre->EMPRESA_LOGO; } ?>">
                                                                <p class="h5 color-view"><?=$cierre->EMPRESA?></p>
                                                                <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i><?=$cierre->EMPRESA_PAIS?></p>
                                                                <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i><?=date("d/m/Y",strtotime($cierre->FECHA))?></p>
                                                                <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i><a id="cantidad_maquina_<?=$producto->id?>_empresa_<?=$cierre->ID_EMPRESA?>_fecha_<?=$cierre->FECHA?>">3</a></p>
                                                            </div>


                                <?php
                                $data['html'][$producto->id] = ob_get_contents();
                                ob_end_clean();
                            }else{
                                $data['contador_maquina'][$producto->id]++;
                                $data['total_maquina'][$producto->id]+=$producto->TOTAL;
                                if(isset($data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA])){
                                    $data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA]++;
                                }else{
                                    $data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA]=1;
                                    ob_start();?>
                                        <div class="col-md-4">
                                            <img class="img-view2" src="<?=$request->url?><?php if(is_null($cierre->EMPRESA_LOGO)){ echo '/images/noimage.png'; }else{ echo '/images/file/empresas/principal/'.$cierre->EMPRESA_LOGO; } ?>">
                                            <p class="h5 color-view"><?=$cierre->EMPRESA?></p>
                                            <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i><?=$cierre->EMPRESA_PAIS?></p>
                                            <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i><?=date("d/m/Y",strtotime($cierre->FECHA))?></p>
                                            <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i><a id="cantidad_maquina_<?=$producto->id?>_empresa_<?=$cierre->ID_EMPRESA?>_fecha_<?=$cierre->FECHA?>">3</a></p>
                                        </div>
                                    <?php
                                    $data['html'][$producto->id].= ob_get_contents();
                                    ob_end_clean();
                                }
                            }
                        }
                    }
                }
                if(isset($data['html'])){
                    foreach($data['html'] as $index=>$value){
                        ob_start();?>
                             </div>
                           </div>
                        </div>
                        <div class="triangulo2"></div>
                    </div>
                   </div>
                </div>
                        <?php
                        $data['html'][$index].= ob_get_contents();
                        ob_end_clean();
                        $data['total_maquina'][$index]='$ '.(number_format(($data['total_maquina'][$index])/1000000, 0, '.', ','));
                    }
                }else{
                    $data['html'][0] = "<div class='row'><div class='col-md-12 text-center'><h3>No hay cierres para este mes...</h3></div></div>";
                }

                $meses_letra=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $data['mes']=$meses_letra[$request->mes].' '.$request->ano;
                break;
        }

        return \Response::json(['data' => $data]);

    }

    public function calendario_action_cerrados(Request $request){
        /*Permiso para ver información propia o todo*/
        $permiso_calendariovendidos_datos='Ver todas';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Calendario cierre vendidos - datos" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Ver propio"){
				$permiso_calendariovendidos_datos='Ver propio';
			  }
            }
          }
        }

        $accion = $request->action;
        switch($accion){
            case 0:
                $query = 'SELECT YEAR(V.FECHACIERRE) AS value, V.IDRESPONSABLE FROM view_oportunidades V WHERE V.CICLO = 90';
                $oportunidades = DB::SELECT($query);
                foreach($oportunidades as $oportunidad){
                    /*aplicar permiso*/
                    if($permiso_calendariovendidos_datos == 'Ver propio'){
                        if($oportunidad->IDRESPONSABLE == Auth::user()->id){
                            /*Tomar el año de en que se cerro la oportunidad*/
                            $data['ano_cierre'][intval($oportunidad->value)]=$oportunidad->value;
                        }
                    }else{
                        /*Tomar el año de en que se cerro la oportunidad*/
                        $data['ano_cierre'][intval($oportunidad->value)]=$oportunidad->value;
                    }
                }

                /*Se verifica que existe $data, si no existe se envia un mensaje el cual indica que no hay datos*/
                if(!isset($data)){
                    $data['msj'] = 'No hay años';
                }
                break;
            case 1:
                /*Oportunidades que tienen almenos una vez el ciclo en 90%*/
                $query = 'SELECT DISTINCT(H.oportunidad_id) FROM historial_oportunidades H WHERE H.key LIKE "ciclo_venta" AND H.value LIKE "90"';
                $oportunidades=DB::select($query);
                foreach($oportunidades as $oportunidad){
                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1';
                    $ciclo = DB::select($query);
                    /*Comprobar si la oportunidad realmente tiene el ciclo de venta en 90%*/
                    if($ciclo[0]->value == "90"){
                        /*aplicar permiso*/
                        $ejecutar = 1; //Si lo ejecute
                        if($permiso_calendariovendidos_datos == 'Ver propio'){
                            $vista = ViewOportunidades::find($oportunidad->oportunidad_id);
                            if($vista->IDRESPONSABLE == Auth::user()->id){
                                $ejecutar = 1; //Si lo ejecute
                            }else{
                                $ejecutar = 0; //No lo ejecute
                            }
                        }
                        if($ejecutar == 1){
                            /*Si hay filtro solo buscar el producto, de lo contrario el valor completo de la oportunidad*/
                            if($request->maquina == ''){
                                /*Tomar el valor de la oportunidad*/
                                $query = 'SELECT REPLACE(H.value, ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.id DESC LIMIT 0,1';
                                $valor = DB::select($query);
                            }else{
                                /*Si existe la variable reiniciar a cero*/
                                if(isset($valor[0]->value)){
                                    $valor[0]->value = 0;
                                }
                                /*Tomar el valor de la maquina buscar*/
                                $query = 'SELECT REPLACE(OP.total, ",", "") AS value FROM oportunidad_productos OP WHERE OP.oportunidad_id = '.$oportunidad->oportunidad_id.' AND OP.producto LIKE "'.$request->maquina.'" AND OP.deleted_at IS NULL';
                                $valor_producto = DB::select($query);
                                /*Si no hay producto el valor del producto es cero*/
                                if(count($valor_producto)>0){
                                    $query = 'SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) * '.$valor_producto[0]->value.' AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "tasa_cambio" ORDER BY H.id DESC LIMIT 0,1';
                                    $valor = DB::select($query);
                                }
                            }
                            /*Tomar el año en que se cerro la oportunidad*/
                            $query = 'SELECT MONTH(H.value) AS Mes, Year(H.value) AS Ano FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.id DESC LIMIT 0,1';
                            $fecha_cierre = DB::select($query);

                            /*Verificar el año que envio*/
                            if($fecha_cierre[0]->Ano == $request->ano){
                                if(!isset($data['mes'][$fecha_cierre[0]->Mes])){
                                    $data['mes'][$fecha_cierre[0]->Mes] = isset($valor[0]->value)? intval($valor[0]->value) : 0;
                                }else{
                                    $data['mes'][$fecha_cierre[0]->Mes]+= isset($valor[0]->value)? $valor[0]->value : 0;
                                }
                            }
                        }
                    }
                }
                /*Encontrar el mayor*/
                if(isset($data['mes'])){
                    $data['mayor']['valor']=max($data['mes']);
                    $data['total'] = 0;
                    /*Encontrar el mes mayor*/
                    foreach($data['mes'] as $index=>$value){
                        $data['total']+=$value;
                        if($value == $data['mayor']['valor']){
                            $data['mayor']['mes'] = $this->mes($index);
                        }
                    }
                    $data['total'] = number_format(($data['total'] / 1000000), 0, '.', ',');
                }else{
                    $data='No hay información';
                }

                /*Encontrar el menor*/
                if(isset($data['mes'])){
                    $data['menor']['valor']=min($data['mes']);
                    /*Encontrar el mes menor*/
                    foreach($data['mes'] as $index=>$value){
                        if($value == $data['menor']['valor']){
                            $data['menor']['mes'] = $this->mes($index);
                        }
                    }
                }else{
                    $data='No hay información';
                }

                break;
            case 2:
                $filtro_maquina='';
                if(!empty($request->maquina)){
                    $filtro_maquina=' AND OP.producto = '.$request->maquina;
                }
                /*lista de calendario anual*/
                if($request->medida == 1){
                    $ancho = "col-md-3";
                }else{
                    $ancho = "col-md-2";
                }
                $cierres = DB::select('SELECT O.id AS ID_OPORTUNIDAD, (SELECT SUM(REPLACE(OP.total, ",", ""))FROM oportunidad_productos AS OP WHERE OP.oportunidad_id = O.id AND OP.deleted_at IS NULL'.$filtro_maquina.') *(SELECT REPLACE(H.value, ",", "") FROM historial_oportunidades AS H WHERE H.oportunidad_id = O.id AND H.key LIKE "tasa_cambio" ORDER BY H.id DESC LIMIT 0,1) AS TOTAL,(SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.id DESC LIMIT 0,1) AS FECHA FROM oportunidades AS O WHERE (SELECT H1.value FROM historial_oportunidades AS H1 WHERE H1.oportunidad_id = O.id AND H1.key LIKE "ciclo_venta" ORDER BY H1.id DESC LIMIT 0,1) = "90" AND (SELECT YEAR(H2.value) FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.id DESC LIMIT 0,1) = "'.$request->ano.'" ORDER BY FECHA ASC');

                foreach($cierres as $cierre){
                    /*aplicar permiso*/
                    $ejecutar = 1; //Si lo ejecute
                    if($permiso_calendariovendidos_datos == 'Ver propio'){
                        $vista = ViewOportunidades::find($cierre->ID_OPORTUNIDAD);
                        if($vista->IDRESPONSABLE == Auth::user()->id){
                            $ejecutar = 1; //Si lo ejecute
                        }else{
                            $ejecutar = 0; //No lo ejecute
                        }
                    }
                    if($ejecutar == 1){
                         if(!isset($condicion["'".date('Y-m',strtotime($cierre->FECHA))."'"])){
                            $condicion["'".date('Y-m',strtotime($cierre->FECHA))."'"]='OP.oportunidad_id = '.$cierre->ID_OPORTUNIDAD;
                             if($cierre->TOTAL > 0){
                                 $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"]='O.id = '.$cierre->ID_OPORTUNIDAD;
                             }else{
                                 $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"]='O.id = 0';
                             }
                        }else{
                            $condicion["'".date('Y-m',strtotime($cierre->FECHA))."'"].=' OR OP.oportunidad_id = '.$cierre->ID_OPORTUNIDAD;
                            if($cierre->TOTAL > 0){
                                $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"].=' OR O.id = '.$cierre->ID_OPORTUNIDAD;
                            }else{
                                $condicion2["'".date('Y-m',strtotime($cierre->FECHA))."'"].=' OR O.id = 0';
                            }
                        }

                        if(isset($cuadro["'".date('Y-m',strtotime($cierre->FECHA))."'"])){
                            $cuadro["'".date('Y-m',strtotime($cierre->FECHA))."'"]['total_mes']+=$cierre->TOTAL;
                        }else{
                            $cuadro["'".date('Y-m',strtotime($cierre->FECHA))."'"]['total_mes']=$cierre->TOTAL;

                            /*mes año de la tarjeta*/
                            $cuadro[date("'".'Y-m',strtotime($cierre->FECHA))."'"]['mes']=intval(date('m',strtotime($cierre->FECHA)));
                            $cuadro[date("'".'Y-m',strtotime($cierre->FECHA))."'"]['ano']=intval(date('Y',strtotime($cierre->FECHA)));

                        }
                    }
                }
                /*Numero de maquinas y empresas por mes*/
                if(isset($condicion)){
                    foreach($condicion as $index=>$c){
                        $numero_maquinas=DB::select('SELECT COUNT(DISTINCT(OP.referencia)) AS NUMERO_MAQUINAS FROM oportunidad_productos AS OP WHERE deleted_at IS NULL AND ('.$c.')'.$filtro_maquina);
                        $cuadro[$index]['numero_maquinas']=$numero_maquinas[0]->NUMERO_MAQUINAS;
                    }
                }
                if(isset($condicion2)){
                    foreach($condicion2 as $index2=>$c2){
                        $numero_empresas=DB::select('SELECT COUNT(DISTINCT(O.empresa_id)) AS NUMERO_EMPRESAS FROM oportunidades AS O WHERE '.$c2);
                        $cuadro[$index2]['numero_empresas']=$numero_empresas[0]->NUMERO_EMPRESAS;
                    }
                }

                $colores=array(0,1,2,3,4,5,5,5,5,4,3,2,1);
                ob_start();?>

                <?php for($i=1;$i<=12;$i++){
                    $entro = 'No';
                    if(isset($cuadro)){
                        foreach($cuadro as $c){
                            if($c['mes']==$i){
                ?>
                            <div class="<?=$ancho?> col-sm-12">
                                <article class="material-card Blue-<?=$colores[$c['mes']]?>">
                                    <h2 class="size-active">
                                        <span>Total Cierre <?=$c['ano']?></span>
                                        <strong>
                                            <i class="fa fa-money"></i>
                                            $ <?=number_format(($c['total_mes']/1000000), 0, '.', ',')?> Millon(es)
                                        </strong>
                                    </h2>
                                    <div class="mc-content">
                                        <div class="img-container">
                                            <img class="img-responsive" src="/images/mesesano/<?=$this->mes($c['mes'])?>.png">
                                        </div>
                                        <div class="mc-description">
                                           Numero de máquinas: <?=$c['numero_maquinas']?>
                                           Numero de empresas: <?=$c['numero_empresas']?>
                                        </div>
                                    </div>
                                    <a class="mc-btn-action">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                    <div class="mc-footer">
                                        <h4>
                                            Acciones
                                        </h4>
                                        <a class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver Mes" onclick="ver_mes(<?=$c['ano']?>,<?=$c['mes']?>)"></a>
                                    </div>
                                </article>
                            </div>
                <?php $entro = 'Si';
                            }
                        }
                    }
                    if($entro == 'No'){
                        if(date('Y-m',strtotime($request->ano.'-'.$i)) <= date('Y-m')){
                ?>
                            <div class="<?=$ancho?> col-sm-12">
                                <article class="material-card Blue-<?=$colores[$i]?>">
                                    <h2 class="size-active">
                                        <span>Total Cierre <?=$request->ano?></span>
                                        <strong>
                                            <i class="fa fa-money"></i>
                                            $ 0 Millon(es)
                                        </strong>
                                    </h2>
                                    <div class="mc-content">
                                        <div class="img-container">
                                            <img class="img-responsive" src="/images/mesesano/<?=$this->mes($i)?>.png">
                                        </div>
                                        <div class="mc-description">
                                           Numero de máquinas: 0
                                           Numero de empresas: 0
                                        </div>
                                    </div>
                                    <a class="mc-btn-action">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                    <div class="mc-footer">

                                    </div>
                                </article>
                            </div>
                    <?php
                        }
                    }
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 3:
                $filtro_maquina='';
                if(!empty($request->maquina)){
                    $filtro_maquina=' AND OP.producto = '.$request->maquina;
                }
                /*ver mes*/
                $m_s = $request->mes;
                if($m_s <= 9){
                    $m_s='0'.$m_s;
                }
                $fecha_inicio = $request->ano.'-'.$m_s.'-01';
                if($request->mes == 12){
                    $fecha_fin = (($request->ano)+1).'-01-01';
                }else{
                    $m_s=($request->mes)+1;
                    if($m_s <= 9){
                        $m_s='0'.$m_s;
                    }
                    $fecha_fin = $request->ano.'-'.$m_s.'-01';
                }
                $cierres = DB::select('SELECT O.id, (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.id DESC LIMIT 0,1) AS FECHA, (SELECT EM.nombre FROM empresas AS EM WHERE EM.id = O.empresa_id) AS EMPRESA, (SELECT EM.id FROM empresas AS EM WHERE EM.id = O.empresa_id) AS ID_EMPRESA, (SELECT EM.logo FROM empresas AS EM WHERE EM.id = O.empresa_id) AS EMPRESA_LOGO, O.pais AS EMPRESA_PAIS, (SELECT REPLACE(H3.value,",","") FROM historial_oportunidades AS H3 WHERE H3.key LIKE "tasa_cambio" AND H3.oportunidad_id = O.id ORDER BY H3.id DESC LIMIT 0,1) AS TASA FROM oportunidades AS O WHERE (SELECT H1.value FROM historial_oportunidades AS H1 WHERE H1.oportunidad_id = O.id AND H1.key LIKE "ciclo_venta" ORDER BY H1.id DESC LIMIT 0,1) = "90" AND (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.id DESC LIMIT 0,1) >= "'.$fecha_inicio.'" AND (SELECT H2.value FROM historial_oportunidades AS H2 WHERE H2.key LIKE "fecha_oc" AND H2.oportunidad_id = O.id ORDER BY H2.id DESC LIMIT 0,1) < "'.$fecha_fin.'"');
                $contador_margen=0;
                $data['margin-top']=0;
                foreach($cierres as $cierre){
                    /*aplicar permiso*/
                    $ejecutar = 1; //Si lo ejecute
                    if($permiso_calendariovendidos_datos == 'Ver propio'){
                        $vista = ViewOportunidades::find($cierre->id);
                        if($vista->IDRESPONSABLE == Auth::user()->id){
                            $ejecutar = 1; //Si lo ejecute
                        }else{
                            $ejecutar = 0; //No lo ejecute
                        }
                    }
                    if($ejecutar == 1){
                        $productos = DB::select('SELECT P.name, R.referencia, R.id, R.foto, (REPLACE(OP.total,",","") * '.$cierre->TASA.') AS TOTAL FROM oportunidad_productos OP INNER JOIN productos P ON OP.producto = P.id INNER JOIN producto_referencias R ON OP.referencia = R.id WHERE OP.oportunidad_id = '.$cierre->id.' AND OP.deleted_at IS NULL'.$filtro_maquina.' ORDER BY P.name ASC');
                        foreach($productos as $producto){
                            if(!isset($data['html'][$producto->id])){
                                $data['contador_maquina'][$producto->id]=1;
                                $data['total_maquina'][$producto->id]=$producto->TOTAL;
                                $contador_margen++;
                                if($contador_margen == 4 ){
                                    $data['margin-top']+=19.16;
                                    $contador_margen=1;
                                }

                                $data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA]=1;
                                ob_start();?>
                                    <div class="col-md-4 mt-5">
                                        <div class="row mr-2 border-card alto-igual">
                                            <div class="col-md-12">
                                                <div class="triangulo"></div>
                                                <div class="row banda-blue mt-5">
                                                    <div class="col-md-12 mt-2">
                                                      <h4 class="mt-4"><?=$producto->name.' - '.$producto->referencia?></h4>
                                                    </div>
                                                </div>
                                                <div class="row mt-5">
                                                    <div class="col-md-6 mt-5">
                                                        <img class="img-view" src="<?=$request->url?>/images/file/<?php if(!empty($producto->foto)){ echo 'productos/'.$producto->foto ; }else{ echo 'clientes/essi_admon.jpg'; } ?>">
                                                    </div>
                                                    <div class="col-md-6 mt-5">
                                                        <p class="color-view h1"><strong id="contador_<?=$producto->id?>">3</strong></p>
                                                        <p class="color-view h4"><i class="fa fa-database" aria-hidden="true"></i> Unidades</p>
                                                        <p class="color-view h1"><strong id="total_<?=$producto->id?>">230</strong></p>
                                                        <p class="color-view h4"><i class="fa fa-usd" aria-hidden="true"></i> Millones</p>
                                                    </div>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="row mb-5">
                                                            <div class="col-md-4">
                                                                <img class="img-view2" src="<?=$request->url?><?php if(empty($cierre->EMPRESA_LOGO)){ echo '/images/noimage.png'; }else{ echo '/images/file/empresas/principal/'.$cierre->EMPRESA_LOGO; } ?>">
                                                                <p class="h5 color-view"><?=$cierre->EMPRESA?></p>
                                                                <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i><?=$cierre->EMPRESA_PAIS?></p>
                                                                <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i><?=date("d/m/Y",strtotime($cierre->FECHA))?></p>
                                                                <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i><a id="cantidad_maquina_<?=$producto->id?>_empresa_<?=$cierre->ID_EMPRESA?>_fecha_<?=$cierre->FECHA?>">3</a></p>
                                                            </div>


                                <?php
                                $data['html'][$producto->id] = ob_get_contents();
                                ob_end_clean();
                            }else{
                                $data['contador_maquina'][$producto->id]++;
                                $data['total_maquina'][$producto->id]+=$producto->TOTAL;
                                if(isset($data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA])){
                                    $data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA]++;
                                }else{
                                    $data['cantidad_total_empresa']["cantidad_maquina_".$producto->id."_empresa_".$cierre->ID_EMPRESA."_fecha_".$cierre->FECHA]=1;
                                    ob_start();?>
                                        <div class="col-md-4">
                                            <img class="img-view2" src="<?=$request->url?><?php if(is_null($cierre->EMPRESA_LOGO)){ echo '/images/noimage.png'; }else{ echo '/images/file/empresas/principal/'.$cierre->EMPRESA_LOGO; } ?>">
                                            <p class="h5 color-view"><?=$cierre->EMPRESA?></p>
                                            <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i><?=$cierre->EMPRESA_PAIS?></p>
                                            <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i><?=date("d/m/Y",strtotime($cierre->FECHA))?></p>
                                            <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i><a id="cantidad_maquina_<?=$producto->id?>_empresa_<?=$cierre->ID_EMPRESA?>_fecha_<?=$cierre->FECHA?>">3</a></p>
                                        </div>
                                    <?php
                                    $data['html'][$producto->id].= ob_get_contents();
                                    ob_end_clean();
                                }
                            }
                        }
                    }
                }
                if(isset($data['html'])){
                    foreach($data['html'] as $index=>$value){
                        ob_start();?>
                             </div>
                           </div>
                        </div>
                        <div class="triangulo2"></div>
                    </div>
                   </div>
                </div>
                        <?php
                        $data['html'][$index].= ob_get_contents();
                        ob_end_clean();
                        $data['total_maquina'][$index]='$ '.(number_format(($data['total_maquina'][$index])/1000000, 0, '.', ','));
                    }
                }else{
                    $data['html'][0] = "<div class='row'><div class='col-md-12 text-center'><h3>No hay cierres para este mes...</h3></div></div>";
                }

                $meses_letra=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $data['mes']=$meses_letra[$request->mes].' '.$request->ano;
                break;
        }
        return \Response::json(['data' => $data]);
    }

    protected function calcular_mes_alto($array){
        $max=max($array);
        for($i=1;$i<=count($array);$i++){
            if($array[$i]==$max)
                return $i;
        }
    }
    protected function calcular_mes_bajo($array){
        $min=min($array);
        for($i=1;$i<=count($array);$i++){
            if($array[$i]==$min)
                return $i;
        }
    }
    protected function mes($mes){
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        return $meses[$mes];
    }
}
