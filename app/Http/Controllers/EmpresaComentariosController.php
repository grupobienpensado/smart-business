<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmpresaComentario;
use App\EmpresaSedeComentario;
use App\User;
use Illuminate\Support\Facades\DB;
use Auth;

date_default_timezone_set("America/Bogota");
class EmpresaComentariosController extends Controller
{
    public function getComentarios($id){
    	$datos = EmpresaComentario::where('empresa_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		//$user = User::findOrFail($value->user_id);
            $usuario = DB::SELECT('SELECT * FROM users WHERE id = '.$value->user_id);
            $user = $usuario[0];
    		$value->user = $user; 
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function saveComentario(Request $request)
    {
	    $dato = new EmpresaComentario;
		$dato->comentario = $request->comentario;
		$dato->empresa_id = $request->empresa_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

    public function getComentariosSede($id){
        $datos = EmpresaSedeComentario::where('empresa_sede_id', $id)->get();
        $model = [];
        foreach ($datos as $key => $value) {
            $user = User::findOrFail($value->user_id);
            $value->user = $user; 
            $model[] = $value;
        }
        return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function saveComentarioSede(Request $request){
        $dato = new EmpresaSedeComentario;
        $dato->comentario = $request->comentario;
        $dato->empresa_sede_id = $request->empresa_sede_id;
        $dato->user_id = Auth::user()->id;
        $dato->save();
        return \Response::json(['success' => true]);
    }
}
