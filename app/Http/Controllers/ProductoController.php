<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use App\Producto;
use App\Oportunidades;
use App\User;
use App\ProductoReferencias;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Auth;

class ProductoController extends Controller
{
    public function index(){        
		$datos = Producto::all();
		$datos_o = Oportunidades::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Productos" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('productos.list',['data' => $data, 'datos' => $datos,"oportundiades"=>$datos_o]);
    }

    public function validarMultiple(Request $request){
		$dato = Producto::where('name',"LIKE", $request->id)->get();
		return response()->json($dato[0]);
	}

    public function create(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Productos" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('productos.create',['data' => $data]);
    }

    public function view($id){ 
    	$model = Producto::findOrFail($id);
    	$referencia = ProductoReferencias::where('producto_id', $id)->get();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Productos" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('productos.view',['data' => $data, 'referencia' => $referencia, 'model' => $model]);
    }

    public function edit($id){ 
    	$model = Producto::findOrFail($id);
    	$referencia = ProductoReferencias::where('producto_id', $id)->get();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Productos" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('productos.edit',['data' => $data, 'referencia' => $referencia, 'model' => $model]);
    }
    public function eliminarReferencia($id){
        $referencia = ProductoReferencias::find($id);
        $imagen = $referencia->foto;
        $referencia->delete();
        if($imagen!=''){
            //Storage::delete('../images/file/productos/'.$imagen);
        }
        $data['msj']='Eliminado correctamente!';
        return \Response::json(['data' => $data]);
    }
    /**
	* guarda un archivo en nuestro directorio local.
	*
	* @return Response
	*/
	public function save(Request $request){
		$dato = new Producto;		
		$dato->categoria=$request->categoria;
	    $dato->name = $request->name;
	    $dato->user_id = Auth::user()->id;
	    $dato->save();

	    if(isset($request->ref) && !empty($request->ref)){
	    	$referencia = $request->ref;
	    	foreach ($referencia as $value) {
	    		if(isset($value["referencia"]) && !empty($value["referencia"])){
			    	$ref = new ProductoReferencias;
			    	$ref->descripcion=$value["descripcion"];
					$ref->referencia=$value["referencia"];
					if (isset($value["logo"]) && !empty($value["logo"])) {
						$logo = $value["logo"];
				        if (!empty($logo)) {
				            $imagen = json_decode($logo);           
				            $data = explode( ',', $imagen->data );
				            $foto = base64_decode($data[1]);

				            $antenombrelogo = uniqid();
				            $nombrelogo =$antenombrelogo.".png";
				            \Storage::disk('producto')->put($nombrelogo,  $foto);
				        
							$ref->foto = $nombrelogo;
						}
					}		    	
					
					$ref->producto_id=$dato->id;
				    $ref->save();
				}
			}
	    }

	    return redirect('/producto/crear');
	}

	public function editsave(Request $request){
		$dato = Producto::find($request->id);		
		$dato->categoria=$request->categoria;
	    $dato->name = $request->name;
	    $dato->user_id = Auth::user()->id;
	    $dato->save();

	    if(isset($request->ref) && !empty($request->ref)){

	    	/*$models = ProductoReferencias::where('producto_id', $dato->id)->get();
		    foreach ($models as $key => $value) {
		    	$model = ProductoReferencias::find($value->id);
		        $model->delete();
		    }*/

	    	$referencia = $request->ref;
	    	foreach ($referencia as $value) {
	    		if(isset($value["referencia"]) && !empty($value["referencia"])){
	    			if(isset($value["id"]) && !empty($value["id"])){
				    	$ref = ProductoReferencias::find($value["id"]);
				    	$ref->descripcion=$value["descripcion"];
						$ref->referencia=$value["referencia"];
						try {
							if (isset($value["logo"]) && !empty($value["logo"])) {	
								$logo = $value["logo"];						
						        if (isset($logo) && !empty($logo)) {
						        	
						            $imagen = json_decode($logo);           
						            $data = explode( ',', $imagen->data );
						            $foto = base64_decode($data[1]);

						            $antenombrelogo = uniqid();
						            $nombrelogo =$antenombrelogo.".png";
						            \Storage::disk('producto')->put($nombrelogo,  $foto);
						        
									$ref->foto = $nombrelogo;
								}
							}
						} catch (Exception $e) { }		    	
						
						$ref->producto_id=$dato->id;
					    $ref->save();
					}else{
                        $ref = new ProductoReferencias;
			    	$ref->descripcion=$value["descripcion"];
					$ref->referencia=$value["referencia"];
					if (isset($value["logo"]) && !empty($value["logo"])) {
						$logo = $value["logo"];
				        if (!empty($logo)) {
				            $imagen = json_decode($logo);
				            $data = explode( ',', $imagen->data );
				            $foto = base64_decode($data[1]);

				            $antenombrelogo = uniqid();
				            $nombrelogo =$antenombrelogo.".png";
				            \Storage::disk('producto')->put($nombrelogo,  $foto);

							$ref->foto = $nombrelogo;
						}
					}

					$ref->producto_id=$dato->id;
				    $ref->save();
                    }
				}
			}
	    }

	    return redirect('producto/ver/'.$dato->id);
	}

	public function find($id){
		$datos = Producto::findOrFail($id);

		$oportundiades = Oportunidades::all();
		$cuerpo='';
		foreach($oportundiades as $da){
          $array=json_decode($da->options);
          $usuario=User::findOrFail($da->user_id);
          for($i=0;$i<count($array);$i++){
          	if($array[$i]==$datos->name){
          		$cuerpo.="<tr><td>OP".$da->id."</td><td>".$da->titulo."</td><td>".ucwords($usuario->nombres." ".$usuario->apellidos)."</td><td><a class='btn btn-sm btn-success display-line' href='/oportunidad/".$da->id."' >
                      <i class='fa fa-eye white'aria-hidden='true'></i>
                    </a></td></tr>";
          	}
          }
		}

		$contenido='<div class="form-group row block" >
	          <div class="col-md-5" style="display: inline-block; text-align: center;position: relative;height: 240px;">
	            <div style="border:0.5px solid #ccc;height: 240px;width: 240px;position: absolute;right: 10%;background-image: url(../images/file/productos/'.$datos->foto.');background-size: contain;background-repeat: no-repeat;"></div>
	          </div>
	          <div class="col-md-5" style="display: inline-block; text-align: center;position: relative;height: 240px;">
	            <div style="border:0.5px solid #ccc;height: 240px;width: 240px;position: absolute;right: 10%;background-image: url(../images/file/productos/'.$datos->segunda_foto.');background-size: contain;background-repeat: no-repeat;"></div>
	          </div>
	          <div class="col-md-12">
	            <label class="titulo">Categoria: <span>'.$datos->categoria.'</span></label>
	            <p><span style="display: block;">Descripcion:</span>'.$datos->descripcion.'</p>
	          </div>
	          <div class="col-md-12">
	          <div class="table-responsive">
            	<table class="table table-striped">
            	<thead>
            		<tr>
            		<th>Oportunidad</th>
            		<th>Titulo</th>
            		<th>Responsable</th>
            		<th>Ver</th>
            		</tr>
            	</thead>
            	<tbody>'.$cuerpo.'
            	</tbody>
            	</table>
            	</div>
	          </div>
	       </div>';
          return ["tabla"=>$contenido];
	}
}
