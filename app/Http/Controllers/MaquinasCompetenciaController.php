<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaquinasCompetencia;
use Auth;

class MaquinasCompetenciaController extends Controller
{
    public function viewcreate(){
		return view('maquinascompetencia.create');
    }

    public function viewmaquina($id){
    	$model = MaquinasCompetencia::findOrFail($id);
  		return view('maquinascompetencia.view',['model' => $model]);
    }

    public function listAjax(){
    	$datos = MaquinasCompetencia::all();
  		return view('maquinascompetencia.list',['datos' => $datos]);
    }

    public function save(Request $request){
    	$dato = new MaquinasCompetencia;
		$dato->name = $request->name;
		$dato->ano_venta = $request->ano_venta;
		$dato->valor_venta = $request->valor_venta;
		$dato->modelo = $request->modelo;
		$dato->marca = $request->marca;
		$dato->capacidad_equipo = $request->capacidad_equipo;
		$dato->referencia = $request->referencia;
		$dato->empresa = $request->empresa;
		$dato->sede = $request->sede;
		$dato->cliente = $request->cliente;
		$dato->pais = $request->pais;
		$dato->departamento = $request->departamento;
		$dato->ciudad = $request->ciudad;
		$dato->direccion = $request->direccion;
		$dato->lat = $request->lat;
		$dato->lng = $request->lng;
		$dato->observaciones = $request->observaciones;
		$dato->observaciones_cliente = $request->observaciones_cliente;
		$dato->detalle = $request->detalle;
		$dato->energia = $request->energia;
		$dato->aire = $request->aire;
		$dato->agua = $request->agua;
		$dato->vapor = $request->vapor;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return redirect('/listmaquinascompetencia');
    }
}
