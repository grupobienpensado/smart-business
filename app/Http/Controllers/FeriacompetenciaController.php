<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\FeriasCompetencias;
use App\Competencia;
use App\FeriasCompetenciasArchivos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
date_default_timezone_set("America/Bogota");

class FeriacompetenciaController extends Controller
{
    public function index($id){
        $data['id'] = $id;
        $data['feria'] = Feria::find($id);
        $data['menu'] = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id, 7);
        return view('ferias.competencia.competencia',['data' => $data]);
    }

    public function accion(Request $request){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $accion = $request->accion;
        switch($accion){
            /*Listado de competencias para la feria*/
            case 0:
                $competencias = FeriasCompetencias::where('id_feria',$request->id)->get();
                ob_start(); ?>
                <div class="col-md-12">
                    <p class="h4"><em>Listado de competencias en la feria</em></p>
                </div>
                <div class="col-md-12">
                    <a class="text-success" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="abrir_modal_competencias()"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para añadir competencia a la feria')"<?php } ?> style="font-size: 6rem;cursor: pointer;" data-toggle="tooltip" data-placement="right" title="Agregar una competencia"><i class="fa fa-plus-circle"></i></a>
                </div>
               <?php if(count($competencias)==0){ ?>
                <div class="col-md-12 text-center">
                    <p class="h3"><strong><em>No hay competencias agregadas a la feria</em></strong></p>
                </div>
                <?php }else{ ?>
                <div class="col-md-12">
                  <ul class="list-unstyled activity-list">
                    <li>
                        <i class="fa fa-bell-o activity-icon pull-left"></i>
                        <p class="text-left">
                            <a href="#">Información</a> Debes dar click en una competencia para ver los  <a href="#">archivos adjuntos</a> <span class="timestamp">0 Arcchivo(s) Cargados</span>
                        </p>
                    </li>
                  </ul>
                </div>
                <?php foreach($competencias as $c){
                $competencia = Competencia::find($c->id_competencia);
                ?>
                <div class="col-sm-6 col-md-1 mb-3" style="cursor:pointer;" onclick="ver_archivos(<?=$c->id?>,<?=$competencia->id?>)">
                    <p><?php if($competencia->logo!=''){ ?><img class="img-round" src="/storage/competencias_files/profile_images/<?=$competencia->logo?>" style="width:80px;"><?php }else{ ?><img class="img-round" src="http://via.placeholder.com/80x80"><?php } ?></p>
                    <p class="h5 text-primary"><?=$competencia->nombre?></p>
                    <p class="h6">Nit: <?=$competencia->nit?></p>
                    <p><a class="text-danger" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="eliminar_competencia(<?=$c->id?>)"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para eliminar una competencia')"<?php } ?>data-toggle="tooltip" data-placement="top" title="Eliminar competencia"><i class="fa fa-trash"></i></a></p>
                </div>
                <?php } } ?>
                <div class="col-md-12">
                   <hr>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Listado de Competencias en la Base de datos y boton agregar*/
            case 1:
                $feria = Feria::find($request->id);
                $competencias = Competencia::all();
                $competencias_feria = FeriasCompetencias::where('id_feria',$request->id)->get();
                ob_start(); ?>
                <div class="col-md-12">
                    <p class="h4"><em>Listado de competencias</em></p>
                </div>
                <div class="col-md-12">
                    <a class="text-success" id="btn_agregar_competencia" style="display:none;" onclick="add_competencia()" title="Agregar una competencia"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="col-md-12" id="listado_competencias">
                    <div class="row">
                      <?php if(count($competencias) > 0){ ?>
                      <div class="col-md-11">
                          <ul class="list-unstyled activity-list">
                            <li>
                                <i class="fa fa-bell-o activity-icon pull-left"></i>
                                <p class="text-left">
                                    <a href="#">Información</a> Debes dar click en una competencia para agregar ala feria <a href="#"><?=$feria->nombre?></a> <span class="timestamp"><?=count($competencias_feria)?> Competencia(s)</span>
                                </p>
                            </li>
                          </ul>
                       </div>
                       <div class="col-md-1 text-right">
                           <a class="text-danger" onclick="close_competencia()" style="font-size:2rem; cursor:pointer;" title="Cerrar competencias"><i class="fa fa-times"></i></a>
                       </div>
                        <?php foreach($competencias as $competencia){
                        $competencia_feria = FeriasCompetencias::where('id_feria',$request->id)->where('id_competencia',$competencia->id)->get();
                        $clase = count($competencia_feria)==0? 'no-select' : 'select';
                        $titulo = count($competencia_feria)==0? 'Agregar' : 'Competencia agregada';
                        ?>
                        <div class="col-sm-6 col-md-4 mb-3 <?=$clase?>" <?php if(count($competencia_feria)==0){ ?>onclick="competencia_agregar(<?=$competencia->id?>,'<?=$competencia->nombre?>')" <?php } ?> >
                            <p title="<?=$titulo?>"><?php if($competencia->logo!=''){ ?><img class="img-round" src="/storage/competencias_files/profile_images/<?=$competencia->logo?>" style="width:80px;"><?php }else{ ?><img class="img-round" src="http://via.placeholder.com/80x80"><?php } ?></p>
                            <p class="h5 text-primary"><?=$competencia->nombre?></p>
                            <p class="h6">Nit: <?=$competencia->nit?></p>
                        </div>
                        <?php
                        }
                        }else{ ?>
                        <div class="col-md-12">
                            <p class="h5"><em>No hay ninguna competencia</em></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                           <hr>
                        </div>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Agregar una competencia a la feria*/
            case 2:
                $dato = new FeriasCompetencias;
                $dato->id_feria = $request->id_feria;
                $dato->id_competencia = $request->id;
                if($dato->save()){
                    $data['msj'] = "Guardado correctamente!";
                }else{
                    $data['msj'] = "Error al guardar!";
                }
                break;
            /*Lista de archivos según competencia seleccionada*/
            case 3:
                $archivos = FeriasCompetenciasArchivos::where('id_feria_competencia',$request->id)->get();
                $feria = Feria::find($request->id_feria);
                $competencia = Competencia::find($request->id_competencia);
                ob_start(); ?>
                <div class="col-md-12">
                    <p class="h4"><strong><em>Archivos de la competencia <?=$competencia->nombre?> en la feria <?=$feria->nombre?></em></strong></p>
                </div>
                <div class="col-md-12">
                    <a class="text-success" style="font-size:6rem; cursor:pointer;" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="add_archivo(<?=$request->id?>)"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para agregar archivos')"<?php } ?> data-toggle="tooltip" data-placement="right" title="Agregar archivos"><i class="fa fa-plus-circle"></i></a>
                </div>
                <?php if(count($archivos)==0){ ?>
                <div class="col-md-12 text-center">
                    <p class="h5"><strong>No hay archivos</strong></p>
                </div>
                <?php }else{ ?>
                <div class="col-md-12">
                    <div class="row">
                <?php foreach($archivos as $archivo){
                        $extension=explode('.',$archivo->nombre_archivo);
                        if($extension[1]=='svg' || $extension[1]=='SVG' || $extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                            $icono="image.png";
                            $etiqueta_a='<a data-gallery href="/storage/ferias/competencia/'.$archivo->archivo.'"><img class="icono-video" src="/storage/ferias/competencia/'.$archivo->archivo.'" style="background: none;"></a>';
                        }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                            $icono="video-file.png";
                            $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;cursor:pointer;" onclick="ver_video(\'/storage/ferias/competencia/'.$archivo->archivo.'\')">';
                        }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                            $icono="pdf.png";
                            $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" onclick="ver_pdf(\'/storage/ferias/competencia/'.$archivo->archivo.'\')" style="background: none;cursor:pointer;">';
                        }else{
                            $icono="";
                            $etiqueta_a="";
                        }
                        ?>
                    <div class="col-sm-6 col-md-2 mb-3">
                       <div class="row">
                           <div class="col-md-12 col-archivo">
                               <p><?=$etiqueta_a?></p>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-md-12">
                               <p class="h5 text-primary"><?=$archivo->nombre_archivo?></p>
                               <p class="h6 text-secondary"><strong><?=$this->fecha(date('Y-m-d',strtotime($archivo->created_at)))?></strong></p>
                               <p><a class="text-danger" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> onclick="eliminar_archivo(<?=$archivo->id?>,'<?=$archivo->nombre_archivo?>')" <?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para eliminar archivos de la competencia')"<?php } ?> style="font-size:2rem;cursor:pointer;" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash"></i></a></p>
                           </div>
                       </div>
                    </div>
                <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <div class="col-md-12">
                    <hr>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Dropzone para cargar archivos*/
            case 4:
                $competencia = FeriasCompetencias::find($request->id);
                ob_start(); ?>
                <form class="dropzone" action="/cargarmultimediacompetenciaferia"  enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?=$request->_token?>">
                    <input type="hidden" id="id_ferias_competencias_carga" name="id" value="<?=$request->id?>">
                    <input type="hidden" id="id_competencia_carga" value="<?=$competencia->id_competencia?>">
                </form>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Eliminar un archivo*/
            case 5:
                $archivo = FeriasCompetenciasArchivos::find($request->id);
                $ferias_competencia = FeriasCompetencias::find($archivo->id_feria_competencia);
                $competencia = Competencia::find($ferias_competencia->id_competencia);
                Storage::delete('ferias/competencia/'.$archivo->archivo);
                FeriasCompetenciasArchivos::destroy($request->id);
                $data['msj'] = 'Eliminado correctamente!';
                $data['id'] = $ferias_competencia->id;
                $data['id_competencia'] = $competencia->id;
                break;
            /*Eliminar la competencia*/
            case 6:
                $ferias_competencia = FeriasCompetencias::find($request->id);
                $archivos = FeriasCompetenciasArchivos::where('id_feria_competencia',$request->id)->get();
                foreach($archivos as $archivo){
                    Storage::delete('ferias/competencia/'.$archivo->archivo);
                    FeriasCompetenciasArchivos::destroy($archivo->id);
                }
                if($ferias_competencia->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar';
                }
                break;
        }
        return \Response::json(['data'=>$data]);
    }
    function fecha($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $f=explode('-',$fecha);
            $fecha_formato = $f[2].' '.$meses[intval($f[1])].' '.$f[0];
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }

    }
}
