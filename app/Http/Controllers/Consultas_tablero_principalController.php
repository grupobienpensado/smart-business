<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\stdClass;
use App\User;
use Auth;

class Consultas_tablero_principalController extends Controller
{
	public function OportunidadesIdentificadas(){
		$usuarios = User::all();
		$y=0;
		foreach($usuarios as $usuario){ 
			if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
				$nombres=explode(" ",$usuario->nombres);

				$usuario1[$y]['nombre']='';
				$usuario1[$y]['valor']=0;
			     
			    $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE value = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
			       
			    foreach ($mis_oportunidades as $m_o) {
			        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
			        if(($oportunidad_buscar[0]->value)==$usuario->id){
			            $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
			            if(count($plazo)>0){
			              $dias=(strtotime($plazo[0]->value))-date('Y-m-d');
			              $dias=$dias/86400;
			              $dias=(int)$dias;
			              $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "probabilidad" order by id DESC');
			              $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "valor_oportunidad" order by id DESC');
			              $moneda=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "moneda" order by id DESC');
			              if(($moneda[0]->value)!='COP'){
			                $cantidad=str_replace(",", "", $valor[0]->value);
			                $valor_total=moneda($cantidad,$moneda[0]->value);
			                $valor_total=(int)$valor_total;
			              }else{
			                $valor_total=$valor[0]->value;
			                $valor_total=str_replace(",", "", $valor_total);
			                
			                $valor_total=(int)$valor_total;
			              }
			              $valor_total=$valor_total/1000000;
			              $valor_total=(int)$valor_total;

			              
			              $usuario1[$y]['valor']+= $valor_total;
			              			              
			            }
			          }
			        }
			        $usuario1[$y]['nombre'] = $nombres[0];
			    $y++;
			}
		} ?>
		<link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/morris.css">
		<div id="donut-example" class="graph"></div>
		<script type="text/javascript" src="{{ url('/') }}/js/material/morris.min.js"></script>
		<script src="{{ url('/') }}/components/amchart/js/morris.min.js"></script>
		<script type="text/javascript">
			Morris.Donut({
			  element: 'donut-example',
			  data: [
			  <?php foreach($usuario1 as $usr){ ?>
			    {label: "<?php echo $usr['nombre']; ?>", value: <?php echo $usr['valor']; ?>},
			  <?php } ?>
			  ]
			});
		</script>
		<?php
		return \Response::json(['usuario' => $usuario1]);   
	}
}

function moneda($cantidad,$moneda_origen)
{   
    $get = file_get_contents("https://www.google.com/finance/converter?a=$cantidad&from=$moneda_origen&to=COP");
    $get = explode("<span class=bld>",$get);
    $get = explode("</span>",$get[1]);  
    return preg_replace("/[^0-9\.]/", null, $get[0]);

}
?>