<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Http\Controllers\Controller;
use App\FeriasPlaneacione;
use App\FeriasPlaneacionesResponsable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class PlaneacionController extends Controller
{

    /**
     * Vistainicial ferias-planeación
     * @return view planeacion.blade.php
     */
    public function index($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $data['menu'] = $menu = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id, 8);
        $data['id'] = $id;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.planeacion', ['data' => $data]);
    }

    public function ActivityActions(Request $request){
        if(isset($request->action)){
            $action = intval($request->action);
            if($action === 1){
                $activity = new FeriasPlaneacione;

                $activity->fecha_inicio = $request->fecha_inicio.":00";
                $activity->fecha_fin = $request->fecha_fin.":00";
                $activity->titulo = $request->titulo;
                $activity->id_feria = $request->id;
                if(isset($request->descripcion)){
                    $activity->descripcion = $request->descripcion;
                }
                if($activity->save()){
                    $last_id = $activity->id;
                    if(isset($request->responsable)){
                    foreach($request->responsable as $val){
                        $respon = new FeriasPlaneacionesResponsable;
                        $respon->nom_responsable = $val;
                        $respon->id_planeacion = $last_id;
                        $respon->save();
                    }
                }
                     return \Response::json(['res' => 1, 'msj' => "Guardado exitosamente"]);
                }
            }else if($action === 2){
                if(isset($request->id)){
                    $id = $request->id;
                    $activity = FeriasPlaneacione::find($id);
                    if(isset($request->fecha_inicio)){
                        $date = explode('(', $request->fecha_inicio);
                        $date = date_create($date[0]);
                        $date_start = date_format($date, 'Y-m-d H:i:s');
                        $activity->fecha_inicio = $date_start;
                    }
                    if(isset($request->fecha_fin)){
                        $date2 = explode('(', $request->fecha_fin);
                        $date2 = date_create($date2[0]);
                        $date_end = date_format($date2, 'Y-m-d H:i:s');
                        $activity->fecha_fin = $date_end;
                    }
                    if($activity->save()){
                        return \Response::json(['res' => 1, 'msj' => "Editado exitosamente"]);
                    }
                }
            }else if($action === 3){
                if(isset($request->id)){
                    $activity = FeriasPlaneacione::find($request->id);
                    if($activity->delete()){
                        return \Response::json(['res' => 1, 'msj' => "Eliminado exitosamente"]);
                    }
                }
            }
        }
    }

    public function TimeLineData(Request $request){
        if(isset($request->id)){
            $id = $request->id;
            $activity = FeriasPlaneacione::where('id_feria',$id)->get();
            $data = array();
            foreach($activity as $key => $val){
                array_push($data, array(
                    'id' => $val->id,
                    'content' => $val->titulo,
                    'start' => $val->fecha_inicio,
                    'end' => $val->fecha_fin
                ));
            }
            return \Response::json(['res' => 1, 'data' => $data]);
        }
    }
}
