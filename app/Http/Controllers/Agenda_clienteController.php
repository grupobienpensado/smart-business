<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\VisitasClientes;
use App\Agenda_cliente;
use App\Cliente;
use App\Agenda_clientes_actividade;
use App\Agenda_clientes_visitante;
use App\EmpresaSedes;
Use App\Empresa;
Use App\Oportunidades;
use App\Agenda_clientes_actividades_responsable;
use App\Agenda_clientes_archivo;
use App\User;
use Illuminate\Support\Facades\Mail;
use Auth;
class Agenda_clienteController extends Controller
{
    //

    public function AjaxAgendaVisita($id)
    {
        $dato = Agenda_clientes_actividade::find($id);
        $agendaclient = App\Agenda_cliente::find($dato->agenda_clientes);
        return \Response::json(['success' => true, 'detalle' => $dato->descripcion, 'oportunidad' => $agendaclient->oportunidad]);
    }

    public function home(){
        $modulo=12;
        $permiso_agendar='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón agendar reunión" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $permiso_agendar="Si";
              }else{
                $permiso_agendar="No";
              }
            }
          }
        }
        /*Permiso ver datos */
        $data['permiso_datos']='Ver propio';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Ver datos" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver todas"){
                $data['permiso_datos']="Ver todas";
              }
            }
          }
        }

        if($data['permiso_datos']=="Ver todas"){
            $datos=Agenda_cliente::all();
        }else{
            $datos=Agenda_cliente::where('user_id',Auth::user()->id)->get();
        }

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Visitas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('agendaclientes.list',["data" => $data, "lista"=>$datos, 'permiso_agendar'=>$permiso_agendar]);
    }

    public function sendMail($id)
    {
        $userResponsables = [];
        $actividades = Agenda_clientes_actividade::where("agenda_clientes",$id)->get();
        foreach ($actividades as $actividad) {
            $responsables = Agenda_clientes_actividades_responsable::where("agenda_clientes_actividades",$actividad->id)->get();
            foreach ($responsables as $responsable) {
                $validUser = false;
                foreach ($userResponsables as $userResponsable) {
                    if ($userResponsable == $responsable->responsable) {
                        $validUser = true;
                    }
                }
                if (!$validUser) {
                    $userResponsables[]=$responsable->responsable;
                }
            }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Visitas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('agendaclientes.sendmail',["data" => $data, "userResponsables"=>$userResponsables, "id"=>$id]);
    }

    public function index($id){
        $datos          = Agenda_cliente::find($id);
        $oportunidad    = Oportunidades::find($datos->oportunidad);
        $empresa        = Empresa::find($oportunidad->empresa_id);
        $visitantes     = Agenda_clientes_visitante::where("agenda_clientes", $id)->get();
        $descripciones  = Agenda_clientes_actividade::where("agenda_clientes", $id)->orderBy('fecha')->get();
        $archivos       = Agenda_clientes_archivo::where("agenda_clientes", $id)->get();

        for ($i=0; $i < count($visitantes); $i++) {
            $cliente = Cliente::find($visitantes[$i]->cliente);
            $visitantes[$i]->nombres = $cliente->nombres;
            $visitantes[$i]->apellidos = $cliente->apellidos;
            $visitantes[$i]->cargo = $cliente->cargo;
            $visitantes[$i]->tratamiento = $cliente->tratamiento;
            $visitantes[$i]->foto = $cliente->foto;
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Visitas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('agendaclientes.view',["data" => $data, "datos"=>$datos,"oportunidad"=>$oportunidad,"descripciones"=>$descripciones,"id"=>$id,"empresa"=>$empresa,"visitantes"=>$visitantes,"archivos"=>$archivos]);
    }

    public function agendar(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Visitas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        $modulo=12;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="crear nueva agenda" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                return view('registrarvisita',["data" => $data]);
              }else{
                return view('oportunidades.nopermiso',["data" => $data]);
              }
            }
          }
        }
    	//return view('registrarvisita');
    }
    public function asignarindex($id){
    	$actividades=Agenda_clientes_actividade::all();
    	$usuarios=User::all();
        $visitantes=Agenda_clientes_visitante::all();
    	/*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Visitas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view("agendaclientes.responsable",["data" => $data, "actividades"=>$actividades,"usuarios"=>$usuarios,"id"=>$id,"visitantes"=>$visitantes]);
    }
    public function buscar_cliente($id){
    	$datos         =   Oportunidades::find($id);
    	$clientes      =   Cliente::where("empresa_id", $datos->empresa_id)->get();
    	$i             =   0;
    	$contenido     =   '';
    	$entrar        =   '';
        $foto_perfil   =   '';


        $ji = 0;
		foreach ($clientes as $key) {
            if (empty($key->foto)) {
                $foto_perfil = '<div class="media-left pull-left"><img src="http://via.placeholder.com/40x40?text=Foto" class="media-object img-circle"></div>';
            }else{
                $foto_perfil = '<div class="media-left pull-left"><img src="'.url("/").'/images/file/clientes/'.$key->foto.'" alt="'.$key->cliente.'" class="media-object img-circle" style="max-width:40px"></div>';
            }

			$contenido.='
                <label class="custom-control custom-radio">
                    <input name="listado_cliente['.$i.'][cliente]" type="checkbox" value="'.$key->id.'" class="rad custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <div class="media clearfix header-bottom custom-control-description">
                        '.$foto_perfil.'
                        <div class="media-body">
                            <a href="#">'.$key->tratamiento." ".$key->cliente.'</a><br>
                            <p class="text-muted username soloprimer normalp">'.$key->cargo.'</p>
                        </div>
                    </div>
                </label>';
            $i++;
		}

    	return ["clientes"=>$contenido];
    }
    public function save(Request $request){
    	$datos=new Agenda_cliente();
    	$datos->oportunidad=$request->cliente;
    	$datos->fecha_inicio=$request->fecha_inicio;
    	$datos->fecha_final=$request->fecha_final;
    	$datos->observaciones=$request->observaciones;
    	$datos->estado="pendiente";
    	$datos->user_id=Auth::user()->id;
    	$datos->save();
    	$id=$datos->id;
        if (isset($request->listado_cliente) && !empty($request->listado_cliente)) {
        	foreach ($request->listado_cliente as $key) {
        		$itm=new Agenda_clientes_visitante();
        		$itm->agenda_clientes=$id;
        		$itm->cliente=$key["cliente"];
        		$itm->user_id=Auth::user()->id;
        		$itm->save();
        	}
        }
        if (isset($request->datos) && !empty($request->datos)) {
        	foreach ($request->datos as $key) {
        		$item=new Agenda_clientes_actividade();
        		$item->agenda_clientes=$id;
        		$item->fecha=$key["dia"];
        		$item->hora_inicio=$key["hora_inicio"];
        		$item->hora_fin=$key["hora_fin"];
        		$item->descripcion=$key["descripcion"];
        		$item->user_id=Auth::user()->id;
        		$item->save();
        	}
        }
    	//return redirect('/asignarresponsable/'.$id);
        return \Response::json(['success' => true, 'id' => $id]);

    }
    public function saveresponsable(Request $request){
    	$datos=Agenda_cliente::findOrFail($request->id);
    	$datos->estado="asignada";
    	$datos->save();

        foreach ($request->visitantes as $key) {
            # code...
            $item=Agenda_clientes_visitante::findOrFail($key["id"]);
            $item->observaciones=$key["observaciones"];
            $item->user_id=Auth::user()->id;
            $item->save();
        }

    	foreach ($request->datos as $key) {
            $valid = Agenda_clientes_actividades_responsable::where("agenda_clientes_actividades",$key["actividad"])
                                                            ->where("responsable",$key["usuario"])
                                                            ->get();
            $valcont = count($valid);
            if ($valcont == 0) {
        		$item                             = new Agenda_clientes_actividades_responsable();
        		$item->agenda_clientes_actividades= $key["actividad"];
        		$item->responsable                = $key["usuario"];
        		$item->user_id                    = Auth::user()->id;
        		$item->save();
            }
    	}

        $logo = $request->file('file-1');
        if ($logo) {
            for ($i=0;$i<count($logo);$i++) {
                # code...
                $antenombrelogo = uniqid();
                $extensionlogo = $logo[$i]->getClientOriginalExtension();
                $nombrelogo =$antenombrelogo.".".$extensionlogo;
                \Storage::disk('agenda')->put($nombrelogo,  \File::get($logo[$i]));

                $dato = new Agenda_clientes_archivo;
                $dato->agenda_clientes  = $request->id;
                $dato->archivo=$nombrelogo;
                $dato->user_id = Auth::user()->id;
                $dato->save();
            }

        }



        return redirect('enviarcorreo/'.$request->id);
    }

    public function sendMailyes(Request $request)
    {
        $datos=Agenda_cliente::findOrFail($request->id);
        $oportunidad    = Oportunidades::find($datos->oportunidad);
        $descripciones  = Agenda_clientes_actividade::where("agenda_clientes", $datos->id)->orderBy('fecha')->get();
        setlocale(LC_ALL, "es_CO.UTF-8");
        $fechaacti = strftime("%A %d %B %Y", strtotime($descripciones[0]->fecha));
        $tituloAsunto = "Visita ".$oportunidad->empresa." ".$oportunidad->pais." - ".$fechaacti;
        $emails = [];
        foreach($request->emails as $email){
            if(!empty($email)){
                $emails[]=$email;
            }
        }
        $data = array('id' => $request->id);
        Mail::send('mail.mailvisitacliente', $data, function($message) use ($emails, $tituloAsunto){
            $message->from('smart@bounces.smartessi.com', 'Smart Business ESSI');
            $message->bcc($emails)->subject($tituloAsunto);
        });
        return redirect('agendacliente/'.$request->id);
    }

    public function viewMail($id){
        return view('mail.mailvisitacliente', ['id' => $id]);
    }

    public function buscar_actividad($id){
        $fecha=$_GET["fecha"];
        $descripciones=Agenda_clientes_actividade::all();
        $participantes=Agenda_clientes_actividades_responsable::all();

        $contenido='<table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display"><thead><tr><th class="text-center">#</th><th>Actividad</th><th class="text-center">Participantes</th><th class="text-center">Hora Inicio</th><th class="text-center">Hora Final</th></tr></thead><tbody>';
        $i=1; $h=0; foreach ($descripciones as $key) {
            # code...
            if($key->agenda_clientes==$id&&$key->fecha==$fecha){
                $respo=[];
                $h=0;
                foreach ($participantes as $value) {
                    # code...
                    if($value->agenda_clientes_actividades==$key->id){

                         $usuarios=User::all();
                         foreach ($usuarios as $us) {
                             # code...
                            if($us->id==$value->responsable){
                                $respo[$h]=$us;
                            }
                         }
                         $h++;
                    }
                }
                $contenido.='<tr>
                        <td class="text-center">'.$i.'</td>
                        <td>'.$key->descripcion.'</td>
                        <td class="text-center" onclick="ver_participantes('.$i.')">'.$h.' Funcionarios </td>
                        <td class="text-center">'.$key->hora_inicio.'</td>
                        <td class="text-center">'.$key->hora_fin.'</td></tr>
                        <tr style="display:none" class="tr" id="tr-'.$i.'"><td colspan="5">
                        <ul>';
                        for($k=0; $k<count($respo);$k++) {
                            # code...
                            $contenido.="<li>".$respo[$k]->username."</li>";
                        }
                        $contenido.='</ul></td>
                        </tr>';
                        $i++;
            }
        }

        return ["tabla"=>$contenido];

    }

    public function uploadfiles(Request $request)
    {

        $logo = $request->file('file-1');
        if ($logo) {
            for ($i=0;$i<count($logo);$i++) {
                # code...
                $antenombrelogo = uniqid();
                $extensionlogo = $logo[$i]->getClientOriginalExtension();
                $nombrelogo =$antenombrelogo.".".$extensionlogo;
                \Storage::disk('agenda')->put($nombrelogo,  \File::get($logo[$i]));

                $dato = new Agenda_clientes_archivo;
                $dato->agenda_clientes  = $request->id;
                $dato->archivo=$nombrelogo;
                $dato->user_id = Auth::user()->id;
                $dato->save();
            }

        }

        return redirect('agendacliente/'.$request->id);
    }
}
