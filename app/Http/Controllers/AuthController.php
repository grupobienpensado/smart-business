<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;

class AuthController extends Controller
{
     use AuthenticatesUsers;
    /**
     * Muestra el formulario para login.
     */
    public function showLogin()
    {
        // Verificamos que el usuario no esté autenticado
        if (Auth::check())
        {
            // Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
            return Redirect('/comercial');
        }
        // Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
        return view('/login');
    }
    /**
     * Valida los datos del usuario.
     */
    public function postLogin(Request $request)
    {
        // Guardamos en un arreglo los datos del usuario.
        $userdata = array(
            'username' => $request->username,
            'password'=> $request->password,
        );
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
        if(Auth::attempt($userdata))
        {
        	$dato = User::findOrFail(Auth::user()->id);
			//$dato->remember_token = sha1(date("Y-m-d H:i:s")."-".Auth::user()->name."-".Auth::user()->id);
			$dato->save();
			//return User::findOrFail(Auth::user()->id);
            // De ser datos válidos nos mandara a la bienvenida
            return Redirect('/comercial');
        }
        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
        return Redirect('/login')
                    ->with('mensaje_error', 'Tus datos son incorrectos')
                    ->withInput();
    }

    /**
     * Muestra el formulario de login mostrando un mensaje de que cerró sesión.
     */
    public function logOut()
    {
        Auth::logout();
        return Redirect('/login')
                    ->with('mensaje_error', 'Tu sesión ha sido cerrada.');
    }

    public function showUser($id)
    {
        $dato = User::findOrFail($id);        
        return \Response::json(['success' => true, 'user' => $dato]);
    }
}
