<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Empresa;
use App\EmpresaSedes;
use App\Cliente;
use App\Paises;
use App\Estados;
use App\Ciudades;
use App\User;
use App\Producto;
use App\Oportunidades;
use App\ProductoReferencias;
use App\Users_otro_crm;
use App\SubtipoActividad;
use Auth;

class selec2Controller extends Controller
{
    /**
	*
	*
	* @return Response
	*/
	public function empresa(Request $request)
  {
      $term = $request->term ?: '';
			$pais = Empresa::where('nombre', 'like', '%'.$term.'%')->pluck('nombre', 'id');
			$valid_tags = [];
			foreach ($pais as $id => $tag) {
				$valid_tags[] = ['id' => $id, 'text' => $tag];
			}
			return response()->json($valid_tags);
  }

	public function subactividad(Request $request)
  {
      $term = $request->term ?: '';
			$pais = SubtipoActividad::where('tipo', 'like', '%'.$term.'%')->pluck('concepto', 'id');
			$valid_tags = [];
			foreach ($pais as $id => $tag) {
				$valid_tags[] = ['id' => $id, 'text' => $tag];
			}
			return response()->json($valid_tags);
  }

    public function sede(Request $request)
    {
        $term = $request->term ?: '';
		$state = $request->state ?: '';
		$idE = Empresa::where('id', $state)->pluck('nombre');
		$sede = EmpresaSedes::where('empresa_id', $state)
		              ->where('ciudad', 'like', '%'.$term.'%')
		              ->pluck('ciudad', 'id');
		$valid_tags = [];
		foreach ($sede as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $idE[0]."/".$tag];
		}
		return \Response::json($valid_tags);
    }

    public function pais(Request $request)
    {
        $term = $request->term ?: '';
		$pais = Paises::where('name', 'like', $term.'%')->pluck('name', 'id');
		$valid_tags = [];
		foreach ($pais as $id => $tag) {
		  $valid_tags[] = ['id' => $tag, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }

    /**
	*
	*
	* @return Response
	*/
	public function estados(Request $request)
    {
        $term = $request->term ?: '';
		$state = $request->state ?: '';
		$state = str_replace("%20", " ", $state);
		$id = Paises::where('name','like', $state)->pluck('id');
		$estados = Estados::where('country_id', $id)
		              ->where('name', 'like', $term.'%')
		              ->pluck('name', 'id');
		$valid_tags = [];
		foreach ($estados as $id => $tag) {
		  $valid_tags[] = ['id' => $tag, 'text' => $tag];
		}
		return \Response::json($valid_tags);
    }

    /**
	*
	*
	* @return Response
	*/
	public function ciudades(Request $request)
    {
        $term = $request->term ?: '';
        $pais = $request->pais ?: '';
		$estado = $request->estado ?: '';
		$estado = str_replace("%20", " ", $estado);
		$estado = str_replace("+", " ", $estado);
		$idP = Paises::where('name','like', $pais)->pluck('id');
		$idE = Estados::where('name','like',$estado)->where('country_id','=',$idP)->pluck('id');
		$ciudades = Ciudades::where('state_id', $idE)
		              ->where('name', 'like', $term.'%')
		              ->pluck('name', 'id');
		$valid_tags = [];
		foreach ($ciudades as $id => $tag) {
		  $valid_tags[] = ['id' => $tag, 'text' => $tag];
		}
		return \Response::json($valid_tags);
    }

     /**
	*
	*
	* @return Response
	*/
	public function personal(Request $request)
    {
        $term = $request->term ?: '';
		$personal = User::where('name', 'like', $term.'%')
		              ->where('tipo', '<>', 'Administrator')
		              ->pluck('name', 'id');
		$valid_tags = [];
		foreach ($personal as $id => $tag) {
		$valid_tags[] = ['id' => $tag, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }


    public function usuarios(Request $request)
    {
        $term = $request->term ?: '';
		$usuarios = User::where('name', 'like', $term.'%')
						  ->where('cargo', 'like', 'Ejecutivo Comercial')
						  ->orWhere('cargo', 'like', 'Partner Solution')
						  ->orWhere('cargo', 'like', 'Gerente Comercial')
                          ->orWhere('cargo', 'like', 'Administrador Smart')
		              ->pluck('name', 'id');
		$valid_tags = [];

		foreach ($usuarios as $id => $tag) {
			$valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		$valid_tags[] = ['id' => '0', 'text' => 'Otro'];
		return response()->json($valid_tags);
    }
    /*Usuarios Solo para plan de trabajo*/
    public function usuariosplantrebajo(Request $request){
        $term = $request->term ?: '';
        $query = 'SELECT U.name, U.id FROM users U WHERE U.deleted_at IS NULL AND U.cargo IN (SELECT C.cargo FROM permiso_modulo_filtros F INNER JOIN permiso_cargos C ON F.id_cargo = C.id WHERE F.deleted_at IS NULL AND F.id_permiso_modulo = 9) AND U.cargo != "'.Auth::user()->cargo.'" AND U.name LIKE "'.$term.'%" ORDER BY U.id ASC';
		$usuarios = DB::SELECT($query);
		$valid_tags = [];

        foreach ($usuarios as $user) {
			$valid_tags[] = ['id' => $user->id, 'text' => $user->name];
		}
		return response()->json($valid_tags);
    }

    public function usuarios2(Request $request)
    {
        $term = $request->term ?: '';
		$usuarios = User::where('name', 'like', $term.'%')
						  ->where('cargo', 'like', 'Ejecutivo Comercial')
						  ->orWhere('cargo', 'like', 'Partner Solution')
						  ->orWhere('cargo', 'like', 'Gerente Comercial')
		              ->pluck('name', 'id');
		$valid_tags = [];

		foreach ($usuarios as $id => $tag) {
			$valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }

    public function usuarios3(Request $request)
    {
        $term = $request->term ?: '';
		$usuarios = User::where('name', 'like', $term.'%')
						  ->where('cargo', '!=', Auth::user()->cargo)
						  ->where('cargo', 'like', 'Ejecutivo Comercial')
						  ->orWhere('cargo', 'like', 'Partner Solution')
						  ->orWhere('cargo', 'like', 'Gerente Comercial')
						  ->orWhere('cargo', 'like', 'Director General')
		              ->pluck('name', 'id');
		$valid_tags = [];

		foreach ($usuarios as $id => $tag) {
			$valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }
    /*Usuarios Solo para comercial*/
    public function usuarioscomercialfiltro(Request $request)
    {
        $term = $request->term ?: '';
        $query = 'SELECT U.name, U.id FROM users U WHERE U.deleted_at IS NULL AND U.cargo IN (SELECT C.cargo FROM permiso_modulo_filtros F INNER JOIN permiso_cargos C ON F.id_cargo = C.id WHERE F.deleted_at IS NULL AND F.id_permiso_modulo = 1) AND U.cargo != "'.Auth::user()->cargo.'" AND U.name LIKE "'.$term.'%" ORDER BY U.id ASC';
		$usuarios = DB::SELECT($query);
		$valid_tags = [];

        foreach ($usuarios as $user) {
			$valid_tags[] = ['id' => $user->id, 'text' => $user->name];
		}
		return response()->json($valid_tags);
    }

    public function otros(Request $request)
    {
        $term = $request->term ?: '';
		$usuarios = User::where('name', 'like', $term.'%')
						  ->where('tipo_user', 'like', 'otro')
		              ->pluck('name', 'id');
		$valid_tags = [];

		foreach ($usuarios as $id => $tag) {
			$valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }
     /**
	*
	*
	* @return Response
	*/
	public function cliente(Request $request)
    {
        $term = $request->term ?: '';
		$dato = $request->dato ?: '';
		$cliente = Cliente::where('empresa_sede_id', $dato)
						  ->where('cliente', 'like', '%'.$term.'%')
		                  ->pluck('cliente', 'id');
		$valid_tags = [];
		foreach ($cliente as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }



    public function cliente1(Request $request)
    {
        $term = $request->term ?: '';
		$dato = $request->dato ?: '';
		$dato = EmpresaSedes::findOrFail($dato);
		$datos = EmpresaSedes::where('empresa_id', '=', $dato->empresa_id)->get();
		$valid_tags = [];
		foreach ($datos as $sede) {
			$cliente = Cliente::where('empresa_sede_id', $sede->id)
							  ->where('cliente', 'like', '%'.$term.'%')
			                  ->pluck('cliente', 'id');
					foreach ($cliente as $id => $tag) {
			  $valid_tags[] = ['id' => $id, 'text' => $tag];
			}
		}
		return response()->json($valid_tags);
    }

    public function cliente2(Request $request)
    {
        $term = $request->term ?: '';
		$cliente = Cliente::where('cliente', 'like', '%'.$term.'%')
		                  ->pluck('cliente', 'id');
		$valid_tags = [];
		foreach ($cliente as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }
     /**
	*
	*
	* @return Response
	*/
	public function sproducto(Request $request)
    {
        $term = $request->term ?: '';
		$datos = Producto::where('name', 'like', $term.'%')
		                  ->pluck('name', 'id');
		$valid_tags = [];
		foreach ($datos as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }

    public function referencia(Request $request)
    {
        $term = $request->term ?: '';
		$dato = $request->dato ?: '';
        if(!is_numeric($dato)){
            $datos = Producto::where('name', 'like', '%'.$dato.'%')
		                  ->get()->pop();

            $dato = $datos->id;

        }

        $referencia = ProductoReferencias::where('producto_id', $dato)
						  ->where('referencia', 'like', '%'.$term.'%')
		                  ->pluck('referencia', 'id');

		$valid_tags = [];
		foreach ($referencia as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }

    public function soportunidad(Request $request)
    {
        $term = $request->term ?: '';
		$datos = Oportunidades::where('titulo', 'like', '%'.$term.'%')
		                  ->pluck('titulo', 'id');
		$valid_tags = [];
		$valid_tags[] = ['id' => "Trabajos Generales ESSI", 'text' => "Trabajos Generales ESSI"];
		foreach ($datos as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }

    public function ssoportunidad(Request $request)
    {
        $term = $request->term ?: '';
		$datos = Oportunidades::where('titulo', 'like', '%'.$term.'%')
		                  ->pluck('titulo', 'id');
		$valid_tags = [];
		$valid_tags[] = ['id' => "", 'text' => "Seleccione una oportunidad"];
		foreach ($datos as $id => $tag) {
		  $valid_tags[] = ['id' => $id, 'text' => $tag];
		}
		return response()->json($valid_tags);
    }

}
