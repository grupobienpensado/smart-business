<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Oportunidades_perdida;
use App\Oportunidades;
use App\HistorialOportunidades;
use Auth;


class Oportunidades_perdidasController extends Controller
{
    public function save(Request $request){
    	$datos = new Oportunidades_perdida();
    	$datos->oportunidad=$request->id;
    	$datos->motivo_perdida=$request->motivo_perdida;
    	$datos->suministro_competencia=$request->suministro_competencia;
    	$datos->empresa=$request->empresa;
    	$datos->equipos_suministrados=$request->equipos_suministrados;
    	$datos->ventaja_competencia=$request->ventaja_competencia;
    	$datos->acciones_realizadas=$request->acciones_realizadas;
    	$datos->monto_oportuno=$request->monto_oportuno;
        $datos->gastos=$request->gastos;
        $datos->horas=$request->horas;
    	$datos->justificacion_monto_oportuno=$request->justificacion_monto_oportuno;
    	$datos->percepcion_essi=$request->percepcion_essi;
    	$datos->percepcion_maquina=$request->percepcion_maquina;
    	$datos->futuras_oportunidades=$request->futuras_oportunidades;
    	$datos->user_id = Auth::user()->id;
    	$datos->save();

    	$dato=Oportunidades::findOrFail($request->id);
        /*if($request->suministro_competencia=="Si"){
            $dato->estado="perdida";
        }else{
            $dato->estado="eliminada";
        }*/
        $dato->estado="eliminada";
		$dato->save();

		$datos=new HistorialOportunidades();
		$datos->key="ciclo_venta";
		$datos->value=0;
        if($request->suministro_competencia=="Si"){
            $datos->eliminada="No";
        }else{
            $datos->eliminada="Si";
        }
		$datos->descripcion=$request->motivo_perdida;
		$datos->oportunidad_id=$request->id;
		$datos->user_id = Auth::user()->id;
		$datos->save();

    	return redirect("/oportunidades");
    }

    public function listAjax(Request $request, $flag = null){
        $buscar = $request->all();
        $historial=[];
        $modulo=3;
        //$datos=[];
        if (isset($buscar["filtro"])) {
            /*Sección del filtro grande (que ya no está)*/
                $op = DB::select("SELECT H.oportunidad_id as id FROM historial_oportunidades H INNER JOIN oportunidades O ON O.id = H.oportunidad_id WHERE H.key = 'responsable' AND H.value = ".$buscar["filtro"]." AND O.estado ='eliminada' GROUP BY H.oportunidad_id");
            foreach ($op as $value) {
                $oportunidad = Oportunidades::findOrFail($value->id);
                $historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
                $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
            }
        }else{
            /*
            Cómo no hay filtro entra por acá
            Consulta los permisos de acceso
            */
            $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="listado oportunidades vigentes, vendidas, perdidas" AND `id_permisomodulo`="'.$modulo.'"');
            if(isset($acceso[0]->id)){
              $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
              if(isset($cargo[0]->id)){
                $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                if(isset($permiso[0]->permiso)){
                  if($permiso[0]->permiso == "Ver propio"){
                      /*Consulta las oportunidades del propietario*/
                    $datos = DB::select("SELECT H.oportunidad_id as id FROM historial_oportunidades H INNER JOIN oportunidades O ON O.id = H.oportunidad_id WHERE H.key = 'responsable' AND H.value = ".Auth::user()->id." AND O.estado ='eliminada' GROUP BY H.oportunidad_id");
                    foreach ($datos as $value) {
                        $oportunidad = Oportunidades::findOrFail($value->id);
                        $historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
                        $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                    }
                  }else{
                      /*Consulta todas las oportunidades perdidas*/
                    $datos=Oportunidades::where("estado","eliminada")->get();
                    foreach ($datos as $value) {
                        if($flag){
                            $historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
                            $historial[] = ['oportunidad'=>$value, "historial"=>$historia];
                        }else{
                            $date = date("Y");
                            $query='Select H.created_at FROM historial_oportunidades H WHERE H.key LIKE "ciclo_venta" AND H.value LIKE "0" AND H.oportunidad_id = '.$value->id.' AND H.created_at >= "'.$date.'" ORDER BY H.id DESC LIMIT 0,1';
                            $fechaperdida = DB::select($query);
                            if(isset($fechaperdida[0]->created_at)){
                                $historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
                                $historial[] = ['oportunidad'=>$value, "historial"=>$historia];
                            }
                        }

                    }
                  }
                }
              }
            }
        }

        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón añadir oportunidad en listados" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Si"){ 
                $permiso_anadir='Si';
              }else{
                $permiso_anadir='No';
              }
            }
          }          
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para exportar Excel*/
        $data['permiso_exportar']='No';
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Oportunidad perdida - Exportar Excel y CSV" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_exportar']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para exportar Excel*/
    	return view("oportunidadesperdidas",['data' => $data, 'datos' => $historial, 'permiso_anadir'=>$permiso_anadir]);
    }
    public function find($id){
    	$datos=Oportunidades_perdida::all();
    	$array=[];
    	foreach ($datos as $key) {
    		# code...
    		if($key->oportunidad==$id){
    			$array=$key;
    		}
    	}
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view("perdida",["data" => $data, "datos"=>$array]);
    }
    public function gestion($id){
        $query = ('SELECT * FROM historial_oportunidades H WHERE H.oportunidad_id = '.$id.' AND H.key LIKE "comentario_gestiones" ORDER BY H.id ASC');
        $data['comentarios'] = DB::select($query);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para crear un comentario*/
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear comentario en calendario de gestion" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_crear_comentario']='Si';
			  }else{
				$data['permiso_crear_comentario']='No';
			  }
            }
          }
        }
        /*Fin Permiso para crear un comentario*/
      return view('oportunidades.perdidas.gestiones',['data' => $data, 'id' => $id]);
    }
}
