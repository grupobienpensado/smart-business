<?php

namespace App\Http\Controllers;

date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use App\User;
use App\ComparacionAno;
use App\Comparaciones;
use Illuminate\Support\Facades\DB;
use Auth;



class ComparacionController extends Controller
{
    //
    public function index(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Dashboard" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("dashboard.comparacion",['data' => $data]);
    }

    public function action(Request $request){
        $accion = $request->action;
        switch($accion){
                case 0:
                    $division = DB::select('SELECT COUNT(DISTINCT(DATE_FORMAT(created_at, "%d-%m-%Y"))) AS numero_columnas FROM comparacion_anos');
                    if($division[0]->numero_columnas <= 3){
                        $limite = $division[0]->numero_columnas;
                        $division = 12 / $division[0]->numero_columnas;
                    }else{
                        $limite = 3;
                        $division = 4;
                    }
                    $filas_grandes = DB::select('SELECT DISTINCT(tipo_informacion) AS fila_grande FROM comparacion_anos');
                    ob_start();?>
                <?php
                    $cont=0;
                    foreach($filas_grandes as $fila){
                        if(isset($suma)){
                            foreach($suma as $index=>$value){
                                $suma[$index]=0;
                            }
                        }
                ?>
                        <div class="card sombra <?php if($cont>0){ ?>mt-5<?php } ?>">
                            <div class="card-header encabezado" role="tab" id="heading<?=$cont?>">
                              <h4 class="mb-0 color-encabezado">
                                <a data-toggle="collapse" href="#collapse<?=$cont?>" role="button" aria-expanded="true" aria-controls="collapse<?=$cont?>">
                                 <i class="fa fa-briefcase" aria-hidden="true"></i>
                                  <?=strtoupper($fila->fila_grande)?>
                                </a>
                              </h4>
                            </div>
                            <div id="collapse<?=$cont?>" class="collapse show" role="tabpanel" aria-labelledby="heading<?=$cont?>" data-parent="#accordion">
                              <div class="card-body">
                                <?php $encabezados = DB::select('SELECT DISTINCT(campo) AS encabezado FROM comparacion_anos WHERE tipo_informacion LIKE "'.$fila->fila_grande.'"');
                                  $division2=12/(count($encabezados) - 1);
                                  ?>
                                <div class="row">
                                    <div class="col-md-2 br-ano">
                                        <h4><?=$encabezados[0]->encabezado?></h4>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <?php for($i=1; $i<=$limite;$i++){ ?>
                                            <div class="col-md-<?=$division?> <?php if($i<$limite && $limite>1){ ?>br-ano<?php } ?>">
                                                <div class="row">
                                                    <?php
                                                        $enc=0;
                                                        foreach($encabezados as $encabezado){
                                                            if($encabezado->encabezado == "monto" || $encabezado->encabezado == "corto" || $encabezado->encabezado == "mediano" || $encabezado->encabezado == "largo" || $encabezado->encabezado == "alta" || $encabezado->encabezado == "media" || $encabezado->encabezado == "baja"){
                                                                $datos_matriz[$enc] = DB::select('SELECT CONCAT("$", " ", FORMAT(valor_campo,0)) AS valor_campo FROM comparacion_anos WHERE tipo_informacion LIKE "'.$fila->fila_grande.'" AND campo LIKE "'.$encabezado->encabezado.'"');
                                                            }else if($encabezado->encabezado == "porcentaje"){
                                                                $datos_matriz[$enc] = DB::select('SELECT CONCAT(valor_campo, " ", "%") AS valor_campo FROM comparacion_anos WHERE tipo_informacion LIKE "'.$fila->fila_grande.'" AND campo LIKE "'.$encabezado->encabezado.'"');
                                                            }else if($encabezado->encabezado == "funcionario"){
                                                                $datos_matriz[$enc] = DB::select('SELECT U.name AS valor_campo FROM comparacion_anos CA INNER JOIN users U ON CA.valor_campo = U.id WHERE CA.tipo_informacion LIKE "'.$fila->fila_grande.'" AND CA.campo LIKE "'.$encabezado->encabezado.'"');
                                                            }else{
                                                                $datos_matriz[$enc] = DB::select('SELECT valor_campo FROM comparacion_anos WHERE tipo_informacion LIKE "'.$fila->fila_grande.'" AND campo LIKE "'.$encabezado->encabezado.'"');
                                                            }

                                                            if($enc>0){
                                                    ?>
                                                    <div class="col-md-<?=$division2?>">
                                                        <h4><?=$encabezado->encabezado?></h4>
                                                    </div>
                                                    <?php } $enc++; } ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <?php $con_dato=0; foreach($datos_matriz[0] as $dato_m){ ?>
                                <div class="row">
                                    <div class="col-md-2 br-ano">
                                        <h6><?=$dato_m->valor_campo?></h6>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <?php for($i=1; $i<=$limite;$i++){ ?>
                                            <div class="col-md-<?=$division?> <?php if($i<$limite && $limite>1){ ?>br-ano<?php } ?>">
                                                <div class="row">
                                                <?php
                                                    $enc=0;
                                                    foreach($encabezados as $encabezado){
                                                    if($enc>0){
                                                        if(!isset($suma[$enc])){
                                                            $suma[$enc] = 0;
                                                        }
                                                    ?>
                                                    <div class="col-md-<?=$division2?>">
                                                        <?php $suma[$enc]+=intval(str_replace("$ ", "", str_replace(",", "", $datos_matriz[$enc][$con_dato]->valor_campo))); ?>
                                                        <h6><?=$datos_matriz[$enc][$con_dato]->valor_campo?></h6>
                                                    </div>
                                                    <?php } $enc++; } ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $con_dato++; } ?>
                                <div class="row">
                                    <div class="col-md-2 br-ano">
                                        <h3>TOTAL</h3>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <?php for($i=1; $i<=$limite;$i++){ ?>
                                            <div class="col-md-<?=$division?> <?php if($i<$limite && $limite>1){ ?>br-ano<?php } ?>">
                                                <div class="row">
                                                <?php
                                                    $enc=0;
                                                    foreach($encabezados as $encabezado){
                                                        if($encabezado->encabezado == "monto" || $encabezado->encabezado == "corto" || $encabezado->encabezado == "mediano" || $encabezado->encabezado == "largo" || $encabezado->encabezado == "alta" || $encabezado->encabezado == "media" || $encabezado->encabezado == "baja"){
                                                            $suma[$enc] = '$ '.number_format($suma[$enc], 0, '.', ',');
                                                        }
                                                    if($enc>0){
                                                    ?>
                                                    <div class="col-md-<?=$division2?>">
                                                        <h3><strong><?=$suma[$enc]?></strong></h3>
                                                    </div>
                                                    <?php } $enc++; } ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                      </div>
                <?php
                $cont++;
                    }
                    $data['html'] = ob_get_contents();
                    ob_end_clean();
                    break;
        }
        return \Response::json(['data' => $data]);
    }

    public function action_new(Request $request){
        switch($request->action){
            /*Encabezados del cuadro comparativo (SELECTS)*/
            case 0:
                $comparaciones = DB::SELECT('SELECT * FROM comparaciones C WHERE C.deleted_at IS NULL ORDER BY C.id DESC');
                ob_start();
                ?>
                <select class="form-control date-select">
                    <?php foreach($comparaciones as $comparacion){ ?>
                    <option value="<?=$comparacion->id?>"><?=$this->fecha($comparacion->created_at)?></option>
                    <?php } ?>
                </select>
                <?php
                $data['primer_select'] = ob_get_contents();
                ob_end_clean();
                ob_start();
                ?>
                <select class="form-control date-select">
                    <?php $i=0; foreach($comparaciones as $comparacion){ ?>
                    <option value="<?=$comparacion->id?>" <?php if($i==1){ ?>Selected<?php } ?> ><?=$this->fecha($comparacion->created_at)?></option>
                    <?php $i++; } ?>
                </select>
                <?php
                $data['segundo_select'] = ob_get_contents();
                ob_end_clean();
                ob_start();
                ?>
                <select class="form-control date-select">
                    <?php $i=0; foreach($comparaciones as $comparacion){ ?>
                    <option value="<?=$comparacion->id?>" <?php if($i==2){ ?>Selected<?php } ?> ><?=$this->fecha($comparacion->created_at)?></option>
                    <?php $i++; } ?>
                </select>
                <?php
                $data['tercer_select'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Informacion del cuadro comparativo*/
            case 1:
                $query = 'SELECT * FROM comparacion_items I WHERE I.deleted_at IS NULL';
                $items = DB::SELECT($query);
                ob_start();
                foreach($items as $fila){
                $query = 'SELECT * FROM comparacion_items_columnas C WHERE C.deleted_at IS NULL AND C.id_item = '.$fila->id.' ORDER BY C.id ASC';
                $columnas = DB::SELECT($query);
                ?>
                <div class="card sombra">
                    <div class="card-header encabezado" role="tab" id="heading<?=$fila->id?>">
                      <h4 class="mb-0 color-encabezado">
                        <a data-toggle="collapse" href="#collapse<?=$fila->id?>" role="button" aria-expanded="true" aria-controls="collapse<?=$fila->id?>">
                         <i class="fa fa-briefcase" aria-hidden="true"></i>
                          <?=strtoupper($fila->item)?>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse<?=$fila->id?>" class="collapse show" role="tabpanel" aria-labelledby="heading<?=$fila->id?>" data-parent="#accordion">
                      <div class="card-body">
                        <div class="row cabecera">
                            <div class="col-md-2 br-ano">
                                <h4><?=$columnas[0]->columna?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                 <?php for($y=0;$y<=2;$y++){ ?>
                                  <div class="col-md-4 <?php if($y<2){ ?>br-ano<?php } ?>">
                                       <div class="row">
                                       <?php
                                        $i=0;
                                        $tamano = 12/(count($columnas) - 1);
                                        foreach($columnas as $columna){
                                            if($i>0){
                                        ?>
                                            <div class="col-md-<?=$tamano?>">
                                                <h4><?=$columna->columna?></h4>
                                            </div>
                                         <?php } $i++; } ?>
                                       </div>
                                  </div>
                                  <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $query = 'SELECT DISTINCT I.valor FROM comparacion_informaciones I WHERE I.deleted_at IS NULL AND I.id_comparacion_items_columnas = '.$columnas[0]->id.' AND (I.id_comparacion = '.$request->primera.' OR I.id_comparacion = '.$request->segunda.' OR I.id_comparacion = '.$request->tercera.')';
                        $primeracolumna = DB::SELECT($query);
                        for($i=0;$i<count($primeracolumna);$i++){
                            $condicion = array($request->primera,$request->segunda,$request->tercera);
                            $bgresaltado = $primeracolumna[$i]->valor=="TOTAL"?"bg-info text-white":"";
                        ?>
                        <div class="row fila-contenido">
                            <div class="col-md-2 br-ano <?=$bgresaltado?>">
                                <h6><?=$primeracolumna[$i]->valor?></h6>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                  <?php for($y=0;$y<=2;$y++){ ?>
                                    <div class="col-md-4 <?php if($y<2){ ?>br-ano<?php } ?>">
                                        <div class="row">
                                           <?php
                                            for($z=1;$z<count($columnas);$z++){
                                                //Primera Información
                                                $query = 'SELECT * FROM comparacion_informaciones I WHERE I.deleted_at IS NULL AND I.id_comparacion_items_columnas = '.$columnas[0]->id.' AND I.id_comparacion = '.$condicion[$y].' ORDER BY I.id ASC';
                                                $primeraColumnaInfo = DB::SELECT($query);
                                                if(isset($primeraColumnaInfo[$i]->valor) && isset($primeracolumna[$i]->valor) && $primeraColumnaInfo[$i]->valor == $primeracolumna[$i]->valor){
                                                    $query = 'SELECT * FROM comparacion_informaciones I WHERE I.deleted_at IS NULL AND I.id_comparacion_items_columnas = '.$columnas[$z]->id.' AND I.id_comparacion = '.$condicion[$y].' ORDER BY I.id ASC';
                                                    $siguienteColumnaInfo = DB::SELECT($query);
                                                    $valor = $siguienteColumnaInfo[$i]->valor;
                                                }else{
                                                    $valor = 'No hay registro!';
                                                }
                                            ?>
                                            <div class="col-md-<?=$tamano?> <?=$bgresaltado?>">
                                                <h6><?=$valor?></h6>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }
    protected function fecha($fechasinformato){
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $fecha_hora = explode(' ',$fechasinformato);
        $fecha = $fecha_hora[0];
        $f = explode('-',$fecha);
        $hora = $fecha_hora[1];
        $hora = date("h:i:s A",strtotime($hora));
        $fechaconformato = $f[2].' de '.$meses[intval($f[1])].' '.$f[0].' '.$hora;
        return $fechaconformato;
    }
}
