<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as Collection;
use App\Oportunidades;
use App\HistorialOportunidades;
use App\Paises;
use App\Competencia;
use App\Producto;
use App\Cliente;
use App\Empresa;
use App\EmpresaSedes;
use App\OportunidadesFormapago;
use App\Actividades;
use App\HorasHombre;
use App\Ciclo;
use App\Historial_oportunidades_archivo;
use App\Historial_oportunidades_comentario;
use App\User;
use App\UserTodos;
use App\OportunidadComentario;
use App\Oportunidades_ingreso;
use App\Pendiente;
use App\Agenda_cliente;
// use App\Actividades_detalles;
use App\OportunidadProducto;
use App\Venta;
use App\Oportunidades_perdida;
use App\Agenda_clientes_actividade;
use App\Agenda_clientes_visitante;
use App\ViewFreelanceLista;
use App\OportunidadesCompetencias;
use App\OportunidadesCompetenciasEquipos;
use App\OportunidadActa;
use App\OportunidadActasInvitado;
use App\OportunidadActasTema;
use App\OportunidadActasAccion;
use App\OportunidadActasAccionesComentario;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;

date_default_timezone_set("America/Bogota");
header("Content-Type: text/html;charset=utf-8");

class OportunidadController extends Controller{
    public function acta($id){
      /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){
          $data['item_menu']='menu_'.$data['item_menu'][0]->id;
      }else{
          $data['item_menu']='';
      }
      /*Fin Consulta para activar el item del menu correspondiente*/
        /*Permiso Boton de ciclo venta*/
        $modulo = 3;
        $data['permiso_btn_ciclo'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón ciclo venta en listados y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_btn_ciclo']='Si';
			  }
            }
          }
        }

        $data['oportunidad'] = Oportunidades::find($id);
        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72" AND deleted_at IS NULL');
        $actas = DB::select('SELECT *, IF(LENGTH(`nombre`)>17,CONCAT(SUBSTRING(`nombre`, 1, 14),"..."),`nombre`) AS nombre_abreviado FROM `oportunidad_actas` WHERE `id_oportunidad`= '.$id.' AND `deleted_at` IS NULL ORDER BY `id` ASC');
        $invitados = DB::select('SELECT * FROM `oportunidad_actas_invitados` WHERE `id_acta` IN (SELECT `id` FROM `oportunidad_actas` WHERE `id_oportunidad`="'.$id.'")');
        $data['invitados_externos'] = DB::select('SELECT DISTINCT(nombre) FROM oportunidad_actas_invitados WHERE tipo LIKE "usuario externo" AND nombre IS NOT NULL');

        $actas2 = OportunidadActa::where('id_oportunidad',$id)->get();
        ob_start();
        $data['numero_vigentes']=0;
        foreach($actas2 as $acta){
            $query = 'IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `oportunidad_actas_invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido';
            $vigentes = DB::select('SELECT A.*, IFNULL(A.estado, "vigente") AS ESTADO , '.$query.' FROM oportunidad_actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($vigentes as $vigente){
                if(isset($vigente->ESTADO) && $vigente->ESTADO == "vigente"){
                    $data['numero_vigentes']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_vigentes']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($vigente->id_acta)>9){ if(($vigente->id_acta)>99){ if(($vigente->id_acta)>999){ $numero=$vigente->id_acta; }else{ $numero='0'.$vigente->id_acta; } }else{  $numero='00'.$vigente->id_acta; } }else{ $numero='000'.$vigente->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigente->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigente->fecha_ejecucion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($vigente->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$vigente->responsable_nombre?> <?=$vigente->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$vigente->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$vigente->descripcion?>" style="cursor:pointer;"><?=$vigente->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($vigente->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$vigente->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-2 encabezado">
        <a class="realizado" data-toggle="tooltip" data-placement="top" title="Realizado" onclick="marcar(<?=$vigente->id?>,'realizado')"><i class="fa fa-check-circle-o fa-2"></i></a>
        <a class="aplazar" data-toggle="tooltip" data-placement="top" title="Aplazar" onclick="marcar(<?=$vigente->id?>,'aplazar')"><i class="fa fa-repeat fa-2"></i></a>
        <a class="cancelar" data-toggle="tooltip" data-placement="top" title="Cancelar" onclick="marcar(<?=$vigente->id?>,'cancelar')"><i class="fa fa-times-circle-o fa-2"></i></a>
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$vigente->id?>,'<?=$numero?>')"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['vigentes'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_realizado']=0;
        foreach($actas as $acta){
            $realizados = DB::select('SELECT *, '.$query.' FROM oportunidad_actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($realizados as $realizado){
                if(isset($realizado->estado) && $realizado->estado == 'Realizado'){
                    $data['numero_realizado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_realizado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($realizado->id_acta)>9){ if(($realizado->id_acta)>99){ if(($realizado->id_acta)>999){ $numero=$realizado->id_acta; }else{ $numero='0'.$realizado->id_acta; } }else{  $numero='00'.$realizado->id_acta; } }else{ $numero='000'.$realizado->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizado->fecha_creacion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizado->fecha_ejecucion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizado->fecha_accion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($realizado->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$realizado->responsable_nombre?> <?=$realizado->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$realizado->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$realizado->descripcion?>" style="cursor:pointer;"><?=$realizado->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($realizado->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$realizado->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$realizado->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['realizados'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_cancelado']=0;
        foreach($actas as $acta){
            $cancelados = DB::select('SELECT A.*, '.$query.' FROM oportunidad_actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($cancelados as $cancelado){
                if(isset($cancelado->estado) && $cancelado->estado == 'Cancelado'){
                    $data['numero_cancelado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_cancelado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($cancelado->id_acta)>9){ if(($cancelado->id_acta)>99){ if(($cancelado->id_acta)>999){ $numero=$cancelado->id_acta; }else{ $numero='0'.$cancelado->id_acta; } }else{  $numero='00'.$cancelado->id_acta; } }else{ $numero='000'.$cancelado->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($cancelado->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($cancelado->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$cancelado->responsable_nombre?> <?=$cancelado->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$cancelado->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-4 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$cancelado->descripcion?>" style="cursor:pointer;"><?=$cancelado->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($cancelado->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$cancelado->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$cancelado->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['cancelados'] = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('oportunidades.acta',['data' => $data, 'id' => $id, 'usuarios' => $usuarios, 'actas' => $actas, 'invitados' => $invitados]);
    }
    public function action_acta(Request $request){
        switch($request->accion){
            /*Guardar cita*/
            case 0:
                $dato = new OportunidadActa;
                $dato->nombre = $request->titulo;
                $dato->estado = "citada";
                $dato->descripcion = $request->descripcion;
                $dato->fecha = $request->fecha.':00';
                $dato->lugar = $request->lugar;
                $dato->id_oportunidad = $request->id_oportunidad;
                $dato->save();
                $id = $dato->id;

                $invitados=$request->invitados;
                $invitado=explode('%%',$invitados);
                $i=0;
                foreach($invitado as $inv){
                    if($inv!=''){
                        $dato = new OportunidadActasInvitado;
                        $dato->tipo = "usuario sistema";
                        $dato->id_usuario = $inv;
                        $dato->id_acta = $id;
                        $dato->save();
                        /*captura correo*/
                        $user_invi = User::find($inv);
                        if($user_invi->correo_corporativo != ''){
                            $emails[$i] = $user_invi->correo_corporativo;
                            $i++;
                        }
                    }
                }

                $invitados_externos=$request->invitadoadicional;
                if(count($invitados_externos)>0){
                    foreach($invitados_externos as $ie){
                        $dato = new OportunidadActasInvitado;
                        $dato->tipo = "usuario externo";
                        $dato->nombre = $ie['nombre'];
                        $emails[$i] = $ie['correo'];
                        $dato->email = $ie['correo'];
                        $dato->id_acta = $id;
                        $dato->save();
                        $i++;
                    }
                }
                /*Envio de correos*/
                setlocale(LC_ALL, "es_CO.UTF-8");
                $fechareunion = strftime("%A %d %B %Y %I %M %p", strtotime($request->fecha.':00'));
                $tituloAsunto = "Reunión en ESSI el día ".$fechareunion;

                if(isset($emails) && count($emails)>0){
                    $data = array('id' => $id);
                    Mail::send('mail.mailactasoportunidad', $data, function($message) use ($emails, $tituloAsunto){
                        $message->from('smart@bounces.smartessi.com', 'Smart Business ESSI');
                        $message->bcc($emails)->subject($tituloAsunto);
                    });
                }
                /*Fin Envio de correos*/

                $datos=$this->listado($request->id_oportunidad);
                return view('oportunidades.acta',['data' => $datos['data'], 'id' => $datos['id'], 'usuarios' => $datos['usuarios'], 'actas' =>$datos['actas'], 'invitados' => $datos['invitados']]);
                break;
            /*Ver Acta de reunion*/
            case 1:
                $acta = OportunidadActa::find($request->id);
                $invitados = OportunidadActasInvitado::where('id_acta',$acta->id)->get();
                $temas = OportunidadActasTema::where('id_acta',$acta->id)->get();
                $pendientes = OportunidadActasAccion::where('id_acta',$acta->id)->get();
                $numero=strlen($acta->id);
                $numero_acta = '';
                for($i=1;$i<=4-$numero;$i++){
                    $numero_acta.='0';
                }
                $numero_acta='AC-'.$numero_acta.$acta->id;
                if($acta->estado == "citada"){
                    ob_start(); ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Acta <?=$numero_acta?> <em>(Citada)</em></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Titulo </p>
                                    <p class="text-secondary"><em><?=$acta->nombre?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Descripción </p>
                                    <p class="text-secondary"><em><?=$acta->descripcion?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Fecha y hora </p>
                                    <p class="text-secondary"><em><?=$this->fecha2(date('Y-m-d',strtotime($acta->fecha))).' '.date('(h:i:s A)', strtotime($acta->fecha))?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Lugar </p>
                                    <p class="text-secondary"><em><?=$acta->lugar?></em></p>
                                </div>
                            </div>
                            <!--Invitados-->
                            <div class="col-md-12">
                                <div class="form-group">
                                  <p>Invitados </p>
                                </div>
                                <?php if(count($invitados) > 0){ ?>
                                    <div class="row">
                                    <?php $i=0; foreach($invitados as $invitado){ $i++; ?>
                                    <?php if($invitado->tipo == "usuario sistema"){
                                    $user = User::find($invitado->id_usuario);
                                    $nombre = explode(' ',$user->nombres);
                                    $apellido = explode(' ',$user->apellidos);
                                    ?>
                                        <div class="col-md-3 my-4">
                                            <img src="/images/file/clientes/<?=$user->foto?>" class="img-circle" style="width: 40px" alt="Avatar">
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$nombre[0].' '.$apellido[0]?> </strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario sistema)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?=$user->cargo?></span>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-md-3 my-4">
                                            <i class="icon ion-person"></i>
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$invitado->nombre?></strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario externo)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?php if(!empty($invitado->email)){ echo '('.$invitado->email.')'; }else{ echo 'No tiene correo'; } ?></span>
                                        </div>
                                    <?php } } ?>
                                    </div>
                                <?php }else{ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay invitados</em></p>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
<?php               $data['ver'] = ob_get_contents();
                    ob_end_clean();
                }else{
                    ob_start(); ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h2>Acta <?=$numero_acta?> <em>(Realizada)</em></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Titulo </p>
                                    <p class="text-secondary"><em><?=$acta->nombre?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Descripción </p>
                                    <p class="text-secondary"><em><?=$acta->descripcion?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Fecha y hora </p>
                                    <p class="text-secondary"><em><?=$this->fecha2(date('Y-m-d',strtotime($acta->fecha))).' '.date('(h:i:s A)', strtotime($acta->fecha))?></em></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                               <div class="form-group">
                                    <p>Lugar </p>
                                    <p class="text-secondary"><em><?=$acta->lugar?></em></p>
                                </div>
                            </div>
                            <!--Invitados-->
                            <div class="col-md-12">
                                <div class="form-group">
                                  <p>Invitados </p>
                                </div>
                                <?php if(count($invitados) > 0){ ?>
                                    <div class="row">
                                    <?php $i=0; foreach($invitados as $invitado){ $i++; ?>
                                    <?php if($invitado->tipo == "usuario sistema"){
                                    $user = User::find($invitado->id_usuario);
                                    $nombre = explode(' ',$user->nombres);
                                    $apellido = explode(' ',$user->apellidos);
                                    $foto = $user->foto==''?'https://placeholdit.imgix.net/~text?txtsize=45&txt='.$nombre[0].'%20'.$apellido[0].'&w=200&h=200':'/images/file/clientes/'.$user->foto;
                                    ?>
                                        <div class="col-md-3 my-4">
                                            <img src="<?=$foto?>" class="img-circle" style="width: 40px" alt="Avatar">
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$nombre[0].' '.$apellido[0]?> </strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario sistema)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?=$user->cargo?></span>
                                        </div>
                                    <?php }else{
                                        $foto = 'https://placeholdit.imgix.net/~text?txtsize=45&txt='.$invitado->nombre.'&w=200&h=200';
                                        ?>
                                        <div class="col-md-3 my-4">
                                            <img src="<?=$foto?>" class="img-circle" style="width: 40px" alt="Avatar">
                                            <p style="font-size: 13px;"><a href="#"><strong><?=$i.'. '.$invitado->nombre?></strong></a></p>
                                            <span class="text-secondary" style="font-size: 11px;">(usuario externo)</span><br>
                                            <span class="text-secondary" style="font-size: 11px;"><?php if(!empty($invitado->email)){ echo '('.$invitado->email.')'; }else{ echo 'No tiene correo'; } ?></span>
                                        </div>
                                    <?php } } ?>
                                    </div>
                                <?php }else{ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay invitados</em></p>
                                </div>
                                <?php } ?>
                            </div>
                            <!--Temas-->
                            <div class="col-md-12">
                                <div class="form-group" style="background-color: #03113a;">
                                  <p class="text-white">Temas </p>
                                </div>
                                <?php $i=100; foreach($temas as $tema){ $i++; ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title titulo-tema">
                                            <a class="collapsed titulo-tema0" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>"><b><?=ucwords(strtolower($tema->titulo))?></b><i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?=$i?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                   <div class="project-section general-info text-left">
								                       <h3>Titulo</h3>
                                                       <p><?=$tema->titulo?></p>
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="project-section general-info text-left">
                                                        <h3>Descripción </h3>
                                                        <p><?=$tema->descripcion?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(count($temas) == 0){ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay Temas</em></p>
                                </div>
                                <?php } ?>
                            </div>
                            <!--Pendientes-->
                            <div class="col-md-12">
                                <div class="form-group" style="background-color: #03113a;">
                                  <p class="text-white">Pendientes </p>
                                </div>
                                <?php $i=1000; foreach($pendientes as $pendiente){ $i++; ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title titulo-tema">
                                            <a class="collapsed titulo-tema<?=$i?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>"><b>Pendiente #<span><?=$i-1000?></span></b><i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?=$i?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                   <div class="project-section general-info text-left">
								                       <h3>Fecha Ejecución</h3>
                                                       <p><?=$this->fecha2(date('Y-m-d',strtotime($pendiente->fecha_ejecucion))).' '.date('(h:i:s A)',strtotime($pendiente->fecha_ejecucion))?></p>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                   <div class="project-section general-info text-left">
								                       <h3>Responsable</h3>
                                                       <?php
                                                            if($pendiente->tipo_responsable == "usuario sistema"){
                                                                $responsable = User::find($pendiente->id_responsable);
                                                                $nombre = explode(' ',$responsable->nombres);
                                                                $apellido = explode(' ',$responsable->apellidos);
                                                                $nombre_completo = $nombre[0].' '.$apellido[0];
                                                            }else{
                                                                $responsable = OportunidadActasInvitado::find($pendiente->id_responsable);
                                                                $nombre_completo = $responsable->nombre;
                                                            }
                                                       ?>
                                                       <p><?=$nombre_completo?></p>
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="project-section general-info text-left">
                                                        <h3>Descripción </h3>
                                                        <p><?=$pendiente->descripcion?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(count($pendientes)==0){ ?>
                                <div class="form-group">
                                    <p class="text-secondary"><em>No hay Pendientes</em></p>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
<?php               $data['ver'] = ob_get_contents();
                    ob_end_clean();
                }
                return \Response::json(['data' => $data]);
                break;
            /*Datos del acta a editar*/
            case 2:
                $dato=OportunidadActa::findorfail($request->id);
                $invitados = DB::select('SELECT *, (SELECT `foto` FROM `users` WHERE `id`=I.`id_usuario`) AS foto, (SELECT CONCAT(SUBSTRING_INDEX(UU.`nombres`, " ", 1 ), " ", SUBSTRING_INDEX(UU.`apellidos`, " ", 1 )) FROM `users` AS UU WHERE `id`=I.`id_usuario`) AS nombre_funcionario, (SELECT `cargo` FROM `users` WHERE `id`=I.`id_usuario`) AS cargo_funcionario FROM `oportunidad_actas_invitados` AS I WHERE I.`id_acta` IN (SELECT A.`id` FROM `oportunidad_actas` AS A WHERE A.`id`="'.$request->id.'") AND I.deleted_at IS NULL');

                return \Response::json(['acta' => $dato, 'invitados' => $invitados]);
                break;
            /*Editar Cita*/
            case 3:
                $dato = OportunidadActa::findorfail($request->id_cita);
                $dato->nombre = $request->titulo;
                $dato->estado = "citada";
                $dato->descripcion = $request->descripcion;
                $dato->fecha = date("Y-m-d H:i:s",strtotime($request->fecha));
                $dato->lugar = $request->lugar;
                $dato->id_oportunidad = $request->id_oportunidad;
                $dato->save();
                $id = $dato->id;

                /*eliminar invitados usuario sistema */
                $usuarios_sistemas = OportunidadActasInvitado::where('id_acta', $id)->where('tipo', "usuario sistema")->get();
                foreach($usuarios_sistemas as $usuario){
                    OportunidadActasInvitado::destroy($usuario->id);
                }
                $invitados=$request->invitados;
                $invitado=explode('%%',$invitados);

                foreach($invitado as $inv){
                    if($inv!=""){
                        $dato = new OportunidadActasInvitado;
                        $dato->tipo = "usuario sistema";
                        $dato->id_usuario = $inv;
                        $dato->id_acta = $id;
                        $dato->save();
                    }
                }

                /*eliminar invitados usuario externo */
                $usuarios_externos = OportunidadActasInvitado::where('id_acta', $id)->where('tipo', "usuario externo")->get();
                foreach($usuarios_externos as $usua){
                    OportunidadActasInvitado::destroy($usua->id);
                }
                $invitados_externos=$request->invitadoadicional;
                $i=0;
                if(isset($invitados_externos)){
                    foreach($invitados_externos as $ie){
                        $dato = new OportunidadActasInvitado;
                        $dato->tipo = "usuario externo";
                        $dato->nombre = $ie['nombre'];
                        $emails[$i] = $ie['correo'];
                        $dato->email = $ie['correo'];
                        $dato->id_acta = $id;
                        $dato->save();
                        $i++;
                    }
                }
                $datos=$this->listado($request->id_oportunidad);
                return view('oportunidades.acta',['data' => $datos['data'], 'id' => $datos['id'], 'usuarios' => $datos['usuarios'], 'actas' =>$datos['actas'], 'invitados' => $datos['invitados']]);
                break;
            /*Eliminar citada*/
            case 4:
                $dato=OportunidadActa::findorfail($request->id);
                $dato->deleted_at=date('Y-m-d H:i:s');
                $data['id'] = $dato->id_oportunidad;
                $dato->save();
                return \Response::json(['msj' => 'Guardado correctamente!','data' => $data]);
                break;
            /*Datos de reunión para iniciar*/
            case 5:
                $usuarios_sistema= DB::select('SELECT *, (SELECT `id` FROM `oportunidad_actas_invitados` WHERE `id_acta`="'.$request->id.'" AND `tipo`="usuario sistema" AND U.id = id_usuario AND `deleted_at` IS NULL) AS id_invitado FROM `users` AS U WHERE `id` IN (SELECT i.`id_usuario` FROM `oportunidad_actas_invitados` AS i WHERE i.`id_acta`="'.$request->id.'" AND i.`tipo`="usuario sistema" AND i.deleted_at IS NULL)');
                $usuarios_externos= DB::select('SELECT * FROM `oportunidad_actas_invitados` WHERE `id_acta`="'.$request->id.'" AND `tipo`="usuario externo" AND deleted_at IS NULL');
                $listado_invitados= DB::select('SELECT * FROM `users` WHERE `id` IN (SELECT `id_usuario` FROM `oportunidad_actas_invitados` WHERE `id_acta`="'.$request->id.'" AND  `tipo`="usuario sistema" AND `deleted_at` IS NULL)');
                $html='';
                $i=0;
                foreach($usuarios_sistema as $user){
                    $nombre=explode(' ',$user->nombres);
                    $apellido=explode(' ',$user->apellidos);
                   $html.='<div class="col-md-1 imagen-asis">
                            <img src="/images/file/clientes/'.$user->foto.'" class="img-circle img-asistencia" alt="Avatar">
                        </div>
                        <div class="col-md-3 nombre-asis">
                           <label class="control-inline fancy-checkbox">
                                <input type="hidden" id="asistenciavalor'.$user->id_invitado.'" name="asistencia['.$i.'][valor]" value="No">
                                <input type="hidden" id="asistenciauser'.$user->id_invitado.'" name="asistencia['.$i.'][user]" value="'.$user->id_invitado.'">
                                <input type="hidden" id="asistenciahorallegada'.$user->id_invitado.'" name="asistencia['.$i.'][horallegada]" value="00:00:00">
                                <input type="checkbox" class="lista-usuarios-asistencia" id="asistio'.$user->id_invitado.'">
                                <span>'.$nombre[0].' '.$apellido[0].'</span>
                           </label>
                        </div>
                        <div class="col-md-2 hora_llegada" id="hora_llegada'.$user->id_invitado.'">

                        </div>';
                    $i++;
                }
                foreach($usuarios_externos as $user){
                  $html.='<div class="col-md-1 imagen-asis">
                            <img src="/images/externo.jpg" class="img-circle img-asistencia" alt="Avatar">
                        </div>
                        <div class="col-md-3 nombre-asis">
                           <label class="control-inline fancy-checkbox">
                                <input type="hidden" id="asistenciavalor'.$user->id.'" name="asistencia['.$i.'][valor]" value="No">
                                <input type="hidden" id="asistenciauser'.$user->id.'" name="asistencia['.$i.'][user]" value="'.$user->id.'">
                                <input type="hidden" id="asistenciahorallegada'.$user->id.'" name="asistencia['.$i.'][horallegada]" value="00:00:00">
                                <input type="checkbox" class="lista-usuarios-asistencia" id="asistio'.$user->id.'">
                                <span>'.$user->nombre.'</span>
                           </label>
                        </div>
                        <div class="col-md-2 hora_llegada" id="hora_llegada'.$user->id.'">

                        </div>';
                    $i++;
                }
                $html_responsable='';
                $select_responsable='<p>Responsable </p>
                <label for="message-text" class="form-control-label"></label>
                <select class="form-control responsable" name="pendiente[0][responsable]">
                    <option value="">Seleccione un responsable</option>';
                foreach($listado_invitados as $usuario){
                    $select_responsable.='<option value="usuario sistema¬'.$usuario->id.'">'.$usuario->name.'</option>';
                    $html_responsable.='<option value="usuario sistema¬'.$usuario->id.'">'.$usuario->name.'</option>';
                }
                foreach($usuarios_externos as $usuario1){
                    $select_responsable.='<option value="usuario externo¬'.$usuario1->id.'">'.$usuario1->nombre.'</option>';
                    $html_responsable.='<option value="usuario externo¬'.$usuario1->id.'">'.$usuario1->nombre.'</option>';
                }
                $select_responsable.='</select>';
                return \Response::json(['html' => $html, 'select_responsable' => $select_responsable, 'html_responsable' => $html_responsable]);
                break;
            /*Guardar Iniciar Reunión*/
            case 6:
                $dato=OportunidadActa::findorfail($request->id);
                $dato->estado = "realizada";
                if(isset($request->fecha) && $request->fecha != ''){
                $dato->fecha_inicio_real = $request->fecha;
                }
                $dato->save();

                $asistencia=$request->asistencia;
                if($asistencia!=""){
                    foreach($asistencia as $as){
                        $dato=OportunidadActasInvitado::findorfail($as['user']);
                        $dato->asistio = $as['valor'];
                        $dato->hora_llegada = date("H:i:s",strtotime($as['horallegada']));
                        $dato->save();
                    }
                }
                $data['id_temas'] = '';
                $temas=$request->temas;
                foreach($temas as $index => $tema){
                    if($tema['titulo'] != '' && $tema['descripcion'] != "" && $request['id'] != ""){
                        if($tema['id'] == ''){
                            $dato = new OportunidadActasTema;
                        }else{
                            $dato = OportunidadActasTema::find($tema['id']);
                        }
                        $dato->titulo = $tema['titulo'];
                        $dato->descripcion = $tema['descripcion'];
                        $dato->id_acta = $request['id'];
                        $dato->save();
                        $data['id_temas'][$index] = $dato->id;
                    }
                }

                $data['id_acciones'] = '';
                $acciones=$request->pendiente;
                foreach($acciones as $index => $accion){
                    if($accion['fecha_creacion'] != '' && $accion['responsable'] != '' && $accion['descripcion'] != ''){
                        if($accion['id'] == ''){
                            $dato = new OportunidadActasAccion;
                        }else{
                            $dato = OportunidadActasAccion::find($accion['id']);
                        }
                        $dato->fecha_creacion = $accion['fecha_creacion'];
                        $dato->fecha_ejecucion = $accion['fecha_ejecucion'];
                        $responsable=explode('¬',$accion['responsable']);
                        $dato->id_responsable = $responsable[1];
                        $dato->tipo_responsable = $responsable[0];
                        $dato->descripcion = $accion['descripcion'];
                        $dato->id_acta = $request->id;
                        $dato->save();
                        $data['id_acciones'][$index] = $dato->id;
                    }
                }
                $data['msj'] = 'Guardado correctamente!';
                return \Response::json(['data' => $data]);
                break;
            /*Guardar Iniciar Reunión*/
            case 7:
                $dato=OportunidadActa::findorfail($request->id);
                $dato->estado = "realizada";
                $dato->fecha_inicio_real = $request->fecha;
                $dato->save();

                $asistencia=$request->asistencia;
                if($asistencia!=""){
                    foreach($asistencia as $as){
                        $dato=OportunidadActasInvitado::findorfail($as['user']);
                        $dato->asistio = $as['valor'];
                        $dato->hora_llegada = date("H:i:s",strtotime($as['horallegada']));
                        $dato->save();
                    }
                }

                $temas=$request->temas;
                foreach($temas as $index => $tema){
                    if($tema['titulo'] != '' && $tema['descripcion'] != "" && $request['id'] != ""){
                        if($tema['id'] == ''){
                            $dato = new OportunidadActasTema;
                        }else{
                            $dato = OportunidadActasTema::find($tema['id']);
                        }
                        $dato->titulo = $tema['titulo'];
                        $dato->descripcion = $tema['descripcion'];
                        $dato->id_acta = $request['id'];
                        $dato->save();
                    }
                }
                $acciones=$request->pendiente;
                foreach($acciones as $index => $accion){
                    if($accion['fecha_creacion'] != '' && $accion['responsable'] != '' && $accion['descripcion'] != ''){
                        if($accion['id'] == ''){
                            $dato = new OportunidadActasAccion;
                        }else{
                            $dato = OportunidadActasAccion::find($accion['id']);
                        }
                        $dato->fecha_creacion = $accion['fecha_creacion'];
                        $dato->fecha_ejecucion = $accion['fecha_ejecucion'];
                        $responsable=explode('¬',$accion['responsable']);
                        $dato->id_responsable = $responsable[1];
                        $dato->tipo_responsable = $responsable[0];
                        $dato->descripcion = $accion['descripcion'];
                        $dato->id_acta = $request->id;
                        $dato->save();
                    }
                }
                $datos=$this->listado($request->id_oportunidad);
                return view('oportunidades.acta',['data' => $datos['data'], 'id' => $datos['id'], 'usuarios' => $datos['usuarios'], 'actas' =>$datos['actas'], 'invitados' => $datos['invitados']]);
                break;
            /*Eliminar accion o tema*/
            case 8:
                if($request->tipo == "tema"){
                    $dato = OportunidadActasTema::find($request->id);
                    if(isset($dato->id) && $dato->id != ""){
                        $dato->delete();
                        $data['msj'] = 'Eliminado el tema';
                    }else{
                        $data['msj'] = 'No se elimino el tema por que no existe en la base de datos';
                    }
                }else{
                    $dato = OportunidadActasAccion::find($request->id);
                    if(isset($dato->id) && $dato->id != ""){
                        $dato->delete();
                        $data['msj'] = 'Eliminado el pendiente';
                    }else{
                        $data['msj'] = 'No se elimino el pendiente por que no existe en la base de datos';
                    }
                }
                return \Response::json(['data' => $data]);
                break;
            /*Comentarios*/
            case 9:
                $comentarios= DB::select('SELECT AAC.* FROM oportunidad_actas_acciones_comentarios AAC INNER JOIN oportunidad_actas_acciones A ON AAC.id_actas_acciones = A.id WHERE A.id = '.$request->id.' ORDER BY AAC.fecha DESC LIMIT 0, 5');
                $html='';
                foreach($comentarios as $comentario){
                    $user = User::findorfail($comentario->id_user);
                    $nombres=explode(' ',$user->nombres);
                    $apellidos=explode(' ',$user->apellidos);
                    $nombre=$nombres[0].' '.$apellidos[0];
                    $html.='<div class="row">
                                <div class="col-md-3">';
                    if($comentario->estado == "Realizado"){
                        $html.='<span class="label label-medium">Realizada</span>';
                    }else if($comentario->estado == "Aplazado"){
                        $html.='<span class="label label-emergency">Aplazada</span>';
                    }else{
                        $html.='<span class="label label-critical">Cancelada</span>';
                    }
                     $html.='</div>
                                <div class="col-md-6">
                                    <p class="comentario-text"><a href="#">'.$nombre.':</a> '.$comentario->comentario.' <br><span class="timestamp hace-comentario">'.date("d/m/Y h:i:s A",strtotime($comentario->fecha)).'</span></p>
                                </div>
                                <div class="col-md-3">
                                    <img src="/images/file/clientes/'.$user->foto.'" class="img-circle img-comentario" alt="Avatar">
                                </div>
                            </div>';
                }
                return \Response::json(['html' => $html]);
                break;
            /*Marcar como realizada una accion*/
            case 10:
                $dato=OportunidadActasAccion::findorfail($request->id);
                $dato->estado="Realizado";
                $dato->fecha_accion=date('Y-m-d H:i:s');
                $dato->save();

                $dato= New OportunidadActasAccionesComentario;
                $dato->comentario=$request->comentario;
                $dato->estado="Realizado";
                $dato->fecha=date('Y-m-d H:i:s');
                $dato->id_user=Auth::user()->id;
                $dato->id_actas_acciones=$request->id;
                $dato->save();
                return \Response::json(['msj' => 'Guardado correctamente!']);
                break;
            /*Aplazar la acción*/
            case 11:
                $dato=OportunidadActasAccion::findorfail($request->id);
                if($dato->aplazado > 0){
                    $dato->aplazado=($dato->aplazado)+1;
                }else{
                    $dato->aplazado=1;
                }
                $dato->fecha_ejecucion=$request->nueva;
                $dato->save();

                $dato= New OportunidadActasAccionesComentario;
                $dato->comentario=$request->comentario;
                $dato->estado="Aplazado";
                $dato->fecha=date('Y-m-d H:i:s');
                $dato->id_user=Auth::user()->id;
                $dato->id_actas_acciones=$request->id;
                $dato->save();
                return \Response::json(['msj' => 'Guardado correctamente!']);
                break;
            /*Cancelar la acción*/
            case 12:
                $dato=OportunidadActasAccion::findorfail($request->id);
                $dato->estado="Cancelado";
                $dato->fecha_accion=date('Y-m-d H:i:s');
                $dato->save();

                $dato= New OportunidadActasAccionesComentario;
                $dato->comentario=$request->comentario;
                $dato->estado="Cancelado";
                $dato->fecha=date('Y-m-d H:i:s');
                $dato->id_user=Auth::user()->id;
                $dato->id_actas_acciones=$request->id;
                $dato->save();
                return \Response::json(['msj' => 'Guardado correctamente!']);
                break;
            /*Eniar correo electronico a todos los invitados*/
            case 13:
                $reunion = OportunidadActa::find($request->id);
                $usuarios_sistema = DB::SELECT('SELECT (U.correo_corporativo) AS CORREO FROM oportunidad_actas_invitados I INNER JOIN users U ON I.id_usuario = U.id WHERE I.id_acta = '.$request->id.' AND I.tipo = "usuario sistema" AND I.deleted_at IS NULL AND U.correo_corporativo IS NOT NULL');
                $usuarios_externos = DB::SELECT('SELECT (I.email) AS CORREO FROM oportunidad_actas_invitados I WHERE I.id_acta = '.$request->id.' AND I.tipo = "usuario externo" AND I.deleted_at IS NULL AND I.email IS NOT NULL');
                $i=0;
                if(count($usuarios_sistema)>0){
                    foreach($usuarios_sistema as $usuario){
                        if($usuario->CORREO != ''){
                            $emails[$i] = $usuario->CORREO;
                            $i++;
                        }
                    }
                }
                if(count($usuarios_externos)>0){
                    foreach($usuarios_externos as $usuario){
                        if($usuario->CORREO != ''){
                            $emails[$i] = $usuario->CORREO;
                            $i++;
                        }
                    }
                }
                if(isset($emails)){
                    /*Envio de correos*/
                    setlocale(LC_ALL, "es_CO.UTF-8");
                    $fechareunion = strftime("%A %d %B %Y %I %M %p", strtotime($reunion->fecha));
                    $tituloAsunto = "Información de la reunión en ESSI el día ".$fechareunion;

                    if(isset($emails) && count($emails)>0){
                        $data = array('id' => $request->id, 'titulo_correo' => $tituloAsunto);
                        Mail::send('mail.mailactasreunionfinalizadaoportunidad', $data, function($message) use ($emails, $tituloAsunto){
                            $message->from('smart@bounces.smartessi.com', 'Smart Business ESSI');
                            $message->bcc($emails)->subject($tituloAsunto);
                        });
                    }
                    /*Fin Envio de correos*/
                    $data['msj'] = 'Enviado correctamente!';
                }else{
                    $data['msj'] = 'Error no hay correos!';
                }
                return \Response::json(['data' => $data]);
                break;
        }
    }
    public function action_two(Request $request){
        $accion=$request->action;
        switch($accion){
            case 0:
                ob_start();
                $i = 1;
                $Datos = $request->Datos;

                foreach($Datos as $dato){
                    $usuario = UserTodos::find($dato["user_id"]);
                    $data['nombre'] = $usuario->nombres.' '.$usuario->apellidos;
                    if($i%2 != 0){
                ?>
                    <div class="row">
                        <div class="container bg-blue-f m-2">
                            <div class="col-sm-12 col-md-6 text-center">
                                <p class="h3 py-2 tittle-a">Registro Anterior</p>
                                <p class="h4 my-1"><?php if($dato["key"]=="fecha_oc"){ echo fecha($dato["value"]); }else{ echo $dato["value"]; } ?></p>
                                <p class="h5 my-1"><em>Creado el : <?=fecha_hora($dato["created_at"])?></em></p>
                                <p class="h5 my-1"><em>Creado por: <?=$data['nombre']?></em></p>
                            </div>
                <?php
                    $i++;
                }else{
                        if(isset($anterior)){
                            $usuario1 = UserTodos::find($anterior["user_id"]);
                            $data['nombre'] = $usuario1->nombres.' '.$usuario1->apellidos;
                            ?>
                            <div class="row">
                                <div class="container bg-blue-f m-2">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <p class="h3 py-2 tittle-a">Registro Anterior</p>
                                        <p class="h4 my-1"><?php if($anterior["key"]=="fecha_oc"){ echo fecha($anterior["value"]); }else{ echo $anterior["value"]; } ?></p>
                                        <p class="h5 my-1"><em>Creado el : <?=fecha_hora($anterior["created_at"])?></em></p>
                                        <p class="h5 my-1"><em>Creado por: <?=$data['nombre']?></em></p>
                                    </div>
                        <?php } ?>
                        <?php $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id ='.$dato["oportunidad_id"].' AND H.key LIKE "comentario_modificacion" AND H.created_at = "'.$dato["created_at"].'" LIMIT 0,1';
                        $comentario = DB::select($query);
                        $comentario_cambio = (isset($comentario[0]->value))? $comentario[0]->value : "No hay comentario, la captura de comentario se implemento desde 3 de Febrero 2018";
                                    ?>
                            <div class="col-sm-12 col-md-6 text-center">
                                <p class="h3 py-2 tittle-n">Registro Nuevo</p>
                                <p class="h4 my-1"><?php if($dato["key"]=="fecha_oc"){ echo fecha($dato["value"]); }else{ echo $dato["value"]; } ?></p>
                                <p class="h5 my-1"><em>Creado el : <?=fecha_hora($dato["created_at"])?></em></p>
                                <p class="h5 my-1"><em>Modificado por: <?=$data['nombre']?></em></p>
                            </div>
                            <div class="col-sm-12 col-md-1 text-center">
                                <img src="<?=$request->dir?>/images/file/clientes/<?=$usuario->foto?>" class="b200" style="width: 50px;" title="<?=$usuario->nombres.' '.$usuario->apellidos?>">
                            </div>
                            <div class="col-sm-12 col-md-11 mb-3">
                                <div class="container py-2 px-2 mt-2 bg-white text-justify b10">
                                    <p class="h5 coment"><?=$comentario_cambio?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    $anterior = $dato;
                    }
                }
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
              /*Agregar una competencia a la oportunidad*/
              case 1:
                $dato = new OportunidadesCompetencias;
                $dato->id_oportunidad = $request->id_oportunidad;
                $dato->id_competencia = $request->id_competencia;
                if($dato->save()){
                  $data['msj'] = 'Guardado correctamente!';
                }else{
                  $data['msj'] = 'error al guardar!';
                }
                break;
              /*Competencias de la oportunidad*/
              case 2:
                $query = 'SELECT OC.id, C.nombre, CONCAT("/storage/competencias_files/profile_images/",C.logo) AS LOGO FROM oportunidades_competencias OC INNER JOIN competencias C ON OC.id_competencia = C.id WHERE OC.id_oportunidad = '.$request->id_oportunidad.' ORDER BY OC.id DESC';
                $competencias = DB::select($query);
                ob_start();
                if(count($competencias) == 0){ ?>
<div class="col-md-12">
    <p class="h5"><strong><em>No hay competencias agregadas para esta oportunidad</em></strong></p>
</div>
                <?php }else{ ?>
<div class="col-md-12">
    <ul class="list-unstyled activity-list">
        <li>
            <i class="fa fa-bell-o activity-icon pull-left"></i>
            <p class="text-left">
                <a href="#">Información</a> Debes dar click en una competencia para ver los equipos de cada <a href="#">Competencia</a> <span class="timestamp" id="numerocompetencias">Selecciona una competencia</span>
            </p>
        </li>
    </ul>
</div>
                <?php foreach($competencias as $competencia){
                    $query = 'SELECT IF(IsNull(SUM(E.valor_pesos)), 0, SUM(E.valor_pesos)) AS TOTAL FROM oportunidades_competencias_equipos E WHERE E.id_oportunidad_competencia = '.$competencia->id.' ORDER BY E.id';
                    $valor = DB::select($query);
                            ?>
<div class="col-xs-6 col-sm-3 col-md-2">
    <img class="img-round" data-toggle="modal" data-target="#ver_competencia" <?php if(!empty($competencia->LOGO)){ ?> src="<?=$competencia->LOGO?>" <?php }else{ ?> src="http://via.placeholder.com/40x40" <?php } ?> style="width:40px;cursor:pointer;" onclick="vercompetencia(<?=$competencia->id?>)">
    <p class="h5 text-primary"><?=$competencia->nombre?></p>
    <p class="h5 text-info"><strong><?='$ '.number_format($valor[0]->TOTAL)?></strong></p>
    <p><a class="text-danger" title="Eliminar <?=$competencia->nombre?>" style="cursor:pointer;" onclick="eliminar_competencia(<?=$competencia->id?>)"><i class="fa fa-trash"></i></a></p>
</div>
                <?php
                }
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
              /*Listado de competencias para que el usuario elija la que desee*/
              case 3:
                $query = 'SELECT C.* FROM competencias C WHERE C.deleted_at IS NULL AND C.id NOT IN (SELECT OP.id_competencia FROM oportunidades_competencias OP WHERE OP.id_oportunidad = '.$request->id_oportunidad.') ORDER BY C.id DESC';
                $competencias = DB::select($query);
                ob_start();
                foreach($competencias as $competencia){ ?>
<div class="col-md-2 no-select" onclick="competencia_agregar(<?=$competencia->id?>,'<?=$competencia->nombre?>')">
    <p><img class="img-round" <?php if(!empty($competencia->logo)){ ?> src="/storage/competencias_files/profile_images/<?=$competencia->logo?>" <?php }else{ ?> src="http://via.placeholder.com/40x40" <?php } ?> style="width:40px;"></p>
    <p class="h5 text-primary"><?=$competencia->nombre?></p>
    <p class="h6">Nit: <?=$competencia->nit?></p>
</div>
          <?php }
                if(count($competencias) == 0){ ?>
<div class="col-md-12 text-center">
    <p class="h5"><strong><em>No hay mas competencias por agregar</em></strong></p>
</div>
         <?php  }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
              /*Eliminar una competencia de la oportunidad*/
              case 4:
              $equipos = OportunidadesCompetenciasEquipos::where('id_oportunidad_competencia',$request->id)->get();
              foreach($equipos as $equipo){
                  $equipo->delete();
              }
              $dato = OportunidadesCompetencias::find($request->id);
              if($dato->delete()){
                $data['msj'] = 'Eliminado correctamente!';
              }else{
                $data['msj'] = 'Error al eliminar!';
              }
                break;
            /*Total de Oportunidades agregadas*/
            case 5:
                $competencias = OportunidadesCompetencias::where('id_oportunidad',$request->id_oportunidad)->get();
                $data['numero'] = count($competencias);
                break;
            /*Ver competencias*/
            case 6:
                $oportunidadcompetencia = OportunidadesCompetencias::find($request->id);
                $competencia = Competencia::find($oportunidadcompetencia->id_competencia);
                $equipos = OportunidadesCompetenciasEquipos::where('id_oportunidad_competencia',$oportunidadcompetencia->id)->get();
                $monedas = array('Dólar Estadounidense','Euro','Yen Japonés','Libra Esterlina','Franco Suizo','Dólar Australiano','Dólar Canadiense','Corona Sueca','Dólar de Hong Kong','Corona Noruega','Dólar Neozelandés','Peso mexicano','Peso Colombiano');
                ob_start(); ?>
<div class="modal-header">
    <h5 class="modal-title">Competencia <?=$competencia->nombre?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
   <input type="hidden" value="<?=$oportunidadcompetencia->id?>" id="id_oportunidadcompetencia">
    <div class="row">
        <div class="col-md-12 text-center">
            <p><a class="text-success" title="Agregar un producto" onclick="equipo_nuevo('abrir')" style="font-size:2rem;cursor:pointer;"><i class="fa fa-plus"></i></a></p>
        </div>
    </div>
    <div class="row" id="nuevo_equipo" style="display:none">
       <div class="col-md-12 text-right">
            <p><a class="text-danger" onclick="equipo_nuevo('cerrar')" title="Cerrar" style="text-size:2rem;cursor:pointer;"><i class="fa fa-times"></i></a></p>
        </div>
        <div class="col-sm-6 col-md-3">
           <p class="h5">Producto</p>
            <input type="text" class="form-control" value="" placeholder="Equipo o Producto" id="producto_nuevo" onkeyup="max_campo(this.value,100,'#texto_producto_nuevo',this.id)" required>
            <p class="text-right invisible" id="texto_producto_nuevo"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
        </div>
        <div class="col-sm-6 col-md-3">
            <p class="h5">Valor</p>
            <input type="text" class="form-control" value="" placeholder="Valor" id="valor_nuevo" onkeyup="formato_numero(this.value,this.id)">
        </div>
        <div class="col-sm-6 col-md-3">
           <p class="h5">Moneda</p>
            <input type="text" class="form-control" value="" id="moneda_nuevo" list="monedas" placeholder="moneda">
            <datalist id="monedas">
                <?php for($i=0;$i<13;$i++){ ?>
                <option value="<?=$monedas[$i]?>"></option>
                <?php } ?>
            </datalist>
        </div>
        <div class="col-sm-6 col-md-3">
            <p class="h5">Valor en pesos</p>
            <input type="text" class="form-control" value="" placeholder="Valor en pesos" id="valorpesos_nuevo" onkeyup="formato_numero(this.value,this.id)">
        </div>
        <div class="col-md-12 mt-3">
            <p class="h5">Descripción</p>
            <textarea class="form-control" value="" placeholder="Descripción" rows="8" id="descripcion_nuevo" onkeyup="max_campo(this.value,500,'#texto_descripcion_nuevo',this.id)"></textarea>
            <p class="text-right invisible" id="texto_descripcion_nuevo"><a class="text-success"><i class="fa fa-pencil"></i>... 1/500</a></p>
        </div>
        <div class="col-md-12 text-center mt-3">
            <a class="btn btn-success text-white" onclick="guardar_equipo('nuevo')"><i class="fa fa-save"></i> Guardar</a>
        </div>
    </div>
    <div class="row" id="lista_equipos">
        <?php if(count($equipos) == 0){ ?>
        <div class="col-md-12 text-center">
            <p class="h4"><strong><em>La lista de equipos esta vacia</em></strong></p>
        </div>
        <?php }else{ ?>
        <?php foreach($equipos as $equipo){ ?>
        <input type="hidden" value="<?=$equipo->id?>" id="id_<?=$equipo->id?>">
        <div class="col-md-11">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                   <p class="h5">Producto</p>
                    <input type="text" class="form-control formulario" value="<?=$equipo->equipo?>" placeholder="Equipo o Producto" id="producto_<?=$equipo->id?>" onkeyup="max_campo(this.value,100,'#texto_producto_<?=$equipo->id?>',this.id)" required readonly>
                    <p class="text-right invisible" id="texto_producto_<?=$equipo->id?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <p class="h5">Valor</p>
                    <input type="text" class="form-control formulario" value="$ <?=number_format($equipo->valor)?>" placeholder="Equipo o Producto" id="valor_<?=$equipo->id?>" onkeyup="formato_numero(this.value,this.id)" readonly>
                </div>
                <div class="col-sm-6 col-md-3">
                    <p class="h5">Moneda</p>
                    <input type="text" class="form-control formulario" value="<?=$equipo->moneda?>" list="monedas" id="moneda_<?=$equipo->id?>" placeholder="moneda" readonly>
                </div>
                <div class="col-sm-6 col-md-3">
                    <p class="h5">Valor en pesos</p>
                    <input type="text" class="form-control formulario" value="$ <?=number_format($equipo->valor_pesos)?>" placeholder="Valor en pesos" id="valorpesos_<?=$equipo->id?>" onkeyup="formato_numero(this.value,this.id)" readonly>
                </div>
            </div>
            <div class="row observaciones-ocultas" style="display:none;" id="fila_<?=$equipo->id?>">
                <div class="col-md-12">
                    <p class="h5">Descripción</p>
                    <textarea class="form-control formulario" placeholder="Descripción" rows="8" id="descripcion_<?=$equipo->id?>" onkeyup="max_campo(this.value,500,'#texto_descripcion_<?=$equipo->id?>',this.id)" readonly><?=$equipo->descripcion?></textarea>
                    <p class="text-right invisible" id="texto_descripcion_<?=$equipo->id?>"><a class="text-success"><i class="fa fa-pencil"></i>... 1/500</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-1 mt-5">
            <a class="text-success btn-editar-equipo" style="cursor:pointer;font-size:1.5rem;" id="btn_editar_equipo<?=$equipo->id?>" onclick="editar_equipo(<?=$equipo->id?>,this.id)" title="Editar el equipo"><i class="fa fa-edit"></i></a>
            <a class="text-success btn-guardar-equipo" style="cursor:pointer;font-size:1.5rem;display:none;" id="btn_editarguardadndo_equipo<?=$equipo->id?>" onclick="guardar_equipo(<?=$equipo->id?>)" title="Guardar el equipo"><i class="fa fa-save"></i></a>
            <a class="text-danger" style="cursor:pointer;font-size:1.5rem;" title="Eliminar el equipo" onclick="eliminar_equipo_competencia(<?=$equipo->id?>,<?=$oportunidadcompetencia->id?>)"><i class="fa fa-trash"></i></a>
            <a class="text-info" style="cursor:pointer;font-size:1.5rem;" id="btn_ver_equipo_<?=$equipo->id?>" onclick="ver_observacion_equipo(<?=$equipo->id?>,this.id)" title="Ver Observación"><i class="fa fa-eye"></i></a>
        </div>
        <?php } } ?>
    </div>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 7:
                if(!isset($request->id)){
                    $dato = new OportunidadesCompetenciasEquipos;
                }else{
                    $dato = OportunidadesCompetenciasEquipos::find($request->id);
                }
                $dato->id_oportunidad_competencia = $request->id_oportunidad_competencia;
                $dato->equipo = $request->equipo;
                $dato->valor = intval(str_replace("$ ","",str_replace(",","",$request->valor)));
                $dato->moneda = $request->moneda;
                $dato->valor_pesos = intval(str_replace("$ ","",str_replace(",","",$request->valor_pesos)));
                $dato->descripcion = $request->descripcion;
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            case 8:
                $dato = OportunidadesCompetenciasEquipos::find($request->id);
                if($dato->delete()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }
    public function index(){
      /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){
          $data['item_menu']='menu_'.$data['item_menu'][0]->id;
      }else{
          $data['item_menu']='';
      }
      /*Fin Consulta para activar el item del menu correspondiente*/
    	$modulo=3;
    	$acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="crear oportunidad" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){
				return view('oportunidades.create',['data' => $data]);
			  }else{
				return view('oportunidades.nopermiso');
			  }
            }
          }
        }
    }

    public function GestionesOp($id){
        $query = ('SELECT * FROM historial_oportunidades H WHERE H.oportunidad_id = '.$id.' AND H.key LIKE "comentario_gestiones" ORDER BY H.id ASC');
        $data['comentarios'] = DB::select($query);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para crear un comentario*/
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear comentario en calendario de gestion" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_crear_comentario']='Si';
			  }else{
				$data['permiso_crear_comentario']='No';
			  }
            }
          }
        }
        /*Fin Permiso para crear un comentario*/

        /*Permiso Boton de ciclo venta*/
        $data['permiso_btn_ciclo'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón ciclo venta en listados y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_btn_ciclo']='Si';
			  }
            }
          }
        }
      return view('oportunidades.gestiones',['data' => $data, 'id' => $id]);
    }

    public function actiongestiones(Request $request){
        $accion = $request->action;
        switch($accion){
            /*Crea contenido sweetAlert para crear comentario*/
            case 0:
                ob_start();?>
<div class="row mb-5">
    <div class="col-md-6">
        <input class="form-control fecha-filtro" type="text" id="fecha_ini" placeholder="fecha inicio" required>
    </div>
    <div class="col-md-6">
        <input class="form-control fecha-filtro" type="text" id="fecha_fin" placeholder="fecha fin" required>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <textarea id="comentario_administrador"><p style="text-align: center; font-size: 15px;"><img title="TinyMCE Logo" src="<?=$request->dir?>/images/file/clientes/essi_admon.jpg" alt="TinyMCE Logo" width="100" height="100" />
  </p>
  <h1 style="text-align: center;">Bienvenido al editor de comentario</h1>
  <h5 style="text-align: center;">Aquí puede escribir el comentario con las herramientas básicas de edición</h5>
</textarea>
    </div>
</div>              <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
            case 1:
                $dato = new HistorialOportunidades;
                $dato->key = 'comentario_gestiones';
                $dato->value = $request->value;
                $dato->descripcion = $request->comentario;
                $dato->oportunidad_id = $request->oportunidad_id;
                $dato->user_id = Auth::user()->id;
                if($dato->save()){
                    $data['msj']='Guardado correctamente!';
                    $data['guardo']='Si';
                }else{
                    $data['msj']='Error al guardar!';
                    $data['guardo']='No';
                }
                break;
            case 2:
                $query = 'SELECT H.value AS fechas, H.descripcion AS HTML FROM historial_oportunidades H WHERE H.value LIKE "%'.$request->fecha.'%" AND H.oportunidad_id = '.$request->oportunidad_id.' LIMIT 0,1';
                $comentarios = DB::select($query);
                $fechas = explode(" ",$comentarios[0]->fechas);
                ob_start();?>
<div class="row mb-5">
    <div class="col-md-6">
        <p class="h3">Fecha Inicio: <br><small><?=$this->formt_fecha($fechas[0])?></small></p>
    </div>
    <div class="col-md-6">
        <p class="h3">Fecha Fin: <br><small><?=$this->formt_fecha($fechas[(count($fechas))-1])?></small></p>
    </div>
</div>
<div class="row rounded punteado">
    <div class="col-md-12">
        <div class="container my-5">
            <?=str_replace("h1","h2",$comentarios[0]->HTML)?>
        </div>
    </div>
</div>               <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function traerDatosOp($id, $fecha)
    {
      $ciclosventa    = DB::select("SELECT H.id, H.key, H.value, H.oportunidad_id, H.descripcion, C.nombre, U.foto, H.created_at FROM historial_oportunidades H INNER JOIN ciclos C ON C.porcentaje = H.value INNER JOIN users U ON U.id = H.user_id WHERE H.key LIKE 'ciclo_venta' AND H.oportunidad_id = :id AND H.descripcion IS NOT NULL AND H.created_at BETWEEN :fechai AND :fechaf ORDER BY H.created_at ASC", ['id' => $id, 'fechai' => $fecha." 00:00:00.000000", 'fechaf' => $fecha." 23:59:59.000000"]);
      $historialop    = HistorialOportunidades::where('oportunidad_id',$id)->where('key','!=',"radio")->whereBetween('created_at', [$fecha." 00:00:00.000000", $fecha." 23:59:59.000000"])->get();
      $actividades    = Actividades::where('oportunidad_id',$id)->whereBetween('created_at', [$fecha." 00:00:00.000000", $fecha." 23:59:59.000000"])->get();
      $agendacliente  = Agenda_cliente::where('oportunidad',$id)->whereBetween('created_at', [$fecha." 00:00:00.000000", $fecha." 23:59:59.000000"])->get();
      $pendientes     = Pendiente::where('oportunidad_id',$id)->whereBetween('created_at', [$fecha." 00:00:00.000000", $fecha." 23:59:59.000000"])->get();
      $hist_pendientes= DB::select("SELECT P.id, P.responsable, P.detalle, P.tipo, H.comentario, H.estado, H.fecha_original, H.fecha_nueva, H.created_at FROM pendientes P INNER JOIN pendientes_historiales H ON P.id = H.pendientes_id WHERE P.oportunidad_id = :id AND H.created_at BETWEEN :fechai AND :fechaf", ['id' => $id, 'fechai' => $fecha." 00:00:00.000000", 'fechaf' => $fecha." 23:59:59.000000"]);
      foreach ($actividades as $value) {
        $user = User::find($value->user_id);
        $value->fotoUser = $user->foto;
      }
      foreach ($historialop as $value) {
        $anterior = HistorialOportunidades::where('oportunidad_id', $value->oportunidad_id)->where('key', $value->key)->where('created_at', '<', $value->created_at)->select('value')->orderBy('id', 'desc')->latest()->first();
        $value->antvalue = "";
        if ($value->key === "sede") {
          if (isset($anterior) && !empty($anterior)) {
            $antsede = EmpresaSedes::find($anterior->value);
            $antempresa = Empresa::find($antsede->empresa_id);
            $value->antvalue = $antempresa->nombre."/".$antsede->pais;
          }

          $sede = EmpresaSedes::find($value->value);
          $empresa = Empresa::find($sede->empresa_id);
          $value->value = $empresa->nombre."/".$sede->pais;
          $value->key = 'empresa';
        }elseif ($value->key === "cliente") {
          if (isset($anterior) && !empty($anterior)) {
            $antcliente = Cliente::find($anterior->value);
            $value->antvalue = $antcliente->cliente;
          }

          $cliente = Cliente::find($value->value);
          $value->value = $cliente->cliente;
        }elseif ($value->key === "responsable") {
          if (isset($anterior) && !empty($anterior) && is_numeric($anterior->value)) {
            $antuser = User::find($anterior->value);
            $value->antvalue = $antuser->name;
          }

          $user = User::find($value->value);
          $value->value = $user->name;
        }elseif ($value->key === "identifico") {
          if (isset($anterior) && !empty($anterior) && is_numeric($anterior->value)) {
              $value->key = "identificó";
              $antuser = User::find($anterior->value);
              $value->antvalue = $antuser->name;
          }

          $user = User::find($value->value);
          $value->value = $user->name;
        }elseif ($value->key === "fecha_ff") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value;}else{$value->antvalue = "";}
          $value->key = 'fecha de cierre';
        }elseif ($value->key === "fecha_oc") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value;}else{$value->antvalue = "";}
          $value->key = 'fecha de apertura';
        }elseif ($value->key === "ciclo_venta") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value.'%';}else{$value->antvalue = "";}
          $value->key = 'ciclo de venta';
          $value->value = $value->value.'%';
        }elseif ($value->key === "tasa_cambio") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value;}else{$value->antvalue = "";}
          $value->key = 'tasa de cambio';
        }elseif ($value->key === "val_pesos") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value;}else{$value->antvalue = "";}
          $value->key = 'valor en pesos';
        }elseif ($value->key === "valor_oportunidad") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value;}else{$value->antvalue = "";}
          $value->key = 'valor de la oportunidad';
        }elseif ($value->key === "margen") {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value.'%';}else{$value->antvalue = "";}
          $value->value = $value->value.'%';
        }else {
          if (isset($anterior) && !empty($anterior)) {$value->antvalue = $anterior->value;}else{$value->antvalue = "";}
        }
      }
      foreach($pendientes as $modelo){
          $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
          $modelo->aplazadas = $aplazadas[0]->total;
          $usuario = User::find($modelo->responsable);
          $modelo->nombreuser = $usuario->name;
          $modelo->fotoUser = $usuario->foto;
      }
      foreach($hist_pendientes as $modelo){
          $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
          $modelo->aplazadas = $aplazadas[0]->total;
          $usuario = User::find($modelo->responsable);
          $modelo->nombreuser = $usuario->name;
          $modelo->fotoUser = $usuario->foto;
      }
      foreach ($agendacliente as $value) {
        $user = User::find($value->user_id);
        $value->fotoUser = $user->foto;
        $agentclientact = Agenda_clientes_actividade::where('agenda_clientes', $value->id)->get();
        $value->visitantes = Agenda_clientes_visitante::where("agenda_clientes", $value->id)->get();
        foreach ($value->visitantes as $model) {
          $client = Cliente::find($model->cliente);
          $model->nameClient = $client->cliente;
          $model->trClient = $client->trato;
          $model->fotoClient = $client->foto;
          $model->cargoClient = $client->cargo;
        }
        $value->countActividades = count($agentclientact);
      }
      return response()->json(['success' => true, 'historialop' => $historialop, 'actividades' => $actividades, 'pendientes' => $pendientes, 'hist_pendientes' => $hist_pendientes, 'agendacliente' => $agendacliente, 'ciclosventa' => $ciclosventa]);
    }

    public function ValidarOpGestiones($id)
    {
      $ciclosventa   = DB::select("SELECT COUNT(`created_at`) AS total, `created_at` FROM historial_oportunidades WHERE `key` LIKE 'ciclo_venta' AND `descripcion` IS NOT NULL AND `oportunidad_id`=".$id." GROUP BY `created_at`");
      $historialop   = DB::select('SELECT COUNT(`created_at`) AS total, `created_at` FROM `historial_oportunidades` WHERE `key` != "comentario_gestiones" AND `oportunidad_id`='.$id.' GROUP BY `created_at`');
      $actividades   = DB::select('SELECT COUNT(`created_at`) AS total, `created_at` FROM `actividades` WHERE `oportunidad_id`='.$id.' GROUP BY `created_at`');
      $pendientes    = DB::select('SELECT COUNT(`created_at`) AS total, `created_at` FROM `pendientes` WHERE `oportunidad_id`='.$id.' GROUP BY `created_at`');
      $agendacliente = DB::select('SELECT COUNT(`created_at`) AS total, `created_at` FROM `agenda_clientes` WHERE `oportunidad`='.$id.' GROUP BY `created_at`');
      $hist_pendientes= DB::select("SELECT COUNT(H.created_at) AS total, H.created_at FROM pendientes P INNER JOIN pendientes_historiales H ON P.id = H.pendientes_id WHERE P.oportunidad_id='.$id.' GROUP BY H.created_at");

      foreach ($historialop as $key => $value) {$value->tipo = "historial";}
      foreach ($actividades as $key => $value) {$value->tipo = "actividades";}
      foreach ($pendientes as $key => $value) {$value->tipo = "pendientes";}
      foreach ($hist_pendientes as $key => $value) {$value->tipo = "pendientes";}
      foreach ($agendacliente as $key => $value) {$value->tipo = "agendavisita";}
      foreach ($ciclosventa as $key => $value) {$value->tipo = "ciclo_venta";}
      $oportunidad=Oportunidades::find($id);
      $fecha_vendida= $oportunidad->created_at;
      $observaciones=HistorialOportunidades::where('oportunidad_id', $id)->where('key', 'observaciones')->select('value')->orderBy('id', 'desc')->latest()->first();
      $fecha_inic=HistorialOportunidades::where('oportunidad_id', $id)->where('key', 'fecha_ff')->select('value')->orderBy('id', 'desc')->latest()->first();
      $fecha_fin=HistorialOportunidades::where('oportunidad_id', $id)->where('key', 'fecha_oc')->select('value')->orderBy('id', 'desc')->latest()->first();

      $arrayOP = array_merge($historialop, $actividades, $pendientes, $agendacliente, $ciclosventa, $hist_pendientes);
      if (isset($observaciones->value) && !empty($observaciones->value)) {
        return response()->json(['success' => true, 'modelo' => $arrayOP, 'fecha' => $fecha_vendida, 'observaciones' => $observaciones->value, 'fechainit' => $fecha_inic->value, 'fechafin' => $fecha_fin->value]);
      }else {
        return response()->json(['success' => true, 'modelo' => $arrayOP, 'fecha' => $fecha_vendida, 'fechainit' => $fecha_inic->value, 'fechafin' => $fecha_fin->value]);
      }
    }

    public function listOportunidades(Request $request){
    	$datos = Oportunidades::all();
      $datos = $datos->sortBy('created_at')->reverse();

      $valid_tags = [];
		  foreach ($datos as $value) {
        	$historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
        	try {
        		$otro = $historia->filter(function ($value, $key) {
					return $value->key == "fecha_oc";
				})->pop();
        		$start = $otro->value;

        		if ($start >= $request->start && $start <= $request->end) {
					$otro = $historia->filter(function ($value, $key) {
						return $value->key == "val_pesos";
					})->pop();
					$val_pesos = $otro->value;

					if ($value->estado == "vendido") {
						$vendida = Venta::where("oportunidad", $value->id)->get()->pop();
						$color = "#A3DE83";
						$textColor = "#272121";
						$url = url('/')."/venta/".$vendida->id;
					}
					if ($value->estado == "pendiente") {
						$color = "#E8B844";
						$textColor = "#272121";
						$url = url('/')."/oportunidad/".$value->id;
					}
					if ($value->estado == "eliminada") {
						$color = "#F03861";
						$textColor = "#F7F7F7";
						$url = url('/')."/veroportunidadperdida/".$value->id;
					}

					$valid_tags[] = [
						'id' 		=> $value->id,
						'title' 	=> $value->empresa."/".$value->ciudad."\n $ ".$val_pesos,
						'start' 	=> $start,
						'color' 	=> $color,
						'textColor' => $textColor,
						'url'		=> $url
					];
				}
        	} catch (Exception $e) {}
        }
		  return response()->json($valid_tags);
    }

    public function calendarioList(){
      /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){
          $data['item_menu']='menu_'.$data['item_menu'][0]->id;
      }else{
          $data['item_menu']='';
      }
      /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('oportunidades.calendar',['data' => $data]);
    }

    public function edit($id){
    	$modulo=3;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="editar oportunidad" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "propia"){
              	$responsable = DB::select('SELECT * FROM `historial_oportunidades` WHERE `key`="responsable" AND `value`="'.Auth::user()->id.'" AND `oportunidad_id` = "'.$id.'" ORDER BY `id` DESC');

              	if(isset($responsable[0]->user_id)){
          			$oportunidad = Oportunidades::findOrFail($id);
			    	$datos = HistorialOportunidades::where('oportunidad_id', $id)->get();
			    	$otro = $datos->filter(function ($value, $key) {
			          return $value->key == "cliente";
			        });
			        $casi = $otro->pop();
			        try {
			        	$cliente = Cliente::find($casi->value);
			        } catch (Exception $e) {

			        }

			        $empresa = Empresa::findOrFail($oportunidad->empresa_id);
                    /*Consulta para activar el item del menu correspondiente*/
                    $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
                    $data['item_menu'] = DB::select($query);
                    if(count($data['item_menu']) > 0){
                        $data['item_menu']='menu_'.$data['item_menu'][0]->id;
                    }else{
                        $data['item_menu']='';
                    }
                    /*Fin Consulta para activar el item del menu correspondiente*/
			        return view('oportunidades.update',
			        	[
                            'data' => $data,
			        		'datos' => $datos,
			        		'oportunidad' => $oportunidad,
			        		'cliente' => $cliente,
			        		'empresa' => $empresa
			        	]
			        );
              	}else{
              		return view('oportunidades.nopermiso');
              	}
              }else if($permiso[0]->permiso == "TODAS"){
              	$oportunidad = Oportunidades::findOrFail($id);
		    	$datos = HistorialOportunidades::where('oportunidad_id', $id)->get();
		    	$otro = $datos->filter(function ($value, $key) {
		          return $value->key == "cliente";
		        });
		        $casi = $otro->pop();
		        try {
		        	$cliente = Cliente::find($casi->value);
		        } catch (Exception $e) {

		        }

		        $empresa = Empresa::findOrFail($oportunidad->empresa_id);
                /*Consulta para activar el item del menu correspondiente*/
                $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
                $data['item_menu'] = DB::select($query);
                if(count($data['item_menu']) > 0){
                    $data['item_menu']='menu_'.$data['item_menu'][0]->id;
                }else{
                    $data['item_menu']='';
                }
                /*Fin Consulta para activar el item del menu correspondiente*/
		        return view('oportunidades.update',
		        	[
		        		'data' => $data,
                        'datos' => $datos,
		        		'oportunidad' => $oportunidad,
		        		'cliente' => $cliente,
		        		'empresa' => $empresa
		        	]
		        );
              }else{
              	return view('oportunidades.nopermiso');
              }
          	}
          }
      	}
    }

    public function view($id){
    	$new= new Oportunidades_ingreso();
    	$new->ip=ObtenerIP();
    	$new->oportunidad_id=$id;
    	$new->user_id = Auth::user()->id;
    	$new->save();

    	$datos = Oportunidades::findOrFail($id);
        ob_start();
        if($datos->id_user_freelance != ''){
            $freelance = ViewFreelanceLista::find($datos->id_user_freelance);
            if(isset($freelance->id)){
?>
<div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="<?=$FOTO=$freelance->FOTOPERFIL!=''?$freelance->FOTOPERFIL:'http://via.placeholder.com/250x250';?>" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading"><?=$freelance->NOMBRECORTO?> </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
       <?php }else{ ?>
<div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/250x250" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading">No hay un freelance asignado </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php } }else{ ?>
 <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/250x250" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading">No hay un freelance asignado </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php }
        $data['freelance'] = ob_get_contents();
        ob_end_clean();
        $data['competencias'] = Competencia::all();
        $data['competencias_oportunidad'] = OportunidadesCompetencias::where('id_oportunidad', $datos->id)->get();
    	$historia = HistorialOportunidades::where('oportunidad_id', $id)->get();
    	$actividades = Actividades::where('oportunidad_id', $id)->get()->sortByDesc('updated_at');
    	$otro = $historia->filter(function ($value, $key) {
          return $value->key == "cliente";
        });
        $casi = $otro->pop();
        if(isset($casi->value)){
            $cliente = Cliente::find($casi->value);
        }else{
            $cliente = "Sin cliente";
        }

        $empresa = Empresa::findOrFail($datos->empresa_id);

        $agentclient = Agenda_cliente::where('oportunidad', $id)->get();
        $actidetalle = Pendiente::where('oportunidad_id', $id)->get();

        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$id.' AND H.key LIKE "cliente" ORDER BY H.id DESC LIMIT 0,1';
        $responsable = DB::select($query);
        if(isset($responsable[0]->value)){
            $c = Cliente::find($responsable[0]->value);
            $data['cliente_responsable'] = isset($c->cliente)?$c->cliente:'No existe cliente responsable';
        }else{
            $data['cliente_responsable'] = 'No existe cliente responsable';
        }


        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar de los listados y en los ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$permiso_editar='Si';
			  }else{
				$permiso_editar='No';
			  }
            }
          }
        }

        $data['permiso_btn_ciclo'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón ciclo venta en listados y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_btn_ciclo']='Si';
			  }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('oportunidades.view',['data' => $data, 'datos' => $historia,'oportunidad' => $datos,'cliente' => $cliente,'empresa' => $empresa,'actividades' => $actividades,'agentclients' => $agentclient, 'actidetalles' => $actidetalle, 'permiso_editar' => $permiso_editar]);
    }

    public function viewperdida($id)
    {
    	$new= new Oportunidades_ingreso();
    	$new->ip=ObtenerIP();
    	$new->oportunidad_id=$id;
    	$new->user_id = Auth::user()->id;
    	$new->save();

    	$datos = Oportunidades::findOrFail($id);
        $data['competencias_oportunidad'] = OportunidadesCompetencias::where('id_oportunidad', $datos->id)->get();
        ob_start();
        if($datos->id_user_freelance != ''){
            $freelance = ViewFreelanceLista::find($datos->id_user_freelance);
            if(isset($freelance->id)){
?>
<div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="<?=$FOTO=$freelance->FOTOPERFIL!=''?$freelance->FOTOPERFIL:'http://via.placeholder.com/250x250';?>" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading"><?=$freelance->NOMBRECORTO?> </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php }else{ ?>
<div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/250x250" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading">No hay un freelance asignado </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php } }else{ ?>
 <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/250x250" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading">No hay un freelance asignado </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php }
        $data['freelance'] = ob_get_contents();
        ob_end_clean();
    	$historia = HistorialOportunidades::where('oportunidad_id', $id)->get();
    	$actividades = Actividades::where('oportunidad_id', $id)->get()->sortByDesc('updated_at');
    	$otro = $historia->filter(function ($value, $key) {
          return $value->key == "cliente";
        });
        $casi = $otro->pop();
        $cliente = Cliente::findOrFail($casi->value);

        $empresa = Empresa::findOrFail($datos->empresa_id);

        $agentclient = Agenda_cliente::where('oportunidad', $id)->get();
        $actidetalle = Pendiente::where('oportunidad_id', $id)->get();

        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar de los listados y en los ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$permiso_editar='Si';
			  }else{
				$permiso_editar='No';
			  }
            }
          }
        }
        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$id.' AND H.key LIKE "cliente" ORDER BY H.id DESC LIMIT 0,1';
        $responsable = DB::select($query);
        if(isset($responsable[0]->value)){
            $c = Cliente::find($responsable[0]->value);
            $data['cliente_responsable'] = $c->cliente;
        }else{
            $data['cliente_responsable'] = 'No existe cliente responsable';
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('oportunidades.perdidas.view2',['data' => $data, 'datos' => $historia,'oportunidad' => $datos,'cliente' => $cliente,'empresa' => $empresa,'actividades' => $actividades,'agentclients' => $agentclient, 'actidetalles' => $actidetalle, 'id' => $id, 'permiso_editar' => $permiso_editar]);
    }

	public function getComentarios($id){
    	$datos = OportunidadComentario::where('oportunidad_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		$user = User::findOrFail($value->user_id);
    		$value->user = $user;
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function saveComentario(Request $request){
	    $dato = new OportunidadComentario;
		$dato->comentario = $request->comentario;
		$dato->oportunidad_id = $request->oportunidad_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

	public function acciones($id){
		$ciclos=Ciclo::all();
		$dato = Oportunidades::find($id);
		$historia = HistorialOportunidades::where('oportunidad_id', $id)->where('key', 'ciclo_venta')->get()->pop();
		$historias = HistorialOportunidades::where('oportunidad_id', $id)
											->where('key', 'ciclo_venta')
											->where('id', "!=", $historia->id)->get();
		$ingresos=Oportunidades_ingreso::where('oportunidad_id', $id)->get()->sortBy('created_at')->reverse();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permisos botón ciclos de venta*/
        $modulo = 3;
        $data['permiso_ciclos_venta'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botones internos ciclo de venta" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_ciclos_venta']='Si';
			  }
            }
          }
        }
		return view('acciones',['data' => $data, "dato"=>$dato,"historia"=>$historia,"historias"=>$historias,"ingresos"=>$ingresos,"ciclos"=>$ciclos]);
	}

	public function acciones2($id){
		$datos = Oportunidades::findOrFail($id);
		$historia = HistorialOportunidades::where('oportunidad_id', $id)->get();
		$ciclos=Ciclo::all();
		$archivos=Historial_oportunidades_archivo::where('oportunidad', $id)->get();
		$comentarios=Historial_oportunidades_comentario::where('oportunidad_id', $id)->get();
		$ingresos=Oportunidades_ingreso::where('oportunidad_id', $id)->get()->sortBy('created_at')->reverse();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('acciones2',['data' => $data, 'datos' => $datos,"historial"=>$historia,"ciclos"=>$ciclos,"archivos"=>$archivos,"comentarios"=>$comentarios,"ingresos"=>$ingresos]);
	}

	public function accionesperdidas($id)
	{
		$ciclos=Ciclo::all();
		$dato = Oportunidades::find($id);
		$historia = HistorialOportunidades::where('oportunidad_id', $id)->where('key', 'ciclo_venta')->get()->pop();
		$historias = HistorialOportunidades::where('oportunidad_id', $id)
											->where('key', 'ciclo_venta')
											->where('id', "!=", $historia->id)->get();
		$ingresos=Oportunidades_ingreso::where('oportunidad_id', $id)->get()->sortBy('created_at')->reverse();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('oportunidades.perdidas.ciclos',["data" => $data, "dato"=>$dato,"historia"=>$historia,"historias"=>$historias,"ingresos"=>$ingresos,"ciclos"=>$ciclos, 'id' => $id]);
	}

	public function leccionesaprendidas($id)
	{
		$dato = Oportunidades_perdida::where("oportunidad",$id)->get()->pop();
		$user = User::find($dato->user_id);
        $query = 'SELECT nombre FROM competencias WHERE id = '.intval($dato->empresa);
        $data['competencia'] = DB::select($query);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('oportunidades.perdidas.lecciones',['data' => $data, 'dato' => $dato, 'id' => $id, 'user' => $user]);
	}

	public function comentarios(){
		$id=$_GET["id"];
		$historia = Historial_oportunidades_comentario::where('historial_oportunidades', $id)->get();
		$contenido='';
		$historia=burbuja3($historia);
		foreach ($historia as $key) {
			# code...
			list($fecha,$hora)=explode(" ",$key->created_at);
			$resta= strtotime(date('Y-m-d'))-strtotime($fecha);
            $fin =round($resta/(60*60*24));
			$datos = User::findOrFail($key->user_id);
			$contenido.='<div class="commen">
						 <div class="img-commen" style="background-image: url(../images/file/clientes/'.$datos->foto.')" ></div>
                          <div class="comentario"><span class="user-commen">'.ucwords($datos->nombres." ".$datos->apellidos).'</span>
                          <p>'.ucfirst($key->comentario).'</p>
                          <span class="time-commen"><i class="fa fa-clock-o"></i> hace '.$fin.' dias</span>
                        </div></div>';
		}
		if(count($historia)<1){
			$contenido.='<div class="commen" >
                          <p style="text-align:center !important">No se han encontrado comentarios sobre esta etapa del ciclo</p>
                        </div>';
		}
		return ["cuerpo"=>$contenido];
	}

	public function saveciclo(Request $request){
		try {
			foreach ($request->datos as $key) {
				# code...
				$historia = HistorialOportunidades::where('oportunidad_id', $request->id)
													->where('key', 'ciclo_venta')
													->where('value', $key["value"])->get()->pop();
				$datos=new HistorialOportunidades();
				$datos->key="ciclo_venta";
				$datos->value=$key["value"];
				$datos->descripcion=$key["descripcion"];
				$datos->oportunidad_id=$request->id;
				$datos->user_id = Auth::user()->id;
				if (isset($historia->created_at) && !empty($historia->created_at)) {
					$datos->created_at = $historia->created_at;
				}
				$datos->save();

				if(isset($key['file']) && !empty($key['file'])){

				$logo = $key['file'];
					for ($i=0;$i<count($logo);$i++) {
						# code...
						$antenombrelogo = uniqid();
						$extensionlogo = $logo[$i]->getClientOriginalExtension();
						$nombrelogo =$antenombrelogo.".".$extensionlogo;
						\Storage::disk('oportunidades')->put($nombrelogo,  \File::get($logo[$i]));

						$dato = new Historial_oportunidades_archivo;
						$dato->historial_oportunidades = $datos->id;
						$dato->archivo  = $nombrelogo;
						$dato->oportunidad = $request->id;
						$dato->user_id = Auth::user()->id;
						$dato->save();
					}
				}

				if($key["value"]==0){
					$dato=Oportunidades::findOrFail($request->id);
					$dato->estado="perdida";
					$dato->save();
				}elseif($key["value"]==90){
					$dato=Oportunidades::findOrFail($request->id);
					$dato->estado="vendido";
					$dato->save();

					$datos=new Venta;
					$datos->oportunidad=$request->id;
					$datos->user_id = Auth::user()->id;
					$datos->save();
				}
			}
		} catch (Exception $e) {}
		return redirect("/acciones/".$request->id);
	}

	public function savefiles(Request $request){
		$logo = $request->file('file-1');
		$datos = HistorialOportunidades::findOrFail($request->id);
		if ($logo) {
			for ($i=0;$i<count($logo);$i++) {
				# code...
				$antenombrelogo = uniqid();
				$extensionlogo 	= $logo[$i]->getClientOriginalExtension();
				$asname 		= $logo[$i]->getClientOriginalName();
				$nombrelogo 	=$antenombrelogo.".".$extensionlogo;
				\Storage::disk('oportunidades')->put($nombrelogo,  \File::get($logo[$i]));

				$dato 							= new Historial_oportunidades_archivo;
				$dato->historial_oportunidades 	= $request->id;
				$dato->archivo  				= $nombrelogo;
				$dato->asname					= $asname;
				$dato->oportunidad 				=$datos->oportunidad_id;
				$dato->user_id 					= Auth::user()->id;
				$dato->save();
			}

		}

		return redirect("/acciones/".$datos->oportunidad_id);
	}

	public function deletefile(Request $request){
		Historial_oportunidades_archivo::destroy($request->id);

		return redirect("/acciones/".$request->oportunidad);
	}

	public function savecomments(Request $request){
		$datos = HistorialOportunidades::findOrFail($request->id);

		$dato = new Historial_oportunidades_comentario;
		$dato->historial_oportunidades = $request->id;
		$dato->comentario  = $request->comentario;
		$dato->oportunidad_id=$datos->oportunidad_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return redirect("/acciones/".$datos->oportunidad_id);
	}

	public function avanzar(){
		$actual=$_GET["actual"];
		$siguiente=$_GET["siguiente"];
		$ciclos=Ciclo::all();
		$contenido='';
		$k=0; for ($i=$actual+1; $i <=$siguiente ; $i++) {
			# code...
			for ($o=0; $o<count($ciclos);$o++) {

				if($ciclos[$o]->porcentaje==$i){
					$label="Observaciones para el ciclo ".$ciclos[$o]->nombre;
							$contenido.='<div class="form-group" style="border-bottom: 0.5px solid #ccc;padding-bottom: 1.5rem;">
							<input type="hidden" name="datos['.$k.'][value]" value="'.$i.'">
		                    <label for="observaciones" class="col-form-label">'.$label.'</label>
		                    <textarea class="form-control" name="datos['.$k.'][descripcion]" rows="2" placeholder="Por favor ingrese una observacion" required></textarea>
		                </div>
		                <div class=" form-group row">
				            <div class="col-md-12 text-center">
								    <input class="fileuploader" name="datos['.$k.'][file]" type="file" multiple />
				            </div>
				         </div>
		                ';

		            $k++;
				}
			}

		}
		return ["cuerpo"=>$contenido];
	}


	public function save(Request $request){
  		$buscar = $request->all();
  		$input['ip'] = $request->ip();

  		$sede = EmpresaSedes::findOrFail($request->sede);
  		$empresa = Empresa::findOrFail($request->empresa);
  		$id  = Paises::where('name', $sede->pais)->pluck('sortname');
  		$cod = Oportunidades::max('id') + 1;
  		$j   = strlen($cod);
  		if ($j<4) {
  			for ($i=$j; $i<4; $i++) {
  				$cod="0".$cod;
  			}
  		}
	    $dato = new Oportunidades;
      if (isset($id[0])) {
        $dato->titulo = "OP-".$id[0]."-".$cod." ".$empresa->nombre."/".$sede->ciudad;
      }

	    $dato->ip = $input['ip'];
	    $dato->user_id = Auth::user()->id;
	    $dato->pais = $sede->pais;
	    $dato->departamento = $sede->departamento;
	    $dato->ciudad = $sede->ciudad;
	    $dato->empresa_id = $request->empresa;
	    $dato->empresa = $empresa->nombre;
	    $dato->save();

	    if (isset($request->productos)) {
			foreach ($request->productos as $datoproduc) {
				$productos = new OportunidadProducto;
				$productos->producto = $datoproduc["producto"];
				if (isset($datoproduc["cantidad"])) { $productos->cantidad = $datoproduc["cantidad"]; }
				if (isset($datoproduc["referencia"])) { $productos->referencia = $datoproduc["referencia"]; }
				if (isset($datoproduc["valor"])) { $productos->valor = $datoproduc["valor"]; }
				if (isset($datoproduc["total"])) { $productos->total = $datoproduc["total"]; }
				$productos->oportunidad_id = $dato->id;
    			$productos->save();
		    }
		    $proccompa = json_encode($request->productos);
		    $his = new HistorialOportunidades;
  			$his->key = "productos";
  			$his->value = $proccompa;
  			$his->oportunidad_id = $dato->id;
  			$his->user_id = Auth::user()->id;
    		$his->save();
	    }

		foreach ($buscar as $key => $value) {
			if ($key != "_token" && $key != "productos" && $key != "empresa" && $key != "forma_pago" && $key != "otro" && $value != "" && !empty($value)) {
				$his = new HistorialOportunidades;
				$his->key = $key;
				$his->value = $value;
				$his->oportunidad_id = $dato->id;
				$his->user_id = Auth::user()->id;
	    		$his->save();
			}
		}

		if (isset($request->forma_pago) && !empty($request->forma_pago)) {
			foreach ($request->forma_pago as $fpago) {
				$his = new OportunidadesFormapago;
				if (isset($fpago["cuota_valor"])) {
					$his->key = "cuota_valor";
					$his->value = $fpago["cuota_valor"];
				}else{
					$his->key = "anticipo";
					$his->value = $fpago["anticipo"];
				}
				if (isset($fpago["cuota_texto"])) {
					$his->texto = $fpago["cuota_texto"];
					$his->num_cuota = $fpago["cuota_numero"];
				}

				$his->oportunidad_id = $dato->id;
				$his->user_id = Auth::user()->id;
    			$his->save();
		    }
	    }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('oportunidades.create',['data' => $data]);
	}

	public function editsave(Request $request){
		$buscar = $request->all();
		$input['ip'] = $request->ip();

		$sede = EmpresaSedes::find($request->sede);
		$empresa = Empresa::find($request->empresa);
		$id  = Paises::where('name', $sede->pais)->pluck('sortname');
		$cod = Oportunidades::max('id') + 1;
		$j   = strlen($cod);
		if ($j<4) {
			for ($i=$j; $i<4; $i++) {
				$cod="0".$cod;
			}
		}
	    $dato = Oportunidades::find($request->id);
	    $dato->titulo = "OP-".$id[0]."-".$cod." ".$empresa->nombre."/".$sede->ciudad;
	    $dato->ip = $input['ip'];
	    $dato->pais = $sede->pais;
	    $dato->departamento = $sede->departamento;
	    $dato->ciudad = $sede->ciudad;
	    $dato->empresa_id = $request->empresa;
	    $dato->empresa = $empresa->nombre;
	    $dato->save();

	    $historia = HistorialOportunidades::where('oportunidad_id', $request->id)->get();
	    $casi = $historia->where('key', 'productos')->pop();
	    $proccompa = json_encode($request->productos);

	    if (isset($request->productos) && $casi->value != $proccompa) {
	    	$prodsBorar = OportunidadProducto::where("oportunidad_id", $dato->id)->get();
	    	foreach ($prodsBorar as $prodborar) {
	    		$prodborar->delete();
	    	}
			foreach ($request->productos as $datoproduc) {
				if (isset($datoproduc["producto"]) && !empty($datoproduc["producto"])) {
					$productos = new OportunidadProducto;
					$productos->producto = $datoproduc["producto"];
					if (isset($datoproduc["cantidad"])) { $productos->cantidad = $datoproduc["cantidad"]; }
					if (isset($datoproduc["referencia"])) { $productos->referencia = $datoproduc["referencia"]; }
					if (isset($datoproduc["valor"])) { $productos->valor = $datoproduc["valor"]; }
					if (isset($datoproduc["total"])) { $productos->total = $datoproduc["total"]; }
					$productos->oportunidad_id = $dato->id;
	    			$productos->save();
				}
		    }

			$his = new HistorialOportunidades;
			$his->key = "productos";
			$his->value = $proccompa;
			$his->oportunidad_id = $dato->id;
			$his->user_id = Auth::user()->id;
    		$his->save();
	    }

		foreach ($buscar as $key => $value) {

			if ($key != "_token" && $key != "id" && $key != "productos" && $key != "empresa" && $key != "forma_pago" && $value != "" && !empty($value)) {
                $casi = $historia->where('key', $key)->pop();
                if (isset($casi->value) && $casi->value != $value) {
					$his = new HistorialOportunidades;
					$his->key = $key;
					$his->value = $value;
					$his->oportunidad_id = $dato->id;
					$his->user_id = Auth::user()->id;
		    		$his->save();
	    		}

	    		if (!isset($casi->value)) {
					$his = new HistorialOportunidades;
					$his->key = $key;
					$his->value = $value;
					$his->oportunidad_id = $dato->id;
					$his->user_id = Auth::user()->id;
		    		$his->save();
	    		}
			}
		}

		if (isset($request->forma_pago)) {
            $datos = OportunidadesFormapago::where("oportunidad_id", $dato->id)->get();
            foreach($datos as $dato1){
                $dato1->delete();
            }
			foreach ($request->forma_pago as $fpago) {
                //$his = OportunidadesFormapago::find($fpago["id_fpago_cuota"]);
				$his = new OportunidadesFormapago;
				if (isset($fpago["cuota_valor"])) {
					$his->key = "cuota_valor";
					$his->value = $fpago["cuota_valor"];
				}else{
					$his->key = "anticipo";
					$his->value = $fpago["anticipo"];
				}
				if (isset($fpago["cuota_texto"])) {
					$his->texto = $fpago["cuota_texto"];
				}
				$his->oportunidad_id = $dato->id;
				$his->user_id = Auth::user()->id;
    			$his->save();
		    }
	    }
	    return redirect("oportunidad/".$request->id);
	}

	public function listAjax(Request $request){
    	$historial 	= [];
    	$modulo 	= 3;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="listado oportunidades vigentes, vendidas, perdidas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver propio"){
              	$datos = DB::select("SELECT `oportunidad_id` as id FROM `historial_oportunidades` WHERE `key` = 'responsable' AND `value` = ".Auth::user()->id." AND `oportunidad_id` IS NOT NULL GROUP BY `oportunidad_id`");
	    		foreach ($datos as $value) {
                    $query = 'SELECT H.value AS value FROM historial_oportunidades H WHERE H.key LIKE "ciclo_venta" AND H.oportunidad_id = '.$value->id.' ORDER BY H.id DESC LIMIT 0,1';
                    $vendida_perdida = DB::select($query);
                    if($vendida_perdida[0]->value != '90' && $vendida_perdida[0]->value != '0'){
                        $oportunidad = Oportunidades::find($value->id);
                        $query = 'SELECT H.value FROM historial_oportunidades AS H WHERE H.key LIKE "responsable" AND H.oportunidad_id = '.$value->id.' ORDER BY H.id DESC LIMIT 0,1';
                        $responsable = DB::select($query);
                        if ($responsable[0]->value == Auth::user()->id) {
                            $historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
                            try {
                                $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                            } catch (Exception $e) { }
                        }
                    }
		        }
              }else{
              	$datos = Oportunidades::where('estado',"!=", "eliminada")->where('estado',"!=", "vendido")->get();
              	$datos = $datos->sortBy('created_at')->reverse();
	    		foreach ($datos as $value) {
		        	$historia = HistorialOportunidades::where('oportunidad_id', $value->id)->get();
		        	$historial[] = ['oportunidad'=>$value, "historial"=>$historia];
		        }
              }
            }
          }
        }

        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar de los listados y en los ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$permiso_editar='Si';
			  }else{
				$permiso_editar='No';
			  }
            }
          }
        }

        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón añadir oportunidad en listados" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
			  if($permiso2[0]->permiso == "Si"){
				$permiso_anadir='Si';
			  }else{
				$permiso_anadir='No';
			  }
            }
          }
        }
        /**Permiso Crear Oportunidad**/
        $data['permiso_crear'] = "No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="crear oportunidad" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_crear'] = "Si";
              }
            }
          }
        }
        /**Permiso botón ciclo venta en listados y ver**/
        $data['permiso_btn_ciclo'] = "No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón ciclo venta en listados y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_btn_ciclo'] = "Si";
              }
            }
          }
        }
        /**Permiso Exportar Excel y CSV**/
        $data['permiso_exportar'] = "No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Exportar Excel y CSV" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_exportar'] = "Si";
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('oportunidades.list',['data' => $data, 'datos' => $historial, 'permiso_editar'=>$permiso_editar, 'permiso_anadir' => $permiso_anadir]);
    }

  public function moneda(Request $request){
		$cantidad = $request->input('cantidad');
		$moneda_origen = $request->input('moneda_origen');
		$moneda_destino = $request->input('moneda_destino');
        $get = file_get_contents("https://www.google.com/finance/converter?a=$cantidad&from=$moneda_origen&to=$moneda_destino");
	    $get = explode("<span class=bld>",$get);
	    $get = explode("</span>",$get[1]);
	    return preg_replace("/[^0-9\.]/", null, $get[0]);
    }

    public function perder($id){
        $oportunidad=Oportunidades::findOrFail($id);
        $data['competencia'] = Competencia::all();
        $consulta = 'SUM(CONVERT(REPLACE(H.valor, ",", ""),UNSIGNED INTEGER))';
        $consulta1 = 'SUM(H.horas)';
        $query = 'SELECT IF(IsNull('.$consulta.'), 0, '.$consulta.') AS total, IF(IsNull('.$consulta1.'), 0, '.$consulta1.') AS tiempo FROM horas_hombres H WHERE H.oportunidad_id = '.$id;
        $data['Horas_hombre'] = DB::select($query);
        $query = 'SELECT SUM(CONVERT(REPLACE(A.alimentacion, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.transportes_internos, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.transportes_intermunicipales, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.tiquete_aereo, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.papeleria, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.invitacion_cliente, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.alquiler_vehiculo, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.gasolina_pasajes, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.hotel, ",", ""),UNSIGNED INTEGER)+CONVERT(REPLACE(A.otros, ",", ""),UNSIGNED INTEGER)) AS total  FROM actividades A WHERE A.tipo_actividad LIKE "Oportunidad" AND A.oportunidad_id = '.$id;
        $data['Gastos'] = DB::select($query);
        $data['Total'] = $data['Horas_hombre'][0]->total + $data['Gastos'][0]->total;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("oportunidadperdida",["data" => $data, "datos"=>$oportunidad]);
    }

    public function saveotro(Request $request){
		  $dato = new User;

			$dato->name=($request->otro['nombre']).' '.($request->otro['apellido']);
			$dato->nombres=$request->otro['nombre'];
			$dato->apellidos=$request->otro['apellido'];
			$dato->correo_personal=$request->otro['correo'];
			$dato->telefono_personal=$request->otro['telefono'];
			$dato->empresa=$request->otro['empresa'];
			$dato->pais_residencia=$request->otro['pais'];
			$dato->cargo=$request->otro['cargo'];
			$dato->tipo_user='otro';

		  $dato->save();
		  return \Response::json(['msj' => "Guardado correctamente!"]);
	}

    public function action(Request $request){
        $accion = $request->accion;
        switch($accion){
            case 0:
                /*Filtrar por fecha*/
                $id=$request->id;
                $fecha_inicio=$request->fecha_inicio;
                $fecha_fin=$request->fecha_fin;
                break;
        }
        /*Permiso para crear un acceder APU*/
        $permiso_apu_acceder='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$permiso_apu_acceder='Si';
			  }
            }
          }
        }
        /*Fin Permiso para crear un acceder APU*/
        /*Permiso para acceder Actas*/
        $permiso_acta_acceder='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$permiso_acta_acceder='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/
        $data = $this->flujo_caja_filtrado($id,$fecha_inicio,$fecha_fin);
        if(isset($request->flujoventas) && $request->flujoventas == "si"){
          /*Menu para oportunidades vendidas*/
          ob_start();
          if(!isset($request->perdida)){
              if(isset($request->id_menu_solo_vendida)){ $id = $request->id_menu_solo_vendida; $data['id_menu_solo_vendida'] = $request->id_menu_solo_vendida; }
          ?>
          <ul class="nav nav-pills" role="tablist">
              <li><a href="/venta/<?=$id?>"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
              <li><a href="/ventafileimportant/<?=$id?>"><i class="fa fa-bar-chart"></i> Archivos</a></li>
              <?php if($permiso_apu_acceder=="Si"){ ?><li><a href="/apu/<?=$id?>"><i class="fa fa-line-chart"></i> APU</a></li><?php } ?>
              <li><a href="/ventaciclos/<?=$id?>"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
              <?php if($permiso_acta_acceder=="Si"){ ?><li><a href="/actasreunion/<?=$id?>"><i class="fa fa-line-chart"></i> Actas reunion</a></li><?php } ?>
              <li class="active"><a href="/ver-oportunidadvendida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
              <li><a href="/venta/gestionesoportunidad/<?=$request->id?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nav nav-pills" role="tablist">
              <li><a href="/veroportunidadperdida/<?=$id?>"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
              <li><a href="/accionesoportunidadperdida/<?=$id?>"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
              <li class="active"><a href="/ver-oportunidadperdida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
              <li><a href="/verlecciones/<?=$id?>"><i class="fa fa-line-chart"></i> Lecciones aprendidas</a></li>
              <li><a href="/perdidas/gestionesoportunidad/<?=$id?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
          </ul>
          <?php
          }
          $data['menu'] = ob_get_contents();
          ob_end_clean();
          /*Fin Menu para oportunidades vendidas*/
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        return view("oportunidades/view-flujo",["data"=>$data]);
    }
    public function flujocaja($id){
        $fecha_inicio='';
        $fecha_fin='';
        $data = $this->flujo_caja_filtrado($id,$fecha_inicio,$fecha_fin);

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("oportunidades/view-flujo",["data"=>$data]);
    }

    public function flujocajavendida($id){
      $fecha_inicio='';
      $fecha_fin='';
      $venta = Venta::find($id);
      $data = $this->flujo_caja_filtrado($venta->oportunidad,$fecha_inicio,$fecha_fin);
      $data['id_menu_solo_vendida'] = $id;
      /*Permiso para crear un acceder APU*/
        $data['permiso_apu_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_acceder']='Si';
			  }
            }
          }
        }
      /*Fin Permiso para crear un acceder APU*/
        /*Permiso para acceder Actas*/
        $data['permiso_acta_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_acta_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/
      /*Menu para oportunidades vendidas*/
      ob_start();?>
      <ul class="nav nav-pills" role="tablist">
          <li><a href="/venta/<?=$id?>"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
          <li><a href="/ventafileimportant/<?=$id?>"><i class="fa fa-bar-chart"></i> Archivos</a></li>
          <?php if($data['permiso_apu_acceder']=="Si"){ ?><li><a href="/apu/<?=$id?>"><i class="fa fa-line-chart"></i> APU</a></li><?php } ?>
          <li><a href="/ventaciclos/<?=$id?>"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
          <?php if($data['permiso_acta_acceder']=="Si"){ ?><li><a href="/actasreunion/<?=$id?>"><i class="fa fa-line-chart"></i> Actas reunion</a></li><?php } ?>
          <li class="active"><a href="/ver-oportunidadvendida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
          <li><a href="/venta/gestionesoportunidad/<?=$venta->oportunidad?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
      </ul>
      <?php
      $data['menu'] = ob_get_contents();
      ob_end_clean();
      /*Fin Menu para oportunidades vendidas*/

      /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){
          $data['item_menu']='menu_'.$data['item_menu'][0]->id;
      }else{
          $data['item_menu']='';
      }
      /*Fin Consulta para activar el item del menu correspondiente*/
      return view("oportunidades/view-flujo",["data"=>$data]);
    }

    public function flujocajaperdida($id){
        $fecha_inicio='';
        $fecha_fin='';
        $data = $this->flujo_caja_filtrado($id,$fecha_inicio,$fecha_fin);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("oportunidades/perdidas/flujo-caja",["data"=>$data]);
    }

    public function informe(Request $request){
        if($request->fechaInicio == ''){
            $nuevafecha = strtotime ( '-6 month' , strtotime(date('Y-m-d')) ) ;
            $request->fechaInicio = date ( 'Y-m-d' , $nuevafecha );
        }
        if($request->fechaFin == ''){
            $request->fechaFin = date('Y-m-d');
        }
        $fechaInicio = $request->fechaInicio.' 00:00:00';
        $fechaFin = $request->fechaFin.' 23:59:59';
        $query = 'SELECT DATE_FORMAT(C.created_at,"%Y-%m") AS MES, COUNT(C.id) AS BITACORA FROM actividades C WHERE C.created_at >= "'.$fechaInicio.'" AND C.created_at <= "'.$fechaFin.'" GROUP BY DATE_FORMAT(C.created_at,"%Y-%m") ORDER BY MES ASC';
        $data['actividades']=DB::SELECT($query);
        $query = 'SELECT DATE_FORMAT(C.created_at,"%Y-%m") AS MES, COUNT(C.id) AS CLIENTES FROM clientes C WHERE C.created_at >= "'.$fechaInicio.'" AND C.created_at <= "'.$fechaFin.'" GROUP BY DATE_FORMAT(C.created_at,"%Y-%m") ORDER BY MES ASC';
        $data['clientes']=DB::SELECT($query);
        $query = 'SELECT DATE_FORMAT(C.created_at,"%Y-%m") AS MES, COUNT(C.id) AS OPORTUNIDADES FROM historial_oportunidades C WHERE C.created_at >= "'.$fechaInicio.'" AND C.created_at <= "'.$fechaFin.'" GROUP BY DATE_FORMAT(C.created_at,"%Y-%m") ORDER BY MES ASC';
        $data['oportunidades']=DB::SELECT($query);
        $query = 'SELECT DATE_FORMAT(C.created_at,"%Y-%m") AS MES, COUNT(C.id) AS PLANTRABAJO FROM plan_trabajos C WHERE C.created_at >= "'.$fechaInicio.'" AND C.created_at <= "'.$fechaFin.'" GROUP BY DATE_FORMAT(C.created_at,"%Y-%m") ORDER BY MES ASC';
        $data['plantrabajo']=DB::SELECT($query);

        $anos = intval(date('Y',strtotime($fechaFin))) - intval(date('Y',strtotime($fechaInicio)));
        if($anos == 0){
            $mesInicio = intval(date('m',strtotime($fechaInicio)));
            $mesFin = intval(date('m',strtotime($fechaFin)));
            $y = 0;
            for($i=$mesInicio;$i<=$mesFin;$i++){
                $nuevafecha = strtotime ( '+'.$y.' month' , strtotime ( $fechaInicio ) ) ;
                $nuevafecha = date ( 'Y-m' , $nuevafecha );
                $data['meses'][$y] = $nuevafecha;
                $y++;
            }
        }else{
            $anoInicio = intval(date('Y',strtotime($fechaInicio)));
            $anoFin = intval(date('Y',strtotime($fechaFin)));
            $y = 0;
            for($i=$anoInicio;$i<=$anoFin;$i++){
                if($i == $anoFin){
                    $mesInicio = intval(date('m',strtotime($fechaInicio)));
                    $mesFin = intval(date('m',strtotime($fechaFin)));
                    for($x=1;$x<=$mesFin;$x++){
                        $data['meses'][$y] = date('Y-m',strtotime ( $i.'-'.$x ));
                        $y++;
                    }
                }else{
                    $fechaCompara = date('Y-m', strtotime($i.'-01'));
                    if($fechaCompara<=date('Y-m',strtotime($fechaInicio))){
                        $mesInicio = intval(date('m',strtotime($fechaInicio)));
                        for($x=$mesInicio;$x<=12;$x++){
                            $data['meses'][$y] = date('Y-m',strtotime ( $i.'-'.$x ));
                            $y++;
                        }
                    }else{
                       for($x=1;$x<=12;$x++){
                            $data['meses'][$y] = date('Y-m',strtotime ( $i.'-'.$x ));
                            $y++;
                        }
                    }
                }
            }
        }
        return \Response::json(['data' => $data, 'success' => true]);
    }

    protected function flujo_caja_filtrado($id,$fecha_inicio, $fecha_fin){
        $data['id'] = $id;
        $oportunidad = oportunidades::find($id);
        /*Responsable*/
        $data['Responsable'] = DB::select('SELECT U.* FROM historial_oportunidades HO INNER JOIN users U ON U.id = HO.value WHERE HO.key LIKE "responsable" AND HO.oportunidad_id LIKE "'.$id.'" ORDER BY HO.created_at DESC LIMIT 0,1');
        /*Fin Responsable*/
        $data['empresa'] = Empresa::find($oportunidad->empresa_id);
        if($fecha_inicio=='' && $fecha_fin==''){
            $data['actividades'] = Actividades::where('oportunidad_id',$id)->orderBy('fecha_actividad')->get();
            $data['filtro_fecha_inicio'] = '';
            $data['filtro_fecha_fin'] = '';
            if(isset($data['actividades'][0]->fecha_actividad)){
                $data['filtro_fecha_inicio'] = $data['actividades'][0]->fecha_actividad;
                $data['filtro_fecha_fin'] = ($data['actividades']->last())->fecha_actividad;
            }else{
                $data['no_datos'] = 'no tiene datos';
            }
        }else{
            $data['actividades_anteriores'] = Actividades::where('oportunidad_id',$id)->where('fecha_actividad','<',$fecha_inicio)->orderBy('fecha_actividad')->get();
            $data['actividades'] = Actividades::where('oportunidad_id',$id)->where('fecha_actividad','>=',$fecha_inicio)->where('fecha_actividad','<=',$fecha_fin)->orderBy('fecha_actividad')->get();
            $data['filtro_fecha_inicio_seleccionado'] = $fecha_inicio;
            $data['filtro_fecha_fin_seleccionado'] = $fecha_fin;
            /*Para calendario fecha maxima y fecha minima*/
            $actividades_filtro = Actividades::where('oportunidad_id',$id)->orderBy('fecha_actividad')->get();
            $data['filtro_fecha_inicio'] = $actividades_filtro[0]->fecha_actividad;
            $data['filtro_fecha_fin'] = ($actividades_filtro->last())->fecha_actividad;
        }
        ob_start();?>
        <script>
        var chart = AmCharts.makeChart( "chartdiv", {
              "type": "serial",
              "theme": "light",
              "dataProvider": [
        <?php $data['valortotal']=0; foreach($data['actividades'] as $actividad){
               $sueldos=HorasHombre::where('actividad_id',$actividad->id)->get();
               $valorhorashombre=0;
               foreach($sueldos as $sueldo){
                   $valorhorashombre+=$this->formato($sueldo->valor);
               }
                $valorindividual=intval(($this->formato($actividad->alimentacion))+($this->formato($actividad->transportes_internos))+($this->formato($actividad->transportes_intermunicipales))+($this->formato($actividad->tiquete_aereo))+($this->formato($actividad->papeleria))+($this->formato($actividad->invitacion_cliente))+($this->formato($actividad->alquiler_vehiculo))+($this->formato($actividad->gasolina_pasajes))+($this->formato($actividad->hotel))+$this->formato($actividad->otros)+$valorhorashombre);
                $data['valortotal']+=$valorindividual;
            ?>
                {
                    "country": "<?=date("d/m/Y",strtotime($actividad->fecha_actividad))?>",
                    "visits": <?=$valorindividual?>
                  },
        <?php } ?>
             ],
              "valueAxes": [ {
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0,
                "title": "Valor"
              } ],
              "gridAboveGraphs": true,
              "startDuration": 1,
              "graphs": [ {
                "balloonText": "[[category]]: <br><b>$ [[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
              } ],
              "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
              },
              "categoryField": "country",
              "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20,
                "labelRotation": 90,
                "fontSize": 10
              },
              "export": {
                "enabled": false
              }

            } );

            AmCharts.makeChart("chartdiv1",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
                        "labelRotation": 90,
                        "fontSize": 10
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] de <br>[[category]]:<br><b>$ [[value]]</b>",
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "Flujo de caja acumulado",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Valor"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": ""
						}
					],
					"dataProvider": [
                        <?php
                        $valoracumulado=0;
                        if(isset($data['actividades_anteriores'])){
                           foreach($data['actividades_anteriores'] as $actividad_anterior){
                                $sueldos_anteriores=HorasHombre::where('actividad_id',$actividad_anterior->id)->get();
                                $valorhorashombre_anterior=0;
                                foreach($sueldos_anteriores as $sueldoanterior){
                                    $valorhorashombre_anterior+=$this->formato($sueldoanterior->valor);
                                }
                                $valoracumulado+=intval(($this->formato($actividad_anterior->alimentacion))+($this->formato($actividad_anterior->transportes_internos))+($this->formato($actividad_anterior->transportes_intermunicipales))+($this->formato($actividad_anterior->tiquete_aereo))+($this->formato($actividad_anterior->papeleria))+($this->formato($actividad_anterior->invitacion_cliente))+($this->formato($actividad_anterior->alquiler_vehiculo))+($this->formato($actividad_anterior->gasolina_pasajes))+($this->formato($actividad_anterior->hotel))+$this->formato($actividad_anterior->otros)+$valorhorashombre_anterior);
                            }
                        }

                        foreach($data['actividades'] as $actividad){
                               $sueldos=HorasHombre::where('actividad_id',$actividad->id)->get();
                               $valorhorashombre=0;
                               foreach($sueldos as $sueldo){
                                   $valorhorashombre+=$this->formato($sueldo->valor);
                               }
                                $valoracumulado+=intval(($this->formato($actividad->alimentacion))+($this->formato($actividad->transportes_internos))+($this->formato($actividad->transportes_intermunicipales))+($this->formato($actividad->tiquete_aereo))+($this->formato($actividad->papeleria))+($this->formato($actividad->invitacion_cliente))+($this->formato($actividad->alquiler_vehiculo))+($this->formato($actividad->gasolina_pasajes))+($this->formato($actividad->hotel))+$this->formato($actividad->otros)+$valorhorashombre);
                            ?>
                        {
							"category": "<?=date("d/m/Y",strtotime($actividad->fecha_actividad))?>",
							"column-1": <?=$valoracumulado?>
						},
                        <?php
                        }
                        $data['valortotal_anterior'] = $valoracumulado;
                        ?>
					]
				}
			);
            </script>
        <?php
            $data['javascript'] = ob_get_contents();
            ob_end_clean();

        $data['fecha_identificacion'] = DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id`='.$id.' AND `key`="fecha_ff" ORDER BY `updated_at` DESC');
        $data['fecha_cerrar'] = DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id`='.$id.' AND `key`="fecha_oc" ORDER BY `updated_at` DESC');

        /**Permiso Cambiar ciclo**/
        $modulo = 3;
        $data['permiso_btn_ciclo'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón ciclo venta en listados y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_btn_ciclo']='Si';
			  }
            }
          }
        }
        return $data;
    }
    protected function formato($valor){
        $valor = intval(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $valor))));
        return $valor;
    }
    protected function formt_fecha($fecha){
        $dias= array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $dia=$dias[intval(date('w', strtotime($fecha)))];
        $mes=$meses[intval(date('m', strtotime($fecha)))];
        return $dia.' '.date('d', strtotime($fecha)).' de '.$mes.' del '.date('Y', strtotime($fecha));
    }
    protected function listado($id){
        $data['oportunidad'] = Oportunidades::find($id);
        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" AND `id`!="72" AND deleted_at IS NULL');
        $actas = DB::select('SELECT *, IF(LENGTH(`nombre`)>17,CONCAT(SUBSTRING(`nombre`, 1, 14),"..."),`nombre`) AS nombre_abreviado FROM `oportunidad_actas` WHERE `id_oportunidad`= '.$id.' AND `deleted_at` IS NULL ORDER BY `id` ASC');
        $invitados = DB::select('SELECT * FROM `oportunidad_actas_invitados` WHERE `id_acta` IN (SELECT `id` FROM `actas` WHERE `id_oportunidad`="'.$id.'")');
        $data['invitados_externos'] = DB::select('SELECT DISTINCT(nombre) FROM oportunidad_actas_invitados WHERE tipo LIKE "usuario externo" AND nombre IS NOT NULL');
        $actas2 = OportunidadActa::where('id_oportunidad',$id)->get();
        ob_start();
        $data['numero_vigentes']=0;
        $query = 'IF(LENGTH(A.descripcion)>45,CONCAT(SUBSTRING(A.descripcion, 1, 41),"..."),A.descripcion) AS descripcion_abreviado, (SELECT INV.`nombre` FROM `invitados` AS INV WHERE INV.`id`=A.`id_responsable`) AS responsable_externo, (SELECT U.`nombres` FROM `users` AS U WHERE U.`id`=A.`id_responsable`) AS responsable_nombre, (SELECT UA.`apellidos` FROM `users` AS UA WHERE UA.`id`=A.`id_responsable`) AS responsable_apellido';
        foreach($actas2 as $acta){
            $vigentes = DB::select('SELECT A.*, IFNULL(A.estado, "vigente") AS ESTADO , '.$query.' FROM oportunidad_actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC LIMIT 0,1');
            if(isset($vigentes[0]->ESTADO) && $vigentes[0]->ESTADO == "vigente"){
                $data['numero_vigentes']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_vigentes']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($vigentes[0]->id_acta)>9){ if(($vigentes[0]->id_acta)>99){ if(($vigentes[0]->id_acta)>999){ $numero=$vigentes[0]->id_acta; }else{ $numero='0'.$vigentes[0]->id_acta; } }else{  $numero='00'.$vigentes[0]->id_acta; } }else{ $numero='000'.$vigentes[0]->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigentes[0]->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($vigentes[0]->fecha_ejecucion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($vigentes[0]->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$vigentes[0]->responsable_nombre?> <?=$vigentes[0]->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$vigentes[0]->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$vigentes[0]->descripcion?>" style="cursor:pointer;"><?=$vigentes[0]->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($vigentes[0]->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$vigentes[0]->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-2 encabezado">
        <a class="realizado" data-toggle="tooltip" data-placement="top" title="Realizado" onclick="marcar(<?=$vigentes[0]->id?>,'realizado')"><i class="fa fa-check-circle-o fa-2"></i></a>
        <a class="aplazar" data-toggle="tooltip" data-placement="top" title="Aplazar" onclick="marcar(<?=$vigentes[0]->id?>,'aplazar')"><i class="fa fa-repeat fa-2"></i></a>
        <a class="cancelar" data-toggle="tooltip" data-placement="top" title="Cancelar" onclick="marcar(<?=$vigentes[0]->id?>,'cancelar')"><i class="fa fa-times-circle-o fa-2"></i></a>
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$acta->id?>,'<?=$numero?>')"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
            }
        }
        $data['vigentes'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_realizado']=0;
        foreach($actas as $acta){
            $realizados = DB::select('SELECT *, '.$query.' FROM oportunidad_actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC LIMIT 0,1');
            if(isset($realizados[0]->estado) && $realizados[0]->estado == 'Realizado'){
                $data['numero_realizado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_realizado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($realizados[0]->id_acta)>9){ if(($realizados[0]->id_acta)>99){ if(($realizados[0]->id_acta)>999){ $numero=$realizados[0]->id_acta; }else{ $numero='0'.$realizados[0]->id_acta; } }else{  $numero='00'.$realizados[0]->id_acta; } }else{ $numero='000'.$realizados[0]->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizados[0]->fecha_creacion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizados[0]->fecha_ejecucion))?></h6>
            </div>
            <div class="col-md-4 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($realizados[0]->fecha_accion))?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($realizados[0]->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$realizados[0]->responsable_nombre?> <?=$realizados[0]->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$realizados[0]->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-3 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$realizados[0]->descripcion?>" style="cursor:pointer;"><?=$realizados[0]->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($realizados[0]->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$realizados[0]->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$acta->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
            }
        }
        $data['realizados'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        $data['numero_cancelado']=0;
        foreach($actas2 as $acta){
            $cancelados = DB::select('SELECT A.*, '.$query.' FROM oportunidad_actas_acciones A WHERE A.id_acta = '.$acta->id.' ORDER BY A.id DESC');
            foreach($cancelados as $cancelado){
                if(isset($cancelado->estado) && $cancelado->estado == 'Cancelado'){
                    $data['numero_cancelado']++;
             ?>
<div class="row contenido">
    <div class="col-md-1 encabezado">
        <div class="row">
            <div class="col-md-4 encabezado-int">
                <h6><?=$data['numero_cancelado']?></h6>
            </div>
            <div class="col-md-8">
                <?php if(($cancelado->id_acta)>9){ if(($cancelado->id_acta)>99){ if(($cancelado->id_acta)>999){ $numero=$cancelado->id_acta; }else{ $numero='0'.$cancelado->id_acta; } }else{  $numero='00'.$cancelado->id_acta; } }else{ $numero='000'.$cancelado->id_acta; } ?>
                <h6>AC-<?=$numero?></h6>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6 encabezado">
                <h6><?=date('d/m/Y (h:i:s A)',strtotime($cancelado->fecha_creacion))?></h6>
            </div>
            <div class="col-md-6 encabezado">
                <h6></h6>
            </div>
        </div>
    </div>
    <div class="col-md-2 encabezado">
        <?php if($cancelado->tipo_responsable == "usuario sistema"){ ?>
        <h6><?=$cancelado->responsable_nombre?> <?=$cancelado->responsable_apellido?></h6>
        <?php }else{ ?>
        <h6><?=$cancelado->responsable_externo?></h6>
        <?php } ?>
    </div>
    <div class="col-md-4 encabezado">
        <h6 data-toggle="tooltip" data-placement="top" title="<?=$cancelado->descripcion?>" style="cursor:pointer;"><?=$cancelado->descripcion_abreviado?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <h6><?php if($cancelado->aplazado == ""){ ?> 0 <?php }else{ ?> <?=$cancelado->aplazado?> <?php } ?></h6>
    </div>
    <div class="col-md-1 encabezado">
        <a class="comentario" title="Comentarios" onclick="comentarios(<?=$cancelado->id?>,<?=$numero?>)"><i class="fa fa-comments fa-2" aria-hidden="true"></i></a>
    </div>
</div>

<?php
                }
            }
        }
        $data['cancelados'] = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        /*Permiso Boton de ciclo venta*/
        $modulo = 3;
        $data['permiso_btn_ciclo'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón ciclo venta en listados y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_btn_ciclo']='Si';
			  }
            }
          }
        }
        $datos['data'] = $data;
        $datos['id'] = $id;
        $datos['usuarios'] = $usuarios;
        $datos['actas'] = $actas;
        $datos['invitados'] = $invitados;

        return $datos;
    }
    protected function fecha2($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $f=explode('-',$fecha);
            $fecha_formato = $f[2].'/'.$meses[intval($f[1])].'/'.$f[0];
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }
    }
}

function burbuja($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["id"]<$array[$j+1]["id"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }

    return $array;
}

function burbuja2($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["value"]<$array[$j+1]["value"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }

    return $array;
}

function burbuja3($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["id"]<$array[$j+1]["id"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }

    return $array;
}

function ObtenerIP() {
   $ip = "";
   if(isset($_SERVER))
   {
       if (!empty($_SERVER['HTTP_CLIENT_IP']))
       {
           $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
   }
   else
   {
        if ( getenv( 'HTTP_CLIENT_IP' ) )
        {
            $ip = getenv( 'HTTP_CLIENT_IP' );
        }
        elseif( getenv( 'HTTP_X_FORWARDED_FOR' ) )
        {
            $ip = getenv( 'HTTP_X_FORWARDED_FOR' );
        }
        else
        {
            $ip = getenv( 'REMOTE_ADDR' );
        }
   }
    // En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma
   if(strstr($ip,','))
   {
        $ip = array_shift(explode(',',$ip));
   }
   return $ip;
}

function fecha_hora($f){
        $meses = array("","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $fecha_hora = explode(' ',$f);
        $fecha = explode('-',$fecha_hora[0]);
        $hora = explode(':',$fecha_hora[1]);
        if(intval($hora[0])>12){
           $hora[0] = intval($hora[0])-12;
           $jornada = "P.M";
       }else{
           $jornada = "A.M";
       }
        $fecha_final = $fecha[2].' de '.$meses[intval($fecha[1])].' '.$fecha[0].' '.$hora[0].':'.$hora[1].':'.$hora[2].' '.$jornada;
        return $fecha_final;
    }
function fecha($f){
        $meses = array("","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $fecha = explode('-',$f);

        $fecha_final = $fecha[2].' de '.$meses[intval($fecha[1])].' '.$fecha[0];
        return $fecha_final;
    }
