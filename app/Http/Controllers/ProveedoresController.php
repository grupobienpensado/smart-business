<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Paises;
use App\Estados;
use App\Ciudades;
use App\Proveedore;
use App\FeriasPatrocinadore;
use App\ProveedoresResponsable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
date_default_timezone_set("America/Bogota");

class ProveedoresController extends Controller
{
    public function index(){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $proveedores= Proveedore::all();
        $i=0;
        $patrocinadores=FeriasPatrocinadore::all();
        $boton['proveedores']=count($proveedores);
        $boton['patrocinadores']=count($patrocinadores);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('proveedores.proveedores',['data' => $data, 'proveedores' => $proveedores, 'i' => $i, 'boton'=>$boton]);

    }

    public function ListadoAutocomplete(Request $request){
        if(isset($request->term)){
            $proveedores = Proveedore::where('nombre','LIKE',"%{$request->term}%")->get();
            return \Response::json($proveedores);
        }
    }

    public function crearproveedores(){
        $paises = DB::select('SELECT * FROM `paises` ORDER BY `name` ASC');
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('proveedores.crearproveedor',['data' => $data, 'paises' => $paises]);
    }

    public function consultarpais($id){
        $pais = Paises::findorfail($id);
        $departamentos = DB::select('SELECT * FROM `estados` WHERE `country_id` = "'.$id.'" ORDER BY `name` ASC');
        $html='';
        foreach($departamentos as $departamento){
            $html.='<option value="'.$departamento->id.'" label="'.$departamento->name.'">';
        }
        return \Response::json(['pais' => $pais, 'html' => $html]);
    }

    public function consultardepartamento($id){
        $departamento = Estados::findorfail($id);
        $ciudades = DB::select('SELECT * FROM `ciudades` WHERE `state_id` = "'.$id.'" ORDER BY `name` ASC');
        $html='';
        foreach($ciudades as $ciudad){
            $html.='<option value="'.$ciudad->id.'" label="'.$ciudad->name.'">';
        }
        return \Response::json(['departamento' => $departamento, 'html' => $html]);
    }

    public function consultarciudad($id){
        $ciudad = Ciudades::findorfail($id);
        return \Response::json(['ciudad' => $ciudad]);
    }
    public function guardarproveedor(Request $request){
        $dato = new Proveedore;
        $dato->nombre=$request->nombre;
        $dato->tags_producto=$request->tags_producto;
        $dato->ubicacion=$request->pais.'%%'.$request->departamento.'%%'.$request->ciudad;
        $dato->comentario=$request->comentario;
        $dato->descripcion=$request->descripcion;

        if (isset($request->logo_values)) {
            if(!empty($request->logo_values)){
                $imagen = json_decode($request->logo_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('feriasproveedores')->put($nombre,  $foto);
                $dato->logo = $nombre;
            }
        }
        if (isset($request->fotoresponsable_values)) {
            if(!empty($request->fotoresponsable_values)){
                $imagen = json_decode($request->fotoresponsable_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre."responsable.jpg";
                \Storage::disk('feriasproveedores')->put($nombre,  $foto);
                $dato->foto_responsable = $nombre;
            }
        }
        if($dato->save()){
            $prov = $dato->id;
        }
        if(isset($request->responsable)){
            for($i = 0; $i < count($request->responsable['nombre']); $i++){
                $responsable = new ProveedoresResponsable;
                $responsable->id_proveedor = $prov;
                $responsable->nombre = $request->responsable['nombre'][$i];
                $responsable->cargo = $request->responsable['cargo'][$i];
                $responsable->correo = $request->responsable['telefono'][$i];
                $responsable->telefono = $request->responsable['celular'][$i];
                $responsable->celular = $request->responsable['correo'][$i];
                $responsable->save();
            }
        }
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function editarproveedores($id){
        $dato=Proveedore::findorfail($id);
        $paises = DB::select('SELECT * FROM `paises` ORDER BY `name` ASC');
        $datos_ubicacion = $dato->ubicacion;
        $u=explode('%%',$datos_ubicacion);
        $pais=DB::select('SELECT * FROM `paises` WHERE `id`="'.$u[0].'"');
        $departamento=DB::select('SELECT * FROM `estados` WHERE `id`="'.$u[1].'"');
        $departamentos=DB::select('SELECT * FROM `estados` WHERE `country_id`="'.$u[0].'"');
        $ciudad=DB::select('SELECT * FROM `ciudades` WHERE `id`="'.$u[2].'"');
        $ciudades=DB::select('SELECT * FROM `ciudades` WHERE `state_id`="'.$u[1].'"');
        $ubicacion[0]['pais']=$pais[0]->id;
        $ubicacion[1]['pais']=$pais[0]->name;
        $ubicacion[0]['departamento']=$departamento[0]->id;
        $ubicacion[1]['departamento']=$departamento[0]->name;
        $ubicacion[0]['ciudad']=$ciudad[0]->id;
        $ubicacion[1]['ciudad']=$ciudad[0]->name;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('proveedores.editarproveedor',['data' => $data, 'dato' => $dato, 'paises' => $paises, 'departamentos' => $departamentos, 'ciudades' => $ciudades, 'ubicacion' => $ubicacion]);
    }

    public function editarproveedor(Request $request){
        $dato = Proveedore::findorfail($request->id);
        $dato->nombre=$request->nombre;
        $dato->tags_producto=$request->tags_producto;
        $dato->ubicacion=$request->pais.'%%'.$request->departamento.'%%'.$request->ciudad;
        $dato->comentario=$request->comentario;
        $dato->descripcion=$request->descripcion;
        $dato->responsable=$request->responsable;
        $dato->datos_contacto_direccion=$request->datos_contacto_direccion;
        $dato->datos_contacto_telefono=$request->datos_contacto_telefono;
        $dato->datos_contacto_celular=$request->datos_contacto_celular;
        $dato->datos_contacto_correo=$request->datos_contacto_correo;

        if (isset($request->logo_values)) {
            if(!empty($request->logo_values)){
                $viejo = $dato->logo;
                if($viejo!=''){
                    Storage::delete('ferias/proveedores/'.$viejo);
                }
                $imagen = json_decode($request->logo_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('feriasproveedores')->put($nombre,  $foto);
                $dato->logo = $nombre;
            }
        }
        if (isset($request->fotoresponsable_values)) {
            if(!empty($request->fotoresponsable_values)){
                $viejo1 = $dato->foto_responsable;
                if($viejo1!=''){
                    Storage::delete('ferias/proveedores/'.$viejo1);
                }
                $imagen = json_decode($request->fotoresponsable_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre."responsable.jpg";
                \Storage::disk('feriasproveedores')->put($nombre,  $foto);
                $dato->foto_responsable = $nombre;
            }
        }
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function eliminarproveedor($id){
        $dato = Ferias_proveedore::findOrFail($id);
        if($dato->logo!=''){
            Storage::delete('ferias/proveedores/'.$dato->logo);
        }
        if($dato->foto_responsable!=''){
            Storage::delete('ferias/proveedores/'.$dato->foto_responsable);
        }
        Ferias_proveedore::destroy($id);
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }

    public function verproveedor($id){
        $dato = Ferias_proveedore::findOrFail($id);
        $u=explode('%%',$dato->ubicacion);
        $pais=DB::select('SELECT name FROM `paises` WHERE `id`="'.$u[0].'"');
        $departamento=DB::select('SELECT name FROM `estados` WHERE `id`="'.$u[1].'"');
        $ciudad=DB::select('SELECT name FROM `ciudades` WHERE `id`="'.$u[2].'"');
        $ubicacion=$pais[0]->name.", ".$departamento[0]->name.", ".$ciudad[0]->name;
        return \Response::json(['dato' => $dato, 'ubicacion' => $ubicacion]);
    }
}
