<?php

namespace App\Http\Controllers;

date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;

use App\User;
use App\Oportunidades;
use App\Metas_oportunidade;
use App\Participacion_probabilidade;
use App\PermisoCargo;
use App\ViewOportunidades;
use App\Actividades;
use App\Comparaciones;
use App\ComparacionInformaciones;
use Illuminate\Support\Facades\DB;
use Auth;

class DashboardgeneralnewController extends Controller
{
    /*Maquetado board en producción.*/
    public function BoardProduccion(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Dashboard" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*boton de guardar*/
        $modulo = 2;
        $data['permiso_guardar_cierre_ano']="No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Guardar cierre ano" AND `id_permisomodulo`="'.$modulo.'"');
		if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_guardar_cierre_ano']="Si";
              }
            }
          }
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        $data['cargos'] = PermisoCargo::where('id','!=','10')->get();
        return view("dashboard.board_general_producion",['data'=>$data]);
    }
    public function BoardProduccionFiltro(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Dashboard" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*boton de guardar*/
        $modulo = 2;
        $data['permiso_guardar_cierre_ano']="No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Guardar cierre ano" AND `id_permisomodulo`="'.$modulo.'"');
		if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_guardar_cierre_ano']="Si";
              }
            }
          }
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        $data['cargos'] = PermisoCargo::where('id','!=','10')->get();
        return view("dashboard.board_general_producion_filtro",['data'=>$data]);
    }
    /*acciones para realizar consulta po AJAX*/
    public function action(Request $request){
    	$accion = $request->action;
    	switch($accion){
    		/*Consulta para el cono de oportunidades*/
    		case 0:
    			if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $oportunidades = Oportunidades::all();
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $query = 'SELECT O.id FROM oportunidades O WHERE (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_ff" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) >= "'.$request->fecha_inicio.'" AND (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_ff" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) <= "'.$request->fecha_fin.'"';
                    $oportunidades=DB::select($query);
                }else if($request->fecha_inicio != ''){
                     $query = 'SELECT O.id FROM oportunidades O WHERE (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_ff" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) >= "'.$request->fecha_inicio.'"';
                     $oportunidades=DB::select($query);
                }else if($request->fecha_fin != ''){
                    $query = 'SELECT O.id FROM oportunidades O WHERE (SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_ff" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) <= "'.$request->fecha_fin.'"';
                    $oportunidades=DB::select($query);
                }
                if(count($oportunidades)>0){
                    foreach($oportunidades as $oportunidad){
                        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "ciclo_venta" AND H.oportunidad_id = '.$oportunidad->id.' ORDER BY H.id DESC LIMIT 0,1';
                        $ciclo = DB::select($query);
                        /*Todas las oportunidades que esten entre 10 y 80 porciento*/
                        if($ciclo[0]->value != "0" && $ciclo[0]->value != "90"){
                            $query = 'SELECT DATEDIFF(H.value, CURDATE()) AS Cantidad_dias FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->id.' AND H.key LIKE "fecha_oc" ORDER BY H.id DESC LIMIT 0,1';
                            $cantidad_dias=DB::select($query);
                            $query = 'SELECT REPLACE(H.value, ",", "") AS valor FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->id.' AND H.key LIKE "val_pesos" ORDER BY H.id DESC LIMIT 0,1';
                            $valor=DB::select($query);
                            $query = 'SELECT H.value AS Probabilidad FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->id.' AND H.key LIKE "Probabilidad" ORDER BY H.id DESC LIMIT 0,1';
                            $probabilidad=DB::select($query);
                            /*Clasificar en plazos*/
                            $meta = Metas_oportunidade::find(1);
                            if($cantidad_dias[0]->Cantidad_dias == 0){
                                $cantidad_dias[0]->Cantidad_dias = 1;
                            }
                            switch($cantidad_dias[0]->Cantidad_dias){
                                case ($cantidad_dias[0]->Cantidad_dias >= 0 && $cantidad_dias[0]->Cantidad_dias <= $meta->corto_dias):
                                    if(isset($data['cp'])){
                                        $data['cp']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                    }else{
                                        $data['cp'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                    }

                                    $data['diferencia_cp'] = $meta->corto_valor - $data['cp'];

                                    switch($probabilidad[0]->Probabilidad){
                                        case 'Alta':
                                            if(isset($data['corto_plazo']['Alta'])){
                                                $data['corto_plazo']['Alta']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['corto_plazo']['Alta'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                        case 'Media':
                                            if(isset($data['corto_plazo']['Media'])){
                                                $data['corto_plazo']['Media']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['corto_plazo']['Media'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                        case 'Baja':
                                            if(isset($data['corto_plazo']['Baja'])){
                                                $data['corto_plazo']['Baja']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['corto_plazo']['Baja'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                    }
                                    break;
                                case ($cantidad_dias[0]->Cantidad_dias > $meta->corto_dias && $cantidad_dias[0]->Cantidad_dias <= $meta->medio_dias):
                                    if(isset($data['mp'])){
                                        $data['mp']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                    }else{
                                        $data['mp'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                    }

                                    $data['diferencia_mp'] = $meta->medio_valor - $data['mp'];

                                    switch($probabilidad[0]->Probabilidad){
                                        case 'Alta':
                                            if(isset($data['medio_plazo']['Alta'])){
                                                $data['medio_plazo']['Alta']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['medio_plazo']['Alta'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                        case 'Media':
                                            if(isset($data['medio_plazo']['Media'])){
                                                $data['medio_plazo']['Media']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['medio_plazo']['Media'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                        case 'Baja':
                                            if(isset($data['medio_plazo']['Baja'])){
                                                $data['medio_plazo']['Baja']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['medio_plazo']['Baja'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                    }
                                    break;
                                case ($cantidad_dias[0]->Cantidad_dias > $meta->medio_dias):
                                    if(isset($data['lp'])){
                                        $data['lp']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                    }else{
                                        $data['lp'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                    }

                                    $data['diferencia_lp'] = $meta->largo_valor - $data['lp'];

                                    switch($probabilidad[0]->Probabilidad){
                                        case 'Alta':
                                            if(isset($data['largo_plazo']['Alta'])){
                                                $data['largo_plazo']['Alta']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['largo_plazo']['Alta'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                        case 'Media':
                                            if(isset($data['largo_plazo']['Media'])){
                                                $data['largo_plazo']['Media']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['largo_plazo']['Media'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                        case 'Baja':
                                            if(isset($data['largo_plazo']['Baja'])){
                                                $data['largo_plazo']['Baja']+= isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }else{
                                                $data['largo_plazo']['Baja'] = isset($valor[0]->valor)?$valor[0]->valor:0;
                                            }
                                            break;
                                    }
                                    break;
                            }
                        }
                    }
                }
                $data['diferencia_lp'] = isset($data['diferencia_lp']) && $data['diferencia_lp']>0? $this->numero($data['diferencia_lp']):0;
                $data['largo_plazo']['Baja'] = isset($data['largo_plazo']['Baja'])? $this->numero($data['largo_plazo']['Baja']):0;
                $data['largo_plazo']['Media'] = isset($data['largo_plazo']['Media'])? $this->numero($data['largo_plazo']['Media']):0;
                $data['largo_plazo']['Alta'] = isset($data['largo_plazo']['Alta'])? $this->numero($data['largo_plazo']['Alta']):0;
                $data['diferencia_mp'] = isset($data['diferencia_mp']) && $data['diferencia_mp']>0? $this->numero($data['diferencia_mp']):0;
                $data['medio_plazo']['Baja'] = isset($data['medio_plazo']['Baja'])? $this->numero($data['medio_plazo']['Baja']):0;
                $data['medio_plazo']['Media'] = isset($data['medio_plazo']['Media'])? $this->numero($data['medio_plazo']['Media']):0;
                $data['medio_plazo']['Alta'] = isset($data['medio_plazo']['Alta'])? $this->numero($data['medio_plazo']['Alta']):0;
                $data['diferencia_cp'] = isset($data['diferencia_cp']) && $data['diferencia_cp']>0? $this->numero($data['diferencia_cp']):0;
                $data['corto_plazo']['Baja'] = isset($data['corto_plazo']['Baja'])? $this->numero($data['corto_plazo']['Baja']):0;
                $data['corto_plazo']['Media'] = isset($data['corto_plazo']['Media'])? $this->numero($data['corto_plazo']['Media']):0;
                $data['corto_plazo']['Alta'] = isset($data['corto_plazo']['Alta'])? $this->numero($data['corto_plazo']['Alta']):0;
    			break;
            /*Consulta para el cono de oportunidades (modal)*/
            case 1:
                $meta = Metas_oportunidade::find(1);
                $probabilidad = Participacion_probabilidade::find(1);
                $limite['yellowFrom'] = 25;
                $limite['yellowTo'] = 50;
                $limite['redFrom'] = 0;
                $limite['redTo'] = 24;
                /*CORTO ALTA*/
                $data0['Corto']['Alta'] = $request->corto_plazo_Alta;
                $maximo = intval(((intval($meta->corto_valor)/1000000)*$probabilidad->alta)/100);
                if($maximo>$data0['Corto']['Alta']){
                    $data0['Corto']['Alta_porcentaje'] = $maximo>0?intval($data0['Corto']['Alta']/$maximo * 100):0;
                }else{
                    $data0['Corto']['Alta_porcentaje'] = 100;
                }
                /*CORTO ALTA*/
                /*CORTO MEDIA*/
                $data0['Corto']['Media'] = $request->corto_plazo_Media;
                $maximo = intval(((intval($meta->corto_valor)/1000000)*$probabilidad->media)/100);
                if($maximo>$data0['Corto']['Media']){
                    $data0['Corto']['Media_porcentaje'] = $maximo>0?intval($data0['Corto']['Media']/$maximo * 100):0;
                }else{
                    $data0['Corto']['Media_porcentaje'] = 100;
                }
                /*CORTO MEDIA*/
                /*CORTO BAJA*/
                $data0['Corto']['Baja'] = $request->corto_plazo_Baja;
                $maximo = intval(((intval($meta->corto_valor)/1000000)*$probabilidad->baja)/100);
                if($maximo>$data0['Corto']['Baja']){
                    $data0['Corto']['Baja_porcentaje'] = $maximo>0?intval($data0['Corto']['Baja']/$maximo * 100):0;
                }else{
                    $data0['Corto']['Baja_porcentaje'] = 100;
                }
                /*CORTO BAJA*/
                $data['Corto'] = $data0['Corto']['Alta'] + $data0['Corto']['Media'] + $data0['Corto']['Baja'];
                $data['Corto'] = number_format($data['Corto'], 0, '.', ',');
                /*MEDIANO ALTA*/
                $data0['Mediano']['Alta'] = $request->medio_plazo_Alta;
                $maximo = intval(((intval($meta->medio_valor)/1000000)*$probabilidad->alta)/100);
                if($maximo>$data0['Mediano']['Alta']){
                    $data0['Mediano']['Alta_porcentaje'] = $maximo>0?intval($data0['Mediano']['Alta']/$maximo * 100):0;
                }else{
                    $data0['Mediano']['Alta_porcentaje'] = 100;
                }
                /*MEDIANO ALTA*/
                /*MEDIANO MEDIA*/
                $data0['Mediano']['Media'] = $request->medio_plazo_Media;
                $maximo = intval(((intval($meta->medio_valor)/1000000)*$probabilidad->media)/100);
                if($maximo>$data0['Mediano']['Media']){
                    $data0['Mediano']['Media_porcentaje'] = $maximo>0?intval($data0['Mediano']['Media']/$maximo * 100):0;
                }else{
                    $data0['Mediano']['Media_porcentaje'] = 100;
                }
                /*MEDIANO MEDIA*/
                /*MEDIANO BAJA*/
                $data0['Mediano']['Baja'] = $request->medio_plazo_Baja;
                $maximo = intval(((intval($meta->medio_valor)/1000000)*$probabilidad->baja)/100);
                if($maximo>$data0['Mediano']['Baja']){
                    $data0['Mediano']['Baja_porcentaje'] = $maximo>0?intval($data0['Mediano']['Baja']/$maximo * 100):0;
                }else{
                    $data0['Mediano']['Baja_porcentaje'] = 100;
                }
                /*MEDIANO BAJA*/
                $data['Mediano'] = $data0['Mediano']['Alta'] + $data0['Mediano']['Media'] + $data0['Mediano']['Baja'];
                $data['Mediano'] = number_format($data['Mediano'], 0, '.', ',');
                /*LARGO ALTA*/
                $data0['Largo']['Alta'] = $request->largo_plazo_Alta;
                $maximo = intval(((intval($meta->largo_valor)/1000000)*$probabilidad->alta)/100);
                if($maximo>$data0['Largo']['Alta']){
                    $data0['Largo']['Alta_porcentaje'] = $maximo>0?intval($data0['Largo']['Alta']/$maximo * 100):0;
                }else{
                    $data0['Largo']['Alta_porcentaje'] = 100;
                }
                /*LARGO ALTA*/
                /*LARGO MEDIA*/
                $data0['Largo']['Media'] = $request->largo_plazo_Media;
                $maximo = intval(((intval($meta->largo_valor)/1000000)*$probabilidad->media)/100);
                if($maximo>$data0['Largo']['Media']){
                    $data0['Largo']['Media_porcentaje'] = $maximo>0?intval($data0['Largo']['Media']/$maximo * 100):0;
                }else{
                    $data0['Largo']['Media_porcentaje'] = 100;
                }
                /*LARGO MEDIA*/
                /*LARGO BAJA*/
                $data0['Largo']['Baja'] = $request->largo_plazo_Baja;
                $maximo = intval(((intval($meta->largo_valor)/1000000)*$probabilidad->baja)/100);
                if($maximo>$data0['Largo']['Baja']){
                    $data0['Largo']['Baja_porcentaje'] = $maximo>0?intval($data0['Largo']['Baja']/$maximo * 100):0;
                }else{
                    $data0['Largo']['Baja_porcentaje'] = 100;
                }
                /*LARGO BAJA*/
                $data['Largo'] = $data0['Largo']['Alta'] + $data0['Largo']['Media'] +$data0['Largo']['Baja'];
                $data['Largo'] = number_format($data['Largo'], 0, '.', ',');

                ob_start(); ?>
                <script>
function drawChart_cono0() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Corto Alta %', <?=$data0['Corto']['Alta_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-1'));
        chart.draw(data, options);
    }
function drawChart_cono1() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Corto Media %', <?=$data0['Corto']['Media_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-2'));
        chart.draw(data, options);
    }
function drawChart_cono2() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Corto Baja %', <?=$data0['Corto']['Baja_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-3'));
        chart.draw(data, options);
    }
function drawChart_cono3() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Mediano Alta %', <?=$data0['Mediano']['Alta_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-4'));
        chart.draw(data, options);
    }
function drawChart_cono4() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Mediano Media %', <?=$data0['Mediano']['Media_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-5'));
        chart.draw(data, options);
    }
function drawChart_cono5() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Mediano Baja %', <?=$data0['Mediano']['Baja_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-6'));
        chart.draw(data, options);
    }
function drawChart_cono6() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Largo Alta %', <?=$data0['Largo']['Alta_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-7'));
        chart.draw(data, options);
    }
function drawChart_cono7() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Largo Media %', <?=$data0['Largo']['Media_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-8'));
        chart.draw(data, options);
    }
function drawChart_cono8() {
    var data = google.visualization.arrayToDataTable([
      ['Label', 'Value'],
      ['Largo Baja %', <?=$data0['Largo']['Baja_porcentaje']?>]
    ]);
        var options = {
            width: 250,
            height: 250,
            redFrom: <?=$limite['redFrom']?>,
            redTo: <?=$limite['redTo']?>,
            yellowFrom: <?=$limite['yellowFrom']?>,
            yellowTo: <?=$limite['yellowTo']?>,
            minorTicks: 5,
            max:100
        };
        var chart = new google.visualization.Gauge(document.getElementById('meter-9'));
        chart.draw(data, options);
    }
                    </script>
                <?php
                $data['html']=ob_get_contents();
                ob_end_clean();
                break;
            /*Consulta para negocios cerrados*/
            case 2:
                $data['total_negocios_cerrados'] = 0;
                $data['valor_negocios_cerrados'] = 0;
                $query = 'SELECT DISTINCT(H.oportunidad_id) AS oportunidad_id FROM historial_oportunidades H WHERE H.key LIKE "ciclo_venta" AND H.value LIKE "90"';
                $oportunidades_vendidas = DB::select($query);
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    foreach ($oportunidades_vendidas as $oportunidad) {
                        $query = 'SELECT YEAR(H.value) AS value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                        $fecha_cierre = DB::select($query);
                        if($fecha_cierre[0]->value == date('Y')){
                            $data['total_negocios_cerrados']++;
                            $query = 'SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) AS value FROM historial_oportunidades H WHERE H.key LIKE "val_pesos" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                            $valor = DB::select($query);
                            $data['valor_negocios_cerrados']+=$valor[0]->value;
                        }
                    }
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    foreach ($oportunidades_vendidas as $oportunidad) {
                        $query = 'SELECT H.value AS value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                        $fecha_cierre = DB::select($query);
                        if($fecha_cierre[0]->value >= $request->fecha_inicio && $fecha_cierre[0]->value <= $request->fecha_fin){
                            $data['total_negocios_cerrados']++;
                            $query = 'SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) AS value FROM historial_oportunidades H WHERE H.key LIKE "val_pesos" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                            $valor = DB::select($query);
                            $data['valor_negocios_cerrados']+=$valor[0]->value;
                        }
                    }
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $data['query']='';
                    foreach ($oportunidades_vendidas as $oportunidad) {
                        $query = 'SELECT H.value AS value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';

                        $fecha_cierre = DB::select($query);
                        if($fecha_cierre[0]->value >= $request->fecha_inicio){
                            $data['query'].= $oportunidad->oportunidad_id.' ';
                            $data['total_negocios_cerrados']++;
                            $query = 'SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) AS value FROM historial_oportunidades H WHERE H.key LIKE "val_pesos" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                            $valor = DB::select($query);
                            $data['valor_negocios_cerrados']+=$valor[0]->value;
                        }
                    }
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    foreach ($oportunidades_vendidas as $oportunidad) {
                        $query = 'SELECT H.value AS value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                        $fecha_cierre = DB::select($query);
                        if($fecha_cierre[0]->value <= $request->fecha_fin){
                            $data['total_negocios_cerrados']++;
                            $query = 'SELECT CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER) AS value FROM historial_oportunidades H WHERE H.key LIKE "val_pesos" AND H.oportunidad_id = '.$oportunidad->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                            $valor = DB::select($query);
                            $data['valor_negocios_cerrados']+=$valor[0]->value;
                        }
                    }
                }
                $data['valor_negocios_cerrados'] = number_format(($data['valor_negocios_cerrados'] / 1000000), 0, '.', ',');
                break;
            /*Consulta para Promedio de cierre*/
            case 3:
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = date('1990-01-01');
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                $noventaporciento = DB::select('SELECT * FROM historial_oportunidades H WHERE H.key = "ciclo_venta" AND H.value LIKE "90" ORDER BY H.id ASC');
                $dias_totales     = 0;
                $ventas = 0;
                foreach ($noventaporciento as $n) {
                    $fecha_identificacion = DB::select('SELECT * FROM historial_oportunidades H WHERE H.key = "fecha_ff" AND H.oportunidad_id = "' . $n->oportunidad_id . '" ORDER BY H.id DESC');
                    $fecha_cierre = DB::select('SELECT * FROM historial_oportunidades H WHERE H.key = "fecha_oc" AND H.oportunidad_id = "' . $n->oportunidad_id . '" ORDER BY H.id DESC');
                    if (isset($fecha_cierre[0]->value) && isset($fecha_identificacion[0]->value)) {
                        if ($fecha_cierre[0]->value >= $filtro_fecha_inicio && $fecha_cierre[0]->value <= $filtro_fecha_fin) {
                            $ventas++;
                            if (((strtotime($fecha_cierre[0]->value)) - (strtotime($fecha_identificacion[0]->value))) > 0) {
                                $dias_totales += (int) (((strtotime($fecha_cierre[0]->value)) - (strtotime($fecha_identificacion[0]->value))) / 86400);
                            }
                        }
                    }
                }
                if (isset($dias_totales) && isset($ventas) && $ventas > 0) {
                    $promedio = ($dias_totales / $ventas) / 30;
                    $data['promedio'] = number_format($promedio, 1, ".", ",");
                } else {
                    $data['promedio'] = 0;
                }
                break;
            /*Monto de negocios cerrados*/
            case 4:
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-m-d');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                $filtro_cargo = '';
                foreach($request->cargos as $cargo){
                    $filtro_cargo = $filtro_cargo==''?'U.cargo LIKE "'.$cargo.'"':$filtro_cargo.' OR U.cargo LIKE "'.$cargo.'"';
                }
                $query = 'SELECT U.id, U.nombres, U.apellidos, U.foto FROM users U WHERE U.deleted_at IS NULL AND ('.$filtro_cargo.')';
                $usuarios = DB::SELECT($query);
                ob_start(); ?>
                <script>
                var chart = AmCharts.makeChart("neg_cerr_chart", {
                "rotate": true,
                "type": "serial",
                "theme": "light",
                "addClassNames": true,
                "responsive": {
                "enabled": true
                },
                "dataProvider": [
                <?php
                $masalto=0;
                $data = [];
                foreach($usuarios as $usuario){
                    $nombres=explode(" ",$usuario->nombres);
                    $valor_usuario=0; $h=0;
                    $mis_oportunidades=DB::select('SELECT DISTINCT H.oportunidad_id FROM historial_oportunidades H WHERE H.oportunidad_id IN (SELECT HO.oportunidad_id FROM historial_oportunidades HO WHERE HO.key = "ciclo_venta" AND HO.value = "90")');
                    foreach ($mis_oportunidades as $m_o) {
                      $fecha_cierre_m_o=DB::select('SELECT * FROM historial_oportunidades HO WHERE HO.key="fecha_oc" AND HO.oportunidad_id="'.$m_o->oportunidad_id.'" ORDER BY HO.id DESC');

                      if((date("Y-m-d", strtotime($fecha_cierre_m_o[0]->value)))>=$filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_cierre_m_o[0]->value)))<=$filtro_fecha_fin){
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                        if(($oportunidad_buscar[0]->value)==$usuario->id){
                          $valoresoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                          $valor_usuario+=(int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $valoresoportunidades[0]->value))));
                        }
                      }
                    }
                    $valor_usuario=round($valor_usuario/1000000);
                    if($masalto<$valor_usuario){
                      $masalto=$valor_usuario;
                    }
                    if($usuario->foto==""){
                      $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                    }else{
                      $foto="/images/file/clientes/".$usuario->foto;
                    }
                    ?>
                    {
                    "name": "<?=$nombres[0]?>",
                    "points": <?=$valor_usuario?>,
                    "color": "#58c1ce",
                    "bullet": "<?=$foto?>"
                    },
                    <?php } ?>
                    ],
                    "valueAxes": [{
                    "maximum": <?=$masalto?>,
                    "minimum": 0,
                    "axisAlpha": 0,
                    "dashLength": 4,
                    "position": "left"
                    }],
                    "startDuration": 1,
                    "graphs": [{
                    "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b> Millones</span>",
                    "bulletOffset": 10,
                    "bulletSize": 40,
                    "bulletColor": "#bd362f",
                    "colorField": "color",
                    "classNameField": "iconClass",
                    "cornerRadiusTop": 8,
                    "customBulletField": "bullet",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0,
                    "type": "column",
                    "valueField": "points"
                    }],
                    "smoothCustomBullets": {
                    "borderRadius": "auto"
                    },
                    "marginTop": 0,
                    "marginRight": 0,
                    "marginLeft": 0,
                    "marginBottom": 0,
                    "autoMargins": true,
                    "categoryField": "name",
                    "categoryAxis": {
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "inside": true,
                    "tickLength": 0
                    },
                    "export": {
                    "enabled": false
                    }
                    });
                    </script>
                <script>
                    var chart = AmCharts.makeChart("chart_radar", {
                    "type": "radar",
                    "theme": "light",
                    "dataProvider": [
                <?php
                foreach($usuarios as $usuario){
                    $tiempo=0;
                    $promedioindividual=0;
                    $totaloportunidadindividual=0;
                    $nombres=explode(" ",$usuario->nombres);
                    $apellidos=explode(" ",$usuario->apellidos);
                    $valor_usuario=0; $h=0;
                    $mis_oportunidades=DB::select('SELECT DISTINCT H.oportunidad_id FROM historial_oportunidades H WHERE H.oportunidad_id IN (SELECT HO.oportunidad_id FROM historial_oportunidades HO WHERE HO.key = "ciclo_venta" AND HO.value = "90")');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades HO WHERE HO.oportunidad_id = '.$m_o->oportunidad_id.' AND HO.key = "responsable" ORDER BY HO.id DESC');
                        if(($oportunidad_buscar[0]->value)==$usuario->id){
                            $historial1=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" ORDER BY `id` DESC');
                            $fecha_fin=0;
                            $fecha_inicio=0;
                            $contador=count($historial1);
                            if($contador>0){
                                if($historial1[0]->value>=90){
                                    $historial2=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_ff" ORDER BY `id` DESC');
                                    if(count($historial2)>0){
                                        $fecha_inicio=strtotime($historial2[0]->value);
                                    }
                                    $historial3=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC');
                                    if(count($historial3)>0){
                                        $fecha_fin=strtotime($historial3[0]->value);
                                    }
                                    if(date('Y-m-d',strtotime($historial3[0]->value)) >= $filtro_fecha_inicio && date('Y-m-d',strtotime($historial3[0]->value)) <= $filtro_fecha_fin){
                                        $totaloportunidadindividual++;
                                    }
                                }
                            }
                            if(date('Y-m-d',strtotime($historial3[0]->value)) >= $filtro_fecha_inicio && date('Y-m-d',strtotime($historial3[0]->value)) <= $filtro_fecha_fin){
                                $tiempo+=$fecha_fin - $fecha_inicio;
                            }
                        }
                    }
                    if($totaloportunidadindividual>0){
                        $dias=$tiempo / 86400;
                        $promedioindividual=($dias/$totaloportunidadindividual)/30;
                        $promedioindividual=number_format($promedioindividual,0,".","");
                    }else{
                        $promedioindividual=0;
                    }
                    $valor_usuario=round($valor_usuario/1000000);
                    if($usuario->foto==""){
                      $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                    }else{
                      $foto="/images/file/clientes/".$usuario->foto;
                    }
                    ?>
                    {
                    "country": "<?php echo $nombres[0].' '.$apellidos[0]?>",
                    "litres": <?php echo $promedioindividual; ?>
                    },
                    <?php
                    }
                    ?>
                    ],
                    "valueAxes": [{
                        "axisTitleOffset": 20,
                        "minimum": 0,
                        "axisAlpha": 0.15
                    }],
                    "startDuration": 2,
                    "graphs": [{
                            "balloonText": "[[value]] Promedio de cierre",
                            "bullet": "round",
                            "lineThickness": 2,
                            "valueField": "litres"
                    }],
                    "categoryField": "country",
                    "export": {
                        "enabled": true
                    }
                    });
                    </script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                ob_start();
                foreach($usuarios as $usuario){
                    $nombres=explode(" ",$usuario->nombres);
                    $apellidos=explode(" ",$usuario->apellidos);
                    $valor_usuario=0;
                    $h=0;
                    $mis_oportunidades=DB::select('SELECT DISTINCT H.oportunidad_id FROM historial_oportunidades H WHERE H.key = "responsable" AND H.value = "'.$usuario->id.'" AND H.oportunidad_id IN (SELECT HO.oportunidad_id FROM historial_oportunidades HO WHERE HO.key = "ciclo_venta" AND HO.value = "90") ORDER BY H.oportunidad_id ASC');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                        if($oportunidad_buscar[0]->value==$usuario->id){
                            $fecha_cierre=DB::select('SELECT value FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC LIMIT 0,1');
                            if(date('Y-m-d',strtotime($fecha_cierre[0]->value)) >= $filtro_fecha_inicio && date('Y-m-d',strtotime($fecha_cierre[0]->value)) <= $filtro_fecha_fin){
                                $h++;
                            }
                        }
                    }
                    $valor_usuario=round($valor_usuario/1000000);
                    if($usuario->foto==""){
                      $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                    }else{
                      $foto="/images/file/clientes/".$usuario->foto;
                    }
                   ?>
                    <div class="col col-md-1">
                        <img src="<?=$foto?>" class="rounded-circle  w-100 shadow-2 mt-2">
                        <div class="card mt-2">
                          <ul class="list-group list-group-flush mb-0">
                            <li class="list-group-item p-0"><h6><?=$nombres[0].' '.$apellidos[0]?></h6></li>
                            <li class="list-group-item bg-essi text-white p-0"><h4><?=$h?></h4></li>
                          </ul>
                        </div>
                    </div>
                    <?php
                    }
                $data['html_cantidad'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Oportunidades Perdidas*/
            case 5:
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                $valor = 0;
                $h     = 0;
                $t     = 0;
                $oportunidades = Oportunidades::all();
                foreach ($oportunidades as $key) {
                    $historial            = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $key->id . ' AND `key` = "ciclo_venta" order by id');
                    $fecha_cierre_perdida = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $key->id . ' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');
                    $contador = count($historial);
                    if ($contador > 0) {
                        if ($historial[$contador - 1]->value == 90) {
                            $fecha_cierre_vendida = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $key->id . ' AND `key` = "fecha_oc" order by updated_at DESC LIMIT 0,1');
                            if (date('Y-m-d', strtotime($fecha_cierre_vendida[0]->value)) >= $filtro_fecha_inicio && date('Y-m-d', strtotime($fecha_cierre_vendida[0]->value)) <= $filtro_fecha_fin) {
                                $t++;
                            }
                        }
                        if ((date("Y-m-d", strtotime($fecha_cierre_perdida[0]->updated_at))) >= $filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_cierre_perdida[0]->updated_at))) <= $filtro_fecha_fin) {
                            if ($historial[$contador - 1]->value == 0) {
                                $t++;
                                $h++;
                                $info = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $key->id . ' AND `key` = "val_pesos" ORDER BY `id` DESC');
                                $valor += ((int) (str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $info[0]->value)))));
                            }
                        }
                    }
                }
                $valor = round($valor / 1000000);
                if ($t === 0) {
                    $ttal_cierre_los = 0;
                } else {
                    $ttal_cierre_los = round($h * 100 / $t, 1);
                }
                $data['porcentaje_respecto_total'] = $ttal_cierre_los;
                $data['numero_oportunidades'] = $h;
                $data['valor'] = '$ '.number_format($valor,"0",",",".");
                break;
            /*Modal Oportunidades perdidas*/
            case 6:
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                $filtro_cargo = '';
                foreach($request->cargos as $cargo){
                    $filtro_cargo = $filtro_cargo==''?'U.cargo LIKE "'.$cargo.'"':$filtro_cargo.' OR U.cargo LIKE "'.$cargo.'"';
                }
                $query = 'SELECT U.id, U.nombres, U.apellidos, U.foto FROM users U WHERE U.deleted_at IS NULL AND ('.$filtro_cargo.')';
                $usuarios = DB::SELECT($query);
                $valor = 0;
                $h     = 0;
                $masalto = 0;
                ob_start(); ?>
                <script>
                var chart = AmCharts.makeChart("m_negocios_perdidos", {
                "rotate": true,
                "type": "serial",
                "theme": "light",
                "dataProvider": [
                <?php
                foreach ($usuarios as $usuario) {
                        $nombres = explode(" ", $usuario->nombres);
                        $mis_oportunidades = DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = ' . $usuario->id . ' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');
                        foreach ($mis_oportunidades as $m_o) {
                            $oportunidad_buscar = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "responsable" ORDER BY `id` DESC');
                            if (($oportunidad_buscar[0]->value) == $usuario->id) {
                                $fecha_cierre_ind_perdida = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "ciclo_venta" ORDER BY `id` DESC LIMIT 0,1');
                                if ((date("Y-m-d", strtotime($fecha_cierre_ind_perdida[0]->updated_at))) >= $filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_cierre_ind_perdida[0]->updated_at))) <= $filtro_fecha_fin) {
                                    $h++;
                                    $info = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "val_pesos" ORDER BY `id` DESC');
                                    $valor += ((int) (str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $info[0]->value)))));
                                }
                            }
                        }
                        $valor = round($valor / 1000000);
                        $masalto = $valor>$masalto?$valor:$masalto;
                        if ($usuario->foto == "") {
                            $foto = "https://placeholdit.imgix.net/~text?txtsize=45&txt=" . $nombres[0] . "&w=200&h=200";
                        } else {
                            $foto = "/images/file/clientes/" . $usuario->foto;
                        }
                ?>
                    {
                        "name": "<?=$nombres[0]?>",
                        "points": <?=$valor?>,
                        "color": "#0270c6",
                        "bullet": "<?=$foto?>"
                    },
                <?php
                }
                ?>
                ],
                "valueAxes": [{
                    "maximum": <?=$masalto+($masalto*0.1)?>,
                    "minimum": 0,
                    "axisAlpha": 0,
                    "dashLength": 4,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "<span style='font-size:13px;'>[[category]]: <b>$ [[value]]</b> Millones</span>",
                    "bulletOffset": 10,
                    "bulletSize": 30,
                    "colorField": "color",
                    "cornerRadiusTop": 8,
                    "customBulletField": "bullet",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0,
                    "type": "column",
                    "valueField": "points"
                }],
                "smoothCustomBullets": {
                    "borderRadius": "auto"
                },
                "marginTop": 0,
                "marginRight": 0,
                "marginLeft": 0,
                "marginBottom": 0,
                "autoMargins": true,
                "categoryField": "name",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "inside": true,
                    "tickLength": 0
                },
                "export": {
                    "enabled": false
                }
                });
                </script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                ob_start();
                $valor = 0;
                $x     = 0;
                $i     = 0;
                $mayor = 0;
                foreach ($usuarios as $usuario) {
                        $h                 = 0;
                        $mis_oportunidades = DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = ' . $usuario->id . ' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');
                        foreach ($mis_oportunidades as $m_o) {
                            $oportunidad_buscar = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "responsable" ORDER BY `id` DESC');
                            if (($oportunidad_buscar[0]->value) == $usuario->id) {
                                $fecha_oc_per = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');
                                if ((date("Y-m-d", strtotime($fecha_oc_per[0]->updated_at))) >= $filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_oc_per[0]->updated_at))) <= $filtro_fecha_fin) {
                                    $historial = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "ciclo_venta" order by id');
                                    $contador  = count($historial);
                                    if ($contador > 0) {
                                        if ($historial[$contador - 1]->value == 0) {
                                            $h++;
                                            $x++;
                                        }
                                    }
                                }
                            }
                        }

                        $vector[$i] = $h;
                        if ($vector[$i] > $mayor) {
                            $mayor = $vector[$i];
                        }
                        $i++;
                }
                $p = 0;

                foreach ($usuarios as $usuario) {
                    $total_oportunidades = 0;
                    $mis_oportunidades = DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = ' . $usuario->id . ' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND (`value` = "0" OR `value` = "90"))');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "responsable" ORDER BY `id` DESC');
                        if (($oportunidad_buscar[0]->value) == $usuario->id) {
                            $fecha_oc_per1 = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');
                            if ((date("Y-m-d", strtotime($fecha_oc_per1[0]->updated_at))) >= $filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_oc_per1[0]->updated_at))) <= $filtro_fecha_fin) {
                                $total_oportunidades++;
                            }
                        }
                    }

                    $nombres = explode(" ", $usuario->nombres);
                    if ($total_oportunidades > 0) {
                        $total_porc_perdida = ($vector[$p] * 100) / $total_oportunidades;
                        $total_porc_perdida = round($total_porc_perdida, 1);
                    } else {
                        $total_porc_perdida = 0;
                    }
                    $cantidadnegociosperdidos[$usuario->id]['nombre']=$nombres[0];
                    $cantidadnegociosperdidos[$usuario->id]['foto']=$usuario->foto;
                    $cantidadnegociosperdidos[$usuario->id]['cantidad']=$vector[$p];
                ?>
                <li class="list-group-item border-0">
                      <div class="row">
                          <div class="pl-0 col-md-4">
                            <h4 class="m-0 p-2"><?=$nombres[0]?></h4>
                          </div>
                          <div class="pl-0 col-md-4">
                            <span class="badge badge-pill bg-primary p-2 w-100" style="font-size:2rem"><?=$total_porc_perdida?>%</span>
                          </div>
                          <div class="pl-0 col-md-4 py-2">
                            <div class="progress mb-0" style="cursor:pointer;" title="<?=$vector[$p]?> <?php if($vector[$p]==1){ ?>oportunidad perdida<?php }else{ ?>oportunidades perdidas<?php } ?>">
                              <div class="progress-bar" role="progressbar" style="width: <?=$total_porc_perdida?>%" aria-valuenow="<?=$total_porc_perdida?>" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                      </div>
                  </li>
                    <?php
                    $p++;
                }
                $data['html_oportunidad_perdida_porcentaje'] = ob_get_contents();
                ob_end_clean();
                ob_start();
                foreach($cantidadnegociosperdidos as $dato){
                ?>
                <div class="col-md-6 col-sm-12">
                    <img src="/images/file/clientes/<?=$dato['foto']?>" class="img-thumbnail rounded-circle w-25 d-inline">
                    <div class="card d-inline ">
                      <div class="card-body d-inline">
                        <span><?=$dato['nombre']?></span>
                      </div>
                    </div>
                    <div class="card d-inline bg-info text-white rounded-0">
                      <div class="card-body d-inline">
                        <span><?=$dato['cantidad']?></span>
                      </div>
                    </div>
                 </div>
                <?php
                }
                $data['html_oportunidad_perdida_cantidad'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Oportunidades Identificadas DONA*/
            case 7:
                $where = '';
                if($request->fecha_inicio != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION >= "'.$request->fecha_inicio.'"';
                }
                if($request->fecha_fin != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION <= "'.$request->fecha_fin.'"';
                }
                $query = 'SELECT V.id, V.CICLO, V.IDRESPONSABLE AS RESPONSABLE, V.CORTORESPONSABLE as RESPONSABLENOMBRE, V.VALOR FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90'.$where;
                $oportunidades=DB::select($query);
                foreach($oportunidades as $oportunidad){
                    $data[$oportunidad->RESPONSABLE]['nombre'] = $oportunidad->RESPONSABLENOMBRE;
                    $data[$oportunidad->RESPONSABLE]['valor'] = !isset($data[$oportunidad->RESPONSABLE]['valor'])?intval($oportunidad->VALOR):$data[$oportunidad->RESPONSABLE]['valor']+intval($oportunidad->VALOR);
                    $total = !isset($total)?$oportunidad->VALOR:$total+$oportunidad->VALOR;
                }
                ob_start(); ?>
                <script>
                var chartpieinicial = AmCharts.makeChart("donutchart", {
                      "responsive": {
                        "enabled": true
                      },
                      "type": "pie",
                      "theme": "light",
                      "dataProvider": [
                <?php
                if(isset($data)){
                    foreach($data as $dato){
                       ?>
                       {
                         "title": "<?=$dato['nombre']?>",
                         "value": <?=number_format(($dato['valor']/1000000), 0, '.', '')?>,
                         "alpha": 1
                       },
                       <?php
                    }
                } ?>
                ],
                      "titleField": "title",
                      "valueField": "value",
                      "labelRadius": -130,
                      "radius": "42%",
                      "innerRadius": "65%",
                      "labelText": ""
                    });
                    chartpieinicial.addListener("rollOverSlice", function(event) {
                        var height = $("#donutchart").height();
                        event.chart.clearLabels();
                        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
                        event.chart.addLabel(null, height - (height * 46 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
                    });
                    chartpieinicial.addListener( "rollOutSlice", function(event){
                        event.chart.clearLabels();
                    });
                </script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                $data['total'] = number_format($total/1000000, 0, '.', ',');
                break;
            /*Oportunidades Identificadas (Modal)*/
            case 8:
                /*Primera dona*/
                $where = '';
                if($request->fecha_inicio != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION >= "'.$request->fecha_inicio.'"';
                }
                if($request->fecha_fin != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION <= "'.$request->fecha_fin.'"';
                }
                $query = 'SELECT V.id, V.CICLO, V.IDRESPONSABLE AS RESPONSABLE, V.CORTORESPONSABLE as RESPONSABLENOMBRE, V.VALOR FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90'.$where;
                $oportunidades=DB::select($query);
                foreach($oportunidades as $oportunidad){
                    $data[$oportunidad->RESPONSABLE]['nombre'] = $oportunidad->RESPONSABLENOMBRE;
                    $data[$oportunidad->RESPONSABLE]['valor'] = !isset($data[$oportunidad->RESPONSABLE]['valor'])?intval($oportunidad->VALOR):$data[$oportunidad->RESPONSABLE]['valor']+intval($oportunidad->VALOR);
                    $total = !isset($total)?$oportunidad->VALOR:$total+$oportunidad->VALOR;
                }
                /*Segunda dona*/
                $alta = $request->largo_plazo_Alta + $request->medio_plazo_Alta + $request->corto_plazo_Alta;
                $media = $request->largo_plazo_Media + $request->medio_plazo_Media + $request->corto_plazo_Media;
                $baja = $request->largo_plazo_Baja + $request->medio_plazo_Baja + $request->corto_plazo_Baja;

                $corto = $request->corto_plazo_Baja + $request->corto_plazo_Media + $request->corto_plazo_Alta;
                $mediano = $request->medio_plazo_Baja + $request->medio_plazo_Media + $request->medio_plazo_Alta;
                $largo = $request->largo_plazo_Baja + $request->largo_plazo_Media + $request->largo_plazo_Alta;
                ob_start(); ?>
                <script>
                    var opor_per_mdl_1 = AmCharts.makeChart("opor_per_mdl_1", {
                      "responsive": {
                        "enabled": true
                      },
                      "type": "pie",
                      "theme": "light",
                      "dataProvider": [
                <?php
                if(isset($data)){
                    foreach($data as $dato){
                       ?>
                       {
                         "title": "<?=$dato['nombre']?>",
                         "value": <?=number_format(($dato['valor']/1000000), 0, '.', '')?>,
                         "alpha": 1
                       },
                       <?php
                    }
                } ?>
                ],
                      "titleField": "title",
                      "valueField": "value",
                      "labelRadius": -130,
                      "radius": "42%",
                      "innerRadius": "65%",
                      "labelText": ""
                    });
                    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
                    opor_per_mdl_1.addListener("rollOverSlice", function(event) {
                        var height = $("#opor_per_mdl_1").height();
                        event.chart.clearLabels();
                        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
                        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
                    });
                    /*remover labels al retirar el cursor*/
                    opor_per_mdl_1.addListener( "rollOutSlice", function(event){
                        event.chart.clearLabels();
                    });
                    var opor_per_mdl_2 = AmCharts.makeChart("opor_per_mdl_2", {
                      "responsive": {
                        "enabled": true
                      },
                      "type": "pie",
                      "theme": "light",
                      "dataProvider": [{
                        "title": "Bajo",
                        "value": <?=$baja?>,
                        "alpha": 1
                      }, {
                        "title": "Medio",
                        "value": <?=$media?>,
                        "alpha": 1
                      }, {
                        "title": "Alto",
                        "value": <?=$alta?>,
                        "alpha": 0.1
                      }],
                      "titleField": "title",
                      "valueField": "value",
                      "labelRadius": -130,
                      "radius": "42%",
                      "innerRadius": "65%",
                      "labelText": ""
                    });
                    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
                    opor_per_mdl_2.addListener("rollOverSlice", function(event) {
                        var height = $("#opor_per_mdl_2").height();
                        event.chart.clearLabels();
                        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
                        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
                    });
                    /*remover labels al retirar el cursor*/
                    opor_per_mdl_2.addListener( "rollOutSlice", function(event){
                        event.chart.clearLabels();
                    });
                    var opor_per_mdl_3 = AmCharts.makeChart("opor_per_mdl_3", {
                      "responsive": {
                        "enabled": true
                      },
                      "type": "pie",
                      "theme": "light",
                      "dataProvider": [{
                        "title": "Corto Plazo",
                        "value": <?=$corto?>,
                        "alpha": 1
                      }, {
                        "title": "Mediano Plazo",
                        "value": <?=$mediano?>,
                        "alpha": 1
                      }, {
                        "title": "Largo Plazo",
                        "value": <?=$largo?>,
                        "alpha": 0.1
                      }],
                      "titleField": "title",
                      "valueField": "value",
                      "labelRadius": -130,
                      "radius": "42%",
                      "innerRadius": "65%",
                      "labelText": ""
                    });

                    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
                    opor_per_mdl_3.addListener("rollOverSlice", function(event) {
                        var height = $("#opor_per_mdl_3").height();
                        event.chart.clearLabels();
                        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 20, null, 0, null, true);
                        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
                    });
                    /*remover labels al retirar el cursor*/
                    opor_per_mdl_3.addListener( "rollOutSlice", function(event){
                        event.chart.clearLabels();
                    });
                </script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                $data['total'] = number_format($total/1000000, 0, '.', ',');
                break;
            /*Oportunidades Identificadas Barras*/
            case 9:
                $where = '';
                if($request->fecha_inicio != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION >= "'.$request->fecha_inicio.'"';
                }
                if($request->fecha_fin != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION <= "'.$request->fecha_fin.'"';
                }
                $query = 'SELECT V.id FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90'.$where;
                $oportunidades = DB::SELECT($query);
                $metas1=Metas_oportunidade::findOrFail(1);
                $corto[0]=0;
                $corto[1]=0;
                $corto[2]=0;
                $medio[0]=0;
                $medio[1]=0;
                $medio[2]=0;
                $largo[0]=0;
                $largo[1]=0;
                $largo[2]=0;
                $cortoprecio[0]=0;
                $cortoprecio[1]=0;
                $cortoprecio[2]=0;
                $medioprecio[0]=0;
                $medioprecio[1]=0;
                $medioprecio[2]=0;
                $largoprecio[0]=0;
                $largoprecio[1]=0;
                $largoprecio[2]=0;
                foreach ($oportunidades as $key) {
                    $fecha="";
                    $valor=0;
                    $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
                    $contador=count($historial);
                    if($contador>0){
                      if($historial[$contador-1]->value>0&&$historial[$contador-1]->value<90){
                        $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "fecha_oc" order by id DESC');
                        if(count($plazo)>0){
                          $dias=(strtotime($plazo[0]->value))-(strtotime(date('Y-m-d')));
                          $dias=(int)$dias;
                          $dias=$dias/86400;
                          $dias=(int)$dias;
                          $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "probabilidad" order by id DESC');
                          $valor_1=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "val_pesos" order by id DESC');
                          if(isset($valor_1[0]->value)){
                              $valor_total=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor_1[0]->value)))));
                          }
                          if($dias<=($metas1->corto_dias)){
                            if(count($probabilidad)>0){
                              if(($probabilidad[0]->value)=='Alta'){
                                $corto[0]++;
                                $cortoprecio[0]+=$valor_total;
                              }elseif(($probabilidad[0]->value)=='Media'){
                                $corto[1]++;
                                $cortoprecio[1]+=$valor_total;
                              }elseif(($probabilidad[0]->value)=='Baja'){
                                $corto[2]++;
                                $cortoprecio[2]+=$valor_total;
                              }
                            }
                          }elseif($dias<=($metas1->medio_dias)){
                            if(count($probabilidad)>0){
                              if(($probabilidad[0]->value)=='Alta'){
                                $medio[0]++;
                                $medioprecio[0]+=$valor_total;
                              }elseif(($probabilidad[0]->value)=='Media'){
                                $medio[1]++;
                                $medioprecio[1]+=$valor_total;
                              }elseif(($probabilidad[0]->value)=='Baja'){
                                $medio[2]++;
                                $medioprecio[2]+=$valor_total;
                              }
                            }
                          }elseif($dias>($metas1->medio_dias)){
                            if(count($probabilidad)>0){
                              if(($probabilidad[0]->value)=='Alta'){
                                $largo[0]++;
                                $largoprecio[0]+=$valor_total;
                              }elseif(($probabilidad[0]->value)=='Media'){
                                $largo[1]++;
                                $largoprecio[1]+=$valor_total;
                              }elseif(($probabilidad[0]->value)=='Baja'){
                                $largo[2]++;
                                $largoprecio[2]+=$valor_total;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                  $data['cortoAlta'] = (int)(round($cortoprecio[0]/1000000));
                  $data['cortoMedia'] = (int)(round($cortoprecio[1]/1000000));
                  $data['cortoBaja'] = (int)(round($cortoprecio[2]/1000000));
                  $data['medianoAlta'] = (int)(round($medioprecio[0]/1000000));
                  $data['medianoMedia'] = (int)(round($medioprecio[1]/1000000));
                  $data['medianoBaja'] = (int)(round($medioprecio[2]/1000000));
                  $data['largoAlta'] = (int)(round($largoprecio[0]/1000000));
                  $data['largoMedia'] = (int)(round($largoprecio[1]/1000000));
                  $data['largoBaja'] = (int)(round($largoprecio[2]/1000000));
                break;
            /*Oportunidades Identificadas Barras (Modal)*/
            case 10:
                $where = '';
                if($request->fecha_inicio != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION >= "'.$request->fecha_inicio.'"';
                }
                if($request->fecha_fin != ''){
                    $where.= ' AND V.FECHAIDENTIFICACION <= "'.$request->fecha_fin.'"';
                }

                $metas1=Metas_oportunidade::findOrFail(1);
                $usuarios = DB::SELECT('SELECT * FROM users U WHERE U.deleted_at IS NULL AND (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Gerente Comercial" OR U.cargo LIKE "Partner Solution")');
                $y=0;
                ob_start();
                foreach($usuarios as $usuario){
                    $nombres=explode(" ",$usuario->nombres);
                    $apellidos=explode(" ",$usuario->apellidos);
                    if($usuario->foto==""){
                      $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                    }else{
                      $foto="/images/file/clientes/".$usuario->foto;
                    }
                    $corto[0]       = 0;
                    $corto[1]       = 0;
                    $corto[2]       = 0;
                    $medio[0]       = 0;
                    $medio[1]       = 0;
                    $medio[2]       = 0;
                    $largo[0]       = 0;
                    $largo[1]       = 0;
                    $largo[2]       = 0;
                    $cortoprecio[0] = 0;
                    $cortoprecio[1] = 0;
                    $cortoprecio[2] = 0;
                    $medioprecio[0] = 0;
                    $medioprecio[1] = 0;
                    $medioprecio[2] = 0;
                    $largoprecio[0] = 0;
                    $largoprecio[1] = 0;
                    $largoprecio[2] = 0;
                    $mis_oportunidades = DB::select('SELECT V.id AS oportunidad_id FROM view_oportunidades V WHERE V.IDRESPONSABLE = '.$usuario->id.' AND V.CICLO > 0 AND V.CICLO < 90'.$where);
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "responsable" ORDER BY `id` DESC');
                        if (($oportunidad_buscar[0]->value) == $usuario->id) {
                            $plazo = DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = ' . $oportunidad_buscar[0]->oportunidad_id . ' AND `key` = "fecha_oc" order by id DESC');
                            if (count($plazo) > 0) {
                                $dias         = (strtotime($plazo[0]->value)) - (strtotime(date('Y-m-d')));
                                $dias         = $dias / 86400;
                                $dias         = (int) $dias;
                                $probabilidad = DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = ' . $oportunidad_buscar[0]->oportunidad_id . ' AND `key` = "probabilidad" order by id DESC');
                                $valor        = DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = ' . $oportunidad_buscar[0]->oportunidad_id . ' AND `key` = "val_pesos" order by id DESC');

                                if (isset($valor[0]->value)) {
                                    $valor_total = ((int) (str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor[0]->value)))));
                                }

                                if ($dias <= ($metas1->corto_dias)) {
                                    if (count($probabilidad) > 0) {
                                        if (($probabilidad[0]->value) == 'Alta') {
                                            $corto[0]++;
                                            $cortoprecio[0] += $valor_total;
                                        } elseif (($probabilidad[0]->value) == 'Media') {
                                            $corto[1]++;
                                            $cortoprecio[1] += $valor_total;
                                        } elseif (($probabilidad[0]->value) == 'Baja') {
                                            $corto[2]++;
                                            $cortoprecio[2] += $valor_total;
                                        }
                                    }
                                } elseif ($dias <= ($metas1->medio_dias)) {
                                    if (count($probabilidad) > 0) {
                                        if (($probabilidad[0]->value) == 'Alta') {
                                            $medio[0]++;
                                            $medioprecio[0] += $valor_total;
                                        } elseif (($probabilidad[0]->value) == 'Media') {
                                            $medio[1]++;
                                            $medioprecio[1] += $valor_total;
                                        } elseif (($probabilidad[0]->value) == 'Baja') {
                                            $medio[2]++;
                                            $medioprecio[2] += $valor_total;
                                        }
                                    }
                                } elseif ($dias > ($metas1->medio_dias)) {
                                    if (count($probabilidad) > 0) {
                                        if (($probabilidad[0]->value) == 'Alta') {
                                            $largo[0]++;
                                            $largoprecio[0] += $valor_total;
                                        } elseif (($probabilidad[0]->value) == 'Media') {
                                            $largo[1]++;
                                            $largoprecio[1] += $valor_total;
                                        } elseif (($probabilidad[0]->value) == 'Baja') {
                                            $largo[2]++;
                                            $largoprecio[2] += $valor_total;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    /*Calcular corto-media-largo de todos los usuarios*/
                    $corto[$usuario->id] = $cortoprecio[0] + $cortoprecio[1] + $cortoprecio[2];
                    $media[$usuario->id] = $medioprecio[0] + $medioprecio[1] + $medioprecio[2];
                    $largo[$usuario->id] = $largoprecio[0] + $largoprecio[1] + $largoprecio[2];
                    /*fin Calcular corto-media-largo de todos los usuarios*/
                    /*Calcular alta-media-baja de todos los usuarios*/
                    $alta[$usuario->id]  = $cortoprecio[0] + $medioprecio[0] + $largoprecio[0];
                    $medio[$usuario->id] = $cortoprecio[1] + $medioprecio[1] + $largoprecio[1];
                    $baja[$usuario->id]  = $cortoprecio[2] + $medioprecio[2] + $largoprecio[2];
                    /*Fin Calcular alta-media-baja de todos los usuarios*/
                    $cortoprecio[0]      = ((int) (round($cortoprecio[0] / 1000000)));
                    $cortoprecio[1]      = ((int) (round($cortoprecio[1] / 1000000)));
                    $cortoprecio[2]      = ((int) (round($cortoprecio[2] / 1000000)));
                    $medioprecio[0]      = ((int) (round($medioprecio[0] / 1000000)));
                    $medioprecio[1]      = ((int) (round($medioprecio[1] / 1000000)));
                    $medioprecio[2]      = ((int) (round($medioprecio[2] / 1000000)));
                    $largoprecio[0]      = ((int) (round($largoprecio[0] / 1000000)));
                    $largoprecio[1]      = ((int) (round($largoprecio[1] / 1000000)));
                    $largoprecio[2]      = ((int) (round($largoprecio[2] / 1000000)));
                    if($cortoprecio[0] > 0 || $cortoprecio[1] > 0 || $cortoprecio[2] > 0 || $medioprecio[0] > 0 || $medioprecio[1] > 0 || $medioprecio[2] > 0 || $largoprecio[0] > 0 || $largoprecio[1] > 0 || $largoprecio[2] > 0){
                    ?>
                    <div class="row">
                       <div class="col-md-3">
                           <img src="<?=$foto?>" class="img-modal-OI">
                           <p class="h3 text-info text-OI"><?=$nombres[0].' '.$apellidos[0]?></p>
                       </div>
                       <div class="col-md-9">
                           <div class="row">
                               <div class="col-md-4">
                                   <div class="graficas-alta" id="graficas-corto<?=$y?>"></div>
                               </div>
                               <div class="col-md-4">
                                   <div class="graficas-mediano" id="graficas-mediano<?=$y?>"></div>
                               </div>
                               <div class="col-md-4">
                                   <div class="graficas-corto" id="graficas-largo<?=$y?>"></div>
                               </div>
                           </div>
                       </div>
                   </div>
                    <script>
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart_OI_corto<?=$y?>);
                    function drawChart_OI_corto<?=$y?>() {
                        var data = google.visualization.arrayToDataTable([
                            ['Plazo', 'Alta Millones:', 'Media Millones:', 'Baja Millones:'],
                            ['Corto Plazo',<?=$cortoprecio[0]?>, <?=$cortoprecio[1]?>, <?=$cortoprecio[2]?>]
                        ]);
                        var options = {
                            width: '30%',
                            height: 400,
                            chart: {
                                title: '',
                                subtitle: '',
                            },
                            vAxis: {format: 'decimal'}
                        };
                        var chart = new google.charts.Bar(document.getElementById('graficas-corto<?=$y?>'));
                        chart.draw(data, google.charts.Bar.convertOptions(options));
                     }
                     google.charts.load('current', {
                        'packages': ['bar']
                     });
                     google.charts.setOnLoadCallback(drawChart_OI_mediano<?=$y?>);
                     function drawChart_OI_mediano<?=$y?>() {
                        var data = google.visualization.arrayToDataTable([
                            ['Plazo', 'Alta Millones:', 'Media Millones:', 'Baja Millones:'],
                            ['Mediano Plazo',<?=$medioprecio[0]?>, <?=$medioprecio[1]?>, <?=$medioprecio[2]?>]
                        ]);
                        var options = {
                            width: '30%',
                            height: 400,
                            chart: {
                                title: '',
                                subtitle: '',
                            },
                            vAxis: {format: 'decimal'}
                        };
                        var chart = new google.charts.Bar(document.getElementById('graficas-mediano<?=$y?>'));
                        chart.draw(data, google.charts.Bar.convertOptions(options));
                      }
                      google.charts.load('current', {
                        'packages': ['bar']
                      });
                      google.charts.setOnLoadCallback(drawChart_OI_largo<?=$y?>);
                      function drawChart_OI_largo<?=$y?>() {
                        var data = google.visualization.arrayToDataTable([
                            ['Plazo', 'Alta Millones:', 'Media Millones:', 'Baja Millones:'],
                            ['Largo Plazo',<?=$largoprecio[0]?>, <?=$largoprecio[1]?>, <?=$largoprecio[2]?>]
                        ]);
                        var options = {
                            width: '30%',
                            height: 400,
                            chart: {
                                title: '',
                                subtitle: '',
                            },
                            vAxis: {format: 'decimal'}
                        };
                        var chart = new google.charts.Bar(document.getElementById('graficas-largo<?=$y?>'));
                        chart.draw(data, google.charts.Bar.convertOptions(options));
                       }
                      </script>
                    <?php
                    }
                    $y++;
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Ciclos de venta*/
            case 11:
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                //Eduard: Oportunidades 0%
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $query = 'SELECT "0" AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = 0 AND O.CICLOFECHACREADO >= "'.$filtro_fecha_inicio.'" AND O.CICLOFECHACREADO <= "'.$filtro_fecha_fin.'") AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = 0 AND O.CICLOFECHACREADO >= "'.$filtro_fecha_inicio.'" AND O.CICLOFECHACREADO <= "'.$filtro_fecha_fin.'") AS TOTALVALOR';
                }else{
                    $query = 'SELECT "0" AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = 0 AND O.CICLOFECHACREADO >= "'.$filtro_fecha_inicio.'" AND O.CICLOFECHACREADO <= "'.$filtro_fecha_fin.'") AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = 0 AND O.CICLOFECHACREADO >= "'.$filtro_fecha_inicio.'" AND O.CICLOFECHACREADO <= "'.$filtro_fecha_fin.'") AS TOTALVALOR';
                }
                $oportunidadesPERDIDAS = DB::SELECT($query);

                //Eduard: Oportunidades 10% al 80%
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $query = 'SELECT C.porcentaje AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje) AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje) AS TOTALVALOR FROM ciclos C WHERE C.id BETWEEN 2 AND 9';
                }else{
                    $query = 'SELECT C.porcentaje AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje AND O.FECHAIDENTIFICACION >= "'.$filtro_fecha_inicio.'" AND O.FECHAIDENTIFICACION <= "'.$filtro_fecha_fin.'") AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje AND O.FECHAIDENTIFICACION >= "'.$filtro_fecha_inicio.'" AND O.FECHAIDENTIFICACION <= "'.$filtro_fecha_fin.'") AS TOTALVALOR FROM ciclos C WHERE C.id BETWEEN 2 AND 9';
                }
                $oportunidadesIDENTIFICADAS = DB::SELECT($query);

                //Eduard: Oportunidades 90%
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $query = 'SELECT "90" AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'") AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'") AS TOTALVALOR';
                }else{
                    $query = 'SELECT "90" AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'") AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'") AS TOTALVALOR';
                }
                $oportunidadesVENDIDAS = DB::SELECT($query);
                $titulo = array('Detección','Presentación','Propuesta','Negociación','Gestión');
                $cantidad = $oportunidadesPERDIDAS[0]->TOTAL + $oportunidadesVENDIDAS[0]->TOTAL;
                foreach($oportunidadesIDENTIFICADAS as $identificadas){
                    $cantidad+=$identificadas->TOTAL;
                }
                $iconos_vigentes = array('<i class="fa fa-search fa-3x"></i>','<i class="fa fa-bullseye fa-3x"></i>','<i class="far fa-money-bill-alt fa-3x"></i>','<i class="fa fa-handshake fa-3x"></i>','<i class="fa fa-check-circle fa-3x"></i>');
                ob_start(); ?>
                <div class="col px-0">
                  <div class="card  border-radius-top-0">
                      <div class="card-body px-0">
                      <h5 class="card-title"><span class="text-info"><?=$oportunidadesPERDIDAS[0]->TOTAL?></span> Oportunidades</h5>
                      <h5 class="card-title"><span class="text-info"><?=number_format(($oportunidadesPERDIDAS[0]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</h5>
                      <h5 class="card-title"><span class="text-info"><?=round($oportunidadesPERDIDAS[0]->TOTAL/$cantidad*100)?></span>%</h5>
                      <i class="fa fa-question-circle fa-3x"></i>
                      <h5 class="card-title"><span class="text-success">0</span>%</h5>
                      <h6 class="card-title">Perdida</h6>
                      </div>
                    </div>
                </div>
                <?php $i=0; foreach($oportunidadesIDENTIFICADAS as $identificadas){ ?>
                <div class="col px-0">
                  <div class="card  border-radius-top-0">
                      <div class="card-body px-0">
                      <h5 class="card-title"><span class="text-info"><?=$identificadas->TOTAL?></span> Oportunidades</h5>
                      <h5 class="card-title"><span class="text-info"><?=number_format(($identificadas->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</h5>
                      <h5 class="card-title"><span class="text-info"><?=round($identificadas->TOTAL/$cantidad*100)?></span>%</h5>
                      <?=$iconos_vigentes[$i]?>
                      <h5 class="card-title"><span class="text-success"><?=$identificadas->CICLO?></span>%</h5>
                      <h6 class="card-title"><?=$titulo[$i]?></h6>
                      </div>
                    </div>
                </div>
                <?php $i++; } ?>
                <div class="col px-0">
                  <div class="card  border-radius-top-0">
                      <div class="card-body px-0">
                      <h5 class="card-title"><span class="text-info"><?=$oportunidadesVENDIDAS[0]->TOTAL?></span> Oportunidades</h5>
                      <h5 class="card-title"><span class="text-info"><?=number_format(($oportunidadesVENDIDAS[0]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</h5>
                      <h5 class="card-title"><span class="text-info"><?=round($oportunidadesVENDIDAS[0]->TOTAL/$cantidad*100)?></span>%</h5>
                      <i class="fas fa-hand-holding-usd fa-3x"></i>
                      <h5 class="card-title"><span class="text-success">90</span>%</h5>
                      <h6 class="card-title">Vendida</h6>
                      </div>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Modal ciclos de ventas*/
            case 12:
                $usuarios = DB::SELECT('SELECT DISTINCT V.IDRESPONSABLE, IF(U.foto="" ,"/images/file/clientes/essi_admon.jpg", CONCAT("/images/file/clientes/",U.foto)) AS IMAGEN, SUBSTRING_INDEX(U.nombres, " ", 1) AS NOMBRE, SUBSTRING_INDEX(U.apellidos, " ", 1) AS APELLIDO FROM view_oportunidades V INNER JOIN users U ON V.IDRESPONSABLE = U.id ORDER BY V.IDRESPONSABLE ASC');
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                ob_start();
                ?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">
                                    <i class="fa fa-question-circle fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">0%</p>
                                    <p style="font-size:9px;color:#305ddb;">Perdida</p>
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-search fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">10%</p>
                                    <p style="font-size:9px;color:#305ddb;">Detección</p>
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-bullseye fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">20%</p>
                                    <p style="font-size:9px;color:#305ddb;">Presentación</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <i class="far fa-money-bill-alt fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">40%</p>
                                    <p style="font-size:9px;color:#305ddb;">Propuesta</p>
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-handshake fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">60%</p>
                                    <p style="font-size:9px;color:#305ddb;">Negociación</p>
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-check-circle fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">80%</p>
                                    <p style="font-size:9px;color:#305ddb;">Gestión</p>
                                </div>
                                <div class="col-md-3">
                                    <i class="fas fa-hand-holding-usd fa-3x"></i>
                                    <p style="font-size:14px;color:#60b90b;">90%</p>
                                    <p style="font-size:9px;color:#305ddb;">Vendida</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $y=0;
                foreach($usuarios as $usuario){
                    //Eduard: Oportunidades 0%
                    $query = 'SELECT "0" AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = 0 AND O.CICLOFECHACREADO >= "'.$filtro_fecha_inicio.'" AND O.CICLOFECHACREADO <= "'.$filtro_fecha_fin.'" AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = 0 AND O.CICLOFECHACREADO >= "'.$filtro_fecha_inicio.'" AND O.CICLOFECHACREADO <= "'.$filtro_fecha_fin.'" AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTALVALOR';

                    $data['oportunidadesPERDIDAS']=$query;
                    $oportunidadesPERDIDAS = DB::SELECT($query);
                    //Eduard: Oportunidades 10% al 80%
                    if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                        $query = 'SELECT C.porcentaje AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTALVALOR FROM ciclos C WHERE C.id BETWEEN 2 AND 9 ORDER BY CICLO ASC';
                    }else{
                        $query = 'SELECT C.porcentaje AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje AND O.FECHAIDENTIFICACION >= "'.$filtro_fecha_inicio.'" AND O.FECHAIDENTIFICACION <= "'.$filtro_fecha_fin.'" AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = C.porcentaje AND O.FECHAIDENTIFICACION >= "'.$filtro_fecha_inicio.'" AND O.FECHAIDENTIFICACION <= "'.$filtro_fecha_fin.'" AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTALVALOR FROM ciclos C WHERE C.id BETWEEN 2 AND 9 ORDER BY CICLO ASC';
                    }
                    $oportunidadesIDENTIFICADAS = DB::SELECT($query);
                    //Eduard: Oportunidades 90%
                    $query = 'SELECT "90" AS CICLO, (SELECT COUNT(*) FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'" AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTAL, (SELECT SUM(O.VALOR) FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'" AND O.IDRESPONSABLE = '.$usuario->IDRESPONSABLE.') AS TOTALVALOR';
                    $oportunidadesVENDIDAS = DB::SELECT($query);

                    $cantidad = $oportunidadesPERDIDAS[0]->TOTAL + $oportunidadesVENDIDAS[0]->TOTAL;
                    foreach($oportunidadesIDENTIFICADAS as $identificadas){
                        $cantidad+=$identificadas->TOTAL;
                    }
                    ?>
                    <div class="col-md-12">
                        <div class="row <?=$clase=($y%2)==0?'intermedia':''?> mx-3">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3 text-center">
                                        <img src="<?=$usuario->IMAGEN?>" class="img-circulo">
                                        <p style="font-size:15px;color:#305ddb;"><?=$usuario->NOMBRE?><br><?=$usuario->APELLIDO?></p>
                                    </div>
                                    <div class="col-md-3 text-center bg-ciclo">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesPERDIDAS[0]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesPERDIDAS[0]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesPERDIDAS[0]->TOTAL?></span> Oport.</label>
                                    </div>
                                    <div class="col-md-3 text-center bg-ciclo2">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesIDENTIFICADAS[0]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesIDENTIFICADAS[0]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesIDENTIFICADAS[0]->TOTAL?></span> Oport.</label>
                                    </div>
                                    <div class="col-md-3 text-center bg-ciclo">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesIDENTIFICADAS[1]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesIDENTIFICADAS[1]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesIDENTIFICADAS[1]->TOTAL?></span> Oport.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row h-100">
                                    <div class="col-md-3 text-center bg-ciclo2">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesIDENTIFICADAS[2]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesIDENTIFICADAS[2]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesIDENTIFICADAS[2]->TOTAL?></span> Oport.</label>
                                    </div>
                                    <div class="col-md-3 text-center bg-ciclo">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesIDENTIFICADAS[3]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesIDENTIFICADAS[3]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesIDENTIFICADAS[3]->TOTAL?></span> Oport.</label>
                                    </div>
                                    <div class="col-md-3 text-center bg-ciclo2">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesIDENTIFICADAS[4]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesIDENTIFICADAS[4]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesIDENTIFICADAS[4]->TOTAL?></span> Oport.</label>
                                    </div>
                                    <div class="col-md-3 text-center bg-ciclo">
                                         <label class="linea3-item mt-5"><?=$porcentaje=$cantidad>0?round($oportunidadesVENDIDAS[0]->TOTAL/$cantidad*100):0?>%</label>
                                         <label class="linea2-item"><span><?=number_format(($oportunidadesVENDIDAS[0]->TOTALVALOR/1000000), 0, ',', '.')?></span> Millones</label>
                                         <label class="linea1-item"><span><?=$oportunidadesVENDIDAS[0]->TOTAL?></span> Oport.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        $y++;
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Ventas y Proyecciones*/
            case 13:
                if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
                $paises = DB::SELECT('SELECT COUNT(DISTINCT O.pais) AS NUMERO FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'"');
                $equipos = DB::SELECT('SELECT SUM(OP.cantidad) AS NUMERO FROM view_oportunidades O INNER JOIN oportunidad_productos OP ON O.id = OP.oportunidad_id WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'" AND OP.deleted_at IS NULL');
                $data['paises'] = ($paises[0]->NUMERO)==''?0:$paises[0]->NUMERO;
                $data['equipos'] = ($equipos[0]->NUMERO)==''?0:$equipos[0]->NUMERO;
                $paisesproyecciones = DB::SELECT('SELECT COUNT(DISTINCT O.pais) AS NUMERO FROM view_oportunidades O WHERE O.CICLO < 90 AND O.CICLO > 0 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'"');
                $equiposproyecciones = DB::SELECT('SELECT SUM(OP.cantidad) AS NUMERO FROM view_oportunidades O INNER JOIN oportunidad_productos OP ON O.id = OP.oportunidad_id WHERE O.CICLO < 90 AND O.CICLO > 0 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'" AND OP.deleted_at IS NULL');
                $data['paisesproyecciones'] = ($paisesproyecciones[0]->NUMERO)==''?0:$paisesproyecciones[0]->NUMERO;
                $data['equiposproyecciones'] = ($equiposproyecciones[0]->NUMERO)==''?0:$equiposproyecciones[0]->NUMERO;
                break;
            /*Modal de Ventas*/
            case 14:
                $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
                foreach($vendidas_oportunidades as $v_o){
                    /*Millones X mes*/
                    $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                        $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));

                        $valor_total=round(((int)$cantidad)/1000000);
                        $valor_total=(int)$valor_total;

                        if(isset($meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))])){
                            $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]+=$valor_total;
                        }else{
                            $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]=$valor_total;
                        }

                    }
                    /*Fin Millones X mes*/

                    /*paises*/

                    $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                    $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
                    if(isset($pais_vendidas_oportunidades[0]->pais) && (date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                        $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));

                        $valor_total=round(((int)$cantidad)/1000000);
                        $valor_total=(int)$valor_total;

                        if(isset($pais_vendida[$pais_vendidas_oportunidades[0]->pais])){
                            $pais_vendida[$pais_vendidas_oportunidades[0]->pais]+=$valor_total;
                        }else{
                           $pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$valor_total;
                           $nombre_pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$pais_vendidas_oportunidades[0]->pais;
                        }
                    }
                    /*Fin paises*/

                    if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        /*Equipos vendidos*/
                        $productos_vendidas_oportunidades=DB::select('SELECT * FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `deleted_at` IS NULL');

                        foreach ($productos_vendidas_oportunidades as $pvo) {

                            if(isset($productos_referencia[$pvo->referencia])){
                                $productos_referencia[$pvo->referencia]+=$pvo->cantidad;
                            }else{
                                $productos_referencia[$pvo->referencia]=$pvo->cantidad;
                                $nombre_productos_referencia[$pvo->referencia]=$pvo->referencia;
                                $productos_buscar=DB::select('SELECT `name` FROM `productos` WHERE `id` = "'.$pvo->producto.'" ');
                                $referencia_buscar=DB::select('SELECT `referencia` FROM `producto_referencias` WHERE `id` = "'.$pvo->referencia.'" ');
                                $nombrecompleto_productos_referencia[$pvo->referencia]=$productos_buscar[0]->name.' - '.$referencia_buscar[0]->referencia;
                            }
                        }
                        /*Fin Equipos vendidos*/
                    }
                }
                if(isset($productos_referencia)){
       		       arsort($productos_referencia);
                }
                ob_start();
                ?>
                <script type="text/javascript" src="/components/amchart/js/dark.js"></script>
                <?php $sum=0; ?>
                <!-- amCharts javascript code -->
                <script type="text/javascript">
                    AmCharts.makeChart("modalventa1",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "columnWidth": 0,
                            "autoMarginOffset": 40,
                            "marginRight": 60,
                            "marginTop": 60,
                            "zoomOutButtonColor": "#FFC800",
                            "startDuration": 1,
                            "startEffect": "easeInSine",
                            "backgroundColor": "#001130",
                            "color": "#00BFFF",
                            "theme": "dark",
                            "categoryAxis": {
                                "gridPosition": "start"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "animationPlayed": true,
                                    "balloonColor": "#FFC800",
                                    "balloonText": "[[title]] de [[category]]:[[value]]",
                                    "behindColumns": true,
                                    "bullet": "diamond",
                                    "bulletBorderThickness": 1,
                                    "bulletColor": "#FFFFFF",
                                    "bulletSize": 13,
                                    "color": "#FFC800",
                                    "id": "AmGraph-1",
                                    "lineAlpha": 1,
                                    "lineColor": "#FF0000",
                                    "title": "Mes",
                                    "type": "smoothedLine",
                                    "valueField": "column-1"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {
                                "animationDuration": 0.1,
                                "fadeOutDuration": 0.06
                            },
                            "titles": [],
                            "dataProvider": [
                                {
                                    "category": "Enero",
                                    "column-1": "<?php if(isset($meses_ventas[1])){ $sum+=$meses_ventas[1]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Febrero",
                                    "column-1": "<?php if(isset($meses_ventas[2])){ $sum+=$meses_ventas[2]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Marzo",
                                    "column-1": "<?php if(isset($meses_ventas[3])){ $sum+=$meses_ventas[3]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Abril",
                                    "column-1": "<?php if(isset($meses_ventas[4])){ $sum+=$meses_ventas[4]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Mayo",
                                    "column-1": "<?php if(isset($meses_ventas[5])){ $sum+=$meses_ventas[5]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Junio",
                                    "column-1": "<?php if(isset($meses_ventas[6])){ $sum+=$meses_ventas[6]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Julio",
                                    "column-1": "<?php if(isset($meses_ventas[7])){ $sum+=$meses_ventas[7]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Agosto",
                                    "column-1": "<?php if(isset($meses_ventas[8])){ $sum+=$meses_ventas[8]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Septiembre",
                                    "column-1": "<?php if(isset($meses_ventas[9])){ $sum+=$meses_ventas[9]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Octubre",
                                    "column-1": "<?php if(isset($meses_ventas[10])){ $sum+=$meses_ventas[10]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Noviembre",
                                    "column-1": "<?php if(isset($meses_ventas[11])){ $sum+=$meses_ventas[11]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Diciembre",
                                    "column-1": "<?php if(isset($meses_ventas[12])){ $sum+=$meses_ventas[12]; echo $sum; }else{ echo $sum; } ?>"
                                }
                            ]
                        }
                    );
                </script>
                <div class="col-md-12" style="background-color:#001130;">
                    <a style="color: #fff; font-size: 20px;"><center>Ventas Realizadas <?=date("Y")?></center></a>
                    <div id="modalventa1" style="width: 100%; height: 400px; background-color: #001130;" ></div>
                </div>
                <script type="text/javascript" src="/components/amchart/js/light.js"></script>
                <!-- amCharts javascript code -->
                <script type="text/javascript">
                    AmCharts.makeChart("modalventa2",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "columnSpacing": 3,
                            "columnWidth": 0.69,
                            "autoMarginOffset": 40,
                            "marginRight": 70,
                            "marginTop": 70,
                            "startDuration": 1,
                            "startEffect": "easeOutSine",
                            "backgroundColor": "#001A4A",
                            "color": "#E7E7E7",
                            "fontSize": 12,
                            "theme": "light",
                            "categoryAxis": {
                                "gridPosition": "start"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonColor": "#00BFFF",
                                    "balloonText": "[[title]]  [[category]]:[[value]]",
                                    "color": "#FFFFFF",
                                    "fillAlphas": 0.9,
                                    "id": "AmGraph-1",
                                    "title": "",
                                    "type": "column",
                                    "valueField": "column-1"
                                },
                                {
                                    "animationPlayed": true,
                                    "balloonColor": "undefined",
                                    "balloonText": "[[title]] of [[category]]:[[value]]",
                                    "behindColumns": true,
                                    "fillAlphas": 0.9,
                                    "id": "AmGraph-2",
                                    "title": "graph 2",
                                    "type": "column",
                                    "valueField": "column-2"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "axisColor": "#0000FF",
                                    "axisThickness": 2,
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {},
                            "titles": [],
                            "dataProvider": [
                            <?php
                                if(isset($nombre_pais_vendida)){
                                foreach($nombre_pais_vendida as $nombre){ ?>
                                {
                                    "category": "<?=$nombre?>",
                                    "column-1": "<?=$pais_vendida[$nombre]?>"
                                },
                            <?php
                                } }
                            ?>

                            ]
                        }
                    );
                </script>
                <div class="col-md-12 mt-5" style="background-color: #001A4A;">
                    <a style="color: #fff; font-size: 20px;"><center>Ventas por paises <?=date('Y')?></center></a>
                    <div id="modalventa2" style="width: 100%; height: 400px; background-color: #001A4A;" ></div>
                </div>
                <script type="text/javascript">
                    AmCharts.makeChart("modalventa3",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "rotate": true,
                            "autoMarginOffset": 40,
                            "maxSelectedSeries": <?php if(isset($nombre_productos_referencia)){ echo count($nombre_productos_referencia); }else{ echo 0; } ?>,
                            "marginRight": 60,
                            "marginTop": 60,
                            "startDuration": 1,
                            "startEffect": "easeOutSine",
                            "backgroundColor": "#02141A",
                            "borderColor": "#FFFFFF",
                            "color": "#00BFFF",
                            "fontSize": 13,
                            "theme": "dark",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisColor": "#00BFFF",
                                "fillColor": "#FF8000"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonText": "[[title]] [[category]]:[[value]]",
                                    "fillAlphas": 1,
                                    "id": "AmGraph-1",
                                    "labelText": "",
                                    "title": "Equipo: ",
                                    "type": "column",
                                    "valueField": "column-1"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "axisColor": "#00BFFF",
                                    "fillColor": "#00BFFF",
                                    "gridColor": "#00BFFF",
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {},
                            "titles": [],
                            "dataProvider": [
                            <?php
                                if(isset($productos_referencia)){
                                foreach($productos_referencia as $clave=>$valor){ ?>
                                {
                                    "category": "<?=$nombrecompleto_productos_referencia[$clave]?>",
                                    "column-1": "<?=$productos_referencia[$clave]?>"
                                },
                            <?php
                                }
                                }
                            ?>

                            ]
                        }
                    );
                </script>
                <div class="col-md-12 mt-5" style="background-color: #02141A;">
                    <a style="color: #fff; font-size: 20px;"><center>Equipos vendidos <?php echo date('Y') ?></center></a>
                    <div id="modalventa3" style="width: 100%; <?php if(isset($nombre_productos_referencia)){ ?> height: calc(63.25px*<?=count($nombre_productos_referencia)?>); <?php }else{ ?> height:230px; <?php } ?> background-color: #02141A;" ></div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Modal de Proyecciones*/
            case 15:
                $todas_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
                foreach($todas_oportunidades as $t_o){
                    $oportunidad_buscar=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "ciclo_venta" ORDER BY `id` DESC');
                    if(((int)$oportunidad_buscar[0]->value)>=10 && ((int)$oportunidad_buscar[0]->value)<90){
                        $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                        if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                            $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                            if(isset($valor[0]->value)){
                                $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));
                            }else{
                                $cantidad = 0;
                            }


                            $valor_total=round(((int)$cantidad)/1000000);
                            $valor_total=(int)$valor_total;

                            if(isset($meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))])){
                                $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]+=$valor_total;
                            }else{
                                $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]=$valor_total;
                            }

                        }
                        /*paises*/
                        $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                        $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
                        if(isset($pais_vendidas_oportunidades[0]->pais) && (date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                            $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                            if(isset($valor[0]->value)){
                                $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));
                            }else{
                                $cantidad=0;
                            }

                            $valor_total=round(((int)$cantidad)/1000000);
                            $valor_total=(int)$valor_total;

                            if(isset($pais_vendida[$pais_vendidas_oportunidades[0]->pais])){
                                $pais_vendida[$pais_vendidas_oportunidades[0]->pais]+=$valor_total;
                            }else{
                               $pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$valor_total;
                               $nombre_pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$pais_vendidas_oportunidades[0]->pais;
                            }

                        }
                        /*Fin paises*/
                        if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        /*Equipos proyectada*/
                        $productos_proyectadas_oportunidades=DB::select('SELECT * FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `deleted_at` IS NULL');

                        foreach ($productos_proyectadas_oportunidades as $ppo) {

                            if(isset($productos_referencia[$ppo->referencia])){
                                $productos_referencia[$ppo->referencia]+=$ppo->cantidad;
                            }else{
                                $productos_referencia[$ppo->referencia]=$ppo->cantidad;
                                $nombre_productos_referencia[$ppo->referencia]=$ppo->referencia;
                                $productos_buscar=DB::select('SELECT `name` FROM `productos` WHERE `id` = "'.$ppo->producto.'" ');
                                $referencia_buscar=DB::select('SELECT `referencia` FROM `producto_referencias` WHERE `id` = "'.$ppo->referencia.'" ');
                                if(!isset($productos_buscar[0]->name)){
                                    $nombreproducto='Producto no encontrado';
                                }else{
                                    $nombreproducto=$productos_buscar[0]->name;
                                }
                                if(!isset($referencia_buscar[0]->referencia)){
                                    $nombrereferencia='Referencia no encontrado';
                                }else{
                                    $nombrereferencia=$referencia_buscar[0]->referencia;
                                }
                                $nombrecompleto_productos_referencia[$ppo->referencia]=$nombreproducto.' - '.$nombrereferencia;
                            }
                        }
                        /*Fin Equipos proyectada*/
                      }
                    }
                }
                arsort($productos_referencia);
                ob_start();
                ?>
                <?php $sum=0; ?>
                <script type="text/javascript">
                    AmCharts.makeChart("modal_proyectadas1",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "columnWidth": 0,
                            "autoMarginOffset": 40,
                            "marginRight": 60,
                            "marginTop": 60,
                            "zoomOutButtonColor": "#FFC800",
                            "startDuration": 1,
                            "startEffect": "easeInSine",
                            "backgroundColor": "#252627",
                            "color": "#FFC800",
                            "theme": "dark",
                            "categoryAxis": {
                                "gridPosition": "start"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "animationPlayed": true,
                                    "balloonColor": "#FFC800",
                                    "balloonText": "[[title]] de [[category]]:[[value]]",
                                    "behindColumns": true,
                                    "bullet": "diamond",
                                    "bulletBorderThickness": 1,
                                    "bulletColor": "#00FF30",
                                    "bulletSize": 13,
                                    "color": "#FFC800",
                                    "id": "AmGraph-1",
                                    "lineAlpha": 1,
                                    "lineColor": "#00CDFF",
                                    "title": "Mes ",
                                    "type": "smoothedLine",
                                    "valueField": "column-1"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {
                                "animationDuration": 0.1,
                                "fadeOutDuration": 0.06
                            },
                            "titles": [],
                            "dataProvider": [
                                {
                                    "category": "Enero",
                                    "column-1": "<?php if(isset($meses_ventas[1])){ $sum+=$meses_ventas[1]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Febrero",
                                    "column-1": "<?php if(isset($meses_ventas[2])){ $sum+=$meses_ventas[2]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Marzo",
                                    "column-1": "<?php if(isset($meses_ventas[3])){ $sum+=$meses_ventas[3]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Abril",
                                    "column-1": "<?php if(isset($meses_ventas[4])){ $sum+=$meses_ventas[4]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Mayo",
                                    "column-1": "<?php if(isset($meses_ventas[5])){ $sum+=$meses_ventas[5]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Junio",
                                    "column-1": "<?php if(isset($meses_ventas[6])){ $sum+=$meses_ventas[6]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Julio",
                                    "column-1": "<?php if(isset($meses_ventas[7])){ $sum+=$meses_ventas[7]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Agosto",
                                    "column-1": "<?php if(isset($meses_ventas[8])){ $sum+=$meses_ventas[8]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Septiembre",
                                    "column-1": "<?php if(isset($meses_ventas[9])){ $sum+=$meses_ventas[9]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Octubre",
                                    "column-1": "<?php if(isset($meses_ventas[10])){ $sum+=$meses_ventas[10]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Noviembre",
                                    "column-1": "<?php if(isset($meses_ventas[11])){ $sum+=$meses_ventas[11]; echo $sum; }else{ echo $sum; } ?>"
                                },
                                {
                                    "category": "Diciembre",
                                    "column-1": "<?php if(isset($meses_ventas[12])){ $sum+=$meses_ventas[12]; echo $sum; }else{ echo $sum; } ?>"
                                }
                            ]
                        }
                    );
                </script>
                <div class="col-md-12" style="background-color: #252627;">
                    <a style="color: #fff; font-size: 20px;"><center>Ventas Proyectadas <?=date("Y")?></center></a>
                    <div id="modal_proyectadas1" style="width: 100%; height: 400px; background-color: #252627;" ></div>
                </div>

                <script type="text/javascript">
                    AmCharts.makeChart("modal_proyectadas2",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "columnSpacing": 3,
                            "columnWidth": 0.68,
                            "autoMarginOffset": 40,
                            "marginRight": 70,
                            "marginTop": 70,
                            "plotAreaBorderColor": "#FFFFFF",
                            "startDuration": 1,
                            "startEffect": "easeInSine",
                            "accessibleTitle": "",
                            "backgroundColor": "#212121",
                            "borderColor": "#FFFFFF",
                            "color": "#E7E7E7",
                            "fontSize": 12,
                            "theme": "default",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisColor": "#00BFFF",
                                "fillColor": "#00BFFF",
                                "title": "",
                                "titleBold": false,
                                "titleColor": "#00BFFF",
                                "titleFontSize": 19,
                                "titleRotation": 0
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonColor": "#00BFFF",
                                    "balloonText": "[[title]]  [[category]]:[[value]]",
                                    "color": "#FFFFFF",
                                    "fillAlphas": 0.9,
                                    "id": "AmGraph-1",
                                    "title": "",
                                    "type": "column",
                                    "valueField": "column-1"
                                },
                                {
                                    "animationPlayed": true,
                                    "balloonColor": "#FFFFFF",
                                    "balloonText": "[[title]] of [[category]]:[[value]]",
                                    "behindColumns": true,
                                    "fillAlphas": 0.9,
                                    "id": "AmGraph-2",
                                    "stepDirection": "left",
                                    "title": "graph 2",
                                    "type": "column",
                                    "valueField": "column-2"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "axisThickness": 2,
                                    "tickLength": 4,
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {
                                "shadowColor": "#FFFFFF"
                            },
                            "titles": [],
                            "dataProvider": [
                                <?php
                                foreach($nombre_pais_vendida as $nombre){ ?>
                                {
                                    "category": "<?=$nombre?>",
                                    "column-1": "<?=$pais_vendida[$nombre]?>"
                                },
                            <?php
                                }
                            ?>
                            ]
                        }
                    );
                </script>
                <div class="col-md-12 mt-5" style="background-color: #212121;">
                    <a style="color: #fff; font-size: 20px;"><center>Proyección de ventas por paises</center></a>
                    <div id="modal_proyectadas2" style="width: 100%; height: 400px; background-color: #212121;" ></div>
                </div>
                <script type="text/javascript">
                    AmCharts.makeChart("modal_proyectadas3",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "rotate": true,
                            "autoMarginOffset": 40,
                            "maxSelectedSeries": <?=count($nombre_productos_referencia)?>,
                            "marginRight": 60,
                            "marginTop": 60,
                            "startDuration": 1,
                            "startEffect": "easeOutSine",
                            "backgroundColor": "#02141A",
                            "borderColor": "#FFFFFF",
                            "color": "#FFC64F",
                            "fontSize": 13,
                            "theme": "dark",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisColor": "#00BFFF",
                                "fillColor": "#FF8000"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonText": "[[title]] [[category]]:[[value]]",
                                    "fillAlphas": 1,
                                    "id": "AmGraph-1",
                                    "labelText": "",
                                    "title": "Equipo: ",
                                    "type": "column",
                                    "valueField": "column-1"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "axisColor": "#00BFFF",
                                    "fillColor": "#00BFFF",
                                    "gridColor": "#00BFFF",
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {},
                            "titles": [],
                            "dataProvider": [
                                <?php
                                    foreach($productos_referencia as $clave=>$valor){ ?>
                                    {
                                        "category": "<?=$nombrecompleto_productos_referencia[$clave]?>",
                                        "column-1": "<?=$productos_referencia[$clave]?>"
                                    },
                                <?php
                                    }
                                ?>
                            ]
                        }
                    );
                </script>
                <div class="col-md-12 mt-5" style="background-color: #02141A;">
                    <a style="color: #fff; font-size: 20px;"><center>Proyección de venta de equipos</center></a>
                    <div id="modal_proyectadas3" style="width: 100%; height: calc(23.92px*<?=count($nombre_productos_referencia)?>); background-color: #02141A;" ></div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Empresas & Clientes*/
            case 16:
                $empresasOportunidades = DB::SELECT('SELECT COUNT(DISTINCT O.empresa_id) AS NUMERO FROM view_oportunidades O WHERE O.CICLO > 0 AND O.CICLO < 90');
                $empresasIdentificadas = DB::SELECT('SELECT COUNT(*) AS NUMERO FROM empresas E WHERE E.deleted_at IS NULL');
                $clientesRegistrados = DB::SELECT('SELECT COUNT(*) AS NUMERO FROM clientes E WHERE E.deleted_at IS NULL');
                $hace_2_meses = date('Y-m-d', strtotime('-2 month'));
                $empresasIdentificadasNuevas = DB::SELECT('SELECT COUNT(*) AS NUMERO FROM empresas E WHERE E.deleted_at IS NULL AND E.created_at >= "'.$hace_2_meses.'"');
                $clientesRegistradosNuevas = DB::SELECT('SELECT COUNT(*) AS NUMERO FROM clientes C WHERE C.deleted_at IS NULL AND C.created_at >= "'.$hace_2_meses.'"');
                $data['empresasOportunidades'] = $empresasOportunidades[0]->NUMERO;
                $data['empresasIdentificadas'] = $empresasIdentificadas[0]->NUMERO;
                $data['clientesRegistrados'] = $clientesRegistrados[0]->NUMERO;
                $data['empresasIdentificadasNuevas'] = $empresasIdentificadasNuevas[0]->NUMERO;
                $data['clientesRegistradosNuevas'] = $clientesRegistradosNuevas[0]->NUMERO;
                break;
            /*Empresas & Clientes (Modal)*/
            case 17:
                $hace_2_meses = date('Y-m-d', strtotime('-2 month'));
                $usuarios = DB::SELECT('SELECT DISTINCT(E.responsable) as id_responsable, SUBSTRING_INDEX(U.nombres, " ", 1) AS NOMBRE, SUBSTRING_INDEX(U.apellidos, " ", 1) AS APELLIDO, U.foto, U.principal AS PAIS, (SELECT COUNT(*) FROM empresas E WHERE E.deleted_at IS NULL AND E.responsable = U.id) AS EMPRESAS, (SELECT COUNT(*) FROM empresas E WHERE E.deleted_at IS NULL AND E.created_at >= "'.$hace_2_meses.'" AND E.responsable = U.id) AS EMPRESASNUEVAS, (SELECT COUNT(*) FROM clientes C WHERE C.deleted_at IS NULL AND C.user_id = U.id) AS CLIENTES, (SELECT COUNT(*) FROM clientes C WHERE C.deleted_at IS NULL AND C.created_at >= "'.$hace_2_meses.'" AND C.user_id = U.id) AS CLIENTESNUEVOS FROM empresas E INNER JOIN users U ON E.responsable = U.id WHERE E.deleted_at IS NULL ORDER BY E.responsable  ASC');
                $empresasIdentificadas = DB::SELECT('SELECT COUNT(*) AS NUMERO FROM empresas E WHERE E.deleted_at IS NULL');
                $clientesRegistrados = DB::SELECT('SELECT COUNT(*) AS NUMERO FROM clientes E WHERE E.deleted_at IS NULL');
                ob_start();
                ?>
                <div class="col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-5" style="background-image: -webkit-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%);">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="h3 text-white"><?=$empresasIdentificadas[0]->NUMERO?></p>
                                    <p class="h4 text-white">Empresas Identificadas</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h5 text-white">Total</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h5 text-white">Nuevos</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5" style="background-image: -webkit-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%);">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="h3 text-white"><?=$clientesRegistrados[0]->NUMERO?></p>
                                    <p class="h4 text-white">Clientes Registrados</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h5 text-white">Total</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h5 text-white">Nuevos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $total_empresas=0;
                    foreach($usuarios as $usuario){
                        $total_empresas+=$usuario->EMPRESAS;
                    ?>
                    <div class="row mb-5">
                        <div class="col-md-2">
                            <div class="divimagen-2" style="background-image: url('/images/file/clientes/<?=$usuario->foto?>');">
                                <span class="nombre-avatar-2"><?=$usuario->NOMBRE?><br><?=$usuario->APELLIDO?></span>
                                <div class="bandera" style="background-image: url('/images/icons/<?=$usuario->PAIS?>')"></div>
                            </div>
                        </div>
                        <div class="col-md-5" style="border-bottom: 1px solid #0012be;">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="h3" style="color: #305ddb;"><?php echo $usuario->EMPRESAS; ?></p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h3" style="color: #305ddb;"><?php echo $usuario->EMPRESASNUEVAS; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5" style="border-bottom: 1px solid #0012be;">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="h3" style="color: #305ddb;"><?php echo $usuario->CLIENTES; ?></p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h3" style="color: #305ddb;"><?php echo $usuario->CLIENTESNUEVOS; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-header bg-white border-0">
                            <h4 class=""><a class="text-info">% Empresa </a> por cada comercial</h4>
                        </div>
                        <div class="card-body p-0">
                           <div class="chartwrapper">
                               <div id="porcentaje_empresa_comercial"></div>
                           </div>
                        </div>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                ob_start();
                ?>
                <script>
                var poremco = AmCharts.makeChart("porcentaje_empresa_comercial", {
                      "responsive": {
                        "enabled": true
                      },
                      "type": "pie",
                      "theme": "light",
                      "dataProvider": [
                        <?php foreach($usuarios as $usuario){ ?>
                          {
                        "title": "<?=$usuario->NOMBRE?>",
                        "value": <?=$porcentaje=$total_empresas>0?round(($usuario->EMPRESAS/$total_empresas)*100):0?>,
                        "alpha": 1
                      },
                        <?php } ?>
                ],
                      "titleField": "title",
                      "valueField": "value",
                      "labelRadius": -130,
                      "radius": "42%",
                      "innerRadius": "65%",
                      "labelText": ""
                    });
                    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
                    poremco.addListener("rollOverSlice", function(event) {
                        var height = $("#porcentaje_empresa_comercial").height();
                        event.chart.clearLabels();
                        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
                        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
                    });
                    /*remover labels al retirar el cursor*/
                    poremco.addListener( "rollOutSlice", function(event){
                        event.chart.clearLabels();
                    });
                </script>
                <?php
                $data['html_script'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Costo de venta*/
            case 18:
                $query = 'SELECT IF(SUM(VALOR)>0,SUM(VALOR),0) AS TOTAL FROM view_oportunidades WHERE CICLO LIKE "90" AND FECHACIERRE >= "'.date('Y-01-01').'" AND FECHACIERRE <= "'.date('Y-12-31').'"';
                $vendidas = DB::SELECT($query);

                /*Gastos*/
                $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                for($i=0;$i<=9;$i++){
                    $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                    $query.= $i<9?' + ':' AS TOTAL ';
                }
                $data['query'] = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE YEAR(A.fecha_actividad) = '.date('Y').' AND A.user_id IN (SELECT U.id FROM users U WHERE U.deleted_at IS NULL AND (U.cargo = "Ejecutivo Comercial" OR U.cargo = "Partner Solution"))';
                $actividades_gastos = DB::select($data['query']);
                $valor_total_gastos=intval($actividades_gastos[0]->TOTAL);
                $ajuste=DB::select('SELECT (SUM(valor_alimentacion) + SUM(valor_transporte_interno) + SUM(valor_transporte_intermunicipal) + SUM(valor_tiquete_aereo) + SUM(valor_papeleria) + SUM(valor_invitacion_cliente) + SUM(valor_alquiler_vehiculo) + SUM(valor_gasolina_pasaje) +SUM(valor_hotel) + SUM(valor_otros) + SUM(valor_salariopropio) +SUM(valor_salariotercero)) AS total FROM ajuste_gastos WHERE user_id IN (SELECT U.id FROM users U WHERE U.deleted_at IS NULL AND (U.cargo = "Ejecutivo Comercial" OR U.cargo = "Partner Solution")) AND anio = '.date("Y").' AND tipo = "valorreal"');
                $valor_total_gastos+=intval($ajuste[0]->total);

                // validar 0
                if ($vendidas[0]->TOTAL === 0) {
                    $porcentaje_gastos = 100;
                }else if($valor_total_gastos === 0){
                  $porcentaje_gastos = 0;
                }else{
                    $porcentaje_gastos=($valor_total_gastos/$vendidas[0]->TOTAL)*100;
                }
                $data['porcentaje_gastos'] = number_format($porcentaje_gastos, 2, '.', ' ');
                break;
            /*Costo de venta (Modal)*/
            case 19:
                ob_start();
                $usuarios = User::where('cargo','Ejecutivo Comercial')->orWhere('cargo','Partner Solution')->orderBy('id', 'asc')->get();
                foreach($usuarios as $usuario){
                    $nombre = explode(' ',$usuario->nombres);
                    $apellido = explode(' ',$usuario->apellidos);
                    $query = 'SELECT IF(SUM(VALOR)>0,SUM(VALOR),0) AS TOTAL FROM view_oportunidades WHERE CICLO LIKE "90" AND FECHACIERRE >= "'.date('Y-01-01').'" AND FECHACIERRE <= "'.date('Y-12-31').'" AND IDRESPONSABLE = '.$usuario->id;
                    $vendidas = DB::SELECT($query);

                    $valor_total_gastos=0;
                    $limite=intval(date('m'));
                    for($x=1;$x<=$limite;$x++){
                        $mes=$x<10?'0'.$x:$x;
                        /*Valor Real*/
                        $query = 'SELECT (A.valor_alimentacion + A.valor_transporte_interno + A.valor_transporte_intermunicipal + A.valor_tiquete_aereo + A.valor_papeleria + A.valor_invitacion_cliente + A.valor_alquiler_vehiculo + A.valor_gasolina_pasaje + A.valor_hotel + A.valor_otros + A.valor_salariopropio + A.valor_salariotercero) AS TOTAL FROM ajuste_gastos A WHERE A.tipo LIKE "valorreal" AND A.anio LIKE "'.date('Y').'" AND A.mes LIKE "'.$mes.'" AND A.user_id = '.$usuario->id;
                        $valorReal = DB::SELECT($query);
                        if(isset($valorReal[0]->TOTAL) && $valorReal[0]->TOTAL > 0){
                            $valor_total_gastos+=$valorReal[0]->TOTAL;
                        }else{
                            /*Gastos*/
                            $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                            $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                            for($i=0;$i<=9;$i++){
                                $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                                $query.= $i<9?' + ':' AS TOTAL ';
                            }
                            $data['query'] = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE YEAR(A.fecha_actividad) = '.date('Y').' AND MONTH(A.fecha_actividad) = '.$x.' AND A.user_id = '.$usuario->id;
                            $actividades_gastos = DB::select($data['query']);
                            $valor_total_gastos+=intval($actividades_gastos[0]->TOTAL);
                        }
                    }
                    // validar 0
                    if ($vendidas[0]->TOTAL == 0) {
                        $porcentaje_gastos = 100;
                    }else if($valor_total_gastos === 0){
                        $porcentaje_gastos = 0;
                    }else{
                        $porcentaje_gastos=($valor_total_gastos/$vendidas[0]->TOTAL)*100;
                    }
                    $data['porcentaje_gastos'] = $porcentaje_gastos==100?number_format($porcentaje_gastos, 0, '.', ' '):number_format($porcentaje_gastos, 2, '.', ' '); ?>
                    <div class="col-md-3">
                        <div class="row mb-3">
                            <div class="col-md-12 text-center">
                                <div class="divimagen-2" style="background-image: url('/images/file/clientes/<?=$usuario->foto?>');">
                                    <div class="bandera" style="background-image: url('/images/icons/<?=$usuario->principal?>')"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-white" style="background-color:#2ecded;">
                                <p class="h5"><?=$nombre[0].' '.$apellido[0]?></p>
                                <p class="h1" style="text-shadow: 1px 2px 4px #636060;"><?=$data['porcentaje_gastos']?>% <a class="h6">Costo de Venta</a></p>
                                <p class="h5">Gastos Totales $ <?=number_format($valor_total_gastos, 0, '.', ',')?></p>
                            </div>
                            <div class="col-md-12 blue-sky darken-2">
                              <div class="card-action1">
                                  <img src="images/graphic.svg" class="mt-1" style="background: none;">
                              </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Cumplimiento Presupuestal*/
            case 20:
                $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $data['mes_actual'] = $meses[intval(date('m'))];
                /*Gastos Mes anterior*/
                $Pasado = date('Y-m-d', strtotime('-1 month')) ; // resta 1 mes
                $MesPasado = intval(date('m',strtotime($Pasado)));
                $data['mes_anterior'] = $meses[$MesPasado];
                $AnoMesPasado = intval(date('Y',strtotime($Pasado)));
                $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                for($i=0;$i<=9;$i++){
                    $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                    $query.= $i<9?' + ':' AS TOTAL ';
                }
                $data['query'] = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE month(A.fecha_actividad) = '.$MesPasado.' AND YEAR(A.fecha_actividad) = '.$AnoMesPasado.' AND A.user_id IN (SELECT U.id FROM users U WHERE U.deleted_at IS NULL AND (U.cargo = "Ejecutivo Comercial" OR U.cargo = "Partner Solution"))';
                $actividades_gastos = DB::select($data['query']);
                /*Fin Gastos Mes anterior*/

                /*Presupuesto Mes Anterior*/
                $query = '';
                $campos = array('alimentacion','transporte_interno','transporte_intermunicipal','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasaje','hotel','otros','salario_propio','salario_tercero');
                for($i=0;$i<=11;$i++){
                    $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(P.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                    $query.= $i<11?' + ':' AS TOTAL ';
                }
                $data['query'] = 'SELECT '.$query.' FROM presupuestos P WHERE P.mes = '.$MesPasado.' AND P.anio = '.$AnoMesPasado.' AND P.user_id IN (SELECT id FROM users WHERE cargo = "Ejecutivo Comercial" OR cargo = "Partner Solution")';
                $presupuesto = DB::SELECT($data['query']);
                /*Fin Presupuesto Mes Anterior*/

                /*Gasto Mes Actual*/
                $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                for($i=0;$i<=9;$i++){
                    $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                    $query.= $i<9?' + ':' AS TOTAL ';
                }
                $query = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE month(A.fecha_actividad) = '.intval(date('m')).' AND YEAR(A.fecha_actividad) = '.intval(date('Y')).' AND A.user_id IN (SELECT U.id FROM users U WHERE U.deleted_at IS NULL AND (U.cargo = "Ejecutivo Comercial" OR U.cargo = "Partner Solution"))';
                $actividades_gastos1 = DB::select($query);
                /*Fin Gasto Mes Actual*/

                /*Presupuesto Mes Actual*/
                $query = '';
                $campos = array('alimentacion','transporte_interno','transporte_intermunicipal','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasaje','hotel','otros','salario_propio','salario_tercero');
                for($i=0;$i<=11;$i++){
                    $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(P.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                    $query.= $i<11?' + ':' AS TOTAL ';
                }
                $data['query'] = 'SELECT '.$query.' FROM presupuestos P WHERE P.mes = '.intval(date('m')).' AND P.anio = '.intval(date('Y')).' AND P.user_id IN (SELECT id FROM users WHERE cargo = "Ejecutivo Comercial" OR cargo = "Partner Solution")';
                $presupuesto1 = DB::SELECT($data['query']);
                /*Fin Presupuesto Mes Actual*/

                /*Gasto Parcial de presupuesto año actual Incluye datos ajuste*/
                $GASTOS_AJUSTES = 0;
                for($i=1;$i<=12;$i++){
                    $query = 'SELECT SUM(CONVERT(valor_alimentacion,UNSIGNED INTEGER) + CONVERT(valor_transporte_interno,UNSIGNED INTEGER) + CONVERT(valor_transporte_intermunicipal,UNSIGNED INTEGER) + CONVERT(valor_tiquete_aereo,UNSIGNED INTEGER) + CONVERT(valor_papeleria,UNSIGNED INTEGER) + CONVERT(valor_invitacion_cliente,UNSIGNED INTEGER) + CONVERT(valor_alquiler_vehiculo,UNSIGNED INTEGER) + CONVERT(valor_gasolina_pasaje,UNSIGNED INTEGER) + CONVERT(valor_hotel,UNSIGNED INTEGER) + CONVERT(valor_otros,UNSIGNED INTEGER)) AS total  FROM ajuste_gastos WHERE anio LIKE "'.date('Y').'" AND CONVERT(mes,UNSIGNED INTEGER) = '.$i.' AND tipo LIKE "valorreal"';
                    $valor_real = DB::SELECT($query);
                    if(intval($valor_real[0]->total) == 0){
                        $mes = $i<10?'0'.$i:$i;
                        $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM actividades WHERE fecha_actividad BETWEEN "'.date('Y-'.$mes.'-01').'" AND "'.date('Y-'.$mes.'-31').'" AND CONVERT(MONTH(fecha_actividad),UNSIGNED INTEGER) NOT IN (SELECT DISTINCT(mes) AS mes FROM ajuste_gastos WHERE anio LIKE "'.date('Y').'")';
                        $gasto = DB::SELECT($query);
                        $GASTOS_AJUSTES+=intval($gasto[0]->total);
                    }else{
                        $GASTOS_AJUSTES+=intval($valor_real[0]->total);
                    }
                }
                $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_interno, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_intermunicipal, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasaje, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM presupuesto_anuals WHERE anio LIKE "'.date('Y').'"';
                $data['presupuesto'] = DB::select($query);
                $data['e_p_a'] = 0;
                if(!empty($data['presupuesto'][0]->total)){
                    $data['e_p_a'] = ($GASTOS_AJUSTES/$data['presupuesto'][0]->total)*100;
                    $data['gasto_parcial_ano_actual'] = number_format($data['e_p_a'], 0, '.', ' ');
                }
                /*Fin Gasto Parcial de presupuesto año actual Incluye datos ajuste*/
                $data['ejecucion_presupuesto_ultimo_mes']=intval($presupuesto[0]->TOTAL)>0?intval($actividades_gastos[0]->TOTAL)/intval($presupuesto[0]->TOTAL)*100:0;
                $data['ejecucion_presupuesto_ultimo_mes']=number_format($data['ejecucion_presupuesto_ultimo_mes'], 0, '.', ',');
                $data['gasto_parcial_mes_actual']=intval($presupuesto1[0]->TOTAL)>0?intval($actividades_gastos1[0]->TOTAL)/intval($presupuesto1[0]->TOTAL)*100:0;
                $data['gasto_parcial_mes_actual']=number_format($data['gasto_parcial_mes_actual'], 0, '.', ',');
                break;
            /*Cumplimiento Presupuestal (Modal)*/
            case 21:
                $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $data['mes_actual'] = $meses[intval(date('m'))];

                ob_start();
                ?>
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-4 text-white text-center" style="background-color:#ae9189;border-right: .5px solid #fff;">
                            <p class="h5">Cumplimiento Presupuesto <?=$meses[intval(date('m'))-1]?> Segun Bitacora</p>
                        </div>
                        <div class="col-md-4 text-white text-center" style="background-color:#9a6e63;border-right: .5px solid #fff;">
                            <p class="h5">Gasto Parcial de presupuesto <?=$data['mes_actual']?> Segun Bitacora</p>
                        </div>
                        <div class="col-md-4 text-white text-center" style="background-color:#5f3326;border-right: .5px solid #fff;">
                            <p class="h5">Gasto Parcial de presupuesto <?=date('Y')?> Incluye datos ajuste</p>
                        </div>
                    </div>
                </div>
                <?php
                $usuarios = User::where('cargo','Ejecutivo Comercial')->orWhere('cargo','Partner Solution')->orderBy('id', 'asc')->get();
                foreach($usuarios as $usuario){
                    $nombres = explode(' ',$usuario->nombres);
                    $apellidos = explode(' ',$usuario->apellidos);
                    /*Gastos Mes anterior*/
                    $Pasado = date('Y-m-d', strtotime('-1 month')) ; // resta 1 mes
                    $MesPasado = intval(date('m',strtotime($Pasado)));
                    $data['mes_anterior'] = $meses[$MesPasado];
                    $AnoMesPasado = intval(date('Y',strtotime($Pasado)));
                    $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                    $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                    for($i=0;$i<=9;$i++){
                        $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                        $query.= $i<9?' + ':' AS TOTAL ';
                    }
                    $data['query'] = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE month(A.fecha_actividad) = '.$MesPasado.' AND YEAR(A.fecha_actividad) = '.$AnoMesPasado.' AND A.user_id = '.$usuario->id;
                    $actividades_gastos = DB::select($data['query']);
                    /*Fin Gastos Mes anterior*/

                    /*Presupuesto Mes Anterior*/
                    $query = '';
                    $campos = array('alimentacion','transporte_interno','transporte_intermunicipal','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasaje','hotel','otros','salario_propio','salario_tercero');
                    for($i=0;$i<=11;$i++){
                        $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(P.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                        $query.= $i<11?' + ':' AS TOTAL ';
                    }
                    $data['query'] = 'SELECT '.$query.' FROM presupuestos P WHERE P.mes = '.$MesPasado.' AND P.anio = '.$AnoMesPasado.' AND P.user_id = '.$usuario->id;
                    $presupuesto = DB::SELECT($data['query']);
                    /*Fin Presupuesto Mes Anterior*/
                    $data['ejecucion_presupuesto_ultimo_mes']=intval($presupuesto[0]->TOTAL)>0?intval($actividades_gastos[0]->TOTAL)/intval($presupuesto[0]->TOTAL)*100:0;
                    $data['ejecucion_presupuesto_ultimo_mes']=number_format($data['ejecucion_presupuesto_ultimo_mes'], 0, '.', ',');

                    /*Gasto Mes Actual*/
                    $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                    $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                    for($i=0;$i<=9;$i++){
                        $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                        $query.= $i<9?' + ':' AS TOTAL ';
                    }
                    $query = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE month(A.fecha_actividad) = '.intval(date('m')).' AND YEAR(A.fecha_actividad) = '.intval(date('Y')).' AND A.user_id = '.$usuario->id;
                    $actividades_gastos1 = DB::select($query);
                    /*Fin Gasto Mes Actual*/

                    /*Presupuesto Mes Actual*/
                    $query = '';
                    $campos = array('alimentacion','transporte_interno','transporte_intermunicipal','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasaje','hotel','otros','salario_propio','salario_tercero');
                    for($i=0;$i<=11;$i++){
                        $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(P.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                        $query.= $i<11?' + ':' AS TOTAL ';
                    }
                    $data['query'] = 'SELECT '.$query.' FROM presupuestos P WHERE P.mes = '.intval(date('m')).' AND P.anio = '.intval(date('Y')).' AND P.user_id = '.$usuario->id;
                    $presupuesto1 = DB::SELECT($data['query']);
                    /*Fin Presupuesto Mes Actual*/
                    $data['gasto_parcial_mes_actual']=intval($presupuesto1[0]->TOTAL)>0?intval($actividades_gastos1[0]->TOTAL)/intval($presupuesto1[0]->TOTAL)*100:0;
                    $data['gasto_parcial_mes_actual']=number_format($data['gasto_parcial_mes_actual'], 0, '.', ',');

                    /*Gasto Parcial de presupuesto año actual Incluye datos ajuste*/
                    $GASTOS_AJUSTES = 0;
                    for($i=1;$i<=12;$i++){
                        $query = 'SELECT SUM(CONVERT(valor_alimentacion,UNSIGNED INTEGER) + CONVERT(valor_transporte_interno,UNSIGNED INTEGER) + CONVERT(valor_transporte_intermunicipal,UNSIGNED INTEGER) + CONVERT(valor_tiquete_aereo,UNSIGNED INTEGER) + CONVERT(valor_papeleria,UNSIGNED INTEGER) + CONVERT(valor_invitacion_cliente,UNSIGNED INTEGER) + CONVERT(valor_alquiler_vehiculo,UNSIGNED INTEGER) + CONVERT(valor_gasolina_pasaje,UNSIGNED INTEGER) + CONVERT(valor_hotel,UNSIGNED INTEGER) + CONVERT(valor_otros,UNSIGNED INTEGER)) AS total  FROM ajuste_gastos WHERE anio LIKE "'.date('Y').'" AND CONVERT(mes,UNSIGNED INTEGER) = '.$i.' AND tipo LIKE "valorreal" AND user_id = '.$usuario->id;
                        $valor_real = DB::SELECT($query);
                        if(intval($valor_real[0]->total) == 0){
                            $mes = $i<10?'0'.$i:$i;
                            $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM actividades WHERE user_id = '.$usuario->id.' AND fecha_actividad BETWEEN "'.date('Y-'.$mes.'-01').'" AND "'.date('Y-'.$mes.'-31').'" AND CONVERT(MONTH(fecha_actividad),UNSIGNED INTEGER) NOT IN (SELECT DISTINCT(mes) AS mes FROM ajuste_gastos WHERE anio LIKE "'.date('Y').'")';
                            $gasto = DB::SELECT($query);
                            $GASTOS_AJUSTES+=intval($gasto[0]->total);
                        }else{
                            $GASTOS_AJUSTES+=intval($valor_real[0]->total);
                        }
                    }
                    $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_interno, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_intermunicipal, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasaje, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM presupuesto_anuals WHERE user_id = '.$usuario->id.' AND anio LIKE "'.date('Y').'"';
                    $data['presupuesto'] = DB::select($query);
                    $data['gasto_parcial_ano_actual'] = 0;
                    if(!empty($data['presupuesto'][0]->total)){
                        $data['e_p_a'] = ($GASTOS_AJUSTES/$data['presupuesto'][0]->total)*100;
                        $data['gasto_parcial_ano_actual'] = number_format($data['e_p_a'], 0, '.', ' ');
                    }
                    /*Fin Gasto Parcial de presupuesto año actual Incluye datos ajuste*/
                ?>
                <div class="col-md-2" style="height: 87px;">
                    <div class="row mb-3">
                        <div class="col-md-12 text-center">
                            <div class="divimagen-2" style="background-image: url('/images/file/clientes/<?=$usuario->foto?>');">
                                <span class="nombre-avatar-2"><?=$nombres[0]?><br><?=$apellidos[0]?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10" style="border-bottom: 1px solid #58302542;height: 87px;">
                    <div class="row mt-5">
                        <div class="col-md-4 text-info text-center">
                            <p class="h4"><?=$data['ejecucion_presupuesto_ultimo_mes']?>%</p>
                        </div>
                        <div class="col-md-4 text-info text-center">
                            <p class="h4"><?=$data['gasto_parcial_mes_actual']?>%</p>
                        </div>
                        <div class="col-md-4 text-info text-center">
                            <p class="h4"><?=$data['gasto_parcial_ano_actual']?>%</p>
                        </div>
                    </div>
                </div>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Uso del tiempo*/
            case 22:
                $query = 'SELECT SUM(((CONVERT(substring(A.tiempo_fin,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_fin,4,2),UNSIGNED INTEGER))/60) - ((CONVERT(substring(A.tiempo_inicio,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_inicio,4,2),UNSIGNED INTEGER))/60)) AS Horas, TA.tipo, TA.concepto AS SUBTIPOCONCEPTO FROM actividades A INNER JOIN subtipo_actividads TA ON A.subtipo = TA.id WHERE A.subtipo IS NOT NULL AND YEAR(A.fecha_actividad) = "'.date('Y').'" GROUP BY A.subtipo ORDER BY Horas DESC LIMIT 0,5';
                $uso_tiempo = DB::SELECT($query);
                foreach($uso_tiempo as $uso){
                    $suma = isset($suma)?$suma+$uso->Horas:$uso->Horas;
                }
                ob_start();
                ?>
                <li class="list-group-item p-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 list-head-child">
                                <h2><?=$porcentaje=round($uso_tiempo[0]->Horas/$suma*100)?>%</h2>
                            </div>
                            <div class="col-md-3 list-head-child">
                                <h4>
                                    <?=round($uso_tiempo[0]->Horas)?><br>
                                    <small class="text-muted">Horas</small>
                                </h4>
                            </div>
                            <div class="col-md-6 list-head-child">
                                <h4>
                                    <?=$uso_tiempo[0]->tipo?><br>
                                    <small class="text-muted"><?=$uso_tiempo[0]->SUBTIPOCONCEPTO?></small>
                                </h4>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
                for($i=1;$i<=4;$i++){
                ?>
                <li class="list-group-item p-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 list-body-child">
                                <h5><?=$porcentaje=round($uso_tiempo[$i]->Horas/$suma*100)?>%</h5>
                            </div>
                            <div class="col-md-3 list-body-child">
                                <h5 class="font-weight-normal"><?=round($uso_tiempo[$i]->Horas)?> horas</h5>
                            </div>
                            <div class="col-md-3">
                                <h5><?=$uso_tiempo[$i]->tipo?></h5>
                            </div>
                            <div class="col-md-4">
                                <h5 class="font-weight-normal"><?=$uso_tiempo[$i]->SUBTIPOCONCEPTO?></h5>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
                }
                ?>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                $query='SELECT SUM(((CONVERT(substring(A.tiempo_fin,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_fin,4,2),UNSIGNED INTEGER))/60) - ((CONVERT(substring(A.tiempo_inicio,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_inicio,4,2),UNSIGNED INTEGER))/60)) AS Horas, A.tipo_actividad FROM actividades A WHERE A.subtipo IS NOT NULL AND YEAR(A.fecha_actividad) = "'.date('Y').'" GROUP BY A.tipo_actividad ORDER BY Horas DESC LIMIT 0,5';
                $uso_tiempo_x_actividad=DB::SELECT($query);
                ob_start();
                ?>
                <script>
                 google.charts.load('current', {
                    'packages': ['corechart']
                });
                google.charts.setOnLoadCallback(drawChartPie);

                function drawChartPie() {
                    var data = google.visualization.arrayToDataTable([
                        ['Task', 'Hours per Day'],
                        <?php for($i=0;$i<count($uso_tiempo_x_actividad);$i++){ ?>
                        ['<?=$uso_tiempo_x_actividad[$i]->tipo_actividad?>', <?=round($uso_tiempo_x_actividad[$i]->Horas)?>],
                        <?php } ?>
                    ]);

                    var options = {
                        width: '100%',
                        height: 400,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('uso_tiempo_chart'));
                    chart.draw(data, options);
                }
                </script>
                <?php
                $data['html2'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Uso del tiempo (Modal)*/
            case 23:
                $query = 'SELECT DISTINCT A.user_id, U.nombres, U.apellidos, CONCAT("/images/file/clientes/",U.foto) as FOTO FROM actividades A INNER JOIN users U ON A.user_id = U.id WHERE YEAR(A.fecha_actividad) = "'.date('Y').'" ORDER BY A.user_id ASC';
                $usuarios = DB::SELECT($query);
                ob_start();
                foreach($usuarios as $usuario){
                    $nombre=explode(' ',$usuario->nombres);
                    $apellido=explode(' ',$usuario->apellidos);
                    $query = 'SELECT SUM(((CONVERT(substring(A.tiempo_fin,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_fin,4,2),UNSIGNED INTEGER))/60) - ((CONVERT(substring(A.tiempo_inicio,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_inicio,4,2),UNSIGNED INTEGER))/60)) AS Horas, TA.tipo, TA.concepto AS SUBTIPOCONCEPTO FROM actividades A INNER JOIN subtipo_actividads TA ON A.subtipo = TA.id WHERE A.user_id = '.$usuario->user_id.' AND A.subtipo IS NOT NULL AND YEAR(A.fecha_actividad) = "'.date('Y').'" GROUP BY A.subtipo ORDER BY Horas DESC LIMIT 0,5';
                    $uso_tiempo = DB::SELECT($query);
                    $suma=0;
                    foreach($uso_tiempo as $uso){
                        $suma+=intval($uso->Horas);
                    }
                    ?>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2 pr-md-0 pr-sm-4">
                                <img src="<?=$usuario->FOTO?>" class="rounded-circle img-thumbnail shadow-2" width="40%">
                                <h4 class="text-info my-2"><?=$nombre[0].' '.$apellido[0]?></h4>
                            </div>
                            <?php for($i=0;$i<=4;$i++){ ?>
                            <div class="col-md-2 col-sm-12">
                                <div class="container card px-0">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row ">
                                                <div class="col-md-12 px-0 palette text-white h-50">
                                                   <?php if(isset($uso_tiempo[$i]->Horas)){ ?>
                                                    <h2 class="mt-3"><?=$porcentaje=round($uso_tiempo[$i]->Horas/$suma*100)?>%</h2>
                                                    <?php }else{ ?>
                                                    <h2 class="mt-3">0%</h2>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-12 px-0 bg-secondary text-white h-50">
                                                   <?php if(isset($uso_tiempo[$i]->Horas)){ ?>
                                                    <h4><?=round($uso_tiempo[$i]->Horas)?> <small class="text-white ml-2">Horas</small></h4>
                                                    <?php }else{ ?>
                                                    <h4>0 <small class="text-white ml-2">Horas</small></h4>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 pl-2">
                                            <?php if(isset($uso_tiempo[$i]->tipo)){ ?>
                                            <h5 class=""><?=$uso_tiempo[$i]->tipo?></h5>
                                            <h5 class="font-weight-normal"><?=$uso_tiempo[$i]->SUBTIPOCONCEPTO?></h5>
                                            <?php }else{ ?>
                                            <h5 class="">No hay tipo</h5>
                                            <h5 class="font-weight-normal">No hay subtipo</h5>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Gestión actividades pendientes*/
            case 24:
                /*Actividades hecha a tiempo*/
                $query = 'SELECT (SELECT COUNT(A.fuera_tiempo) FROM actividades A WHERE YEAR(A.fecha_actividad) = "'.date('Y').'" AND A.fuera_tiempo LIKE "true") AS FUERATIEMPO, (SELECT COUNT(A.fuera_tiempo) FROM actividades A WHERE YEAR(A.fecha_actividad) = "'.date('Y').'" AND A.fuera_tiempo LIKE "false") AS HECHAATIEMPO, (SELECT COUNT(A.fuera_tiempo) FROM actividades A WHERE YEAR(A.fecha_actividad) = "'.date('Y').'") AS TOTAL';
                $actividades=DB::SELECT($query);
                $data['porcentaje_hecha_tiempo'] = round($actividades[0]->HECHAATIEMPO/$actividades[0]->TOTAL*100);

                /*Actividades pendientes semana*/
                $ultimoDiasemanasiguiente=strtotime("+7 Day",strtotime(date("Y-m-d")));
                $ultimoDiasemanasiguiente=date ( 'Y-m-d' , $ultimoDiasemanasiguiente );
                $ultimoDiamessiguiente=strtotime("+1 Month",strtotime(date("Y-m-d")));
                $ultimoDiamessiguiente=date ( 'Y-m-d' , $ultimoDiamessiguiente );
                $query = 'SELECT (SELECT COUNT(*) FROM pendientes P1 WHERE P1.estado LIKE "ejecutando" AND P1.fecha_ejecucion >= "'.date('Y-m-d').'" AND P1.fecha_ejecucion <= "'.$ultimoDiasemanasiguiente.'") AS TOTALSEMANA, (SELECT COUNT(*) FROM pendientes P1 WHERE P1.estado LIKE "ejecutando" AND P1.fecha_ejecucion >= "'.date('Y-m-d').'" AND P1.fecha_ejecucion <= "'.$ultimoDiamessiguiente.'") AS TOTALMES, (SELECT COUNT(*) FROM pendientes_historiales H1 WHERE H1.estado LIKE "aplazado" AND YEAR(H1.created_at) = '.date('Y').') AS TOTALAPLAZADA';
                $semana_mes = DB::SELECT($query);

                $data['pendientes_semana']=$semana_mes[0]->TOTALSEMANA;

                /*Actividades pendientes del mes*/
                $data['pendientes_mes']=$semana_mes[0]->TOTALMES;
                /*Actividades aplazadas */
                $data['aplazadas_mes']=$semana_mes[0]->TOTALAPLAZADA;
                break;
            /*Gestión actividades pendientes (Modal)*/
            case 25:
                /*Actividades hecha a tiempo*/
                $query = 'SELECT COUNT(A.fuera_tiempo) AS TIEMPO, A.user_id, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1)," ",SUBSTRING_INDEX(U.apellidos, " ", 1)) AS NOMBRE, (SELECT COUNT(*) FROM actividades A1 WHERE YEAR(A1.fecha_actividad) = "'.date('Y').'" AND A1.user_id = A.user_id) AS TOTAL FROM actividades A INNER JOIN users U ON A.user_id = U.id WHERE YEAR(A.fecha_actividad) = "'.date('Y').'" AND A.fuera_tiempo LIKE "false" GROUP BY A.user_id';
                $actividades=DB::SELECT($query);

                /*PENDIENTES SEMANA Y MES*/
                $ultimoDiasemanasiguiente=strtotime("+7 Day",strtotime(date("Y-m-d")));
                $ultimoDiasemanasiguiente=date ( 'Y-m-d' , $ultimoDiasemanasiguiente );
                $ultimoDiamessiguiente=strtotime("+1 Month",strtotime(date("Y-m-d")));
                $ultimoDiamessiguiente=date ( 'Y-m-d' , $ultimoDiamessiguiente );
                $query = 'SELECT DISTINCT P.user_id, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) AS NOMBRE, (SELECT COUNT(*) FROM pendientes P1 WHERE P1.estado LIKE "ejecutando" AND P1.fecha_ejecucion >= "'.date("Y-m-d").'" AND P1.fecha_ejecucion <= "'.$ultimoDiasemanasiguiente.'" AND P1.user_id = P.user_id) AS TOTALSEMANA, (SELECT COUNT(*) FROM pendientes P2 WHERE P2.estado LIKE "ejecutando" AND P2.fecha_ejecucion >= "'.date("Y-m-d").'" AND P2.fecha_ejecucion <= "'.$ultimoDiamessiguiente.'" AND P2.user_id = P.user_id) AS TOTALMES, (SELECT COUNT(*) FROM pendientes P3 WHERE P3.estado LIKE "ejecutando" AND P3.fecha_ejecucion < "'.date("Y-m-d").'" AND P3.user_id = P.user_id) AS TOTALVENCIDA  FROM pendientes P INNER JOIN users U ON P.user_id = U.id WHERE P.estado LIKE "ejecutando" ORDER BY P.user_id ASC';
                $semana_mes = DB::SELECT($query);

                /*FIN PENDIENTES SEMANA Y MES*/

                /*Actividades Aplazadas */
                $query = 'SELECT DISTINCT H.user_id, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) AS NOMBRE, (SELECT COUNT(*) FROM pendientes_historiales H1 WHERE H1.estado LIKE "aplazado" AND YEAR(H1.created_at) = '.date('Y').' AND H1.user_id = H.user_id) AS TOTAL FROM pendientes_historiales H INNER JOIN users U ON H.user_id = U.id WHERE YEAR(H.created_at) = '.date('Y').' AND H.estado LIKE "aplazado"';
                $apalzadas =DB::SELECT($query);
                /*Fin Actividades Aplazadas */
                ob_start();
                ?>
                <script>
                    var ges_act_pen_2 = AmCharts.makeChart( "ges_act_pen_2", {
                                "type": "radar",
                                "theme": "light",
                                "dataProvider": [
                                    <?php foreach($actividades as $actividad){ ?>
                                    {
                                  "direction": "<?=$actividad->NOMBRE?>",
                                  "value": <?=round($actividad->TIEMPO/$actividad->TOTAL*100)?>
                                },
                                    <?php } ?>
                                        ],
                        "valueAxes": [ {
                            "gridType": "circles",
                            "minimum": 0,
                            "autoGridCount": false,
                            "axisAlpha": 0.2,
                            "fillAlpha": 0.05,
                            "fillColor": "#FFFFFF",
                            "gridAlpha": 0.08,
                            "guides": [ {
                                "angle": 225,
                                "fillAlpha": 0.3,
                                "fillColor": "#fff",
                                "tickLength": 0,
                                "toAngle": 315,
                                "toValue": 14,
                                "value": 0,
                                "lineAlpha": 0,

                            },
                                        {
                              "angle": 45,
                              "fillAlpha": 0.3,
                              "fillColor": "#fff",
                              "tickLength": 0,
                              "toAngle": 135,
                              "toValue": 14,
                              "value": 0,
                              "lineAlpha": 0,
                        } ],
                            "position": "left"
                        } ],
                        "startDuration": 1,
                        "graphs": [ {
                            "balloonText": "[[category]]: [[value]] %",
                            "bullet": "round",
                            "fillAlphas": 0.3,
                            "valueField": "value"
                        } ],
                        "categoryField": "direction",
                        "export": {
                            "enabled": false
                        }
                    });

                    var ges_act_pen_1 = AmCharts.makeChart("ges_act_pen_1", {
                        "type": "serial",
                        "theme": "light",
                        "legend": {
                            "horizontalGap": 10,
                            "maxColumns": 1,
                            "position": "right",
                            "useGraphSettings": true,
                            "markerSize": 10
                        },
                        "dataProvider": [
                            <?php
                            foreach($semana_mes as $usuario){ ?>
                                      {
                                          "name": "<?=$usuario->NOMBRE?>",
                                          "mes": <?=$usuario->TOTALMES?>,
                                          "semana": <?=$usuario->TOTALSEMANA?>,
                                          "vencida": <?=$usuario->TOTALVENCIDA?>
                                      },
                        <?php } ?>
                            ],
                        "valueAxes": [{
                            "stackType": "regular",
                            "axisAlpha": 0.3,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "Mes",
                            "type": "column",
                            "color": "#000000",
                            "fillColors": "#31B404",
                            "valueField": "mes"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "Semana",
                            "type": "column",
                            "color": "#000000",
                            "fillColors": "#FFBF00",
                            "valueField": "semana"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "Vencida",
                            "type": "column",
                            "color": "#000000",
                            "fillColors": "#F02525",
                            "valueField": "vencida"
                        }],
                        "categoryField": "name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "position": "left"
                        },
                        "export": {
                            "enabled": true
                         }

                    });

                    var ges_act_pen_3 = AmCharts.makeChart("ges_act_pen_3", {
                            "type": "radar",
                            "theme": "light",
                            "dataProvider": [
                                <?php foreach ($apalzadas as $usuario) { ?>
                                        {
                                          "direction": "<?=$usuario->NOMBRE?>",
                                          "value": <?=$usuario->TOTAL?>
                                        },
                                <?php } ?>
                                ],
                        "valueAxes": [{
                        "gridType": "circles",
                        "minimum": 0,
                        "autoGridCount": false,
                        "axisAlpha": 0.2,
                        "fillAlpha": 0.05,
                        "fillColor": "#FFFFFF",
                        "gridAlpha": 0.08,
                        "guides": [ {
                            "angle": 225,
                            "fillAlpha": 0.3,
                            "fillColor": "#fff",
                            "tickLength": 0,
                            "toAngle": 315,
                            "toValue": 14,
                            "value": 0,
                            "lineAlpha": 0,

                        },
                                    {
                          "angle": 45,
                          "fillAlpha": 0.3,
                          "fillColor": "#fff",
                          "tickLength": 0,
                          "toAngle": 135,
                          "toValue": 14,
                          "value": 0,
                          "lineAlpha": 0,
                    } ],
                        "position": "left"
                    } ],
                    "startDuration": 1,
                    "graphs": [ {
                        "balloonText": "[[category]]: [[value]] Aplazadas",
                        "bullet": "round",
                        "fillAlphas": 0.3,
                        "valueField": "value"
                    } ],
                    "categoryField": "direction",
                    "export": {
                        "enabled": false
                    }
                });
                </script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Cumplimiento de bitacora*/
            case 26:
                $actividades=Actividades::all();
                /*Cumplimiento Llenado */
                $act_llenado=DB::select('SELECT `id` FROM `actividades` WHERE `fuera_tiempo` = "false"');
                $cumplimiento['bitacora']=number_format((count($act_llenado)/count($actividades)*100), 1, '.', ' ');
                /*Fin Cumplimiento Llenado */

                /*Cumplimiento Plan Semanal */
                $plansemanalcumplido=DB::select('SELECT `id` FROM `plan_trabajo_descripcions` WHERE `fuera_tiempo` = "false"');
                $plansemanaltotal=DB::select('SELECT `id` FROM `plan_trabajo_descripcions`');
                $porcentajecumplimientosemanal=number_format(((count($plansemanalcumplido))/(count($plansemanaltotal))*100), 1, '.', ' ');
                /*Fin Cumplimiento Plan Semanal*/

                /*Cumplimiento Presupuesto Mensual */
                $meses_cumplidos=0;
                $meses_con_presupuesto=0;
                for($i=0;$i<=11;$i++){
                    $mes_buscar=$i+1;
                    $ano_actual=date('Y');
                    if($mes_buscar<10){
                        $mes_consulta='0'.$mes_buscar;
                    }else{
                        $mes_consulta=$mes_buscar;
                    }
                    $mes_con_presupuesto=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
                    if(count($mes_con_presupuesto)>0){
                        $meses_con_presupuesto++;

                        $presupuesto_mes=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'"  AND `updated_at` LIKE "'.$ano_actual.'-'.$mes_consulta.'%" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
                        if(count($presupuesto_mes)==0){
                            $meses_cumplidos++;
                        }
                    }
                }
                $porcentajecumplimientopresupuesto=number_format(($meses_cumplidos/$meses_con_presupuesto*100), 1, '.', ' ');
                /*Fin Cumplimiento Presupuesto Mensual*/
                $data['cumplimiento_bitacora'] = $cumplimiento['bitacora'];
                $data['porcentajecumplimientosemanal'] = $porcentajecumplimientosemanal;
                $data['porcentajecumplimientopresupuesto'] = $porcentajecumplimientopresupuesto;
                break;
            /*Cumplimiento Llenado*/
            case 27:
                $actividades=DB::select('SELECT `id` FROM `actividades` WHERE YEAR(`fecha_actividad`) = "'.date('Y').'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');

                $usuarios=User::all();
                /*Cumplimiento Llenado */
                $act_llenado=DB::select('SELECT `id` FROM `actividades` WHERE `fuera_tiempo` = "false" AND YEAR(`fecha_actividad`) = "'.date('Y').'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');

                if((count($actividades)*100)==0){
                    $cumplimiento['bitacora']=number_format(0, 1, '.', ' ');
                }else{
                    $cumplimiento['bitacora']=number_format((count($act_llenado)/count($actividades)*100), 1, '.', ' ');
                }
                    $colors= array(0 => '#1c4597', 1 => '#0079f4', 2 => '#00d6fb', 3 => '#5ac3e1', 4 => '#97f5fd', 5 => '#1c4597', 6 => '#0079f4', 7 => '#00d6fb', 8 => '#5ac3e1', 9 => '#97f5fd');
                    $c=0;
                    $mayor_cumplimiento=0;
                    foreach($usuarios as $usuario){
                        if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
                            $nombres=explode(" ",$usuario->nombres);
                            $act_llenado_usuario=DB::select('SELECT `id` FROM `actividades` WHERE `fuera_tiempo` = "false" AND `user_id` = "'.$usuario->id.'" AND YEAR(`fecha_actividad`) = "'.date('Y').'"');
                            $todas_act_usuario=DB::select('SELECT `id` FROM `actividades` WHERE `user_id` = "'.$usuario->id.'"');
                            if(count($todas_act_usuario)==0){
                                $cumplimiento_x_usr[$usuario->id]['bitacora']=0;
                            }else{
                                $cumplimiento_x_usr[$usuario->id]['bitacora']=number_format((count($act_llenado_usuario)/count($todas_act_usuario)*100), 1, '.', ' ');
                            }

                            $cumplimiento_x_usr[$usuario->id]['nombre']=$nombres[0];
                            $cumplimiento_x_usr[$usuario->id]['color']=$colors[0];
                            $cumplimiento_x_usr[$usuario->id]['foto']='/images/file/clientes/'.$usuario->foto;
                            $c++;

                            if($mayor_cumplimiento<$cumplimiento_x_usr[$usuario->id]['bitacora']){
                                $mayor_cumplimiento=$cumplimiento_x_usr[$usuario->id]['bitacora'];
                            }
                        }
                    }
                    /*Fin Cumplimiento Llenado */

                    /*Cumplimiento Plan Semanal */
                    $colors= array(0 => '#1c4597', 1 => '#0079f4', 2 => '#00d6fb', 3 => '#5ac3e1', 4 => '#97f5fd', 5 => '#1c4597', 6 => '#0079f4', 7 => '#00d6fb', 8 => '#5ac3e1', 9 => '#97f5fd');
                    $c=0;
                    $mayor_cumplimiento1=0;
                    foreach($usuarios as $usuario){
                        if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
                            $nombres=explode(" ",$usuario->nombres);
                            $plansemanalcumplido=DB::select('SELECT `id` FROM `plan_trabajo_descripcions` WHERE `fuera_tiempo` = "false" AND `user_id` = "'.$usuario->id.'" AND YEAR(`fecha`) = "'.date("Y").'"');
                            $plansemanaltotal=DB::select('SELECT `id` FROM `plan_trabajo_descripcions` WHERE `user_id` = "'.$usuario->id.'"');
                            $todas_act_usuario=DB::select('SELECT `id` FROM `actividades` WHERE `user_id` = "'.$usuario->id.'"');
                            if(count($todas_act_usuario)==0){
                                $cumplimiento_x_usr1[$usuario->id]['semanal']=0;
                            }else if(count($plansemanaltotal) > 0){
                                $cumplimiento_x_usr1[$usuario->id]['semanal']=number_format((count($plansemanalcumplido)/count($plansemanaltotal)*100), 1, '.', ' ');
                            }else{
                                $cumplimiento_x_usr1[$usuario->id]['semanal']=0;
                            }

                            $cumplimiento_x_usr1[$usuario->id]['nombre']=$nombres[0];
                            $cumplimiento_x_usr1[$usuario->id]['color']=$colors[1];
                            $cumplimiento_x_usr1[$usuario->id]['foto']=url("/").'/images/file/clientes/'.$usuario->foto;
                            $c++;

                            if($mayor_cumplimiento1<$cumplimiento_x_usr1[$usuario->id]['semanal']){
                                $mayor_cumplimiento1=$cumplimiento_x_usr1[$usuario->id]['semanal'];
                            }
                        }
                    }
                    /*Fin Cumplimiento Plan Semanal*/
                    $colors= array(0 => '#1c4597', 1 => '#0079f4', 2 => '#00d6fb', 3 => '#5ac3e1', 4 => '#97f5fd', 5 => '#1c4597', 6 => '#0079f4', 7 => '#00d6fb', 8 => '#5ac3e1', 9 => '#97f5fd');
                    /*Cumplimiento Presupuesto Mensual */
                    $meses_cumplidos=0;
                    $meses_con_presupuesto=0;
                    for($i=0;$i<=11;$i++){
                        $mes_buscar=$i+1;
                        $ano_actual=date('Y');
                        if($mes_buscar<10){
                            $mes_consulta='0'.$mes_buscar;
                        }else{
                            $mes_consulta=$mes_buscar;
                        }
                        $mes_con_presupuesto=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `anio` = "'.date("Y").'"');
                        if(count($mes_con_presupuesto)>0){
                            $meses_con_presupuesto++;

                            $presupuesto_mes=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'"  AND `updated_at` LIKE "'.$ano_actual.'-'.$mes_consulta.'%"');
                            if(count($presupuesto_mes)==0){
                                $meses_cumplidos++;
                            }
                        }
                        foreach($usuarios as $usu){
                            if(($usu->cargo)=='Ejecutivo Comercial' || ($usu->cargo)=='Partner Solution'){
                                $mes_con_presupuesto=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `user_id`="'.$usu->id.'"');
                                if(count($mes_con_presupuesto)>0){
                                    if(!isset($meses_con_presupuesto1[$usu->id])){
                                        $meses_con_presupuesto1[$usu->id]=1;
                                    }else{
                                        $meses_con_presupuesto1[$usu->id]++;
                                    }
                                    $presupuesto_mes=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `user_id`="'.$usu->id.'" AND `updated_at` LIKE "'.$ano_actual.'-'.$mes_consulta.'%"');
                                    if(count($presupuesto_mes)==0){
                                        if(!isset($meses_cumplidos1[$usu->id])){
                                            $meses_cumplidos1[$usu->id]=1;
                                        }else{
                                            $meses_cumplidos1[$usu->id]++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                if($meses_con_presupuesto == 0){
                    $porcentajecumplimientopresupuesto = number_format(0, 1, '.', ' ');
                }else{
                    $porcentajecumplimientopresupuesto=number_format(($meses_cumplidos/$meses_con_presupuesto*100), 1, '.', ' ');
                }
                    $mayor_cumplimiento2=0;
                    foreach ($usuarios as $usu) {
                        if(($usu->cargo)=='Ejecutivo Comercial' || ($usu->cargo)=='Partner Solution'){
                            $c=0;
                            $nombres=explode(' ', $usu->nombres);
                            $porcentajecumplimientopresupuesto1[$usu->id]['nombre']=$nombres[0];
                            $porcentajecumplimientopresupuesto1[$usu->id]['color']=$colors[2];
                            $porcentajecumplimientopresupuesto1[$usu->id]['foto'] = url("/").'/images/file/clientes/'.$usu->foto;
                            if(isset($meses_con_presupuesto1[$usu->id]) && ($meses_con_presupuesto1[$usu->id])>0){
                                if(isset($meses_cumplidos1[$usu->id])){
                                    $porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']=number_format(($meses_cumplidos1[$usu->id]/$meses_con_presupuesto1[$usu->id]*100), 1, '.', ' ');
                                }else{
                                    $porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']=0;
                                }
                            }else{
                                $porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']=0;
                            }

                            if($mayor_cumplimiento2<$porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']){
                                $mayor_cumplimiento2=$porcentajecumplimientopresupuesto1[$usu->id]['presupuesto'];
                            }
                            $c++;

                        }
                    }
                    /*Fin Cumplimiento Presupuesto Mensual*/
                ob_start();
                ?>
                <script>
                var chart = AmCharts.makeChart("top_x_div1", {
                    "rotate": true,
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": [
                        <?php foreach($cumplimiento_x_usr as $c_x_u){ ?>
			          {
			              "name": "<?php echo $c_x_u['nombre'] ?>",
			              "points": <?php echo $c_x_u['bitacora'] ?>,
			              "color": "<?php echo $c_x_u['color'] ?>",
			              "bullet": "<?php echo $c_x_u['foto'] ?>"
			          },
			        <?php } ?>
                        ],
                    "valueAxes": [{
                        "maximum": <?=$mayor_cumplimiento+($mayor_cumplimiento*0.1)?>,
                        "minimum": 0,
                        "axisAlpha": 0,
                        "dashLength": 4,
                        "position": "left"
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
                        "bulletOffset": 10,
                        "bulletSize": 30,
                        "colorField": "color",
                        "cornerRadiusTop": 8,
                        "customBulletField": "bullet",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0,
                        "type": "column",
                        "valueField": "points"
                    }],
                    "marginTop": 0,
                    "marginRight": 0,
                    "marginLeft": 0,
                    "marginBottom": 0,
                    "autoMargins": true,
                    "categoryField": "name",
                    "categoryAxis": {
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        "inside": true,
                        "tickLength": 0
                    },
                    "export": {
                        "enabled": false
                    }
                });
                var chart = AmCharts.makeChart("top_x_div2", {
                    "rotate": true,
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": [
                        <?php foreach($cumplimiento_x_usr1 as $c_x_u1){ ?>
			          {
			              "name": "<?php echo $c_x_u1['nombre'] ?>",
			              "points": <?php echo $c_x_u1['semanal'] ?>,
			              "color": "<?php echo $c_x_u1['color'] ?>",
			              "bullet": "<?php echo $c_x_u1['foto'] ?>"
			          },
			        <?php } ?>
                    ],
                    "valueAxes": [{
                        "maximum": <?=$mayor_cumplimiento1+($mayor_cumplimiento1*0.1)?>,
                        "minimum": 0,
                        "axisAlpha": 0,
                        "dashLength": 4,
                        "position": "left"
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
                        "bulletOffset": 10,
                        "bulletSize": 30,
                        "colorField": "color",
                        "cornerRadiusTop": 8,
                        "customBulletField": "bullet",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0,
                        "type": "column",
                        "valueField": "points"
                    }],
                    "marginTop": 0,
                    "marginRight": 0,
                    "marginLeft": 0,
                    "marginBottom": 0,
                    "autoMargins": true,
                    "categoryField": "name",
                    "categoryAxis": {
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        "inside": true,
                        "tickLength": 0
                    },
                    "export": {
                        "enabled": false
                    }
                });
                var chart = AmCharts.makeChart("top_x_div3", {
                    "rotate": true,
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": [
                        <?php foreach($porcentajecumplimientopresupuesto1 as $porcentaje){ ?>
                      {
                          "name": "<?php echo $porcentaje['nombre'] ?>",
                          "points": <?php echo $porcentaje['presupuesto'] ?>,
                          "color": "<?php echo $porcentaje['color'] ?>",
                          "bullet": "<?php echo $porcentaje['foto'] ?>"
                      },
                    <?php } ?>
                        ],
                    "valueAxes": [{
                        "maximum": <?=$mayor_cumplimiento2+($mayor_cumplimiento2*0.1)?>,
                        "minimum": 0,
                        "axisAlpha": 0,
                        "dashLength": 4,
                        "position": "left"
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
                        "bulletOffset": 10,
                        "bulletSize": 30,
                        "colorField": "color",
                        "cornerRadiusTop": 8,
                        "customBulletField": "bullet",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0,
                        "type": "column",
                        "valueField": "points"
                    }],
                    "marginTop": 0,
                    "marginRight": 0,
                    "marginLeft": 0,
                    "marginBottom": 0,
                    "autoMargins": true,
                    "categoryField": "name",
                    "categoryAxis": {
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        "inside": true,
                        "tickLength": 0
                    },
                    "export": {
                        "enabled": false
                    }
                });
                </script>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Guardar el dashboard por Evento Click*/
            case 28:
                $dato = new Comparaciones;
                $dato->evento = 0;
                $dato->id_user=Auth::user()->id;
                $dato->save();
                $id = $dato->id;

                /*Negocios Cerrados*/
                $filtro_fecha_inicio = date('Y-01-01');
                $filtro_fecha_fin = date('Y-12-31');
                $filtro_cargo = 'U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Partner Solution" OR U.cargo LIKE "Gerente Comercial" OR U.cargo LIKE "Director General"';
                $query = 'SELECT U.id, U.nombres, U.apellidos, U.foto FROM users U WHERE U.deleted_at IS NULL AND ('.$filtro_cargo.')';
                $usuarios = DB::SELECT($query);
                $i=1;
                $TOVALOR=0;
                $TOOPORTUNIDADES=0;
                foreach($usuarios as $usuario){
                    $nombres=explode(" ",$usuario->nombres);
                    $apellidos=explode(" ",$usuario->apellidos);
                    $valor_usuario=0;
                    $totaloportunidades=0;
                    $mis_oportunidades=DB::select('SELECT DISTINCT H.oportunidad_id FROM historial_oportunidades H WHERE H.oportunidad_id IN (SELECT HO.oportunidad_id FROM historial_oportunidades HO WHERE HO.key = "ciclo_venta" AND HO.value = "90")');
                    foreach ($mis_oportunidades as $m_o) {
                      $fecha_cierre_m_o=DB::select('SELECT * FROM historial_oportunidades HO WHERE HO.key="fecha_oc" AND HO.oportunidad_id="'.$m_o->oportunidad_id.'" ORDER BY HO.id DESC');

                      if((date("Y-m-d", strtotime($fecha_cierre_m_o[0]->value)))>=$filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_cierre_m_o[0]->value)))<=$filtro_fecha_fin){
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                        if(($oportunidad_buscar[0]->value)==$usuario->id){
                          $valoresoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                          $valor_usuario+=(int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $valoresoportunidades[0]->value))));
                          $TOVALOR+=(int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $valoresoportunidades[0]->value))));
                          $totaloportunidades++;
                          $TOOPORTUNIDADES++;
                        }
                      }
                    }
                    $valor_usuario='$ '.number_format($valor_usuario, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $nombres[0].' '.$apellidos[0];
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 1;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $valor_usuario;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 2;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $totaloportunidades;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 3;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $i++;
                }
                //TOTAL
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 1;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOVALOR='$ '.number_format($TOVALOR, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOVALOR;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 2;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOOPORTUNIDADES;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 3;
                $dato->id_comparacion = $id;
                $dato->save();

                $i=1;
                $TOPRROMEDIO=0;
                foreach($usuarios as $usuario){
                    $tiempo=0;
                    $promedioindividual=0;
                    $totaloportunidadindividual=0;
                    $valor_usuario=0;
                    $mis_oportunidades=DB::select('SELECT DISTINCT H.oportunidad_id FROM historial_oportunidades H WHERE H.oportunidad_id IN (SELECT HO.oportunidad_id FROM historial_oportunidades HO WHERE HO.key = "ciclo_venta" AND HO.value = "90")');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades HO WHERE HO.oportunidad_id = '.$m_o->oportunidad_id.' AND HO.key = "responsable" ORDER BY HO.id DESC');
                        if(($oportunidad_buscar[0]->value)==$usuario->id){
                            $historial1=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" ORDER BY `id` DESC');
                            $fecha_fin=0;
                            $fecha_inicio=0;
                            $contador=count($historial1);
                            if($contador>0){
                                if($historial1[0]->value>=90){
                                    $historial2=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_ff" ORDER BY `id` DESC');
                                    if(count($historial2)>0){
                                        $fecha_inicio=strtotime($historial2[0]->value);
                                    }
                                    $historial3=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC');
                                    if(count($historial3)>0){
                                        $fecha_fin=strtotime($historial3[0]->value);
                                    }
                                    if(date('Y-m-d',strtotime($historial3[0]->value)) >= $filtro_fecha_inicio && date('Y-m-d',strtotime($historial3[0]->value)) <= $filtro_fecha_fin){
                                        $totaloportunidadindividual++;
                                    }
                                }
                            }
                            if(date('Y-m-d',strtotime($historial3[0]->value)) >= $filtro_fecha_inicio && date('Y-m-d',strtotime($historial3[0]->value)) <= $filtro_fecha_fin){
                                $tiempo+=$fecha_fin - $fecha_inicio;
                            }
                        }
                    }
                    if($totaloportunidadindividual>0){
                        $dias=$tiempo / 86400;
                        $promedioindividual=($dias/$totaloportunidadindividual)/30;
                        $promedioindividual=number_format($promedioindividual,0,".","");
                    }else{
                        $promedioindividual=0;
                    }
                    $valor_usuario=round($valor_usuario/1000000);

                    $TOPRROMEDIO+=$promedioindividual;
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $promedioindividual;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 4;
                    $dato->id_comparacion = $id;
                    $dato->save();
                    $i++;
                }
                //TOTAL
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOPRROMEDIO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 4;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Oportunidades Perdidas*/
                $query = 'SELECT U.id, U.nombres, U.apellidos, U.foto FROM users U WHERE U.deleted_at IS NULL AND ('.$filtro_cargo.')';
                $usuarios = DB::SELECT($query);
                $h     = 0;
                $i=1;
                $TOVALOR=0;
                $TOCANTIDAD=0;
                foreach ($usuarios as $usuario) {
                    $valor = 0;
                    $total_x_usuario = 0;
                    $nombres = explode(" ", $usuario->nombres);
                    $apellidos = explode(" ", $usuario->apellidos);
                    $mis_oportunidades = DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = ' . $usuario->id . ' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "responsable" ORDER BY `id` DESC');
                        if (($oportunidad_buscar[0]->value) == $usuario->id) {
                            $fecha_cierre_ind_perdida = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "ciclo_venta" ORDER BY `id` DESC LIMIT 0,1');
                            if ((date("Y-m-d", strtotime($fecha_cierre_ind_perdida[0]->updated_at))) >= $filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_cierre_ind_perdida[0]->updated_at))) <= $filtro_fecha_fin) {
                                $h++;
                                $total_x_usuario++;
                                $TOCANTIDAD++;
                                $info = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "val_pesos" ORDER BY `id` DESC');
                                $valor += ((int) (str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $info[0]->value)))));
                                $TOVALOR+=((int) (str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $info[0]->value)))));
                            }
                        }
                    }
                    $valor = "$ ".number_format($valor, 0, ',', '.');

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $nombres[0].' '.$apellidos[0];
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 5;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $valor;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 6;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $total_x_usuario;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 7;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $vector[$i]=$total_x_usuario;
                    $i++;
                }

                //TOTAL
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 5;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOVALOR = "$ ".number_format($TOVALOR, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOVALOR;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 6;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCANTIDAD;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 7;
                $dato->id_comparacion = $id;
                $dato->save();

                $i=1;
                $TOPORCENTAJE=0;
                foreach ($usuarios as $usuario) {
                    $total_oportunidades = 0;
                    $mis_oportunidades = DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = ' . $usuario->id . ' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND (`value` = "0" OR `value` = "90"))');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "responsable" ORDER BY `id` DESC');
                        if (($oportunidad_buscar[0]->value) == $usuario->id) {
                            $fecha_oc_per1 = DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = ' . $m_o->oportunidad_id . ' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');
                            if ((date("Y-m-d", strtotime($fecha_oc_per1[0]->updated_at))) >= $filtro_fecha_inicio && (date("Y-m-d", strtotime($fecha_oc_per1[0]->updated_at))) <= $filtro_fecha_fin) {
                                $total_oportunidades++;
                            }
                        }
                    }
                    if ($total_oportunidades > 0) {
                        $total_porc_perdida = ($vector[$i] * 100) / $total_oportunidades;
                        $total_porc_perdida = round($total_porc_perdida, 1);
                    } else {
                        $total_porc_perdida = 0;
                    }
                    $TOPORCENTAJE+=$total_porc_perdida;

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $total_porc_perdida;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 42;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $i++;
                }
                //TOTAL
                $dato = new ComparacionInformaciones;
                $dato->valor = round($TOPORCENTAJE, 1);
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 42;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Oportunidades Vigentes por Comercial*/
                $query = 'SELECT SUM(V.VALOR) as VALOR_TOTAL_COMERCIAL, (SELECT COUNT(*) FROM view_oportunidades V1 WHERE V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE) AS CANTIDAD, V.RESPONSABLE  FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90 GROUP BY V.IDRESPONSABLE';
                $oprtunidadesVigentes=DB::SELECT($query);
                $i=1;
                $TOCANTIDAD=0;
                $TOVALOR=0;
                foreach($oprtunidadesVigentes as $usuario){
                    $TOVALOR+=$usuario->VALOR_TOTAL_COMERCIAL;
                    $TOCANTIDAD+=$usuario->CANTIDAD;
                    $valor = '$ '.number_format($usuario->VALOR_TOTAL_COMERCIAL, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->RESPONSABLE;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 8;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->CANTIDAD;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 9;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $valor;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 10;
                    $dato->id_comparacion = $id;
                    $dato->save();
                    $i++;
                }
                //TOTAL
                $TOVALOR = '$ '.number_format($TOVALOR, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 8;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCANTIDAD;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 9;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOVALOR;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 10;
                $dato->id_comparacion = $id;
                $dato->save();
                /*Oportunidades Vigentes por Ciclo de Venta*/
                $query = 'SELECT SUM(V.VALOR) as VALOR_TOTAL_CICLO, (SELECT COUNT(*) FROM view_oportunidades V1 WHERE V1.CICLO > 0 AND V1.CICLO < 90 AND V1.CICLO = V.CICLO) AS CANTIDAD, V.CICLO FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90 GROUP BY V.CICLO';
                $oportunidadesVigentes = DB::SELECT($query);
                $i=1;
                $TOCANTIDAD=0;
                $TOVALOR=0;
                foreach($oportunidadesVigentes as $usuario){
                    $TOCANTIDAD+=$usuario->CANTIDAD;
                    $TOVALOR+=$usuario->VALOR_TOTAL_CICLO;
                    $valor = '$ '.number_format($usuario->VALOR_TOTAL_CICLO, 0, ',', '.');
                    $ciclo = 'Ciclo '.$usuario->CICLO.'%';
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $ciclo;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 11;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->CANTIDAD;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 12;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $valor;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 13;
                    $dato->id_comparacion = $id;
                    $dato->save();
                    $i++;
                }
                //TOTAL
                $TOVALOR = '$ '.number_format($TOVALOR, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 11;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCANTIDAD;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 12;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOVALOR;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 13;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Meta de Identificación de Oportunidades*/
                $meta = Metas_oportunidade::find(1);

                $dato = new ComparacionInformaciones;
                $dato->valor = "Corto";
                $dato->orden = 1;
                $dato->id_comparacion_items_columnas = 14;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $meta->corto_dias;
                $dato->orden = 1;
                $dato->id_comparacion_items_columnas = 15;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = number_format($meta->corto_valor, 0, ',', '.');
                $dato->orden = 1;
                $dato->id_comparacion_items_columnas = 16;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = "Mediano";
                $dato->orden = 2;
                $dato->id_comparacion_items_columnas = 14;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $meta->medio_dias;
                $dato->orden = 2;
                $dato->id_comparacion_items_columnas = 15;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = number_format($meta->medio_valor, 0, ',', '.');
                $dato->orden = 2;
                $dato->id_comparacion_items_columnas = 16;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = "Largo";
                $dato->orden = 3;
                $dato->id_comparacion_items_columnas = 14;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $meta->largo_dias;
                $dato->orden = 3;
                $dato->id_comparacion_items_columnas = 15;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = number_format($meta->largo_valor, 0, ',', '.');
                $dato->orden = 3;
                $dato->id_comparacion_items_columnas = 16;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Oportunidades Identificada según plazo*/
                $query = 'SELECT V.IDRESPONSABLE, V.RESPONSABLE, SUM(V.VALOR) AS VALOR, (SELECT IFNULL(SUM(V1.VALOR), 0) FROM view_oportunidades V1 WHERE DATEDIFF(V1.FECHACIERRE,"'.date('Y-m-d').'")>0 AND DATEDIFF(V1.FECHACIERRE,"'.date('Y-m-d').'")<='.$meta->corto_dias.' AND V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE) AS CORTO, (SELECT IFNULL(SUM(V1.VALOR), 0) FROM view_oportunidades V1 WHERE DATEDIFF(V1.FECHACIERRE,"'.date('Y-m-d').'")>'.$meta->corto_dias.' AND DATEDIFF(V1.FECHACIERRE,"'.date('Y-m-d').'")<='.$meta->medio_dias.' AND V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE) AS MEDIANO, (SELECT IFNULL(SUM(V1.VALOR), 0) FROM view_oportunidades V1 WHERE DATEDIFF(V1.FECHACIERRE,"'.date('Y-m-d').'")>'.$meta->medio_dias.' AND V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE) AS LARGO FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90 GROUP BY V.IDRESPONSABLE';
                $oportunidades_plazo = DB::SELECT($query);
                $i=1;
                $TOCORTO=0;
                $TOMEDIANO=0;
                $TOLARGO=0;
                foreach($oportunidades_plazo as $usuario){
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->RESPONSABLE;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 17;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $corto = '$ '.number_format($usuario->CORTO, 0, ',', '.');
                    $TOCORTO+=$usuario->CORTO;
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $corto;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 18;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $mediano = '$ '.number_format($usuario->MEDIANO, 0, ',', '.');
                    $TOMEDIANO+=$usuario->MEDIANO;
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $mediano;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 19;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $largo = '$ '.number_format($usuario->LARGO, 0, ',', '.');
                    $TOLARGO+=$usuario->LARGO;
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $largo;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 20;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $i++;
                }
                //VALOR TOTAL
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 17;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOCORTO = '$ '.number_format($TOCORTO, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCORTO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 18;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOMEDIANO = '$ '.number_format($TOMEDIANO, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOMEDIANO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 19;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOLARGO = '$ '.number_format($TOLARGO, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOLARGO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 20;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Oportunidades Identificadas según probabilidad*/
                $query = 'SELECT V.IDRESPONSABLE, V.RESPONSABLE, SUM(V.VALOR) AS VALOR, (SELECT IFNULL(SUM(V1.VALOR),0) FROM view_oportunidades V1 WHERE V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE AND V1.PROBABILIDAD LIKE "Alta") AS ALTA, (SELECT IFNULL(SUM(V1.VALOR),0) FROM view_oportunidades V1 WHERE V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE AND V1.PROBABILIDAD LIKE "Media") AS MEDIA, (SELECT IFNULL(SUM(V1.VALOR),0) FROM view_oportunidades V1 WHERE V1.CICLO > 0 AND V1.CICLO < 90 AND V1.IDRESPONSABLE = V.IDRESPONSABLE AND V1.PROBABILIDAD LIKE "Baja") AS BAJA FROM view_oportunidades V WHERE V.CICLO > 0 AND V.CICLO < 90 GROUP BY V.IDRESPONSABLE';
                $oportunidades_probabilidad = DB::SELECT($query);
                $i=1;
                $TOALTA=0;
                $TOMEDIA=0;
                $TOBAJA=0;
                foreach($oportunidades_probabilidad as $usuario){

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->RESPONSABLE;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 21;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $TOALTA+=$usuario->ALTA;
                    $alta = '$ '.number_format($usuario->ALTA, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $alta;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 22;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $TOMEDIA+=$usuario->MEDIA;
                    $media = '$ '.number_format($usuario->MEDIA, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $media;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 23;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $TOBAJA+=$usuario->BAJA;
                    $baja = '$ '.number_format($usuario->BAJA, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $baja;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 24;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $i++;
                }
                //VALOR TOTAL
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 21;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOALTA = '$ '.number_format($TOALTA, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOALTA;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 22;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOMEDIA = '$ '.number_format($TOMEDIA, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOMEDIA;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 23;
                $dato->id_comparacion = $id;
                $dato->save();

                $TOBAJA = '$ '.number_format($TOBAJA, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = $TOBAJA;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 24;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Equipos Vendidos*/
                $query = 'SELECT
                            OP.producto,
                            P.name AS PRODUCTO,
                            SUM(OP.cantidad) AS CANTIDAD,
                            (SELECT SUM(CONVERT(REPLACE(OP1.total, ",", ""),UNSIGNED INTEGER)) FROM oportunidad_productos OP1 WHERE OP1.deleted_at IS NULL AND OP1.producto = OP.producto AND OP1.oportunidad_id IN (SELECT V.id FROM view_oportunidades V WHERE V.CICLO = 90 AND YEAR(V.FECHACIERRE) = '.date('Y').')) AS MONTO
                            FROM oportunidad_productos OP INNER JOIN productos P ON OP.producto = P.id WHERE OP.deleted_at IS NULL AND OP.oportunidad_id IN (SELECT V.id FROM view_oportunidades V WHERE V.CICLO = 90 AND YEAR(V.FECHACIERRE) = '.date('Y').') GROUP BY OP.producto';
                $equipos_vendidos = DB::SELECT($query);
                $i=1;
                $TOCANTIDAD=0;
                $TOMONTO=0;
                foreach($equipos_vendidos as $equipo){
                    $TOCANTIDAD+=$equipo->CANTIDAD;
                    $TOMONTO+=$equipo->MONTO;

                    $monto = '$ '.number_format($equipo->MONTO, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $equipo->PRODUCTO;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 25;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $equipo->CANTIDAD;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 26;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $monto;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 27;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $i++;
                }
                //VALOR TOTAL
                $TOMONTO = '$ '.number_format($TOMONTO, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 25;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCANTIDAD;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 26;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOMONTO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 27;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Equipos Vendidos por Pais*/
                $query = 'SELECT V.pais, COUNT(*) AS CANTIDAD, SUM(V.VALOR) AS MONTO FROM view_oportunidades V WHERE V.CICLO = 90 AND YEAR(V.FECHACIERRE) = '.date('Y').' GROUP BY V.pais';
                $equipos_pais = DB::SELECT($query);

                $i=1;
                $TOCANTIDAD=0;
                $TOMONTO=0;
                foreach($equipos_pais as $pais){
                    $TOCANTIDAD+=$pais->CANTIDAD;
                    $TOMONTO+=$pais->MONTO;

                    $monto = '$ '.number_format($pais->MONTO, 0, ',', '.');
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $pais->pais;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 28;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $pais->CANTIDAD;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 29;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $monto;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 30;
                    $dato->id_comparacion = $id;
                    $dato->save();
                    $i++;
                }
                //VALOR TOTAL
                $TOMONTO = '$ '.number_format($TOMONTO, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 28;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCANTIDAD;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 29;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOMONTO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 30;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Equipos Vendidos por Meses*/
                $query = 'SELECT MONTH(V.FECHACIERRE) AS MES, COUNT(*) AS CANTIDAD, SUM(V.VALOR) AS MONTO FROM view_oportunidades V WHERE V.CICLO = 90 AND YEAR(V.FECHACIERRE) = '.date('Y').' GROUP BY MONTH(V.FECHACIERRE) ORDER BY MES ASC';
                $meses_ventas = DB::SELECT($query);
                $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $TOCANTIDAD=0;
                $TOMONTO=0;
                for($i=1;$i<=12;$i++){
                    $CANTIDAD = 0;
                    $MONTO = 0;
                    for($y=0;$y<count($meses_ventas);$y++){
                        if($meses_ventas[$y]->MES == $i){
                            $CANTIDAD = $meses_ventas[$y]->CANTIDAD;
                            $MONTO = '$ '.number_format($meses_ventas[$y]->MONTO, 0, ',', '.');
                            $TOMONTO+=$meses_ventas[$y]->MONTO;
                        }
                    }
                    $TOCANTIDAD+=$CANTIDAD;
                    $dato = new ComparacionInformaciones;
                    $dato->valor = $meses[$i];
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 31;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $CANTIDAD;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 32;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $MONTO;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 33;
                    $dato->id_comparacion = $id;
                    $dato->save();
                }
                //VALOR TOTAL
                $TOMONTO = '$ '.number_format($TOMONTO, 0, ',', '.');
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 31;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCANTIDAD;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 32;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOMONTO;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 33;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Clientes y Contactos*/
                $query = 'SELECT E.responsable, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1)," ",SUBSTRING_INDEX(U.apellidos, " ", 1)) AS FUNCIONARIO, COUNT(*) AS EMPRESAS, (SELECT COUNT(*) FROM clientes C WHERE C.deleted_at IS NULL AND C.user_id = E.responsable) AS CLIENTES  FROM empresas E INNER JOIN users U ON E.responsable = U.id WHERE E.deleted_at IS NULL GROUP BY E.responsable';
                $clientes_contactos = DB::SELECT($query);
                $i=1;
                $TOEMPRESAS=0;
                $TOCLIENTES=0;
                foreach($clientes_contactos as $usuario){
                    $TOEMPRESAS+=$usuario->EMPRESAS;
                    $TOCLIENTES+=$usuario->CLIENTES;

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->FUNCIONARIO;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 34;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->EMPRESAS;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 35;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $usuario->CLIENTES;
                    $dato->orden = $i;
                    $dato->id_comparacion_items_columnas = 36;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $i++;
                }
                $dato = new ComparacionInformaciones;
                $dato->valor = "TOTAL";
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 34;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOEMPRESAS;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 35;
                $dato->id_comparacion = $id;
                $dato->save();

                $dato = new ComparacionInformaciones;
                $dato->valor = $TOCLIENTES;
                $dato->orden = $i;
                $dato->id_comparacion_items_columnas = 36;
                $dato->id_comparacion = $id;
                $dato->save();

                /*Costo Venta*/
                $usuarios = User::where('cargo','Ejecutivo Comercial')->orWhere('cargo','Partner Solution')->orderBy('id', 'asc')->get();
                $xz=1;
                foreach($usuarios as $usuario){
                    $nombre = explode(' ',$usuario->nombres);
                    $apellido = explode(' ',$usuario->apellidos);
                    $query = 'SELECT IF(SUM(VALOR)>0,SUM(VALOR),0) AS TOTAL FROM view_oportunidades WHERE CICLO LIKE "90" AND FECHACIERRE >= "'.date('Y-01-01').'" AND FECHACIERRE <= "'.date('Y-12-31').'" AND IDRESPONSABLE = '.$usuario->id;
                    $vendidas = DB::SELECT($query);

                    $valor_total_gastos=0;
                    $limite=intval(date('m'));
                    for($x=1;$x<=$limite;$x++){
                        $mes=$x<10?'0'.$x:$x;
                        /*Valor Real*/
                        $query = 'SELECT (A.valor_alimentacion + A.valor_transporte_interno + A.valor_transporte_intermunicipal + A.valor_tiquete_aereo + A.valor_papeleria + A.valor_invitacion_cliente + A.valor_alquiler_vehiculo + A.valor_gasolina_pasaje + A.valor_hotel + A.valor_otros + A.valor_salariopropio + A.valor_salariotercero) AS TOTAL FROM ajuste_gastos A WHERE A.tipo LIKE "valorreal" AND A.anio LIKE "'.date('Y').'" AND A.mes LIKE "'.$mes.'" AND A.user_id = '.$usuario->id;
                        $valorReal = DB::SELECT($query);
                        if(isset($valorReal[0]->TOTAL) && $valorReal[0]->TOTAL > 0){
                            $valor_total_gastos+=$valorReal[0]->TOTAL;
                        }else{
                            /*Gastos*/
                            $query = 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(H.valor,0), ".", ""), ",", "" ), UNSIGNED INTEGER)) + ';
                            $campos = array('alimentacion','transportes_internos','transportes_intermunicipales','tiquete_aereo','papeleria','invitacion_cliente','alquiler_vehiculo','gasolina_pasajes','hotel','otros');
                            for($i=0;$i<=9;$i++){
                                $query.= 'SUM(CONVERT( REPLACE(REPLACE (IFNULL(A.'.$campos[$i].',0), ".", ""), ",", "" ), UNSIGNED INTEGER))';
                                $query.= $i<9?' + ':' AS TOTAL ';
                            }
                            $data['query'] = 'SELECT '.$query.' FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE YEAR(A.fecha_actividad) = '.date('Y').' AND MONTH(A.fecha_actividad) = '.$x.' AND A.user_id = '.$usuario->id;
                            $actividades_gastos = DB::select($data['query']);
                            $valor_total_gastos+=intval($actividades_gastos[0]->TOTAL);
                        }
                    }
                    // validar 0
                    if ($vendidas[0]->TOTAL == 0) {
                        $porcentaje_gastos = 100;
                    }else if($valor_total_gastos === 0){
                        $porcentaje_gastos = 0;
                    }else{
                        $porcentaje_gastos=($valor_total_gastos/$vendidas[0]->TOTAL)*100;
                    }
                    $data['porcentaje_gastos'] = $porcentaje_gastos==100?number_format($porcentaje_gastos, 0, '.', ' '):number_format($porcentaje_gastos, 2, '.', ' ');

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $nombre[0].' '.$apellido[0];
                    $dato->orden = $xz;
                    $dato->id_comparacion_items_columnas = 37;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $data['porcentaje_gastos'].'%';
                    $dato->orden = $xz;
                    $dato->id_comparacion_items_columnas = 38;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $xz++;
                }

                /*Uso del tiempo*/
                $xz=1;
                $query = 'SELECT DISTINCT A.user_id, U.nombres, U.apellidos, CONCAT("/images/file/clientes/",U.foto) as FOTO FROM actividades A INNER JOIN users U ON A.user_id = U.id WHERE YEAR(A.fecha_actividad) = "'.date('Y').'" ORDER BY A.user_id ASC';
                $usuarios = DB::SELECT($query);
                ob_start();
                foreach($usuarios as $usuario){
                    $nombre=explode(' ',$usuario->nombres);
                    $apellido=explode(' ',$usuario->apellidos);
                    $query = 'SELECT SUM(((CONVERT(substring(A.tiempo_fin,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_fin,4,2),UNSIGNED INTEGER))/60) - ((CONVERT(substring(A.tiempo_inicio,1,2),UNSIGNED INTEGER)) + (CONVERT(substring(A.tiempo_inicio,4,2),UNSIGNED INTEGER))/60)) AS Horas, TA.tipo, TA.concepto AS SUBTIPOCONCEPTO FROM actividades A INNER JOIN subtipo_actividads TA ON A.subtipo = TA.id WHERE A.user_id = '.$usuario->user_id.' AND A.subtipo IS NOT NULL AND YEAR(A.fecha_actividad) = "'.date('Y').'" GROUP BY A.subtipo ORDER BY Horas DESC LIMIT 0,5';
                    $uso_tiempo = DB::SELECT($query);
                    $suma=0;
                    foreach($uso_tiempo as $uso){
                        $suma+=intval($uso->Horas);
                    }

                    $porcentaje=$suma>0?round($uso_tiempo[0]->Horas/$suma*100):0;


                    $dato = new ComparacionInformaciones;
                    $dato->valor = $nombre[0].' '.$apellido[0];
                    $dato->orden = $xz;
                    $dato->id_comparacion_items_columnas = 39;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $porcentaje.'%';
                    $dato->orden = $xz;
                    $dato->id_comparacion_items_columnas = 40;
                    $dato->id_comparacion = $id;
                    $dato->save();

                    $dato = new ComparacionInformaciones;
                    $dato->valor = $uso_tiempo[0]->SUBTIPOCONCEPTO;
                    $dato->orden = $xz;
                    $dato->id_comparacion_items_columnas = 41;
                    $dato->id_comparacion = $id;
                    $dato->save();
                    $xz++;
                }
                $data['msj'] = 'Guardado Satisfactoriamente!';
                break;
            /*Modal de Ventas Filtrado*/
            case 29:
				if($request->fecha_inicio == '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = date('Y-01-01');
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio != '' && $request->fecha_fin == ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = date('Y-12-31');
                }else if($request->fecha_inicio == '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = '1990-01-01';
                    $filtro_fecha_fin = $request->fecha_fin;
                }else if($request->fecha_inicio != '' && $request->fecha_fin != ''){
                    $filtro_fecha_inicio = $request->fecha_inicio;
                    $filtro_fecha_fin = $request->fecha_fin;
                }
				$vendidas_oportunidades = DB::SELECT('SELECT O.id AS oportunidad_id FROM view_oportunidades O WHERE O.CICLO = 90 AND O.FECHACIERRE >= "'.$filtro_fecha_inicio.'" AND O.FECHACIERRE <= "'.$filtro_fecha_fin.'"');
                foreach($vendidas_oportunidades as $v_o){
                    $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    /*paises*/
                    $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                    $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
                    if(isset($pais_vendidas_oportunidades[0]->pais) && (date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                        $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));

                        $valor_total=round(((int)$cantidad)/1000000);
                        $valor_total=(int)$valor_total;

                        if(isset($pais_vendida[$pais_vendidas_oportunidades[0]->pais])){
                            $pais_vendida[$pais_vendidas_oportunidades[0]->pais]+=$valor_total;
                        }else{
                           $pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$valor_total;
                           $nombre_pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$pais_vendidas_oportunidades[0]->pais;
                        }
                    }
                    /*Fin paises*/

                    if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        /*Equipos vendidos*/
                        $productos_vendidas_oportunidades=DB::select('SELECT * FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `deleted_at` IS NULL');

                        foreach ($productos_vendidas_oportunidades as $pvo) {

                            if(isset($productos_referencia[$pvo->referencia])){
                                $productos_referencia[$pvo->referencia]+=$pvo->cantidad;
                            }else{
                                $productos_referencia[$pvo->referencia]=$pvo->cantidad;
                                $nombre_productos_referencia[$pvo->referencia]=$pvo->referencia;
                                $productos_buscar=DB::select('SELECT `name` FROM `productos` WHERE `id` = "'.$pvo->producto.'" ');
                                $referencia_buscar=DB::select('SELECT `referencia` FROM `producto_referencias` WHERE `id` = "'.$pvo->referencia.'" ');
                                $nombrecompleto_productos_referencia[$pvo->referencia]=$productos_buscar[0]->name.' - '.$referencia_buscar[0]->referencia;
                            }
                        }
                        /*Fin Equipos vendidos*/
                    }
                }
                if(isset($productos_referencia)){
       		       arsort($productos_referencia);
                }
                ob_start();
                ?>
                <script type="text/javascript" src="/components/amchart/js/dark.js"></script>
                <?php $sum=0; ?>
                <!-- amCharts javascript code -->
                <script type="text/javascript" src="/components/amchart/js/light.js"></script>
                <!-- amCharts javascript code -->
                <script type="text/javascript">
                    AmCharts.makeChart("modalventa2",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "columnSpacing": 3,
                            "columnWidth": 0.69,
                            "autoMarginOffset": 40,
                            "marginRight": 70,
                            "marginTop": 70,
                            "startDuration": 1,
                            "startEffect": "easeOutSine",
                            "backgroundColor": "#001A4A",
                            "color": "#E7E7E7",
                            "fontSize": 12,
                            "theme": "light",
                            "categoryAxis": {
                                "gridPosition": "start"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonColor": "#00BFFF",
                                    "balloonText": "[[title]]  [[category]]:[[value]]",
                                    "color": "#FFFFFF",
                                    "fillAlphas": 0.9,
                                    "id": "AmGraph-1",
                                    "title": "",
                                    "type": "column",
                                    "valueField": "column-1"
                                },
                                {
                                    "animationPlayed": true,
                                    "balloonColor": "undefined",
                                    "balloonText": "[[title]] of [[category]]:[[value]]",
                                    "behindColumns": true,
                                    "fillAlphas": 0.9,
                                    "id": "AmGraph-2",
                                    "title": "graph 2",
                                    "type": "column",
                                    "valueField": "column-2"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "axisColor": "#0000FF",
                                    "axisThickness": 2,
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {},
                            "titles": [],
                            "dataProvider": [
                            <?php
                                if(isset($nombre_pais_vendida)){
                                foreach($nombre_pais_vendida as $nombre){ ?>
                                {
                                    "category": "<?=$nombre?>",
                                    "column-1": "<?=$pais_vendida[$nombre]?>"
                                },
                            <?php
                                } }
                            ?>

                            ]
                        }
                    );
                </script>
                <div class="col-md-12 mt-5" style="background-color: #001A4A;">
                    <a style="color: #fff; font-size: 20px;"><center>Ventas por paises</center></a>
                    <div id="modalventa2" style="width: 100%; height: 400px; background-color: #001A4A;" ></div>
                </div>
                <script type="text/javascript">
                    AmCharts.makeChart("modalventa3",
                        {
                            "type": "serial",
                            "categoryField": "category",
                            "rotate": true,
                            "autoMarginOffset": 40,
                            "maxSelectedSeries": <?php if(isset($nombre_productos_referencia)){ echo count($nombre_productos_referencia); }else{ echo 0; } ?>,
                            "marginRight": 60,
                            "marginTop": 60,
                            "startDuration": 1,
                            "startEffect": "easeOutSine",
                            "backgroundColor": "#02141A",
                            "borderColor": "#FFFFFF",
                            "color": "#00BFFF",
                            "fontSize": 13,
                            "theme": "dark",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisColor": "#00BFFF",
                                "fillColor": "#FF8000"
                            },
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonText": "[[title]] [[category]]:[[value]]",
                                    "fillAlphas": 1,
                                    "id": "AmGraph-1",
                                    "labelText": "",
                                    "title": "Equipo: ",
                                    "type": "column",
                                    "valueField": "column-1"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "axisColor": "#00BFFF",
                                    "fillColor": "#00BFFF",
                                    "gridColor": "#00BFFF",
                                    "title": ""
                                }
                            ],
                            "allLabels": [],
                            "balloon": {},
                            "titles": [],
                            "dataProvider": [
                            <?php
                                if(isset($productos_referencia)){
                                foreach($productos_referencia as $clave=>$valor){ ?>
                                {
                                    "category": "<?=$nombrecompleto_productos_referencia[$clave]?>",
                                    "column-1": "<?=$productos_referencia[$clave]?>"
                                },
                            <?php
                                }
                                }
                            ?>

                            ]
                        }
                    );
                </script>
                <div class="col-md-12 mt-5" style="background-color: #02141A;">
                    <a style="color: #fff; font-size: 20px;"><center>Equipos vendidos</center></a>
                    <div id="modalventa3" style="width: 100%; <?php if(isset($nombre_productos_referencia)){ ?> height: calc(63.25px*<?=count($nombre_productos_referencia)?>); <?php }else{ ?> height:230px; <?php } ?> background-color: #02141A;" ></div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
    	}
    	return \Response::json(['data' => $data]);
    }
    protected function numero($n){
        $n = $n/1000000;
        $n = number_format($n, 0, '.', '');
        return $n;
    }
}
