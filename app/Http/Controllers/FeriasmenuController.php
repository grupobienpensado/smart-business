<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\Paises;
use App\Estados;
use App\Ciudades;
use App\FeriasPatrocinadore;
use App\Proveedore;
use App\Ferias_multimedias;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
date_default_timezone_set("America/Bogota");

class FeriasmenuController extends Controller
{
    public function menu(){
        $dt     = \Carbon\Carbon::now();
        $fecha  = $dt->toDateTimeString();
        $last = Feria::where('estado', 'Activo')
                    ->where('fecha_inicio', '<=' ,$fecha)
                    ->where('fecha_fin', '>=' ,$fecha)->get()->pop();
        $proxima = Feria::where('estado', 'Activo')->where('fecha_inicio', '>=' ,$fecha)->get()->pop();
        $futuras = Feria::where('estado', 'Posible')->where('fecha_inicio', '>=' ,$fecha)->get();
        $exponente = Feria::where('estado', 'Activo')->where('tipo', 'Exponente')->get();
        $visitante = Feria::where('estado', 'Activo')->where('tipo', 'Visitante')->get();
        $noasistidas = Feria::where('estado', 'Posible')->where('fecha_inicio', '<' ,$fecha)->get();
        $patrocinadores = FeriasPatrocinadore::all();
        $proveedores = Proveedore::all();
        $data['ferias_proveedores'] = count($proveedores);
        $data['ferias_patrocinadores'] = count($patrocinadores);
        $data['ferias_noasistida'] = count($noasistidas);
        $data['ferias_visitante'] = count($visitante);
        $data['ferias_exponente'] = count($exponente);
        $data['ferias_futuras'] = count($futuras);
        $prox['no_hay'] = "No hay una feria proxima!";
        if(!empty($proxima->id)){
            $meses=['','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
            $ubicacion = isset($last->ubicacion)? explode('%%',$last->ubicacion):'';
            $estado = isset($last->ubicacion[1])? Estados::find($ubicacion[1]):'';
            $mes=intval(date('m',strtotime($proxima->fecha_inicio)));
            $estado_name = isset($estado->name)? $estado->name:'';
            $prox['nombre_lugar'] = 'Proxima feria '.$proxima->nombre.', '.$estado_name;
            $prox['fecha'] = date('d',strtotime($proxima->fecha_inicio)).'/'.$meses[$mes].'/'.intval(date('Y',strtotime($proxima->fecha_inicio)));
        }
        /*Permiso para acceder*/
        $query = 'SELECT * FROM permiso_menu PM WHERE PM.id_menu = 36 AND (SELECT C.cargo FROM permiso_cargos C WHERE C.id = PM.id_cargo) = "'.Auth::user()->cargo.'" ORDER BY PM.id DESC LIMIT 0,1';
        $permiso_acceso = DB::SELECT($query);
        $data["permiso"]="No";
        if($permiso_acceso[0]->permiso == "Si"){
            $data["permiso"]="Si";
        }
        /*Permiso para crear una feria*/
        $modulo=21;
        $data['permiso_crear']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_crear']="Si";
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        if(!empty($last->id)){
            $fecha_inicio=$last->fecha_inicio;
            $fecha_fin=$last->fecha_fin;
            $ubicacion = explode('%%',$last->ubicacion);
            $pais = Paises::find($ubicacion[0]);
            $estado = Estados::find($ubicacion[1]);
            $ubi=$estado->name.', '.$pais->name;
            $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                $mes=intval(date('m',strtotime($fecha_inicio)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }else{
                $mes1=intval(date('m',strtotime($fecha_inicio)));
                $mes2=intval(date('m',strtotime($fecha_fin)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }
            if($data["permiso"]=="Si"){
                return view('ferias.menu',['feria' => $last, 'fecha_ubicacion' => $fecha_ubicacion, 'proxima' => $prox, 'data' => $data]);
            }else{
                return view('oportunidades.nopermiso');
            }
        }else{
            if($data["permiso"]=="Si"){
                return view('ferias.menu',['proxima' => $prox, 'data' => $data]);
            }else{
                return view('oportunidades.nopermiso');
            }
        }
    }

    public function accion(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*activar una feria (Iconos VISITANTE ó EXPONENTE)*/
            case 0:
                ob_start(); ?>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8 botones-activacion">
       <a href="#" onclick="activar_la_feria(<?=$request->id?>,'Visitante')" class="icon-button twitter2" title="Visitante"><i class="fa fa-male"></i><span></span></a>
       <a href="#" onclick="activar_la_feria(<?=$request->id?>,'exponente')" class="icon-button facebook2" title="Exponente"><i class="fa fa-users"></i><span></span></a>
    </div>
    <div class="col-md-2"></div>
</div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Formulario para activar feria como exponente*/
            case 1:
                $feria = Feria::find($request->id);
                $categorias= DB::select('SELECT DISTINCT(`categoria`) FROM `productos`');
                $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" ORDER BY `id` ASC');
                $responsables_essi=DB::select('SELECT * FROM `users` WHERE `cargo`="Partner Solution" OR `cargo`="Ejecutivo Comercial" OR `cargo`="Gerente Comercial" ORDER BY `id`');
                ob_start(); ?>
<div class="row">
    <div class="col-md-12 text-center">
        <p class="h3">Datos como exponente / feria <em><?=$feria->nombre?></em></p>
    </div>
</div>
<form id="formulario_exponente" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?=$request->_token?>">
    <input type="hidden" name="id_feria" value="<?=$request->id?>">
    <div class="row">
        <div class="col-sm-12 col-md-12 text-center">
            <div class="dropzone" style="min-width: 1024px !important; max-width: 1024px !important;min-height: 184px !important;max-height: 184px !important;margin-top: 2%;" data-ajax="false" data-originalsave="true">
                <input type="file" name="baner" accept="image/gif, image/jpeg, image/png" required="required">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Equipos Exhibidos</h3>
            <div class="row">
                <div class="col-md-12">
                   <div class="row" style="margin-left: 20%; margin-right: 20%;">
                       <div class="col-md-12">
                           <h6>Categoria</h6>
                            <select id="tipo" class="form-control" style="height: 37px;" onchange="formulario_exponente_tipo(this.value)">
                                <option value="" disabled selected>Seleccione una categoria</option>
                                <?php foreach($categorias as $categoria){ ?>
                                <option value="<?= $categoria->categoria ?>"><?= $categoria->categoria ?></option>
                                <?php } ?>
                            </select><br>
                            <h6>Maquina</h6>
                            <select id="maquina" class="form-control" style="height: 37px;" onchange="formulario_exponente_maquina(this.value)">
                                <option value="" disabled selected>Seleccione una categoria</option>
                            </select><br>
                            <h6>Referencia</h6>
                            <select id="referencia" class="form-control" style="height: 37px;">
                                <option value="" disabled selected>Seleccione una categoria</option>
                            </select>
                       </div>
                   </div>
                </div>
                <div class="col-md-12 text-center mt-3">
                    <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Agregar Maquina" onclick="formulario_exponente_agregar_maquina()">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar maquina
                    </a>
                    <div class="alert alert-danger" id="mensaje_maquina_error" role="alert" style="display: none">
                      <a class="text-danger" style="font-size: 2rem;"><i class="fa fa-times"></i></a> Debe seleccionar una categoria, una maquina y una referencia
                    </div>
                    <div class="alert alert-success" id="mensaje_maquina_exito" role="alert" style="display: none">
                      <a class="text-success" style="font-size: 2rem;"><i class="fa fa-check-circle"></i></a> Agregado correctamente
                    </div>
                    <div class="alert alert-warning" id="mensaje_maquina_repetida" role="alert" style="display: none">
                      <a class="text-warning" style="font-size: 2rem;"><i class="fa fa-exclamation-circle"></i></a> Maquina ya existe
                    </div>
                    <div class="alert alert-success" id="mensaje_maquina_eliminado" role="alert" style="display: none">
                      <a class="text-success" style="font-size: 2rem;"><i class="fa fa-check-circle"></i></a> Eliminado correctamente
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="hidden" value="" name="referencias" id="referencias_agregadas">
                    <div class="row cont-imagenes">

                    </div>
                </div>
            </div>
            <hr size="2px" color="black" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
              <a class="btn btn-primary" data-toggle="collapse" href="#Invitados_Responsable_Atuendos" role="button" aria-expanded="false" aria-controls="Invitados_Responsable_Atuendos">
                Invitados, Responsable y Atuendos
              </a>
              <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#Objetivos_Alcances" aria-expanded="false" aria-controls="Objetivos_Alcances">
                Objetivos y Alcances
              </button>
              <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#Estrategias" aria-expanded="false" aria-controls="Estrategias">
               Estrategias
              </button>
            </p>
            <div class="collapse mt-3" id="Invitados_Responsable_Atuendos">
              <div class="card card-body">
                 <div class="row">
                     <div class="col-md-12">
                        <h6>Cantidad de personas ESSI en feria</h6>
                        <input type="hidden" class="form-control" id="cantidad_personas" name="cantidad_personas" required>
                        <div class="row listado_funcionarios_invitados mb-5">

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-header">
                                    <h6>Invitados</h6>
                                    <em>Seleccione los funcionarios invitados</em>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach($usuarios as $user){
                               $nombre=explode(' ',$user->nombres);
                               $apellido=explode(' ',$user->apellidos);
                               if($user->foto == ""){
                                   $foto='essi_admon.jpg';
                               }else{
                                   $foto=$user->foto;
                               }
                            ?>
                            <div class="col-md-2 activar-funcionario" title="Agregar como invitado" id="af_<?= $user->id ?>">
                                <img src="/images/file/clientes/<?=$foto?>" class="img-circle foto-inv" alt="Avatar"/>
                                <h5><?=$nombre[0].' '.$apellido[0]?></h5>
                            </div>
                            <?php } ?>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12">
                        <h6>Responsable ESSI</h6>
                        <select class="form-control" name="responsable" style="height:37px;">
                            <option value="">Seleccione un responsable</option>
                            <?php foreach($responsables_essi as $r){ ?>
                            <option value="<?=$r->id?>"><?=$r->nombres.' '.$r->apellidos?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Atuento de asistentes</h6>
                        <textarea class="form-control" name="atuendo" rows="8" required></textarea>
                    </div>
                </div>
              </div>
            </div>
            <div class="collapse mt-3" id="Objetivos_Alcances">
              <div class="card card-body">
                 <div class="row">
                    <div class="col-md-12">
                        <h6>Objetivos</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-objetivo" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Objetivo"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <p class="h5"><strong>Item #</strong></p>
                            </div>
                            <div class="col-md-11">
                                <p class="h5"><strong>Objetivo</strong></p>
                            </div>
                        </div>
                        <div class="listado-objetivos">
                        <div class="row n_obj" id="n_obj0">
                            <div class="col-md-1 num-objetivo">
                                <p class="h5"><strong>1</strong></p>
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="objetivos[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                 <div class="row">
                   <div class="col-md-12">
                        <h6>Alcances</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-alcance" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Alcance"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <p class="h5"><strong>Item #</strong></p>
                            </div>
                            <div class="col-md-11">
                                <p class="h5"><strong>Alcance</strong></p>
                            </div>
                        </div>
                        <div class="listado-alcances">
                        <div class="row n_alc" id="n_alc0">
                            <div class="col-md-1 num-alcance">
                                <p class="h5"><strong>1</strong></p>
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="alcances[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                 </div>
              </div>
            </div>
            <div class="collapse mt-3" id="Estrategias">
              <div class="card card-body">
                 <div class="row">
                    <div class="col-md-12">
                        <h6>Estrategias</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-estrategia" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Estrategia"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <p class="h5"><strong>Item #</strong></p>
                            </div>
                            <div class="col-md-11">
                                <p class="h5"><strong>Estrategia</strong></p>
                            </div>
                        </div>
                        <div class="listado-estrategias">
                        <div class="row n_est" id="n_est0">
                            <div class="col-md-1 num-estrategia">
                                <p class="h5"><strong>1</strong></p>
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="estrategia[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="row mt-5" id="btn_save_exponente">
        <div class="col-md-12 text-center">
            <p class="text-light"><a class="text-light btn btn-success" onclick="guardar_exponente()"><i class="fa fa-save"></i> Guardar</a></p>
        </div>
        <div class="col-md-12">
            <div class="alert alert-warning" id="msj_falta_campos_exponente" role="alert" style="display: none">
              <a class="text-warning" style="font-size: 2rem;"><i class="fa fa-exclamation-circle"></i></a> Faltan campos por completar
            </div>
        </div>
    </div>
    <div class="row mt-5" id="msj_save_exponente" style="display: none;">
        <div class="col-md-12 text-center">
            <p class="h4"><strong>Esta Seguro?</strong></p>
            <p class="h5">Realmente desea guardarlo como exponente</p>
        </div>
        <div class="col-md-12 text-center">
            <p class="text-light">
                <a class="text-light btn btn-success" onclick="guardar_exponente_desicion('si')"><i class="fa fa-save"></i> Si</a>
                <a class="text-light btn btn-danger" onclick="guardar_exponente_desicion('no')"><i class="fa fa-times"></i> No</a>
            </p>
        </div>
    </div>
</form>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Lista de galeria de la vista cargargaleria.blade.php*/
            case 2:
                $datos = Ferias_multimedias::where('id_feria', $request->id)->where('tipo', 'Archivo')->get();
                $data['html']='';
                foreach($datos as $dato){
                    $nombre_archivo=$dato->name;
                    $antiguo=$dato->name;
                    $extension=explode('.',$dato->contenido);
                    if($extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                        $icono="image.png";
                        $etiqueta_a='<a data-gallery href="/storage/ferias/multimedia/'.$dato->contenido.'"><img src="/images/iconosferias/'.$icono.'" style="background: none;"></a>';
                    }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                        $icono="video-file.png";
                        $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
                    }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                        $icono="pdf.png";
                        $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
                    }else{
                        $icono="";
                        $etiqueta_a="";
                    }
                    $data['html'].='<div class="col-md-2">
                    <div class="row">
                        <div class="col-md-10">
                            '.$etiqueta_a.'
                            <h6 style="color: cornflowerblue;">'.$nombre_archivo.'</h6>
                        </div>
                        <div class="col-md-1">
                            <div class="row">
                                <div class="col-md-12" style="top: 10px; cursor:pointer;">
                                    <img src="/images/iconosferias/edit.png" title="Renombrar" onclick="rename('.$dato->id.',\''.$antiguo.'\')">
                                </div>
                                <div class="col-md-12" style="margin-top: 25px; cursor:pointer;">
                                    <img src="/images/iconosferias/delete.png" title="Eliminar" onclick="deletefile('.$dato->id.')">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                </div>';
                }
                break;
        }
        return \Response::json(['data'=>$data]);
    }

    public function consultarlistadoferiasfuturas(Request $request){
        /*Permiso para crear una feria*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }

        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

        $dt     = \Carbon\Carbon::now();
        $fecha  = $dt->toDateTimeString();
        $ferias = Feria::where('estado', 'Posible')->where('fecha_inicio', '>=' ,$fecha)->get();
        ob_start(); ?>
            <div class="row">
                <div class="col-md-1">
                    <div class="punto-futuras">
                        <img src="<?=$request->url?>/images/iconosferias/menu/futuras_icono.png">
                    </div>
                </div>
                <div class="col-md-11 text-center linea-futuras bordes-redondos-derecha">
                    <h3 class="mt-3">Ferias Futuras</h3>
                </div>
            </div>
            <?php if(count($ferias)>0){ ?>
            <div class="row area-busqueda mt-3">
                <div class="col-md-12 text-center mt-3 buscar-futura">
                    <input type="text" class="form-control" id="buscar_futura" placeholder="&#xf002;">
                </div>
            </div>
            <?php }
            $cont=1;
            foreach($ferias as $feria){
                $fecha_inicio=$feria->fecha_inicio;
                $fecha_fin=$feria->fecha_fin;
                $ubicacion = explode('%%',$feria->ubicacion);
                $pais = Paises::find($ubicacion[0]);
                $estado = Estados::find($ubicacion[1]);
                $ubi=$estado->name.', '.$pais->name;
                if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                    $mes=intval(date('m',strtotime($fecha_inicio)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }else{
                    $mes1=intval(date('m',strtotime($fecha_inicio)));
                    $mes2=intval(date('m',strtotime($fecha_fin)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }
                if($feria->logo == ''){
                    $feria->logo = "https://placeholdit.imgix.net/~text?txtsize=45&txt=".$feria->nombre."&w=200&h=200";
                }else{
                    $feria->logo = '/storage/ferias/'.$feria->logo;
                }
                 /*Aplicar permiso*/
                if($data['permiso_agregar_editar_eliminar'] == "Si"){
                    $elemento_a = 'href="/cargargaleria/'.$feria->id.'" style="cursor:pointer;"';
                    $elemento_a1 = 'href="/editarferias/'.$feria->id.'" style="cursor:pointer;"';
                    $elemento_a2 = 'onclick="activar_feria('.$feria->id.')" style="cursor:pointer;"';
                }else{
                    $elemento_a = 'style="cursor:no-drop;" onclick="no_permiso(\'No tiene permiso para Agregar imagenes\')"';
                    $elemento_a1 = 'style="cursor:no-drop;" onclick="no_permiso(\'No tiene permiso para editar la feria\')"';
                    $elemento_a2 = 'style="cursor:no-drop;" onclick="no_permiso(\'No tiene permiso para activar la feria\')"';
                }
                switch ($cont) {
                    case 1: ?>
                        <div class="row mt-3 fila-feria-futura">
                            <div class="col-md-4 text-right">
                                <div class="view view-tenth img-derecha">
                                    <img class="bordes-redondos" src="<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a <?=$elemento_a?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Agregar galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a1?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Editar ferias"><i class="fa fa-pencil" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a2?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Activar"><i class="fa fa-check-square-o" aria-hidden="true"></i> | </a>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        break;
                    case 2: ?>
                        <div class="col-md-4">
                            <div class="view view-tenth img-centro">
                                <img class="bordes-redondos" src="<?=$feria->logo?>">
                                <div class="mask text-white">
                                  <h2><?=$feria->nombre?></h2>
                                  <p><?=$fecha_ubicacion?></p>
                                  <a <?=$elemento_a?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Agregar galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> | </a>
                                  <a <?=$elemento_a1?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Editar ferias"><i class="fa fa-pencil" aria-hidden="true"></i> | </a>
                                  <a <?=$elemento_a2?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Activar"><i class="fa fa-check-square-o" aria-hidden="true"></i> | </a>
                                  <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    case 3: ?>
                            <div class="col-md-4 text-left">
                                <div class="view view-tenth">
                                    <img class="bordes-redondos" src="<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a <?=$elemento_a?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Agregar galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a1?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Editar ferias"><i class="fa fa-pencil" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a2?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Activar"><i class="fa fa-check-square-o" aria-hidden="true"></i> | </a>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $cont=0;
                        break;
                }
                $cont++;
             }
             if(count($ferias)>0){
                 $residuo = (count($ferias)) % 3;
                 if($residuo>0){ ?>
                    </div>
                <?php
                 }
             }
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasfuturasfiltro(Request $request){
        /*Permiso para crear una feria*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

        $dt     = \Carbon\Carbon::now();
        $fecha  = $dt->toDateTimeString();
        $ferias = Feria::where('estado', 'Posible')->where('fecha_inicio', '>=' ,$fecha)->get();
        $ferias= DB::select('SELECT * FROM `ferias` WHERE `estado`="Posible" AND `fecha_inicio`>="'.$fecha.'" AND `nombre` LIKE "%'.$request->filtro.'%"');
        ob_start();

            $cont=1;
            foreach($ferias as $feria){
                $fecha_inicio=$feria->fecha_inicio;
                $fecha_fin=$feria->fecha_fin;
                $ubicacion = explode('%%',$feria->ubicacion);
                $pais = Paises::find($ubicacion[0]);
                $estado = Estados::find($ubicacion[1]);
                $ubi=$estado->name.', '.$pais->name;
                if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                    $mes=intval(date('m',strtotime($fecha_inicio)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }else{
                    $mes1=intval(date('m',strtotime($fecha_inicio)));
                    $mes2=intval(date('m',strtotime($fecha_fin)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }
                if($feria->logo == ''){
                    $feria->logo = "https://placeholdit.imgix.net/~text?txtsize=45&txt=".$feria->nombre."&w=200&h=200";
                }else{
                    $feria->logo = '/storage/ferias/'.$feria->logo;
                }
                /*Aplicar permiso*/
                if($data['permiso_agregar_editar_eliminar'] == "Si"){
                    $elemento_a = 'href="/cargargaleria/'.$feria->id.'" style="cursor:pointer;"';
                    $elemento_a1 = 'href="/editarferias/'.$feria->id.'" style="cursor:pointer;"';
                    $elemento_a2 = 'onclick="activar_feria('.$feria->id.')" style="cursor:pointer;"';
                }else{
                    $elemento_a = 'style="cursor:no-drop;" onclick="no_permiso(\'No tiene permiso para Agregar imagenes\')"';
                    $elemento_a1 = 'style="cursor:no-drop;" onclick="no_permiso(\'No tiene permiso para editar la feria\')"';
                    $elemento_a2 = 'style="cursor:no-drop;" onclick="no_permiso(\'No tiene permiso para activar la feria\')"';
                }
                switch ($cont) {
                    case 1: ?>
                        <div class="row mt-3 fila-feria-futura">
                            <div class="col-md-4 text-right">
                                <div class="view view-tenth img-derecha">
                                    <img class="bordes-redondos" src="<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a <?=$elemento_a?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Agregar galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a1?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Editar ferias"><i class="fa fa-pencil" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a2?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Activar"><i class="fa fa-check-square-o" aria-hidden="true"></i> | </a>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        break;
                    case 2: ?>
                        <div class="col-md-4">
                            <div class="view view-tenth img-centro">
                                <img class="bordes-redondos" src="<?=$feria->logo?>">
                                <div class="mask text-white">
                                  <h2><?=$feria->nombre?></h2>
                                  <p><?=$fecha_ubicacion?></p>
                                  <a <?=$elemento_a?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Agregar galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> | </a>
                                  <a <?=$elemento_a1?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Editar ferias"><i class="fa fa-pencil" aria-hidden="true"></i> | </a>
                                  <a <?=$elemento_a2?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Activar"><i class="fa fa-check-square-o" aria-hidden="true"></i> | </a>
                                  <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    case 3: ?>
                            <div class="col-md-4 text-left">
                                <div class="view view-tenth">
                                    <img class="bordes-redondos" src="<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a <?=$elemento_a?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Agregar galeria"><i class="fa fa-picture-o" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a1?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Editar ferias"><i class="fa fa-pencil" aria-hidden="true"></i> | </a>
                                      <a <?=$elemento_a2?> class="text-white" data-toggle="tooltip" data-placement="bottom" title="Activar"><i class="fa fa-check-square-o" aria-hidden="true"></i> | </a>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $cont=0;
                        break;
                }
                $cont++;
             }
             if(count($ferias)>0){
                 $residuo = (count($ferias)) % 3;
                 if($residuo>0){ ?>
                    </div>
                <?php
                 }
             }
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasexponente(Request $request){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $datos = Feria::where("estado","Activo")->where("tipo","Exponente")->get();
        ob_start(); ?>
        <?php $cont=0; foreach($datos as $dato){
        ?>
        <style>
            <?php $con=0; foreach($datos as $dato){ ?>
            .menu-itemR<?= $con ?> .fa{
                vertical-align: super;
                font-size: x-large;
            }
            .menuR<?= $con ?> {
              -moz-filter: url("#shadowed-goo");
              -webkit-filter: url("#shadowed-goo");
                      filter: url("#shadowed-goo");
            }

             .menu-itemR<?= $con ?>,  .menu-open-buttonR<?= $con ?> {
              background: #041d60;
              border-radius: 100%;
              width: 60px;
              height: 60px;
              margin-left: -40px;
              position: absolute;
              top: 20px;
              color: white;
              text-align: center;
              line-height: 80px;
              -webkit-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
              -webkit-transition: -webkit-transform ease-out 200ms;
              transition: -webkit-transform ease-out 200ms;
              -moz-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
              -moz-transition: -moz-transform ease-out 200ms;
              transition: -moz-transform ease-out 200ms;
              transition: transform ease-out 200ms;
              transition: transform ease-out 200ms, -webkit-transform ease-out 200ms, -moz-transform ease-out 200ms;
            }

             .menu-openR<?= $con ?> {
              display: none;
            }

             .hamburgerR<?= $con ?> {
              width: 25px;
              height: 3px;
              background: white;
              display: block;
              position: absolute;
              top: 50%;
              left: 50%;
              margin-left: -12.5px;
              margin-top: -1.5px;
              -webkit-transition: -webkit-transform 200ms;
              transition: -webkit-transform 200ms;
              -moz-transition: -moz-transform 200ms;
              transition: -moz-transform 200ms;
              transition: transform 200ms;
              transition: transform 200ms, -webkit-transform 200ms, , -moz-transform 200ms;
            }

             .hamburger-1 {
              -webkit-transform: translate3d(0, -8px, 0);
              -moz-transform: translate3d(0, -8px, 0);
                      transform: translate3d(0, -8px, 0);
            }

             .hamburger-2 {
              -webkit-transform: translate3d(0, 0, 0);
              -moz-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
            }

             .hamburger-3 {
              -webkit-transform: translate3d(0, 8px, 0);
              -moz-transform: translate3d(0, 8px, 0);
                      transform: translate3d(0, 8px, 0);
            }

             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-1 {
              -webkit-transform: translate3d(0, 0, 0) rotate(45deg);
              -moz-transform: translate3d(0, 0, 0) rotate(45deg);
                      transform: translate3d(0, 0, 0) rotate(45deg);
            }
             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-2 {
              -webkit-transform: translate3d(0, 0, 0) scale(0.1, 1);
              -moz-transform: translate3d(0, 0, 0) scale(0.1, 1);
                      transform: translate3d(0, 0, 0) scale(0.1, 1);
            }
             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-3 {
              -webkit-transform: translate3d(0, 0, 0) rotate(-45deg);
              -moz-transform: translate3d(0, 0, 0) rotate(-45deg);
                      transform: translate3d(0, 0, 0) rotate(-45deg);
            }

            .menuR<?= $con ?> {
              position: absolute;
              /*left: 50%;*/
              margin-left: -119px;
              padding-top: 20px;
              padding-left: 80px;
              width: 650px;
              height: 100px;
              box-sizing: border-box;
              font-size: 20px;
              text-align: left;
            }

             .menu-itemR<?= $con ?>:hover {
              background: white;
              color: #041d60;
            }
             .menu-itemR<?= $con ?>:nth-child(3) {
              -webkit-transition-duration: 180ms;
              -moz-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(4) {
              -webkit-transition-duration: 180ms;
              -moz-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(5) {
              -webkit-transition-duration: 180ms;
              -moz-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(6) {
              -webkit-transition-duration: 180ms;
              -moz-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
            .menu-itemR<?= $con ?>:nth-child(7) {
              -webkit-transition-duration: 180ms;
              -moz-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
            .menu-itemR<?= $con ?>:nth-child(8) {
              -webkit-transition-duration: 180ms;
              -moz-transition-duration: 180ms;
                      transition-duration: 180ms;
            }

             .menu-open-buttonR<?= $con ?> {
              z-index: 2;
              -moz-transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
              -webkit-transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
                      transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
              -moz-transition-duration: 400ms;
              -webkit-transition-duration: 400ms;
                      transition-duration: 400ms;
              -moz-transform: scale(1.1, 1.1) translate3d(0, 0, 0);
              -webkit-transform: scale(1.1, 1.1) translate3d(0, 0, 0);
                      transform: scale(1.1, 1.1) translate3d(0, 0, 0);
              cursor: pointer;
            }

             .menu-open-buttonR<?= $con ?>:hover {
              -moz-transform: scale(1.2, 1.2) translate3d(0, 0, 0);
              -webkit-transform: scale(1.2, 1.2) translate3d(0, 0, 0);
                      transform: scale(1.2, 1.2) translate3d(0, 0, 0);
            }

             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> {
              -moz-transition-timing-function: linear;
              -webkit-transition-timing-function: linear;
                      transition-timing-function: linear;
              -moz-transition-duration: 200ms;
              -webkit-transition-duration: 200ms;
                      transition-duration: 200ms;
              -moz-transform: scale(0.8, 0.8) translate3d(0, 0, 0);
              -webkit-transform: scale(0.8, 0.8) translate3d(0, 0, 0);
                      transform: scale(0.8, 0.8) translate3d(0, 0, 0);
            }

             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?> {
              -moz-transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
              -webkit-transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
                      transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
              -moz-transition-duration: 190ms;
              -webkit-transition-duration: 190ms;
                      transition-duration: 190ms;
              -moz-transform: translate3d(62.5px, 0, 0);
              -webkit-transform: translate3d(62.5px, 0, 0);
                      transform: translate3d(62.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1599px) {
                .menuR<?= $con ?> {
                    margin-left: -64px;
                }
                .menu-itemR<?= $con ?>, .menu-open-buttonR<?= $con ?>{
                    width: 40px;
                    height: 40px;
                    line-height: 57px;
                }
                .hamburgerR<?=$cont?>{
                    width: 15px;
                    height: 2px;
                    left: 62%;
                }
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
                    -moz-transform: translate3d(44px, 0, 0);
                    -webkit-transform: translate3d(44px, 0, 0);
                              transform: translate3d(44px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menuR<?= $con ?> {
                    margin-left: -64px;
                }
                .menu-itemR<?= $con ?>, .menu-open-buttonR<?= $con ?>{
                    width: 50px;
                    height: 50px;
                    line-height: 57px;
                }
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
                    -moz-transform: translate3d(55px, 0, 0);
                    -webkit-transform: translate3d(55px, 0, 0);
                              transform: translate3d(55px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
              -moz-transition-duration: 290ms;
              -webkit-transition-duration: 290ms;
                      transition-duration: 290ms;
              -moz-transform: translate3d(125px, 0, 0);
              -webkit-transform: translate3d(125px, 0, 0);
                      transform: translate3d(125px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
                        -moz-transform: translate3d(88px, 0, 0);
                        -webkit-transform: translate3d(88px, 0, 0);
                              transform: translate3d(88px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
                    -moz-transform: translate3d(110px, 0, 0);
                    -webkit-transform: translate3d(110px, 0, 0);
                              transform: translate3d(110px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
              -moz-transition-duration: 390ms;
              -webkit-transition-duration: 390ms;
                      transition-duration: 390ms;
              -moz-transform: translate3d(187.5px, 0, 0);
              -webkit-transform: translate3d(187.5px, 0, 0);
                      transform: translate3d(187.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
               .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
                     -moz-transform: translate3d(132px, 0, 0);
                     -webkit-transform: translate3d(132px, 0, 0);
                              transform: translate3d(132px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
                    -moz-transform: translate3d(165px, 0, 0);
                    -webkit-transform: translate3d(165px, 0, 0);
                              transform: translate3d(165px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
              -moz-transition-duration: 490ms;
              -webkit-transition-duration: 490ms;
                      transition-duration: 490ms;
              -moz-transform: translate3d(250px, 0, 0);
              -webkit-transform: translate3d(250px, 0, 0);
                      transform: translate3d(250px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
                    -moz-transform: translate3d(176px, 0, 0);
                    -webkit-transform: translate3d(176px, 0, 0);
                              transform: translate3d(176px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
                    -moz-transform: translate3d(220px, 0, 0);
                    -webkit-transform: translate3d(220px, 0, 0);
                              transform: translate3d(220px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
              -moz-transition-duration: 590ms;
              -webkit-transition-duration: 590ms;
                      transition-duration: 590ms;
              -moz-transform: translate3d(312.5px, 0, 0);
              -webkit-transform: translate3d(312.5px, 0, 0);
                      transform: translate3d(312.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
                    -moz-transform: translate3d(220px, 0, 0);
                    -webkit-transform: translate3d(220px, 0, 0);
                              transform: translate3d(220px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
                    -moz-transform: translate3d(275px, 0, 0);
                    -webkit-transform: translate3d(275px, 0, 0);
                              transform: translate3d(275px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
              -moz-transition-duration: 690ms;
                -webkit-transition-duration: 690ms;
                      transition-duration: 690ms;
              -moz-transform: translate3d(375px, 0, 0);
                -webkit-transform: translate3d(375px, 0, 0);
                      transform: translate3d(375px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
                    -moz-transform: translate3d(264px, 0, 0);
                    -webkit-transform: translate3d(264px, 0, 0);
                              transform: translate3d(264px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
                    -moz-transform: translate3d(330px, 0, 0);
                    -webkit-transform: translate3d(330px, 0, 0);
                              transform: translate3d(330px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(9) {
              -moz-transition-duration: 790ms;
                -webkit-transition-duration: 790ms;
                      transition-duration: 790ms;
              -moz-transform: translate3d(437.5px, 0, 0);
                -webkit-transform: translate3d(437.5px, 0, 0);
                      transform: translate3d(437.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(9) {
                    -moz-transform: translate3d(308px, 0, 0);
                    -webkit-transform: translate3d(308px, 0, 0);
                              transform: translate3d(308px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(9) {
                    -moz-transform: translate3d(385px, 0, 0);
                    -webkit-transform: translate3d(385px, 0, 0);
                              transform: translate3d(385px, 0, 0);
                    }
            }
            <?php $con++; } ?>
        </style>
        <?php $cont++; } ?>
        <div class="row">
            <div class="col-md-1">
                <div class="punto-exponente">
                    <img src="<?=$request->url?>/images/iconosferias/menu/exponente.png">
                </div>
            </div>
            <div class="col-md-11 text-center linea-exponente bordes-redondos-derecha">
                <h3 class="mt-3">Asistente como Exponente</h3>
            </div>
        </div>
        <div class="row area-busqueda mt-3">
            <div class="col-md-12 text-center mt-3 buscar-exponente">
                <input type="text" class="form-control" id="buscar_exponente" placeholder="&#xf002;">
            </div>
        </div>
        <div class="row mt-3 list-exponente">
            <?php $cont=0; foreach($datos as $dato){
            /*Aplicar Permiso*/
            $elemento_a = 'class="menu-itemR'.$cont.'" onclick="no_permiso(\'Usted no tiene permisos para cargar la ubicación\')"';
            $elemento_a2 = 'href="#" onclick="no_permiso(\'Usted no tiene permisos para cargar galeria\')"';
            $elemento_a3 = 'href="#" onclick="no_permiso(\'Usted no tiene permisos para editar la feria exponente\')"';
            if($data['permiso_agregar_editar_eliminar']=="Si"){
                $elemento_a = 'class="menu-itemR'.$cont.' subir-ubicacion1"';
                $elemento_a2 = 'href="/cargargaleria/'.$dato->id.'"';
                $elemento_a3 = 'href="/editarexponente/'.$dato->id.'"';
            }
            $fecha_inicio=$dato->fecha_inicio;
            $fecha_fin=$dato->fecha_fin;
            $ubicacion = explode('%%',$dato->ubicacion);
            $pais = Paises::find($ubicacion[0]);
            $estado = Estados::find($ubicacion[1]);
            $ubi=$estado->name.', '.$pais->name;
            if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                $mes=intval(date('m',strtotime($fecha_inicio)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }else{
                $mes1=intval(date('m',strtotime($fecha_inicio)));
                $mes2=intval(date('m',strtotime($fecha_fin)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }
            ?>
            <div class="col-md-6 mt-5">
                <div class="row fila-exponente">
                    <div class="col-md-5">
                        <img class="imagen-exponente" src="<?=$request->url?>/storage/ferias/<?=$dato->logo?>">
                    </div>
                    <div class="col-md-7 text-left color-exponente">
                        <h3 class="mt-0"><b><?=$dato->nombre?></b></h3>
                        <hr><br>
                        <h6><?=$fecha_ubicacion?></h6>
                        <h5 class="text-dark"><?=substr($dato->descripcion, 0, 90)?> ...</h5>
                        <nav class="menuR<?=$cont?>">
                          <input type="checkbox" href="#" class="menu-openR<?=$cont?>" name="menu-openR<?=$cont?>" id="menu-openR<?=$cont?>"/>
                          <label class="menu-open-buttonR<?=$cont?>" for="menu-openR<?=$cont?>">
                            <span class="hamburgerR<?=$cont?> hamburger-1"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-2"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-3"></span>
                          </label>

                          <a href="#" <?=$elemento_a?> id="ubi_<?=$dato->id?>" data-toggle="tooltip" data-placement="top" title="Cargar Ubicación"> <i class="fa fa-map-marker"></i> </a>
                          <a <?=$elemento_a2?> class="menu-itemR<?=$cont?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Galeria"> <i class="fa fa-picture-o"></i> </a>
                          <a href="<?=$request->url?>/presupuesto/<?=$dato->id?>" data-toggle="tooltip" data-placement="top" title="Crear Presupuesto" class="menu-itemR<?=$cont?>"> <i class="fa fa-money"></i> </a>
                          <a href="<?=$request->url?>/feria/patrocinador/<?=$dato->id?>&<?=$dato->nombre?>" data-toggle="tooltip" data-placement="top" title="Solicitudes Patrocinio" class="menu-itemR<?=$cont?>"> <i class="fa fa-usd"></i> </a>
                          <a href="<?=$request->url?>/feria/ver/<?=$dato->id?>" data-toggle="tooltip" title="Ver feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-eye"></i> </a>
                          <a <?=$elemento_a3?> data-toggle="tooltip" title="Editar feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-pencil"></i> </a>
                          <a href="<?=$request->url?>/feria/leccionesaprendidas/<?=$dato->id?>" data-toggle="tooltip" title="Lecciones Aprendidas" class="menu-itemR<?=$cont?>"> <i class="fa fa-comments-o"></i> </a>
                        </nav>
                    </div>
                </div>
            </div>
            <?php $cont++; } ?>
        </div>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasexponentefiltro(Request $request){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $datos= DB::select('SELECT * FROM `ferias` WHERE `estado`="Activo" AND `tipo`="Exponente" AND `nombre` LIKE "%'.$request->filtro.'%"');
        ob_start(); ?>
            <?php $cont=0; foreach($datos as $dato){
            /*Aplicar Permiso*/
            $elemento_a = 'class="menu-itemR'.$cont.'" onclick="no_permiso(\'Usted no tiene permisos para cargar la ubicación\')"';
            $elemento_a2 = 'href="#" onclick="no_permiso(\'Usted no tiene permisos para cargar galeria\')"';
            $elemento_a3 = 'href="#" onclick="no_permiso(\'Usted no tiene permisos para editar la feria exponente\')"';
            if($data['permiso_agregar_editar_eliminar']=="Si"){
                $elemento_a = 'class="menu-itemR'.$cont.' subir-ubicacion1"';
                $elemento_a2 = 'href="/cargargaleria/'.$dato->id.'"';
                $elemento_a3 = 'href="/editarexponente/'.$dato->id.'"';
            }
            $fecha_inicio=$dato->fecha_inicio;
            $fecha_fin=$dato->fecha_fin;
            $ubicacion = explode('%%',$dato->ubicacion);
            $pais = Paises::find($ubicacion[0]);
            $estado = Estados::find($ubicacion[1]);
            $ubi=$estado->name.', '.$pais->name;
            if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                $mes=intval(date('m',strtotime($fecha_inicio)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }else{
                $mes1=intval(date('m',strtotime($fecha_inicio)));
                $mes2=intval(date('m',strtotime($fecha_fin)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }
            ?>
            <div class="col-md-6 mt-5">
                <div class="row fila-exponente">
                    <div class="col-md-5">
                        <img class="imagen-exponente" src="<?=$request->url?>/storage/ferias/<?=$dato->logo?>">
                    </div>
                    <div class="col-md-7 text-left color-exponente">
                        <h3 class="mt-0"><b><?=$dato->nombre?></b></h3>
                        <hr><br>
                        <h6><?=$fecha_ubicacion?></h6>
                        <h5 class="text-dark"><?=substr($dato->descripcion, 0, 90)?> ...</h5>
                        <nav class="menuR<?=$cont?>">
                          <input type="checkbox" href="#" class="menu-openR<?=$cont?>" name="menu-openR<?=$cont?>" id="menu-openR<?=$cont?>"/>
                          <label class="menu-open-buttonR<?=$cont?>" for="menu-openR<?=$cont?>">
                            <span class="hamburgerR<?=$cont?> hamburger-1"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-2"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-3"></span>
                          </label>

                          <a href="#" <?=$elemento_a?> id="ubi_<?=$dato->id?>" data-toggle="tooltip" data-placement="top" title="Cargar Ubicación"> <i class="fa fa-map-marker"></i> </a>
                          <a <?=$elemento_a2?> class="menu-itemR<?=$cont?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Galeria"> <i class="fa fa-picture-o"></i> </a>
                          <a href="<?=$request->url?>/presupuesto/<?=$dato->id?>" data-toggle="tooltip" data-placement="top" title="Crear Presupuesto" class="menu-itemR<?=$cont?>"> <i class="fa fa-money"></i> </a>
                          <a href="<?=$request->url?>/feria/patrocinador/<?=$dato->id?>&<?=$dato->nombre?>" data-toggle="tooltip" data-placement="top" title="Solicitudes Patrocinio" class="menu-itemR<?=$cont?>"> <i class="fa fa-usd"></i> </a>
                          <a href="<?=$request->url?>/feria/ver/<?=$dato->id?>" data-toggle="tooltip" title="Ver feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-eye"></i> </a>
                          <a <?=$elemento_a3?> data-toggle="tooltip" title="Editar feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-pencil"></i> </a>
                          <a href="<?=$request->url?>/feria/leccionesaprendidas/<?=$dato->id?>" data-toggle="tooltip" title="Lecciones Aprendidas" class="menu-itemR<?=$cont?>"> <i class="fa fa-comments-o"></i> </a>
                        </nav>
                    </div>
                </div>
            </div>
            <?php $cont++; } ?>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasvisitante(Request $request){
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $datos = Feria::where("estado","Activo")->where("tipo","Visitante")->get();
        ob_start(); ?>
        <?php $cont=0; foreach($datos as $dato){  ?>
        <style>
            <?php $con=0; foreach($datos as $dato){ ?>
            .menu-itemR<?= $con ?> .fa{
                vertical-align: super;
                font-size: x-large;
            }
            .menuR<?= $con ?> {
              -webkit-filter: url("#shadowed-goo");
                      filter: url("#shadowed-goo");
            }

             .menu-itemR<?= $con ?>,  .menu-open-buttonR<?= $con ?> {
              background: #041d60;
              border-radius: 100%;
              width: 60px;
              height: 60px;
              margin-left: -40px;
              position: absolute;
              top: 20px;
              color: white;
              text-align: center;
              line-height: 80px;
              -webkit-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
              -webkit-transition: -webkit-transform ease-out 200ms;
              transition: -webkit-transform ease-out 200ms;
              transition: transform ease-out 200ms;
              transition: transform ease-out 200ms, -webkit-transform ease-out 200ms;
            }

             .menu-openR<?= $con ?> {
              display: none;
            }

             .hamburgerR<?= $con ?> {
              width: 25px;
              height: 3px;
              background: white;
              display: block;
              position: absolute;
              top: 50%;
              left: 50%;
              margin-left: -12.5px;
              margin-top: -1.5px;
              -webkit-transition: -webkit-transform 200ms;
              transition: -webkit-transform 200ms;
              transition: transform 200ms;
              transition: transform 200ms, -webkit-transform 200ms;
            }

             .hamburger-1 {
              -webkit-transform: translate3d(0, -8px, 0);
                      transform: translate3d(0, -8px, 0);
            }

             .hamburger-2 {
              -webkit-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
            }

             .hamburger-3 {
              -webkit-transform: translate3d(0, 8px, 0);
                      transform: translate3d(0, 8px, 0);
            }

             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-1 {
              -webkit-transform: translate3d(0, 0, 0) rotate(45deg);
                      transform: translate3d(0, 0, 0) rotate(45deg);
            }
             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-2 {
              -webkit-transform: translate3d(0, 0, 0) scale(0.1, 1);
                      transform: translate3d(0, 0, 0) scale(0.1, 1);
            }
             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-3 {
              -webkit-transform: translate3d(0, 0, 0) rotate(-45deg);
                      transform: translate3d(0, 0, 0) rotate(-45deg);
            }

            .menuR<?= $con ?> {
              position: absolute;
              /*left: 50%;*/
              margin-left: -119px;
              padding-top: 20px;
              padding-left: 80px;
              width: 650px;
              height: 100px;
              box-sizing: border-box;
              font-size: 20px;
              text-align: left;
            }

             .menu-itemR<?= $con ?>:hover {
              background: white;
              color: #041d60;
            }
             .menu-itemR<?= $con ?>:nth-child(3) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(4) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(5) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(6) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
            .menu-itemR<?= $con ?>:nth-child(7) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }

             .menu-open-buttonR<?= $con ?> {
              z-index: 2;
              -webkit-transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
                      transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
              -webkit-transition-duration: 400ms;
                      transition-duration: 400ms;
              -webkit-transform: scale(1.1, 1.1) translate3d(0, 0, 0);
                      transform: scale(1.1, 1.1) translate3d(0, 0, 0);
              cursor: pointer;
            }

             .menu-open-buttonR<?= $con ?>:hover {
              -webkit-transform: scale(1.2, 1.2) translate3d(0, 0, 0);
                      transform: scale(1.2, 1.2) translate3d(0, 0, 0);
            }

             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> {
              -webkit-transition-timing-function: linear;
                      transition-timing-function: linear;
              -webkit-transition-duration: 200ms;
                      transition-duration: 200ms;
              -webkit-transform: scale(0.8, 0.8) translate3d(0, 0, 0);
                      transform: scale(0.8, 0.8) translate3d(0, 0, 0);
            }

             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?> {
              -webkit-transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
                      transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
              -webkit-transition-duration: 190ms;
                      transition-duration: 190ms;
              -webkit-transform: translate3d(62.5px, 0, 0);
                      transform: translate3d(62.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1599px) {
                .menuR<?= $con ?> {
                    margin-left: -64px;
                }
                .menu-itemR<?= $con ?>, .menu-open-buttonR<?= $con ?>{
                    width: 40px;
                    height: 40px;
                    line-height: 57px;
                }
                .hamburgerR<?=$cont?>{
                    width: 15px;
                    height: 2px;
                    left: 62%;
                }
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
                      -webkit-transform: translate3d(44px, 0, 0);
                              transform: translate3d(44px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menuR<?= $con ?> {
                    margin-left: -64px;
                }
                .menu-itemR<?= $con ?>, .menu-open-buttonR<?= $con ?>{
                    width: 50px;
                    height: 50px;
                    line-height: 57px;
                }
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
                      -webkit-transform: translate3d(55px, 0, 0);
                              transform: translate3d(55px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
              -webkit-transition-duration: 290ms;
                      transition-duration: 290ms;
              -webkit-transform: translate3d(125px, 0, 0);
                      transform: translate3d(125px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
                      -webkit-transform: translate3d(88px, 0, 0);
                              transform: translate3d(88px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
                      -webkit-transform: translate3d(110px, 0, 0);
                              transform: translate3d(110px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
              -webkit-transition-duration: 390ms;
                      transition-duration: 390ms;
              -webkit-transform: translate3d(187.5px, 0, 0);
                      transform: translate3d(187.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
               .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
                      -webkit-transform: translate3d(132px, 0, 0);
                              transform: translate3d(132px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
                      -webkit-transform: translate3d(165px, 0, 0);
                              transform: translate3d(165px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
              -webkit-transition-duration: 490ms;
                      transition-duration: 490ms;
              -webkit-transform: translate3d(250px, 0, 0);
                      transform: translate3d(250px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
                      -webkit-transform: translate3d(176px, 0, 0);
                              transform: translate3d(176px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
                      -webkit-transform: translate3d(220px, 0, 0);
                              transform: translate3d(220px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
              -webkit-transition-duration: 590ms;
                      transition-duration: 590ms;
              -webkit-transform: translate3d(312.5px, 0, 0);
                      transform: translate3d(312.5px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
                      -webkit-transform: translate3d(220px, 0, 0);
                              transform: translate3d(220px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
                      -webkit-transform: translate3d(275px, 0, 0);
                              transform: translate3d(275px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
              -webkit-transition-duration: 690ms;
                      transition-duration: 690ms;
              -webkit-transform: translate3d(375px, 0, 0);
                      transform: translate3d(375px, 0, 0);
            }
            @media (min-width: 1366px) and (max-width: 1559px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
                      -webkit-transform: translate3d(264px, 0, 0);
                              transform: translate3d(264px, 0, 0);
                    }
            }
            @media (min-width: 1600px) and (max-width: 1919px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
                      -webkit-transform: translate3d(330px, 0, 0);
                              transform: translate3d(330px, 0, 0);
                    }
            }
            <?php $con++; } ?>
        </style>
        <?php $cont++; } ?>
        <div class="row">
            <div class="col-md-1">
                <div class="punto-visitante">
                    <img src="<?=$request->url?>/images/iconosferias/menu/visitante.png">
                </div>
            </div>
            <div class="col-md-11 text-center linea-visitante bordes-redondos-derecha">
                <h3 class="mt-3">Asistente como Visistante</h3>
            </div>
        </div>
        <div class="row area-busqueda mt-3">
            <div class="col-md-12 text-center mt-3 buscar-exponente">
                <input type="text" class="form-control" id="buscar_visitante" placeholder="&#xf002;">
            </div>
        </div>
        <div class="row mt-3 list-visitante">
            <?php $cont=0; foreach($datos as $dato){
            $fecha_inicio=$dato->fecha_inicio;
            $fecha_fin=$dato->fecha_fin;
            $ubicacion = explode('%%',$dato->ubicacion);
            $pais = Paises::find($ubicacion[0]);
            $estado = Estados::find($ubicacion[1]);
            $ubi=$estado->name.', '.$pais->name;
            if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                $mes=intval(date('m',strtotime($fecha_inicio)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }else{
                $mes1=intval(date('m',strtotime($fecha_inicio)));
                $mes2=intval(date('m',strtotime($fecha_fin)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }
            ?>
            <div class="col-md-6 mt-5">
                <div class="row fila-exponente">
                    <div class="col-md-5">
                       <?php if(!empty($dato->logo)){ ?>
                        <img class="imagen-exponente" src="<?=$request->url?>/storage/ferias/<?=$dato->logo?>">
                        <?php }else{ ?>
                        <img class="imagen-exponente" src="http://via.placeholder.com/200x200?text=imagen">
                        <?php } ?>
                    </div>
                    <div class="col-md-7 text-left color-exponente">
                        <h3 class="mt-0"><b><?=$dato->nombre?></b></h3>
                        <hr><br>
                        <h6><?=$fecha_ubicacion?></h6>
                        <h5 class="text-dark"><?=substr($dato->descripcion, 0, 90)?> ...</h5>
                        <nav class="menuR<?=$cont?>">
                          <input type="checkbox" href="#" class="menu-openR<?=$cont?>" name="menu-openR<?=$cont?>" id="menu-openR<?=$cont?>"/>
                          <label class="menu-open-buttonR<?=$cont?>" for="menu-openR<?=$cont?>">
                            <span class="hamburgerR<?=$cont?> hamburger-1"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-2"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-3"></span>
                          </label>

                          <a href="<?=$request->url?>/cargargaleria/<?=$dato->id?>" class="menu-itemR<?=$cont?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Galeria"> <i class="fa fa-picture-o"></i> </a>
                          <a href="<?=$request->url?>/presupuesto/<?=$dato->id?>" data-toggle="tooltip" data-placement="top" title="Crear Presupuesto" class="menu-itemR<?=$cont?>"> <i class="fa fa-money"></i> </a>
                          <a href="<?=$request->url?>/feria/patrocinador/<?=$dato->id?>&<?=$dato->nombre?>" data-toggle="tooltip" data-placement="top" title="Solicitudes Patrocinio" class="menu-itemR<?=$cont?>"> <i class="fa fa-usd"></i> </a>
                          <a href="<?=$request->url?>/feria/ver/<?=$dato->id?>" data-toggle="tooltip" title="Ver feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-eye"></i> </a>
                          <a href="<?=$request->url?>/editarferias/<?=$dato->id?>" data-toggle="tooltip" title="Editar feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-pencil"></i> </a>
                        </nav>
                    </div>
                </div>
            </div>
            <?php $cont++; } ?>
        </div>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasvisitantefiltro(Request $request){
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $datos= DB::select('SELECT * FROM `ferias` WHERE `estado`="Activo" AND `tipo`="Visitante" AND `nombre` LIKE "%'.$request->filtro.'%"');
         ob_start(); ?>
            <?php $cont=0; foreach($datos as $dato){
            $fecha_inicio=$dato->fecha_inicio;
            $fecha_fin=$dato->fecha_fin;
            $ubicacion = explode('%%',$dato->ubicacion);
            $pais = Paises::find($ubicacion[0]);
            $estado = Estados::find($ubicacion[1]);
            $ubi=$estado->name.', '.$pais->name;
            if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                $mes=intval(date('m',strtotime($fecha_inicio)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }else{
                $mes1=intval(date('m',strtotime($fecha_inicio)));
                $mes2=intval(date('m',strtotime($fecha_fin)));
                $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
            }
            ?>
            <div class="col-md-6 mt-5">
                <div class="row fila-exponente">
                    <div class="col-md-5">
                        <img class="imagen-exponente" src="<?=$request->url?>/storage/ferias/<?=$dato->logo?>">
                    </div>
                    <div class="col-md-7 text-left color-exponente">
                        <h3 class="mt-0"><b><?=$dato->nombre?></b></h3>
                        <hr><br>
                        <h6><?=$fecha_ubicacion?></h6>
                        <h5 class="text-dark"><?=substr($dato->descripcion, 0, 90)?> ...</h5>
                        <nav class="menuR<?=$cont?>">
                          <input type="checkbox" href="#" class="menu-openR<?=$cont?>" name="menu-openR<?=$cont?>" id="menu-openR<?=$cont?>"/>
                          <label class="menu-open-buttonR<?=$cont?>" for="menu-openR<?=$cont?>">
                            <span class="hamburgerR<?=$cont?> hamburger-1"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-2"></span>
                            <span class="hamburgerR<?=$cont?> hamburger-3"></span>
                          </label>

                          <a href="<?=$request->url?>/cargargaleria/<?=$dato->id?>" class="menu-itemR<?=$cont?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Galeria"> <i class="fa fa-picture-o"></i> </a>
                          <a href="<?=$request->url?>/presupuesto/<?=$dato->id?>" data-toggle="tooltip" data-placement="top" title="Crear Presupuesto" class="menu-itemR<?=$cont?>"> <i class="fa fa-money"></i> </a>
                          <a href="<?=$request->url?>/feria/patrocinador/<?=$dato->id?>&<?=$dato->nombre?>" data-toggle="tooltip" data-placement="top" title="Solicitudes Patrocinio" class="menu-itemR<?=$cont?>"> <i class="fa fa-usd"></i> </a>
                          <a href="<?=$request->url?>/feria/ver/<?=$dato->id?>" data-toggle="tooltip" title="Ver feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-eye"></i> </a>
                          <a href="<?=$request->url?>/editarexponente/<?=$dato->id?>" data-toggle="tooltip" title="Editar feria" class="menu-itemR<?=$cont?>"> <i class="fa fa-pencil"></i> </a>
                        </nav>
                    </div>
                </div>
            </div>
            <?php $cont++; } ?>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasnoasistidas(Request $request){
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

        $dt     = \Carbon\Carbon::now();
        $fecha  = $dt->toDateTimeString();
        $ferias = Feria::where('estado', 'Posible')->where('fecha_inicio', '<' ,$fecha)->get();
        ob_start(); ?>
            <div class="row">
                <div class="col-md-1">
                    <div class="punto-perdida">
                        <img src="<?=$request->url?>/images/iconosferias/menu/no_asistidas.png">
                    </div>
                </div>
                <div class="col-md-11 text-center linea-perdida bordes-redondos-derecha">
                    <h3 class="mt-3">Ferias Perdidas</h3>
                </div>
            </div>
            <?php if(count($ferias)>0){ ?>
            <div class="row area-busqueda mt-3">
                <div class="col-md-12 text-center mt-3 buscar-futura">
                    <input type="text" class="form-control" id="buscar_perdida" placeholder="&#xf002;">
                </div>
            </div>
            <?php }
            $cont=1;
            foreach($ferias as $feria){
                $fecha_inicio=$feria->fecha_inicio;
                $fecha_fin=$feria->fecha_fin;
                $ubicacion = explode('%%',$feria->ubicacion);
                $pais = Paises::find($ubicacion[0]);
                $estado = Estados::find($ubicacion[1]);
                $ubi=$estado->name.', '.$pais->name;
                if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                    $mes=intval(date('m',strtotime($fecha_inicio)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }else{
                    $mes1=intval(date('m',strtotime($fecha_inicio)));
                    $mes2=intval(date('m',strtotime($fecha_fin)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }
                switch ($cont) {
                    case 1: ?>
                        <div class="row mt-3 fila-feria-perdida">
                            <div class="col-md-4 text-right">
                                <div class="view view-tenth img-derecha">
                                    <img class="bordes-redondos" src="<?=$request->url?>/storage/ferias/<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        break;
                    case 2: ?>
                        <div class="col-md-4">
                            <div class="view view-tenth img-centro">
                                <img class="bordes-redondos" src="<?=$request->url?>/storage/ferias/<?=$feria->logo?>">
                                <div class="mask text-white">
                                  <h2><?=$feria->nombre?></h2>
                                  <p><?=$fecha_ubicacion?></p>
                                  <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    case 3: ?>
                            <div class="col-md-4 text-left">
                                <div class="view view-tenth">
                                    <img class="bordes-redondos" src="<?=$request->url?>/storage/ferias/<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $cont=0;
                        break;
                }
                $cont++;
             }
             if(count($ferias)>0){
                 $residuo = (count($ferias)) % 3;
                 if($residuo>0){ ?>
                    </div>
                <?php
                 }
             }
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlistadoferiasnoasistidasfiltro(Request $request){

        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

        $dt     = \Carbon\Carbon::now();
        $fecha  = $dt->toDateTimeString();
        $ferias= DB::select('SELECT * FROM `ferias` WHERE `estado`="Posible" AND `fecha_inicio`<"'.$fecha.'" AND `nombre` LIKE "%'.$request->filtro.'%"');

        ob_start();
            $cont=1;
            foreach($ferias as $feria){
                $fecha_inicio=$feria->fecha_inicio;
                $fecha_fin=$feria->fecha_fin;
                $ubicacion = explode('%%',$feria->ubicacion);
                $pais = Paises::find($ubicacion[0]);
                $estado = Estados::find($ubicacion[1]);
                $ubi=$estado->name.', '.$pais->name;
                if(date('m',strtotime($fecha_inicio)) == date('m',strtotime($fecha_fin))){
                    $mes=intval(date('m',strtotime($fecha_inicio)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }else{
                    $mes1=intval(date('m',strtotime($fecha_inicio)));
                    $mes2=intval(date('m',strtotime($fecha_fin)));
                    $fecha_ubicacion=date('d',strtotime($fecha_inicio)).' de '.$meses[$mes1].' al '.date('d',strtotime($fecha_fin)).' de '.$meses[$mes2].' de '.date('Y',strtotime($fecha_inicio)).', '.$ubi;
                }
                switch ($cont) {
                    case 1: ?>
                        <div class="row mt-3 fila-feria-perdida">
                            <div class="col-md-4 text-right">
                                <div class="view view-tenth img-derecha">
                                    <img class="bordes-redondos" src="<?=$request->url?>/storage/ferias/<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        break;
                    case 2: ?>
                        <div class="col-md-4">
                            <div class="view view-tenth img-centro">
                                <img class="bordes-redondos" src="<?=$request->url?>/storage/ferias/<?=$feria->logo?>">
                                <div class="mask text-white">
                                  <h2><?=$feria->nombre?></h2>
                                  <p><?=$fecha_ubicacion?></p>
                                  <a  class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    case 3: ?>
                            <div class="col-md-4 text-left">
                                <div class="view view-tenth">
                                    <img class="bordes-redondos" src="<?=$request->url?>/storage/ferias/<?=$feria->logo?>">
                                    <div class="mask text-white">
                                      <h2><?=$feria->nombre?></h2>
                                      <p><?=$fecha_ubicacion?></p>
                                      <a  class="text-white" data-toggle="tooltip" data-placement="bottom" title="Ver" onclick="verFeria(<?=$feria->id?>)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $cont=0;
                        break;
                }
                $cont++;
             }
             if(count($ferias)>0){
                 $residuo = (count($ferias)) % 3;
                 if($residuo>0){ ?>
                    </div>
                <?php
                 }
             }
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

}
