<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Venta;
use App\Oportunidades;
use App\HistorialOportunidades;
use App\Actividades;
use App\Cliente;
use App\Empresa;
use App\Ventas_archivo;
use App\Ventas_archivos_comentario;
use App\User;
use App\Apu;
use App\Gasto;
use App\Otras_maquina;
use App\Ventas_liquidacione;
use App\Agenda_cliente;
use App\Actividades_detalles;
use App\CategoriaNoArchivo;
use App\Ciclo;
use App\ApuHistoria;
use App\ApuEquipo;
use App\Producto;
use App\ProductoReferencias;
use App\ApuComentario;
use App\ApuOtrosGastos;
use App\ViewFreelanceLista;
use Auth;

use App\Historial_oportunidades_archivo;
use App\Historial_oportunidades_comentario;
use App\Oportunidades_ingreso;
use App\OportunidadesCompetencias;

class PersonalizadoController extends Controller{
    public function index($flag = null){
        $valUseId = Auth::user()->id;
        $historial = [];

        $ventas=Venta::where('oportunidad','257')->orWhere('oportunidad','218')->get();
        foreach ($ventas as $value){
            $oportunidad = Oportunidades::find($value->oportunidad);
            if (isset($oportunidad) && !empty($oportunidad)) {
                /*Año actual*/
                $date = date("Y");
                $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$value->oportunidad.' ORDER BY H.id DESC LIMIT 0,1';
                $FECHA_CIERRE = DB::select($query);
                if(isset($FECHA_CIERRE[0]->value) && date('Y', strtotime($FECHA_CIERRE[0]->value)) == $date){
                    $historia = HistorialOportunidades::where('oportunidad_id', $value->oportunidad)->get();
                    $oportunidad->venta_id = $value->id;
                    $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                }
            }
        }
        $permiso_editar='No';
        $permiso_anadir='No';
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("personalizado.oportunidadesvendidas",['data' => $data, 'datos' => $historial, 'permiso_editar'=>$permiso_editar, 'permiso_anadir'=>$permiso_anadir]);
    }
}
