<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\FeriasLeccione;
use App\FeriasLeccionesArchivo;
use App\FeriasLeccionesComentario;
use App\Feria;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
date_default_timezone_set("America/Bogota");

class LeccionesaprendidasController extends Controller
{
    public function index($id){
        $data['menu'] = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id, 11);
        $data['id'] = $id;
        $respuestas=FeriasLeccione::where('id_feria',$id)->where('respuesta','!=','')->get();
        $data['respuestas']=count($respuestas);
        $archivos=FeriasLeccionesArchivo::where('id_feria',$id)->get();
        $data['archivos']=count($archivos);
        $comentarios=FeriasLeccionesComentario::where('id_feria',$id)->get();
        $data['comentarios']=count($comentarios);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.viewleccionesaprendidas',['data'=>$data]);
    }

    public function lecciones($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $lecciones = FeriasLeccione::where('id_feria',$id)->orderBy('pregunta', 'ASC')->get();
        foreach($lecciones as $leccion){
            $accion[$leccion->pregunta] = 'onclick="editar_pregunta('.$leccion->id.')"';
            $respuesta[$leccion->pregunta] = $leccion->respuesta;
        }
        ob_start(); ?>
        <div class="col-md-12 text-right">
            <a class="cursor-mano cerrar" data-toggle="tooltip" data-placement="top" data-original-title="Cerrar Preguntas" ><img class="eliminar-conferencista" src="/images/iconosferias/conferencia/eliminar_icon.svg"></a>
        </div>
        <div class="col-md-12 ml-5 mt-3 mb-3 item-conferen">
            <div class="row">
                <div class="col-md-6 bg-white sombra-cuadro">
                    <div class="row">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">Acerca de la ubicación, posición y tamaño del stand</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[1])){ echo $accion[1]; }else{ echo 'onclick="crear_pregunta(1)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?> ><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo">
                        <div class="col-md-12 text-justify mt-3">
                            <p><?php if(isset($respuesta[1])){ echo $respuesta[1]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row ml-3 mr-3 sombra-arriba">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">Que mejora hará en el siguiente stand de feria</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano"  data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[2])){ echo $accion[2]; }else{ echo 'onclick="crear_pregunta(2)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?> ><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo ml-3 mr-3 sombra-cuadro">
                        <div class="col-md-12 bg-white text-justify mt-3">
                            <p><?php if(isset($respuesta[2])){ echo $respuesta[2]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bg-white sombra-cuadro mt-3">
                    <div class="row">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">Cual es su analisis de los stand de la competencia</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[3])){ echo $accion[3]; }else{ echo 'onclick="crear_pregunta(3)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?>><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo">
                        <div class="col-md-12 text-justify mt-3">
                            <p><?php if(isset($respuesta[3])){ echo $respuesta[3]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3">
                    <div class="row ml-3 mr-3 sombra-arriba">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">Acerca de los impresos a entregar, que hay por mejorar o tener en cuenta</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano"  data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[4])){ echo $accion[4]; }else{ echo 'onclick="crear_pregunta(4)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?>><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo ml-3 mr-3 sombra-cuadro">
                        <div class="col-md-12 bg-white text-justify mt-3">
                            <p><?php if(isset($respuesta[4])){ echo $respuesta[4]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bg-white sombra-cuadro mt-3">
                    <div class="row">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">Que se debe contemplar respecto a equipos,, cuales traer, presentación, consideraciones</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[5])){ echo $accion[5]; }else{ echo 'onclick="crear_pregunta(5)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?>><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo">
                        <div class="col-md-12 text-justify mt-3">
                            <p><?php if(isset($respuesta[5])){ echo $respuesta[5]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3">
                    <div class="row ml-3 mr-3 sombra-arriba">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">comentarios positivos recibidos de los visitantes</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano"  data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[6])){ echo $accion[6]; }else{ echo 'onclick="crear_pregunta(6)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?>><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo ml-3 mr-3 sombra-cuadro">
                        <div class="col-md-12 bg-white text-justify mt-3">
                            <p><?php if(isset($respuesta[6])){ echo $respuesta[6]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bg-white sombra-cuadro mt-3">
                    <div class="row">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">comentarios negativos recibidos de los visitantes</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano" data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[7])){ echo $accion[7]; }else{ echo 'onclick="crear_pregunta(7)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?>><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo">
                        <div class="col-md-12 text-justify mt-3">
                            <p><?php if(isset($respuesta[7])){ echo $respuesta[7]; } ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3">
                    <div class="row ml-3 mr-3 sombra-arriba">
                        <div class="col-md-10 title-conferencista text-left">
                            <h5 class="mt-4 mb-4"><img class="ml-5 mr-3" src="/images/iconosferias/conferencia/conferencia_icon.svg">Acerca de la conferencia, positivas, negativas, futuras segun la propia y otras</h5>
                        </div>
                        <div class="col-md-2 title-conferencista text-right">
                            <a class="cursor-mano"  data-toggle="tooltip" data-placement="top" data-original-title="Editar pregunta" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ if(isset($accion[8])){ echo $accion[8]; }else{ echo 'onclick="crear_pregunta(8)"'; } }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?>><i class="fa fa-pencil mt-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="row alto-fijo ml-3 mr-3 sombra-cuadro">
                        <div class="col-md-12 bg-white text-justify mt-3">
                            <p><?php if(isset($respuesta[8])){ echo $respuesta[8]; } ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $preguntas = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $preguntas]);
    }

    public function consultarrespuesta($id){
        $dato=FeriasLeccione::find($id);
        ob_start(); ?>
        <textarea class="form-control" rows="8" id="respuesta"><?=$dato->respuesta?></textarea>
         <?php
        $data['content'] = ob_get_contents();
        ob_end_clean();
        return \Response::json(['data' => $data]);
    }
    public function guardarrespuesta(Request $request){
        if($request->tipo == "editar"){
            $dato=FeriasLeccione::find($request->id);
            $dato->respuesta=$request->respuesta;
            $dato->save();
            return \Response::json(['msj' => "Guardado correctamente"]);
        }else{
            $dato = new FeriasLeccione;
            $dato->respuesta=$request->respuesta;
            $dato->pregunta=$request->pregunta;
            $dato->id_feria=$request->id_feria;
            $dato->save();
            return \Response::json(['msj' => "Guardado correctamente"]);
        }

    }

    public function consultardocumentoslecciones($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $archivos=FeriasLeccionesArchivo::where('id_feria',$id)->orderBy('id', 'ASC')->get();
        $meses=['','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
        ob_start(); ?>
        <div class="col-md-12 text-right">
            <a class="cursor-mano cerrar" data-toggle="tooltip" data-placement="top" data-original-title="Cerrar Archivos" ><img class="eliminar-conferencista" src="/images/iconosferias/conferencia/eliminar_icon.svg"></a>
        </div>
        <div class="col-md-12">
            <a class="cursor-mano" <?php if($data['permiso_agregar_editar_eliminar'] == "Si"){ ?>onclick="subirarchivo()"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para añadir archivos')"<?php } ?> data-toggle="tooltip" data-placement="top" data-original-title="Crear archivo"><img class="mr-3 mt-2" src="/images/iconosferias/conferencia/masconfere_icon.svg"></a>
        </div>
        <?php foreach($archivos as $archivo){
        $user=User::find($archivo->id_user);
        ?>

        <div class="col-md-3 mb-5">
            <div class="row">
                <div class="col-md-12 text-danger cursor-mano" onclick="ver_archivo('/storage/ferias/lecciones/<?=$archivo->archivo?>')">
                    <a><i class="fa fa-file-pdf-o fa-5" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-12">
                    <h6><?=$archivo->nombre?></h6>
                    <h5><?=$user->nombres.' '.$user->apellidos?></h5>
                    <h5><?=date('d',strtotime($archivo->updated_at)).'/'.$meses[intval(date('m',strtotime($archivo->updated_at)))].'/'.date('Y',strtotime($archivo->updated_at)).' '.date('h:i:s A',strtotime($archivo->updated_at))?></h5>
                    <img <?php if($data['permiso_agregar_editar_eliminar'] == "Si"){ ?>onclick="eliminar_archivo(<?=$archivo->id?>)"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos de eliminar archivos')"<?php } ?> data-toggle="tooltip" data-placement="top" data-original-title="Eliminar archivo" class="eliminar-conferencista" src="/images/iconosferias/conferencia/eliminar_icon.svg">
                </div>
            </div>
        </div>
        <?php
        }
        if(count($archivos) == 0){ ?>
            <div class="col-md-12">
                <h3>No hay archivos cargados</h3>
            </div>
        <?php
        }

        $data['content'] = ob_get_contents();
        ob_end_clean();
        return \Response::json(['data' => $data]);
    }
    public function  eliminararchivolecciones($id){
        $archivo=FeriasLeccionesArchivo::find($id);
        Storage::delete('ferias/lecciones/'.$archivo->archivo);
        $archivo->delete();
        return \Response::json(['msj' => "Eliminado correctamente"]);
    }

    public function consultarcomentarios($id){
        $comentarios = FeriasLeccionesComentario::where('id_feria',$id)->get();
         ob_start(); ?>
            <div class="col-md-12 text-right">
                <a class="cursor-mano cerrar" data-toggle="tooltip" data-placement="top" data-original-title="Cerrar Comentarios" ><img class="eliminar-conferencista" src="/images/iconosferias/conferencia/eliminar_icon.svg"></a>
            </div>
            <div class="comments col-md-12 pt-5">
                <?php foreach($comentarios as $comentario){
                    $user=User::find($comentario->id_user);
                ?>
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url('/images/file/clientes/<?=$user->foto?>')"></div>
                    </div>
                    <div class="comment-block">
                        <p class="comment-text text-left">
                            <?=$comentario->comentario?>
                        </p>
                        <div class="bottom-comment">
                            <div class="comment-date">
                                <?=date("d/m/Y (h:i:s A)",strtotime($comentario->created_at))?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div>
            <div class="col-md-12 pt-4">
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url('/images/file/clientes/<?=Auth::user()->foto?>')"></div>
                    </div>
                    <div class="comment-block">
                        <form autocomplete="off" id="messages_box">
                           <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..."></textarea>
                        </form>
                        <div class="pull-right">
                        <button type="button" class="btn btn-success" onclick="do_comments()" id="send_btn">Enviar <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function comentarlecciones(Request $request){
        $dato = new FeriasLeccionesComentario;
        $dato->comentario=$request->comentario;
        $dato->id_feria=$request->id_feria;
        $dato->id_user=$request->id_user;
        $dato->save();
        return \Response::json(['msj' => "Guardado correctamente"]);
    }
}
