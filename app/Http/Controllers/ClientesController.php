<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;
use App\Cliente;
use App\Empresa;
use App\ClienteRedessociales;
use App\ClienteTelefonos;
use App\ClienteMails;
use App\EmpresaSedes;
use App\ClienteComentario;
use App\User;
use Auth;

date_default_timezone_set("America/Bogota");

class ClientesController extends Controller
{
    public function index()
    {
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Clientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	$modulo=7;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="crear cliente" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				return view('clientes.crear',['data' => $data]);
			  }else{
				return view('oportunidades.nopermiso',['data' => $data]);
			  }
            }
          }          
        }
    }

    public function indexedit($id)
    {
    	$model = Cliente::findOrFail($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Clientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('clientes.editar',['model' => $model, 'data' => $data]);
    }

    public function eliminarcliente($id){
    	$model = Cliente::find($id);
    	$model->delete();
    	return \Response::json(['success' => true]);
    }

    public function view($id)
    {
    	$modulo=7;
    	$permiso_editar='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar en listar y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				$permiso_editar="Si";
			  }else{
				$permiso_editar="No";
			  }
            }
          }          
        }
    	$model = Cliente::findOrFail($id);

        $sede = EmpresaSedes::where('id', $model->empresa_sede_id)->get();
        $sede = $sede[0];
        $empresa = Empresa::where('id', $sede->empresa_id)->get();
        $empresa = $empresa[0];

        $query_porcentaje = 'CONVERT(IF(IsNull(C.foto), 0, (100/47)*5)+IF(IsNull(C.nombres), 0, (100/47)*1)+IF(IsNull(C.apellidos), 0, (100/47)*1)+IF(IsNull(C.empresa_id), 0, (100/47)*1)+IF(IsNull(C.empresa_sede_id), 0, (100/47)*1)+IF(IsNull(C.user_id), 0, (100/47)*1)+IF(IsNull(C.profesion), 0, (100/47)*2)+IF(IsNull(C.jefe_inmediato), 0, (100/47)*1)+IF(IsNull(C.n_a_pesos), 0, (100/47)*2)+IF(IsNull(C.tratamiento), 0, (100/47)*1)+IF(IsNull(C.estado_civil), 0, (100/47)*1)+IF(IsNull(C.cargo), 0, (100/47)*1)+IF(IsNull(C.fecha_nacimiento), 0, (100/47)*4)+IF(IsNull(C.num_hijos), 0, (100/47)*1)+IF(IsNull(C.pais), 0, (100/47)*1)+IF(IsNull(C.departamento), 0, (100/47)*1)+IF(IsNull(C.ciudad), 0, (100/47)*1)+IF(IsNull(C.perfil), 0, (100/47)*5),UNSIGNED INTEGER) + ';
        $query_porcentaje.= '(SELECT CONVERT(IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)),UNSIGNED INTEGER) FROM cliente_telefonos CT WHERE CT.cliente_id = '.$id.') + ';
        $query_porcentaje.= '(SELECT CONVERT(IF(COUNT(*)>0,(100/47)*4,0),UNSIGNED INTEGER) FROM cliente_redessociales CR WHERE CR.cliente_id = '.$id.') + ';
        $query_porcentaje.= '(SELECT CONVERT(IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)),UNSIGNED INTEGER) FROM cliente_mails CM WHERE CM.cliente_id = '.$id.') AS Porcentaje';
        $data['porcentaje'] = DB::select('SELECT '.$query_porcentaje.' FROM clientes C WHERE C.id = '.$id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Clientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('clientes.ver',['data' => $data, 'model' => $model, 'sede' => $sede, 'empresa' => $empresa, 'permiso_editar' => $permiso_editar]);
    }

	public function edit(Request $request)
	{

		$logo = $request->logo_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombrelogo = uniqid();
            $nombrelogo =$antenombrelogo.".png";
            \Storage::disk('cliente')->put($nombrelogo,  $foto);
        }

	    $dato = Cliente::findOrFail($request->id);
	    if (isset($nombrelogo)) {
			$dato->foto = $nombrelogo;
		} 
	    $dato->nombres           = $request->nombres;
	    $dato->apellidos         = $request->apellidos;
	    $dato->cliente         	 = $request->nombres." ".$request->apellidos;
	    $dato->tratamiento       = $request->tratamiento;
	    $dato->estado_civil      = $request->estado_civil;
	    $dato->profesion         = $request->profesion;
	    $dato->cargo             = $request->cargo;
	    $dato->jefe_inmediato    = $request->jefe_inmediato;
	    $dato->n_a_pesos         = $request->n_a_pesos;
	    $dato->num_hijos         = $request->num_hijos;
	    $dato->pais              = $request->pais;
	    $dato->departamento      = $request->departamento;
	    $dato->ciudad            = $request->ciudad;
	    $dato->fecha_nacimiento  = $request->fecha_nacimiento;
	    $dato->perfil            = $request->perfil;
	    $dato->empresa_id   = $request->empresa;
	    $dato->empresa_sede_id   = $request->empresa_sede_id;
	    if (!empty($request->responsable)) {
	    	$dato->user_id       = $request->responsable;
	    }else{
	    	$dato->user_id       = Auth::user()->id;
	    }
	    $dato->save();

	    foreach ($request->telefono as $key) {
	    	if (isset($key["telefono_numero"])) {
	    		if (!empty($key["id"])) {
	    			$telefono = ClienteTelefonos::findOrFail($key["id"]);
	    		}else{
	    			$telefono = new ClienteTelefonos;
	    		}
	    		
		    	$telefono->cliente_id = $dato->id;
			    $telefono->telefono_tipo = $key["telefono_tipo"];
				$telefono->telefono_indp = $key["telefono_indp"];
				$telefono->telefono_indc = $key["telefono_indc"];
				$telefono->telefono_numero = $key["telefono_numero"];
				$telefono->telefono_ext = $key["telefono_ext"];
				$telefono->save();
	    	}		    	
	    }

	    foreach ($request->redsocial as $key) {
	    	if (isset($key["valor"])) {

	    		if (!empty($key["id"])) {
	    			$redsocial = ClienteRedessociales::findOrFail($key["id"]);
	    		}else{
	    			$redsocial = new ClienteRedessociales;
	    		}
		    	$redsocial->cliente_id = $dato->id;
			    $redsocial->tipo = $key["tipo"];
				$redsocial->valor = $key["valor"];
				$redsocial->save();
	    	}	    	
	    }

	    foreach ($request->correo as $key) {
	    	if (isset($key["mail"])) {
	    		if (!empty($key["id"])) {
	    			$mail = ClienteMails::findOrFail($key["id"]);
	    		}else{
	    			$mail = new ClienteMails;
	    		}
		    	$mail->cliente_id = $dato->id;
			    $mail->mail = $key["mail"];
				$mail->save();
	    	}	    	
	    }
	    return redirect('cliente/'.$request->id);
	}

	public function save(Request $request)
	{

		$logo = $request->logo_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombrelogo = uniqid();
            $nombrelogo =$antenombrelogo.".png";
            \Storage::disk('cliente')->put($nombrelogo,  $foto);
        }

	    $dato = new Cliente;
	    if (isset($nombrelogo)) {
			$dato->foto = $nombrelogo;
		}
	    $dato->nombres           = $request->nombres;
	    $dato->apellidos         = $request->apellidos;
	    $dato->cliente         	 = $request->nombres." ".$request->apellidos;
	    $dato->tratamiento       = $request->tratamiento;
	    $dato->estado_civil      = $request->estado_civil;
	    $dato->profesion         = $request->profesion;
	    $dato->cargo             = $request->cargo;
	    $dato->jefe_inmediato    = $request->jefe_inmediato;
	    $dato->num_hijos         = $request->num_hijos;
	    $dato->n_a_pesos         = $request->n_a_pesos;
	    $dato->pais              = $request->pais;
	    $dato->departamento      = $request->departamento;
	    $dato->ciudad            = $request->ciudad;
	    $dato->fecha_nacimiento  = $request->fecha_nacimiento;
	    $dato->perfil            = $request->perfil;
	    $dato->empresa_id   = $request->empresa;
	    $dato->empresa_sede_id   = $request->empresa_sede_id;
	    if (!empty($request->responsable)) {
	    	$dato->user_id       = $request->responsable;
	    }else{
	    	$dato->user_id       = Auth::user()->id;
	    }
	    $dato->save();

	    foreach ($request->telefono as $key) {
	    	if (isset($key["telefono_numero"])) {
	    		$telefono = new ClienteTelefonos;
		    	$telefono->cliente_id = $dato->id;
			    $telefono->telefono_tipo = $key["telefono_tipo"];
				$telefono->telefono_indp = $key["telefono_indp"];
				$telefono->telefono_indc = $key["telefono_indc"];
				$telefono->telefono_numero = $key["telefono_numero"];
				$telefono->telefono_ext = $key["telefono_ext"];
				$telefono->save();
	    	}		    	
	    }

	    foreach ($request->redsocial as $key) {
	    	if (isset($key["valor"])) {
	    		$redsocial = new ClienteRedessociales;
		    	$redsocial->cliente_id = $dato->id;
			    $redsocial->tipo = $key["tipo"];
				$redsocial->valor = $key["valor"];
				$redsocial->save();
	    	}	    	
	    }

	    foreach ($request->correo as $key) {
	    	if (isset($key)) {
	    		$mail = new ClienteMails;
		    	$mail->cliente_id = $dato->id;
			    $mail->mail = $key;
				$mail->save();
	    	}	    	
	    }
	    return redirect('/crearcliente');
	}

	public function getComentarios($id){
    	$datos = ClienteComentario::where('cliente_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		$user = User::findOrFail($value->user_id);
    		$value->user = $user; 
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function saveComentario(Request $request)
    {
	    $dato = new ClienteComentario;
		$dato->comentario = $request->comentario;
		$dato->cliente_id = $request->cliente_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

	public function listAjax(){
        /**Permiso Exportar Excel y CSV**/
        $modulo=7;
        $data['permiso_exportar'] = "No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Exportar Excel y CSV" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_exportar'] = "Si";
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Clientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('clientes.listar',['data' => $data]);
    }

    public function listfil($id){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Clientes" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('clientes.viewfilter', ['data' => $data, 'id' => $id]);
    }

    public function listclient(){
    	$modulo=7;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="listar, mosaico" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Ver propio"){ 
				$permiso_listar="Ver propio";
			  }else{
				$permiso_listar="Ver todas";
			  }
            }
          }          
        }

        $permiso_editar='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar en listar y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){ 
				$permiso_editar="Si";
			  }else{
				$permiso_editar="No";
			  }
            }
          }          
        }

        $permiso_eliminar='No';
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
			  if($permiso2[0]->permiso == "Si"){ 
				$permiso_eliminar="Si";
			  }else{
				$permiso_eliminar="No";
			  }
            }
          }          
        }
        $query_porcentaje = '(IF(IsNull(C.foto), 0, (100/47)*5)+IF(IsNull(C.nombres), 0, (100/47)*1)+IF(IsNull(C.apellidos), 0, (100/47)*1)+IF(IsNull(E.nombre), 0, (100/47)*1)+IF(IsNull(S.ciudad), 0, (100/47)*1)+IF(IsNull(U.name), 0, (100/47)*1)+IF(IsNull(C.profesion), 0, (100/47)*2)+IF(IsNull(C.jefe_inmediato), 0, (100/47)*1)+IF(IsNull(C.n_a_pesos), 0, (100/47)*2)+IF(IsNull(C.tratamiento), 0, (100/47)*1)+IF(IsNull(C.estado_civil), 0, (100/47)*1)+IF(IsNull(C.cargo), 0, (100/47)*1)+IF(IsNull(C.fecha_nacimiento), 0, (100/47)*4)+IF(IsNull(C.num_hijos), 0, (100/47)*1)+IF(IsNull(C.pais), 0, (100/47)*1)+IF(IsNull(C.departamento), 0, (100/47)*1)+IF(IsNull(C.ciudad), 0, (100/47)*1)+IF(IsNull(C.perfil), 0, (100/47)*5)) AS Parte_porcentaje';
        $query_campos_faltante='CONCAT(IF(IsNull(C.foto), "* Foto&#10;", ""),IF(IsNull(C.nombres), "* Nombres&#10;", ""),IF(IsNull(C.apellidos), "* Apellidos&#10;", " "),IF(IsNull(E.nombre), "* Empresa&#10;", ""),IF(IsNull(S.ciudad), "* Sede&#10;", ""),IF(IsNull(U.name), "* Responsable&#10;", ""),IF(IsNull(C.profesion), "* Profesion&#10;", ""),IF(IsNull(C.jefe_inmediato), "* Jefe Inmediato&#10;", ""),IF(IsNull(C.n_a_pesos), "* Nivel de autorización en pesos&#10;", ""),IF(IsNull(C.tratamiento), "* Trato&#10;", ""),IF(IsNull(C.estado_civil), "* Estado Civil&#10;", ""),IF(IsNull(C.cargo), "* Cargo&#10;", ""),IF(IsNull(C.fecha_nacimiento), "* Fecha de nacimiento&#10;", ""),IF(IsNull(C.num_hijos), "* Numero de hijos&#10;", ""),IF(IsNull(C.pais), "* Pais Nacimiento&#10;", ""),IF(IsNull(C.departamento), "* Departamento de nacimiento&#10;", ""),IF(IsNull(C.ciudad), "* Ciudad de nacimiento&#10;", ""),IF(IsNull(C.perfil), "* Perfil&#10;", "")) AS Campos_faltante';
        $model = DB::select('SELECT C.id, C.foto, C.cliente, C.tratamiento, E.nombre, S.ciudad, C.cargo, C.perfil, C.n_a_pesos, U.name, '.$query_porcentaje.', '.$query_campos_faltante.', C.created_at, C.updated_at FROM clientes C LEFT JOIN empresas E ON E.id = C.empresa_id LEFT JOIN empresa_sedes S ON S.id = C.empresa_sede_id LEFT JOIN users U ON U.id = C.user_id WHERE C.deleted_at IS NULL');
    	$contador=0;
    	$valid_tags = [];
    	foreach ($model as $value) {  

    		if($permiso_listar=="Ver propio"){
    			if($value->name == Auth::user()->name){
    				$redes = DB::select('SELECT tipo, valor FROM cliente_redessociales WHERE cliente_id = '.$value->id);
                    $porcentaje_tel = DB::select('SELECT IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)) AS porcentaje, IF(COUNT(*)<2,"* Telefono personal o corporativo&#10;","") AS faltante FROM cliente_telefonos CT WHERE CT.cliente_id = '.$value->id);
                    $porcentaje_redes = DB::select('SELECT IF(COUNT(*)>0,(100/47)*4,0) AS porcentaje, IF(COUNT(*)=0,"* Redes Sociales&#10;","") AS faltante FROM cliente_redessociales CR WHERE CR.cliente_id = '.$value->id);
                    $porcentaje_correo = DB::select('SELECT IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)) AS porcentaje, IF(COUNT(*)<2,"* Correo personal o corporativo&#10;","") AS faltante FROM cliente_mails CM WHERE CM.cliente_id = '.$value->id);
                    $p = $value->Parte_porcentaje + $porcentaje_tel[0]->porcentaje + $porcentaje_redes[0]->porcentaje + $porcentaje_correo[0]->porcentaje;
                    $value->porcentaje = number_format($p, 0, ',', ' ');
                    $f = $value->Campos_faltante . $porcentaje_tel[0]->faltante . $porcentaje_redes[0]->faltante . $porcentaje_correo[0]->faltante;
                    $value->faltante = $f;
                    $value->faltante = !empty($value->faltante)?"Datos por completar:&#10;".substr($value->faltante, 0, -5):"No faltan datos";
					//\Carbon\Carbon::setLocale('es');
			       /* $dt2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at);
			        $dt3 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at);
			        $value->updated_at = $dt3->format('d/m/Y H:i');
			        $value->created_at = $dt2->format('d/m/Y H:i');*/
		    		$value->redes = $redes;
		    		$nombre   = explode(" ",$value->name);
		    		if (isset($nombre[2])) {
		    			$value->name = $nombre[0]." ".$nombre[2];
		    		}else{
		    			$value->name = $nombre[0]." ".$nombre[1];
		    		}
		    		$valid_tags[] = $value;

		    		$contador++;
    			}
    		}else{
    			$redes = DB::select('SELECT tipo, valor FROM cliente_redessociales WHERE cliente_id = '.$value->id);
                $porcentaje_tel = DB::select('SELECT IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)) AS porcentaje, IF(COUNT(*)<2,"* Telefono personal o corporativo&#10;","") AS faltante FROM cliente_telefonos CT WHERE CT.cliente_id = '.$value->id);
                $porcentaje_redes = DB::select('SELECT IF(COUNT(*)>0,(100/47)*4,0) AS porcentaje, IF(COUNT(*)=0,"* Redes Sociales&#10;","") AS faltante FROM cliente_redessociales CR WHERE CR.cliente_id = '.$value->id);
                $porcentaje_correo = DB::select('SELECT IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)) AS porcentaje, IF(COUNT(*)<2,"* Correo personal o corporativo&#10;","") AS faltante FROM cliente_mails CM WHERE CM.cliente_id = '.$value->id);
                $p = $value->Parte_porcentaje + $porcentaje_tel[0]->porcentaje + $porcentaje_redes[0]->porcentaje + $porcentaje_correo[0]->porcentaje;
                $value->porcentaje = number_format($p, 0, ',', ' ');
                $f = $value->Campos_faltante . $porcentaje_tel[0]->faltante . $porcentaje_redes[0]->faltante . $porcentaje_correo[0]->faltante;
                $value->faltante = $f;
                $value->faltante = !empty($value->faltante)?"Datos por completar:&#10;".substr($value->faltante, 0, -5):"No faltan datos";
				//\Carbon\Carbon::setLocale('es');
		       /* $dt2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at);
		        $dt3 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at);
		        $value->updated_at = $dt3->format('d/m/Y H:i');
		        $value->created_at = $dt2->format('d/m/Y H:i');*/
	    		$value->redes = $redes;
	    		$nombre   = explode(" ",$value->name);
	    		if (isset($nombre[2])) {
	    			$value->name = $nombre[0]." ".$nombre[2];
	    		}else{
	    			$value->name = $nombre[0]." ".$nombre[1];
	    		}
	    		$valid_tags[] = $value;

	    		$contador++;
    		}    		
    	}   

  		return \Response::json(['success' => true, 'model' => $valid_tags, 'contador' => $contador, 'permiso_editar' => $permiso_editar, 'permiso_eliminar' => $permiso_eliminar]);
    }

    public function listaclientes(){    	
    	$datos = Cliente::all();
  		return ['datos' => $datos];
    }

    public function listaclientesfil($id){    	
    	$model = DB::select('SELECT C.id, C.foto, C.cliente, C.tratamiento, E.nombre, S.ciudad, C.cargo, C.perfil, C.n_a_pesos, U.name, C.created_at, C.updated_at FROM clientes C INNER JOIN empresas E ON E.id = C.empresa_id INNER JOIN empresa_sedes S ON S.id = C.empresa_sede_id INNER JOIN users U ON U.id = C.user_id WHERE C.deleted_at IS NULL AND E.id ='.$id);
    	$valid_tags = [];
    	foreach ($model as $value) {    		
    		$redes = DB::select('SELECT tipo, valor FROM cliente_redessociales WHERE cliente_id = '.$value->id);
			//\Carbon\Carbon::setLocale('es');
	        /*$dt2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at);
	        $dt3 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at);
	        $value->updated_at = $dt3->format('d/m/Y H:i');
	        $value->created_at = $dt2->format('d/m/Y H:i');*/
    		$value->redes = $redes;
    		$nombre   = explode(" ",$value->name);
    		if (isset($nombre[2])) {
    			$value->name = $nombre[0]." ".$nombre[2];
    		}else{
    			$value->name = $nombre[0]." ".$nombre[1];
    		}
    		$valid_tags[] = $value;
    	}   

  		return \Response::json(['success' => true, 'model' => $valid_tags]);
    }
}
