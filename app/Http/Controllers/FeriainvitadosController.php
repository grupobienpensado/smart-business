<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\Cliente;
use App\Empresa;
use App\FeriasInvitado;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
date_default_timezone_set("America/Bogota");

class FeriainvitadosController extends Controller
{
    public function crearinviado($id){
        $dato = Feria::find($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.crearinvitados', ['data' => $data, 'id' => $id, 'dato' => $dato]);
    }

    public function consultarclientes(){
        $empresas = Empresa::all();
        $clientes = Cliente::all();
        ob_start(); ?>
<div class="row">
    <div class="col-md-12">
        <label>Elija una Empresa</label>
        <input class="form-control empresa" list="empresas">
        <datalist id="empresas">
        <?php foreach($empresas as $empresa){ ?>
            <option value="<?=$empresa->id?>" label="<?=$empresa->nombre?>">
        <?php } ?>
        </datalist>
    </div>
    <div class="col-md-10">
         <input type="text" id="empresa_mostrar" class="form-control" value="" style="display: none;" readonly="">
    </div>
    <div class="col-md-2">
         <i class="fa fa-pencil cambiar-empresa" style="display: none; color: orangered; font-size: x-large; cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar empresa"></i>
    </div>
    <div class="col-md-12">
        <label>Elija un Cliente</label>
        <input class="form-control cliente" list="clientes">
        <datalist id="clientes">

        </datalist>
    </div>
    <div class="col-md-10">
         <input type="text" id="cliente_mostrar" class="form-control" value="" style="display: none;" readonly="">
    </div>
    <div class="col-md-2">
         <i class="fa fa-pencil cambiar-cliente" style="display: none; color: orangered; font-size: x-large; cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar invitado"></i>
    </div>
    <div class="col-md-12">
        <label>Comentario</label>
        <textarea class="form-control" id="objetivo_invitacion" rows="4" placeholder="Escriba un comentario"></textarea>
    </div>
</div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return \Response::json(['html' => $html]);
    }

    public function consultarempresas($id){
        $empresa = Empresa::find($id);
        $clientes = DB::select('SELECT * FROM `clientes` WHERE `empresa_id`="'.$id.'" ORDER BY `nombres` ASC');
        ob_start();
        foreach($clientes as $cliente){ ?>
            <option value="<?=$cliente->id?>" label="<?=$cliente->nombres.' '.$cliente->apellidos?>">
        <?php }
        $data['content'] = ob_get_contents();
        ob_end_clean();
        $data['nombre_empresa'] = $empresa->nombre;

        return \Response::json(['data' => $data]);
    }

    public function consultarclientes2($id){
        $cliente = Cliente::find($id);
        return \Response::json(['nombre' => $cliente->nombres.' '.$cliente->apellidos]);
    }

    public function guardarinvitado(Request $request){
        $dato = new FeriasInvitado;
        $dato->tipo="Cliente";
        $dato->id_cliente=$request->id;
        $dato->id_feria=$request->id_feria;
        $dato->comentario=$request->motivo;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function guardarinvitadonuevo(Request $request){
        $dato = new FeriasInvitado;
        if (isset($request->foto_values)) {
            if(!empty($request->foto_values)){
                $imagen = json_decode($request->foto_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('feriasinvitados')->put($nombre,  $foto);
                $dato->foto = $nombre;
            }
        }
        $dato->tipo="Externo";
        $dato->nombre=$request->nombre;
        $dato->empresa=$request->empresa;
        $dato->cargo=$request->cargo;
        $dato->correo=$request->correo;
        $dato->telefono=$request->telefono;
        $dato->id_feria=$request->id_feria;
        $dato->comentario=$request->motivo_invitacion;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }
    public function editarinvitadocliente(Request $request){
        $dato = FeriasInvitado::find($request->id);
        $dato->id_cliente=$request->id_cliente;
        $dato->comentario=$request->motivo;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function eliminarinvitado($id){
        $dato = FeriasInvitado::find($id);
        if($dato->tipo=="Externo"){
            $foto=$dato->foto;
            if(!empty($foto)){
                 Storage::delete('ferias/invitados/'.$foto);
            }
        }
        $dato->delete();
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }

     public function editarinvitado($id_feria, $id_invitado){
        $dato = Feria::find($id_feria);
        $invitado = FeriasInvitado::find($id_invitado);
         /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.editarinvitado', ['data' => $data, 'id' => $id_feria, 'dato' => $dato, 'invitado' => $invitado]);
    }

    public function editarinvitadonuevo(Request $request){
        $id = $request->id_invitado;
        $dato = FeriasInvitado::find($id);
        if(!empty($request->foto_values)){
            $foto_anterior = $dato->foto;
            if(!empty($foto_anterior)){
                Storage::delete('ferias/invitados/'.$foto_anterior);
            }
            $imagen = json_decode($request->foto_values);
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);
            $antenombre = sha1(date("Y-m-d H:i:s"));
            $nombre =$antenombre.".jpg";
            \Storage::disk('feriasinvitados')->put($nombre,  $foto);
            $dato->foto = $nombre;
        }
        $dato->tipo="Externo";
        $dato->nombre=$request->nombre;
        $dato->empresa=$request->empresa;
        $dato->cargo=$request->cargo;
        $dato->correo=$request->correo;
        $dato->telefono=$request->telefono;
        $dato->id_feria=$request->id_feria;
        $dato->comentario=$request->motivo_invitacion;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }
}
