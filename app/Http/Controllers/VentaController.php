<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Venta;
use App\Oportunidades;
use App\HistorialOportunidades;
use App\Actividades;
use App\Cliente;
use App\Empresa;
use App\Ventas_archivo;
use App\Ventas_archivos_comentario;
use App\User;
use App\Apu;
use App\Gasto;
use App\Otras_maquina;
use App\Ventas_liquidacione;
use App\Agenda_cliente;
use App\Actividades_detalles;
use App\CategoriaNoArchivo;
use App\Ciclo;
use App\ApuHistoria;
use App\ApuEquipo;
use App\Producto;
use App\ProductoReferencias;
use App\ApuComentario;
use App\ApuOtrosGastos;
use App\ViewFreelanceLista;
use Auth;

use App\Historial_oportunidades_archivo;
use App\Historial_oportunidades_comentario;
use App\Oportunidades_ingreso;
use App\OportunidadesCompetencias;

class VentaController extends Controller
{
    public function findsale($id){
    	$venta = Venta::findOrFail($id);
    	$datos = Oportunidades::findOrFail($venta->oportunidad);
    	$historia = HistorialOportunidades::where('oportunidad_id', $venta->oportunidad)->get();
    	$actividades = Actividades::where('oportunidad_id', $venta->oportunidad)->get();
        //$liquidaciones = Ventas_liquidacione::all();
    	$otro = $historia->filter(function ($value, $key) {
          return $value->key == "cliente";                            
        });
        $casi = $otro->pop();
        $cliente = Cliente::findOrFail($casi->value); 
    	
        $empresa = Empresa::findOrFail($datos->empresa_id);
    	$ventas_archivos=Ventas_archivo::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ventas.venta',['data' => $data, 'datos' => $historia,'oportunidad' => $datos,'cliente' => $cliente,'empresa' => $empresa,'actividades' => $actividades,"id"=>$id,'archivos'=>$ventas_archivos,"venta"=>$venta]);
    }

    public function upload_sale(Request $request){
    	$logo = $request->file('file');
    	$antenombrelogo = uniqid();
		$extensionlogo = $logo->getClientOriginalExtension();
		$nombrelogo =$antenombrelogo.".".$extensionlogo;
		\Storage::disk($request->carpeta)->put($nombrelogo,  \File::get($logo));

		$new= new Ventas_archivo();
    	$new->ventas=$request->id;
    	$new->nombre=$request->nombre;
    	$new->carpeta=$request->carpeta;
    	$new->archivo=$nombrelogo;
    	$new->descripcion=$request->descripcion;
    	$new->user_id = Auth::user()->id;
    	$new->save();
    	
    	return redirect("/venta/".$request->id);
    }

    public function file(){
    	$id=$_GET["id"];

    	$new=Ventas_archivo::findOrFail($id);
    	list($nombre,$ext)=explode(".", $new->archivo);
        if($ext=="jpg"||$ext=="JPG"){
            $imagen="jpg.png";
        }elseif($ext=="png"||$ext=="PNG"){
            $imagen="png.png";
        }elseif($ext=="DOC"||$ext=="doc"||$ext=="docx"||$ext=="DOCX"){
            $imagen="word-logo.png";
        }elseif($ext=="pdf"||$ext=="PDF"){
            $imagen="pdf-logo.png";
        }elseif($ext=="ppt"||$ext=="PPT"){
            $imagen="ppt-logo.png";
        }elseif($ext=="xls"||$ext=="XLS"||$ext=="XLSX"||$ext=="xlsx"){
            $imagen="excel-logo.png";
        }else{
            $imagen="file.png";
        }
        $usuario=User::findOrFail($new->user_id);
    	$lista=Ventas_archivos_comentario::all();

    	$lista=ordenar($lista);
    	$content='';
    	$content.='<div class="row form-group">
            <div class="col-md-6" style="border-right: 0.5px solid #ccc;">
                <div>
                    <div class="subidor" style="background-image: url(../../images/icons/'.$imagen.'); display: inline-block;"></div>
                    <div class="info-file">
                        <label class="black-color"><span style="font-weight:bold">Nombre:</span> '.$new->nombre.'</label>
                        <label class="black-color justificado"><span style="font-weight:bold">Descripcion:</span> <span class="block">'.$new->descripcion.'</span></label>
                        <label class="black-color"><span style="font-weight:bold">Subido El:</span> <span>'.$new->created_at.'</span></label>
                    </div>
                </div>
                <div>
                    <div class="caja-comentarios">';
                        foreach ($lista as $key) {
                        	# code...
                        	if($key->ventas_archivos==$id){
                        		$comentador=User::findOrFail($key->user_id);
                        		$content.='<div class="comment">
                        		<div class="foto-comenta" style="background-image: url(../..//images/file/clientes/'.$comentador->foto.')"></div>
                        		<div style="display:inline-block">
	                            <div style="font-weight: bold; line-height: 1">'.ucwords($comentador->nombres." ".$comentador->apellidos).' <span>escribió</span></div>
	                            <div style="line-height: 1">'.$key->comentario.'</div>
	                            <span style="font-size: 12px;">'.$key->created_at.'</span>
	                            </div>
	                        </div>';
                        	}
                        	
                        }
                        
                    $content.='</div>
                    <div class="form-group">
                    <form enctype="multipart/form-data" method="{{url("guardar_comentario_archivo")}}" id="saveComenario" type="POST">
                    '.csrf_field().'
                    <input type="hidden" value="'.$id.'" name="id">
                        <div>
                            <textarea class="form-control" name="comentario" placeholder="escriba aqui un comentario"></textarea>
                        </div>
                        <a class="btn btn-essi pull-right" href="javascript:guardarComentario()"><i class="fa fa-send-o"></i> Comentar</a>
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="text-align: center;position: relative;">
                <p>Subido Por</p>
                <div class="subidor-foto" style="background-image: url(../..//images/file/clientes/'.$usuario->foto.')"></div>
                <label class="block">'.ucwords($usuario->nombres." ".$usuario->apellidos).'</label>
                <button type="button" class="btn btn-default margen-50 detalleModal" onclick="openDocument(`'.$new->carpeta.'`,`'.$new->archivo.'`,'.$id.',`'.$ext.'`)"><i class="fa fa-eye"></i> Ver Archivo</button><span style="display: inherit;">'.$new->contador.' veces visto </span>
            </div>
        </div>';

        return ["contenido"=>$content];
    }

    public function savecomments(Request $request){

    	$datos= new Ventas_archivos_comentario();
    	$datos->ventas_archivos=$request->id;
    	$datos->comentario=$request->comentario;
    	$datos->user_id = Auth::user()->id;
    	$datos->save();

    	$lista=Ventas_archivos_comentario::all();

    	$lista=ordenar($lista);
    	$content='';
    	foreach ($lista as $key) {
        	if($key->ventas_archivos==$request->id){
        		$comentador=User::findOrFail($key->user_id);
        		$content.='<div class="comment">
                <div style="font-weight: bold; line-height: 1">'.ucwords($comentador->nombres." ".$comentador->apellidos).' <span>escribió</span></div>
                <div style="line-height: 1">'.$key->comentario.'</div>
                <span style="font-size: 12px;">'.$key->created_at.'</span></div>';
        	}    	
        }

    	return \Response::json(['success' => true,'contenido'=>$content]);
    }

    public function visitors($id){
    	$new=Ventas_archivo::findOrFail($id);
    	$new->contador=$new->contador+1;
    	$new->save();
    }

    public function liquidador($id){
        $venta = Venta::findOrFail($id);
        $datos = Oportunidades::findOrFail($venta->oportunidad);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("ventas.liquidador",["data" => $data, "oportunidad"=>$datos,"id"=>$id]);
    }

    public function guardarliquidacion(Request $request){
        foreach ($request->datos as $key) {
            if($key["si"]=="si"){
                $datos=new Ventas_liquidacione();
                $datos->ventas=$request->id;
                $datos->detalles=$key["detalles"];
                $datos->valor=$key["valor"];
                $datos->observaciones=$key["observaciones"];
                $datos->user_id = Auth::user()->id;
                $datos->save();
            }
        }
        return redirect("/venta/".$request->id);
    }
    // Edwin/: Esta es la funcion que guarda los APU y reactiva el listado 
    public function saveapu(Request $request)
    {
        $input  = $request->all();
        $dato = new ApuHistoria;
        $dato->user_id=Auth::user()->id;
        foreach ($input as $key => $value) {
            if ($key !== "productos" && $key !== "otrogasto" && $key !== "_token") {
                $dato->$key=$value;
            }            
        }
        $dato->save();
        $id = $dato->id;

        if(isset($input["otrogasto"])){
            foreach ($input["otrogasto"] as $otro) {
                $dato1 = new ApuOtrosGastos;
                foreach($otro as $index => $value){
                    if($index == 'valor'){
                        $value = intval(str_replace(",", "", $value));
                    }
                    $dato1->$index = $value;
                }
                $dato1->id_apu = $id;
                $dato1->save();
            }
        }
        foreach ($input["productos"] as $value) {
            if (isset($value["producto"]) && !empty($value["producto"])) {
                $equipo = new ApuEquipo;
                $equipo->apu_id=$dato->id;
                foreach ($value as $key2 => $value2) {
                    $equipo->$key2=$value2;
                } 
                $equipo->save();    
            }       
        }   

        return \Response::json(['success' => true]);
    }
    // Edwin/: Funciones de guardar y mostar comentarios
    public function getComentarios($id){
    	$datos = ApuComentario::where('oportunidad_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		$user = User::findOrFail($value->user_id);
    		$value->user = $user;
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function saveComentario(Request $request){
	    $dato = new ApuComentario;
		$dato->comentario = $request->comentario;
		$dato->oportunidad_id = $request->oportunidad_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

    // Edwin/: Esta es la funcion que lista todos los APU al regargar la pagina
    public function listapu($id)
    {
        $objAPU = [];
        $historialAPU = ApuHistoria::where("oportunidad_id",$id)->get()->sortByDesc('id');
        foreach ($historialAPU as $value) {
            $equiposAPU = ApuEquipo::where("apu_id",$value->id)->get();

            for ($i=0; $i < count($equiposAPU); $i++) { 
                if (!empty($equiposAPU[$i]->producto)) { 
                  if (is_numeric($equiposAPU[$i]->producto)) {
                    $producto = Producto::where('id',"=", $equiposAPU[$i]->producto)->get()->pop();
                    $equiposAPU[$i]->producto = $producto->name;
                  }else{
                    $equiposAPU[$i]->producto = "Otro";
                  }
                  if (is_numeric($equiposAPU[$i]->referencia)) {
                    $referencia = ProductoReferencias::where('id',"=", $equiposAPU[$i]->referencia)->get()->pop();
                    $equiposAPU[$i]->referencia = $referencia->referencia; 
                  }else{
                    $equiposAPU[$i]->referencia = "Estandar";
                  }
                } 
            }
            $otros_gastos = ApuOtrosGastos::where('id_apu',$value->id)->get();
            ob_start(); ?>
            <tr style="background-color: #909090;color: #fff;">
                <td colspan="3"><strong>Otros Gastos</strong></td>
            </tr>
            <?php $i=0; foreach($otros_gastos as $otro){ $i++;?>
            <tr>
                <td><?=$i?></td>
                <td><?=$otro->concepto?></td>
                <td class="dinero">$ <?=number_format($otro->valor)?></td>
            </tr>
            <?php } ?>
            <?php if(count($otros_gastos)==0){ ?>
            <tr>
                <td colspan="3"><strong><em>No hay registro de otros gastos</em></strong></td>
            </tr>
            <?php
            }
            $data['otros_gastos'][$value->id] = ob_get_contents();
            ob_end_clean();

            $value->equipos = $equiposAPU;
            $objAPU[] = $value;

        }
        return \Response::json(['success' => true, 'datos'=>$objAPU, 'data' => $data]);
    }

    // Edwin/: 
    public function index($flag = null){
        $valUseId = Auth::user()->id; 
        $historial = [];

        $modulo=3;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="listado oportunidades vigentes, vendidas, perdidas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver propio"){
                  /*Consulta para ver las oportunidades del propietario (owner)*/
                $ventas=Venta::where('user_id',"=", Auth::user()->id)->get();
                foreach ($ventas as $value) {
                    $oportunidad = Oportunidades::find($value->oportunidad);
                    if (isset($oportunidad) && !empty($oportunidad)) {
                        if($flag){
                            /*Histórico*/
                            $historia = HistorialOportunidades::where('oportunidad_id', $value->oportunidad)->get();
                            $oportunidad->venta_id = $value->id;
                            $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                        }else{
                            /*Año actual*/
                            $date = date("Y");
                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$value->oportunidad.' ORDER BY H.id DESC LIMIT 0,1';
                            $FECHA_CIERRE = DB::select($query);
                            if(isset($FECHA_CIERRE[0]->value) && date('Y', strtotime($FECHA_CIERRE[0]->value)) == $date){
                                $historia = HistorialOportunidades::where('oportunidad_id', $value->oportunidad)->get();
                                $oportunidad->venta_id = $value->id;
                                $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                            }
                        }
                    }
                }
              }else{
                  /*Trabajar la consulta acá, cuando se es admin*/
                $ventas=Venta::all();
                foreach ($ventas as $value){
                    $oportunidad = Oportunidades::find($value->oportunidad);
                    if (isset($oportunidad) && !empty($oportunidad)) {
                        if($flag){
                            /*Histórico*/
                            $historia = HistorialOportunidades::where('oportunidad_id', $value->oportunidad)->get();
                            $oportunidad->venta_id = $value->id;
                            $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                        }else{
                            /*Año actual*/
                            $date = date("Y");
                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.key LIKE "fecha_oc" AND H.oportunidad_id = '.$value->oportunidad.' ORDER BY H.id DESC LIMIT 0,1';
                            $FECHA_CIERRE = DB::select($query);
                            if(isset($FECHA_CIERRE[0]->value) && date('Y', strtotime($FECHA_CIERRE[0]->value)) == $date){
                                $historia = HistorialOportunidades::where('oportunidad_id', $value->oportunidad)->get();
                                $oportunidad->venta_id = $value->id;
                                $historial[] = ['oportunidad'=>$oportunidad, "historial"=>$historia];
                            }
                        }
                    }
                }
              }
            }
          }
        }

        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar de los listados y en los ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "Si"){ 
                $permiso_editar='Si';
              }else{
                $permiso_editar='No';
              }
            }
          }          
        }
        
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón añadir oportunidad en listados" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Si"){ 
                $permiso_anadir='Si';
              }else{
                $permiso_anadir='No';
              }
            }
          }          
        }

        /*Permiso para exportar a Excel y CSV*/
        $data['permiso_exportar']='No';
        $modulo = 4;
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Exportar Excel y CSV" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Si"){
                $data['permiso_exportar']='Si';
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
            return view("ventas.ventas",['data' => $data, 'datos' => $historial, 'permiso_editar'=>$permiso_editar, 'permiso_anadir'=>$permiso_anadir]);
    }

    public function viewsale($id){
        

        $venta = Venta::find($id);
        $data['venta'] = $venta;
        $datos = Oportunidades::find($venta->oportunidad);
        $data['competencias_oportunidad'] = OportunidadesCompetencias::where('id_oportunidad', $datos->id)->get();
        ob_start();
        if($datos->id_user_freelance != ''){
            $freelance = ViewFreelanceLista::find($datos->id_user_freelance);
            if(isset($freelance->id)){
?>
<div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="<?=$FOTO=$freelance->FOTOPERFIL!=''?$freelance->FOTOPERFIL:'http://via.placeholder.com/250x250';?>" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading"><?=$freelance->NOMBRECORTO?> </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php }else{ ?>
<div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/250x250" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading">No hay un freelance asignado </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php } }else{ ?>
 <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
    <a class="media-left waves-light">
        <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/250x250" alt="Usuario" style="background: none;">
    </a>
    <div class="media-body">
        <h4 class="media-heading">No hay un freelance asignado </h4>
        <p class="subtitulo"> Freelance</p>
    </div>
</div>
        <?php }
        $data['freelance'] = ob_get_contents();
        ob_end_clean();
        $historia = HistorialOportunidades::where('oportunidad_id', $venta->oportunidad)->get();
        $actividades = Actividades::where('oportunidad_id', $venta->oportunidad)->get()->sortByDesc('updated_at');
        $otro = $historia->filter(function ($value, $key) {
          return $value->key == "cliente";                            
        });
        $new= new Oportunidades_ingreso();
        $new->ip=ObtenerIP();
        $new->oportunidad_id=$venta->oportunidad;
        $new->user_id = Auth::user()->id;
        $new->save();

        $casi = $otro->pop();
        $cliente = Cliente::findOrFail($casi->value);         
        $empresa = Empresa::findOrFail($datos->empresa_id);
        $agentclient = Agenda_cliente::where('oportunidad', $venta->oportunidad)->get();
        $actidetalle = Actividades_detalles::where('oportunidad_id', $venta->oportunidad)->get();

        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar de los listados y en los ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
              if($permiso1[0]->permiso == "Si"){ 
                $permiso_editar='Si';
              }else{
                $permiso_editar='No';
              }
            }
          }          
        }
        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$id.' AND H.key LIKE "cliente" ORDER BY H.id DESC LIMIT 0,1';
        $responsable = DB::select($query);
        if(isset($responsable[0]->value)){
            $c = Cliente::find($responsable[0]->value);
            $data['cliente_responsable'] = $c->cliente;
        }else{
            $data['cliente_responsable'] = 'No existe cliente responsable';
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        /*Permiso para acceder APU*/
        $data['permiso_apu_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder APU*/

        /*Permiso para acceder Actas*/
        $data['permiso_actas_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_actas_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/
        return view('ventas.view2',['data' => $data, 'datos' => $historia,'oportunidad' => $datos,'cliente' => $cliente,'empresa' => $empresa,'actividades' => $actividades,'agentclients' => $agentclient, 'actidetalles' => $actidetalle, "id"=>$id, 'permiso_editar' => $permiso_editar]);

    }

    public function viewimportanfile($id){
        $categoriaarchi = CategoriaNoArchivo::all();
        $data['venta'] = Venta::find($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        $modulo = 4;
        $data['permiso_cargar'] = "No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Archivos Importantes - Cargar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_cargar']='Si';
			  }
            }
          }
        }

        /*Permiso para acceder APU*/
        $data['permiso_apu_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder APU*/

        /*Permiso para acceder Actas*/
        $data['permiso_acta_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_acta_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/

        /*Permiso Archivos Importantes - Acceder*/
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Archivos Importantes - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				return view('ventas.fileimportant',["data" => $data, "datos"=>$categoriaarchi, "id"=>$id]);
			  }else{
                return view('oportunidades.nopermiso');
              }
            }
          }
        }
    }

    public function viewciclosventa($id){
        $venta = Venta::find($id);
        $data['venta'] = $venta;
        $ciclos=Ciclo::all();
        $dato = Oportunidades::find($venta->oportunidad);
        $historia = HistorialOportunidades::where('oportunidad_id', $venta->oportunidad)
                                            ->where('key', 'ciclo_venta')->get()->pop();
        $historias = HistorialOportunidades::where('oportunidad_id', $venta->oportunidad)
                                            ->where('key', 'ciclo_venta')
                                            ->where('id', "!=", $historia->id)->get();
        $ingresos=Oportunidades_ingreso::where('oportunidad_id', $venta->oportunidad)->get()->sortBy('created_at')->reverse();

        /*Permiso para acceder APU*/
        $data['permiso_apu_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder APU*/

        /*Permiso para acceder Actas*/
        $data['permiso_acta_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_acta_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        return view('ventas.ciclosventa',['data' => $data, 'dato' => $dato,"historia"=>$historia,"historias"=>$historias,"ciclos"=>$ciclos,"ingresos"=>$ingresos, "id"=>$id]);
    }

    public function apu($id){
        $venta = Venta::find($id);
        $data['venta'] = $venta;
        $oportunidad = Oportunidades::find($venta->oportunidad);
        $datos = HistorialOportunidades::where('oportunidad_id', $venta->oportunidad)->get();
        $otro = $datos->filter(function ($value, $key) {
          return $value->key == "cliente";                            
        });
        $casi = $otro->pop();
        try {  
            $cliente = Cliente::find($casi->value); 
        } catch (Exception $e) {
            
        }
        
        $empresa = Empresa::find($oportunidad->empresa_id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para acceder APU*/
        $data['permiso_apu_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder APU*/

        /*Permiso para acceder Actas*/
        $data['permiso_acta_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_acta_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/

        /*Permiso para crear un APU*/
        $data['permiso_apu_crear']='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - crear" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_crear']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para crear un APU*/
        if($data['permiso_apu_acceder'] == "Si"){
        return view('ventas.apuhistorial',
            [   
                'data'       => $data,
                'datos'       => $datos,
                'oportunidad' => $oportunidad,
                'cliente'     => $cliente,
                'empresa'     => $empresa,
                "id"          => $id
            ]
        );
        }else{
             return view('oportunidades.nopermiso');
        }
    }

    public function gestion($id){
        $query = ('SELECT * FROM historial_oportunidades H WHERE H.oportunidad_id = '.$id.' AND H.key LIKE "comentario_gestiones" ORDER BY H.id ASC');
        $data['comentarios'] = DB::select($query);
        $data['venta'] = Venta::where('oportunidad',$id)->get();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Oportunidades" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para crear un comentario*/
        $modulo=3;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear comentario en calendario de gestion" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_crear_comentario']='Si';
			  }else{
				$data['permiso_crear_comentario']='No';
			  }
            }
          }
        }
        /*Fin Permiso para crear un comentario*/
        /*Permiso para acceder APU*/
        $data['permiso_apu_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_apu_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder APU*/
        /*Permiso para acceder Actas*/
        $data['permiso_acta_acceder']='No';
        $modulo=4;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_acta_acceder']='Si';
			  }
            }
          }
        }
        /*Fin Permiso para acceder Actas*/
      return view('ventas.gestion',['data' => $data, 'id' => $id]);
    }
}

function ordenar($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["id"]<$array[$j+1]["id"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }
 
    return $array;
}

function ObtenerIP() {
   $ip = "";
   if(isset($_SERVER))
   {
       if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
       {
           $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
   }
   else
   {
        if ( getenv( 'HTTP_CLIENT_IP' ) )
        {
            $ip = getenv( 'HTTP_CLIENT_IP' );
        }
        elseif( getenv( 'HTTP_X_FORWARDED_FOR' ) )
        {
            $ip = getenv( 'HTTP_X_FORWARDED_FOR' );
        }
        else
        {
            $ip = getenv( 'REMOTE_ADDR' );
        }
   }  
    // En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma 
   if(strstr($ip,','))
   {
        $ip = array_shift(explode(',',$ip));
   }
   return $ip;
}
