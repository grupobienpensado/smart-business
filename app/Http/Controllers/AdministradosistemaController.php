<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Oportunidad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
use Carbon\Carbon;
date_default_timezone_set("America/Bogota");


class AdministradosistemaController extends Controller
{
    public function view(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Administrador Sistema" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('administrador.administradorsistema',['data' => $data]);
    }


    public function action(Request $request){

        $accion = $request->accion;
        $fecha_inicial = $request->fecha_inicial;
        $fecha_final = $request->fecha_final;
        $fecha_ff = $request->fecha_ff;
        $fecha_oc = $request->fecha_oc;
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        switch($accion){
            /*oportunidades identificadas*/
            case 0:
                $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Gerente Comercial" OR U.cargo LIKE "Partner Solution" OR U.cargo LIKE "Director General") AND U.deleted_at IS NULL';
                $usuarios = DB::select($query);
                foreach($usuarios as $usuario){
                    $query = 'SELECT DISTINCT(H.oportunidad_id) FROM `historial_oportunidades` H WHERE H.key LIKE "responsable" AND H.value LIKE "'.$usuario->Id_user.'" ORDER BY `H`.`oportunidad_id` ASC';
                    $oportunidades = DB::select($query);
                    try {
                        foreach($oportunidades as $oportunidad){
                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "responsable" ORDER BY H.updated_at DESC LIMIT 0,1';
                            $responsable = DB::select($query);
                            if($responsable[0]->value == $usuario->Id_user){
                                $query = 'SELECT H.value AS ciclo FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1';
                                $ciclo = DB::select($query);
                                if(intval($ciclo[0]->ciclo) >= 10 && intval($ciclo[0]->ciclo) <= 80){
                                   /*Columna 1 - Dias sin identificar*/
                                   $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $fecha_identificacion = DB::select($query);
                                    if(isset($data['fecha_oportunidad_reciente'][$usuario->Id_user])){
                                        if($data['fecha_oportunidad_reciente'][$usuario->Id_user] < $fecha_identificacion[0]->value){
                                            $data['fecha_oportunidad_reciente'][$usuario->Id_user] = $fecha_identificacion[0]->value;
                                            $data['numero_dias'][$usuario->Id_user] = $this->dateDiff($fecha_identificacion[0]->value);
                                        }
                                    }else{
                                        $data['fecha_oportunidad_reciente'][$usuario->Id_user] = $fecha_identificacion[0]->value;
                                        $data['numero_dias'][$usuario->Id_user] = $this->dateDiff($fecha_identificacion[0]->value);
                                    }
                                    /*Columna 2 - Proximo Cierre*/
                                    $query = 'SELECT H.value, SUBSTRING(O.titulo, 12,1000) AS titulo FROM historial_oportunidades H INNER JOIN oportunidades O ON H.oportunidad_id = O.id  WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $fecha_cierre = DB::select($query);
                                    if(isset($data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user])){
                                        if($data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user] > $fecha_cierre[0]->value && date('Y-m-d') <= $fecha_cierre[0]->value){
                                            $data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user] = $fecha_cierre[0]->value;
                                            $data['nombre_cierre_oportunidad_cercana'][$usuario->Id_user] = $fecha_cierre[0]->titulo;
                                            /*valor cierre opp */
                                            $query = 'SELECT CONCAT(FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $valor_oportunidad = DB::select($query);
                                            $data["valor_cierre_oportunidad_cercana"][$usuario->Id_user] = $valor_oportunidad[0]->value;

                                        }
                                    }else if(date('Y-m-d') <= $fecha_cierre[0]->value){
                                        $data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user] = $fecha_cierre[0]->value;
                                        $data['nombre_cierre_oportunidad_cercana'][$usuario->Id_user] = $fecha_cierre[0]->titulo;
                                        $query = 'SELECT CONCAT(FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad = DB::select($query);
                                        $data["valor_cierre_oportunidad_cercana"][$usuario->Id_user] = isset($valor_oportunidad[0]->value)?$valor_oportunidad[0]->value:0;

                                    }
                                    /*Columna 3 - Identificada Rango Fecha*/
                                    if(!empty($fecha_inicial) && !empty($fecha_final)){

                                        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $filtrada = DB::select($query);
                                        if(isset($filtrada[0]->value)){
                                            if(date('Y-m-d',strtotime($filtrada[0]->value)) >= $fecha_inicial && date('Y-m-d',strtotime($filtrada[0]->value)) <= $fecha_final){
                                                $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                $valor_oportunidad = DB::select($query);
                                                if(isset($data["identificadas_filtrada"][$usuario->Id_user])){
                                                    $data["identificadas_filtrada"][$usuario->Id_user]++;
                                                    $data["identificadas_filtrada_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                }else{
                                                    $data["identificadas_filtrada"][$usuario->Id_user] = 1;
                                                    $data["identificadas_filtrada_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                }
                                            }
                                        }

                                    }else{
                                        $dia = date('d');
                                        if($dia <= 15){
                                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $quincenal = DB::select($query);
                                            if(isset($quincenal[0]->value)){
                                                if(date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) >= date('Y-m-01 00:00:00') && date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) <= date('Y-m-15 00:00:00')){
                                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                    $valor_oportunidad = DB::select($query);
                                                    if(isset($data["primera_quincena"][$usuario->Id_user])){
                                                        $data["primera_quincena"][$usuario->Id_user]++;
                                                        $data["primera_quincena_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                    }else{
                                                        $data["primera_quincena"][$usuario->Id_user] = 1;
                                                        $data["primera_quincena_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                    }
                                                }
                                            }
                                        }else{
                                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $quincenal = DB::select($query);
                                            if(isset($quincenal[0]->value)){
                                                if(date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) >= date('Y-m-16 00:00:00') && date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) <= date('Y-m-31 00:00:00')){
                                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                    $valor_oportunidad = DB::select($query);
                                                    if(isset($data["segunda_quincena"][$usuario->Id_user])){
                                                        $data["segunda_quincena"][$usuario->Id_user]++;
                                                        $data["segunda_quincena_valor"][$usuario->Id_user]+=isset($valor_oportunidad[0]->value)?$valor_oportunidad[0]->value:0;
                                                    }else{
                                                        $data["segunda_quincena"][$usuario->Id_user] = 1;
                                                        $data["segunda_quincena_valor"][$usuario->Id_user]=isset($valor_oportunidad[0]->value)?$valor_oportunidad[0]->value:0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    /*Columna 4 - Identificadas por mes actual*/
                                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $por_mes = DB::select($query);
                                    $mes = date('Y-m');
                                    if(date('Y-m',strtotime($por_mes[0]->value)) == $mes){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_porMes = DB::select($query);
                                        if(isset($data["identificada_por_mesActual"][$usuario->Id_user])){
                                            $data["identificada_por_mesActual"][$usuario->Id_user]++;
                                            $data["identificada_por_mesActual_valor"][$usuario->Id_user]+=isset($valor_oportunidad_porMes[0]->value)?$valor_oportunidad_porMes[0]->value:0;
                                        }
                                        else {
                                            $data["identificada_por_mesActual"][$usuario->Id_user] = 1;
                                            $data["identificada_por_mesActual_valor"][$usuario->Id_user]=isset($valor_oportunidad_porMes[0]->value)?$valor_oportunidad_porMes[0]->value:0;
                                        }
                                    }
                                    /*Columna 5 - Identificadas por año actual*/
                                     $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $por_ano = DB::select($query);
                                    $ano = date('Y');
                                    if(date('Y',strtotime($por_ano[0]->value)) == $ano){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_porAno = DB::select($query);
                                        if(isset($data["identificada_por_añoActual"][$usuario->Id_user])){
                                            $data["identificada_por_añoActual"][$usuario->Id_user]++;
                                            $data["identificada_por_añoActual_valor"][$usuario->Id_user]+=isset($valor_oportunidad_porAno[0]->value)?$valor_oportunidad_porAno[0]->value:0;
                                        }
                                        else {
                                            $data["identificada_por_añoActual"][$usuario->Id_user] = 1;
                                            $data["identificada_por_añoActual_valor"][$usuario->Id_user]=isset($valor_oportunidad_porAno[0]->value)?$valor_oportunidad_porAno[0]->value:0;
                                        }
                                    }
                                    /*Columna 6 - Identificadas vigentes*/
                                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $vigentes = DB::select($query);
                                    if($vigentes[0]->value){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_vigente = DB::select($query);
                                        if(isset($data["identificada_por_vigentes"][$usuario->Id_user])){
                                            $data["identificada_por_vigentes"][$usuario->Id_user]++;
                                            $data["identificada_por_vigentes_valor"][$usuario->Id_user]+=isset($valor_oportunidad_vigente[0]->value)?$valor_oportunidad_vigente[0]->value:0;
                                        }
                                        else {
                                            $data["identificada_por_vigentes"][$usuario->Id_user] = 1;
                                            $data["identificada_por_vigentes_valor"][$usuario->Id_user]=isset($valor_oportunidad_vigente[0]->value)?$valor_oportunidad_vigente[0]->value:0;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        $data['error'] = $e;
                    }

                }
                /*Columna 7 - Identificadas % de Total*/
                $total_vigentes = 0;
                foreach($data['identificada_por_vigentes_valor'] as $valor_vigente){
                     $total_vigentes+=$valor_vigente;
                }
                foreach($data['identificada_por_vigentes_valor'] as $index => $value){
                     $data['total_vigentes'][$index]=number_format(($value/$total_vigentes*100), 0, ',', ' ');
                }

                if(!empty($request->fecha_inicial) && !empty($request->fecha_final)){
                    $dia_inicial = date('d',strtotime($request->fecha_inicial));
                    $mes_inicial = substr($meses[intval(date('m',strtotime($request->fecha_inicial)))], 0, 3);
                    $dia_final = date('d',strtotime($request->fecha_final));
                    $mes_final = substr($meses[intval(date('m',strtotime($request->fecha_final)))], 0, 3);
                    if(date("Y",strtotime($request->fecha_inicial)) != date("Y",strtotime($request->fecha_final))){
                        $mes_inicial.=' '.date("Y",strtotime($request->fecha_inicial));
                        $mes_final.=' '.date("Y",strtotime($request->fecha_final));
                    }
                }else{
                    $dia = intval(date('m'));
                    if($dia <= 15){
                        $dia_inicial = 1;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = 15;
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }else{
                        $dia_inicial = 16;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = cal_days_in_month(CAL_GREGORIAN, date('m'),date('Y'));
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }
                }
                $rango_fechas = $dia_inicial.' '.$mes_inicial.' - '.$dia_final.' '.$mes_final;
                ob_start(); ?>
    <div class="row mt-5 ml-5 mr-5 bg-white border-round shadow ">
        <div class="col-md-12">
            <p class="h2 color-title">Oportunidades Identificadas</p>
        </div>
        <div class="col-md-12 text-right">
            <img src="<?=$request->dir?>/images/icons/calendar.svg" id="ver_ciclo" class="printer mr-3" data-toggle="tooltip" data-placement="top" title="Ver Oportunidad">
            <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir" id="capture">
        </div>
        <!-- Headers Opp Identificadas -->
        <div class="col-md-6 mt-3 mb-3">
            <div class="row mt-3 mb-3 ml-3 bg-blue h-100 border-round-left color-white">
                <div class="col-md-6 mt-3 mb-3">
                    <div class="row margin-media">
                        <div class="col-md-12">
                            <a class="h6">Dias sin identificar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <a class="h6">Proximo Cierre</a>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <a class="h6">Identificada <?=$rango_fechas?></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 mt-3 mb-3 e-identificadas">
            <div class="row mt-3 mb-3 mr-3 h-100 bg-blue border-round-right color-white">
                <div class="col-md-3 mt-3 mb-3">
                    <a class="h6">Identificadas <?=$meses[intval(date('m'))]?></a>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <a class="h6">Identificada <?=date('Y')?></a>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <a class="h6">Vigentes</a>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <a class="h6">% de Total</a>
                </div>
            </div>
        </div>
        <?php $sombra=0; foreach($usuarios as $usuario){
                        $identificada_con_o_sin_filtro = 0;
                        $identificada_con_o_sin_filtro_valor = 0;
                        if(isset($data["identificadas_filtrada"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["identificadas_filtrada"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["identificadas_filtrada_valor"][$usuario->Id_user];
                        }else if(isset($data["primera_quincena"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["primera_quincena"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["primera_quincena_valor"][$usuario->Id_user];
                        }else if(isset($data["segunda_quincena"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["segunda_quincena"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["segunda_quincena_valor"][$usuario->Id_user];
                        }

                        if(!isset($data['numero_dias'][$usuario->Id_user])){ $data['numero_dias'][$usuario->Id_user] = 0; }
                        if(!isset($data['fecha_oportunidad_reciente'][$usuario->Id_user])){ $data['fecha_oportunidad_reciente'][$usuario->Id_user] = ''; }
                        if(!isset($data['nombre_cierre_oportunidad_cercana'][$usuario->Id_user])){ $data['nombre_cierre_oportunidad_cercana'][$usuario->Id_user] = ''; }
                        if(!isset($data['valor_cierre_oportunidad_cercana'][$usuario->Id_user])){ $data['valor_cierre_oportunidad_cercana'][$usuario->Id_user] = 0; }
                        if(!isset($data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user])){ $data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user] = ''; }
                        if(!isset($data["identificada_por_mesActual"][$usuario->Id_user])){ $data["identificada_por_mesActual"][$usuario->Id_user] = 0; }
                        if(!isset($data["identificada_por_mesActual_valor"][$usuario->Id_user])){ $data["identificada_por_mesActual_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data["identificada_por_añoActual"][$usuario->Id_user])){ $data["identificada_por_añoActual"][$usuario->Id_user] = 0; }
                        if(!isset($data["identificada_por_añoActual_valor"][$usuario->Id_user])){ $data["identificada_por_añoActual_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data['identificada_por_vigentes'][$usuario->Id_user])){ $data['identificada_por_vigentes'][$usuario->Id_user] = 0; }
                        if(!isset($data["identificada_por_vigentes_valor"][$usuario->Id_user])){ $data["identificada_por_vigentes_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data["total_vigentes"][$usuario->Id_user])){ $data["total_vigentes"][$usuario->Id_user] = 0; }



                        if($sombra == 1){
                            $clase = 'row-underlined';
                            $sombra = -1;
                        }else{
                            $clase = '';
                        }

                        if(empty($usuario->Foto)){
                            $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                        }else{
                            $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                        }
                        $mes = date('F');
                        $year = date('Y');

                    ?>

        <!--  -->

        <div class="col-md-6 mb-3 fila-identificadas">
            <div class="row ml-3 border-round-left <?=$clase?> h-100">
                <div class="col-md-3">
                    <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                </div>

                <div class="col-md-3">
                    <p class="h2 color-content-g"><strong><?=$data['numero_dias'][$usuario->Id_user]?></strong></p>
                    <p class="h6 color-content-p">
                        <?=$this->fecha($data['fecha_oportunidad_reciente'][$usuario->Id_user])?>
                    </p>
                </div>
                <div class="col-md-3">
                    <p class="h5 color-content-g"><strong><?=$data['nombre_cierre_oportunidad_cercana'][$usuario->Id_user]?></strong></p>
                    <p class="h6 color-content-p">$
                        <?=$data['valor_cierre_oportunidad_cercana'][$usuario->Id_user]?>
                    </p>
                    <p class="h6 color-content-p">
                        <?=$this->fecha($data['fecha_cierre_oportunidad_cercana'][$usuario->Id_user])?>
                    </p>
                </div>
                <div class="col-md-3">
                    <p class="h5 color-content-g"><strong><?=$identificada_con_o_sin_filtro?> Oportunidades</strong></p>
                    <p class="h6 color-content-p">$
                        <?=$this->millones($identificada_con_o_sin_filtro_valor)?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-3 fila-identificadas">
            <div class="row mr-3 border-round-right <?=$clase?> h-100">
                <div class="col-md-3">
                    <p class="h5 color-content-g"><strong><?=$data["identificada_por_mesActual"][$usuario->Id_user]?> Oportunidades</strong></p>
                    <p class="h6 color-content-p">$
                        <?=$this->millones($data["identificada_por_mesActual_valor"][$usuario->Id_user])?>
                    </p>
                </div>
                <div class="col-md-3">
                    <p class="h5 color-content-g"><strong><?=$data["identificada_por_añoActual"][$usuario->Id_user]?> Oportunidades</strong></p>
                    <p class="h6 color-content-p">$
                        <?=$this->millones($data["identificada_por_añoActual_valor"][$usuario->Id_user])?>
                    </p>
                </div>
                <div class="col-md-3">
                    <p class="h5 color-content-g"><strong><?=$data['identificada_por_vigentes'][$usuario->Id_user]?> Oportunidades</strong></p>
                    <p class="h6 color-content-p">$
                        <?=$this->millones($data["identificada_por_vigentes_valor"][$usuario->Id_user])?>
                    </p>
                </div>
                <div class="col-md-3">
                    <p class="h2 color-content-g"><strong><?=$data['total_vigentes'][$usuario->Id_user]?> %</strong></p>
                </div>
            </div>
        </div>
        <?php
                    $sombra++;
                } ?>
            <!--<div class="col-md-12 btn-view text-right">
                        <img data-toggle="tooltip" data-placement="top" title="Ver Oportunidad" src="<?=$request->dir?>/images/ver_oportunidad_identificada.png">
                    </div>-->
    </div>
    <?php
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Negocios Cerrados*/
            case 1:
                /*Usuarios*/
                $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Gerente Comercial" OR U.cargo LIKE "Partner Solution" OR U.cargo LIKE "Director General") AND U.deleted_at IS NULL';
                $usuarios = DB::select($query);
                foreach($usuarios as $usuario){
                    /*Oportunidades*/
                    $query = 'SELECT DISTINCT(H.oportunidad_id) FROM `historial_oportunidades` H WHERE H.key LIKE "responsable" AND H.value LIKE "'.$usuario->Id_user.'" ORDER BY `H`.`oportunidad_id` ASC';
                    $oportunidades = DB::select($query);
                    try{
                        foreach($oportunidades as $oportunidad){
                            /*Responsables*/
                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "responsable" ORDER BY H.updated_at DESC LIMIT 0,1';
                            $responsable = DB::select($query);
                            if($responsable[0]->value == $usuario->Id_user){
                                /*Ciclo*/
                                $query = 'SELECT H.value AS ciclo FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1';
                                $ciclo = DB::select($query);
                                if(intval($ciclo[0]->ciclo) == 90){
                                   /*Columna 1 - Ultimos Negocios Cerrados*/
                                   $query = 'SELECT H.value, SUBSTRING(O.titulo, 12,1000) AS titulo FROM historial_oportunidades H INNER JOIN oportunidades O ON H.oportunidad_id = O.id  WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                   $fecha_cierre = DB::select($query);
                                   if(isset($data['fecha_negocios_cerrados'][$usuario->Id_user])){
                                       if($data['fecha_negocios_cerrados'][$usuario->Id_user] > $fecha_cierre[0]->value && date('Y-m-d') >= $fecha_cierre[0]->value){
                                           $data['fecha_negocios_cerrados'][$usuario->Id_user] = $fecha_cierre[0]->value;
                                           $data['nombreEmp_negocios_cerrados'][$usuario->Id_user] = $fecha_cierre[0]->titulo;
                                           $query = 'SELECT CONCAT(FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                           $valor_oportunidad_cerrada = DB::select($query);
                                           $data["valor_negocios_cerrados"][$usuario->Id_user] = $valor_oportunidad_cerrada[0]->value;
                                        }
                                    }else if(date('Y-m-d') >= $fecha_cierre[0]->value){
                                        $data['fecha_ultimos_negocios_cerrados'][$usuario->Id_user] = $fecha_cierre[0]->value;
                                        $data['nombreEmp_negocios_cerrados'][$usuario->Id_user] = $fecha_cierre[0]->titulo;
                                        $query = 'SELECT CONCAT(FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_cerrada = DB::select($query);
                                        $data["valor_negocios_cerrados"][$usuario->Id_user] = $valor_oportunidad_cerrada[0]->value;
                                    }
                                   /*Columna 2 - NC con Rangos*/
                                    if(!empty($fecha_inicial) && !empty($fecha_final)){
                                        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $filtrada = DB::select($query);
                                        if(isset($filtrada[0]->value)){
                                            if(date('Y-m-d',strtotime($filtrada[0]->value)) >= $fecha_inicial && date('Y-m-d',strtotime($filtrada[0]->value)) <= $fecha_final){
                                                $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                $valor_oportunidad = DB::select($query);
                                                if(isset($data["negocioCerrado_filtrada"][$usuario->Id_user])){
                                                    $data["negocioCerrado_filtrada"][$usuario->Id_user]++;
                                                    $data["negocioCerrado_filtrada_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                }else{
                                                    $data["negocioCerrado_filtrada"][$usuario->Id_user] = 1;
                                                    $data["negocioCerrado_filtrada_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                }
                                            }
                                        }

                                    }else{
                                        $dia = date('d');
                                        if($dia <= 15){
                                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $quincenal = DB::select($query);
                                            if(isset($quincenal[0]->value)){
                                                if(date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) >= date('Y-m-01 00:00:00') && date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) <= date('Y-m-15 00:00:00')){
                                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                    $valor_oportunidad = DB::select($query);
                                                    if(isset($data["primera_quincena"][$usuario->Id_user])){
                                                        $data["primera_quincena"][$usuario->Id_user]++;
                                                        $data["primera_quincena_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                    }else{
                                                        $data["primera_quincena"][$usuario->Id_user] = 1;
                                                        $data["primera_quincena_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                    }
                                                }
                                            }
                                        }else{
                                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $quincenal = DB::select($query);
                                            if(isset($quincenal[0]->value)){
                                                if(date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) >= date('Y-m-16 00:00:00') && date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) <= date('Y-m-31 00:00:00')){
                                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                    $valor_oportunidad = DB::select($query);
                                                    if(isset($data["segunda_quincena"][$usuario->Id_user])){
                                                        $data["segunda_quincena"][$usuario->Id_user]++;
                                                        $data["segunda_quincena_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                    }else{
                                                        $data["segunda_quincena"][$usuario->Id_user] = 1;
                                                        $data["segunda_quincena_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    /*Columna 3 - NC por mes*/
                                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $por_mes = DB::select($query);
                                    $mes = date('Y-m');
                                    if(date('Y-m',strtotime($por_mes[0]->value)) == $mes){
                                       $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                       $valor_oportunidad_porMes = DB::select($query);
                                       if(isset($data["cerradas_por_mesActual"][$usuario->Id_user])){
                                             $data["cerradas_por_mesActual"][$usuario->Id_user]++;
                                             $data["cerradas_por_mesActual_valor"][$usuario->Id_user]+=$valor_oportunidad_porMes[0]->value;
                                       } else {
                                             $data["cerradas_por_mesActual"][$usuario->Id_user] = 1;
                                             $data["cerradas_por_mesActual_valor"][$usuario->Id_user]=$valor_oportunidad_porMes[0]->value;
                                       }
                                    }
                                    /*Columna 4 - NC por año*/
                                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $por_ano = DB::select($query);
                                    $ano = date('Y');
                                    if(date('Y',strtotime($por_ano[0]->value)) == $ano){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_porAno = DB::select($query);
                                        if(isset($data["cerradas_por_añoActual"][$usuario->Id_user])){
                                            $data["cerradas_por_añoActual"][$usuario->Id_user]++;
                                            $data["cerradas_por_añoActual_valor"][$usuario->Id_user]+=$valor_oportunidad_porAno[0]->value;
                                        }
                                        else {
                                            $data["cerradas_por_añoActual"][$usuario->Id_user] = 1;
                                            $data["cerradas_por_añoActual_valor"][$usuario->Id_user]=$valor_oportunidad_porAno[0]->value;
                                        }
                                    }
                                    /*Columna 5 NC %Total*/
                                     $total_cerradas = 0;
                                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $cerradas = DB::select($query);
                                    $ano = date('Y');
                                    if(date('Y',strtotime($cerradas[0]->value)) == $ano){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_cerrada = DB::select($query);
                                        if(isset($data["cant_cerradas"][$usuario->Id_user])){
                                            $data["cant_cerradas"][$usuario->Id_user]++;
                                            $data["cant_cerradas_valor"][$usuario->Id_user]+=$valor_oportunidad_cerrada[0]->value;
                                            foreach($data['cant_cerradas_valor'] as $valor_vigente){
                                                 $total_cerradas+=$valor_vigente;
                                            }
                                            foreach($data['cant_cerradas_valor'] as $index => $value){
                                                 $data['porcentaje_por_cerradas'][$index]=number_format(($value/$total_cerradas*100), 0, ',', ' ');
                                            }
                                        }
                                        else {
                                            $data["cant_cerradas"][$usuario->Id_user] = 1;
                                            $data["cant_cerradas_valor"][$usuario->Id_user]=$valor_oportunidad_cerrada[0]->value;

                                        }
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        $data['error']=$e;
                    }
                }
                if(!empty($request->fecha_inicial) && !empty($request->fecha_final)){
                    $dia_inicial = date('d',strtotime($request->fecha_inicial));
                    $mes_inicial = substr($meses[intval(date('m',strtotime($request->fecha_inicial)))], 0, 3);
                    $dia_final = date('d',strtotime($request->fecha_final));
                    $mes_final = substr($meses[intval(date('m',strtotime($request->fecha_final)))], 0, 3);
                    if(date("Y",strtotime($request->fecha_inicial)) != date("Y",strtotime($request->fecha_final))){
                        $mes_inicial.=' '.date("Y",strtotime($request->fecha_inicial));
                        $mes_final.=' '.date("Y",strtotime($request->fecha_final));
                    }
                }else{
                    $dia = intval(date('m'));
                    if($dia <= 15){
                        $dia_inicial = 1;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = 15;
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }else{
                        $dia_inicial = 16;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = cal_days_in_month(CAL_GREGORIAN, date('m'),date('Y'));
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }
                }
                $rango_fechas = $dia_inicial.' '.$mes_inicial.' - '.$dia_final.' '.$mes_final;
                ob_start(); ?>
        <div class="row mt-5 ml-5 mr-5 bg-white border-round shadow">
            <div class="col-md-12">
                <p class="h2 color-title-nc">Negocios Cerrados</p>
            </div>
            <div class="col-md-12 text-right">
                <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
            </div>
            <div class="col-md-12 mt-3 mb-3 e-cerrados">
                <div class="row mt-3 mb-3 ml-3 mr-3 bg-blue-medio h-100 border-round-nc color-white">
                    <div class="col-md-4 mt-3">
                        <div class="row margin-media">
                            <div class="col-md-12">
                                <a class="h6">Ultimo Negocio Cerrado</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a class="h6">
                            <?=$rango_fechas?>
                        </a>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a class="h6">
                            <?=$meses[intval(date('m'))]?>
                        </a>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a class="h6"> Cerrados <?= Carbon::now()->year ?></a>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a class="h6">% del Total</a>
                    </div>
                </div>
            </div>

            <?php $sombra=0; foreach($usuarios as $usuario){

                        $identificada_con_o_sin_filtro = 0;
                        $identificada_con_o_sin_filtro_valor = 0;
                        if(isset($data["negocioCerrado_filtrada"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["negocioCerrado_filtrada"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["negocioCerrado_filtrada_valor"][$usuario->Id_user];
                        }else if(isset($data["primera_quincena"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["primera_quincena"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["primera_quincena_valor"][$usuario->Id_user];
                        }else if(isset($data["segunda_quincena"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["segunda_quincena"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["segunda_quincena_valor"][$usuario->Id_user];
                        }

                        if(!isset($data['fecha_ultimos_negocios_cerrados'][$usuario->Id_user])){ $data['fecha_ultimos_negocios_cerrados'][$usuario->Id_user] = ''; }
                        if(!isset($data['nombreEmp_negocios_cerrados'][$usuario->Id_user])){ $data['nombreEmp_negocios_cerrados'][$usuario->Id_user] = 'No Registra '; }
                        if(!isset($data['valor_negocios_cerrados'][$usuario->Id_user])){ $data['valor_negocios_cerrados'][$usuario->Id_user] = 0; }
                        if(!isset($data["cerradas_por_mesActual"][$usuario->Id_user])){ $data["cerradas_por_mesActual"][$usuario->Id_user] = 0; }
                        if(!isset($data["cerradas_por_mesActual_valor"][$usuario->Id_user])){ $data["cerradas_por_mesActual_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data["cerradas_por_añoActual"][$usuario->Id_user])){ $data["cerradas_por_añoActual"][$usuario->Id_user] = 0; }
                        if(!isset($data["cerradas_por_añoActual_valor"][$usuario->Id_user])){ $data["cerradas_por_añoActual_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data["porcentaje_por_cerradas"][$usuario->Id_user])){ $data["porcentaje_por_cerradas"][$usuario->Id_user] = 0; }

                        if($sombra == 1){
                            $clase = 'row-underlined';
                            $sombra = -1;
                        }else{
                            $clase = '';
                        }

                        if(empty($usuario->Foto)){
                            $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                        }else{
                            $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                        }

                    ?>


            <div class="col-md-12 mb-3 fila-cerrados">
                <div class="row ml-3 mr-3 border-round-nc <?=$clase?> h-100">
                    <div class="col-md-2">
                        <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                    </div>

                    <div class="col-md-2">
                        <p class="h5 color-content-g"><strong><?=$data['nombreEmp_negocios_cerrados'][$usuario->Id_user]?></strong></p>
                        <p class="h6 color-content-p">$
                            <?=$data['valor_negocios_cerrados'][$usuario->Id_user]?>
                        </p>
                        <p class="h6 color-content-p">
                            <?=$this->fecha($data['fecha_ultimos_negocios_cerrados'][$usuario->Id_user])?>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <p class="h5 color-content-g"><strong><?=$identificada_con_o_sin_filtro?> Oportunidades</strong></p>
                        <p class="h6 color-content-p">$
                            <?=$this->millones($identificada_con_o_sin_filtro_valor)?>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <p class="h5 color-content-g"><strong><?=$data["cerradas_por_mesActual"][$usuario->Id_user]?> Oportunidades</strong></p>
                        <p class="h6 color-content-p">$
                            <?=$this->millones($data["cerradas_por_mesActual_valor"][$usuario->Id_user])?>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <p class="h5 color-content-g"><strong><?=$data["cerradas_por_añoActual"][$usuario->Id_user]?> Oportunidades</strong></p>
                        <p class="h6 color-content-p">$
                            <?=$this->millones($data["cerradas_por_añoActual_valor"][$usuario->Id_user])?>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <p class="h2 color-content-g"><strong><?=$data['porcentaje_por_cerradas'][$usuario->Id_user]?> %</strong></p>
                    </div>
                </div>
            </div>

            <?php
                    $sombra++;
                }
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
            break;
            /*Negocios Perdidos*/
            case 2:
                 /*Usuarios*/
                $total_valor_perdidos=0;
                $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Gerente Comercial" OR U.cargo LIKE "Partner Solution") AND U.deleted_at IS NULL';
                $usuarios = DB::select($query);
                try{
                    foreach($usuarios as $usuario){
                        /*Oportunidades*/
                        $query = 'SELECT DISTINCT(H.oportunidad_id) FROM `historial_oportunidades` H WHERE H.key LIKE "responsable" AND H.value LIKE "'.$usuario->Id_user.'" ORDER BY `H`.`oportunidad_id` ASC';
                        $oportunidades = DB::select($query);
                        foreach($oportunidades as $oportunidad){
                            /*Responsables*/
                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "responsable" ORDER BY H.updated_at DESC LIMIT 0,1';
                            $responsable = DB::select($query);
                            if($responsable[0]->value == $usuario->Id_user){
                                /*Ciclo*/
                                $query = 'SELECT H.value AS ciclo FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1';
                                $ciclo = DB::select($query);
                                if(intval($ciclo[0]->ciclo) == 0){
                                   /*Columna 1 - Ultimos Negocios Perdidos*/
                                   $query = 'SELECT H.value,SUBSTRING(H.updated_at, 1,11) AS fecha, SUBSTRING(O.titulo, 12,1000) AS titulo FROM historial_oportunidades H INNER JOIN oportunidades O ON H.oportunidad_id = O.id  WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.updated_at DESC LIMIT 0,1';
                                   $fecha_cierre = DB::select($query);
                                   if(isset($data['fecha_negocios_perdidos'][$usuario->Id_user])){
                                       if($data['fecha_negocios_perdidos'][$usuario->Id_user] > $fecha_cierre[0]->value && date('Y-m-d') >= $fecha_cierre[0]->value){
                                           $data['fecha_negocios_perdidos'][$usuario->Id_user] = $fecha_cierre[0]->fecha;
                                           $data['nombreEmp_negocios_perdidos'][$usuario->Id_user] = $fecha_cierre[0]->titulo;
                                           $query = 'SELECT CONCAT(FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                           $valor_oportunidad_perdida = DB::select($query);
                                           $data["valor_negocios_perdidos"][$usuario->Id_user] = $valor_oportunidad_perdida[0]->value;
                                        }
                                    }else if(date('Y-m-d') >= $fecha_cierre[0]->value){
                                        $data['fecha_ultimos_negocios_perdidos'][$usuario->Id_user] = $fecha_cierre[0]->fecha;
                                        $data['nombreEmp_negocios_perdidos'][$usuario->Id_user] = $fecha_cierre[0]->titulo;
                                        $query = 'SELECT CONCAT(FORMAT(CONVERT(REPLACE(H.value, ",", ""),UNSIGNED INTEGER)/1000000,0)," ","Millones") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_perdida = DB::select($query);
                                        $data["valor_negocios_perdidos"][$usuario->Id_user] = $valor_oportunidad_perdida[0]->value;
                                    }
                                   /*Columna 2 - NP con Rangos*/
                                    if(!empty($fecha_inicial) && !empty($fecha_final)){
                                         $query = 'SELECT SUBSTRING(H.updated_at, 1,11) AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $filtrada = DB::select($query);
                                        if(isset($filtrada[0]->value)){
                                            if(date('Y-m-d',strtotime($filtrada[0]->value)) >= $fecha_inicial && date('Y-m-d',strtotime($filtrada[0]->value)) <= $fecha_final){
                                                $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                $valor_oportunidad = DB::select($query);
                                                if(isset($data["negocioCerrado_filtrada"][$usuario->Id_user])){
                                                    $data["negocioCerrado_filtrada"][$usuario->Id_user]++;
                                                    $data["negocioCerrado_filtrada_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                }else{
                                                    $data["negocioCerrado_filtrada"][$usuario->Id_user] = 1;
                                                    $data["negocioCerrado_filtrada_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                }
                                            }
                                        }

                                    }else{
                                        $dia = date('d');
                                        if($dia <= 15){
                                            $query = 'SELECT SUBSTRING(H.updated_at, 1,11) AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $quincenal = DB::select($query);
                                            if(isset($quincenal[0]->value)){
                                                if(date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) >= date('Y-m-01 00:00:00') && date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) <= date('Y-m-15 00:00:00')){
                                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                    $valor_oportunidad = DB::select($query);
                                                    if(isset($data["primera_quincena"][$usuario->Id_user])){
                                                        $data["primera_quincena"][$usuario->Id_user]++;
                                                        $data["primera_quincena_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                    }else{
                                                        $data["primera_quincena"][$usuario->Id_user] = 1;
                                                        $data["primera_quincena_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                    }
                                                }
                                            }
                                        }else{
                                            $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC LIMIT 0,1';
                                            $quincenal = DB::select($query);
                                            if(isset($quincenal[0]->value)){
                                                if(date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) >= date('Y-m-16 00:00:00') && date('Y-m-d 00:00:00',strtotime($quincenal[0]->value)) <= date('Y-m-31 00:00:00')){
                                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                                    $valor_oportunidad = DB::select($query);
                                                    if(isset($data["segunda_quincena"][$usuario->Id_user])){
                                                        $data["segunda_quincena"][$usuario->Id_user]++;
                                                        $data["segunda_quincena_valor"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                                    }else{
                                                        $data["segunda_quincena"][$usuario->Id_user] = 1;
                                                        $data["segunda_quincena_valor"][$usuario->Id_user]=$valor_oportunidad[0]->value;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    /*Columna 3 - NP por mes*/
                                    $query = 'SELECT SUBSTRING(H.updated_at, 1,11) AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $por_mes = DB::select($query);
                                    $mes = date('Y-m');
                                    if(date('Y-m',strtotime($por_mes[0]->value)) == $mes){
                                       $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                       $valor_oportunidad_porMes = DB::select($query);
                                       if(isset($data["perdidas_por_mesActual"][$usuario->Id_user])){
                                             $data["perdidas_por_mesActual"][$usuario->Id_user]++;
                                             $data["perdidas_por_mesActual_valor"][$usuario->Id_user]+=$valor_oportunidad_porMes[0]->value;
                                       } else {
                                             $data["perdidas_por_mesActual"][$usuario->Id_user] = 1;
                                             $data["perdidas_por_mesActual_valor"][$usuario->Id_user]=$valor_oportunidad_porMes[0]->value;
                                       }
                                    }
                                    /*Columna 4 - NC por año*/
                                    $query = 'SELECT SUBSTRING(H.updated_at, 1,11) AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $por_ano = DB::select($query);
                                    $ano = date('Y');
                                    if(date('Y',strtotime($por_ano[0]->value)) == $ano){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS plata FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_porAno = DB::select($query);
                                        if(isset($data["perdidas_por_añoActual"][$usuario->Id_user])){
                                            $data["perdidas_por_añoActual"][$usuario->Id_user]++;
                                            $data["perdidas_por_añoActual_valor"][$usuario->Id_user]+=$valor_oportunidad_porAno[0]->plata;
                                        }
                                        else {
                                            $data["perdidas_por_añoActual"][$usuario->Id_user] = 1;
                                            $data["perdidas_por_añoActual_valor"][$usuario->Id_user]=$valor_oportunidad_porAno[0]->plata;
                                        }
                                    }
                                    /*Columna 5 NP %Total*/
                                    $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" AND YEAR ( H.updated_at ) = YEAR(CURDATE())  ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $perdidos = DB::select($query);
                                    if(isset($perdidos[0]->value)){
                                        $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                        $valor_oportunidad_cerrada = DB::select($query);
                                        $total_valor_perdidos+=$valor_oportunidad_cerrada[0]->value;
                                        if(isset($data["cant_perdidas"][$usuario->Id_user])){
                                            $data["cant_perdidas"][$usuario->Id_user]++;
                                            $data["cant_perdidas_valor"][$usuario->Id_user]+=$valor_oportunidad_cerrada[0]->value;
                                        }else{
                                            $data["cant_perdidas"][$usuario->Id_user] = 1;
                                            $data["cant_perdidas_valor"][$usuario->Id_user]=$valor_oportunidad_cerrada[0]->value;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $data['error']=$e;
                }
                if(!empty($request->fecha_inicial) && !empty($request->fecha_final)){
                    $dia_inicial = date('d',strtotime($request->fecha_inicial));
                    $mes_inicial = substr($meses[intval(date('m',strtotime($request->fecha_inicial)))], 0, 3);
                    $dia_final = date('d',strtotime($request->fecha_final));
                    $mes_final = substr($meses[intval(date('m',strtotime($request->fecha_final)))], 0, 3);
                    if(date("Y",strtotime($request->fecha_inicial)) != date("Y",strtotime($request->fecha_final))){
                        $mes_inicial.=' '.date("Y",strtotime($request->fecha_inicial));
                        $mes_final.=' '.date("Y",strtotime($request->fecha_final));
                    }
                }else{
                    $dia = intval(date('m'));
                    if($dia <= 15){
                        $dia_inicial = 1;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = 15;
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }else{
                        $dia_inicial = 16;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = cal_days_in_month(CAL_GREGORIAN, date('m'),date('Y'));
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }
                }
                $rango_fechas = $dia_inicial.' '.$mes_inicial.' - '.$dia_final.' '.$mes_final;
                ob_start(); ?>
                <div class="row mt-5 ml-5 mr-5 bg-white border-round shadow">
                    <div class="col-md-12">
                        <p class="h2 color-title-np">Negocios Perdidos</p>
                    </div>
                    <div class="col-md-12 text-right">
                        <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
                    </div>
                    <!-- Headers NP -->
                    <div class="col-md-12 mt-3 mb-3 ">
                        <div class="row mt-3 mb-3 ml-3 mr-3 bg-blue-alto h-100 border-round-nc color-white">
                            <div class="col-md-3 mt-3 offset-md-1">
                                <a class="h6">Ultimos Negocios Perdidos</a>
                            </div>
                            <div class="col-md-2 mt-3">
                                <a class="h6">
                                    <?=$rango_fechas?>
                                </a>
                            </div>
                            <div class="col-md-2 mt-3">
                                <a class="h6">
                                    <?=$meses[intval(date('m'))]?>
                                </a>
                            </div>
                            <div class="col-md-2 mt-3">
                                <a class="h6">Perdidos <?= Carbon::now()->year ?></a>
                            </div>
                            <div class="col-md-2 mt-3">
                                <a class="h6">% del Total</a>
                            </div>
                        </div>
                    </div>
                    <?php $sombra=0; foreach($usuarios as $usuario){

                        $identificada_con_o_sin_filtro = 0;
                        $identificada_con_o_sin_filtro_valor = 0;
                        if(isset($data["negocioCerrado_filtrada"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["negocioCerrado_filtrada"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["negocioCerrado_filtrada_valor"][$usuario->Id_user];
                        }else if(isset($data["primera_quincena"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["primera_quincena"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["primera_quincena_valor"][$usuario->Id_user];
                        }else if(isset($data["segunda_quincena"][$usuario->Id_user])){
                            $identificada_con_o_sin_filtro = $data["segunda_quincena"][$usuario->Id_user];
                            $identificada_con_o_sin_filtro_valor = $data["segunda_quincena_valor"][$usuario->Id_user];
                        }

                    /*Porcentaje de valor de perdidas*/
                    if(isset($data["cant_perdidas_valor"][$usuario->Id_user]) && $total_valor_perdidos>0){
                        $data["porcentaje_por_perdidos"][$usuario->Id_user] = $data["cant_perdidas_valor"][$usuario->Id_user]/$total_valor_perdidos * 100;
                        $data["porcentaje_por_perdidos"][$usuario->Id_user] = number_format($data["porcentaje_por_perdidos"][$usuario->Id_user], 0, ',', ' ');
                    }
                    /*Fin Porcentaje de valor de perdidas*/
                        if(!isset($data['fecha_ultimos_negocios_perdidos'][$usuario->Id_user])){ $data['fecha_ultimos_negocios_perdidos'][$usuario->Id_user] = ''; }
                        if(!isset($data['nombreEmp_negocios_perdidos'][$usuario->Id_user])){ $data['nombreEmp_negocios_perdidos'][$usuario->Id_user] = 'Sin Negocios perdidos'; }
                        if(!isset($data['valor_negocios_perdidos'][$usuario->Id_user])){ $data['valor_negocios_perdidos'][$usuario->Id_user] = 0; }
                        if(!isset($data["perdidas_por_mesActual"][$usuario->Id_user])){ $data["perdidas_por_mesActual"][$usuario->Id_user] = 0; }
                        if(!isset($data["perdidas_por_mesActual_valor"][$usuario->Id_user])){ $data["perdidas_por_mesActual_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data["perdidas_por_añoActual"][$usuario->Id_user])){ $data["perdidas_por_añoActual"][$usuario->Id_user] = 0; }
                        if(!isset($data["perdidas_por_añoActual_valor"][$usuario->Id_user])){ $data["perdidas_por_añoActual_valor"][$usuario->Id_user] = 0; }
                        if(!isset($data["porcentaje_por_perdidos"][$usuario->Id_user])){ $data["porcentaje_por_perdidos"][$usuario->Id_user] = 0; }

                        if($sombra == 1){
                            $clase = 'row-underlined';
                            $sombra = -1;
                        }else{
                            $clase = '';
                        }

                        if(empty($usuario->Foto)){
                            $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                        }else{
                            $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                        }

                    ?>

                    <div class="col-md-12 mb-3 fila-perdidos">
                        <div class="row ml-3 mr-3 border-round-nc <?=$clase?> h-100">
                            <div class="col-md-1">
                                <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                            </div>
                            <div class="col-md-3">
                                <p class="h5 color-content-g"><strong><?=$data['nombreEmp_negocios_perdidos'][$usuario->Id_user]?></strong></p>
                                <p class="h6 color-content-p">$
                                    <?=$data['valor_negocios_perdidos'][$usuario->Id_user]?>
                                </p>
                                <p class="h6 color-content-p">
                                    <?=$this->fecha($data['fecha_ultimos_negocios_perdidos'][$usuario->Id_user])?>
                                </p>

                            </div>
                            <div class="col-md-2">
                                <p class="h5 color-content-g"><strong><?=$identificada_con_o_sin_filtro?> Oportunidades</strong></p>
                                <p class="h6 color-content-p">$
                                    <?=$this->millones($identificada_con_o_sin_filtro_valor)?>
                                </p>

                            </div>
                            <div class="col-md-2">
                                <p class="h5 color-content-g"><strong><?=$data["perdidas_por_mesActual"][$usuario->Id_user]?> Oportunidades</strong></p>
                                <p class="h6 color-content-p">$
                                    <?=$this->millones($data["perdidas_por_mesActual_valor"][$usuario->Id_user])?>
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="h5 color-content-g"><strong><?=$data["perdidas_por_añoActual"][$usuario->Id_user]?> Oportunidades</strong></p>
                                <p class="h6 color-content-p">$
                                    <?=$this->millones($data["perdidas_por_añoActual_valor"][$usuario->Id_user])?>
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="h2 color-content-g"><strong><?=$data['porcentaje_por_perdidos'][$usuario->Id_user]?> %</strong></p>
                            </div>
                        </div>
                    </div>

                    <?php
                    $sombra++;
                }
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
             /*Modal - Plazo para cierre de oportunidades*/
                case 3:
                /*Usuarios*/
                $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Gerente Comercial" OR U.cargo LIKE "Partner Solution" OR U.cargo LIKE "Director General") AND U.deleted_at IS NULL';
                $usuarios = DB::select($query);
                foreach($usuarios as $usuario){
                    /*Oportunidades*/
                    $query = 'SELECT DISTINCT(H.oportunidad_id) FROM `historial_oportunidades` H WHERE H.key LIKE "responsable" AND H.value LIKE "'.$usuario->Id_user.'" ORDER BY `H`.`oportunidad_id` ASC';
                    $oportunidades = DB::select($query);
                    foreach($oportunidades as $oportunidad){
                        /*Responsables*/
                        $query = 'SELECT H.value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "responsable" ORDER BY H.updated_at DESC LIMIT 0,1';
                        $responsable = DB::select($query);
                        if($responsable[0]->value == $usuario->Id_user){
                            /*Ciclo*/
                            $query = 'SELECT H.value AS ciclo FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "ciclo_venta" ORDER BY H.id DESC LIMIT 0,1';
                            $ciclo = DB::select($query);
                            if(intval($ciclo[0]->ciclo) >= 10 && intval($ciclo[0]->ciclo) <= 80){
                                /*plazos, Corto - Medio - Largo*/
                                $query = 'SELECT H.value AS Finicio FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_ff" ORDER BY H.updated_at DESC';
                                $query2 = 'SELECT H.value AS Fcierre FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "fecha_oc" ORDER BY H.updated_at DESC';
                                $fecha_ff = DB::select($query);
                                $fecha_oc = DB::select($query2);

                                $data['fecha_ff'][$oportunidad->oportunidad_id][$usuario->Id_user] = $fecha_ff[0]->Finicio;
                                $data['fecha_oc'][$oportunidad->oportunidad_id][$usuario->Id_user] = $fecha_oc[0]->Fcierre;
                                $data['rango_dias'][$oportunidad->oportunidad_id][$usuario->Id_user] = $this->dateDiff2($fecha_ff[0]->Finicio, $fecha_oc[0]->Fcierre);

                                if(isset($data['rango_dias'][$oportunidad->oportunidad_id][$usuario->Id_user])){
                                     if(!isset( $data["plazo_corto"][$oportunidad->oportunidad_id][$usuario->Id_user])){  $data["plazo_corto"][$oportunidad->oportunidad_id][$usuario->Id_user] = 0; }
                                     if(!isset( $data["plazo_medio"][$oportunidad->oportunidad_id][$usuario->Id_user])){  $data["plazo_medio"][$oportunidad->oportunidad_id][$usuario->Id_user] = 0; }
                                     if(!isset( $data["plazo_largo"][$oportunidad->oportunidad_id][$usuario->Id_user])){  $data["plazo_largo"][$oportunidad->oportunidad_id][$usuario->Id_user] = 0; }
                                     if(!isset( $data["valor_plazo_corto"][$usuario->Id_user])){  $data["valor_plazo_corto"][$usuario->Id_user] = 0; }
                                     if(!isset( $data["valor_plazo_medio"][$usuario->Id_user])){  $data["valor_plazo_medio"][$usuario->Id_user] = 0; }
                                     if(!isset( $data["valor_plazo_largo"][$usuario->Id_user])){  $data["valor_plazo_largo"][$usuario->Id_user] = 0; }

                                    $query_rangos =  'SELECT M.corto_dias as rango FROM metas_oportunidades M';
                                    $rango = DB::select($query_rangos);
                                    $query = 'SELECT REPLACE(REPLACE(H.value, ".", ""), ",", "") AS value FROM historial_oportunidades H WHERE H.oportunidad_id = '.$oportunidad->oportunidad_id.' AND H.key LIKE "val_pesos" ORDER BY H.updated_at DESC LIMIT 0,1';
                                    $valor_oportunidad = DB::select($query);
                                    if($data['rango_dias'][$oportunidad->oportunidad_id][$usuario->Id_user] < $rango[0]->rango){
                                        if(isset($data["plazo_corto"][$usuario->Id_user])){
                                            $data["plazo_corto"][$oportunidad->oportunidad_id][$usuario->Id_user]++;
                                            $data["valor_plazo_corto"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                        } else {
                                            $data["plazo_corto"][$oportunidad->oportunidad_id][$usuario->Id_user] = 1;
                                            $data["valor_plazo_corto"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                        }

                                    }else if($data['rango_dias'][$oportunidad->oportunidad_id][$usuario->Id_user] > $rango[0]->rango &&  $data['rango_dias'][$oportunidad->oportunidad_id][$usuario->Id_user] < 150) {
                                        if(isset($data["plazo_medio"][$usuario->Id_user])){
                                            $data["plazo_medio"][$oportunidad->oportunidad_id][$usuario->Id_user]++;
                                            $data["valor_plazo_medio"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                        } else {
                                            $data["plazo_medio"][$oportunidad->oportunidad_id][$usuario->Id_user] = 1;
                                            $data["valor_plazo_medio"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                        }
                                    } else {
                                         if(isset($data["plazo_largo"][$usuario->Id_user])){
                                            $data["plazo_largo"][$oportunidad->oportunidad_id][$usuario->Id_user]++;
                                            $data["valor_plazo_largo"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                        } else {
                                            $data["plazo_largo"][$oportunidad->oportunidad_id][$usuario->Id_user] = 1;
                                            $data["valor_plazo_largo"][$usuario->Id_user]+=$valor_oportunidad[0]->value;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
              ob_start(); ?>
                        <?php foreach($usuarios as $usuario){

                     if(!isset( $data["valor_plazo_corto"][$usuario->Id_user])){  $data["valor_plazo_corto"][$usuario->Id_user] = 0; }
                     if(!isset( $data["valor_plazo_medio"][$usuario->Id_user])){  $data["valor_plazo_medio"][$usuario->Id_user] = 0; }
                     if(!isset( $data["valor_plazo_largo"][$usuario->Id_user])){  $data["valor_plazo_largo"][$usuario->Id_user] = 0; }

                     if(empty($usuario->Foto)){
                            $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                     }else{
                            $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                     }

                     ?>

                        <div class="col-md-12 fila-plazocierre">
                            <div class="row ml-modal mt-3">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                        </div>
                                        <div class="col-md-7">
                                            <p class="h2 color-content-g">$
                                                <?=$this->millones2($data["valor_plazo_corto"][$usuario->Id_user])?>
                                            </p>
                                            <p class="h5">Millones</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row ml-5">
                                        <div class="col-md-12">
                                            <p class="h2 color-content-g">$
                                                <?=$this->millones2($data["valor_plazo_medio"][$usuario->Id_user])?>
                                            </p>
                                            <p class="h5">Millones</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row ml-5">
                                        <div class="col-md-12">
                                            <p class="h2 color-content-g">$
                                                <?=$this->millones2($data["valor_plazo_largo"][$usuario->Id_user])?>
                                            </p>
                                            <p class="h5">Millones</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 linea-separadora">
                            <hr size="2px" color="#3aaee1" />
                        </div>
                        <?php
              }
              $data['cuerpo'] = ob_get_contents();
              ob_end_clean();
              break;
              /*Bitacora*/
              case 4:
                /*Usuarios*/
                $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Partner Solution") AND U.deleted_at IS NULL';
                $usuarios = DB::select($query);
                foreach($usuarios as $usuario){
                    /*Filtrado por fechas*/
                    if(isset($fecha_inicial) && isset($fecha_final)){
                        if(!empty($fecha_inicial) && !empty($fecha_final)){
                               /*Columna 1 - Cantidad de Actividades*/
                                $query = 'SELECT COUNT(*) as Cantidad FROM actividades WHERE user_id = '.$usuario->Id_user.' AND fecha_actividad BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'"';
                                $total_Act_Planeadas = DB::select($query);
                                $data['total_Act'][$usuario->Id_user] =  $total_Act_Planeadas[0]->Cantidad;
                                /*Horas de Acttividades*/
                                $query_horas = 'SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(tiempo_fin) - TIME_TO_SEC(tiempo_inicio))) AS Segundos FROM actividades WHERE user_id = '.$usuario->Id_user.'  AND fecha_actividad BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'"';
                                $total_Horas = DB::select($query_horas);
                                $data['total_Act_horas'][$usuario->Id_user] = $total_Horas[0]->Segundos +'';

                                /*Columna 2 - No planeados*/
                                $query_noPlaneadas = 'SELECT COUNT(*) as cant FROM actividades WHERE user_id = '.$usuario->Id_user.' AND fecha_actividad BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'" AND origen_datos LIKE "P"';
                                $No_planeadas = DB::select($query_noPlaneadas);
                                if ( $No_planeadas[0]->cant > 0){
                                    $data['porcentaje_NoPlaneadas'][$usuario->Id_user] = floor((($No_planeadas[0]->cant )*100)/$total_Act_Planeadas[0]->Cantidad);

                                }
                                /* Columna 3 - Correctamente Diligenciado */
                                $query_correctos = 'SELECT COUNT(*) as cantidad FROM actividades WHERE user_id = '.$usuario->Id_user.' AND estado_aprobacion LIKE "Aprobado"  AND fecha_actividad BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'"';
                                $Act_correctos = DB::select($query_correctos);
                                if ( $Act_correctos[0]->cantidad > 0 ){
                                    $data['porcentaje_correctamente_diligenciado'][$usuario->Id_user] = floor((($Act_correctos[0]->cantidad )*100)/$total_Act_Planeadas[0]->Cantidad);
                                }
                                /* Columna 4 - Llenadas a tiempo*/
                                $query_tiempo = 'SELECT COUNT(*) as cant FROM actividades WHERE user_id = '.$usuario->Id_user.' AND fuera_tiempo LIKE "false" AND fecha_actividad BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'"';
                                $A_tiempo = DB::select($query_tiempo);
                                if ( $A_tiempo[0]->cant > 0 ){
                                    $data['porcentaje_aTiempo'][$usuario->Id_user] = floor((( $A_tiempo[0]->cant )*100)/$total_Act_Planeadas[0]->Cantidad);
                                }
                        }
                    } else {
                        /*Sin Filtrar - Se muestra por mes actual
                        Columna 1 - Cantidad de Actividades*/
                            $query = 'SELECT COUNT(*) as Cantidad FROM actividades WHERE user_id = '.$usuario->Id_user.' AND MONTH(fecha_actividad) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha_actividad) = YEAR(CURRENT_TIMESTAMP)';
                            $total_Act_Planeadas = DB::select($query);
                            $data['total_Act'][$usuario->Id_user] =  $total_Act_Planeadas[0]->Cantidad;
                            /*Horas de Acttividades*/
                            $query_horas = 'SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(tiempo_fin) - TIME_TO_SEC(tiempo_inicio))) AS Segundos FROM actividades WHERE user_id = '.$usuario->Id_user.'  AND MONTH(fecha_actividad) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha_actividad) = YEAR(CURRENT_TIMESTAMP)';
                            $total_Horas = DB::select($query_horas);
                            $data['total_Act_horas'][$usuario->Id_user] = $total_Horas[0]->Segundos +'';

                        /*Columna 2 - No planeados*/
                            $query_noPlaneadas = 'SELECT COUNT(*) as cant FROM actividades WHERE user_id = '.$usuario->Id_user.' AND origen_datos LIKE "N"  AND MONTH(fecha_actividad) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha_actividad) = YEAR(CURRENT_TIMESTAMP)';
                            $No_planeadas = DB::select($query_noPlaneadas);
                            if ( $No_planeadas[0]->cant > 0){
                                $data['porcentaje_NoPlaneadas'][$usuario->Id_user] = floor((($No_planeadas[0]->cant )*100)/$total_Act_Planeadas[0]->Cantidad);

                            }
                        /* Columna 3 - Correctamente Diligenciado */
                            $query_correctos = 'SELECT COUNT(*) as cantidad FROM actividades WHERE user_id = '.$usuario->Id_user.' AND estado_aprobacion LIKE "Aprobado"  AND MONTH(fecha_actividad) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha_actividad) = YEAR(CURRENT_TIMESTAMP)';
                            $Act_correctos = DB::select($query_correctos);
                            if ( $Act_correctos[0]->cantidad > 0 ){
                                $data['porcentaje_correctamente_diligenciado'][$usuario->Id_user] = floor((($Act_correctos[0]->cantidad )*100)/$total_Act_Planeadas[0]->Cantidad);
                            }
                        /* Columna 4 - Llenadas a tiempo*/
                            $query_tiempo = 'SELECT COUNT(*) as cant FROM actividades WHERE user_id = '.$usuario->Id_user.' AND fuera_tiempo = false  AND MONTH(fecha_actividad) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha_actividad) = YEAR(CURRENT_TIMESTAMP)';
                            $A_tiempo = DB::select($query_tiempo);
                            if ( $A_tiempo[0]->cant > 0 ){
                                $data['porcentaje_aTiempo'][$usuario->Id_user] = floor((( $A_tiempo[0]->cant )*100)/$total_Act_Planeadas[0]->Cantidad);
                            }
                    }
                }


              ob_start(); ?>
                            <div class="row mt-5 ml-5 mr-5 bg-white border-round shadow">
                                <div class="col-md-12">
                                    <p class="h2 color-title-np">Bitácora</p>
                                </div>
                                <div class="col-md-12 text-right">
                                    <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
                                </div>
                                <div class="col-md-12 mt-3 mb-3 e-bitacora">
                                    <div class="row mt-3 mb-3 ml-3 mr-3 bg-blue-bitacora h-100 border-round-nc color-white">
                                        <div class="col-md-4 mt-3">
                                            <div class="row margin-media">
                                                <div class="col-md-12">
                                                    <a class="h6">Cantidad</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 mt-3">
                                            <a class="h6">Porcentaje de actividades planeadas</a>
                                        </div>
                                        <div class="col-md-2 mt-3">
                                            <a class="h6">Correctamente Diligenciado</a>
                                        </div>
                                        <div class="col-md-2 mt-3">
                                            <a class="h6">LLenado a tiempo</a>
                                        </div>
                                        <div class="col-md-2 mt-3">
                                            <a class="h6">Uso tiempo</a>
                                        </div>
                                    </div>
                                </div>


                                <?php  $sombra=0; foreach($usuarios as $usuario){

                        if(!isset($data['total_Act'][$usuario->Id_user])){ $data['total_Act'][$usuario->Id_user] = 0; }
                        if(!isset($data['total_Act_horas'][$usuario->Id_user])){ $data['total_Act_horas'][$usuario->Id_user] = 0; }
                        if(!isset($data['porcentaje_NoPlaneadas'][$usuario->Id_user])){ $data['porcentaje_NoPlaneadas'][$usuario->Id_user] = 0; }
                        if(!isset($data['porcentaje_correctamente_diligenciado'][$usuario->Id_user])){  $data['porcentaje_correctamente_diligenciado'][$usuario->Id_user] = 0; }
                        if(!isset($data['porcentaje_aTiempo'][$usuario->Id_user])){  $data['porcentaje_aTiempo'][$usuario->Id_user] = 0; }

                        if($sombra == 1){
                            $clase = 'row-underlined';
                            $sombra = -1;
                        }else{
                            $clase = '';
                        }

                        if(empty($usuario->Foto)){
                                $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                        }else{
                                $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                        }

                     ?>
                                <div class="col-md-12 mb-3 fila-bitacora">
                                    <div class="row ml-3 mr-3 border-round-nc <?=$clase?> h-100 ">
                                        <div class="col-md-2">
                                            <!-- data-toggle="tooltip" data-placement="top" title="nombre_usuario"  -->
                                            <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                        </div>
                                        <div class="col-md-2">
                                            <p class="h3 color-content-g"><strong><?= $data['total_Act'][$usuario->Id_user] ?> Actividades</strong></p>
                                            <p class="h4 color-title-bitacora">
                                                <?= $data['total_Act_horas'][$usuario->Id_user] ?> horas</p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="h2 color-title-bitacora"><strong> <?= $data['porcentaje_NoPlaneadas'][$usuario->Id_user]?> %</strong></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="h2 color-title-bitacora"><strong><?= $data['porcentaje_correctamente_diligenciado'][$usuario->Id_user]?> %</strong></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="h2 color-title-bitacora"><strong><?= $data['porcentaje_aTiempo'][$usuario->Id_user]?> %</strong></p>
                                        </div>
                                        <div class="col-md-2 btn-view-ut">
                                            <img class="mt-5" data-toggle="tooltip" data-placement="top" title="Uso Tiempo" src="<?=$request->dir?>/images/ver_oportunidad_identificada.png">
                                        </div>
                                    </div>
                                </div>


                                <?php
                $sombra++;
              }
              $data['cuerpo'] = ob_get_contents();
              ob_end_clean();
              break;
              /*Plan de trabajo*/
              case 5:
                /*Usuarios*/
                $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Partner Solution") AND U.deleted_at IS NULL';
                $usuarios = DB::select($query);
                foreach($usuarios as $usuario){
                    /*Filtrado por fechas*/
                    if(isset($fecha_inicial) && isset($fecha_final)){
                        if(!empty($fecha_inicial) && !empty($fecha_final)){
                               /*Columna 1 - Cantidad de Actividades*/
                                $query = 'SELECT COUNT(*) as Cantidad FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.' AND fecha BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'"';
                                $total_Act_Planeadas = DB::select($query);
                                $data['total_Act'][$usuario->Id_user] =  $total_Act_Planeadas[0]->Cantidad;
                                /*Horas de Acttividades*/
                                $query_horas = 'SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(hora_fin) - TIME_TO_SEC(hora_inicio))) AS Segundos FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.'  AND fecha BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'"';
                                $total_Horas = DB::select($query_horas);
                                $data['total_Act_horas'][$usuario->Id_user] = $total_Horas[0]->Segundos +'';

                                /*Columna 2 - Uso Tiempo*/

                                /* Columna 3 - Actividades cumplidas*/
                                $query_cumplidas = 'SELECT COUNT(*) as Cumplidas FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.' AND fecha BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'" AND plazo LIKE "S" ';
                                $cumplidas = DB::select($query_cumplidas);
                                $data['cumplidas'][$usuario->Id_user] = $cumplidas[0]->Cumplidas;
                                if($cumplidas[0]->Cumplidas > 0){
                                    $data['no_cumplidas'][$usuario->Id_user] = ($total_Act_Planeadas[0]->Cantidad) - ($cumplidas[0]->Cumplidas);
                                }

                                /* Columna 4 - Hecha a Tiempo*/
                                $query_tiempo = 'SELECT COUNT(*) as porcentaje FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.' AND fecha BETWEEN "'.$fecha_inicial.'" AND "'.$fecha_final.'" AND fuera_tiempo = false';
                                $A_tiempo = DB::select($query_tiempo);
                                $data['Porc_Tiempo'][$usuario->Id_user] = $A_tiempo[0]->porcentaje;
                                if ( $A_tiempo[0]->porcentaje > 0 ){
                                        $data['porcentaje_aTiempo'][$usuario->Id_user] = floor((( $A_tiempo[0]->porcentaje )*100)/$total_Act_Planeadas[0]->Cantidad);
                                }
                        }
                    } else {
                        /*Sin Filtrar - Se muestra por mes actual
                        Columna 1 - Cantidad de Actividades*/
                            $query = 'SELECT COUNT(*) as Cantidad FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.' AND MONTH(fecha) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha) = YEAR(CURRENT_TIMESTAMP)';
                            $total_Act_Planeadas = DB::select($query);
                            $data['total_Act'][$usuario->Id_user] =  $total_Act_Planeadas[0]->Cantidad;
                            /*Horas de Acttividades*/
                            $query_horas = 'SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(hora_fin) - TIME_TO_SEC(hora_inicio))) AS Segundos FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.'  AND MONTH(fecha) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha) = YEAR(CURRENT_TIMESTAMP)';
                            $total_Horas = DB::select($query_horas);
                            $data['total_Act_horas'][$usuario->Id_user] = $total_Horas[0]->Segundos +'';

                            /*Columna 2 - Uso Tiempo*/

                            /* Columna 3 - Actividades cumplidas*/
                            $query_cumplidas = 'SELECT COUNT(*) as Cumplidas FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.' AND MONTH(fecha) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha) = YEAR(CURRENT_TIMESTAMP) AND plazo LIKE "S"  ';
                            $cumplidas = DB::select($query_cumplidas);
                            $data['cumplidas'][$usuario->Id_user] = $cumplidas[0]->Cumplidas;
                            if($cumplidas[0]->Cumplidas > 0){
                                $data['no_cumplidas'][$usuario->Id_user] = ($total_Act_Planeadas[0]->Cantidad) - ($cumplidas[0]->Cumplidas);
                            }
                            /* Columna 4 - Hecha a Tiempo*/
                            $query_tiempo = 'SELECT COUNT(*) as porcentaje FROM plan_trabajo_descripcions WHERE user_id = '.$usuario->Id_user.' AND MONTH(fecha) = MONTH(CURRENT_TIMESTAMP) AND YEAR(fecha) = YEAR(CURRENT_TIMESTAMP) AND fuera_tiempo = false';
                            $A_tiempo = DB::select($query_tiempo);
                            $data['Porc_Tiempo'][$usuario->Id_user] = $A_tiempo[0]->porcentaje;
                            if ( $A_tiempo[0]->porcentaje > 0 ){
                                    $data['porcentaje_aTiempo'][$usuario->Id_user] = floor((( $A_tiempo[0]->porcentaje )*100)/$total_Act_Planeadas[0]->Cantidad);
                             }
                    }
                }
                ob_start(); ?>
                                    <div class="row mt-5 ml-5 mr-5 bg-white border-round shadow">
                                        <div class="col-md-12">
                                            <p class="h2 color-title-pt">Plan de Trabajo</p>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
                                        </div>
                                        <div class="col-md-12 mt-3 mb-3 ">
                                            <div class="row mt-3 mb-3 ml-3 mr-3 bg-blue-plan h-100 border-round-nc color-white">
                                                <div class="col-md-4 mt-3">
                                                    <div class="row margin-media">
                                                        <div class="col-md-12">
                                                            <a class="h6">Cantidad</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mt-3">
                                                    <a class="h6">Uso tiempo</a>
                                                </div>
                                                <div class="col-md-3 mt-3">
                                                    <a class="h6">Actividades cumplidas</a>
                                                </div>
                                                <div class="col-md-3 mt-3">
                                                    <a class="h6">Hecha a Tiempo</a>
                                                </div>
                                            </div>
                                        </div>


                                        <?php  $sombra=0; foreach($usuarios as $usuario){

                        if(!isset($data['total_Act'][$usuario->Id_user])){ $data['total_Act'][$usuario->Id_user] = 0; }
                        if(!isset($data['total_Act_horas'][$usuario->Id_user])){ $data['total_Act_horas'][$usuario->Id_user] = 0; }
                        if(!isset($data['porcentaje_aTiempo'][$usuario->Id_user])){  $data['porcentaje_aTiempo'][$usuario->Id_user] = 0; }
                        if(!isset($data['cumplidas'][$usuario->Id_user])){  $data['cumplidas'][$usuario->Id_user] = 0; }
                        if(!isset($data['no_cumplidas'][$usuario->Id_user])){  $data['no_cumplidas'][$usuario->Id_user] = 0; }

                        if($sombra == 1){
                            $clase = 'row-underlined-plan';
                            $sombra = -1;
                        }else{
                            $clase = '';
                        }

                        if(empty($usuario->Foto)){
                                $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                        }else{
                                $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                        }



                     ?>
                                        <style>
                                            #chartdiv<?=$usuario->Id_user?> {
                                                width: 100%;
                                                height: 80px;
                                                font-size: 11px;
                                            }

                                        </style>

                                        <div class="col-md-12 mb-3 fila-planTrabajo">
                                            <div class="row ml-3 mr-3 border-round-nc  <?=$clase?> h-100 ">
                                                <div class="col-md-2">
                                                    <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                                </div>
                                                <div class="col-md-2">
                                                    <p class="h3 color-content-g"><strong><?= $data['total_Act'][$usuario->Id_user] ?> Actividades</strong></p>
                                                    <p class="h4 color-title-bitacora">
                                                        <?= $data['total_Act_horas'][$usuario->Id_user] ?> horas</p>
                                                </div>
                                                <div class="col-md-2 btn-view-ut">
                                                    <img class="mt-5" data-toggle="tooltip" data-placement="top" title="Uso Tiempo" src="<?=$request->dir?>/images/ver_oportunidad_identificada.png">
                                                </div>
                                                <div class="col-md-3">
                                                    <div id="chartdiv<?=$usuario->Id_user?>"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row mt-5">
                                                        <div class="col-md-4 text-right">
                                                            <p class="h4 color-title-pen">
                                                                <?= $data['porcentaje_aTiempo'][$usuario->Id_user] ?> % </p>
                                                        </div>
                                                        <div class="col-md-8 mt-3">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" style="width: <?=$data['porcentaje_aTiempo'][$usuario->Id_user]?>%" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            var chart = AmCharts.makeChart("chartdiv<?=$usuario->Id_user?>", {
                                                "type": "pie",
                                                "theme": "light",
                                                "colors": [
                                                    "#67b7dc",
                                                    "#E6E6E6",

                                                ],
                                                "dataProvider": [{
                                                    "title": "Cumplidas",
                                                    "value": <?= $data['cumplidas'][$usuario->Id_user] ?>,
                                                }, {
                                                    "title": "Total",
                                                    "value": <?= $data['no_cumplidas'][$usuario->Id_user] ?>,

                                                }],
                                                "titleField": "title",
                                                "valueField": "value",
                                                "labelRadius": 5,

                                                "radius": "38%",
                                                "innerRadius": "40%",
                                                "labelText": "[[title]]",
                                                "export": {
                                                    "enabled": false
                                                }
                                            });

                                        </script>

                                        <?php
                $sombra++;
              }
              $data['cuerpo'] = ob_get_contents();
              ob_end_clean();
              break;
              /* Gestion Pendientes*/
              case 6:
              /*Usuarios*/
              $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Partner Solution") AND U.deleted_at IS NULL';
              $usuarios = DB::select($query);
              foreach($usuarios as $usuario){
                  /* Primera Tabla*/
                   if(isset($fecha_ff) && isset($fecha_oc)){
                        if(!empty($fecha_ff) && !empty($fecha_oc)){
                          /*Aplazados*/
                            $query_aplazados_porRango = 'SELECT COUNT(id) as Cantrango FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "'.$fecha_ff.'" AND "'.$fecha_oc.'" ';
                            $query_aplazados_porSemana = 'SELECT COUNT(id) as Cantsemana FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-21" AND "2018-01-27"';
                            $query_aplazados_porMes = 'SELECT COUNT(id) as CantMes FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND MONTH(updated_at) = MONTH(CURRENT_TIMESTAMP) AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                            $aplazados_rango = DB::select($query_aplazados_porRango);
                            $aplazados_semana = DB::select($query_aplazados_porSemana);
                            $aplazados_mes = DB::select($query_aplazados_porMes);
                            $data['aplazados_rango'][$usuario->Id_user] = $aplazados_rango[0]->Cantrango;
                            $data['aplazados_semana'][$usuario->Id_user] = $aplazados_semana[0]->Cantsemana;
                            $data['aplazados_mes'][$usuario->Id_user] = $aplazados_mes[0]->CantMes;
                          /*Cancelados*/
                            $query_cancelado_porRango = 'SELECT COUNT(id) as Cantrango FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "'.$fecha_ff.'" AND "'.$fecha_oc.'" ';
                            $query_cancelado_porSemana = 'SELECT COUNT(id) as Cantsemana FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-21" AND "2018-01-27"';
                            $query_cancelado_porMes = 'SELECT COUNT(id) as CantMes FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND MONTH(updated_at) = MONTH(CURRENT_TIMESTAMP) AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                            $cancelado_rango = DB::select($query_cancelado_porRango);
                            $cancelado_semana = DB::select($query_cancelado_porSemana);
                            $cancelado_mes = DB::select($query_cancelado_porMes);
                            $data['cancelado_rango'][$usuario->Id_user] = $cancelado_rango[0]->Cantrango;
                            $data['cancelado_semana'][$usuario->Id_user] = $cancelado_semana[0]->Cantsemana;
                            $data['cancelado_mes'][$usuario->Id_user] = $cancelado_mes[0]->CantMes;
                          /*Aprobados*/
                            $query_aprobado_porRango = 'SELECT COUNT(id) as Cantrango FROM pendientes WHERE estado LIKE "aprobado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "'.$fecha_ff.'" AND "'.$fecha_oc.'" ';
                            $query_aprobado_porSemana = 'SELECT COUNT(id) as Cantsemana FROM pendientes WHERE estado LIKE "aprobado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-21" AND "2018-01-27"';
                            $query_aprobado_porMes = 'SELECT COUNT(id) as CantMes FROM pendientes WHERE estado LIKE "aprobado" AND user_id = '.$usuario->Id_user.' AND MONTH(updated_at) = MONTH(CURRENT_TIMESTAMP) AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                            $aprobado_rango = DB::select($query_aprobado_porRango);
                            $aprobado_semana = DB::select($query_aprobado_porSemana);
                            $aprobado_mes = DB::select($query_aprobado_porMes);
                            $data['aprobado_rango'][$usuario->Id_user] = $aprobado_rango[0]->Cantrango;
                            $data['aprobado_semana'][$usuario->Id_user] = $aprobado_semana[0]->Cantsemana;
                            $data['aprobado_mes'][$usuario->Id_user] = $aprobado_mes[0]->CantMes;
                        }
                    } else {
                          /*Aplazados*/
                            $query_aplazados_porRango = 'SELECT COUNT(id) as Cantrango FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-15" AND "2018-01-31"';
                            $query_aplazados_porSemana = 'SELECT COUNT(id) as Cantsemana FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-21" AND "2018-01-27"';
                            $query_aplazados_porMes = 'SELECT COUNT(id) as CantMes FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND MONTH(updated_at) = MONTH(CURRENT_TIMESTAMP) AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                            $aplazados_rango = DB::select($query_aplazados_porRango);
                            $aplazados_semana = DB::select($query_aplazados_porSemana);
                            $aplazados_mes = DB::select($query_aplazados_porMes);
                            $data['aplazados_rango'][$usuario->Id_user] = $aplazados_rango[0]->Cantrango;
                            $data['aplazados_semana'][$usuario->Id_user] = $aplazados_semana[0]->Cantsemana;
                            $data['aplazados_mes'][$usuario->Id_user] = $aplazados_mes[0]->CantMes;

                          /*Cancelados*/
                            $query_cancelado_porRango = 'SELECT COUNT(id) as Cantrango FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-15" AND "2018-01-31"';
                            $query_cancelado_porSemana = 'SELECT COUNT(id) as Cantsemana FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-21" AND "2018-01-27"';
                            $query_cancelado_porMes = 'SELECT COUNT(id) as CantMes FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND MONTH(updated_at) = MONTH(CURRENT_TIMESTAMP) AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                            $cancelado_rango = DB::select($query_cancelado_porRango);
                            $cancelado_semana = DB::select($query_cancelado_porSemana);
                            $cancelado_mes = DB::select($query_cancelado_porMes);
                            $data['cancelado_rango'][$usuario->Id_user] = $cancelado_rango[0]->Cantrango;
                            $data['cancelado_semana'][$usuario->Id_user] = $cancelado_semana[0]->Cantsemana;
                            $data['cancelado_mes'][$usuario->Id_user] = $cancelado_mes[0]->CantMes;
                          /*Aprobados*/
                            $query_aprobado_porRango = 'SELECT COUNT(id) as Cantrango FROM pendientes WHERE estado LIKE "aprobado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-15" AND "2018-01-31"';
                            $query_aprobado_porSemana = 'SELECT COUNT(id) as Cantsemana FROM pendientes WHERE estado LIKE "aprobado" AND user_id = '.$usuario->Id_user.' AND updated_at BETWEEN "2018-01-21" AND "2018-01-27"';
                            $query_aprobado_porMes = 'SELECT COUNT(id) as CantMes FROM pendientes WHERE estado LIKE "aprobado" AND user_id = '.$usuario->Id_user.' AND MONTH(updated_at) = MONTH(CURRENT_TIMESTAMP) AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                            $aprobado_rango = DB::select($query_aprobado_porRango);
                            $aprobado_semana = DB::select($query_aprobado_porSemana);
                            $aprobado_mes = DB::select($query_aprobado_porMes);
                            $data['aprobado_rango'][$usuario->Id_user] = $aprobado_rango[0]->Cantrango;
                            $data['aprobado_semana'][$usuario->Id_user] = $aprobado_semana[0]->Cantsemana;
                            $data['aprobado_mes'][$usuario->Id_user] = $aprobado_mes[0]->CantMes;
                   }
                  /* Segunda Tabla*/
                      /*Canceladas por Año*/
                        $query_Canceladas = 'SELECT COUNT(id) as Canceladas FROM pendientes WHERE estado LIKE "cancelado" AND user_id = '.$usuario->Id_user.' AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                        $canceladas_Ano = DB::select($query_Canceladas);
                        $data['Año_canceladas'][$usuario->Id_user] = $canceladas_Ano[0]->Canceladas;
                      /*Aplazadas por Año*/
                        $query_Aplazadas = 'SELECT COUNT(id) as Aplazadas FROM pendientes_historiales WHERE estado LIKE "aplazado" AND user_id = '.$usuario->Id_user.' AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                        $aplazadas_Ano = DB::select($query_Aplazadas);
                        $data['Año_aplazadas'][$usuario->Id_user] = $aplazadas_Ano[0]->Aplazadas;
                      /*Vencidas por Año*/
                        $query_Vencidas = 'SELECT COUNT(id) as Vencidas FROM pendientes WHERE estado = "ejecutando" AND fecha_ejecucion < CURRENT_TIMESTAMP AND user_id = '.$usuario->Id_user.' AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                        $vencidas_Ano = DB::select($query_Vencidas);
                        $data['Año_vencidas'][$usuario->Id_user] = $vencidas_Ano[0]->Vencidas;
                      /*Por Aprobar por Año*/
                        $query_por_Aprobar = 'SELECT COUNT(id) as Por_Aprobar FROM pendientes WHERE estado = "terminado" AND user_id = '.$usuario->Id_user.' AND YEAR(updated_at) = YEAR(CURRENT_TIMESTAMP)';
                        $por_Aprobar_Ano = DB::select($query_por_Aprobar);
                        $data['Año_Por_Aprobar'][$usuario->Id_user] = $por_Aprobar_Ano[0]->Por_Aprobar;
                      /*Vigentes Semana - 7 dias desde hoy*/
                        $query_vigentes_semana = 'SELECT COUNT(*) as Vigentes_semana FROM pendientes WHERE estado = "ejecutando" AND user_id = '.$usuario->Id_user.'  AND fecha_ejecucion BETWEEN (NOW() - INTERVAL 1 DAY ) AND DATE_ADD(NOW(), INTERVAL 6 DAY)';
                        $vigentes_semana = DB::select($query_vigentes_semana);
                        $data['Vigentes_Semana'][$usuario->Id_user] = $vigentes_semana[0]->Vigentes_semana;
                      /*Vigentes Mes - desde día 8*/
                        $query_vigentes_mes = 'SELECT COUNT(*) AS Vigentes_mes FROM pendientes WHERE estado = "ejecutando" AND user_id = '.$usuario->Id_user.' AND fecha_ejecucion BETWEEN DATE_ADD(NOW(), INTERVAL 7 DAY) AND DATE_ADD(NOW(), INTERVAL 30 DAY)';
                        $vigentes_mes = DB::select($query_vigentes_mes);
                        $data['Vigentes_Mes'][$usuario->Id_user] = $vigentes_mes[0]->Vigentes_mes;
                      /*Vigentes Total*/
                        $query_total = 'SELECT COUNT(*) AS Vigentes_total FROM pendientes WHERE estado = "ejecutando" AND user_id = '.$usuario->Id_user.' AND fecha_ejecucion >= DATE_ADD(NOW(), INTERVAL 31 DAY) ';
                        $vigentes_total = DB::select($query_total);
                        $data['Vigentes_total'][$usuario->Id_user] = $vigentes_total[0]->Vigentes_total;
              }
              if(!empty($request->fecha_ff) && !empty($request->fecha_oc)){
                    $dia_inicial = date('d',strtotime($request->fecha_ff));
                    $mes_inicial = substr($meses[intval(date('m',strtotime($request->fecha_ff)))], 0, 3);
                    $dia_final = date('d',strtotime($request->fecha_oc));
                    $mes_final = substr($meses[intval(date('m',strtotime($request->fecha_oc)))], 0, 3);
                    if(date("Y",strtotime($request->fecha_ff)) != date("Y",strtotime($request->fecha_oc))){
                        $mes_inicial.=' '.date("Y",strtotime($request->fecha_ff));
                        $mes_final.=' '.date("Y",strtotime($request->fecha_oc));
                    }
                }else{
                    $dia = intval(date('m'));
                    if($dia <= 15){
                        $dia_inicial = 1;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = 15;
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }else{
                        $dia_inicial = 16;
                        $mes_inicial = substr($meses[intval(date('m'))], 0, 3);
                        $dia_final = cal_days_in_month(CAL_GREGORIAN, date('m'),date('Y'));
                        $mes_final = substr($meses[intval(date('m'))], 0, 3);
                    }
                }
                $rango_fechas = $dia_inicial.' '.$mes_inicial.' - '.$dia_final.' '.$mes_final;
              ob_start(); ?>
                                            <div class="col-md-12 mt-5 ">
                                                <div class="row mt-5 ml-5 mr-5 bg-white border-round shadow ">
                                                    <div class="col-md-12">
                                                        <p class="h2 color-title-pen">Gestión Pendientes</p>
                                                    </div>
                                                    <div class="col-md-12 text-right">
                                                        <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
                                                    </div>
                                                    <div class="col-md-12 mt-3 mb-3 e-pendientes">
                                                        <div class="row mt-3 mb-3 ml-3 mr-3 bg-blue-gpendiente h-100 border-round-nc color-white">
                                                            <div class="col-md-6 mt-3">
                                                                <div class="row margin-media">
                                                                    <div class="col-md-12">
                                                                        <a class="h6">Aplazados</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mt-3">
                                                                <a class="h6">Cancelados</a>
                                                            </div>
                                                            <div class="col-md-3 mt-3">
                                                                <a class="h6">Aprobados</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 ">
                                                        <div class="row ml-3 mr-3 border-round-nc h-100 row-0-pendiente">
                                                            <div class="col-md-6">
                                                                <div class="row margin-media border-left-pendiente">
                                                                    <div class="col-md-4">
                                                                        <p class="h5">
                                                                            <?=$rango_fechas?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h5">Semana</p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h5">
                                                                            <?=$meses[intval(date("m"))]?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row border-left-pendiente">
                                                                    <div class="col-md-4">
                                                                        <p class="h5">
                                                                            <?=$rango_fechas?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h5">Semana</p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h5">
                                                                            <?=$meses[intval(date("m"))]?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row border-left-pendiente">
                                                                    <div class="col-md-4">
                                                                        <p class="h5">
                                                                            <?=$rango_fechas?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h5">Semana</p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h5">
                                                                            <?=$meses[intval(date("m"))]?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php  $sombra=0; foreach($usuarios as $usuario){

                        if(!isset($data['aplazados_rango'][$usuario->Id_user])){ $data['aplazados_rango'][$usuario->Id_user] = 0; }
                        if(!isset($data['aplazados_semana'][$usuario->Id_user])){ $data['aplazados_semana'][$usuario->Id_user] = 0; }
                        if(!isset($data['aplazados_mes'][$usuario->Id_user])){ $data['aplazados_mes'][$usuario->Id_user] = 0; }
                        if(!isset($data['cancelado_rango'][$usuario->Id_user])){ $data['cancelado_rango'][$usuario->Id_user] = 0; }
                        if(!isset($data['cancelado_semana'][$usuario->Id_user])){ $data['cancelado_semana'][$usuario->Id_user] = 0; }
                        if(!isset($data['cancelado_mes'][$usuario->Id_user])){ $data['cancelado_mes'][$usuario->Id_user] = 0; }
                        if(!isset($data['aprobado_rango'][$usuario->Id_user])){ $data['aprobado_rango'][$usuario->Id_user] = 0; }
                        if(!isset($data['aprobado_semana'][$usuario->Id_user])){ $data['aprobado_semana'][$usuario->Id_user] = 0; }
                        if(!isset($data['aprobado_mes'][$usuario->Id_user])){ $data['aprobado_mes'][$usuario->Id_user] = 0; }
                        if(!isset($data['Año_canceladas'][$usuario->Id_user])){ $data['Año_canceladas'][$usuario->Id_user] = 0; }
                        if(!isset($data['Año_aplazadas'][$usuario->Id_user])){ $data['Año_aplazadas'][$usuario->Id_user] = 0; }
                        if(!isset($data['Año_vencidas'][$usuario->Id_user])){ $data['Año_vencidas'][$usuario->Id_user] = 0; }
                        if(!isset($data['Año_Por_Aprobar'][$usuario->Id_user])){ $data['Año_Por_Aprobar'][$usuario->Id_user] = 0; }
                        if(!isset($data['Vigentes_Semana'][$usuario->Id_user])){ $data['Vigentes_Semana'][$usuario->Id_user] = 0; }
                        if(!isset($data['Vigentes_Mes'][$usuario->Id_user])){ $data['Vigentes_Mes'][$usuario->Id_user] = 0; }
                        if(!isset($data['Vigentes_total'][$usuario->Id_user])){ $data['Vigentes_total'][$usuario->Id_user] = 0; }



                        if($sombra == 1){
                            $clase = 'row-underlined-pendiente';
                            $sombra = -1;
                        }else{
                            $clase = '';
                        }

                        if(empty($usuario->Foto)){
                                $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                        }else{
                                $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                        }

                     ?>


                                                    <div class="col-md-12 fila-pendientes">
                                                        <div class="row ml-3 mr-3 mb-2 border-round-nc h-100  <?=$clase?> color-pendiente">
                                                            <div class="col-md-3">
                                                                <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['aplazados_rango'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['aplazados_semana'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['aplazados_mes'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="col-md-3">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['cancelado_rango'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['cancelado_semana'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['cancelado_mes'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['aprobado_rango'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['aprobado_semana'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="h2">
                                                                            <?= $data['aprobado_mes'][$usuario->Id_user] ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <?php
                $sombra++;
              }
                ?>

                                                        <div class="col-md-12 mt-5 fila-pendientes">
                                                            <div class="row margen-pendiente ">
                                                                <div class="col-md-6 head-gpendiente ">
                                                                    <div class="row h-100">
                                                                        <div class="col-md-6">
                                                                            <div class="row margin-media h-100 border-l">
                                                                                <div class="col-md-12">
                                                                                    <p class="h5"><strong>Canceladas</strong></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 border-t">
                                                                            <p class="h5"><strong>Aplazadas</strong></p>
                                                                        </div>
                                                                        <div class="col-md-3 border-t">
                                                                            <p class="h5"><strong>Vencidas</strong></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 head-gpendiente">
                                                                    <div class="row h-100">
                                                                        <div class="col-md-3 border-t">
                                                                            <p class="h5"><strong>Por Aprobar</strong></p>
                                                                        </div>
                                                                        <div class="col-md-3 border-t">
                                                                            <p class="h5"><strong>Vigentes Semana</strong></p>
                                                                            <p class="h5"><strong>7 días desde hoy</strong></p>
                                                                        </div>
                                                                        <div class="col-md-3 border-t">
                                                                            <p class="h5"><strong>Vigentes Mes</strong></p>
                                                                            <p class="h5"><strong>desde día 8</strong></p>
                                                                        </div>
                                                                        <div class="col-md-3 border-L">
                                                                            <p class="h5"><strong>Vigentes Total</strong></p>
                                                                            <p class="h5"><strong>desde día 31</strong></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php  $sombra=0; foreach($usuarios as $usuario){


                    if($sombra == 1){
                        $clase = 'row-underlined-pendiente';
                        $sombra = -1;
                    }else{
                        $clase = '';
                    }

                    if(empty($usuario->Foto)){
                        $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                    }else{
                        $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                    }

                ?>

                                                                <div class="col-md-6 mb-5 fila-pendientes">
                                                                    <div class="row h-100">
                                                                        <div class="col-md-3">
                                                                            <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                                                        </div>
                                                                        <div class="col-md-3 color-cancelada">
                                                                            <p class="h2">
                                                                                <?= $data['Año_canceladas'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-3 color-aplazada">
                                                                            <p class="h2">
                                                                                <?= $data['Año_aplazadas'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-3 color-vencida">
                                                                            <p class="h2">
                                                                                <?= $data['Año_vencidas'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 mb-5">
                                                                    <div class="row h-100">
                                                                        <div class="col-md-3 color-aprobar">
                                                                            <p class="h2">
                                                                                <?= $data['Año_Por_Aprobar'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-3 color-vigentes">
                                                                            <p class="h2">
                                                                                <?= $data['Vigentes_Semana'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-3 color-vigentem">
                                                                            <p class="h2">
                                                                                <?= $data['Vigentes_Mes'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-3 color-vigentet">
                                                                            <p class="h2">
                                                                                <?= $data['Vigentes_total'][$usuario->Id_user] ?>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <?php
                $sombra++;

              }

                $data['cuerpo'] = ob_get_contents();
              ob_end_clean();
              break;
            /*Datos */
            case 7:

                /*Responsables*/
                 $query = 'SELECT U.id AS Id_user, U.name AS Nombre, U.foto AS Foto FROM `users` U WHERE (U.cargo LIKE "Ejecutivo Comercial" OR U.cargo LIKE "Partner Solution") AND U.deleted_at IS NULL';
                 $usuarios = DB::select($query);
                 $query_responsables = 'SELECT DISTINCT(responsable) AS resp FROM empresas WHERE responsable != "" ORDER BY resp ASC';
                 $responsable_empresa = DB::select($query_responsables);
                    foreach($usuarios as $usuario){
                        if(isset($fecha_ff) && isset($fecha_oc)){
                            if(!empty($fecha_ff) && !empty($fecha_oc)){
                              /*Actualizacion de datos*/
                                /*Creó Oportunidades*/
                                $query_cantOpp_creadas = 'SELECT COUNT(id) as cantidad FROM oportunidades WHERE user_id = '.$usuario->Id_user.'  AND created_at BETWEEN "'.$fecha_ff.'" AND "'.$fecha_oc.'" ';
                                $cant_creadas_Opp = DB::select($query_cantOpp_creadas);
                                $data['cant_creadas_Opp'][$usuario->Id_user] = $cant_creadas_Opp[0]->cantidad;
                                /*Actualizó Oportunidades*/
                                /*Creó Empresas*/
                                $query_cantEmp_creadas = 'SELECT COUNT(id) as cantidad FROM empresas WHERE user_id = '.$usuario->Id_user.'  AND created_at BETWEEN "'.$fecha_ff.'" AND "'.$fecha_oc.'" ';
                                $cant_creadas_Emp = DB::select($query_cantEmp_creadas);
                                $data['cant_creadas_Emp'][$usuario->Id_user] = $cant_creadas_Emp[0]->cantidad;
                                /*Actualizó Empresas*/
                                /*Creó Contactos/Clientes*/
                                $query_cantCli_creadas = 'SELECT COUNT(id) as cantidad FROM clientes WHERE user_id = '.$usuario->Id_user.'  AND created_at BETWEEN "'.$fecha_ff.'" AND "'.$fecha_oc.'" ';
                                $cant_creadas_Cli = DB::select($query_cantCli_creadas);
                                $data['cant_creadas_Cli'][$usuario->Id_user] = $cant_creadas_Cli[0]->cantidad;

                            }
                         } else {
                        /*Actualizacion de datos*/
                            /*Creó Oportunidades*/
                            $query_cantOpp_creadas = 'SELECT COUNT(id) as cantidad FROM oportunidades WHERE user_id = '.$usuario->Id_user.'  AND YEAR(created_at) = YEAR(NOW())';
                            $cant_creadas_Opp = DB::select($query_cantOpp_creadas);
                            $data['cant_creadas_Opp'][$usuario->Id_user] = $cant_creadas_Opp[0]->cantidad;
                            /*Actualizó Oportunidades*/
                            /*Creó Empresas*/
                            $query_cantEmp_creadas = 'SELECT COUNT(id) as cantidad FROM empresas WHERE user_id = '.$usuario->Id_user.'  AND YEAR(created_at) = YEAR(NOW())';
                            $cant_creadas_Emp = DB::select($query_cantEmp_creadas);
                            $data['cant_creadas_Emp'][$usuario->Id_user] = $cant_creadas_Emp[0]->cantidad;
                            /*Actualizó Empresas*/
                            /*Creó Contactos/Clientes*/
                            $query_cantCli_creadas = 'SELECT COUNT(id) as cantidad FROM clientes WHERE user_id = '.$usuario->Id_user.'  AND YEAR(created_at) = YEAR(NOW())';
                            $cant_creadas_Cli = DB::select($query_cantCli_creadas);
                            $data['cant_creadas_Cli'][$usuario->Id_user] = $cant_creadas_Cli[0]->cantidad;

                        }
                        /*Registros Actuales*/
                            /*Cantidad de Empresas por responsable*/
                            $query_cant_empresas = 'SELECT COUNT(id) as cantidad FROM empresas WHERE responsable = '.$usuario->Id_user.' AND deleted_at IS NULL';
                            $cant_empresas = DB::select($query_cant_empresas);
                            $data['Cant_Empresas'][$usuario->Id_user] = $cant_empresas[0]->cantidad;
                            /*Cantidad de Clientes por responsable*/
                            $query_cant_clientes = 'SELECT COUNT(id) as cantidad FROM clientes  WHERE user_id = '.$usuario->Id_user.' AND deleted_at IS NULL';
                            $cant_clientes = DB::select($query_cant_clientes);
                            $data['Cant_clientes'][$usuario->Id_user] = $cant_clientes[0]->cantidad;

                            /*Promedio de Datos completados por empresas*/
                            $promedio_empresas[$usuario->Id_user] = DB::select('SELECT FORMAT(SUM(CONVERT(IF(IsNull(E.logo), 0, (100/26)*3) + IF(IsNull(E.principal), 0, (100/26)*5) + IF(IsNull(E.nombre), 0, (100/26)*1) + IF(IsNull(E.nit), 0, (100/26)*1) + IF(IsNull(E.pagina_web), 0, (100/26)*1) + IF(IsNull(E.finauguracion), 0, (100/26)*1) + IF(IsNull(E.ciudad), 0, (100/26)*1) + IF(IsNull(E.nombre_p_a_cargo), 0, (100/26)*3) + IF(IsNull(E.cargo), 0, (100/26)*1) + IF(IsNull(E.email_contacto), 0, (100/26)*1) + IF(IsNull(E.email_contabilidad), 0, (100/26)*1) + IF(IsNull(E.observaciones), 0, (100/26)*3),UNSIGNED INTEGER) + (SELECT CONVERT(IF(COUNT(*) > 0, (100/26)*3, 0), UNSIGNED INTEGER) FROM empresa_redessociales ER WHERE ER.empresas_id = E.id))/COUNT(E.id),0) AS Porcentaje FROM empresas E WHERE E.responsable='.$usuario->Id_user.' AND E.deleted_at IS NULL');

                            /*Promedio de Datos completados por clientes*/
                            $query_porcentaje = 'FORMAT(SUM(CONVERT(IF(IsNull(C.foto), 0, (100/47)*5)+IF(IsNull(C.nombres), 0, (100/47)*1)+IF(IsNull(C.apellidos), 0, (100/47)*1)+IF(IsNull(C.empresa_id), 0, (100/47)*1)+IF(IsNull(C.empresa_sede_id), 0, (100/47)*1)+IF(IsNull(C.user_id), 0, (100/47)*1)+IF(IsNull(C.profesion), 0, (100/47)*2)+IF(IsNull(C.jefe_inmediato), 0, (100/47)*1)+IF(IsNull(C.n_a_pesos), 0, (100/47)*2)+IF(IsNull(C.tratamiento), 0, (100/47)*1)+IF(IsNull(C.estado_civil), 0, (100/47)*1)+IF(IsNull(C.cargo), 0, (100/47)*1)+IF(IsNull(C.fecha_nacimiento), 0, (100/47)*4)+IF(IsNull(C.num_hijos), 0, (100/47)*1)+IF(IsNull(C.pais), 0, (100/47)*1)+IF(IsNull(C.departamento), 0, (100/47)*1)+IF(IsNull(C.ciudad), 0, (100/47)*1)+IF(IsNull(C.perfil), 0, (100/47)*5),UNSIGNED INTEGER) + ';
                            $query_porcentaje.= '(SELECT CONVERT(IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)),UNSIGNED INTEGER) FROM cliente_telefonos CT WHERE CT.cliente_id = C.id) + ';
                            $query_porcentaje.= '(SELECT CONVERT(IF(COUNT(*)>0,(100/47)*4,0),UNSIGNED INTEGER) FROM cliente_redessociales CR WHERE CR.cliente_id = C.id) + ';
                            $query_porcentaje.= '(SELECT CONVERT(IF(COUNT(*)=1,(100/47)*3,IF(COUNT(*)=2,(100/47)*6,0)),UNSIGNED INTEGER) FROM cliente_mails CM WHERE CM.cliente_id = C.id))/COUNT(*),0) AS Porcentaje';
                            $promedio_clientes[$usuario->Id_user] = DB::select('SELECT '.$query_porcentaje.' FROM clientes C WHERE C.user_id = '.$usuario->Id_user.' AND C.deleted_at IS NULL');
                    }
                ob_start(); ?>
                                                                    <!-- HTML Headers - Actualizacion de datos -->

                                                                    <div class="col-md-12">
                                                                        <p class="h2 color-title-pen">Actualización de Datos</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-right mb-4">
                                                                        <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer-datos mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
                                                                    </div>
                                                                    <div class="col-md-12 e-datos-actualizacion">
                                                                        <div class="row bottom-up-data">
                                                                            <div class="col-md-4 bg-oportunidades color-white">
                                                                                <p class="h5"><i class="fa fa-rocket mt-2 mb-2 fa-3"></i> Oportunidades</p>
                                                                                <div class="row bg-white">
                                                                                    <div class="col-md-6">
                                                                                        <p class="h6 color-title-pen">Creó</p>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <p class="h6 color-title-pen">Actualizó</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 bg-Empresa color-white">
                                                                                <p class="h5"><i class="fa fa-industry mt-2 mb-2 fa-3"></i> Empresa</p>
                                                                                <div class="row bg-white">
                                                                                    <div class="col-md-6">
                                                                                        <p class="h6 color-title-pen">Creó</p>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <p class="h6 color-title-pen">Actualizó</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 bg-contact color-white">
                                                                                <p class="h5"><i class="fa fa-id-card-o mt-2 mb-2 fa-3"></i> Contactos</p>
                                                                                <div class="row bg-white">
                                                                                    <div class="col-md-6">
                                                                                        <p class="h6 color-title-pen">Creó</p>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <p class="h6 color-title-pen">Actualizó</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <?php  $sombra=0; foreach($usuarios as $usuario){


                    if($sombra == 1){
                        $clase = 'row-underlined-pendiente';
                        $sombra = -1;
                    }else{
                        $clase = '';
                    }

                    if(empty($usuario->Foto)){
                        $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                    }else{
                        $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                    }
                     if(!isset($data['cant_creadas_Emp'][$usuario->Id_user])){ $data['cant_creadas_Emp'][$usuario->Id_user] = 0; }
                      if(!isset($data['cant_creadas_Cli'][$usuario->Id_user])){ $data['cant_creadas_Cli'][$usuario->Id_user] = 0; }
                      if(!isset($data['cant_creadas_Opp'][$usuario->Id_user])){ $data['cant_creadas_Opp'][$usuario->Id_user] = 0; }

                ?>

                                                                    <!-- HTML BODY - Actualizacion de datos -->
                                                                    <div class="col-md-12 fila-datos">
                                                                        <div class="row <?=$clase?>">
                                                                            <div class="col-md-5 ">
                                                                                <div class="row mr-1">
                                                                                    <div class="col-md-4">
                                                                                        <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <p class="h3 color-title-pen">
                                                                                            <?=$data['cant_creadas_Opp'][$usuario->Id_user] ?>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <p class="h3 color-title-pen">-</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <div class="row mr-4">
                                                                                    <div class="col-md-3">
                                                                                        <p class="h3 color-title-pen">
                                                                                            <?= $data['cant_creadas_Emp'][$usuario->Id_user] ?>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <p class="h3 color-title-pen">-</p>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <p class="h3 color-title-pen">
                                                                                            <?=$data['cant_creadas_Cli'][$usuario->Id_user] ?>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <p class="h3 color-title-pen">-</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <?php
                $sombra++;
              }
              $data['cuerpo_actualizacion'] = ob_get_contents();
              ob_end_clean();


              ob_start();  ?>
                                                                        <!-- HTML Headers - Registros Actuales -->

                                                                        <div class="col-md-12">
                                                                            <p class="h2 color-title-pen">Registros Actuales</p>
                                                                        </div>
                                                                        <div class="col-md-12 text-right mb-4">
                                                                            <img src="<?=$request->dir?>/images/icons/printer.svg" class="printer-datos mr-5" data-toggle="tooltip" data-placement="top" title="Imprimir">
                                                                        </div>
                                                                        <div class="col-md-12 e-datos-actuales">
                                                                            <div class="row bottom-up-data">
                                                                                <div class="col-md-6 bg-Empresa color-white">
                                                                                    <p class="h5"><i class="fa fa-industry mt-2 mb-2 fa-3"></i> Empresa</p>
                                                                                    <div class="row bg-white">
                                                                                        <div class="col-md-6">
                                                                                            <p class="h6 color-title-pen">Cantidad</p>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <p class="h6 color-title-pen">Llenado</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 bg-contact color-white">
                                                                                    <p class="h5"><i class="fa fa-id-card-o mt-2 mb-2 fa-3"></i> Contactos</p>
                                                                                    <div class="row bg-white">
                                                                                        <div class="col-md-6">
                                                                                            <p class="h6 color-title-pen">Cantidad</p>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <p class="h6 color-title-pen">Llenado</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <?php  $sombra=0; foreach($usuarios as $usuario){

                      if(!isset($data['Cant_Empresas'][$usuario->Id_user])){ $data['Cant_Empresas'][$usuario->Id_user] = 0; }
                      if(!isset($data['Cant_clientes'][$usuario->Id_user])){ $data['Cant_clientes'][$usuario->Id_user] = 0; }


                    if($sombra == 1){
                        $clase = 'row-underlined-pendiente';
                        $sombra = -1;
                    }else{
                        $clase = '';
                    }

                    if(empty($usuario->Foto)){
                        $foto='https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Foto&w=200&h=200';
                    }else{
                        $foto=$request->dir.'/images/file/clientes/'.$usuario->Foto;
                    }
                ?>
                                                                        <!--HTML BODY - Registros Actuales-->
                                                                        <div class="col-md-12 fila-datos">
                                                                            <div class="row <?=$clase?>">
                                                                                <div class="col-md-4 ">
                                                                                    <div class="row ml-2">
                                                                                        <div class="col-md-4">
                                                                                            <img class="img-circle img-size mt-3" data-toggle="tooltip" data-placement="top" title="<?=$usuario->Nombre?>" src="<?=$foto?>">
                                                                                        </div>
                                                                                        <div class="col-md-8">
                                                                                            <p class="h3 color-title-pen">
                                                                                                <?= $data['Cant_Empresas'][$usuario->Id_user] ?>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="row mr-4">
                                                                                        <div class="col-md-4">
                                                                                            <div class="row mt-4">
                                                                                                <div class="col-md-4 text-right">
                                                                                                    <p class="h5 color-title-pen">
                                                                                                        <?=$promedio_empresas[$usuario->Id_user][0]->Porcentaje?>% </p>
                                                                                                </div>
                                                                                                <div class="col-md-8">
                                                                                                    <div class="progress">
                                                                                                        <div class="progress-bar" role="progressbar" style="width: <?=$promedio_empresas[$usuario->Id_user][0]->Porcentaje?>%" aria-valuenow="<?=$promedio_empresas[$usuario->Id_user][0]->Porcentaje?>" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <p class="h3 color-title-pen">
                                                                                                <?= $data['Cant_clientes'][$usuario->Id_user] ?>
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="row mt-4">
                                                                                                <div class="col-md-4 text-right">
                                                                                                    <p class="h5 color-title-pen">
                                                                                                        <?=$promedio_clientes[$usuario->Id_user][0]->Porcentaje?>% </p>
                                                                                                </div>
                                                                                                <div class="col-md-8">
                                                                                                    <div class="progress">
                                                                                                        <div class="progress-bar" role="progressbar" style="width: <?=$promedio_clientes[$usuario->Id_user][0]->Porcentaje?>%" aria-valuenow="<?=$promedio_clientes[$usuario->Id_user][0]->Porcentaje?>" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                $sombra++;
              }
              $data['cuerpo_actuales'] = ob_get_contents();
              ob_end_clean();
              break;

        }
        return \Response::json(['data'=>$data]);
    }







    function dateDiff($datetime1) {
        $Y = date('Y', strtotime($datetime1));
        $m = date('m', strtotime($datetime1));
        $d = date('d', strtotime($datetime1));
        $date = Carbon::createFromDate($Y,$m,$d)->diff(Carbon::now())->format('%y %m %d');
        return $this->calculardias($date);
    }

    function dateDiff2($datetime1, $datetime2) {
        $Y = date('Y', strtotime($datetime1));
        $m = date('m', strtotime($datetime1));
        $d = date('d', strtotime($datetime1));
        $Y1 = date('Y', strtotime($datetime2));
        $m1 = date('m', strtotime($datetime2));
        $d1 = date('d', strtotime($datetime2));
        $fin = Carbon::createFromDate($Y1, $m1, $d1, 'America/Bogota');
        $date = Carbon::createFromDate($Y,$m,$d)->diff($fin)->format('%y %m %d');
        return $this->calculardias($date);
    }

    function calculardias($numero){
        $numero = explode(' ',$numero);
        /*años a dias*/
        $anos_a_dias = 0;
        if($numero[0]>0){
            $anos_a_dias=intval($numero[0])*365;
        }
        /*meses a dias*/
        $meses_a_dias = 0;
        if($numero[1]>0){
            $meses_a_dias=intval($numero[1])*30;
        }
        $dias = $anos_a_dias+$meses_a_dias+intval($numero[2]);
        return $dias;
    }

    function fecha($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $f=explode('-',$fecha);
            $fecha_formato = $f[2].' '.$meses[intval($f[1])].' '.$f[0];
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }

    }

    function millones($valor){
        $valor = number_format($valor/1000000);
        return $valor.' Millones';
    }

    function millones2($valor){
        $valor = number_format($valor/1000000);
        return $valor;
    }

}
