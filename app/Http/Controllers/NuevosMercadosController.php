<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\NuevosMercado;
use App\NuevosMercadosComentario;
use App\NuevosMercadosAdjunto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class NuevosMercadosController extends Controller
{

    public function index(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Nuevos Mercados" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        $modulo=24;
        $data['permiso_crear']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_crear']="Si";
              }
            }
          }
        }
        $data['permiso_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_eliminar']="Si";
              }
            }
          }
        }
        return view('nuevosmercados.index',['data' => $data]);
    }

    public function VerMercado($id){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Nuevos Mercados" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        $data['profile'] = NuevosMercado::find($id);
        return view('nuevosmercados.vermercado', ['data' => $data]);
    }

    public function CrearNuevoMercado(Request $request){
        if(isset($request->market_title) && isset($request->market_description)){
            $nmarkets = new NuevosMercado;
            $nmarkets->titulo = $request->market_title;
            $nmarkets->descripcion = $request->market_description;
            if (isset($request->market_thumb_values)) {
                $imagen = json_decode($request->market_thumb_values);
                $data = explode(',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre = $antenombre.".jpg";
                Storage::disk('nuevosmercados')->put($nombre,  $foto);
                $nmarkets->foto = "/storage/nuevos_mercados/profile_images/".$nombre;
            }
            if($nmarkets->save()){
                return \Response::json(['res' => 1]);
            }else{
                return \Response::json(['res' => 0, 'msj' => "Error, al guardar nuevo mercado."]);
            }
        }else{
            return \Response::json(['res' => 0, 'msj' => "Por favor, llene todos los campos."]);
        }
    }

    /*Listar los mercados en cards y dependiendo del estado*/
    public function ListarNuevosMercados(Request $request){
        /*Vigente-1 Rechazadas-0 Aceptado-2	*/
        if(isset($request->type_market)){
            $type = $request->type_market;
            $nmarkets = NuevosMercado::where('estado', $type)->where('deleted_at', '=', null)->get();
            ob_start();
            foreach($nmarkets as $markets){?>
            <div class="col-md-4 col-sm-12 my-2">
                    <div class="card shadow-1 h-100">
                       <div class="reveal-overlay d-flex align-items-center">
                           <div class="container w-75 h-75 d-flex align-items-center justify-content-center">
                               <a class="fa fa-eye fa-3x m-4" aria-hidden="true" href="<?php echo url("/nuevos-mercados/ver/{$markets->id}"); ?>" target="_blank"></a>
                               <a class="fa fa-trash-o fa-3x m-4" aria-hidden="true" onclick="DelMarket(<?php echo $markets->id; ?>)"></a>
                           </div>
                       </div>
                        <div class="container h-100">
                            <div class="row h-100">
                                <div class="col-sm-5 p-5">
                                    <img src="<?php echo $markets->foto; ?>" alt="Imagen del mercado" class="rounded-circle img-circle-avatar shadow-3">
                                </div>
                                <div class="col-sm-7">
                                    <h3 class="card-text text-info"><?php echo $markets->titulo; ?>
                                        <br>
                                        <small class=""><span class="badge badge-primary font-weight-bold">5</span> Oportunidades</small>
                                    </h3>
                                    <p class="text-left text-dark"><?php echo $markets->descripcion; ?></p>
                                    <small class="float-right mt-2 bottom-text">Creado <?php
                                           $date = date_create($markets->created_at);
                                           echo date_format($date, 'Y/m/d');
                                    ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            $news_markets_html = ob_get_contents();
            ob_end_clean();
            if($news_markets_html){
                return \Response::json(['res' => 1, 'data' => $news_markets_html]);
            }
        }
    }

    public function EliminarNuevoMercado(Request $request){
        if(isset($request->market_id)){
            if(NuevosMercado::destroy($request->market_id)){
                return \Response::json(['res' => 1, 'msj' => 'Eliminado correctamente']);
            }
        }
    }

    public function LastUpdate(Request $request){
        if(isset($request->new_market_id)){
            $nmarkets = NuevosMercado::find($request->new_market_id);
            $nmarkets->updated_at = time();
            if($nmarkets->save()){
                $nmarkets = NuevosMercado::find($request->new_market_id);
                $date = date_create($nmarkets->updated_at);
                $date = date_format($date, 'Y/m/d');
                return \Response::json(['res' => 1, 'msj' => "Bien.", 'updated_date' => $date]);
            }else{
                return \Response::json(['res' => 0, 'msj' => "No se pudo actualizar."]);
            }
        }
    }

    public function GetUpdatedData(Request $request){
        if(isset($request->new_market_id)){
            ///
        }
    }

    public function GuardarJustificacion(Request $request){
        if(isset($request->new_market_id) && !empty($request->justificacion_nuevo_mercado)){
            $nmarkets = new NuevosMercadosComentario;
            $nmarkets->comentario = $request->justificacion_nuevo_mercado;
            $nmarkets->id_nuevo_mercado = $request->new_market_id;
            $nmarkets->tipo = 1;
            $nmarkets->id_user = Auth::user()->id;
            if($nmarkets->save()){
                $new_estate = $this->ChangeEstate($request->new_market_id, $request->estado_mercado);
                if($new_estate){
                    return \Response::json(['res' => 1, 'msj' => "Comentario guardado", 'estado' => $request->estado_mercado]);
                }else{
                    return \Response::json(['res' => 0, 'msj' => "No se cambió el estado"]);
                }
            }else{
                return \Response::json(['res' => 0, 'msj' => "No se pudo guardar el comentario."]);
            }
        }
    }

    public function ChangeEstate($id_market, $state){
        if($id_market && $state !== false){
            $nmarkets = NuevosMercado::find($id_market);
            $nmarkets->estado = $state;
            if($nmarkets->save()){
                return true;
            }else{
                return false;
            }
        }
    }

    public function CargarAdjuntos(Request $request){
        if(isset($request->id_new_market_attach) && !empty($request->custom_name_file) && !empty($request->attach_new_market)){
            $nmarkets = new NuevosMercadosAdjunto;
            $nmarkets->id_nuevo_mercado = $request->id_new_market_attach;
            $nmarkets->id_user_uploader = Auth::user()->id;
            $nmarkets->tipo_archivo = $request->tipo_archivo;
            $nmarkets->nombre_archivo = $request->custom_name_file;
            $principal = $request->file('attach_new_market');
            $antenombreprincipal = uniqid();
            $extensionprincipal = $principal->getClientOriginalExtension();
            $nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
            $nmarkets->url_archivo = "/storage/nuevos_mercados/attachment_files/".$nombreprincipal;
            \Storage::disk('nuevosmercadosattachments')->put($nombreprincipal,  \File::get($principal));
            if($nmarkets->save()){
                return \Response::json(['res' => 1, 'msj' => "Adjunto guardado."]);
            }else{
                return \Response::json(['res' => 0, 'msj' => "No se pudo guardar el adjunto."]);
            }
        }else{
            return \Response::json(['res' => 0, 'msj' => "Falta información."]);
        }
    }

    /*Llenar adjuntos*/
    public function ListarAdjuntos(Request $request){
        if(isset($request->id_new_market)){
            $nmarkets = NuevosMercadosAdjunto::where('id_nuevo_mercado', $request->id_new_market)->orderBy('tipo_archivo', 'asc')->get();
            return \Response::json(['res' => 1, 'data' => $nmarkets]); 
        }
    }

    public function acciones(Request $request){
        switch($request->accion){
            /*Listado de comentarios*/
            case 0:
                $query = 'SELECT C.comentario, SUBSTRING_INDEX(C.created_at, " ", 1 ) AS CREADO, CONCAT("/images/file/clientes/",U.foto) AS FOTOGRAFIA, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 )," ",SUBSTRING_INDEX(U.apellidos, " ", 1 )) AS NOMBREUSUARIO FROM nuevos_mercados_comentarios C INNER JOIN users U ON C.id_user = U.id WHERE C.deleted_at IS NULL AND C.id_nuevo_mercado = '.$request->id_nuevo_mercado;
                $data['comentarios'] = DB::SELECT($query);
                break;
            /*Listado de archivos adjuntos*/
            case 1:
                $lista_imagenes = NuevosMercadosAdjunto::where('tipo_archivo','1')->get();
                ob_start(); ?>
                <div class="swiper-wrapper">
                <?php
                foreach($lista_imagenes as $lista){
                    ?>
                    <div class="swiper-slide"><img title="<?=$lista->nombre_archivo?>" src="<?=$lista->url_archivo?>" style="width:40%;"></div>
                    <?php
                }
                ?>
                </div>
                <div class="swiper-pagination"></div>
                <?php
                $data['html_imagenes'] = ob_get_contents();
                ob_end_clean();
                $lista_archivos = NuevosMercadosAdjunto::where('tipo_archivo','2')->get();
                ob_start(); ?>
                <p class="h3"><a class="text-success" style="font-size:2rem;" onclick="ver_adjunto()"><i class="fa fa-eye"></i></a></p>
                <div class="swiper-wrapper">
                <?php
                foreach($lista_archivos as $lista){
                    $extencion = explode(".",$lista->url_archivo);
                    if($extencion[1] == "doc" || $extencion[1] == "DOC" || $extencion[1] == "docm" || $extencion[1] == "DOCM" || $extencion[1] == "docx" || $extencion[1] == "DOCX" || $extencion[1] == "dot" || $extencion[1] == "DOT" || $extencion[1] == "dotm" || $extencion[1] == "DOTM" || $extencion[1] == "dotx" || $extencion[1] == "DOTX"){
                        $archivo = '<img src="/images/word.png" class="img-fluid mx-auto d-block" alt="Icono Generico">';
                    }else if($extencion[1] == "csv" || $extencion[1] == "CSV" || $extencion[1] == "dbf" || $extencion[1] == "DBF" || $extencion[1] == "dif" || $extencion[1] == "DIF" || $extencion[1] == "ods" || $extencion[1] == "ODS" || $extencion[1] == "xla" || $extencion[1] == "XLA" || $extencion[1] == "xlam" || $extencion[1] == "XLAM" || $extencion[1] == "xls" || $extencion[1] == "XLS" || $extencion[1] == "xlsb" || $extencion[1] == "XLSB" || $extencion[1] == "xlsm" || $extencion[1] == "XLSM" || $extencion[1] == "xlsx" || $extencion[1] == "XLSX" || $extencion[1] == "xlt" || $extencion[1] == "XLT"){
                        $archivo = '<img src="/images/excel.png" class="img-fluid mx-auto d-block" alt="Icono Generico">';
                    }else if($extencion[1] == "pot" || $extencion[1] == "potm" || $extencion[1] == "potx" || $extencion[1] == "ppa" || $extencion[1] == "ppam" || $extencion[1] == "pps" || $extencion[1] == "ppsm" || $extencion[1] == "ppsx" || $extencion[1] == "ppt" || $extencion[1] == "pptm" || $extencion[1] == "pptx"){
                        $archivo = '<img src="/images/powerpoint.png" class="img-fluid mx-auto d-block" alt="Icono Generico">';
                    }else if($extencion[1] == "pdf" || $extencion[1] == "PDF"){
                        $archivo = '<img src="/images/pdf.png" class="img-fluid mx-auto d-block" alt="Icono Generico">';
                    }else{
                        $archivo = '<a style="font-size:8rem"><i class="fa fa-file"></i></a>';
                    }
                    ?>
                    <div class="swiper-slide" data-ruta="<?=$lista->url_archivo?>"><p title="<?=$lista->nombre_archivo?>"><?=$archivo?></p></div>
                    <?php
                }
                    ?>
                </div>
                <div class="swiper-pagination"></div>
                <?php
                $data['html_archivos'] = ob_get_contents();
                ob_end_clean();
                $lista_videos = NuevosMercadosAdjunto::where('tipo_archivo','3')->get();
                ob_start(); ?>
                <div class="swiper-wrapper">
                <?php
                foreach($lista_videos as $lista){
                    ?>
                    <div class="swiper-slide"><div class="embed-responsive embed-responsive-16by9"><iframe title="<?=$lista->nombre_archivo?>" class="embed-responsive-item" src="<?=$lista->url_archivo?>" allowfullscreen></iframe></div></div>
                    <?php
                }
                ?>
                </div>
                <div class="swiper-pagination"></div>
                <?php
                $data['html_videos'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }
}
