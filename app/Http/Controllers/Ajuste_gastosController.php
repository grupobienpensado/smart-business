<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Ajuste_gastos;
use App\Presupuesto;
use App\User;
use Auth;

class Ajuste_gastosController extends Controller
{
    //

	public function ajuste_save(Request $request){

		$datos=new Ajuste_gastos();
		$datos->anio  = $request->anio;
		$datos->mes  = $request->mes;
		$datos->valor_alimentacion = $request->signo_alimentacion.$request->valor_alimentacion;
		$datos->valor_transporte_interno =$request->signo_transporte_interno.$request->valor_transporte_interno;
		$datos->valor_transporte_intermunicipal = $request->signo_transporte_intermunicipal.$request->valor_transporte_intermunicipal;
		$datos->valor_tiquete_aereo = $request->signo_tiquete_aereo.$request->valor_tiquete_aereo;
		$datos->valor_papeleria = $request->signo_papeleria.$request->valor_papeleria;
		$datos->valor_invitacion_cliente = $request->signo_invitacion_cliente.$request->valor_invitacion_cliente;
		$datos->valor_alquiler_vehiculo = $request->signo_alquiler_vehiculo.$request->valor_alquiler_vehiculo;
		$datos->valor_gasolina_pasaje = $request->signo_gasolina_pasaje.$request->valor_gasolina_pasaje;
		$datos->valor_hotel = $request->signo_hotel.$request->valor_hotel;
		$datos->valor_otros = $request->signo_otros.$request->valor_otros;
		$datos->user_id = Auth::user()->id;
		$datos->save();

		return redirect('/gastos');
	}

	public function ajuste_lista(){
		$permiso_filtro_usuario='No';
		$modulo=13;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario, presupuesto anual" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				$permiso_filtro_usuario="Si";
			  }
            }
          }          
        }
        $permiso_filtro_mes='No';
		$acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select mes, presupuesto anual" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){ 
				$permiso_filtro_mes="Si";
			  }
            }
          }          
        }

        
        $permiso_usr="Ver Sumatoria";
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="información de entrada, presupuestos, ajuste" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
              if($permiso2[0]->permiso == "Ver propio"){ 
                $permiso_usr="Ver propio";
              }
            }
          }          
        }

		$fun=User::findOrFail(Auth::user()->id);
        /*Usuarios que aparecen en el SELECT*/
        $query = 'SELECT C.cargo FROM permiso_modulo_filtros PMF INNER JOIN permiso_cargos C ON PMF.id_cargo = C.id WHERE PMF.deleted_at IS NULL AND PMF.id_permiso_modulo = 13';
        $cargos = DB::SELECT($query);
        $where='';
        if(count($cargos)>0){
            foreach($cargos as $cargo){
                $where.=!empty($where)?' OR cargo = "'.$cargo->cargo.'"':' WHERE (cargo = "'.$cargo->cargo.'"';
            }
        }
        $where.=!empty($where)?")":"";
        /*fin Usuarios que aparecen en el SELECT*/
		$usuarios_fil=DB::select('SELECT * FROM `users` '.$where.' ORDER BY `id` ASC');
		$usuario=User::findOrFail(Auth::user()->id);
		$fecha=date('Y-m-d');
        $fecha = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
        $fecha = date ( 'Y-m-j' , $fecha );
        
		
		$mes=date('m', strtotime($fecha));
		$ano=date('Y', strtotime($fecha));
		if($permiso_usr=="Ver propio"){
			$actividades=DB::select('SELECT * FROM `actividades` WHERE `user_id` = "'.Auth::user()->id.'" AND MONTH ( `fecha_actividad` ) = "'.$mes.'" AND YEAR( `fecha_actividad` ) = "'.$ano.'" ORDER BY `id` ASC');
		}else{
			$actividades=DB::select('SELECT * FROM `actividades` WHERE MONTH ( `fecha_actividad` ) = "'.$mes.'" AND YEAR( `fecha_actividad` ) = "'.$ano.'" ORDER BY `id` ASC');
		}		
		
		$valor['total']=0;
		$valor['alimentacion']=0;
		$valor['transporte_interno']=0;
		$valor['transporte_intermunicipal']=0;
		$valor['tiquete_aereo']=0;
		$valor['papeleria']=0;
		$valor['invitacion_cliente']=0;
		$valor['alquiler_vehiculo']=0;
		$valor['gasolina_pasaje']=0;
		$valor['hotel']=0;
		$valor['otros']=0;
		$valor['salariopropio']=0;
		$valor['salariotercero']=0;
		foreach ($actividades as $p) {

			$valor['alimentacion']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alimentacion)))));
			
			$valor['transporte_interno']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transportes_internos)))));
			
			$valor['transporte_intermunicipal']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transportes_intermunicipales)))));
			
			$valor['tiquete_aereo']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->tiquete_aereo)))));
			
			$valor['papeleria']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->papeleria)))));
			
			$valor['invitacion_cliente']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->invitacion_cliente)))));
			
			$valor['alquiler_vehiculo']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alquiler_vehiculo)))));
			
			$valor['gasolina_pasaje']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->gasolina_pasajes)))));
			
			$valor['hotel']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->hotel)))));
			
			$valor['otros']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->otros)))));

			if($permiso_usr=="Ver propio"){
				$salariopropio=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" AND `id_user`="'.$usuario->id.'" ORDER BY `id` ASC');
			}else{
				$salariopropio=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" AND `id_user`!="" ORDER BY `id` ASC');
			}
			
			$salariotercero=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" AND `id_user`="" ORDER BY `id` ASC');

			
			foreach ($salariopropio as $sp) {
				$valor['salariopropio']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $sp->valor))));
			}

			foreach ($salariotercero as $st) {
				$valor['salariotercero']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $st->valor))));
			}
			
		}
		$valor['total']+=$valor['alimentacion']+$valor['transporte_interno']+$valor['transporte_intermunicipal']+$valor['tiquete_aereo']+$valor['papeleria']+$valor['invitacion_cliente']+$valor['alquiler_vehiculo']+$valor['gasolina_pasaje']+$valor['hotel']+$valor['otros']+$valor['salariopropio']+$valor['salariotercero'];
		
		if($permiso_usr=="Ver propio"){
			$ajustes=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id` = "'.Auth::user()->id.'" AND `mes` = "'.$mes.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
		}else{
			$ajustes=DB::select('SELECT * FROM `ajuste_gastos` WHERE `mes` = "'.$mes.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
		}		

		$valor1['valorReal']['total']=0;
		$valor1['valorReal']['alimentacion']=0;
		$valor1['valorReal']['transporte_interno']=0;
		$valor1['valorReal']['transporte_intermunicipal']=0;
		$valor1['valorReal']['tiquete_aereo']=0;
		$valor1['valorReal']['papeleria']=0;
		$valor1['valorReal']['invitacion_cliente']=0;
		$valor1['valorReal']['alquiler_vehiculo']=0;
		$valor1['valorReal']['gasolina_pasaje']=0;
		$valor1['valorReal']['hotel']=0;
		$valor1['valorReal']['otros']=0;
		$valor1['valorReal']['salariopropio']=0;
		$valor1['valorReal']['salariotercero']=0;

		$valor1['ajuste']['total']=0;
		$valor1['ajuste']['alimentacion']=0;
		$valor1['ajuste']['transporte_interno']=0;
		$valor1['ajuste']['transporte_intermunicipal']=0;
		$valor1['ajuste']['tiquete_aereo']=0;
		$valor1['ajuste']['papeleria']=0;
		$valor1['ajuste']['invitacion_cliente']=0;
		$valor1['ajuste']['alquiler_vehiculo']=0;
		$valor1['ajuste']['gasolina_pasaje']=0;
		$valor1['ajuste']['hotel']=0;
		$valor1['ajuste']['otros']=0;
		$valor1['ajuste']['salariopropio']=0;
		$valor1['ajuste']['salariotercero']=0;

		foreach ($ajustes as $p) {

			if($p->tipo == "valorreal"){
				$valor1['valorReal']['id']=$p->id;
				$valor1['valorReal']['alimentacion']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alimentacion)))));
				
				$valor1['valorReal']['transporte_interno']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_interno)))));
				
				$valor1['valorReal']['transporte_intermunicipal']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_intermunicipal)))));
				
				$valor1['valorReal']['tiquete_aereo']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_tiquete_aereo)))));
				
				$valor1['valorReal']['papeleria']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_papeleria)))));
				
				$valor1['valorReal']['invitacion_cliente']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_invitacion_cliente)))));
				
				$valor1['valorReal']['alquiler_vehiculo']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alquiler_vehiculo)))));
				
				$valor1['valorReal']['gasolina_pasaje']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_gasolina_pasaje)))));
				
				$valor1['valorReal']['hotel']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_hotel)))));
				
				$valor1['valorReal']['otros']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_otros)))));

				$valor1['valorReal']['salariopropio']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariopropio)))));

				$valor1['valorReal']['salariotercero']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariotercero)))));
			}else if($p->tipo == "ajuste"){
				$valor1['ajuste']['id']=$p->id;
				if($p->signo_alimentacion == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}				
				$valor1['ajuste']['alimentacion']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alimentacion)))));

				if($p->signo_transporte_interno == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}				
				$valor1['ajuste']['transporte_interno']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_interno)))));

				if($p->signo_transporte_intermunicipal == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}				
				$valor1['ajuste']['transporte_intermunicipal']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_intermunicipal)))));
				
				if($p->signo_tiquete_aereo == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['tiquete_aereo']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_tiquete_aereo)))));
				
				if($p->signo_papeleria == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['papeleria']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_papeleria)))));
				
				if($p->signo_invitacion_cliente == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['invitacion_cliente']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_invitacion_cliente)))));
				
				if($p->signo_alquiler_vehiculo == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['alquiler_vehiculo']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alquiler_vehiculo)))));
				
				if($p->signo_gasolina_pasaje == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['gasolina_pasaje']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_gasolina_pasaje)))));
				
				if($p->signo_hotel == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['hotel']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_hotel)))));
				
				if($p->signo_otros == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['otros']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_otros)))));

				if($p->signo_salariopropio == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['salariopropio']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariopropio)))));

				if($p->signo_salariotercero == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['salariotercero']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariotercero)))));
			}else{
				$valor1['valor']['id']=$p->id;
			}
						
		}

		if($permiso_usr=="Ver Sumatoria"){
			$valor1['ajuste']['alimentacion']=$valor['alimentacion']-$valor1['valorReal']['alimentacion'];
			$valor1['ajuste']['transporte_interno']=$valor['transporte_interno']-$valor1['valorReal']['transporte_interno'];
			$valor1['ajuste']['transporte_intermunicipal']=$valor['transporte_intermunicipal']-$valor1['valorReal']['transporte_intermunicipal'];
			$valor1['ajuste']['tiquete_aereo']=$valor['tiquete_aereo']-$valor1['valorReal']['tiquete_aereo'];
			$valor1['ajuste']['papeleria']=$valor['papeleria']-$valor1['valorReal']['papeleria'];
			$valor1['ajuste']['invitacion_cliente']=$valor['invitacion_cliente']-$valor1['valorReal']['invitacion_cliente'];
			$valor1['ajuste']['alquiler_vehiculo']=$valor['alquiler_vehiculo']-$valor1['valorReal']['alquiler_vehiculo'];
			$valor1['ajuste']['gasolina_pasaje']=$valor['gasolina_pasaje']-$valor1['valorReal']['gasolina_pasaje'];
			$valor1['ajuste']['hotel']=$valor['hotel']-$valor1['valorReal']['hotel'];
			$valor1['ajuste']['otros']=$valor['otros']-$valor1['valorReal']['otros'];
			$valor1['ajuste']['salariopropio']=$valor['salariopropio']-$valor1['valorReal']['salariopropio'];
			$valor1['ajuste']['salariotercero']=$valor['salariotercero']-$valor1['valorReal']['salariotercero'];
		}
		$valor1['valorReal']['total']+=$valor1['valorReal']['alimentacion']+$valor1['valorReal']['transporte_interno']+$valor1['valorReal']['transporte_intermunicipal']+$valor1['valorReal']['tiquete_aereo']+$valor1['valorReal']['papeleria']+$valor1['valorReal']['invitacion_cliente']+$valor1['valorReal']['alquiler_vehiculo']+$valor1['valorReal']['gasolina_pasaje']+$valor1['valorReal']['hotel']+$valor1['valorReal']['otros']+$valor1['valorReal']['salariopropio']+$valor1['valorReal']['salariotercero'];
		$valor1['ajuste']['total']=$valor1['ajuste']['alimentacion']+$valor1['ajuste']['transporte_interno']+$valor1['ajuste']['transporte_intermunicipal']+$valor1['ajuste']['tiquete_aereo']+$valor1['ajuste']['papeleria']+$valor1['ajuste']['invitacion_cliente']+$valor1['ajuste']['alquiler_vehiculo']+$valor1['ajuste']['gasolina_pasaje']+$valor1['ajuste']['hotel']+$valor1['ajuste']['otros']+$valor1['ajuste']['salariopropio']+$valor1['ajuste']['salariotercero'];

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Gastos y Presupuesto" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		
		return view('ajuste.ajuste',["data" => $data,"valor"=>$valor, "ajustes"=>$ajustes, "usuario"=>$usuario, "valor1"=>$valor1, 'fun'=>$fun, 'usuarios_fil'=>$usuarios_fil,'permiso_filtro_usuario'=>$permiso_filtro_usuario, 'permiso_filtro_mes' => $permiso_filtro_mes, "permiso_usr"=> $permiso_usr]);
	}

	public function filtrarajuste(Request $request){

		$permiso_usr="Ver Sumatoria";
		$modulo=13;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="información de entrada, presupuestos, ajuste" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Ver propio"){ 
                $permiso_usr="Ver propio";
              }
            }
          }          
        }

		if($request->user!=''){
			if($request->user!='sumatoria'){
				$usuario=User::findOrFail($request->user);        
			}else{
				$usuario='';

			}            
        }else{
        	if($permiso_usr=="Ver Propio"){
        		$usuario=User::findOrFail(Auth::user()->id);
        	}else{
        		$usuario='';
        	}
        	
        }

        if($request->mes!=''){
            $mes=date('m', strtotime($request->mes));
			$ano=date('Y', strtotime($request->mes));
        }else{
        	$fecha=date('Y-m-d');
	        $fecha = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
	        $fecha = date ( 'Y-m-j' , $fecha );

			$mes=date('m', strtotime($fecha));
			$ano=date('Y', strtotime($fecha));
        }
		if($usuario!=''){
			$actividades=DB::select('SELECT * FROM `actividades` WHERE `user_id` = "'.$usuario->id.'" AND MONTH ( `fecha_actividad` ) = "'.$mes.'" AND YEAR( `fecha_actividad` ) = "'.$ano.'" ORDER BY `id` ASC');
		}else{
			$actividades=DB::select('SELECT * FROM `actividades` WHERE MONTH ( `fecha_actividad` ) = "'.$mes.'" AND YEAR( `fecha_actividad` ) = "'.$ano.'" ORDER BY `id` ASC');

		}		
		
		$valor['total']=0;
		$valor['alimentacion']=0;
		$valor['transporte_interno']=0;
		$valor['transporte_intermunicipal']=0;
		$valor['tiquete_aereo']=0;
		$valor['papeleria']=0;
		$valor['invitacion_cliente']=0;
		$valor['alquiler_vehiculo']=0;
		$valor['gasolina_pasaje']=0;
		$valor['hotel']=0;
		$valor['otros']=0;
		$valor['salariopropio']=0;
		$valor['salariotercero']=0;
		foreach ($actividades as $p) {

			$valor['alimentacion']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alimentacion)))));
			
			$valor['transporte_interno']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transportes_internos)))));
			
			$valor['transporte_intermunicipal']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transportes_intermunicipales)))));
			
			$valor['tiquete_aereo']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->tiquete_aereo)))));
			
			$valor['papeleria']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->papeleria)))));
			
			$valor['invitacion_cliente']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->invitacion_cliente)))));
			
			$valor['alquiler_vehiculo']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alquiler_vehiculo)))));
			
			$valor['gasolina_pasaje']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->gasolina_pasajes)))));
			
			$valor['hotel']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->hotel)))));
			
			$valor['otros']+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->otros)))));
			
			if($usuario!=''){
				$salariopropio=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" AND `id_user`="'.$usuario->id.'" ORDER BY `id` ASC');
			}else{
				$salariopropio=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" ORDER BY `id` ASC');
			}
			

			if($usuario!=''){
				$salariotercero=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" AND `id_user` = "" ORDER BY `id` ASC');
			}else{
				$salariotercero=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$p->id.'" AND `id_user` = "" ORDER BY `id` ASC');
			}			
			foreach ($salariopropio as $sp) {
				$valor['salariopropio']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $sp->valor))));
			}

			foreach ($salariotercero as $st) {
				$valor['salariotercero']+=(int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $st->valor))));
			}
			
		}
		$valor['total']+=$valor['alimentacion']+$valor['transporte_interno']+$valor['transporte_intermunicipal']+$valor['tiquete_aereo']+$valor['papeleria']+$valor['invitacion_cliente']+$valor['alquiler_vehiculo']+$valor['gasolina_pasaje']+$valor['hotel']+$valor['otros']+$valor['salariopropio']+$valor['salariotercero'];
		
		if($usuario!=''){
			$ajustes=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id` = "'.$usuario->id.'" AND `mes` = "'.$mes.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
		}else{
			$ajustes=DB::select('SELECT * FROM `ajuste_gastos` WHERE `mes` = "'.$mes.'" AND `anio` = "'.$ano.'" ORDER BY `id` ASC');
		}

		$valor1['valorReal']['total']=0;
		$valor1['valorReal']['alimentacion']=0;
		$valor1['valorReal']['transporte_interno']=0;
		$valor1['valorReal']['transporte_intermunicipal']=0;
		$valor1['valorReal']['tiquete_aereo']=0;
		$valor1['valorReal']['papeleria']=0;
		$valor1['valorReal']['invitacion_cliente']=0;
		$valor1['valorReal']['alquiler_vehiculo']=0;
		$valor1['valorReal']['gasolina_pasaje']=0;
		$valor1['valorReal']['hotel']=0;
		$valor1['valorReal']['otros']=0;
		$valor1['valorReal']['salariopropio']=0;
		$valor1['valorReal']['salariotercero']=0;

		$valor1['ajuste']['total']=0;
		$valor1['ajuste']['alimentacion']=0;
		$valor1['ajuste']['transporte_interno']=0;
		$valor1['ajuste']['transporte_intermunicipal']=0;
		$valor1['ajuste']['tiquete_aereo']=0;
		$valor1['ajuste']['papeleria']=0;
		$valor1['ajuste']['invitacion_cliente']=0;
		$valor1['ajuste']['alquiler_vehiculo']=0;
		$valor1['ajuste']['gasolina_pasaje']=0;
		$valor1['ajuste']['hotel']=0;
		$valor1['ajuste']['otros']=0;
		$valor1['ajuste']['salariopropio']=0;
		$valor1['ajuste']['salariotercero']=0;

		foreach ($ajustes as $p) {

			if($p->tipo == "valorreal"){
				$valor1['valorReal']['id']=$p->id;
				$valor1['valorReal']['alimentacion']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alimentacion)))));
				
				$valor1['valorReal']['transporte_interno']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_interno)))));
				
				$valor1['valorReal']['transporte_intermunicipal']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_intermunicipal)))));
				
				$valor1['valorReal']['tiquete_aereo']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_tiquete_aereo)))));
				
				$valor1['valorReal']['papeleria']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_papeleria)))));
				
				$valor1['valorReal']['invitacion_cliente']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_invitacion_cliente)))));
				
				$valor1['valorReal']['alquiler_vehiculo']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alquiler_vehiculo)))));
				
				$valor1['valorReal']['gasolina_pasaje']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_gasolina_pasaje)))));
				
				$valor1['valorReal']['hotel']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_hotel)))));
				
				$valor1['valorReal']['otros']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_otros)))));

				$valor1['valorReal']['salariopropio']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariopropio)))));

				$valor1['valorReal']['salariotercero']+=((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariotercero)))));
			}else if($p->tipo == "ajuste"){
				$valor1['ajuste']['id']=$p->id;
				if($p->signo_alimentacion == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}				
				$valor1['ajuste']['alimentacion']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alimentacion)))));

				if($p->signo_transporte_interno == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}				
				$valor1['ajuste']['transporte_interno']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_interno)))));

				if($p->signo_transporte_intermunicipal == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}				
				$valor1['ajuste']['transporte_intermunicipal']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_transporte_intermunicipal)))));
				
				if($p->signo_tiquete_aereo == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['tiquete_aereo']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_tiquete_aereo)))));
				
				if($p->signo_papeleria == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['papeleria']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_papeleria)))));
				
				if($p->signo_invitacion_cliente == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['invitacion_cliente']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_invitacion_cliente)))));
				
				if($p->signo_alquiler_vehiculo == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['alquiler_vehiculo']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_alquiler_vehiculo)))));
				
				if($p->signo_gasolina_pasaje == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['gasolina_pasaje']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_gasolina_pasaje)))));
				
				if($p->signo_hotel == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['hotel']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_hotel)))));
				
				if($p->signo_otros == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['otros']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_otros)))));

				if($p->signo_salariopropio == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['salariopropio']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariopropio)))));

				if($p->signo_salariotercero == '-'){
					$signo=(-1);
				}else{
					$signo=1;
				}
				$valor1['ajuste']['salariotercero']+=$signo*((int)(str_replace('-', '', str_replace(',', '', str_replace('+', '', $p->valor_salariotercero)))));
			}else{
				$valor1['valor']['id']=$p->id;
			}
						
		}
		if($permiso_usr=="Ver Sumatoria"){
			$valor1['ajuste']['alimentacion']=$valor['alimentacion']-$valor1['valorReal']['alimentacion'];
			$valor1['ajuste']['transporte_interno']=$valor['transporte_interno']-$valor1['valorReal']['transporte_interno'];
			$valor1['ajuste']['transporte_intermunicipal']=$valor['transporte_intermunicipal']-$valor1['valorReal']['transporte_intermunicipal'];
			$valor1['ajuste']['tiquete_aereo']=$valor['tiquete_aereo']-$valor1['valorReal']['tiquete_aereo'];
			$valor1['ajuste']['papeleria']=$valor['papeleria']-$valor1['valorReal']['papeleria'];
			$valor1['ajuste']['invitacion_cliente']=$valor['invitacion_cliente']-$valor1['valorReal']['invitacion_cliente'];
			$valor1['ajuste']['alquiler_vehiculo']=$valor['alquiler_vehiculo']-$valor1['valorReal']['alquiler_vehiculo'];
			$valor1['ajuste']['gasolina_pasaje']=$valor['gasolina_pasaje']-$valor1['valorReal']['gasolina_pasaje'];
			$valor1['ajuste']['hotel']=$valor['hotel']-$valor1['valorReal']['hotel'];
			$valor1['ajuste']['otros']=$valor['otros']-$valor1['valorReal']['otros'];
			$valor1['ajuste']['salariopropio']=$valor['salariopropio']-$valor1['valorReal']['salariopropio'];
			$valor1['ajuste']['salariotercero']=$valor['salariotercero']-$valor1['valorReal']['salariotercero'];
		}
		$valor1['valorReal']['total']+=$valor1['valorReal']['alimentacion']+$valor1['valorReal']['transporte_interno']+$valor1['valorReal']['transporte_intermunicipal']+$valor1['valorReal']['tiquete_aereo']+$valor1['valorReal']['papeleria']+$valor1['valorReal']['invitacion_cliente']+$valor1['valorReal']['alquiler_vehiculo']+$valor1['valorReal']['gasolina_pasaje']+$valor1['valorReal']['hotel']+$valor1['valorReal']['otros']+$valor1['valorReal']['salariopropio']+$valor1['valorReal']['salariotercero'];
		$valor1['ajuste']['total']+=$valor1['ajuste']['alimentacion']+$valor1['ajuste']['transporte_interno']+$valor1['ajuste']['transporte_intermunicipal']+$valor1['ajuste']['tiquete_aereo']+$valor1['ajuste']['papeleria']+$valor1['ajuste']['invitacion_cliente']+$valor1['ajuste']['alquiler_vehiculo']+$valor1['ajuste']['gasolina_pasaje']+$valor1['ajuste']['hotel']+$valor1['ajuste']['otros']+$valor1['ajuste']['salariopropio']+$valor1['ajuste']['salariotercero'];
		$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
		$total_ajus=$valor["total"]-$valor1["valorReal"]["total"];
		if($usuario!=''){
			$imagen=$usuario->foto;
		}else{
			$imagen='essi_admon.jpg';
		}
		$html='';
		$html.='<div class="row cabecera">
  			<div class="col-md-12">
  				<div class="avatar_ajuste">
  					<img src="'.$request->url.'/images/file/clientes/'.$imagen.'" class="rounded-circle img-responsive">
                </div>
  			</div>  			
  		</div>
  		<div class="row titulo">
  			<div class="col-md-12">
  				<h3>Ajuste del mes '.$meses2[(((int)($mes))-1)].' del '.$ano.'</h3>
  			</div>
  		</div>
  		<div class="row cont_tabla">
  			<div class="col-md-12">
  				<div class="row">
		  			<div class="col-md-3 head" style="text-align: right;">
		  				Concepto
		  			</div>
		  			<div class="col-md-3 head">
		  				Valor
		  			</div>
		  			<div class="col-md-3 head">
		  				Valor Real
		  			</div>
		  			<div class="col-md-3 head">
		  				Ajuste
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 total" style="text-align: right;">
		  				Total
		  			</div>
		  			<div class="col-md-3 total" id="total_valor">
		  				$ '.number_format($valor["total"]).'
		  			</div>
		  			<div class="col-md-3 total" id="total_valor_real">
		  				$ '.number_format($valor1["valorReal"]["total"]).'
		  			</div>
		  			<div class="col-md-3 total" id="total_ajuste" style="color: '; if($total_ajus >=0 ){ $html.='green'; }else{ $html.='red'; } $html.='">
		  				$ '.number_format($total_ajus).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Alimentación
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_alimentacion">
		  				$ '.number_format($valor['alimentacion']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_alimentacion" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['alimentacion']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_alimentacion" style="color: ';  if($valor1['ajuste']['alimentacion'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['alimentacion']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Transporte Interno
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_transporte_interno">
		  				$ '.number_format($valor['transporte_interno']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_transporte_interno" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['transporte_interno']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_transporte_interno" style="color: '; if($valor1['ajuste']['transporte_interno'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['transporte_interno']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Transporte Intermunicipal
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_transporte_intermunicipal">
		  				$ '.number_format($valor['transporte_intermunicipal']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_transporte_intermunicipal" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['transporte_intermunicipal']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_transporte_intermunicipal" style="color: '; if($valor1['ajuste']['transporte_intermunicipal'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['transporte_intermunicipal']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Tiquete Aereo
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_tiquete_aereo">
		  				$ '.number_format($valor['tiquete_aereo']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_tiquete_aereo" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['tiquete_aereo']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_tiquete_aereo" style="color: '; if($valor1['ajuste']['tiquete_aereo'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['tiquete_aereo']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Papeleria
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_papeleria">
		  				$ '.number_format($valor['papeleria']).'
		  			</div>
		  			<div class="col-md-3 concepto" >
		  				<input class="form-control2 form-control dinero valor_real" id="vr_papeleria" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['papeleria']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_papeleria" style="color: '; if($valor1['ajuste']['papeleria'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['papeleria']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Invitación Cliente
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_invitacion_cliente">
		  				$ '.number_format($valor['invitacion_cliente']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_invitacion_cliente" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['invitacion_cliente']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_invitacion_cliente" style="color: '; if($valor1['ajuste']['invitacion_cliente'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['invitacion_cliente']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Alquiler Vehiculo
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_alquiler_vehiculo">
		  				$ '.number_format($valor['alquiler_vehiculo']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_alquiler_vehiculo" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['alquiler_vehiculo']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_alquiler_vehiculo" style="color: '; if($valor1['ajuste']['alquiler_vehiculo'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['alquiler_vehiculo']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Gasolina y Pasaje
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_gasolina_pasaje">
		  				$ '.number_format($valor['gasolina_pasaje']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_gasolina_pasaje" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['gasolina_pasaje']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_gasolina_pasaje" style="color: '; if($valor1['ajuste']['gasolina_pasaje'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['gasolina_pasaje']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Hotel
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_hotel">
		  				$ '.number_format($valor['hotel']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_hotel" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['hotel']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_hotel" style="color: '; if($valor1['ajuste']['hotel'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['hotel']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Otros
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_otros">
		  				$ '.number_format($valor['otros']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_otros" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['otros']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_otros" style="color: '; if($valor1['ajuste']['otros'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['otros']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Salario Propio
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_salariopropio">
		  				$ '.number_format($valor['salariopropio']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_salariopropio" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['salariopropio']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_salariopropio" style="color: '; if($valor1['ajuste']['salariopropio'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='">
		  				$ '.number_format($valor1['ajuste']['salariopropio']).'
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Salario Tercero
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_salariotercero">
		  				$ '.number_format($valor['salariotercero']).'
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_salariotercero" type="text" placeholder="$0" value="'.number_format($valor1['valorReal']['salariotercero']).'" style="cursor: no-drop;" disabled>
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_salariotercero" style="color: '; if($valor1['ajuste']['salariotercero'] >=0 ){ $html.= 'green'; }else{ $html.= 'red'; } $html.='" >
		  				$ '.number_format($valor1['ajuste']['salariotercero']).'
		  			</div>
		  		</div>
  			</div>
  		</div>';
		return \Response::json(['html' => $html]);
	}

	public function ajusteguardar(Request $request){
		$fecha=date('Y-m-d');
        $fecha = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
        $fecha = date ( 'Y-m-j' , $fecha );

		if(isset($request->valor['id'])){
			$datos=Ajuste_gastos::findOrFail($request->valor['id']);
		}else{
			$datos=new Ajuste_gastos();
		}
		$datos->anio  = date('Y', strtotime($fecha));
		$datos->mes  = date('m', strtotime($fecha));
		$datos->tipo = $request->valor['tipo'];
		$datos->valor_alimentacion = $request->valor['valor_alimentacion'];
		$datos->valor_transporte_interno =$request->valor['valor_transporte_interno'];
		$datos->valor_transporte_intermunicipal = $request->valor['valor_transporte_intermunicipal'];
		$datos->valor_tiquete_aereo = $request->valor['valor_tiquete_aereo'];
		$datos->valor_papeleria = $request->valor['valor_papeleria'];
		$datos->valor_invitacion_cliente = $request->valor['valor_invitacion_cliente'];
		$datos->valor_alquiler_vehiculo = $request->valor['valor_alquiler_vehiculo'];
		$datos->valor_gasolina_pasaje = $request->valor['valor_gasolina_pasaje'];
		$datos->valor_hotel = $request->valor['valor_hotel'];
		$datos->valor_otros = $request->valor['valor_otros'];
		$datos->valor_salariopropio = $request->valor['valor_salariopropio'];
		$datos->valor_salariotercero = $request->valor['valor_salariotercero'];
		$datos->user_id = Auth::user()->id;
		$datos->save();

		if(isset($request->valorreal['id'])){
			$datos1=Ajuste_gastos::findOrFail($request->valorreal['id']);
		}else{
			$datos1=new Ajuste_gastos();
		}
		$datos1->anio  = date('Y', strtotime($fecha));
		$datos1->mes  = date('m', strtotime($fecha));
		$datos1->tipo = $request->valorreal['tipo'];
		$datos1->valor_alimentacion = $request->valorreal['valor_alimentacion'];
		$datos1->valor_transporte_interno =$request->valorreal['valor_transporte_interno'];
		$datos1->valor_transporte_intermunicipal = $request->valorreal['valor_transporte_intermunicipal'];
		$datos1->valor_tiquete_aereo = $request->valorreal['valor_tiquete_aereo'];
		$datos1->valor_papeleria = $request->valorreal['valor_papeleria'];
		$datos1->valor_invitacion_cliente = $request->valorreal['valor_invitacion_cliente'];
		$datos1->valor_alquiler_vehiculo = $request->valorreal['valor_alquiler_vehiculo'];
		$datos1->valor_gasolina_pasaje = $request->valorreal['valor_gasolina_pasaje'];
		$datos1->valor_hotel = $request->valorreal['valor_hotel'];
		$datos1->valor_otros = $request->valorreal['valor_otros'];
		$datos1->valor_salariopropio = $request->valorreal['valor_salariopropio'];
		$datos1->valor_salariotercero = $request->valorreal['valor_salariotercero'];
		$datos1->user_id = Auth::user()->id;
		$datos1->save();


		if(isset($request->ajuste['id'])){
			$datos2=Ajuste_gastos::findOrFail($request->ajuste['id']);
		}else{
			$datos2=new Ajuste_gastos();
		}
		$datos2->anio  = date('Y', strtotime($fecha));
		$datos2->mes  = date('m', strtotime($fecha));
		$datos2->tipo = $request->ajuste['tipo'];
		$datos2->signo_alimentacion = $request->ajuste['signo_alimentacion'];
		$datos2->valor_alimentacion = $request->ajuste['valor_alimentacion'];
		$datos2->signo_transporte_interno =$request->ajuste['signo_transporte_interno'];
		$datos2->valor_transporte_interno =$request->ajuste['valor_transporte_interno'];
		$datos2->signo_transporte_intermunicipal = $request->ajuste['signo_transporte_intermunicipal'];
		$datos2->valor_transporte_intermunicipal = $request->ajuste['valor_transporte_intermunicipal'];
		$datos2->signo_tiquete_aereo = $request->ajuste['signo_tiquete_aereo'];
		$datos2->valor_tiquete_aereo = $request->ajuste['valor_tiquete_aereo'];
		$datos2->signo_papeleria = $request->ajuste['signo_papeleria'];
		$datos2->valor_papeleria = $request->ajuste['valor_papeleria'];
		$datos2->signo_invitacion_cliente = $request->ajuste['signo_invitacion_cliente'];
		$datos2->valor_invitacion_cliente = $request->ajuste['valor_invitacion_cliente'];
		$datos2->signo_alquiler_vehiculo = $request->ajuste['signo_alquiler_vehiculo'];
		$datos2->valor_alquiler_vehiculo = $request->ajuste['valor_alquiler_vehiculo'];
		$datos2->signo_gasolina_pasaje = $request->ajuste['signo_gasolina_pasaje'];
		$datos2->valor_gasolina_pasaje = $request->ajuste['valor_gasolina_pasaje'];
		$datos2->signo_hotel = $request->ajuste['signo_hotel'];
		$datos2->valor_hotel = $request->ajuste['valor_hotel'];
		$datos2->signo_otros = $request->ajuste['signo_otros'];
		$datos2->valor_otros = $request->ajuste['valor_otros'];
		$datos2->signo_salariopropio = $request->ajuste['signo_salariopropio'];
		$datos2->valor_salariopropio = $request->ajuste['valor_salariopropio'];
		$datos2->signo_salariotercero = $request->ajuste['signo_salariotercero'];
		$datos2->valor_salariotercero = $request->ajuste['valor_salariotercero'];
		$datos2->user_id = Auth::user()->id;
		$datos2->save();

		return redirect('/ajustes');
	}
}
