<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\stdClass;
use App\Oportunidades;
use App\Actividades;
use App\Producto;
use App\Ajuste_gastos;
use App\Presupuesto;
use App\Presupuesto_anual;
use App\ActividadComentario;
use App\Actividades_detalles;
use App\User;
use App\Cargos;
use App\HorasHombre;
use App\Tipo_actividades;
use App\Estado_actividade;
use App\Parafiscales;
use Auth;

class PruebaController extends Controller
{
	public function gastos(){
    	$fun=User::findOrFail(Auth::user()->id);
    	$usuarios_fil=DB::select('SELECT * FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution" ORDER BY `id` ASC');
    	$model = Actividades::all();
    	$lista = Ajuste_gastos::all();
    	$presupuesto_ind = Presupuesto::all();
    	$mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.Auth::user()->id.'" AND `mes`="'.(int)(date('m')).'" ORDER BY `id`');
    	$presupuesto = DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.Auth::user()->id.'" AND `anio` = "'.date('Y').'" ORDER BY `id`');
    	$usuarios = User::all();
    	$ajuste=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.Auth::user()->id.'" AND `mes` = "'.date('m').'" AND `anio`="'.date("Y").'" AND `tipo`="ajuste"');
    	$suma_ajuste=0;
    	if(isset($ajuste[0]->id)){
    		$suma_ajuste=$ajuste[0]->valor_alimentacion+$ajuste[0]->valor_transporte_interno+$ajuste[0]->valor_transporte_intermunicipal+$ajuste[0]->valor_tiquete_aereo+$ajuste[0]->valor_papeleria+$ajuste[0]->valor_invitacion_cliente+$ajuste[0]->valor_alquiler_vehiculo+$ajuste[0]->valor_gasolina_pasaje+$ajuste[0]->valor_hotel+$ajuste[0]->valor_otros;
    	}
    	
    	$ajusteano=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.Auth::user()->id.'" AND `anio`="'.date("Y").'" AND `tipo`="ajuste"');
    	$ajustemeses=[];
    	foreach ($ajusteano as $aa) {
    		for($i=1;$i<=12;$i++){
    			if($i<10){
    				$y='0'.$i;
    			}
    			if($aa->mes==$y){
    				$ajustemeses[$i]=$aa->valor_alimentacion+$aa->valor_transporte_interno+$aa->valor_transporte_intermunicipal+$aa->valor_tiquete_aereo+$aa->valor_papeleria+$aa->valor_invitacion_cliente+$aa->valor_alquiler_vehiculo+$aa->valor_gasolina_pasaje+$aa->valor_hotel+$aa->valor_otros;
    			}
    			
    		}
    	}

    	return view('xx.prueba',["actividades"=>$model,"id"=>Auth::user()->id,"individuales"=>$presupuesto_ind,"ajustes"=>$lista,"anuales"=>$presupuesto,"usuarios"=>$usuarios, "mi_presupuesto"=>$mi_presupuesto, "suma_ajuste"=>$suma_ajuste, 'ajustemeses'=>$ajustemeses, 'fun'=>$fun, 'usuarios'=>$usuarios, 'usuarios_fil'=>$usuarios_fil]);
    }

    public function filtrargastos(Request $request){
    	if($request->usuario!=''){
    		$usuario_f=$request->usuario;
    	}else{
    		$usuario_f=Auth::user()->id;
    	}
    	if($request->mes!=''){
    		$mes_completo_f=$request->mes;
    		$mes_f=date('m', strtotime($request->mes));
    		$ano_f=date('Y', strtotime($request->mes));
    	}else{
    		$mes_completo_f='';
    		$mes_f=date('m');
    		$ano_f=date('Y');
    	}
    	if($request->tipo!=''){
    		$tipo_f=$request->tipo;
    	}else{
    		$tipo_f="";
    	}
    	$filtros['usuario_f']=$usuario_f; 
    	$filtros['mes_completo_f']=$mes_completo_f;
    	$filtros['tipo_f']=$tipo_f;
    	$fun=User::findOrFail(Auth::user()->id);
    	$usuarios_fil=DB::select('SELECT * FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution" ORDER BY `id` ASC');
    	$model = Actividades::all();
    	$lista = Ajuste_gastos::all();
    	$presupuesto_ind = Presupuesto::all();
    	$mi_presupuesto = DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$usuario_f.'" AND `mes`="'.(int)($mes_f).'" ORDER BY `id`');
    	$presupuesto = DB::select('SELECT * FROM `presupuesto_anuals` WHERE `user_id` = "'.$usuario_f.'" AND `anio` = "'.$ano_f.'" ORDER BY `id`');
    	$usuarios = User::all();
    	$ajuste=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.$usuario_f.'" AND `mes` = "'.$mes_f.'" AND `anio`="'.$ano_f.'" AND `tipo`="ajuste"');
    	$suma_ajuste=0;
    	if(isset($ajuste[0]->id)){
    		if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
		      $suma_ajuste=$ajuste[0]->valor_alimentacion;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
		      $suma_ajuste=$ajuste[0]->valor_transporte_interno;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
		      $suma_ajuste=$ajuste[0]->valor_transporte_intermunicipal;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
		      $suma_ajuste=$ajuste[0]->valor_tiquete_aereo;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
		      $suma_ajuste=$ajuste[0]->valor_papeleria;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
		      $suma_ajuste=$ajuste[0]->valor_invitacion_cliente;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
		      $suma_ajuste=$ajuste[0]->valor_alquiler_vehiculo;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
		      $suma_ajuste=$ajuste[0]->valor_gasolina_pasaje;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
		      $suma_ajuste=$ajuste[0]->valor_hotel;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
		      $suma_ajuste=$ajuste[0]->valor_otros;
		    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
		     $suma_ajuste=$ajuste[0]->valor_alimentacion+$ajuste[0]->valor_transporte_interno+$ajuste[0]->valor_transporte_intermunicipal+$ajuste[0]->valor_tiquete_aereo+$ajuste[0]->valor_papeleria+$ajuste[0]->valor_invitacion_cliente+$ajuste[0]->valor_alquiler_vehiculo+$ajuste[0]->valor_gasolina_pasaje+$ajuste[0]->valor_hotel+$ajuste[0]->valor_otros;
		    }
    		
    	}
    	
    	$ajusteano=DB::select('SELECT * FROM `ajuste_gastos` WHERE `user_id`="'.$usuario_f.'" AND `anio`="'.$ano_f.'" AND `tipo`="ajuste"');
    	$ajustemeses=[];
    	foreach ($ajusteano as $aa) {
    		for($i=1;$i<=12;$i++){
    			if($i<10){
    				$y='0'.$i;
    			}
    			if($aa->mes==$y){
    				if(isset($ajuste[0]->id)){
			    		if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
					      $ajustemeses[$i]=$aa->valor_alimentacion;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
					      $ajustemeses[$i]=$aa->valor_transporte_interno;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
					      $ajustemeses[$i]=$aa->valor_transporte_intermunicipal;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
					      $ajustemeses[$i]=$aa->valor_tiquete_aereo;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
					      $ajustemeses[$i]=$aa->valor_papeleria;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
					      $ajustemeses[$i]=$aa->valor_invitacion_cliente;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
					      $ajustemeses[$i]=$aa->valor_alquiler_vehiculo;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
					      $ajustemeses[$i]=$aa->valor_gasolina_pasaje;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
					      $ajustemeses[$i]=$aa->valor_hotel;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
					      $ajustemeses[$i]=$aa->valor_otros;
					    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
					     $ajustemeses[$i]=$aa->valor_alimentacion+$aa->valor_transporte_interno+$aa->valor_transporte_intermunicipal+$aa->valor_tiquete_aereo+$aa->valor_papeleria+$aa->valor_invitacion_cliente+$aa->valor_alquiler_vehiculo+$aa->valor_gasolina_pasaje+$aa->valor_hotel+$aa->valor_otros;
					    }
			    	}
    			}
    			
    		}
    	}

    	return view('xx.prueba',["actividades"=>$model,"id"=>Auth::user()->id,"individuales"=>$presupuesto_ind,"ajustes"=>$lista,"anuales"=>$presupuesto,"usuarios"=>$usuarios, "mi_presupuesto"=>$mi_presupuesto, "suma_ajuste"=>$suma_ajuste, 'ajustemeses'=>$ajustemeses, 'fun'=>$fun, 'usuarios'=>$usuarios, 'usuarios_fil'=>$usuarios_fil, 'filtros'=>$filtros]);
    }
}