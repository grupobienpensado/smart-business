<?php

namespace App\Http\Controllers;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
Use App\User;
Use App\Users_familia;
Use App\Users_estudio;
Use App\Users_meta;
Use App\Users_contacto_emergencia;
Use App\Users_paise;
Use App\Paises;
Use App\Estados;
Use App\Ciudades;
Use App\PermisoCargo;
use Illuminate\Support\Facades\DB;
use Auth;

class FuncionarioController extends Controller
{
    //

    public function save(Request $request){
    	$nombrelogo='';
        $nombrelogo1='';
        $logo = $request->foto_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombrelogo =$antenombre.".png";
            \Storage::disk('cliente')->put($nombrelogo,  $foto);
        }
    	$logo_principal = $request->principal_values;
        if (!empty($logo_principal)) {
            $imagen1 = json_decode($logo_principal);
            $data1 = explode( ',', $imagen1->data );
            $foto1 = base64_decode($data1[1]);

            $antenombre1 = uniqid();
            $nombrelogo1 =$antenombre1.".png";
            \Storage::disk('cliente')->put($nombrelogo1,  $foto1);
        }

        $nomnre1 = explode( ' ', $request->nombres );
        $apellido1 = explode( ' ', $request->apellidos );
    	$datos=new User();
    	$datos->nombres=$request->nombres;
    	$datos->apellidos=$request->apellidos;
      $datos->name=$nomnre1[0].' '.$apellido1[0];
    	$datos->tipo_documento=$request->tipo_documento;
    	$datos->documento=$request->documento;
    	$datos->profesion=$request->profesion;
    	$datos->cargo=$request->cargo;
    	$datos->telefono_personal=$request->telefono_personal;
        $datos->telefono_corporativo=$request->telefono_corporativo;
    	$datos->cargo=$request->cargo;
    	$datos->correo_personal=$request->correo_personal;
        $datos->correo_corporativo=$request->correo_corporativo;
        $datos->tipo_sangre=$request->tipo_sangre;
    	$datos->fecha_nacimiento=$request->fecha_nacimiento;
    	$datos->pais_nacimiento=$request->pais_nacimiento;
    	$datos->departamento_nacimiento=$request->departamento_nacimiento;
    	$datos->ciudad_nacimiento=$request->ciudad_nacimiento;
        $datos->pais_residencia=$request->pais_residencia;
        $datos->departamento_residencia=$request->departamento_residencia;
        $datos->ciudad_residencia=$request->ciudad_residencia;
        $datos->direccion=$request->direccion;
        $datos->descripcion=$request->descripcion;
        $datos->lat=$request->lat;
        $datos->lon=$request->lng;
        $datos->fecha_vinculacion=$request->fecha_vinculacion;
        $datos->color=$request->color;
        $datos->estado_civil=$request->estado_civil;
        $datos->salario_basico=$request->salario_basico;
        $datos->bonificacion_fija=$request->bonificacion_fija;
        $datos->aux_transporte=$request->aux_transporte;
        $datos->aux_alimentacion=$request->aux_alimentacion;
        $datos->aux_vivienda=$request->aux_vivienda;
        $datos->otros_auxilios=$request->otros_auxilios;
        $datos->hijos=$request->hijos;
    	$datos->username=$request->documento;
    	$datos->password=Hash::make($request->documento);
    	$datos->foto=$nombrelogo;
        $datos->foto_completa=$nombrelogo1;
        $datos->principal=strtolower(str_replace(" ","_",$request->principal)).".png";
    	$datos->save();
        if (isset($request->alternas) && !empty($request->alternas)) {
            foreach ($request->alternas as $key) {
                # code...
                $alterno=new Users_paise();
                $alterno->user=$datos->id;
                $alterno->nombre=$key;
                $alterno->pais=strtolower(str_replace(" ","_",$key)).".png";
                $alterno->user_id=Auth::user()->id;
                $alterno->save();
            }
        }

        $contacto= new Users_contacto_emergencia();
        $contacto->nombre=$request->nombre_emergencia;
        $contacto->parentesco=$request->parentesco_emergencia;
        $contacto->telefono=$request->telefono;
        $contacto->users=$datos->id;
        $contacto->user_id=Auth::user()->id;
        $contacto->save();

        if($request->estado_civil=="Casado/a"||$request->estado_civil=="Unión Libre"){
            $familia=new Users_familia();
            $familia->parentesco=$request->familia[0]->parentesco;
            $familia->nombre=$request->familia[0]->nombre;
            $familia->fecha_nacimiento=$request->familia[0]->fecha_nacimiento;
            $familia->users=$datos->id;
            $familia->user_id=Auth::user()->id;
            $familia->save();
        }
        if($request->hijos>0){
          if(count($request->hijos_inf)>0){
            foreach($request->hijos_inf as $key){
                $familia=new Users_familia();
                $familia->parentesco=$key["parentesco"];
                $familia->nombre=$key["nombre"];
                $familia->fecha_nacimiento=$key["fecha_nacimiento"];
                $familia->users=$datos->id;
                $familia->user_id=Auth::user()->id;
                $familia->save();
            }
          }
        }
        if(count($request->meta)>0){
            foreach ($request->meta as $key) {
                $familia=new Users_meta();
                $familia->mes=$key["mes"];
                $familia->oportunidad_identificada=$key["oportunidad_identificada"];
                $familia->monto_identificado=$key["monto_identificado"];
                $familia->oportunidad_concretada=$key["oportunidad_concretada"];
                $familia->monto_concretado=$key["monto_concretado"];
                $familia->users=$datos->id;
                $familia->user_id=Auth::user()->id;
                $familia->save();
            }
        }
        foreach ($request->educacion as $key) {
            $estudio=new Users_estudio();
            $estudio->titulo=$key["titulo"];
            $estudio->anio=$key["anio"];
            $estudio->institucion=$key["institucion"];
            $estudio->users=$datos->id;
            $estudio->user_id=Auth::user()->id;
            $estudio->save();
        }

    	return redirect("funcionarios");

    }

    public function index(){
        /*Permiso para Agregar*/
        $modulo=16;
        $data['permiso_agregar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="agregar usuario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar']="Si";
              }
            }
          }
        }

        /*Permiso para Editar*/
        $data['permiso_editar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="editar funcionario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_editar']="Si";
              }
            }
          }
        }
        /*Permiso para Ver*/
        $data['permiso_ver']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Ver usuario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_ver']="Si";
              }
            }
          }
        }

        $lista=User::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Funcionarios" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("usuarios.funcionarios",["data" => $data, "lista"=>$lista]);
    }

    public function buscar_funcionario($id){
        /*$datos=User::findOrFail($id);
        $contacto=Users_contacto_emergencia::all();
        $estudio=Users_estudio::all();
        $familia=Users_familia::all();
        $metas=Users_meta::all();

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Funcionarios" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        /*return view("usuarios.funcionario",["data" => $data, "usuario"=>$datos,"estudios"=>$estudio,"familias"=>$familia,"metas"=>$metas,"contacto"=>$contacto]);*/

        $data['funcionario'] = User::findOrFail($id);
        $nombre = explode(' ',$data['funcionario']->nombres);
        $apellido = explode(' ',$data['funcionario']->apellidos);
        $data['nombre'] = $nombre[0].' '.$apellido[0];
        $data['paises'] = Paises::all();
        $data['estados'] = DB::select('SELECT * FROM estados WHERE country_id = (SELECT id FROM paises WHERE name LIKE "'.$data['funcionario']->pais_nacimiento.'" LIMIT 0,1)');
        $data['ciudades'] = DB::select('SELECT * FROM ciudades WHERE state_id = (SELECT id FROM estados WHERE name LIKE "'.$data['funcionario']->departamento_nacimiento.'" LIMIT 0,1)');
        $data['estados2'] = DB::select('SELECT * FROM estados WHERE country_id = (SELECT id FROM paises WHERE name LIKE "'.$data['funcionario']->pais_residencia.'" LIMIT 0,1)');
        $data['ciudades2'] = DB::select('SELECT * FROM ciudades WHERE state_id = (SELECT id FROM estados WHERE name LIKE "'.$data['funcionario']->departamento_residencia.'" LIMIT 0,1)');
        $data['contacto_emergencia'] = Users_contacto_emergencia::where('users','=',$id)->get();
        $data['Conyuge'] = Users_familia::where('users', $id)->where('parentesco', "LIKE", "Conyuge")->get();
        $data['hijos'] = Users_familia::where('users', $id)->where('parentesco', "LIKE", "Hijo")->get();
        $data['estudio'] = Users_estudio::where('users', $id)->get();
        $data['meses'] = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $data['meta'] = DB::select('SELECT * FROM users_metas WHERE users = '.$id.' AND anio LIKE "'.date('Y').'" ORDER BY CONVERT(mes,UNSIGNED INTEGER) ASC');
        $data['paises_secundarios'] = Users_paise::where('user','=',$id)->get();
        return view("usuarios.funcionariover",["data" => $data]);
    }

    public function editarfuncionario($id){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Funcionarios" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        $data['funcionario'] = User::findOrFail($id);
        $nombre = explode(' ',$data['funcionario']->nombres);
        $apellido = explode(' ',$data['funcionario']->apellidos);
        $data['nombre'] = $nombre[0].' '.$apellido[0];
        $data['paises'] = Paises::all();
        $data['estados'] = DB::select('SELECT * FROM estados WHERE country_id = (SELECT id FROM paises WHERE name LIKE "'.$data['funcionario']->pais_nacimiento.'" LIMIT 0,1)');
        $data['ciudades'] = DB::select('SELECT * FROM ciudades WHERE state_id = (SELECT id FROM estados WHERE name LIKE "'.$data['funcionario']->departamento_nacimiento.'" LIMIT 0,1)');
        $data['estados2'] = DB::select('SELECT * FROM estados WHERE country_id = (SELECT id FROM paises WHERE name LIKE "'.$data['funcionario']->pais_residencia.'" LIMIT 0,1)');
        $data['ciudades2'] = DB::select('SELECT * FROM ciudades WHERE state_id = (SELECT id FROM estados WHERE name LIKE "'.$data['funcionario']->departamento_residencia.'" LIMIT 0,1)');
        $data['contacto_emergencia'] = Users_contacto_emergencia::where('users','=',$id)->get();
        $data['Conyuge'] = Users_familia::where('users', $id)->where('parentesco', "LIKE", "Conyuge")->get();
        $data['hijos'] = Users_familia::where('users', $id)->where('parentesco', "LIKE", "Hijo")->get();
        $data['estudio'] = Users_estudio::where('users', $id)->get();
        $data['meses'] = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $data['meta'] = DB::select('SELECT * FROM users_metas WHERE users = '.$id.' AND anio LIKE "'.date('Y').'" ORDER BY CONVERT(mes,UNSIGNED INTEGER) ASC');
        $data['paises_secundarios'] = Users_paise::where('user','=',$id)->get();
        $data['cargos'] = PermisoCargo::all();
        return view("usuarios.funcionarioeditar",["data" => $data]);
    }

    public function action(Request $request){
        $accion = $request->action;
        switch($accion){
            case 0:
                $familia = Users_familia::where('users', $request->id)->where('parentesco', "=", "Conyuge")->get();
                ob_start(); ?>
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="conyugue">Conyugue</label>
                    <input class="form-control" type="hidden" name="familia[0][parentesco]" value="Cónyuge">
                    <input class="form-control" type="text" id="conyugue" name="familia[0][nombre]" placeholder="Conyugue" value="<?php if(isset($familia[0]->nombre)){ echo $familia[0]->nombre; } ?>">
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="conyugue_fecha_nacimiento">Fecha Nacimiento</label>
                    <input class="form-control fecha_ff" id="conyugue_fecha_nacimiento" name="familia[0][fecha_nacimiento]" placeholder="Fecha Nacimiento" value="<?php if(isset($familia[0]->fecha_nacimiento)){ echo $familia[0]->fecha_nacimiento; } ?>">
                </div>
                <?php
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;

            case 1:
                $numero = $request->hijos;
                $familia = Users_familia::where('users', $request->id)->where('parentesco', "=", "Hijo")->get();
                ob_start();
                for($i=0;$i<$numero;$i++){ ?>
                <?php if($i == 0){ ?>
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                </div>
                <?php } ?>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="pais_residencia">Nombre del hijo</label>
                    <input type="hidden" value="Hijo" name="hijos_inf[<?=$i?>][parentesco]">
                    <input class="form-control" type="text" name="hijos_inf[<?=$i?>][nombre]" value="<?php if(isset($familia[$i]->nombre)){ echo $familia[$i]->nombre; } ?>">
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="pais_residencia">Fecha de nacimiento</label>
                    <input class="form-control fecha_ff" type="text" name="hijos_inf[<?=$i?>][fecha_nacimiento]" value="<?php if(isset($familia[$i]->fecha_nacimiento)){ echo $familia[$i]->fecha_nacimiento; } ?>">
                </div>
                <?php
                }
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
            case 2:
                $numero = $request->numero;
                ob_start(); ?>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3 titulo-obtenido<?=$numero?>">
                    <label for="titulo">Titulo Obtenido</label>
                    <input class="form-control" type="text" name="educacion[<?=$numero?>][titulo]" placeholder="Titulo Obtenido">
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3 text-left mt-3 titulo-obtenido<?=$numero?>">
                    <label for="ano">Año culminación</label>
                    <input class="form-control" type="number" name="educacion[<?=$numero?>][anio]" placeholder="Año culminación">
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3 titulo-obtenido<?=$numero?>">
                    <label for="institucion">Institución</label>
                    <input class="form-control" type="text" name="educacion[<?=$numero?>][institucion]" placeholder="Institución">
                </div>
                <div class="col-sm-12 col-md-1 col-lg-1 mt-3 titulo-obtenido<?=$numero?>">
                    <a class="btn btn-danger mt-4" onclick="eliminar_titulo('.titulo-obtenido<?=$numero?>')"><i class="fa fa-trash white"></i></a>
                </div>
                <?php
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
            case 3:
                $pais = Paises::where('name','LIKE',$request->pais)->get();
                ob_start(); ?>
                <img class="form-control pais-p" src="<?=$request->url?>/images/icons/<?=$pais[0]->imagen?>">
                <?php
                $data['cuerpo'] = ob_get_contents();
                ob_end_clean();
                break;
            case 4:
                if(!empty($request->paises)){
                    ob_start(); ?>
                    <div class="row">
                    <?php
                    foreach($request->paises as $pais){
                        $paises = Paises::where('name','LIKE',$pais)->get(); ?>
                        <div class="col-sm-4 col-md-2 col-lg-2">
                            <img class="form-control pais-s" src="<?=$request->url?>/images/icons/<?=$paises[0]->imagen?>">
                        </div>
                        <?php
                    } ?>
                    </div>
                    <?php
                    $data['cuerpo'] = ob_get_contents();
                    ob_end_clean();
                }else{
                    $data['cuerpo'] = '';
                }
                break;
            case 5:
                $formulario = $request->formulario;

                /*Fotos*/
                $logo_anterior=$request->foto;
                $logo = $request->foto_values;
                if (!empty($logo)) {
                    $imagen = json_decode($logo);
                    $data = explode( ',', $imagen->data );
                    $foto = base64_decode($data[1]);
                    $antenombre = uniqid();
                    $nombrelogo =$antenombre.".png";
                    \Storage::disk('cliente')->put($nombrelogo,  $foto);
                    if($logo_anterior!=''){
                        Storage::delete('../images/file/clientes/'.$logo_anterior);
                    }
                }
                $logo_principal_anterior=$request->foto_completa;
                $logo_principal = $request->principal_values;
                if (!empty($logo_principal)) {
                    $imagen1 = json_decode($logo_principal);
                    $data1 = explode( ',', $imagen1->data );
                    $foto1 = base64_decode($data1[1]);

                    $antenombre1 = uniqid();
                    $nombrelogo1 =$antenombre1.".png";
                    \Storage::disk('cliente')->put($nombrelogo1,  $foto1);
                    if($logo_principal_anterior!=''){
                        File::delete(public_path().$id.'archivo.txt');
                        Storage::delete('../images/file/clientes/'.$logo_principal_anterior);
                    }
                }
                /*Fin Fotos*/

                /*Datos Basicos*/
                $dato=User::findOrFail($request->id);
                foreach($formulario as $index=>$value){
                    if($index == 'password'){
                        if($value != ''){
                            $dato->password = Hash::make($value);
                        }
                    }else if($index == 'principal'){
                        if(!empty($value)){
                            $dato->$index = strtolower(str_replace(" ","_",$value)).".png";
                        }
                    }else if($index == 'salario_basico' || $index == 'bonificacion_fija' || $index == 'aux_transporte' || $index == 'aux_alimentacion' || $index == 'aux_vivienda' || $index == 'otros_auxilios'){
                          if($value == ''){
                              $dato->$index = 0;
                          }else{
                              $dato->$index = $value;
                          }
                    }else{
                        $dato->$index = $value;
                    }
                }
                if(isset($nombrelogo)){ $dato->foto = $nombrelogo; }
                if(isset($nombrelogo1)){ $dato->foto_completa = $nombrelogo1; }
                if($dato->save()){
                    $data['msj1'] = "Guardado Datos Basico";
                }else{
                    $data['msj1'] = "No guardado Datos Basico";
                }
                /*Fin Datos Basicos*/

                /*Datos contacto de Emergencia*/
                $datos = Users_contacto_emergencia::where('users', $request->id)->get();
                foreach($datos as $dato){
                    $dato->delete();
                }
                $dato2= new Users_contacto_emergencia;
                foreach($request->contacto_emergencia as $index=>$value){
                    $dato2->$index = $value;
                }
                $dato2->users = $request->id;
                $dato2->user_id = Auth::user()->id;
                if($dato2->save()){
                    $data['msj2'] = "Guardado Contacto de Emergencia";
                }else{
                    $data['msj2'] = "No guardado Contacto de Emergencia";
                }
                /*FIN Datos contacto de Emergencia*/

                /*Datos Familia*/
                $datos = Users_familia::where('users', $request->id)->get();
                foreach($datos as $dato){
                    $dato->delete();
                }
                $dato3= new Users_familia;
                if(isset($request->familia[0])){
                    foreach($request->familia[0] as $index=>$value){
                        $dato3->$index = $value;
                    }
                    $dato3->users = $request->id;
                    $dato3->user_id = Auth::user()->id;
                    if($dato3->save()){
                        $data['msj3'] = "Guardado Datos Familia-Conyugue";
                    }else{
                        $data['msj3'] = "No guardado Datos Familia-Conyugue";
                    }
                }

                if(isset($request->hijos_inf)){
                    foreach($request->hijos_inf as $hijos){
                        $dato31= new Users_familia;
                        foreach($hijos as $index=>$value){
                            $dato31->$index = $value;
                        }
                        $dato31->users = $request->id;
                        $dato31->user_id = Auth::user()->id;
                        if($dato31->save()){
                            $data['msj31'] = "Guardado Datos Familia-Hijos";
                        }else{
                            $data['msj31'] = "No guardado Datos Familia-Hijos";
                        }
                    }
                }
                /*FIN Datos Familia*/

                /*Datos Educación*/
                $datos = Users_estudio::where('users', $request->id)->get();
                foreach($datos as $dato){
                    $dato->delete();
                }
                foreach($request->educacion as $educacion){
                    $dato4= new Users_estudio;
                    foreach($educacion as $index=>$value){
                        $dato4->$index = $value;
                    }
                    $dato4->users = $request->id;
                    $dato4->user_id = Auth::user()->id;
                    if($dato4->save()){
                        $data['msj4'] = "Guardado Datos Educación";
                    }else{
                        $data['msj4'] = "No guardado Datos Educación";
                    }
                }
                /*FIN Datos Educación*/

                /*Datos metas para el año Actual*/
                $datos = Users_meta::where('users', $request->id)->where('anio', "LIKE", date('Y'))->get();
                foreach($datos as $dato){
                    $dato->delete();
                }
                foreach($request->metas as $metas){
                    $dato6= new Users_meta;
                    $dato6->anio = date('Y');
                    foreach($metas as $index=>$value){
                        $dato6->$index=$value;
                    }
                    $dato6->users = $request->id;
                    $dato6->user_id = Auth::user()->id;
                    if($dato6->save()){
                        $data['msj6'] = "Guardado Datos metas";
                    }else{
                        $data['msj6'] = "No guardado Datos metas";
                    }
                }
                /*FIN Datos metas para el año Actual*/

                /*Datos Paises secundarios*/
                $datos = Users_paise::where('user', $request->id)->get();
                foreach($datos as $dato){
                    $dato->delete();
                }
                if(!empty($request->paises_secundarios)){
                    foreach($request->paises_secundarios as $value){
                        $dato5= new Users_paise;
                        $dato5->nombre = $value;
                        $dato5->pais = strtolower(str_replace(" ","_",$value)).".png";
                        $dato5->user = $request->id;
                        $dato5->user_id = Auth::user()->id;
                        if($dato5->save()){
                            $data['msj5'] = "Guardado Datos Paises Secundarios";
                        }else{
                            $data['msj5'] = "No guardado Datos Paises Secundarios";
                        }
                    }
                }
                /*Fin Datos Paises secundarios*/
                break;
        }

        return \Response::json(['data' => $data]);
    }
}
