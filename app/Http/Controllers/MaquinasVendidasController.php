<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\MaquinasVendida;
use Auth;
use App\Empresa;
use App\Producto;
use App\ProductoReferencias;
use App\EmpresaSedes;
use App\Cliente;
use App\User;
use App\ViewOportunidades;


class MaquinasVendidasController extends Controller
{
	public function viewcreate(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Maquinas vendidas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		$modulo=8;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="crear maquina vendida" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				return view('maquinasvendidas.create',['data' => $data]);
			  }else{
				return view('oportunidades.nopermiso');
			  }
            }
          }          
        }		
    }

    public function edit($id){
    	$model = MaquinasVendida::findOrFail($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Maquinas vendidas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
		return view('maquinasvendidas.edit',['data' => $data, 'model' => $model]);
    }

    public function eliminar($id){
    	$model = MaquinasVendida::find($id);
    	if (isset($model) && !empty($model)) {
    		$model->delete();
    		return \Response::json(['success' => true]);
    	}else{
    		return \Response::json(['success' => false]);
    	}
    }

    public function viewmaquina($id){
    	$model = MaquinasVendida::findOrFail($id);
    	$empresa = Empresa::find($model->empresa);
    	$producto = Producto::find($model->producto); 
    	$referencia = ProductoReferencias::find($model->referencia);
    	$sede = EmpresaSedes::find($model->sede);
    	$cliente = Cliente::find($model->cliente);
    	$vendedor = User::find($model->vendedor_id);
  		//return view('maquinasvendidas.view',['model' => $model]);
  		return \Response::json(['success' => true,'model'=>$model,'empresa'=>$empresa,'producto'=>$producto,'referencia'=>$referencia,'sede'=>$sede,'cliente'=>$cliente,'vendedor'=>$vendedor]);
    }

    public function listAjax(){
    	$permiso_editar="No";
    	$modulo=8;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar maquina vendida" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				$permiso_editar="Si";
			  }else{
				$permiso_editar="No";
			  }
            }
          }          
        }
        $permiso_eliminar="No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón eliminar maquina vendida" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){ 
				$permiso_eliminar="Si";
			  }else{
				$permiso_eliminar="No";
			  }
            }
          }          
        }
    	$datos = DB::select('SELECT M.id, P.name, R.referencia, M.valor_venta, R.foto, M.ano_venta, S.lat, S.lng, M.observaciones, E.nombre, E.logo, U.name AS nombres, S.ciudad FROM maquinas_vendidas M INNER JOIN productos P ON P.id = M.producto INNER JOIN producto_referencias R ON R.id = M.referencia INNER JOIN empresa_sedes S ON S.id = M.sede INNER JOIN empresas E ON E.id = M.empresa LEFT JOIN users U ON U.id = M.vendedor_id');
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Maquinas vendidas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('maquinasvendidas.list',['data' => $data, 'datos' => $datos, 'permiso_editar' => $permiso_editar, 'permiso_eliminar' => $permiso_eliminar]);
    }

    public function lista(){
        $permiso_editar="No";
    	$modulo=8;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar maquina vendida" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){
				$data['permiso_editar']="Si";
			  }else{
				$data['permiso_editar']="No";
			  }
            }
          }
        }
        $permiso_eliminar="No";
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón eliminar maquina vendida" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_eliminar']="Si";
			  }else{
				$data['permiso_eliminar']="No";
			  }
            }
          }
        }

        /*Permiso para acceder Maquinas Vendidas*/
        $data['permiso_maquinasvendidas']='No';
        $modulo=8;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Acceso Mapa, Calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){
				$data['permiso_maquinasvendidas']='Si';
			  }
            }
          }
        }
        /*Permiso para ver datos de Maquinas Vendidas Propio o todos*/
        $data['permiso_maquinasvendidas_datos']='Ver todas';
        $modulo=8;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Datos Propios Mapa, Calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Ver propio"){
				$data['permiso_maquinasvendidas_datos']='Ver propio';
			  }
            }
          }
        }

        $WHERE = $data['permiso_maquinasvendidas_datos']=='Ver propio'?' WHERE U.id = '.Auth::user()->id:'';
    	$data['datos'] = DB::select('SELECT M.id, P.name, R.referencia, M.valor_venta, R.foto, M.ano_venta, S.lat, S.lng, M.observaciones, E.nombre, E.logo, U.name AS nombres, U.id AS IDVENDEDOR, S.ciudad FROM maquinas_vendidas M INNER JOIN productos P ON P.id = M.producto INNER JOIN producto_referencias R ON R.id = M.referencia INNER JOIN empresa_sedes S ON S.id = M.sede INNER JOIN empresas E ON E.id = M.empresa LEFT JOIN users U ON U.id = M.vendedor_id'.$WHERE);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Maquinas vendidas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        if($data['permiso_maquinasvendidas'] == "Si"){
            return view('maquinasvendidas.list2',['data' => $data]);
        }else{
            return view('oportunidades.nopermiso');
        }

    }

    public function action(request $request){
        /*Permiso pObtener datos propios o ver toda la informacion de maquinas vendidas*/
        $permiso_maquinasvendidas_datos='Ver todas';
        $modulo=8;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Datos Propios Mapa, Calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Ver propio"){
				$permiso_maquinasvendidas_datos='Ver propio';
			  }
            }
          }
        }
        switch($request->action){
            /*Listado de Iconos de los productos*/
            case 0:
                if(!empty($request->filtro)){
                    $query = 'SELECT *, (SELECT IFNULL(SUM(OP.cantidad),0) FROM oportunidad_productos OP INNER JOIN view_oportunidades V ON V.id = OP.oportunidad_id WHERE OP.deleted_at IS NULL AND V.CICLO = 90 AND OP.producto = P.id GROUP BY OP.producto LIMIT 0,1) AS VENDIDASDESPUESSMART, (SELECT COUNT(*) FROM maquinas_vendidas MV WHERE MV.producto = P.id) AS VENDIDASANTESSMART  FROM productos P WHERE P.name LIKE "%'.$request->filtro.'%"';
                }else{
                    $query = 'SELECT *, (SELECT IFNULL(SUM(OP.cantidad),0) FROM oportunidad_productos OP INNER JOIN view_oportunidades V ON V.id = OP.oportunidad_id WHERE OP.deleted_at IS NULL AND V.CICLO = 90 AND OP.producto = P.id GROUP BY OP.producto LIMIT 0,1) AS VENDIDASDESPUESSMART, (SELECT COUNT(*) FROM maquinas_vendidas MV WHERE MV.producto = P.id) AS VENDIDASANTESSMART  FROM productos P';
                }
                $productos = DB::SELECT($query);
                ob_start();
?>
<div class="row">
<?php
                if(count($productos)>0){
                    foreach($productos as $producto){
                        if($permiso_maquinasvendidas_datos == "Ver propio"){
                            $query = 'SELECT IFNULL(SUM(OP.cantidad),0) FROM oportunidad_productos OP INNER JOIN productos P ON OP.producto = P.id WHERE OP.deleted_at IS NULL AND (SELECT HO.value FROM historial_oportunidades HO WHERE HO.oportunidad_id = OP.oportunidad_id AND HO.key LIKE "responsable" ORDER BY HO.id DESC LIMIT 0,1) = '.Auth::user()->id.' AND (SELECT HO.value FROM historial_oportunidades HO WHERE HO.oportunidad_id = OP.oportunidad_id AND HO.key LIKE "ciclo_venta" ORDER BY HO.id DESC LIMIT 0,1) = 90 AND P.name LIKE "'.$producto->name.'"';
                            $query1 = 'SELECT COUNT(MV.id) FROM maquinas_vendidas MV INNER JOIN productos P ON MV.producto = P.id WHERE MV.vendedor_id = '.Auth::user()->id.' AND P.name LIKE "'.$producto->name.'"';
                            $query2 = 'SELECT ('.$query.') AS CANTIDAD, ('.$query1.') AS CANTIDAD1';
                            $cantidad_total_vendedor = DB::SELECT($query2);
                            $numero = $cantidad_total_vendedor[0]->CANTIDAD + $cantidad_total_vendedor[0]->CANTIDAD1;
                            $data[$producto->id] = $query2;
                        }else{
                            $VAS=$producto->VENDIDASANTESSMART==""?0:$producto->VENDIDASANTESSMART;
                            $VDS=$producto->VENDIDASDESPUESSMART==""?0:$producto->VENDIDASDESPUESSMART;
                            $numero = $VAS+$VDS;
                        }
?>

    <div class="col-2 mt-4">
        <img src="/images/iconosmapamaquinasvendidas/<?=$producto->id?>.png" style="width:30px;">
    </div>
    <div class="col-md-4">
        <p class="h6 text-info text-left"><?=$producto->name?><br> Equipos: <?=$numero?></p>
    </div>
<?php
                    }
                }else{
?>
    <div class="col-md-12">
        <p class="h4 text-info text-left">No hay Productos</p>
    </div>
<?php
                }
?>
</div>
<?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function listaJSON(){
        /*Permiso pObtener datos propios o ver toda la informacion de maquinas vendidas*/
        $permiso_maquinasvendidas_datos='Ver todas';
        $modulo=8;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Datos Propios Mapa, Calendario" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Ver propio"){
				$permiso_maquinasvendidas_datos='Ver propio';
			  }
            }
          }
        }
        //Ejemplo Eduard:
        //$_GET["name_category"];
        //$_GET["id"];
        if(isset($_GET["filtro"]["name_category"]) && $_GET["filtro"]["name_category"]!=""){
            $filtro = ' P.producto IN (SELECT P2.id FROM productos P2 WHERE P2.name LIKE "%'.$_GET["filtro"]["name_category"].'%") AND ';
            $filtro2 = ' P.id IN (SELECT P2.id FROM productos P2 WHERE P2.name LIKE "%'.$_GET["filtro"]["name_category"].'%") AND ';
        }else{
            $filtro = '';
            $filtro2 = '';
        }
        if($permiso_maquinasvendidas_datos=='Ver propio'){
            $filtro_permiso = ' AND V.IDRESPONSABLE = '.Auth::user()->id;
            $filtro_permiso2 = ' AND V.vendedor_id = '.Auth::user()->id;
        }else{
            $filtro_permiso = '';
            $filtro_permiso2 = '';
        }
        $query = 'SELECT IFNULL(V.EMPRESALOGO,"https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20foto&w=200&h=200") AS EMPRESALOGO, V.empresa, YEAR(V.FECHACIERRE) AS ANOVENTA, (SELECT P1.name FROM productos P1 WHERE P1.id = P.producto) AS PRODUCTO, (SELECT IFNULL(CONCAT("/images/file/productos/", R1.foto),"https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20foto&w=200&h=200") FROM producto_referencias R1 WHERE R1.id = P.referencia) AS PRODUCTOFOTO, (SELECT P1.id FROM productos P1 WHERE P1.id = P.producto) AS IDPRODUCTO, (SELECT R.referencia FROM producto_referencias R WHERE R.id = P.referencia) AS PRODUCTOREFERENCIA, V.CORTORESPONSABLE, (SELECT FORMAT(S.lat,2) FROM empresa_sedes S WHERE S.importancia_sede = "Principal" AND S.empresa_id = V.empresa_id LIMIT 0,1) AS latitud, (SELECT FORMAT(S.lng,2) FROM empresa_sedes S WHERE S.importancia_sede = "Principal" AND S.empresa_id = V.empresa_id LIMIT 0,1) AS longitud FROM oportunidad_productos P INNER JOIN view_oportunidades V ON P.oportunidad_id = V.id WHERE'.$filtro.' P.deleted_at IS NULL AND V.CICLO = 90'.$filtro_permiso;
        $markers = DB::SELECT($query);
        $query = 'SELECT (SELECT IFNULL(CONCAT("/images/file/empresas/principal/",E.logo),"https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20foto&w=200&h=200") FROM empresas E WHERE E.id = V.empresa) AS EMPRESALOGO, (SELECT E.nombre FROM empresas E WHERE E.id = V.empresa) AS empresa, V.ano_venta AS ANOVENTA, P.name AS PRODUCTO, (SELECT IFNULL(CONCAT("/images/file/productos/",R.foto),"https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20foto&w=200&h=200") FROM producto_referencias R WHERE R.id = V.referencia) AS PRODUCTOFOTO, V.producto AS IDPRODUCTO, (SELECT R.referencia FROM producto_referencias R WHERE R.id = V.referencia) AS PRODUCTOREFERENCIA, IFNULL((SELECT CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1 ), " ", SUBSTRING_INDEX(U.apellidos, " ", 1 )) FROM users U WHERE U.id = V.vendedor_id),"Sin Vendedor") AS CORTORESPONSABLE, (SELECT FORMAT(S.lat,2) FROM empresa_sedes S WHERE S.importancia_sede = "Principal" AND S.empresa_id = V.empresa LIMIT 0,1) AS latitud, (SELECT FORMAT(S.lng,2) FROM empresa_sedes S WHERE S.importancia_sede = "Principal" AND S.empresa_id = V.empresa LIMIT 0,1) AS longitud FROM  maquinas_vendidas V INNER JOIN productos P ON V.producto = P.id WHERE'.$filtro2.' (SELECT FORMAT(S.lat,2) FROM empresa_sedes S WHERE S.importancia_sede = "Principal" AND S.empresa_id = V.empresa LIMIT 0,1) IS NOT NULL AND (SELECT FORMAT(S.lng,2) FROM empresa_sedes S WHERE S.importancia_sede = "Principal" AND S.empresa_id = V.empresa LIMIT 0,1) IS NOT NULL'.$filtro_permiso2;
        $markers2 = DB::SELECT($query);
        $markers = array_merge($markers, $markers2);
        return \Response::json(['markers' => $markers]);
    }

    public function save(Request $request){
    	$dato = new MaquinasVendida;
		$dato->producto = $request->producto;
		$dato->referencia = $request->referencia;
		$dato->empresa = $request->empresa;
		$dato->sede = $request->sede;
		$dato->cliente = $request->cliente;
		$dato->ano_venta = $request->ano_venta;
		$dato->valor_venta = $request->valor_venta;
		$dato->margen = $request->margen;
		$dato->vendedor_id = $request->vendedor_id;
		$dato->observaciones = $request->observaciones;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return redirect('/listmaquinasvendidas');
    }

    public function editsave(Request $request){
    	$dato = MaquinasVendida::find($request->id);
		$dato->producto = $request->producto;
		$dato->referencia = $request->referencia;
		$dato->empresa = $request->empresa;
		$dato->sede = $request->sede;
		$dato->cliente = $request->cliente;
		$dato->ano_venta = $request->ano_venta;
		$dato->valor_venta = $request->valor_venta;
		$dato->margen = $request->margen;
		$dato->vendedor_id = $request->vendedor_id;
		$dato->observaciones = $request->observaciones;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return redirect('/listmaquinasvendidas');
    }
}
