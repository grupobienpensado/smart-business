<?php

namespace App\Http\Controllers;

date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\DB;
use App\Oportunidades;
use App\HistorialOportunidades;
use App\Actividades_detalles;
use App\Plan_trabajo;
use App\Plan_trabajo_descripcion;
use App\Presupuesto;
use App\Presupuesto_anual;
use App\Actividades;
use App\Users_meta;
use App\Metas_oportunidade;
use App\Participacion_probabilidade;
use App\Empresa;
use App\Cliente;
use App\Ciclo;
use App\Gasto;
use App\Tipo_actividades;
use App\Users_megaarchivo;
use App\Principal;
use App\Menuses;
use App\ComparacionAno;
use Auth;

class ComercialController extends Controller
{
    //
    public function plantrabajo($datos){
        $informacion=explode('¬', $datos);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Plan de trabajo" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view("plantrabajo.calendar",["data" => $data, "semana"=>$informacion[0],"id_funcionario"=>$informacion[1]]);
    }
    public function semana($tipo){
      $fecha=date('Y-m-d') ; // fecha.
      #separas la fecha en subcadenas y asignarlas a variables
      #relacionadas en contenido, por ejemplo dia, mes y anio.

      $diaa   = substr($fecha,8,2);
      $mes = substr($fecha,5,2);
      $anio = substr($fecha,0,4);

      $dia = date('w',  mktime(0,0,0,$mes,$diaa,$anio));
      //donde:

      #W (mayúscula) te devuelve el número de semana
      #w (minúscula) te devuelve el número de día dentro de la semana (0=domingo, #6=sabado)

      if((int)$dia==0){
        $semana=date('W',  mktime(0,0,0,$mes,$diaa,$anio))+1;
      }else{
        $semana=date('W',  mktime(0,0,0,$mes,$diaa,$anio));
      }
      if((int)$dia<=1){
        $semana_cerrada=$semana-1;
        $semana_abierta=$semana;
      }else{
        $semana_cerrada=$semana;
        $semana_abierta=$semana+1;
      }
        if($tipo == 0){
            return $semana_cerrada;
        }else{
            return $semana_abierta;
        }
    }

    public function index(Request $request){
      if((Auth::user()->id)==72){
        $us='visitante';
        $pw='visitante2017';
        $usuario=Users_megaarchivo::where('name',$us)->get();
        $datos='';
        if(count($usuario)===0){
            $msj='No existe usuario';
            $ruta='megaarchivo.login';
        }else{
            foreach($usuario as $usua){
                $cont=$usua->contrasena;
            }
            if($cont === $pw){
                foreach($usuario as $usua){
                    $datos=$usua->id;
                }
                $ruta='megaarchivo.tipo';
                $msj='';
            }else{
                $msj='La contraseña es incorrecta';
                $ruta='megaarchivo.login';
            }
        }
        $datos2=Principal::all();
        $datos1=Menuses::all();
        $datos3=Oportunidades::all();
        return view($ruta,['msj' => $msj, 'usuario' => $datos, 'datos' => $datos2, 'menu' => $datos1, 'oportunidades' => $datos3]);
      }else{
        /* $get = file_get_contents("https://www.google.com/finance/converter?a=1&from=EUR&to=COP");
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);
        $euro= preg_replace("/[^0-9\.]/", null, $get[0]);*/
        $euro=3503;

        /*$get = file_get_contents("https://www.google.com/finance/converter?a=1&from=USD&to=COP");
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);
        $dolar= preg_replace("/[^0-9\.]/", null, $get[0]);*/
        $dolar=2909;

        /*Nuevo Permiso informacion de comercial de entrada*/
          $query = 'SELECT * FROM permiso_dashboard_comerciales P WHERE P.id_cargo = (SELECT PC.id FROM permiso_cargos PC WHERE PC.cargo LIKE "'.Auth::user()->cargo.'" LIMIT 0,1)';
          $permisodashboardcomercial = DB::SELECT($query);
          /*Datos Propios*/
          if(isset($permisodashboardcomercial[0]->datos_propios) && $permisodashboardcomercial[0]->datos_propios == 1){
              $usuario=User::findOrFail(Auth::user()->id);
              $id_funcionario = Auth::user()->id;
          }else if(isset($permisodashboardcomercial[0]->id_user_dashboard) && $permisodashboardcomercial[0]->id_user_dashboard != ''){
              $usuario=User::findOrFail($permisodashboardcomercial[0]->id_user_dashboard);
              $id_funcionario = $permisodashboardcomercial[0]->id_user_dashboard;
          }else{
              $usuario=User::findOrFail(53);
              $id_funcionario=53;
          }

          /**Permiso Accion hipervinculo 7 botones**/
          $modulo=1;
            $data['permiso_hipervinculo'] = "No";
            $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Acción hipervínculo 7 botones" AND `id_permisomodulo`="'.$modulo.'"');
            if(isset($acceso[0]->id)){
              $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
              if(isset($cargo[0]->id)){
                $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                if(isset($permiso[0]->permiso)){
                  if($permiso[0]->permiso == "Si"){
                    $data['permiso_hipervinculo'] = "Si";
                  }
                }
              }
            }
        $fecha=date('Y-m-d') ; // fecha.
        #separas la fecha en subcadenas y asignarlas a variables
        #relacionadas en contenido, por ejemplo dia, mes y anio.

        $diaa   = substr($fecha,8,2);
        $mes = substr($fecha,5,2);
        $anio = substr($fecha,0,4);

        $dia = date('w',  mktime(0,0,0,$mes,$diaa,$anio));
        //donde:
        #W (mayúscula) te devuelve el número de semana
        #w (minúscula) te devuelve el número de día dentro de la semana (0=domingo, #6=sabado)

        if((int)$dia==0){
            $semana=date('W',  mktime(0,0,0,$mes,$diaa,$anio))+1;
        }else{
            $semana=date('W',  mktime(0,0,0,$mes,$diaa,$anio));
        }
        if((int)$dia<=1){
            $semana_cerrada=$semana-1;
            $semana_abierta=$semana;
        }else{
            $semana_cerrada=$semana;
            $semana_abierta=$semana+1;
        }
        $buscar = $request->all();
        if (isset($buscar["filtro"])) {
            $usuario=User::findOrFail($buscar["filtro"]);
            $actividades_detalles=Actividades_detalles::all();
            $oportunidades=Oportunidades::where('user_id','=',$buscar["filtro"])->get();
            //$plan_trabajo_descripcion=Plan_trabajo_descripcion::all();
            $plan_trabajo_descripcion = DB::SELECT('SELECT PTD.* FROM plan_trabajo_descripcions PTD WHERE PTD.plan_trabajo IN (SELECT PT.id FROM plan_trabajos PT WHERE PT.user_id = '.$buscar["filtro"].' AND (PT.semana LIKE '.$semana_cerrada.' OR PT.semana LIKE '.$semana_abierta.'))');
            //$planes=Plan_trabajo::where('user_id',$usuario->id)->get();
            //$planes=Plan_trabajo::where('semana','=',$this->semana(0))->orWhere('semana','=',$this->semana(1))->get();
            $planes = DB::SELECT('SELECT * FROM plan_trabajos PT WHERE PT.user_id = '.$buscar["filtro"].' AND (PT.semana LIKE '.$semana_cerrada.' OR PT.semana LIKE '.$semana_abierta.')');
            $presupuestos=Presupuesto::all();
            $actividades=Actividades::where('user_id','=',$buscar["filtro"])->get();
            $metas=Users_meta::all();
            $presupuestos=Presupuesto::all();
            $lista1=[];
            $lista2=[];
            $id_funcionario=$buscar["filtro"];
            $ano_actual = date('Y');
            $user_metas=DB::select('SELECT * FROM users_metas WHERE users = '.$id_funcionario.' AND anio LIKE "'.$ano_actual.'" ORDER BY id');
        }else{
          $actividades_detalles=Actividades_detalles::all();
          $oportunidades=Oportunidades::all();
          //$plan_trabajo_descripcion=Plan_trabajo_descripcion::all();
          $plan_trabajo_descripcion = DB::SELECT('SELECT PTD.* FROM plan_trabajo_descripcions PTD WHERE PTD.plan_trabajo IN (SELECT PT.id FROM plan_trabajos PT WHERE PT.user_id = '.$usuario->id.' AND (PT.semana LIKE '.$semana_cerrada.' OR PT.semana LIKE '.$semana_abierta.'))');
          //$planes=Plan_trabajo::where('user_id',Auth::user()->id)->get();
          $planes = DB::SELECT('SELECT * FROM plan_trabajos PT WHERE PT.user_id = '.$usuario->id.' AND (PT.semana LIKE '.$semana_cerrada.' OR PT.semana LIKE '.$semana_abierta.')');
          $presupuestos=Presupuesto::all();
          $actividades=Actividades::all();
            $metas=Users_meta::all();
            $presupuestos=Presupuesto::all();
          $lista1=[];
          $lista2=[];
          $ano_actual = date('Y');
          $user_metas=DB::select('SELECT * FROM users_metas WHERE users = '.$id_funcionario.' AND anio LIKE "'.$ano_actual.'" ORDER BY id');

        }
        /*Oportunidades*/
            $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE value = '.$id_funcionario.' AND `key` = "responsable" AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
            $total_mis_oportunidades['numero']=0;
            foreach ($mis_oportunidades as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                if(($oportunidad_buscar[0]->value)==$id_funcionario){
                  $total_mis_oportunidades['numero']++;
                }
            }
        /*Fin Oportunidades*/
        /* Valor Oportunidades */
            $total_mis_oportunidades['valor']=0;

            foreach ($mis_oportunidades as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');

                if(($oportunidad_buscar[0]->value)==$id_funcionario){
                    $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    if(count($plazo)>0){
                        $valor_pesos=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = "'.$oportunidad_buscar[0]->oportunidad_id.'" AND `key` = "val_pesos" ORDER BY `id` DESC');
                        $convalorpesos=count($valor_pesos);
                        if($convalorpesos>0){
                          $valor_total=(int)(str_replace(",", "",($valor_pesos[0]->value)));
                        }
                        $valor_total=!isset($valor_total)?0:round($valor_total/1000000);
                        $valor_total=(int)$valor_total;
                        $total_mis_oportunidades['valor']+= $valor_total;
                    }
                }
            }
        /*Fin Valor Oportunidades */
        /*Oportunidades ESSI */
            $essi_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
            $o_essi['numero']=count($essi_oportunidades);
            $o_essi['valor']=0;
            $valor_total=0;
            foreach ($essi_oportunidades as $e_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$e_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                if(count($plazo)>0){
                  $valor_pesos=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = "'.$oportunidad_buscar[0]->oportunidad_id.'" AND `key` = "val_pesos" ORDER BY `id` DESC');
                  $convalorpesos=count($valor_pesos);
                  if($convalorpesos>0){
                    $valor_total+=(int)(str_replace(",", "",($valor_pesos[0]->value)));
                  }else{
                    $valor_total+=0;
                  }
                }
            }
            $valor_total=round($valor_total/1000000);
            $valor_total=(int)$valor_total;
            $o_essi['valor']= $valor_total;

        /*Fin Oportunidades ESSI */

        /*Oportunidades Cerradas */
            $mis_oportunidades_cerradas=DB::select('SELECT DISTINCT `oportunidad_id` FROM `historial_oportunidades` WHERE `oportunidad_id` IN ( SELECT `oportunidad_id` FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90")');
            $h=0;
            $valor_usuario=0;
            foreach ($mis_oportunidades_cerradas as $m_o_c) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o_c->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                $fecha_cierre = DB::select('SELECT value FROM historial_oportunidades WHERE oportunidad_id = '.$m_o_c->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC LIMIT 0,1');
                if(($oportunidad_buscar[0]->value)==$id_funcionario && date('Y',strtotime($fecha_cierre[0]->value)) == date('Y')){
                  $h++;
                  $valor_pesos=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o_c->oportunidad_id.' AND `key` = "val_pesos" order by `id` DESC');
                  $convalorpesos=count($valor_pesos);
                  if($convalorpesos>0){
                    $valor_usuario+=(int)(str_replace(",", "",($valor_pesos[0]->value)));
                  }
                }
            }
            $oportunidad_cerrada['numero']=$h;
            $oportunidad_cerrada['valor']=round($valor_usuario/1000000);
        /*Fin Oportunidades cerradas */

        /*promedio cierre*/
            $valor_usuario=0; $h=0;
            $promedioindividual=0;
            $totaloportunidadindividual=0;
            $tiempo=0;
            $data['fecha_cierre'] = '';
            foreach ($mis_oportunidades_cerradas as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                $fecha_cierre = DB::select('SELECT value FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC LIMIT 0,1');
                if(($oportunidad_buscar[0]->value)==$usuario->id && date('Y',strtotime($fecha_cierre[0]->value)) == date('Y')){
                  $historial1=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" ORDER BY `id` DESC');
                  $fecha_fin=0;
                  $fecha_inicio=0;
                  $contador=count($historial1);
                  if($contador>0){
                    if($historial1[0]->value>=90){
                      $totaloportunidadindividual++;
                      $historial2=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_ff" ORDER BY `id` DESC');
                      if(count($historial2)>0){
                        $fecha_inicio=strtotime($historial2[0]->value);
                      }
                      $historial3=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC');
                      if(count($historial3)>0){
                        $fecha_fin=strtotime($historial3[0]->value);
                      }
                    }
                  }
                  $tiempo+=$fecha_fin - $fecha_inicio;
                }
            }
            if($totaloportunidadindividual>0){
              $dias=$tiempo / 86400;
              $promedioindividual=($dias/$totaloportunidadindividual)/30;
              $promedioindividual=number_format(round($promedioindividual),0,".","");
            }else{
              $promedioindividual=0;
            }
        /*Fin Promedio Cierre */
        /*Negocios Perdidos*/
            $h=0;
            $i=0;
            $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');
            foreach ($mis_oportunidades as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                if(($oportunidad_buscar[0]->value)==$id_funcionario){
                  $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" order by id');
                  $contador=count($historial);
                  if($contador>0){
                    if($historial[$contador-1]->value==0 && date('Y',strtotime($historial[$contador-1]->updated_at))==date('Y')){
                      $h++;
                    }
                  }
                }
            }
            $vector[$i]=$h;
            $p=0;
            $total_oportunidades=0;
            $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` IN (SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
            foreach ($mis_oportunidades as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                $fecha_cierre=DB::select('SELECT value FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC LIMIT 0,1');
                if(($oportunidad_buscar[0]->value)==$id_funcionario && date('Y',strtotime($fecha_cierre[0]->value)) == date('Y')){
                  $total_oportunidades++;
                }
            }
           $total_oportunidades+=$vector[$p];
            if($total_oportunidades>0){
                $total_porc_perdida=($vector[$p]*100)/$total_oportunidades;
                $total_porc_perdida=round($total_porc_perdida, 1);
            }else{
                $total_porc_perdida=0;
            }
        /*Fin Negocios Perdidos */
        /*Costo de venta */
            $historialoportunidades=HistorialOportunidades::where('key',"=", "ciclo_venta")->where('value',"=", "90")->get();
            $gasto_x_usuario_ventas=0;
            foreach ($historialoportunidades as $ho) {
                if($ho->oportunidad_id != ""){
                    $query='SELECT H.value FROM historial_oportunidades AS H WHERE H.key = "responsable" AND H.oportunidad_id = '.$ho->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                    $responsable = DB::select($query);
                    $query='SELECT H.value FROM historial_oportunidades AS H WHERE H.key = "fecha_oc" AND H.oportunidad_id = '.$ho->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                    $fecha_cierre = DB::select($query);
                    if($responsable[0]->value == $id_funcionario && date("Y", strtotime($fecha_cierre[0]->value)) == date("Y")){
                        $query = 'SELECT CONVERT(REPLACE(REPLACE(H.value, ".", ""), ",", ""),UNSIGNED INTEGER) AS value FROM historial_oportunidades AS H WHERE H.key = "val_pesos" AND H.oportunidad_id = '.$ho->oportunidad_id.' ORDER BY H.id DESC LIMIT 0,1';
                        $valor = DB::select($query);
                        $gasto_x_usuario_ventas+=$valor[0]->value;
                    }
                }
            }

            /*fin por usuario*/

            $gasto_x_usuario_gasto=0;
            $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) + SUM(CONVERT(REPLACE(REPLACE(H.valor, ".", ""), ",", ""),UNSIGNED INTEGER)) AS total FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE YEAR(A.fecha_actividad) = "'.date("Y").'" AND A.user_id = '.$id_funcionario;
            $actividades_ano_actual = DB::select($query);
          $gasto_x_usuario_gasto = $actividades_ano_actual[0]->total;
            if($gasto_x_usuario_ventas==0){
                $gasto_x_usuario_porcentaje=100;
            }else{
                $gasto_x_usuario_porcentaje=($gasto_x_usuario_gasto/$gasto_x_usuario_ventas)*100;
            }
            $gasto_x_usuario_porcentaje = number_format($gasto_x_usuario_porcentaje, 1, '.', ' ');
        /*Fin Costo Venta */
        /*Presupuesto Mensual*/

            $ano_actual=date('Y');

            $dia_actual=date('d');
            if($dia_actual<=5){
                $mes_actual=date('m');
            }else{
                $mes_actual=date('m')+1;
                if($mes_actual==13){
                    $mes_actual=1;
                }
            }
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $total_presupuesto_m=0;
            foreach($presupuestos as $presupuesto){
                if(((int)$presupuesto->user_id) == ((int)$id_funcionario)){
                  if(((int)$presupuesto->mes)==((int)$mes_actual) && ((int)$presupuesto->anio)==((int)$ano_actual)){
                     $total_presupuesto_m+=((int)(str_replace(",", "", $presupuesto->alimentacion)))+((int)(str_replace(",", "", $presupuesto->transporte_interno)))+((int)(str_replace(",", "", $presupuesto->transporte_intermunicipal)))+((int)(str_replace(",", "", $presupuesto->tiquete_aereo)))+((int)(str_replace(",", "", $presupuesto->papeleria)))+((int)(str_replace(",", "", $presupuesto->invitacion_cliente)))+((int)(str_replace(",", "", $presupuesto->alquiler_vehiculo)))+((int)(str_replace(",", "", $presupuesto->gasolina_pasaje)))+((int)(str_replace(",", "", $presupuesto->hotel)))+((int)(str_replace(",", "", $presupuesto->otros)));
                  }
                }
            }
            $p_m['valor']=$total_presupuesto_m/1000000;
            $p_m['mes']=$meses[(int)$mes_actual].' '.$ano_actual;
        /*Fin Ejecucion Presupuesto */
        /*Bitacora*/
            $hace3dias=strtotime("-3 Day",strtotime(date("Y-m-d")));
            $hace3dias=date ( 'Y-m-d' , $hace3dias );
            $antiayer=strtotime("-2 Day",strtotime(date("Y-m-d")));
            $antiayer=date ( 'Y-m-d' , $antiayer );
            $ayer=strtotime("-1 Day",strtotime(date("Y-m-d")));
            $ayer=date ( 'Y-m-d' , $ayer );
            $hoy=date('Y-m-d');
            $actividades_registradass=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` = "'.$hoy.'" AND `user_id`="'.$id_funcionario.'"');
            $actividades_registradas['hoy']=count($actividades_registradass);
            $actividades_registradass=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` = "'.$ayer.'" AND `user_id`="'.$id_funcionario.'"');
            $actividades_registradas['ayer']=count($actividades_registradass);
            $actividades_registradass=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` = "'.$antiayer.'" AND `user_id`="'.$id_funcionario.'"');
            $actividades_registradas['antiayer']=count($actividades_registradass);
            $actividades_registradass=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` = "'.$hace3dias.'" AND `user_id`="'.$id_funcionario.'"');
            $actividades_registradas['hace3dias']=count($actividades_registradass);
        /*Fin Bitacora */
        /*Meta Ventas */
            $valor_usuario=0;
            $ventas_x_meses[0]=0;$ventas_x_meses[1]=0;$ventas_x_meses[2]=0;$ventas_x_meses[3]=0;$ventas_x_meses[4]=0;$ventas_x_meses[5]=0;$ventas_x_meses[6]=0;$ventas_x_meses[7]=0;$ventas_x_meses[8]=0;$ventas_x_meses[9]=0;$ventas_x_meses[10]=0;$ventas_x_meses[11]=0;
            $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
            foreach ($mis_oportunidades as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                if($usuario->cargo=='Gerente Comercial' || $usuario->cargo=='Director General'){
                    $valoresoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');

                    if(isset($valoresoportunidades[0]->value)){
                        $valor_usuario=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valoresoportunidades[0]->value)))));
                    }

                    $fecha_cierre=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    if(count($fecha_cierre)>0){
                        $dia=date('Y-m-d',strtotime($fecha_cierre[0]->value));

                        if($dia>(date('Y-01-01')) && $dia<(date('Y-02-01'))){

                            $ventas_x_meses[0]=$ventas_x_meses[0]+$valor_usuario;

                        }elseif($dia>(date('Y-02-01')) && $dia<(date('Y-03-01'))){
                            $ventas_x_meses[1]=$ventas_x_meses[1]+$valor_usuario;
                        }elseif($dia>(date('Y-03-01')) && $dia<(date('Y-04-01'))){

                            $ventas_x_meses[2]=$ventas_x_meses[2]+$valor_usuario;

                        }elseif($dia>(date('Y-04-01')) && $dia<(date('Y-05-01'))){

                            $ventas_x_meses[3]=$ventas_x_meses[3]+$valor_usuario;

                        }elseif($dia>(date('Y-05-01')) && $dia<(date('Y-06-01'))){

                            $ventas_x_meses[4]=$ventas_x_meses[4]+$valor_usuario;

                        }elseif($dia>(date('Y-06-01')) && $dia<(date('Y-07-01'))){
                            $ventas_x_meses[5]=$ventas_x_meses[5]+$valor_usuario;
                        }elseif($dia>(date('Y-07-01')) && $dia<(date('Y-08-01'))){
                            $ventas_x_meses[6]=$ventas_x_meses[6]+$valor_usuario;
                        }elseif($dia>(date('Y-08-01')) && $dia<(date('Y-09-01'))){
                            $ventas_x_meses[7]=$ventas_x_meses[7]+$valor_usuario;
                        }elseif($dia>(date('Y-09-01')) && $dia<(date('Y-10-01'))){
                            $ventas_x_meses[8]=$ventas_x_meses[8]+$valor_usuario;
                        }elseif($dia>(date('Y-10-01')) && $dia<(date('Y-11-01'))){
                            $ventas_x_meses[9]=$ventas_x_meses[9]+$valor_usuario;
                        }elseif($dia>(date('Y-11-01')) && $dia<(date('Y-12-01'))){
                            $ventas_x_meses[10]=$ventas_x_meses[10]+$valor_usuario;
                        }elseif($dia>(date('Y-12-01')) && $dia<=(date('Y-12-31'))){
                            $ventas_x_meses[11]=$ventas_x_meses[11]+$valor_usuario;
                        }
                    }
                }else{
                    if(($oportunidad_buscar[0]->value)==$id_funcionario){

                        $valoresoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');

                        if(isset($valoresoportunidades[0]->value)){
                            $valor_usuario=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valoresoportunidades[0]->value)))));
                        }
                        $fecha_cierre=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                        if(count($fecha_cierre)>0){
                            $dia=date('Y-m-d',strtotime($fecha_cierre[0]->value));

                            if($dia>(date('Y-01-01')) && $dia<(date('Y-02-01'))){

                                $ventas_x_meses[0]=$ventas_x_meses[0]+$valor_usuario;

                            }elseif($dia>(date('Y-02-01')) && $dia<(date('Y-03-01'))){
                                $ventas_x_meses[1]=$ventas_x_meses[1]+$valor_usuario;
                            }elseif($dia>(date('Y-03-01')) && $dia<(date('Y-04-01'))){

                                $ventas_x_meses[2]=$ventas_x_meses[2]+$valor_usuario;

                            }elseif($dia>(date('Y-04-01')) && $dia<(date('Y-05-01'))){

                                $ventas_x_meses[3]=$ventas_x_meses[3]+$valor_usuario;

                            }elseif($dia>(date('Y-05-01')) && $dia<(date('Y-06-01'))){

                                $ventas_x_meses[4]=$ventas_x_meses[4]+$valor_usuario;

                            }elseif($dia>(date('Y-06-01')) && $dia<(date('Y-07-01'))){
                                $ventas_x_meses[5]=$ventas_x_meses[5]+$valor_usuario;
                            }elseif($dia>(date('Y-07-01')) && $dia<(date('Y-08-01'))){
                                $ventas_x_meses[6]=$ventas_x_meses[6]+$valor_usuario;
                            }elseif($dia>(date('Y-08-01')) && $dia<(date('Y-09-01'))){
                                $ventas_x_meses[7]=$ventas_x_meses[7]+$valor_usuario;
                            }elseif($dia>(date('Y-09-01')) && $dia<(date('Y-10-01'))){
                                $ventas_x_meses[8]=$ventas_x_meses[8]+$valor_usuario;
                            }elseif($dia>(date('Y-10-01')) && $dia<(date('Y-11-01'))){
                                $ventas_x_meses[9]=$ventas_x_meses[9]+$valor_usuario;
                            }elseif($dia>(date('Y-11-01')) && $dia<(date('Y-12-01'))){
                                $ventas_x_meses[10]=$ventas_x_meses[10]+$valor_usuario;
                            }elseif($dia>(date('Y-12-01')) && $dia<=(date('Y-12-31'))){
                                $ventas_x_meses[11]=$ventas_x_meses[11]+$valor_usuario;
                            }
                        }
                    }
                }
            }

            for($i=11;$i>=0;$i--){
                $contador=0;
                for($y=0;$y<=$i;$y++){
                    $contador+=$ventas_x_meses[$y];
                }
                $ventas_x_meses[$i]=$contador;
                $ventas_x_meses[$i]=round($ventas_x_meses[$i]/1000000);

            }
      $data['user_metas'] = $user_metas;
      $usuario_meta[0]=0; $usuario_meta[1]=0; $usuario_meta[2]=0; $usuario_meta[3]=0; $usuario_meta[4]=0; $usuario_meta[5]=0; $usuario_meta[6]=0; $usuario_meta[7]=0; $usuario_meta[8]=0; $usuario_meta[9]=0; $usuario_meta[10]=0; $usuario_meta[11]=0;
            foreach ($user_metas as $u_m) {
                if(((int)($u_m->mes))==1){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[0]+=(int)$valor;
                }elseif(((int)($u_m->mes))==2){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[1]+=(int)$valor;
                }elseif(((int)($u_m->mes))==3){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[2]+=(int)$valor;
                }elseif(((int)($u_m->mes))==4){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[3]+=(int)$valor;
                }elseif(((int)($u_m->mes))==5){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[4]+=(int)$valor;
                }elseif(((int)($u_m->mes))==6){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[5]+=(int)$valor;
                }elseif(((int)($u_m->mes))==7){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[6]+=(int)$valor;
                }elseif(((int)($u_m->mes))==8){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[7]+=(int)$valor;
                }elseif(((int)($u_m->mes))==9){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[8]+=(int)$valor;
                }elseif(((int)($u_m->mes))==10){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[9]+=(int)$valor;
                }elseif(((int)($u_m->mes))==11){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[10]+=(int)$valor;
                }elseif(((int)($u_m->mes))==12){
                    $valor=str_replace(",", "", ($u_m->monto_concretado));
                    $usuario_meta[11]+=(int)$valor;
                }

            }
            for($i=11;$i>=0;$i--){
                $contador=0;
                for($y=0;$y<=$i;$y++){
                    if(isset($usuario_meta[$y])){
                        $contador+=$usuario_meta[$y];
                    }else{
                        $contador=0;
                    }

                }

                $usuario_meta[$i]=$contador;
                $usuario_meta[$i]=round($usuario_meta[$i]/1000000);
            }

        /*Fin Meta Ventas */

        /*Gastos Oportunidades */

        /*Cumplimiento Presupuestal*/

        $actividades_c_p_mes_actual=DB::select('SELECT * FROM `actividades` WHERE `user_id` = "'.$id_funcionario.'" AND YEAR(`fecha_actividad`)="'.date('Y').'" AND MONTH(`fecha_actividad`)="'.date('m').'" order by id DESC');

        $presupuesto_c_p_mes_actual=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$id_funcionario.'" AND `anio`="'.date('Y').'" AND `mes`="'.(int)(date('m')).'" order by id DESC');
        $sumatotal_a=0;
        $sumatotal_p=0;
        foreach($actividades_c_p_mes_actual as $a){
            $sumatotal_a+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->alimentacion)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->transportes_internos)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->transportes_intermunicipales)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->tiquete_aereo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->papeleria)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->invitacion_cliente)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->alquiler_vehiculo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->gasolina_pasajes)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->hotel)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->otros)))));
        }

        foreach($presupuesto_c_p_mes_actual as $p){
            $sumatotal_p+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alimentacion)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transporte_interno)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transporte_intermunicipal)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->tiquete_aereo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->papeleria)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->invitacion_cliente)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alquiler_vehiculo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->gasolina_pasaje)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->hotel)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->otros)))));
        }
        if($sumatotal_p>0){
            $presupuestocomercial['mes_actual']=round($sumatotal_a/$sumatotal_p*100);
        }else{
            $presupuestocomercial['mes_actual']=0;
        }

        $fechamesanterior=strtotime ( '-1 month' , strtotime ( date('Y-m-d') ) ) ;
        $fechamesanterior=date ( 'Y-m-j' , $fechamesanterior );

        $actividades_c_p_mes_anterior=DB::select('SELECT * FROM `actividades` WHERE `user_id` = "'.$id_funcionario.'" AND YEAR(`fecha_actividad`)="'.date("Y", strtotime($fechamesanterior)).'" AND MONTH(`fecha_actividad`)="'.date("m", strtotime($fechamesanterior)).'" order by id DESC');

        $presupuesto_c_p_mes_anterior=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$id_funcionario.'" AND `anio`="'.date("Y", strtotime($fechamesanterior)).'" AND `mes`="'.(int)(date("m", strtotime($fechamesanterior))).'" order by id DESC');
        $sumatotal_a1=0;
        $sumatotal_p1=0;
        foreach($actividades_c_p_mes_anterior as $a){

            $sumatotal_a1+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->alimentacion)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->transportes_internos)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->transportes_intermunicipales)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->tiquete_aereo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->papeleria)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->invitacion_cliente)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->alquiler_vehiculo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->gasolina_pasajes)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->hotel)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->otros)))));

        }

        foreach($presupuesto_c_p_mes_anterior as $p){
            $sumatotal_p1+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alimentacion)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transporte_interno)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transporte_intermunicipal)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->tiquete_aereo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->papeleria)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->invitacion_cliente)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alquiler_vehiculo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->gasolina_pasaje)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->hotel)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->otros)))));

        }
        if($sumatotal_p1>0){
            $presupuestocomercial['mes_anterior']=round($sumatotal_a1/$sumatotal_p1*100);
        }else{
            $presupuestocomercial['mes_anterior']=0;
        }

    $fechamestrasanterior=strtotime ( '-2 month' , strtotime ( date('Y-m-d') ) ) ;
        $fechamestrasanterior=date ( 'Y-m-j' , $fechamestrasanterior );

        $actividades_c_p_mes_anterior=DB::select('SELECT * FROM `actividades` WHERE `user_id` = "'.$id_funcionario.'" AND YEAR(`fecha_actividad`)="'.date("Y", strtotime($fechamestrasanterior)).'" AND MONTH(`fecha_actividad`)="'.date("m", strtotime($fechamestrasanterior)).'" order by id DESC');

        $presupuesto_c_p_mes_anterior=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` = "'.$id_funcionario.'" AND `anio`="'.date("Y", strtotime($fechamestrasanterior)).'" AND `mes`="'.(int)(date("m", strtotime($fechamestrasanterior))).'" order by id DESC');
        $sumatotal_a1=0;
        $sumatotal_p1=0;
        foreach($actividades_c_p_mes_anterior as $a){

            $sumatotal_a1+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->alimentacion)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->transportes_internos)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->transportes_intermunicipales)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->tiquete_aereo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->papeleria)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->invitacion_cliente)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->alquiler_vehiculo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->gasolina_pasajes)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->hotel)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $a->otros)))));

        }

        foreach($presupuesto_c_p_mes_anterior as $p){
            $sumatotal_p1+=((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alimentacion)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transporte_interno)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->transporte_intermunicipal)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->tiquete_aereo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->papeleria)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->invitacion_cliente)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->alquiler_vehiculo)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->gasolina_pasaje)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->hotel)))))+((int)(str_replace('$ ', '', str_replace(',', '', str_replace('.', '', $p->otros)))));

        }
        if($sumatotal_p1>0){
            $presupuestocomercial['mes_trasanterior']=round($sumatotal_a1/$sumatotal_p1*100);
        }else{
            $presupuestocomercial['mes_trasanterior']=0;
        }

        /*Fin Cumplimiento Presupuestal */


        /*Fin Oportunidades */

        /*Cumplimiento Presupuesto*/
          /*Mes Anterior*/
          $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(H.valor, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE A.user_id = '.$id_funcionario.' AND MONTH(A.fecha_actividad) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND YEAR(A.fecha_actividad) = YEAR(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND CONVERT(MONTH(A.fecha_actividad),UNSIGNED INTEGER) NOT IN (SELECT DISTINCT(mes) AS mes FROM ajuste_gastos WHERE anio LIKE YEAR(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND mes LIKE MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)))';
          $gastos_cumplimiento = DB::select($query);

          $query = 'SELECT SUM(CONVERT(valor_alimentacion,UNSIGNED INTEGER) + CONVERT(valor_transporte_interno,UNSIGNED INTEGER) + CONVERT(valor_transporte_intermunicipal,UNSIGNED INTEGER) + CONVERT(valor_tiquete_aereo,UNSIGNED INTEGER) + CONVERT(valor_papeleria,UNSIGNED INTEGER) + CONVERT(valor_invitacion_cliente,UNSIGNED INTEGER) + CONVERT(valor_alquiler_vehiculo,UNSIGNED INTEGER) + CONVERT(valor_gasolina_pasaje,UNSIGNED INTEGER) + CONVERT(valor_hotel,UNSIGNED INTEGER) + CONVERT(valor_otros,UNSIGNED INTEGER) + CONVERT(valor_salariopropio,UNSIGNED INTEGER) + CONVERT(valor_salariotercero,UNSIGNED INTEGER)) AS total  FROM ajuste_gastos WHERE user_id = '.$id_funcionario.' AND anio LIKE YEAR(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND mes = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND tipo LIKE "valorreal"';
          $ajuste_cumplimiento = DB::select($query);

          $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_interno, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_intermunicipal, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasaje, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(salario_propio, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(salario_tercero, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM presupuestos WHERE user_id = '.$id_funcionario.' AND mes = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND anio LIKE YEAR(DATE_ADD(CURDATE(),INTERVAL -1 MONTH))';
          $presupuesto_cumplimiento = DB::select($query);
          $data['cumplimiento_presupuesto_mes_anterior'] = 0;
          if(!empty($presupuesto_cumplimiento[0]->total)){
            $cumplimiento_presupuesto_mes_anterior = (($gastos_cumplimiento[0]->total + $ajuste_cumplimiento[0]->total)/$presupuesto_cumplimiento[0]->total)*100;
            $data['cumplimiento_presupuesto_mes_anterior'] = number_format($cumplimiento_presupuesto_mes_anterior, 1, '.', ' ');
          }

          /*Mes Actual*/
          $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(H.valor, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM actividades A INNER JOIN horas_hombres H ON A.id = H.actividad_id WHERE A.user_id = '.$id_funcionario.' AND MONTH(A.fecha_actividad) = MONTH(CURDATE()) AND YEAR(A.fecha_actividad) = YEAR(CURDATE())';
          $gastos_cumplimiento = DB::select($query);
          $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_interno, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_intermunicipal, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasaje, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(salario_propio, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(salario_tercero, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM presupuestos WHERE user_id = '.$id_funcionario.' AND mes = MONTH(CURDATE()) AND anio LIKE YEAR(CURDATE())';
          $presupuesto_cumplimiento = DB::select($query);
          $data['cumplimiento_presupuesto_mes_actual'] = 0;
          if(!empty($presupuesto_cumplimiento[0]->total)){
            $cumplimiento_presupuesto_mes_actual = ($gastos_cumplimiento[0]->total/$presupuesto_cumplimiento[0]->total)*100;
            $data['cumplimiento_presupuesto_mes_actual'] = number_format($cumplimiento_presupuesto_mes_actual, 1, '.', ' ');
          }
        /*Fin Cumplimiento Presupuesto*/

        /*Gastos de Oportunidades*/
        $query = 'SELECT CONCAT("OP",O.id) AS Op, O.empresa AS Empresa, (SELECT H.value FROM historial_oportunidades AS H WHERE H.key LIKE "ciclo_venta" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) AS Ciclo, (SELECT H.value FROM historial_oportunidades AS H WHERE H.key LIKE "fecha_ff" AND H.oportunidad_id = O.id ORDER BY H.id DESC LIMIT 0,1) AS Fecha_Creacion, COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.total_general, ".", ""), ",", ""),UNSIGNED INTEGER)),0) AS Total FROM actividades A INNER JOIN oportunidades O ON A.oportunidad_id = O.id WHERE A.user_id = '.$id_funcionario.' AND YEAR(A.fecha_actividad) = YEAR(CURDATE()) AND A.oportunidad_id IS NOT NULL group by A.oportunidad_id ORDER BY Total DESC';
        $gastos_oportunidades = DB::select($query);
        $query = 'SELECT COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.total_general, ".", ""), ",", ""),UNSIGNED INTEGER)),0) AS Total FROM actividades A WHERE A.user_id = '.$id_funcionario.' AND YEAR(A.fecha_actividad) = YEAR(CURDATE()) AND A.oportunidad_id IS NOT NULL';
        $data['sumatoria_oportrunidad'] = DB::select($query);
        $query = 'SELECT COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.total_general, ".", ""), ",", ""),UNSIGNED INTEGER)),0) AS Total FROM actividades A WHERE A.user_id = '.$id_funcionario.' AND YEAR(A.fecha_actividad) = YEAR(CURDATE()) AND A.tipo_actividad LIKE "Empresa" AND A.empresa_id IS NOT NULL';
        $data['sumatoria_empresa'] = DB::select($query);
        $query = 'SELECT COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.total_general, ".", ""), ",", ""),UNSIGNED INTEGER)),0) AS Total FROM actividades A WHERE A.user_id = '.$id_funcionario.' AND YEAR(A.fecha_actividad) = YEAR(CURDATE()) AND A.tipo_actividad LIKE "Gestion comercial"';
        $data['sumatoria_gestion_comercial'] = DB::select($query);
        $query = 'SELECT COALESCE(SUM(CONVERT(REPLACE(REPLACE(A.total_general, ".", ""), ",", ""),UNSIGNED INTEGER)),0) AS Total FROM actividades A WHERE A.user_id = 48 AND YEAR(A.fecha_actividad) = YEAR(CURDATE()) AND A.tipo_actividad LIKE "Gestion interna ESSI"';
        $data['sumatoria_gestion_interna'] = DB::select($query);
        ob_start();
            ?>
<div class="col-sm-12 col-md-3">
    <div class="row border-t border-r">
        <div class="col-md-12">
            <?php if(isset($gastos_oportunidades[0])){ ?>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-primary"><?php if(isset($gastos_oportunidades[0]->Op)){ echo $gastos_oportunidades[0]->Op; } ?> <br><a class="text-dark h4 cursor" title="<?php if(isset($gastos_oportunidades[0]->Empresa)){ echo $gastos_oportunidades[0]->Empresa; } ?>"><?php if(isset($gastos_oportunidades[0]->Empresa)){ echo $this->recorte($gastos_oportunidades[0]->Empresa); } ?></a></h5>
                    <small><?php if(isset($gastos_oportunidades[0]->Fecha_Creacion)){ echo $this->fecha($gastos_oportunidades[0]->Fecha_Creacion); } ?></small>
                </div>
                <div class="col-md-12">
                    <span class="badge badge-pill badge-primary <?php if(isset($gastos_oportunidades[0]->Ciclo)){ echo $this->clase($gastos_oportunidades[0]->Ciclo); } ?> mt-2">Ciclo <?php if(isset($gastos_oportunidades[0]->Ciclo)){ echo $gastos_oportunidades[0]->Ciclo; } ?>%</span>
                    <h5 class="mt-3 dark-blue-1">$<?php if(isset($gastos_oportunidades[0]->Total)){ echo number_format($gastos_oportunidades[0]->Total, 0, ',', '.'); } ?></h5>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-3">
    <div class="row border-t border-r">
        <div class="col-md-12">
            <?php if(isset($gastos_oportunidades[1])){ ?>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-primary"><?php if(isset($gastos_oportunidades[1]->Op)){ echo $gastos_oportunidades[1]->Op; } ?> <br><a class="text-dark h4 cursor" title="<?php if(isset($gastos_oportunidades[1]->Empresa)){ echo $gastos_oportunidades[1]->Empresa; } ?>"><?php if(isset($gastos_oportunidades[1]->Empresa)){ echo $this->recorte($gastos_oportunidades[1]->Empresa); } ?></a></h5>
                    <small><?php if(isset($gastos_oportunidades[1]->Fecha_Creacion)){ echo $this->fecha($gastos_oportunidades[1]->Fecha_Creacion); } ?></small>
                </div>
                <div class="col-md-12">
                    <span class="badge badge-pill badge-primary <?php if(isset($gastos_oportunidades[1]->Ciclo)){ echo $this->clase($gastos_oportunidades[1]->Ciclo); } ?> mt-2">Ciclo <?php if(isset($gastos_oportunidades[1]->Ciclo)){ echo $gastos_oportunidades[1]->Ciclo; } ?>%</span>
                    <h5 class="mt-3 dark-blue-1">$<?php if(isset($gastos_oportunidades[1]->Total)){ echo number_format($gastos_oportunidades[1]->Total, 0, ',', '.'); } ?></h5>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-3">
    <div class="row border-t border-r">
        <div class="col-md-12">
            <?php if(isset($gastos_oportunidades[2])){ ?>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-primary"><?php if(isset($gastos_oportunidades[2]->Op)){ echo $gastos_oportunidades[2]->Op; } ?> <br><a class="text-dark h4 cursor" title="<?php if(isset($gastos_oportunidades[2]->Empresa)){ echo $gastos_oportunidades[2]->Empresa; } ?>"><?php if(isset($gastos_oportunidades[2]->Empresa)){ echo $this->recorte($gastos_oportunidades[2]->Empresa); } ?></a></h5>
                    <small><?php if(isset($gastos_oportunidades[2]->Fecha_Creacion)){ echo $this->fecha($gastos_oportunidades[2]->Fecha_Creacion); } ?></small>
                </div>
                <div class="col-md-12">
                    <span class="badge badge-pill badge-primary <?php if(isset($gastos_oportunidades[2]->Ciclo)){ echo $this->clase($gastos_oportunidades[2]->Ciclo); } ?> mt-2">Ciclo <?php if(isset($gastos_oportunidades[2]->Ciclo)){ echo $gastos_oportunidades[2]->Ciclo; } ?>%</span>
                    <h5 class="mt-3 dark-blue-1">$<?php if(isset($gastos_oportunidades[2]->Total)){ echo number_format($gastos_oportunidades[2]->Total, 0, ',', '.'); } ?></h5>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-3">
    <div class="row border-t border-r">
        <div class="col-md-12">
            <?php if(isset($gastos_oportunidades[3])){ ?>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-primary"><?php if(isset($gastos_oportunidades[3]->Op)){ echo $gastos_oportunidades[3]->Op; } ?> <br><a class="text-dark h4 cursor" title="<?php if(isset($gastos_oportunidades[3]->Empresa)){ echo $gastos_oportunidades[3]->Empresa; } ?>"><?php if(isset($gastos_oportunidades[3]->Empresa)){ echo $this->recorte($gastos_oportunidades[3]->Empresa); } ?></a></h5>
                    <small><?php if(isset($gastos_oportunidades[3]->Fecha_Creacion)){ echo $this->fecha($gastos_oportunidades[3]->Fecha_Creacion); } ?></small>
                </div>
                <div class="col-md-12">
                    <span class="badge badge-pill badge-primary <?php if(isset($gastos_oportunidades[3]->Ciclo)){ echo $this->clase($gastos_oportunidades[3]->Ciclo); } ?> mt-2">Ciclo <?php if(isset($gastos_oportunidades[3]->Ciclo)){ echo $gastos_oportunidades[3]->Ciclo; } ?>%</span>
                    <h5 class="mt-3 dark-blue-1">$<?php if(isset($gastos_oportunidades[3]->Total)){ echo number_format($gastos_oportunidades[3]->Total, 0, ',', '.'); } ?></h5>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
         <?php
        $data['Gastos_oportunidades'] = ob_get_contents();
        ob_end_clean();
        /*Fin Gastos de Oportunidades*/

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Dashboard" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
      return view("dashboard.comercial",["data" => $data, "usuario"=>$usuario,"planes"=>$planes,"plan_descripciones"=>$plan_trabajo_descripcion,"presupuestos"=>$presupuestos,'oportunidades'=>$oportunidades,"actividades"=>$actividades,"metas"=>$metas,"total_mis_oportunidades"=>$total_mis_oportunidades,'o_essi'=>$o_essi,'oportunidad_cerrada'=>$oportunidad_cerrada,'promedioindividual'=>$promedioindividual,'total_porc_perdida'=>$total_porc_perdida,'gasto_x_usuario_porcentaje'=>$gasto_x_usuario_porcentaje, 'p_m'=>$p_m,'actividades_registradas'=>$actividades_registradas,'ventas_x_meses'=>$ventas_x_meses, 'usuario_meta'=>$usuario_meta, 'id_funcionario'=>$id_funcionario, 'presupuestocomercial'=>$presupuestocomercial]);
      }

    }

    public function fecha($fecha){
            $f = explode('-',$fecha);
            $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $fecha_final = $f[2].' de '.$meses[intval($f[1])].' '.$f[0];
            return $fecha_final;
        }
    public function clase($ciclo){
        $clase = '';
        switch($ciclo){
            case 0:
                $clase = "bg-warning";
                break;
            case 90:
                $clase = "bg-success";
                break;
            default:
                $clase = "bg-info";
                break;
        }
        return $clase;
    }

    public function recorte($texto){
        $numero = strlen($texto);
        if($numero > 11){
            $texto = substr($texto, 0, 8);
            $texto.='..';
        }
        return $texto;
    }

    public function init(){
        $modulo=2;
        $permiso_acceder="No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="boton en el menu y ver interfaz modulo" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $permiso_acceder="Si";
              }
            }
          }
        }
        if($permiso_acceder=="Si"){
        /*$get = file_get_contents("https://www.google.com/finance/converter?a=1&from=EUR&to=COP");
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);
        $euro= preg_replace("/[^0-9\.]/", null, $get[0]);*/
        $euro=3503;
        /*$get = file_get_contents("https://www.google.com/finance/converter?a=1&from=USD&to=COP");
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);
        $dolar= preg_replace("/[^0-9\.]/", null, $get[0]);*/
        $dolar=2909;

		$data['permiso_guardar_cierre_ano']="No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Guardar cierre ano" AND `id_permisomodulo`="'.$modulo.'"');
		if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_guardar_cierre_ano']="Si";
              }
            }
          }
        }

        $actividades_detalles=Actividades_detalles::all();
        $oportunidades=Oportunidades::all();
        $tipo_actividades=Tipo_actividades::all();
        $metas=Metas_oportunidade::all();
        $participaciones=Participacion_probabilidade::all();
        $usuarios=User::all();
        $empresas=Empresa::all();
        $clientes=Cliente::all();
        $ciclos=Ciclo::all();
        $ciclos=burbuja($ciclos);
        $actividades=Actividades::all();
        $presupuestos=Presupuesto::all();
        $presupuesto_anual=Presupuesto_anual::all();
        $gastos=Gasto::all();
        $metas1=Metas_oportunidade::findOrFail(1);
        /*traigo las historias que sean del 90%*/
        $historialoportunidades=HistorialOportunidades::where('key',"=", "ciclo_venta")->where('value',"=", "90")->get();
        /*Fin traigo las historias que sean del 90%*/
        $total_negocio_cerrado=0;
        $tiempo=0;
        foreach($historialoportunidades as $ho){
            $fecha_cierre_vendida=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "fecha_oc" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `id` DESC');
            if((date("Y", strtotime($fecha_cierre_vendida[0]->value)))==(date('Y'))){
                /* Negocio Cerrado */
                    /*Traigo el valor de la oportunidad */
                    $historialoportunidades1=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "val_pesos" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `updated_at` DESC');
                    /*$historialoportunidades1=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "valor_oportunidad" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `updated_at` DESC');*/
                    /*Fin Traigo el valor de la oportunidad */

                    /*Traigo la moneda de la oportunidad */
                    $historialoportunidades2=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "moneda" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `updated_at` DESC');
                    /*Fin Traigo la moneda de la oportunidad */

                    $cantidad='';
                    $moneda_origen='';
                    $valor='';
                    $cho1=0;
                    /*Solo tomo el primer registro el cual es el mas actual y guardo la cantidad */
                    $cantidad=(int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $historialoportunidades1[0]->value))));
                    /*foreach($historialoportunidades1 as $ho1){
                        if($cho1==0){
                            $cantidad=str_replace(",", "", ($ho1->value));
                            $cantidad=(int)$cantidad;
                        }
                        $cho1++;
                    }*/
                    /*Fin solo tomo el primer registro el cual es el mas actual y guardo la cantidad */
                    $cho2=0;
                    /*Solo tomo el primer registro el cual es el mas actual y guardo la moneda origen */
                    foreach($historialoportunidades2 as $ho2){
                        if($cho2==0){
                            $moneda_origen=($ho2->value);
                        }
                        $cho2++;
                    }
                    /*Fin solo tomo el primer registro el cual es el mas actual y guardo la moneda origen */

                    /*pregunto si la moneda origen es igual a la destino para evitar error en conversion */

                    /*if($moneda_origen!='COP'){
                        $valor=moneda($cantidad,$moneda_origen,$euro,$dolar);
                        $total_negocio_cerrado+=((int)$valor);
                    }else{
                        $total_negocio_cerrado+=$cantidad;
                    }*/
                    $total_negocio_cerrado+=$cantidad;
                    /*Fin pregunto si la moneda origen es igual a la destino para evitar error en conversion */
                /*Fin Negocio Cerrado */

                /*Promedio Cierre */
                    /*Traigo la fecha de inicio para restarcela a la fecha fin*/
                    $historialoportunidades3=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "fecha_ff" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `updated_at` DESC');
                    $historialoportunidades4=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "fecha_oc" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `updated_at` DESC');
                    $cho3=0;
                    foreach($historialoportunidades3 as $ho3){
                        if($cho3==0){
                            $fecha_inicio=strtotime($ho3->value);
                        }
                        $cho3++;
                    }
                    $cho4=0;
                    foreach($historialoportunidades4 as $ho4){
                        if($cho4==0){
                            $fecha_fin=strtotime($ho4->value);
                        }
                        $cho4++;
                    }
                    $tiempo+=$fecha_fin - $fecha_inicio;
                    /*Fin Traigo la fecha de inicio para restarcela a la fecha fin*/

                /* Fin Promedio Cierre */

            }
        }

        /* //Esto es para sacar los dias horas minutos y segundos de unos segundos
        $d = floor($tiempo / 86400);
        $h = floor(($tiempo - ($d * 86400)) / 3600);
        $m = floor(($tiempo - ($d * 86400) - ($h * 3600)) / 60);
        $s = $tiempo % 60;
        var_dump('Dias: '.$d.', horas: '.$h.', minutos: '.$m.', segundos: '.$s.' '.$ho->oportunidad_id);*/
        $dias=$tiempo / 86400;
        $total_negocio_cerrado_para_gasto=$total_negocio_cerrado;
        $total_negocio_cerrado=round($total_negocio_cerrado/1000000);
        $total_negocio_cerrado=(int)$total_negocio_cerrado;
        $total_negocio_cerrado=number_format($total_negocio_cerrado);




    /*donas*/
        $y=0;
        $donas[0]['corto']=0;
        $donas[0]['mediano']=0;
        $donas[0]['largo']=0;
        $donas[1]['Alta']=0;
        $donas[1]['Media']=0;
        $donas[1]['Baja']=0;
        $total_oportunidades_identificadas=0;
           foreach($usuarios as $usuario){
            if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
                //var_dump('Corto: '.$donas[0]['corto'].' Mediano: '.$donas[0]['medio']=0.' Largo: '.$donas[0]['lago']);
              $nombres=explode(" ",$usuario->nombres);
              $usuario1[$y][0]='';
              $usuario1[$y][1]=0;

              $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE value = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

              foreach ($mis_oportunidades as $m_o) {
                $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');

                if(($oportunidad_buscar[0]->value)==$usuario->id){
                  $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                  if(count($plazo)>0){

                    $dias=(strtotime($plazo[0]->value))-(strtotime(date('Y-m-d')));
                    if($dias<0){
                        $dias=0;
                    }
                    $dias=$dias/86400;
                    $dias=(int)$dias;



                    $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "probabilidad" order by id DESC');
                     $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                    /*$valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "valor_oportunidad" order by id DESC');
                    $moneda=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "moneda" order by id DESC');
                    if(($moneda[0]->value)!='COP'){
                      $cantidad=str_replace(",", "", $valor[0]->value);
                      $valor_total=moneda($cantidad,$moneda[0]->value,$euro,$dolar);
                      $valor_total=(int)$valor_total;
                    }else{
                      $valor_total=$valor[0]->value;
                      $valor_total=str_replace(",", "", $valor_total);
                      $valor_total=(int)$valor_total;
                    }
                    $valor_total=round($valor_total/1000000);
                    $valor_total=(int)$valor_total;*/
                    if(isset($valor[0]->value)){
                        $total_oportunidades_identificadas+=str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor[0]->value)));
                        $valor_total=(((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor[0]->value)))))/1000000);
                    }else{
                        $valor_total=0;
                    }

                    $usuario1[$y][1]+= $valor_total;


                    if($dias<=($metas1->corto_dias)){
                        $donas[0]['corto']+=$valor_total;
                    }elseif($dias<=($metas1->medio_dias)){
                        $donas[0]['mediano']+=$valor_total;
                    }elseif($dias>($metas1->medio_dias)){
                        $donas[0]['largo']+=$valor_total;
                    }
                    if(count($probabilidad)>0){
                      if(($probabilidad[0]->value)=='Alta'){
                        $donas[1]['Alta']+=$valor_total;
                      }elseif(($probabilidad[0]->value)=='Media'){
                        $donas[1]['Media']+=$valor_total;
                      }elseif(($probabilidad[0]->value)=='Baja'){
                        $donas[1]['Baja']+=$valor_total;
                      }
                    }

                  }
                }
              }
              $usuario1[$y][0] = $nombres[0];
              $y++;
            }
          }

    /*fin donas*/
    /*Gastos*/
    $actividades_gastos = DB::select('SELECT * FROM `actividades` WHERE YEAR(`fecha_actividad`) = '.date('Y').' ORDER BY `id` ASC');
    $valor_total_gastos=0;
    foreach($actividades_gastos as $actividad){
        $usuarios_comerciales_partner=DB::select('SELECT * FROM `users` WHERE `id` = "'.$actividad->user_id.'"');
        if(count($usuarios_comerciales_partner)>0){
            if($usuarios_comerciales_partner[0]->cargo=="Ejecutivo Comercial" || $usuarios_comerciales_partner[0]->cargo=="Partner Solution"){
                $gastos=str_replace(",","",$actividad->alimentacion)+str_replace(",","",$actividad->transportes_internos)+str_replace(",","",$actividad->transportes_intermunicipales)+str_replace(",","",$actividad->tiquete_aereo)+str_replace(",","",$actividad->papeleria)+str_replace(",","",$actividad->invitacion_cliente)+str_replace(",","",$actividad->alquiler_vehiculo)+str_replace(",","",$actividad->gasolina_pasajes)+str_replace(",","",$actividad->hotel)+str_replace(",","",$actividad->otros);
                $horas_hombre=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$actividad->id.'"');
                $valor_propio=0;
                foreach ($horas_hombre as $h_h) {
                  $valor_propio+=str_replace(",","",$h_h->valor);
                }
                $valor_total_gastos+=$gastos+$valor_propio;
            }
        }
    }

    foreach($usuarios as $usuario){
        if($usuario->cargo=="Ejecutivo Comercial" || $usuario->cargo=="Partner Solution"){
            $ajuste=DB::select('SELECT (SUM(valor_alimentacion) + SUM(valor_transporte_interno) + SUM(valor_transporte_intermunicipal) + SUM(valor_tiquete_aereo) + SUM(valor_papeleria) + SUM(valor_invitacion_cliente) + SUM(valor_alquiler_vehiculo) + SUM(valor_gasolina_pasaje) +SUM(valor_hotel) + SUM(valor_otros) + SUM(valor_salariopropio) +SUM(valor_salariotercero)) AS total FROM ajuste_gastos WHERE user_id = '.$usuario->id.' AND anio = '.date("Y").' AND tipo = "valorreal"');
            $valor_total_gastos+=$ajuste[0]->total;
        }
    }
    // validar 0
    if ($total_negocio_cerrado_para_gasto === 0) {
        $porcentaje_gastos = 100;
    }else if($valor_total_gastos === 0){
      $porcentaje_gastos = 0;
    }else{
        $porcentaje_gastos=($valor_total_gastos/$total_negocio_cerrado_para_gasto)*100;
    }

    $porcentaje_gastos = number_format($porcentaje_gastos, 2, '.', ' ');

    $mes_anterior=strtotime("-1 Month",strtotime(date("Y-m")));
    // if ($mes_anterior === 0) {
    //   $mes_anterior = 12;
    // }
    $mes_anterior=date ( 'Y-m-d' , $mes_anterior );
    $mes_anterior_menor=(date('Y-m', strtotime($mes_anterior))).'-01';
    $ultimo_dia=cal_days_in_month ( CAL_GREGORIAN, date("m", strtotime($mes_anterior_menor)), date("Y") );
    $mes_anterior_mayor=(date('Y-m', strtotime($mes_anterior))).'-'.$ultimo_dia;

    $gastos_ultimo_mes=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` >= "'.$mes_anterior_menor.'" AND `fecha_actividad` <= "'.$mes_anterior_mayor.'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');

    $g['gastos_ultimo_mes']=0;
    $valor_total_gastos1=0;
    foreach($gastos_ultimo_mes as $g_u_mes){
        $valor1=((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->alimentacion)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->transportes_internos)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->transportes_intermunicipales)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->tiquete_aereo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->papeleria)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->invitacion_cliente)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->alquiler_vehiculo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->gasolina_pasajes)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->hotel)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->otros)))));
        $valor_total_gastos1+=$valor1;
    }

    $mes_anterior_ajuste=date('m', strtotime($mes_anterior));
    $ano_anterior_ajuste=date('Y', strtotime($mes_anterior));
    $ajuste_ultimo_mes=DB::select('SELECT * FROM `ajuste_gastos` WHERE `tipo` = "ajuste" AND `mes` = "'.$mes_anterior_ajuste.'" AND `anio` = "'.$ano_anterior_ajuste.'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
    $valor1=0;
    $valor_total_ajuste=0;
    foreach($ajuste_ultimo_mes as $a_u_m){
        $valor1=$a_u_m->valor_alimentacion+$a_u_m->valor_transporte_interno+$a_u_m->valor_transporte_intermunicipal+$a_u_m->valor_tiquete_aereo+$a_u_m->valor_papeleria+$a_u_m->valor_invitacion_cliente+$a_u_m->valor_alquiler_vehiculo+$a_u_m->valor_gasolina_pasaje+$a_u_m->valor_hotel+$a_u_m->valor_otros;
        $valor_total_ajuste+=$valor1;
    }

    foreach($usuarios as $user){
        if(($user->cargo)=='Ejecutivo Comercial' || ($user->cargo)=='Partner Solution'){
            $valor_total_gastos11=0;
            $valor_total_ajuste1=0;
            foreach ($gastos_ultimo_mes as $g_u_mes) {
                if($g_u_mes->user_id == $user->id){
                    $valor11=((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->alimentacion)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->transportes_internos)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->transportes_intermunicipales)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->tiquete_aereo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->papeleria)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->invitacion_cliente)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->alquiler_vehiculo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->gasolina_pasajes)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->hotel)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_u_mes->otros)))));
                    $valor_total_gastos11+=$valor11;
                }
            }

            foreach($ajuste_ultimo_mes as $a_u_m){
                if($a_u_m->user_id==$user->id){
                    $valor11=$a_u_m->valor_alimentacion+$a_u_m->valor_transporte_interno+$a_u_m->valor_transporte_intermunicipal+$a_u_m->valor_tiquete_aereo+$a_u_m->valor_papeleria+$a_u_m->valor_invitacion_cliente+$a_u_m->valor_alquiler_vehiculo+$a_u_m->valor_gasolina_pasaje+$a_u_m->valor_hotel+$a_u_m->valor_otros;
                    $valor_total_ajuste1+=$valor11;
                }

            }
            $g_x_u[$user->id]['gastos_ultimo_mes']=$valor_total_gastos11-$valor_total_ajuste1;
        }
    }
    /*$g['gastos_ultimo_mes']=$valor_total_gastos1-$valor_total_ajuste;*/
    $g['gastos_ultimo_mes']=$valor_total_gastos1;
    $ano_actual=date ('Y-01-01');
    $gastos_ano_actual=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` >= "'.$ano_actual.'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
    $g['gastos_ano_actual']=0;
    $valor_total_gastos2=0;
    foreach($gastos_ano_actual as $g_a_actual){
        $valor2=((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->alimentacion)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->transportes_internos)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->transportes_intermunicipales)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->tiquete_aereo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->papeleria)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->invitacion_cliente)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->alquiler_vehiculo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->gasolina_pasajes)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->hotel)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->otros)))));
        $valor_total_gastos2+=$valor2;
    }
    $ajuste_ano_actual=DB::select('SELECT * FROM `ajuste_gastos` WHERE `tipo` = "ajuste" AND `anio` = "'.date("Y").'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');

    $valor1=0;
    $valor_total_ajuste2=0;
    foreach($ajuste_ano_actual as $a_a_a){
        if( ((int)($a_a_a->mes)) <= ((int)(date("m"))) ){
            $valor1=$a_a_a->valor_alimentacion+$a_a_a->valor_transporte_interno+$a_a_a->valor_transporte_intermunicipal+$a_a_a->valor_tiquete_aereo+$a_a_a->valor_papeleria+$a_a_a->valor_invitacion_cliente+$a_a_a->valor_alquiler_vehiculo+$a_a_a->valor_gasolina_pasaje+$a_a_a->valor_hotel+$a_a_a->valor_otros;
            $valor_total_ajuste2+=$valor1;
        }
    }
    $presupuesto_cumplimiento_presupuestal=DB::select('SELECT * FROM `presupuestos` WHERE `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
    foreach($usuarios as $user){
        if(($user->cargo)=='Ejecutivo Comercial' || ($user->cargo)=='Partner Solution'){
            $valor_total_gastos21=0;
            foreach ($gastos_ano_actual as $g_a_actual) {
                if($g_a_actual->user_id == $user->id){
                    $valor21=((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->alimentacion)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->transportes_internos)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->transportes_intermunicipales)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->tiquete_aereo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->papeleria)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->invitacion_cliente)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->alquiler_vehiculo)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->gasolina_pasajes)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->hotel)))))+((int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $g_a_actual->otros)))));
                    $valor_total_gastos21+=$valor21;
                }
            }
            $valor1=0;
            $valor_total_ajuste21=0;
            foreach($ajuste_ano_actual as $a_a_a){
                if( ((int)($a_a_a->mes)) <= ((int)(date("m"))) ){
                    if($a_a_a->user_id == $user->id){
                        $valor1=$a_a_a->valor_alimentacion+$a_a_a->valor_transporte_interno+$a_a_a->valor_transporte_intermunicipal+$a_a_a->valor_tiquete_aereo+$a_a_a->valor_papeleria+$a_a_a->valor_invitacion_cliente+$a_a_a->valor_alquiler_vehiculo+$a_a_a->valor_gasolina_pasaje+$a_a_a->valor_hotel+$a_a_a->valor_otros;
                        $valor_total_ajuste21+=$valor1;
                    }
                }
            }
            $g_x_u[$user->id]['gastos_ano_actual']=$valor_total_gastos21-$valor_total_ajuste21;
        }
    }
    $g['gastos_ano_actual']=$valor_total_gastos2-$valor_total_ajuste2;



    $mes_actual=date ('Y-m-01');
    $dias_mes_actual = cal_days_in_month(CAL_GREGORIAN, ((int)date('m')), ((int)date('Y')));
    $mes_actual2=(date ('Y-m')).'-'.$dias_mes_actual;
    $mes_actual2=date('Y-m-d', strtotime($mes_actual2));
    $gastos_mes_actual=DB::select('SELECT * FROM `actividades` WHERE `fecha_actividad` >= "'.$mes_actual.'" AND `fecha_actividad` <= "'.$mes_actual2.'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
    $g['gastos_mes_actual']=0;
    $valor_total_gastos3=0;
    foreach($gastos_mes_actual as $g_m_actual){
        $valor3=((int)(str_replace(",", "", $g_m_actual->alimentacion)))+((int)(str_replace(",", "", $g_m_actual->transportes_internos)))+((int)(str_replace(",", "", $g_m_actual->transportes_intermunicipales)))+((int)(str_replace(",", "", $g_m_actual->tiquete_aereo)))+((int)(str_replace(",", "", $g_m_actual->papeleria)))+((int)(str_replace(",", "", $g_m_actual->invitacion_cliente)))+((int)(str_replace(",", "", $g_m_actual->alquiler_vehiculo)))+((int)(str_replace(",", "", $g_m_actual->gasolina_pasajes)))+((int)(str_replace(",", "", $g_m_actual->hotel)))+((int)(str_replace(",", "", $g_m_actual->otros)));
        $valor_total_gastos3+=$valor3;
    }
    foreach($usuarios as $user){
        $valor_total_gastos31=0;
        foreach ($gastos_mes_actual as $g_m_actual) {
            if($g_m_actual->user_id == $user->id){
                $valor31=((int)(str_replace(",", "", $g_m_actual->alimentacion)))+((int)(str_replace(",", "", $g_m_actual->transportes_internos)))+((int)(str_replace(",", "", $g_m_actual->transportes_intermunicipales)))+((int)(str_replace(",", "", $g_m_actual->tiquete_aereo)))+((int)(str_replace(",", "", $g_m_actual->papeleria)))+((int)(str_replace(",", "", $g_m_actual->invitacion_cliente)))+((int)(str_replace(",", "", $g_m_actual->alquiler_vehiculo)))+((int)(str_replace(",", "", $g_m_actual->gasolina_pasajes)))+((int)(str_replace(",", "", $g_m_actual->hotel)))+((int)(str_replace(",", "", $g_m_actual->otros)));
                $valor_total_gastos31+=$valor31;
            }
        }
        $g_x_u[$user->id]['gastos_mes_actual']=$valor_total_gastos31;
    }
    $g['gastos_mes_actual']=$valor_total_gastos3;

    $anno_actual=date('Y');
    $presupuesto_del_ano=0;
    $presupuesto_anual=DB::select('SELECT * FROM `presupuesto_anuals` WHERE `anio` = "'.date('Y').'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
    foreach($presupuesto_anual as $p_a){
        if(((int)($p_a->anio)) == ((int)$anno_actual)){
            $presupuesto_del_ano+=((int)(str_replace(",", "", $p_a->alimentacion)))+((int)(str_replace(",", "", $p_a->transporte_interno)))+((int)(str_replace(",", "", $p_a->transporte_intermunicipal)))+((int)(str_replace(",", "", $p_a->tiquete_aereo)))+((int)(str_replace(",", "", $p_a->papeleria)))+((int)(str_replace(",", "", $p_a->invitacion_cliente)))+((int)(str_replace(",", "", $p_a->alquiler_vehiculo)))+((int)(str_replace(",", "", $p_a->gasolina_pasaje)))+((int)(str_replace(",", "", $p_a->hotel)))+((int)(str_replace(",", "", $p_a->otros)));
        }
    }

    foreach($usuarios as $user){
        if(($user->cargo)=='Ejecutivo Comercial' || ($user->cargo)=='Partner Solution'){
            $presupuesto_del_ano1=0;
            foreach ($presupuesto_anual as $p_a) {
                if($p_a->user_id == $user->id){
                    if(((int)($p_a->anio)) == ((int)$anno_actual)){
                        $presupuesto_del_ano1+=((int)(str_replace(",", "", $p_a->alimentacion)))+((int)(str_replace(",", "", $p_a->transporte_interno)))+((int)(str_replace(",", "", $p_a->transporte_intermunicipal)))+((int)(str_replace(",", "", $p_a->tiquete_aereo)))+((int)(str_replace(",", "", $p_a->papeleria)))+((int)(str_replace(",", "", $p_a->invitacion_cliente)))+((int)(str_replace(",", "", $p_a->alquiler_vehiculo)))+((int)(str_replace(",", "", $p_a->gasolina_pasaje)))+((int)(str_replace(",", "", $p_a->hotel)))+((int)(str_replace(",", "", $p_a->otros)));
                    }
                }
            }
            if($presupuesto_del_ano1>0){
                $g_x_u[$user->id]['p_a_e']=$g_x_u[$user->id]['gastos_ano_actual']/$presupuesto_del_ano1*100;
            }else{
                $g_x_u[$user->id]['p_a_e']=0;
            }
            $g_x_u[$user->id]['p_a_e']=number_format($g_x_u[$user->id]['p_a_e'], 1, '.', ' ');
        }
    }
    if($presupuesto_del_ano>0){
        $p_a_e=$g['gastos_ano_actual']/$presupuesto_del_ano*100;
    }else{
        $p_a_e=0;
    }
    $p_a_e=number_format($p_a_e, 1, '.', ' ');
    $g['p_a_e']=$p_a_e;
    /*Fin Gastos */

    /*Gastos por Usuario*/
    foreach ($usuarios as $usuar) {
        if(($usuar->cargo)=='Ejecutivo Comercial' || ($usuar->cargo)=='Partner Solution'){
            /*por usuario*/

            $gasto_x_usuario[$usuar->id]['ventas']=0;
            foreach ($historialoportunidades as $ho) {
                if($ho->key=='ciclo_venta' && $ho->value=='90'){
                    $historialoportunidades_vendidas=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "responsable" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `id` DESC');
                    if(count($historialoportunidades_vendidas)>0){
                        if($historialoportunidades_vendidas[0]->value == $usuar->id){
                            $historial_x_usuario_valor=DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` = "val_pesos" AND `oportunidad_id` = '.$ho->oportunidad_id.' ORDER BY `id` DESC');
                            if(isset($historial_x_usuario_valor[0]->value)){
                                $fc = DB::select('SELECT value FROM historial_oportunidades H WHERE H.key = "fecha_oc" AND H.oportunidad_id = '.$historial_x_usuario_valor[0]->oportunidad_id.' ORDER BY created_at DESC LIMIT 0,1');
                                if(date('Y', strtotime($fc[0]->value)) == date('Y')){
                                    $gasto_x_usuario[$usuar->id]['ventas']+=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $historial_x_usuario_valor[0]->value)))));
                                }
                            }
                        }
                    }
                }
            }

            /*fin por usuario*/
            $nombres=explode(" ",$usuar->nombres);
            $gasto_x_usuario[$usuar->id]['id_user']=$usuar->id;
            $gasto_x_usuario[$usuar->id]['nombre']=$nombres[0];
            $gasto_x_usuario[$usuar->id]['foto']=$usuar->foto;
            $gasto_x_usuario[$usuar->id]['pais']=$usuar->principal;
            $gasto_x_usuario[$usuar->id]['gasto']=0;
            foreach($actividades as $actividad){
                if($usuar->id == $actividad->user_id && date('Y', strtotime($actividad->fecha_actividad)) == date('Y')){
                    //$valor=str_replace(",", "", $actividad->total_general);
                    $gastos=str_replace(",","",$actividad->alimentacion)+str_replace(",","",$actividad->transportes_internos)+str_replace(",","",$actividad->transportes_intermunicipales)+str_replace(",","",$actividad->tiquete_aereo)+str_replace(",","",$actividad->papeleria)+str_replace(",","",$actividad->invitacion_cliente)+str_replace(",","",$actividad->alquiler_vehiculo)+str_replace(",","",$actividad->gasolina_pasajes)+str_replace(",","",$actividad->hotel)+str_replace(",","",$actividad->otros);
                    $horas_hombre=DB::select('SELECT * FROM `horas_hombres` WHERE `actividad_id` = "'.$actividad->id.'"');
                    $valor_propio=0;
                    foreach ($horas_hombre as $h_h) {
                      $valor_propio+=str_replace(",","",$h_h->valor);
                    }
                    $gasto_x_usuario[$usuar->id]['gasto']+=$gastos+$valor_propio;
                }
            }
            if($gasto_x_usuario[$usuar->id]['ventas']==0){
                $gasto_x_usuario[$usuar->id]['porcentaje']=100;
            }else{
                $gasto_x_usuario[$usuar->id]['porcentaje']=($gasto_x_usuario[$usuar->id]['gasto']/$gasto_x_usuario[$usuar->id]['ventas'])*100;
            }
            $gasto_x_usuario[$usuar->id]['gasto']=number_format($gasto_x_usuario[$usuar->id]['gasto'], 0, '.', ',');
            if($gasto_x_usuario[$usuar->id]['porcentaje']<100){
              $gasto_x_usuario[$usuar->id]['porcentaje'] = number_format($gasto_x_usuario[$usuar->id]['porcentaje'], 2, '.', ' ');
            }

        }
    }
    /*Fin Gastos por Usuario*/
    /*Tipo Actividades*/
    $numero_tipo_actividad=DB::select('SELECT `tipo_actividad`, SUM(`tiempo_fin` - `tiempo_inicio`) as `horas` FROM `actividades` GROUP BY `tipo_actividad` ORDER BY `horas` DESC');
    $total_horas_actividad=0;
    foreach ($numero_tipo_actividad as $n_t_a) {
        $total_horas_actividad+=$n_t_a->horas;
    }

    //rsort($numero_tipo_actividad);

    /*fin Tipo Actividades */

    /*Gestión Actividades Pendientes */
    $HOY=date('Y-m-d');
    $gestion_actividades=DB::select('SELECT DISTINCT(`actividades_detalles`) FROM `actividades_detalles_canceladas`');
    $gestion_actividades1=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "pendiente" AND `fecha_pendiente` < "'.$HOY.'"');
    $pendientes_menores_hoy=0;
    foreach ($gestion_actividades1 as $g_a1) {
        $gestion_actividades11=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$g_a1->id.'"');
        if(count($gestion_actividades11)>0){

        }else{
            $pendientes_menores_hoy++;
        }

    }

    $gestion_actividades2=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "cancelado"');
    $x=count($gestion_actividades)+$pendientes_menores_hoy+count($gestion_actividades2);

    $todo=0;

    foreach ($actividades_detalles as $a_d) {
        if($a_d->fecha_pendiente < $HOY){
            $gestion_actividades=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$a_d->id.'"');
            if(count($gestion_actividades)==0){
                $todo++;
            }
        }
    }

    $gestion_actividades=DB::select('SELECT DISTINCT(`actividades_detalles`) FROM `actividades_detalles_canceladas`');
    $punto=count($gestion_actividades)+$todo;
    if($punto==0){
        $hechas_tiempo=0;
    }else{
        $hechas_tiempo=1-$x/$punto;

    }
    $gestion['hechas_tiempo']=number_format($hechas_tiempo*100, 1, '.', ' ');
    foreach ($usuarios as $usuario) {

        $gestion_actividades=DB::select('SELECT DISTINCT(`actividades_detalles`) FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` IN ( SELECT `id` FROM `actividades_detalles` WHERE `responsable` = '.$usuario->id.' )');
        $gestion_actividades1=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "pendiente" AND `fecha_pendiente` < "'.$HOY.'" AND `responsable` = "'.$usuario->id.'"');
        $pendientes_menores_hoy=0;
        foreach ($gestion_actividades1 as $g_a1) {
            $gestion_actividades11=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$g_a1->id.'"');
            if(count($gestion_actividades11)>0){

            }else{
                $pendientes_menores_hoy++;
            }

        }

        $gestion_actividades2=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "cancelado" AND `responsable` = "'.$usuario->id.'"');
        $x=count($gestion_actividades)+$pendientes_menores_hoy+count($gestion_actividades2);

        $todo=0;

        foreach ($actividades_detalles as $a_d) {
            if($a_d->fecha_pendiente < $HOY && $a_d->responsable == $usuario->id){
                $gestion_actividades=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$a_d->id.'"');
                if(count($gestion_actividades)==0){
                    $todo++;
                }
            }
        }

        $gestion_actividades=DB::select('SELECT DISTINCT(`actividades_detalles`) FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` IN ( SELECT `id` FROM `actividades_detalles` WHERE `responsable` = "'.$usuario->id.'" )');
        $punto=count($gestion_actividades)+$todo;
        if($punto==0){
            $hechas_tiempo=0;
        }else{
            $hechas_tiempo=1-$x/$punto;
        }
        $gestion_usuario[$usuario->id]['hechas_tiempo']=number_format($hechas_tiempo*100, 1, '.', ' ');
    }
        /*Actividades pendientes de la semana */
        $ultimoDiasemanasiguiente=strtotime("+7 Day",strtotime(date("Y-m-d")));
        $ultimoDiasemanasiguiente=date ( 'Y-m-d' , $ultimoDiasemanasiguiente );
        $gestion_actividades=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "pendiente"');

        $a_pendientes=0;
        foreach ($gestion_actividades as $g_a) {
            $gestion_actividades1=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$g_a->id.'" ORDER BY `fecha_ejecucion` DESC');
            if(count($gestion_actividades1)>0){
                if($gestion_actividades1[0]->fecha_ejecucion >= $HOY && $gestion_actividades1[0]->fecha_ejecucion <= $ultimoDiasemanasiguiente){
                    $a_pendientes++;
                }
            }elseif($g_a->fecha_pendiente >= $HOY && $g_a->fecha_pendiente <= $ultimoDiasemanasiguiente){
                $a_pendientes++;
            }
        }

        $gestion['pendientes_semana']=$a_pendientes;
        foreach($usuarios as $usuario){
            $gestion_actividades=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "pendiente" AND `responsable` = "'.$usuario->id.'"');

            $a_pendientes=0;
            foreach ($gestion_actividades as $g_a) {
                $gestion_actividades1=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$g_a->id.'" ORDER BY `fecha_ejecucion` DESC');
                if(count($gestion_actividades1)>0){
                    if($gestion_actividades1[0]->fecha_ejecucion >= $HOY && $gestion_actividades1[0]->fecha_ejecucion <= $ultimoDiasemanasiguiente){
                        $a_pendientes++;
                    }
                }elseif($g_a->fecha_pendiente >= $HOY && $g_a->fecha_pendiente <= $ultimoDiasemanasiguiente){
                    $a_pendientes++;
                }
            }
            $gestion1[$usuario->id]['pendientes_semana']=$a_pendientes;

        }
        /*Fin Actividades pendientes de la semana */
        /*Actividades pendientes del mes */
        $ultimoDiamessiguiente=strtotime("+1 Month",strtotime(date("Y-m-d")));
        $ultimoDiamessiguiente=date ( 'Y-m-d' , $ultimoDiamessiguiente );

        $gestion_actividades=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "pendiente"');

        $a_pendientes=0;
        foreach ($gestion_actividades as $g_a) {
            $gestion_actividades1=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$g_a->id.'" ORDER BY `fecha_ejecucion` DESC');
            if(count($gestion_actividades1)>0){
                if($gestion_actividades1[0]->fecha_ejecucion >= $HOY && $gestion_actividades1[0]->fecha_ejecucion <= $ultimoDiamessiguiente){
                    $a_pendientes++;
                }
            }elseif($g_a->fecha_pendiente >= $HOY && $g_a->fecha_pendiente <= $ultimoDiamessiguiente){
                $a_pendientes++;
            }
        }
        $gestion['pendientes_mes']=$a_pendientes;
        foreach($usuarios as $usuario){

            $gestion_actividades=DB::select('SELECT * FROM `actividades_detalles` WHERE `estado` = "pendiente" AND `responsable` = "'.$usuario->id.'"');

            $a_pendientes=0;
            foreach ($gestion_actividades as $g_a) {
                $gestion_actividades1=DB::select('SELECT * FROM `actividades_detalles_canceladas` WHERE `actividades_detalles` = "'.$g_a->id.'" ORDER BY `fecha_ejecucion` DESC');
                if(count($gestion_actividades1)>0){
                    if($gestion_actividades1[0]->fecha_ejecucion >= $HOY && $gestion_actividades1[0]->fecha_ejecucion <= $ultimoDiamessiguiente){
                        $a_pendientes++;
                    }
                }elseif($g_a->fecha_pendiente >= $HOY && $g_a->fecha_pendiente <= $ultimoDiamessiguiente){
                    $a_pendientes++;
                }
            }
            $gestion1[$usuario->id]['pendientes_mes']=$a_pendientes;
        }
        /*Fin Actividades pendientes del mes */
        /*Actividades aplazadas */
        $gestion_actividades=DB::select('SELECT DISTINCT(`actividades_detalles`) FROM actividades_detalles_canceladas ');
        $gestion['aplazadas_mes']=count($gestion_actividades);
        foreach($usuarios as $usuario){
            $gestion_actividades=DB::select('SELECT DISTINCT(`actividades_detalles`) FROM actividades_detalles_canceladas WHERE `actividades_detalles` IN (SELECT `id` FROM `actividades_detalles` WHERE `responsable` = "'.$usuario->id.'")');
            $gestion1[$usuario->id]['aplazadas_mes']=count($gestion_actividades);
        }
        /*Fin Actividades aplazadas */
    /*Fin Gestión Actividades Pendientes */



    /*  */

    /**/

    /*Ventas*/

        $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
        $total_equipos_vendidos=0;
        $total_pais_vendidos=0;
        foreach($vendidas_oportunidades as $v_o){
            $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
            if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                /*Equipos vendidos*/
                $productos_vendidas_oportunidades=DB::select('SELECT SUM(`cantidad`) as totalEquipos FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `deleted_at` IS NULL');
                $total_equipos_vendidos+=(int)($productos_vendidas_oportunidades[0]->totalEquipos);
                /*Fin Equipos vendidos*/

                /*paises*/
                $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
                if(isset($pais_vendidas_oportunidades[0]->pais)){
                    if(isset($pais_vendida[$pais_vendidas_oportunidades[0]->pais])){
                        $pais_vendida[$pais_vendidas_oportunidades[0]->pais]+=1;
                    }else{
                       $pais_vendida[$pais_vendidas_oportunidades[0]->pais]=1;
                       $total_pais_vendidos+=1;
                    }
                }
                /*Fin paises*/
            }

        }

    /*Fin Ventas*/

    /*Proyecciones*/

    $todas_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
    $total_equipos_proyectados=0;
    $total_pais_proyectados=0;
    foreach($todas_oportunidades as $t_o){
        $oportunidad_buscar=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "ciclo_venta" ORDER BY `id` DESC');
        if(((int)$oportunidad_buscar[0]->value)>=10 && ((int)$oportunidad_buscar[0]->value)<90){
            $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');

            if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                /*Equipos proyecciones*/
                $productos_proyecciones_oportunidades=DB::select('SELECT SUM(`cantidad`) as totalEquipos FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `deleted_at` IS NULL');
                $total_equipos_proyectados+=(int)($productos_proyecciones_oportunidades[0]->totalEquipos);
                /*Fin Equipos proyecciones*/

                /*paises*/
                $sede_proyectados_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                $pais_proyectados_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_proyectados_oportunidades[0]->value.'"');
                if(isset($pais_proyectados_oportunidades[0]->pais)){
                    if(isset($pais_proyectados[$pais_proyectados_oportunidades[0]->pais])){
                        $pais_proyectados[$pais_proyectados_oportunidades[0]->pais]+=1;
                    }else{
                       $pais_proyectados[$pais_proyectados_oportunidades[0]->pais]=1;
                       $total_pais_proyectados+=1;
                    }
                }
                /*Fin paises*/
            }
        }
    }

    /*Fin Proyecciones*/
        /*Cumplimiento Presupuesto Acumulado (Con Ajuste) del año actual*/
        $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM actividades WHERE fecha_actividad BETWEEN "'.date('Y-01-01').'" AND "'.date('Y-m-d').'" AND CONVERT(MONTH(fecha_actividad),UNSIGNED INTEGER) NOT IN (SELECT DISTINCT(mes) AS mes FROM ajuste_gastos WHERE anio LIKE "'.date('Y').'")';
        $data['gastos'] = DB::select($query);
        $query = 'SELECT SUM(CONVERT(valor_alimentacion,UNSIGNED INTEGER) + CONVERT(valor_transporte_interno,UNSIGNED INTEGER) + CONVERT(valor_transporte_intermunicipal,UNSIGNED INTEGER) + CONVERT(valor_tiquete_aereo,UNSIGNED INTEGER) + CONVERT(valor_papeleria,UNSIGNED INTEGER) + CONVERT(valor_invitacion_cliente,UNSIGNED INTEGER) + CONVERT(valor_alquiler_vehiculo,UNSIGNED INTEGER) + CONVERT(valor_gasolina_pasaje,UNSIGNED INTEGER) + CONVERT(valor_hotel,UNSIGNED INTEGER) + CONVERT(valor_otros,UNSIGNED INTEGER) + CONVERT(valor_salariopropio,UNSIGNED INTEGER) + CONVERT(valor_salariotercero,UNSIGNED INTEGER)) AS total  FROM ajuste_gastos WHERE anio LIKE "'.date('Y').'" AND CONVERT(mes,UNSIGNED INTEGER) <= '.intval(date('m')).' AND tipo LIKE "valorreal"';
        $data['valor_real'] = DB::select($query);
        $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_interno, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_intermunicipal, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasaje, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM presupuestos WHERE CONVERT(mes,UNSIGNED INTEGER) <= '.intval(date('m')).' AND anio LIKE "'.date('Y').'"';
        $data['presupuesto'] = DB::select($query);
        $data['e_p_a'] = 0;
        if(!empty($data['presupuesto'][0]->total)){
            $data['e_p_a'] = (($data['gastos'][0]->total + $data['valor_real'][0]->total)/$data['presupuesto'][0]->total)*100;
            $data['e_p_a'] = number_format($data['e_p_a'], 1, '.', ' ');
        }
        /*Fin Cumplimiento Presupuesto Acumulado (Con Ajuste) del año actua*/

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Dashboard" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Cumplimiento Presupuestal MODAL*/
        $users = DB::select('SELECT * FROM users WHERE cargo = "Ejecutivo Comercial" OR cargo = "Partner Solution" ORDER BY id ASC');
        ob_start();
        foreach($users as $user){
            $nombres=explode(" ",$user->nombres);
            /*Cumplimiento Presupuestal mes anterior*/
            $query = 'SELECT '.constructor_consulta('A.alimentacion').' + '.constructor_consulta('A.transportes_internos').' + '.constructor_consulta('A.transportes_intermunicipales').' + '.constructor_consulta('A.tiquete_aereo').' + '.constructor_consulta('A.papeleria').' + '.constructor_consulta('A.invitacion_cliente').' + '.constructor_consulta('A.alquiler_vehiculo').' + '.constructor_consulta('A.gasolina_pasajes').' + '.constructor_consulta('A.hotel').' + '.constructor_consulta('A.otros').' AS total  FROM actividades A INNER JOIN users U ON A.user_id = U.id WHERE MONTH(A.fecha_actividad) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND YEAR(A.fecha_actividad) = YEAR(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND U.id = '.$user->id;
            $gastos_cumplimiento = DB::select($query);
            $query = 'SELECT '.constructor_consulta('alimentacion').' + '.constructor_consulta('transporte_interno').' + '.constructor_consulta('transporte_intermunicipal').' + '.constructor_consulta('tiquete_aereo').' + '.constructor_consulta('papeleria').' + '.constructor_consulta('invitacion_cliente').' + '.constructor_consulta('alquiler_vehiculo').' + '.constructor_consulta('gasolina_pasaje').' + '.constructor_consulta('hotel').' + '.constructor_consulta('otros').' AS total FROM presupuestos WHERE mes = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND anio = YEAR(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) AND user_id = 47';
            $presupuesto = DB::select($query);
            if($presupuesto[0]->total > 0){
                $c_p_m_a =  $gastos_cumplimiento[0]->total / $presupuesto[0]->total *100;
                $c_p_m_a = number_format($c_p_m_a, 2, ',', ' ');
            }else{
                $c_p_m_a = 0;
            }
            /*Fin Cumplimiento Presupuestal mes anterior*/

            /*Cumplimiento Parcial Presupuesto Mes Actual*/
            $query = 'SELECT '.constructor_consulta('A.alimentacion').' + '.constructor_consulta('A.transportes_internos').' + '.constructor_consulta('A.transportes_intermunicipales').' + '.constructor_consulta('A.tiquete_aereo').' + '.constructor_consulta('A.papeleria').' + '.constructor_consulta('A.invitacion_cliente').' + '.constructor_consulta('A.alquiler_vehiculo').' + '.constructor_consulta('A.gasolina_pasajes').' + '.constructor_consulta('A.hotel').' + '.constructor_consulta('A.otros').' AS total  FROM actividades A WHERE MONTH(A.fecha_actividad) = MONTH(CURDATE()) AND YEAR(A.fecha_actividad) = YEAR(CURDATE()) AND A.user_id = '.$user->id;
            $gastos_cumplimiento = DB::select($query);
            $query = 'SELECT '.constructor_consulta('alimentacion').' + '.constructor_consulta('transporte_interno').' + '.constructor_consulta('transporte_intermunicipal').' + '.constructor_consulta('tiquete_aereo').' + '.constructor_consulta('papeleria').' + '.constructor_consulta('invitacion_cliente').' + '.constructor_consulta('alquiler_vehiculo').' + '.constructor_consulta('gasolina_pasaje').' + '.constructor_consulta('hotel').' + '.constructor_consulta('otros').' AS total FROM presupuestos WHERE mes = MONTH(CURDATE()) AND anio = YEAR(CURDATE()) AND user_id = '.$user->id;
            $presupuesto = DB::select($query);
            if($presupuesto[0]->total > 0){
                $c_p_p_m_a =  $gastos_cumplimiento[0]->total / $presupuesto[0]->total *100;
                $c_p_p_m_a = number_format($c_p_p_m_a, 2, ',', ' ');
            }else{
                $c_p_p_m_a = 0;
            }
            /*Fin Cumplimiento Parcial Presupuesto Mes Actual*/

            /*Cumplimiento Presupuesto Acumulado (con Ajuste) del año actual*/
            $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_internos, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transportes_intermunicipales, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasajes, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM actividades WHERE user_id = '.$user->id.' AND fecha_actividad BETWEEN "'.date('Y-01-01').'" AND "'.date('Y-m-d').'" AND CONVERT(MONTH(fecha_actividad),UNSIGNED INTEGER) NOT IN (SELECT DISTINCT(mes) AS mes FROM ajuste_gastos WHERE user_id = '.$user->id.' AND anio LIKE "'.date('Y').'")';
            $gastos_cumplimiento = DB::select($query);

            $query = 'SELECT SUM(CONVERT(valor_alimentacion,UNSIGNED INTEGER) + CONVERT(valor_transporte_interno,UNSIGNED INTEGER) + CONVERT(valor_transporte_intermunicipal,UNSIGNED INTEGER) + CONVERT(valor_tiquete_aereo,UNSIGNED INTEGER) + CONVERT(valor_papeleria,UNSIGNED INTEGER) + CONVERT(valor_invitacion_cliente,UNSIGNED INTEGER) + CONVERT(valor_alquiler_vehiculo,UNSIGNED INTEGER) + CONVERT(valor_gasolina_pasaje,UNSIGNED INTEGER) + CONVERT(valor_hotel,UNSIGNED INTEGER) + CONVERT(valor_otros,UNSIGNED INTEGER) + CONVERT(valor_salariopropio,UNSIGNED INTEGER) + CONVERT(valor_salariotercero,UNSIGNED INTEGER)) AS total  FROM ajuste_gastos WHERE user_id = '.$user->id.' AND anio LIKE "'.date('Y').'" AND CONVERT(mes,UNSIGNED INTEGER) <= '.intval(date('m')).' AND tipo LIKE "valorreal"';
            $valor_real_cumplimiento = DB::select($query);

            $query = 'SELECT (COALESCE(SUM(CONVERT(REPLACE(REPLACE(alimentacion, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_interno, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(transporte_intermunicipal, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(tiquete_aereo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(papeleria, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(invitacion_cliente, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(alquiler_vehiculo, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(gasolina_pasaje, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(hotel, ".", ""), ",", ""),UNSIGNED INTEGER)),0) + COALESCE(SUM(CONVERT(REPLACE(REPLACE(otros, ".", ""), ",", ""),UNSIGNED INTEGER)),0)) AS total FROM presupuestos WHERE user_id = '.$user->id.' AND CONVERT(mes,UNSIGNED INTEGER) <= '.intval(date('m')).' AND anio LIKE "'.date('Y').'"';
            $presupuesto = DB::select($query);

            $c_p_a_a_a = 0;
            if($presupuesto[0]->total > 0){
                $c_p_a_a_a = (($gastos_cumplimiento[0]->total + $valor_real_cumplimiento[0]->total)/$presupuesto[0]->total)*100;
                $c_p_a_a_a = number_format($c_p_a_a_a, 2, '.', ' ');
            }
            /*Fin Cumplimiento Presupuesto Acumulado (con Ajuste) del año actual*/
             ?>
<tr>
    <td colspan="2" class="td-85">
        <div class="divimagen-2" style="background-image: url('/images/file/clientes/<?=$user->foto?>');"><span class="blue nombre-avatar-2"><?=$nombres[0]?></span></div>
    </td>
    <td class="td-marron">
        <label class="linea3-item2"><?=$c_p_m_a?> %</label>
    </td>
    <td class="td-marron">
        <label class="linea3-item2"><?=$c_p_p_m_a?>%</label>
    </td>
    <td class="td-marron">
        <label class="linea3-item2"><?=$c_p_a_a_a?>%</label>
    </td>
</tr>
            <?php
        }
            $data['modal_cumplimiento_presupuestal'] = ob_get_contents();
            ob_end_clean();

        /*Fin Cumplimiento Presupuestal*/
        return view("dashboard.dashboardprincipal",["data"=>$data,"oportunidades"=>$oportunidades,"metas"=>$metas,"participaciones"=>$participaciones,"usuarios"=>$usuarios,"empresas"=>$empresas,"clientes"=>$clientes,"ciclos"=>$ciclos,"actividades"=>$actividades,"presupuestos"=>$presupuestos,"gastos"=>$gastos, 'total_negocio_cerrado' => $total_negocio_cerrado, 'dias' => $dias, 'metas1' => $metas1, 'dolar' => $dolar, 'euro' => $euro, 'usuario1' => $usuario1, 'total_oportunidades_identificadas' => $total_oportunidades_identificadas, 'donas' => $donas, 'porcentaje_gastos' => $porcentaje_gastos, 'gasto_x_usuario' => $gasto_x_usuario, 'g' => $g, 'g_x_u' => $g_x_u, 'numero_tipo_actividad' => $numero_tipo_actividad, 'tipo_actividades' => $tipo_actividades, 'total_horas_actividad' => $total_horas_actividad, 'gestion' => $gestion, 'gestion1' => $gestion1, 'gestion_usuario' => $gestion_usuario, 'total_equipos_vendidos' => $total_equipos_vendidos, 'total_pais_vendidos' => $total_pais_vendidos, 'total_equipos_proyectados' => $total_equipos_proyectados, 'total_pais_proyectados' => $total_pais_proyectados, 'presupuesto_cumplimiento_presupuestal'=>$presupuesto_cumplimiento_presupuestal]);
      }else{
        return view("oportunidades.nopermiso");
      }
    }



    public function llenado_presupuesto(){
        $colors= array(0 => '#1c4597', 1 => '#0079f4', 2 => '#00d6fb', 3 => '#5ac3e1', 4 => '#97f5fd', 5 => '#1c4597', 6 => '#0079f4', 7 => '#00d6fb', 8 => '#5ac3e1', 9 => '#97f5fd');
        $usuarios=User::all();
        /*Cumplimiento Presupuesto Mensual */
        $meses_cumplidos=0;
        $meses_con_presupuesto=0;
        for($i=0;$i<=11;$i++){
            $mes_buscar=$i+1;
            $ano_actual=date('Y');
            if($mes_buscar<10){
                $mes_consulta='0'.$mes_buscar;
            }else{
                $mes_consulta=$mes_buscar;
            }
            $mes_con_presupuesto=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `anio` = "'.date("Y").'"');
            if(count($mes_con_presupuesto)>0){
                $meses_con_presupuesto++;

                $presupuesto_mes=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'"  AND `updated_at` LIKE "'.$ano_actual.'-'.$mes_consulta.'%"');
                if(count($presupuesto_mes)==0){
                    $meses_cumplidos++;
                }
            }
            foreach($usuarios as $usu){
                if(($usu->cargo)=='Ejecutivo Comercial' || ($usu->cargo)=='Partner Solution'){
                    $mes_con_presupuesto=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `user_id`="'.$usu->id.'"');
                    if(count($mes_con_presupuesto)>0){
                        if(!isset($meses_con_presupuesto1[$usu->id])){
                            $meses_con_presupuesto1[$usu->id]=1;
                        }else{
                            $meses_con_presupuesto1[$usu->id]++;
                        }
                        $presupuesto_mes=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `user_id`="'.$usu->id.'" AND `updated_at` LIKE "'.$ano_actual.'-'.$mes_consulta.'%"');
                        if(count($presupuesto_mes)==0){
                            if(!isset($meses_cumplidos1[$usu->id])){
                                $meses_cumplidos1[$usu->id]=1;
                            }else{
                                $meses_cumplidos1[$usu->id]++;
                            }
                        }
                    }
                }
            }
        }

    if($meses_con_presupuesto == 0){
    	$porcentajecumplimientopresupuesto = number_format(0, 1, '.', ' ');
    }else{
    	$porcentajecumplimientopresupuesto=number_format(($meses_cumplidos/$meses_con_presupuesto*100), 1, '.', ' ');
    }
        $mayor_cumplimiento2=0;
        foreach ($usuarios as $usu) {
            if(($usu->cargo)=='Ejecutivo Comercial' || ($usu->cargo)=='Partner Solution'){
                $c=0;
                $nombres=explode(' ', $usu->nombres);
                $porcentajecumplimientopresupuesto1[$usu->id]['nombre']=$nombres[0];
                $porcentajecumplimientopresupuesto1[$usu->id]['color']=$colors[2];
                $porcentajecumplimientopresupuesto1[$usu->id]['foto'] = url("/").'/images/file/clientes/'.$usu->foto;
                if(isset($meses_con_presupuesto1[$usu->id]) && ($meses_con_presupuesto1[$usu->id])>0){
                    if(isset($meses_cumplidos1[$usu->id])){
                        $porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']=number_format(($meses_cumplidos1[$usu->id]/$meses_con_presupuesto1[$usu->id]*100), 1, '.', ' ');
                    }else{
                        $porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']=0;
                    }
                }else{
                    $porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']=0;
                }

                if($mayor_cumplimiento2<$porcentajecumplimientopresupuesto1[$usu->id]['presupuesto']){
                    $mayor_cumplimiento2=$porcentajecumplimientopresupuesto1[$usu->id]['presupuesto'];
                }
                $c++;

            }
        }
        /*Fin Cumplimiento Presupuesto Mensual*/
        return view("graficas.cumplimientopreupuesto",['porcentajecumplimientopresupuesto1' => $porcentajecumplimientopresupuesto1, 'mayor_cumplimiento2' => $mayor_cumplimiento2]);
    }

    public function llenado_semanal(){
        $usuarios=User::all();
        /*Cumplimiento Plan Semanal */
        $colors= array(0 => '#1c4597', 1 => '#0079f4', 2 => '#00d6fb', 3 => '#5ac3e1', 4 => '#97f5fd', 5 => '#1c4597', 6 => '#0079f4', 7 => '#00d6fb', 8 => '#5ac3e1', 9 => '#97f5fd');
        $c=0;
        $mayor_cumplimiento1=0;
        foreach($usuarios as $usuario){
            if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
                $nombres=explode(" ",$usuario->nombres);
                $plansemanalcumplido=DB::select('SELECT `id` FROM `plan_trabajo_descripcions` WHERE `fuera_tiempo` = "false" AND `user_id` = "'.$usuario->id.'" AND YEAR(`fecha`) = "'.date("Y").'"');
                $plansemanaltotal=DB::select('SELECT `id` FROM `plan_trabajo_descripcions` WHERE `user_id` = "'.$usuario->id.'"');
                $todas_act_usuario=DB::select('SELECT `id` FROM `actividades` WHERE `user_id` = "'.$usuario->id.'"');
                if(count($todas_act_usuario)==0){
                    $cumplimiento_x_usr1[$usuario->id]['semanal']=0;
                }else if(count($plansemanaltotal) > 0){
                    $cumplimiento_x_usr1[$usuario->id]['semanal']=number_format((count($plansemanalcumplido)/count($plansemanaltotal)*100), 1, '.', ' ');
                }else{
                    $cumplimiento_x_usr1[$usuario->id]['semanal']=0;
                }

                $cumplimiento_x_usr1[$usuario->id]['nombre']=$nombres[0];
                $cumplimiento_x_usr1[$usuario->id]['color']=$colors[1];
                $cumplimiento_x_usr1[$usuario->id]['foto']=url("/").'/images/file/clientes/'.$usuario->foto;
                $c++;

                if($mayor_cumplimiento1<$cumplimiento_x_usr1[$usuario->id]['semanal']){
                    $mayor_cumplimiento1=$cumplimiento_x_usr1[$usuario->id]['semanal'];
                }
            }
        }
        /*Fin Cumplimiento Plan Semanal*/
        return view("graficas.cumplimientosemanal",['cumplimiento_x_usr1' => $cumplimiento_x_usr1, 'mayor_cumplimiento1' => $mayor_cumplimiento1]);
    }

    public function llenado_bitacora(){

        $actividades=DB::select('SELECT `id` FROM `actividades` WHERE YEAR(`fecha_actividad`) = "'.date('Y').'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');

        $usuarios=User::all();
        /*Cumplimiento Llenado */
        $act_llenado=DB::select('SELECT `id` FROM `actividades` WHERE `fuera_tiempo` = "false" AND YEAR(`fecha_actividad`) = "'.date('Y').'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');

    if((count($actividades)*100)==0){
    	$cumplimiento['bitacora']=number_format(0, 1, '.', ' ');
    }else{
    	$cumplimiento['bitacora']=number_format((count($act_llenado)/count($actividades)*100), 1, '.', ' ');
    }
        $colors= array(0 => '#1c4597', 1 => '#0079f4', 2 => '#00d6fb', 3 => '#5ac3e1', 4 => '#97f5fd', 5 => '#1c4597', 6 => '#0079f4', 7 => '#00d6fb', 8 => '#5ac3e1', 9 => '#97f5fd');
        $c=0;
        $mayor_cumplimiento=0;
        foreach($usuarios as $usuario){
            if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
                $nombres=explode(" ",$usuario->nombres);
                $act_llenado_usuario=DB::select('SELECT `id` FROM `actividades` WHERE `fuera_tiempo` = "false" AND `user_id` = "'.$usuario->id.'" AND YEAR(`fecha_actividad`) = "'.date('Y').'"');
                $todas_act_usuario=DB::select('SELECT `id` FROM `actividades` WHERE `user_id` = "'.$usuario->id.'"');
                if(count($todas_act_usuario)==0){
                    $cumplimiento_x_usr[$usuario->id]['bitacora']=0;
                }else{
                    $cumplimiento_x_usr[$usuario->id]['bitacora']=number_format((count($act_llenado_usuario)/count($todas_act_usuario)*100), 1, '.', ' ');
                }

                $cumplimiento_x_usr[$usuario->id]['nombre']=$nombres[0];
                $cumplimiento_x_usr[$usuario->id]['color']=$colors[0];
                $cumplimiento_x_usr[$usuario->id]['foto']=url("/").'/images/file/clientes/'.$usuario->foto;
                $c++;

                if($mayor_cumplimiento<$cumplimiento_x_usr[$usuario->id]['bitacora']){
                    $mayor_cumplimiento=$cumplimiento_x_usr[$usuario->id]['bitacora'];
                }
            }
        }
        /*Fin Cumplimiento Llenado */
       return view("graficas.cumplimientobitacora",['cumplimiento' => $cumplimiento, 'cumplimiento_x_usr' => $cumplimiento_x_usr, 'mayor_cumplimiento' => $mayor_cumplimiento]);
    }

    public function dashboardconsultacumplimiento(){
        $actividades=Actividades::all();
        /*Cumplimiento Llenado */
        $act_llenado=DB::select('SELECT `id` FROM `actividades` WHERE `fuera_tiempo` = "false"');
        $cumplimiento['bitacora']=number_format((count($act_llenado)/count($actividades)*100), 1, '.', ' ');

        /*Fin Cumplimiento Llenado */

        /*Cumplimiento Plan Semanal */
        $plansemanalcumplido=DB::select('SELECT `id` FROM `plan_trabajo_descripcions` WHERE `fuera_tiempo` = "false"');
        $plansemanaltotal=DB::select('SELECT `id` FROM `plan_trabajo_descripcions`');
        $porcentajecumplimientosemanal=number_format(((count($plansemanalcumplido))/(count($plansemanaltotal))*100), 1, '.', ' ');

        /*Fin Cumplimiento Plan Semanal*/

        /*Cumplimiento Presupuesto Mensual */
        $meses_cumplidos=0;
        $meses_con_presupuesto=0;
        for($i=0;$i<=11;$i++){
            $mes_buscar=$i+1;
            $ano_actual=date('Y');
            if($mes_buscar<10){
                $mes_consulta='0'.$mes_buscar;
            }else{
                $mes_consulta=$mes_buscar;
            }
            $mes_con_presupuesto=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
            if(count($mes_con_presupuesto)>0){
                $meses_con_presupuesto++;

                $presupuesto_mes=DB::select('SELECT * FROM `presupuestos` WHERE `mes` = "'.$mes_buscar.'"  AND `updated_at` LIKE "'.$ano_actual.'-'.$mes_consulta.'%" AND `user_id` IN (SELECT `id` FROM `users` WHERE `cargo` = "Ejecutivo Comercial" OR `cargo` = "Partner Solution")');
                if(count($presupuesto_mes)==0){
                    $meses_cumplidos++;
                }
            }
        }

        $porcentajecumplimientopresupuesto=number_format(($meses_cumplidos/$meses_con_presupuesto*100), 1, '.', ' ');

        /*Fin Cumplimiento Presupuesto Mensual*/

        return \Response::json(['cumplimiento' => $cumplimiento, 'porcentajecumplimientosemanal' => $porcentajecumplimientosemanal, 'porcentajecumplimientopresupuesto' => $porcentajecumplimientopresupuesto]);
    }

    public function graficasventas(){

        $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
        foreach($vendidas_oportunidades as $v_o){
            /*Millones X mes*/
            $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
            if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));

                $valor_total=round(((int)$cantidad)/1000000);
                $valor_total=(int)$valor_total;

                if(isset($meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))])){
                    $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]+=$valor_total;
                }else{
                    $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]=$valor_total;
                }

            }

            /*Fin Millones X mes*/
        }
        if(isset($meses_ventas)){
            return view("graficas.ventas",["meses_ventas"=>$meses_ventas]);
        }else{
            return view("graficas.ventas");
        }
    }

    public function graficasventas1(){

        $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
        foreach($vendidas_oportunidades as $v_o){
            /*paises*/
            $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
            $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
            $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
            if(isset($pais_vendidas_oportunidades[0]->pais) && (date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));

                $valor_total=round(((int)$cantidad)/1000000);
                $valor_total=(int)$valor_total;

                if(isset($pais_vendida[$pais_vendidas_oportunidades[0]->pais])){
                    $pais_vendida[$pais_vendidas_oportunidades[0]->pais]+=$valor_total;
                }else{
                   $pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$valor_total;
                   $nombre_pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$pais_vendidas_oportunidades[0]->pais;
                }
            }
            /*Fin paises*/
        }
    	if(isset($pais_vendida)){
        	return view("graficas.ventas1",["pais_vendida"=>$pais_vendida,"nombre_pais_vendida"=>$nombre_pais_vendida]);
        }else{
			return view("graficas.ventas1");
        }
    }

    public function graficasventas2(){

        $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');

        foreach($vendidas_oportunidades as $v_oo){
            $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_oo->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
            if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                /*Equipos vendidos*/
                $productos_vendidas_oportunidades=DB::select('SELECT * FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$v_oo->oportunidad_id.'" AND `deleted_at` IS NULL');

                foreach ($productos_vendidas_oportunidades as $pvo) {

                    if(isset($productos_referencia[$pvo->referencia])){
                        $productos_referencia[$pvo->referencia]+=$pvo->cantidad;
                    }else{
                        $productos_referencia[$pvo->referencia]=$pvo->cantidad;
                        $nombre_productos_referencia[$pvo->referencia]=$pvo->referencia;
                        $productos_buscar=DB::select('SELECT `name` FROM `productos` WHERE `id` = "'.$pvo->producto.'" ');
                        $referencia_buscar=DB::select('SELECT `referencia` FROM `producto_referencias` WHERE `id` = "'.$pvo->referencia.'" ');
                        $nombrecompleto_productos_referencia[$pvo->referencia]=$productos_buscar[0]->name.' - '.$referencia_buscar[0]->referencia;
                    }
                }
                /*Fin Equipos vendidos*/
            }
        }
        if(isset($productos_referencia)){
       		arsort($productos_referencia);
        	return view("graficas.ventas2",["productos_referencia"=>$productos_referencia,"nombre_productos_referencia"=>$nombre_productos_referencia,'nombrecompleto_productos_referencia'=>$nombrecompleto_productos_referencia]);
        }else{
        	return view("graficas.ventas2");
        }

    }

    public function graficasproyectadas(){

        $todas_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

        foreach($todas_oportunidades as $t_o){
            $oportunidad_buscar=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "ciclo_venta" ORDER BY `id` DESC');
            if(((int)$oportunidad_buscar[0]->value)>=10 && ((int)$oportunidad_buscar[0]->value)<90){
                $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                    $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                    if(isset($valor[0]->value)){
                        $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));
                    }else{
                        $cantidad = 0;
                    }


                    $valor_total=round(((int)$cantidad)/1000000);
                    $valor_total=(int)$valor_total;

                    if(isset($meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))])){
                        $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]+=$valor_total;
                    }else{
                        $meses_ventas[((int)(date("m", strtotime($fechacierre[0]->value))))]=$valor_total;
                    }

                }

            }
        }
        return view("graficas.proyectadas",["meses_ventas"=>$meses_ventas]);
    }

    public function graficasproyectadas1(){
        $todas_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

        foreach($todas_oportunidades as $t_o){
            $oportunidad_buscar=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "ciclo_venta" ORDER BY `id` DESC');
            if(((int)$oportunidad_buscar[0]->value)>=10 && ((int)$oportunidad_buscar[0]->value)<90){
                /*paises*/
                $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
                if(isset($pais_vendidas_oportunidades[0]->pais) && (date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                    $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                    if(isset($valor[0]->value)){
                        $cantidad=str_replace(".", "", str_replace(",", "", $valor[0]->value));
                    }else{
                        $cantidad=0;
                    }

                    $valor_total=round(((int)$cantidad)/1000000);
                    $valor_total=(int)$valor_total;

                    if(isset($pais_vendida[$pais_vendidas_oportunidades[0]->pais])){
                        $pais_vendida[$pais_vendidas_oportunidades[0]->pais]+=$valor_total;
                    }else{
                       $pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$valor_total;
                       $nombre_pais_vendida[$pais_vendidas_oportunidades[0]->pais]=$pais_vendidas_oportunidades[0]->pais;
                    }

                }
                /*Fin paises*/
            }
        }
        return view("graficas.proyectadas1",["pais_vendida"=>$pais_vendida,"nombre_pais_vendida"=>$nombre_pais_vendida]);
    }

    public function graficasproyectadas2(){
        $todas_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

        foreach($todas_oportunidades as $t_o){
            $oportunidad_buscar=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `key` = "ciclo_venta" ORDER BY `id` DESC');
            if(((int)$oportunidad_buscar[0]->value)>=10 && ((int)$oportunidad_buscar[0]->value)<90){
                /*paises*/
                $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$t_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');

                if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                    /*Equipos proyectada*/
                    $productos_proyectadas_oportunidades=DB::select('SELECT * FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$t_o->oportunidad_id.'" AND `deleted_at` IS NULL');

                    foreach ($productos_proyectadas_oportunidades as $ppo) {

                        if(isset($productos_referencia[$ppo->referencia])){
                            $productos_referencia[$ppo->referencia]+=$ppo->cantidad;
                        }else{
                            $productos_referencia[$ppo->referencia]=$ppo->cantidad;
                            $nombre_productos_referencia[$ppo->referencia]=$ppo->referencia;
                            $productos_buscar=DB::select('SELECT `name` FROM `productos` WHERE `id` = "'.$ppo->producto.'" ');
                            $referencia_buscar=DB::select('SELECT `referencia` FROM `producto_referencias` WHERE `id` = "'.$ppo->referencia.'" ');
                            if(!isset($productos_buscar[0]->name)){
                                $nombreproducto='Producto no encontrado';
                            }else{
                                $nombreproducto=$productos_buscar[0]->name;
                            }
                            if(!isset($referencia_buscar[0]->referencia)){
                                $nombrereferencia='Referencia no encontrado';
                            }else{
                                $nombrereferencia=$referencia_buscar[0]->referencia;
                            }
                            $nombrecompleto_productos_referencia[$ppo->referencia]=$nombreproducto.' - '.$nombrereferencia;
                        }
                    }
                    /*Fin Equipos proyectada*/
                }
            }
        }
        arsort($productos_referencia);
        return view("graficas.proyectadas2",["productos_referencia"=>$productos_referencia,"nombre_productos_referencia"=>$nombre_productos_referencia,'nombrecompleto_productos_referencia'=>$nombrecompleto_productos_referencia]);
    }

    public function action(Request $request){
        $accion = $request->action;
        $datos = $request->datos;
        switch($accion){
            case 0:
                /*Guardar tablero*/
                foreach($datos as $d){
                    $dato = new ComparacionAno;
                    foreach($d as $index=>$value){
                        $dato->$index=$value;
                    }
                    $dato->save();
                }
                $data=maquinasvendidas(0);
                foreach($data['productos_referencia'] as $clave=>$valor){
                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos";
                    $dato->campo = "equipo";
                    $dato->valor_campo = $data['nombrecompleto_productos_referencia'][$clave];
                    $dato->save();

                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos";
                    $dato->campo = "cantidad";
                    $dato->valor_campo = $data['productos_referencia'][$clave];
                    $dato->save();

                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos";
                    $dato->campo = "monto";
                    $dato->valor_campo = $data['valor'][$clave];
                    $dato->save();
                }
                $data1=maquinasvendidas(1);
                foreach($data1['nombre_pais_vendida'] as $nombre){
                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos por pais";
                    $dato->campo = "pais";
                    $dato->valor_campo = $nombre;
                    $dato->save();

                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos por pais";
                    $dato->campo = "cantidad";
                    $dato->valor_campo = $data1['cant_equipos'][$nombre];
                    $dato->save();

                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos por pais";
                    $dato->campo = "monto";
                    $dato->valor_campo = $data1['pais_vendida'][$nombre];
                    $dato->save();
                }
                $data2=maquinasvendidas(2);
                $sum=0;
                $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                for($i=1;$i<=12;$i++){
                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos por meses";
                    $dato->campo = "meses";
                    $dato->valor_campo = $meses[$i];
                    $dato->save();

                    if(isset($data2['cant_equipos'][$i])){ $cantidad_equipos=$data2['cant_equipos'][$i]; }else{ $cantidad_equipos=0; }
                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos por meses";
                    $dato->campo = "cantidad";
                    $dato->valor_campo = $cantidad_equipos;
                    $dato->save();

                    if(isset($meses_ventas[$i])){ $sum+=$data2['meses_ventas'][$i]; }
                    $dato = new ComparacionAno;
                    $dato->tipo_informacion = "Equipos vendidos por meses";
                    $dato->campo = "monto";
                    $dato->valor_campo = $sum;
                    $dato->save();
                }
                $data['msj'] = "Guardado correctamente!";
                break;
        }
        return \Response::json(['data' => $data]);
    }

}

function constructor_consulta($columna){
    $consulta = 'IF(ISNULL(SUM(CONVERT(REPLACE(REPLACE('.$columna.', ".", ""), ",", ""),UNSIGNED INTEGER))), 0, SUM(CONVERT(REPLACE(REPLACE('.$columna.', ".", ""), ",", ""),UNSIGNED INTEGER)))';
    return $consulta;
}

function moneda($cantidad,$moneda_origen,$euro,$dolar)
    {   try{
            /*$get = file_get_contents("https://www.google.com/finance/converter?a=$cantidad&from=$moneda_origen&to=COP");
            $get = explode("<span class=bld>",$get);
            $get = explode("</span>",$get[1]);
            return preg_replace("/[^0-9\.]/", null, $get[0]);*/

            if($moneda_origen=='EUR'){
                $valor=$cantidad*$euro;
                return($valor);
            }elseif($moneda_origen=='USD'){
                $valor=$cantidad*$dolar;
                return($valor);
            }
        }catch(Exception $e){

        }
    }
function burbuja($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["porcentaje"]>$array[$j+1]["porcentaje"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }

    return $array;
}

function maquinasvendidas($accion){
    switch($accion){
            case 0:
                /*Consultar maquinas vendidas*/
                $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
                foreach($vendidas_oportunidades as $v_oo){
                    $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_oo->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        /*Equipos vendidos*/
                        $productos_vendidas_oportunidades=DB::select('SELECT * FROM `oportunidad_productos` WHERE `oportunidad_id` = "'.$v_oo->oportunidad_id.'" AND `deleted_at` IS NULL');

                        foreach ($productos_vendidas_oportunidades as $pvo) {
                            $tasa_cambio=DB::select('SELECT `value` FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$pvo->oportunidad_id.'" AND `key` = "tasa_cambio" ORDER BY `created_at` DESC');
                            $pvo->valor = entero($pvo->valor);
                            $tasa_cambio[0]->value = entero($tasa_cambio[0]->value);
                            if(isset($data['productos_referencia'][$pvo->referencia])){
                                $data['productos_referencia'][$pvo->referencia]+=$pvo->cantidad;
                                $data['valor'][$pvo->referencia]+=$pvo->cantidad*$pvo->valor*(intval($tasa_cambio[0]->value));
                            }else{
                                $data['valor'][$pvo->referencia]=$pvo->cantidad*$pvo->valor*(intval($tasa_cambio[0]->value));
                                $data['productos_referencia'][$pvo->referencia]=$pvo->cantidad;
                                $data['nombre_productos_referencia'][$pvo->referencia]=$pvo->referencia;
                                $productos_buscar=DB::select('SELECT `name` FROM `productos` WHERE `id` = "'.$pvo->producto.'" ');
                                $referencia_buscar=DB::select('SELECT `referencia` FROM `producto_referencias` WHERE `id` = "'.$pvo->referencia.'" ');
                                $data['nombrecompleto_productos_referencia'][$pvo->referencia]=$productos_buscar[0]->name.' - '.$referencia_buscar[0]->referencia;
                            }
                        }
                        /*Fin Equipos vendidos*/
                    }
                }
                arsort($data['productos_referencia']);
                break;

            case 1:
                $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
                foreach($vendidas_oportunidades as $v_o){
                    /*paises*/
                    $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    $sede_vendidas_oportunidades=DB::select('SELECT * FROM `historial_oportunidades` WHERE `oportunidad_id` = "'.$v_o->oportunidad_id.'" AND `key` = "sede" ORDER BY `id` DESC');
                    $pais_vendidas_oportunidades=DB::select('SELECT `pais` FROM `empresa_sedes` WHERE `id` = "'.$sede_vendidas_oportunidades[0]->value.'"');
                    if(isset($pais_vendidas_oportunidades[0]->pais) && (date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                        $valor_total=str_replace(".", "", str_replace(",", "", $valor[0]->value));
                        $valor_total=(int)$valor_total;
                        $cantidad_equipos=DB::select('SELECT SUM(`cantidad`) AS Cantidad FROM oportunidad_productos WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `deleted_at` IS NULL');
                        if(isset($data['pais_vendida'][$pais_vendidas_oportunidades[0]->pais])){
                            $data['cant_equipos'][$pais_vendidas_oportunidades[0]->pais]+=$cantidad_equipos[0]->Cantidad;
                            $data['pais_vendida'][$pais_vendidas_oportunidades[0]->pais]+=$valor_total;
                        }else{
                           $data['cant_equipos'][$pais_vendidas_oportunidades[0]->pais]=$cantidad_equipos[0]->Cantidad;
                           $data['pais_vendida'][$pais_vendidas_oportunidades[0]->pais]=$valor_total;
                           $data['nombre_pais_vendida'][$pais_vendidas_oportunidades[0]->pais]=$pais_vendidas_oportunidades[0]->pais;
                        }
                    }
                    /*Fin paises*/
                }
                break;
            case 2:
                $vendidas_oportunidades=DB::select('SELECT DISTINCT(`oportunidad_id`) FROM `historial_oportunidades` WHERE `key` = "ciclo_venta" AND `value` = "90"');
                foreach($vendidas_oportunidades as $v_o){
                    /*Millones X mes*/
                    $fechacierre=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
                    if((date("Y", strtotime($fechacierre[0]->value)))==(date('Y'))){
                        $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
                        $valor_total=str_replace(".", "", str_replace(",", "", $valor[0]->value));
                        $valor_total=(int)$valor_total;

                        $cantidad_equipos=DB::select('SELECT SUM(`cantidad`) AS Cantidad FROM oportunidad_productos WHERE `oportunidad_id` = '.$v_o->oportunidad_id.' AND `deleted_at` IS NULL');
                        if(isset($data['meses_ventas'][((int)(date("m", strtotime($fechacierre[0]->value))))])){
                            $data['meses_ventas'][((int)(date("m", strtotime($fechacierre[0]->value))))]+=$valor_total;
                            $data['cant_equipos'][((int)(date("m", strtotime($fechacierre[0]->value))))]+=$cantidad_equipos[0]->Cantidad;
                        }else{
                            $data['meses_ventas'][((int)(date("m", strtotime($fechacierre[0]->value))))]=$valor_total;
                            $data['cant_equipos'][((int)(date("m", strtotime($fechacierre[0]->value))))]=$cantidad_equipos[0]->Cantidad;
                        }

                    }
                    /*Fin Millones X mes*/
                }
                break;
    }


        return $data;
}

function entero($numero){
    $numero = intval(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $numero))));
    return $numero;
}

