<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\stdClass;
use App\Pendiente;
use App\PendientesHistoriales;
use App\Oportunidades;
use App\Tipo_actividades;
use App\User;
use App\SubtipoActividad;
use App\Empresa;
use Auth;

class PendientesController extends Controller
{
  	public function jsonVigentes(){
        /*Permiso para Ver Información*/
        $data['permiso_informacion']="Ver propio";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Información de las listas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Ver todas"){
                $data['permiso_informacion']="Ver todas";
              }
            }
          }
        }
        /*FIN Permiso para Ver Información*/
        $datos = [];
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        if($data['permiso_informacion'] == "Ver todas"){
            $modelos = Pendiente::where('estado', 'ejecutando')->where('fecha_ejecucion', '>=', $fecha)->orderBy('fecha_ejecucion', 'ASC')->latest()->get();
        }else{
            $modelos = Pendiente::where('estado', 'ejecutando')->where('responsable', Auth::user()->id)->where('fecha_ejecucion', '>=', $fecha)->orderBy('fecha_ejecucion', 'ASC')->latest()->get();
        }

        foreach($modelos as $modelo){
            if(!empty($modelo->oportunidad_id)){
                $oportunidad = Oportunidades::find($modelo->oportunidad_id);
                $modelo->oportunidad = $oportunidad->empresa.", ".$oportunidad->pais;
            }elseif (!empty($modelo->empresa_id)) {
              $oportunidad = Empresa::find($modelo->empresa_id);
              $modelo->oportunidad = $oportunidad->nombre;
            }else {
              $modelo->oportunidad ="";
            }
            $subtipo = SubtipoActividad::find($modelo->subtipo);
            $modelo->subtipo = isset($subtipo->concepto) ? $subtipo->concepto : "";
            $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
            $modelo->aplazadas = $aplazadas[0]->total;
            $usuario = User::find($modelo->responsable);
            $modelo->nombreuser = $usuario->name;
            $datos[]=$modelo;
        }

        if($data['permiso_informacion'] == "Ver todas"){
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado'");
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado'");
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado'");
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= '".$fecha."'");
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < '".$fecha."'");
        }else{
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
        }

        $cancelado = $cancelado[0]->total;
        $aprobado = $aprobado[0]->total;
        $terminado = $terminado[0]->total;
        $ejecutando = $ejecutando[0]->total;
        $vencidos = $vencidos[0]->total;

        return response()->json(['success' => true, 'modelos' => $datos, 'cancelado' => $cancelado, 'aprobado' => $aprobado, 'terminado' => $terminado, 'ejecutando' => $ejecutando, 'vencidos' => $vencidos]);
    }

    public function jsonVencidas(){
        /*Permiso para Ver Información*/
        $data['permiso_informacion']="Ver propio";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Información de las listas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Ver todas"){
                $data['permiso_informacion']="Ver todas";
              }
            }
          }
        }
        /*FIN Permiso para Ver Información*/
        $datos = [];
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        if($data['permiso_informacion'] == "Ver todas"){
            $modelos = Pendiente::where('estado', 'ejecutando')->where('fecha_ejecucion', '<', $fecha)->orderBy('fecha_ejecucion', 'ASC')->latest()->get();
        }else{
            $modelos = Pendiente::where('estado', 'ejecutando')->where('responsable', Auth::user()->id)->where('fecha_ejecucion', '<', $fecha)->orderBy('fecha_ejecucion', 'ASC')->latest()->get();
        }
        foreach($modelos as $modelo){
            if(!empty($modelo->oportunidad_id)){
                $oportunidad = Oportunidades::find($modelo->oportunidad_id);
                $modelo->oportunidad = $oportunidad->empresa.", ".$oportunidad->pais;
            }elseif (!empty($modelo->empresa_id)) {
              $oportunidad = Empresa::find($modelo->empresa_id);
              $modelo->oportunidad = $oportunidad->nombre;
            }else {
              $modelo->oportunidad ="";
            }
            $subtipo = SubtipoActividad::find($modelo->subtipo);
            $modelo->subtipo = isset($subtipo->concepto) ? $subtipo->concepto : "";
            $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
            $modelo->aplazadas = $aplazadas[0]->total;
            $usuario = User::find($modelo->responsable);
            $modelo->nombreuser = $usuario->name;
            $datos[]=$modelo;
        }

        if($data['permiso_informacion'] == "Ver todas"){
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado'");
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado'");
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado'");
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= '".$fecha."'");
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < '".$fecha."'");
        }else{
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
        }

        $cancelado = $cancelado[0]->total;
        $aprobado = $aprobado[0]->total;
        $terminado = $terminado[0]->total;
        $ejecutando = $ejecutando[0]->total;
        $vencidos = $vencidos[0]->total;

        return response()->json(['success' => true, 'modelos' => $datos, 'cancelado' => $cancelado, 'aprobado' => $aprobado, 'terminado' => $terminado, 'ejecutando' => $ejecutando, 'vencidos' => $vencidos]);
    }

    public function jsonPorAprobar(){
        /*Permiso para Ver Información*/
        $data['permiso_informacion']="Ver propio";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Información de las listas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Ver todas"){
                $data['permiso_informacion']="Ver todas";
              }
            }
          }
        }
        /*FIN Permiso para Ver Información*/
        $datos = [];
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        if($data['permiso_informacion'] == "Ver todas"){
            $modelos = Pendiente::where('estado', 'terminado')->orderBy('fecha_ejecucion', 'ASC')->latest()->get();
        }else{
            $modelos = Pendiente::where('estado', 'terminado')->where('responsable', Auth::user()->id)->orderBy('fecha_ejecucion', 'ASC')->latest()->get();
        }
        foreach($modelos as $modelo){
            $comentario_penhis = PendientesHistoriales::where('estado', 'terminado')->where('pendientes_id', $modelo->id)->select('comentario')->orderBy('id', 'desc')->latest()->first();
            $modelo->comentario = validarComentario($comentario_penhis);
            if(!empty($modelo->oportunidad_id)){
                $oportunidad = Oportunidades::find($modelo->oportunidad_id);
                $modelo->oportunidad = $oportunidad->empresa.", ".$oportunidad->pais;
            }elseif (!empty($modelo->empresa_id)) {
              $oportunidad = Empresa::find($modelo->empresa_id);
              $modelo->oportunidad = $oportunidad->nombre;
            }else {
              $modelo->oportunidad ="";
            }
            $subtipo = SubtipoActividad::find($modelo->subtipo);
            $modelo->subtipo = isset($subtipo->concepto) ? $subtipo->concepto : "";
            $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
            $modelo->aplazadas = $aplazadas[0]->total;
            $usuario = User::find($modelo->responsable);
            $modelo->nombreuser = $usuario->name;
            $datos[]=$modelo;
        }

        if($data['permiso_informacion'] == "Ver todas"){
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado'");
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado'");
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado'");
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= '".$fecha."'");
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < '".$fecha."'");
        }else{
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
        }

        $cancelado = $cancelado[0]->total;
        $aprobado = $aprobado[0]->total;
        $terminado = $terminado[0]->total;
        $ejecutando = $ejecutando[0]->total;
        $vencidos = $vencidos[0]->total;

        return response()->json(['success' => true, 'modelos' => $datos, 'cancelado' => $cancelado, 'aprobado' => $aprobado, 'terminado' => $terminado, 'ejecutando' => $ejecutando, 'vencidos' => $vencidos]);
    }

    public function jsonFinalizadas(){
        /*Permiso para Ver Información*/
        $data['permiso_informacion']="Ver propio";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Información de las listas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Ver todas"){
                $data['permiso_informacion']="Ver todas";
              }
            }
          }
        }
        /*FIN Permiso para Ver Información*/
        $datos = [];
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        if($data['permiso_informacion'] == "Ver todas"){
            $modelos = Pendiente::where('estado', 'aprobado')->orderBy('fecha_ejecucion', 'ASC')->latest()->take(40)->get();
        }else{
            $modelos = Pendiente::where('estado', 'aprobado')->where('responsable', Auth::user()->id)->orderBy('fecha_ejecucion', 'ASC')->latest()->take(40)->get();
        }
        foreach($modelos as $modelo){
            $comentario_penhis = PendientesHistoriales::where('estado', 'aprobado')->where('pendientes_id', $modelo->id)->select('comentario')->orderBy('id', 'desc')->latest()->first();
            $modelo->comentario = validarComentario($comentario_penhis);
            if(!empty($modelo->oportunidad_id)){
                $oportunidad = Oportunidades::find($modelo->oportunidad_id);
                $modelo->oportunidad = $oportunidad->empresa.", ".$oportunidad->pais;
            }elseif (!empty($modelo->empresa_id)) {
              $oportunidad = Empresa::find($modelo->empresa_id);
              $modelo->oportunidad = $oportunidad->nombre;
            }else {
              $modelo->oportunidad ="";
            }
            $subtipo = SubtipoActividad::find($modelo->subtipo);
            $modelo->subtipo = isset($subtipo->concepto) ? $subtipo->concepto : "";
            $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
            $modelo->aplazadas = $aplazadas[0]->total;
            $usuario = User::find($modelo->responsable);
            $modelo->nombreuser = $usuario->name;
            $datos[]=$modelo;
        }
        if($data['permiso_informacion'] == "Ver todas"){
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado'");
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado'");
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado'");
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= '".$fecha."'");
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < '".$fecha."'");
        }else{
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
        }

        $cancelado = $cancelado[0]->total;
        $aprobado = $aprobado[0]->total;
        $terminado = $terminado[0]->total;
        $ejecutando = $ejecutando[0]->total;
        $vencidos = $vencidos[0]->total;

        return response()->json(['success' => true, 'modelos' => $datos, 'cancelado' => $cancelado, 'aprobado' => $aprobado, 'terminado' => $terminado, 'ejecutando' => $ejecutando, 'vencidos' => $vencidos]);
    }

    public function jsonCanceladas(){
        /*Permiso para Ver Información*/
        $data['permiso_informacion']="Ver propio";
        $modulo = 11;
        $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Información de las listas" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso3[0]->id)){
          $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo3[0]->id)){
            $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso3[0]->permiso)){
              if($permiso3[0]->permiso == "Ver todas"){
                $data['permiso_informacion']="Ver todas";
              }
            }
          }
        }
        /*FIN Permiso para Ver Información*/
        $datos = [];
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        if($data['permiso_informacion'] == "Ver todas"){
            $modelos = Pendiente::where('estado', 'cancelado')->orderBy('fecha_ejecucion', 'ASC')->latest()->take(40)->get();
        }else{
           $modelos = Pendiente::where('estado', 'cancelado')->where('responsable', Auth::user()->id)->orderBy('fecha_ejecucion', 'ASC')->latest()->take(40)->get();
        }

        foreach($modelos as $modelo){
            $comentario_penhis = PendientesHistoriales::where('estado', 'cancelado')->where('pendientes_id', $modelo->id)->select('comentario')->orderBy('id', 'desc')->latest()->first();
            $modelo->comentario = validarComentario($comentario_penhis);
            if(!empty($modelo->oportunidad_id)){
                $oportunidad = Oportunidades::find($modelo->oportunidad_id);
                $empresa_oportunidad = isset($oportunidad->empresa)?$oportunidad->empresa:'No hay empresa';
                $pais_oportunidad = isset($oportunidad->pais)?$oportunidad->pais:'No hay pais';
                $modelo->oportunidad = $empresa_oportunidad.", ".$pais_oportunidad;
            }elseif (!empty($modelo->empresa_id)) {
              $oportunidad = Empresa::find($modelo->empresa_id);
              $modelo->oportunidad = $oportunidad->nombre;
            }else {
              $modelo->oportunidad ="";
            }
            $subtipo = SubtipoActividad::find($modelo->subtipo);
            $modelo->subtipo = isset($subtipo->concepto) ? $subtipo->concepto : "";
            $aplazadas = DB::select("SELECT COUNT(id) AS total FROM pendientes_historiales WHERE pendientes_id=".$modelo->id);
            $modelo->aplazadas = $aplazadas[0]->total;
            $usuario = User::find($modelo->responsable);
            $modelo->nombreuser = $usuario->name;
            $datos[]=$modelo;
        }
        if($data['permiso_informacion'] == "Ver todas"){
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado'");
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado'");
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado'");
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= '".$fecha."'");
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < '".$fecha."'");
        }else{
            $terminado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'terminado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $cancelado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'cancelado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $aprobado = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'aprobado' AND `responsable` = :id", ['id' => Auth::user()->id]);
            $ejecutando = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` >= :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
            $vencidos = DB::select("SELECT COUNT(id) AS total FROM `pendientes` WHERE `estado` LIKE 'ejecutando' AND `fecha_ejecucion` < :fecha AND `responsable` = :id", ['id' => Auth::user()->id, 'fecha' => $fecha]);
        }

        $cancelado = $cancelado[0]->total;
        $aprobado = $aprobado[0]->total;
        $terminado = $terminado[0]->total;
        $ejecutando = $ejecutando[0]->total;
        $vencidos = $vencidos[0]->total;

        return response()->json(['success' => true, 'modelos' => $datos, 'cancelado' => $cancelado, 'aprobado' => $aprobado, 'terminado' => $terminado, 'ejecutando' => $ejecutando, 'vencidos' => $vencidos]);
    }

    public function jsonTiposActividad(){
        $modelos = Tipo_actividades::all();
        $codigoHTML = "";
        foreach($modelos as $modelo){
            if(!empty($modelo->orden)){
                $codigoHTML .='<option value="'.$modelo->concepto.'">'.$modelo->concepto.'</option>';
            }
        }
        $oportunidades = Oportunidades::where('estado', 'pendiente')->orWhere('estado', 'vendido')->get();
        $codigoHTMLOP = '<option label="Trabajos Generales ESSI" value="Trabajos Generales ESSI">';
        foreach ($oportunidades as $tag) {
            $codigoHTMLOP .= '<option label="'.$tag->empresa.'/'.$tag->ciudad.'" value="'.$tag->id.'">';
        }
        return response()->json(['success' => true, 'actividades' => $codigoHTML, 'oportunidades' => $codigoHTMLOP]);
    }

    public function aplazarPendiente(Request $request){
      $principal = $request->file('file');
      if ($principal) {
        $antenombreprincipal = uniqid();
        $nombrearchivo = $principal->getClientOriginalName();
        $extensionprincipal = $principal->getClientOriginalExtension();
        $nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
        \Storage::disk('pendientes')->put($nombreprincipal,  \File::get($principal));
      }

      $actividad = Pendiente::find($request->id);
      $fecha_original = $actividad->fecha_ejecucion;
      $actividad->fecha_ejecucion = $request->fecha;
      $actividad->estado  = "ejecutando";
      $actividad->save();

      $dato = new PendientesHistoriales;
      $dato->pendientes_id  = $request->id;
      $dato->estado  = "aplazado";
      $dato->comentario = $request->comentario;
      if (isset($nombreprincipal)) {
        $dato->archivo  = $nombreprincipal;
        $dato->nombre_archivo = $nombrearchivo;
      }
      $dato->fecha_original = $fecha_original;
      $dato->fecha_nueva  = $request->fecha;
      $dato->user_id = Auth::user()->id;
      $dato->save();

      return response()->json(['success' => true]);
    }

    public function cancelarPendiente(Request $request){
      $principal = $request->file('file');
      if ($principal) {
        $antenombreprincipal = uniqid();
        $nombrearchivo = $principal->getClientOriginalName();
        $extensionprincipal = $principal->getClientOriginalExtension();
        $nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
        \Storage::disk('pendientes')->put($nombreprincipal,  \File::get($principal));
      }

      $actividad = Pendiente::find($request->id);
      $actividad->estado  = "cancelado";
      $actividad->save();

      $dato = new PendientesHistoriales;
        $dato->pendientes_id  = $request->id;
        $dato->estado  = "cancelado";
        $dato->comentario = $request->comentario;
        if (isset($nombreprincipal)) {
        $dato->archivo  = $nombreprincipal;
        $dato->nombre_archivo = $nombrearchivo;
      }
      $dato->user_id = Auth::user()->id;
      $dato->save();
      return response()->json(['success' => true]);
    }

    public function confirmarPendiente(Request $request){
      $principal = $request->file('file');
      if ($principal) {
        $antenombreprincipal = uniqid();
        $nombrearchivo = $principal->getClientOriginalName();
        $extensionprincipal = $principal->getClientOriginalExtension();
        $nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
        \Storage::disk('pendientes')->put($nombreprincipal,  \File::get($principal));
      }

      $actividad = Pendiente::find($request->id);
      $actividad->estado  = "terminado";
      $actividad->save();

      $dato = new PendientesHistoriales;
      $dato->pendientes_id  = $request->id;
      $dato->estado  = "terminado";
      $dato->comentario = $request->comentario;
      if (isset($nombreprincipal)) {
        $dato->archivo  = $nombreprincipal;
        $dato->nombre_archivo = $nombrearchivo;
      }
      $dato->user_id = Auth::user()->id;
      $dato->save();
      return response()->json(['success' => true]);
    }

    public function aprobarPendiente(Request $request){
      $principal = $request->file('file');
      if ($principal) {
        $antenombreprincipal = uniqid();
        $nombrearchivo = $principal->getClientOriginalName();
        $extensionprincipal = $principal->getClientOriginalExtension();
        $nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
        \Storage::disk('pendientes')->put($nombreprincipal,  \File::get($principal));
      }

      $actividad = Pendiente::find($request->id);
      $actividad->estado  = "aprobado";
      $actividad->save();

      $dato = new PendientesHistoriales;
      $dato->pendientes_id  = $request->id;
      $dato->estado  = "aprobado";
      $dato->comentario = $request->comentario;
      if (isset($nombreprincipal)) {
        $dato->archivo  = $nombreprincipal;
        $dato->nombre_archivo = $nombrearchivo;
      }
      $dato->user_id = Auth::user()->id;
      $dato->save();
      return response()->json(['success' => true]);
    }

    public function verPendiente($id){
        $dato = Pendiente::find($id);
        $usuario = User::find($dato->responsable);
        $dato->user = $usuario;
        if(!empty($dato->oportunidad_id)){
            $oportunidad = Oportunidades::find($dato->oportunidad_id);
            $dato->oportunidad = $oportunidad->empresa.", ".$oportunidad->pais;
        }elseif (!empty($dato->empresa_id)) {
          $oportunidad = Empresa::find($dato->empresa_id);
          $dato->oportunidad = $oportunidad->nombre;
        }else {
          $dato->oportunidad ="";
        }
        $subtipo = SubtipoActividad::find($dato->subtipo);
        $dato->subtipo = isset($subtipo->concepto) ? $subtipo->concepto : "";
        $historia = PendientesHistoriales::where('pendientes_id', $id)->orderBy('id', 'desc')->latest()->get();
        $historias = [];
        foreach($historia as $modelo){
            $usuario = User::find($modelo->user_id);
            $modelo->user = $usuario;
            $historias[]=$modelo;
        }
        return response()->json(['success' => true, 'modelo' => $dato, 'historias' => $historias]);
    }

    public function savePendientes(Request $request){
      $detalle 					= new Pendiente;
      $detalle->detalle = $request->comentario;
      $detalle->tipo  	= $request->tipo_actividad;
      $detalle->subtipo  	= $request->subtipo_actividad;
      $detalle->empresa_id  	= $request->empresa_id;
      $detalle->oportunidad_id  	= $request->oportunidad_id;
      $detalle->fecha_ejecucion  	= $request->fecha;
      // if ($request->oportunidad != "Trabajos Generales ESSI") {
      //   $detalle->oportunidad_id = $request->oportunidad;
      // }
      $detalle->estado 			= "ejecutando";
      $detalle->responsable = Auth::user()->id;
      $detalle->user_id 		= Auth::user()->id;
      $detalle->save();
      return response()->json(['success' => true]);
    }

}

function validarComentario($value)
{
  if (isset($value->comentario) && !empty($value->comentario)) {
    return $value->comentario;
  }else {
    return "Sin comentario";
  }
}
