<?php

namespace App\Http\Controllers;

date_default_timezone_set("America/Bogota");
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Metas_oportunidade;
use App\Participacion_probabilidade;
use App\Tipo_actividades;
use App\Ciclo;
use App\Cargos;
use App\Parafiscales;
use App\Emergencias;
use App\Paises;
use App\MenuCrm;
use App\PermisoModulo;
use App\PermisoCargo;
use App\PermisoAccesos;
use App\Permiso;
use App\Configuracion;
use App\PermisoModuloFiltros;
use App\PermisoDashboardComerciales;
use App\User;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
date_default_timezone_set("America/Bogota");
class ConfiguraciongeneralController extends Controller
{
    //
    public function configuracion_general(Request $request){
            /*Consulta para activar el item del menu correspondiente*/
            $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Configuración" ORDER BY `orden` DESC LIMIT 0, 1';
            $data['item_menu'] = DB::select($query);
            if(count($data['item_menu']) > 0){
                $data['item_menu']='menu_'.$data['item_menu'][0]->id;
            }else{
                $data['item_menu']='';
            }
            /*Fin Consulta para activar el item del menu correspondiente*/
          $modulo=17;
          $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón en el menú y ver interfaz modulo" AND `id_permisomodulo`="'.$modulo.'"');
          if(isset($acceso[0]->id)){
            $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
            if(isset($cargo[0]->id)){
              $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
              if(isset($permiso[0]->permiso)){
                if($permiso[0]->permiso == "Si"){
                    $paises=Paises::all();
                    $cargos=Cargos::all();
                    $emergencias=Emergencias::all();
                    $parafiscales=Parafiscales::findorfail(1);
                    $metas=Metas_oportunidade::findorfail(1);
                    $participacion=Participacion_probabilidade::findorfail(1);
                    $tipos_actividades=DB::select('SELECT * FROM `tipo_actividades` ORDER BY `orden` DESC');
                    $step='activo';
                    $ciclos=DB::select('SELECT * FROM `ciclos` ORDER BY `porcentaje` DESC');
                    return view('configuraciongeneral.confi',["data" => $data, "ciclos"=>$ciclos,"metas"=>$metas,"participacion"=>$participacion, 'tipos_actividades'=>$tipos_actividades, "cargos" => $cargos, 'parafiscales' => $parafiscales, 'emergencias'=>$emergencias, 'paises'=>$paises, 'step'=>$step]);
                }else{
                    return view('oportunidades.nopermiso');
                }
              }
            }
          }
    }

    public function save_conf_dashboard(Request $request){
        $dato = Metas_oportunidade::findorfail(1);

        foreach ($request->dias as $key => $value) {
            $dato->$key = $value;
        }
        foreach ($request->valor as $key => $value) {
            $dato->$key = str_replace(",", "", $value);
        }
        $dato->save();

        $dato1 = Participacion_probabilidade::findorfail(1);
        foreach ($request->porcentaje as $key1 => $value1) {
            $dato1->$key1 = (int)(str_replace(" %", "", $value1));
        }
        $dato1->save();

        return \Response::json(['msj' => "Guardado correctamente!"]);
    }

    public function save_conf_tipoactividad(Request $request){

        foreach ($request->actividad as $actividades) {
                if (isset($actividades["id"])) {
                    $dato = Tipo_actividades::findorfail($actividades['id']);
                }else{
                    $dato = new Tipo_actividades;
                }
                if(!empty($actividades["orden"])){
                    $dato->orden = $actividades['orden'];
                    $dato->concepto = $actividades['concepto'];
                    $dato->descripcion = $actividades['descripcion'];
                    $dato->user_id = Auth::user()->id;
                    if(!empty($actividades["imagen"])){

                        $imagen = json_decode($actividades["imagen"]);
                        $data = explode( ',', $imagen->data );
                        $foto = base64_decode($data[1]);

                        $antenombre = sha1(date("Y-m-d H:i:s"));
                        $nombre =$antenombre.".png";
                        \Storage::disk('iconos')->put($nombre,  $foto);
                        $dato->foto = $nombre;
                    }
                    $dato->save();
                }
        }
        return \Response::json(['msj' => "Guardado correctamente!"]);
    }

    public function eliminartipoactividades($id){
        $dato = Tipo_actividades::findorfail($id);
        $tipos=Tipo_actividades::where('orden',">", $dato->orden)->get();
        foreach($tipos as $tipo){
            $dato1 = Tipo_actividades::findorfail($tipo->id);
            $dato1->orden--;
            $dato1->save();
        }

        Tipo_actividades::destroy($id);
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }

    public function nuevotipoactividades($numero){
        $dato = new Tipo_actividades;
        $dato->orden = $numero;
        $dato->user_id = Auth::user()->id;
        $dato->save();
        $id = $dato->id;
        return \Response::json(['id' => $id]);
    }
    public function ciclosventas(Request $request){
        $msj='';
        foreach ($request->ciclos as $ciclo) {
            if (isset($ciclo["id"])) {
                $dato = Ciclo::findorfail($ciclo["id"]);

            }else{
                $dato = new Ciclo;

            }

            if(!empty($ciclo["nombre"])){
                $dato->nombre = $ciclo['nombre'];
                $dato->porcentaje = $ciclo['porcentaje'];
                $dato->dias = $ciclo['dias'];
                $dato->user_id = Auth::user()->id;

                $dato->save();
            }
        }
        $ciclos= Ciclo::all();
        return \Response::json(['msj' => "Guardado correctamente!",'ciclos'=>$ciclos]);
    }

    public function ciclosventas2(Request $request){
        $msj='';
        foreach ($request->ciclos as $ciclo) {
            if (isset($ciclo["id"])) {
                $dato = Ciclo::findorfail($ciclo["id"]);

            }else{
                $dato = new Ciclo;

            }

            if(!empty($ciclo["nombre"])){
                $dato->nombre = $ciclo['nombre'];
                $dato->porcentaje = $ciclo['porcentaje'];
                $dato->dias = $ciclo['dias'];
                $dato->user_id = Auth::user()->id;

                if(!empty($ciclo["imagen"])){
                    if(isset($dato->imagen)){
                        if(!empty($dato->imagen)){
                            Storage::delete($dato->imagen);
                        }
                    }
                    $imagen = json_decode($ciclo["imagen"]);
                    $data = explode( ',', $imagen->data );
                    $foto = base64_decode($data[1]);

                    $antenombre = sha1(date("Y-m-d H:i:s"));
                    $nombre =$antenombre."imagen.png";
                    \Storage::disk('porcentajes')->put($nombre,  $foto);
                    $dato->imagen = "images/porcentajes/".$nombre;
                }
                $dato->save();
            }
        }
        $ciclos= Ciclo::all();
        return \Response::json(['msj' => "Guardado correctamente!",'ciclos'=>$ciclos]);
    }

    public function nuevocicloventa(){
        $dato = new Ciclo;
        $dato->user_id = Auth::user()->id;
        $dato->save();
        $id = $dato->id;
        return \Response::json(['id' => $id]);
    }

    public function eliminarciclo($id){
        Ciclo::destroy($id);
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }
    public function horashombre(Request $request){
        $dato = Parafiscales::findorfail(1);

        foreach ($request->parafiscales as $key => $value) {
            $dato->$key = $value;
        }
        $dato->save();

        foreach ($request->cargo as $cargos) {
            $dato = Cargos::findorfail($cargos['id']);
            foreach($cargos as $key => $value){
                $dato->$key = $value;
            }
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function usuarios(Request $request){
        foreach ($request->usuarios_cargo as $cargos) {
            $dato = Cargos::findorfail($cargos['id']);
            foreach($cargos as $key => $value){
                $dato->$key = $value;
            }
            $dato->save();
        }
        foreach ($request->usuarios_emeregencia as $emergencias) {
            $dato1 = Emergencias::findorfail($emergencias['id']);
            foreach($emergencias as $key => $value){
                $dato1->$key = $value;
            }
            $dato1->save();
        }
        return \Response::json(['msj' => 'Guardado correctamente!']);
    }

    public function nuevocargo(){
        $dato = new Cargos;
        $dato->save();
        $id = $dato->id;
        return \Response::json(['id' => $id]);
    }

    public function eliminarcargo($id){
        Cargos::destroy($id);
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }

    public function nuevaemergencia(){
        $dato = new Emergencias;
        $dato->save();
        $id = $dato->id;
        return \Response::json(['id' => $id]);
    }

    public function eliminaremergencia($id){
        Emergencias::destroy($id);
        return \Response::json(['msj' => 'Eliminado correctamente!']);
    }
    public function pais(Request $request){
        $dato = Paises::findorfail($request->id);

        $imagen = json_decode($request->foto_values);
        $data = explode( ',', $imagen->data );
        $foto = base64_decode($data[1]);
        $antenombre = sha1(date("Y-m-d H:i:s"));
        $nombre =$antenombre.".png";
        \Storage::disk('iconos')->put($nombre,  $foto);
        $dato->imagen = $nombre;
        $dato->save();

        $id=$request->id;

        return \Response::json(['msj' => "Guardado correctamente!",'id'=>$id,'imagen'=>$nombre]);
        var_dump($request->foto_values);

    }

    public function consultarpais($id){
        $datos = Paises::all();
        foreach($datos as $dato){
            if(($dato->name)==$id){
                $imagen=$dato->imagen;
            }
        }
        return \Response::json(['imagen' => $imagen]);
    }

    public function eliminarbandera($id){
        $dato = Paises::findorfail($id);
        $dato->imagen='';
        $dato->save();

        $imagen="https://placeholdit.imgix.net/~text?txtsize=45&txt=".str_replace(' ', '%20', $dato->name)."&w=200&h=200";
        return \Response::json(['msj' => 'Eliminada la imagen correctamente!','imagen' => $imagen]);
    }

    public function cambiarnumeroorden(){
        $datos=Tipo_actividades::all();
        return \Response::json(['tipos' => $datos]);
    }

    public function ayudatipoactividad($id){
        $datos = Tipo_actividades::all();
        foreach($datos as $dato){
            if(($dato->concepto)==$id){
                $msj=$dato->descripcion;
            }
        }
        return \Response::json(['msj' => $msj]);
    }

    public function accion(Request $request){
        $fecha_funcionamiento_permisos = '2018-05-19 00:00:00';
        switch($request->accion){
            /*Listar Modulos*/
            case 0:
                $modulos = PermisoModulo::all();
                ob_start();
                $i=0;
                foreach($modulos as $modulo){ $i++; ?>
                <div class="row">
                    <div class="col-md-1">
                        <strong><?=$i?></strong>
                    </div>
                    <div class="col-md-3 text-capitalize">
                        <strong><?=ucfirst(strtolower($modulo->modulo))?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=$this->fecha($modulo->created_at)?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=$this->fecha($modulo->updated_at)?></strong>
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" style="cursor:pointer;font-size:2rem;" onclick="modulo_permisos('editar','<?=$modulo->modulo?>',<?=$modulo->id?>)" data-toggle="tooltip" data-placement="right" title="Editar Modulo"><i class="fa fa-pencil"></i></a>
                        <?php if($modulo->created_at <= $fecha_funcionamiento_permisos){ ?>
                        <a class="text-success" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="right" title="Modulo funcionando en plataforma"><i class="fa fa-check"></i></a>
                        <?php }else{ ?>
                        <a class="text-danger" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="right" title="Modulo no esta funcionando en plataforma"><i class="fa fa-times"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Crear y Editar Modulos Permisos*/
            case 1:
                if(isset($request->id_modulo)){
                    $dato = PermisoModulo::find($request->id_modulo);
                }else{
                    $dato = new PermisoModulo;
                }
                $dato->modulo = $request->modulo;
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar';
                }
                break;
            /*Listar Cargos*/
            case 2:
                $cargos = PermisoCargo::all();
                ob_start();
                $i=0;
                foreach($cargos as $cargo){ $i++; ?>
                <div class="row">
                    <div class="col-md-1">
                        <strong><?=$i?></strong>
                    </div>
                    <div class="col-md-3 text-capitalize">
                        <strong><?=ucfirst(strtolower($cargo->cargo))?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=$this->fecha($cargo->created_at)?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=$this->fecha($cargo->updated_at)?></strong>
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" style="cursor:pointer;font-size:2rem;" onclick="cargo_permisos('editar','<?=$cargo->modulo?>',<?=$cargo->id?>)" data-toggle="tooltip" data-placement="right" title="Editar Modulo"><i class="fa fa-pencil"></i></a>
                        <?php if($cargo->created_at <= $fecha_funcionamiento_permisos){ ?>
                        <a class="text-success" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="right" title="Cargo funcionando en plataforma"><i class="fa fa-check"></i></a>
                        <?php }else{ ?>
                        <a class="text-danger" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="right" title="Cargo no esta funcionando en plataforma"><i class="fa fa-times"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php
                }
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Crear y Editar Cargo Permisos*/
            case 3:
                if(isset($request->id_cargo)){
                    $dato = PermisoCargo::find($request->id_cargo);
                }else{
                    $dato = new PermisoCargo;
                }
                $dato->cargo = $request->cargo;
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar';
                }
                break;
            /*Listado de Tipos de acceso pero separados por modulos*/
            case 4:
                $modulos = PermisoModulo::all();
                $i=0;
                ob_start();
                ?>
                <div id="accordion_modulo">
                 <?php foreach($modulos as $modulo){ $i++; ?>
                  <div class="card">
                    <div class="card-header" id="heading<?=$this->numeros_letras($i)?>">
                      <h5 class="mb-0">
                        <button class="btn btn-link" onclick="lista_accesos(<?=$modulo->id?>,'collapse<?=$this->numeros_letras($i)?>')" data-toggle="collapse" data-target="#collapse<?=$this->numeros_letras($i)?>" aria-expanded="true" aria-controls="collapse<?=$this->numeros_letras($i)?>">
                          <?=ucfirst(strtolower($modulo->modulo))?>
                        </button>
                      </h5>
                    </div>

                    <div id="collapse<?=$this->numeros_letras($i)?>" class="collapse show" aria-labelledby="heading<?=$this->numeros_letras($i)?>" data-parent="#accordion_modulo">
                      <div class="card-body">

                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Listado de accesos de un modulo*/
            case 5:
                $accesos = PermisoAccesos::where('id_permisomodulo',$request->id_modulo)->get();
                $i = 0;
                ob_start();?>
                <div class="row">
                    <div class="col md-12 text-center">
                        <a class="text-success" style="cursor:pointer;font-size:3rem;" onclick="acceso_permisos('crear',<?=$request->id_modulo?>,'<?=$request->id_contenedor?>')" data-toggle="tooltip" data-placement="right" title="Crear acceso"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <strong>Numero</strong>
                    </div>
                    <div class="col-md-3">
                        <strong>Acceso</strong>
                    </div>
                    <div class="col-md-3">
                        <strong>Creado</strong>
                    </div>
                    <div class="col-md-3">
                        <strong>Editado</strong>
                    </div>
                    <div class="col-md-2">
                        <strong>Acción</strong>
                    </div>
                </div>
                <div class="container listado-acceso">
                <?php if(count($accesos) == 0){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <p class="h5 text-center">
                            <strong>No hay accesos en este modulo</strong>
                        </p>
                    </div>
                </div>
                <?php } ?>
                <?php foreach($accesos as $acceso){ $i++; ?>
                <div class="row">
                    <div class="col-md-1">
                        <strong><?=$i?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=ucfirst(strtolower($acceso->acceso))?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=$this->fecha($acceso->created_at)?></strong>
                    </div>
                    <div class="col-md-3">
                        <strong><?=$this->fecha($acceso->updated_at)?></strong>
                    </div>
                    <div class="col-md-2">
                       <a class="text-info" style="cursor:pointer;font-size:2rem;" onclick="acceso_permisos_cargo(<?=$acceso->id?>,'<?=$acceso->acceso?>')" data-toggle="tooltip" data-placement="right" title="Administrar Permisos"><i class="fa fa-user"></i></a>
                        <?php if($acceso->created_at <= $fecha_funcionamiento_permisos){ ?>
                        <a class="text-success" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="right" title="Acceso funcionando en plataforma"><i class="fa fa-check"></i></a>
                        <?php }else{ ?>
                        <a class="text-warning" style="cursor:pointer;font-size:2rem;" onclick="acceso_permisos('editar',<?=$request->id_modulo?>,'<?=$request->id_contenedor?>','<?=$acceso->acceso?>',<?=$acceso->id?>)" data-toggle="tooltip" data-placement="right" title="Editar Acceso"><i class="fa fa-pencil"></i></a>
                        <a class="text-danger" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="right" title="Acceso no esta funcionando en plataforma"><i class="fa fa-times"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Crear acceso*/
            case 6:
                if(isset($request->id_acceso)){
                    $dato = PermisoAccesos::find($request->id_acceso);
                }else{
                    $dato = new PermisoAccesos;
                    $dato->id_permisomodulo = $request->id_modulo;
                }
                $dato->acceso = $request->acceso;
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Crear HTML con los cargos y permisos que tienen actualmente sobre el acceso*/
            case 7:
                $cargos = PermisoCargo::all();
                $opciones[0] = array('Si','No');
                $opciones[1] = array('Datos Propios','Datos Usu Diego Tara');
                $opciones[2] = array('Ver propio','Ver todas');
                $opciones[3] = array('propia','ninguna','TODAS');
                $especial[0] = array('Datos Propios','sin seleccion');
                $especial[1] = array('PROPIAS','TODAS');
                $especial[2] = array('Ver propio','Ver Sumatoria');
                $especial[3] = array('propia','TODAS','ninguna');
                ob_start(); ?>
                    <div class="row margen-abajo">
                        <div class="col-md-6">
                            <strong>Cargo</strong>
                        </div>
                        <div>
                            <strong>Permiso</strong>
                        </div>
                    </div>
                    <form id="formulario_permisosacceso" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="accion" value="8">
                        <input type="hidden" name="id_acceso" value="<?=$request->id_acceso?>">
                    <?php $i=0; foreach($cargos as $cargo){
                    $permiso_almacenado = DB::select('SELECT * FROM permisos WHERE id_acceso = "'.$request->id_acceso.'" AND id_cargo = "'.$cargo->id.'" ORDER BY id_cargo ASC');
                    $data['query'] = 'SELECT * FROM permisos WHERE id_acceso = "'.$request->id_acceso.'" AND id_cargo = "'.$cargo->id.'" ORDER BY id_cargo ASC';
                    if(count($permiso_almacenado)>0){
                        $permiso=$permiso_almacenado[0]->permiso;
                    }else{
                        $permiso="No asignado";
                    }
                    $options = array('Si','No','Datos Propios','Datos Usu Diego Tara','Ver propio','Ver todas','propia','ninguna','TODAS','sin seleccion','PROPIAS','Ver Sumatoria');
                    switch($request->id_acceso){
                        case ($request->id_acceso == 39 || $request->id_acceso == 44):
                            $options = $especial[0];
                            break;
                        case ($request->id_acceso == 50):
                            $options = $especial[1];
                            break;
                        case ($request->id_acceso == 57 || $request->id_acceso == 77):
                            $options = $especial[2];
                            break;
                        case ($request->id_acceso == 84 || $request->id_acceso == 86 || $request->id_acceso == 87):
                            $options = $especial[3];
                            break;
                        default:
                            if(isset($permiso_almacenado[0]->permiso)){
                                foreach($opciones as $index=>$opcion){
                                    foreach($opcion as $o){
                                        if($permiso_almacenado[0]->permiso == $o){
                                            $options = $opciones[$index];
                                        }
                                    }
                                }
                            }
                    }
                    ?>
                    <?php if(count($permiso_almacenado)>0){ ?>
                        <input type="hidden" name="formulario[<?php echo $i ?>][id]" value="<?=$permiso_almacenado[0]->id?>">
                    <?php } ?>
                        <input type="hidden" name="formulario[<?php echo $i ?>][id_cargo]" value="<?php echo $cargo->id ?>">
                        <div class="row my-3">
                            <div class="col-md-6">
                                <?php echo $cargo->cargo ?>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control" style="height: 31px;" name="formulario[<?php echo $i ?>][permiso]">
                                    <?php foreach($options as $option){ ?>
                                    <option value="<?=$option?>" <?php if($permiso == $option){ ?>Selected<?php } ?> ><?=$option?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                            <?php
                        $i++;
                        }
                        ?>
                    </form>
                <?php
                $data['html']=ob_get_contents();
                ob_end_clean();
                break;
            case 8:
                $formulario = $request->formulario;
                foreach($formulario as $form){
                    if(isset($form['id'])){
                        $dato = Permiso::find($form['id']);
                    }else{
                        $dato = new Permiso;
                    }
                    $dato->id_acceso = $request->id_acceso;
                    $dato->id_cargo = $form['id_cargo'];
                    $dato->permiso = $form['permiso'];
                    if($dato->save()){
                        $data['msj'] = 'Guardado correctamente!';
                    }else{
                        $data['msj'] = 'Error al guardar';
                    }
                }
                break;
            /*HTML que contiene la estructura para modificar los días de BITACORA - PLAN DE TRABAJO - FECHA PRESUPUESTO -PRESUPUESTO ANUAL */
            case 9:
                $bitacora = Configuracion::where('campo','dias bitacora')->get();
                $plan_trabajo = Configuracion::where('campo','dias plan trabajo')->get();
                $presupuesto = Configuracion::where('campo','fecha presupuesto')->get();
                $presupuestoAnual = Configuracion::where('campo','presupuesto anual')->get();
                ob_start(); ?>

                <div class="row">
                    <div class="col-md-12 my-3 text-center">
                        <p class="h3"><strong>Configurar Dias bitacora</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled activity-list">
                            <li>
                                <i class="fa fa-bell-o activity-icon pull-left"></i>
                                <p class="text-left">
                                    <a href="#">Bitacora</a> La cantidad que se ingresen son los <a href="#">días atras en los que se podra editar</a>
                                </p>
                            </li>
                            <li>
                                <i class="fa fa-bell-o activity-icon pull-left"></i>
                                <p class="text-left">
                                    <a href="#">Plan de trabajo</a> La cantidad que se ingresen son los <a href="#">días pasados de la semana actual</a>
                                </p>
                            </li>
                            <li>
                                <i class="fa fa-bell-o activity-icon pull-left"></i>
                                <p class="text-left">
                                    <a href="#">Fecha Presupuesto</a> La cantidad que se ingresen es el <a href="#">día maximo del mes actual para editar el presupuesto</a>
                                </p>
                            </li>
                            <li>
                                <i class="fa fa-bell-o activity-icon pull-left"></i>
                                <p class="text-left">
                                    <a href="#">Presupuesto Anual</a> Elija la opción de editar <a href="#">presupuesto año actual o proximo año</a>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 my-3 text-center">
                        <p class="h5"><strong>Bitacora (Dias)</strong></p>
                        <input type="text" class="form-control confi-dias" onchange="dias_configuracion('bitacora',this.value,this.id)" id="configuracion_dias_bitacora" value="<?=$bitacora[0]->valor?>">
                    </div>
                    <div class="col-md-4 my-3 text-center">
                        <p class="h5"><strong>Plan de Trabajo (Dias)</strong></p>
                        <input type="text" class="form-control confi-dias" onchange="dias_configuracion('plantrabajo',this.value,this.id)" id="configuracion_diasplantrabajo" value="<?=$plan_trabajo[0]->valor?>">
                    </div>
                    <div class="col-md-4 my-3 text-center">
                        <p class="h5"><strong>Fecha Presupuesto (todos los de cada mes)</strong></p>
                        <input type="text" class="form-control confi-dias" onchange="dias_configuracion('presupuesto',this.value,this.id)" id="configuracion_fechapresupuesto" value="<?=$presupuesto[0]->valor?>">
                    </div>
                    <div class="col-md-4 my-3 text-center">
                        <p class="h5"><strong>Presupuesto Anual</strong></p>
                        <select class="form-control confi-dias" onchange="dias_configuracion('presupuestoAnual',this.value,this.id)" id="configuracion_presupuestoanual" style="height: 53%;">
                            <option value="Año Actual" <?php if($presupuestoAnual[0]->valor == "Año Actual"){ echo 'Selected'; } ?> >Año Actual</option>
                            <option value="Año Siguiente" <?php if($presupuestoAnual[0]->valor == "Año Siguiente"){ echo 'Selected'; } ?> >Año Siguiente</option>
                        </select>
                    </div>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Guardar los dias para la tabla configuracions*/
            case 10:
                $dato = Configuracion::find($request->id);
                $dato->campo=$request->campo;
                $dato->valor=$request->valor;
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            /*Contenido de administración de los SELECT de Filtro*/
            case 11:
                $cargos = PermisoCargo::all();
                $query = 'SELECT DISTINCT(F.id_permiso_modulo), (SELECT M.modulo FROM permiso_modulos M WHERE M.id = F.id_permiso_modulo) AS MODULO FROM permiso_modulo_filtros F WHERE F.deleted_at IS NULL';
                $filtros = DB::SELECT($query);
                ob_start();?>
                <div class="container contenido_select">
                    <div class="row">
                       <div class="col-md-12 my-4">
                           <strong>Cargos Filtros</strong>
                       </div>
                        <div class="col-md-1">
                            <strong>Item</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Modulo</strong>
                        </div>
                        <div class="col-md-5">
                            <strong>Cargos</strong>
                        </div>
                        <div class="col-md-2">
                            <strong>Accion</strong>
                        </div>
                    </div>
                <?php
                $i=1;
                foreach($filtros as $filtro){
                    $query = 'SELECT C.cargo FROM permiso_modulo_filtros F INNER JOIN permiso_cargos C ON F.id_cargo = C.id WHERE F.deleted_at IS NULL AND F.id_permiso_modulo = '.$filtro->id_permiso_modulo;
                    $filtros1 = DB::SELECT($query);
                    ?>
                    <div class="row">
                        <div class="col-md-1">
                            <?=$i?>
                        </div>
                        <div class="col-md-4">
                            <?=$filtro->MODULO?>
                        </div>
                        <div class="col-md-5">
                            <?php for($y=0;$y<count($filtros1);$y++){ ?>
                            <?=$cargo=$y==0?$filtros1[$y]->cargo:','.$filtros1[$y]->cargo?>
                            <?php } ?>
                        </div>
                        <div class="col-md-2">
                            <a class="btn btn-sm btn-subirposicion" onclick="anadir_cargos(<?=$filtro->id_permiso_modulo?>)" title="Añadir Cargos"><i class="fa fa-male" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                </div>
                <div class="container info_dash_comercial">
                    <div class="row">
                       <div class="col-md-12 my-4">
                           <strong>Información Dashboard Comercial</strong>
                       </div>
                        <div class="col-md-1">
                            <strong>Item</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Usuario</strong>
                        </div>
                        <div class="col-md-5">
                            <strong>Información</strong>
                        </div>
                        <div class="col-md-2">
                            <strong>Acción</strong>
                        </div>
                    </div>
                    <?php $i=1; foreach($cargos as $cargo){
                    $permiso = PermisoDashboardComerciales::where('id_cargo',$cargo->id)->first();
                    if( isset($permiso->datos_propios) && $permiso->datos_propios == 1){
                        $informacion = "Datos Propios";
                    }else if(isset($permiso->id_user_dashboard)){
                        $usuario = User::find($permiso->id_user_dashboard);
                        $informacion = 'Información '.$usuario->nombres.' '.$usuario->apellidos;
                    }else{
                        $informacion = 'No hay un usuario seleccionado';
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-1">
                            <strong><?=$i?></strong>
                        </div>
                        <div class="col-md-4">
                            <strong><?=$cargo->cargo?></strong>
                        </div>
                        <div class="col-md-5">
                            <strong><?=$informacion?></strong>
                        </div>
                        <div class="col-md-2">
                            <a class="btn btn-sm btn-subirposicion" onclick="user_dashboard(<?=$cargo->id?>)" title="Modificar usuario del dashboard comercial"><i class="fa fa-male" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            case 12:
                $cargos = DB::select('SELECT * FROM `permiso_cargos` ORDER BY `id` ASC');
                ob_start(); ?>
                    <div class="row margen-abajo">
                        <div class="col-md-6">
                            <strong>Cargo</strong>
                        </div>
                        <div>
                            <strong>Permiso</strong>
                        </div>
                    </div>
                    <form id="formulario_permisosselect" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="accion" value="13">
                        <input type="hidden" name="id_permiso_modulo" value="<?=$request->id?>">
                    <?php $i=0; foreach($cargos as $cargo){
                    $permiso_almacenado = DB::select('SELECT * FROM `permiso_modulo_filtros` WHERE `id_permiso_modulo` = "'.$request->id.'" AND `id_cargo` = "'.$cargo->id.'" AND `deleted_at` IS NULL ORDER BY `id` DESC');
                    if(count($permiso_almacenado)>0){
                        $permiso="Si";
                    }else{
                        $permiso="No";
                    }
                    ?>
                        <input type="hidden" name="formulario[<?php echo $i ?>][id_cargo]" value="<?php echo $cargo->id ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo $cargo->cargo ?>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" name="formulario[<?php echo $i ?>][permiso]" id="hiddenmyonoffswitch<?php echo $cargo->id ?>" value="<?php echo $permiso ?>">
                                <div class="onoffswitch input-permisos-menu">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox p_item_menu" id="myonoffswitch<?php echo $cargo->id ?>" <?php if($permiso=="Si") { ?>checked <?php } ?>>
                                    <label class="onoffswitch-label" for="myonoffswitch<?php echo $cargo->id ?>">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                            <?php
                        $i++;
                        }
                        ?>
                    </form>
            <?php
            $data['html']=ob_get_contents();
            ob_end_clean();
                break;
            /*Guardar los cargos que tienen permisos*/
            case 13:
                /*Eliminar permisos viejos*/
                $cargos_anteriores = PermisoModuloFiltros::where('id_permiso_modulo',$request->id_permiso_modulo)->get();
                foreach($cargos_anteriores as $cargo){
                    $cargo->delete();
                }
                /*FIN Eliminar permisos viejos*/
                $formulario = $request->formulario;
                foreach($formulario as $cargo_permiso){
                    if($cargo_permiso['permiso'] == "Si"){
                        $dato = new PermisoModuloFiltros;
                        $dato->id_cargo=$cargo_permiso['id_cargo'];
                        $dato->id_permiso_modulo=$request->id_permiso_modulo;
                        $dato->save();
                    }
                }
                $data['msj'] = 'Guardado correctamente!';
                break;
            /*Guardar los cargos con su respectivo usuario a visualiza en el dashboard comercial*/
            case 14:
                $query = 'SELECT U.id, CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1), " ", SUBSTRING_INDEX(U.apellidos, " ", 1)) AS USUARIO FROM users U INNER JOIN permiso_cargos C ON U.cargo = C.cargo WHERE U.deleted_at IS NULL';
                $usuarios = DB::SELECT($query);
                ob_start();
                ?>
                <form id="formulario_usuariosdashboard" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                       <input type="hidden" value="15" name="accion">
                       <input type="hidden" value="<?=$request->id?>" name="id_cargo_dashboard">
                        <select class="form-control" name="usuario_dashboard" style="height: 37px;">
                            <option value="">Seleccione un usuario</option>
                            <?php foreach($usuarios as $usuario){ ?>
                            <option value="<?=$usuario->id?>"><?=$usuario->USUARIO?></option>
                            <?php } ?>
                            <option value="Datos Propios">Datos Propios</option>
                        </select>
                    </div>
                </div>
                </form>
                <?php
                $data['html'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Guardar los cargos con su respectivo usuario a visualiza en el dashboard comercial*/
            case 15:
                $permiso = PermisoDashboardComerciales::where('id_cargo',$request->id_cargo_dashboard)->first();
                if(isset($permiso->id)){
                    $dato = PermisoDashboardComerciales::find($permiso->id);
                }else{
                    $dato = new PermisoDashboardComerciales;
                    $dato->id_cargo = $request->id_cargo_dashboard;
                }
                if($request->usuario_dashboard == "Datos Propios"){
                    $dato->datos_propios = 1;
                    $dato->id_user_dashboard = NULL;
                }else{
                    $dato->datos_propios = 2;
                    $dato->id_user_dashboard = $request->usuario_dashboard;
                }
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar';
                }
                break;
        }
        return \Response::json(['data' => $data]);
    }

    public function consultar_menu(){
        $menu = DB::select('select * from `menu_crms` WHERE `sub_item` IS NULL AND `deleted_at` IS NULL ORDER BY `orden` ASC');
        $paises = Paises::all();
        $modulos = PermisoModulo::all();
        ob_start(); ?>
        <div class="content">
				<div class="main-header margen-top">
					<h2>Permiso</h2>
					<em id="permiso_actual">Permiso menu</em>
				</div>
				<div class="main-content">
					<!-- TABS -->
					<div class="widget">
						<div class="widget-header">
							<h3>Administrar Permisos</h3>
						</div>
						<div class="widget-content">

							<ul class="nav nav-pills permisos-accesos" role="tablist">
								<li class="active" id="p_Menu"><a href="#chart1" role="tab" data-toggle="tab"><i class="fa fa-bars"></i> Menu</a></li>
								<li id="p_Modulos"><a href="#chart2" onclick="permisos_accion('lista_modulos')" role="tab" data-toggle="tab"><i class="fa fa-modx"></i> Módulos</a></li>
                                <li id="p_Cargos"><a href="#chart3" onclick="permisos_accion('lista_cargo')" role="tab" data-toggle="tab"><i class="fa fa-address-card-o"></i> Cargos</a></li>
                                <li id="p_Tipos_Acceso"><a href="#chart4" onclick="permisos_accion('lista_acceso')" role="tab" data-toggle="tab"><i class="fa fa-check-square"></i> Tipos Acceso</a></li>
								<li id="p_Permisos_Acceso"><a href="#chart5" onclick="dias_asignados()" role="tab" data-toggle="tab"><i class="fa fa-wheelchair-alt"></i> Dias Asignados</a></li>
								<li id="p_contenido_select"><a href="#chart6" onclick="contenido_select()" role="tab" data-toggle="tab"><i class="fa fa-check-circle"></i> Select Filtro</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="chart1">
									<div class="row">
                                        <div class="col-md-12">
                                            <a class="crear-menu"><i class="fa fa-plus-circle" aria-hidden="true" title="Crear un nuevo item"></i></a>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-md-1">
                                            <strong>Orden</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Icono</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Item</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Url</strong>
                                        </div>
                                        <div class="col-md-2">
                                            <strong>Acción</strong>
                                        </div>
                                    </div>
                                    <?php foreach($menu as $m){ ?>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <a><?php echo $m->orden ?></a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><i class="<?php echo $m->logo ?>" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><?php echo $m->item ?></a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><em>/</em><?php echo $m->url ?></a>
                                        </div>
                                        <div class="col-md-2">
                                            <a target="_blank" class="btn btn-sm btn-subirposicion" onclick="crearpermiso(<?php echo $m->id ?>,'<?php echo $m->item ?>')" title="Crear permisos <?php echo $m->item ?>"><i class="fa fa-male" aria-hidden="true"></i></a>
                                            <a target="_blank" class="btn btn-sm btn-subirposicion" onclick="bajar(<?php echo $m->id ?>)" title="Bajar posición <?php echo $m->item ?>"><i class="fa fa-arrow-down"></i></a>
                                            <a target="_blank" class="btn btn-sm btn-subirposicion" onclick="subir(<?php echo $m->id ?>)" title="Subir posición <?php echo $m->item ?>"><i class="fa fa-arrow-up"></i></a>
                                            <a target="_blank" id="menu_<?php echo $m->id ?>" class="btn btn-sm btn-subitem" title="Agregar Sub-item <?php echo $m->item ?>"><i class="fa fa-plus"></i></a>
                                            <a target="_blank" class="btn btn-sm btn-editar" onclick="editarmenuitem(<?php echo $m->id ?>)" title="Editar item <?php echo $m->item ?>"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-sm btn-eliminar" id="eliminar_item_<?php echo $m->id ?>" title="Eliminar Item <?php echo $m->item ?>"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </div>
                                    <?php } ?>
								</div>
								<div class="tab-pane fade" id="chart2">
									<div class="row">
                                        <div class="col md-12 text-center">
                                            <a class="text-success" style="cursor:pointer;font-size:3rem;" onclick="modulo_permisos('crear')" data-toggle="tooltip" data-placement="right" title="Crear Modulo"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <strong>Numero</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Modulo</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Creado</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Editado</strong>
                                        </div>
                                        <div class="col-md-2">
                                            <strong>Acción</strong>
                                        </div>
                                    </div>
                                    <div class="container" id="listado_modulos">

                                    </div>
								</div>
								<div class="tab-pane fade" id="chart3">
									<div class="row">
                                        <div class="col md-12 text-center">
                                            <a class="text-success" style="cursor:pointer;font-size:3rem;" onclick="cargo_permisos('crear')" data-toggle="tooltip" data-placement="right" title="Crear Cargo"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <strong>Numero</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Cargo</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Creado</strong>
                                        </div>
                                        <div class="col-md-3">
                                            <strong>Editado</strong>
                                        </div>
                                        <div class="col-md-2">
                                            <strong>Acción</strong>
                                        </div>
                                    </div>
                                    <div class="container" id="listado_cargos_permisos">

                                    </div>
								</div>
								<div class="tab-pane fade" id="chart4">
								</div>
								<div class="tab-pane fade" id="chart5">

								</div>
								<div class="tab-pane fade" id="chart6">

								</div>
							</div>
						</div>
					</div>
					<!-- END TABS -->
                </div>
        </div>
<?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultar_subitem_menu($id){
        $menu = MenuCrm::find($id);
        $submenu = DB::select('select * from `menu_crms` WHERE `sub_item` = "'.$id.'" ORDER BY `orden` ASC');
        ob_start(); ?>
            <?php if(count($submenu) > 0){ ?>
            <div class="row">
                <div class="col-md-4 margen-left">
                    <strong>Item</strong>
                </div>
                <div class="col-md-6 margen-left">
                    <strong>Url</strong>
                </div>
                <div class="col-md-2">
                    <strong>Acción</strong>
                </div>
            </div>
            <?php } ?>
            <?php foreach($submenu as $m){ ?>
            <div class="row">
                <div class="col-md-4 margen-left">
                    <a><?php echo $m->item ?></a>
                </div>
                <div class="col-md-6 margen-left">
                    <a><em>/</em><?php echo $m->url ?></a>
                </div>
                <div class="col-md-2">
                    <a target="_blank" class="btn btn-sm btn-subirposicion" onclick="crearpermiso(<?php echo $m->id ?>,'<?php echo $m->item ?>')" title="Crear permisos"><i class="fa fa-male" aria-hidden="true"></i></a>
                    <a target="_blank" onclick="editarsubitem(<?php echo $m->id ?>,<?php echo $menu->id ?>,'<?php echo $menu->item ?>','<?php echo $m->item ?>','<?php echo $m->url ?>')" class="btn btn-sm btn-editar-subitem" title="Editar item"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-eliminar-subitem" id="subitemeliminar_<?php echo $m->id ?>" title="Eliminar Item"><i class="fa fa-trash"></i></a>
                </div>
            </div>
            <?php } ?>

            <div class="row">
                <input type="hidden" id="subitem_id" name="id" value="<?php echo $id ?>">
                <?php if(count($submenu) > 0){ ?> <div class="col-md-4 margen-left"> <?php }else{ ?> <div class="col-md-6 margen-left"> <?php } ?>
                    <input type="text" class="form-control" id="subitem_item" name="item" placeholder="Ingrese el nombre del subitem" value="" required>
                </div>
                <div class="col-md-6 margen-left">
                    <input type="text" class="form-control" id="subitem_url" name="url" placeholder="No es necesario ingresar /" value="" required>
                </div>
                <div class="col-md-2">

                </div>
            </div>

<?php
        $html = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $html, 'menu' => $menu]);
    }

    public function guardarsubitem(Request $request){
        $dato = new MenuCrm;
        $dato->orden=0;
        $dato->item=$request->item;
        $dato->url=$request->url;
        $dato->sub_item=$request->id;
        $dato->save();

        return \Response::json(['msj' => "Guardado correctamente!"]);
    }

    public function eliminarsubitem($id){
        $dato = MenuCrm::find($id);
        $dato->delete();
        return \Response::json(['msj' => "Eliminado correctamente!"]);
    }

    public function itemsprincipales($id){
        $principales = DB::select('select * from `menu_crms` WHERE `sub_item` IS NULL ORDER BY `orden` ASC');
        ob_start(); ?>
        <select class="form-control" id="subitemeditarsub_item">
        <?php foreach($principales as $item){ ?>
            <option value="<?php echo $item->id ?>" <?php if($item->id == $id){ echo "Selected"; } ?>><?php echo $item->item ?></option>
        <?php } ?>
        </select>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $html]);
    }

    public function editarsubitem(Request $request){
        $id=$request->id;
        $dato = MenuCrm::find($id);
        $dato->item = $request->item;
        $dato->sub_item = $request->sub_item;
        $dato->url = $request->url;
        $dato->save();
        return \Response::json(['msj' => "Editado correctamente!"]);
    }

    public function consultariconos($id){
        $dato = MenuCrm::find($id);
        $iconos = DB::select('select * from `iconos` ORDER BY `id` ASC');
        ob_start(); ?>
                <div class="row">
                    <div class="col-md-6 escoger_icono_elegido" style="display:none;">
                        <label>Icono</label>
                        <input type="text" class="form-control" id="editarmenuitem_logo" value="<?php echo $dato->logo ?>" list="iconos">
                        <datalist id="iconos">
                           <?php foreach($iconos as $icono){ ?>
                          <option value="fa <?php echo $icono->clase ?>" label="<?php echo $icono->alias ?>">
                            <?php } ?>
                        </datalist>
                    </div>
                    <div class="col-md-6 ver_icono_elegido" style="margin-top:4%;">
                        <div class="row">
                            <div class="col-md-6 icono_elegido" style="text-align: right;">
                                <i class="<?php echo $dato->logo ?>"></i>
                            </div>
                            <div class="col-md-6" style="text-align: left">
                                <a title="Editar icono" style="cursor:pointer; color: orange;">
                                    <i class="fa fa-pencil editar_icono_elegido" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Item</label>
                        <input type="text" class="form-control" id="editarmenuitem_item" value="<?php echo $dato->item ?>">
                    </div>
                    <div class="col-md-12">
                        <label>Url <i>(No necesita ingresar /)</i></label>
                        <input type="text" class="form-control" id="editarmenuitem_url" value="<?php echo $dato->url ?>">
                    </div>
                </div>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $html]);
    }

    public function editaritem(Request $request){
        $id=$request->id;
        $dato = MenuCrm::find($id);
        $dato->logo = $request->logo;
        $dato->item = $request->item;
        $dato->url = $request->url;
        $dato->save();
        return \Response::json(['msj' => "Editado correctamente!"]);
    }

    public function eliminar_itemmenu($id){
        $dato = MenuCrm::find($id);
        $dato->deleted_at=date('Y-m-d H:i:s');
        $dato->save();
        return \Response::json(['msj' => "Eliminado correctamente!"]);
    }

    public function nuevoitemmenu(){
        $iconos = DB::select('select * from `iconos` ORDER BY `id` ASC');
        ob_start(); ?>
                <div class="row">
                    <div class="col-md-6 escoger_icono_elegido">
                        <label>Icono</label>
                        <input type="text" class="form-control" id="editarmenuitem_logo" list="iconos">
                        <datalist id="iconos">
                           <?php foreach($iconos as $icono){ ?>
                          <option value="fa <?php echo $icono->clase ?>" label="<?php echo $icono->alias ?>">
                            <?php } ?>
                        </datalist>
                    </div>
                    <div class="col-md-6 ver_icono_elegido" style="margin-top:4%;display:none;">
                        <div class="row">
                            <div class="col-md-6 icono_elegido" style="text-align: right;">
                                <i class=""></i>
                            </div>
                            <div class="col-md-6" style="text-align: left">
                                <a title="Editar icono" style="cursor:pointer; color: orange;">
                                    <i class="fa fa-pencil editar_icono_elegido" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Item</label>
                        <input type="text" class="form-control" id="editarmenuitem_item" value="">
                    </div>
                    <div class="col-md-12">
                        <label>Url <i>(No necesita ingresar https://smartessi.com/)</i></label>
                        <input type="text" class="form-control" id="editarmenuitem_url" value="">
                    </div>
                </div>
        <?php
        $html=ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $html]);
    }

    public function guardaritem(Request $request){
        $orden = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL ORDER BY `orden` DESC');

        $dato = new MenuCrm;
        $dato->orden = (intval($orden[0]->orden))+1;
        $dato->logo = $request->logo;
        $dato->item = $request->item;
        $dato->url = $request->url;
        $dato->save();
        return \Response::json(['msj' => 'Guardado correctamente']);
    }

    public function bajarposicionitemmenu($id){
        $dato = MenuCrm::find($id);
        $orden_actual=$dato->orden;
        $orden_ultimo = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL ORDER BY `orden` DESC');
        if($orden_ultimo[0]->orden == $orden_actual){
            $datos = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL AND `id` != "'.$id.'" ORDER BY `orden` DESC');
            foreach($datos as $d){
                $item = MenuCrm::find($d->id);
                $item->orden=(intval($d->orden))+1;
                $item->save();
            }
            $dato->orden=1;
            $dato->save();
        }else{
            $orden_buscar=(intval($orden_actual))+1;
            $item_modificar = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL AND `orden` = "'.$orden_buscar.'" ORDER BY `orden` DESC');
            $item = MenuCrm::find($item_modificar[0]->id);
            $item->orden=$orden_buscar-1;
            $item->save();

            $dato->orden=(intval($orden_actual))+1;
            $dato->save();
        }
        return \Response::json(['msj' => 'Bajado correctamente']);
    }

    public function subirposicionitemmenu($id){
        $dato = MenuCrm::find($id);
        $orden_actual=$dato->orden;
        $orden_primero = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL ORDER BY `orden` ASC');
        if($orden_primero[0]->orden == $orden_actual){
            $datos = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL AND `id` != "'.$id.'" ORDER BY `orden` ASC');
            foreach($datos as $d){
                $item = MenuCrm::find($d->id);
                $item->orden=(intval($d->orden))-1;
                $item->save();
            }
            $dato->orden=count($datos)+1;
            $dato->save();
        }else{
            $orden_buscar=(intval($orden_actual))-1;
            $item_modificar = DB::select('select * from `menu_crms` WHERE `deleted_at` IS NULL AND `sub_item` IS NULL AND `orden` = "'.$orden_buscar.'" ORDER BY `orden` DESC');
            $item = MenuCrm::find($item_modificar[0]->id);
            $item->orden=$orden_buscar+1;
            $item->save();

            $dato->orden=(intval($orden_actual))-1;
            $dato->save();
        }
        return \Response::json(['msj' => 'Subido correctamente']);
    }

    public function crearpermisoitem($id){
        $cargos = DB::select('SELECT * FROM `permiso_cargos` ORDER BY `id` ASC');
            ob_start(); ?>
                <div class="row margen-abajo">
                    <div class="col-md-6">
                        <strong>Cargo</strong>
                    </div>
                    <div>
                        <strong>Permiso</strong>
                    </div>
                </div>
                <form id="formulario_permisosmenu" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id_menu" value="<?php echo $id ?>">
                <?php $i=0; foreach($cargos as $cargo){
                $permiso_almacenado = DB::select('SELECT * FROM `permiso_menu` WHERE `id_menu` = "'.$id.'" AND `id_cargo` = "'.$cargo->id.'" ORDER BY `id` DESC');
                if(count($permiso_almacenado)>0){
                    $permiso=$permiso_almacenado[0]->permiso;
                }else{
                    $permiso="No";
                }
                ?>
                    <input type="hidden" name="formulario[<?php echo $i ?>][id_cargo]" value="<?php echo $cargo->id ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo $cargo->cargo ?>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="formulario[<?php echo $i ?>][permiso]" id="hiddenmyonoffswitch<?php echo $cargo->id ?>" value="<?php echo $permiso ?>">
                            <div class="onoffswitch input-permisos-menu">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox p_item_menu" id="myonoffswitch<?php echo $cargo->id ?>" <?php if($permiso=="Si") { ?>checked <?php } ?>>
                                <label class="onoffswitch-label" for="myonoffswitch<?php echo $cargo->id ?>">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                        <?php
                    $i++;
                    }
                    ?>
                </form>
        <?php
        $html=ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $html]);
    }

    public function guardarpermisoitemmenu(Request $request){
        $id_menu=$request->id_menu;
        $datos=$request->formulario;
        foreach($datos as $dato){
            $permiso_almacenado = DB::select('SELECT * FROM `permiso_menu` WHERE `id_menu` = "'.$id_menu.'" AND `id_cargo` = "'.$dato["id_cargo"].'" ORDER BY `id` DESC');
            if(count($permiso_almacenado)>0){
                $id=$permiso_almacenado[0]->id;
                DB::table('permiso_menu')->where('id', $id)->update(['permiso' => $dato["permiso"]]);
            }else{
                DB::table('permiso_menu')->insert(['id_menu' => $id_menu, 'permiso' => $dato["permiso"], 'id_cargo' => $dato["id_cargo"], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            }
        }
        return \Response::json(['msj' => "Guardado correctamente!"]);
    }

    function fecha($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $fecha_hora = explode(' ',$fecha);
            $f=explode('-',$fecha_hora[0]);
            $h=explode(':',$fecha_hora[1]);
            if($h[0]>12){
                $h[0]= intval($h[0]) - 12;
                $jornada = 'P.M';
            }else{
                $jornada = 'A.M';
            }
            $fecha_formato = $f[2].'/'.$meses[intval($f[1])].'/'.$f[0].' '.$h[0].':'.$h[1].':'.$h[2].' '.$jornada;
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }
    }
    function numeros_letras($posicion){
        $numeros = array('One','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten','Eleven','Twelve','Thirteen','Fourteen','Fifteen','Sixteen','Seventeen','Eighteen','Nineteen','Twenty','Twenty_one','Twenty_two','Twenty_three','Twenty_four','Twenty_five','Twenty_six','Twenty_seven','Twenty_eight','Twenty_nine','Thirty','Thirty_one','Thirty_two','Thirty_three','Thirty_four','Thirty_five','Thirty_six','Thirty_seven','Thirty_eight','Thirty_nine','Forty','Forty_one','Forty_two','Forty_three','Forty_four','Forty_five','Forty_six','Forty_seven','Forty_eight','Forty_nine','Fifty','Fifty_one','Fifty_two','Fifty_three','Fifty_four','Fifty_five','Fifty_six','Fifty_seven','Fifty_eight','Fifty_nine','Sixty','Sixty_one','Sixty_two','Sixty_three','Sixty_four','Sixty_five','Sixty_six','Sixty_seven','Sixty_eight','Sixty_nine','Seventy','Seventy_one','Seventy_two','Seventy_three','Seventy_four','Seventy_five','Seventy_six','Seventy_seven','Seventy_eight','Seventy_nine','Eighty','Eighty_one','Eighty_two','Eighty_three','Eighty_four','Eighty_five','Eighty_six','Eighty_seven','Eighty_eight','Eighty_nine','Ninety','Ninety_one','Ninety_two','Ninety_three','Ninety_four','Ninety_five','Ninety_six','Ninety_seven','Ninety_eight','Ninety_nine','Hundred');
        if(isset($numeros[$posicion])){
            return $numeros[$posicion];
        }else{
            return 'No existe numero';
        }
    }
}

?>
