<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
use App\Plan_trabajo;
use App\Plan_trabajo_descripcion;
use App\Oportunidades;
use App\AprobacionPlanTrabajos;
use App\User;
use App\SubtipoActividad;
use App\Empresa;
use App\Configuracion;
use Auth;

class Plan_trabajoController extends Controller
{
	public function AjaxPlanTrabajo($id)
	{
		$planTrabajo = Plan_trabajo_descripcion::where('plan_trabajo_descripcions.id', $id)
									->leftJoin('subtipo_actividads', 'plan_trabajo_descripcions.subtipo', '=', 'subtipo_actividads.id')
									->select('plan_trabajo_descripcions.*', 'subtipo_actividads.concepto')->latest()->first();
		if(!empty($planTrabajo->oportunidad)){
			$oportunidad = Oportunidades::find($planTrabajo->oportunidad);
			$planTrabajo->oportunidadDato = isset($oportunidad->empresa).", ".isset($oportunidad->pais);
			$planTrabajo->oportunidad = $planTrabajo->oportunidad;
		}elseif (!empty($planTrabajo->empresa_id)) {
			$oportunidad = Empresa::find($planTrabajo->empresa_id);
			$planTrabajo->oportunidadDato = $oportunidad->nombre;
			$planTrabajo->oportunidad = $planTrabajo->empresa_id;
		}else {
			$planTrabajo->oportunidadDato ="";
			$planTrabajo->oportunidad = "";

		}
		return response()->json($planTrabajo);
	}

    public function eliminar($id){
        $model = Plan_trabajo_descripcion::find($id);
        if (isset($model) && !empty($model)) {
            $model->delete();
            return \Response::json(['success' => true]);
        }else{
            return \Response::json(['success' => false]);
        }
    }

    public function listPlanesTrabajo($id, Request $request){
    	$model=DB::select("SELECT * FROM plan_trabajo_descripcions WHERE fecha >= :start AND fecha <= :end AND user_id = :id ORDER BY tipo_actividad ASC", ['id' => $id, 'start' => $request->start, 'end' => $request->end]);

    	$valid_tags = [];
			foreach ($model as $tag) {
				try {
							$hora_inicio   = explode(":",$tag->hora_inicio);
	            $hora_fin      = explode(":",$tag->hora_fin);

	            $start = \Carbon\Carbon::createFromTime($hora_inicio[0], $hora_inicio[1], 0, 'America/Bogota');
	            $end   = \Carbon\Carbon::createFromTime($hora_fin[0], $hora_fin[1], 0, 'America/Bogota');

	            //var_dump("start-> ".$start->format('h:i:s')."<br>"."end->   ".$end->format('h:i:s')."<br>");

							if(!empty($tag->oportunidad)){
	              $oportunidad = Oportunidades::find($tag->oportunidad);
								$tag->oportunidad_id = $tag->oportunidad;
	              $tag->oportunidad = isset($oportunidad->empresa).", ".isset($oportunidad->pais);
	            }elseif (!empty($tag->empresa_id)) {
	              $oportunidad = Empresa::find($tag->empresa_id);
	              $tag->oportunidad = $oportunidad->nombre;
								$tag->oportunidad_id = $tag->empresa_id;
	            }else {
	              $tag->oportunidad ="";
								$tag->oportunidad_id = "";
	            }
	            // $tag->subtipo = isset($tag->subtipo) ? $tag->subtipo : "";

							if (!empty($tag->subtipo)) {
								$subActividad = SubtipoActividad::find($tag->subtipo);
								$tag->subtipotexto = $subActividad->concepto;
							}else {
								$tag->subtipotexto = "";
							}

	            $aprobaciones = AprobacionPlanTrabajos::where('plantrabajo_id', $tag->id)->get();
	            $model = [];
	            foreach ($aprobaciones as $key => $aprobacion) {
	                $user = User::findOrFail($aprobacion->user_id);
	                $aprobacion->user = $user;
	                $model[] = $aprobacion;
	            }

	            if (isset($tag->estado_aprobacion) && !empty($tag->estado_aprobacion)) {
	                if ($tag->estado_aprobacion == "SinRevisar"){
	                    $tag->color = "#ea8900";
	                }elseif ($tag->estado_aprobacion == "Aprobado") {
	                    $tag->color = "#016090";
	                }else{
	                    $tag->color = "#D95350";
	                }
	            }else{
	              $tag->color = "#363C41";
	            }

							$valid_tags[] = ['id' => $tag->id, 'subtipo' => $tag->subtipo,'title' => $tag->tipo_actividad."\n".$tag->subtipotexto, 'start' => $tag->fecha."T".$start->format('H:i:s'), 'end' => $tag->fecha."T".$end->format('H:i:s'), 'overlap' => false, 'color' => $tag->color, 'oportunidad' => $tag->oportunidad, 'observaciones' => $tag->observaciones, 'tipo_actividad' => $tag->tipo_actividad,'oportunidad_id' => $tag->oportunidad_id, 'allDay' => false, "comentario" => $model];



				} catch (Exception $e) {}
			}
			return response()->json($valid_tags);
    }

    public function gesAprobar(Request $request)
    {
        $dato = Plan_trabajo_descripcion::find($request->id);
        if (isset($dato) && !empty($dato)) {
            $dato->estado_aprobacion = $request->text;
            $dato->save();

            $aprobador = new AprobacionPlanTrabajos;
            $aprobador->estado = $request->text;
            $aprobador->detalle = $request->detalleaprobar;
            $aprobador->plantrabajo_id = $dato->id;
            $aprobador->user_id = Auth::user()->id;
            $aprobador->save();

            return \Response::json(['success' => true]);
        }else{
            return \Response::json(['success' => false]);
        }
    }

    public function index(Request $request)
    {
    	$modulo=9;
    	$permiso_filtro='No';
      $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso[0]->id)){
        $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo[0]->id)){
          $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso[0]->permiso)){
		  			if($permiso[0]->permiso == "Si"){$permiso_filtro="Si";}else{$permiso_filtro="No";}
          }
        }
      }

      $permiso_aprobar='No';
      $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Aprobar plan de trabajo" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso3[0]->id)){
        $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo3[0]->id)){
          $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso3[0]->permiso)){
            if($permiso3[0]->permiso == "Si"){
              $permiso_aprobar="Si";
            }else{
              $permiso_aprobar="No";
            }
          }
        }
      }

      $permiso_filtro_semana='No';
      $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select semana" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso1[0]->id)){
        $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo1[0]->id)){
          $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso1[0]->permiso)){
		  			if($permiso1[0]->permiso == "Si"){$permiso_filtro_semana="Si";}else{$permiso_filtro_semana="No";}
          }
        }
      }
      $permiso_crear_actividad='No';
      $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear actividad" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso2[0]->id)){
        $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo2[0]->id)){
          $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso2[0]->permiso)){
		  			if($permiso2[0]->permiso == "Si"){$permiso_crear_actividad="Si";}else{$permiso_crear_actividad="No";}
          }
        }
      }
      $data['permiso_actividad_grupal']='No';
      $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear actividad en grupo" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso3[0]->id)){
        $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo3[0]->id)){
          $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso3[0]->permiso)){
		  	if($permiso3[0]->permiso == "Si"){$data['permiso_actividad_grupal']="Si";}else{$data['permiso_actividad_grupal']="No";}
          }
        }
      }
  		$oportunidades = Oportunidades::where('estado','pendiente')->orWhere('estado','vendido')->get();
      /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Plan de trabajo" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){$data['item_menu']='menu_'.$data['item_menu'][0]->id;}else{$data['item_menu']='';}
      /*Fin Consulta para activar el item del menu correspondiente*/
        //Eduard: Dias de Plan de trabajo
        $dias = Configuracion::find(2);
        $data['diaslimite'] = $dias->valor;
        //Eduard: Fin Dias de Plan de trabajo
  		return view('plantrabajo.calendar',['data' => $data, 'oportunidades'=>$oportunidades, 'permiso_filtro' => $permiso_filtro, 'permiso_filtro_semana' => $permiso_filtro_semana, 'permiso_crear_actividad' => $permiso_crear_actividad, 'permiso_aprobar'=>$permiso_aprobar]);
    }
    public function action(Request $request){
        $accion = $request->action;
        switch($accion){
            /*Caso en el que la actividad del plan de trabajo sea grupal*/
            case 0:
                $usuarios = User::where('cargo','Ejecutivo Comercial')->orWhere('cargo','Gerente Comercial')->orWhere('cargo','Partner Solution')->orWhere('cargo','Administrador Smart')->get();
                ob_start();
                foreach($usuarios as $usuario){ if(Auth::user()->id != $usuario->id){ ?>
<img src="/images/file/clientes/<?=$usuario->foto?>" class="img-rounded no-select" id="usuario_<?=$usuario->id?>" title="<?=$usuario->name?>" style="width: 60px;">
                <?php
                } }
                $data['html']=ob_get_contents();
                ob_end_clean();
                break;
        }
        return \Response::json(['data' => $data]);
    }

		public function pruebaindex(Request $request)
    {
    	$modulo=9;
    	$permiso_filtro='No';
      $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select usuario" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso[0]->id)){
        $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo[0]->id)){
          $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso[0]->permiso)){
		  			if($permiso[0]->permiso == "Si"){$permiso_filtro="Si";}else{$permiso_filtro="No";}
          }
        }
      }

      $permiso_aprobar='No';
      $acceso3 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Aprobar plan de trabajo" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso3[0]->id)){
        $cargo3 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo3[0]->id)){
          $permiso3 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso3[0]->permiso)){
            if($permiso3[0]->permiso == "Si"){
              $permiso_aprobar="Si";
            }else{
              $permiso_aprobar="No";
            }
          }
        }
      }

      $permiso_filtro_semana='No';
      $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select semana" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso1[0]->id)){
        $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo1[0]->id)){
          $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso1[0]->permiso)){
		  			if($permiso1[0]->permiso == "Si"){$permiso_filtro_semana="Si";}else{$permiso_filtro_semana="No";}
          }
        }
      }
      $permiso_crear_actividad='No';
      $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear actividad" AND `id_permisomodulo`="'.$modulo.'"');
      if(isset($acceso2[0]->id)){
        $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
        if(isset($cargo2[0]->id)){
          $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
          if(isset($permiso2[0]->permiso)){
		  			if($permiso2[0]->permiso == "Si"){$permiso_crear_actividad="Si";}else{$permiso_crear_actividad="No";}
          }
        }
      }
  		$oportunidades = Oportunidades::where('estado','pendiente')->orWhere('estado','vendido')->get();
      /*Consulta para activar el item del menu correspondiente*/
      $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Plan de trabajo" ORDER BY `orden` DESC LIMIT 0, 1';
      $data['item_menu'] = DB::select($query);
      if(count($data['item_menu']) > 0){$data['item_menu']='menu_'.$data['item_menu'][0]->id;}else{$data['item_menu']='';}
      /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('xx.xx2',['data' => $data, 'oportunidades'=>$oportunidades, 'permiso_filtro' => $permiso_filtro, 'permiso_filtro_semana' => $permiso_filtro_semana, 'permiso_crear_actividad' => $permiso_crear_actividad, 'permiso_aprobar'=>$permiso_aprobar]);
    }

    public function saveplan(Request $request){
        if($request->usuarios_actividades_grupo == ''){
			$plantrabajo = new Plan_trabajo;
    	    $plantrabajo->semana = $request->semana;
			$plantrabajo->estado = "activo";
			$plantrabajo->user_id=Auth::user()->id;
			$plantrabajo->save();
			$id=$plantrabajo->id;

			$plantrabajodes = new Plan_trabajo_descripcion;
			$plantrabajodes->plan_trabajo=$id;
			$plantrabajodes->fecha=$request->fecha;
			$plantrabajodes->jornada=$request->jornada;
			$plantrabajodes->hora_inicio=$request->hora_inicio;
			$plantrabajodes->hora_fin=$request->hora_fin;
			$plantrabajodes->observaciones=$request->observaciones;
			$plantrabajodes->tipo_actividad=$request->tipo_actividad;
			$plantrabajodes->subtipo=$request->subtipo;
			$plantrabajodes->empresa_id=$request->empresa_id;
			$plantrabajodes->oportunidad=$request->oportunidad;
			$plantrabajodes->fuera_tiempo=$request->fuera_tiempo;
			$plantrabajodes->estado="pendiente";
			$plantrabajodes->user_id=Auth::user()->id;
			$plantrabajodes->save();
        }else{
            $users = explode('%%',$request->usuarios_actividades_grupo);
            $nombres = '';
            foreach($users as $user){
                $U = User::find($user);
                $nombres.= $U->name.', ';
            }
            $observacion = 'Creado por: '.Auth::user()->nombres.' '.Auth::user()->apellidos.', Cargo: '.Auth::user()->cargo.', el día: '.date('d').' de '.$this->mes(date('m')).' '.date('Y').' a las '.date("h:i:s A").'. Personas incluidas en la actividad del plan de trabajo ( '.$nombres.') ';
            $plantrabajo = new Plan_trabajo;
    	    $plantrabajo->semana = $request->semana;
			$plantrabajo->estado = "activo";
			$plantrabajo->user_id=Auth::user()->id;
			$plantrabajo->save();
			$id=$plantrabajo->id;

			$plantrabajodes = new Plan_trabajo_descripcion;
			$plantrabajodes->plan_trabajo=$id;
			$plantrabajodes->fecha=$request->fecha;
			$plantrabajodes->jornada=$request->jornada;
			$plantrabajodes->hora_inicio=$request->hora_inicio;
			$plantrabajodes->hora_fin=$request->hora_fin;
			$plantrabajodes->observaciones=$observacion.$request->observaciones;
			$plantrabajodes->tipo_actividad=$request->tipo_actividad;
			$plantrabajodes->subtipo=$request->subtipo;
			$plantrabajodes->empresa_id=$request->empresa_id;
			$plantrabajodes->oportunidad=$request->oportunidad;
			$plantrabajodes->fuera_tiempo=$request->fuera_tiempo;
			$plantrabajodes->estado="pendiente";
			$plantrabajodes->user_id=Auth::user()->id;
			$plantrabajodes->save();

            foreach($users as $user){
                $plantrabajo = new Plan_trabajo;
                $plantrabajo->semana = $request->semana;
                $plantrabajo->estado = "activo";
                $plantrabajo->user_id=$user;
                $plantrabajo->save();
                $id=$plantrabajo->id;

                $plantrabajodes = new Plan_trabajo_descripcion;
                $plantrabajodes->plan_trabajo=$id;
                $plantrabajodes->fecha=$request->fecha;
                $plantrabajodes->jornada=$request->jornada;
                $plantrabajodes->hora_inicio=$request->hora_inicio;
                $plantrabajodes->hora_fin=$request->hora_fin;
                $plantrabajodes->observaciones=$observacion.$request->observaciones;
                $plantrabajodes->tipo_actividad=$request->tipo_actividad;
                $plantrabajodes->subtipo=$request->subtipo;
                $plantrabajodes->empresa_id=$request->empresa_id;
                $plantrabajodes->oportunidad=$request->oportunidad;
                $plantrabajodes->fuera_tiempo=$request->fuera_tiempo;
                $plantrabajodes->estado="pendiente";
                $plantrabajodes->user_id=$user;
                $plantrabajodes->save();
            }
        }
			return \Response::json(['success' => true]);
	}

	public function editplan(Request $request){
		$plantrabajodes = Plan_trabajo_descripcion::find($request->id);
		$plantrabajodes->fecha=$request->fecha;
		$plantrabajodes->jornada=$request->jornada;
		$plantrabajodes->hora_inicio=$request->hora_inicio;
		$plantrabajodes->hora_fin=$request->hora_fin;
		$plantrabajodes->observaciones=$request->observaciones;
		$plantrabajodes->tipo_actividad=$request->tipo_actividad;
		$plantrabajodes->subtipo=$request->subtipo;
		$plantrabajodes->empresa_id=$request->empresa_id;
		$plantrabajodes->oportunidad=$request->oportunidad;
		$plantrabajodes->fuera_tiempo=$request->fuera_tiempo;
		$plantrabajodes->save();

		return \Response::json(['success' => true]);
	}
    protected function mes($mes){
        $mes = intval($mes);
        $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        return $meses[$mes];
    }
}
