<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Archivos;


class StorageController extends Controller
{
	//protected $oPhpPresentation;
    //protected $htmlOutput;

	public function index()
    {
        return view('new');
    }
    /**
	* guarda un archivo en nuestro directorio local.
	*
	* @return Response
	*/
	public function save(Request $request)
	{

		// Estructura de la carpeta deseada
		//$estructura = './nivel1/nivel2/nivel3/';

		// Para crear una estructura anidada se debe especificar
		// el parámetro $recursive en mkdir().

		//if(!mkdir($estructura, 0777, true)) {
		   // die('Fallo al crear las carpetas...');
		//}
	 
		//obtenemos el campo file definido en el formulario
		$file = $request->file('file');

		//obtenemos el nombre del archivo
		$antenombre = sha1(date("Y-m-d H:i:s"));
		$extension = $file->getClientOriginalExtension();
		$nombre =$antenombre.".".$extension;

		//indicamos que queremos guardar un nuevo archivo en el disco local
		\Storage::disk('local')->put($nombre,  \File::get($file));
		//Storage::disk('google')->put($nombre, \File::get($file));

		$dato = new Archivos;
		$dato->name = $nombre;
		$dato->extesion = $extension;  
		$dato->username= Auth::user()->name;
		$dato->user_id = Auth::user()->id;
		$dato->save();

		return "archivo guardado ".Auth::user()->name." - ".Auth::user()->id  ;
	}



	

    
}
