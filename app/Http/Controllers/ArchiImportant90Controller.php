<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoriaNoArchivo;
use App\ArchivosNoImportante;
use App\ArchivoNoComentario;
use App\User;
use Auth;

date_default_timezone_set("America/Bogota");

class ArchiImportant90Controller extends Controller
{
    public function getCategoria($id){
    	$datos = CategoriaNoArchivo::all();
    	$valid_tags = [];
		foreach ($datos as $tag) {
			$results = DB::select('SELECT COUNT(id) AS archivo FROM `archivos_no_importantes` WHERE cat_id = :id AND oportunidad_id = :idop', ['id' => $tag->id, 'idop' => $id]);
			$tag->archivo = $results[0]->archivo;
			$valid_tags[]=$tag;
		}
    	return \Response::json(['success' => true, 'datos' => $valid_tags]);
    }

    public function getArchivo($id, $idop){
    	$cate = CategoriaNoArchivo::find($id);
    	$datos = ArchivosNoImportante::where('cat_id', $id)->where('oportunidad_id', $idop)->get();
    	foreach ($datos as $tag) {
			$results = DB::select('SELECT COUNT(id) AS comentarios FROM `archivo_no_comentarios` WHERE arch_id = :id', ['id' => $tag->id]);
			$tag->comentarios = $results[0]->comentarios;
			$valid_tags[]=$tag;
		}
    	return \Response::json(['success' => true, 'datos' => $datos, 'cate' => $cate]);
    }

    public function getComentarios($id){
    	$datos = ArchivoNoComentario::where('arch_id', $id)->get();
    	$model = [];
    	foreach ($datos as $key => $value) {
    		$user = User::findOrFail($value->user_id);
    		$value->user = $user; 
    		$model[] = $value;
    	}
    	return \Response::json(['success' => true, 'datos' => $model]);
    }

    public function save(Request $request)
    {
		$logo = $request->foto_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombreprincipal =$antenombre.".png";
            \Storage::disk('catearchivo')->put($nombreprincipal,  $foto);
        }

	    $dato = new CategoriaNoArchivo;
		$dato->nombre = $request->nombre;
		
		if (isset($nombreprincipal)) {
			$dato->foto = $nombreprincipal;
		}
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

    public function saveArchivo(Request $request)
    {
    	$principal = $request->file('file');
		if ($principal) {			
			$antenombreprincipal = uniqid();
			$extensionprincipal = $principal->getClientOriginalExtension();
			$nombreprincipal = $antenombreprincipal.".".$extensionprincipal;
			\Storage::disk('archivosi')->put($nombreprincipal,  \File::get($principal));
		}

	    $dato = new ArchivosNoImportante;
		$dato->nombre = $request->nombre;
		$dato->oportunidad_id = $request->idop;
		$dato->comentario = $request->comentario;
		$dato->cat_id = $request->cat_id;
		if (isset($nombreprincipal)) {
			$dato->file = $nombreprincipal;
		}
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }

    public function saveComentario(Request $request)
    {
	    $dato = new ArchivoNoComentario;
		$dato->comentario = $request->comentario;
		$dato->arch_id = $request->arch_id;
		$dato->user_id = Auth::user()->id;
		$dato->save();
		return \Response::json(['success' => true]);
    }
}