<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\Producto;
use App\FeriasBitacora;
use App\ProductoReferencias;
use App\Ferias_multimedias;
use App\FeriasConference;
use App\Ferias_exponentes;
use App\Proveedore;
use App\FeriasBitacorasProductos;
use App\FeriasNecesidade;
use App\FeriasNecesidadesProveedore;
use App\FeriasNecesidadesComentario;
use App\FeriasInvitado;
use App\FeriasPatrocinadore;
use App\FeriasSolicitudesPatrocinio;
use App\FeriasOportunidade;
use App\FeriasCliente;
use App\Paises;
use App\Estados;
use App\Ciudades;
use APP\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
date_default_timezone_set("America/Bogota");

class FeriasController extends Controller
{
    public function crear(){
        /*Permiso para crear una feria*/
        $modulo=21;
        $data['permiso_crear']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_crear']="Si";
              }
            }
          }
        }
        $paises = Paises::all();
        $proveedores=Proveedore::all();
        $patrocinadores=FeriasPatrocinadore::all();
        $boton['proveedores']=count($proveedores);
        $boton['patrocinadores']=count($patrocinadores);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        if($data['permiso_crear']=="Si"){
            return view('ferias.crearferia',['data' => $data, 'paises' => $paises, 'boton'=>$boton]);
        }else{
            return view('oportunidades.nopermiso');
        }
    }

    public function jsonFeria($id){
        $dato = Feria::find($id);
        $ubicacion = explode('%%',$dato->ubicacion);
        $pais = Paises::find($ubicacion[0]);
        $estado = Estados::find($ubicacion[1]);
        $ciudad = Ciudades::find($ubicacion[2]);
        $u = $pais->name.' '.$estado->name.', '.$ciudad->name;
        $galeria_archivo = Ferias_multimedias::where("id_feria",$id)->where("tipo","Archivo")->get();
        $galeria_link = Ferias_multimedias::where("id_feria",$id)->where("tipo","Link")->get();
        return \Response::json(['success' => 'true', 'model' => $dato, 'total_link' => count($galeria_link), 'total_archivo' => count($galeria_archivo), 'u' => $u]);
    }

    public function consultargaleria($id){
        $galeria= DB::select('SELECT * FROM `ferias_multimedias` WHERE `id_feria`="'.$id.'" ORDER BY `name` ASC');
        $imagenes='';
        $videos='';
        $archivos='';
        $contador['imagenes']=0;
        $contador['videos']=0;
        $contador['archivos']=0;
        foreach($galeria as $g){
            $extension=explode('.',$g->contenido);
            $nombre=$g->name;
            if($extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                $imagenes.='<div class="col-md-2 col-archivo">
                <a data-gallery href="/storage/ferias/multimedia/'.$g->contenido.'">
                <img src="/storage/ferias/multimedia/'.$g->contenido.'" onclick="setTimeout(\'cerrar_modal()\',3000);" class="icono-video" onclick="abrir(\'imagen\',\''.$g->contenido.'\')" data-toggle="tooltip" data-placement="bottom" title="'.$nombre.'">
                </a></div>';
                $contador['imagenes']++;
            }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                $videos.='<div class="col-md-2 col-archivo">
                <img src="/images/iconosferias/video-ferias.png" class="icono-video" onclick="abrir(\'video\',\''.$g->contenido.'\')" data-toggle="tooltip" data-placement="bottom" title="'.$nombre.'"><br>
            </div>';
                $contador['videos']++;
            }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                $archivos.='<div class="col-md-1 col-archivo">
                <img src="/images/iconosferias/archivo-ferias.png" class="icono-video" onclick="abrir(\'archivo\',\''.$g->contenido.'\')" data-toggle="tooltip" data-placement="bottom" title="'.$nombre.'"><br>
            </div>';
                $contador['archivos']++;
            }
        }
        ob_start(); ?>
            <div class="widget-header">
                <h2>Galeria</h2>
            </div>
            <div class="widget-content">
                <div class="row">
                    <div class="col-md-1">

                    </div>
                    <div class="col-md-3 seleccionado">
                        <img src="http://megaarchivo.smartessi.com/multimedia/explorador/play_blanco.png" class="video imagen"><a>Videos (<?= $contador['videos']?>)</a>
                    </div>
                    <div class="col-md-3 no_seleccionado">
                        <img src="http://megaarchivo.smartessi.com/multimedia/explorador/Img_gris.png" class="foto imagen"><a>Imagenes (<?= $contador['imagenes']?>)</a>
                    </div>
                    <div class="col-md-3 no_seleccionado">
                        <img src="http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_gris.png" class="carpeta imagen"><a>Documentos (<?= $contador['archivos']?>)</a>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="row margen-top archivos" id="cont-videos">
                    <?php echo $videos ?>
                </div>

                <div class="row margen-top archivos" id="cont-imagenes">
                    <?php echo $imagenes ?>
                </div>

                <div class="row margen-top archivos" id="cont-archivos">
                    <?php echo $archivos ?>
                </div>
            </div>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function consultarlinks($id){
        $links= DB::select('SELECT * FROM `ferias_multimedias` WHERE `id_feria`="'.$id.'" AND `tipo`="Link"');
        ob_start(); ?>
        <div class="widget widget-table">
            <div class="widget-header">
                <h3><i class="fa fa-link"></i> Links de la feria </h3></div>
            <div class="widget-content">
                <table class="table table-condensed table-dark-header">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center">Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach($links as $link){ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $link->contenido ?></td>
                        </tr>
                        <?php $i++; }
                        if(count($links) == 0){ ?>
                        <tr>
                            <td colspan="2" class="text-center">No hay registros....</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function getFeriasPosibles(){
        $fecha_actual = date("Y-m-d H:i:s");
        $categorias= DB::select('SELECT DISTINCT(`categoria`) FROM `productos`');
        $datos = Feria::where("estado","Posible")
                      ->where("fecha_fin", ">=", $fecha_actual)->get();
        $responsables_essi=DB::select('SELECT * FROM `users` WHERE `cargo`="Partner Solution" OR `cargo`="Ejecutivo Comercial" OR `cargo`="Gerente Comercial" ORDER BY `id`');
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.posible',['data' => $data, 'datos' => $datos, 'categorias' => $categorias, 'responsables_essi' => $responsables_essi]);
    }

    public function listadecategorias(){
        $categorias= DB::select('SELECT DISTINCT(`categoria`) FROM `productos`');
        ob_start(); ?>
            <h6>Categoria</h6>
            <select id="tipo" class="form-control">
                <option value="" disabled selected>Seleccione una categoria</option>
                <?php foreach($categorias as $categoria){ ?>
                <option value="<?= $categoria->categoria ?>"><?= $categoria->categoria ?></option>
                <?php } ?>
            </select><br>
            <h6>Maquina</h6>
            <select id="maquina" class="form-control">
                <option value="" disabled selected>Seleccione una categoria</option>
            </select><br>
            <h6>Referencia</h6>
            <select id="referencia" class="form-control">
                <option value="" disabled selected>Seleccione una categoria</option>
            </select>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function getFeriasPerdidas(){
        $fecha_actual = date("Y-m-d H:i:s");
        $datos = Feria::where("estado","Posible")
                      ->where("fecha_fin", "<", $fecha_actual)->get();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.perdida',['data' => $data, 'datos' => $datos]);
    }

    public function getFeriasAsistidas(){
        $datos = Feria::where("estado","Activo")->get();
        ob_start(); ?>
        <style>
            <?php $con=0; foreach($datos as $dato){ ?>
            .menuR<?= $con ?> {
              -webkit-filter: url("#shadowed-goo");
                      filter: url("#shadowed-goo");
            }

             .menu-itemR<?= $con ?>,  .menu-open-buttonR<?= $con ?> {
              background: #041d60;
              border-radius: 100%;
              width: 80px;
              height: 80px;
              margin-left: -40px;
              position: absolute;
              top: 20px;
              color: white;
              text-align: center;
              line-height: 80px;
              -webkit-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
              -webkit-transition: -webkit-transform ease-out 200ms;
              transition: -webkit-transform ease-out 200ms;
              transition: transform ease-out 200ms;
              transition: transform ease-out 200ms, -webkit-transform ease-out 200ms;
            }

             .menu-openR<?= $con ?> {
              display: none;
            }

             .hamburgerR<?= $con ?> {
              width: 25px;
              height: 3px;
              background: white;
              display: block;
              position: absolute;
              top: 50%;
              left: 50%;
              margin-left: -12.5px;
              margin-top: -1.5px;
              -webkit-transition: -webkit-transform 200ms;
              transition: -webkit-transform 200ms;
              transition: transform 200ms;
              transition: transform 200ms, -webkit-transform 200ms;
            }

             .hamburger-1 {
              -webkit-transform: translate3d(0, -8px, 0);
                      transform: translate3d(0, -8px, 0);
            }

             .hamburger-2 {
              -webkit-transform: translate3d(0, 0, 0);
                      transform: translate3d(0, 0, 0);
            }

             .hamburger-3 {
              -webkit-transform: translate3d(0, 8px, 0);
                      transform: translate3d(0, 8px, 0);
            }

             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-1 {
              -webkit-transform: translate3d(0, 0, 0) rotate(45deg);
                      transform: translate3d(0, 0, 0) rotate(45deg);
            }
             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-2 {
              -webkit-transform: translate3d(0, 0, 0) scale(0.1, 1);
                      transform: translate3d(0, 0, 0) scale(0.1, 1);
            }
             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> .hamburger-3 {
              -webkit-transform: translate3d(0, 0, 0) rotate(-45deg);
                      transform: translate3d(0, 0, 0) rotate(-45deg);
            }

            .menuR<?= $con ?> {
              /*position: absolute;*/
              left: 50%;
              margin-left: -119px;
              padding-top: 20px;
              padding-left: 80px;
              width: 650px;
              height: 100px;
              box-sizing: border-box;
              font-size: 20px;
              text-align: left;
            }

             .menu-itemR<?= $con ?>:hover {
              background: white;
              color: #041d60;
            }
             .menu-itemR<?= $con ?>:nth-child(3) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(4) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(5) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
             .menu-itemR<?= $con ?>:nth-child(6) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }
            .menu-itemR<?= $con ?>:nth-child(7) {
              -webkit-transition-duration: 180ms;
                      transition-duration: 180ms;
            }

             .menu-open-buttonR<?= $con ?> {
              z-index: 2;
              -webkit-transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
                      transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
              -webkit-transition-duration: 400ms;
                      transition-duration: 400ms;
              -webkit-transform: scale(1.1, 1.1) translate3d(0, 0, 0);
                      transform: scale(1.1, 1.1) translate3d(0, 0, 0);
              cursor: pointer;
            }

             .menu-open-buttonR<?= $con ?>:hover {
              -webkit-transform: scale(1.2, 1.2) translate3d(0, 0, 0);
                      transform: scale(1.2, 1.2) translate3d(0, 0, 0);
            }

             .menu-openR<?= $con ?>:checked + .menu-open-buttonR<?= $con ?> {
              -webkit-transition-timing-function: linear;
                      transition-timing-function: linear;
              -webkit-transition-duration: 200ms;
                      transition-duration: 200ms;
              -webkit-transform: scale(0.8, 0.8) translate3d(0, 0, 0);
                      transform: scale(0.8, 0.8) translate3d(0, 0, 0);
            }

             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?> {
              -webkit-transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
                      transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
              -webkit-transition-duration: 190ms;
                      transition-duration: 190ms;
              -webkit-transform: translate3d(90px, 0, 0);
                      transform: translate3d(90px, 0, 0);
            }
            @media screen and (max-width: 1367px) {
                .menuR<?= $con ?> {
                    margin-left: -64px;
                }
                .menu-itemR<?= $con ?>, .menu-open-buttonR<?= $con ?>{
                    width: 50px;
                    height: 50px;
                    line-height: 57px;
                }
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
                      -webkit-transform: translate3d(55px, 0, 0);
                              transform: translate3d(55px, 0, 0);
                    }
            }
            @media screen and (max-width: 1600px) {
                .menuR<?= $con ?> {
                    margin-left: -64px;
                }
                .menu-itemR<?= $con ?>, .menu-open-buttonR<?= $con ?>{
                    width: 50px;
                    height: 50px;
                    line-height: 57px;
                }
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(3) {
                      -webkit-transform: translate3d(55px, 0, 0);
                              transform: translate3d(55px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
              -webkit-transition-duration: 290ms;
                      transition-duration: 290ms;
              -webkit-transform: translate3d(180px, 0, 0);
                      transform: translate3d(180px, 0, 0);
            }
            @media screen and (max-width: 1600px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(4) {
                      -webkit-transform: translate3d(110px, 0, 0);
                              transform: translate3d(110px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
              -webkit-transition-duration: 390ms;
                      transition-duration: 390ms;
              -webkit-transform: translate3d(270px, 0, 0);
                      transform: translate3d(270px, 0, 0);
            }
            @media screen and (max-width: 1600px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(5) {
                      -webkit-transform: translate3d(165px, 0, 0);
                              transform: translate3d(165px, 0, 0);
                    }
            }
             .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
              -webkit-transition-duration: 490ms;
                      transition-duration: 490ms;
              -webkit-transform: translate3d(360px, 0, 0);
                      transform: translate3d(360px, 0, 0);
            }
            @media screen and (max-width: 1600px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(6) {
                      -webkit-transform: translate3d(220px, 0, 0);
                              transform: translate3d(220px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
              -webkit-transition-duration: 590ms;
                      transition-duration: 590ms;
              -webkit-transform: translate3d(450px, 0, 0);
                      transform: translate3d(450px, 0, 0);
            }
            @media screen and (max-width: 1600px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(7) {
                      -webkit-transform: translate3d(275px, 0, 0);
                              transform: translate3d(275px, 0, 0);
                    }
            }
            .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
              -webkit-transition-duration: 690ms;
                      transition-duration: 690ms;
              -webkit-transform: translate3d(540px, 0, 0);
                      transform: translate3d(540px, 0, 0);
            }
            @media screen and (max-width: 1600px) {
                .menu-openR<?= $con ?>:checked ~ .menu-itemR<?= $con ?>:nth-child(8) {
                      -webkit-transform: translate3d(330px, 0, 0);
                              transform: translate3d(330px, 0, 0);
                    }
            }
            <?php $con++; } ?>
        </style>
        <?php
        $estilo = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.asistida',['data' => $data, 'datos' => $datos, "estilo" => $estilo]);
    }

    public function viewFeria($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $datos = Feria::find($id);
        $exponente = "";
        $htmls = '';
        if($datos->tipo === "Exponente"){
            $exponente = Ferias_exponentes::where('id_feria', $id)->first();
            $referencias = explode("%%",$exponente->equipos);
            $responsable = User::find($exponente->responsable);
            foreach ($referencias as $ref) {
                if(!empty($ref)){
                    $referencia = ProductoReferencias::find($ref);
                    $producto = Producto::find($referencia->producto_id);
                    try {
                        if(!empty($referencia->foto) && $referencia->foto != Null){
                            $foto = url('/')."/images/file/productos/".$referencia->foto;
                        }else{
                            $foto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Foto&w=100&h=100';
                        }
                        $html = '
                        <div class="col-3 placeholder img-sedes">
                            <div class="img-thumbnail img-sedes">
                              <a href="">
                                <img src="'.$foto.'" style="max-width: 50%;" class="img-fluid mx-auto d-block">
                              </a>
                            </div>
                            <div class="text-muted centrado">
                                <h6 style="margin-bottom: 0px;">'.$producto->name.' '.$referencia->referencia.'</h6>
                            </div>
                        </div>';
                    } catch (Exception $e) { }
                    $htmls .= $html;
                }
			}
        }else{
            $responsable = User::find($datos->user_id);
        }
        $ubication = explode('%%', $datos->ubicacion);
        $pais = Paises::find($ubication[0]);
        $estado = Estados::find($ubication[1]);
        $ciudad = Ciudades::find($ubication[2]);

        $datos['pais'] = $pais->name;
        $datos['estado'] = $estado->name;
        $datos['ciudad'] = $ciudad->name;
        $datos['invitados_total'] = Feriasinvitado::where('id_feria', $id)->count();
        if($datos->tipo === "Exponente"){
            $datos['invitados_essi'] = count(explode("%%", $exponente->cantidad_personas));
        }else{
            $datos['invitados_essi'] = 0;
        }


        $menu = $this->FeriaCommonMenu($id, 1);
        /*echo "<pre>";
        print_r($exponente);
        echo "</pre>";
        exit();*/
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.view', ['id' => $id, 'feria' => $datos, 'exponente' => $exponente, 'productos' => $htmls, 'responsable' => $responsable, 'menu' => $menu, 'data'=>$data]);
    }

    public function viewBitacora($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }

        $dato = Feria::find($id);
        $ubicacion = explode('%%',$dato->ubicacion);
        $pais = Paises::find($ubicacion[0]);
        $estado = Estados::find($ubicacion[1]);
        $ciudad = Ciudades::find($ubicacion[2]);
        $data['ubicacion'] = isset($pais->name)?$pais->name.',':'No hay Pais,';
        $data['ubicacion'].= isset($estado->name)?' '.$estado->name.' -':' No hay Estado -';
        $data['ubicacion'].= isset($ciudad->name)?' '.$ciudad->name:' No hay Ciudad';
        $menu = $this->FeriaCommonMenu($id, 5);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.bitacora', ['data' => $data, 'id' => $id, 'model' => $dato, 'menu' => $menu]);
    }

    public function viewDashboard($id){
        $menu = $this->FeriaCommonMenu($id, 5);
        $oportunidades = FeriasOportunidade::where('id_feria',$id)->get();
        $paises = DB::select('SELECT DISTINCT(O.pais) AS pais From ferias_oportunidades AS FO Inner Join oportunidades AS O On FO.id_oportunidad = O.id Where FO.id_feria = '.$id);
        $millones = DB::select('SELECT (SELECT HO.`value` FROM `historial_oportunidades` AS HO WHERE HO.`oportunidad_id` = FO.`id_oportunidad` AND HO.`key`="val_pesos" ORDER BY HO.`created_at` DESC LIMIT 0, 1) AS valor FROM `ferias_oportunidades` AS FO WHERE FO.`id_feria` = '.$id);
        $clientes = FeriasCliente::where('id_feria',$id)->get();
        $empresas = DB::select('SELECT DISTINCT(C.empresa_id) AS empresa From ferias_clientes AS FC Inner Join clientes AS C On FC.id_cliente = C.id Where FC.id_feria = '.$id);
        $valor_real = DB::select("SELECT valor FROM ferias_presupuesto_ajustes WHERE id_feria = ".$id);
        $proyeccion = DB::select("SELECT valor FROM ferias_presupuestos WHERE id_feria = ".$id);
        $patrocinio = DB::select("SELECT SUM(patrocinio_total) AS TOTAL FROM ferias_solicitudes_patrocinios WHERE estado = '1' AND id_feria = ".$id);
        $i=0;
        foreach($empresas as $e){
            $sede=DB::select('SELECT `pais` From `empresa_sedes` WHERE `empresa_id` = "'.$e->empresa.'" AND `importancia_sede`="Principal" ORDER BY `created_at` DESC LIMIT 0, 1');
            $pais_sede[$i]=$sede[0]->pais;
            $i++;
        }
        $data['paises_clientes'] = 0;
        if($i>0){
            $paises_unicos=array_unique($pais_sede);
            $data['paises_clientes'] = count($paises_unicos);
        }
        $data['oportunidades'] = count($oportunidades);
        $data['paises'] = count($paises);
        $total_millones=0;
        foreach($millones as $m){
            $total_millones+=intval(str_replace(",", "", $m->valor));
        }
        $data['millones'] = number_format(($total_millones / 1000000), 0, '.', ',');
        $data['clientes'] = count($clientes);
        $data['empresas'] = count($empresas);
        $data['real']=0;
        foreach($valor_real as $vr){
            $data['real']+=intval(str_replace("$ ", "", str_replace(".", "", $vr->valor)));
        }
        $data['proyeccion']=0;
        foreach($proyeccion as $p){
            $data['proyeccion']+=intval(str_replace("$ ", "", str_replace(".", "", $p->valor)));
        }
        $data['cumplimiento']=($data['real']/$data['proyeccion'])*100;
        $data['patrocinio']=intval($patrocinio[0]->TOTAL);
        if($data['patrocinio']==0){
            $data['patrocinado']=0;
        }else{
            $data['patrocinado']=($data['patrocinio']/$data['real'])*100;
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.dashbitacora', ['id' => $id, 'menu' => $menu, 'data' => $data]);
    }

    public function jsonBitDashVisitantes($id){
        $list = DB::select('SELECT COUNT(`id`) AS num, `fecha` FROM `ferias_bitacoras` WHERE `feria_id` = '.$id.' GROUP BY `fecha` ORDER BY `fecha` ASC');
        $total = DB::select('SELECT COUNT(`id`) AS num FROM `ferias_bitacoras` WHERE `feria_id` = '.$id);
        $total = $total[0];
        return \Response::json(['success' => 'true', 'total' => $total->num, 'list' => $list]);
    }

    public function jsonBitDashHorarios($id){
        $list = DB::select('SELECT COUNT(`id`) as num, HOUR(`hora`) as hora FROM `ferias_bitacoras` WHERE `feria_id` = '.$id.' GROUP BY HOUR(`hora`) ORDER BY `hora` ASC');
        return \Response::json(['success' => 'true', 'list' => $list]);
    }

    public function jsonBitDashPais($id){
        $list = DB::select('SELECT COUNT(F.id) AS num, F.pais, P.imagen FROM ferias_bitacoras F
                INNER JOIN paises P ON P.name = F.pais WHERE F.feria_id = '.$id.' GROUP BY F.pais, P.imagen  ORDER BY P.imagen');
        $total = DB::select('SELECT COUNT(DISTINCT `pais`) AS num FROM `ferias_bitacoras` WHERE `feria_id` = '.$id);
        $total = $total[0];
        return \Response::json(['success' => 'true', 'total' => $total->num, 'list' => $list]);
    }

    public function jsonBitDashProductos($id){
        $list = DB::select('SELECT COUNT(DISTINCT P.producto, P.referencia, P.id) AS num, P.producto, P.referencia FROM ferias_bitacoras_productos P INNER JOIN ferias_bitacoras B ON P.bitacora_id = B.id INNER JOIN ferias F ON B.feria_id = '.$id.' GROUP BY P.producto, P.referencia');
        return \Response::json(['success' => 'true', 'list' => $list]);
    }

    public function searchFeriaLast(){
        $dt     = \Carbon\Carbon::now();
        //$fecha  = $dt->format('Y-m-d g:i:s');
        $fecha  = $dt->toDateTimeString();
        $last = Feria::where('estado', 'Activo')
                    ->where('fecha_inicio', '<=' ,$fecha)
                    ->where('fecha_fin', '>=' ,$fecha)->get()->pop();
        if(!empty($last->id)) return redirect('/feria/ver/'.$last->id);
        else return redirect('/feriasasistidas');
    }

    public function jsonBitacora($id){
        $meses=['','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
        $dias=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
        $dato = FeriasBitacora::where("feria_id",$id)
                                ->orderBy('fecha', 'desc')
                                ->orderBy('hora', 'desc')
                                ->get();
        $j = count($dato) - 1;
        for ($i = 0; $i <= $j; $i++) {
            try{
                $user = User::find($dato[$i]->user_id);
                $nombre   = explode(" ",$user->nombres);
                $apellido = explode(" ",$user->apellidos);
                $dato[$i]->nombre_user = $nombre[0] ." ". $apellido[0];
                $fecha[$i] = $dias[intval(date('w', strtotime($dato[$i]->fecha)))].' '.date('d', strtotime($dato[$i]->fecha)).'/'.$meses[intval(date('m', strtotime($dato[$i]->fecha)))].'/'.date('Y', strtotime($dato[$i]->fecha)).' '.date("h:i:s A",strtotime($dato[$i]->hora));
            }catch (Exception $e) {}
        }
        return \Response::json(['success' => 'true', 'model' => $dato, 'fecha'=>$fecha]);
    }

    public function calendarFeria(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.calendar',['data' => $data]);
    }

    public function calendarFeriados(){
        $proveedores=Proveedore::all();
        $patrocinadores=FeriasPatrocinadore::all();
        $boton['proveedores']=count($proveedores);
        $boton['patrocinadores']=count($patrocinadores);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.calendario',['data' => $data, 'boton'=>$boton]);
    }

    public function jsonViewBitacora($id){
        $dato = FeriasBitacora::find($id);
        try{
            $user = User::find($dato->user_id);
            $nombre   = explode(" ",$user->nombres);
            $apellido = explode(" ",$user->apellidos);
            $dato->nombre_user = $nombre[0] ." ". $apellido[0];
        }catch (Exception $e) {}
        $getProducts = FeriasBitacorasProductos::where('bitacora_id',"=", $id)->get();

        $valid_tags = [];
        foreach($getProducts as $getProduct){
            $producto = Producto::where('name', 'like', '%'.$getProduct->producto.'%')
		                  ->get()->pop();
            $referencia = ProductoReferencias::where('producto_id',"=", $producto->id)
                                            ->where('referencia', 'like', '%'.$getProduct->referencia.'%')
                                            ->get()->pop();
            $referencia->producto = $getProduct->producto;
            $valid_tags[] = $referencia;
        }
        $feria = Feria::find($dato->feria_id);
        return \Response::json(['success' => 'true', 'model' => $dato, 'productos' => $valid_tags, 'feria'=> $feria]);
    }

    public function viewCamara($id){
        $data['menu'] = $this->FeriaCommonMenu($id, 6);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.camara',['data' => $data]);
    }

    public function pushCamara(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.pushcamara',['data' => $data]);
    }

    public function saveBitacora(Request $request){
        $inputs = $request->all();

        $dato = new FeriasBitacora;
        foreach($inputs as $clave => $valor){
            if(isset($valor) && !empty($valor) && $clave !== "productos"){
                $dato->$clave = $valor;
            }
        }
        $dato->user_id=Auth::user()->id;
        $dato->save();

        if(isset($request->productos) && !empty($request->productos)){
            foreach($request->productos as $valor){
                if(isset($valor['referencia']) && !empty($valor['referencia'])){
                    $producto = new FeriasBitacorasProductos;
                    $producto->bitacora_id = $dato->id;
                    $producto->producto = $valor['producto'];
                    $producto->referencia = $valor['referencia'];
                    $producto->save();
                }
            }
        }
        return \Response::json(['success' => 'true']);
    }

    public function jsonCalendarFerias(Request $request){
        /*$datos = Feria::where("fecha_inicio", ">=", $request->start)
                      ->where("fecha_fin", "<=", $request->end)->get();*/
        $datos = Feria::all();
        $fecha_actual = date("Y-m-d H:i:s");
        $valid_tags = [];
		foreach ($datos as $tag) {
			try {

                if ($tag->estado == "Posible") {
                    if ($tag->fecha_fin < $fecha_actual){
                        $tag->color = "#D95350";
                    }else{
                        $tag->color = "#D4AC0D";
                    }
                }elseif($tag->estado == "Activo"){
                    if($tag->tipo == "Exponente"){
                        $tag->color = "#16A085";
                    }else{
                        $tag->color = "#016090";
                    }
                }

    			$valid_tags[] = ['id' => $tag->id,'title' => $tag->nombre, 'start' => $tag->fecha_inicio, 'end' => $tag->fecha_fin, 'color' => $tag->color, 'dato' => $tag];
			} catch (Exception $e) {

			}
		}
        return response()->json($valid_tags);
        //return \Response::json(['success' => 'true', 'model' => $datos]);
    }

    public function guardarferia(Request $request){
        $dato = new Feria;
        $dato->nombre = $request->nombre;
        $dato->descripcion = $request->descripcion;
        $dato->ubicacion = $request->pais.'%%'.$request->departamento.'%%'.$request->ciudad;
        $dato->fecha_inicio = $request->fecha_inicio;
        $dato->fecha_fin = $request->fecha_fin;
        $dato->user_id=Auth::user()->id;
        $dato->estado = "Posible";
        if (isset($request->logo_values)) {
            $imagen = json_decode($request->logo_values);
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);
            $antenombre = sha1(date("Y-m-d H:i:s"));
            $nombre =$antenombre.".jpg";
            \Storage::disk('ferias')->put($nombre,  $foto);
            $dato->logo = $nombre;
        }
        $dato->save();
        $id = $dato->id;

        $links=$request->link;
        foreach($links as $link){
            $dato = new Ferias_multimedias;
            $dato->tipo = "Link";
            $dato->contenido = $link["contenido"];
            $dato->id_feria = $id;
            $dato->save();
        }
         return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function editar($id){
        $feria = Feria::findOrFail($id);
        $links = Ferias_multimedias::where('id_feria', $id)->where('tipo', 'Link')->get();
        $paises = Paises::all();
        $ubicacion=explode('%%',$feria->ubicacion);
        if(isset($ubicacion[0]) && isset($ubicacion[1])){
            $u[0]=Paises::find($ubicacion[0]);
            $u[1]=Estados::find($ubicacion[1]);
            $u[2]=Ciudades::find($ubicacion[2]);
        }else{
            $u="No existe";
        }

        $proveedores=Proveedore::all();
        $patrocinadores=FeriasPatrocinadore::all();
        $boton['proveedores']=count($proveedores);
        $boton['patrocinadores']=count($patrocinadores);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.editarferia',['data' => $data, 'feria' => $feria, 'links' => $links, 'paises' => $paises, 'u' => $u, 'boton' => $boton]);
    }

    public function editarferia(Request $request){
        $dato = Feria::findOrFail($request->id);
        $dato->nombre = $request->nombre;
        $dato->descripcion = $request->descripcion;
        $dato->ubicacion = $request->pais.'%%'.$request->departamento.'%%'.$request->ciudad;
        $dato->fecha_inicio = $request->fecha_inicio;
        $dato->fecha_fin = $request->fecha_fin;
        $dato->user_id=Auth::user()->id;
        //$dato->estado = "Posible";
        $logo_anterior=$dato->logo;
        if (isset($request->logo_values)) {
            if(!empty($request->logo_values)){
                $imagen = json_decode($request->logo_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('ferias')->put($nombre,  $foto);
                $dato->logo = $nombre;
                if($logo_anterior!=''){
                    Storage::delete('ferias/'.$logo_anterior);
                }
            }
        }
        $dato->save();

        $datos = Ferias_multimedias::where('id_feria', $request->id)->where('tipo', 'Link')->get();
        foreach($datos as $dato){
            $dato->delete();
        }

        $links=$request->link;
        foreach($links as $link){
            $dato = new Ferias_multimedias;
            $dato->tipo = "Link";
            $dato->contenido = $link["contenido"];
            $dato->id_feria = $request->id;
            $dato->save();
        }
        return \Response::json(['msj' => 'Editado Correctamente! '.$logo_anterior]);
    }

    public function cargargaleria($id){
         /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $datos = Ferias_multimedias::where('id_feria', $id)->where('tipo', 'Archivo')->get();
        $html='';
        foreach($datos as $dato){
            $nombre_archivo=$dato->name;
            $antiguo=$dato->name;
            $extension=explode('.',$dato->contenido);
            if($extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                $icono="image.png";
                $etiqueta_a='<a data-gallery href="/storage/ferias/multimedia/'.$dato->contenido.'"><img src="/images/iconosferias/'.$icono.'" style="background: none;"></a>';
            }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                $icono="video-file.png";
                $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
            }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                $icono="pdf.png";
                $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
            }else{
                $icono="";
                $etiqueta_a="";
            }
            /*Aplicar Permiso*/
            $elemento_img = 'onclick="no_permiso(\'Usted no tiene permisos para editar el archivo\')"';
            $elemento_img2 = 'onclick="no_permiso(\'Usted no tiene permisos para eliminar el archivo\')"';
            if($data['permiso_agregar_editar_eliminar']=="Si"){
                $elemento_img = 'onclick="rename('.$dato->id.',\''.$antiguo.'\')"';
                $elemento_img2 = 'onclick="deletefile('.$dato->id.')"';
            }
            $html.='<div class="col-md-2">
            <div class="row">
                <div class="col-md-10">
                    '.$etiqueta_a.'
                    <h6 style="color: cornflowerblue;">'.$nombre_archivo.'</h6>
                </div>
                <div class="col-md-1">
                    <div class="row">
                        <div class="col-md-12" style="top: 10px; cursor:pointer;">
                            <img src="/images/iconosferias/edit.png" title="Renombrar" '.$elemento_img.'>
                        </div>
                        <div class="col-md-12" style="margin-top: 25px; cursor:pointer;">
                            <img src="/images/iconosferias/delete.png" title="Eliminar" '.$elemento_img2.'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">
        </div>';
        }
        $proveedores=Proveedore::all();
        $patrocinadores=FeriasPatrocinadore::all();
        $boton['proveedores']=count($proveedores);
        $boton['patrocinadores']=count($patrocinadores);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.cargargaleria',['data' => $data, 'id' => $id, 'html' => $html, 'boton' => $boton]);
    }

    public function renamefile(Request $request){
        $dato = ferias_multimedias::findOrFail($request->id);
        $nombre_antiguo=explode('.',$dato->name);
        $request->nuevonombre=str_replace('.','',$request->nuevonombre);
        $dato->name = $request->nuevonombre.'.'.$nombre_antiguo[1];
        $dato->save();

        $datos = Ferias_multimedias::where('id_feria', $dato->id_feria)->where('tipo', 'Archivo')->get();
        $html='';
        foreach($datos as $dato){
            $nombre_archivo=$dato->name;
            $antiguo=$dato->name;
            $extension=explode('.',$dato->contenido);
            if($extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                $icono="image.png";
                $etiqueta_a='<a data-gallery href="/storage/ferias/multimedia/'.$dato->contenido.'"><img src="/images/iconosferias/'.$icono.'" style="background: none;"></a>';
            }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                $icono="video-file.png";
                $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
            }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                $icono="pdf.png";
                $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
            }else{
                $icono="";
                $etiqueta_a="";
            }
            $html.='<div class="col-md-2">
            <div class="row">
                <div class="col-md-10">
                    '.$etiqueta_a.'
                    <h6 style="color: cornflowerblue;">'.$nombre_archivo.'</h6>
                </div>
                <div class="col-md-1">
                    <div class="row">
                        <div class="col-md-12" style="top: 10px; cursor:pointer;">
                            <img src="/images/iconosferias/edit.png" title="Renombrar" onclick="rename('.$dato->id.',\''.$antiguo.'\')">
                        </div>
                        <div class="col-md-12" style="margin-top: 25px; cursor:pointer;">
                            <img src="/images/iconosferias/delete.png" title="Eliminar" onclick="deletefile('.$dato->id.')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">
        </div>';
        }
        return \Response::json(['msj' => $html]);
    }

    public function deletefile(Request $request){
        $dato = Ferias_multimedias::findOrFail($request->id);
        Storage::delete('ferias/multimedia/'.$dato->contenido);
        Ferias_multimedias::destroy($request->id);

        $datos = Ferias_multimedias::where('id_feria', $dato->id_feria)->where('tipo', 'Archivo')->get();
        $html='';
        foreach($datos as $dato){
            $nombre_archivo=$dato->name;
            $antiguo=$dato->name;
            $extension=explode('.',$dato->contenido);
            if($extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                $icono="image.png";
                $etiqueta_a='<a data-gallery href="/storage/ferias/multimedia/'.$dato->contenido.'"><img src="/images/iconosferias/'.$icono.'" style="background: none;"></a>';
            }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                $icono="video-file.png";
                $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
            }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                $icono="pdf.png";
                $etiqueta_a='<img src="/images/iconosferias/'.$icono.'" style="background: none;">';
            }else{
                $icono="";
                $etiqueta_a="";
            }
            $html.='<div class="col-md-2">
            <div class="row">
                <div class="col-md-10">
                    '.$etiqueta_a.'
                    <h6 style="color: cornflowerblue;">'.$nombre_archivo.'</h6>
                </div>
                <div class="col-md-1">
                    <div class="row">
                        <div class="col-md-12" style="top: 10px; cursor:pointer;">
                            <img src="/images/iconosferias/edit.png" title="Renombrar" onclick="rename('.$dato->id.',\''.$antiguo.'\')">
                        </div>
                        <div class="col-md-12" style="margin-top: 25px; cursor:pointer;">
                            <img src="/images/iconosferias/delete.png" title="Eliminar" onclick="deletefile('.$dato->id.')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">
        </div>';
        }

        return \Response::json(['msj' => $html]);
    }

    public function activar_posible($id){
        $dato = Feria::findOrFail($id);
        $dato->estado="Activo";
        $dato->tipo="Visitante";
        $dato->save();
        return \Response::json(['msj' => "Guardado"]);
    }

    public function buscarmaquina($id){
        $maquinas= DB::select('SELECT * FROM `productos` WHERE `categoria`="'.$id.'" ORDER BY `id` ASC');
        $html='';
        foreach($maquinas as $maquina){
            $html.='<option value="'.$maquina->id.'">'.$maquina->name.'</option>';
        }
        return \Response::json(['maquinas' => $html]);
    }

    public function buscarreferencia($id){
        $referencias= DB::select('SELECT * FROM `producto_referencias` WHERE `producto_id`="'.$id.'" ORDER BY `id` ASC');
        $html='';
        foreach($referencias as $referencia){
            $html.='<option value="'.$referencia->id.'">'.$referencia->referencia.'</option>';
        }
        return \Response::json(['referencia' => $html]);
    }

    public function imagenmaquina($id){
        $referencia= ProductoReferencias::findOrFail($id);
        $maquina= Producto::findOrFail($referencia->producto_id);
        $html='<div class="col-md-3" id="img-maquina'.$referencia->id.'">
                        <div class="row">
                            <div class="col-md-9 text-center">
                                <img class="img-maquinas" src="/images/file/productos/'.$referencia->foto.'">
                                <h6>'.$maquina->name.' - '.$referencia->referencia.'</h6>
                            </div>
                            <div class="col-md-3">
                                <i class="fa fa-times-circle eliminar-maqui" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick=\'eliminar_maquina("img-maquina'.$referencia->id.'","'.$referencia->id.'")\'></i>
                            </div>
                        </div>
                    </div>';
        return \Response::json(['imagen' => $html]);
    }

    public function guardarexponente(Request $request){
        $objetivos=$request->objetivos;
        $obj='';
        $alcances=$request->alcances;
        $alc='';
        $estrategias=$request->estrategia;
        $est='';
        $dato= Feria::findOrFail($request->id_feria);
        $dato->estado="Activo";
        $dato->tipo="Exponente";
        $dato->save();

        $dato = New Ferias_exponentes;

        if (isset($request->baner_values)) {
            if(!empty($request->baner_values)){
                $imagen = json_decode($request->baner_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('ferias')->put($nombre,  $foto);
                $dato->baner = $nombre;
            }
        }

        $dato->id_feria = $request->id_feria;
        $dato->equipos = $request->referencias;
        $dato->cantidad_personas = $request->cantidad_personas;

        $dato->responsable = $request->responsable;
        foreach($objetivos as $objetivo){
            if($obj==''){
                $obj=$objetivo;
            }else{
                $obj.='%%%___%%%'.$objetivo;
            }
        }
        $dato->objetivos = $obj;
        foreach($alcances as $alcance){
            if($alc==''){
                $alc=$alcance;
            }else{
                $alc.='%%%___%%%'.$alcance;
            }
        }
        $dato->alcances = $alc;
        foreach($estrategias as $estrategia){
            if($est==''){
                $est=$estrategia;
            }else{
                $est.='%%%___%%%'.$estrategia;
            }
        }
        $dato->estrategia = $est;
        $dato->atuendo = $request->atuendo;
        $dato->save();

        return \Response::json(['msj' => "Guardado Correctamente"]);
    }

    public function vergaleria($id){
        $galeria= DB::select('SELECT * FROM `ferias_multimedias` WHERE `id_feria`="'.$id.'" ORDER BY `name` ASC');
        //$galeria = $galeria->sortByDesc('contenido');
        $imagenes='';
        $videos='';
        $archivos='';
        $contador['imagenes']=0;
        $contador['videos']=0;
        $contador['archivos']=0;
        foreach($galeria as $g){
            $extension=explode('.',$g->contenido);
            //$nombre=explode('___',$extension[0]);
            $nombre=$g->name;
            if($extension[1]=='jpeg' || $extension[1]=='JPEG' || $extension[1]=='jpg' || $extension[1]=='JPG' || $extension[1]=='png' || $extension[1]=='PNG' || $extension[1]=='bmp' || $extension[1]=='BMP' || $extension[1]=='gif' || $extension[1]=='GIF' || $extension[1]=='tif' || $extension[1]=='TIF'){
                $imagenes.='<div class="col-md-2 col-archivo">
                <a data-gallery href="/storage/ferias/multimedia/'.$g->contenido.'">
                <img src="/storage/ferias/multimedia/'.$g->contenido.'" class="icono-video"  data-toggle="tooltip" data-placement="bottom" title="'.$nombre.'">
                </a></div>';
                $contador['imagenes']++;
            }else if($extension[1]=='avi' || $extension[1]=='AVI' || $extension[1]=='mpeg' || $extension[1]=='MPEG' || $extension[1]=='mov' || $extension[1]=='MOV' || $extension[1]=='wmv' || $extension[1]=='WMV' || $extension[1]=='rm' || $extension[1]=='RM' || $extension[1]=='flv' || $extension[1]=='FLV' || $extension[1]=='mp4' || $extension[1]=='MP4'){
                $videos.='<div class="col-md-2 col-archivo">
                <img src="/images/iconosferias/video-ferias.png" class="icono-video" onclick="abrir(\'video\',\''.$g->contenido.'\')"><br>
                <a>'.$nombre.'</a>
            </div>';
                $contador['videos']++;
            }else if($extension[1]=='pdf' || $extension[1]=='PDF'){
                $archivos.='<div class="col-md-1 col-archivo">
                <img src="/images/iconosferias/archivo-ferias.png" class="icono-video" onclick="abrir(\'archivo\',\''.$g->contenido.'\')" data-toggle="tooltip" data-placement="bottom" title="'.$nombre.'">
                </div>';
                $contador['archivos']++;
            }
        }
        $menu = $this->FeriaCommonMenu($id, 2);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.vergaleria',['data' => $data, 'id' => $id, 'galeria' => $galeria, 'imagenes' => $imagenes, 'videos' => $videos, 'archivos' => $archivos, 'contador' => $contador, 'menu' => $menu]);
    }

    public function verinviados($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $data['datos'] = Feria::findOrFail($id);
        $data['invitados_nuevos'] = FeriasInvitado::where('id_feria',$id)->where('tipo','Externo')->get();
        $data['invitados_clientes'] = DB::select('SELECT I.`id` AS id_principal, I.`id_cliente` AS id, (SELECT C.foto FROM `clientes` AS C WHERE C.`id`=I.`id_cliente`) AS foto, (SELECT CONCAT(C.`nombres`, " ", C.`apellidos`) FROM `clientes` AS C WHERE C.`id`=I.`id_cliente`) AS nombre, (SELECT (SELECT E.`nombre` FROM `empresas` AS E WHERE E.`id`=C.`empresa_id`) FROM `clientes` AS C WHERE C.`id`=I.`id_cliente`) AS empresa, (SELECT C.`cargo` FROM `clientes` AS C WHERE C.`id`=I.`id_cliente`) AS cargo, (SELECT (SELECT GROUP_CONCAT(M.`mail`) FROM `cliente_mails` AS M WHERE M.`cliente_id`=C.`id`) FROM `clientes` AS C WHERE C.`id`=I.`id_cliente`) AS correo FROM `ferias_invitados` AS I WHERE `id_feria`="'.$id.'" AND `tipo`="Cliente" ORDER BY `id` ASC');
        foreach($data['invitados_clientes'] as $cliente){
            $telefonos = DB::select('SELECT * FROM `cliente_telefonos` WHERE `cliente_id`="'.$cliente->id.'" AND `telefono_tipo`="Telefono" ORDER BY `id`');
            $t[$cliente->id]['telefonos']="";
            foreach($telefonos as $telefono){

                if(!empty($t[$cliente->id]['telefonos'])){
                    $t[$cliente->id]['telefonos'].=", ";
                }
                $t[$cliente->id]['telefonos'].="(".$telefono->telefono_indp.") (".$telefono->telefono_indc.") ".$telefono->telefono_numero." ext(".$telefono->telefono_ext.") ";

            }

            $celulares = DB::select('SELECT * FROM `cliente_telefonos` WHERE `cliente_id`="'.$cliente->id.'" AND `telefono_tipo`="Celular" ORDER BY `id`');
            $t[$cliente->id]['celulares']="";
            foreach($celulares as $celular){

                if(!empty($t[$cliente->id]['celulares'])){
                    $t[$cliente->id]['celulares'].=", ";
                }
                $t[$cliente->id]['celulares'].="(".$celular->telefono_indp.") (".$celular->telefono_indc.") ".$celular->telefono_numero;
            }

        }
        if(count($data['invitados_clientes'])==0){
            $t='';
        }
        $menu = $this->FeriaCommonMenu($id, 3);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.verinvitados',['data' => $data, 't'=>$t, 'menu' => $menu]);
    }

    public function listadefuncionarios(Request $request){
        $datos=$request->personas;
        $url_basico=$request->url_basico;
        $persona=explode('%%',$datos);
        $usuarios= DB::select('SELECT * FROM `users` WHERE `id`!="1" ORDER BY `id` ASC');
            ob_start(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-header">
                            <h2>Invitados</h2>
                            <em>Seleccione los funcionarios invitados</em>
                        </div>
                    </div>
                </div>
                <div class="row">
                <?php foreach($usuarios as $user){
                   $nombre=explode(' ',$user->nombres);
                   $apellido=explode(' ',$user->apellidos);
                   if($user->foto == ""){
                       $foto='essi_admon.jpg';
                   }else{
                       $foto=$user->foto;
                   }
                    $con=0;
                    foreach($persona as $p){
                        if($user->id == $p){
                            $con++;
                        }
                    }
                    if($con==0){
                        $clase="activar-funcionario";
                        $title="Agregar como invitado";
                    }else{
                        $clase="desactivar-funcionario";
                        $title="Eliminar de la lista de invitados";
                    }
                ?>
                    <div class="col-md-3 <?= $clase ?>" title="<?= $title ?>" id="af_<?= $user->id ?>">
                        <img src="<?= $url_basico ?>/images/file/clientes/<?= $foto ?>" class="img-circle foto-inv" alt="Avatar" />
						<h4><?= $nombre[0].' '.$apellido[0] ?></h4>
                    </div>
                <?php } ?>
                </div>
             <?php
            $cuerpo = ob_get_contents();
            ob_end_clean();
            return \Response::json(['content' => $cuerpo]);
    }

    public function anadiruserinv($id){
        $usuario=User::find($id);
        $nombre=explode(' ',$usuario->nombres);
        $apellido=explode(' ',$usuario->apellidos);
        if($usuario->foto == ""){
               $foto='essi_admon.jpg';
           }else{
               $foto=$usuario->foto;
           }
        ob_start(); ?>
        <div class="col-md-1 text-center" id="funcionario_invitado<?= $usuario->id ?>">
            <img src="/images/file/clientes/<?= $foto ?>" class="img-circle foto-inv2" alt="Avatar">
            <p class="parra h6"><a href="#"><strong><?= $nombre[0].' '.$apellido[0] ?></strong></a></p>
            <p class="h6"><span class="text-muted"><?= $usuario->cargo ?></span></p>
        </div>
         <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $cuerpo]);
    }

    public function FeriaCommonMenu($id, $active){
            $datos = Feria::find($id);
            $count_galeria = Ferias_multimedias::where('id_feria', $id)->count();
            $count_invitados = FeriasInvitado::where('id_feria', $id)->count();
            $count_conferencia = FeriasConference::where('id_feria', $id)->count();
            $count_bitacora = FeriasBitacora::where('feria_id', $id)->count();
            $count_patrocinios = FeriasSolicitudesPatrocinio::where('id_feria', $id)->count();
            /*
            Parametros de referencia para la clase
            1.feria
            2.Galería
            3.Invitados
            4.Conferencia
            5.Bitacora
            6.Camara
            7.Competencia
            8.Planeacion
            9.Presupuestos
            10.Patrocinios
            11.Lecciones
            12.Necesidades
            */
            ob_start();?>
            <ul class="nav nav-pills" role="tablist">
                <li class="<?php echo $class = ($active === 1)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/feria/ver/<?php echo $id; ?>">
                        <i class="fa fa-university"></i> Feria
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 2)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/feria/galeria/<?php echo $id; ?>">
                        <i class="fa fa-picture-o"></i> Galeria
                        <?php if($count_galeria){ ?><span class="badge badge-light"><?php echo $count_galeria; ?></span><?php } ?>
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 3)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/invitados/ver/<?php echo $id; ?>">
                        <i class="fa fa-users"></i> Invitados
                        <?php if($count_invitados){ ?><span class="badge badge-light"><?php echo $count_invitados; ?></span><?php } ?>
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 4)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/feria/conferencia/<?php echo $id; ?>">
                        <i class="fa fa-desktop"></i> Conferencia
                        <?php if($count_conferencia){ ?><span class="badge badge-light"><?php echo $count_conferencia; ?></span><?php } ?>
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 5)? "active" : ""; ?>">
                    <a href="<?php echo url('feria/bitacora'); ?>/<?php echo $id; ?>">
                        <i class="fa fa-calendar"></i> Bitacora
                        <?php if($count_bitacora){ ?><span class="badge badge-light"><?php echo $count_bitacora; ?></span><?php } ?>
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 6)? "active" : ""; ?>">
                   <!--<a href="<?php echo url('feria/visualizar/camara'); ?>/<?php echo $id; ?>">-->
                    <a href="http://corporativo.smartessi.com/camara/camara.html">
                        <i class="fa fa-camera"></i> Camara
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 7)? "active" : ""; ?>">
                    <a href="/feria/competencia/<?php echo $id; ?>">
                        <i class="fa fa-hand-rock-o"></i> Competencia
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 8)? "active" : ""; ?>">
                    <a href="<?php echo url('feria/planeacion'); ?>/<?php echo $id; ?>">
                        <i class="fa fa-clock-o"></i> Planeacion
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 9)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/presupuesto/<?php echo $id; ?>">
                        <i class="fa fa-money"></i> Presupuestos
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 10)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/feria/patrocinador/<?php echo $id; ?>&<?php echo isset($datos->nombre)? $datos->nombre : ""; ?>">
                        <i class="fa fa-handshake-o "></i> Patrocinios
                        <?php if($count_patrocinios){ ?><span class="badge badge-light"><?php echo $count_patrocinios; ?></span><?php } ?>
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 11)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/feria/leccionesaprendidas/<?php echo $id; ?>">
                        <i class="fa fa-check"></i> Lecciones
                    </a>
                </li>
                <li class="<?php echo $class = ($active === 12)? "active" : ""; ?>">
                    <a href="<?php echo url('/'); ?>/feria/necesidades/<?php echo $id; ?>">
                        <i class="fa fa-list-alt"></i> Necesidades
                    </a>
                </li>
            </ul>
            <?php
            $nav = ob_get_contents();
            ob_end_clean();
            return $nav;
        }

    public function ListarClientesInput(Request $request){
        if(isset($request->term)){
            $results = array();
            $query = "Select clientes.*, empresas.nombre As nom_empresa From clientes
            Inner Join empresas On empresas.id = clientes.empresa_id
            Where clientes.id Not In (Select ferias_clientes.id_cliente From ferias_clientes)
            And (clientes.cliente Like '%{$request->term}%' or empresas.nombre Like '%{$request->term}%')";
            $clientes_data = DB::select($query);

            foreach ($clientes_data as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->cliente.' - '.$query->nom_empresa ];
            }

            return \Response::json($results);
        }
    }

    public function ListarOportunidadesInput(Request $request){
        if(isset($request->term)){
            $results = array();
            $query = "Select * From oportunidades Where oportunidades.id Not In (Select ferias_oportunidades.id_oportunidad From ferias_oportunidades)
            And (oportunidades.empresa Like '%{$request->term}%' Or oportunidades.id = '{$request->term}')";
            $opor_data = DB::select($query);

            foreach ($opor_data as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->titulo.'- OP'.$query->id ];
            }

            return \Response::json($results);
        }
    }

    public function AccionesFeriaOportunidad(Request $request){
        if(isset($request->action)){
            $accion = intval($request->action);
            if($accion === 1){
                if(isset($request->oportunidad_id) && isset($request->id_feria)){
                    $fer_opor = new FeriasOportunidade;
                    $fer_opor->id_oportunidad = $request->oportunidad_id;
                    $fer_opor->id_feria = $request->id_feria;
                    if($fer_opor->save()){
                        return \Response::json(['res' => 1, 'msj' => 'Guardado con éxito.']);
                    }
                }
            }
            if($accion === 2){
            }
        }
    }

    public function AccionesFeriaCliente(Request $request){
        if(isset($request->action_c)){
            $accion = intval($request->action_c);
            if($accion === 1){
                if(isset($request->id_cliente_c) && isset($request->id_feria_c)){
                    $fer_client = new FeriasCliente;
                    $fer_client->id_cliente = $request->id_cliente_c;
                    $fer_client->id_feria = $request->id_feria_c;
                    if($fer_client->save()){
                        return \Response::json(['res' => 1, 'msj' => 'Guardado con éxito.']);
                    }
                }
            }
            if($accion === 2){
            }
        }
    }

    public function ListarClientesOportunidades(Request $request){
        if(isset($request->id)){
            $id = $request->id;

            $query1 = "Select ferias_oportunidades.id, oportunidades.empresa, empresas.logo
            From ferias_oportunidades left Join
            oportunidades On ferias_oportunidades.id_oportunidad = oportunidades.id
            left Join
            empresas On empresas.id = oportunidades.empresa_id
            Where ferias_oportunidades.id_feria ={$id}";

            $query2 = "Select ferias_clientes.id, clientes.cliente, clientes.foto
            From ferias_clientes left Join
            clientes On ferias_clientes.id_cliente = clientes.id
            Where ferias_clientes.id_feria ={$id}";
            $oportunidades = DB::select($query1);
            $clientes = DB::select($query2);

            ob_start();?>
            <?php foreach($oportunidades as $row){?>
                <div class="col-md-4 mt-5 mb-5">
                          <div class="card h-100">
                            <div class="card-body">
                              <div class="w-60 text-center m-4">
                              <img src="<?php echo url('/'); ?>/images/file/empresas/principal/<?php echo $row->logo; ?>" alt="" class="w-100">
                              </div>
                               <h4><?php echo $row->empresa; ?></h4>
                               <!--<div class="container">
                                   <p>producto 1</p>
                                   <p>producto 2</p>
                                   <p>producto 3</p>
                               </div>-->
                              </div>
                            </div>
                      </div>
            <?php
                  }
            $oport = ob_get_contents();
            ob_end_clean();
            unset($row);

            ob_start();?>
            <?php foreach($clientes as $row){?>
                <div class="col-md-4 mt-5 mb-5">
                          <div class="card h-100">
                            <div class="card-body">
                              <div class="w-60 text-center m-4">
                              <img src="<?php echo url('/'); ?>/images/file/clientes/<?php echo $row->foto; ?>" alt="" class="w-100">
                              </div>
                               <h4><?php echo $row->cliente; ?></h4>
                              </div>
                            </div>
                      </div>
            <?php
                  }
            $clients = ob_get_contents();
            ob_end_clean();
            return \Response::json(['res' => 1, 'oportunidades' => $oport, 'clientes' => $clients]);


        }
    }

    /**
     * [[Description]]
     * @param  integer $id ID de la feria
     * @return view Retorna vista necesidades.blade.php
     */
    public function necesidades($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $data['menu'] = $this->FeriaCommonMenu($id, 12);
        $user = User::find(Auth::user()->id);

        $data['user_img'] = url("/")."/images/file/clientes/".$user->foto;
        $data['id_feria'] = $id;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.necesidades',['data' => $data]);
    }

    public function GuardarNecesidades(Request $request){
        if(isset($request->id_feria)){
            $necesidad = new FeriasNecesidade;
            $necesidad->titulo = $request->titulo;
            $necesidad->cantidad = $request->cantidad;
            $necesidad->id_feria = $request->id_feria;
            if($necesidad->save()){
                if(isset($request->proveedores)){
                    $idneed = $necesidad->id;
                    foreach($request->proveedores as $row){
                        $nece_provee = new FeriasNecesidadesProveedore;
                        $nece_provee->id_necesidad = $idneed;
                        $nece_provee->id_proveedor = $row;
                        $nece_provee->save();
                    }
                }
                return \Response::json(['res' => 1, 'msj' => "Guardado con éxito"]);
            }
        }
    }

    public function ListarNecesidades(){
        $query1 = "Select ferias_necesidades.*, proveedores.nombre As nom_provee, proveedores.logo From ferias_necesidades Left Join proveedores On ferias_necesidades.id_proveedor_seleccionado = proveedores.id";
        $necesidades = DB::select($query1);

        ob_start();?>
        <?php foreach($necesidades as $row1){ ?>
        <div id="accordion_<?php echo $row1->id; ?>" role="tablist" class="needs-accordion" data-id="<?php echo $row1->id; ?>">
                <div class="card">
                   <div <?php if(!$row1->id_proveedor_seleccionado){?> data-toggle="tooltip" data-placement="right" title="No se ha seleccionado un proveedor" data-trigger="hover" data-animation="false" <?php } ?>>
                    <div class="card-header" role="tab" id="headingThree_<?php echo $row1->id; ?>" data-toggle="collapse" href="#collapseThree_<?php echo $row1->id; ?>" aria-expanded="false">
                       <?php
                        $icon = array();
                        $display = array();
                        if(!$row1->id_proveedor_seleccionado){
                            $icon['bg_color'] = "bg-warning";
                            $icon['icon'] = "fa-exclamation-triangle";
                            $display['frm'] = "";
                            $display['cont'] = "d-none";
                        }else{
                            $icon['bg_color'] = "bg-success";
                            $icon['icon'] = "fa-check";
                            $display['frm'] = "d-none";
                            $display['cont'] = "";
                        }
                        ?>
                        <div class="circle-flag <?php echo $icon['bg_color']; ?> text-white img-circle pull-left">
                            <i class="fa <?php echo $icon['icon']; ?> fa-lg" aria-hidden="true"></i>
                        </div>
                        <h4 class="text-left">
                            <?php echo $row1->titulo; ?>
                        </h4>
                        <div class="pull-right">
                            <h5><?php echo "$ ".number_format($row1->cantidad, 0, ' ', ',');?></h5>
                        </div>
                    </div>
                    </div>
                    <div id="collapseThree_<?php echo $row1->id; ?>" class="collapse" role="tabpanel" aria-labelledby="headingThree_<?php echo $row1->id; ?>" data-parent="#accordion_<?php echo $row1->id; ?>">
                        <div class="card-body container text-left">
                            <div class="row">
                            <div class="col-md-12">
                               <h3 class="text-capitalize text-info text-center">Seleccionar proveedor</h3>
                               <?php
                                    $query2 = "Select proveedores.logo, proveedores.nombre, ferias_necesidades_proveedores.id_proveedor, ferias_necesidades_proveedores.id From ferias_necesidades_proveedores Left Join proveedores On ferias_necesidades_proveedores.id_proveedor = proveedores.id Where ferias_necesidades_proveedores.id_necesidad = {$row1->id}";

                                    $proveedores = DB::select($query2); ?>
                                <form class="select-proveedor-frm <?php echo $display['frm']; ?>">
                                   <input type="hidden" name="need_id" value="<?php echo $row1->id; ?>">
                                    <div class="cc-selector mt-4 mb-2">
                                       <?php foreach($proveedores as $row2){?>
                                            <input id="<?php echo $row2->nombre.'_'.$row2->id; ?>" type="radio" name="prov_selected" value="<?php echo $row2->id_proveedor; ?>" data-parsley-required >
                                            <label class="drinkcard-cc mr-3 ml-3" for="<?php echo $row2->nombre.'_'.$row2->id; ?>" style="background-image: url(<?php echo url("storage/ferias/proveedores")."/".$row2->logo; ?>);"></label>

                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" id="motivo_seleccion" name="motivo_seleccion" rows="2" placeholder="Motivo de selección" data-parsley-required></textarea>
                                    </div>
                                    <div class="text-center mb-4">
                                      <button type="submit" class="btn btn-success">Guardar selección</button>
                                    </div>
                                </form>
                                <div class="container <?php echo $display['cont']; ?>">
                                    <img src="<?php echo url("storage/ferias/proveedores")."/".$row1->logo; ?>" class="rounded mx-auto d-block" alt="">
                                    <h4>Motivo de la selección</h4>
                                    <p><?php echo $row1->motivo_seleccion; ?></p>
                                    <?php if($row1->resultado && $row1->id_proveedor_seleccionado){ ?>
                                    <h4>Resultado</h4>
                                    <p><?php echo $row1->resultado; ?></p>
                                    <?php } ?>
                                </div>
                                <?php if(!$row1->resultado && $row1->id_proveedor_seleccionado){?>
                                <form class="resultado-proveedor-frm">
                                  <input type="hidden" value="<?php echo $row1->id; ?>" name="need_id">
                                   <h4>Resultado del proveedor</h4>
                                    <div class="form-group">
                                        <textarea class="form-control" id="resultado" name="resultado" rows="2" placeholder="Resultado" data-parsley-required></textarea>
                                    </div>
                                    <div class="text-center mb-4">
                                        <button class="btn btn-success">Guardar resultado</button>
                                    </div>
                                </form>
                                <?php } ?>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         <?php
        }
        $necesidades = ob_get_contents();
        ob_end_clean();
        $data['necesidades'] = $necesidades;
        return \Response::json(['res' => 1, 'data' => $data]);
    }

    public function GuardarProveedorSelecionado(Request $request){
        $need_id = $request->need_id;
        $necesidad = FeriasNecesidade::find($need_id);
        if(isset($request->need_id) && !isset($request->resultado)){
            $necesidad->id_proveedor_seleccionado = $request->prov_selected;
            $necesidad->motivo_seleccion = $request->motivo_seleccion;
            if($necesidad->save()){
                return \Response::json(['res' => 1, 'msj' => "Guardado correctamente"]);
            }
        }else{
            $necesidad->resultado = $request->resultado;
            if($necesidad->save()){
                return \Response::json(['res' => 1, 'msj' => "Guardado correctamente"]);
            }
        }
    }

    public function GuardarNecesidadComentario(Request $request){
        if(isset($request->comentario)){
            $do = new FeriasNecesidadesComentario;
            $do->comentario = $request->comentario;
            $do->id_usuario = Auth::user()->id;
            $do->id_necesidad = $request->id_need;
            if($do->save()){
                return \Response::json(['res' => 1, "msj" => "Guardado"]);
            }else{
                return \Response::json(['res' => 0, "msj" => "No se guardó el comentario."]);
            }
        }
    }

    public function ListarNecesidadComentarios(Request $request){
        if(isset($request->id_need)){
            $query = "Select users.name, users.foto, ferias_necesidades_comentarios.comentario,
             ferias_necesidades_comentarios.created_at,
             ferias_necesidades_comentarios.id_necesidad
             From users Left Join
             ferias_necesidades_comentarios On ferias_necesidades_comentarios.id_usuario =
             users.id
             Where ferias_necesidades_comentarios.id_necesidad = {$request->id_need}";

            $consulta = DB::select($query);
            ob_start();?>

                   <?php foreach ($consulta as $row){?>
                        <div class="comment-wrap">
                            <div class="photo">
                                <div class="avatar" style="background-image: url('<?php echo url('/').'/images/file/clientes/'.$row->foto; ?>')"></div>
                            </div>
                            <div class="comment-block">
                                <p class="comment-text text-left">
                                    <?php echo $row->comentario; ?>
                                </p>
                                <div class="bottom-comment">
                                    <div class="comment-date">
                                        <?php echo date("d/m/Y (h:i A)", strtotime($row->created_at)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                   <?php
                }

            $comments_wraps = ob_get_contents();
            ob_end_clean();
            $data['comentarios'] = $comments_wraps;
            return \Response::json(['res' => 1, "data" => $data]);
        }
    }

}
