<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Requests;
use App\User;
use App\Feria;
use App\Proveedore;
use App\FeriasPatrocinadore;
use App\Paises;
use App\FeriasSolicitudesPatrocinio;
use App\FeriasSolicitudesPatrociniosComentario;
use App\FeriasPatrocinadoresComentario;
use App\FeriasPatrocinadoresResponsable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
date_default_timezone_set("America/Bogota");

class PatrocinadorController extends Controller
{

    public function patrocinadores(){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $query = "SELECT *, (SELECT count(S.id) FROM ferias_solicitudes_patrocinios AS S WHERE S.id_patrocinador = P.id AND S.estado = 1 ) AS Aprobado, (SELECT count(S.id) FROM ferias_solicitudes_patrocinios AS S WHERE S.id_patrocinador = P.id AND S.estado = 2 ) AS Rechazado, (SELECT count(S.id) FROM ferias_solicitudes_patrocinios AS S WHERE S.id_patrocinador = P.id AND S.estado = 3 ) AS Pendiente FROM ferias_patrocinadores AS P;";

        $pat_data = DB::select($query);
        $proveedores=Proveedore::all();
        $patrocinadores=FeriasPatrocinadore::all();
        $boton['proveedores']=count($proveedores);
        $boton['patrocinadores']=count($patrocinadores);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.listadopatrocinadores',['data' => $data, 'pdata' => $pat_data, 'boton'=>$boton]);
    }

    public function patrocinadorescrear(){
        $paises=Paises::all();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.crearpatrocinadores', ['data' => $data, 'paises' => $paises]);
    }

    public function guardar(Request $request){
        $patid;
        $datos=$request->formulario;
        $dato = new FeriasPatrocinadore;
        foreach($datos as $index=>$value){
            $dato->$index = $value;
        }

        if (isset($request->logo_values)) {
            $imagen = json_decode($request->logo_values);
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);
            $antenombre = sha1(date("Y-m-d H:i:s"));
            $nombre = $antenombre.".jpg";
            \Storage::disk('feriaspatrocinadores')->put($nombre,  $foto);
            $dato->logo = $nombre;
        }

        $dato->ubicacion=$request->pais.'%%'.$request->departamento.'%%'.$request->ciudad;
        $dato->descripcion = $request->formulario['descripcion'];
        if($dato->save()){
            $patid = $dato->id;
        }
        if(isset($request->responsable)){
            for($i =0; $i < count($request->responsable['nombre']); $i++){
                $responsable = new FeriasPatrocinadoresResponsable;
                $responsable->id_patrocinador = $patid;
                $responsable->nombre = $request->responsable['nombre'][$i];
                $responsable->cargo = $request->responsable['cargo'][$i];
                $responsable->correo = $request->responsable['correo'][$i];
                $responsable->telefono = $request->responsable['telefono'][$i];
                $responsable->celular = $request->responsable['celular'][$i];
                $responsable->observaciones = $request->responsable['observaciones'][$i];
                $responsable->save();
            }

        }
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function patrocinadoreseditar($id){
        $dato = FeriasPatrocinadore::findOrFail($id);
        $paises=Paises::all();
        $u=explode('%%',$dato->ubicacion);
        $ubicacion['pais']=$u[0];
        $pais = DB::select('SELECT * FROM paises WHERE name="'.$u[0].'" ORDER BY name ASC');
        $ubicacion['departamento']=$u[1];
        $departamentos = DB::select('SELECT * FROM estados WHERE country_id = "'.$pais[0]->id.'" ORDER BY name ASC');
        $ubicacion['ciudad']=$u[2];
        $departamento = DB::select('SELECT * FROM estados WHERE name = "'.$u[1].'" ORDER BY name ASC');
        $ciudades = DB::select('SELECT * FROM ciudades WHERE state_id = "'.$departamento[0]->id.'" ORDER BY name ASC');

        $query = "Select ferias_patrocinadores_responsables.*
        From ferias_patrocinadores_responsables
        Where ferias_patrocinadores_responsables.id_patrocinador = {$id}";

        $responsables = DB::select($query);

        ob_start(); ?>
        <div class="actions text-center">
                          <a class="clone btn btn-success"><i class="fa fa-plus-square text-white"></i></a>
                          <a class="remove btn btn-danger"><i class="fa fa-minus-square text-white" aria-hidden="true"></i></a>
                    </div>
<?php
        $c = 0;
        foreach($responsables as $row){
?>
        <div id="res_info_<?php echo $c; ?>">
           <input type="hidden" value="<?php echo $row->id; ?>" name="responsable[id][]">
            <div class="col-md-4 mt-sm-5">
                <input type="text" class="form-control" name="responsable[nombre][]" value="<?php echo $row->nombre; ?>" placeholder="Nombre del responsable" required>
            </div>
            <div class="col-md-4 mt-sm-5">
                <input type="email" class="form-control" name="responsable[correo][]" value="<?php echo $row->correo; ?>" placeholder="Correo del responsable" required>
            </div>
            <div class="col-md-4 mt-sm-5">
                <input class="input form-control telefonofijo" name="responsable[telefono][]" value="<?php echo $row->telefono; ?>" type="text" placeholder="Teléfono">
            </div>
            <div class="col-md-4 mt-5">
                <input class="input form-control telefonocelular" name="responsable[celular][]" value="<?php echo $row->celular; ?>" type="text" placeholder="Celular">
            </div>
            <div class="col-md-4 mt-5">
                <input class="input form-control" name="responsable[cargo][]" value="<?php echo $row->cargo; ?>" type="text" placeholder="Cargo">
            </div>
            <div class="col-md-4 mt-5">
                <textarea placeholder="Observaciones..." name="responsable[observaciones][]" value="<?php echo $row->observaciones; ?>" class="form-control"></textarea>
            </div>
        </div>
        <?php
            $c++;
        }
        if(count($responsables)==0){ ?>
            <div id="res_clone" class="clonedInput">
                <div class="col-md-4 mt-sm-5">
                    <input type="text" class="form-control" name="responsable[nombre][]" value="" placeholder="Nombre del responsable" required>
                </div>
                <div class="col-md-4 mt-sm-5">
                    <input type="email" class="form-control" name="responsable[correo][]" placeholder="Correo del responsable" required>
                </div>
                <div class="col-md-4 mt-sm-5">
                    <input class="input form-control telefonofijo" name="responsable[telefono][]" type="text" placeholder="Teléfono">
                </div>
                <div class="col-md-4 mt-5">
                    <input class="input form-control telefonocelular" name="responsable[celular][]" type="text" placeholder="Celular">
                </div>
                <div class="col-md-4 mt-5">
                    <input class="input form-control" name="responsable[cargo][]" type="text" placeholder="Cargo">
                </div>
                <div class="col-md-4 mt-5">
                    <textarea placeholder="Observaciones..." name="responsable[observaciones][]" class="form-control"></textarea>
                </div>
            </div>
        <?php
        }

        $responsables_data = ob_get_contents();
        ob_end_clean();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.editarpatrocinadores', ['data' => $data, 'paises' => $paises, 'departamentos' => $departamentos, 'ciudades' => $ciudades, 'dato' => $dato, 'ubicacion' => $ubicacion, "responsables" => $responsables_data]);
    }

    public function editarPatrocinadorEdwin(Request $request){
        $datos=$request->formulario;
        $dato = FeriasPatrocinadore::find($request->id);
        foreach($datos as $index=>$value){
            $dato->$index=$value;
        }

        if (isset($request->logo_values)) {
            if(!empty($request->logo_values)){
                $logo_viejo = $dato->logo;
                if($logo_viejo!=''){
                    Storage::delete('ferias/patrocinadores/'.$logo_viejo);
                }
                $imagen = json_decode($request->logo_values);
                $data = explode( ',', $imagen->data );
                $foto = base64_decode($data[1]);
                $antenombre = sha1(date("Y-m-d H:i:s"));
                $nombre =$antenombre.".jpg";
                \Storage::disk('feriaspatrocinadores')->put($nombre,  $foto);
                $dato->logo = $nombre;
            }
        }

        if(isset($request->responsable)){
            for($i =0; $i < count($request->responsable['nombre']); $i++){
                if(isset($request->responsable['id'][$i]) && !empty($request->responsable['id'][$i])){
                    $responsable = FeriasPatrocinadoresResponsable::find($request->responsable['id'][$i]);
                    $responsable->nombre = $request->responsable['nombre'][$i];
                    $responsable->cargo = $request->responsable['cargo'][$i];
                    $responsable->correo = $request->responsable['correo'][$i];
                    $responsable->telefono = $request->responsable['telefono'][$i];
                    $responsable->celular = $request->responsable['celular'][$i];
                    $responsable->observaciones = $request->responsable['observaciones'][$i];
                    $responsable->id_patrocinador = $request->id;
                    $responsable->save();
                }else{
                    $responsable =  new FeriasPatrocinadoresResponsable;
                    $responsable->nombre = $request->responsable['nombre'][$i];
                    $responsable->cargo = $request->responsable['cargo'][$i];
                    $responsable->correo = $request->responsable['correo'][$i];
                    $responsable->telefono = $request->responsable['telefono'][$i];
                    $responsable->celular = $request->responsable['celular'][$i];
                    $responsable->observaciones = $request->responsable['observaciones'][$i];
                    $responsable->id_patrocinador = $request->id;
                    $responsable->save();
                }

            }

        }
        $dato->ubicacion=$request->pais.'%%'.$request->departamento.'%%'.$request->ciudad;
        $dato->save();
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function eliminarpatrocinador($id){
        $dato = FeriasPatrocinadore::findOrFail($id);
        $logo_viejo = $dato->logo;
        if($logo_viejo!=''){
            Storage::delete('ferias/patrocinadores/'.$logo_viejo);
        }
        $del = FeriasPatrocinadore::find($id);
        $del->delete();
        return \Response::json(['msj' => 'Eliminado Correctamente!']);
    }

    public function verpatrocinador($id){
        $dato = FeriasPatrocinadore::findOrFail($id);
        $ubicacion=str_replace("%%", ", ", $dato->ubicacion);
        return \Response::json(['dato' => $dato, 'ubicacion' =>$ubicacion]);
    }

    public function listsponsor($id_feria, $nom_feria){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $sponsors =DB::table('ferias_patrocinadores')->select('id', 'nombre')->get();

        $query_1 = "Select Sum(ferias_solicitudes_patrocinios.patrocinio_inicial) As p_solicitado,
        Sum(ferias_solicitudes_patrocinios.patrocinio_total) As p_aprobado,
        ((Sum(ferias_solicitudes_patrocinios.patrocinio_total) /
        Sum(ferias_solicitudes_patrocinios.patrocinio_inicial)) * 100) As
        por_efectividad
        From ferias_solicitudes_patrocinios
        Where ferias_solicitudes_patrocinios.id_feria = {$id_feria} And
        ferias_solicitudes_patrocinios.estado != 2";

        $query_3 = "Select Sum(ferias_solicitudes_patrocinios.patrocinio_inicial) As p_solicitado,
        Sum(ferias_solicitudes_patrocinios.patrocinio_total) As p_aprobado,
        ((Sum(ferias_solicitudes_patrocinios.patrocinio_total) /
        Sum(ferias_solicitudes_patrocinios.patrocinio_inicial)) * 100) As
        por_efectividad
        From ferias_solicitudes_patrocinios
        Where ferias_solicitudes_patrocinios.estado != 2";

        $query_4 = "Select Count(*) As cant_solicitudes, (Select count(*)
        From ferias_solicitudes_patrocinios
        Where ferias_solicitudes_patrocinios.estado = 1) As cant_aportantes,
        ((Select count(*) From ferias_solicitudes_patrocinios
        Where ferias_solicitudes_patrocinios.estado = 1) /
        Count(ferias_solicitudes_patrocinios.id) * 100) As sol_por_efectividad
        From ferias_solicitudes_patrocinios";

        $feria_data= feria::find($id_feria);
        if(isset($feria_data->fecha_inicio)){
            $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
            $fecha_entrada = strtotime($feria_data->fecha_inicio);
            $query = "";
            if($fecha_actual > $fecha_entrada){
                $query = "Select SUM(CONVERT(REPLACE(REPLACE(ferias_presupuesto_ajustes.valor, '$', ''), '.', '') ,UNSIGNED INTEGER)) AS valor, REPLACE(FORMAT((SUM(CONVERT(REPLACE(REPLACE(ferias_presupuesto_ajustes.valor, '$', ''), '.', '') ,UNSIGNED INTEGER))/1000000),1),'.',',') AS proyectado
                From ferias_presupuesto_ajustes
                Where ferias_presupuesto_ajustes.id_feria ={$id_feria}";
                //echo "La fecha entrada ya ha pasado";
            }else{
                $query = "Select SUM(CONVERT(REPLACE(REPLACE(ferias_presupuestos.valor, '$', ''), '.', '') ,UNSIGNED INTEGER)) AS valor, REPLACE(FORMAT((SUM(CONVERT(REPLACE(REPLACE(ferias_presupuesto_ajustes.valor, '$', ''), '.', '') ,UNSIGNED INTEGER))/1000000),1),'.',',') AS proyectado
                From ferias_presupuestos
                Where ferias_presupuestos.id_feria ={$id_feria}";
                //echo "Aun falta algun tiempo";
            }
            $datos_board["presupuesto"]["essi"] = DB::select($query);
        }


        $datos_board["presupuesto"]["left"] = DB::select($query_1);
        $datos_board["patrocinio"]["left"] = DB::select($query_3);
        $datos_board["patrocinio"]["right"] = DB::select($query_4);

        $menu = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id_feria, 10);

        /*Patrocinios para la feria*/
        $query = 'SELECT REPLACE(FORMAT((SUM(patrocinio_total)/1000000), 1), ".", ",") AS patrocinio, SUM(patrocinio_total) AS valor_total FROM ferias_solicitudes_patrocinios WHERE estado = 1 AND id_feria = '.$id_feria;
        $datos_board['Patrocinador']['patrocinio'] = DB::select($query);
        $query1 = 'SELECT FORMAT(('.$datos_board['Patrocinador']['patrocinio'][0]->valor_total.'/SUM(REPLACE(REPLACE(valor, ".", ""), "$ ", "")))*100,1) AS patrocinado, REPLACE(FORMAT((SUM(REPLACE(REPLACE(valor, ".", ""), "$ ", ""))/1000000),1),".",",") AS valor_real, SUM(REPLACE(REPLACE(valor, ".", ""), "$ ", "")) AS valor_sin_comas_real FROM ferias_presupuesto_ajustes WHERE id_feria = '.$id_feria;
        $datos_board['Patrocinador']['patrocinado'] = DB::select($query1);
        $query2 = 'SELECT SP.*, P.nombre AS nombre, P.logo AS logo FROM ferias_solicitudes_patrocinios SP INNER JOIN ferias_patrocinadores P ON SP.id_patrocinador = P.id WHERE SP.estado = 1 AND SP.id_feria = '.$id_feria.' ORDER BY SP.patrocinio_total ASC';
        $datos_board['Patrocinador']['lista'] = DB::select($query2);
        /*Fin Patrocinios para la feria*/

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        return view('ferias.feriapatrocinador', ['data' => $data, 'nom_feria' => $nom_feria, 'id_feria' => $id_feria,'sponsor_data' => $sponsors, "datos_board" => $datos_board, 'menu' => $menu]);
    }

    public function savesponsorship(Request $request){
        if(isset($request->id_feria) && isset($request->vlr_inicial) && isset($request->id_sponsor)){
            $file = $request->file('attached_file_sol');
            $filename = "";
            $lastInsert;
            if($file){
                $filename = $file->getClientOriginalName().'___'.time();
                $file->move(public_path('storage/ferias/solicitud_patrocinio/solicitudes_files'), $filename);
            }
            $soli = new FeriasSolicitudesPatrocinio;
            $soli->patrocinio_inicial = intval(str_replace("$ ", "", str_replace(",", "", $request->vlr_inicial)));
            $soli->archivo_solicitud = $filename;
            $soli->id_feria = $request->id_feria;
            $soli->id_patrocinador = $request->id_sponsor;
            $soli->id_responsable = $request->res_dep;
            if($soli->save()){
                $lastInsert = $soli->id;
            }
            if(isset($request->comment_sol) && $lastInsert){
                if($this->save_comment($request->comment_sol, $lastInsert)){
                    return \Response::json(['res' => 1, "msj" => "Se insertaron todos los datos correctamente"]);
                }
            }else{
                    return \Response::json(['res' => 1, "msj" => "Se creó correctamente"]);
                }
        }
    }

 public function listsponsorship(Request $request){
    /*Permiso para Agregar, Editar, Eliminar*/
    $modulo=21;
    $data['permiso_agregar_editar_eliminar']='No';
    $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
    if(isset($acceso[0]->id)){
      $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
      if(isset($cargo[0]->id)){
        $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
        if(isset($permiso[0]->permiso)){
          if($permiso[0]->permiso == "Si"){
            $data['permiso_agregar_editar_eliminar']="Si";
          }
        }
      }
    }
     $id_feria = $request->input('id_feria');
     $estado = $request->input('estado');
     $string = ($estado)? " And ferias_solicitudes_patrocinios.estado = $estado" : "";

     $query = "Select ferias_solicitudes_patrocinios.*, ferias_patrocinadores.logo, ferias_patrocinadores.nombre From ferias_solicitudes_patrocinios Inner Join ferias_patrocinadores On ferias_solicitudes_patrocinios.id_patrocinador = ferias_patrocinadores.id Where ferias_solicitudes_patrocinios.id_feria = $id_feria".$string;

     $consulta = DB::select($query);

     ob_start();?>
    <div class="animated slideInRight" id="list" style="margin-bottom: 30px;">
        <div class="card-block">
            <div class="table-responsive">
                <table id="example" cellspacing="0" class="table table-striped table table-striped table-bordered display">
                    <thead>
                        <tr>
                            <th class="text-center">Patrocinador</th>
                            <th class="text-center">Presupuesto solicitado</th>
                            <?php  if($estado == 1){  ?>
                            <th class="text-center">Presupuesto recibido</th>
                            <?php } ?>
                            <th class="text-center">comentarios</th>
                            <th class="text-center">Archivo solicitud</th>
                            <?php  if($estado == 1 || $estado == 2){  ?>
                            <th class="text-center">Archivo respuesta</th>
                            <?php } ?>
                            <?php  if($estado == 3){  ?>
                            <th class="text-center">Acciones</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody id="sponsor_table">
                        <?php foreach ($consulta as $row){?>
                        <tr>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <img src="<?php echo $request->URL."/storage/ferias/patrocinadores/".$row->logo; ?>" width="50" height="50">
                            </td>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <?php echo  "$".number_format($row->patrocinio_inicial, 0, ',', '.'); ?>
                            </td>
                            <?php  if($estado == 1){  ?>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <?php echo $total = ($row->patrocinio_total) ? "$".number_format($row->patrocinio_total, 0, ',', '.') : "No especificado"; ?>
                            </td>
                            <?php } ?>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <?php if($row->comentario_flag){?>
                                <button type="button" class="btn btn-info list_comments" data-idr="<?php echo $row->id; ?>"><i class="fa fa-commenting-o"></i> Ver comentarios</button>
                                <?php
                            }else{
                                echo "Sin comentarios";
                            }
                            ?>
                            </td>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <?php if($row->archivo_solicitud){?>
                                <a class="fa fa-file-pdf-o fa-2x" aria-hidden="true" onclick="show_doc('solicitudes_files/<?php echo $row->archivo_solicitud; ?>')"></a>
                                <?php
                            }else{
                                echo "Sin documento";
                            }
                            ?>
                            </td>
                            <?php  if($estado != 3){  ?>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <?php if($row->archivo_respuesta){?>
                                <a class="fa fa-file-pdf-o fa-2x" aria-hidden="true" onclick="show_doc('respuestas_files/<?php echo $row->archivo_respuesta; ?>')"></a>
                                <?php
                            }else{
                                echo "Sin documento";
                            }
                            ?>
                            </td>
                            <?php } ?>
                            <?php  if($estado == 3){  ?>
                            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top">
                                <a class="fa fa-check-circle fa-2x <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> action_btn <?php } ?>" aria-hidden="true" data-idr="<?php echo $row->id; ?>" data-action="1" data-toggle="tooltip" data-placement="left" title="Aprobar solicitud" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?> onclick="no_permiso('Usted no tiene permisos para aprobar la solicitud')" <?php } ?>></a>
                                <a class="fa fa-times-circle fa-2x <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> action_btn <?php } ?>" aria-hidden="true" data-idr="<?php echo $row->id; ?>" data-action="2" data-toggle="tooltip" data-placement="left" title="Cancelar solicitud" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?> onclick="no_permiso('Usted no tiene permisos para cancelar la solicitud')" <?php } ?>></a>
                                <a class="fa fa-comment fa-2x <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> comment_btn <?php } ?>" aria-hidden="true" data-idr="<?php echo $row->id; ?>" data-action="3" data-toggle="tooltip" data-placement="left" title="Agregar comentario" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?> onclick="no_permiso('Usted no tiene permisos para comentar la solicitud')" <?php } ?>></a>
                            </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
        $cuerpo = ob_get_contents();
        ob_end_clean();
		if($cuerpo){
        print json_encode(array("res"=> 1, "cuerpo_tabla"=>$cuerpo));
		}else{
			 print json_encode(array("res"=> 0, "msj"=> "No hay solicitudes"));
        }
 }

    public function actionrequest(Request $request){
        //1 Aprobar, 2 Cancelar, 3comentar
        if(isset($request->id_req) && isset($request->action)){
            $file = $request->file('attached_file');
            $filename = "";
            if($file){
                $filename = $file->getClientOriginalName().'___'.time();
                $file->move(public_path('storage/ferias/solicitud_patrocinio/respuestas_files'),$filename);
            }
            switch($request->action){
                case 1:
                    $data = FeriasSolicitudesPatrocinio::findOrFail($request->id_req);
                    $data->estado = 1;
                    $data->archivo_respuesta = $filename;
                    if($request->comment){
                        $data->comentario_flag = 1;
                        $this->save_comment($request->comment, $request->id_req);
                    }
                    $data->patrocinio_total = intval(str_replace("$ ", "", str_replace(",", "", $request->total)));
                    if($data->save()){
                        return \Response::json(['res' => 1, "msj" => "Aprovado"]);
                    }
                    break;

                case 2:
                    $data = FeriasSolicitudesPatrocinio::findOrFail($request->id_req);
                    $data->estado = 2;
                    $data->archivo_respuesta = $filename;
                    if($request->comment){
                        $data->comentario_flag = 1;
                        $this->save_comment($request->comment, $request->id_req);
                    }
                    $data->patrocinio_total = 0;
                    if($data->save()){
                        return \Response::json(['res' => 1, "msj" => "Cancelado"]);
                    }
                    break;
                case 3:
                    if($this->save_comment($request->comment_body, $request->id_req)){
                        return \Response::json(['res' => 1, "msj" => "Mensaje guardado"]);
                    }
                    break;
            }
        }

    }

    public function save_comment($comment_body, $id_req){
        $comment = new FeriasSolicitudesPatrociniosComentario;
        $comflag= FeriasSolicitudesPatrocinio::findOrFail($id_req);
        $dflag = FeriasSolicitudesPatrocinio::where('id', $id_req)->value('comentario_flag');
        if($dflag == 0){
            $comflag->comentario_flag = 1;
        }
        $comment->comentario = $comment_body;
        $comment->id_solicitud = $id_req;
        $comment->id_usuario = Auth::user()->id;
        if($comment->save() && $comflag->save()){
            return true;
        }
    }

 public function list_comments(Request $request){
     $query = "Select ferias_solicitudes_patrocinios_comentarios.comentario,
    ferias_solicitudes_patrocinios_comentarios.created_at, users.name, users.foto
    From ferias_solicitudes_patrocinios_comentarios Left Join
    users On ferias_solicitudes_patrocinios_comentarios.id_usuario = users.id
    Where ferias_solicitudes_patrocinios_comentarios.id_solicitud = {$request->id_req}";

     $consulta = DB::select($query);
     ob_start();?>
            <?php foreach ($consulta as $row){?>
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url('<?php echo url('/').'/images/file/clientes/'.$row->foto; ?>')"></div>
                    </div>
                    <div class="comment-block">
                        <p class="comment-text text-left">
                            <?php echo $row->comentario; ?>
                        </p>
                        <div class="bottom-comment">
                            <div class="comment-date">
                                <?php echo date("d/m/Y (h:i A)", strtotime($row->created_at)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }

        $cuerpo = ob_get_contents();
        ob_end_clean();
		if($cuerpo){
            return \Response::json(['res' => 1, "data" => $cuerpo]);
		}else{
            return \Response::json(['res' => 0, "msj" => "No hay comentarios"]);
			}
    }


    public function details_sponsor($id = null){
        if($id){
            $p_query = "Select ferias_patrocinadores.*
            From ferias_patrocinadores
            Where ferias_patrocinadores.id = {$id}";

            $r_query = "Select ferias_patrocinadores_responsables.*
            From ferias_patrocinadores_responsables
            Where ferias_patrocinadores_responsables.id_patrocinador = {$id}";

            $sp_query = "Select ferias_solicitudes_patrocinios.*, ferias.nombre As f_nombre,
            ferias_exponentes.responsable As f_r_nombre
            From ferias_solicitudes_patrocinios Left Join
            ferias On ferias_solicitudes_patrocinios.id_feria = ferias.id Left Join
            ferias_exponentes On ferias.id = ferias_exponentes.id_feria
            Where ferias_solicitudes_patrocinios.id_patrocinador = {$id}";

            $id_usr = Auth::user()->id;
            $img_query = "Select users.foto, users.id From users Where users.id = {$id_usr}";


            $patrocinador = DB::select($p_query);
            $responsable = DB::select($r_query);
            $solicitudes = DB::select($sp_query);
            $img_profile = DB::select($img_query);
            /*Consulta para activar el item del menu correspondiente*/
            $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
            $data['item_menu'] = DB::select($query);
            if(count($data['item_menu']) > 0){
                $data['item_menu']='menu_'.$data['item_menu'][0]->id;
            }else{
                $data['item_menu']='';
            }
            /*Fin Consulta para activar el item del menu correspondiente*/
            return view('ferias.verpatrocinador', ['data' => $data, 'datos_p' => $patrocinador, 'datos_r' => $responsable, 'datos_sp' => $solicitudes, "id_pat" => $id, "img" => $img_profile]);
        }
    }

    public function sponsor_comments(Request $request){
     $query = "Select ferias_patrocinadores_comentarios.*,
        ferias_patrocinadores_comentarios.id_patrocinador, users.name, users.foto
        From ferias_patrocinadores_comentarios left Join
        users On ferias_patrocinadores_comentarios.id_usuario = users.id
        Where ferias_patrocinadores_comentarios.id_patrocinador = {$request->id_pat}";

        $comments = DB::select($query);
        ob_start();?>
                <?php foreach ($comments as $row){?>
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url('<?php echo url('/').'/images/file/clientes/'.$row->foto; ?>')"></div>
                    </div>
                    <div class="comment-block">
                        <p class="comment-text text-left">
                            <?php echo $row->comentario; ?>
                        </p>
                        <div class="bottom-comment">
                            <div class="comment-date">
                                <?php echo date("d/m/Y (h:i A)", strtotime($row->created_at)); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
}
        $mensajes = ob_get_contents();
        ob_end_clean();
		if($mensajes){
            return \Response::json(['res' => 1, "comments" => $mensajes]);
		}else{
            return \Response::json(['res' => 0, "msj" => "No hay comentarios aún"]);
			}
    }

    public function comment(Request $request){
        if(isset($request->comentario)){
            $do = new FeriasPatrocinadoresComentario;
            $do->comentario = $request->comentario;
            $do->id_usuario = Auth::user()->id;
            $do->id_patrocinador = $request->id_pat_;
            if($do->save()){
                return \Response::json(['res' => 1, "msj" => "Guardado"]);
            }else{
                return \Response::json(['res' => 0, "msj" => "No se guardó el comentario."]);
            }
        }
    }

    public function list_responsible(Request $request){
        if(isset($request->id_responsable)){
            $query = "Select ferias_patrocinadores_responsables.nombre,
            ferias_patrocinadores_responsables.id
            From ferias_patrocinadores_responsables
            Where ferias_patrocinadores_responsables.id_patrocinador = {$request->id_responsable}";

            $options = DB::select($query);

            ob_start();?>
            <option value="false">Seleccione...</option>
            <?php foreach($options as $row){ ?>
            <option value="<?php echo $row->id; ?>"><?php echo $row->nombre; ?></option>
            <?php
            }

            $options_body = ob_get_contents();
            ob_end_clean();
            if($options_body){
                return \Response::json(['res' => 1, "options" => $options_body]);
            }else{
                return \Response::json(['res' => 0, "msj" => "No hay responsables"]);
            }


        }
    }

    public function verlistpatrocinador($id){
        $dato = FeriasPatrocinadore::find($id);
        $responsables = FeriasPatrocinadoresResponsable::where('id_patrocinador',$id)->get();
        ob_start();?>
        <div class="row">
            <div class="col-md-12">
                <img src="/storage/ferias/patrocinadores/<?=$dato->logo?>" class="img-circle" alt="Avatar">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><?=$dato->nombre?></h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Nit: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$dato->nit?></em></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Ubicación: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=str_replace("%%",", ",$dato->ubicacion)?></em></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Tipo financiador: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$dato->tipo_financiador?></em></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Descripción: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$dato->descripcion?></em></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Responsables</h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <?php $i=1; foreach($responsables as $responsable){ ?>
            <div class="col-md-1">
                #<?=$i?>
            </div>
            <div class="col-md-5 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Nombre: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$responsable->nombre?></em></h4>
            </div>
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Cargo: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$responsable->cargo?></em></h4>
            </div>
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Telefono: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$responsable->telefono?></em></h4>
            </div>
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Celular: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$responsable->celular?></em></h4>
            </div>
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Correo: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$responsable->correo?></em></h4>
            </div>
            <div class="col-md-6 text-right">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;">Observaciones: </h4>
            </div>
            <div class="col-md-6 text-left">
                <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em><?=$responsable->observaciones?></em></h4>
            </div>
            <hr>
            <?php $i++; } ?>
        </div>
        <?php
        if(count($responsables)==0){ ?>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4 style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;"><em>No hay responsables</em></h4>
                </div>
            </div>
        <?php
        }
        $content = ob_get_contents();
        ob_end_clean();
        return \Response::json(['content' => $content]);
    }
}
