<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Feria;
use App\Ferias_presupuestos;
use App\Ferias_presupuestos_admin;
use App\FeriasPresupuestoAjuste;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
date_default_timezone_set("America/Bogota");

class PresupuestoferiaController extends Controller
{
    public function crear($id){
        $dato=Feria::findOrFail($id);
        $conceptos= DB::select('SELECT *, (SELECT `valor` FROM `ferias_presupuestos` AS FP WHERE FP.`id_concepto`=FPA.`id` AND FP.`id_feria`="'.$id.'") AS valor FROM `ferias_presupuestos_admins` AS FPA ORDER BY `id` ASC');
        $i=0;
        $total=0;
        foreach($conceptos as $concepto){
            $v=str_replace(".", "", $concepto->valor);
            $v=str_replace("$ ", "", $v);
            $v=intval($v);
            $total+=$v;

        }
        if($total != 0){
            $total=number_format($total, 0, ',', '.');
        }
        $menu = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id, 9);
        /*Fecha limite presupuesto*/
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $data['nombre'] = Auth::user()->nombres.' '.Auth::user()->apellidos;
        $fehca_inicio = strtotime ( '+2 day' , strtotime ( $dato->fecha_inicio ) ) ;
        $fehca_inicio = date ( 'Y-m-d' , $fehca_inicio );
        $data['fecha_limite_presupuesto'] = date('d',strtotime($fehca_inicio)).' de '.$meses[intval(date('m',strtotime($fehca_inicio)))].' del '.date('Y',strtotime($fehca_inicio));
        if($fehca_inicio>=date('Y-m-d')){
            $data['permiso_presupuesto'] = "Si";
        }else{
            $data['permiso_presupuesto'] = "No";
            $data['vencio_presupuesto'] = (strtotime($fehca_inicio)-strtotime(date('Y-m-d')))/86400;
            $data['vencio_presupuesto'] = abs($data['vencio_presupuesto']);
            $data['vencio_presupuesto'] = floor($data['vencio_presupuesto']);
        }
        /*Fin Fecha limite presupuesto*/
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        return view('ferias.presupuesto',['dato' => $dato, 'conceptos' => $conceptos, 'i' => $i, 'total' => $total, 'menu' => $menu, 'data'=>$data]);
    }

    public function guardarferiapresupuesto(Request $request){
        $presupuestos= DB::select('SELECT * FROM `ferias_presupuestos` WHERE `id_feria`="'.$request->id.'" ORDER BY `id` ASC');
        foreach($presupuestos as $presupuesto){
            $dato=Ferias_presupuestos::findOrFail($presupuesto->id);
            $dato->delete();
        }
        $items=$request->presupuesto;
        foreach($items as $item){
            $dato = new Ferias_presupuestos;
            $dato->concepto=$item['concepto'];
            $dato->id_concepto=$item['id_concepto'];
            $dato->valor=$item['valor'];
            $dato->id_feria=$request->id;
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function presupuestoadministrable(){
        $datos = Ferias_presupuestos_admin::all();
        $i=0;
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.presupuestoadministrable',['data' => $data, 'datos' => $datos, 'i' => $i]);
    }

    public function guardarferiapresupuestoadmin(Request $request){
        $presupuestos=$request->presupuesto;
        foreach($presupuestos as $presupuesto){
            $dato=Ferias_presupuestos_admin::findOrFail($presupuesto['id']);
            $dato->concepto=$presupuesto['concepto'];
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }

    public function eliminaritempresupuesto($id){
        $dato=Ferias_presupuestos_admin::findOrFail($id);
        $dato->delete();
        return \Response::json(['msj' => 'Eliminado Correctamente!']);
    }

    public function crearitempresupuesto(){
        $dato= new Ferias_presupuestos_admin;
        $dato->save();
        $id = $dato->id;
        return \Response::json(['id' => $id]);
    }

    public function compararpresupuesto($id){
        /*Permiso para Agregar, Editar, Eliminar*/
        $modulo=21;
        $data['permiso_agregar_editar_eliminar']='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Agregar, Editar, Eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_agregar_editar_eliminar']="Si";
              }
            }
          }
        }
        $dato=Feria::findOrFail($id);
        $conceptos= DB::select('SELECT *, (SELECT `valor` FROM `ferias_presupuestos` AS FP WHERE FP.`id_concepto`=FPA.`id` AND FP.`id_feria`="'.$id.'") AS valor, (SELECT FPAJ.`valor` FROM `ferias_presupuesto_ajustes` AS FPAJ WHERE FPAJ.`id_concepto`=FPA.`id` AND FPAJ.`id_feria`="'.$id.'") AS valor_real FROM `ferias_presupuestos_admins` AS FPA ORDER BY `id` ASC');
        $i=0;
        $total=0;
        foreach($conceptos as $concepto){
            $v=str_replace(".", "", $concepto->valor);
            $v=str_replace("$ ", "", $v);
            $v=intval($v);
            $total+=$v;

        }
        if($total != 0){
            $total=number_format($total, 0, ',', '.');
        }
        $menu = app('App\Http\Controllers\FeriasController')->FeriaCommonMenu($id, 9);
        /*Fecha limite valor real*/
        $meses=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $data['nombre'] = Auth::user()->nombres.' '.Auth::user()->apellidos;
        $fehca_fin = strtotime ( '+10 day' , strtotime ( $dato->fecha_fin ) ) ;
        $fehca_fin = date ( 'Y-m-d' , $fehca_fin );
        $data['fecha_limite_real'] = date('d',strtotime($fehca_fin)).' de '.$meses[intval(date('m',strtotime($fehca_fin)))].' del '.date('Y',strtotime($fehca_fin));
        if($fehca_fin>=date('Y-m-d')){
            $data['permiso_real'] = "Si";
        }else{
            $data['permiso_real'] = "No";
            $data['vencio_real'] = (strtotime($fehca_fin)-strtotime(date('Y-m-d')))/86400;
            $data['vencio_real'] = abs($data['vencio_real']);
            $data['vencio_real'] = floor($data['vencio_real']);
        }
        /*Fin Fecha limite valor real*/
        $data['total_valor_real'] = DB::select('SELECT REPLACE(FORMAT(SUM(REPLACE(REPLACE(valor, ".", ""), "$ ", "")),0),",",".") AS total FROM ferias_presupuesto_ajustes WHERE id_feria = '.$id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Ferias" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('ferias.compararpresupuesto',['dato' => $dato, 'conceptos' => $conceptos, 'i' => $i, 'total' => $total, 'menu' => $menu, 'data' => $data]);
    }

    public function guardarajuste(Request $request){
        $datos = FeriasPresupuestoAjuste::where('id_feria', $request->id)->get();
        foreach($datos as $dato){
            $dato->delete();
        }
        $ajustes = $request->ajuste;
        foreach($ajustes as $ajuste){
            $dato = new FeriasPresupuestoAjuste;
            $dato->id_feria=$request->id;
            $dato->id_concepto=$ajuste['id_concepto'];
            $dato->valor=$ajuste['valor'];
            $dato->save();
        }
        return \Response::json(['msj' => 'Guardado Correctamente!']);
    }
}
