<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\MapaLecheros;
use App\MapaLecheroTitulos;
use App\MapaLecheroVersiones;
use App\MapaLecheroArchivos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Auth;
use Carbon\Carbon;
date_default_timezone_set("America/Bogota");

class MapalecheroController extends Controller
{
    public function index(){
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 55 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para editar titulo*/
        $modulo=18;
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear, Editar, Eliminar titulo" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC');
            if(isset($permiso1[0]->permiso)){
                $data['permiso_titulo'] = $permiso1[0]->permiso;
            }
          }
        }

        /*Permiso para acceder al modulo*/
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Acceso al modulo" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){
				return view('mapalechero.mapalechero',['data' => $data]);
			  }else{
				return view('oportunidades.nopermiso',['data' => $data]);
			  }
            }
          }
        }
        /*Fin Permiso para acceder al modulo*/
    }

    public function action(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*Listado de todos los registros de mapa lechero*/
            case 0:
                /*Permisos para ver el listado*/
                $modulo=18;
                $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Ver listado" AND `id_permisomodulo`="'.$modulo.'"');
                if(isset($acceso[0]->id)){
                  $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                  if(isset($cargo[0]->id)){
                    $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC');
                    if(isset($permiso[0]->permiso)){
                      if($permiso[0]->permiso == "propia"){
                        $mapas = MapaLecheros::where('id_user',Auth::user()->id)->get();
                      }else if($permiso[0]->permiso == "TODAS"){
                        $mapas = MapaLecheros::all();
                      }else{
                          $mapas = "No";
                      }
                    }
                  }
                }
                /*Permiso para eliminar*/
                $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Eliminar mapa lechero" AND `id_permisomodulo`="'.$modulo.'"');
                if(isset($acceso[0]->id)){
                  $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                  if(isset($cargo[0]->id)){
                    $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC');
                    if(isset($permiso[0]->permiso)){
                      if($permiso[0]->permiso == "Si"){
                          $eliminar="Si";
                      }else{
                          $eliminar="No";
                      }
                    }
                  }
                }
                if($mapas == "No"){
                    ob_start();  ?>
<div class="col-md-12">
<p class="h2 text-center">No tiene permiso para ver el listado de mapa lechero</p>
</div>
                <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                }else{
                    $titulos = MapaLecheroTitulos::all();
                    ob_start();
                    foreach($mapas as $mapa){
                        $data['palabras'][$mapa->id] = 0;
                        $data['titulos'][$mapa->id] = 0;
                        $data['Actualizacion'][$mapa->id] = $mapa->updated_at != ''? $mapa->updated_at:$mapa->created_at;
                        $query = 'SELECT * FROM users U WHERE U.id = '.$mapa->id_user;
                        $usuario = DB::SELECT($query);
                        $user = $usuario[0];
                        $nombres = isset($user->nombres) && !empty($user->nombres)?explode(' ',$user->nombres):'Sin nombre';
                        $apellidos = isset($user->apellidos) && !empty($user->apellidos)?explode(' ',$user->apellidos):'Sin nombre';
                        $versiones = MapaLecheroVersiones::where('id_mapa_lechero',$mapa->id)->get();
                        $data['versiones'][$mapa->id] = count($versiones);
                        foreach($titulos as $titulo){
                            $palabras = MapaLecheroVersiones::where('id_mapa_lechero',$mapa->id)->where('id_mapa_lechero_titulo',$titulo->id)->orderBy('created_at', 'asc')->get()->last();
                            $data['palabras'][$mapa->id] += isset($palabras->contenido)? count(explode(" ", $palabras->contenido)):0;
                            $data['titulos'][$mapa->id] += count($palabras)>0? 1:0;
                            $data['Actualizacion'][$mapa->id] = isset($palabras->created_at) && $data['Actualizacion'][$mapa->id] < $palabras->created_at? $palabras->created_at : $data['Actualizacion'][$mapa->id];
                        }
                        $data['Actualizacion'][$mapa->id] = $this->fecha(date('Y-m-d', strtotime($data['Actualizacion'][$mapa->id])));
?>
<div class="col-sm-12 col-md-4 swing-in-top-fwd my-4">
    <div class="card-front">
        <img src="/images/file/clientes/<?=$user->foto?>" alt="User Avatar" class="img-circle img-avatar">
        <p class="h2"><span><?=$mapa->nombre?></span></p>
        <p class="h3"><?=$nombres[0].' '.$apellidos[0]?></p>
        <p class="h4">Creado: <span><?=$this->fecha(date('Y-m-d', strtotime($mapa->created_at)))?></span></p>
        <p class="h5"><a class="text-white" href="/mapa-lechero/version/<?=$mapa->id?>"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="bottom" title="Editar Mapa Lechero"></i></a></p>
        <?php if($eliminar == "Si"){ ?>
        <p class="h5 ml-5"><i class="fa fa-times" data-toggle="tooltip" data-placement="bottom" title="Eliminar Mapa Lechero" onclick="eliminar(<?=$mapa->id?>)"></i></p>
        <?php } ?>
        <div class="container">
            <div class="row ul mr-3" id="<?=$mapa->id?>">
                <div class="col-md-12">
                    <h5 class="mt-4 text-white">V</h5>
                </div>
                <div class="col-md-12">
                    <h5 class="mt-4 text-white">P</h5>
                </div>
                <div class="col-md-12">
                    <h5 class="mt-4 text-white">T</h5>
                </div>
                <div class="col-md-12">
                    <h5 class="mt-4 text-white">A</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
                    }
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                }
                break;
            /*Eliminar un mapa lechero*/
            case 1:
                $mapa = MapaLecheros::find($request->id);
                $mapa->deleted_at = date('Y-m-d H:i:s');
                if($mapa->save()){
                    $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'No se elimino correctamente!';
                }
                break;
            case 2:
                $mapa = new MapaLecheros;
                $mapa->nombre = $request->nombre;
                $mapa->id_user = Auth::user()->id;
                if($mapa->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'No se guardo correctamente!';
                }
                break;
            case 3:
                $titulos = MapaLecheroTitulos::all();
                ob_start(); ?>
                <div class="container">
                   <?php $i=1; foreach($titulos as $titulo){ ?>
                    <div class="row mb-3">
                        <div class="col-md-10 text-left"><?=$i.'. '.$titulo->titulo?></div>
                        <div class="col-md-1"><a class="text-warning" style="cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Editar <?=$titulo->titulo?>" onclick="editar_titulo_1(<?=$titulo->id?>,'<?=$titulo->titulo?>')"><i class="fa fa-pencil"></i></a></div>
                        <div class="col-md-1"><a class="text-danger" style="cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar <?=$titulo->titulo?>" onclick="eliminar_titulo(<?=$titulo->id?>)"><i class="fa fa-times"></i></a></div>
                    </div>
                    <?php $i++; } ?>
                </div>
                <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
            case 4:
                $dato = MapaLecheroTitulos::find($request->id);
                $dato->titulo = $request->titulo;
                if($dato->save()){
                  $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            case 5:
                $dato = MapaLecheroTitulos::find($request->id);
                $dato->deleted_at = date('Y-m-d H:i:s');
                if($dato->save()){
                  $data['msj'] = 'Eliminado correctamente!';
                }else{
                    $data['msj'] = 'Error al eliminar!';
                }
                break;
            case 6:
                $dato = new MapaLecheroTitulos;
                $dato->titulo = $request->titulo;
                $dato->id_user = Auth::user()->id;
                if($dato->save()){
                  $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
        }
        return \Response::json(['data'=>$data]);
    }

    public function version($id){
        $data['mapa'] = MapaLecheros::find($id);
        $data['titulos'] = MapaLecheroTitulos::all();
        $usuario= DB::SELECT('SELECT * FROM users U WHERE U.id = '.$data['mapa']->id_user);
        $data['user'] = $usuario[0];
        $data['fecha_creado'] = $this->fecha(date('Y-m-d', strtotime($data['mapa']->created_at)));

        $titulos = MapaLecheroTitulos::all();
        foreach($titulos as $titulo){
            $palabras = MapaLecheroVersiones::where('id_mapa_lechero',$id)->where('id_mapa_lechero_titulo',$titulo->id)->orderBy('created_at', 'asc')->get()->last();
            $data['palabras'][$titulo->id] = isset($palabras->contenido)? count(explode(" ", $palabras->contenido)):0;
            $data['ultima_modificacion'][$titulo->id] = $this->fecha(date('Y-m-d', strtotime($titulo->created_at)));
        }

        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `id` = 55 ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/

        /*Permiso para acceder al modulo*/
        $modulo=18;
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Crear y editar versiones" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC');
            if(isset($permiso[0]->permiso)){
			  $data['create_edit']=$permiso[0]->permiso;
            }
          }
        }
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Ver versiones" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "propia"){
                  if($data['mapa']->id_user == Auth::user()->id){
                      return view('mapalechero.mapalecheroeditar',['data' => $data]);
                  }else{
                      return view('oportunidades.nopermiso',['data' => $data]);
                  }
			  }else if($permiso1[0]->permiso == "TODAS"){
				return view('mapalechero.mapalecheroeditar',['data' => $data]);
			  }else{
                  return view('oportunidades.nopermiso',['data' => $data]);
              }
            }
          }
        }
        /*Fin Permiso para acceder al modulo*/
    }

    public function action2(Request $request){
        $accion = $request->accion;
        switch($accion){
            /*Version para cada titulo*/
            case 0:
                $versiones = MapaLecheroVersiones::where('id_mapa_lechero',$request->mapa)->where('id_mapa_lechero_titulo',$request->titulo)->orderBy('id','DESC')->get();
                ob_start(); ?>
                 <div class="col-sm-12 col-md-1 mt-2">
                     <div class="arrows" data-toggle="tooltip" data-placement="top" title="Versiones recientes" onclick="anterior_pagina(<?=$request->titulo?>)"></div>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2" id="crear-version" onclick="crear_version(<?=$request->mapa?>,<?=$request->titulo?>)">
                     <i class="fa fa-plus-square fx-10" data-toggle="tooltip" data-placement="top" title="Crear Versión"></i>
                 </div>
                 <?php
                if(count($versiones) >= 4){
                    $i=0; foreach($versiones as $version){ if($i<4){ ?>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" <?php if($i==0){ ?> title="Versión actual <?=count($versiones)?>" <?php }else{ ?>title="Versión anterior <?=count($versiones)-$i?>"<?php } ?> onclick="verversion(<?=$version->id?>)">
                     <p <?php if($i==0){ ?>class="actual"<?php }else{ ?>class="anterior"<?php } ?> >V<?=count($versiones)-$i?></p>
                     <em><?=count(explode(" ", $version->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($version->created_at))))?></em>
                 </div>
                 <?php
                    $i++; } }
                }else{
                    switch(count($versiones)){
                        case 0:
                ?>
                 <div class="col-sm-12 col-md-8 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                        case 1:
                ?>
                <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión actual <?=count($versiones)?>" onclick="verversion(<?=$versiones[0]->id?>)">
                     <p class="actual">V<?=count($versiones)?></p>
                     <em><?=count(explode(" ", $versiones[0]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[0]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-6 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                        case 2:
                ?>
                <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión actual <?=count($versiones)?>" onclick="verversion(<?=$versiones[0]->id?>)">
                     <p class="actual">V<?=count($versiones)?></p>
                     <em><?=count(explode(" ", $versiones[0]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[0]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión anterior <?=count($versiones)-1?>" onclick="verversion(<?=$versiones[1]->id?>)">
                     <p class="anterior">V<?=count($versiones)-1?></p>
                     <em><?=count(explode(" ", $versiones[1]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[1]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-4 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                        case 3:
                            ?>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión actual <?=count($versiones)?>" onclick="verversion(<?=$versiones[0]->id?>)">
                     <p class="actual">V<?=count($versiones)?></p>
                     <em><?=count(explode(" ", $versiones[0]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[0]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión anterior <?=count($versiones)-1?>" onclick="verversion(<?=$versiones[1]->id?>)">
                     <p class="anterior">V<?=count($versiones)-1?></p>
                     <em><?=count(explode(" ", $versiones[1]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[1]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión anterior <?=count($versiones)-2?>" onclick="verversion(<?=$versiones[2]->id?>)">
                     <p class="anterior">V<?=count($versiones)-2?></p>
                     <em><?=count(explode(" ", $versiones[2]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[2]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                    } } ?>
                 <div class="col-sm-12 col-md-1 mt-2">
                     <div class="arrows-2" data-toggle="tooltip" data-placement="top" title="Versiones anteriores" onclick="siguiente_pagina(<?=$request->titulo?>)"></div>
                 </div>
                <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
            /*Paginado de versiones*/
            case 1:
                $X = $request->paginado*4;
                $query = 'SELECT * FROM mapa_lechero_versiones WHERE id_mapa_lechero_titulo = '.$request->titulo.' AND id_mapa_lechero = '.$request->mapa.' AND deleted_at IS NULL ORDER BY id DESC LIMIT '.$X.',4';
                $versiones = DB::select($query);

                $query = 'SELECT * FROM mapa_lechero_versiones WHERE id_mapa_lechero_titulo = '.$request->titulo.' AND id_mapa_lechero = '.$request->mapa.' AND deleted_at IS NULL ORDER BY id DESC LIMIT '.$X.',5';
                $versiones_2 = DB::select($query);
                $data['parar'] = count($versiones_2)==5?'no':'si';

                $query = 'SELECT * FROM mapa_lechero_versiones WHERE id_mapa_lechero_titulo = '.$request->titulo.' AND id_mapa_lechero = '.$request->mapa.' AND deleted_at IS NULL ORDER BY id DESC';
                $versiones_todas = DB::select($query);
                ob_start(); ?>
                <?php
                if(count($versiones) >= 4){
                    $i=0; foreach($versiones as $version){ if($i<4){ ?>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" <?php if($i==0 && $request->paginado==0){ ?> title="Versión actual <?=count($versiones_todas)?>" <?php }else{ ?>title="Versión anterior <?=count($versiones_todas)-$X-$i?>"<?php } ?> onclick="verversion(<?=$version->id?>)">
                     <p <?php if($i==0 && $request->paginado==0){ ?>class="actual"<?php }else{ ?>class="anterior"<?php } ?> >V<?=count($versiones_todas)-$X-$i?></p>
                     <em><?=count(explode(" ", $version->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($version->created_at))))?></em>
                 </div>
                 <?php
                    $i++; } }
                }else{
                    switch(count($versiones)){
                        case 0:
                ?>
                 <div class="col-sm-12 col-md-8 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                        case 1:
                ?>
                <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" <?php if($request->paginado==0){ ?>title="Versión actual <?=count($versiones_todas)?>"<?php }else{ ?>title="Versión anterior <?=count($versiones_todas)-$X?>"<?php } ?> onclick="verversion(<?=$versiones[0]->id?>)">
                     <p <?php if($request->paginado==0){ ?>class="actual"<?php }else{ ?>class="anterior"<?php } ?>>V<?=count($versiones_todas)-$X?></p>
                     <em><?=count(explode(" ", $versiones[0]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[0]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-6 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                        case 2:
                ?>
                <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" <?php if($request->paginado==0){ ?>title="Versión actual <?=count($versiones_todas)?>"<?php }else{ ?>title="Versión anterior <?=count($versiones_todas)-$X?>"<?php } ?> onclick="verversion(<?=$versiones[0]->id?>)">
                     <p <?php if($request->paginado==0){ ?>class="actual"<?php }else{ ?>class="anterior"<?php } ?>>V<?=count($versiones_todas)-$X?></p>
                     <em><?=count(explode(" ", $versiones[0]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[0]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión anterior <?=count($versiones_todas)-$X-1?>" onclick="verversion(<?=$versiones[1]->id?>)">
                     <p class="anterior">V<?=count($versiones_todas)-$X-1?></p>
                     <em><?=count(explode(" ", $versiones[1]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[1]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-4 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                        case 3:
                            ?>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" <?php if($request->paginado==0){ ?>title="Versión actual <?=count($versiones_todas)?>"<?php }else{ ?>title="Versión anterior <?=count($versiones_todas)-$X?>"<?php } ?> onclick="verversion(<?=$versiones[0]->id?>)" >
                     <p <?php if($request->paginado==0){ ?>class="actual"<?php }else{ ?>class="anterior"<?php } ?>>V<?=count($versiones_todas)-$X?></p>
                     <em><?=count(explode(" ", $versiones[0]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[0]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión anterior <?=count($versiones_todas)-$X-1?>" onclick="verversion(<?=$versiones[1]->id?>)">
                     <p class="anterior">V<?=count($versiones_todas)-$X-1?></p>
                     <em><?=count(explode(" ", $versiones[1]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[1]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 version" data-toggle="tooltip" data-placement="top" title="Versión anterior <?=count($versiones_todas)-$X-2?>" onclick="verversion(<?=$versiones[2]->id?>)">
                     <p class="anterior">V<?=count($versiones_todas)-$X2?></p>
                     <em><?=count(explode(" ", $versiones[2]->contenido))?> palabras</em>
                     <em><?=$this->fecha(date('Y-m-d',(strtotime($versiones[2]->created_at))))?></em>
                 </div>
                 <div class="col-sm-12 col-md-2 mt-2 no-version">
                     <p class="mt-5"><em>No existen más versiones</em></p>
                 </div>
                <?php       break;
                    } } ?>
                <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
            case 2:
                $query = 'SELECT * FROM mapa_lechero_versiones WHERE id_mapa_lechero_titulo = '.$request->titulo.' AND id_mapa_lechero = '.$request->mapa.' AND deleted_at IS NULL ORDER BY id DESC';
                $ultima_version = DB::select($query);
                ob_start(); ?>
                <div class="col-sm-12 col-md-12 pt-4">
                    <div class="comment-wrap">
                        <div class="photo">
                            <div class="avatar" style="background-image: url('/images/file/clientes/<?=Auth::user()->foto?>')" data-toggle="tooltip" data-placement="top" title="<?=Auth::user()->name?>"></div>
                        </div>
                        <div class="comment-block">
                            <form autocomplete="off" id="messages_box">
                               <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..."></textarea>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <textarea id="comentario_administrador"><?php if(isset($ultima_version[0]->contenido)){ print $ultima_version[0]->contenido; }?></textarea>
                </div>

                <?php
                $data['comentario'] = ob_get_contents();
                ob_end_clean();
                ob_start(); ?>
                    <div class="col-md-12">
                        <a class="snip1489" data-toggle="tooltip" data-placement="top" title="Guardar Versión" onclick="guardar_version(<?=$request->mapa?>,<?=
$request->titulo?>)"><i class="fa fa-save" aria-hidden="true"></i></a>
                    </div>
                <?php
                $data['btn_guardar'] = ob_get_contents();
                ob_end_clean();
                break;
            case 3:
                $dato = new MapaLecheroVersiones;
                $dato->contenido = $request->contenido;
                $dato->comentario = $request->comentario;
                $dato->id_user = Auth::user()->id;
                $dato->id_mapa_lechero_titulo = $request->titulo;
                $dato->id_mapa_lechero = $request->mapa;
                if($dato->save()){
                    $data['msj'] = 'Guardado correctamente!';
                }else{
                    $data['msj'] = 'Error al guardar!';
                }
                break;
            case 4:
                $version = MapaLecheroVersiones::find($request->version);
                $user = DB::SELECT('SELECT * FROM users U WHERE U.id = '.$version->id_user);
                $usuario = $user[0];
                ob_start(); ?>
                <div class="col-sm-12 col-md-12 pt-4">
                    <div class="comment-wrap">
                        <div class="photo">
                            <div class="avatar" style="background-image: url('/images/file/clientes/<?=$usuario->foto?>')" data-toggle="tooltip" data-placement="top" title="<?=$usuario->name?>"></div>
                        </div>
                        <div class="comment-block">
                            <form autocomplete="off" id="messages_box">
                               <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..." readonly><?=$version->comentario?></textarea>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                   <h3 class="my-4">Contenido de la versión</h3>
                    <textarea id="comentario_administrador"><?=$version->contenido?></textarea>
                </div>

                <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
            case 5:
                $archivos = MapaLecheroArchivos::where('id_mapa_lechero',$request->id)->get();
                ob_start(); ?>
                <div class="container mt-5">
                   <div class="row my-3">
                      <div class="col-md-12 text-center">
                            <a class="text-info" style="font-size: 4rem; cursor:pointer;" title="Cargar imagen" onclick="subir_imagen()"><i class="fa fa-plus"></i></a>
                      </div>
                   </div>
                    <div class="row">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th class="text-center" scope="col">Imagen</th>
                              <th class="text-center" scope="col">nombre</th>
                              <th class="text-center" scope="col">Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php $i = 1; foreach($archivos as $archivo){ ?>
                            <tr>
                              <th scope="row" style="width:10%"><?=$i?></th>
                              <td style="width:20%"><img src="<?=$archivo->archivo?>" style="width: 50%"></td>
                              <td><p class="h4"><?=$archivo->nombre?></p></td>
                              <td style="width:10%"><a class="text-info" style="font-size: 2rem; cursor:pointer;" title="Copiar link" onclick="getlink('<?=$archivo->archivo?>')"><i class="fa fa-link"></i></a></td>
                            </tr>
                            <?php $i++; } ?>
                          </tbody>
                        </table>
                    </div>
                </div>

                <?php
                $data['contenido'] = ob_get_contents();
                ob_end_clean();
                break;
            case 6:
                $image = $request->file('imagen');
                var_dump($image);
                $imageName = uniqid();
                $imegeExtension = $image->getClientOriginalExtension();
                $imageName = $imageName.'.'.$imegeExtension;
                $NameOriginal = $image->getClientOriginalName();
                $image->move(public_path('storage/MapaLechero'),$imageName);

                $dato = new MapaLecheroArchivos;
                $dato->nombre = $NameOriginal;
                $dato->archivo = "/storage/MapaLechero/".$imageName;
                $dato->id_mapa_lechero = $request->id_mapa_lechero;
                $dato->save();
                $data['msj'] = 'Guardado correctamente!';
                break;
        }
        return \Response::json(['data'=>$data]);
    }

    function fecha($fecha){
        if($fecha != ''){
            $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            $f=explode('-',$fecha);
            $fecha_formato = $f[2].'/'.$meses[intval($f[1])].'/'.$f[0];
            return $fecha_formato;
        }else{
            return "No existe fecha";
        }
    }
}
