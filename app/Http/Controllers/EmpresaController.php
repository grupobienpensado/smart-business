<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Empresa;
use App\EmpresaSedes;
use App\EmpresaTelefonos;
use App\EmpresaSedesTelefonos;
use App\EmpresaRedessociales;
use App\Cliente;
use App\Oportunidades;
use App\EmpresaComentario;
use App\EmpresaSedeComentario;
use Auth;
header("Content-Type: text/html;charset=utf-8");
date_default_timezone_set("America/Bogota");
class EmpresaController extends Controller
{
    public function index()
    {
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	$modulo=5;
    	$acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="crear empresa" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				return view('empresa.create',["data" => $data]);
			  }else{
				return view('oportunidades.nopermiso');
			  }
            }
          }          
        }
    }

    public function indexedit($id)
    {
    	$model = Empresa::findOrFail($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('empresa.editempresa',['data' => $data, 'model' => $model]);
    }

    public function eliminarEmpresa($id){
    	$model = Empresa::find($id);
    	if (isset($model) && !empty($model)) {
            $sedes = EmpresaSedes::where("empresa_id", $id)->get();
            foreach($sedes as $sede){
                $datos = EmpresaSedeComentario::where("empresa_sede_id", $sede->id)->get();
                foreach($datos as $dato){
                    $dato->delete();
                }

                $datos = EmpresaSedesTelefonos::where("empresa_sede_id", $sede->id)->get();
                foreach($datos as $dato){
                    $dato->delete();
                }

                $sede->delete();
            }

            $telefonos = EmpresaTelefonos::where("empresas_id", $id)->get();
            foreach($telefonos as $telefono){
                $telefono->delete();
            }

            $datos = EmpresaComentario::where("empresa_id", $id)->get();
            foreach($datos as $dato){
                $dato->delete();
            }

            $datos = EmpresaRedessociales::where("empresas_id", $id)->get();
            foreach($datos as $dato){
                $dato->delete();
            }

    		$model->delete();
    		return \Response::json(['success' => true]);
    	}else{
    		return \Response::json(['success' => false]);
    	}    	
    }

    public function eliminarSede($id){
    	$model = EmpresaSedes::find($id);
    	if (isset($model) && !empty($model)) {
            $datos = EmpresaSedeComentario::where("empresa_sede_id", $id)->get();
            foreach($datos as $dato){
                $dato->delete();
            }

            $datos = EmpresaSedesTelefonos::where("empresa_sede_id", $id)->get();
            foreach($datos as $dato){
                $dato->delete();
            }
    		$model->delete();
    		return \Response::json(['success' => true]);
    	}else{
    		return \Response::json(['success' => false]);
    	}
    }

    public function indexeditsede($id)
    {
    	$sede = EmpresaSedes::findOrFail($id);
    	$model = Empresa::findOrFail($sede->empresa_id);
    	$telefonos = EmpresaSedesTelefonos::where('empresa_sede_id', $id)->get();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
        return view('empresa.sede.editsede',['data' => $data, 'sede' => $sede, 'model' => $model, 'telefonos' => $telefonos]);
    }

    public function indexsede($id){	
    	$model = Empresa::findOrFail($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('empresa.sede.create',['data' => $data, 'model' => $model]);
    }

    /**
	* guarda un archivo en nuestro directorio local.
	*
	* @return Response
	*/
	public function save(Request $request){	

		$logo = $request->logo_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombrelogo =$antenombre.".png";
            \Storage::disk('empresa')->put($nombrelogo,  $foto);
        }
        $principal = $request->principal_values;
        if (!empty($principal)) {
            $imagen = json_decode($principal);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombreprincipal =$antenombre.".png";
            \Storage::disk('empresa')->put($nombreprincipal,  $foto);
        }  


        
	    $dato = new Empresa;
		$dato->nombre = $request->nombre;
		$dato->nit = $request->nit;
		if (isset($nombrelogo)) {
			$dato->logo = $nombrelogo;
		}
		if (isset($nombreprincipal)) {
			$dato->principal = $nombreprincipal;
		}
		$dato->tipo_identificacion = $request->tipo_identificacion;
		$dato->dv = $request->dv;
		$dato->finauguracion = $request->finauguracion;
		$dato->ciudad = $request->ciudad;
		try {
			if (!empty($request->cliente_id)) {			
				$cliente = Cliente::find($request->cliente_id);
				$dato->nombre_p_a_cargo = $cliente->nombres." ".$cliente->apellidos;	
			}
		} catch (Exception $e) {
			
		}		
		$dato->cliente_id = $request->cliente_id;
		$dato->cargo = $request->cargo;
		$dato->pagina_web = $request->pagina_web;
		$dato->email_contacto = $request->email_contacto;
		$dato->email_contabilidad = $request->email_contabilidad;
		$dato->observaciones = $request->observaciones;
	    $dato->user_id       = Auth::user()->id;
	    $dato->responsable   = $request->responsable;
	    $dato->save();
	    
	    /*foreach ($request->telefono as $key) {
	    	if (isset($key["telefono_numero"])) {
	    		$telefono = new EmpresaTelefonos;
		    	$telefono->empresas_id = $dato->id;
			    $telefono->telefono_tipo = $key["telefono_tipo"];
				$telefono->telefono_indp = $key["telefono_indp"];
				$telefono->telefono_indc = $key["telefono_indc"];
				$telefono->telefono_numero = $key["telefono_numero"];
				$telefono->telefono_ext = $key["telefono_ext"];
				$telefono->save();
	    	}		    	
	    }*/

	    foreach ($request->redsocial as $key) {
	    	if (isset($key["valor"])) {
	    		$redsocial = new EmpresaRedessociales;
		    	$redsocial->empresas_id = $dato->id;
			    $redsocial->tipo = $key["tipo"];
				$redsocial->valor = $key["valor"];
				$redsocial->save();
	    	}	    	
	    }
	   

	   return \Response::json(['success' => true, 'id' => $dato->id]);
	}

	public function editsave(Request $request){	

		$logo = $request->logo_values;
        if (!empty($logo)) {
            $imagen = json_decode($logo);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombrelogo =$antenombre.".png";
            \Storage::disk('empresa')->put($nombrelogo,  $foto);
        }
        $principal = $request->principal_values;
        if (!empty($principal)) {
            $imagen = json_decode($principal);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombreprincipal =$antenombre.".png";
            \Storage::disk('empresa')->put($nombreprincipal,  $foto);
        }  
        
	    $dato = Empresa::findOrFail($request->id);
		$dato->nombre = $request->nombre;
		$dato->nit = $request->nit;
		if (isset($nombrelogo)) {
			$dato->logo = $nombrelogo;
		}
		if (isset($nombreprincipal)) {
			$dato->principal = $nombreprincipal;
		}
		$dato->tipo_identificacion = $request->tipo_identificacion;
		$dato->dv = $request->dv;
		$dato->finauguracion = $request->finauguracion;
		$dato->ciudad = $request->ciudad;
		if (isset($request->cliente_id) && !empty($request->cliente_id)) {
			$cliente = Cliente::findorfail($request->cliente_id);
			$dato->nombre_p_a_cargo = $cliente->nombres." ".$cliente->apellidos;
			$dato->cliente_id = $request->cliente_id;
		}	

		$dato->cargo = $request->cargo;
		$dato->pagina_web = $request->pagina_web;
		$dato->email_contacto = $request->email_contacto;
		$dato->email_contabilidad = $request->email_contabilidad;
		$dato->observaciones = $request->observaciones;
		$dato->user_id       = Auth::user()->id;
	    $dato->responsable   = $request->responsable;
	    $dato->save();
	    
	    /*foreach ($request->telefono as $key) {
	    	if (isset($key["telefono_numero"])) {
	    		$telefono = new EmpresaTelefonos;
		    	$telefono->empresas_id = $dato->id;
			    $telefono->telefono_tipo = $key["telefono_tipo"];
				$telefono->telefono_indp = $key["telefono_indp"];
				$telefono->telefono_indc = $key["telefono_indc"];
				$telefono->telefono_numero = $key["telefono_numero"];
				$telefono->telefono_ext = $key["telefono_ext"];
				$telefono->save();
	    	}
		    	
	    }*/

	    $EmpresaRedessociales = EmpresaRedessociales::where('empresas_id', $dato->id)->get();
	    foreach ($EmpresaRedessociales as $key => $value) {
	    	$model = EmpresaRedessociales::findorfail($value->id);
	        $model->delete();
	    }
	    


	    foreach ($request->redsocial as $key) {
	    	if (isset($key["valor"])) {
	    		$redsocial = new EmpresaRedessociales;
		    	$redsocial->empresas_id = $dato->id;
			    $redsocial->tipo = $key["tipo"];
				$redsocial->valor = $key["valor"];
				$redsocial->save();
	    	}	    	
	    }
	   

	   return \Response::json(['success' => true]);
	}

	public function savesede(Request $request){	
		
		$principal = $request->principal_values;
        if (!empty($principal)) {
            $imagen = json_decode($principal);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombreprincipal =$antenombre.".png";
            \Storage::disk('empresasede')->put($nombreprincipal,  $foto);
        }

	    $dato = new EmpresaSedes;
	    if (isset($nombreprincipal)) {
			$dato->principal = $nombreprincipal;
		}

		$dato->empresa_id = $request->empresa_id;
		$dato->pais = $request->pais;		
		$dato->departamento = $request->departamento;
		$dato->ciudad = $request->ciudad;
		$dato->direccion = $request->direccion;
		$dato->finauguracion = $request->finauguracion;
		$dato->importancia_sede = $request->importancia_sede;
		$dato->tipo_sede = $request->tipo_sede;
		$dato->litros_dia = $request->litros_dia;
		$dato->encargado = $request->encargado;
		$dato->email_contacto = $request->email_contacto;
		$dato->email_contabilidad = $request->email_contabilidad;
		$dato->lat = $request->lat;
		$dato->lng = $request->lng;
		$dato->observaciones = $request->observaciones;
		$dato->user_id = Auth::user()->id;
	    $dato->save();
	    
	    foreach ($request->telefono as $key) {
	    	if (isset($key["telefono_numero"])) {
	    		$telefono = new EmpresaSedesTelefonos;
		    	$telefono->empresa_sede_id = $dato->id;
			    $telefono->telefono_tipo = $key["telefono_tipo"];
				$telefono->telefono_indp = $key["telefono_indp"];
				$telefono->telefono_indc = $key["telefono_indc"];
				$telefono->telefono_numero = $key["telefono_numero"];
				$telefono->telefono_ext = $key["telefono_ext"];
				$telefono->save();
	    	}
	    }
	   	return redirect('/agregarsede/'.$request->empresa_id);
	}

	public function editsede(Request $request){	
		$principal = $request->principal_values;
        if (!empty($principal)) {
            $imagen = json_decode($principal);           
            $data = explode( ',', $imagen->data );
            $foto = base64_decode($data[1]);

            $antenombre = uniqid();
            $nombreprincipal =$antenombre.".png";
            \Storage::disk('empresasede')->put($nombreprincipal,  $foto);
        }

	    $dato = EmpresaSedes::findOrFail($request->id);
	    if (isset($nombreprincipal)) {
			$dato->principal = $nombreprincipal;
		}

		$dato->empresa_id = $request->empresa_id;
		$dato->pais = $request->pais;		
		$dato->departamento = $request->departamento;
		$dato->ciudad = $request->ciudad;
		$dato->direccion = $request->direccion;
		$dato->finauguracion = $request->finauguracion;
		$dato->importancia_sede = $request->importancia_sede;
		$dato->tipo_sede = $request->tipo_sede;
		$dato->litros_dia = $request->litros_dia;
		$dato->encargado = $request->encargado;
		$dato->email_contacto = $request->email_contacto;
		$dato->email_contabilidad = $request->email_contabilidad;
		$dato->lat = $request->lat;
		$dato->lng = $request->lng;
		$dato->observaciones = $request->observaciones;
		$dato->user_id = Auth::user()->id;
	    $dato->save();

	    $EmpresaSedesTelefonos = EmpresaSedesTelefonos::where('empresa_sede_id', $dato->id)->get();
	    foreach ($EmpresaSedesTelefonos as $key => $value) {
	    	$model = EmpresaSedesTelefonos::findorfail($value->id);
	        $model->delete();
	    }
	    
	    foreach ($request->telefono as $key) {
	    	if (isset($key["telefono_numero"])) {
	    		$telefono = new EmpresaSedesTelefonos;
		    	$telefono->empresa_sede_id = $dato->id;
			    $telefono->telefono_tipo = $key["telefono_tipo"];
				$telefono->telefono_indp = $key["telefono_indp"];
				$telefono->telefono_indc = $key["telefono_indc"];
				$telefono->telefono_numero = $key["telefono_numero"];
				$telefono->telefono_ext = $key["telefono_ext"];
				$telefono->save();
	    	}
	    }
	   	return redirect('/empresasede/'.$request->id);
	}
	

    public function listmap(){
        /**Permiso Exportar Excel y CSV**/
        $modulo=5;
        $data['permiso_exportar'] = "No";
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Exportar Excel y CSV" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){
                $data['permiso_exportar'] = "Si";
              }
            }
          }
        }
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
  		return view('empresa.list',['data' => $data]);
    }

    public function listmapjson(){
        $query_porcentaje = 'CONVERT((IF(IsNull(E.logo), 0, (100/26)*3) + IF(IsNull(E.principal), 0, (100/26)*5) + IF(IsNull(E.nombre), 0, (100/26)*1) + IF(IsNull(E.nit), 0, (100/26)*1) + IF(IsNull(E.pagina_web), 0, (100/26)*1) + IF(IsNull(E.finauguracion), 0, (100/26)*1) + IF(IsNull(E.ciudad), 0, (100/26)*1) + IF(IsNull(E.nombre_p_a_cargo), 0, (100/26)*3) + IF(IsNull(E.cargo), 0, (100/26)*1) + IF(IsNull(E.email_contacto), 0, (100/26)*1) + IF(IsNull(E.email_contabilidad), 0, (100/26)*1) + IF(IsNull(E.observaciones), 0, (100/26)*3)),UNSIGNED INTEGER) + ';
        $query_porcentaje_redes = '( SELECT CONVERT(IF(COUNT(*) > 0, (100/26)*3, 0), UNSIGNED INTEGER) FROM empresa_redessociales ER WHERE ER.empresas_id = E.id ) AS Porcentaje';
        $query_campos_faltante = 'CONCAT(IF(IsNull(E.logo), "* Logo&#10;", ""), IF(IsNull(E.principal), "* Foto&#10;", ""), IF(IsNull(E.nombre), "* Nombre&#10;", ""), IF(IsNull(E.nit), "* Nit&#10;", ""), IF(IsNull(E.pagina_web), "* Pagina web&#10;", ""), IF(IsNull(E.finauguracion), "* Fecha de inaguración&#10;", ""), IF(IsNull(E.ciudad), "* Ciudad&#10;", ""), IF(IsNull(E.nombre_p_a_cargo), "* Nombre de persona de mas alto cargo&#10;", ""), IF(IsNull(E.cargo), "* Cargo&#10;", ""), IF(IsNull(E.email_contacto), "* Email de contacto&#10;", ""), IF(IsNull(E.email_contabilidad), "* Email de contabilidad&#10;", ""), IF(IsNull(E.observaciones), "* Observaciones&#10;", ""), ';
        $query_campos_faltante.= '( SELECT IF(COUNT(*) = 0, "* Redes sociales&#10;", "") FROM empresa_redessociales ER WHERE ER.empresas_id = E.id )) AS Campos_faltantes';
    	$modulo=5;
    	$acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="listar, mapa, mosaico" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Ver propio"){ 
				$model = DB::select('SELECT E.id,E.logo,E.nombre,U.name,E.created_at, E.updated_at, '.$query_porcentaje.$query_porcentaje_redes.','.$query_campos_faltante.' FROM empresas E INNER JOIN users U ON U.id = E.responsable AND E.deleted_at IS NULL WHERE E.responsable="'.Auth::user()->id.'"');
			  }else{
				$model = DB::select('SELECT E.id,E.logo,E.nombre,U.name,E.created_at, E.updated_at, '.$query_porcentaje.$query_porcentaje_redes.','.$query_campos_faltante.' FROM empresas E INNER JOIN users U ON U.id = E.responsable AND E.deleted_at IS NULL');
			  }
            }
          }          
        }

        $permiso_editar='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar en listar y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){ 
				$permiso_editar="Si";
			  }else{
				$permiso_editar="No";
			  }
            }
          }          
        }
        $permiso_eliminar='No';
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón eliminar" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
			  if($permiso2[0]->permiso == "Si"){ 
				$permiso_eliminar="Si";
			  }else{
				$permiso_eliminar="No";
			  }
            }
          }          
        }

        
    	$contador=count($model);
    	$valid_tags = [];
    	foreach ($model as $value) {    		
    		$temp = DB::select('SELECT SUM(litros_dia) AS tlitros_dia FROM empresa_sedes WHERE empresa_id = '.$value->id.' AND `deleted_at` IS NULL');
    		$sedes = DB::select('SELECT ciudad FROM empresa_sedes WHERE empresa_id = '.$value->id.' AND `deleted_at` IS NULL');
    		$sprincipal = DB::select('SELECT pais FROM empresa_sedes WHERE importancia_sede = "Principal" AND empresa_id = '.$value->id.' AND `deleted_at` IS NULL');
    		$cliente = DB::select('SELECT tratamiento, cliente FROM clientes WHERE empresa_id = '.$value->id.' AND `deleted_at` IS NULL');
    		$opp = DB::select('SELECT id FROM oportunidades WHERE empresa_id = '.$value->id);
    		$redes = DB::select('SELECT tipo, valor FROM empresa_redessociales WHERE empresas_id = '.$value->id);
			$total = 0;
			foreach ($opp as $value2) {
				$hola = DB::select("SELECT COUNT(id) AS valor FROM historial_oportunidades WHERE `key` = 'ciclo_venta' AND value <> 0 AND value <> 100 AND value <> 90 AND oportunidad_id = ".$value2->id." GROUP BY oportunidad_id");
				if (isset($hola[0])) {
				  $hola = $hola[0]->valor;
				  if ($hola>0) {
				    $total = $total + 1;
				  }
				}
			}
			/*\Carbon\Carbon::setLocale('es');
	        $dt2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at);
	        $dt3 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at);
	        $value->updated_at = $dt3->format('d/m/Y H:i');
	        $value->created_at = $dt2->format('d/m/Y H:i');*/
    		$value->nsedes = count($sedes);
    		$value->sedes = $sedes;
    		$value->redes = $redes;
    		$nombre   = explode(" ",$value->name);
    		if (isset($nombre[2])) {
    			$value->name = $nombre[0]." ".$nombre[2];
    		}else{
    			$value->name = $nombre[0]." ".$nombre[1];
    		}
    		$value->tlitros_dia = number_format($temp[0]->tlitros_dia);
    		if (isset($sprincipal[0]->pais) && !empty($sprincipal[0]->pais)) {
	    		$value->pais = $sprincipal[0]->pais;
	    	}
    		$value->nclient = count($cliente);
    		$value->clientes = $cliente;
    		$value->numop = $total;
            $value->faltante = !empty($value->Campos_faltantes)?"Datos por completar:&#10;".substr($value->Campos_faltantes, 0, -5):"No faltan datos";
    		$valid_tags[] = $value;
    	}   

  		return \Response::json(['success' => true, 'model' => $valid_tags, 'contador' =>$contador, 'permiso_editar' => $permiso_editar, 'permiso_eliminar' => $permiso_eliminar]);
    }

    public function listsedesmapjson(){

    	$modulo=5;
    	$acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="listar, mapa, mosaico" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Ver propio"){ 
				$model = DB::select('SELECT E.id, E.logo, S.pais, S.ciudad, S.litros_dia, S.lat, S.lng, E.nombre FROM empresa_sedes S INNER JOIN empresas E ON S.empresa_id = E.id INNER JOIN empresa_redessociales ER ON E.id = ER.empresas_id WHERE E.deleted_at IS NULL AND S.lng IS NOT NULL AND S.lat IS NOT NULL AND E.responsable="'.Auth::user()->id.'"');
			  }else{
				$model = DB::select('SELECT E.id, E.logo, S.pais, S.ciudad, S.litros_dia, S.lat, S.lng, E.nombre FROM empresa_sedes S INNER JOIN empresas E ON S.empresa_id = E.id INNER JOIN empresa_redessociales ER ON E.id = ER.empresas_id WHERE E.deleted_at IS NULL AND S.lng IS NOT NULL AND S.lat IS NOT NULL');
			  }
            }
          }          
        }
    	    	  

  		return \Response::json(['success' => true, 'model' => $model]);
    }

    public function view($id){
    	$modulo=5;
    	$permiso_editar='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar en listar y ver" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				$permiso_editar="Si";
			  }else{
				$permiso_editar="No";
			  }
            }
          }          
        }

        $permiso_anadir_cliente='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón añadir cliente" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){ 
				$permiso_anadir_cliente="Si";
			  }else{
				$permiso_anadir_cliente="No";
			  }
            }
          }          
        }

        $permiso_anadir_sede='No';
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón añadir sede" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
			  if($permiso2[0]->permiso == "Si"){ 
				$permiso_anadir_sede="Si";
			  }else{
				$permiso_anadir_sede="No";
			  }
            }
          }          
        }
        $query_porcentaje = 'CONVERT((IF(IsNull(E.logo), 0, (100/26)*3) + IF(IsNull(E.principal), 0, (100/26)*5) + IF(IsNull(E.nombre), 0, (100/26)*1) + IF(IsNull(E.nit), 0, (100/26)*1) + IF(IsNull(E.pagina_web), 0, (100/26)*1) + IF(IsNull(E.finauguracion), 0, (100/26)*1) + IF(IsNull(E.ciudad), 0, (100/26)*1) + IF(IsNull(E.nombre_p_a_cargo), 0, (100/26)*3) + IF(IsNull(E.cargo), 0, (100/26)*1) + IF(IsNull(E.email_contacto), 0, (100/26)*1) + IF(IsNull(E.email_contabilidad), 0, (100/26)*1) + IF(IsNull(E.observaciones), 0, (100/26)*3)),UNSIGNED INTEGER) + ';
        $query_porcentaje.= '( SELECT CONVERT(IF(COUNT(*) > 0, (100/26)*3, 0), UNSIGNED INTEGER) FROM empresa_redessociales ER WHERE ER.empresas_id = E.id ) AS Porcentaje';
        $data['porcentaje'] = DB::select('SELECT '.$query_porcentaje.' FROM empresas E WHERE E.id = '.$id);
    	$model = Empresa::find($id);
    	$sedes = EmpresaSedes::where("empresa_id", $model->id)->get();
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('empresa.view',['data' => $data, 'model' => $model, 'sedes' => $sedes, 'permiso_editar' => $permiso_editar, 'permiso_anadir_cliente' => $permiso_anadir_cliente, 'permiso_anadir_sede' => $permiso_anadir_sede]);
    }

    public function viewsede($id){
    	$modulo=6;
    	$permiso_anadir_cliente='No';
        $acceso = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón añadir cliente" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
			  if($permiso[0]->permiso == "Si"){ 
				$permiso_anadir_cliente="Si";
			  }else{
				$permiso_anadir_cliente="No";
			  }
            }
          }          
        }
        $permiso_eliminar_sede='No';
        $acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón eliminar sede" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso1[0]->id)){
          $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo1[0]->id)){
            $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso1[0]->permiso)){
			  if($permiso1[0]->permiso == "Si"){ 
				$permiso_eliminar_sede="Si";
			  }else{
				$permiso_eliminar_sede="No";
			  }
            }
          }          
        }

        $permiso_editar_sede='No';
        $acceso2 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón editar sede" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso2[0]->id)){
          $cargo2 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo2[0]->id)){
            $permiso2 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso2[0]->id.'" AND `id_cargo`="'.$cargo2[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso2[0]->permiso)){
			  if($permiso2[0]->permiso == "Si"){ 
				$permiso_editar_sede="Si";
			  }else{
				$permiso_editar_sede="No";
			  }
            }
          }          
        }
    	$model = EmpresaSedes::find($id);
        /*Consulta para activar el item del menu correspondiente*/
        $query='SELECT * FROM `menu_crms` WHERE `item` LIKE "Empresas" ORDER BY `orden` DESC LIMIT 0, 1';
        $data['item_menu'] = DB::select($query);
        if(count($data['item_menu']) > 0){
            $data['item_menu']='menu_'.$data['item_menu'][0]->id;
        }else{
            $data['item_menu']='';
        }
        /*Fin Consulta para activar el item del menu correspondiente*/
    	return view('empresa.sede.view',['data' => $data, 'model' => $model, 'permiso_anadir_cliente'=>$permiso_anadir_cliente, 'permiso_eliminar_sede' => $permiso_eliminar_sede, 'permiso_editar_sede' => $permiso_editar_sede]);
    }
}
