<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'savefilecomen',
        'validarselectproducto',
        'registraroportunidad2',
		'cambiarmoneda',
		'savecomenoport',
		'uploadfiles',
		'savecomments',
		'ciclosave',
		'ventasave',
		'crearempresa',
		'OportunidadesIdentificadas',
        'savefilecomen90',
        'filtrarpresupuesto',
        'filtrarpresupuestoanual',
        'ajusteguardar',
        'filtrargastos',
        'filtrarajuste',
        'filtrargastosprueba',
        'login',
        'actividad/aprobar',
        'plantrabajo/aprobar',
        'feria/bitacora/crear',
        'aplazarpendiente',
        'confirmarpendiente',
        'cancelarpendiente',
        'aprobarpendiente',
        'crearpendiente',
    ];
}
