<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OportunidadActasAccion extends Model
{
	use SoftDeletes;
    protected $table = 'oportunidad_actas_acciones';
    protected $dates = ['deleted_at'];
}
