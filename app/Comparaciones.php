<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comparaciones extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
}
