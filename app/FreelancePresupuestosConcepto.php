<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreelancePresupuestosConcepto extends Model
{
    protected $table = 'freelance_presupuestos_concepto';
}
