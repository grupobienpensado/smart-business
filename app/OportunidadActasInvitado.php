<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OportunidadActasInvitado extends Model
{
	use SoftDeletes;
    protected $table = 'oportunidad_actas_invitados';
    protected $dates = ['deleted_at'];
}
