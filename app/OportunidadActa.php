<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OportunidadActa extends Model
{
	use SoftDeletes;
    protected $table = 'oportunidad_actas';
    protected $dates = ['deleted_at'];
}
