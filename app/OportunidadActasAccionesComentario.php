<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OportunidadActasAccionesComentario extends Model
{
    protected $table = 'oportunidad_actas_acciones_comentarios';
}
