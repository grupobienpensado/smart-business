<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        User::create(array(
            'username'  => 'usuario 5',
            'email'     => 'usuario5@admin.com',
            'name'=> 'usuario 5',
            'tipo'=> 'comercial',
            'password' => Hash::make('usuario5') // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
        ));
    }
}
