<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesRedessocialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_redessociales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo',13);
            $table->text('valor');
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')
                  ->references('id')->on('clientes')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
