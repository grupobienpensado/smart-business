<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto',100)->nullable();
            $table->string('nombres',30);
            $table->string('apellidos',30);
            $table->string('tratamiento',10);           
            $table->string('estado_civil',10);
            $table->string('profesion',50);
            $table->string('cargo',50);
            $table->string('jefe_inmediato',50);
            $table->string('n_a_pesos',50);
            $table->string('pais',50);
            $table->string('departamento',50);
            $table->string('ciudad',50);
            $table->date('fecha_nacimiento');
            $table->text('perfil');
            $table->text('observaciones')->nullable();
            $table->integer('empresa_sede_id')->unsigned();
            $table->foreign('empresa_sede_id')
                  ->references('id')->on('empresa_sedes')
                  ->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
