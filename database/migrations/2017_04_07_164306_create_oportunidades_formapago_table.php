<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadesFormapagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oportunidades_formapagos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('key');
            $table->text('value');
            $table->integer('oportunidad_id')->unsigned();            
            $table->integer('user_id')->unsigned();
            $table->foreign('oportunidad_id')
                  ->references('id')->on('oportunidades')
                  ->onDelete('cascade');
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
