@extends('template.app')
@section('title', 'Pendientes')
@section('content')
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="/css/pendientes/pendientes.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<style>
<?php if($data['permiso_exportar']=="No"){ ?>
    .dt-buttons{
        display: none;
    }
<?php } ?>
</style>
<!-- MAIN CONTENT WRAPPER -->
<div class="card" style="margin-bottom: 30px;">
    <div class="card-block">
        <div class="row">
            <div class="col-lg-3 puff-in-center">
                <div class="main-header">
                    <h2>Pendientes</h2>
                    <em>Listado de actividades pendientes</em>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="top-content">
                    <ul class="list-inline quick-access">
                        <li>
                            <a href="#" onclick="traerVencidas()">
                                <div class="quick-access-item slide-in-elliptic-left-fwd0 bg-color-blue">
                                    <i class="fa fa-exclamation-triangle"></i>
                                    <h5>Vencidas</h5><em id="totalVencidas">4 Actividades</em>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="traerVigentes()">
                                <div class="quick-access-item slide-in-elliptic-left-fwd">
                                    <i class="fa fa-th-list"></i>
                                    <h5>Vigentes</h5><em id="totalVigentes">4 Actividades</em>
                                </div>
                            </a>
                        </li>
                        @if(isset(Auth::user()->id) && (Auth::user()->id === 1 || Auth::user()->id === 54 || Auth::user()->id === 53 || Auth::user()->id === 82))
                        <li>
                            <a href="#" onclick="traerPorAprobar()">
                                <div class="quick-access-item slide-in-elliptic-left-fwd2">
                                    <i class="fa fa-check-square-o"></i>
                                    <h5>Por aprobar</h5><em id="totalPorAprobar">5 Actividades</em>
                                </div>
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="#" onclick="traerFinalizadas()">
                                <div class="quick-access-item slide-in-elliptic-left-fwd2">
                                    <i class="fa fa-check"></i>
                                    <h5>Finalizados</h5><em id="totalFinalizadas">4 Actividades</em>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="traerCanceladas()">
                                <div class="quick-access-item slide-in-elliptic-left-fwd3">
                                    <i class="fa fa-ban"></i>
                                    <h5>Cancelados</h5><em id="totalCanceladas">5 Actividades</em>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" <?php if($data['permiso_crear']=="Si"){ ?> onclick="crearPendiente()" <?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para agregar una actividad pendiente')"<?php } ?>>
                                <div class="quick-access-item slide-in-elliptic-left-fwd3">
                                    <i class="fa fa-plus"></i>
                                    <h5>Crear</h5><em id="totalCanceladas">Nuevo pendiente</em>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- main -->
        <div class="cssload-thecube">
            <div class="cssload-cube cssload-c1"></div>
            <div class="cssload-cube cssload-c2"></div>
            <div class="cssload-cube cssload-c4"></div>
            <div class="cssload-cube cssload-c3"></div>
        </div>
        <div class="content">
            <div class="main-content">

                <!-- LISTADO DE VIGENTES -->
                <div class="widget widget-table card-vigentes">
                    <div class="widget-header">
                        <h3><i class="fa fa-table"></i> Listado de actividades vigentes</h3>
                    </div>
                    <div class="widget-content">
                        <table class="table table-sorting table-hover table-bordered datatable table-striped datatable-vigentes">
                            <thead>
                                <tr>
                                    <th>Itemp</th>
                                    <th>Tipo</th>
                                    <th>Subtipo</th>
                                    <th>Ejecutar</th>
                                    <th>Gestion</th>
                                    <th>Responsable</th>
                                    <th>Detalle</th>
                                    <th>Creada</th>
                                    <th>Actualizada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" class="odd">
                                    <td class="sorting_1"><span class="textoajustado">1</span></td>
                                    <td><span class="textoajustado">Trabajos generales ESSI</span></td>
                                    <td><span class="textoajustado">Tramites de legalizacion y solicitud de viaticos</span></td>
                                    <td><span class="textoajustado unalinea">08 dic. 2017</span></td>
                                    <td><span class="textoajustado">7 veces aplazada </span></td>
                                    <td><span class="textoajustado">Silvia Diaz</span></td>
                                    <td><span class="textoajustado">Hacer cuentas con Jenny sobre legalizaciones de viaticos que no se le han pasado</span></td>
                                    <td><span class="textoajustado unalinea">03 dic. 2017</span></td>
                                    <td><span class="textoajustado unalinea">03 dic. 2017</span></td>
                                    <td>
                                        <div class="div-btn ver" name="415" data-toggle="tooltip" title="" data-original-title="Ver actividad"><i class="fa fa-eye"></i></div>

                                        <div class="div-btn terminar" name="415" data-toggle="tooltip" title="" data-original-title="Confirmar realizado"><i class="fa fa-check-square-o"></i></div>

                                        <div class="div-btn aplazar" name="415" data-toggle="tooltip" title="" data-original-title="Aplazar"><i class="fa fa-share"></i></div>

                                        <div class="div-btn cancelar" name="415" data-toggle="tooltip" title="" data-original-title="Cancelar"><i class="fa fa-times"></i></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END LISTADO DE VIGENTES -->
                <!-- LISTADO DE VENCIDAS -->
                <div class="widget widget-table card-vencidas">
                    <div class="widget-header">
                        <h3><i class="fa fa-table"></i> Listado de actividades vencidas</h3>
                    </div>
                    <div class="widget-content">
                        <table class="table table-sorting table-hover table-bordered table-striped datatable-vencidas">
                            <thead>
                                <tr>
                                    <th>Itemp</th>
                                    <th>Tipo</th>
                                    <th>Subtipo</th>
                                    <th>Ejecutar</th>
                                    <th>Gestion</th>
                                    <th>Responsable</th>
                                    <th>Detalle</th>
                                    <th>Creada</th>
                                    <th>Actualizada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" class="odd">
                                    <td class="sorting_1"><span class="textoajustado">1</span></td>
                                    <td><span class="textoajustado">Trabajos generales ESSI</span></td>
                                    <td><span class="textoajustado">Tramites de legalizacion y solicitud de viaticos</span></td>
                                    <td><span class="textoajustado unalinea">08 dic. 2017</span></td>
                                    <td><span class="textoajustado">7 veces aplazada </span></td>
                                    <td><span class="textoajustado">Silvia Diaz</span></td>
                                    <td><span class="textoajustado">Hacer cuentas con Jenny sobre legalizaciones de viaticos que no se le han pasado</span></td>
                                    <td><span class="textoajustado unalinea">03 dic. 2017</span></td>
                                    <td><span class="textoajustado unalinea">03 dic. 2017</span></td>
                                    <td>
                                        <div class="div-btn ver" name="415" data-toggle="tooltip" title="" data-original-title="Ver actividad"><i class="fa fa-eye"></i></div>

                                        <div class="div-btn terminar" name="415" data-toggle="tooltip" title="" data-original-title="Confirmar realizado"><i class="fa fa-check-square-o"></i></div>

                                        <div class="div-btn aplazar" name="415" data-toggle="tooltip" title="" data-original-title="Aplazar"><i class="fa fa-share"></i></div>

                                        <div class="div-btn cancelar" name="415" data-toggle="tooltip" title="" data-original-title="Cancelar"><i class="fa fa-times"></i></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END LISTADO DE VENCIDAS -->
                <!-- LISTADO DE CANCELADAS -->
                <div class="widget widget-table card-canceladas">
                    <div class="widget-header">
                        <h3><i class="fa fa-table"></i> Listado de actividades canceladas</h3>
                    </div>
                    <div class="widget-content">
                        <table class="table table-sorting table-hover table-bordered table-striped datatable-canceladas">
                            <thead>
                                <tr>
                                    <th>Itemp</th>
                                    <th>Tipo</th>
                                    <th>Subtipo</th>
                                    <th>Ejecutar</th>
                                    <th>Gestion</th>
                                    <th>Responsable</th>
                                    <th>Detalle</th>
                                    <th>C. Gestion</th>
                                    <th>Creada</th>
                                    <th>Actualizada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" class="odd">
                                    <td class="sorting_1">1</td>
                                    <td><span class="textoajustado">PRODUCTOS LACTEOS PARAISO, Ecuador</span></td>
                                    <td><span class="textoajustado">Gestion de aprobacion de ofertas</span></td>
                                    <td><span class="textoajustado unalinea">25 ago. 2017</span></td>
                                    <td><span class="textoajustado">7 veces aplazada </span></td>
                                    <td><span class="textoajustado">Oscar Sandoval</span></td>
                                    <td><span class="textoajustado">Visitar nuevamente a este cliente en su planta para definir la venta de estos equipos</span></td>
                                    <td><span class="textoajustado">Visitar nuevamente a este cliente en su planta para definir la venta de estos equipos</span></td>
                                    <td><span class="textoajustado unalinea">18 ago. 2017</span></td>
                                    <td><span class="textoajustado unalinea">23 ago. 2017</span></td>
                                    <td>
                                        <div class="div-btn ver" name="80" data-toggle="tooltip" title="" data-original-title="Ver actividad"><i class="fa fa-eye"></i></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END LISTADO DE CANCELADAS -->
                <!-- LISTADO DE PORAPROBAR -->
                <div class="widget widget-table card-poraprobar">
                    <div class="widget-header">
                        <h3><i class="fa fa-table"></i> Listado de actividades por aprobar</h3>
                    </div>
                    <div class="widget-content">
                        <table class="table table-sorting table-hover table-bordered table-striped datatable-poraprobar">
                            <thead>
                                <tr>
                                    <th>Itemp</th>
                                    <th>Tipo</th>
                                    <th>Subtipo</th>
                                    <th>Ejecutar</th>
                                    <th>Gestion</th>
                                    <th>Responsable</th>
                                    <th>Detalle</th>
                                    <th>C. Gestion</th>
                                    <th>Creada</th>
                                    <th>Actualizada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" class="odd">
                                    <td class="sorting_1">1</td>
                                    <td><span class="textoajustado">PRODUCTOS LACTEOS PARAISO, Ecuador</span></td>
                                    <td><span class="textoajustado">Gestion de aprobacion de ofertas</span></td>
                                    <td><span class="textoajustado unalinea">25 ago. 2017</span></td>
                                    <td><span class="textoajustado">7 veces aplazada </span></td>
                                    <td><span class="textoajustado">Oscar Sandoval</span></td>
                                    <td><span class="textoajustado">Visitar nuevamente a este cliente en su planta para definir la venta de estos equipos</span></td>
                                    <td><span class="textoajustado">Visitar nuevamente a este cliente en su planta para definir la venta de estos equipos</span></td>
                                    <td><span class="textoajustado unalinea">18 ago. 2017</span></td>
                                    <td><span class="textoajustado unalinea">23 ago. 2017</span></td>
                                    <td>
                                        <div class="div-btn ver" name="80" data-toggle="tooltip" title="" data-original-title="Ver actividad"><i class="fa fa-eye"></i></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END LISTADO DE PORAPROBAR -->
                <!-- LISTADO DE FINALIZADAS -->
                <div class="widget widget-table card-finalizadas">
                    <div class="widget-header">
                        <h3><i class="fa fa-table"></i> Listado de actividades finalizadas</h3>
                    </div>
                    <div class="widget-content">
                        <table class="table table-sorting table-hover table-bordered table-striped datatable-finalizadas">
                            <thead>
                                <tr>
                                    <th>Itemp</th>
                                    <th>Tipo</th>
                                    <th>Subtipo</th>
                                    <th>Ejecutar</th>
                                    <th>Gestion</th>
                                    <th>Responsable</th>
                                    <th>Detalle</th>
                                    <th>C. Gestion</th>
                                    <th>Creada</th>
                                    <th>Actualizada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" class="odd">
                                    <td class="sorting_1">1</td>
                                    <td><span class="textoajustado">PRODUCTOS LACTEOS PARAISO, Ecuador</span></td>
                                    <td><span class="textoajustado">Gestion de aprobacion de ofertas</span></td>
                                    <td><span class="textoajustado unalinea">25 ago. 2017</span></td>
                                    <td><span class="textoajustado">7 veces aplazada </span></td>
                                    <td><span class="textoajustado">Oscar Sandoval</span></td>
                                    <td><span class="textoajustado">Visitar nuevamente a este cliente en su planta para definir la venta de estos equipos</span></td>
                                    <td><span class="textoajustado">Visitar nuevamente a este cliente en su planta para definir la venta de estos equipos</span></td>
                                    <td><span class="textoajustado unalinea">18 ago. 2017</span></td>
                                    <td><span class="textoajustado unalinea">23 ago. 2017</span></td>
                                    <td>
                                        <div class="div-btn ver" name="80" data-toggle="tooltip" title="" data-original-title="Ver actividad"><i class="fa fa-eye"></i></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END LISTADO DE FINALIZASAS -->
            </div>
        </div>
        <!-- /main -->
    </div>
</div>
<!-- END CONTENT WRAPPER -->
@endsection @section('scripts')
<script src="/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="/components/file/js/plugins/purify.min.js"></script>

<script src="/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="/js/html5imageupload.js?v1.4.3"></script>
<script src="/js/jquery.blueimp-gallery.min.js"></script>

<script src='{{ url("/") }}/components/newfullcalendar/lib/moment.min.js'></script>
<script src="/js/plugins/moment-with-locales.js"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="assets/js/plugins/datatable/exts/dataTables.colVis.bootstrap.js"></script>
<script src="assets/js/plugins/datatable/exts/dataTables.tableTools.min.js"></script>
<script src="assets/js/plugins/datatable/exts/dataTables.colReorder.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>

<script src="/js/plugins/material.min.js"></script>
<script src="/js/plugins/material2.min.js"></script>
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript" src="/js/parsley.min.js"></script>
<script src="assets/js/king-table.js"></script>
<script>
    $(function() {
        $("body").on('change', '.cambiarTexto', function() {
            var idTexto = $(this).attr('title');
            var id = $(this).attr('id');
            var val = this.value;
            if ($('#browsers').find('option').filter(function() {
                    if (parseInt(this.value) === parseInt(val)) {
                        console.log(this.label, 'esto paso por este lado');
                        $("#" + idTexto).val(this.label).removeClass('ocultar');
                        $("#" + id).addClass('ocultar');
                        return 1;
                    }
                }).length) {}
        });

        $("body").on('click', '.cambiarValor', function() {
            var id = $(this).attr('title');
            var idTexto = $(this).attr('id');
            $("#" + id).removeClass('ocultar');
            $("#" + idTexto).addClass('ocultar');
        });

        $('.cssload-thecube').show();
        $('.main-content').hide();
        moment.locale('es');
        $(".quick-access-item").on("click", function() {
            $(".quick-access-item").removeClass("bg-color-blue");
            $(this).addClass("bg-color-blue");
        });

        $("body").on("click", ".aplazar", function() {
            var id = $(this).attr("name");
            swal({
                title: 'Aplazar pendiente',
                animation: "slide-from-top",
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cerrar',
                showLoaderOnConfirm: true,
                html: `
                        <form id="formGuardar" class="row" novalidate="novalidate" enctype="multipart/form-data">
                          <div class="form-group col-md-12">
                            <p>Fecha a  ejecutar</p>
                            <input type="text" class="swal2-input date" name="fecha" placeholder="Fecha a ejecutar" required>
                          </div>
                          <div class="form-group col-md-12">
                            <p>¿Porque deseas aplazar este pendiente?</p>
                            <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Ingrese un comentario" style="margin-top: 20px; margin-bottom: 20px; height: 185px;" required></textarea>
                          </div>
                          <div class="form-group col-md-12">
                            <p>Adjuntar un archivo</p>
                            <input id="input-1" type="file" class="file" name="file">
                          </div>
                        </form>
                    `,
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        if ($("form").parsley().validate()) {

                            var fd = new FormData(document.getElementById("formGuardar"));
                            fd.append("id", id);
                            var request = $.ajax({
                                url: "/aplazarpendiente",
                                type: "POST",
                                data: fd,
                                processData: false,
                                contentType: false
                            });
                            request.done(function(msg) {
                                resolve(msg)
                            });
                            request.fail(function(jqXHR, textStatus) {
                                resolve(false)
                            });
                        } else {
                            reject('Perdón!, Es necesario que complete los campos requeridos.');
                        }
                    });
                },
                onOpen: function() {
                    $('.date').bootstrapMaterialDatePicker({
                        time: false,
                        clearButton: true,
                        nowButton: true,
                        setDate: moment(),
                        lang: 'es',
                        minDate: moment(),
                        cancelText: 'Cancelar',
                        okText: 'Aceptar',
                        nowText: 'Nuevo',
                        clearText: 'Limpiar'
                    });

                    $("#input-1").fileinput({
                        language: "es",
                        maxFileCount: 10,
                        showUpload: false,
                        allowedFileExtensions: ["jpg", "gif", "png", "pdf"]
                    });
                }
            }).then(function(result) {
                if (result.success) {
                    swal("Exito!", "Pandiente aplazado con exito.", "success");
                    traerVigentes();
                } else {
                    swal("Error!", "Algo ha salido mal.", "warning");
                }
            }).catch(swal.noop)
        });

        $("body").on("click", ".cancelar", function() {
            var id = $(this).attr("name");
            swal({
                title: 'Cancelar pendiente',
                animation: "slide-from-top",
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cerrar',
                showLoaderOnConfirm: true,
                html: `
                        <form id="formGuardar" class="row" novalidate="novalidate" enctype="multipart/form-data">
                          <div class="form-group col-md-12">
                            <p>¿Porque deseas cancelar este pendiente?</p>
                            <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Ingrese un comentario" style="margin-top: 20px; margin-bottom: 20px; height: 185px;" required></textarea>
                          </div>
                          <div class="form-group col-md-12">
                            <p>Adjuntar un archivo</p>
                            <input id="input-1" type="file" class="file" name="file">
                          </div>
                        </form>
                    `,
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        if ($("form").parsley().validate()) {

                            var fd = new FormData(document.getElementById("formGuardar"));
                            fd.append("id", id);
                            var request = $.ajax({
                                url: "/cancelarpendiente",
                                type: "POST",
                                data: fd,
                                processData: false,
                                contentType: false
                            });
                            request.done(function(msg) {
                                resolve(msg)
                            });
                            request.fail(function(jqXHR, textStatus) {
                                resolve(false)
                            });
                        } else {
                            reject('Perdón!, Es necesario que complete los campos requeridos.');
                        }
                    });
                },
                onOpen: function() {
                    $('.date').bootstrapMaterialDatePicker({
                        time: false,
                        clearButton: true,
                        nowButton: true,
                        setDate: moment(),
                        lang: 'es',
                        minDate: moment(),
                        cancelText: 'Cancelar',
                        okText: 'Aceptar',
                        nowText: 'Nuevo',
                        clearText: 'Limpiar'
                    });
                    $("#input-1").fileinput({
                        language: "es",
                        maxFileCount: 10,
                        showUpload: false,
                        allowedFileExtensions: ["jpg", "gif", "png", "pdf"]
                    });
                }
            }).then(function(result) {
                if (result.success) {
                    swal("Exito!", "Pendiente cancelado con exito.", "success");
                    traerCanceladas();
                } else {
                    swal("Error!", "Algo ha salido mal.", "warning");
                }
            }).catch(swal.noop)
        });

        $("body").on("click", ".terminar", function() {
            var id = $(this).attr("name");
            swal({
                title: 'Confirmar realizado',
                animation: "slide-from-top",
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cerrar',
                showLoaderOnConfirm: true,
                html: `<form id="formGuardar" class="row" novalidate="novalidate" enctype="multipart/form-data">
                          <div class="form-group col-md-12">
                            <p>Por favor ingresa un comentario</p>
                            <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Ingrese un comentario" style="margin-top: 20px; margin-bottom: 20px; height: 185px;" required></textarea>
                          </div>
                          <div class="form-group col-md-12">
                            <p>Adjuntar un archivo</p>
                            <input id="input-1" type="file" class="file" name="file">
                          </div>
                        </form>`,
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        if ($("form").parsley().validate()) {

                            var fd = new FormData(document.getElementById("formGuardar"));
                            fd.append("id", id);
                            var request = $.ajax({
                                url: "/confirmarpendiente",
                                type: "POST",
                                data: fd,
                                processData: false,
                                contentType: false
                            });
                            request.done(function(msg) {
                                resolve(msg)
                            });
                            request.fail(function(jqXHR, textStatus) {
                                resolve(false)
                            });
                        } else {
                            reject('Perdón!, Es necesario que complete los campos requeridos.');
                        }
                    });
                },
                onOpen: function() {
                    $('.date').bootstrapMaterialDatePicker({
                        time: false,
                        clearButton: true,
                        nowButton: true,
                        setDate: moment(),
                        lang: 'es',
                        minDate: moment(),
                        cancelText: 'Cancelar',
                        okText: 'Aceptar',
                        nowText: 'Nuevo',
                        clearText: 'Limpiar'
                    });
                    $("#input-1").fileinput({
                        language: "es",
                        maxFileCount: 10,
                        showUpload: false,
                        allowedFileExtensions: ["jpg", "gif", "png", "pdf"]
                    });
                }
            }).then(function(result) {
                if (result.success) {
                    swal("Exito!", "Pendiente gestionado con exito.", "success");
                    traerVigentes();
                } else {
                    swal("Error!", "Algo ha salido mal.", "warning");
                }
            }).catch(swal.noop)
        });

        $("body").on("click", ".aprobar", function() {
            var id = $(this).attr("name");
            swal({
                title: 'Aprobar realizado',
                animation: "slide-from-top",
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cerrar',
                showLoaderOnConfirm: true,
                html: `
                        <form id="formGuardar" class="row" novalidate="novalidate" enctype="multipart/form-data">
                          <div class="form-group col-md-12">
                            <p>Por favor ingresa un comentario</p>
                            <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Ingrese un comentario" style="margin-top: 20px; margin-bottom: 20px; height: 185px;" required></textarea>
                          </div>
                          <div class="form-group col-md-12">
                            <p>Adjuntar un archivo</p>
                            <input id="input-1" type="file" class="file" name="file">
                          </div>
                        </form>
                    `,
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        if ($("form").parsley().validate()) {

                            var fd = new FormData(document.getElementById("formGuardar"));
                            fd.append("id", id);
                            var request = $.ajax({
                                url: "/aprobarpendiente",
                                type: "POST",
                                data: fd,
                                processData: false,
                                contentType: false
                            });
                            request.done(function(msg) {
                                resolve(msg)
                            });
                            request.fail(function(jqXHR, textStatus) {
                                resolve(false)
                            });
                        } else {
                            reject('Perdón!, Es necesario que complete los campos requeridos.');
                        }
                    });
                },
                onOpen: function() {
                    $('.date').bootstrapMaterialDatePicker({
                        time: false,
                        clearButton: true,
                        nowButton: true,
                        setDate: moment(),
                        lang: 'es',
                        minDate: moment(),
                        cancelText: 'Cancelar',
                        okText: 'Aceptar',
                        nowText: 'Nuevo',
                        clearText: 'Limpiar'
                    });
                    $("#input-1").fileinput({
                        language: "es",
                        maxFileCount: 10,
                        showUpload: false,
                        allowedFileExtensions: ["jpg", "gif", "png", "pdf"]
                    });
                }
            }).then(function(result) {
                if (result.success) {
                    swal("Exito!", "Pendiente aprobado con exito.", "success");
                    traerPorAprobar();
                } else {
                    swal("Error!", "Algo ha salido mal.", "warning");
                }
            }).catch(swal.noop)
        });

        $("body").on("click", ".ver", function() {
            var id = $(this).attr("name");
            var jqxhr = $.ajax("verpendiente/" + id)
                .always(function(data) {
                    if (data.success) {
                        var html = `
                        <div class="bloque-datos">
                            <img src="/images/file/clientes/` + data.modelo.user.foto + `" class="imag-avatar">
                            <div class="moment-arriba">
                                <span class="user-archivo">` + data.modelo.user.name + `</span>
                                <span class="fecha">` + moment(data.modelo.created_at).calendar() + `</span>
                            </div>
                            <div class="moment-medio">
                                <span class="estado">` + data.modelo.estado + `</span>
                                <div class="archivo-contenedor">
                                   <span>` + data.modelo.tipo + `</span>
                                </div>
                            </div>
                            <span class="comentario">` + data.modelo.detalle + `</span>
                        </div>
                        `;
                        $.each(data.historias, function(index, value) {
                            html += `
                                <div class="bloque-datos">
                                    <img src="/images/file/clientes/` + value.user.foto + `" class="imag-avatar">
                                    <div class="moment-arriba">
                                        <span class="user-archivo">` + value.user.name + `</span>
                                        <span class="fecha">` + moment(value.created_at).calendar() + `</span>
                                    </div>
                                    <div class="moment-medio">
                                        <span class="estado">` + value.estado + `</span>
                                        <div class="archivo-contenedor">
                                           <span><i class="fa fa-file"></i> ` + value.nombre_archivo + `</span>
                                        </div>
                                    </div>
                                    <span class="comentario">` + value.comentario + `</span>
                                </div>`;
                        });
                        swal({
                            title: data.modelo.oportunidad,
                            animation: "slide-from-top",
                            confirmButtonText: 'Aceptar',
                            html: html
                        });
                    }
                })
        });

        $("body").on("click", ".rechazar", function() {
            var id = $(this).attr("name");
            swal({
                title: 'Rechazar pendiente',
                animation: "slide-from-top",
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cerrar',
                showLoaderOnConfirm: true,
                html: `
                        <form id="formGuardar" class="row" novalidate="novalidate" enctype="multipart/form-data">
                          <div class="form-group col-md-12">
                            <p>Nueva fecha a  ejecutar</p>
                            <input type="text" class="swal2-input date" name="fecha" placeholder="Fecha a ejecutar" required>
                          </div>
                          <div class="form-group col-md-12">
                            <p>¿Porque deseas aplazar este pendiente?</p>
                            <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Ingrese un comentario" style="margin-top: 20px; margin-bottom: 20px; height: 185px;" required></textarea>
                          </div>
                          <div class="form-group col-md-12">
                            <p>Adjuntar un archivo</p>
                            <input id="input-1" type="file" class="file" name="file">
                          </div>
                        </form>`,
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        if ($("form").parsley().validate()) {

                            var fd = new FormData(document.getElementById("formGuardar"));
                            fd.append("id", id);
                            var request = $.ajax({
                                url: "/aplazarpendiente",
                                type: "POST",
                                data: fd,
                                processData: false,
                                contentType: false
                            });
                            request.done(function(msg) {
                                resolve(msg)
                            });
                            request.fail(function(jqXHR, textStatus) {
                                resolve(false)
                            });
                        } else {
                            reject('Perdón!, Es necesario que complete los campos requeridos.');
                        }
                    });
                },
                onOpen: function() {
                    $('.date').bootstrapMaterialDatePicker({
                        time: false,
                        clearButton: true,
                        nowButton: true,
                        setDate: moment(),
                        lang: 'es',
                        minDate: moment(),
                        cancelText: 'Cancelar',
                        okText: 'Aceptar',
                        nowText: 'Nuevo',
                        clearText: 'Limpiar'
                    });

                    $("#input-1").fileinput({
                        language: "es",
                        maxFileCount: 10,
                        showUpload: false,
                        allowedFileExtensions: ["jpg", "gif", "png", "pdf"]
                    });
                }
            }).then(function(result) {
                if (result.success) {
                    swal({
                        title: "Exito!",
                        text: "Pandiente aplazado con exito.",
                        type: "success",
                        showConfirmButton: false,
                        timer: 1000
                    });
                    traerPorAprobar();
                } else {
                    swal("Error!", "Algo ha salido mal.", "warning");
                }
            }).catch(swal.noop)
        });
        traerVencidas();
    });

    traerVigentes = function() {
        $('.cssload-thecube').show();
        $('.main-content').hide();
        var jqxhr = $.ajax("jsonvigentes")
            .always(function(data) {
                if (data.success) {
                    $("#totalCanceladas").html(data.cancelado + ' Actividades');
                    $("#totalFinalizadas").html(data.aprobado + ' Actividades');
                    $("#totalPorAprobar").html(data.terminado + ' Actividades');
                    $("#totalVigentes").html(data.ejecutando + ' Actividades');
                    $("#totalVencidas").html(data.vencidos + ' Actividades');
                    if ($('.datatable-vigentes').length > 0) {
                        dtTableVigentes.clear().draw();
                    }
                    $.each(data.modelos, function(index, value) {
                        dtTableVigentes.row.add([
                            `<span class="textoajustado">` + (index + 1) + `</span>`,
                            `<span class="textoajustado">` + value.tipo + `<br>` + value.oportunidad + `</span>`,
                            `<span class="textoajustado">` + value.subtipo + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.fecha_ejecucion).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado">` + value.aplazadas + ` veces aplazada </span>`,
                            `<span class="textoajustado">` + value.nombreuser + `</span>`,
                            `<span class="textoajustado">` + value.detalle + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.created_at).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.updated_at).format('YYYY-MM-DD') + `</span>`,
                            `<div class="unalinea"><div class="div-btn ver" name="` + value.id + `" data-toggle="tooltip" title="Ver actividad"><i class="fa fa-eye"></i></div>
                            <div class="div-btn terminar" name="` + value.id + `" data-toggle="tooltip" title="Confirmar realizado"><i class="fa fa-check-square-o"></i></div>
                            <div class="div-btn aplazar" name="` + value.id + `" data-toggle="tooltip" title="Aplazar"><i class="fa fa-share"></i></div>
                            <div class="div-btn cancelar" name="` + value.id + `" data-toggle="tooltip" title="Cancelar"><i class="fa fa-times"></i></div></div>`
                        ]).draw(false);
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                    $(".card-vigentes, .card-vencidas, .card-canceladas, .card-poraprobar, .card-finalizadas")
                        .hide().removeClass("slide-in-bottom");
                    $('.cssload-thecube').hide();
                    $('.main-content ').show();
                    $(".card-vigentes").show().addClass("slide-in-bottom");
                }
            });
    }

    traerVencidas = function() {
        $('.cssload-thecube').show();
        $('.main-content').hide();
        var jqxhr = $.ajax("jsonvencidas")
            .always(function(data) {
                if (data.success) {
                    if ($('.datatable-vencidas').length > 0) {
                        dtTableVencidas.clear().draw();
                    }
                    $("#totalCanceladas").html(data.cancelado + ' Actividades');
                    $("#totalFinalizadas").html(data.aprobado + ' Actividades');
                    $("#totalPorAprobar").html(data.terminado + ' Actividades');
                    $("#totalVigentes").html(data.ejecutando + ' Actividades');
                    $("#totalVencidas").html(data.vencidos + ' Actividades');
                    $.each(data.modelos, function(index, value) {
                        dtTableVencidas.row.add([
                            `<span class="textoajustado">` + (index + 1) + `</span>`,
                            `<span class="textoajustado">` + value.tipo + `<br>` + value.oportunidad + `</span>`,
                            `<span class="textoajustado">` + value.subtipo + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.fecha_ejecucion).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado">` + value.aplazadas + ` veces aplazada </span>`,
                            `<span class="textoajustado">` + value.nombreuser + `</span>`,
                            `<span class="textoajustado">` + value.detalle + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.created_at).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.updated_at).format('YYYY-MM-DD') + `</span>`,
                            `<div class="div-btn ver" name="` + value.id + `" data-toggle="tooltip" title="Ver actividad"><i class="fa fa-eye"></i></div>
                            <div class="div-btn <?php if($data['permiso_pendientes']=='Si'){ ?>terminar"<?php }else{ ?>" onclick="no_permiso('No tiene permisos para confirmar, aplazar o cancelar pendientes')"<?php } ?> name="` + value.id + `" data-toggle="tooltip" title="Confirmar realizado"><i class="fa fa-check-square-o"></i></div>
                            <div class="div-btn <?php if($data['permiso_pendientes']=='Si'){ ?>aplazar"<?php }else{ ?>" onclick="no_permiso('No tiene permisos para confirmar, aplazar o cancelar pendientes')"<?php } ?> name="` + value.id + `" data-toggle="tooltip" title="Aplazar"><i class="fa fa-share"></i></div>
                            <div class="div-btn <?php if($data['permiso_pendientes']=='Si'){ ?>cancelar"<?php }else{ ?>" onclick="no_permiso('No tiene permisos para confirmar, aplazar o cancelar pendientes')"<?php } ?> name="` + value.id + `" data-toggle="tooltip" title="Cancelar"><i class="fa fa-times"></i></div>`
                        ]).draw(false);
                    });
                    $('[data-toggle="tooltip"]').tooltip();

                    $(".card-vigentes, .card-vencidas, .card-canceladas, .card-poraprobar, .card-finalizadas")
                        .hide().removeClass("slide-in-bottom");

                    $('.cssload-thecube').hide();
                    $('.main-content ').show();
                    $(".card-vencidas").show().addClass("slide-in-bottom");
                }
            });
    }

    traerPorAprobar = function() {
        $('.cssload-thecube').show();
        $('.main-content').hide();
        var jqxhr = $.ajax("jsonporaprobar")
            .always(function(data) {
                if (data.success) {
                    $("#totalCanceladas").html(data.cancelado + ' Actividades');
                    $("#totalFinalizadas").html(data.aprobado + ' Actividades');
                    $("#totalPorAprobar").html(data.terminado + ' Actividades');
                    $("#totalVigentes").html(data.ejecutando + ' Actividades');
                    $("#totalVencidas").html(data.vencidos + ' Actividades');
                    if ($('.datatable-poraprobar').length > 0) {
                        dtTablePoaprobar.clear().draw();
                    }
                    $.each(data.modelos, function(index, value) {
                        dtTablePoaprobar.row.add([
                            `<span class="textoajustado">` + (index + 1) + `</span>`,
                            `<span class="textoajustado">` + value.tipo + `<br>` + value.oportunidad + `</span>`,
                            `<span class="textoajustado">` + value.subtipo + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.fecha_ejecucion).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado">` + value.aplazadas + ` veces aplazada </span>`,
                            `<span class="textoajustado">` + value.nombreuser + `</span>`,
                            `<span class="textoajustado">` + value.detalle + `</span>`,
                            `<span class="textoajustado">` + value.comentario + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.created_at).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.updated_at).format('YYYY-MM-DD') + `</span>`,
                            `<div class="unalinea"><div class="div-btn ver" name="` + value.id + `" data-toggle="tooltip" title="Ver actividad"><i class="fa fa-eye"></i></div>
                            <div class="div-btn <?php if($data['permiso_aprobar_rechazar']=='Si'){ ?>aprobar"<?php }else{ ?>" onclick="no_permiso('No tiene permisos para aprobar o rechazar pendientes')"<?php } ?> name="` + value.id + `" data-toggle="tooltip" title="Aprobar actividad"><i class="fa fa-check-square-o"></i></div>
                            <div class="div-btn <?php if($data['permiso_aprobar_rechazar']=='Si'){ ?>rechazar"<?php }else{ ?>" onclick="no_permiso('No tiene permisos para aprobar o rechazar pendientes')"<?php } ?> name="` + value.id + `" data-toggle="tooltip" title="Rechazar actividad"><i class="fa fa-times"></i></div></div>`

                        ]).draw(false);
                    });
                    $('[data-toggle="tooltip"]').tooltip();

                    $(".card-vigentes, .card-vencidas, .card-canceladas, .card-poraprobar, .card-finalizadas")
                        .hide().removeClass("slide-in-bottom");
                    $('.cssload-thecube').hide();
                    $('.main-content ').show();
                    $(".card-poraprobar").show().addClass("slide-in-bottom");

                }
            });
    }

    traerFinalizadas = function() {
        $('.cssload-thecube').show();
        $('.main-content').hide();
        var jqxhr = $.ajax("jsonfinalizadas")
            .always(function(data) {
                if (data.success) {
                    $("#totalCanceladas").html(data.cancelado + ' Actividades');
                    $("#totalFinalizadas").html(data.aprobado + ' Actividades');
                    $("#totalPorAprobar").html(data.terminado + ' Actividades');
                    $("#totalVigentes").html(data.ejecutando + ' Actividades');
                    $("#totalVencidas").html(data.vencidos + ' Actividades');
                    if ($('.datatable-finalizadas').length > 0) {
                        dtTableFinalizadas.clear().draw();
                    }
                    $.each(data.modelos, function(index, value) {
                        dtTableFinalizadas.row.add([
                            `<span class="textoajustado">` + (index + 1) + `</span>`,
                            `<span class="textoajustado">` + value.tipo + `<br>` + value.oportunidad + `</span>`,
                            `<span class="textoajustado">` + value.subtipo + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.fecha_ejecucion).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado">` + value.aplazadas + ` veces aplazada </span>`,
                            `<span class="textoajustado">` + value.nombreuser + `</span>`,
                            `<span class="textoajustado">` + value.detalle + `</span>`,
                            `<span class="textoajustado">` + value.comentario + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.created_at).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.updated_at).format('YYYY-MM-DD') + `</span>`,
                            `<div class="div-btn ver" name="` + value.id + `" data-toggle="tooltip" title="Ver actividad"><i class="fa fa-eye"></i></div>`
                        ]).draw(false);
                    });
                    $('[data-toggle="tooltip"]').tooltip();

                    $(".card-vigentes, .card-vencidas, .card-canceladas, .card-poraprobar, .card-finalizadas")
                        .hide().removeClass("slide-in-bottom");
                    $(".card-finalizadas").show().addClass("slide-in-bottom");
                    $('.cssload-thecube').hide();
                    $('.main-content ').show();

                }
            });
    }

    traerCanceladas = function() {
        $('.cssload-thecube').show();
        $('.main-content').hide();
        var jqxhr = $.ajax("jsoncanceladas")
            .always(function(data) {
                if (data.success) {
                    $("#totalCanceladas").html(data.cancelado + ' Actividades');
                    $("#totalFinalizadas").html(data.aprobado + ' Actividades');
                    $("#totalPorAprobar").html(data.terminado + ' Actividades');
                    $("#totalVigentes").html(data.ejecutando + ' Actividades');
                    $("#totalVencidas").html(data.vencidos + ' Actividades');
                    if ($('.datatable-canceladas').length > 0) {
                        dtTableCanceladas.clear().draw();
                    }
                    $.each(data.modelos, function(index, value) {
                        dtTableCanceladas.row.add([
                            `<span class="textoajustado">` + (index + 1) + `</span>`,
                            `<span class="textoajustado">` + value.tipo + `<br>` + value.oportunidad + `</span>`,
                            `<span class="textoajustado">` + value.subtipo + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.fecha_ejecucion).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado">` + value.aplazadas + ` veces aplazada </span>`,
                            `<span class="textoajustado">` + value.nombreuser + `</span>`,
                            `<span class="textoajustado">` + value.detalle + `</span>`,
                            `<span class="textoajustado">` + value.comentario + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.created_at).format('YYYY-MM-DD') + `</span>`,
                            `<span class="textoajustado unalinea">` + moment(value.updated_at).format('YYYY-MM-DD') + `</span>`,
                            `<div class="div-btn ver" name="` + value.id + `" data-toggle="tooltip" title="Ver actividad"><i class="fa fa-eye"></i></div>`
                        ]).draw(false);
                    });
                    $('[data-toggle="tooltip"]').tooltip();

                    $(".card-vigentes, .card-vencidas, .card-canceladas, .card-poraprobar, .card-finalizadas")
                        .hide().removeClass("slide-in-bottom");
                    $(".card-canceladas").show().addClass("slide-in-bottom");
                    $('.cssload-thecube').hide();
                    $('.main-content ').show();
                }
            });
    }

    crearPendiente = function() {
        var jqxhr = $.ajax("listactivpendientes")
            .always(function(data) {
                if (data.success) {
                    swal({
                        title: 'Crear pendiente',
                        animation: "slide-from-top",
                        showCancelButton: true,
                        confirmButtonText: 'Guardar',
                        cancelButtonText: 'Cerrar',
                        showLoaderOnConfirm: true,
                        html: `
                            <form id="formGuardar" class="row" novalidate="novalidate" enctype="multipart/form-data">
                              <div class="col-md-6">
                                <p>Fecha a  ejecutar</p>
                                <input type="text" class="swal2-input date" name="fecha" placeholder="Fecha a ejecutar" required>
                              </div>
                              <div class="col-md-6">
                                <p>Tipo de actividad</p>
                                <select name="tipo_actividad" class="swal2-input" id="tipo_actividad" required>
                                    <option value="">Seleccione un tipo de actividad</option>
                                    <option>Oportunidad</option>
                                    <option>Empresa</option>
                                    <option>Gestion comercial</option>
                                    <option>Gestion interna ESSI</option>
                                </select>
                              </div>
                              <div class="col-md-6 contSubTipo"></div>
                              <div class="col-md-6 addOtro"></div>
                              <div class="col-md-12">
                                <p>Por favor ingrese un comentario</p>
                                <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Ingrese un comentario" style="margin-top: 20px; margin-bottom: 20px; height: 185px;"></textarea>
                              </div>
                              <div class="col-md-12">
                                <p>Adjuntar un archivo</p>
                                <input id="input-1" type="file" class="file" name="file">
                              </div>
                            </form>
                        `,
                        preConfirm: function() {
                            return new Promise(function(resolve, reject) {
                                if ($("form").parsley().validate()) {
                                    var fd = new FormData(document.getElementById("formGuardar"));
                                    var request = $.ajax({
                                        url: "/crearpendiente",
                                        type: "POST",
                                        data: fd,
                                        processData: false,
                                        contentType: false
                                    });
                                    request.done(function(msg) {
                                        resolve(msg)
                                    });
                                    request.fail(function(jqXHR, textStatus) {
                                        resolve(false)
                                    });
                                } else {
                                    reject('Perdón!, Es necesario que complete los campos requeridos.');
                                }
                            });
                        },
                        onOpen: function() {
                            $('.date').bootstrapMaterialDatePicker({
                                time: false,
                                clearButton: true,
                                nowButton: true,
                                setDate: moment(),
                                lang: 'es',
                                minDate: moment(),
                                cancelText: 'Cancelar',
                                okText: 'Aceptar',
                                nowText: 'Nuevo',
                                clearText: 'Limpiar'
                            });

                            $("#input-1").fileinput({
                                language: "es",
                                maxFileCount: 10,
                                showUpload: false,
                                allowedFileExtensions: ["jpg", "gif", "png", "pdf"]
                            });

                            $(document).on('change', '#tipo_actividad', function() {
                                var valor = $(this).val();
                                console.log("cambio", valor, '===', "Empresa");
                                $.ajax("subactividad?term=" + valor)
                                    .always(function(data) {
                                        if (data) {
                                            var html = '<p>Subactividad</p><select name="subtipo_actividad" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                                            $.each(data, function(key, value) {
                                                html += '<option value="' + value.id + '">' + value.text + '</option>';
                                            });
                                            html += '</select>';
                                            $(".addOtro").html("");
                                            $(".contSubTipo").html(html);
                                            console.log(data);
                                        }
                                    });
                                if (valor === "Empresa") {
                                    $.ajax("empresa")
                                        .always(function(data) {
                                            if (data) {
                                                var html = '<p>Empresas</p><input id="empresa_idTexto" class="swal2-input cambiarValor ocultar" title="empresa_id"><input list="browsers" id="empresa_id" class="swal2-input cambiarTexto" name="empresa_id" placeholder="Ingrese una empresa" title="empresa_idTexto" required><datalist id="browsers">';
                                                $.each(data, function(key, value) {
                                                    html += '<option label="' + value.text + '" value="' + value.id + '"></option>';
                                                });
                                                html += '</datalist>';
                                                $(".addOtro").html(html);
                                                console.log(data);
                                            }
                                        });
                                } else if (valor === "Oportunidad") {
                                    $.ajax("ssoportunidad")
                                        .always(function(data) {
                                            if (data) {
                                                var html = '<p>Oportunidad</p><input id="oportunidad_idTexto" class="swal2-input cambiarValor ocultar" title="oportunidad_id"><input list="browsers"  id="oportunidad_id"  class="swal2-input cambiarTexto" name="oportunidad_id" placeholder="Ingrese una oportunidad" title="oportunidad_idTexto" required><datalist id="browsers">';
                                                $.each(data, function(key, value) {
                                                    html += '<option label="' + value.text + '" value="' + value.id + '"></option>';
                                                });
                                                html += '</datalist>';
                                                $(".addOtro").html(html);
                                                console.log(data);
                                            }
                                        });
                                }
                            });
                            // $(document).on('change', '#browsers', function() {
                            //   console.log($(this).attr('title'), 'salio esta joda');
                            // });
                        }
                    }).then(function(result) {
                        if (result.success) {
                            swal("Exito!", "Pandiente aplazado con exito.", "success");
                        } else {
                            swal("Error!", "Algo ha salido mal.", "warning");
                        }
                    }).catch(swal.noop)
                }
            });

    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
