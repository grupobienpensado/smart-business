<!DOCTYPE html>
<html lang="es">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="author" content="Edwin Ferreira Martinez">
        <meta name="description" content="control de siervos básico, agregar, editar, eliminar, listar y ver">
        <meta name="keywords" content="Control, Siervos, Iglesia, Registro, Asambleas de Dios">

        <title>Agenda Smart - Iniciar Sesión</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/login/animate.css">
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('/') }}/assets/ico/kingadmin-favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ url('/') }}/assets/ico/kingadmin-favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('/') }}/assets/ico/kingadmin-favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ url('/') }}/assets/ico/kingadmin-favicon57x57.png">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
        <link rel="shortcut icon" href="{{ url('/') }}/assets/ico/kingadmin-favicon144x144.png">
        <style type="text/css">
        body {
        margin: 0;
        background: #000;
        }
        video {
        position: fixed;
        top: 50%;
        left: 50%;
        min-width: 100%;
        min-height: 100%;
        width: auto;
        height: auto;
        z-index: -100;
        transform: translateX(-50%) translateY(-50%);
        /*background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;*/
        background-size: cover;
        transition: 1s opacity;
        }
        .stopfade {
        opacity: .5;
        }

        #polina {
        font-family: Agenda-Light, Agenda Light, Agenda, Arial Narrow, sans-serif;
        font-weight:100;
        background: rgba(0,0,0,0.3);
        color: white;
        padding: 2rem;
        width: 20%;
        margin:2rem;
        float: right;
        font-size: 1.2rem;
        position: absolute;
        right: 0;
        bottom: -10px;
        }
        h1 {
        font-size: 3rem;
        text-transform: uppercase;
        margin-top: 0;
        letter-spacing: .3rem;
        }
        #polina button {
        display: block;
        width: 80%;
        padding: .4rem;
        border: none;
        margin: 1rem auto;
        font-size: 1.3rem;
        background: rgba(255,255,255,0.23);
        color: #fff;
        border-radius: 3px;
        cursor: pointer;
        transition: .3s background;
        }
        #polina button:hover {
        background: rgba(0,0,0,0.5);
        }

        a {
        display: inline-block;
        color: #fff;
        text-decoration: none;
        background:rgba(0,0,0,0.5);
        padding: .5rem;
        transition: .6s background;
        }
        a:hover{
        background:rgba(0,0,0,0.9);
        }
        @media screen and (max-width: 500px) {
        div{width:70%;}
        }
        @media screen and (max-device-width: 800px) {
        html { background: url(/images/corporativo.jpg) #000 no-repeat center center fixed; }
        #bgvid { display: none; }
        }

        input[type=text], input[type=password] {
        font-family: FontAwesome;
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
        }

        input[type=submit] {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        }

        input[type=submit]:hover {
        background-color: #45a049;
        }

        </style>

        <link rel="icon" href="{{ url('/') }}/images/favicon.ico" sizes="32x32">
</head>
<body>
    <video id="bgvid" playsinline autoplay muted loop>
      <source src="{{ url('/') }}/storage/MEGAARCHIVO/Crm.mp4" type="video/mp4">
    </video>
    <div id="polina" class="animated bounceInUp">
    <div style="text-align: center;"><img src="{{ url('/') }}/images/imagenes/logo.png"></div>
    <center><h3 style="font-family: 'Raleway Regular';">Iniciar sesion</h3></center>

    <form action="{{ route('login') }}" method="POST">
     @if(Session::has('mensaje_error'))
        <div style="color: red">{{ Session::get('mensaje_error') }}</div>
    @endif
    {{ csrf_field() }}
    <input type="text" style="font-family: 'Raleway Regular';" class="form-control texto" id="username-email" name="username" placeholder='&#xf26e;' required="required">
    <input type="password" style="font-family: 'Raleway Regular';" class="form-control texto" id="password" name="password" placeholder="&#xf023;" required="required">
    <input type="submit" value="Entrar" style="background-color: #051d60">
    </form>


    </div>
</body>
</html>
