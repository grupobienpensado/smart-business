@extends('template.app3')

@section('title', 'Configuración General')

@section('content')

<link rel="stylesheet" type="text/css" href="/css/configuraciongeneral/style.css">
<link rel="stylesheet" href="/images/configuracion_crm/step/css/style.css">
<link href="/css/html5imageupload.css?v1.3" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<h1 class="titulo"><img src="/images/configuracion_crm/configuracion.gif">Configuración General del Sistema</h1>
	</div>	

	<div class="col-md-12" style="text-align: center;">
		<div class="step-nav">
			<div class="step first">
				<div class="radial-progress active" data-index="0" data-toggle="tooltip" data-original-title="Configurar Dashboard" title="Configurar Dashboard">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-dashboard fa-fw"></i></p>
			</div>	
			<!--<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="1" data-toggle="tooltip" data-original-title="Configurar Tipo de Actividad" title="Configurar Tipo de Actividad">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-building" aria-hidden="true"></i></p>
			</div>-->
			<!--<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="2" data-toggle="tooltip" data-original-title="Configurar Ciclos y Ventas" title="Configurar Ciclos y Ventas">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-exchange" aria-hidden="true"></i></p>
			</div>-->
			<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="1" data-toggle="tooltip" data-original-title="Configurar Horas Hombre" title="Configurar Horas Hombre">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-hourglass-start" aria-hidden="true"></i></p>
			</div>
			<!--<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="4" data-toggle="tooltip" data-original-title="Configurar Usuarios" title="Configurar Usuarios">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-user" aria-hidden="true"></i></p>
			</div>-->
			<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="2" data-toggle="tooltip" data-original-title="Configurar Paises" title="Configurar Paises">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-globe" aria-hidden="true"></i></p>
			</div>
            <div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" id="configurar_permisos" data-index="3" data-toggle="tooltip" data-original-title="Configurar Paises" title="Configurar Permisos">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></p>
			</div>
		</div>
		
		<div class="container-fluid animated flipInX confi" id="conf_1">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Dashboard</h2>
					<h4>Participación según probabilidad</h4>
					<form id="formulario_conf_dashboard" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }} 
						<div class="row">
							<div class="col-md-4" style="margin: 1%;">
								<img src="{{ url('/') }}/images/configuracion_crm/alta.png" style="border: 0 !important; margin-bottom: 1%;">
								<h3>Alta</h3>
								<div class="form-group">
									<label for="porcentaje1">Porcentaje</label>
									<input type="text" id="porcentaje1" name="porcentaje[alta]" class="form-control porcentaje formulariodashboard"  placeholder="&#xf040; &nbsp;Porcentaje" value="{{ $participacion->alta }} %">
								</div>
							</div>
							<div class="col-md-3" style="margin: 1%;">
								<img src="{{ url('/') }}/images/configuracion_crm/media.png" style="border: 0 !important; margin-bottom: 1%;">
								<h3>Media</h3>
								<div class="form-group">
									<label for="porcentaje2">Porcentaje</label>
									<input type="text" id="porcentaje2" name="porcentaje[media]" class="form-control porcentaje formulariodashboard" placeholder="&#xf040; &nbsp; Porcentaje" value="{{ $participacion->media }} %">
								</div>
								
							</div>
							<div class="col-md-4" style="margin: 1%;">
								<img src="{{ url('/') }}/images/configuracion_crm/baja.png" style="border: 0 !important; margin-bottom: 1%;">
								<h3>Baja</h3>
								<div class="form-group">
									<label for="porcentaje3">Porcentaje</label>
									<input type="text" id="porcentaje3" name="porcentaje[baja]" class="form-control porcentaje formulariodashboard" placeholder="&#xf040; &nbsp; Porcentaje" value="{{ $participacion->baja }} %">
								</div>
								
							</div>
						</div>
						<h4>Metas Oportunidades General</h4>
						<div class="col-md-4" style="margin: 1%; min-height: 338px;">
							<img src="{{ url('/') }}/images/configuracion_crm/corto_plazo.png" style="border: 0 !important; margin-bottom: 1%; width: 21.9%;">
							<h3>Corto Plazo</h3>
							<div class="dias_group">							
								<div class="form-group">
									<label for="cantidad1">Cantidad días</label>
									<input type="text" id="cantidad1" name="dias[corto_dias]" class="form-control dias formulariodashboard" placeholder="&#xf271; &nbsp;Dias" value="{{ number_format($metas->corto_dias) }}">
								</div>
								<div class="form-group">
									<label for="valor1">Valor</label>
									<input type="text" id="valor1" name="valor[corto_valor]" class="form-control valor formulariodashboard" placeholder="&#xf0d6; &nbsp;Valor" value="{{ number_format($metas->corto_valor) }}">
								</div>
							</div>
						</div>
						<div class="col-md-3" style="margin: 1%; min-height: 338px;">
							<img src="{{ url('/') }}/images/configuracion_crm/medio_plazo.png" style="border: 0 !important; margin-bottom: 1%; width: 30%;">
							<h3>Mediano Plazo</h3>
							<div class="dias_group">							
								<div class="form-group">
									<label for="cantidad2">Cantidad días</label>
									<input type="text" id="cantidad2" name="dias[medio_dias]" class="form-control dias formulariodashboard"  placeholder="&#xf271; &nbsp;Dias" value="{{ $metas->medio_dias }}">
								</div>
								<div class="form-group">
									<label for="valor2">Valor</label>
									<input type="text" id="valor2" name="valor[medio_valor]" class="form-control valor formulariodashboard" placeholder="&#xf0d6; &nbsp;Valor" value="{{ number_format($metas->medio_valor) }}">
								</div>
							</div>
						</div>
						<div class="col-md-4" style="margin: 1%;">
							<img src="{{ url('/') }}/images/configuracion_crm/largo_plazo.png" style="border: 0 !important; margin-bottom: 1%; width: 21.9%;">
							<h3>Largo Plazo</h3>							
							<div class="form-group">
								<label for="cantidad3">Cantidad días</label>
								<input type="text" id="cantidad3" name="dias[largo_dias]" class="form-control dias formulariodashboard" readonly placeholder="&#xf271; &nbsp;Dias" value="{{ $metas->largo_dias }}">
							</div>
							<div class="form-group">
								<label for="valor3">Valor</label>
								<input type="text" id="valor3" name="valor[largo_valor]" class="form-control valor formulariodashboard" placeholder="&#xf0d6; &nbsp;Valor" value="{{ number_format($metas->largo_valor) }}">
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>
		<!--<div class="container-fluid animated flipInX confi" id="conf_2" style="display: none">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Tipo de Actividad</h2>
					<img src="{{ url('/') }}/images/configuracion_crm/actividad.png" style="border: 0 !important;"><br>
					<button type="button" class="btn btn-success add-actividad" data-toggle="tooltip" data-original-title="Agregar Actividad"><i class="fa fa-plus" aria-hidden="true"></i></button>
					<input type="hidden" id="fila_act" value="{{ count($tipos_actividades) }}">
					<form id="conf_actividad" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }} 
						<?php $num=0;  ?>
						@foreach($tipos_actividades as $t_a)
						<div class="row act" id="actividad_insertada{{ $num }}">
							<input type="hidden" name="actividad[{{ $num }}][id]" value="{{ $t_a->id }}">
							<div class="col-md-2">
								<div class="form-group">
									<label for="orden{{ $num }}">Orden</label>
									<input type="text" style="text-align: center;" id="orden{{ $num }}" placeholder="&#xf03c; Numero" class="form-control solonumero numero valor_id_{{ $t_a->id }}" name="actividad[{{ $num }}][orden]" value="{{ $t_a->orden }}">
								</div>
							</div>
							<div class="col-md-9" style="margin-top: 2%;">
								<div class="panel-group" id="actividades" role="tablist" aria-multiselectable="true">
								  <div class="panel panel-default colapsable">
								    <div class="panel-heading colapsable2" role="tab" id="titulo{{ $num }}">
								      <h4 class="panel-title">
								        <a  data-toggle="collapse" id="titulo_nombre{{ $num }}" data-parent="#actividades" href="#collapse{{ $num }}" aria-expanded="true" aria-controls="collapse{{ $num }}">
								          @if(($t_a->concepto)!='') {{ $t_a->concepto }} @else Actividad @endif
								        </a>								        
								      </h4>
								    </div>
								    <div id="collapse{{ $num }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="titulo{{ $num }}">
								      <div class="panel-body">
								        <div class="row">									
											<div class="col-md-10">
												<div class="form-group">
													<label for="concepto{{ $num }}">Concepto</label>
													<input type="text" id="concepto{{ $num }}" placeholder="&#xf0f6; Concepto" class="form-control actividad titulo" name="actividad[{{ $num }}][concepto]" value="{{ $t_a->concepto }}">
												</div>
											</div>
											<div class="col-md-2">
												<?php $imagen_usotiempo=''; if($t_a->foto != ''){ $imagen_usotiempo=url('/').'/images/icons/'.$t_a->foto; } ?>
												<div class="dropzone imagen-usotiempo" style='background-image: url("{{$imagen_usotiempo}}")' data-width="32" data-height="32" data-ajax="false" data-originalsave="true">
						                        	<input type="file" name="actividad[{{ $num }}][imagen]" accept="image/gif, image/jpeg, image/png">
						                      </div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="descripcion{{ $num }}">Descripcion</label>
													<textarea class="form-control actividad" id="descripcion{{ $num }}" name="actividad[{{ $num }}][descripcion]" rows="8" placeholder="&#xf0f6; Descripcion">{{ $t_a->descripcion }}</textarea>
												</div>
											</div>	
										</div>	
								      </div>
								    </div>
								  </div>
								</div>
							</div>
							<div class="col-md-1" style="margin-top: 2%;">
				                <a  class="btn btn-danger remove-actividad" onclick="remove_actividad({{ $t_a->id }},'{{ $num }}')" id='{{ $num }}' data-toggle="tooltip" data-original-title="Eliminar Actividad"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
				            </div>
						</div>
						<?php $num++; ?>
						@endforeach
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>-->
		<!--<div class="container-fluid animated flipInX confi" id="conf_3" style="display: none">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Ciclos y Ventas</h2>
					<img src="{{ url('/') }}/images/configuracion_crm/ciclosventas.png" style="border: 0 !important; margin-bottom: 1%;"><br>
					<button type="button" class="btn btn-success add-ciclo" data-toggle="tooltip" data-original-title="Agregar Ciclo"><i class="fa fa-plus" aria-hidden="true"></i></button>
					<input type="hidden" id="fila_ciclo" value="{{ count($ciclos) }}">
					<form id="formulario_ciclos_ventas" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }} 					    
					    <div class="row">
					    	<div class="col-md-12">
					    		<div class="panel-group" id="accordion-ciclo" role="tablist" aria-multiselectable="true">
									<?php $num=0; ?>
									@foreach($ciclos as $ciclo)
									@if(!empty($ciclo->imagen)) <?php $img=$ciclo->imagen; ?> @else <?php $img='images/noimage.png'; ?> @endif
									<div class="row" id="numero_ciclo{{ $num }}">
										<div class="col-md-11">
											<div class="panel panel-default cabecero_colp2" style="margin-bottom: 2%">
												<div class="panel-heading" role="tab" id="heading{{ $num }}">
												  <h4 class="panel-title">
												    <div class="row collapsed" role="button" data-toggle="collapse" data-parent="#accordion-ciclo" href="#ciclo{{ $num }}" aria-expanded="false" aria-controls="ciclo{{ $num }}">
												        <div class="col-md-1">
												            <img id="imagen_ciclo{{ $ciclo->id }}" src="{{ url('/') }}/{{ $img }}" style="border: 0 !important;margin-bottom: 1%;width: 80%;text-align: left;">
												        </div>
												        <div class="col-md-9" style="text-align: left;">
												        	<span style="font-size: 21px;font-weight: bold;" id="titulo_nombre_ciclos{{ $num }}">{{ $ciclo->nombre }}</span><br>
												            <span id="titulo_dias_ciclos{{ $num }}">Tiempo de transición: {{ $ciclo->dias }} dias</span>
												    	</div>
														<div class="col-md-2" style="text-align: right;">
														    <span style="font-size: 33px;text-shadow: 3px 1px 2px #ccc;" id="titulo_porcentaje_ciclos{{ $num }}">{{ $ciclo->porcentaje }}%</span>
														</div>
												    </div>
												  </h4>
												</div>
												<div id="ciclo{{ $num }}" class="panel-collapse collapse" style="background-color: #555555;" role="tabpanel" aria-labelledby="heading{{ $num }}">
												  <div class="panel-body">
												  	<input type="hidden" name="ciclos[{{ $num }}][id]" value="{{ $ciclo->id }}">
												    <div class="row"> 
												    	<div class="col-md-4">
												    		<div class="form-group">
															    <label for="nombre_ciclos{{ $num }}" style="color: #fff !important;">Nombre</label>
															    <input type="text" class="form-control ciclos g_ciclos nombre_ciclos" name="ciclos[{{ $num }}][nombre]" id="nombre_ciclos{{ $num }}" placeholder="Nombre" value="{{ $ciclo->nombre }}">
															</div>
												    	</div>
												    	<div class="col-md-4">
												    		<div class="form-group">
															    <label for="diastransicion{{ $num }}" style="color: #fff !important;">Dias de Transición</label>
															    <input type="text" class="form-control ciclos g_ciclos dias_transicion" name="ciclos[{{ $num }}][dias]" id="diastransicion{{ $num }}" placeholder="Dias de Transición" value="{{ $ciclo->dias }}">
															</div>
												    	</div>
												    	<div class="col-md-4">
												    		<div class="form-group">
															    <label for="porcentajeciclos{{ $num }}" style="color: #fff !important;">Porcentaje</label>
															    <input type="text" class="form-control ciclos g_ciclos porcen_ciclos" name="ciclos[{{ $num }}][porcentaje]" id="porcentajeciclos{{ $num }}" placeholder="Porcentaje" value="{{ $ciclo->porcentaje }}">
															</div>
												    	</div>
												    </div>
												    <div class="row">
												    	<div class="col-md-12" style="text-align: center;">
										                      <div class="dropzone imagen-perfil" style="height: 100px; width:100px; max-height: 100px; min-height: 100px;" data-width="32" data-height="32" data-ajax="false" data-originalsave="true">
										                        <input type="file" name="ciclos[{{ $num }}][imagen]" accept="image/gif, image/jpeg, image/png">
										                      </div>
													    </div>
													</div>
												  </div>
												</div>
											</div>
										</div>
										<div class="col-md-1">
											<a class="btn btn-danger remove-ciclo" onclick="remove_ciclo({{ $ciclo->id }},'{{ $num }}')" data-toggle="tooltip" data-original-title="Eliminar Ciclo"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
										</div>
									</div>
									
									<?php $num++; ?>
									@endforeach
								</div>
					    	</div>
					    	
					    </div>
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>-->
		<div class="container-fluid animated flipInX confi" id="conf_2" style="display: none;">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Horas Hombre</h2>
					<img src="{{ url('/') }}/images/configuracion_crm/horashombre.png" style="border: 0 !important; margin-bottom: 1%;"><br>
					<form id="formulario_horas_hombre" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }}
					    <div class="row">
					    	<div class="col-md-12">
					    		<div class="form-group" style="text-align: center;">
								    <label for="parafiscales">Porcentaje Parafiscales</label>
								    <input type="text" id="parafiscales" class="form-control parafiscales_hh hh" style="text-align: center;" placeholder="&#xf069; Parafiscales" name="parafiscales[porcentaje]" value="{{ $parafiscales->porcentaje }}">
								</div>
					    	</div>
					    </div>
					    <?php $num=0; ?>
					    @foreach($cargos as $cargo)
					    <input type="hidden" name="cargo[{{ $num }}][id]" value="{{ $cargo->id }}">
					    <div class="row">
					    	<div class="col-md-6">
						    	<div class="form-group">
								    <label for="cargo{{ $num }}">Cargo</label>
								    <input type="text" id="cargo{{ $num }}" class="form-control cargo_hh hh" placeholder="&#xf2bc; Cargo" name="cargo[{{ $num }}][nombre]" value="{{ $cargo->nombre }}">
								</div>
					    	</div>
					    	<div class="col-md-6">
					    		<div class="form-group">
								    <label for="salario{{ $num }}">Salario</label>
								    <input type="text" class="form-control salario_hh valor hh" placeholder="&#xf155; Salario" id="salario{{ $num }}" name="cargo[{{ $num }}][salario]" value="{{ $cargo->salario }}">
								</div>
					    	</div>
					    </div>
					    <?php $num++; ?>
					    @endforeach
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>
		<!--<div class="container-fluid animated flipInX confi" id="conf_5" style="display: none;">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Usuarios</h2>
					<img src="{{ url('/') }}/images/configuracion_crm/usuarios.png" style="border: 0 !important; margin-bottom: 1%;"><br>
					<form id="formulario_usuarios" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }}
					    <div class="row">
							<div class="col-md-12" style="margin-top: 2%;">
								<div class="panel-group" id="cargos_usuarios" role="tablist" aria-multiselectable="true">
								  <div class="panel panel-default colapsable">
								    <div class="panel-heading colapsable2" role="tab">
								      <h4 class="panel-title">
								        <a  data-toggle="collapse" data-parent="#cargos_usuarios" href="#collapse_cargos" aria-expanded="true" aria-controls="collapse_cargos">
								          Cargos 
								        </a>								        
								      </h4>
								    </div>
								    <div id="collapse_cargos" class="panel-collapse collapse" role="tabpanel" aria-labelledby="titulo">
								      <div class="panel-body">
								      <button type="button" class="btn btn-success add-cargo" data-toggle="tooltip" data-original-title="Agregar Cargo"><i class="fa fa-plus" aria-hidden="true"></i></button>
								      <input type="hidden" id="total_cargos" value="{{ count($cargos) }}">
									  <?php $num=0; ?>
									    <div class="row" id="contenedor_cargos">
										@foreach($cargos as $cargo)
											<div class="col-md-12" id="cargo_insert{{ $num }}">
												<input type="hidden" name="usuarios_cargo[{{ $num }}][id]" value="{{ $cargo->id }}">
										        <div class="row">									
													<div class="col-md-11">
														<div class="form-group">
															<label for="nombre_cargo{{ $num }}">Nombre</label>
															<input type="text" id="nombre_cargo{{ $num }}" placeholder="&#xf007; Cargo" class="form-control cargo_user" name="usuarios_cargo[{{ $num }}][nombre]" value="{{ $cargo->nombre }}">
														</div>
													</div>
													<div class="col-md-1">
														<a class="btn btn-danger remove-cargo" onclick="remove_cargo({{ $cargo->id }},'{{ $num }}')" data-toggle="tooltip" data-original-title="Eliminar Cargo"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
													</div>
												</div>
											</div>
										<?php $num++; ?>
										@endforeach
										</div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12" style="margin-top: 2%;">
								<div class="panel-group" id="emergencias_usuarios" role="tablist" aria-multiselectable="true">
								  <div class="panel panel-default colapsable">
								    <div class="panel-heading colapsable2" role="tab">
								      <h4 class="panel-title">
								        <a  data-toggle="collapse" data-parent="#emergencias_usuarios" href="#collapse_emergencias" aria-expanded="true" aria-controls="collapse_emergencias">
								          Emergencias 
								        </a>								        
								      </h4>
								    </div>
								    <div id="collapse_emergencias" class="panel-collapse collapse" role="tabpanel" aria-labelledby="titulo">
								      <div class="panel-body">
								      <button type="button" class="btn btn-success add-emercencia" data-toggle="tooltip" data-original-title="Agregar Emergencia"><i class="fa fa-plus" aria-hidden="true"></i></button>
								      <input type="hidden" id="total_emergencias" value="{{ count($emergencias) }}">
									  <?php $num=0; ?>
									    <div class="row" id="contenedor_emergencias">
										@foreach($emergencias as $emergencia)
											<div class="col-md-12" id="emergencia_insert{{ $num }}">
												<input type="hidden" name="usuarios_emeregencia[{{ $num }}][id]" value="{{ $emergencia->id }}">
										        <div class="row">									
													<div class="col-md-11">
														<div class="form-group">
															<label for="nombre_emergencia{{ $num }}">Nombre</label>
															<input type="text" id="nombre_emergencia{{ $num }}" placeholder="&#xf0f9; Emergencia" class="form-control emergencia_user" name="usuarios_emeregencia[{{ $num }}][nombre]" value="{{ $emergencia->nombre }}">
														</div>
													</div>
													<div class="col-md-1">
														<a class="btn btn-danger remove-emergencia" onclick="remove_emergencia({{ $emergencia->id }},'{{ $num }}')" data-toggle="tooltip" data-original-title="Eliminar Emergencia"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
													</div>
												</div>
											</div>
										<?php $num++; ?>
										@endforeach
										</div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho					
				</div>
			</div>
		</div>-->
		<div class="container-fluid animated flipInX confi" id="conf_3" style="display: none;">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Paises</h2>
					<div class="row paises">
						@foreach($paises as $pais)
						<div class="col-md-2">
							<div class="row">
								<div class="col-md-10">
									@if($pais->imagen!="")
						                <img class="im_ban" id="bandera{{ $pais->id }}" src="{{ url('/') }}/images/icons/{{ $pais->imagen }}" onclick="editar_pais({{ $pais->id }},'{{ $pais->name }}')">
						            @else
						            	<img class="im_ban" id="bandera{{ $pais->id }}" src="https://placeholdit.imgix.net/~text?txtsize=45&txt={{ str_replace(' ', '%20', $pais->name) }}&w=200&h=200" onclick="editar_pais({{ $pais->id }},'{{ $pais->name }}')">
						            @endif
								</div>
								<div class="col-md-2 bandera{{ $pais->id }}" style="text-align: center; @if(($pais->imagen)=='') display: none; @endif">
									<a onclick="remove_bandera({{ $pais->id }},'bandera{{ $pais->id }}')" style="color: red; cursor: pointer;" data-toggle="tooltip" data-original-title="Eliminar Bandera {{ $pais->name }}"><i class="fa fa-trash-o " aria-hidden="true"></i></a>
								</div>
							</div>
							
				            <h4>{{ $pais->name }}</h4>
						</div>
						@endforeach
					</div>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>
        <div class="container-fluid animated flipInx confi" id="conf_4" style="display: none;">
            <div class="row">
                <div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
                <div class="col-md-8 bordeado" style="background-color: #fff;">
                    <img src="/images/configuracion_crm/configuracion.gif" class="cargando">
                    <div class="permisos">

                    </div>
                </div>
                <div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
            </div>
        </div>
	</div>
</div>
<div class="modal fade" id="editarPais" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-header">
		  	<img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
		    <h4 class="modal-title" style="margin-top: 1%">Editar Bandera</h4>
		    
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="modal-body">
		    <div class="row margen-inferior">
			    <div class="col-md-12">
		          <form method="POST" id="formulario_pais" enctype="multipart/form-data">
		          	@if (count($errors) > 0)
				        <div class="alert alert-danger">
				            <ul>
				                @foreach ($errors->all() as $error)
				                  <li>{{ $error }}</li>
				                @endforeach
				            </ul>
				        </div>
				    @endif
		            {{ csrf_field() }}
		            <input type="hidden" id="id_pais_editar" name="id" value="">
		            <h3 id="nombre_pais" style="margin-top: 2%;"></h3>
		            <div class="col-md-12" style="text-align: center; margin: 10px">
		              <div class="dropzone imagen-pais" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
		                <input type="file" name="foto" accept="image/gif, image/jpeg, image/png">
		              </div>
		            </div>
		          </form>
			    </div> 
		      </div>
		    </div>
		  </div>
	</div>
</div>
<div id="guardado-exitoso" class="alert alert-success guardado-exitoso animated fadeInRight" style="display: none">
  <strong>Guardado!</strong><br> Guardado exitosamente.
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	url_basico='{{ url("/") }}';
</script>
<script src="/images/configuracion_crm/step/js/index.js"></script>
<script src="/js/jquery.maskMoney.min.js"></script>
<script src="/js/configuraciongeneral/script.js"></script><!--porcentajes-->
<!--archivo -->
<script src="/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="/components/file/js/plugins/purify.min.js"></script>
<script type="text/javascript" src="/js/html5imageupload.js?v1.4.3"></script>
<script src="/js/configuraciongeneral/archivo.js"></script>
<!--archivo -->
<!--tooltip-->
<script src="/assets/js/king-common.js"></script>
<script src="/demo-style-switcher/assets/js/deliswitch.js"></script>
<!--tooltip-->

@endsection
