@extends('template.app')

@section('title', 'Configuración General')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/configuraciongeneral/style.css">
<link rel="stylesheet" href="{{ url('/') }}/images/configuracion_crm/step/css/style.css">

<div class="row">
	<div class="col-md-12">
		<h1 class="titulo"><img src="{{ url('/') }}/images/configuracion_crm/configuracion.gif">Configuración General del Sistema</h1>
	</div>	

	<div class="col-md-12" style="text-align: center;">
		<div class="step-nav">
			<div class="step first">
				<div class="radial-progress active" data-index="0" data-toggle="tooltip" data-original-title="Configurar Dashboard">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-dashboard fa-fw"></i></p>
			</div>	
			<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="1" data-toggle="tooltip" data-original-title="Configurar Tipo de Actividad">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p><i class="fa fa-building" aria-hidden="true"></i></p>
			</div>
			<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="2">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p>3</p>
			</div>
			<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="3">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p>4</p>
			</div>
			<div class="step">
				<div class="line">
					<div class="progress"></div>
				</div>
				<div class="radial-progress" data-index="4">
				    <div class="circle">
				        <div class="mask full">
				            <div class="fill"></div>
				        </div>
				        <div class="mask half">
				            <div class="fill"></div>
				            <div class="fill fix"></div>
				        </div>
				        <div class="shadow"></div>
				    </div>
			    	<div class="inset"><div class="inner-circle"></div></div>
				</div>
				<p>5</p>
			</div>
		</div>
		
		<div class="container-fluid animated flipInX confi" id="conf_1">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Dashboard</h2>
					<h4>Participación según probabilidad</h4>
					<form id="conf_dashboard" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }} 
						<div class="row">
							<div class="col-md-4" style="margin: 1%;">
								<img src="{{ url('/') }}/images/configuracion_crm/alta.png" style="border: 0 !important; margin-bottom: 1%;">
								<h3>Alta</h3>
								<div class="form-group">
									<label for="porcentaje1">Porcentaje</label>
									<input type="text" id="porcentaje1" name="porcentaje[alta]" class="form-control porcentaje formulariodashboard"  placeholder="&#xf040; &nbsp;Porcentaje" value="{{ $participacion->alta }} %">
								</div>
							</div>
							<div class="col-md-3" style="margin: 1%;">
								<img src="{{ url('/') }}/images/configuracion_crm/media.png" style="border: 0 !important; margin-bottom: 1%;">
								<h3>Media</h3>
								<div class="form-group">
									<label for="porcentaje2">Porcentaje</label>
									<input type="text" id="porcentaje2" name="porcentaje[media]" class="form-control porcentaje formulariodashboard" placeholder="&#xf040; &nbsp; Porcentaje" value="{{ $participacion->media }} %">
								</div>
								
							</div>
							<div class="col-md-4" style="margin: 1%;">
								<img src="{{ url('/') }}/images/configuracion_crm/baja.png" style="border: 0 !important; margin-bottom: 1%;">
								<h3>Baja</h3>
								<div class="form-group">
									<label for="porcentaje3">Porcentaje</label>
									<input type="text" id="porcentaje3" name="porcentaje[baja]" class="form-control porcentaje formulariodashboard" placeholder="&#xf040; &nbsp; Porcentaje" value="{{ $participacion->baja }} %">
								</div>
								
							</div>
						</div>
						<h4>Metas Oportunidades General</h4>
						<div class="col-md-4" style="margin: 1%; min-height: 338px;">
							<img src="{{ url('/') }}/images/configuracion_crm/plazo.png" style="border: 0 !important; margin-bottom: 1%; width: 15%;">
							<h3>Corto</h3>
							<div class="dias_group">							
								<div class="form-group">
									<label for="cantidad1">Cantidad días</label>
									<input type="text" id="cantidad1" name="dias[corto_dias]" class="form-control dias formulariodashboard" placeholder="&#xf271; &nbsp;Dias" value="{{ number_format($metas->corto_dias) }}">
								</div>
								<div class="form-group">
									<label for="valor1">Valor</label>
									<input type="text" id="valor1" name="valor[corto_valor]" class="form-control valor formulariodashboard" placeholder="&#xf0d6; &nbsp;Valor" value="{{ number_format($metas->corto_valor) }}">
								</div>
							</div>
						</div>
						<div class="col-md-3" style="margin: 1%; min-height: 338px;">
							<img src="{{ url('/') }}/images/configuracion_crm/plazo.png" style="border: 0 !important; margin-bottom: 1%; width: 30%;">
							<h3>Mediano</h3>
							<div class="dias_group">							
								<div class="form-group">
									<label for="cantidad2">Cantidad días</label>
									<input type="text" id="cantidad2" name="dias[medio_dias]" class="form-control dias formulariodashboard"  placeholder="&#xf271; &nbsp;Dias" value="{{ $metas->medio_dias }}">
								</div>
								<div class="form-group">
									<label for="valor2">Valor</label>
									<input type="text" id="valor2" name="valor[medio_valor]" class="form-control valor formulariodashboard" placeholder="&#xf0d6; &nbsp;Valor" value="{{ number_format($metas->medio_valor) }}">
								</div>
							</div>
						</div>
						<div class="col-md-4" style="margin: 1%;">
							<img src="{{ url('/') }}/images/configuracion_crm/plazo.png" style="border: 0 !important; margin-bottom: 1%;">
							<h3>Largo</h3>							
							<div class="form-group">
								<label for="cantidad3">Cantidad días</label>
								<input type="text" id="cantidad3" name="dias[largo_dias]" class="form-control dias formulariodashboard" readonly placeholder="&#xf271; &nbsp;Dias" value="{{ $metas->largo_dias }}">
							</div>
							<div class="form-group">
								<label for="valor3">Valor</label>
								<input type="text" id="valor3" name="valor[largo_valor]" class="form-control valor formulariodashboard" placeholder="&#xf0d6; &nbsp;Valor" value="{{ number_format($metas->largo_valor) }}">
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>
		<div class="container-fluid animated flipInX confi" id="conf_2" style="display: none">
			<div class="row">
				<div class="col-md-2" style="color: transparent;">
					Espacio Izquierdo
				</div>
				<div class="col-md-8 bordeado" style="background-color: #fff;">
					<h2>Tipo de Actividad</h2>
					<img src="{{ url('/') }}/images/configuracion_crm/actividad.png" style="border: 0 !important;"><br>
					<button type="button" class="btn btn-success add-actividad" data-toggle="tooltip" data-original-title="Agregar Actividad"><i class="fa fa-plus" aria-hidden="true"></i></button>
					<input type="hidden" id="fila_act" value="{{ count($tipos_actividades) }}">
					<form id="conf_actividad" method="POST" enctype="multipart/form-data">
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                  <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    @endif
					    {{ csrf_field() }} 
						<?php $num=0;  ?>
						@foreach($tipos_actividades as $t_a)
						<div class="row act" id="actividad_insertada{{ $num }}">
							<input type="hidden" name="actividad[{{ $num }}][id]" value="{{ $t_a->id }}">
							<div class="col-md-2">
								<div class="form-group">
									<label for="orden{{ $num }}">Orden</label>
									<input type="text" style="text-align: center;" id="orden{{ $num }}" placeholder="&#xf03c; Numero" class="form-control actividad numero" name="actividad[{{ $num }}][orden]" value="{{ $t_a->orden }}">
								</div>
							</div>
							<div class="col-md-9" style="margin-top: 2%;">
								<div class="panel-group" id="actividades" role="tablist" aria-multiselectable="true">
								  <div class="panel panel-default">
								    <div class="panel-heading" role="tab" id="titulo{{ $num }}">
								      <h4 class="panel-title">
								        <a  data-toggle="collapse" id="titulo_nombre{{ $num }}" data-parent="#actividades" href="#collapse{{ $num }}" aria-expanded="true" aria-controls="collapse{{ $num }}">
								          {{ $t_a->concepto }} 
								        </a>
								      </h4>
								    </div>
								    <div id="collapse{{ $num }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="titulo{{ $num }}">
								      <div class="panel-body">
								        <div class="row">									
											<div class="col-md-12">
												<div class="form-group">
													<label for="concepto{{ $num }}">Concepto</label>
													<input type="text" id="concepto{{ $num }}" placeholder="&#xf0f6; Concepto" class="form-control actividad titulo" name="actividad[{{ $num }}][concepto]" value="{{ $t_a->concepto }}">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="descripcion{{ $num }}">Descripcion</label>
													<textarea class="form-control actividad" id="descripcion{{ $num }}" name="actividad[{{ $num }}][descripcion]" rows="8" placeholder="&#xf0f6; Descripcion">{{ $t_a->descripcion }}</textarea>
												</div>
											</div>	
										</div>	
								      </div>
								    </div>
								  </div>
								</div>
							</div>
							<div class="col-md-1" style="margin-top: 2%;">
				                <a  class="btn btn-danger remove-actividad" id='{{ $num }}' data-toggle="tooltip" data-original-title="Eliminar Actividad"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
				            </div>
						</div>
						<?php $num++; ?>
						@endforeach
					</form>
				</div>
				<div class="col-md-2" style="color: transparent;">
					Espacio Derecho
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('scripts')
<script type="text/javascript">
	url_basico='{{ url("/") }}';
</script>
<script src="{{ url('/') }}/images/configuracion_crm/step/js/index.js"></script>
<script src="../js/jquery.maskMoney.min.js"></script>
<script src="{{ url('/') }}/js/configuraciongeneral/script.js"></script><!--porcentajes-->

@endsection