<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Visitas Clientes')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
    <style type="text/css">
      .tooltip-inner {
        white-space: pre-wrap;
      }
      .nuevo-tamano{
        width: 14%
      }
      .tamano30{
        width: 30px !important;
        max-width: 30px !important;
      }
      .unalinea{
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
      }

      .mas-pequeño{
        line-height: 1.5;
        font-size: small;
        color: #636363;
        font-weight: 400;
        display: block !important;
        margin: 0px !important;
      }

      .centrar-vertical{
        vertical-align: middle !important;
      }

      .justify{
        text-align: justify;
      }
    </style>
       <div class="container-fluid animated slideInDown">
      <div class="row">

        <div class="col-md-12 panel-view">
          <div class="pull-right">
            <div class="btn-group">
              <?php if($permiso_agendar=="Si"){ ?>
              <a href="{{url('/')}}/agendarvisita" class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Agendar visita</a>
              <?php } ?>
            </div>
          </div>
          <h2>Listado de Visitas de Clientes</h2>
          <div class="table-responsive">
            <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
              <thead>
                <tr>                  
                  <th class="text-center">Item</th>
                  <th class="text-center">Cliente</th>
                  <th class="text-center">Fecha inicio</th>
                  <th class="text-center">Fecha final</th>
                  <th class="text-center">Agendado desde</th>
                  <th class="text-center">Agendado por </th>
                  <th class="text-center">Num. actividades </th>
                  <th class="text-center">Num. visitantes </th>
                  <th class="text-center">Acciones</th>
                </tr>
              </thead>
              <tbody>
                @php setlocale(LC_ALL, "es_CO.UTF-8");  @endphp 
                @foreach($lista as $dato)
                  <?php  
                    $oportunidad  = App\Oportunidades::find($dato->oportunidad);
                    if(isset($oportunidad->empresa_id)){
                    $empresa      = App\Empresa::find($oportunidad->empresa_id);
                    $query        = 'SELECT * FROM users U WHERE U.id = '.$dato->user_id;
                    $user         = Illuminate\Support\Facades\DB::select($query);
                    $usuario      = $user[0];
                    $visitantes   = App\Agenda_clientes_visitante::where("agenda_clientes", $dato->id)->get();
                    $actividades   = App\Agenda_clientes_actividade::where("agenda_clientes", $dato->id)->get();

                    $nombre   = explode(" ",$usuario->nombres);
                    $apellido = explode(" ",$usuario->apellidos); 
                    $nombre_usuario = $nombre[0] ." ". $apellido[0];
                  ?>
                <tr>
                  <td class="centrar-vertical"><p class="mas-pequeño"></p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ $empresa->nombre }}</p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ strftime("%d/%m/%Y", strtotime($dato->fecha_inicio)) }}</p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ strftime("%d/%m/%Y", strtotime($dato->fecha_final)) }}</p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->created_at)) }}</p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ $nombre_usuario }}</p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ count($actividades) }} actividades</p></td>
                  <td class="centrar-vertical"><p class="mas-pequeño">{{ count($visitantes) }} visitantes</p></td>
                  <td class="centrar-vertical"><a @if($dato->estado!="pendiente") href="{{ url('/') }}/agendacliente/{{ $dato->id }}" @else href="{{ url('/') }}/asignarresponsable/{{ $dato->id }}" @endif class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver visita"><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                </tr>
                <?php } ?>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    
@endsection

@section('scripts')
<script type="text/javascript" src="/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/components/datatable/js/dataTables.material.min.js"></script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ visitas de clientes",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ visitas de clientes",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 visitas de clientes",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      }
    });

    var t = $('#example').DataTable();
     
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
  });
</script>
@endsection
