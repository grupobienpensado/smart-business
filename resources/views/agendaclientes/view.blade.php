@extends('template.app')
@section('title', 'Ver visita de cliente')
@section('content')
<link href='{{ url("/") }}/components/newfullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='{{ url("/") }}/components/newfullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">

<style type="text/css">
	.cliente-1 {
		border-collapse: collapse;
	    background-color: #051d60;
	    background-image: url({{ url('/') }}/images/mail/ver_banner.jpg);
	    background-repeat: no-repeat;
	    background-position: center;
	    min-height: 500px;
	}
	.cliente-3{
		background-color: #051d60;
    	color: aliceblue !important;
	}

	.cliente-3>p{
		font-family: 'Roboto Condensed', sans-serif;
	    font-size: 18px;
	    font-weight: 300 !important;
	    line-height: 1.1;
	    color: aliceblue;
	}
	.cliente-4{
		background-color: #efefef;
	}
	p.parrafo-date {
	    color: #555555;
	    font-size: large;
	}

	p.parrafo-tite {
	    font-size: x-large;
	    font-weight: bold;
	}

	.sombra_superior{
		float: left;
		top: 220px;
    	position: absolute;
    	display: table;
	}

	h2.titulo {
	    display: table-cell;
	    vertical-align: middle;
	    color: #f9f9f9 !important;
	    position: relative;
	    left: 10px;
	    font-size: 45px !important;
	    font-family: 'Roboto Slab', serif;
	}

	h2>span {
	    color: #5fc2ff;
	    float: left;
	}

	h2.title-genel.pull-right {
	    color: #5fc2ff;
	    font-size: 30px;
	    font-weight: bold;
	}
	.media-body{
	    display: block;
    	vertical-align: top;
    	text-align: center;
    	line-height: 0.9;
	}

	.media-object-client {
	    display: initial;
	}

	p:first-letter {
	text-transform: uppercase !important;
	}

	span:first-letter {
	text-transform: uppercase !important;
	}

	.soloprimer:first-letter{
	text-transform: uppercase !important;
	}

	.capitalize{
	text-transform: capitalize !important;
	}

	.text-muted{
	text-transform: lowercase;
	}
	p.normalp{
	font-weight: 400;
	}
	.unalinea{
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	}

  	.username {
	    text-align: center;
	}

	.swal2-modal.swal2-noanimation.swal2-show {
	    width: 70% !important;
	    overflow-x: auto !important;
	}

	.espacio{
		padding: 10px;
	}

	/*.fc-left>h2 {
	    text-transform: capitalize;
	}*/

	*{
	  margin:0;
	}

	ol.indicator {
	    display: none !important;
	}

	h3.title {
	    display: none !important;
	}

	a.close {
	    margin-top: 140px !important;
	    padding: 6px !important;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	    font-size: 60px !important;
	    font-weight: 100;
	    text-decoration: none;
	    text-shadow: 0 0 2px #000;
	    text-align: center;
	    background: rgba(0,0,0,.5);
	    -webkit-box-sizing: content-box;
	    -moz-box-sizing: content-box;
	    box-sizing: content-box;
	    border: 3px solid #fff;
	    -webkit-border-radius: 50%;
	    -moz-border-radius: 50%;
	    border-radius: 50%;
	    color: aliceblue !important;
	}

.contenedor{
  width:90px;
  height:240px;
  position:absolute;
  right:0px;
  bottom:0px;
  z-index: 9999;
}
.botonF1{
  width:60px;
  height:60px;
  border-radius:100%;
  background:#F44336;
  right:0;
  bottom:0;
  position:absolute;
  margin-right:16px;
  margin-bottom:16px;
  border:none;
  outline:none;
  color:#FFF;
  font-size:36px;
  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
  transition:.3s;  
}
span{
  transition:.5s;  
}
.botonF1:hover span{
  transform:rotate(360deg);
}
.botonF1:active{
  transform:scale(1.1);
}
/*.btn{
  width:40px;
  height:40px;
  border-radius:100%;
  border:none;
  color:#FFF;
  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
  font-size:28px;
  outline:none;
  position:absolute;
  right:0;
  bottom:0;
  margin-right:26px;
  transform:scale(0);
}*/
.botonF2{
  background:#2196F3;
  margin-bottom:85px;
  transition:0.5s;
}
.botonF3{
  background:#673AB7;
  margin-bottom:130px;
  transition:0.7s;
}
.botonF4{
  background:#009688;
  margin-bottom:175px;
  transition:0.9s;
}
.botonF5{
  background:#FF5722;
  margin-bottom:220px;
  transition:0.99s;
}
.animacionVer{
  transform:scale(1);
}

#myModal, .modal-lg, #modal_video {
    min-height: auto !important;
    bottom: -20px;
}

.logo-essi-video {
    z-index: 999999;
    right: 4%;
    top: 6%;
}

.archi-img{
	    width: 60px;
}
</style>
<?php setlocale(LC_ALL, "es_CO.UTF-8");  ?>
<div class="main-content">
	<div class="widget">
		<div class="pull-right">
            <div class="btn-group">
				<a href="{{url('agendarvisita')}}" class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Agendar visita</a>
				<a href="{{url('visitasclientes')}}" class="btn btn-sm btn-info"><i class="fa fa-list" aria-hidden="true"></i> Listado de visitas</a>
			</div>
		</div>
		<div class="widget-content cliente-1">
			<h2 class="title-genel pull-right">Agenda visita cliente</h2>			
			<div class="sombra_superior">				
				<div class="profile-empresa pull-left">
			        <img src="
			           @if(!empty($empresa->logo))
			              {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
			           @else
			              http://via.placeholder.com/250x250/fff/948e8e?text=Logo 
			           @endif" class="img-fluid mx-auto d-block img-empresa">
			    </div>
			    <h2 class="titulo">{{ $empresa->nombre }}<br><span>{{ $empresa->ciudad }}</span></h2>
			</div>
		</div>
		<div class="widget-content cliente-2">			
			<div class="row">
				<div class="col-md-6">
					<p class="parrafo-tite">Fecha inicio</p>
					<p class="parrafo-date">{{ strftime("%A %d %B %Y", strtotime($datos->fecha_inicio)) }}</p>
				</div>
				<div class="col-md-6">
					<p class="parrafo-tite">Fecha fin</p>
					<p class="parrafo-date">{{ strftime("%A %d %B %Y", strtotime($datos->fecha_final)) }}</p>
				</div>
			</div>
		</div>
		<div class="widget-content cliente-3">			
			<h2 class="espacio">Acerca de la visita</h2>
			<p>{{ $datos->observaciones }}</p>
			<div style="display: grid;">
				<div style="padding: 10px; margin: 10px; display: inline-block;">
					@foreach($archivos as $archivo)
						@if(isset($archivo) && !empty($archivo))
						<?php 
							$extension   = explode(".",$archivo->archivo); 
							$extension   = $extension[1];
							if ($extension == "BMP" || $extension == "GIF" || $extension == "PNG" || $extension == "JPG" || $extension == "TIF" || $extension == "bmp" || $extension == "gif" || $extension == "png" || $extension == "jpg" || $extension == "tif") { ?>
								<a data-gallery="" title="{{ $archivo->archivo }}" href="{{url('/')}}/images/file/agenda/{{ $archivo->archivo }}"><img src="{{ url('/') }}/images/blancos/images.png" class="espacio archi-img"></a>
						<?php }elseif ($extension === "AVI" || $extension === "MOV" || $extension === "WMV" || $extension === "FLv" || $extension === "MP4" || $extension === "avi" || $extension === "mov" || $extension === "wmv" || $extension === "flv" || $extension === "mp4") { ?>
								<img src="{{ url('/') }}/images/blancos/video.png" class="espacio archi-img" onclick="openVideo('{{ $archivo->archivo }}')">						
						<?php }elseif ($extension === "PDF" || $extension === "pdf") { ?>
								<img src="{{ url('/') }}/images/blancos/pdf.png" class="espacio archi-img" onclick="openDocument('{{ $archivo->archivo }}')">
						<?php }elseif ($extension === "OCX" || $extension === "ocx" || $extension === "DOC" || $extension === "doc") { ?>
								<img src="{{ url('/') }}/images/blancos/word.png" class="espacio archi-img" onclick="openDocument('{{ $archivo->archivo }}')">
						<?php }elseif ($extension === "XLSX" || $extension === "xlsx" || $extension === "XLSM" || $extension === "xlsm" || $extension === "XML" || $extension === "xml") { ?>
								<img src="{{ url('/') }}/images/blancos/excel.png" class="espacio archi-img" onclick="openDocument('{{ $archivo->archivo }}')">
						<?php }else{ ?>
								<a href="{{url('/')}}/images/file/agenda/{{ $archivo->archivo }}"><img src="{{ url('/') }}/images/blancos/otros.png" class="espacio archi-img"></a>
						<?php } ?>
						@endif
					@endforeach
				</div>
			</div>

		</div>
		<div class="widget-content">	
			<div class="row">	
			@foreach($visitantes as $visita)
				<?php 
		            $cadena = str_replace(array("\n", "\r", "\n\r"), " ", $visita->observaciones);
		        ?>
				<div class="col-md-3" style="display: grid;" onclick="abrirDetalle('{{ $visita->tratamiento." ".$visita->nombres." ".$visita->apellidos }}', '{{ $visita->cargo }}', '{{ $cadena }}', '{{ $visita->foto }}')">													
						<div class="media-left">
							<img src="@if(!empty($visita->foto))
							{{ url('/') }}/images/file/clientes/{{ $visita->foto }}
							@else
							http://via.placeholder.com/300x400/051d60/fff?text=Foto 
							@endif" alt="Edinson Ossa" class="media-object-client img-circle" style="max-width:80px; border-radius: 50%;">
						</div>
					    <div class="media-body">
					        <p style="margin: 3px;">{{ $visita->tratamiento." ".$visita->nombres." ".$visita->apellidos }}</p>
					        <p class="text-muted username soloprimer normalp" style="padding: 0px !important;margin: 0px !important;">
					        	{{ $visita->cargo }}
					        </p>                       
					    </div>			                    			
				</div>
			@endforeach
			</div>
		</div>
		<div class="widget-content cliente-4">
			<div id='calendar'></div>
		</div>
	</div>
</div>

<!--<div class="contenedor">
<button class="botonF1">
  <span><i class="fa fa-paperclip"></i></span>
</button>
 </div>-->

 <div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <img src="{{ url('/') }}/images/logo.png" class="logo-essi-video"/>
    <div id="modal_video" class="modal-content" style="border: 0; background: rgba(240, 248, 255, 0);">
    </div>
  </div>
</div>
@endsection


@section('scripts')
<script src='{{ url("/") }}/components/newfullcalendar/lib/moment.min.js'></script>
<!--<script src='{{ url("/") }}/components/newfullcalendar/lib/jquery.min.js'></script>-->
<script src='{{ url("/") }}/components/newfullcalendar/fullcalendar.min.js'></script>
<script src='{{ url("/") }}/components/newfullcalendar/locale-all.js'></script>


<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>

<script type="text/javascript">
	abrirDetalle = function(nombre, cargo, detalle, foto) {
		if (foto === "") {
			foto = `<img src="http://via.placeholder.com/300x400/051d60/fff?text=Foto " alt="Edinson Ossa" class="media-object-client img-circle" style="max-width:80px; border-radius: 50%;">`;
		}else{
			foto = `<img src="{{ url('/') }}/images/file/clientes/`+foto+`" alt="Edinson Ossa" class="media-object-client img-circle" style="max-width:80px; border-radius: 50%;">`;
		}
		swal({
		  title: 'Agenda visita cliente',
		  animation: "slide-from-top",
		  confirmButtonText: 'Aceptar',
		  showLoaderOnConfirm: true,
		  html:
			`<div class="form-group row">				    	
				<div class="col-md-4" style="display: grid;">
		        	<div class="media-left">
						`+foto+`
					</div>
				    <div class="media-body">
				        <p style="margin: 3px;">`+nombre+`</p>
				        <p class="text-muted username soloprimer normalp" style="padding: 0px !important;margin: 0px !important;">
				        	`+cargo+`
				        </p>                       
				    </div>	
		        </div>
				<div class="col-md-8">
					<p style="    color: #555;text-align: justify;font-weight: 400;">`+detalle+`</p>
				</div>		          	
		    </div>`
		})
	}
	$(function() {
		$('#calendar').fullCalendar({ 
			defaultView: 'agendaWeek',
			defaultDate: moment("{{ $descripciones[0]->fecha }}"),
			locale: 'es',
            allDaySlot: false,
			navLinks: false,
			selectable: false,
			selectHelper: false,
			axisFormat : "HH:mm",
    		agenda : "HH:mm",
			weekNumbers: true,
			weekNumbersWithinDays: true,			
			columnFormat: 'dddd D',
			editable: false,
			eventLimit: false,
			events: [
				<?php 
					for ($i = 0; $i < count($descripciones); $i++) {
						try {
							$hora_inicio   = explode(":",$descripciones[$i]->hora_inicio);
				            $hora_fin      = explode(":",$descripciones[$i]->hora_fin);

				            $start = \Carbon\Carbon::createFromTime($hora_inicio[0], $hora_inicio[1], 0, 'America/Bogota');
				            $end   = \Carbon\Carbon::createFromTime($hora_fin[0], $hora_fin[1], 0, 'America/Bogota');
				            $responsables  = App\Agenda_clientes_actividades_responsable::where("agenda_clientes_actividades", $descripciones[$i]->id)->get();

				            for ($j = 0; $j < count($responsables); $j++) {
				            	$user  = App\User::find($responsables[$j]->responsable);
				            	$nombre   = explode(" ",$user->nombres);
                              	$apellido = explode(" ",$user->apellidos); 
					    		if (isset($nombre[2])) {
					    			$user->name = $nombre[0] ." ". $apellido[0];
					    		}else{
					    			$user->name = $nombre[0] ." ". $apellido[0];
					    		}
				            	$responsables[$j]->user = $user;
			            	}

							$start = $descripciones[$i]->fecha."T".$start->format('H:i:s'); 
							$end   = $descripciones[$i]->fecha."T".$end->format('H:i:s');

							echo "{
					            title  : '".$descripciones[$i]->descripcion."',
					            start  : '".$start."',
					            end    : '".$end."',
					            color  : '#051d60',
					            textColor: '#fff',
					            objeto : ".$descripciones[$i].",
					            responsables : ".$responsables."
					        },";
						} catch (Exception $e) { }
					}
				?>
		    ],	
		    eventClick: function(event) {
				var diasemanaActual = moment().day();	
				var semana = moment(event.start).week();
				var semanaActual = moment().week();
				
				swal({
				  title: 'Agenda visita cliente',
				  animation: "slide-from-top",
				  confirmButtonText: 'Aceptar',
				  showLoaderOnConfirm: true,
				  html:
			    	`<div class="form-group row">				    	
			    		<div class="col-12">
			            	<h5 class="card-header centrado"> Detalle de la actividad </h5>
			            	<p id="detalleActividad"></p>
			            </div>
			    		<div class="col-md-4">
							<h5 class="card-header centrado"> Fecha </h5>
							<p class="parrafo-date" id="generalInit"></p>
						</div>
						<div class="col-md-4">
							<h5 class="card-header centrado"> Hora inicio </h5>
							<p class="parrafo-date" id="horaInit"></p>
						</div>
						<div class="col-md-4">
							<h5 class="card-header centrado"> Hora fin </h5>
							<p class="parrafo-date" id="horaFin"></p>
						</div>			          	
			        </div>
			        <div class="row" id="listResponsables"></div>`,
				  onOpen: function () {
				    var count = 0;
				    moment.locale("es");
				    $("#detalleActividad").text(event.objeto.descripcion);
				    $("#generalInit").text(moment(event.objeto.fecha).format('dddd, DD MMMM YYYY'));
				    $("#horaInit").text(event.objeto.hora_inicio);
				    $("#horaFin").text(event.objeto.hora_fin);
				    var html = ``;
				    console.log(event.responsables);

				    $.each( event.responsables, function( key, value ) {
				    	console.log(value, "este es el valor");
				    	var foto = `<img src="http://via.placeholder.com/200x200/051d60/fff?text=Foto" alt="Edinson Ossa" class="media-object-client img-circle" style="max-width:80px; border-radius: 50%;">`;
				    	if (value.user.foto != "") {
				    		foto = `<img src="{{ url('/') }}/images/file/clientes/`+value.user.foto+`" alt="Edinson Ossa" class="media-object-client img-circle" style="max-width:80px; border-radius: 50%;">`;
				    	}

				    	html += `
				    		<div class="col-md-2" style="display: grid;">													
								<div class="media-left">`+foto+`</div>
							    <div class="media-body">
							        <p style="margin: 3px;">`+value.user.name+`</p>
							        <p class="text-muted username soloprimer normalp" style="padding: 0px !important;margin: 0px !important;">
							        	`+value.user.cargo+`
							        </p>                       
							    </div>			                    			
							</div>`;
					});   
					$("#listResponsables").html(html);  
				       
				  }
				})
		        return false;
		    },	    
			timeFormat: 'HH:mm:ss',
		});
	});

	$('.botonF1').hover(function(){
	  $('.btn').addClass('animacionVer');
	})
	$('.contenedor').mouseleave(function(){
	  $('.btn').removeClass('animacionVer');
	})

		openVideo = function(nombre) {
		   	dir = "{{url('/')}}/images/file/agenda/"+nombre;
		    video = `<div id="play_video_modal" class="caja">
		                <video id="Video1" src="`+dir+`" loop preload="auto">
		                  Tu navegador no implementa el elemento <code>video</code>.
		                </video>
		                <div id="buttonbar">
		                    <button class="video_button" id="restart" onclick="restart();"><i class="fa fa-repeat" aria-hidden="true"></i></button> 
		                    <button class="video_button" id="rew" onclick="skip(-10)"><i class="fa fa-backward" aria-hidden="true"></i></button>
		                    <button class="video_button" id="play" onclick="vidplay()"><i class="fa fa-play" aria-hidden="true"></i></button>
		                    <button class="video_button" id="fastFwd" onclick="skip(10)"><i class="fa fa-forward" aria-hidden="true"></i></button>
		                    <button class="video_button pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
		                    <button class="video_button pull-right" onclick="pantallaCompletaVideo()"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>
		                </div> 
		            <div>`;

		    $("#modal_video").html(video);
		    $("#myModal").modal();
		}

		openDocument = function(nombre) {
		    dir = "{{url('/')}}/images/file/agenda/"+nombre;
		    contenido = `<iframe src="https://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:850px;" frameborder="0"></iframe>`;
		    $("#modal_video").html(contenido);
		    $("#myModal").modal();
		}

		           
		           
		function vidplay() {
		   var video = document.getElementById("Video1");
		   var button = $("#play");

		   if (video.paused) {
		      video.play();
		      button.html('<i class="fa fa-pause" aria-hidden="true"></i>');
		   } else {
		      video.pause();
		      button.html('<i class="fa fa-play" aria-hidden="true"></i>');
		   }
		}

		function restart() {
		    var video = document.getElementById("Video1");
		    video.currentTime = 0;
		}

		function skip(value) {
		    var video = document.getElementById("Video1");
		    video.currentTime += value;
		}  

		$(function () {
		    $('#myModal').on('hidden.bs.modal', function (e) {
		        var video = document.getElementById("Video1");
		        var button = $("#play");
		          video.pause();
		          button.html('<i class="fa fa-play" aria-hidden="true"></i>');
		    });
		});
</script>

@endsection
