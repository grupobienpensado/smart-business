@extends('template.app')
@section('title', 'Ver Empresa')
@section('content') 
<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
    text-align: left;
}
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
}
.table td{
    border: 0 !important;
}
.usuario-imagen{
    height: 100px;
    width: 100px;
    margin: 5px;
    display: inline-block;
    background-size: cover;
    border-radius: 100%;
    border: 0.5px solid;
}

.usuario-imagen2{
    height: 80px;
    width: 80px;
    margin: 5px;
    display: inline-block;
    background-size: cover;
    border-radius: 100%;
    border: 0.5px solid;
}
/*li{
    margin-left: 35px;
    list-style: none;
}*/
.seleccion{
    opacity: 0.2;
    border: 0.5px solid #15ce23;
}

label.card-header {
    background: #051d60;
    color: aliceblue;
    font-weight: 500;
    width: 100%;
    text-align: center;
    margin-bottom: 30px;
    margin-top: 30px;
}

.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}

.rowmail {
    padding: 4px;
}

.ca12{
    margin-bottom: 40px;
}

.pull-right {
    margin-right: 30px;
}
</style>
<div class="widget">
    <div class="row">
        <div class="col-md-12 ca12">
                <h2 style="padding-left: 30px;">Seleccione el personal que recibirá este correo</h2>
            <form method="POST" action="{{ url('sendmailsave') }}"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $id }}">
                <div id="elementos"></div>
                <ul>        
                    <li><label class="card-header">Responsables</label></li>
                    <div class="col-md-12">
                        <div class="row">                                                          
                            @foreach($userResponsables as $userResponsable)
                            <?php 
                                $usuario = App\User::find($userResponsable);
                                $nombre   = explode(" ",$usuario->nombres);
                                $apellido = explode(" ",$usuario->apellidos); 
                                if (isset($nombre[2])) {
                                    $usuario->name = $nombre[0] ." ". $apellido[0];
                                }else{
                                    $usuario->name = $nombre[0] ." ". $apellido[0];
                                }

                                $fotouser = str_replace(' ', '%20', $usuario->foto);
                                $fotouser = urlencode($fotouser);
                            ?>
                            <div class="col-md-2" style="text-align: center;">
                                <div class="usuario-imagen seleccion" data-title="<?php if(!empty($usuario->correo_corporativo)) echo $usuario->correo_corporativo; else echo $usuario->correo_personal; ?>" style="background-image: url({{ url('/') }}/images/file/clientes/{{ urldecode($fotouser) }})"></div>

                                <p>{{ $usuario->name }}</p>
                            </div>
                            @endforeach
                        </div>
                    </div> 
                    <li><label class="card-header">Otros Usuarios</label></li> 
                    <?php 
                        $usuariOtros = App\User::all();
                        foreach ($usuariOtros as $key) {
                            $valUser = false;
                            foreach ($userResponsables as $userResponsable){
                                if ($userResponsable == $key->id) {
                                    $valUser = true;
                                }
                            }
                            if (!$valUser) {
                                $nombre   = explode(" ",$key->nombres);
                                $apellido = explode(" ",$key->apellidos); 
                                if (isset($nombre[2])) {
                                    $key->name = $nombre[0] ." ". $apellido[0];
                                }else{
                                    $key->name = $nombre[0] ." ". $apellido[0];
                                }

                                $fotouser = str_replace(' ', '%20', $key->foto);
                                $fotouser = urlencode($fotouser);
                                ?>
                                <div class="col-md-2" style="text-align: center;">
                                    <div class="usuario-imagen" data-title="<?php if(!empty($key->correo_corporativo)) echo $key->correo_corporativo; else echo $key->correo_personal; ?>" style="background-image: url({{ url('/') }}/images/file/clientes/{{ urldecode($fotouser) }})"></div>

                                    <p>{{ $key->name }}</p>
                                </div>
                                <?php
                            }
                        }
                    ?>
                         

                    <li><label class="card-header">Agregar mas</label></li> 
                    <div class="form-group" id="productosMas"> 
                        <div class="col-md-6 rowmail" style="text-align: center;">
                            <input type="hidden" id="count" value="1">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button id="btn1" class="btn btn-primary add-more margentop" type="button" onclick="addRowProducts()">+</button>
                                </span>
                               <input type="email" name="emails[]" class="form-control" placeholder="Ingresar correo electrónico">                    
                            </div>
                        </div> 
                    </div>
                </ul>
                <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enviar correos</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$("body").on("click",".usuario-imagen",function(e){
    if(!$(this).hasClass("seleccion")){
        $(this).addClass("seleccion");
    }else{
        $(this).removeClass("seleccion");
    }
    i=0;
    contenido='';
    $("#elementos").html(contenido);
    $(".seleccion").each(function(){
      contenido+='<input type="hidden" name="emails[]" value="'+$(this).attr("data-title")+'">';
      i++;
    });
    setTimeout(function(){
        $("#elementos").html(contenido);
    },500);    
});

$(function() {
    i=0;
    contenido='';
    $("#elementos").html(contenido);
    $(".seleccion").each(function(){
      contenido+='<input type="hidden" name="emails[]" value="'+$(this).attr("data-title")+'">';
      i++;
    });
    setTimeout(function(){
        $("#elementos").html(contenido);
    },500);
})

addRowProducts = function(){
    $count = $('#count').val();
    $count++;

    $("#productosMas").find('.btn')
        .prop('onclick',null)
        .off('click')
        .removeClass('add-more btn-primary')
        .addClass('btn-danger')
        .text('-')
        .on('click', function(){
            var element = $(this).parents('.hijo');
            element.removeClass('rollIn').addClass('hinge');
            setTimeout(function(){ element.remove(); }, 1000); 
        });

    $("#productosMas").prepend(`
        <div class="col-md-6 rowmail animated rollIn" style="text-align: center;">
            <input type="hidden" id="count" value="1">
            <div class="input-group">
                <span class="input-group-btn">
                    <button id="btn`+$count+`" class="btn btn-primary add-more margentop" type="button" onclick="addRowProducts()">+</button>
                </span>
               <input type="email" name="emails[]" class="form-control" placeholder="Ingresar correo electrónico">                    
            </div>
        </div>`);
    $("#productosMas").find('#count').val($count); 
}
</script>
@endsection