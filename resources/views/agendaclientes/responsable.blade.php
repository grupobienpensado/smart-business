<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Asignar Responsable')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/component.css" />
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">

<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
    text-align: left;
}
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
}
.table td{
    border: 0 !important;
}
.usuario-imagen{
    height: 100px;
    width: 100px;
    margin: 5px;
    display: inline-block;
    background-size: cover;
    border-radius: 100%;
    border: 0.5px solid;
}

.usuario-imagen2{
    height: 80px;
    width: 80px;
    margin: 5px;
    display: inline-block;
    background-size: cover;
    border-radius: 100%;
    border: 0.5px solid;
}
/*li{
    margin-left: 35px;
    list-style: none;
}*/
.seleccion{
    opacity: 0.2;
    border: 0.5px solid #15ce23;
}

label.card-header {
    background: #051d60;
    color: aliceblue;
    font-weight: 500;
    width: 100%;
    text-align: center;
    margin-bottom: 30px;
    margin-top: 30px;
}

.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}






.custom-file-upload-hidden {
    display: none;
    visibility: hidden;
    position: absolute;
    left: -9999px;
}
.custom-file-upload {
    display: block;
    width: auto;
    font-size: 16px;
    margin-top: 30px;
    label {
        display: block;
        margin-bottom: 5px;
    }
}

.file-upload-wrapper {
    position: relative; 
    margin-bottom: 5px;
    //border: 1px solid #ccc;
}
.file-upload-input {
    color: #333;
    font-size: 16px;
    padding: 11px 17px; 
    border: none;
    float: left; /* IE 9 Fix */
}

.file-upload-button {
    cursor: pointer;
    display: inline-block;
    color: #fff;
    font-size: 16px;
    text-transform: uppercase;
    padding: 11px 20px;
    border: none;
    margin-left: -1px;
    float: left;
    background-color: #1e92af;
}
.margentop{
    margin-top: 23px !important;
}
</style>
    <div class="container-fluid animated flipInX">
      <div class="row">

        <div class="col-md-12 panel-view">
            <div class="pull-right">
                <div class="btn-group">
                  <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
                </div>
              </div>
            <form method="POST" action="{{ url('responsablessave') }}"  enctype="multipart/form-data">
            {{ csrf_field() }}
            <div id="elementos"></div>
              <input type="hidden" name="id" value="{{$id}}">
                <div class="panel-title">
                    <h2>Agregar observaciones a los clientes visitantes</h2>
                </div>
                @php $i=0; @endphp
                @foreach($visitantes as $visia)
                    @if($visia->agenda_clientes==$id)
                        @php
                        $cliente=App\Cliente::findOrFail($visia->cliente);

                        $fotoclient = str_replace(' ', '%20', url('/')."/images/file/clientes/".$cliente->foto);
                        @endphp
                        <div class="col-md-12">
                            <div class="col-md-1">
                                <div class="usuario-imagen2" style="background-image: url({{ urldecode($fotoclient) }})"></div>
                            </div>
                            <input type="hidden" name="visitantes[{{$i}}][id]" value="{{$visia->id}}">
                            <div class="col-md-11 form-group">
                            <label>{{$cliente->tratamiento." ".$cliente->nombres.' '.$cliente->apellidos.", ".$cliente->cargo}}</label>
                                <textarea class="form-control" rowspan="2" name="visitantes[{{$i}}][observaciones]" placeholder="Escriba aqui algunas observaciones para esta visita sobre {{$cliente->nombres.' '.$cliente->apellidos}}" required></textarea>
                            </div>
                        </div>
                        @php $i++; @endphp
                    @endif
                @endforeach
                <hr>
                <div class="panel-title">
                    <h2>Asignar responsables a las actividades</h2>
                </div>
                <ul>
                    @foreach($actividades as $actividad)
                    @if($id==$actividad->agenda_clientes)
                        <li><label class="card-header">{{ucfirst($actividad->descripcion)}}</label></li>
                        <div class="col-md-12">
                            <div class="row">                                                            
                                @foreach($usuarios as $usuario)
                                <?php 
                                    $nombre   = explode(" ",$usuario->nombres);
                                    $apellido = explode(" ",$usuario->apellidos); 
                                    if (isset($nombre[2])) {
                                        $usuario->name = $nombre[0] ." ". $apellido[0];
                                    }else{
                                        $usuario->name = $nombre[0] ." ". $apellido[0];
                                    }

                                    $fotouser = str_replace(' ', '%20', $usuario->foto);
                                    $fotouser = urlencode($fotouser);
                                ?>
                                <div class="col-md-2" style="text-align: center;">
                                    <div class="usuario-imagen" data-title="{{$actividad->id}}" name="{{$usuario->id}}" style="background-image: url({{ url('/') }}/images/file/clientes/{{ urldecode($fotouser) }})"></div>

                                    <p>{{ $usuario->name }}</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @endforeach
                    
                </ul>

                <label class="card-header">Adjuntar archivo</label>
                <input type="hidden" name="id" value="{{$id}}">

                <div class="form-group" id="productosMas">
                    <div class="row animated hijo rollIn" id="rowProductos1">
                        <div class="col-10">
                            <input type="hidden" id="count" value="1">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button id="btn1" class="btn btn-primary add-more margentop" type="button" onclick="addRowProducts()">+</button>
                                </span>
                               <div class="custom-file-upload">
                                    <input type="file" class="file" name="file-1[]" multiple />
                                </div>                     
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-essi pull-right" style="display: none"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </form>
        </div>
      </div>
    </div>
@endsection

@section('scripts')

<!--<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>-->

<!--<script src="{{url('/')}}/js/material/upload-file/js/custom-file-input.js"></script>-->

<script type="text/javascript">
$("body").on("click",".usuario-imagen",function(e){
    if(!$(this).hasClass("seleccion")){
        $(this).addClass("seleccion");
    }else{
        $(this).removeClass("seleccion");
    }
    i=0;
    contenido='';
    $("#elementos").html(contenido);
    $(".seleccion").each(function(){
      contenido+='<input type="hidden" name="datos['+i+'][usuario]" value="'+$(this).attr("name")+'"><input type="hidden" name="datos['+i+'][actividad]" value="'+$(this).attr("data-title")+'">';
      i++;
    })
    setTimeout(function(){
        $("#elementos").html(contenido);
    },500);

    if(i>0){
        $(".btn-essi").show();
    }else{
        $(".btn-essi").hide();
    }
})

addRowProducts = function(){
    $count = $('#count').val();
    $count++;

    $("#productosMas").find('.btn')
        .prop('onclick',null)
        .off('click')
        .removeClass('add-more btn-primary')
        .addClass('btn-danger')
        .text('-')
        .on('click', function(){
            var element = $(this).parents('.hijo');
            element.removeClass('rollIn').addClass('hinge');
            setTimeout(function(){ element.remove(); }, 1000); 
        });
    $("#productosMas").prepend(`<div class="row hijo input-appendable-wrapper animated rollIn"  id="rowProductos`+$count+`">
            <div class="col-10">
                <div class="input-group input-group-appendable">
                    <span class="input-group-btn">
                        <button id="btn`+$count+`" class="btn add-more btn-primary margentop" type="button" onclick="addRowProducts()">+</button>
                    </span>
                     <div class="custom-file-upload">
                        <!--<label for="file">File: </label>--> 
                        <input type="file" class="file`+$count+`" name="file-1[]"  />
                    </div>                 
                </div>
            </div>
        </div>`);
    $("#productosMas").find('#count').val($count);
    $('.file'+$count).customFile();
}






//Reference: 
//https://www.onextrapixel.com/2012/12/10/how-to-create-a-custom-file-input-with-jquery-css3-and-php/
;(function($) {

          // Browser supports HTML5 multiple file?
          var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
              isIE = /msie/i.test( navigator.userAgent );

          $.fn.customFile = function() {

            return this.each(function() {

              var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
                  $wrap = $('<div class="file-upload-wrapper">'),
                  $input = $('<input type="text" class="file-upload-input" />'),
                  // Button that will be used in non-IE browsers
                  $button = $('<button type="button" class="file-upload-button"><i class="fa fa-cloud-upload"></i></button>'),
                  // Hack for IE
                  $label = $('<label class="file-upload-button" for="'+ $file[0].id +'"><i class="fa fa-cloud-upload"></i></label>');

              // Hide by shifting to the left so we
              // can still trigger events
              $file.css({
                position: 'absolute',
                left: '-9999px'
              });

              $wrap.insertAfter( $file )
                .append( $file, $input, ( isIE ? $label : $button ) );

              // Prevent focus
              $file.attr('tabIndex', -1);
              $button.attr('tabIndex', -1);

              $button.click(function () {
                $file.focus().click(); // Open dialog
              });

              $file.change(function() {

                var files = [], fileArr, filename;

                // If multiple is supported then extract
                // all filenames from the file array
                if ( multipleSupport ) {
                  fileArr = $file[0].files;
                  for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                    files.push( fileArr[i].name );
                  }
                  filename = files.join(', ');

                // If not supported then just take the value
                // and remove the path to just show the filename
                } else {
                  filename = $file.val().split('\\').pop();
                }

                $input.val( filename ) // Set the value
                  .attr('title', filename) // Show filename in title tootlip
                  .focus(); // Regain focus

              });

              $input.on({
                blur: function() { $file.trigger('blur'); },
                keydown: function( e ) {
                  if ( e.which === 13 ) { // Enter
                    if ( !isIE ) { $file.trigger('click'); }
                  } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                    // On some browsers the value is read-only
                    // with this trick we remove the old input and add
                    // a clean clone with all the original events attached
                    $file.replaceWith( $file = $file.clone( true ) );
                    $file.trigger('change');
                    $input.val('');
                  } else if ( e.which === 9 ){ // TAB
                    return;
                  } else { // All other keys
                    return false;
                  }
                }
              });

            });

          };

          // Old browser fallback
          if ( !multipleSupport ) {
            $( document ).on('change', 'input.customfile', function() {

              var $this = $(this),
                  // Create a unique ID so we
                  // can attach the label to the input
                  uniqId = 'customfile_'+ (new Date()).getTime(),
                  $wrap = $this.parent(),

                  // Filter empty input
                  $inputs = $wrap.siblings().find('.file-upload-input')
                    .filter(function(){ return !this.value }),

                  $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

              // 1ms timeout so it runs after all other events
              // that modify the value have triggered
              setTimeout(function() {
                // Add a new input
                if ( $this.val() ) {
                  // Check for empty fields to prevent
                  // creating new inputs when changing files
                  if ( !$inputs.length ) {
                    $wrap.after( $file );
                    $file.customFile();
                  }
                // Remove and reorganize inputs
                } else {
                  $inputs.parent().remove();
                  // Move the input so it's always last on the list
                  $wrap.appendTo( $wrap.parent() );
                  $wrap.find('input').focus();
                }
              }, 1);

            });
          }

}(jQuery));

$('input[type=file]').customFile();

</script>
@endsection