<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Listado de oportunidades perdidas')

@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
<style type="text/css">
tamano{width:14%;}
.tamano-col{width:8%!important;}
.tooltip-inner{white-space:pre-wrap;}
.unalinea{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;}
.mas-pequeño{line-height:1.5;font-size:small;color:#636363;font-weight:400;display:block!important;margin:0!important;}
.centrar-vertical{vertical-align:middle!important;}
.justify{text-align:justify;}
.dt-buttons button {height: 3.7rem;margin: 0;padding: 0.5em 1em;}
</style>  
    <div class="container-fluid animated slideInDown">
      <div class="row">
        <div class="col-sm-12  panel-view">
          <div class="contenido">
            <div class="pull-right form-inline">
              <div class="btn-group">

              </div>
              <div class="btn-group">

                <a href="{{ url('oportunidades') }}" class="btn btn-essi btn-sm"><i class="fa fa-list" aria-hidden="true"></i> Oportunidades vigentes</a>
                <a href="{{ url('ventas') }}" class="btn btn-info btn-sm">
                  <i class="fa fa-list" aria-hidden="true"></i>
                  Oportunidades vendidas
                </a>
              </div>
            </div>

            <h3>Oportunidades perdidas  ({{ count($datos) }} registros - <span id="subtotalpro"></span>)</h3>

            <hr>
            <div class="table-responsive">
              <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                <thead>
                  <tr>
                    <th class="centrado tamano-col">Item</th>
                    <th class="centrado tamano-col">Codigo</th>
                    <th class="centrado">Empresa<br>Ciudad</th>
                    <th class="centrado tamano-col">Productos</th>
                    <th class="centrado tamano-col">Valor Oportunidad</th>
                    <th class="centrado tamano-col">Responsable</th>
                    <th class="centrado tamano-col">Pais</th>
                    <th class="centrado tamano-col">Fecha identificación</th>
                    <th class="centrado tamano-col">Fecha perdida</th>
                    <th class="centrado tamano-col">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; ?>
                  @foreach($datos as $dato)
                  <tr class="text-center dato">
                    <td class="centrar-vertical"><p class="mas-pequeño"></p></td>
                    <td class="centrar-vertical"><a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">OP{{ $dato["oportunidad"]->id }}</p></a></td>

                    <td class="centrar-vertical">
                      <a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato["oportunidad"]->empresa }} <br> {{ $dato["oportunidad"]->ciudad }}</p></a>
                    </td>

                    <td class="unalinea centrar-vertical" style="text-align: center;">
                    <?php
                      $productosRes = App\OportunidadProducto::where("oportunidad_id",$dato["oportunidad"]->id)->get();
                      $tootilProductos = 0;
                      foreach ($productosRes as $res) {
                        try {
                          if (!empty($res->producto)) {
                            if (is_numeric($res->producto)) {
                              $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                            }else{
                              $producto->name = "Otro";
                            }
                            if (is_numeric($res->referencia)) {
                              $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                            }else{
                              $referencia->referencia = "Estandar";
                            }
                            echo '<p class="mas-pequeño">'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.',</p>';
                          }
                        } catch (Exception $e) {}
                      }
                    ?>
                  </td>

                    <td class="unalinea centrar-vertical">
                      <a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                        <p class="mas-pequeño">
                      <?php
                        try {
                          $otro = $dato["historial"]->filter(function ($value, $key) {
                            return $value->key == "val_pesos";
                          });
                          $valpesosoportunidad = $otro->pop();
                          if(isset($valpesosoportunidad->value)){
                            echo "$ ".$valpesosoportunidad->value;
                          }else{
                            echo "$ 0";
                          }


                        } catch (Exception $e) { }
                      ?>
                        </p>
                      </a>
                    </td>

                    <td class="centrar-vertical">
                      <a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                        <p class="mas-pequeño">
                      <?php
                        $otro = $dato["historial"]->filter(function ($value, $key) {
                          return $value->key == "responsable";
                        });
                        $casi = $otro->pop();
                        if(isset($casi->value)){
                          $nombreU = App\User::find($casi->value);
                          if (isset($nombreU)) {
                            $nombre   = explode(" ",$nombreU->nombres);
                            $apellido = explode(" ",$nombreU->apellidos);
                            echo $nombre[0] ." ". $apellido[0];
                          }else{
                            echo $casi->value;
                          }
                        }
                      ?>
                        </p>
                      </a>
                    </td>

                    <td class="centrar-vertical">
                      <a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                        <p class="mas-pequeño">{{ $dato["oportunidad"]->pais}}</p>
                      </a>
                    </td>

                    <td class="centrar-vertical">
                      <a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                        <p class="mas-pequeño">
                      <?php
                        $otro = $dato["historial"]->filter(function ($value, $key) {
                          return $value->key == "fecha_ff";
                        });
                        $casi = $otro->pop();
                        if (isset($casi->value)) {echo date($casi->value);}
                      ?>
                        </p>
                      </a>
                    </td>
                    <td class="centrar-vertical">
                      <a href="{{ url('veroportunidadperdida/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                        <p class="mas-pequeño">
                        <?php
                          $fecha_cerrada = Illuminate\Support\Facades\DB::select('SELECT `updated_at` FROM `historial_oportunidades` WHERE `oportunidad_id` = '.$dato['oportunidad']->id.' AND `key` LIKE "ciclo_venta" ORDER BY `updated_at` DESC LIMIT 0,1');
                          $dt3 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $fecha_cerrada[0]->updated_at);
                          echo $dt3->format('Y-m-d');
                        ?>
                        </p>
                      </a>
                    </td>
                    <td class="text-center centrar-vertical">
                      <a href="{{ url('/') }}/veroportunidadperdida/{{ $dato['oportunidad']->id }}" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver oportunidad">
                        <i class="fa fa-eye"></i>
                      </a>
                      <a href="{{ url('/') }}/oportunidad/edit/{{ $dato['oportunidad']->id }}" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar oportunidad">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>

    <script type="text/javascript" charset="utf-8">
      $(function () {
          $("[data-toggle='tooltip']").tooltip();

          $('#productos').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Filtrar por productos",
            productos: true
          });

          $('#responsable').select2({
          placeholder: "Seleccione una responsable",
          ciudad: true,
          tokenSeparators: [','],
          ajax: {
            dataType: 'json',
            url: '{{ url("vendedores") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function(data, page) {
              return {
                results: data
              };
            },
          }
        });

        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                <?php if($data['permiso_exportar'] == "Si"){ ?>
                'csv',
                'excel',
                <?php } ?>
                {
                    init: function(api, node, config) {
                       $(node).removeClass('dt-button');
                    },
                    className: 'btn btn-secondary mr-2 rounded',
                    text: window.location.href.indexOf("oportunidadesperdidas-historico/1") > -1? 'Año actual' : 'Histórico',
                    action: function ( e, dt, node, config ) {
                        if(window.location.href.indexOf("oportunidadesperdidas-historico/1") > -1){
                            window.location.replace("/oportunidadesperdidas");
                        }else{
                            window.location.replace("/oportunidadesperdidas-historico/1");
                        }
                    }
                }
        ],
          language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ oportunidades perdidas",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ oportunidades perdidas",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 oportunidades perdidas",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          },
          order: [ [8,'asc'] ],
        });

        var t = $('#example').DataTable();

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        var suma = 0;
        $('#example tr.dato').each(function(){
          var cadena = $(this).find('td').eq(4).text();
          cadena = cadena.replace("$", "");
          cadena = cadena.replace(/\./g, "");
          cadena = cadena.replace(/\,/g, "");
          cadena = cadena.replace(/ /g, "");
          suma += parseInt(cadena||0,10) //numero de la celda 3
        });
        //console.log("suma ",suma);
        suma = suma.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        suma = suma.split('').reverse().join('').replace(/^[\,]/,'');
        //console.log("suma ",suma);
        $('#subtotalpro').html("$ "+suma);
      });
    </script>
@endsection
