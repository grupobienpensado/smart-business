@extends('template.app')
@section('title', 'Admin-sistema')

@section('content')
<link href="/css/administrador/administradorsistema.css" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<input type="hidden" id="url_base" value="{{url('/')}}">
{{csrf_field()}}
<div class="container">
    <div class="loader-cylon" style="display: none;"></div>
    <!--MENU-->
    <div class="row">
        <div class="col-md-12" id="fill_canvas"></div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row menu margin-menu-l mt-4">
                <div class="col-sm-12 col-md-3 active" id="list_oportunidades" onclick="list_oportunidades('list_oportunidades') ">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Oportunidades</h6>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3" id="list_negociosCerrados" onclick="list_negociosCerrados('list_negociosCerrados')" >
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Negocios cerrados</h6>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3" id="list_negociosPerdidos" onclick="list_negociosPerdidos('list_negociosPerdidos')" >
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Negocios perdidos</h6>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3" id="list_Bitacora" onclick="list_Bitacora('list_Bitacora')" >
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Bitacora</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row menu margin-menu-r mt-4">
                <div class="col-sm-12 col-md-3" id="list_PlanTrabajo" onclick="list_PlanTrabajo('list_PlanTrabajo')" >
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Plan de trabajo</h6>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 " id="list_Pendientes" onclick="list_Pendientes('list_Pendientes')" >
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Gestión Pendientes</h6>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3" id="list_Datos" onclick="list_Datos('list_Datos')">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Datos</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Filtro-->
    <div class="row">
        <div class="col-md-12">
            <div class="row filtro mt-5 margin-filtro-l margin-filtro-r">
                <div class="col-sm-12 col-md-4 filtro-new">
                    <input class="form-control fecha-filtro" type="text" name="fecha_ff" id="fecha_ff" placeholder="fecha inicio" required>
                </div>
                <div class="col-sm-12 col-md-4 filtro-new">
                    <input class="form-control fecha-filtro" type="text" name="fecha_oc" id="fecha_oc" placeholder="fecha fin" required>
                </div>
                <div class="col-sm-12 col-md-4 btn-filtrar">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Aplicar Filtros</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--CONTENIDO-->
    <div class="row">
        <!--Oportunidades-->
        <div class="col-md-12 mt-5 e-identificadas">
        </div>
        <!--Negocios Cerrados -->
        <div class="col-md-12 mt-5 e-cerrados no-display">

        </div>
        <!--Negocios Perdidos -->
        <div class="col-md-12 mt-5 e-perdidos no-display">

        </div>
        <!--Bitacora-->
        <div class="col-md-12 mt-5  e-bitacora no-display">

        </div>
        <!--Plan de Trabajo-->
        <div class="col-md-12 mt-5 e-planTrabajo no-display">

        </div>
        <!-- Gestión de Pendientes-->
        <div class="col-md-12 mt-5 e-pendientes no-display">

        </div>
        <!--Datos-->
        <div class="col-md-12 mt-5 e-datos no-display">
            <div class="row ml-5 mr-5">
                <!--Actualización de Datos-->
               <div class="col-md-6 border-datos ">
                    <div class="row e-datos-actualizacion">

                    </div>
                </div>
                <!--Registros Actuales-->
                <div class="col-md-6 ">
                     <div class="row ml-3 bg-white border-datos2 e-datos-actuales">

                    </div>
                </div>
            </div>
        </div>
        <!--Gastos-->
    <!--<div class="col-md-12 mt-5">
            Costo de venta
            <div class="row ml-5 mr-5 border-datos">
                <div class="col-md-12">
                    <p class="h4 color-title-pen">Costo Venta</p>
                </div>
                <div class="col-md-12">
                    <div class="row margen-costo mb-4">
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row bg-gastos h-100">
                                <div class="col-md-12">
                                    <p class="h3 color-white">ESSI</p>
                                    <p class="h3 color-white">42%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row bg-gastos-gray h-100">
                                <div class="col-md-12">
                                    <img class="img-circle img-size-modal mt-3" src="{{url('/')}}/images/file/clientes/595ba3a89f142.png">
                                    <p class="h3 color-title-pen">15%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row bg-gastos-gray h-100">
                                <div class="col-md-12">
                                    <img class="img-circle img-size-modal mt-3" src="{{url('/')}}/images/file/clientes/595ea6fc8ec24.png">
                                    <p class="h3 color-title-pen">15%</p>
                                </div>
                            </div>
                        </div>
                       <div class="col-md-2 border-right-costo mt-4">
                            <div class="row bg-gastos-gray h-100">
                                <div class="col-md-12">
                                    <img class="img-circle img-size-modal mt-3" src="{{url('/')}}/images/file/clientes/595eab55da4a8.png">
                                    <p class="h3 color-title-pen">15%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row bg-gastos-gray h-100">
                                <div class="col-md-12">
                                    <img class="img-circle img-size-modal mt-3" src="{{url('/')}}/images/file/clientes/599f4a1d9ef24.png">
                                    <p class="h3 color-title-pen">15%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row bg-gastos-gray h-100">
                                <div class="col-md-12">
                                    <img class="img-circle img-size-modal mt-3" src="{{url('/')}}/images/file/clientes/599f4fe9ced08.png">
                                    <p class="h3 color-title-pen">15%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            Cumplimiento Presupuestal
            <div class="row ml-5 mr-5 mt-4 border-datos">
                <div class="col-md-12">
                    <p class="h4 color-title-pen">Cumplimiento Presupuestal</p>
                </div>
                <div class="col-md-12">
                    <div class="row margen-costo2 mb-4">
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row h-100 shadown-presupuesta">
                                <div class="col-md-12 bg-white border-presupuestal-up">
                                    <p class="h3 color-title-pen">Oscar</p>
                                </div>
                                <div class="col-md-12 bg-costo-item border-presupuestal-down">
                                    <p class="h3 color-white">15%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row h-100 shadown-presupuesta">
                                <div class="col-md-12 bg-white border-presupuestal-up">
                                    <p class="h3 color-title-pen">Mayra</p>
                                </div>
                                <div class="col-md-12 bg-costo-item border-presupuestal-down">
                                    <p class="h3 color-white">30%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row h-100 shadown-presupuesta">
                                <div class="col-md-12 bg-white border-presupuestal-up">
                                    <p class="h3 color-title-pen">Silvia</p>
                                </div>
                                <div class="col-md-12 bg-costo-item border-presupuestal-down">
                                    <p class="h3 color-white">30%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row h-100 shadown-presupuesta">
                                <div class="col-md-12 bg-white border-presupuestal-up">
                                    <p class="h3 color-title-pen">Johana</p>
                                </div>
                                <div class="col-md-12 bg-costo-item border-presupuestal-down">
                                    <p class="h3 color-white">15%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 border-right-costo mt-4">
                            <div class="row h-100 shadown-presupuesta">
                                <div class="col-md-12 bg-white border-presupuestal-up">
                                    <p class="h3 color-title-pen">Diego</p>
                                </div>
                                <div class="col-md-12 bg-costo-item border-presupuestal-down">
                                    <p class="h3 color-white">10%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- Titulo lateral -->
    <div class="row title-side" style="display: none;">
        <div class="col-md-12">
            <p class="h1 color-content-g texto-vertical">Oportunidades Identificadas</p>
        </div>
    </div>
</div>

<!--Modal - Ver Oportunidad Identificada  -->
<div class="modal fade bd-viewoportunidad-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-round">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Plazo para cierre de oportunidades</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <img src="/images/icons/essi_logo.png" class="logo-essi">
            </div>
            <div class="modal-body ">
                <div class="row">
                    <div class="col-md-12 e-plazocierre">
                        <div class="row margen-modal">
                            <div class="col-md-4">
                                <div class="row bg-blue mr-0-5 border-round-btn-modal">
                                    <div class="col-md-12">
                                        <p class="h5 color-white">Corto Plazo</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row bg-blue-medio mr-0-5 border-round-btn-modal">
                                    <div class="col-md-12">
                                        <p class="h5 color-white">Mediano Plazo</p>
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row bg-blue-alto border-round-btn-modal">
                                    <div class="col-md-12">
                                        <p class="h5 color-white">Largo Plazo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!--Grafica dona plan de trabajo-->
<script src="{{url('/')}}/js/administrador/amcharts.js"></script>
<script src="{{url('/')}}/js/administrador/pie.js"></script>
<script src="{{url('/')}}/js/administrador/export.min.js"></script>
<link rel="stylesheet" href="{{url('/')}}/css/administrador/export.css" type="text/css" media="all" />
<script src="{{url('/')}}/js/administrador/light.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="{{url('/')}}/js/administrador/administradorsistema.js"></script>
<script>
</script>
@endsection
