@extends('template.app')
@section('title', 'Proveedores')

@section('content')
<link rel="stylesheet" href="{{url('/')}}/css/ferias/botonessuperioresferias.css" type="text/css" media="all" />
<style>
    .inform{
        text-align: left !important;
    }
</style>
<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <!--<a href="{{ url('calendario/feria') }}" class="btn btn-info btn-sm">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Calendario de ferias
        </a>
        <a href="{{ url('feriasperdidas') }}" class="btn btn-danger btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias perdidas
        </a>
        <a href="{{ url('feriasposibles') }}" class="btn btn-warning btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Posibles ferias
        </a>
        <a href="{{ url('crearproveedores') }}" class="btn btn-success btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Crear Proveedor
        </a>
        <a href="{{ url('feria/patrocinadores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Patrocinadores
        </a>-->
        <a href="{{url('feria/patrocinadores')}}" class="btn btn-sm btn-patrocinador">
            Patrocinadores | {{$boton['patrocinadores']}}
        </a>
        <a href="{{url('proveedores')}}" class="btn btn-sm btn-proveedor">
            Proveedores | {{$boton['proveedores']}}
        </a>
        <a href="{{url('ferias/menu')}}" class="btn btn-sm btn-feria">
            Feria
        </a>
          <a <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>href="{{ url('crearproveedores') }}"<?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para crear un proveedor')" <?php } ?> class="btn btn-success btn-sm text-white">
                <i class="fa fa-list" aria-hidden="true"></i>
                Crear Proveedor
            </a>
      </div>
      <h2>Listado de proveedores</h2>
    </div>
</div>

<div class="card animated slideInDown" style="margin-bottom: 30px;">
    <div class="card-block">
        <div class="table-responsive" id="lista-mostrar">
          <table id="example" width="100%" class="table table-striped table table-striped table-bordered display">
            <thead>
              <tr>
                <th class="centrado">Item</th>
                <th class="centrado">Logo</th>
                <th class="centrado">Nombre</th>
                <th class="centrado">Ubicacion</th>
                <th class="centrado">comentario</th>
                <th class="centrado">Responsable</th>
                <th class="centrado">Creada</th>
                <th class="centrado">Editada</th>
                <th class="centrado">Acciones</th>
              </tr>
            </thead>
            <tbody>
                @foreach($proveedores as $proveedor)
                <tr>
                    <td>
                        {{$i+1}}
                    </td>
                    <td>
                        @if($proveedor->logo != '')
                        <img style="width:50px;" src="{{url('/')}}/storage/ferias/proveedores/{{$proveedor->logo}}">
                        @else
                        <img style="width:50px;" src="{{url('/')}}/storage/ferias/conferencista/noImagen.png">
                        @endif
                    </td>
                    <td>
                        {{$proveedor->nombre}}
                    </td>
                    <td>
                        <?php $u=explode('%%',$proveedor->ubicacion); $pais = Illuminate\Support\Facades\DB::select('SELECT * FROM `paises` WHERE `id` = "'.$u[0].'"'); $departamento = Illuminate\Support\Facades\DB::select('SELECT * FROM `estados` WHERE `id` = "'.$u[1].'"'); $ciudad = Illuminate\Support\Facades\DB::select('SELECT * FROM `ciudades` WHERE `id` = "'.$u[2].'"');  ?>
                        {{$pais[0]->name}}, {{$departamento[0]->name}}, {{$ciudad[0]->name}}
                    </td>
                    <td>
                        {{$proveedor->comentario}}
                    </td>
                    <td>
                        {{$proveedor->responsable}}
                    </td>
                    <td>
                        {{ date("d/m/Y (h:i:s A)",strtotime($proveedor->created_at)) }}
                    </td>
                    <td>
                        {{ date("d/m/Y (h:i:s A)",strtotime($proveedor->updated_at)) }}
                    </td>
                    <td>
                        <a href="#" class="btn btn-success btn-sm ver_proveedor" id="ver-{{$proveedor->id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver Proveedor">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        <a <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>href="/editarproveedor/{{$proveedor->id}}"<?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar un proveedor')" <?php } ?> class="btn btn-warning btn-sm text-white" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Proveedor">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="btn btn-danger btn-sm <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>eliminar_proveedor<?php } ?>" id="el-{{$proveedor->id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar Proveedor" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?> onclick="no_permiso('Usted no tiene permisos para eliminar un proveedor')" <?php } ?> >
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
                @php $i++; @endphp
                @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>

<!-- Ver -->

<div class="modal fade ver-proveedor-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top: 10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Proveedor <em class="nombre-proveedor"></em></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="main-content">
					<!-- NAV TABS -->
					<ul class="nav nav-tabs nav-tabs-custom-colored tabs-iconized">
						<li class="active"><a href="#profile-tab" data-toggle="tab"><i class="fa fa-user"></i> Perfil</a></li>
					</ul>
					<!-- END NAV TABS -->
					<div class="tab-content profile-page">
						<!-- PROFILE TAB CONTENT -->
						<div class="tab-pane profile active" id="profile-tab">
							<div class="row">
								<div class="col-md-3">
									<div class="user-info-left">
										<img id="image_proveedor" src="assets/img/profile-avatar.png" alt="Profile Picture" style="max-width: 133px" />
										<h2><a class="nombre-proveedor">Jonathan Smith</a> <i class="fa fa-circle green-font online-icon"></i><sup class="sr-only">online</sup></h2>
										<div class="contact">
											<a href="#" class="btn btn-block btn-custom-primary" id="creado"><i class="fa fa-plus"></i> Creado</a>
											<a href="#" class="btn btn-block btn-custom-secondary" id="editado"><i class="fa fa-pencil"></i> Editado</a>
											<ul class="list-inline social">
												<li><a href="#" title="Facebook"><i class="fa fa-facebook-square"></i></a></li>
												<li><a href="#" title="Twitter"><i class="fa fa-twitter-square"></i></a></li>
												<li><a href="#" title="Google Plus"><i class="fa fa-google-plus-square"></i></a></li>
											</ul>
										</div><br>
<!--                                        <img id="image_contacto" src="assets/img/profile-avatar.png" alt="Profile Picture" style="max-width: 133px" />-->
										<h2><a class="contacto">Jonathan Smith</a> <i class="fa fa-circle green-font online-icon"></i><sup class="sr-only">online</sup></h2>
                                        <div class="contact">
											<a href="#" class="btn btn-block btn-custom-secondary"><i class="fa fa-male"></i> Responsable</a>
										</div>
									</div>
								</div>
								<div class="col-md-9 inform">
									<div class="user-info-right">
										<div class="basic-info">
											<h3><i class="fa fa-square"></i> Informacion Basica</h3>
											<p class="data-row">
												<span class="data-name">Nombre</span>
												<span class="data-value nombre-proveedor">jonasmith</span>
											</p>
											<p class="data-row">
												<span class="data-name">Ubicación</span>
												<span class="data-value" id="ubicacion"></span>
											</p>
											<p class="data-row">
												<span class="data-name">Comentario</span>
												<span class="data-value" id="comentario">Male</span>
											</p>
										</div>
										<div class="contact_info">
											<h3><i class="fa fa-square"></i> Informacion de contacto</h3>
											<p class="data-row">
												<span class="data-name">Responsable</span>
												<span class="data-value" id="responsable">me@jonasmith.com</span>
											</p>
											<p class="data-row">
												<span class="data-name">Dirección</span>
												<span class="data-value" id="direccion">(1800) 221 - 876543</span>
											</p>
											<p class="data-row">
												<span class="data-name">Telefono</span>
												<span class="data-value" id="telefono">Riverside City 66, 80123 Denver<br />Colorado</span>
											</p>
                                            <p class="data-row">
												<span class="data-name">Celular</span>
												<span class="data-value" id="celular">Riverside City 66, 80123 Denver<br />Colorado</span>
											</p>
                                            <p class="data-row">
												<span class="data-name">Correo</span>
												<span class="data-value" id="correo">Riverside City 66, 80123 Denver<br />Colorado</span>
											</p>
										</div>
										<div class="about">
											<h3><i class="fa fa-square"></i> Descripción</h3>
											<p id="descripcion">Dramatically facilitate proactive solutions whereas professional intellectual capital. Holisticly utilize competitive e-markets through intermandated meta-services. Objectively.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END PROFILE TAB CONTENT -->
					</div>
				</div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
    $('[data-toggle="tooltip"]').tooltip();

    $('body').on('click','.eliminar_proveedor',function(){
        id=$(this).attr('id');
        id=id.replace('el-','');
       swal({
          title: 'Esta seguro?',
          text: "Va a eliminar un proveedor!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar'
        }).then(function () {
           $.ajax({
                url: "{{url('/')}}/eliminarproveedor/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal(
                        'Eliminado!',
                        e.msj,
                        'success'
                      ).then(function () {
                        parent.window.location.reload(true);
                    });
                }
            });
        })
    });
    $('body').on('click','.ver_proveedor',function(){
        id=$(this).attr('id');
        id=id.replace('ver-','');
        id=parseInt(id);
        $('.ver-proveedor-modal').modal('show');
        $.ajax({
            url: "{{url('/')}}/verproveedor/"+id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('.nombre-proveedor').html(e.dato.nombre);
                if((e.dato.logo) == '' || (e.dato.logo) == null){
                    $('#image_proveedor').attr('src','{{url("/")}}/storage/ferias/conferencista/noImagen.png');
                }else{
                    $('#image_proveedor').attr('src','{{url("/")}}/storage/ferias/proveedores/'+e.dato.logo);
                }
                $('#creado').html('<i class="fa fa-plus"></i> '+formato(e.dato.created_at));
                $('#editado').html('<i class="fa fa-pencil"></i> '+formato(e.dato.updated_at));
//                if((e.dato.foto_responsable) == '' || (e.dato.foto_responsable) == null){
//                    $('#image_contacto').attr('src','{{url("/")}}/storage/ferias/conferencista/noImagen.png');
//                }else{
//                    $('#image_contacto').attr('src','{{url("/")}}/storage/ferias/proveedores/'+e.dato.foto_responsable);
//                }
                $('.contacto').html(e.dato.responsable);
                $('#ubicacion').html(e.ubicacion);
                $('#comentario').html(e.dato.comentario);
                $('#responsable').html(e.dato.responsable);
                $('#direccion').html(e.dato.datos_contacto_direccion);
                $('#telefono').html(e.dato.datos_contacto_telefono);
                $('#celular').html(e.dato.datos_contacto_celular);
                $('#correo').html(e.dato.datos_contacto_correo);
                $('#descripcion').html(e.dato.descripcion);
            }
        });
    });

function formato(texto){
  fecha_completa=texto.split(' ');
  solo_fecha=fecha_completa[0].split('-');
  solo_hora=fecha_completa[1].split(':');
  fecha_real=solo_fecha[2]+'/'+solo_fecha[1]+'/'+solo_fecha[0];
  if(parseInt(solo_hora[0])>=13){
      hora=parseInt(solo_hora[0]) - 12;
     fecha_real=fecha_real+' '+hora+':'+solo_hora[1]+':'+solo_hora[2]+' P.M';
  }else{
      fecha_real=fecha_real+' '+solo_hora[0]+':'+solo_hora[1]+':'+solo_hora[2]+' A.M';
  }
  return fecha_real;
}
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
