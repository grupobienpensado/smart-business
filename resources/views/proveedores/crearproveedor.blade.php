@extends('template.app')
@section('title', 'Crear proveedor')

@section('content')
<link href="{{ url('/') }}/css/ferias/crearproveedor.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('calendario/feria') }}" class="btn btn-info btn-sm">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Calendario de ferias
        </a>
        <a href="{{ url('feriasperdidas') }}" class="btn btn-danger btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias perdidas
        </a>
        <a href="{{ url('feriasposibles') }}" class="btn btn-warning btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Posibles ferias
        </a>
        <a href="{{ url('proveedores') }}" class="btn btn-info btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Proveedores
        </a>
      </div>
    </div>
</div>
<div class="main-header">
    <h2>Proveedores</h2>
    <em>Crear</em>
</div>
<div class="card animated slideInDown" style="margin-bottom: 30px;">
    <div class="card-block">
        <input type="hidden" id="URL" value="{{url('/')}}">
        <form id="formulario_basico" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12 col-logo">
                    <div class="dropzone logo" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                      <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                    </div>
                </div>
                <div class="col-md-6 datos">
                    <label for="nombre">Nombre: </label>
                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" >
                </div>
                <div class="col-md-6 datos">
                    <label for="tags">Tags del producto <em>(Ingrese una palabra y luego un enter para separarla)</em></label><br>
                    <input type="text" value="" id="tags" name="tags_producto" data-role="tagsinput">
                </div>
                <div class="col-md-4 datos">
                    <label for="pais">Pais</label>
                    <div class="row fila-pais">
                        <div class="col-md-10">
                            <input type="text" id="pais_mostrar" class="form-control pais_mostrar" value="" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil cambiar-pais" aria-hidden="true" title="Cambiar de pais"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="pais" name="pais" list="lista_paises" placeholder="Seleccione un pais" >
                    <datalist id="lista_paises">
                      @foreach($paises as $pais)
                      <option value="{{$pais->id}}" label="{{$pais->name}}">
                      @endforeach
                    </datalist>
                </div>
                <div class="col-md-4">
                    <label for="departamento">Departamento <em>(Estado)</em></label>
                    <div class="row fila-departamento">
                        <div class="col-md-10">
                            <input type="text" id="departamento_mostrar" class="form-control departamento_mostrar" value="" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil cambiar-departamento" aria-hidden="true" title="Cambiar de departamento"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control no-cursor" id="departamento" name="departamento" list="lista_departamentos" placeholder="Seleccione un pais" readonly>
                    <datalist id="lista_departamentos">

                    </datalist>
                </div>
                <div class="col-md-4">
                    <label for="departamento">Ciudad <em>(Municipio)</em></label>
                    <div class="row fila-ciudad">
                        <div class="col-md-10">
                            <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" value="" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil cambiar-ciudad" aria-hidden="true" title="Cambiar de ciudad"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control no-cursor" id="ciudad" name="ciudad" list="lista_ciudades" placeholder="Seleccione un pais" readonly>
                    <datalist id="lista_ciudades">

                    </datalist>
                </div>
                <div class="col-md-12 datos fila-comentario">
                    <label for="comentario">Comentario <em>Ejemplo (Es bueno o malo)</em></label>
                    <input type="text" class="form-control" id="comentario" name="comentario" placeholder="comentario" >
                </div>
                <div class="col-md-12 datos mt-4">
                    <label for="comentario">Descripción</label>
                    <textarea class="form-control" id="descripcion" name="descripcion" rows="5" placeholder="Descripción" ></textarea>
                </div>

                <div id="reponsables_container" class="container p-0 m-0">

                <h3 class="text-center text-muted text-capitalize">Responsables</h3>
                <div class="btn-group actions mt-4" role="group">
                  <a class="clone btn btn-success text-white">Añadir</a>
                  <a class="remove btn btn-danger text-white">Eliminar</a>
                </div>


                <div id="responsables-row-1" class="ProveRespon">
                <div class="col-md-6 datos mt-4">
                    <input type="text" class="form-control" name="responsable[nombre][]" placeholder="Nombre Responsable" >
                </div>
                <div class="col-md-6 datos mt-4">
                    <input type="text" class="form-control" name="responsable[cargo][]" placeholder="Cargo Responsable" >
                </div>
                <div class="col-md-6 datos mt-4">
                    <input type="text" class="form-control" name="responsable[telefono][]" placeholder="Telefono Responsable">
                </div>
                <div class="col-md-6 datos mt-4">
                    <input type="text" class="form-control" name="responsable[celular][]" placeholder="Celular Responsable">
                </div>
                <div class="col-md-12 datos mt-4">
                    <input type="email" class="form-control" name="responsable[correo][]" placeholder="Correo Responsable" >
                    <hr class="inner-separator">
                </div>

                </div>

                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-success guardar_proveedor"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{url('/')}}/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/crearproveedor.js"></script>
@endsection
