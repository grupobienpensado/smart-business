@extends('template.app')
@section('title', 'Editar proveedor')

@section('content')
<link href="{{ url('/') }}/css/ferias/crearproveedor.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<style>
@if($dato->logo != "")
.dropzone:after {
    content: '' !important;
    }

@endif
@if($dato->foto_responsable == "")
.foto_responsable:after {
    content: 'Suelte aqui la imagen del responsable' !important;
}
@endif
</style>
<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('calendario/feria') }}" class="btn btn-info btn-sm">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Calendario de ferias
        </a>
        <a href="{{ url('feriasperdidas') }}" class="btn btn-danger btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias perdidas
        </a>
        <a href="{{ url('feriasposibles') }}" class="btn btn-warning btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Posibles ferias
        </a>
        <a href="{{ url('proveedores') }}" class="btn btn-info btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Proveedores
        </a>
      </div>
    </div>
</div>
<div class="main-header">
    <h2>Proveedores</h2>
    <em>Editar</em>
</div>
<div class="card animated slideInDown" style="margin-bottom: 30px;">
    <div class="card-block">
        <input type="hidden" id="URL" value="{{url('/')}}">
        <form id="formulario_basico" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$dato->id}}">
            <div class="row">
                <div class="col-md-12 col-logo">
                    <div class="dropzone logo" style="background-image: url('{{url('/')}}/storage/ferias/proveedores/{{$dato->logo}}'); text-align: center;" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                      <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                    </div>
                </div>
                <div class="col-md-6 datos">
                    <label for="nombre">Nombre: </label>
                    <input type="text" id="nombre" name="nombre" value="{{$dato->nombre}}" class="form-control" placeholder="Nombre" required="required">
                </div>
                <div class="col-md-6 datos">
                    <label for="tags">Tags del producto <em>(Ingrese una palabra y luego un enter para separarla)</em></label><br>
                    <input type="text" id="tags" name="tags_producto" value="{{$dato->tags_producto}}" data-role="tagsinput">
                </div>
                <div class="col-md-4 datos">
                    <label for="pais">Pais</label>
                    <div class="row fila-pais" style="display: block;">
                        <div class="col-md-10">
                            <input type="text" id="pais_mostrar" class="form-control pais_mostrar" value="{{$ubicacion[1]['pais']}}" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil cambiar-pais" aria-hidden="true" title="Cambiar de pais"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control" value="{{$ubicacion[0]['pais']}}" style="display: none;" id="pais" name="pais" list="lista_paises" placeholder="Seleccione un pais" required="required">
                    <datalist id="lista_paises">
                      @foreach($paises as $pais)
                      <option value="{{$pais->id}}" label="{{$pais->name}}">
                      @endforeach
                    </datalist>
                </div>
                <div class="col-md-4">
                    <label for="departamento">Departamento <em>(Estado)</em></label>
                    <div class="row fila-departamento" style="display: block;">
                        <div class="col-md-10">
                            <input type="text" id="departamento_mostrar" class="form-control departamento_mostrar" value="{{$ubicacion[1]['departamento']}}" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil cambiar-departamento" aria-hidden="true" title="Cambiar de departamento"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control" style="display: none;" id="departamento" value="{{$ubicacion[0]['departamento']}}" name="departamento" list="lista_departamentos" placeholder="Seleccione un pais" required="required">
                    <datalist id="lista_departamentos">
                         @foreach($departamentos as $departamento)
                        <option value="{{$departamento->id}}" label="{{$departamento->name}}"></option>
                        @endforeach
                    </datalist>
                </div>
                <div class="col-md-4">
                    <label for="departamento">Ciudad <em>(Municipio)</em></label>
                    <div class="row fila-ciudad" style="display: block;">
                        <div class="col-md-10">
                            <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" value="{{$ubicacion[1]['ciudad']}}" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil cambiar-ciudad" aria-hidden="true" title="Cambiar de ciudad"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control" style="display: none;" id="ciudad" name="ciudad" value="{{$ubicacion[0]['ciudad']}}" list="lista_ciudades" placeholder="Seleccione un pais" required="required">
                    <datalist id="lista_ciudades">
                        @foreach($ciudades as $ciudad)
                        <option value="{{$ciudad->id}}" label="{{$ciudad->name}}"></option>
                        @endforeach
                    </datalist>
                </div>
                <div class="col-md-12 datos fila-comentario">
                    <label for="comentario">Comentario <em>Ejemplo (Es bueno o malo)</em></label>
                    <input type="text" class="form-control" id="comentario" name="comentario" value="{{$dato->comentario}}" placeholder="comentario" required="required">
                </div>
                <div class="col-md-12 datos margen">
                    <label for="comentario">Descripción</label>
                    <textarea class="form-control" id="descripcion" name="descripcion" rows="8" placeholder="Descripción" required="required">{{$dato->descripcion}}</textarea>
                </div>

                <div class="col-md-6 datos margen">
                    <label for="responsable">Nombre Responsable</label>
                    <input type="text" class="form-control" id="responsable" value="{{$dato->responsable}}" name="responsable" placeholder="Nombre Responsable" required="required">
                </div>
                <div class="col-md-6 datos margen">
                    <label for="direccion">Dirección Responsable</label>
                    <input type="text" class="form-control" id="direccion" value="{{$dato->datos_contacto_direccion}}" name="datos_contacto_direccion" placeholder="Dirección Responsable" required="required">
                </div>
                <div class="col-md-6 datos margen">
                    <label for="telefono">Telefono Responsable</label>
                    <input type="text" class="form-control telefonofijo" id="telefono" value="{{$dato->datos_contacto_telefono}}" name="datos_contacto_telefono" placeholder="Telefono Responsable">
                </div>
                <div class="col-md-6 datos margen">
                    <label for="celular">Celular Responsable</label>
                    <input type="text" class="form-control telefonocelular" id="celular" value="{{$dato->datos_contacto_celular}}" name="datos_contacto_celular" placeholder="Celular Responsable">
                </div>
                <div class="col-md-12 datos margen">
                    <label for="celular">Correo Responsable</label>
                    <input type="email" class="form-control" id="correo" name="datos_contacto_correo" value="{{$dato->datos_contacto_correo}}" placeholder="Correo Responsable" required="required">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-success editar_proveedor"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{url('/')}}/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script type="text/javascript" src="{{ url('/') }}/js/proveedores/crearproveedor.js"></script>
@endsection
