@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Agenda Clientes')

@section('content')

<style type="text/css">
.alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img {
  
    border: solid 0px !important;
}
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
.row-2{
	padding: 50px;
}

.row-2 th{
	padding: .3rem !important;
}
.th{
	background-color: #eceeef;
	border-bottom: 2px solid #d2d3d4 !important;
}

.cuadro{
	width: 14.28%;
	min-width: 14.28%;
	background-color: #fff;
}
.minutero{
	text-align: right;
    border-bottom: 0.5px solid #ccc;
}
.actividad{
	margin: 2px;
    padding: 1px;
    text-align: center;
    border-radius: 5px;
    line-height: 1;
    color: #fff;
    z-index: 99;
}

.tr td{
	padding: 13px !important;
}
.tr textarea{
	margin: 5px;
}
.alert {
    -webkit-animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    margin-bottom: 10px;
}

.alert-dark {
    background-color: rgba(228, 104, 104, 0.9);
    border-color: rgba(0, 0, 0, 0.8);
    color: #fff;
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}

.form-control[readonly] {
    background-color: #fff;
    opacity: 1;
}
.modal-body .form-group.row.block{
	margin-top: 65px;
}
.logo-empresa1{
  width: 150px;
  height: 150px;
  background-size: cover;
  border: 0.5px solid;
  display: inline-block;
}
tr li, .lista{
  margin-left: 35px;
  line-height: 1;
}

.visita-imagen{
  border: 0.5px solid #ccc;
  height: 90px;
  width: 90px;
  display: inline-block;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 100%;
  text-align: center;
}
.inline-block{
  display: inline-block;
}
.relativo{
  position: relative;
}
.margen{
  margin: 10px;
  height: auto;
  width: 30%;
  border: 0.5px solid #ccc;
}
.datos{
  width: 100%;
  bottom: 0;
  display: block;
}
.nombre-persona{
    position: absolute;
    bottom: -5px;
    width: 100%;
    right: 2%;
    line-height: 0.8;
  
}
.nombre{
  position: absolute;
  bottom: 12px;
  width: 100%;
  right: 2%;
  font-weight: bold;
}
.info-profile{
  border-top: 0.5px solid #ccc;
  display: inline-block;
  padding: 5px;
}
.info-profile label{
  font-weight: bold;
}
.info-profile span{
    display: block;
    text-align: justify;
    letter-spacing: 0.3px;
    white-space: pre-line;
    line-height: 1;
}
.nav-tabs-space{
  width: 30% !important;
}
.tab-content{
  padding: 5px;
}
.nav>li>a {
    padding: 3px 15px 25px 15px !important;
}
.tab-pane label{
  width: 100%;
  text-align: left;
  font-size: 17px;
  font-weight: bold;
}
.tab-pane p{
  text-align: left;
  color: #000;
  font-weight: 300;
  white-space: pre-wrap;
  border-bottom: 0.5px solid #ccc;
}
</style>
@php


function fecha($str)
{
	$date = explode("-",$str);
	return $date[2] . "/". $date[1] ."/". $date[0];
}

$nuevafecha=strtotime($datos->fecha_inicio);
$domingo_siguiente = date ( 'Y-m-d' , $nuevafecha );
$hora="00:30";


$semana=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
@endphp
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/component.css" />
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" class="btn btn-sm btn-success acerca"><i class="fa fa-file" aria-hidden="true"></i> Acerca de la visita</a>
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
    	<div class="col-md-12 text-center inline-block">
        <div class="logo-empresa1" style="background-image: url('{{url('/')}}/images/file/empresas/principal/{{$empresa->logo}}')"></div>
    		<h3>Plan de Actividades</h3>
    		<h5>Del {{fecha($datos->fecha_inicio)." hasta el ".fecha($datos->fecha_final)}}</h5>
    	</div>
    	<div class="col-md-12 text-center inline-block">
        <h4>Clientes Visitantes</h4>
        <ul class="nav nav-tabs" role="tablist">                
          @php $i=0; $o=0; @endphp
          @foreach($visitantes as $visita)
          @if($visita->agenda_clientes==$id)
          @php 
          $i++;
          $usuario=App\Cliente ::findOrFail($visita->cliente);
          @endphp
            <li class="nav-tabs-space"><a href="#cliente{{$i}}" aria-controls="home" role="tab" data-toggle="tab"><div class="visita-imagen" name="{{$i}}" style="background-image: url('{{url('/')}}/images/file/clientes/{{$usuario->foto}}')"></div>
            <span class="nombre">{{$usuario->tratamiento." ".$usuario->nombres." ".$usuario->apellidos}}</span>
            <div class="nombre-persona">{{$usuario->cargo.", ".$usuario->profesion}}</div></a></li>
          @endif
          @endforeach
        </ul>
        <div class="tab-content">
        @foreach($visitantes as $visita)
          @if($visita->agenda_clientes==$id)
          @php  $o++; $usuario=App\Cliente ::findOrFail($visita->cliente); @endphp
          <div class="tab-pane fade in" id="cliente{{$o}}">
          @if($visita->observaciones!="")
            <div>
              <label>Observación sobre esta visita:</label>
              <p>{{$visita->observaciones}}</p>
            </div>
            @endif
          @if($usuario->perfil!="")
            <div>
              <label>Perfil:</label>
              <p>{{$usuario->perfil}}</p>
            </div>
            @endif
            @if($usuario->observaciones!="")
            <div>
              <label>Descripción:</label>
              <p>{{$usuario->observaciones}}</p>
            </div>
            @endif
          </div>
          @endif
          @endforeach
        </div>
      </div>
      <?php $files=false;
          foreach($archivos as $archivo){
           if($archivo->agenda_clientes==$id){
            $files=true;
           }
          }
        ?>
        @if($files)
        <div class="col-md-12 text-center inline-block">
        <h4>Archivos sobre esta agenda</h4>
          @foreach($archivos as $archivo)
            @if($archivo->agenda_clientes==$id)
                @php
                list($nombre,$ext)=explode(".", $archivo->archivo);
                if($ext=="jpg"||$ext=="JPG"){
                    $imagen="jpg.png";
                }elseif($ext=="png"||$ext=="PNG"){
                    $imagen="png.png";
                }elseif($ext=="DOC"||$ext=="doc"){
                    $imagen="word-logo.png";
                }elseif($ext=="pdf"||$ext=="PDF"){
                    $imagen="pdf-logo.png";
                }elseif($ext=="ppt"||$ext=="PPT"){
                    $imagen="ppt.png";
                }elseif($ext=="xls"||$ext=="XLS"||$ext=="XLSX"||$ext=="xlsx"){
                    $imagen="excel-logo.png";
                }else{
                    $imagen="file.png";
                }
                @endphp
                <div style="width: 5%; display: inline-block; margin: 15px;"><a href="{{url('/')}}/images/file/agenda/{{$archivo->archivo}}" target="_blank"><img src="{{url('/')}}/images/icons/{{$imagen}}" style="width: 64px !important;" class="delete"></a></div>
            @endif
            @endforeach
        </div>
        @endif
      <div class="row-2">
      	<table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
          <thead>
          	<tr>
          		<th></th>
              @php $p=0; @endphp 
              @for($i=date("w",strtotime($datos->fecha_inicio));$i<(date("w",strtotime($datos->fecha_inicio))+7);$i++)
              @php $o=$i-date("w",strtotime($datos->fecha_inicio)); @endphp
              @php
                $fecha2=strtotime("+".$o." Day",strtotime($datos->fecha_inicio));
                $dia_siguiente = date ( 'Y-m-d' , $fecha2 );
                $dias[$p]=$dia_siguiente;
              @endphp
	          	<th class="text-center th">{{$semana[date("w",strtotime($dia_siguiente))]}}</th>
              @php $p++; @endphp
              @endfor
          	</tr>
          </thead>
          <tbody>
          	@php $color=''; @endphp
          	@while($hora!="00:00")
          	<tr>
          		<td class="relativo minutero">
          			{{$hora}}
          		</td>
          		@for($i=0;$i<7;$i++)
          		@php $actividad=false; @endphp
          		<td class="cuadro" name="{{$dias[$i]}}" id="{{$hora}}">
	          			@php
		                  $nombre_actividad=''; 
		                  @endphp
	          			@foreach($descripciones as $value)
	          			@if($value->agenda_clientes==$id&&$value->fecha==$dias[$i]&&($value->hora_inicio<=$hora&&$value->hora_fin>=$hora))
	          				@php $actividad=true;
                    $nombre_actividad=$value->descripcion;
	          				$color=$value->color;
	          				$break;
	          				@endphp
	          			@endif
	          			@endforeach
	          			<div @if($actividad) class="actividad" style="background-color: {{$color}};" @endif>{{$nombre_actividad}}</div>
          		</td>
          		@endfor
          	</tr>
          	@php $nuevafecha=strtotime("+30 Minutes",strtotime($hora));
			       $hora = date ( 'H:i' , $nuevafecha ); @endphp
          	@endwhile
          </tbody>
      	</table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guardar_evento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Listado de Actividades <span id="fecha"></span></h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
	    <div class="form-group row block" >
       		<div class="col-md-12" id="lista-actividades">
       			
       		</div>
       </div>
    </div>    
    </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="archivos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
          <h4 class="modal-title" id="myModalLabel">Archivos Acerca De Esta Visita</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" >
          <form method="POST" action="{{ url('archivosagendaclientes') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$id}}">
            <div class=" form-group row">
                <div class="col-md-12" style="text-align: center;">
                  <div class="box">
                    <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} Archivos seleccionados" multiple />
                    <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Cargar Archivos&hellip;</span></label>
                  </div>
                </div>
            </div>
            <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
          </form>
        </div>    
      </div>
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="{{url('/')}}/js/material/upload-file/js/custom-file-input.js"></script>
<script type="text/javascript" charset="utf-8">

$("body").on("click",".acerca",function(e){
  $("#archivos").modal();
})
function ver_participantes(id){
		mo=$("#tr-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr-"+id).show("clip");
		}else{
			$("#tr-"+id).hide("clip");
		}
		
}
$("body").on("click",".cuadro",function(e){
  $("#guardar_evento").modal();
	$("#fecha").html($(this).attr("name"));
	fecha=$(this).attr("name");
  id=<?php echo $id; ?>;
	$("#lista-actividades").html('');
	setTimeout(function(e){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/consultaractividadesagenda/"+id, 
        cache: false,
        type: 'GET',
        data:{"fecha":fecha},
        success: function(e){ 
        	$("#lista-actividades").html(e.tabla);
        }
    });},2000)

})

$("body").on("click",".visita-imagen",function(e){
  id=$(this).attr("name");
  vd=$("#dato-"+id).is(":visible");
  if(!vd){
    $("#dato-"+id).show();
  }else{
    $("#dato-"+id).hide();
  }
})
</script>

@endsection