@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Presupuesto')

@section('content')

<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}
.table td{
    border: 0 !important;
}
form{
  width: 100%;
}
.btn{
  margin: 5px;
}
.alert {
    -webkit-animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    margin-bottom: 10px;
}

.alert-dark {
    background-color: #317d27;
    border-color: rgba(0, 0, 0, 0.8);
    color: #fff;
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}
td, td.text-center{
  line-height: 3 !important;
}
</style>
@php

@endphp
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-sm-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>Liquidador de comisiones oportunidad {{$oportunidad->titulo}}</h3>
			</div>
        <div class="col-md-12">
          <div class="alert alert-dark bpx" role="alert" style="display: none;">
            
        </div>
        </div>
        <form method="POST" action="{{ url('saveliquidacion') }}" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{$id}}">
        {{ csrf_field() }}
      	 <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th class="text-center">Aplica</th>
                 <th class="text-center">Descripción</th>
                 <th class="text-center">Valor</th>
                 <th class="text-center">observaciones</th>
               </tr>
             </thead>
             <tbody>
              <tr>
              <input type="hidden" name="datos[0][si]" value="no">
              <input type="hidden" name="datos[0][detalles]" value="Identificación de Oportunidad">
              <input type="hidden" name="datos[0][valor]" value="10" id="valor-0">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[0][si]" value="si" id="0"></td>
                <td>Identificación de Oportunidad</td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-0" name="datos[0][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[1][si]" value="no">
              <input type="hidden" name="datos[1][detalles]" value="Tipo de cliente">
              <input type="hidden" name="datos[1][valor]" value="10" id="valor-1">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[1][si]" value="si" id="1"></td>
                <td>Tipo de cliente </td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-1" name="datos[1][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[2][si]" value="no">
              <input type="hidden" name="datos[2][detalles]" value="Eficacia del forecast comercial">
              <input type="hidden" name="datos[2][valor]" value="15" id="valor-2">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[2][si]" value="si" id="2"></td>
                <td>Eficacia del forecast comercial</td>
                <td class="text-center">15 %</td>
                <td><textarea class="form-control control-2" name="datos[2][observaciones]" readonly="" rowspan="2" ></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[3][si]" value="no">
              <input type="hidden" name="datos[3][detalles]" value="Elaboración de la Oferta - Propuesta">
              <input type="hidden" name="datos[3][valor]" value="10" id="valor-3">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[3][si]" value="si" id="3"></td>
                <td>Elaboración de la Oferta - Propuesta </td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-3" name="datos[3][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[4][si]" value="no">
              <input type="hidden" name="datos[4][detalles]" value="Seguimiento y Gestión del negocio">
              <input type="hidden" name="datos[4][valor]" value="10" id="valor-4">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[4][si]" value="si" id="4"></td>
                <td>Seguimiento y Gestión del negocio </td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-4" name="datos[4][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[5][si]" value="no">
              <input type="hidden" name="datos[5][detalles]" value="Cierre de la Negociación">
              <input type="hidden" name="datos[5][valor]" value="10" id="valor-5">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[5][si]" value="si" id="5"></td>
                <td>Cierre de la Negociación </td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-5" name="datos[5][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[6][si]" value="no">
              <input type="hidden" name="datos[6][detalles]" value="Acompañamiento Ejecución">
              <input type="hidden" name="datos[6][valor]" value="15" id="valor-6">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[6][si]" value="si" id="6"></td>
                <td>Acompañamiento Ejecución </td>
                <td class="text-center">15 %</td>
                <td><textarea class="form-control control-6" name="datos[6][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[7][si]" value="no">
              <input type="hidden" name="datos[7][detalles]" value="Entrega y Liquidación">
              <input type="hidden" name="datos[7][valor]" value="10" id="valor-7">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[7][si]" value="si" id="7"></td>
                <td>Entrega y Liquidación</td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-7" name="datos[7][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>
              <tr>
              <input type="hidden" name="datos[8][si]" value="no">
              <input type="hidden" name="datos[8][detalles]" value="Recaudo Total de Cartera">
              <input type="hidden" name="datos[8][valor]" value="10" id="valor-8">
                <td class="text-center"><input type="checkbox" class="checkar" name="datos[8][si]" value="si" id="8"></td>
                <td>Recaudo Total de Cartera</td>
                <td class="text-center">10 %</td>
                <td><textarea class="form-control control-8" name="datos[8][observaciones]" readonly="" rowspan="2"></textarea></td>
              </tr>              
             </tbody>
           </table>
           <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
           <button type="button" onclick="openDocument()" class="btn btn-success pull-right"><i class="fa fa-info-circle" aria-hidden="true"></i> Ver Condiciones De Liquidación</button>
         </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Ver Información</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" id="cuerpo_comment">
        
    </div>
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" charset="utf-8">
direccion='{{url("/")}}';
total=0;
openDocument = function() {
  $("#upload").modal();
        dir = direccion+"/images/comision.docx";
        contenido = `<iframe src="http://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>`;
        $("#cuerpo_comment").html(contenido);
    }

    $("body").on("click",".checkar",function(e){
      validacion=false;
      num=$(this).attr("id");

      if($(this).prop("checked")){
        total=total+parseInt($("#valor-"+num).val());
        $(".control-"+num).removeAttr("readonly");
        $(".control-"+num).attr("required",true)
      }else{
        total=total-parseInt($("#valor-"+num).val());
        $(".control-"+num).attr("readonly",true);
        $(".control-"+num).removeAttr("required");
      }
      
      $(".checkar").each(function(){
        if($(this).prop("checked")){
           validacion=true;
        }
      })
      if(validacion){
        $(".bpx").html('<span style="font-weight:bold">Atención!</span> Su porcentaje de liquidación correspondiente a esta oportunidad es de: <span style="font-weight:bold">'+total+'% </span>')
        $(".bpx").show();
      }else{
        $(".bpx").hide();
      }
    })
</script>

@endsection