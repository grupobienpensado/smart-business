@extends('template.app')
@section('title', 'Proceso De Oportunidad')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/component.css" />
<link href="{{ url('/') }}/components/uploader/jquery.fileuploader.css" media="all" rel="stylesheet" type="text/css"/>

<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
<style type="text/css">
    .marco-flotando {
        width: 70%;
        margin-left: auto !important;
        margin-right: auto !important;
        margin-top: auto !important;
        margin-bottom: auto !important;
        text-align: left !important;
        border-radius: 20px;
        -moz-border-radius: 20px;
        -webkit-border-radius: 20px;
        border: 1px solid #ccc;
        overflow: hidden;
        padding-right: 30px;
    }

    .izquierdo {
        text-align: center;
        background: #051d60;
        border-radius: 20px;
        -moz-border-radius: 20px;
        -webkit-border-radius: 20px;
    }

    .izquierdo>h2 {
        color: #5fc2ff;
        font-size: 8em;
        text-align: center;
    }

    .izquierdo>p {
        margin-top: 25px;
        color: aliceblue;
        font-size: large;
        font-weight: 400;
        margin-left: 20px;
    }

    .derecho{        
        text-align: center;
    }

    p {
        font-weight: 400;
    }

    .pciclos{
        width: 100%;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        font-size: 9px;
    }

    .iconfiles{
        width: 60px;
        padding: 8px;
    }

    .imagesfile {
        display: grid;
    }

    .iclonciclo {
        width: 30px;
    }
    .contentimg{
        padding: 10px;
        margin: auto;
        display: inline-block;
    }
    .contenticons{
        margin: auto;
        display: inline-block;
    }
    .centro>h2{
        text-align: center;
    }

    .ciclos-ante {
        float: left;
        margin: 5px;
        border-radius: 20px;
        -moz-border-radius: 20px;
        -webkit-border-radius: 20px;
        border: 1px solid #ccc;
        padding: 8px 35px 0px;
    }

    .ciclos-ante>h2 {
        font-weight: 900;
        margin: 0;
        color: #ccc;
    }

    .gris{
        color: #ccc;
    }

    .sin-margin{
        margin: 0;
    }

    .conten-text {
        margin: 20px;
        margin-right: 70px;
        margin-bottom: -50px;
    }

    .fuera-border{
        width: 70%;
        margin-left: auto !important;
        margin-right: auto !important;
        margin-top: auto !important;
        margin-bottom: auto !important;
        text-align: left !important;
        border-radius: 20px;
        -moz-border-radius: 20px;
        -webkit-border-radius: 20px;
        border: 1px solid #ccc;
        overflow: hidden;
    }

    .comentRing {
        float: right;
    }

    .active>.ciclos-ante {
        background: #051d60;
    }

    .active>.ciclos-ante>h2 {
        color: #f5f5f5;
    }

    a:link   
    {   
     text-decoration:none;   
    } 

    a.ciclos-ante:hover {
        background: #5fc2ff !important;
    }

    .contentimg>li {
        float: left;
    }

    .contentimg>li>a:focus, .contentimg>li>a:hover {
        text-decoration: none;
        background-color: #051d60;
    }

    .conten-text>p>span {
        font-size: 4em;
        font-weight: 900;
        line-height: 0.5;
    }

    #ingresos .relativo{
        border-bottom: 0.5px solid #ccc;
        padding: 5px;
    }

    .img-commen2{
        width: 80px;
        height: 80px;
        float: left;
        margin-right: 15px;
        background-repeat: no-repeat;
        background-size: cover;
        border: 0.5px solid #ccc;
        border-radius: 100%;
    }

    .segunda-fecha{
        position: absolute;
        right: 30px;
        padding-top: 20px;
        font-size: 20px;
    }
    .primera-fecha{
        position: absolute;
        right: 30px;
        
    }

    .infor{
        height: 60px;
        vertical-align: middle;
        display: block;
        text-align: left;
    }
    .infor p{
        margin-bottom: 0;
    }

    .usuario-list{
        margin-top: 1.2rem;
        margin-bottom: 0;
        color: #000;
    }

    .clear{
        border-bottom: 0.5px solid #fff;
    }

    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    div.modal {
        height: 90%;
        top: 40px;
    }

    .modal-content {
        top: 40px;
    }

    .faltanteContinuardanger {
        color: #d9534f;
        font-weight: 600;
        top: 1px !important;
        position: absolute;
        right: 0px;
        width: 140px;
        text-align: center;
    }

    .faltanteContinuarwarning {
        color: #f0ad4e;
        font-weight: 600;
        top: 1px !important;
        position: absolute;
        right: 0px;
        width: 140px;
        text-align: center;
    }
</style>
<?php setlocale(LC_ALL, "es_CO.UTF-8"); ?>

<div class="widget">
    <ul class="nav nav-pills" role="tablist">
        <li><a href="{{ url('venta') }}/{{ $id }}"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
        <li><a href="{{ url('ventafileimportant') }}/{{ $id }}"><i class="fa fa-bar-chart"></i> Archivos</a></li>
        @if($data['permiso_apu_acceder']=="Si")<li><a href="{{ url('apu') }}/{{ $id }}"><i class="fa fa-line-chart"></i> APU</a></li>@endif
        <li class="active"><a href="#chart4" role="tab" data-toggle="tab"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
        @if($data['permiso_acta_acceder']=="Si")<li><a href="{{ url('actasreunion') }}/{{ $id }}"><i class="fa fa-line-chart"></i> Actas reunion</a></li>@endif
        <li><a href="/ver-oportunidadvendida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
        <li><a href="/venta/gestionesoportunidad/<?=$data['venta']->oportunidad?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-title">
                <h2>Ciclo de la oportunidad - {{ $dato->empresa }}/{{ $dato->ciudad }}</h2>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#transicion" role="tab" data-toggle="tab">Transición</a></li>
                <li><a href="#ingresos" role="tab" data-toggle="tab">Listado de ingresos</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="tab-content">    
    <div class="widget tab-pane fade in active" id="transicion">
        <div class="row">
            <div class="col-md-12">            
                <div class="marco-flotando">
                    <div class="row">
                        <div class="izquierdo col-md-4">
                            <p style="border-bottom: solid 1px;">
                                <?php 
                                    $cicloP = App\Ciclo::where('porcentaje', $historia->value)->get()->pop();
                                    echo $cicloP->nombre; 
                                ?>
                            </p>
                            <h2>{{ $historia->value }}%</h2>
                            <p style="border-top: solid 1px;">{{ strftime("%d %B %Y", strtotime($historia->updated_at)) }}</p>
                        </div> 
                        <div class="centro col-md-7">
                            <h2>Detalles del ciclo</h2>
                            <div>
                                <?php print $historia->descripcion; ?>                                                      
                            </div>
                            
                            <!--<?php $resCiclo = 0; foreach ($ciclos as $ciclo): ?>
                                <?php if ($ciclo->porcentaje > $historia->value && $resCiclo == 0):  
                                    $resCiclo= $ciclo->porcentaje;
                                    $dias   = (strtotime($historia->created_at)-strtotime(date("Y-m-d H:i:s")))/86400;
                                    $dias   = abs($dias); $dias = floor($dias);
                                    $dias   = $ciclo->dias - $dias;?>
                                    <?php if ($dias<0): 
                                        $dias = $dias * -1;?>
                                        <div class="faltanteContinuardanger">
                                            Plazo para avanzar al {{ $ciclo->porcentaje }}% {{ $dias }} dias vencidos 
                                        </div>
                                    <?php else: ?>
                                        <div class="faltanteContinuarwarning">
                                            Plazo para avanzar al {{ $ciclo->porcentaje }}% {{ $dias }} dias
                                        </div>
                                    <?php endif ?>                                     
                                <?php endif ?>
                            <?php endforeach ?>-->

                             
                            <div class="imagesfile">
                                <div class="contentimg"> 
                                    <?php 
                                        $archivos = App\Historial_oportunidades_archivo::where('historial_oportunidades', $historia->id)->get();
                                        foreach ($archivos as $archivo) {
                                            if (isset($archivo) && !empty($archivo)) {
                                                $extension   = explode(".",$archivo->archivo); 
                                                $extension   = $extension[1];
                                                if ($extension == "BMP" || $extension == "GIF" || $extension == "PNG" || $extension == "JPG" || $extension == "TIF" || $extension == "bmp" || $extension == "gif" || $extension == "png" || $extension == "jpg" || $extension == "tif") { ?>
                                                    <a data-gallery="" title="{{ $archivo->archivo }}" href="{{url('/')}}/images/file/oportunidades/{{ $archivo->archivo }}"><img src="{{ url('/') }}/images/iconciclos/images.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}"></a>
                                                <?php }elseif ($extension === "AVI" || $extension === "MOV" || $extension === "WMV" || $extension === "FLv" || $extension === "MP4" || $extension === "avi" || $extension === "mov" || $extension === "wmv" || $extension === "flv" || $extension === "mp4") { ?>
                                                        <img src="{{ url('/') }}/images/iconciclos/video.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openVideo('{{ $archivo->archivo }}')">                      
                                                <?php }elseif ($extension === "PDF" || $extension === "pdf") { ?>
                                                        <img src="{{ url('/') }}/images/iconciclos/pdf.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openDocument('{{ $archivo->archivo }}')">
                                                <?php }elseif ($extension === "OCX" || $extension === "ocx" || $extension === "DOC" || $extension === "doc") { ?>
                                                        <img src="{{ url('/') }}/images/iconciclos/word.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openDocument('{{ $archivo->archivo }}')">
                                                <?php }elseif ($extension === "XLSX" || $extension === "xlsx" || $extension === "XLSM" || $extension === "xlsm" || $extension === "XML" || $extension === "xml") { ?>
                                                        <img src="{{ url('/') }}/images/iconciclos/excell.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openDocument('{{ $archivo->archivo }}')">
                                                <?php }else{ ?>
                                                        <a href="{{url('/')}}/images/file/oportunidades/{{ $archivo->archivo }}"><img src="{{ url('/') }}/images/iconciclos/otros.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}"></a>
                                                <?php } 
                                            }
                                        }
                                    ?>                              
                                </div>
                            </div>
                        </div>               
                        <div class="derecho col-md-1 imagesfile">
                            <!--<div class="row contenticons">
                                <div class="col-ms-12 avanzar" name="{{ $historia->id }}" data-name="{{ $historia->value }}">
                                    <img class="iclonciclo" src="{{ url('/') }}/images/iconciclos/avanzar.png">
                                    <p class="pciclos">Avanzar</p>
                                </div>
                            </div>
                            <div class="row contenticons">
                                <div class="col-ms-12">
                                    <img class="iclonciclo atras" src="{{ url('/') }}/images/iconciclos/retroceder.png">
                                    <p class="pciclos">Retroceder</p>
                                </div>
                            </div>
                            <div class="row contenticons">
                                <div class="col-ms-12">
                                    <img class="iclonciclo eliminar" onclick="recargar({{$dato->id}})" src="{{ url('/') }}/images/iconciclos/perdida.png">
                                    <p class="pciclos">Perdida</p>
                                </div>
                            </div>-->
                            <div class="row contenticons">
                                <div class="col-ms-12">                                
                                    <img class="iclonciclo cargar" name="{{ $historia->id }}" src="{{ url('/') }}/images/iconciclos/archivos.png">
                                    <p class="pciclos">{{ count($archivos) }} Archivos</p>
                                </div>
                            </div>                        
                            <div class="row contenticons">
                                <div class="col-ms-12">
                                    <?php
                                        $comentarios=App\Historial_oportunidades_comentario::where('historial_oportunidades', $historia->id)->get(); 
                                    ?>
                                    <img class="iclonciclo comentar" name="{{ $historia->id }}" src="{{ url('/') }}/images/iconciclos/comentarios.png">
                                    <p class="pciclos">{{ count($comentarios) }} Comentarios</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="imagesfile">
                    <ul class="nav contentimg" role="tablist">
                        <?php foreach ($historias as $vhistoria): ?>
                            <li>
                                <a href="#panel{{ $vhistoria->id }}" class="ciclos-ante" role="tab" data-toggle="tab">
                                    <p class="sin-margin gris">
                                        <?php 
                                            $cicloP = App\Ciclo::where('porcentaje', $vhistoria->value)->get()->pop();
                                            echo $cicloP->nombre; 
                                        ?>
                                    </p>
                                    <h2>{{ $vhistoria->value }}%</h2>
                                    <p class="gris">{{ strftime("%d / %m / %Y", strtotime($vhistoria->created_at)) }}</p>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>

                <div class="imagesfile tab-content">
                    <?php foreach ($historias as $vhistoria): ?>
                        <div class="contentimg fuera-border tab-pane fade in" id="panel{{ $vhistoria->id }}"> 
                            <div class="conten-text">
                                <p><span>{{ $vhistoria->value }}%</span> {{ $vhistoria->descripcion }}</p>
                                <div class="imagesfile">
                                    <div class="contentimg">                                
                                        <?php 
                                            $archivos = App\Historial_oportunidades_archivo::where('historial_oportunidades', $vhistoria->id)->get();
                                            foreach ($archivos as $archivo) {
                                                if (isset($archivo) && !empty($archivo)) {
                                                    $extension   = explode(".",$archivo->archivo); 
                                                    $extension   = $extension[1];
                                                    if ($extension == "BMP" || $extension == "GIF" || $extension == "PNG" || $extension == "JPG" || $extension == "TIF" || $extension == "bmp" || $extension == "gif" || $extension == "png" || $extension == "jpg" || $extension == "tif") { ?>
                                                        <a data-gallery="" title="{{ $archivo->archivo }}" href="{{url('/')}}/images/file/oportunidades/{{ $archivo->archivo }}"><img src="{{ url('/') }}/images/iconciclos/images.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}"></a>
                                                    <?php }elseif ($extension === "AVI" || $extension === "MOV" || $extension === "WMV" || $extension === "FLv" || $extension === "MP4" || $extension === "avi" || $extension === "mov" || $extension === "wmv" || $extension === "flv" || $extension === "mp4") { ?>
                                                            <img src="{{ url('/') }}/images/iconciclos/video.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openVideo('{{ $archivo->archivo }}')">                      
                                                    <?php }elseif ($extension === "PDF" || $extension === "pdf") { ?>
                                                            <img src="{{ url('/') }}/images/iconciclos/pdf.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openDocument('{{ $archivo->archivo }}')">
                                                    <?php }elseif ($extension === "OCX" || $extension === "ocx" || $extension === "DOC" || $extension === "doc") { ?>
                                                            <img src="{{ url('/') }}/images/iconciclos/word.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openDocument('{{ $archivo->archivo }}')">
                                                    <?php }elseif ($extension === "XLSX" || $extension === "xlsx" || $extension === "XLSM" || $extension === "xlsm" || $extension === "XML" || $extension === "xml") { ?>
                                                            <img src="{{ url('/') }}/images/iconciclos/excell.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}" onclick="openDocument('{{ $archivo->archivo }}')">
                                                    <?php }else{ ?>
                                                            <a href="{{url('/')}}/images/file/oportunidades/{{ $archivo->archivo }}"><img src="{{ url('/') }}/images/iconciclos/otros.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="{{ $archivo->asname }}"></a>
                                                    <?php } 
                                                }
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="comentRing">
                                <?php
                                    $comentarios=App\Historial_oportunidades_comentario::where('historial_oportunidades', $vhistoria->id)->get(); 
                                ?>
                                <img class="iclonciclo comentar" name="{{ $vhistoria->id }}" src="{{ url('/') }}/images/iconciclos/comentarios.png">
                                <p class="pciclos">{{ count($comentarios) }} Comentarios</p>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>    

    <div class="widget tab-pane fade in" id="ingresos">
        <div class="row">
            <div class="col-md-12">
                <?php 
                    foreach ($ingresos as $ingreso): 
                        $user=App\User::find($ingreso->user_id);
                        if(isset($user->id)){
                ?>             
                <div class="relativo">
                    <div class="img-commen2" style="background-image: url(../images/file/clientes/{{$user->foto}})"></div>
                    <span class="primera-fecha">{{ strftime("%A, %d de %B de %Y", strtotime($ingreso->created_at)) }}</span>
                    <span class="segunda-fecha">
                        <i class="fa fa-clock-o"></i>
                        {{ strftime("%R", strtotime($ingreso->created_at)) }}
                    </span>
                    <div class="infor">
                        <h4 class="usuario-list">{{ucwords($user->nombres." ".$user->apellidos)}}</h4>
                        <p>IP {{$ingreso->ip}}</p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php } endforeach ?>
            </div>
        </div>
    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Cargar Archivos</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <form method="POST" action="{{ url('uploadfiles') }}" enctype="multipart/form-data">
               <input type="hidden" name="id" id="subir-id">
               {{ csrf_field() }}
               <div class=" form-group row">
                  <div class="col-md-12 text-center">
                     <div class="box">
                        <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} Archivos seleccionados" multiple />
                        <label for="file-1">
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                              <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                           </svg>
                           <span>Cargar Archivos&hellip;</span>
                        </label>
                     </div>
                  </div>
               </div>
               <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="vendido" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Oportunidad Vendida</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <form method="POST" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
               <input type="hidden" name="id" value="{{$dato->id}}">
               <input type="hidden" name="datos[0][value]" value="90">
               <p style="text-align: justify; color: #000; font-weight: 400">Esta a punto de pasar esta oportunidad a la fase de ventas, si desea continuar escriba por favor digite una descripcion, de lo contrario cierre la ventana actual</p>
               {{ csrf_field() }}
               <div class=" form-group row">
                  <div class="col-md-12">
                     <label for="observaciones" class="col-form-label">Observaciones</label>
                     <textarea class="form-control" name="datos[0][descripcion]" rows="2" placeholder="Por favor ingrese una observacion" required></textarea>
                  </div>
               </div>
               <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-send-o" aria-hidden="true"></i> Enviar</button>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="avanzar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Avanzar En El Ciclo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <form method="POST" id="formCicloSave" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
               <input type="hidden" name="id" value="{{$dato->id}}">
               <input type="hidden" name="id-etapa" id="etapa-siguiente">
               <input type="hidden" value="{{ $historia->value }}" id="porcen_actual">
               {{ csrf_field() }}
               <div class=" form-group row" id="ciclo_siguiente">
                  <div class="col-12">
                     <div id="output"></div>
                     <label for="ciclo_venta" class="col-form-label">Siguiente Ciclo</label>
                     <select name="ciclo_venta" class="form-control" id="ciclo_venta" required>
                        <option>Seleccione una opción</option>
                        @foreach($ciclos as $ciclo)
                        @if($ciclo->porcentaje>$historia->value&&$ciclo->porcentaje<=90)
                        <option value="{{$ciclo->porcentaje}}" data-image="{{ url('/')}}/{{$ciclo->imagen}}">{{$ciclo->nombre}}</option>
                        @endif
                        @endforeach
                     </select>
                  </div>
                  <div id="lista-obser" class="col-12">
                  </div>
                  <div id="loaders" style="display: none;">
                     <div class="loader"></div>
                     <small>Cargando Archivos...</small>
                  </div>
               </div>
               <button type="submit" class="btn btn-essi pull-right btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="atras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Regresar En El Ciclo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <form method="POST" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
               <input type="hidden" name="id-etapa" id="etapa-atras">
               <input type="hidden" name="id" value="{{$dato->id}}">
               {{ csrf_field() }}
               <div class=" form-group row">
                  <div class="col-md-12">
                     <label for="ciclo_venta2" class="col-form-label">Siguiente Ciclo</label>
                     <select name="datos[0][value]" class="form-control" id="ciclo_venta2" required>
                        <option>Seleccione una opción</option>
                        @foreach($ciclos as $ciclo)
                        @if($ciclo->porcentaje<$historia->value&&$ciclo->porcentaje!=0)
                        <option value="{{$ciclo->porcentaje}}" data-image="{{ url('/')}}/{{$ciclo->imagen}}">{{$ciclo->nombre}}</option>
                        @endif
                        @endforeach
                     </select>
                     <div class="form-group" style="border-bottom: 0.5px solid #ccc;padding-bottom: 1.5rem;">
                        <label for="observaciones" class="col-form-label">Observaciones</label>
                        <textarea class="form-control" name="datos[0][descripcion]" rows="2" placeholder="Por favor ingrese una observacion" required></textarea>
                     </div>
                  </div>
               </div>
               <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="comentar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Comentarios</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <div class=" form-group row">
               <div class="col-md-12">
                  <div class="div-commen">
                  </div>
               </div>
            </div>
            <form method="POST" action="{{ url('savecomments') }}" enctype="multipart/form-data">
               {{ csrf_field() }}
               <input type="hidden" name="id" id="id-commentario">
               <div class=" form-group row">
                  <div class="col-md-12" id="ciclo_siguiente">
                     <textarea rowspan="5" class="form-control" name="comentario" placeholder="Digite aqui un comentario"></textarea>
                  </div>
               </div>
               <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-send-o" aria-hidden="true"></i> Enviar</button>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="archivos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Archivo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <div class=" form-group row">
               <div class="col-md-12" id="modal_video">
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <form method="POST" action="{{ url('deletefile') }}" enctype="multipart/form-data">
               {{ csrf_field() }}
               <input type="hidden" name="id" id="delete-archivo">
               <input type="hidden" name="oportunidad" value="{{$dato->id}}">
               <div class=" form-group row">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-md btn-danger white" href="javascript:void(0)"><i class="fa fa-trash"></i> Eliminar Archivo</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal fade bd-example-modal-lg" id="documento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <iframe id="ver_pdf" src="http://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>

@endsection

@section('scripts')  
<script src="{{url('/')}}/js/material/upload-file/js/custom-file-input.js"></script>
<script src="{{ url('/')}}/js/plugins/VerticalTimeline/js/modernizr.custom.js"></script>
<script src="{{ url('/') }}/components/uploader/jquery.fileuploader.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>


<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>

<script type="text/javascript">
    function recargar(id){
        location.href="{{url('/')}}/oportunidadperdida/"+id;
    }

    function addUserPic (opt) {
        if (!opt.id) {
            return opt.text;
        }               
        var optimage = $(opt.element).data('image'); 
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
            '<span><img src="' + optimage + '" style="width: 20px;" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    }

    $("body").on("click",".comentar",function(e){
        $("#comentar").modal();
        $("#id-commentario").val($(this).attr("name"));
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
        var jqxhr = $.ajax({ 
            url: "{{ url('/') }}/vercomentarios", 
            cache: false,
            type: 'GET',
            data:{"id":$(this).attr("name")},
            success: function(e){ 
                $(".div-commen").html(e.cuerpo);
            }
        });
    });
    $("body").on("click",".avanzar",function(e){
        if($(this).attr("data-name")=="80"){
            $("#vendido").modal();
        }else{
            $("#avanzar").modal();
        }
        $("#ciclo_venta").select2({
            templateResult: addUserPic,
            templateSelection: addUserPic
        });
    })

    $("body").on("click",".atras",function(e){
        $("#atras").modal();
        $("#ciclo_venta2").select2({
            templateResult: addUserPic,
            templateSelection: addUserPic
        });
    })

    $("body").on("click",".cargar",function(e){
        $("#subir-id").val($(this).attr("name"));
        $("#upload").modal();
    })

    $("body").on("click",".delete",function(e){
        $("#delete-archivo").val($(this).attr("data-name"));
    })

    $("body").on("change","#ciclo_venta",function(){
        if($(this).val()!==90){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
            var jqxhr = $.ajax({ 
                url: "{{ url('/') }}/avanzaretapa", 
                cache: false,
                type: 'GET',
                data:{"actual":$("#porcen_actual").val(),"siguiente":$(this).val()},
                success: function(e){ 
                    $("#lista-obser").html(e.cuerpo);
                    $(".fileuploader").fileuploader({     
                      captions: {
                        button: function(options) { return 'Escoger ' + (options.limit == 1 ? 'File' : 'Archivos'); },
                        feedback: function(options) { return 'Escoger ' + (options.limit == 1 ? 'file' : 'archivos') + ' subir'; },
                        feedback2: function(options) { return options.length + ' ' + (options.length > 1 ? ' archivos' : ' el archivo fue') + ' elegidos'; },
                        drop: 'Suelta los archivos aquí para Cargar',
                        paste: '<div class="fileuploader-pending-loader"><div class="left-half" style="animation-duration: ${ms}s"></div><div class="spinner" style="animation-duration: ${ms}s"></div><div class="right-half" style="animation-duration: ${ms}s"></div></div> Pasting a file, click here to cancel.',
                        removeConfirmation: '¿Seguro que desea eliminar este archivo?',
                        errors: {
                          folderUpload: 'No puedes subir carpetas.'
                        }
                      }
                    });
                }
            });
        }
    })

    openVideo = function(nombre) {
        dir = "{{url('/')}}/images/file/oportunidades/"+nombre;
        video = `<div id="play_video_modal" class="caja">
                    <video id="Video1" src="`+dir+`" loop preload="auto">
                      Tu navegador no implementa el elemento <code>video</code>.
                    </video>
                    <div id="buttonbar">
                        <button class="video_button" id="restart" onclick="restart();"><i class="fa fa-repeat" aria-hidden="true"></i></button> 
                        <button class="video_button" id="rew" onclick="skip(-10)"><i class="fa fa-backward" aria-hidden="true"></i></button>
                        <button class="video_button" id="play" onclick="vidplay()"><i class="fa fa-play" aria-hidden="true"></i></button>
                        <button class="video_button" id="fastFwd" onclick="skip(10)"><i class="fa fa-forward" aria-hidden="true"></i></button>
                        <button class="video_button pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <button class="video_button pull-right" onclick="pantallaCompletaVideo()"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>
                    </div> 
                <div>`;

        $("#modal_video").html(video);
        $("#archivos").modal();
    }

    openDocument = function(nombre) {
        dir = "{{url('/')}}/images/file/oportunidades/"+nombre;
        /*contenido = `<iframe src="http://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:850px;" frameborder="0"></iframe>`;
        $("#modal_video").html(contenido);
        $("#archivos").modal();*/
        $('#documento').modal('show');
        $('#ver_pdf').attr('src',dir);
    }

               
               
    function vidplay() {
       var video = document.getElementById("Video1");
       var button = $("#play");

       if (video.paused) {
          video.play();
          button.html('<i class="fa fa-pause" aria-hidden="true"></i>');
       } else {
          video.pause();
          button.html('<i class="fa fa-play" aria-hidden="true"></i>');
       }
    }

    function restart() {
        var video = document.getElementById("Video1");
        video.currentTime = 0;
    }

    function skip(value) {
        var video = document.getElementById("Video1");
        video.currentTime += value;
    }  

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#archivos').on('hidden.bs.modal', function (e) {
            var video = document.getElementById("Video1");
            var button = $("#play");
              video.pause();
              button.html('<i class="fa fa-play" aria-hidden="true"></i>');
        });
    });
</script>
@endsection
