@extends('template.app')

@section('title', 'Ver Oportunidades')

@section('content')
<link rel="stylesheet" type="text/css" href="/components/3Dtimeline/css/style.css" />
<style type="text/css">
    .titulo-semana,.valor-dia{width:13%;display:-webkit-inline-box;font-weight:800}.titulo-semana{background:#999;min-height:16px;color:#fff;text-align:-webkit-center}.col-md-4.mesesano h2{text-align:center;color:#051d60;font-size:20px;text-transform:capitalize}.espacio-lleno,.valor-dia{text-align:-webkit-center}.col-md-4.mesesano{min-height:4cm}.valor-dia span{position:absolute;top:1px;right:2px}.valor-dia{background:#fbfdff;min-height:26px;color:#4c4c4c;font-size:8px;position:relative}.btn-sm,.form-control-sm{z-index:inherit!important}.content{text-align:-webkit-auto}form,html{font-size:13px!important}.note-popover{display:none}#form7{outline:0!important;box-shadow:0 0 10px #719ECE;font-weight:700;border:0;background-color:#dcdcdc;border-bottom:#051d60 solid}.parrafo,p.normalp{font-weight:400}.subtitulo_coment{margin-bottom:4px;font-size:small!important;color:#54575a}.soloprimer:first-letter,.uppercase,p:first-letter,span:first-letter{text-transform:uppercase!important}.capitalize{text-transform:lowercase!important;text-transform:capitalize!important}.text-inicial{font-variant:initial!important}.text-muted{text-transform:lowercase}.unalinea{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}.parrafo{color:#555;font-size:16px}.espacio-lleno{width:20px;min-width:20px;min-height:20px;position:absolute;background:#f0f8ff;font-size:10px}.row.calendarAno{position:relative;min-height:160px;margin-top:20px}.contenedor-calendar{position:absolute;height:90%;width:90%;top:50%;left:50%;transform:translate(-50%,-50%)}h2.mes-ano{position:absolute;top:-40px;font-size:15px;text-transform:capitalize}.tittle-a{background-color:#303F9F;color:#fff}.tittle-n{background-color:#1976D2;color:#fff}.bg-blue-f{background-color:#EEE}.coment{min-height:45px;line-height:25px}.b10{border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px 10px 10px 10px;border:0 solid #000}.b200{border-radius:200px;-moz-border-radius:200px;-webkit-border-radius:200px 200px 200px 200px;border:0 solid #000}.b-0{bottom:0}.modal-lg{min-width:1200px}.no-select:hover{filter:opacity(.5);cursor:pointer}.select{filter:opacity(.5);cursor:no-drop}.img-round{border-radius:200px;-moz-border-radius:200px;-webkit-border-radius:200px 200px 200px 200px;border:0 solid #000}
</style>
<div class="widget">
    <ul class="nav nav-pills" role="tablist">
        <li class="active"><a href="#chart1" role="tab" data-toggle="tab"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
        <li><a href="{{ url('ventafileimportant') }}/{{ $id }}"><i class="fa fa-bar-chart"></i> Archivos</a></li>
        @if($data['permiso_apu_acceder']=="Si")<li><a href="{{ url('apu') }}/{{ $id }}"><i class="fa fa-line-chart"></i> APU</a></li>@endif
        <li><a href="{{ url('ventaciclos') }}/{{ $id }}"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
        @if($data['permiso_actas_acceder']=="Si")<li><a href="{{ url('actasreunion') }}/{{ $id }}"><i class="fa fa-line-chart"></i> Actas reunion</a></li>@endif
        <li><a href="/ver-oportunidadvendida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
        <li><a href="/venta/gestionesoportunidad/<?=$data['venta']->oportunidad?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
    </ul>
    <div class="card-block">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <?php if($permiso_editar=="Si"){ ?>
                    <a href="{{ url('oportunidad/edit') }}/{{$oportunidad->id}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                    <?php } ?>
                    <a href="#titulobservaciones" id="comentarbtn" class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Comentar</a>
                </div>
            </div>
        </div>
        <div class="profile-empresa">
            <img src="
               @if(!empty($empresa->logo))
                  {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
               @else
                  http://via.placeholder.com/250x250/fff/948e8e?text=Logo
               @endif" class="img-fluid mx-auto d-block img-empresa">
        </div>
        <div class="row">
            <div class="col-6 col-md-6 centrado">
                <div class="hijo">
                    <h2 class="titulo">{{ $empresa->nombre }}</h2>
                    <p><i class="fa fa-map-marker"></i><span class="soloPri">
                        <?php
                            setlocale(LC_ALL, "es_CO.UTF-8");
                            $optval = \Illuminate\Support\Facades\DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` LIKE "sede" AND `oportunidad_id` = '.$oportunidad->id.' ORDER BY `id` DESC LIMIT 1');
                            if(isset($optval[0]->value)){
                                $sede = App\EmpresaSedes::findOrFail($optval[0]->value);
                                echo $sede->ciudad.", ".$sede->pais;
                            }else{
                                echo 'No existe sede';
                            }
                        ?>
                    </span></p>
                    <p class="text-muted capitalize">{{ $empresa->nombre_p_a_cargo }}/{{ $empresa->cargo }}</p>
                </div>
            </div>
            <div class="col-6 col-md-6 centrado" style="padding-top: 70px">
                <p class="subtitulo">
                    <img src="{{ url('/') }}/images/form/eye.png">
                    <span class="subtitulo2">Identificado el </span>
                    <?php
                        $otro = $datos->filter(function ($value, $key) {
                          return $value->key == "fecha_ff";
                        });

                        $respal = $otro;
                        $total = count($otro);
                    ?>
                        <input type="hidden" id="fecha_ff" value="{{ $respal }}"> @if($total>1)
                        <a href="#" onclick="llenarTabla('fecha_ff')" class="histrialModal">
                        <span class="badge badge-pill badge-danger bg-danger"><?php echo count($respal); ?></span>
                    </a> @endif
                        <?php
                        $casi = $otro->pop();
                        if (isset($casi->value)) {echo strftime("%d %B %Y", strtotime($casi->value));}
                    ?>

                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <img src="{{ url('/') }}/images/form/hand-shake.png">
                            <span class="subtitulo2">Por cerrar el </span>
                            <?php
                        $otro = $datos->filter(function ($value, $key) {
                          return $value->key == "fecha_oc";
                        });

                        $respal = $otro;
                        $total = count($otro);
                    ?>
                                <input type="hidden" id="fecha_oc" value="{{ $respal }}"> @if($total>1)
                                <a href="#" onclick="llenarTabla('fecha_oc')" class="histrialModal">
                        <span class="badge badge-pill badge-danger bg-danger"><?php echo count($respal); ?></span>
                    </a> @endif
                                <?php
                        $casi = $otro->pop();
                        if (isset($casi->value)) {echo strftime("%d %B %Y", strtotime($casi->value));}
                    ?>


                </p>
                <p class="subtitulo" style="text-transform: none;"><span class="subtitulo2">Cliente Responsable:</span> {{$data['cliente_responsable']}}</p>
            </div>
            <div class="col-md-12">
                <div class="hijo2 row justify-content-md-center" style="
                                                border: solid 1px rgba(186, 190, 193, 0.55);
                                                padding: 20px;
                                                margin-top: 20px;
                                                margin-bottom: 20px;">
                    <?php
                            //aqui son los eliominados corregir
                            $productosRes = App\OportunidadProducto::where("oportunidad_id",$oportunidad->id)->get();
                            $productosDel = App\OportunidadProducto::onlyTrashed()->where("oportunidad_id",$oportunidad->id)->get();
                            $htmlrespal = '';
                            foreach ($productosDel as $res) {
                            try {
                                if (isset($res->producto) && is_numeric($res->producto)) {
                                    //$imgproducto = App\Producto::where('name', '=', $res->producto)->pluck('foto');
                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                                    if (isset($res->referencia) && is_numeric($res->referencia)) {
                                        $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                                    }else{
                                        $referencia->referencia = "Estandar";
                                    }

                                    $htmlrespal .= '<tr>
                                      <td>'.$res->created_at.'</td>
                                      <td>'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.'</td>
                                    </tr>';
                                }
                            } catch (Exception $e) {
                                //$imgproducto = App\Producto::where('name', '=', $res)->pluck('foto');
                            }
                          }

                            //$respal = $productosDel;
                            $total = count($productosDel);
                        ?>
                        <input type="hidden" id="productos" value="{{ $htmlrespal }}">
                        <div class="col-3 col-sm-2 placeholder img-sedes">
                            <p class="subtitulo2">Oportunidad</p>
                            <p class="subtitulo uppercase">OP-{{ $oportunidad->id }}</p>
                        </div>
                        <?php
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "moneda";
                            });
                            $moneda = $otro->pop();

                          foreach ($productosRes as $res) {
                            try {
                                if (isset($res->producto) && is_numeric($res->producto)) {
                                    //$imgproducto = App\Producto::where('name', '=', $res->producto)->pluck('foto');
                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                                    if (isset($res->referencia) && is_numeric($res->referencia)) {
                                        $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                                    }else{
                                        $referencia->referencia = "Estandar";
                                    }
                                    $html = '<div class="col-3 col-sm-2 placeholder img-sedes">
                                    <div class="img-thumbnail img-sedes">
                                      <a href="">
                                        <img src="';
                                            if(!empty($referencia->foto) && $referencia->foto != Null){
                                                $html .= url('/')."/images/file/productos/".$referencia->foto;
                                            }else{
                                              $html .= 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Foto&w=100&h=100';
                                            }
                                            $html .='" style="max-width: 50%;" class="img-fluid  menu mx-auto d-block">
                                      </a>
                                    </div>
                                    <div class="text-muted centrado"><h6 style="margin-bottom: 0px;">'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.'</h6><p>'.$res->total.' <span class="uppercase">'.$moneda->value.'</span></p>';
                                        if($total>1){
                                            $html .= '<a href="#" onclick="llenarTabla(\'productos\')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger bg-danger">'.$total.'</span>
                                            </a>';
                                        }
                                    $html .= '</div>
                                    </div>';
                                    echo $html;
                                }
                            } catch (Exception $e) {
                                //$imgproducto = App\Producto::where('name', '=', $res)->pluck('foto');
                            }
                          }
                        ?>
                            <!-- fin, aqui termina el error es por los productos -->
                </div>
            </div>
            <div class="col-md-12">
                <div class="hijo2 row justify-content-md-center">
                    <div class="table-responsive">
                        <table class="table product-table">
                            <thead>
                                <tr class="tr-table">
                                    <th class="centrado">Ciclo de Venta</th>
                                    <th class="centrado">Responsable</th>
                                    <th class="centrado">Identifico</th>
                                    <th class="centrado">Freelance</th>
                                    <th class="centrado">Probabilidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado">
                                        <h2><strong>
                                        <?php
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "ciclo_venta";
                                            });

                                            $respal = $otro;
                                            $total = count($otro);
                                        ?>
                                        <input type="hidden" id="ciclo_venta" value="{{ $respal }}">
                                         @if($total>1)
                                        <a href="#" onclick="llenarTabla('ciclo_venta')" class="histrialModal">
                                            <span class="badge badge-pill badge-danger bg-danger"><?php echo $total; ?></span>
                                        </a>
                                        @endif
                                        <?php $casi = $otro->pop();  if (isset($casi->value)) {echo $casi->value."%";}?>

                                        </strong></h2>
                                        <p style="color: #555;">
                                            <?php
                                                switch ($casi->value) {
                                                    case 0:
                                                        echo "Perdida";
                                                        break;
                                                    case 10:
                                                        echo "Detección de oportunidad";
                                                        break;
                                                    case 20:
                                                        echo "Presentación y evaluación";
                                                        break;
                                                    case 30:
                                                        echo "Especificación de productos";
                                                        break;
                                                    case 40:
                                                        echo "Propuesta / Cotización";
                                                        break;
                                                    case 60:
                                                        echo "Negociacion / Revisió";
                                                        break;
                                                    case 80:
                                                        echo "Gestión de orden de compra";
                                                        break;
                                                    case 90:
                                                        echo "Vendida con OC o Contrato";
                                                        break;
                                                    case 100:
                                                        echo "Facturación y entrega";
                                                        break;
                                                }
                                            ?>
                                        </p>
                                    </td>
                                    <td class="centrar-div">
                                        <?php
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "responsable";
                                            });

                                            $respal = $otro;
                                            $total = count($otro);
                                            $otroRespal = [];
                                            foreach ($respal as $valf) {
                                                if (is_numeric($valf->value)) {
                                                    $uderidenti = App\UserTodos::find($valf->value);
                                                }else{
                                                    $uderidenti = App\UserTodos::where('name', '=', $valf->value)->get()->pop();
                                                }

                                                if(isset($uderidenti->name)){
                                                    $valf->value = $uderidenti->name;
                                                }else{
                                                    $valf->value = 'No tiene nombre'." o "."No tiene apellido";
                                                }

                                                $otroRespal[] = $valf;
                                            }
                                            $objRespal = json_encode($otroRespal);

                                        ?>
                                            <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
                                                <?php $casi = $otro->pop();
                                            if (is_numeric($casi->value)) {
                                                $uderidenti = App\UserTodos::find($casi->value);
                                            }else{
                                                $uderidenti = App\UserTodos::where('name', '=', $casi->value)->get();
                                                if(isset($uderidenti[0])){
                                                    $uderidenti = $uderidenti[0];
                                                }
                                            }
                                        ?>
                                                <?php if (isset($uderidenti) && !empty($uderidenti) ): ?>
                                                <a class="media-left waves-light">
                                                    <?php if(isset($uderidenti->foto)){ ?>
                                                    <img class="rounded-circle observaciones-img" src="/images/file/clientes/{{ $uderidenti->foto }}" alt="Usuario">
                                                    <?php }else{ ?>
                                                    <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/60x60" alt="Usuario">
                                                    <?php } ?>
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        <input type="hidden" id="responsable" value="{{ $objRespal }}">
                                                        <?php
                                                    if(isset($uderidenti->nombres) && isset($uderidenti->apellidos)){
                                                        $nombre   = explode(" ",$uderidenti->nombres);
                                                        $apellido = explode(" ",$uderidenti->apellidos);
                                                        echo $nombre[0] ." ". $apellido[0];
                                                    }else{
                                                        echo "Sin nombre o apellido";
                                                    }
                                                    ?>
                                                            @if($total>1)
                                                            <a href="#" onclick="llenarTabla('responsable')" class="histrialModal">
                                                        <span class="badge badge-pill badge-danger bg-danger"><?php echo $total; ?></span>
                                                    </a> @endif
                                                    </h4>
                                                    <p class="subtitulo">@if(isset($uderidenti->cargo)) {{ $uderidenti->cargo}} @else No hay cargo @endif</p>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                    </td>
                                    <td class="centrar-div">
                                        <!--Aqui esta el error-->
                                        <?php
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "identifico";
                                            });

                                            $respal = $otro;
                                            $total = count($otro);

                                            $otroRespal = [];
                                            if(isset($respal) && !empty($respal)){
                                                foreach ($respal as $valf) {
                                                    if (is_numeric($valf->value)) {
                                                        $uderidenti = App\UserTodos::find($valf->value);
                                                    }else{
                                                        $uderidenti = App\UserTodos::where('name', '=', $valf->value)->get()->pop();
                                                    }
                                                    if(isset($uderidenti) && !empty($uderidenti)){
                                                        $nombre   = explode(" ",$uderidenti->nombres);
                                                        $apellido = explode(" ",$uderidenti->apellidos);
                                                        if(isset($nombre[0]) && isset($apellido[0])){
                                                            $valf->value = $nombre[0] ." ". $apellido[0];
                                                        }else{
                                                            $valf->value = "No hay nombre!";
                                                        }
                                                        $valf->value = $nombre[0] ." ". $apellido[0];
                                                        $otroRespal[] = $valf;
                                                    }else{
                                                        $otroRespal[] = $valf;
                                                    }
                                                }
                                                $objRespal = json_encode($otroRespal);
                                            }
                                        ?>
                                            <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">

                                                <?php
                                            $casi = $respal->pop();
										if(isset($casi->value) && !empty($casi->value)){
                                            if (is_numeric($casi->value)) {
                                                $uderidenti = App\UserTodos::find($casi->value);
                                            }else{
                                                $uderidenti = App\UserTodos::where('name', '=', $casi->value)->get();
                                                $uderidenti = isset($uderidenti[0])?$uderidenti[0]:'';
                                            }
										}else{
											$otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "identifico_otro";
                                            });

                                            $respal = $otro;
                                            $total = count($otro);
                                            $casi = $otro->pop();
                                            $otroRespal = [];
                                            foreach ($respal as $valf) {
                                                if (is_numeric($valf->value)) {
                                                    $uderidenti = App\UserTodos::find($valf->value);
                                                }else{
                                                    $uderidenti = App\UserTodos::where('name', '=', $valf->value)->get()->pop();
                                                }
                                                $nombre   = explode(" ",$uderidenti->nombres);
                                                $apellido = explode(" ",$uderidenti->apellidos);
                                                if(isset($nombre[0]) && isset($apellido[0])){
                                                    $valf->value = $nombre[0] ." ". $apellido[0];
                                                }else{
                                                    $valf->value = "No hay nombre!";
                                                }

                                                $otroRespal[] = $valf;
                                            }
                                            $objRespal = json_encode($otroRespal);
											if (is_numeric($casi->value)) {
                                                $uderidenti = App\UserTodos::find($casi->value);
                                            }else{
                                                $uderidenti = App\UserTodos::where('name', '=', $casi->value)->get();
                                                $uderidenti = isset($uderidenti[0])?$uderidenti[0]:'';
                                            }
										}
                                        ?>
                                                    <?php if (isset($uderidenti) && !empty($uderidenti) ){ ?>
                                                    <a class="media-left waves-light">
												@if($uderidenti->foto != '')
                                                <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/{{ $uderidenti->foto }}" alt="Usuario">
												@else
												<img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/essi_admon.jpg" alt="Usuario">
												@endif
                                            </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <input type="hidden" id="identifico" value="{{ $objRespal }}">

                                                            <?php
                                                        if(isset($uderidenti->nombres) && isset($uderidenti->apellidos)){
                                                            $nombre   = explode(" ",$uderidenti->nombres);
                                                            $apellido = explode(" ",$uderidenti->apellidos);
                                                            if(isset($nombre[0]) && isset($apellido[0])){
                                                                echo $nombre[0] ." ". $apellido[0];
                                                            }else{
                                                                echo "No hay nombre!";
                                                            }
                                                        }
                                                    ?>
                                                                @if($total>1)
                                                                <a href="#" onclick="llenarTabla('identifico')" class="histrialModal">
                                                        <span class="badge badge-pill badge-danger bg-danger"><?php echo $total; ?></span>
                                                    </a> @endif
                                                        </h4>
                                                        <p class="subtitulo">@if(isset($uderidenti->cargo)) {{ $uderidenti->cargo}} @else No hay cargo @endif</p>
                                                    </div>
                                                    <?php } ?>
                                            </div>
                                    </td>
                                    <td class="centrar-div"><?=$data['freelance']?></td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "probabilidad";
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                                $casi = $otro->pop();
                                            ?>
                                            <img src="{{ url('/') }}/images/<?php if (isset($casi->value)) {echo $casi->value;} ?>.png" style="width: 30px;">
                                            <input type="hidden" id="probabilidad" value="{{ $respal }}">
                                            <?php if (isset($casi->value)) {echo $casi->value;} ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('probabilidad')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger bg-danger"><?php echo count($respal); ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr class="tr-table">
                                    <th class="centrado" colspan="2">Valor Oportunidad</th>
                                    <th class="centrado">Tasa de cambio</th>
                                    <th class="centrado">Valor en Pesos</th>
                                    <th class="centrado">Margen</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado" colspan="2" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "valor_oportunidad";
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                            ?>

                                            <input type="hidden" id="valor_oportunidad" value="{{ $respal }}">

                                            <?php
                                                $valoracomvertir = $otro->pop();
                                                if (isset($valoracomvertir->value)) {echo $valoracomvertir->value;}
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('valor_oportunidad')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger bg-danger"><?php echo $total; ?></span>
                                            </a>
                                            @endif

                                            <?php
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "moneda";
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                            ?>

                                            <input type="hidden" id="moneda" value="{{ $respal }}">

                                            <?php
                                                $moneda = $otro->pop();
                                                if (isset($moneda->value)) {echo $moneda->value;}
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('moneda')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger bg-danger"><?php echo count($total); ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "tasa_cambio";
                                                });
                                                $respal = $otro;
                                                $total = count($otro);
                                                $casi = $otro->pop();
                                            ?>

                                            <input type="hidden" id="tasa_cambio" value="{{ $respal }}">
                                            <?php
                                                if (isset($casi->value)) {
                                                    $cantidad = str_replace(',','',$casi->value);
                                                    $cantidad = (int)$cantidad;
                                                    $cantidad = number_format($cantidad);
                                                    echo "$ ".$cantidad;
                                                }
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('tasa_cambio')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger bg-danger"><?php echo count($respal); ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php
                                                $otro = $datos->filter(function ($value, $key) {
                                                    return $value->key == "val_pesos";
                                                });
                                                $valpesosoportunidad = $otro->pop();
                                                if(isset($valpesosoportunidad->value)){
                                                    echo "$ ".$valpesosoportunidad->value;
                                                }else{
                                                    echo "$ 0";
                                                }
                                            ?>
                                        </strong></h2>
                                    </td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "margen";
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                            ?>
                                            <input type="hidden" id="margen" value="{{ $respal }}">
                                            <?php
                                                $casi = $otro->pop();
                                                if (isset($casi->value)) {echo $casi->value;}
                                            ?>%
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('margen')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger bg-danger"><?php echo count($respal); ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table product-table">
                           <thead>
                                <tr class="tr-table">
                                    <th class="centrado">Competencia</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;"><a onclick="anadir_competencia()" class="text-success" style="font-size:2rem;cursor:pointer;" data-toggle="tooltip" data-placement="right" title="Añadir Competencia"><i class="fa fa-plus"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <div class="row" id="competencias_oportunidad">

                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr class="tr-table">
                                    <th class="centrado">Forma de pago</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <?php
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "otra_forma_pago";
                                            });

                                            $respal = $otro;
                                            $total = count($otro);
                                            $casi = $otro->pop();
                                        ?>
                                            <p style="font-size: larger;">
                                                <input type="hidden" id="otra_forma_pago" value="{{ $respal }}">
                                                <?php
                                                if (isset($casi->value)) {echo $casi->value;}
                                            ?>
                                                    @if($total>1)
                                                    <a href="#" onclick="llenarTabla('otra_forma_pago')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger soloprimer parrafo bg-danger"><?php echo count($respal); ?></span>
                                            </a> @endif
                                            </p>

                                            <?php $formpag = App\OportunidadesFormapago::where('oportunidad_id','=', $oportunidad->id)->get();
                                            $i=0;
                                            echo '<table>';
                                            foreach ($formpag as $key => $value) {
                                                if ($value->key=="cuota_valor") {
                                                    $i++;
                                                    $value->key="Cuota ".$i;
                                                }
                                                echo '<tbody>
                                                        <tr>
                                                            <td style="min-width: 100px;text-align: left;"><p class="soloprimer parrafo">'.$value->key.'</p></th>
                                                            <td style="min-width: 100px;text-align: left;"><p class="soloprimer parrafo">'.$value->value.'%</p></th>
                                                            <td style="text-align: left;"><p class="soloprimer parrafo">'.$value->texto.'</p></th>
                                                        </tr>
                                                    </tbody>';
                                            }
                                            echo '</table>';
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                            <thead><a name="titulobservaciones"></a>
                                <tr class="tr-table">
                                    <th class="centrado">Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="" style="">
                                        <div class="media mb-1">
                                            <a class="media-left waves-light"><?php $user = App\UserTodos::findOrFail($oportunidad->user_id); ?>
                                               <?php if(isset($user->foto)){ ?>
                                                <img class="rounded-circle observaciones-img" src="/images/file/clientes/{{ $user->foto }}" alt="Usuario">
                                                <?php }else{ ?>
                                                <img class="rounded-circle observaciones-img" src="http://via.placeholder.com/60x60" alt="Usuario">
                                                <?php } ?>
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading observaciones-title">
                                                    <?php
                                                if(isset($user->nombres) && isset($user->apellidos)){
                                                    $nombre   = explode(" ",$user->nombres);
                                                    $apellido = explode(" ",$user->apellidos);
                                                    if(isset($nombre[0]) && isset($apellido[0])){
                                                        echo $nombre[0] ." ". $apellido[0];
                                                    }else{
                                                        echo "No hay nombre!";
                                                    }
                                                } ?>
                                                </h4>
                                                <p class="subtitulo_coment">
                                                    <?php
                                                        $otro = $datos->filter(function ($value, $key) {
                                                          return $value->key == "observaciones";
                                                        });

                                                        $respal = $otro;
                                                        $total = count($otro);
                                                    ?>

                                                        <input type="hidden" id="observaciones" value="{{ $respal }}">

                                                        <?php
                                                        $casi = $otro->pop();
                                                        if (isset($casi->value)) {echo $casi->value;}
                                                    ?>
                                                            @if($total>1)
                                                            <a href="#" onclick="llenarTabla('observaciones')" class="histrialModal">
                                                        <span class="badge badge-pill badge-danger bg-danger"><?php echo count($respal); ?></span>
                                                    </a> @endif
                                                </p>
                                            </div>
                                        </div>
                                        <div id="lisComentariosId"></div>
                                        <form id="formComentarios">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="oportunidad_id" value="{{ $oportunidad->id }}">
                                            <div class="md-form" style="background-color: rgba(174, 219, 251, 0.71);margin-top: 50px;">
                                                <textarea type="text" id="form7" class="form-control" name="comentario" placeholder="Añadir comentario"></textarea>
                                            </div>
                                        </form>
                                        <div class="centrado mb-5">
                                            <a href="#" class="boton negro redondo" onclick="guardarComentario({{ $oportunidad->id }})"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal de cambios -->
<div class="modal fade" id="exampleModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="my_modal_content">
            <div class="modal-header">
                <h5 class="modal-title">Historial de cambios</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display" id="ver_anterior" style="display: none;">
                    <thead>
                        <tr>
                            <th class="centrado">Fecha</th>
                            <th class="centrado">Valor</th>
                        </tr>
                    </thead>
                    <tbody id="idTable">

                    </tbody>
                </table>
                <div class="historial-cambios" style="display: none;">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- fin modal de cambios -->

<!-- modal de cambios -->
<div class="modal fade" id="detalleModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <span style="display:block;text-align:center;">
            <!--<img src="{{ url('/') }}/images/empresas/kMbwrX5R.jpeg" style="height: 140px;width: 140px;">-->
            </span>
                <div id="contenbodyModal">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- fin modal de cambios -->

<!--Modal para agregar competencia-->
<div class="modal fade" id="modal_anadir_competencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Competencias</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled activity-list">
                            <li>
                                <i class="fa fa-bell-o activity-icon pull-left"></i>
                                <p class="text-left">
                                    <a href="#">Información</a> Debes dar click en una competencia para agregar ala oportunidad <a href="#"><?=$oportunidad->titulo?></a> <span class="timestamp" id="numerocompetencias"><?=count($data['competencias_oportunidad'])?> Competencia(s) Agregadas</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row" id="Listacompetencias">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal para agregar competencia-->

<!--Modal para ver los equipos de la competencia-->
 <div class="modal fade bd-example-modal-lg" id="ver_competencia" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    </div>
  </div>
</div>
<!--Fin Modal para ver los equipos de la competencia-->
@endsection
@section('scripts')
<script type="text/javascript" src="/js/parsley.min.js"></script>
<script src="/components/3Dtimeline/js/modernizr.custom.63321.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>

<script type="text/javascript">
    direccion="{{ url('/') }}";
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
        lista_oportunidades_competencias();
        listCompetencias();
        moment.locale('es');
        getComentarios({{ $oportunidad->id }});
        $("body").on("click",".clickea",function(e){
            id=$(this).attr("data-name");
            $(".div-oculto").hide("clip");
            $("#"+id).show("clip");
        });
        $( "#comentarbtn" ).click(function() {
          setTimeout(function(){ $("#form7").focus() }, 1000);
        });
    });

    guardarComentario = function (id) {
        event.preventDefault();
        if(true === $("#formComentarios").parsley().validate()){
          console.log("llega");
          var datos = new FormData($("#formComentarios")[0]);
          $.ajax({
            url: "{{ url('savecomenoport') }}",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
              if (data.success) {
                $('#formComentarios')[0].reset();
                getComentarios(id);
              }else{
                swal("Algo salio mal, vuelve a intentar");
              }
            }
        });
        }else{
          swal("Faltan campos por completar!");
        }
    }


    $(document).on("click",".histrialModal",function(e){
        $("#exampleModal").modal();
    });

    var parent, modal_height;
    function setModalMargin(){
        margin = 0;
        parent = $('#exampleModal').height();
        modal_height = $("#my_modal_content").height();
        console.log('parent: '+parent+' modal_height: '+modal_height);
        if(modal_height > parent){
            margin = (modal_height - parent) / 2;
           }
        $("#my_modal_content").css('margin-top', margin+"px");
    }
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        $("#my_modal_content").css('margin-top', '0%');
    });

    getComentarios = function (id) {
        $.ajax({
          url: "{{ url('listcomenoport') }}/"+id,
          type: 'GET',
          success: function(data){
            if (data.success) {
              console.log(data.datos);
              var html3 = "";
              for(i in data.datos){
                comentario = data.datos[i];
                html3 += `
                <div class="media mb-1">
                  <a class="media-left waves-light">
                      <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/`+comentario.user.foto+`" alt="Usuario">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading observaciones-title">`+comentario.user.nombres+` `+comentario.user.apellidos+` <small>`+moment(comentario.created_at).calendar()+`</small> </h4>
                      <p class="subtitulo_coment">`+comentario.comentario+`</p>
                  </div>
                </div>`;
              }
              $("#lisComentariosId").html(html3);
            }else{

            }
          }
        });
    }

    llenarTabla =  function (Datos) {
        if (Datos === "productos") {
            $('#example').css('display','block');
            $('.historial-cambios').css('display','none');
            $("#idTable").html($("#"+Datos).val());
        }else{
            html = "";
            //console.log(Datos);

            Datos = $("#"+Datos).val();
            console.log(Datos);
            Datos = JSON.parse(Datos);
            console.log(Datos);

            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "{{url('/')}}/oportunidad-action",
                data: {
                    _token: CSRF_TOKEN,
                    action: 0,
                    dir: "{{url('/')}}",
                    Datos: Datos
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#example').css('display','none');
                    $('.historial-cambios').css('display','block');
                    $('.historial-cambios').html(e.data['cuerpo']);
                    setModalMargin();
                }
            });
        }
    }
    function consultar_usuario(id){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/oportunidad-action",
            data: {
                _token: CSRF_TOKEN,
                action: 0,
                id: id
            },
            cache: false,
            type: 'POST',
            success: function(e){
                nombre = e.data['nombre'];
            }
        });
    }
    function fecha_hora(f){
        meses = ["","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        fecha_hora = f.split(" ");
        fecha = fecha_hora[0].split("-");
        hora = fecha_hora[1].split(":");
        if(hora[0]>12){
           hora[0] = hora[0]-12;
           jornada = "P.M";
       }else{
           jornada = "A.M";
       }
        fecha_final = fecha[2]+' de '+meses[parseInt(fecha[1])]+' '+fecha[0]+' '+hora[0]+':'+hora[1]+':'+hora[2]+' '+jornada;
        return fecha_final;
    }
    function fecha(f){
        meses = ["","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        fecha = f.split("-");

        fecha_final = fecha[2]+' de '+meses[parseInt(fecha[1])]+' '+fecha[0];
        return fecha_final;
    }
    verCliente = function () {

        dato = [];
        dato["cliente"] = "{{ $cliente->cliente }}";
        dato["empresa"] = "{{ $cliente->empresa }}";
        dato["unidad_negocio"] = "{{ $cliente->unidad_negocio }}";
        <?php $cliente->perfil = str_replace(array("\n", "\r", "\n\r"), " <br> ", $cliente->perfil); ?>
        dato["perfil"] = `{{ $cliente->perfil }}`;
        dato["cargo"] = "<?php if(isset($cliente->cargo)){ echo $cliente->cargo; }else{ echo 'No hay cargo'; } ?>";
        dato["jefe_inmediato"] = "{{ $cliente->jefe_inmediato }}";
        dato["n_a_pesos"] = "{{ $cliente->n_a_pesos }}";
        dato["pais"] = "{{ $cliente->pais }}";
        dato["departamento"] = "{{ $cliente->departamento }}";
        dato["ciudad"] = "{{ $cliente->ciudad }}";

        html = `<label class="col-form-label fondo_title">Cliente</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["cliente"]+`
                </p>
                <label class="col-form-label fondo_title">Empresa</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["empresa"]+`
                </p>
                <label class="col-form-label fondo_title">Unidad de negocio</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["unidad_negocio"]+`
                </p>
                <label class="col-form-label fondo_title">Perfil</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["perfil"]+`
                </p>
                <label class="col-form-label fondo_title">Cargo</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["cargo"]+`
                </p>
                <label class="col-form-label fondo_title">Jefe inmediato</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["jefe_inmediato"]+`
                </p>
                <label class="col-form-label fondo_title">Valor aprovado</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    $ `+dato["n_a_pesos"]+`
                </p>
                <label class="col-form-label fondo_title">Ubicacion</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["pais"]+`, `+dato["ciudad"]+` - `+dato["departamento"]+`
                </p>`;

        $("#contenbodyModal").html(html);
    }

    verEmpresa = function() {
        nombre = "{{ $empresa->nombre }}";
        nit = "{{ $empresa->nit }}";
        pagina_web = "{{ $empresa->pagina_web }}";
        telefono = "{{ $empresa->telefono }}";
    }

    /**
     * Función para abrir modal y añadir una competencia a la oportunidad
     */
    function anadir_competencia(){
        $('#modal_anadir_competencia').modal('show');
    }

    /**
     * Función para agregar una competencia a la oportunidad
     * @param  INT id IDENTIFICACIÓN DE LA COMPETENCIA
     * @param  STRING nombre NOMBRE DE LA COMPETENCIA
     */
    function competencia_agregar(id,nombre){
        swal({
              title: 'Seguro?',
              text: "Va agregar la competencia "+nombre,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'No',
              confirmButtonText: 'Si, agregar'
            }).then(function(){
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                    url: "/oportunidad-action",
                    data: {
                        _token: CSRF_TOKEN,
                        action: 1,
                        id_oportunidad:<?=$oportunidad->id?>,
                        id_competencia:id
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj'] == 'Guardado correctamente!'){
                        swal(
                              'Agregada',
                              'Agregada correctamente!',
                              'success'
                            );
                        }else{
                           swal(
                              'Error',
                              e.data['msj'],
                              'error'
                            );
                        }
                        lista_oportunidades_competencias();
                        listCompetencias();
                        numerototalcompetenciasagregadas();
                    }
                });
        });
    }

    /**
     * Función para traer el listado de competencias que tiene la oportunidad
     */
    function lista_oportunidades_competencias(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/oportunidad-action",
            data: {
                _token: CSRF_TOKEN,
                action: 2,
                id_oportunidad:<?=$oportunidad->id?>
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#competencias_oportunidad').html(e.data['html']);
            }
        });
    }
    /**
     * Función para traer el listado de competencias que no estan agregadas a la oportunidad
     */
    function listCompetencias(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/oportunidad-action",
            data: {
                _token: CSRF_TOKEN,
                action: 3,
                id_oportunidad:<?=$oportunidad->id?>
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#Listacompetencias').html(e.data['html']);
            }
        });
    }
    /**
     * Función para eliminar una competencia de la oportunidad
     * @param  INT id IDENTIFICACIÓN DE LA TABLA oportunidades_competencias
     */
    function eliminar_competencia(id){
        swal({
              title: 'Seguro?',
              text: "Va elimminar la competencia",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'No',
              confirmButtonText: 'Si, eliminar'
            }).then(function(){
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                    url: "/oportunidad-action",
                    data: {
                        _token: CSRF_TOKEN,
                        action: 4,
                        id:id
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj'] == 'Eliminado correctamente!'){
                        swal(
                              'Eliminado',
                               e.data['msj'],
                              'success'
                            );
                        }else{
                           swal(
                              'Error',
                               e.data['msj'],
                              'error'
                            );
                        }
                        lista_oportunidades_competencias();
                        listCompetencias();
                        numerototalcompetenciasagregadas();
                    }
                });
            });
    }
/**
 * Función para eliminar el equipo de la competencia
 * @param INT id IDENTIFICACIÓN DEL LA TABLA oportunidades_competencias_equipos
 */
function eliminar_equipo_competencia(id,id_oportunidadcompetencia){
     swal({
          title: 'Seguro?',
          text: "Va elimminar el equipo seleccionado",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'No',
          confirmButtonText: 'Si, eliminar'
        }).then(function(){
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/oportunidad-action",
                data: {
                    _token: CSRF_TOKEN,
                    action: 8,
                    id:id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == 'Eliminado correctamente!'){
                    swal(
                          'Eliminado',
                           e.data['msj'],
                          'success'
                        );
                    }else{
                       swal(
                          'Error',
                           e.data['msj'],
                          'error'
                        );
                    }
                    lista_oportunidades_competencias();
                    vercompetencia(id_oportunidadcompetencia);
                }
            });
     });
}
    /**
     * Función para encontrar el total de competencias agregadas
     */
    function numerototalcompetenciasagregadas(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/oportunidad-action",
            data: {
                _token: CSRF_TOKEN,
                action: 5,
                id_oportunidad:<?=$oportunidad->id?>
            },
            cache: false,
            type: 'POST',
            success: function(e){
               $('#numerocompetencias').html(e.data['numero']+' Competencia(s) Agregadas');
            }
        });
    }

/**
 * Función para ver la competencia
 * @param INT id IDENTIFICACIÓN DE LA TABLA oportunidades_competencias
 */
function vercompetencia(id){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/oportunidad-action",
        data: {
            _token: CSRF_TOKEN,
            action: 6,
            id:id
        },
        cache: false,
        type: 'POST',
        success: function(e){
           $('#ver_competencia .modal-content').html(e.data['html']);
        }
    });
}

    /**
     * Función para mostrar formulario de un nuevo equipo
     * @param STRING accion ABRIR O CERRAR
     */
    function equipo_nuevo(accion){
        if(accion == 'abrir'){
            $('#nuevo_equipo').css('display','block');
            $('#lista_equipos').css('display','none');
        }else{
            $('#nuevo_equipo').css('display','none');
            $('#lista_equipos').css('display','block');
        }
    }

    /**
     * Función para guardar o editar un equipo de la competencia que tiene asignada la oportunidad
     * @param INT equipo IDENTIFICACIÓN DEL EQUIPO SI SE VA A EDITAR O 'nuevo' SI SE VA A CREAR UN NUEVO REGISTRO
     */
    function guardar_equipo(equipo){
        var CSRF_TOKEN = $('input[name=_token]').val();
        if(equipo == 'nuevo'){
            var parametros = {
                    _token: CSRF_TOKEN,
                    action: 7,
                    id_oportunidad_competencia : $('#id_oportunidadcompetencia').val(),
                    equipo : $('#producto_'+equipo).val(),
                    valor: $('#valor_'+equipo).val(),
                    moneda: $('#moneda_'+equipo).val(),
                    valor_pesos: $('#valorpesos_'+equipo).val(),
                    descripcion: $('#descripcion_'+equipo).val()
            };
        }else{
            var parametros = {
                    _token: CSRF_TOKEN,
                    action: 7,
                    id : $('#id_'+equipo).val(),
                    id_oportunidad_competencia : $('#id_oportunidadcompetencia').val(),
                    equipo : $('#producto_'+equipo).val(),
                    valor: $('#valor_'+equipo).val(),
                    moneda: $('#moneda_'+equipo).val(),
                    valor_pesos: $('#valorpesos_'+equipo).val(),
                    descripcion: $('#descripcion_'+equipo).val()
            };
        }
        if($('#producto_'+equipo).val() != '' && $('#valor_'+equipo).val() != '' && $('#moneda_'+equipo).val() != '' && $('#valorpesos_'+equipo).val() != ''){
           swal({
              title: 'Seguro?',
              text: "Desea Guardar el equipo",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, Guardar'
            }).then(function(){
                var jqxhr = $.ajax({
                    url: "/oportunidad-action",
                    data: parametros,
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj']=='Guardado correctamente!'){
                            swal(
                                  'Guardado',
                                  e.data['msj'],
                                  'success'
                                );
                        }else{
                           swal(
                              'Error',
                              e.data['msj'],
                              'error'
                            );
                        }
                       vercompetencia($('#id_oportunidadcompetencia').val());
                       lista_oportunidades_competencias()
                    }
                });
            });
        }else{
            swal(
              'Alerta',
              'Faltan campos por llenar. Recuerde que son obligatorios (Equipo, Valor, Moneda, Valor en pesos)',
              'warning'
            );
        }
    }

    /**
     * Función para habilitar los campos del equipo a editar
     * @param INT id IDENTIFICACION DEL EQUIPO DE LA TABLA oportunidades_competencias_equipos
     */
    function editar_equipo(id,id_boton){
        $(".formulario").each(function(){
            $(this).attr('readonly','readonly');
        });
        $(".btn-editar-equipo").each(function(){
            $(this).css('display','inline');
        });
        $(".btn-guardar-equipo").each(function(){
            $(this).css('display','none');
        });
        ver_observacion_equipo(id,'btn_ver_equipo_'+id);
        $('#producto_'+id).removeAttr('readonly');
        $('#valor_'+id).removeAttr('readonly');
        $('#moneda_'+id).removeAttr('readonly');
        $('#valorpesos_'+id).removeAttr('readonly');
        $('#descripcion_'+id).removeAttr('readonly');
        $('#'+id_boton).css('display','none');
        $('#btn_editarguardadndo_equipo'+id).css('display','inline');
    }

    /**
     * Función para mostrar la observación del equipo que esta oculto
     * @param INT id IDENTIFICACIÓN DE LA FILA
     * @param STRING id_boton IDENTIFICACION DEL BOTON CON EL OJITO
     */
    function ver_observacion_equipo(id,id_boton){
        $(".observaciones-ocultas").each(function(){
            if($(this).attr('id') != "fila_"+id){
                $(this).css('display','none');
                var identidad = ($(this).attr('id')).replace("fila_", "");;
                $('#btn_ver_equipo_'+identidad).html('<i class="fa fa-eye"></i>');
            }
        });
        if($("#fila_"+id).css('display') == 'none'){
            $("#fila_"+id).css('display','block');
            $('#'+id_boton).html('<i class="fa fa-eye-slash"></i>');
        }else{
            $("#fila_"+id).css('display','none');
            $('#'+id_boton).html('<i class="fa fa-eye"></i>');
        }
    }

    /**
     * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
     * @param  STRING texto  TEXTO INGRESADO EN EL CAMPO
     * @param  INT max_long NUMERO MAXIMO DE CARACTERES PARA EL CAMPO
     * @param  STRING id  IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
     * @param  STRING id_campo  IDENTIFICACION DEL CAMPO EDITADO
     */
    function max_campo(texto,max_long,id,id_campo){
        numeroCaracteres = texto.length;
        $(id).removeClass('invisible');
        if(numeroCaracteres > max_long){
            $('#'+id_campo).val(texto.substr(0,max_long));
            $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
        }else{
            $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
        }
    }
    /**
     * Función para dar formato valor
     */
    function formato_numero(valor,id){
        valor = replaceAll(valor, "$ ", "" );
        if(valor != ""){
           valor = replaceAll(valor, ",", "" );
           valor = parseFloat(valor);

           if(isNaN(valor)){
                $('#'+id).val('$ 0');
            }else{
                valor = number_format(valor,0);
                $('#'+id).val("$ "+valor);
            }
        }else{
           $('#'+id).val('$ 0');
        }
    }
    function number_format(amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    function replaceAll( text, busca, reemplaza ){
      while (text.toString().indexOf(busca) != -1)
          text = text.toString().replace(busca,reemplaza);
      return text;
    }
</script>

@endsection
