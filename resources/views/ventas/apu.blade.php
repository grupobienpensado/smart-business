@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'APU')

@section('content')

<style type="text/css">
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
    height: 17px;
}
.form-control3 {
    width: 100%;
    padding: 0px !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    height: 17px !important;
    font-size: 0.8rem !important;
}
.table td{
    line-height: 0 !important;
    border: 0 !important;
}
.estirar{
  width: 100%
}
.form-control:disabled, .form-control[readonly] {
     background-color: transparent !important;
}
</style>
@php
$dias=cal_days_in_month ( CAL_GREGORIAN, date("m")+1, date("Y") );
$semana=["Dom","Lun","Mar","Mie","Jue","Vie","Sab"];
$meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic"];
$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

if(date('m')==12){
  $anio=date('Y')+1;
}else{
  $anio=date("Y");
}
@endphp
<div class="container animated slideInDown">
  <div class="row">
    <main class="col-sm-12 col-md-12 pt-5">
      <div class="pull-right">   
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>APU para venta oportunidad</h3>
			</div>
      	<div class="row">
        <form method="POST" action="{{ url('saveapu') }}" enctype="multipart/form-data" class="estirar">
        <input type="hidden" name="id" value="{{$id}}">
        <input type="hidden" name="version" value="{{$version+1}}">
        {{ csrf_field() }}
      	 <div class="col-md-12">
           <table id="tabla" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
                <tr>
                  <th colspan="5" style="padding: 0;background-color: #d0cfcf;text-align: center;">Maquinas ESSI</th>
                </tr>
               <tr>
                 <th width="50" class="text-center" style="padding: 2px;">#</th>
                 <th class="text-center" style="padding: 2px;">Descripción</th>
                 <th width="100" class="text-center" style="padding: 2px;">Cantidad</th>
                 <th width="180" class="text-center" style="padding: 2px;">Valor Unitario</th>
                 <th width="180" class="text-center" style="padding: 2px;">Valor Total</th>
               </tr>
             </thead>
             <tbody>
             <tr class="tr0">
              <td class="text-center">1</td>
               <td ><input type="hidden" value="productos" name="maquina[0][tipo]"/>
               <select class="form-control3 form-control opciones" id="0" name="maquina[0][concepto]">
                 <option>Seleccione una opción</option>
                 @foreach($productos as $producto)
                 <option value="{{$producto->id}}">{{$producto->name}}</option>
                 @endforeach
               </select></td>
               <td><input class="form-control2 form-control cant-0" onkeyup="primerafila(0)" type="number" name="maquina[0][cantidad]" min="0" ></td>
               <td><input class="form-control2 form-control dinero unidad-0" onkeyup="primerafila(0)"  type="text" name="maquina[0][valor_unitario]"></td>
               <td><input class="form-control2 form-control dinero total-0 total" readonly="" type="text" name="maquina[0][valor_total]"></td>
             </tr>
             </tbody>
             <tfoot>
               <tr>
               <td colspan="4" style="text-align: right; font-weight: bold; font-size: 0.7rem !important;">Costo Total De La Maquina:</td>
               <td><input class="form-control2 form-control dinero" type="text" name="total_maquina" id="total"></td>
             </tr>
             </tfoot>
           </table>
           <table id="tabla2" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
                <tr>
                  <th colspan="5" style="padding: 0;background-color: #d0cfcf;text-align: center;">Otros Equipos</th>
                </tr>
               <tr>
                 <th width="50" class="text-center" style="padding: 2px;">#</th>
                 <th class="text-center" style="padding: 2px;">Descripción</th>
                 <th width="100" class="text-center" style="padding: 2px;">Cantidad</th>
                 <th width="180" class="text-center" style="padding: 2px;">Valor Unitario</th>
                 <th width="180" class="text-center" style="padding: 2px;">Valor Total</th>
               </tr>
             </thead>
             <tbody>
             <tr class="otros0">
              <td class="text-center">1</td>
               <td ><input type="hidden" value="otras_maquinas" name="otros[0][tipo]"/>
               <select class="form-control3 form-control opciones2" data-name="0" name="otros[0][concepto]">
                 <option>Seleccione una opción</option>
                 @foreach($otros as $otro)
                 <option value="{{$otro->id}}">{{$otro->concepto}}</option>
                 @endforeach
               </select></td>
               <td><input class="form-control2 form-control cants-0" onkeyup="segundafila(0)" type="number" name="otros[0][cantidad]" min="0" ></td>
               <td><input class="form-control2 form-control dinero unidads-0" onkeyup="segundafila(0)"  type="text" name="otros[0][valor_unitario]"></td>
               <td><input class="form-control2 form-control dinero totals-0 totals" readonly="" type="text" name="otros[0][valor_total]"></td>
             </tr>
             </tbody>
             <tfoot>
               <tr>
               <td colspan="4" style="text-align: right; font-weight: bold; font-size: 0.7rem !important;">Costo Total de Otros Equipos:</td>
               <td><input class="form-control2 form-control dinero" type="text" name="total_equipos" id="totals"></td>
             </tr>
             </tfoot>
           </table>
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
                <tr>
                  <th colspan="5" style="padding: 0;background-color: #d0cfcf;text-align: center;">Otros Gastos</th>
                </tr>
               <tr>
                 <th width="50" class="text-center" style="padding: 2px;">#</th>
                 <th class="text-center" style="padding: 2px;">Descripción</th>
                 <th width="100" class="text-center" style="padding: 2px;">Cantidad</th>
                 <th width="180" class="text-center" style="padding: 2px;">Valor Unitario</th>
                 <th width="180" class="text-center" style="padding: 2px;">Valor Total</th>
               </tr>
             </thead>
             <tbody>
             @php $i=1; @endphp
             @foreach($gastos as $key)
             <tr>
              <td class="text-center">{{$i}}</td>
               <td ><input type="hidden" value="gastos" name="gastos[{{$i-1}}][tipo]"/>
               <input type="hidden" name="gastos[{{$i-1}}][concepto]" value="{{$key->id}}"><input class="form-control3 form-control" value="{{$key->concepto}}" type="text"></td>
               <td><input class="form-control2 form-control cantg-{{$i}}" onkeyup="gastofila({{$i}})" type="number" name="gastos[{{$i-1}}][cantidad]" min="0" ></td>
               <td><input class="form-control2 form-control dinero unidadg-{{$i}}" onkeyup="gastofila({{$i}})"  type="text" name="gastos[{{$i-1}}][valor_unitario]"></td>
               <td><input class="form-control2 form-control dinero totalg-{{$i}} total_g" readonly="" type="text" name="gastos[{{$i-1}}][valor_total]"></td>
             </tr>
             @php $i++; @endphp
             @endforeach
             </tbody>
             <tfoot>
               <tr>
               <td colspan="4" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Costo Total de Otros Gastos:</td>
               <td><input class="form-control2 form-control dinero" type="text" name="total_gastos" id="total_g" readonly=""></td>
             </tr>
             </tfoot>
           </table>
           <div>
             <table cellspacing="0" width="75% !important" class="table table-striped table table-striped table-bordered display">
             <tfoot>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Costo Fabricación:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="costo_fabricacion" id="fabricacion" disabled>
                  </td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Valor Venta:</td>
                 <td><input class="form-control2 form-control dinero" type="text" style="width: 75%;display: inline-block;" name="valor_venta" id="venta">
                 <select class="form-control3 form-control" id="tipo_moneda" name="moneda" style="width: 25%;display: inline-block;float: right;" onchange="fun_moneda()">
                     <option>Seleccione una opción</option>
                     <option value="COP">PESOS</option>
                     <option value="USD">USD</option>
                     <option value="EUR">EURO</option>
                   </select>
                  </td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Valor Negociación Sugerido:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="negociacion_sugerida" id="sugerido" disabled></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Valor Negociación Real:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="negociacion_real" id="real" ></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Precio Venta:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="precio_venta" id="precioventa" disabled></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Utilidad:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="utilidad" id="utilidad" disabled></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Margen Bruto de Venta:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="margen_bruto_venta" id="margen" disabled></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Porcentaje de financión:</td>
                 <td><input class="form-control2 form-control financia" type="text" onkeyup="financiacion()" name="porcentaje_financiacion" id="porcentaje" placeholder="%"></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Meses De Financiación:</td>
                 <td><input class="form-control2 form-control financia" type="number" onkeyup="financiacion()" name="meses_financionacion" id="meses"></td>
               </tr>
               <tr>
                  <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Valor De Financión:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="valor_financion" id="financiamiento" disabled=""></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Utilidad Final Descontando Financiación:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="utilidad_final" id="finalfinancia" readonly=""></td>
               </tr>
               <tr>
                 <td colspan="3" style="text-align: right; font-weight: bold; font-size: 0.8rem !important;">Margen Bruto Final:</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="margen_bruto_final" id="margen_final" readonly=""></td>
               </tr>
             </tfoot>
           </table>
           </div>
           
           <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
         </div>
      </form>
      </div>
    </main>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" charset="utf-8">

function primerafila(id){
  
  cant=$(".cant-"+id).val();
  unidad=$(".unidad-"+id).val();
  if(unidad!=""){unidad=unidad.split(",").join("").split(".").join(",")}else{unidad=0}
  if(cant!=""){cant=cant.split(",").join("").split(".").join(",")}else{cant=0}
  valor=cant*unidad;
  $(".total-"+id).val(formatNumber.new(valor));
  i=0; 
  total=0;
  $(".total").each(function(){
    vtotal=$(".total-"+i).val();
    if(vtotal!=""){vtotal=vtotal.split(",").join("").split(".").join(",")}else{vtotal=0;}
    total=parseInt(total)+parseInt(vtotal);
    i++;
  })
  
  $("#total").val(formatNumber.new(total));
  funTotal();
}

function segundafila(id){
  
  cant=$(".cants-"+id).val();
  unidad=$(".unidads-"+id).val();
  if(unidad!=""){unidad=unidad.split(",").join("").split(".").join(",")}else{unidad=0}
  if(cant!=""){cant=cant.split(",").join("").split(".").join(",")}else{cant=0}
  valor=cant*unidad;
  $(".totals-"+id).val(formatNumber.new(valor));
  i=0; 
  total=0;
  $(".totals").each(function(){
    vtotal=$(".totals-"+i).val();
    if(vtotal!=""){vtotal=vtotal.split(",").join("").split(".").join(",")}else{vtotal=0;}
    total=parseInt(total)+parseInt(vtotal);
    i++;
  })
  
  $("#totals").val(formatNumber.new(total));
  funTotal();
}

function gastofila(id){
  cant=$(".cantg-"+id).val();
  unidad=$(".unidadg-"+id).val();
  if(unidad!=""){unidad=unidad.split(",").join("").split(".").join(",")}else{unidad=0}
  if(cant!=""){cant=cant.split(",").join("").split(".").join(",")}else{cant=0}
  valor=cant*unidad;
  $(".totalg-"+id).val(formatNumber.new(valor));
  o=1; 
  total=0;
  $(".total_g").each(function(){
    vtotal=$(".totalg-"+o).val();
    if(vtotal!=""){vtotal=vtotal.split(",").join("").split(".").join(",")}else{vtotal=0;}
    total=parseInt(total)+parseInt(vtotal);
    o++;
  })
  $("#total_g").val(formatNumber.new(total));
  funTotal();
}

function fun_moneda() {
      var val = 1;
      $("#real").val("");
      if ($("#tipo_moneda").val() != "COP") { 
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
          var jqxhr = $.ajax({ 
              url: "{{ url('/') }}/cambiarmoneda", 
              data: {
                  _token: CSRF_TOKEN,
                  cantidad:val,
                  moneda_origen:$("#tipo_moneda").val(),
                  moneda_destino:"COP"
              },
              cache: false,
              type: 'POST',
              success: function(e){  
                  e = parseInt(e);
                  var num = e;
                  if(!isNaN(num)){
                  num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
                  num = num.split('').reverse().join('').replace(/^[\.]/,'');
                  e = num;
                  }
                  $("#sugerido").val("$"+e);
                  funTotal();
              }
          });
      }else{
          $("#sugerido").val(val);
          funTotal();
      }

  }

function financiacion(){
  meses=$("#meses").val();
  porcentaje=$("#porcentaje").val();
  valor=$("#precioventa").val();

  if(meses!=""){meses=meses.split(",").join("").split(".").join(",")}else{meses=0;}
  if(valor!=""){valor=valor.split(",").join("").split(".").join(",").split("$").join("")}else{valor=0;}
  console.log(parseFloat(porcentaje));
  console.log(parseFloat(porcentaje)/100);
  gral=parseInt(valor)*parseInt(meses)*(parseFloat(porcentaje)/100);
  $("#financiamiento").val("$"+formatNumber.new(gral));

  utilidad=$("#utilidad").val();
  precioventa=$("#precioventa").val();
  if(utilidad!=""){utilidad=utilidad.split(",").join("").split(".").join(",").split("$").join("")}else{utilidad=0;}
  if(precioventa!=""){precioventa=precioventa.split(",").join("").split(".").join(",").split("$").join("")}else{precioventa=0;}
  finalfinancia=utilidad-gral;

  $("#finalfinancia").val("$"+formatNumber.new(finalfinancia));
  final=(finalfinancia/precioventa)*100;
  $("#margen_final").val(final+"%");

}
function funTotal(){
  
  uno=$("#total").val();
  dos=$("#total_g").val();
  tres=$("#totals").val();
  if(uno!=""){uno=uno.split(",").join("").split(".").join(",")}else{uno=0;}
  if(dos!=""){dos=dos.split(",").join("").split(".").join(",")}else{dos=0;}
  if(tres!=""){tres=tres.split(",").join("").split(".").join(",")}else{tres=0;}

  total=parseInt(uno)+parseInt(dos)+parseInt(tres);
  precioventa=$("#precioventa").val().split(",").join("").split(".").join(",").split("$").join("");
  diferencia=precioventa-total;
  margen=(diferencia/precioventa)*100

  porcentaje=$("#porcentaje").val();
  fabiracion=(porcentaje/100)*total;

  $("#utilidad").val("$"+formatNumber.new(diferencia));
  $("#fabricacion").val("$"+formatNumber.new(total));
  $("#margen").val(margen.toFixed(2)+"%");
  $("#financiamiento").val("$"+formatNumber.new(fabiracion))
  financiacion();
}

$("body").on("keyup","#real",function(e){
  var val=$("#venta").val();
  if($("#tipo_moneda").val()=="COP"){
    $("#precioventa").val(val)
    funTotal();
  }else{
    val=val.split("$").join("");
    if(val!=""){val=val.split(",").join("").split(".").join(",")}else{val=0;}
    equivalente=$(this).val();
    if(equivalente!=""){equivalente=equivalente.split(",").join("").split(".").join(",")}else{equivalente=0;}
    result=equivalente*val;
    $("#precioventa").val("$"+formatNumber.new(result));
    funTotal();
    
  }
})

$("body").on("keyup","#fabricacion",function(e){
  funTotal();
})


$("body").on("change",".opciones",function(e){
  id=$(this).attr("id");
  id=parseInt(id)+1;
  if(!$(".tr"+id).length){
    $("#tabla tbody:last ").append('<tr class="tr'+id+'">'
              +'<td class="text-center">'+(id+1)+'</td>'
               +'<td ><input type="hidden" value="productos" name="maquina['+id+'][tipo]"/><select name="maquina['+id+'][concepto]" class="form-control3 form-control opciones" id="'+id+'">'
                 +'<option>Seleccione una opción</option>'
                 <?php foreach($productos as $producto){ ?>
                 +'<option value="{{$producto->id}}">{{$producto->name}}</option>'
                <?php } ?>
               +'</select></td>'
               +'<td><input class="form-control2 form-control cant-'+id+'" onkeyup="primerafila('+id+')" type="number" name="maquina['+id+'][cantidad]" min="0" ></td>'
               +'<td><input class="form-control2 form-control dinero unidad-'+id+'" onkeyup="primerafila('+id+')" type="text" name="maquina['+id+'][valor_unitario]"></td>'
               +'<td><input class="form-control2 form-control dinero total-'+id+' total" readonly="" type="text" name="maquina['+id+'][valor_total]"></td>'
             +'</tr>')
    $(".dinero").maskMoney();
  }
  
});

$("body").on("change",".opciones2",function(e){
  id=$(this).attr("data-name");
  id=parseInt(id)+1;
  if(!$(".otros"+id).length){
    $("#tabla2 tbody:last ").append('<tr class="otros'+id+'">'
              +'<td class="text-center">'+(id+1)+'</td>'
               +'<td ><input type="hidden" value="otras_maquinas" name="otros['+id+'][tipo]"/><select name="otros['+id+'][concepto]" class="form-control3 form-control opciones2" data-name="'+id+'">'
                 +'<option>Seleccione una opción</option>'
                 <?php foreach($productos as $producto){ ?>
                 +'<option value="{{$producto->id}}">{{$producto->name}}</option>'
                <?php } ?>
               +'</select></td>'
               +'<td><input class="form-control2 form-control cants-'+id+'" onkeyup="segundafila('+id+')" type="number" name="otros['+id+'][cantidad]" min="0" ></td>'
               +'<td><input class="form-control2 form-control dinero unidads-'+id+'" onkeyup="segundafila('+id+')"  type="text" name="otros['+id+'][valor_unitario]"></td>'
               +'<td><input class="form-control2 form-control dinero totals-'+id+' totals" readonly="" type="text" name="otros['+id+'][valor_total]"></td>'
             +'</tr>')

    $(".dinero").maskMoney();
  }
  
});
function realizado(id){
		mo=$("#tr-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr-"+id).show("clip");
		}else{
			$("#tr-"+id).hide("clip");
		}
}

function cancelar(id){
		mo=$("#tr2-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr2-"+id).show("clip");
		}else{
			$("#tr2-"+id).hide("clip");
		}
		
}

function ver(id){
		mo=$("#tr3-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr3-"+id).show("clip");
		}else{
			$("#tr3-"+id).hide("clip");
		}
		
}
$("body").on("click",".cuadro",function(e){
	$("#fecha").html($(this).attr("name"));
	$("#fecha_actividad").val($(this).attr("name"));
	$("#hora_inicio").val($(this).attr("id"));
	fecha=$(this).attr("name");
	hora=$(this).attr("id");
	$("#lista-actividades").html('');
	setTimeout(function(e){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/consultarplanestrabajo/"+fecha, 
        cache: false,
        type: 'GET',
        data:{"hora":hora},
        success: function(e){ 
        	$("#lista-actividades").html(e.tabla);
        }
    });},2000)

})
$(function () {
   $(".dinero").maskMoney();
        i = 0;
        $('#oportunidad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    })

var formatNumber = {
 separador: ",", // separador para los miles
 sepDecimal: '.', // separador para los decimales
 formatear:function (num){
 num +='';
 var splitStr = num.split(',');
 var splitLeft = splitStr[0];
 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
 var regx = /(\d+)(\d{3})/;
 while (regx.test(splitLeft)) {
 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
 }
 return this.simbol + splitLeft +splitRight;
 },
 new:function(num, simbol){
 this.simbol = simbol ||'';
 return this.formatear(num);
 }
}
</script>

@endsection