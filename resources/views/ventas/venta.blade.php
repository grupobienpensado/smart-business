<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Ver Oportunidades')

<!--@section('sidebar')
    @parent    
@endsection-->

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="{{ url('/') }}/MDB/MDB Free/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="{{ url('/') }}/MDB/MDB Free/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ url('/')}}/js/plugins/VerticalTimeline/css/default.css" />
<link rel="stylesheet" type="text/css" href="{{ url('/')}}/js/plugins/VerticalTimeline/css/component.css" />
<style type="text/css">
    .btn.btn-sm {
        padding: .5rem 0.6rem;
        margin: 0px;
    }

  .titulo{
    font-weight: bold;
    color: #051d60;
    font-size: 2rem;
  }

  .subtitulo{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: small;
    color: #54575a;
  }

  .subtitulo2{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: 14px;
    color: #051d60;
    font-weight: bold;
  }

  .subtitulo3{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: 14px;
    color: #54575a;
    font-weight: bold;
  }

  .hijo{
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
    margin-bottom: auto;
    width: 50%;
    text-align: left;    
  }

  .hijo2{
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
    margin-bottom: auto;
    width: 80%;
    text-align: left;    
  }

  .tex-hijo{
    display:inline-block;
    vertical-align:middle;
    line-height:normal;
  }

  .alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -ms-border-radius: 2px;
    -o-border-radius: 2px;
    border-radius: 0px;
    border: solid 1px rgba(0, 0, 0, 0.09);
  }

  .list-group-item{
    padding-top: 5px;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }

  .panel-view{
    -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    margin-top: 20px;
    margin-bottom: 20px;
    background-color: #fff;
    width: 80%;
  }

   .profile-empresa{
    margin-right: auto!important;
    margin-left: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    width: 250px;
    height: 250px;
    -webkit-border-radius: 200px 200px 200px 200px;
    position: relative;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  .img-empresa{
    padding: 30px;
    border: solid 0px;
    line-height: 140;
    position: absolute;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center;
    top: 50%;
    bottom: 50%;
  }

  .mx-auto {
    margin-right: auto!important;
    margin-left: auto!important;
    margin-bottom: auto;
    margin-top: auto;
  }

    img {
        border: solid 0px;
    }

    .observaciones-img{
        width: 60px;
    }

    .centrar-div{
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .tr-table{
        background-color: #063F9C;
        color: aliceblue;
    }

    .mb-1{
        margin-bottom: 0px !important;
        margin-top: 5px;
    }
    .centrado-v{
        display: table-cell;vertical-align: middle;
    }

    .boton {
    zoom: 2;
    vertical-align: baseline;
    cursor: pointer;
    text-decoration: none;
    font: 12px Arial;
    padding: .5em;
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
  }
  .boton:hover { text-decoration: none; }
   
  /* Redondez */
   
  .redondo {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
  }
   
  /* Color */
   
  .negro {
    color: #051d60;
    border: solid 1px #d7d7d7;
    background: #fff;
  }
  .negro:hover {
    background: #000;
    background: -webkit-gradient(linear, left top, left bottom, from(#444), to(#000));
    background: -moz-linear-gradient(top,  #444,  #000);
    filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#000000');
  }
.tabs.active {
    margin: 0.5px;
    background-color: #5b5d5c;
    color: #fff;
    padding: 9px;
    background-image: -moz-linear-gradient( 90deg, rgb(81,80,80) 0%, rgb(133,133,133) 49%, rgb(185,185,185) 100%);
     background-image: -webkit-linear-gradient( 90deg, rgb(81,80,80) 0%, rgb(133,133,133) 49%, rgb(185,185,185) 100%);
     background-image: -ms-linear-gradient( 90deg, rgb(81,80,80) 0%, rgb(133,133,133) 49%, rgb(185,185,185) 100%);
     box-shadow: 0px 2px 5px 0px rgba(9, 42, 33, 0.6),inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);

}
.alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img{
  border:none !important;
}
.tabs{
    width: 24%;
    text-align: center;
    margin: 0.5px 3.5px !important;
    background-color: #1f63dd;
    color: #fff;
    padding: 5px;
    border: 4px solid #d1d1d1;
    border-radius: 50px;
    background-image: -moz-linear-gradient( 90deg, rgb(1,62,124) 0%, rgb(3,144,245) 100%);
     background-image: -webkit-linear-gradient( 90deg, rgb(1,62,124) 0%, rgb(3,144,245) 100%);
     background-image: -ms-linear-gradient( 90deg, rgb(1,62,124) 0%, rgb(3,144,245) 100%);
     box-shadow: 0px 2px 5px 0px rgba(9, 42, 33, 0.6),inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);

}
.tabs a {
    color: #fff;
}
</style> 
<style type="text/css">
.panel-group {
    margin-top: 30px;
    margin-bottom: 20px;
}
.panel-group .panel {
    margin-bottom: 0;
    border-radius: 4px;
}
.panel-default {
    border-color: #ddd;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-default>.panel-heading {
    color: #FFF;
    background-color: #02498c;
    border-color: #ddd;
}
.panel-group .panel-heading {
    border-bottom: 0;
}
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: inherit;
}
.panel-title>.small, .panel-title>.small>a, .panel-title>a, .panel-title>small, .panel-title>small>a {
    color: inherit;
}
[role=button] {
    cursor: pointer;
}
.collapse.in {
    display: block;
}
.collapse {
    display: none;
}
.panel-default>.panel-heading+.panel-collapse>.panel-body {
    border-top-color: #ddd;
}
.panel-group .panel-heading+.panel-collapse>.list-group, .panel-group .panel-heading+.panel-collapse>.panel-body {
    border-top: 1px solid #ddd;
}
.panel-body {
    padding: 15px;
}
.btn-group-vertical>.btn-group:after, .btn-group-vertical>.btn-group:before, .btn-toolbar:after, .btn-toolbar:before, .clearfix:after, .clearfix:before, .container-fluid:after, .container-fluid:before, .container:after, .container:before, .dl-horizontal dd:after, .dl-horizontal dd:before, .form-horizontal .form-group:after, .form-horizontal .form-group:before, .modal-footer:after, .modal-footer:before, .modal-header:after, .modal-header:before, .nav:after, .nav:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .navbar:after, .navbar:before, .pager:after, .pager:before, .panel-body:after, .panel-body:before, .row:after, .row:before {
    display: table;
    content: " ";
}
.nav-tabs{
    border: 0 !important;
}
.archivo-carpeta{
    height: 64px;
    width: 64px;
    border: 0.5px solid #ccc;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: 0;
    display: inline-block;
    margin: 5px;
    border-radius: 5px;
    position: relative;
    text-align: center;
}
.archivo-carpeta span{
    position: absolute;
    bottom: -13px;
    left: 0;
    right: 0;
    font-size: 12px;
    font-weight: bold;
}

.logo-essi-modal {
    margin-top: 15px !important;
    left: 0;
    position: absolute;
}
.subidor{
    height: 150px;
    width: 150px;
    background-size: cover;
    background-repeat: no-repeat;
}
.info-file{
    width: 305px;
    display: inline-block;
    color: #000;
    padding: 10px;
    text-align: left;
}
.black-color{
    color: #000;
    font-size: .8rem !important;
}
.justificado{
    text-align: justify;
    width: 100%;
}
.subidor-foto{
    height: 150px;
    width: 150px;
    background-size: cover;
    background-repeat: no-repeat;
    display: inline-block;
    text-align: right;
    border: 0.5px solid #ccc;
    border-radius: 100%;
}
.caja-comentarios{
    border: 0.5px solid #ccc;
    height: 150px;
    overflow-y: scroll;
    background-color: #ececec;
}
.comment{
    margin: 8px;
    border: 0.5px solid #ccc;
    background-color: #fff;
    border-radius: 5px;
    padding: 5px;
    text-align: left;
}
.margen-50{
    margin-top: 60px;
}
.icono{
    text-align: center;
    right: 0;
    position: absolute;
    top: 0;
    margin: 5px;
    border: 0.5px solid;
    border-radius: 100%;
    width: 25px;
    height: 25px;
}
.nombre-file{
    height: 75px;
    position: absolute;
    width: 100%;
    line-height: 1;
}
.foto-comenta{
    background-size: cover;
    background-repeat: no-repeat;
    border: 0.5px solid;
    margin-right: 10px;
    font-size:cover;
    border-radius:100%; 
    height:60px;
    width:60px;
    float:left;
}
a{
    color: #FFF !important;
}
</style>
<style type="text/css">
.nav>li>a{
    padding: 0 !important;
    background-color: transparent !important;
  }
  .nav-tabs>li>a:hover{
    background-color: transparent !important;
    border: none !important;
  }
  .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
    background-color: transparent !important;
    border: none !important;
    padding-top: 3px;
  }
.panel.panel-default{
    text-align: left
}
</style>
<div class="container-fluid animated flipInX" id="list">
    <div class="row justify-content-md-center">
        <div class="col-12 col-md-auto panel-view">
        <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
                    </div>
                </div>
            </div>
        <ul class="nav nav-tabs" role="tablist">
          <li class="tabs active"><a href="#contrato" role="tab" data-toggle="tab">Oportunidad</a></li>
          <li class="tabs"><a href="#archivos" role="tab" data-toggle="tab">Archivos</a></li>
          <li class="tabs"><a href="#transicion" role="tab" data-toggle="tab">Transición</a></li>
          
          <li class="tabs"><a href="#liquidacion" role="tab" data-toggle="tab">Ver Liquidación</a></li>
          <li class="tabs-btn"><a data-toggle="modal" data-target="#upload" aria-controls="profile">Agregar Archivos</a></li>
          <li class="tabs-btn"><a href="{{url('liquidador')}}/{{$id}}" aria-controls="profile">Liquidador de Comisiones</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="contrato" style="margin-top: 30px;">
            <div class="profile-empresa">
                <img src="
                   @if(!empty($empresa->logo))
                      {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
                   @else
                      https://placeholdit.imgix.net/~text?txtsize=250&txt=Logo&w=250&h=250 
                   @endif" class="img-fluid mx-auto d-block img-empresa">
            </div>
            <div class="row">
                <div class="col-6 col-md-6 centrado">
                    <div  class="hijo">
                        <h2 class="titulo">
                            {{ $empresa->nombre }} 
                            <span class="subtitulo">                            
                            </span>
                        </h2>
                        <p class="subtitulo">
                            <span class="subtitulo2">{{ $empresa->nombre_p_a_cargo }}</span>/<span class="subtitulo3">{{ $empresa->cargo }}</span>
                        </p> 
                    </div>
                </div>
                <div class="col-6 col-md-6 centrado" style="padding-top: 70px">    
                    <p class="subtitulo">
                    </p>     
                </div>
                <div class="col-md-12">
                    <div class="hijo2 row justify-content-md-center" style="border: solid 1px rgba(186, 190, 193, 0.55); padding: 20px; margin-top: 20px; margin-bottom: 20px;">
                            
                            <div class="col-3 col-sm-2 placeholder img-sedes"> 
                                <p class="subtitulo2">Oportunidad</p>
                                <p class="subtitulo">{{ $oportunidad->titulo }}</p>
                            </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="hijo2 row justify-content-md-center">
                        <div class="table-responsive">
                            <table class="table product-table">
                                <thead>
                                    <tr class="tr-table">
                                        <th class="centrado">Ciclo de Venta</th>
                                        <th class="centrado">Responsable</th>
                                        <th class="centrado">Probabilidad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="centrado">
                                        </td>
                                        <td class="centrar-div">
                                            <div class="media mb-1">
                                                <a class="media-left waves-light">
                                                    <img class="img-fluid rounded-circle observaciones-img" src="https://mdbootstrap.com/img/Photos/Avatars/avatar-13.jpg" alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                    </h4>
                                                    <p class="subtitulo">xxxxxxx</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                            <h2><strong>
                                            </strong></h2>
                                        </td>                                        
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr class="tr-table">
                                        <th class="centrado">Valor Oportunidad</th>
                                        <th class="centrado">Identifico</th>
                                        <th class="centrado">UEN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                            <h2><strong> 
                                            </strong></h2>
                                        </td>
                                        <td class="centrar-div">
                                            <div class="media mb-1">
                                                <a class="media-left waves-light">
                                                    <img class="img-fluid rounded-circle observaciones-img" src="https://mdbootstrap.com/img/Photos/Avatars/avatar-13.jpg" alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                    </h4>
                                                    <p class="subtitulo">xxxxxxx</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                            <h5><strong>
                                            </strong></h5>
                                        </td>                                        
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr class="tr-table">
                                        <th class="centrado">Valor en Pesos</th>
                                        <th class="centrado">Margen</th>
                                        <th class="centrado">Segmento</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                            <h2><strong>
                                            </strong></h2>
                                        </td>
                                        <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                            <h2><strong>
                                            </strong></h2>
                                        </td>
                                        <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center">
              <div class="col-md-12">
                <p class="subtitulo2 centrado" style="padding-top: 50px;">Forma de pago</p><a name="titulobservaciones"></a>
              </div>
              <div class="col-md-12 centrado">
                <div class="hijo2">
                    <p class="subtitulo" style="font-size: larger;">
                    </p>
                </div>
              </div>
            </div>
            <div class="row justify-content-md-center">
              <div class="col-md-12">
                <p class="subtitulo2 centrado" style="padding-top: 50px;">Observaciones</p><a name="titulobservaciones"></a>
              </div>
              <div class="col-md-12 centrado">
                <!--First review-->
                <div class="hijo2">
                  <div class="media mb-1">
                      <a class="media-left waves-light">
                          <img class="rounded-circle observaciones-img" src="https://mdbootstrap.com/img/Photos/Avatars/avatar-13.jpg" alt="Generic placeholder image">
                      </a>
                      <div class="media-body">
                          <h4 class="media-heading observaciones-title"><?php $user = App\User::findOrFail($oportunidad->user_id); echo $user->nombres ." ". $user->apellidos; ?></h4>
                          <p class="subtitulo">
                            </p>

                            
                          </p>
                      </div>
                  </div>
                  
                </div>
              </div>
            </div>
            </div>
            <div class="tab-pane fade" id="archivos">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                  <div class="panel panel-default">
                    <div class="panel-heading relativo" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" data-name="contratos" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Contrato y Orden de Compra ( Archivos)
                          
                        </a>
                      </h4>
                      <span class="icono abrir-contratos"><i class="fa fa-angle-right"></i></span>
                      <span class="icono cerrar-contratos" style="display: none"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading relativo" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" data-name="propuestas" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Propuesta ( Archivos)
                          
                        </a>
                      </h4>
                      <span class="icono abrir-propuestas"><i class="fa fa-angle-right"></i></span>
                      <span class="icono cerrar-propuestas" style="display: none"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading relativo" role="tab" id="headingFour">
                      <h4 class="panel-title">
                        <a class="collapsed" data-name="apu" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          APU
                        </a>
                      </h4>
                      <span class="icono abrir-apu"><i class="fa fa-angle-right"></i></span>
                      <span class="icono cerrar-apu" style="display: none"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        <a class="btn btn-success" style="width: 100%" href="{{url('/')}}/apu/{{$id}}" target="_blank">Agregar APU</a>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading relativo" role="tab" id="headingFive">
                      <h4 class="panel-title">
                        <a class="collapsed" data-name="anexos" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                          Anexos ( Archivos)
                        </a>
                        
                      </h4>
                      <span class="icono abrir-anexos"><i class="fa fa-angle-right"></i></span>
                      <span class="icono cerrar-anexos" style="display: none"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                      <div class="panel-body">
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="tab-pane fade" id="transicion">
            bxcvbxcncvbn
            </div>
            <div class="tab-pane fade" id="liquidacion">
               <div class="panel panel-default">
               <div class="row">
                   <div class="col-md-12">
                           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                             <thead>
                               <tr> 
                                <th class="text-center">#</th>
                                 <th class="text-center">Descripción</th>
                                 <th class="text-center">observaciones</th>
                                 <th class="text-center">Valor</th>
                               </tr>
                             </thead>
                             <tbody>           
                             </tbody>
                             <tfoot>
                                 <tr>
                                     <td colspan="3" style="text-align: right; font-weight: bold">Total:</td>
                                     <td class="text-center" style="font-weight: bold">%</td>
                                 </tr>
                             </tfoot>
                           </table>
                   </div>
               </div>
               </div> 
            </div>
        </div>
        </div>
    </div>
</div>   

<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Cargar Archivos</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('upload_sale') }}" enctype="multipart/form-data">
    <input type="hidden" name="id" value="{{$id}}">
        {{ csrf_field() }}
            <div class=" form-group row">
              <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-6" style="justify-content: center;">
                        <label for="nombre" class="col-form-label">Archivo</label>
                        <input id="input-1" type="file" class="file" name="file">
                    </div>
                    <div class="col-6" style="justify-content: center;">
                        <label for="carpeta" class="col-form-label">Carpeta</label>
                        <select class="form-control" name="carpeta">
                            <option value="">Seleccione categoria</option>
                            <option value="contrato">Contrato y Orden De Compra</option>
                            <option value="propuesta">Propuesta</option>
                            <option value="anexos">Anexos</option>
                        </select>
                        <label for="nombre" class="col-form-label">Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                        <div class=" form-group row">
                          <div class="col-md-12">
                            <label for="observaciones" class="col-form-label">Observaciones</label>
                                <textarea class="form-control" name="descripcion" rows="5" placeholder="Por favor ingrese una observacion" required></textarea>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
          <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
        </form>
    </div>
    </div>
  </div>
</div>    
<div class="modal fade" id="ver_archivos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Ver Información</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" id="cuerpo_comment">
        
    </div>
    </div>
  </div>
</div>

    <!-- modal de cambios -->
    <div class="modal fade" id="exampleModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Historial de cambios</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="table-responsive">            
                <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                  <thead>
                    <tr>
                      <th class="centrado">Fecha</th>
                      <th class="centrado">Valor</th>                 
                    </tr>
                  </thead>
                  <tbody id="idTable">                 
                    
                  </tbody>
                </table>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- fin modal de cambios -->

    <!-- modal de cambios -->
    <div class="modal fade" id="detalleModal">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Archivo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="cuerpo_archivo">
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-essi" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- fin modal de cambios -->

@endsection


@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/js/mdb.min.js" crossorigin="anonymous"></script>
<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>

<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/tether.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/mdb.min.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
  <!--sweetalert -->
<script type="text/javascript" src="{{ url('/') }}/js/plugins/sweetalert/sweetalert.min.js"></script>


<script src="{{ url('/') }}/components/3Dtimeline/js/modernizr.custom.63321.js"></script>
<script src="{{ url('/')}}/js/plugins/VerticalTimeline/js/modernizr.custom.js"></script>
<script type="text/javascript">

    direccion="{{ url('/') }}/images/file/ventas/";

    $(function() {
        getComentarios({{ $oportunidad->id }});
    });
    /*$('.tabs a').click(function (e) {
      e.preventDefault()
      
      $(".tabs").removeClass("active");
      $(this).parent().addClass("active");
      //$(this).tab('show');
      $(".fade").each(function(e){
        $(this).attr("aria-expanded",false);
      })
      id=$(this).attr("href").split("#").join("");
      $("#"+id).attr("aria-expanded",true);
    })*/

    $("body").on("click",".panel-title a",function(e){
        $(this).attr("data-name")

        if(!$(".abrir-"+$(this).attr("data-name")).is(":visible")){
            $(".abrir-"+$(this).attr("data-name")).show("clip");
            $(".cerrar-"+$(this).attr("data-name")).hide("clip");
        }else{
            $(".abrir-"+$(this).attr("data-name")).hide("clip");
            $(".cerrar-"+$(this).attr("data-name")).show("clip"); 
        }
    })

    openDocument = function(url, nombre,id,ext) {
        $('#ver_archivos').modal('hide');
        $("#detalleModal").modal();
        dir = direccion+"/"+url+"/"+nombre;
        if(ext=="JPG"||ext=="jpg"||ext=="PNG"||ext=="png"||ext=="jpeg"||ext=="JPEG"){
            $("#cuerpo_archivo").html("<img src='"+dir+"' style='width:100%;'>")
        }else{
            contenido = `<iframe src="http://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>`;
            $("#cuerpo_archivo").html(contenido);
        }
        $.ajax({
          url: "{{ url('guardar_visita') }}/"+id,
          type: 'GET',
          success: function(data){
            
          }
        });
    }

    guardarComentario = function () {
        if(true === $("#saveComenario").parsley().validate()){
          console.log("llega"); 
          var datos = new FormData($("#saveComenario")[0]);
          $.ajax({
            url: "{{ url('guardar_comentario_archivo') }}",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
              if (data.success) {
                $('#saveComenario')[0].reset();
                $(".caja-comentarios").html(data.contenido)
              }else{
                swal("Algo salio mal, vuelve a intentar");
              }
            }
        }); 
        }else{
          swal("Faltan campos por completar!");
        }
    }
    
    getComentarios = function (id) {
        $.ajax({
          url: "{{ url('listcomenoport') }}/"+id,
          type: 'GET',
          success: function(data){
            if (data.success) {
              console.log(data.datos);
              var html3 = "";
              for(i in data.datos){
                comentario = data.datos[i];
                html3 += `
                <div class="media mb-1">
                  <a class="media-left waves-light">
                      <img class="rounded-circle observaciones-img" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(18)-mini.jpg" class="rounded-circle z-depth-1-half" alt="Generic placeholder image">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading observaciones-title">`+comentario.user.nombres+` `+comentario.user.apellidos+` <small>`+comentario.created_at+`</small> </h4>
                      <p class="subtitulo">`+comentario.comentario+`</p>
                  </div>
                </div>`;
              }
              $("#lisComentariosId").html(html3);
            }else{

            }
          }
        });
    }    


    llenarTabla =  function (Datos) {
        if (Datos === "productos") {
            $("#idTable").html($("#"+Datos).val());
        }else{
            html = "";
            //console.log(Datos);
            Datos = $("#"+Datos).val();
            Datos = JSON.parse(Datos);

            $.each(Datos, function( index, element ) {
                html += `<tr>
                  <td>`+element["created_at"]+`</td>
                  <td>`+element["value"]+`</td>
                </tr>`;
            });
            $("#idTable").html(html);
        }
    }

    verCliente = function () {
        dato = [];
        dato["cliente"] = "{{ $cliente->cliente }}";
        dato["empresa"] = "{{ $cliente->empresa }}";
        dato["unidad_negocio"] = "{{ $cliente->unidad_negocio }}";
        dato["perfil"] = "{{ $cliente->perfil }}";
        dato["cargo"] = "{{ $cliente->cargo }}";
        dato["jefe_inmediato"] = "{{ $cliente->jefe_inmediato }}";
        dato["n_a_pesos"] = "{{ $cliente->n_a_pesos }}";
        dato["pais"] = "{{ $cliente->pais }}";
        dato["departamento"] = "{{ $cliente->departamento }}";
        dato["ciudad"] = "{{ $cliente->ciudad }}";

        html = `<label class="col-form-label fondo_title">Cliente</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["cliente"]+`
                </p>
                <label class="col-form-label fondo_title">Empresa</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["empresa"]+`
                </p>
                <label class="col-form-label fondo_title">Unidad de negocio</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["unidad_negocio"]+`
                </p>
                <label class="col-form-label fondo_title">Perfil</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["perfil"]+`
                </p>
                <label class="col-form-label fondo_title">Cargo</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["cargo"]+`
                </p>
                <label class="col-form-label fondo_title">Jefe inmediato</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["jefe_inmediato"]+`
                </p>
                <label class="col-form-label fondo_title">Valor aprovado</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    $ `+dato["n_a_pesos"]+`
                </p>
                <label class="col-form-label fondo_title">Ubicacion</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["pais"]+`, `+dato["ciudad"]+` - `+dato["departamento"]+`
                </p>`;

        $("#contenbodyModal").html(html);
    }

    verEmpresa = function() {
        nombre = "{{ $empresa->nombre }}";
        nit = "{{ $empresa->nit }}";
        pagina_web = "{{ $empresa->pagina_web }}";
        telefono = "{{ $empresa->telefono }}";
    }

$("body").on("click",".archivo-carpeta",function(e){
    console.log($(this).attr("data-name"));
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/ver_detalles_archivo", 
        cache: false,
        type: 'GET',
        data:{"id":$(this).attr("data-name")},
        success: function(e){ 
            $("#ver_archivos").modal();
           $("#cuerpo_comment").html(e.contenido);
        }
    });
})
</script>
@endsection

