@extends('template.app')
@section('title', 'APU')

@section('content')
<style type="text/css">
	.parrafo{
	    color: #555;
	    font-weight: 400;
	    font-size: 16px;
	    text-align: justify;
	    line-height: 1.2;
	}

	.form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

 	input#subtotalpro, #subtotalpromas, #subtotalgastos, .totales {
        height: 60px;
        font-size: 40px;
        text-align: right;
        font-weight: 700;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
    .dinero{
    	text-align: right;
    }

    .sumarGastos, .parrafoGastos, .totales{
    	margin-top: 8px;
    }

    .selgrande{
    	height: 60px !important;
    	font-size: 25px;
    	text-align: right;
    	font-weight: 700;
    }

    .totales2{
	    height: 40px !important;
		font-size: 20px;
		text-align: right;
		font-weight: 700;
    }

    .panel-title {
	    font-size: 18px;
	    text-align: justify;
	}

	.panel-title > a {
	    font-weight: 500 !important;
	}

	.val_collespan{
	   	position: absolute;
	    right: 70px;
	    top: 10px;
	    color: #555;
	    font-size: 18px;
	    font-weight: 400;
	}

    .negro {
        float: right;
        top: -28px;
        right: -1px;
        position: relative;
    }
</style>
<div class="widget">
	<ul class="nav nav-pills" role="tablist">
        <li><a href="{{ url('venta') }}/{{ $id }}"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
        <li><a href="{{ url('ventafileimportant') }}/{{ $id }}"><i class="fa fa-bar-chart"></i> Archivos</a></li>
        <li class="active"><a href="#" role="tab" data-toggle="tab"><i class="fa fa-line-chart"></i> APU</a></li>
        <li><a href="{{ url('ventaciclos') }}/{{ $id }}"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
        @if($data['permiso_acta_acceder']=="Si")<li><a href="{{ url('actasreunion') }}/{{ $id }}"><i class="fa fa-line-chart"></i> Actas reunion</a></li>@endif
        <li><a href="/ver-oportunidadvendida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
        <li><a href="/venta/gestionesoportunidad/<?=$data['venta']->oportunidad?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
    </ul>

	<div class="widget-header">
		<h2>APU</h2>
	</div>
	<div class="widget-content">
        <div class="row">
			<div class="col-md-12">
				<div class="panel-title">
		            <div class="pull-right">
		                <div class="btn-group">
		                    @if($data['permiso_apu_crear'] == "Si")
		                	<button class="btn btn-lg btn-warning cancelarbtn" onclick="ocultarFormulario();">
		                		<i class="fa fa-exclamation-triangle"></i>
		                		<small>Ocultar APU</small>
		                	</button>
		                    <button class="btn btn-lg btn-primary anadirbtn" onclick="verFormulario();">
		                    	<i class="fa fa-plus"></i>
		                    	<small>Añadir APU</small>
		                    </button>
		                    @endif
		                </div>
		            </div>
		        </div>
		        <br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form id="formulario" style="display: none;">

	                <!-- Create Post Form -->
	                @if (count($errors) > 0)
	                    <div class="alert alert-danger">
	                        <ul>
	                            @foreach ($errors->all() as $error)
	                                <li>{{ $error }}</li>
	                            @endforeach
	                        </ul>
	                    </div>
	                @endif
	                <!-- Fin Create Post Form -->
	                {{ csrf_field() }}

	                <div id="output"></div>
					<!--<div class="card animated flipInX" style="margin-bottom: 30px;">
						<div class="card-block">
							<h3>Costo fabricación de equipos</h3>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-5">
			                            <label for="productos" class="col-form-label">Producto/s</label>
			                        </div>
			                        <div class="col-2">
			                            <label for="productos" class="col-form-label">Referencia</label>
			                        </div>
			                        <div class="col-1">
			                            <label for="productos" class="col-form-label">Cantidad</label>
			                        </div>
			                        <div class="col-2">
			                            <label for="productos" class="col-form-label">Valor unitario</label>
			                        </div>
			                        <div class="col-2">
			                            <label for="productos" class="col-form-label">Valor total</label>
			                        </div>
			                    </div>
			                </div>

		                    <?php
                            $i = 0;
		                        /*$productosRes = App\OportunidadProducto::where("oportunidad_id",$oportunidad->id)->get();
		                        $i = 0;
		                        foreach ($productosRes as $res) {
		                            try {
		                                $i++;
		                                if (!empty($res->producto)) {
		                                  if (is_numeric($res->producto)) {
		                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
		                                  }else{
		                                    $producto->name = "Otro";
		                                  }
		                                  if (is_numeric($res->referencia)) {
		                                    $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
		                                  }else{
		                                    $referencia->referencia = "Estandar";
		                                  }
		                                }
		                        ?>
		                                    <div class="form-group">
		                                    <div class="row animated rollIn" id="listAddProductos{{ $i }}">
		                                        <div class="col-5">
		                                            <select name="productos[{{ $i }}][producto]" class="form-control" required readonly>
		                                                <option value="{{ $res->producto }}">{{ $producto->name }}</option>
		                                            </select>
		                                            <input type="hidden" name="productos[{{ $i }}][tipo]" value="fijo">
		                                        </div>
		                                        <div class="col-2">
		                                            <select name="productos[{{ $i }}][referencia]" class="form-control" readonly>
		                                                <option value="{{ $res->referencia }}">{{ $referencia->referencia }}</option>
		                                            </select>
		                                        </div>
		                                        <div class="col-1">
		                                            <input type="text" id="textCantidad{{ $i }}" class="col-12 form-control" name="productos[{{ $i }}][cantidad]" onkeyup="calcularValorTotal({{ $i }})" placeholder="0" value="<?php if (isset($res->cantidad) && !empty($res->cantidad)) { echo $res->cantidad; } ?>" readonly>
		                                        </div>
		                                        <div class="col-2">
		                                            <input type="text" id="textValor{{ $i }}" class="col-12 form-control dinero" name="productos[{{ $i }}][valor]" onkeyup="calcularValorTotal({{ $i }})" placeholder="$0" value="<?php if (isset($res->valor) && !empty($res->valor)) { echo $res->valor; } ?>">
		                                        </div>
		                                        <div class="col-2">
		                                            <div class="input-group">
		                                                <input type="text" id="numberValor{{ $i }}" class="col-12 form-control sumarTotales" name="productos[{{ $i }}][total]" placeholder="$0" value="<?php if (isset($res->total) && !empty($res->total)) { echo $res->total; } ?>" readonly>
		                                            </div>
		                                        </div>
		                                    </div>
		                                    </div>
		                                    <?php/*
		                            } catch (Exception $e) {
		                            }
		                        }*/
		                    ?>

			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-6">
			                        </div>
			                        <div class="col-2">
			                            <h2>Total</h2>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="subtotalpro" class="col-12 form-control" name="t_equipos_f" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                </div>
		                </div>
	                </div>-->
	                <div class="card animated flipInX" style="margin-bottom: 30px;">
	                    <div class="card-block">
	                       <h3>Nombre del APU</h3>
	                       <div class="row">
	                           <div class="col-md-12">
	                               <input type="text" class="form-control" id="nombre_apu" name="nombre_APU" placeholder="Ingrese el nombre del APU">
	                           </div>
	                       </div>
	                    </div>
	                </div>
	                <div class="card animated flipInX" style="margin-bottom: 30px;">
						<div class="card-block">
							<h3>Costo fabricación de equipos</h3>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-5">
			                            <label for="productos" class="col-form-label">Producto/s</label>
			                        </div>
			                        <div class="col-2">
			                            <label for="productos" class="col-form-label">Referencia</label>
			                        </div>
			                        <div class="col-1">
			                            <label for="productos" class="col-form-label">Cantidad</label>
			                        </div>
			                        <div class="col-2">
			                            <label for="productos" class="col-form-label">Valor unitario</label>
			                        </div>
			                        <div class="col-2">
			                            <label for="productos" class="col-form-label">Valor total</label>
			                        </div>
			                    </div>
			                </div>

			                <div class="form-group" id="listProductos">
			                    <div class="row">
			                        <div class="col-5">
			                            <select id="selectProducto0" name="productos[0][producto]" class="form-control productos0" onchange="buscarDimSelec2(0)"></select>
			                            <input type="hidden" name="productos[0][tipo]" value="dinamico">
			                        </div>
			                        <div class="col-2">
			                            <select id="referencia0" name="productos[0][referencia]" class="form-control referencia0"></select>
			                        </div>
			                        <div class="col-1">
			                            <input type="text" id="textCantidad0" class="col-12 form-control" name="productos[0][cantidad]" onkeyup="calcularValorTotal(0)" placeholder="0">
			                        </div>
			                        <div class="col-2">
			                            <input type="text" id="textValor0" class="col-12 form-control dinero" name="productos[0][valor]" onkeyup="calcularValorTotal(0)" placeholder="$0">
			                        </div>
			                        <div class="col-2">
			                            <div class="input-group">
			                                <input type="text" id="numberValor0" class="col-12 form-control sumarTotalesmas" name="productos[0][total]" placeholder="$0" readonly>
			                                <span class="input-group-btn">
			                                    <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarProductos()"><i class="fa fa-plus text-success"></i></button>
			                                </span>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-6">
			                        </div>
			                        <div class="col-2">
			                            <h2>Total</h2>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="subtotalpromas" class="col-12 form-control" name="t_equipos_d" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                </div>
		                </div>
	                </div>
	                <div class="card animated flipInX" style="margin-bottom: 30px;">
						<div class="card-block">
							<h3>Otros Gastos</h3>
							<div class="form-group" id="listProductos">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Tiquetes aereos y viaticos del cliente:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="tiquetescliente" class="col-12 form-control sumarGastos dinero" name="tiquetescliente" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Tiquetes y viaticos funcionarios essi:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="tiquetesessi" class="col-12 form-control sumarGastos dinero" name="tiquetesessi" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">otras gestiones comerciales:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="otrasgestiones" class="col-12 form-control sumarGastos dinero" name="otrasgestiones" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Comisiones para terceros:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="comisionesterceros" class="col-12 form-control sumarGastos dinero" name="comisionesterceros" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
									</div>
									<div class="col-md-6">
										<div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Costos de diseño para nuevos sistemas:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="costosdiseño" class="col-12 form-control sumarGastos dinero" name="costosdiseno" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Guacales:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="guacales" class="col-12 form-control sumarGastos dinero" name="guacales" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Gastos de transporte:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="gastostrasporte" class="col-12 form-control sumarGastos dinero" name="gastostrasporte" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
					                    <div class="row" style="display: none;">
					                        <div class="col-3">
					                            <p class="parrafoGastos">Otros gastos:</p>
					                        </div>
					                        <div class="col-5">
					                            <input type="text" id="otrosgastos" class="col-12 form-control sumarGastos dinero" name="otrosgastos" onkeyup="mostarValorTotal()" placeholder="$0">
					                        </div>
					                    </div>
									</div>
								</div>
					            <div class="row mx-4" style="background-color: #0000000d;">
					                <div class="col-md-12 text-center">
                                    <p class="h5"><strong>Otros Gastos</strong></p>
					                    <a class="text-success" onclick="addotrogasto()" style="font-size:2rem;cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Agregar un gasto adicional"><i class="fa fa-plus"></i></a>
					                </div>
					            </div>
                                <div class="row mx-4" style="background-color: #0000000d;">
                                    <div class="col-md-1">
                                        <strong>Item</strong>
                                    </div>
                                    <div class="col-md-5">
                                        <strong>Concepto</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <strong>Valor</strong>
                                    </div>
                                </div>
                                <div class="row mx-4" id="contenedor_otros_gastos" style="background-color: #0000000d;">
                                    <div class="col-md-1 gasto-0">
                                        <strong class="numero">1</strong>
                                    </div>
                                    <div class="col-md-5 gasto-0">
                                        <input type="text" class="form-control" name="otrogasto[0][concepto]" maxlength="199" required>
                                    </div>
                                    <div class="col-md-5 gasto-0">
                                        <input type="text" style="margin-top:0px !important;" class="form-control sumarGastos dinero" name="otrogasto[0][valor]" placeholder="$ 0" onkeyup="mostarValorTotal()" id="otros_gastos0" required>
                                    </div>
                                    <div class="col-md-1 gasto-0">
                                        <a class="text-danger" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="top" title="Eliminar gasto adicional" onclick="deletegasto(0)"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
			                </div>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-6">
			                        </div>
			                        <div class="col-2">
			                            <h2>Total</h2>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="subtotalgastos" class="col-12 form-control" name="t_otros_gastos" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-4">
			                        </div>
			                        <div class="col-4">
			                            <h2>Total final</h2>
			                        </div>
			                        <div class="col-2">
			                            <h2>Moneda</h2>
			                        </div>
			                        <div class="col-2">
			                            <h2>Tasa de cambio</h2>
			                        </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-4">
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="totalFinal" class="col-12 form-control dinero totales" name="tf_final" placeholder="$0" readonly>
			                        </div>
			                        <div class="col-2">
			                            <?php
				                            $otro = $datos->filter(function ($value, $key) {
				                              return $value->key == "moneda";
				                            });
				                            $casi = $otro->pop();
				                        ?>
				                        <input type="text" class="col-12 form-control totales" value="{{ $casi->value }}" name="tf_moneda" placeholder="Moneda" readonly>
			                        </div>
			                        <div class="col-2">
									    <?php
				                            $otro = $datos->filter(function ($value, $key) {
				                              return $value->key == "tasa_cambio";
				                            });
				                            $casi = $otro->pop();
				                            $cantidad = str_replace(',','',$casi->value);
				                            $cantidad = (int)$cantidad;
				                            $cantidad = number_format($cantidad);
				                        ?>
			                            <input type="text" id="tasaCambio" class="col-12 form-control dinero totales" name="tf_tasa_cambio" placeholder="0" value="{{ $cantidad }}">
			                        </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <div class="row">
			                        <div class="col-6">
			                        </div>
			                        <div class="col-2">
			                            <h2>Total pesos</h2>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="valTotalPesos" class="col-12 form-control totales" name="tf_pesos" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                </div>
			                <hr>
			                <div class="form-group">
			                	<h2 style="text-align: center;">Datos de venta</h2>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>Valor venta:</h3>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="newvalorventa" class="col-12 form-control totales2 dinero" name="dv_venta" placeholder="$0">
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>Moneda:</h3>
			                        </div>
			                        <div class="col-4">
				                        <?php
				                            $otro = $datos->filter(function ($value, $key) {
				                              return $value->key == "moneda";
				                            });
				                            $casi = $otro->pop();
				                        ?>
				                        <select class="form-control totales2" name="dv_moneda" id="selec_moneda" required>
				                            <option value="EUR" <?php if ($casi->value === "EUR"){echo "selected"; } ?>>Euro (€)</option>
				                            <option value="COP" <?php if ($casi->value === "COP"){echo "selected"; } ?>>Colombia Peso (COP)</option>
				                            <option value="USD" <?php if ($casi->value === "USD"){echo "selected"; } ?>>US Dollar (USD)</option>
				                        </select>
				                    </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>Tasa pactada:</h3>
			                        </div>
			                        <div class="col-4">
			                        	<?php
				                            $otro = $datos->filter(function ($value, $key) {
				                              return $value->key == "tasa_cambio";
				                            });
				                            $casi = $otro->pop();
				                            $cantidad = str_replace(',','',$casi->value);
				                            $cantidad = (int)$cantidad;
				                            $cantidad = number_format($cantidad);
				                        ?>
			                            <input type="text" id="tasaPActada" class="col-12 form-control totales2" value="{{ $cantidad }}" name="dv_tasa_pactada" placeholder="$0">
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>Valor pesos:</h3>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="tovalorpesos" class="col-12 form-control totales2" name="dv_valor_pesos" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>Financiación:</h3>
			                        </div>
			                        <div class="col-2">
			                        	<div class="input-group">
									      <input type="number" id="numfinanciacion" class="form-control totales2" name="dv_financiacion_p" placeholder="0%">
									      <span class="input-group-addon totales2">%</span>
									    </div>
			                        </div>
			                        <div class="col-2">
			                        	<input type="text" id="numdeftotal" class="col-12 form-control totales2" name="dv_financiacion_r" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                    <br><br>
			                    <h2 style="text-align: center;">Datos de Utilidad</h2>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>+ Valor venta:</h3>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="numValVenta" class="col-12 form-control totales2" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>- Costo fabricación:</h3>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="costofabricacion" class="col-12 form-control totales2" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h3>- Financiación:</h3>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="financiacion2" class="col-12 form-control totales2" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h2>= Utilidad:</h2>
			                        </div>
			                        <div class="col-4">
			                            <input type="text" id="numUtilidad" class="col-12 form-control totales" name="du_utilidad" placeholder="$0" readonly>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-5">
			                        </div>
			                        <div class="col-3">
			                            <h2>= Margen bruto:</h2>
			                        </div>
			                        <div class="col-4">
			                        	<div class="input-group">
			                        		<input type="text" id="margenBruto" class="col-12 form-control totales" name="du_margen_bruto" placeholder="$0" readonly>
											<span class="input-group-addon totales">%</span>
									    </div>
			                        </div>
			                    </div>
			                </div>

		                </div>
	                </div>
	                <button type="button" class="btn btn-essi pull-right btn-submit col-2"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>

                </form>
			</div>
			<div class="col-md-12">
				<div class="panel-group" id="accordion2">

				</div>
			</div>

           <div class="col-md-12">
				<div class="panel-group">
                    <div class="row justify-content-md-center">
                        <a name="titulobservaciones"></a>
                        <div class="col-md-12">
                            <h5 class="card-header"> Observaciones </h5>
                        </div>
                        <div class="col-md-12 centrado">
                             <div id="lisComentariosId"></div>
                              <form id="formComentarios">
                                {{ csrf_field() }}
                                <input type="hidden" name="oportunidad_id" value="{{ $oportunidad->id }}">
                                <textarea type="text" class="form-control" name="comentario" placeholder="Añadir comentario" style="border-right: 0;width: 98%;"></textarea>
                              </form>
                              <div class="centrado">
                                <a href="#" class="boton negro redondo" onclick="guardarComentario({{ $oportunidad->id }})"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                              </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ url('/') }}/js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>

<script type="text/javascript">
    var html_formulario = $('#formulario').html();
     guardarComentario = function (id) {
        event.preventDefault();
        if(true === $("#formComentarios").parsley().validate()){
          console.log("llega");
          var datos = new FormData($("#formComentarios")[0]);
          $.ajax({
            url: "{{ url('savecomenapu') }}",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
              if (data.success) {
                $('#formComentarios')[0].reset();
                getComentarios(id);
              }else{
                swal("Algo salio mal, vuelve a intentar");
              }
            }
        });
        }else{
          swal("Faltan campos por completar!");
        }
    }

     getComentarios = function (id) {
        $.ajax({
          url: "{{ url('listcomenapu') }}/"+id,
          type: 'GET',
          success: function(data){
            if (data.success) {
              console.log(data.datos);
              var html3 = "";
              moment.locale('es');
              for(i in data.datos){
                comentario = data.datos[i];
                if (comentario.user.foto === "" || comentario.user.foto === null) {
                    fotod = `<img src="http://via.placeholder.com/40x40?text=`+comentario.user.name+`" class="media-object img-circle">`;
                }else{
                    fotod = `<img src="{{url('/')}}/images/file/clientes/`+comentario.user.foto+`" alt="`+comentario.user.name+`" class="media-object img-circle" style="max-width:40px">`;
                }
                html3 += `
                <div class="media clearfix header-bottom">
                      <div class="media-left">
                        `+fotod+`
                      </div>
                      <div class="media-body" style="text-align: start;">
                        <label style="text-align: start;font-size: 14px !important;">
                          `+comentario.user.nombres+` `+comentario.user.apellidos+` <small>`+moment(comentario.created_at).calendar()+`</small>
                        </label><br>
                        <p class="text-muted username soloprimer normalp" style="font-size: 15px;font-weight: 400;">`+comentario.comentario+`</p>
                      </div>
                 </div>`;
              }
              $("#lisComentariosId").html(html3);
            }else{

            }
          }
        });
    }
	i={{ $i }};

	function verFormulario() {
		$("#formulario").show("slow");
		$(".anadirbtn").hide("slow");
		$(".cancelarbtn").show("slow");
	}

	function ocultarFormulario() {
		$("#formulario").hide("slow");
		$(".anadirbtn").show("slow");
		$(".cancelarbtn").hide("slow");
	}

	mostrarlistadoapu = function() {
		var request = $.ajax({
		  url: "{{ url('apulist') }}/{{ $oportunidad->id }}",
		  type: "GET",
		  processData: false,
		  contentType: false
		});

		request.done(function( msg ) {
		  	if (msg.success) {
		  		moment.locale('es');
				console.log("Exito!","APU guardado con exito.","success");
				ocultarFormulario();

				var html = '';
				$("#accordion2").html(html);
                var ik =  msg.datos.length + 1;
				$.each(msg.datos, function(k, dato) {
                    dato.nombre_apu = dato.nombre_apu != null ? dato.nombre_apu : "APU Sin Nombre";
                    dato.dv_financiacion_p = dato.dv_financiacion_p != null ? dato.dv_financiacion_p : 0;
					ik = ik - 1;
					html = `

					<div class="panel panel-default">
						<div class="panel-heading">

                            <div class="row">
                                <div class="col-md-7">
                                    <h3 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne`+ik+`"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i> `+dato.nombre_apu+`</a>
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <p class="h3 panel-title mt-1">
                                        <span style="color: #948686;">`+moment(dato.created_at).calendar()+`</span>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title mt-4">
                                        <span style="position: relative;color: #948686;">Utilidad:</span>
                                        <span class="pull-right" style="position: relative;color: #948686;">$ `;
                                        if (dato.du_utilidad) { html += dato.du_utilidad; }else{ html += "0"; }
                                        if (dato.du_margen_bruto) { html += ` (`+dato.du_margen_bruto+`%)`; }else{ html += " (0%)"; }
                                        html += `</span>
                                    </h3>
                                </div>
                            </div>
						</div>
						<div id="collapseOne`+ik+`" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="card animated flipInX" style="width: 100%;display:none;">
									<div class="card-block">
										<h3>Costo fabricación de equipos</h3>
										<div class="col-8">
						                <table class="table table-condensed table-dark-header">
											<thead>
												<tr>
													<th>Item</th>
													<th>Producto</th>
													<th>Referencia</th>
													<th>Cantidad</th>
													<th>Valor unitario</th>
													<th>Valor total</th>
												</tr>
											</thead>
											<tbody>`;
							                var ij = 0;
							                $.each(dato.equipos, function(j, equipo) {
							                	if (equipo.tipo === "fijo") {
							                		ij++;
							                		html += `
													<tr>
														<td>`+ij+`</td>
														<td>`+equipo.producto+`</td>
														<td>`+equipo.referencia+`</td>
														<td class="dinero">`+equipo.cantidad+`</td>
														<td class="dinero">$ `+equipo.valor+`</td>
														<td class="dinero">$ `+equipo.total+`</td>
													</tr>`;
				                                }
							                });

							                html += `
						                		<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td class="dinero"><h3>Total: $ `+dato.t_equipos_f+`</h3></td>
												</tr>
						                	</tbody>
										</table>
										</div>
					                </div>
				                </div>
				                <div class="card animated flipInX" style="width: 100%;">
									<div class="card-block">
										<h3>Costo Fabricación Equipos</h3>
										<div class="col-8">
										<table class="table table-condensed table-dark-header">
											<thead>
												<tr>
													<th>Item</th>
													<th>Producto</th>
													<th>Referencia</th>
													<th>Cantidad</th>
													<th>Valor unitario</th>
													<th>Valor total</th>
												</tr>
											</thead>
											<tbody>`;
						                var ij = 0;
						                $.each(dato.equipos, function(j, equipo) {
						                	if (equipo.tipo === "dinamico") {
						                		ij++;
						                		html += `
						                		<tr>
													<td>`+ij+`</td>
													<td>`+equipo.producto+`</td>
													<td>`+equipo.referencia+`</td>
													<td class="dinero">`+equipo.cantidad+`</td>
													<td class="dinero">$ `+equipo.valor+`</td>
													<td class="dinero">$ `+equipo.total+`</td>
												</tr>`;
			                                }
						                });

						                html +=`
						                		<tr>
													<td></td>
													<td></td>
													<td></td>
													<td colspan="3" class="dinero"><h3>Total: $ `+dato.t_equipos_d+`</h3></td>
												</tr>
						                	</tbody>
										</table>
										</div>
					                </div>
				                </div>
				                <div class="card animated flipInX" style="width: 100%;">
									<div class="card-block">
										<h3>Otros gastos del proyecto</h3>
										<div class="col-5">
											<table class="table table-condensed table-dark-header">
												<thead>
													<tr>
														<th>Item</th>
														<th>Gasto</th>
														<th>Valor</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Tiquetes aereos y viaticos del cliente</td>
														<td class="dinero">$ `;
														if (dato.tiquetescliente) { html += dato.tiquetescliente; }else{ html += "0"; }
														html +=`</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Tiquetes y viaticos funcionarios essi</td>
														<td class="dinero">$ `;
														if (dato.tiquetesessi) { html += dato.tiquetesessi; }else{ html += "0"; }
														html +=`</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Otras gestiones comerciales</td>
														<td class="dinero">$ `;
														if (dato.otrasgestiones) { html += dato.otrasgestiones; }else{ html += "0"; }
														html +=`</td>
													</tr>

													<tr>
														<td>4</td>
														<td>Comisiones para terceros</td>
														<td class="dinero">$ `;
														if (dato.comisionesterceros) { html += dato.comisionesterceros; }else{ html += "0"; }
														html +=`</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Costos de diseño para nuevos sistemas</td>
														<td class="dinero">$ `;
														if (dato.costosdiseno) { html += dato.costosdiseno; }else{ html += "0"; }
														html +=`</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Guacales</td>
														<td class="dinero">$ `;
														if (dato.guacales) { html += dato.guacales; }else{ html += "0"; }
														html +=`</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Gastos de transporte</td>
														<td class="dinero">$ `;
														if (dato.gastostrasporte) { html += dato.gastostrasporte; }else{ html += "0"; }
														html +=`</td>
													</tr>
													<tr>
														<td>8</td>
														<td>Otros gastos</td>
														<td class="dinero">$ `;
														if (dato.otrosgastos) { html += dato.otrosgastos; }else{ html += "0"; }
														html +=`</td>
													</tr>
                                                    `+msg.data['otros_gastos'][dato.id]+`
													<tr>
														<td></td>
														<td colspan="2" class="dinero"><h3>Total: $ `+dato.t_otros_gastos+`</h3></td>
													</tr>
												</tbody>
											</table>
										</div>
						                <div class="form-group">
						                    <div class="row">
						                        <div class="col-6">
						                        </div>
						                        <div class="col-6">
													<table class="table table-condensed table-dark-header">
														<thead>
															<tr>
																<th class="centrado" style="width: 40%;">Total final</th>
																<th class="centrado" style="width: 30%;">Moneda</th>
																<th class="centrado" style="width: 30%;">Tasa de cambio</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="centrado"><h3>$ `+dato.tf_final+`</h3></td>
																<td class="centrado"><h3>`+dato.tf_moneda+`</h3></td>
																<td class="centrado"><h3>`+dato.tf_tasa_cambio+`</h3></td>
															</tr>
															<tr>
																<td colspan="3" class="dinero"><h3>Total pesos $ `+dato.tf_pesos+`</h3></td>
															</tr>
														</tbody>
													</table>
						                        </div>
						                    </div>
						                </div>
						                <hr>
						                <div class="form-group">
						                	<h2 style="text-align: center;">Datos de venta</h2>
						                    <div class="row">
						                        <div class="col-6">
						                        </div>
						                        <div class="col-6">
						                        	<table class="table table-condensed table-dark-header">
														<thead>
															<tr>
																<th>Item</th>
																<th>Tipo</th>
																<th>Valor</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>1</td>
																<td>Valor venta</td>
																<td class="dinero">$ `+dato.dv_venta+`</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Moneda</td>
																<td class="dinero">`+dato.dv_moneda+`</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Tasa pactada</td>
																<td class="dinero">`+dato.dv_tasa_pactada+`</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Valor pesos</td>
																<td class="dinero">$ `+dato.dv_valor_pesos+`</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Financiación</td>
																<td class="dinero">`+dato.dv_financiacion_p+` % - $ `+dato.dv_financiacion_r+`</td>
															</tr>
														</tbody>
													</table>
						                        </div>
						                    </div>
						                    <h2 style="text-align: center;">Datos de Utilidad</h2>
						                    <div class="row">
						                        <div class="col-6">
						                        </div>
						                        <div class="col-6">
						                        	<table class="table table-condensed table-dark-header">
														<thead>
															<tr>
																<th></th>
																<th>Tipo</th>
																<th>Valor</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td><strong>+<strong></td>
																<td>Valor venta</td>
																<td class="dinero">$ `+dato.dv_valor_pesos+`</td>
															</tr>
															<tr>
																<td><strong>-<strong></td>
																<td>Costo fabricación</td>
																<td class="dinero">$ `+dato.tf_pesos+`</td>
															</tr>
															<tr>
																<td><strong>-<strong></td>
																<td>Financiación</td>
																<td class="dinero">`+dato.dv_financiacion_r+`</td>
															</tr>
															<tr>
																<td><h3>=</h3></td>
																<td><h3>Utilidad</h3></td>
																<td class="dinero"><h3>$ `+dato.du_utilidad+`</h3></td>
															</tr>
															<tr>
																<td><h3>=</h3></td>
																<td><h3>Margen bruto</h3></td>
																<td class="dinero"><h3>`+dato.du_margen_bruto+` %</h3></td>
															</tr>
														</tbody>
													</table>
						                        </div>
						                    </div>
						                </div>

					                </div>
				                </div>
	                		</div>
						</div>
					</div>`;

	                $("#accordion2").append(html);
			    });
			}else{
				console.log("Error!","Algo ha salido mal.","warning");
			}
		});

		request.fail(function( jqXHR, textStatus ) {
			console.log("Error!","Algo ha salido mal.","warning");
		});
	}

	$('.btn-submit').click(function(e){
		if(true === $("#formulario").parsley().validate()){
			var element = $(this).parent().parent().parent().parent().parent();

			var fd = new FormData(document.getElementById("formulario"));
			fd.append("oportunidad_id", {{ $oportunidad->id }});
			var request = $.ajax({
			  url: "{{ url('saveapu') }}",
			  type: "POST",
			  data: fd,
			  processData: false,
			  contentType: false
			});

			request.done(function( msg ) {
			  	if (msg.success) {
					swal("Exito!","APU guardado con exito.","success");
					ocultarFormulario();
					mostrarlistadoapu();
                    $('#formulario').html(html_formulario);
				}else{
					swal("Error!","Algo ha salido mal.","warning");
				}
			});

			request.fail(function( jqXHR, textStatus ) {
				swal("Error!","Algo ha salido mal.","warning");
			});
		}else{
			e.preventDefault();
			var element = $(this).parent().parent().parent().parent().parent();
			$(element).addClass('animated rubberBand');
			$(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			  	$(element).removeClass('animated rubberBand');
			});

			$("#output").removeClass(' alert alert-success');
			$("#output").addClass("alert alert-danger animated flip").html("Perdón!, Es necesario que complete todos los campos");
			$("#output").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			  	$("#output").removeClass('animated flip');
			});
		}
	});


	mostarValorTotal = function(){
        var valSumaGeneral = 0;
        $(".sumarTotales").each(function() {
            var valSumaT = verificar(this.id);
            valSumaGeneral = parseInt(valSumaGeneral) + parseInt(valSumaT);
        });

        TotalhHombre = parseInt(valSumaGeneral);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        //document.getElementById("subtotalpro").value = TotalhHombre;
        //document.getElementById("valor_oportunidad").value = TotalhHombre;

        var valSumaGeneralmas = 0;
        $(".sumarTotalesmas").each(function() {
            var valSumaM = verificar(this.id);
            valSumaGeneralmas = parseInt(valSumaGeneralmas) + parseInt(valSumaM);
        });

        TotalhHombre = parseInt(valSumaGeneralmas);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        document.getElementById("subtotalpromas").value = TotalhHombre;

        var valSumaGeneralGastos = 0;
        $(".sumarGastos").each(function() {
            var valSumaG = verificar(this.id);
            valSumaGeneralGastos = parseInt(valSumaGeneralGastos) + parseInt(valSumaG);
        });

        TotalhHombre = parseInt(valSumaGeneralGastos);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        document.getElementById("subtotalgastos").value = TotalhHombre;

        var valSumaG = verificar("subtotalgastos");
        //var valSumaT = verificar("subtotalpro");
        var valSumaT = 0;
		var valSumaM = verificar("subtotalpromas");
        var TotalGeneral = parseInt(valSumaT) + parseInt(valSumaM) + parseInt(valSumaG);

        TotalhHombre = parseInt(TotalGeneral);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        $("#totalFinal").val(TotalhHombre);

        var valTotalPesos = verificar("totalFinal");
        TotalhHombre = parseInt(valTotalPesos);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        $("#totalFinal").val(TotalhHombre);
        $("#valTotalPesos").val(TotalhHombre);
        $("#costofabricacion").val(TotalhHombre);
        //document.getElementById("valor_oportunidad").value = TotalhHombre;
    }

    calcularValorTotal = function (val) {
        var cantidad=verificar("textCantidad"+val);
        var valor=verificar("textValor"+val);
        texValTotal = parseInt(valor*cantidad);

        if(!isNaN(texValTotal)){
            texValTotal = texValTotal.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            texValTotal = texValTotal.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("numberValor"+val).value = texValTotal;
        }

        mostarValorTotal();
    }

	quitarProductos = function (id) {
      $(id).removeClass('rollIn').addClass('hinge');
      setTimeout(function(){ $(id).remove(); }, 1000);
    }
    i
    agregarProductos = function () {
      i++;
      dato = `
        <div class="row animated rollIn" id="listAddProductos`+i+`">
            <div class="col-5">
                <select id="selectProducto`+i+`" name="productos[`+i+`][producto]" class="form-control productos`+i+`" onchange="buscarDimSelec2(`+i+`)" required></select>
                <input type="hidden" name="productos[`+i+`][tipo]" value="dinamico">
            </div>
            <div class="col-2">
                <select id="referencia`+i+`" name="productos[`+i+`][referencia]" class="form-control"></select>
            </div>
            <div class="col-1">
                <input type="text" id="textCantidad`+i+`" class="col-12 form-control" name="productos[`+i+`][cantidad]" onkeyup="calcularValorTotal(`+i+`)" placeholder="0">
            </div>
            <div class="col-2">
                <input type="text" id="textValor`+i+`" class="col-12 form-control dinero" name="productos[`+i+`][valor]" onkeyup="calcularValorTotal(`+i+`)" placeholder="$0">
            </div>
            <div class="col-2">
                <div class="input-group">
                    <input type="text" id="numberValor`+i+`" class="col-12 form-control sumarTotalesmas" name="productos[`+i+`][total]" placeholder="$0" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarProductos('#listAddProductos`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                    </span>
                </div>
            </div>
        </div>`;
        $( "#listProductos" ).prepend(dato);
        $('.productos'+i).select2({
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });

        $(".dinero").maskMoney();
    }

    $(function () {
        getComentarios({{ $oportunidad->id }});
    	mostrarlistadoapu();
        setTimeout(function(){
        	try {
	            mostarValorTotal();
	            if ($('#selec_moneda').val() === "COP") {
	                $('#tasa_cambio').val(1);
	                //$("#val_pesos").val($("#valor_oportunidad").val());
	                $('#tasa_cambio').prop('readonly', true);
	            }else{
	                $('#tasa_cambio').prop('readonly', false);
	            }

	            cambio  = $("#tasa_cambio").val();
	            //valor   = $("#valor_oportunidad").val();
	            cambio  = cambio.replace(/,/g, '');
	            valor   = valor.replace(/,/g, '');
	            total   = valor*cambio;
	            if(!isNaN(total)){
	                total = total.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
	                total = total.split('').reverse().join('').replace(/^[\,]/,'');
	                $("#val_pesos").val(total);
	            }
            }catch(err) { }
        }, 3000);

        $('body').on('change','#selec_moneda', function() {
            if ($(this).val() === "COP") {
                $('#tasa_cambio').val(1);
                $('#tasa_cambio').prop('readonly', true);
            }else{
                $('#tasa_cambio').prop('readonly', false);
            }
        });



        $('body').on('keyup','input', function() {

        	var valTotalPesos 	= verificar("totalFinal");
            var tasaPActada 	= verificar("tasaCambio");
	        TotalhHombre = parseInt(valTotalPesos) * parseInt(tasaPActada);
	        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
	        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
	        $("#valTotalPesos").val(TotalhHombre);
	        $("#costofabricacion").val(TotalhHombre);

            var valTotalPesos 	= verificar("newvalorventa");
            var tasaPActada 	= verificar("tasaPActada");
	        TotalhHombre = parseInt(valTotalPesos) * parseInt(tasaPActada);
	        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
	        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
	        $("#tovalorpesos").val(TotalhHombre);
	        $("#numValVenta").val(TotalhHombre);

	        var ciento1 	= verificar("numfinanciacion");
 	 		var cantidad1 	= verificar("tovalorpesos");
			var resultado 	= ciento1*cantidad1/100;
			var total 		= Math.round((cantidad1-resultado) *100)/100;

			TotalhHombre 	= parseInt(resultado);
	        var TotalhHombre= TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
	        TotalhHombre 	= TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
			document.getElementById("numdeftotal").value 	= TotalhHombre;
			document.getElementById("financiacion2").value 	= TotalhHombre;

			var datoX 		= verificar("tovalorpesos");
            var datoY 		= verificar("financiacion2");
            var datoCosto 	= verificar("costofabricacion");
            var valPesos 	= verificar("numValVenta");

            var resulUtilidad = parseInt(datoX) - parseInt(datoCosto) - parseInt(datoY);
            var TotalhHombre= resulUtilidad.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
	        TotalhHombre 	= TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("numUtilidad").value 	= TotalhHombre;

            var utilidad = verificar("numUtilidad");
            var resultado = utilidad/valPesos*100;
            var TotalhHombre= resultado.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
	        TotalhHombre 	= TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("margenBruto").value 	= parseFloat(TotalhHombre).toFixed(1);
        });

        $('body').on('change','#tasa_cambio', function() {
            cambio  = $(this).val();
            cambio  = cambio.replace(/,/g, '');
            valor   = valor.replace(/,/g, '');
            total   = valor*cambio;
            if(!isNaN(total)){
                total = total.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                total = total.split('').reverse().join('').replace(/^[\,]/,'');
                $("#val_pesos").val(total);
            }
        });

        $(".dinero").maskMoney();

        $('.productos0').select2({
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });




    });


    buscarDimSelec2 = function (i) {
        console.log("entro aqui"+0);
        var ref = $.get( "{{ url('/') }}/referencia?term=&dato="+$("#selectProducto"+i).val());
        ref.done(function( data ) {
            console.log(data);
            $('#referencia'+i).html(" ");
            $.each(data, function (k, item) {
                $('#referencia'+i).append($('<option>', {
                    value: item.id,
                    text : item.text
                }));
            });
        });
    }


    verificar = function(id){
        var obj=document.getElementById(id);
        if(!obj.value){
            value="0";
        }else{
            value=obj.value;
            value = value.replace(/,/g, "");
        }

        if(validate_importe(value,1))
        {
            // marcamos como erroneo
            obj.style.borderColor="#808080";
            return value;
        }else{
            // marcamos como erroneo
            obj.style.borderColor="#f00";
            return 0;
        }
    }

    validate_importe = function(value,decimal){
        if(decimal==undefined)
            decimal=0;

        if(decimal==1)
        {
            // Permite decimales tanto por . como por ,
            var patron=new RegExp("^[0-9]+((,|\.)[0-9]{1,2})?$");
        }else{
            // Numero entero normal
            var patron=new RegExp("^([0-9])*$")
        }

        if(value && value.search(patron)==0)
        {
            return true;
        }
        return false;
    }
    var otroGasto = 0;
    function addotrogasto(){
        otroGasto++;
        html = `<div class="col-md-1 gasto-`+otroGasto+`">
                                        <strong class="numero"></strong>
                                    </div>
                                    <div class="col-md-5 gasto-`+otroGasto+`">
                                        <input type="text" class="form-control" name="otrogasto[`+otroGasto+`][concepto]" maxlength="199" required>
                                    </div>
                                    <div class="col-md-5 gasto-`+otroGasto+`">
                                        <input type="text" style="margin-top:0px !important;" class="form-control dinero sumarGastos" id="otros_gastos`+otroGasto+`" name="otrogasto[`+otroGasto+`][valor]" placeholder="$ 0" onkeyup="mostarValorTotal()" required>
                                    </div>
                                    <div class="col-md-1 gasto-`+otroGasto+`">
                                        <a class="text-danger" style="cursor:pointer;font-size:2rem;" data-toggle="tooltip" data-placement="top" title="Eliminar gasto adicional" onclick="deletegasto(`+otroGasto+`)"><i class="fa fa-trash"></i></a>
                                    </div>`;
        $('#contenedor_otros_gastos').append(html);
        crear_numero();
        $(".dinero").maskMoney();
    }

    function deletegasto(id){
        $('.gasto-'+id).remove();
        crear_numero();
    }

    function crear_numero(){
        numero = 1;
        $(".numero").each(function(){
            $(this).html(numero);
            numero++;
        });
    }
</script>
@endsection
