<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="es-ES">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2018.0.0.379"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <script type="text/javascript">
   // Update the 'nojs'/'js' class on the html node
document.documentElement.className = document.documentElement.className.replace(/\bnojs\b/g, 'js');

// Check that all required assets are uploaded and up-to-date
if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["museutils.js", "museconfig.js", "webpro.js", "jquery.watch.js", "require.js", "index.css"], "outOfDate":[]};
</script>

  <title>Inicio</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/clientesgbp/html_form_clientes/css/site_global.css?crc=444006867"/>
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/clientesgbp/html_form_clientes/css/index.css?crc=106230402" id="pagesheet"/>
  <!-- IE-only CSS -->
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/clientesgbp/html_form_clientes/css/iefonts_index.css?crc=4223891781"/>
  <![endif]-->
  <!-- Other scripts -->
  <script type="text/javascript">
   var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script src="https://webfonts.creativecloud.com/open-sans:n8,n7,i4,n4:default.js" type="text/javascript"></script>
   </head>
 <body>

  <div class="clearfix borderbox" id="page"><!-- column -->
   <div class="clip_frame colelem" id="u97" data-sizePolicy="fixed" data-pintopage="page_fixedCenter"><!-- svg -->
    <img class="svg" id="u95" src="{{url('/')}}/clientesgbp/html_form_clientes/images/svg-pegado-106731x304.svg?crc=120506868" width="595" height="170" alt="" data-mu-svgfallback="images/svg%20pegado%20106731x304_poster_.png?crc=534143014"/>
   </div>
   <div class="rounded-corners clearfix colelem" id="u424"><!-- column -->
    <div class="clearfix colelem" id="u106-4" data-sizePolicy="fixed" data-pintopage="page_fixedCenter"><!-- content -->
     <p>Base de Datos Clientes</p>
    </div>
    <form class="form-grp clearfix colelem" id="widgetu110" method="post" enctype="multipart/form-data" action="{{url('/')}}/guardarclientesgbp"><!-- none box -->
        {{ csrf_field() }}
     <div class="position_content" id="widgetu110_position_content">
      <div class="fld-grp clearfix colelem" id="widgetu116" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u119-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu116_input"><!-- content --><span class="actAsPara">Empresa:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u117-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u117-3"><input class="wrapped-input" type="text" spellcheck="false" id="widgetu116_input" name="formulario[empresa]" tabindex="1"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu339" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u340-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu339_input"><!-- content --><span class="actAsPara">Ubicación:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u342-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u342-3"><input class="wrapped-input" type="text" id="widgetu339_input" name="formulario[ubicacion]" tabindex="2"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu351" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u352-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu351_input"><!-- content --><span class="actAsPara">Descripción:</span></label>
       <span class="fld-textarea actAsDiv shadow rounded-corners clearfix colelem" id="u354-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u354-3"><textarea class="wrapped-input" id="widgetu351_input" name="formulario[descripcion]" tabindex="3"></textarea></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu136" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u138-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu136_input"><!-- content --><span class="actAsPara">Nombre:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u137-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u137-3"><input class="wrapped-input" type="text" spellcheck="false" id="widgetu136_input" name="formulario[nombre]" tabindex="4"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu363" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u364-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu363_input"><!-- content --><span class="actAsPara">Cargo:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u366-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u366-3"><input class="wrapped-input" type="text" id="widgetu363_input" name="formulario[cargo]" tabindex="5"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu325" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u327-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu325_input"><!-- content --><span class="actAsPara">E-mail:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u328-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u328-3"><input class="wrapped-input" type="text" id="widgetu325_input" name="formulario[email]" tabindex="6"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu375" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u377-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu375_input"><!-- content --><span class="actAsPara">Teléfono:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u378-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u378-3"><input class="wrapped-input" type="text" id="widgetu375_input" name="formulario[telefono]" tabindex="7"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu236" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u239-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu236_input"><!-- content --><span class="actAsPara">Sitio web:</span></label>
       <span class="fld-input NoWrap actAsDiv shadow rounded-corners clearfix colelem" id="u237-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u237-3"><input class="wrapped-input" type="text" value="http://" spellcheck="false" id="widgetu236_input" name="formulario[sitio_web]" tabindex="8"/></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu388" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u390-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu388_input"><!-- content --><span class="actAsPara">CRM:</span></label>
       <span class="fld-textarea actAsDiv shadow rounded-corners clearfix colelem" id="u391-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u391-3"><textarea class="wrapped-input" id="widgetu388_input" name="formulario[crm]" tabindex="9"></textarea></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu400" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u402-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu400_input"><!-- content --><span class="actAsPara">Software:</span></label>
       <span class="fld-textarea actAsDiv shadow rounded-corners clearfix colelem" id="u403-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u403-3"><textarea class="wrapped-input" id="widgetu400_input" name="formulario[software]" tabindex="10"></textarea></div></span>
      </div>
      <div class="fld-grp clearfix colelem" id="widgetu412" data-required="true"><!-- none box -->
       <label class="fld-label actAsDiv clearfix colelem" id="u414-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" for="widgetu412_input"><!-- content --><span class="actAsPara">Observaciones:</span></label>
       <span class="fld-textarea actAsDiv shadow rounded-corners clearfix colelem" id="u415-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content --><div id="u415-3"><textarea class="wrapped-input" id="widgetu412_input" name="formulario[observaciones]" tabindex="11"></textarea></div></span>
      </div>
      <button class="submit-btn NoWrap transition rounded-corners clearfix colelem" id="u148-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true" type="submit" value="Enviar" tabindex="12"><!-- content -->
       <div style="margin-top:-20px;height:20px;">
        <p>Enviar</p>
       </div>
      </button>
      <div class="clearfix colelem" id="pu150-4"><!-- group -->
       <div class="clearfix grpelem" id="u150-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content -->
        <p>Enviando formulario...</p>
       </div>
       <div class="clearfix grpelem" id="u149-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content -->
        <p>El servidor ha detectado un error.</p>
       </div>
       <div class="clearfix grpelem" id="u111-4" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true"><!-- content -->
        <p>Formulario recibido.</p>
       </div>
      </div>
     </div>
    </form>
   </div>
   <div class="verticalspacer" data-offset-top="2292" data-content-above-spacer="2292" data-content-below-spacer="62" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>
  </div>
  <!-- Other scripts -->
  <script type="text/javascript">
   // Decide weather to suppress missing file error or not based on preference setting
var suppressMissingFileError = false
</script>
  <script type="text/javascript">
   window.Muse.assets.check=function(d){if(!window.Muse.assets.checked){window.Muse.assets.checked=!0;var b={},c=function(a,b){if(window.getComputedStyle){var c=window.getComputedStyle(a,null);return c&&c.getPropertyValue(b)||c&&c[b]||""}if(document.documentElement.currentStyle)return(c=a.currentStyle)&&c[b]||a.style&&a.style[b]||"";return""},a=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),
16);return 0},g=function(g){for(var f=document.getElementsByTagName("link"),h=0;h<f.length;h++)if("text/css"==f[h].type){var i=(f[h].href||"").match(/\/?css\/([\w\-]+\.css)\?crc=(\d+)/);if(!i||!i[1]||!i[2])break;b[i[1]]=i[2]}f=document.createElement("div");f.className="version";f.style.cssText="display:none; width:1px; height:1px;";document.getElementsByTagName("body")[0].appendChild(f);for(h=0;h<Muse.assets.required.length;){var i=Muse.assets.required[h],l=i.match(/([\w\-\.]+)\.(\w+)$/),k=l&&l[1]?
l[1]:null,l=l&&l[2]?l[2]:null;switch(l.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");f.className+=" "+k;k=a(c(f,"color"));l=a(c(f,"backgroundColor"));k!=0||l!=0?(Muse.assets.required.splice(h,1),"undefined"!=typeof b[i]&&(k!=b[i]>>>24||l!=(b[i]&16777215))&&Muse.assets.outOfDate.push(i)):h++;f.className="version";break;case "js":h++;break;default:throw Error("Unsupported file type: "+l);}}d?d().jquery!="1.8.3"&&Muse.assets.outOfDate.push("jquery-1.8.3.min.js"):Muse.assets.required.push("jquery-1.8.3.min.js");
f.parentNode.removeChild(f);if(Muse.assets.outOfDate.length||Muse.assets.required.length)f="Todo esta bien llene el formulario.",g&&Muse.assets.outOfDate.length&&(f+="\nOut of date: "+Muse.assets.outOfDate.join(",")),g&&Muse.assets.required.length&&(f+="\nMissing: "+Muse.assets.required.join(",")),suppressMissingFileError?(f+="\nUse SuppressMissingFileError key in AppPrefs.xml to show missing file error pop up.",console.log(f)):console.log(f)};location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi)?
setTimeout(function(){g(!0)},5E3):g()}};
var muse_init=function(){require.config({baseUrl:""});require(["jquery","museutils","whatinput","webpro","jquery.watch"],function(d){var $ = d;$(document).ready(function(){try{
window.Muse.assets.check($);/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.makeButtonsVisibleAfterSettingMinWidth();/* body */
Muse.Utils.initWidget('#widgetu110', ['#bp_infinity'], function(elem) { return new WebPro.Widget.Form(elem, {validationEvent:'submit',errorStateSensitivity:'high',fieldWrapperClass:'fld-grp',formSubmittedClass:'frm-sub-st',formErrorClass:'frm-subm-err-st',formDeliveredClass:'frm-subm-ok-st',notEmptyClass:'non-empty-st',focusClass:'focus-st',invalidClass:'fld-err-st',requiredClass:'fld-err-st',ajaxSubmit:true}); });/* #widgetu110 */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
}catch(b){if(b&&"function"==typeof b.notify?b.notify():Muse.Assert.fail("Error calling selector function: "+b),false)throw b;}})})};

</script>
  <!-- RequireJS script -->
  <script src="{{url('/')}}/clientesgbp/html_form_clientes/scripts/require.js?crc=4157109226" type="text/javascript" async data-main="scripts/museconfig.js?crc=4153641093" onload="if (requirejs) requirejs.onError = function(requireType, requireModule) { if (requireType && requireType.toString && requireType.toString().indexOf && 0 <= requireType.toString().indexOf('#scripterror')) window.Muse.assets.check(); }" onerror="window.Muse.assets.check();"></script>
   </body>
</html>
