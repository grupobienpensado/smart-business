<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Proceso De Oportunidad')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
@php 
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
list($fecha_opor,$hora)=explode(" ",$datos->created_at);
$cal_mes=explode("-",$fecha_opor);
@endphp
<link rel="stylesheet" type="text/css" href="{{ url('/')}}/js/plugins/VerticalTimeline/css/default.css" />
<link href="{{ url('/') }}/components/uploader/jquery.fileuploader.css" media="all" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="{{ url('/')}}/js/plugins/VerticalTimeline/css/component.css" />
<style type="text/css">
.nivel{
    font-size: 18px;
    position: absolute;
    left: 0;
    right: 0;
}

.tiempo{
    position: absolute;
    right: 0;
    top: 0;
    margin-right: 10px;
    margin-top: 3px;
}

.tiempo2{
    position: absolute;
    right: 0;
    top: 0;
    margin-right: 10px;
    margin-top: 23px !important;
    font-size: 17px;
    color: #d02f13;
}
    
.btn-lateral{
    height: 65px;
    width: 46px;
    margin-right: -45px;
}

.btn-superior{
    border-top-right-radius: 10px;
    background-color: #e07014;
}

.btn-inferior{
    border-bottom-right-radius: 10px;
    background-color: #39c77d;
}

.botones{
    width: 100%;
    display: inline-block;
    text-align: center;
}
.cuadro-boton{
    display: inline-block;
    margin: 10px;
    position: relative;
    background-color: #fff;
    padding: 6px;
    border-radius: 100%;
}
.body-contenido p{
    font-size: 15px;
    font-weight: 400;
    text-align: justify;
    line-height: 1;
}
.body-contenido{
    width: 100%;
    padding: 10px;
    display: inline-block;
}
.resumen{
    position: absolute;
    font-size: 15px;
}
.margen-derecho-17{
    margin-right: 17px;
}
.div-commen{
    padding: 10px;
    border: .5px solid #ccc;
    margin: 10px;
    background: #ccc;
    overflow-x: hidden;
    overflow-y: scroll;
    height: 450px;
}
.commen{
    border: 0.1px solid #968d8d;
    padding: 10px;
    border-radius: 5px;
    font-size: 15px;
    background-color: #fff;
    margin: 11px;
}

.user-commen{
    font-size: 20px;
}

.commen p{
    text-align: justify;
    line-height: 1;
    font-weight: 400;
    color: #000;
}

.time-commen{
    color: #000;
    font-size: 12px;
}

.img-commen{
    width: 60px;
    height: 60px;
    float: left;
    margin-right: 15px;
    background-repeat: no-repeat;
    background-size: cover;
    border: 0.5px solid #ccc;
}
.img-commen2{
    width: 80px;
    height: 80px;
    float: left;
    margin-right: 15px;
    background-repeat: no-repeat;
    background-size: cover;
    border: 0.5px solid #ccc;
    border-radius: 100%;
}
.comentario{
    display: block;
    position: relative;
    margin-left: 75px;
}
.titulo-btn{
    position: absolute;
    left: -5px;
    right: 0;
    font-size: 10px;
    bottom: -19px;
    text-align: center;
    width: 63px;
}

.saltar{
    margin-top: 0 !important;
    margin-bottom: 0 !important;
    border: 0;
    border-top: 0 !important;

}
.navegacion{
    position: absolute;
    left: -118px;
    z-index: 99;
    background-color: #ccc;
    border-top: 1px solid #fff;
}
.titulo-navegacion{
    background-color: #051d60;
    color: #fff;
    padding: 5px;
    width: 198px;
    left: -4px;
    top: 0;
    text-align: center;
    float: left;
    margin-bottom: 0px;
}
.fecha-navegacion{
    background-color: #0275d8;
    color: #fff;
    padding: 5px;
    width: 198px;
    left: 0px;
    top: 0;
    margin-bottom: 0;
    text-align: center;
}
.tabs a {
    font-size: 13px;
    margin-left: 0px;
    margin-right: 0px;
}
.clear{
    border-bottom: 0.5px solid #fff;
}

.navegacion p span{
    float: right;
    margin-right: 5px;
    font-size: 13px;
}
.contenedor-nav{
    position: relative;
    margin-top: 45px;
}
.usuario-list{
    margin-top: 1.2rem;
    margin-bottom: 0;
    color: #000;
}
#ingresos .relativo{
    border-bottom: 0.5px solid #ccc;
    padding: 5px;
}
.infor{
    height: 60px;
    vertical-align: middle;
    display: block;
    text-align: left;
}
.infor p{
    margin-bottom: 0;
}
.segunda-fecha{
    position: absolute;
    right: 0;
    top: 20px;
    font-size: 20px;
}
.primera-fecha{
    position: absolute;
    right: 0;
    
}
li a {
    color: #000000 !important;
}
.alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img {
    -webkit-border-radius: 2px !important;
    -moz-border-radius: 2px !important;
    -ms-border-radius: 2px !important;
    -o-border-radius: 2px !important;
    border-radius: 0px !important;
    border: solid 0px !important;
}
.relativo{
    position: relative;
}
.validate-p{
    text-align: left
}
</style>
<style>
/* Center the loader */
#loaders{
    position: relative;
    left: 50%;
    bottom: 0px;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: 25px 0 0 -75px;

}
.loader {
  position: relative;
  left: 50%;
  bottom: 0px;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: 25px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  border-bottom: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}




</style>
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/component.css" />
@php
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
$semana=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
@endphp
    <div class="container-fluid animated flipInX">
      <div class="row">
        <div class="col-sm-12 panel-view">
            <div class="pull-right">
                <div class="btn-group">
                  <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
                  
                </div>
              </div>
              <div class="panel-title">
                    <h2>Ciclo De Oportunidad</h2>
                </div>
                <hr>
              <div class="row">
                <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#transicion" role="tab" data-toggle="tab">Transición</a></li>
                    <li><a href="#ingresos" role="tab" data-toggle="tab">Listado de ingresos</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="transicion">
                    <ul class="cbp_tmtimeline">
                     @php $i=0; @endphp
                        @foreach($historial as $key)
                        @if($key->key=="ciclo_venta")
                            @php $num_comen=0;
                                $num_archivos=0;
                                $array[$i]=$key;
                                $i++;

                                list($fecha,$hora)=explode(" ",$key->created_at);
                                $calmes=explode("-",$fecha);

                            @endphp
                            
                            @foreach($ciclos as $ciclo)
                                @if($ciclo->porcentaje==$key->value)
                                    @php 
                                        $list=explode(" ",$ciclo->nombre);
                                        $por=$ciclo->dias;
                                    @endphp
                                @endif
                            @endforeach
                            @if($i==1)
                            @php
                               $resta= strtotime(date('Y-m-d'))-strtotime($fecha);
                               $fin =round($resta/(60*60*24)); 
                               $fin=$por-$fin;
                               if($fin>0){
                                $msg="Plazo para transición: ".$fin." dias";
                               }else{
                                $msg="ha excedido el plazo de transición en ".($fin*-1)." dias";
                               }
                            @endphp
                            @endif
                        @if($i==1)
                            @php
                            $resta= strtotime(date('Y-m-d'))-strtotime($fecha);
                            $fin =round($resta/(60*60*24));
                            @endphp
                        @else
                            @php
                            list($fecha,$hora)=explode(" ",$key->created_at);
                            list($fecha2,$hora)=explode(" ",$array[$i-2]["created_at"]);
                            $resta= strtotime($fecha2)-strtotime($fecha);
                            $fin =round($resta/(60*60*24));
                            @endphp
                        @endif
                        @foreach($archivos as $archivo)
                        @if($archivo->historial_oportunidades==$key->id)
                            @php $num_archivos++; @endphp
                        @endif
                        @endforeach
                        @foreach($comentarios as $comentario)
                        @if($comentario->historial_oportunidades==$key->id)
                            @php $num_comen++; @endphp
                        @endif
                        @endforeach
                        <li class="relativo">
                            <time class="cbp_tmtime" ><span>{{$calmes[2]."/".$meses[(int)$calmes[1]-1]."/".$calmes[0]}}</span> <span>{{$key->value}}%</span></time>
                            <div class="cbp_tmicon cbp_tmicon-screen"></div>
                            <div class="cbp_tmlabel">
                            <span class="tiempo"><i class="fa fa-clock-o"></i> Hace {{$fin}} dias</span>
                            @if($i==1&&$key->value!=0)<span class="tiempo2"><i class="fa fa-warning"></i> {{$msg}}</span>@endif
                                <h2 class="lista" data-name="<?php echo $i; ?>">{{$list[2]}}</h2>
                                <div class="validate-p" id="p-<?php echo $i; ?>" style="display: none">
                                    <div class="body-contenido">
                                        <p><?php print($key->descripcion) ?>
                                        </p>
                                        <div style="width:100%; text-align: center; <?php if(print($key->descripcion)!=""){ ?>border-right: 0.3px solid #a7d9f5;<?php } ?>" >
                                        @foreach($archivos as $archivo)
                                        @if($archivo->historial_oportunidades==$key->id)
                                            @php
                                            list($nombre,$ext)=explode(".", $archivo->archivo);
                                            if($ext=="jpg"||$ext=="JPG"){
                                                $imagen="jpg.png";
                                            }elseif($ext=="png"||$ext=="PNG"){
                                                $imagen="png.png";
                                            }elseif($ext=="DOC"||$ext=="doc"){
                                                $imagen="word-logo.png";
                                            }elseif($ext=="pdf"||$ext=="PDF"){
                                                $imagen="pdf-logo.png";
                                            }elseif($ext=="ppt"||$ext=="PPT"){
                                                $imagen="ppt.png";
                                            }elseif($ext=="xls"||$ext=="XLS"||$ext=="XLSX"||$ext=="xlsx"){
                                                $imagen="excel-logo.png";
                                            }else{
                                                $imagen="file.png";
                                            }
                                            @endphp
                                            <div style="width: 100%;text-align: center; display: inline-block;"><img src="{{url('/')}}/images/icons/{{$imagen}}" style="width: 64px !important;" class="delete" data-name="{{$archivo->id}}" onclick="openDocument('{{$archivo->archivo}}','{{$ext}}')"></div>
                                        @endif
                                        @endforeach
                                        </div>
                                    </div>
                                    <div class="botones upload">
                                        @if($i==1&&$key->value!=0&&$key->value!=90)
                                        @if($i==1&&$key->value>10)
                                        <div class="cuadro-boton atras"><img src="{{url('/')}}/images/icons/atras.png" style="width: 40px;"><div class="titulo-btn"><span>Devolver</span></div></div>
                                        @endif
                                        <div class="cuadro-boton eliminar" onclick="recargar({{$datos->id}})"><img src="{{url('/')}}/images/porcentajes/0.png" style="width: 40px;"><div class="titulo-btn"><span>Perdida</span></div></div>
                                        <div class="cuadro-boton avanzar" name="{{$key->id}}" data-name="{{$key->value}}"><img src="{{url('/')}}/images/icons/siguiente.png" style="width: 40px;"><div class="titulo-btn"><span>Avanzar</span></div></div>
                                        @endif
                                        <div class="cuadro-boton cargar" name="{{$key->id}}" ><img src="{{url('/')}}/images/icons/cargar.png" style="width: 40px;"><div class="titulo-btn"><span>{{$num_archivos}} Archivos</span></div></div>
                                        <div class="cuadro-boton comentar" name="{{$key->id}}"><img src="{{url('/')}}/images/icons/chat.png" style="width: 40px;"><div class="titulo-btn"><span>{{$num_comen}} Comentario</span></div></div>
                                    </div>
                                </div>
                                <div class="resumen resumen<?php echo $i; ?>"><span class="margen-derecho-17"><i class="fa fa-wechat"> {{$num_comen}} Comentarios</i></span><span class="margen-derecho-17"><i class="fa fa-file"> {{$num_archivos}} Archivos</i></span></div>
                            </div>
                        </li>
                        @endif
                        @endforeach
                           
                    </ul>
                    </div>
                    <div class="tab-pane fade" id="ingresos">
                        @php $fechav=''; @endphp
                          @foreach($ingresos as $ingreso)
                          @if($ingreso->oportunidad_id==$datos->id)
                          @php 
                          list($fecha,$hora)=explode(" ",$ingreso["created_at"]); 
                          $pila=explode("-",$fecha);
                          $dia_semana=$semana[date("w",strtotime($fecha))];
                          @endphp
                          @php
                          $user=App\User::findOrFail($ingreso->user_id);
                          @endphp
                          <div class="relativo">
                          <div class="img-commen2" style="background-image: url(../images/file/clientes/{{$user->foto}})"></div>
                          <span class="primera-fecha">{{$dia_semana}}, {{$pila[2]." de ".$meses[(int)$pila[1]-1]." de ".$pila[0] }}</span>
                          <span class="segunda-fecha"><i class="fa fa-clock-o"></i>{{substr($hora,0,-3)}}</span>
                            <div class="infor">
                                <h4 class="usuario-list">{{ucwords($user->nombres." ".$user->apellidos)}}</h4>
                                <p>IP {{$ingreso->ip}}</p>
                            </div>
                          </div>
                          <div class="clear"></div>
                          @php
                          $fechav=$fecha;
                          @endphp
                          @endif
                          @endforeach
                    </div>
                </div>
                </div>
                </div>
              </div>
            </div>    
        </div>
    </div>
</div> 
@if($i>0)
@php
    $porcen_actual=$array[0]->value;
@endphp
@else
@php
    $porcen_actual=10;
@endphp
@endif
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Cargar Archivos</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('uploadfiles') }}" enctype="multipart/form-data">
    <input type="hidden" name="id" id="subir-id">
        {{ csrf_field() }}
            <div class=" form-group row">
              <div class="col-md-12 text-center">
                <div class="box">
                    <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} Archivos seleccionados" multiple />
                    <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Cargar Archivos&hellip;</span></label>
                </div>
              </div>
          </div>
          <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
        </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="vendido" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Oportunidad Vendida</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
    <input type="hidden" name="id" value="{{$datos->id}}">
    <input type="hidden" name="datos[0][value]" value="90">
        <p style="text-align: justify; color: #000; font-weight: 400">Esta a punto de pasar esta oportunidad a la fase de ventas, si desea continuar escriba por favor digite una descripcion, de lo contrario cierre la ventana actual</p>
        {{ csrf_field() }}
            <div class=" form-group row">
              <div class="col-md-12">
                <label for="observaciones" class="col-form-label">Observaciones</label>
                    <textarea class="form-control" name="datos[0][descripcion]" rows="2" placeholder="Por favor ingrese una observacion" required></textarea>
              </div>
          </div>
          <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-send-o" aria-hidden="true"></i> Enviar</button>
        </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="avanzar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Avanzar En El Ciclo</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" id="formCicloSave" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
    <input type="hidden" name="id" value="{{$datos->id}}">
    <input type="hidden" name="id-etapa" id="etapa-siguiente">
    <input type="hidden" value="{{$porcen_actual}}" id="porcen_actual">
        {{ csrf_field() }}
            <div class=" form-group row" id="ciclo_siguiente">
                <div class="col-12">
                    <div id="output"></div>
                    <label for="ciclo_venta" class="col-form-label">Siguiente Ciclo</label>
                    <select name="ciclo_venta" class="form-control" id="ciclo_venta" required>
                    <option>Seleccione una opción</option>
                    @foreach($ciclos as $ciclo)
                        @if($ciclo->porcentaje>$array[0]->value&&$ciclo->porcentaje<=90)
                        <option value="{{$ciclo->porcentaje}}" data-image="{{ url('/')}}/{{$ciclo->imagen}}">{{$ciclo->nombre}}</option>
                        @endif
                    @endforeach
                    </select> 
                </div>  
                <div id="lista-obser" class="col-12">
                
                </div>
                <div id="loaders" style="display: none;"><div class="loader"></div><small>Cargando Archivos...</small></div>
                
             </div>
          <button type="submit" class="btn btn-essi pull-right btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
        </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="atras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Regresar En El Ciclo</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
    <input type="hidden" name="id-etapa" id="etapa-atras">
    <input type="hidden" name="id" value="{{$datos->id}}">
        {{ csrf_field() }}
            <div class=" form-group row">
              <div class="col-md-12">
                    <label for="ciclo_venta2" class="col-form-label">Siguiente Ciclo</label>
                    <select name="datos[0][value]" class="form-control" id="ciclo_venta2" required>
                    <option>Seleccione una opción</option>
                    @foreach($ciclos as $ciclo)
                        @if($ciclo->porcentaje<$array[0]->value&&$ciclo->porcentaje!=0)
                        <option value="{{$ciclo->porcentaje}}" data-image="{{ url('/')}}/{{$ciclo->imagen}}">{{$ciclo->nombre}}</option>
                        @endif
                    @endforeach
                    </select> 
                <div class="form-group" style="border-bottom: 0.5px solid #ccc;padding-bottom: 1.5rem;">
                    <label for="observaciones" class="col-form-label">Observaciones</label>
                    <textarea class="form-control" name="datos[0][descripcion]" rows="2" placeholder="Por favor ingrese una observacion" required></textarea>
                </div>
              </div>
          </div>
          <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
        </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Marcar Oportunidad como perdida</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('ciclosave') }}" enctype="multipart/form-data">
    <input type="hidden" name="id" value="{{$datos->id}}">
    <input type="hidden" name="datos[0][value]"  value="0">
        {{ csrf_field() }}
            <div class=" form-group row">
              <div class="col-md-12">
                <div class="form-group" style="border-bottom: 0.5px solid #ccc;padding-bottom: 1.5rem;">
                    <label for="observaciones" class="col-form-label">Observaciones</label>
                    <textarea class="form-control" name="datos[0][descripcion]" rows="2" placeholder="Por favor ingrese una observacion" required></textarea>
                </div>
              </div>
          </div>
          <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</button>
        </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="comentar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Comentarios</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
            <div class=" form-group row">
              <div class="col-md-12">
                  <div class="div-commen">
                      
                    </div>
              </div>
          </div>
        <form method="POST" action="{{ url('savecomments') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id-commentario">
          <div class=" form-group row">
              <div class="col-md-12" id="ciclo_siguiente">
                <textarea rowspan="5" class="form-control" name="comentario" placeholder="Digite aqui un comentario"></textarea>
              </div>
            </div>
          <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-send-o" aria-hidden="true"></i> Enviar</button>
        </form>
    </div>
    </div>
  </div>
</div>
<div class="modal fade" id="archivos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Archivo</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
        <div class=" form-group row">
          <div class="col-md-12" id="modal_video">
          </div>
      </div>
    </div>
    <div class="modal-footer">
    <form method="POST" action="{{ url('deletefile') }}" enctype="multipart/form-data">
     {{ csrf_field() }}
        <input type="hidden" name="id" id="delete-archivo">
        <input type="hidden" name="oportunidad" value="{{$datos->id}}">
        <div class=" form-group row">
          <div class="col-md-12">
          <button type="submit" class="btn btn-md btn-danger white" href="javascript:void(0)"><i class="fa fa-trash"></i> Eliminar Archivo</button>
          </div>
      </div>
      </form>
    </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')  
<script src="{{url('/')}}/js/material/upload-file/js/custom-file-input.js"></script>
<script src="{{ url('/')}}/js/plugins/VerticalTimeline/js/modernizr.custom.js"></script>

<script src="{{ url('/') }}/components/uploader/jquery.fileuploader.min.js" type="text/javascript"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script type="text/javascript">

$('.btn-submit').click(function(e){

  if(true === $("#formCicloSave").parsley().validate()){
    //e.preventDefault();
    $("#loaders").show("swing");
  
    var element = $(this).parent().parent().parent();
  
  
  //if(remaining < 0){}
  }else{
    e.preventDefault();
    var element = $(this).parent().parent().parent();
    $(element).addClass('animated rubberBand');  
    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(element).removeClass('animated rubberBand');
    });

    $("#output").removeClass(' alert alert-success');
    $("#output").addClass("alert alert-danger animated flip").html("Perdón!, Es necesario que complete todos los campos");
    $("#output").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $("#output").removeClass('animated flip');
    });
  }
});

function recargar(id){
    location.href="{{url('/')}}/oportunidadperdida/"+id;
}
openDocument = function(nombre,ext) {
    $("#archivos").modal();
    $("#modal_video").html('');
    dir = '{{url("/")}}/images/file/oportunidades/'+nombre;
    if(ext=="jpg"||ext=="JPG"||ext=="png"||ext=="PNG"||ext=="JPEG"||ext=="jpeg"){
        $("#modal_video").html("<img src='"+dir+"' style='width: 100%' >");
    }else{
        contenido = `<iframe src="http://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:500px;" frameborder="0"></iframe>`;
    $("#modal_video").html(contenido);
    }
     
}

$("body").on("click","#btnSlide",function(e){
    if($(".navegacion").is(":visible")){
        $(".navegacion").hide("swing");
    }else{
        $(".navegacion").show("swing");
    }
})

$("body").on("click",".cargar",function(e){
    $("#subir-id").val($(this).attr("name"));
    $("#upload").modal();
})

$("body").on("click",".delete",function(e){
    $("#delete-archivo").val($(this).attr("data-name"));
})
$("body").on("click",".lista",function(e){
    $(this).attr("data-name");
    vd=$("#p-"+$(this).attr("data-name")).is(":visible");
    //$(".validate-p").hide("slow");
    if(!vd){
        $("#p-"+$(this).attr("data-name")).show("slow");
        $(".resumen"+$(this).attr("data-name")).hide("slow");
    }else{
        $("#p-"+$(this).attr("data-name")).hide("slow");
        $(".resumen"+$(this).attr("data-name")).show("slow");
    }
});

function addUserPic (opt) {
    if (!opt.id) {
        return opt.text;
    }               
    var optimage = $(opt.element).data('image'); 
    if(!optimage){
        return opt.text;
    } else {
        var $opt = $(
        '<span><img src="' + optimage + '" style="width: 20px;" /> ' + $(opt.element).text() + '</span>'
        );
        return $opt;
    }
};

$("body").on("click",".comentar",function(e){
    $("#comentar").modal();
    $("#id-commentario").val($(this).attr("name"));
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/vercomentarios", 
        cache: false,
        type: 'GET',
        data:{"id":$(this).attr("name")},
        success: function(e){ 
            $(".div-commen").html(e.cuerpo);
        }
    });
});
$("body").on("click",".avanzar",function(e){
    if($(this).attr("data-name")=="80"){
        $("#vendido").modal();
    }else{
        $("#avanzar").modal();
    }
    $("#ciclo_venta").select2({
        templateResult: addUserPic,
        templateSelection: addUserPic
    });
})

$("body").on("click",".atras",function(e){
    $("#atras").modal();
    $("#ciclo_venta2").select2({
        templateResult: addUserPic,
        templateSelection: addUserPic
    });
})

$("body").on("change","#ciclo_venta",function(){
    if($(this).val()!==90){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/avanzaretapa", 
        cache: false,
        type: 'GET',
        data:{"actual":$("#porcen_actual").val(),"siguiente":$(this).val()},
        success: function(e){ 
            $("#lista-obser").html(e.cuerpo);
            $(".fileuploader").fileuploader({
 
  captions: {
    button: function(options) { return 'Escoger ' + (options.limit == 1 ? 'File' : 'Archivos'); },
    feedback: function(options) { return 'Escoger ' + (options.limit == 1 ? 'file' : 'archivos') + ' subir'; },
    feedback2: function(options) { return options.length + ' ' + (options.length > 1 ? ' archivos' : ' el archivo fue') + ' elegidos'; },
    drop: 'Suelta los archivos aquí para Cargar',
    paste: '<div class="fileuploader-pending-loader"><div class="left-half" style="animation-duration: ${ms}s"></div><div class="spinner" style="animation-duration: ${ms}s"></div><div class="right-half" style="animation-duration: ${ms}s"></div></div> Pasting a file, click here to cancel.',
    removeConfirmation: '¿Seguro que desea eliminar este archivo?',
    errors: {
      folderUpload: 'No puedes subir carpetas.'
    }
  }
});
        }
    });
  }else{

  }
})

</script>
@endsection