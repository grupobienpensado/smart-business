<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Agregar Visita')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">

.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
    .form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
}
.table td{
    border: 0 !important;
}
.nombre-contenedor{
    display: inline-block;
    padding: 5px;
    color: #000;
    box-shadow: 1px 3px 3px #888888;
}
.custom-control-description{
    font-size: 15px;
  color: #000000;
}
</style>
    <div class="container-fluid animated flipInX">
      <div class="row">

        <div class="col-md-12 panel-view">
            <div class="pull-right">
                <div class="btn-group">
                  <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
                </div>
              </div>
            <form id="formSave" enctype="multipart/form-data">
                <div class="panel-title">
                    <h2>Agenda visita de un cliente</h2>
                </div>
                <hr>
                {{ csrf_field() }}
                <div class="form-group row"> 
                    <label for="cliente" class="col-2 col-form-label">Oportunidad</label>
                    <div class="col-10">
                        <select name="cliente" class="form-control" id="cliente" required></select>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12"><h5>Seleccione los funcionarios que asistirán</h5></div>
                    <div id="lista-clientes" class="col-md-12">
                        
                    </div>
                </div>                
                <div class="form-group row">
                    <div class="col-6">
                        <label for="profesion" class="col-form-label">Fecha de llegada de la visita</label>
                        <input class="form-control date" type="text" name="fecha_inicio" id="fechaini" placeholder="Por favor ingrese la fecha de inicio de la visita" required>
                    </div>
                    <div class="col-6">
                        <label for="jefe_inmediato" class="col-form-label">Fecha Final de la visita</label>
                        <input class="form-control date" type="text" name="fecha_final" id="fechaend" placeholder="Por favor ingrese la fecha final de la visita" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="observaciones" class="col-form-label">Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="5" placeholder="Por favor ingrese una observaciones" required></textarea>
                </div>
                <div class="form-group">
                    <table id="listado" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                      <thead>
                        <tr>
                            <th class="text-center th">Dia</th>
                            <th class="text-center th">Hora Inicio</th>
                            <th class="text-center th">Hora Final</th>
                            <th class="text-center th">Actividad</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr id="0">
                          <td><input class="form-control2 form-control date2" type="text" name="datos[0][dia]" placeholder="Dia de la actividad" required></td>
                          <td><input class="form-control2 form-control time valtim" id="time0" name="datos[0][hora_inicio]" type="text" step="1800" placeholder="Hora inicio de la actividad" required></td>
                          <td><input class="form-control2 form-control time" id="timend0" name="datos[0][hora_fin]" type="text" step="1800" placeholder="Hora final de la actividad" required></td>
                          <td><input class="form-control2 form-control" type="text" name="datos[0][descripcion]" placeholder="Agregue una descripcion"></td>
                          <td width="30px">
                          <span class="input-group-btn" id="btnadd-0">
                            <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregar_fila(0)"><i class="fa fa-plus text-success"></i></button>
                          </span>
                          <span class="input-group-btn" id="btndelete-0" style="display: none">
                            <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="eliminar_fila(0)"><i class="fa fa-trash text-danger"></i></button>
                          </span>
                          </td>
                      </tr>
                      </tbody>
                    </table>
                </div>
                <button type="button" class="btn btn-essi pull-right" onclick="guardarForm()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </form>
        </div>
      </div>
    </div>
@endsection

@section('scripts')

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>

<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>

<script type="text/javascript">
$(function () {

  $('.date').bootstrapMaterialDatePicker({
      time: false,
      clearButton: true,
      nowButton: true,
      minDate : moment(),
      lang: 'es',
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar'
  }).on('close', function(e){
    if(($("#fechaend").val() !== "") && ($("#fechaend").val() !== "")) {
      var a = moment($("#fechaend").val(), 'YYYY-MM-DD');
      var b = moment($("#fechaini").val(), 'YYYY-MM-DD'); 
      c = a.diff(b);
      console.log(c);
      if (c<0) {
          $("#fechaend").val("");
          swal("La fecha final no puede ser menor a la inicial");
      }else{
        $('.date2').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            nowButton: true,
            minDate : moment($("#fechaini").val()),
            maxDate : moment($("#fechaend").val()),
            lang: 'es',
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar'
        }).on('close', function(e){
            var a = moment($("#fechaend").val(), 'YYYY-MM-DD');
            var b = moment($("#fechaini").val(), 'YYYY-MM-DD'); 
            c = a.diff(b);
            console.log(c);
            if (c<0) {
                $("#fechaend").val("");
                swal("La fecha final no puede ser menor a la inicial");
            }
        });
      }
    }
  }); 

  

  $('.time').bootstrapMaterialDatePicker({
      time: true,
      date: false,
      clearButton: true,
      nowButton: true,
      lang: 'es',
      format : 'HH:mm',
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar',
      weekStart : 0 
  });

        $('#cliente').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    }) 
var i=1;
$("body").on("change","#cliente",function(){
    
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/buscarclientes/"+$(this).val(), 
        cache: false,
        type: 'GET',
        success: function(e){ 
            $("#lista-clientes").html(e.clientes);
        }
    });
});
function agregar_fila(valor){
    $("#btnadd-"+valor).hide();
    $("#btndelete-"+valor).show();
    $("#listado tbody:last").append('<tr id="'+i+'">'+
      '<td><input class="form-control2 form-control date2" type="text" name="datos['+i+'][dia]" placeholder="Dia de la actividad" required></td>'+
      '<td><input class="form-control2 form-control time valtim" id="time'+i+'" name="datos['+i+'][hora_inicio]" type="text" step="1800" placeholder="Hora inicio de la actividad" required></td>'+
      '<td><input class="form-control2 form-control time" id="timend'+i+'" name="datos['+i+'][hora_fin]" type="text" step="1800" placeholder="Hora final de la actividad" required></td>'+
      '<td><input class="form-control2 form-control" type="text" name="datos['+i+'][descripcion]" placeholder="Agregue una descripcion" required></td>'+
      '<td><span class="input-group-btn" id="btnadd-'+i+'">'+
        '<button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregar_fila('+i+')"><i class="fa fa-plus text-success"></i></button>'+
    '</span>'+
    '<span class="input-group-btn" id="btndelete-'+i+'" style="display: none">'+
        '<button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="eliminar_fila('+i+')"><i class="fa fa-trash text-danger"></i></button>'+
    '</span></td>'+
  '</tr>');
  i++;
  $('.time').bootstrapMaterialDatePicker({
    time: true,
    date: false,
    clearButton: true,
    nowButton: true,
    lang: 'es',
    format : 'HH:mm',
    cancelText : 'Cancelar',
    okText: 'Aceptar',
    nowText: 'Nuevo',
    clearText: 'Limpiar',
    weekStart : 0 
  });

  if(($("#fechaend").val() !== "") && ($("#fechaend").val() !== "")) {
    $('.date2').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        nowButton: true,
        minDate : moment($("#fechaini").val()),
        maxDate : moment($("#fechaend").val()),
        lang: 'es',
        cancelText : 'Cancelar',
        okText: 'Aceptar',
        nowText: 'Nuevo',
        clearText: 'Limpiar'
    }).on('close', function(e){
        var a = moment($("#fechaend").val(), 'YYYY-MM-DD');
        var b = moment($("#fechaini").val(), 'YYYY-MM-DD'); 
        c = a.diff(b);
        console.log(c);
        if (c<0) {
            $("#fechaend").val("");
            swal("La fecha final no puede ser menor a la inicial");
        }
    });
  }
}

function eliminar_fila(id){
    $("#"+id).remove();
}

guardarForm = function () {
  var valtime = true;
  event.preventDefault();
  cont = 0;
  $(".rad:checked").each(function() {
      cont++;
  });

  $(".valtim").each(function() {
      console.log(this.id);
      var id = this.id;
      var r = id.replace("time", "");
      console.log(r, "este es r");
      var b = moment($("#time"+r).val(), 'HH:mm');
      var a = moment($("#timend"+r).val(), 'HH:mm'); 
      c = a.diff(b);
      console.log(c);
      if (c<=0) {
        valtime = false;
        $("#timend"+r).val("");
        swal("La fecha final no puede ser menor a la inicial");
      }
  });
  if (valtime) {
    if (cont>0) {
      if(true === $("#formSave").parsley().validate()){
        console.log("llega");
        swal({   title: "Esta Seguro?",   
          text: "Va a guardar esta información",   
          type: "info",  
          showCancelButton: true,   
          showLoaderOnConfirm: true,
          animation: "slide-from-top",
          confirmButtonText: 'Guardar',
          cancelButtonText: 'Cerrar',
          preConfirm: function () {
            return new Promise(function (resolve, reject) {
              var datos = new FormData($("#formSave")[0]);
          
              var request = $.ajax({
                url: "{{ url('visitasave') }}",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST'
            });   
                     
            request.done(function( msg ) {console.log("ya");
                resolve(msg)
            });
             
            request.fail(function( jqXHR, textStatus ) {
              resolve(false)
            });
            })
          },
        }).then(function(data){ 
          if (data.success) {
            $('#formSave')[0].reset();
            url = "{{ url('/') }}/asignarresponsable/"+data.id;
            console.log(url);
            window.location = url;
            swal("Agenda guardada con exito"); 
            
          }else{
            swal("Algo salio mal, vuelve a intentar");
          }
        }).catch(swal.noop);  
      }else{
        swal("Faltan campos por completar!");
      }
    }else{
      swal("No has seleccionado ningún cliente, selecciona al menos uno para continuar");
    }
  }
}
</script>
@endsection
