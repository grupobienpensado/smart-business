@extends('template.app')

@section('title', 'dashboard')

<!--@section('sidebar')
    @parent
@endsection-->

@section('content')
@php
date_default_timezone_set("America/Bogota");
$contador_ls=0;
@endphp
<link rel="stylesheet" href="{{ url('/') }}/css/dashboard/btnsave.css">
<style type="text/css">
    .fa-3{font-size:4em}
    .alert,.badge,.breadcrumb,.card,.card .card-header,.dropdown-menu,.file-custom,.input-group-addon,.jumbotron,.nav .nav-link,.nav-tabs,.navbar,.navbar-toggler,.page-item:first-child .page-link,.page-item:last-child .page-link,.pagination-lg .page-item:first-child .page-link,.pagination-lg .page-item:last-child .page-link,.pagination-sm .page-item:first-child .page-link,.pagination-sm .page-item:last-child .page-link,.popover,.tooltip-inner,img{border:none!important}
    .container2{padding:15px 30px;min-height:500px}
    .division-1{-webkit-box-shadow:10px 14px 8px -8px rgba(0,0,0,0.54);-moz-box-shadow:10px 14px 8px -8px rgba(0,0,0,0.54);box-shadow:0 1px 17px 0 rgba(0,0,0,0.6);border-radius:5px;background-color:#fff;min-height:730px;max-height:730px;position:relative}
    .titulo-division-1{height:27px;background-color:#014eda;padding-top:2px;color:#fff;width:100%;text-align:center;position:absolute;overflow-x:hidden;top:15px}
    .titulo-division-2{height:27px;background-color:#00b0d2;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px}
    .tab-titulo-division-2{height:27px;background-color:#5f0101;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px}
    .tab-titulo-division-3{background-color:#1f628c;padding-top:2px;padding-left:5px;color:#fff;width:100%;font-size:14px;overflow-x:hidden}
    .collections-title2{margin-bottom:0!important;font-size:12px;padding:5px;color:#000;display:inline-block;margin-left:15px}
    .padding-top-4{padding-top:1px}
    .task-cat2{padding:3px 7px;color:#fff;font-weight:700;font-size:.8rem;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;background-clip:padding-box}
    .titulo-division-3{height:27px;background-color:#004f9b;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);background-image:-webkit-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);background-image:-ms-linear-gradient(90deg,#0390f5 0%,#013e7c 100%)}
    .titulo-division-4{height:27px;background-color:#1e8000;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#c7eb1f 0%,#33691e 100%);background-image:-webkit-linear-gradient(90deg,#c7eb1f 0%,#33691e 100%);background-image:-ms-linear-gradient(90deg,#c7eb1f 0%,#33691e 100%)}
    .titulo-division-4-v2{height:27px;background-color:#1e8000;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#90e631 0%,#1a3c0e 100%);background-image:-webkit-linear-gradient(90deg,#90e631 0%,#1a3c0e 100%);background-image:-ms-linear-gradient(90deg,#90e631 0%,#1a3c0e 100%)}
    .titulo-division-4-v3{height:27px;background-color:#1e8000;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#ce9f1d 0%,#f9bd12 100%);background-image:-webkit-linear-gradient(90deg,#ce9f1d 0%,#f9bd12 100%);background-image:-ms-linear-gradient(90deg,#ce9f1d 0%,#f9bd12 100%)}
    .titulo-division-5{height:27px;background-color:#004f9b;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#92d1ff 0%,#013e7c 100%);background-image:-webkit-linear-gradient(90deg,#92d1ff 0%,#013e7c 100%);background-image:-ms-linear-gradient(90deg,#92d1ff 0%,#013e7c 100%)}
    #chartdiv{padding-top:50px;height:600px!important;overflow:initial!important}
    #chartdiv1{padding-top:50px;height:600px!important;overflow:initial!important}
    #chartdiv2{padding-top:50px;height:600px!important;overflow:initial!important}
    .amcharts-chart-div a{display:none!important}
    .caja-tab{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:5px;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
    .caja-lineaspark{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:0!important;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
    .caja-tab-2{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:5px;float:left;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
    .margen-superior .col-md-8{-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
    .caja-derecha{margin-left:0!important}
    .cuadro-1-linea-1{display:block;text-align:center;font-size:11px}
    .cuadro-1-linea-2{text-align:center;display:block;font-size:29px;font-weight:initial;color:#00b0d2}
    .cuadro-1-linea-3{text-align:center;display:block;font-size:24px;font-weight:initial;color:#00b0d2}
    .hr{margin-bottom:0!important;margin-top:0!important}
    .cuadro-2-linea-2{text-align:center;display:block;font-size:35px;font-weight:initial;color:#00b0d2}
    .cuadro-3-linea-1{display:block;text-align:center;font-size:11px;color:#2657dc}
    .cuadro-3-linea-2{text-align:center;display:block;font-size:35px;font-weight:initial;color:#2657dc}
    .tab-cuadro-3-linea-1{display:block;text-align:center;font-size:11px;color:#FFF}
    .tab-cuadro-3-linea-2{text-align:center;display:block;font-size:35px;font-weight:initial;color:#FFF}
    .margen-superior{margin-top:5px}
    #donut-example{height:200px!important}
    #morris-bar{height:200px!important}
    .item{padding-top:4px;max-width:13.2%;min-width:13.2%;height:137px;display:inline-block;text-align:center;border:.5px solid #ccc;margin-top:5px}
    .linea1-item{font-size:15px!important;display:block;line-height:.8;color:#000}
    .linea2-item{font-size:11px!important;display:block;line-height:.8;color:#000}
    .linea3-item{font-size:17px;display:block;line-height:.8;color:#000}
    .linea4-item{font-size:14px;display:block;font-weight:700;color:#60b90b;line-height:.8}
    .linea5-item{font-size:9px;color:#305ddb}
    .blue{color:#305ddb}
    .centrar-contenido{left:0;right:0;text-align:center}
    .item2{width:19.8%;margin:.1%;background-color:#e8e8e8;float:left;text-align:center;border:.5px solid #ccc;padding-top:4px}
    .linea1-item2{font-size:33px!important;color:#305ddb;display:block;font-weight:700;line-height:.8;position:relative;height:46px}
    .linea2-item2{font-size:13px;color:#305ddb;display:block;z-index:9;position:absolute;right:0;left:0;top:30px}
    .item2 label:hover,.item2 span:hover{color:#868686}
    .select-mes{position:absolute;right:0;margin-right:19px;height:27px;width:209px;font-weight:100;font-size:12px;border:1px solid #306fdf}
    .logo-essi{left:5px;position:absolute;margin-top:15px}
    #lista-comercial{width:100%;height:500px}
    .margen-inferior{margin-bottom:20px}
    .borde-derecho{border-right:1px solid #d7d9da;text-align:center}
    #h6{line-height:1px;margin-top:12px}
    #millones{font-size:12px}
    #barra-horizontal{width:100%;height:250px}
    #pie{width:100%;height:250px}
    #negocio_perdido{width:100%;height:500px}
    .borde-inferior{border-bottom:1px solid #d7d9da}
    .pink.accent-2{background-color:#0093c9!important}
    .task-cat{padding:6px 7px;color:#fff;font-weight:700;font-size:.8rem;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;background-clip:padding-box}
    .pink{background-color:#0093c9!important}
    .progress{background-color:rgba(255,64,129,0.22);position:relative;height:4px;display:block;width:100%;background-color:#ddd;border-radius:2px;margin:.5rem 0 1rem;overflow:hidden}
    .progress .determinate{position:absolute;background-color:inherit;top:0;left:0;bottom:0;background-color:#0093c9;-webkit-transition:width .3s linear;-moz-transition:width .3s linear;-o-transition:width .3s linear;-ms-transition:width .3s linear;transition:width .3s linear}
    .progress2 .determinate{position:absolute;background-color:inherit;top:0;left:0;bottom:0;background-color:#36a2e3;-webkit-transition:width .3s linear;-moz-transition:width .3s linear;-o-transition:width .3s linear;-ms-transition:width .3s linear;transition:width .3s linear}
    #contenedor-perdida{position:absolute;top:0;bottom:0;left:0;right:0;width:90%;height:30%;margin:auto}
    .lista-caja{padding:15px 0 0}
    #cantidad-perdida{width:100%;height:500px}
    .divimagen{width:110px;height:110px;background-size:cover;border-radius:100%;display:inline-block;position:relative;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:1px 1px 12px -1px rgba(0,0,0,0.56);margin-top:50px}
    .divimagen-2{width:55px;height:55px;background-size:cover;border-radius:100%;display:inline-block;position:relative;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:1px 1px 12px -1px rgba(0,0,0,0.56)}
    .margen-contenedor{padding:0 75px}
    .cuadro{display:inline-block;margin-left:50px;font-size:10px;margin-top:23px;width:65px}
    .cuadro-cono{display:inline-block;margin-left:20px;font-size:10px;margin-top:23px;width:120px;float:left}
    .cuadro-color-azul{width:20px;height:8px;float:left;display:inline-block;background-color:#0061a6;margin-right:5px;margin-top:4px}
    .cuadro-color-verde{width:20px;height:8px;float:left;display:inline-block;background-color:#00a944;margin-right:5px;margin-top:4px}
    .cuadro-color-gris{width:20px;height:8px;float:left;display:inline-block;background-color:#6e91a4;margin-right:5px;margin-top:4px}
    .titulo-color{display:inline-block;float:left}
    .borde-inferior-2{padding-bottom:27px;border-bottom:1px solid #0012be}
    .nombre-avatar{position:absolute;top:124px;font-size:13px;right:0;left:0;text-align:center}
    .nombre-avatar-2{position:absolute;top:55px;font-size:13px;right:0;left:0;text-align:center}
    .celda-celeste{background-color:#e3edf6}
    .celda-azul{background-color:#cce1f2}
    .graficas{width:55%;text-align:right;float:right}
    .titulo-graficas{width:100%;text-align:right;float:right;padding:0 75px}
    .linea-titulo-grafica{width:19%;display:inline-block;text-align:center}
    .graficas-alta{height:200px;width:32%;float:left;margin-right:7px}
    .graficas-mediano{height:200px;width:32%;float:left;margin-right:7px}
    .graficas-corto{height:200px;width:32%;float:left}
    .tabla{width:100%;text-align:center}
    .td-85{height:85px}
    .tabla-padding{padding:15px 40px 40px 15px !important}
    .borde-azul{border-bottom:1px solid #0748c8}
    .linea3-item2{font-size:22px;display:block;line-height:.8;color:#66ae20}
    #dona-2{height:450px}
    .h6{text-align:center}
    .cabecera-tabla{background-color:#00629e;color:#fff}
    .subcabecera-tabla{text-align:center;background-color:#01356f;color:#FFF;font-size:12px}
    .numero-titulo{font-size:25px;text-align:center;background-image:-moz-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%);background-image:-webkit-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%);background-image:-ms-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%)}
    .letra-titulo{display:block;font-size:13px}
    .width-100{width:100%}
    .bandera{position:absolute;bottom:0;right:0;margin-right:-13px;margin-bottom:-6px;width:30px;height:30px;background-size:contain;border-radius:100%}
    .numero-tabla{font-size:25px;padding:7px}
    .width-10{width:10%;display:inline-block;position:relative}
    .width-30{width:29%;display:inline-block}
    .heigth-250{height:250px}
    .height-200{height:200px}
    .absoluto{position:absolute}
    .height-100{height:100%;width:100%}
    .relativo{position:relative}
    .texto-final{position:absolute;width:100%;bottom:0}
    .bottom-0{bottom:0}
    .derecha{text-align:right}
    .dona{height:300px;width:100%}
    .bloque{display:block}
    .card-content{text-align:center}
    .card .card-content{border-radius:0 0 2px 2px}
    .white-text{color:#fff!important}
    .purple{background-color:#507e8d!important}
    .card-stats-title{font-size:1.2rem}
    .card .card-content p{margin:0;color:inherit}
    .card-stats-title i{font-size:1.2rem}
    .card-stats-number{font-size:1.8rem;line-height:2rem;color:#fff;margin:.2rem 0;font-weight:500}
    .card .card-action{padding:10px 14px;border-top:1px solid rgba(160,160,160,0.2);}
    .purple.darken-2{background-color:#395469!important}
    .width-20{width:19%}
    .blue-sky{background-color:#2ecded!important}
    .blue-sky.darken-2{background-color:#0087be!important}
    .blue-king{background-color:#002365!important}
    .blue-king.darken-2{background-color:#0093d6!important}
    .blue-sky .card-stats-number,.blue-king .card-stats-number{font-size:.9rem;line-height:2rem;color:#fff;margin:.2rem 0;font-weight:500}
    .division-caja-tab-2{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:11px;float:left;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
    .marron-1{background-color:#ae9189}
    .marron-2{background-color:#9a6e63}
    .marron-3{background-color:#845448}
    .marron-4{background-color:#5f3326}
    #radar{height:250px;width:100%;margin-top:15px}
    .padding-7{padding:7px}
    .caja-20{display:inline-block;border:.5px solid #d7d7d7;text-align:center;margin-top:36px;height:170px;position:relative}
    .linea-titulo{margin-top:20px;font-size:21px;line-height:1px}
    .nombre-caja{font-size:10px!important;color:#636e7f}
    .height-40{height:40px;bottom:0;position:absolute;width:100%}
    .caja-contenedora{text-align:center;right:0;left:0}
    .subcaja{display:inline-block;border:.2px solid #ccc;position:relative;height:70px;background-color:#fff}
    .distin-fondo{background:#f8f8f8}
    .tamano-30{font-size:30px}
    .titulo-pendientes{color:#000;font-size:14px;padding:0 8px}
    .linea-menor{font-size:11px;display:block;width:70%;margin-left:15%}
    .tres-dos{position:absolute;bottom:0;height:32px;right:0;left:0;text-align:center}
    .progress2{position:relative;height:4px;display:block;width:100%;background-color:#395468;border-radius:2px;margin:.5rem 0 1rem;overflow:hidden}
    @media (max-width: 600px) {
    .container2{padding:0}
    .linea2-item2{font-size:13px;color:#305ddb;display:block;padding:5px;z-index:9;position:relative;right:0;left:0;top:30px}
    .item{padding-top:4px;max-width:90px;min-width:90px;height:130px;display:inline-block;text-align:center;border:.5px solid #ccc;margin-top:5px}
    .tabs a{font-size:13px;margin-left:0;margin-right:0}
    .select-mes{position:relative;right:0;height:27px;width:100%;font-weight:100;font-size:12px;border:1px solid #306fdf;margin-top:13px}
    #oportunidad_perdida{height:500px;display:block}
    }
    .card-action1{border-top:1px solid rgba(160,160,160,0.2);padding:10px 14px}
    .titulo-card{padding:10px;font-size:17px;color:#fff}
    .blue-sky .card-stats-title,.blue-king .card-stats-title{line-height:1.3px}
    .texto-sombra{font-size:35px;text-shadow:1px 2px 4px #636060}
    .blue-sky .card-stats-title,.blue-king .card-stats-title{font-size:.7rem}
    .caja-avatar{width:100%;position:relative;height:71px;text-align:center}
    .cabecera-marron-1{background-color:#ae9189;padding:12px;border-right:.5px solid #fff;text-align:center}
    .cabecera-marron-2{background-color:#9a6e63;padding:12px;border-right:.5px solid #fff;text-align:center}
    .cabecera-marron-3{background-color:#845448;padding:12px;border-right:.5px solid #fff;text-align:center}
    .cabecera-marron-4{background-color:#5f3326;padding:12px;border-right:.5px solid #fff;text-align:center}
    .titulo-marron-1{color:#fff}
    .td-marron{border-bottom:.5px solid #ccc;text-align:center}
    .td-marron .linea3-item2{font-size:28px;display:block;line-height:.8;color:#5d5d5d}
    .tabla-2 .td-85{height:85px;width:125px;text-align:center}
    .caja-negra{height:64px;width:100%;line-height:.9;background-color:#494949;margin-top:5px;position:absolute;bottom:0}
    .caja-negra span{font-size:8px;color:#bdbdbd;line-height:10px;white-space:pre-line}
    .tamano-110{height:110px;text-align:center}
    .img-caja{top:-30px;position:relative;left:0;right:0;text-align:center}
    .borde-derecho{border-right:1px solid #01c7ec}
    .fondo-gris{background-color:#e4e4e4}
    .linea3-item3{font-size:18px;display:block;line-height:.8;text-align:right;margin-right:5%}
    .tabla-3{width:100%;text-align:center}
    .sostenido{background-color:#d0eff4!important}
    #linea-pendiente{width:100%;height:450px}
    #radar-pendiente{width:100%;height:450px}
    #radar2-pendiente{width:100%;height:450px}
    table{width:100%}
    .cuadro-color-rojo-cono{width:20px;height:8px;float:left;display:inline-block;margin-right:5px;margin-top:4px;background-color:#bc0301}
    .cuadro-color-amarillo-cono{width:20px;height:8px;float:left;display:inline-block;margin-right:5px;margin-top:4px;background-color:#ffa901}
    .cuadro-color-verde-cono{width:20px;height:8px;float:left;display:inline-block;margin-right:5px;margin-top:4px;background-color:#00690c}
    .negocios_cerrados2 span{display:block;font-size:13px}
    .margen-titulo{margin-top:1%}
    .estilo_modificado{background-color:#014eda;color:#fff;width:97%;margin-bottom:5%;padding:0}
    @media (max-width: 1366px) {
    .task-cat{padding:0!important}
    }
    /*Lo comentado en app.css*/
    .modal-dialog{margin:200px auto !important;}
    #myModal, .modal-lg, #modal_video{min-width:75% !important;min-height:100% !important;}
    #myModal, .modal-lg, #modal_video{min-width:100% !important;}
    .modal-header{height:40px !important;text-align:center !important;border-bottom:0 !important;}
    .modal-title{color:#034bdb !important;font-size:22px !important; right:0 !important;left:50px !important;position:absolute !important;width:90%;margin-left:5%;border-bottom:1px solid #eceeef;}
    .modal-content{margin-top:0px;}
    @media (max-width:600px){.modal-content{width:360px;}
    }
    .alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img{-webkit-border-radius:2px !important;-moz-border-radius:2px !important;-ms-border-radius:2px !important;-o-border-radius:2px !important;border-radius:0px !important;border:solid 1px rgba(0, 0, 0, 0.09) !important;}
    /*fin lo comentado en app.css*/

    #uso_tiempo_chart{
        width: 100%;
        height: 30rem;
        position: relative;
        top: 1rem;
        left: -30px;
        width: 130%;
    }
    .list-head-child{}
    .list-head-child:nth-child(1) {
        background-color: #01c9ed;
        color: white;
    }
    .list-head-child:nth-child(1) h2{
        margin-top: 10px;
    }
    .list-head-child:nth-child(2) {
        background-color: #d6dde5;
    }
    .list-head-child:nth-child(3) {
        border-top: 1px solid #ddd;
        border-right: 1px solid #ddd;
    }
    .list-body-child{
        background-color: #d6dde5;
        margin: 4px auto;
    }
    .list-body-child:nth-child(1){
        color: #01c9ed;
    }
</style>
    <?php $meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
          $tipos_actividad=["Estimacion de inversiones y propuestas","Identificacion de oportunidades en cliente","Mejoramiento continuo","No aplica al cargo","Tramites de legalizacion y solicitud de viaticos","Ventas"];
    ?>
    <link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/morris.css">
    <link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/export.css" type="text/css" media="all" />

    {{ csrf_field() }}
    <div class="container2 animated flipInX">
        <div class="row">
            <div class="col-md-4 col-sm-12 cono">
          <div class="division-1">
            <div class="titulo-division-1">Cono de Oportunidadess</div>
            <div id="chartdiv"></div>
            <div class="cuadro-cono">
                <div class="cuadro-color-rojo-cono"></div><div class="titulo-color">LP - largo plazo</div><br>
                <div class="cuadro-color-amarillo-cono"></div><div class="titulo-color">MP - Mediano plazo</div><br>
                <div class="cuadro-color-verde-cono"></div><div class="titulo-color">CP - Corto plazo</div>
              </div>
          </div>
        </div>
            <div class="col-md-8 col-sm-7">
        <select class="select-mes" style="display: none;">
          <option>Seleccione mes</option>
          <?php for($ms=11;$ms>=0; $ms--){
            if($ms<(int)date("m")){
            if($ms<10){
              $ms="0".$ms;
            }
            $cal_mes=strtotime("-".$ms." Month", strtotime(date("Y-m")));
            $cal_mes=date("Y-m",$cal_mes);
            $lis_mes=explode("-",$cal_mes);

            ?>
            <option>{{$meses[$lis_mes[1]-1]}}/{{date("Y")}}</option>
          <?php }
          } ?>
        </select>
          <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="tabs active" id="pestana1"><a href="#oportunidad" aria-controls="home" class="active" role="tab" data-toggle="tab">Gestión de Oportunidades</a></li>
              <li role="presentation" class="tabs" id="pestana2"><a href="#comercial" aria-controls="profile" role="tab" data-toggle="tab">Gestión Comercial</a></li>
            </ul>
            <!-- PANEL NUMERO UNO -->
            <?php
              $valor=0; $h=0; $suma_meses=0;
              foreach ($oportunidades as $key) {
                  $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
                  $contador=count($historial);
                  if($contador>0){
                    if($historial[$contador-1]->value>=90){
                        $cierre_fecha=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "fecha_oc" order by id DESC');
                        if($cierre_fecha[0]->value >= date('Y-01-01') && $cierre_fecha[0]->value <= date('Y-12-31')){
                            $h++;
                        }


                      $prom_cierre=strtotime($historial[$contador-1]->created_at)-strtotime($key->created_at);
                      $prom_cierre=$prom_cierre/(60*60*24*30);
                      $suma_meses=$suma_meses+$prom_cierre;
                    $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                    foreach($info as $i){
                      if($i->key=="val_pesos"){
                        $valor=$valor+str_replace(",", ".", str_replace(".", "",$i->value));
                      }
                    }
                  }
                }
              }
              $valor=round($valor/1000000);
              if ($h===0) {$ttal_prom_cierre=0;}else {$ttal_prom_cierre=round($suma_meses/$h,1);}
             ?>
            <div class="tab-content bg-white" style="padding-bottom: 0;">
              <div role="tabpanel" class="tab-pane active" id="oportunidad">
              <div class="row caja-derecha">
                <div class="col-md-2 caja-tab negocios_cerrados">
                  <span class="cuadro-1-linea-1">Negocios cerrados</span>
                  <span class="cuadro-1-linea-2">{{$h}}</span>
                  <hr class="hr">
                  <!--<span class="cuadro-1-linea-3">$ {{number_format($valor,"0",".",",")}} M</span> CODIGO JESUS-->
                  <span class="cuadro-1-linea-3">$ {{ $total_negocio_cerrado }} M</span>
                </div>
                <div class="col-md-2 caja-tab negocios_cerrados">
                  <span class="cuadro-1-linea-1">Promedio de Cierre</span>
                  <!--<span class="cuadro-2-linea-2">{{$ttal_prom_cierre}}</span> CODIGO JESUS -->
                  <span class="cuadro-2-linea-2">
                  <?php
                  $noventaporciento=DB::select('SELECT * FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90" order by id');
                  $dias_totales=0;
                  foreach($noventaporciento as $n){
                    $fecha_inicio=DB::select('SELECT * FROM historial_oportunidades WHERE `key` = "fecha_ff" AND `oportunidad_id` = "'.$n->oportunidad_id.'" ORDER BY `id` DESC');

                    $fecha_cierre=DB::select('SELECT * FROM historial_oportunidades WHERE `key` = "fecha_oc" AND `oportunidad_id` = "'.$n->oportunidad_id.'" order by id DESC');
                    if(isset($fecha_cierre[0]->value) && isset($fecha_inicio[0]->value)){
                        if($fecha_cierre[0]->value >= date('Y-01-01') && $fecha_cierre[0]->value <= date('Y-12-31')){
                            if(((strtotime($fecha_cierre[0]->value))-(strtotime($fecha_inicio[0]->value)))>0){
                                $dias_totales+=(int)(((strtotime($fecha_cierre[0]->value))-(strtotime($fecha_inicio[0]->value)))/86400);
                            }
                        }
                    }
                  }
                  ?>
                  <?php if(isset($dias_totales) && isset($h) && $h>0){ $promedio=($dias_totales/$h)/30; $promedio=number_format($promedio,1,".",","); }else{ $promedio=0; } ?>{{$promedio}}</span>
                  <span class="cuadro-1-linea-1">Meses</span>
                </div>
                <?php
                  $valor=0; $h=0; $t=0;
                  foreach ($oportunidades as $key) {
                      $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
                      $fecha_cierre_perdida=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');

                        $contador=count($historial);
                        if($contador>0){
                            if($historial[$contador-1]->value==90){
                                  $fecha_cierre_vendida=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "fecha_oc" order by updated_at DESC LIMIT 0,1');
                                if(date('Y',strtotime($fecha_cierre_vendida[0]->value)) == date('Y')){
                                    $t++;
                                }
                              }
                            if((date("Y", strtotime($fecha_cierre_perdida[0]->updated_at)))==(date('Y'))){

                          if($historial[$contador-1]->value==0){
                            $t++;
                            $h++;
                            $prom_cierre=strtotime($historial[$contador-1]->created_at)-strtotime($key->created_at);
                            $prom_cierre=$prom_cierre/(60*60*24*30);
                            $suma_meses=$suma_meses+$prom_cierre;

                          $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "val_pesos" ORDER BY `id` DESC');
                          $valor+=((int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $info[0]->value)))));

                        }
                      }
                    }
                  }
                  $valor=round($valor/1000000);
                  if ($t===0) {$ttal_cierre_los=0;} else {$ttal_cierre_los=round($h*100/$t,1);}
                 ?>
                <div class="col-md-8 oportunidad_perdida">
                  <div class="titulo-division-2">Oportunidades Perdidas</div>
                  <div class="col-md-4 caja-tab-2"><span class="cuadro-3-linea-2">{{$ttal_cierre_los}} %</span><span class="cuadro-3-linea-1">Respecto al Total</span></div>
                  <div class="col-md-4 caja-tab-2"><span class="cuadro-3-linea-2">{{$h}}</span><span class="cuadro-3-linea-1">Oportunidades</span></div>
                  <div class="col-md-4 caja-tab-2"><span class="cuadro-3-linea-2">$ {{number_format($valor,"0",",",".")}}</span><span class="cuadro-3-linea-1" >Millones</span></div>
                </div>
              </div>
              <div class="row caja-derecha margen-superior">
                <div class="col-md-4 caja-tab negocios_cerrados2">
                    <h4>Oportunidades Identificadas<span>($ {{number_format(round($total_oportunidades_identificadas/1000000), 0, '.', ',')}} Millones)</span></h4>
                    <div id="donut-example" class="graph"></div>
                </div>
                <div class="col-md-8 oportunidad_identificada">
                  <div class="titulo-division-3">Oportunidades Identificadas</div>
                    <div id="morris-bar" class="graph"></div>
                </div>
              </div>
              <?php $p=$q=$r=$s=$t=$u=$v=$w=$y=$x=$z=0;
              $valorp=$valorq=$valorr=$valors=$valort=$valoru=$valorv=$valorw=$valorx=$valory=$valorz=0;
              foreach ($oportunidades as $key) {
                  $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
                  $contador=count($historial);
                  if($contador>0){
                    if($historial[$contador-1]->value==0){
                      $q++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorq=$valorp+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==10){
                      $r++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorr=$valorr+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==20){
                      $s++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valors=$valors+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==30){
                      $t++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valort=$valort+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==40){
                      $u++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valoru=$valoru+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==50){
                      $v++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorv=$valorv+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==60){
                      $w++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorw=$valorw+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==70){
                      $x++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorx=$valorx+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==80){
                      $y++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valory=$valory+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==90){
                      $z++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorz=$valorz+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }
                    if($historial[$contador-1]->value==100){
                      $p++;
                      $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' ');
                      foreach($info as $i){
                        if($i->key=="val_pesos"){
                          $valorp=$valorp+str_replace(",", ".", str_replace(".", "",$i->value));
                        }
                      }
                    }

                  }
                }
              $valorp=round($valorp/1000000);
              $valorq=round($valorq/1000000);
              $valorr=round($valorr/1000000);
              $valors=round($valors/1000000);
              $valort=round($valort/1000000);
              $valoru=round($valoru/1000000);
              $valorv=round($valorv/1000000);
              $valorw=round($valorw/1000000);
              $valorx=round($valorx/1000000);
              $valory=round($valory/1000000);
              $valorz=round($valorz/1000000);
             ?>
              <div class="row caja-derecha margen-superior" >
                <div class="col-md-8 caja-tab centrar-contenido ciclo_venta">
                  <div class="titulo-division-4">Ciclos de Venta</div>
                  <?php $total_ciclo=0; foreach($ciclos as $ciclo){
                    if($ciclo->id != 12){
                    $q=0; $valorq=0;
                    $nombre_ciclo=explode(" ",$ciclo->nombre);
                    foreach ($oportunidades as $key) {
                      $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
                      $contador=count($historial);
                      if($contador>0){
                        if($historial[$contador-1]->value==$ciclo->porcentaje){
                            if($historial[$contador-1]->value == 0){
                                if((date("Y", strtotime($historial[$contador-1]->updated_at)))==(date('Y'))){
                                    $permiso_ciclo_venta = "si";
                                }else{
                                    $permiso_ciclo_venta = "no";
                                }
                            }else if($historial[$contador-1]->value == 90){
                                $fecha_vendida=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "fecha_oc" ORDER BY id DESC LIMIT 0,1');
                                if((date("Y", strtotime($fecha_vendida[0]->value)))==(date('Y'))){
                                    $permiso_ciclo_venta = "si";
                                }else{
                                    $permiso_ciclo_venta = "no";
                                }
                            }else{
                                $permiso_ciclo_venta = "si";
                            }
                            if($permiso_ciclo_venta == "si"){
                              $total_ciclo++;
                              $q++;
                              $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "val_pesos" ORDER BY `id` DESC');
                              if(isset($info[0]->value)){
                                  $valorq+=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $info[0]->value)))));
                              }
                            }
                        }
                      }
                     }
                     /*Crear variables para guardarlas en localstorage parte 7*/
                      $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades vigentes por ciclo de venta";
                      $localstorage[$contador_ls]['campo'] = "ciclo de venta";
                      $localstorage[$contador_ls]['valor_campo'] = 'ciclo '.$ciclo->porcentaje.' %';
                      $contador_ls++;
                      $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades vigentes por ciclo de venta";
                      $localstorage[$contador_ls]['campo'] = "cantidad";
                      $localstorage[$contador_ls]['valor_campo'] = $q;
                      $contador_ls++;
                      $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades vigentes por ciclo de venta";
                      $localstorage[$contador_ls]['campo'] = "monto";
                      $localstorage[$contador_ls]['valor_campo'] = $valorq;
                      $contador_ls++;
                    /*Crear variables para guardarlas en localstorage parte 7*/
                    $valorq=round($valorq/1000000);

                    $datos_ciclo[$ciclo->porcentaje]['q']=$q;
                    $datos_ciclo[$ciclo->porcentaje]['valorq']=$valorq;
                    if($ciclo->imagen!=""){
                        $datos_ciclo[$ciclo->porcentaje]['imagen']=$ciclo->imagen;
                    }else{
                        $datos_ciclo[$ciclo->porcentaje]['imagen']='components/amchart/images/img1.png';
                    }
                    $datos_ciclo[$ciclo->porcentaje]['porcentaje']=$ciclo->porcentaje;
                    $datos_ciclo[$ciclo->porcentaje]['nombre_ciclo']=isset($nombre_ciclo[2])?$nombre_ciclo[2]:'';
                     } } ?>
                    <?php foreach($datos_ciclo as $dato){ ?>
                    <div class="item relativo">
                      <label class="linea1-item"><span class="blue"><?php echo $dato['q']; ?></span> Oport.</label>
                      <label class="linea2-item"><span class="blue">{{number_format($dato['valorq'],"0",",",".")}}</span> Millones</label>
                      <label class="linea3-item"><span class="blue"><?php echo round(($dato['q']*100)/$total_ciclo); ?></span>%</label>
                      <img src="{{url('/')}}/{{$dato['imagen']}}">
                      <div class="texto-final">
                      <span class="linea4-item">{{$dato['porcentaje']}}%</span>
                      <span class="linea5-item">{{$dato['nombre_ciclo']}}</span>
                      </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-md-2 caja-tab centrar-contenido VENTAS_ABRIR" style="cursor: pointer;">
                  <div class="titulo-division-4-v2">Ventas</div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="cuadro-3-linea-2" style="font-size: 300;">{{$total_equipos_vendidos}}</span>
                      <h6>Equipos</h6>
                      <hr class="hr">
                    </div>
                    <div class="col-md-12">
                      <span class="cuadro-3-linea-2" style="font-size: 300;">{{$total_pais_vendidos}}</span>
                      <h6>Países</h6>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 caja-tab centrar-contenido PROYECTADAS_ABRIR" style="cursor: pointer;">
                  <div class="titulo-division-4-v3">Proyecciones</div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="cuadro-3-linea-2" style="font-size: 300;">{{$total_equipos_proyectados}}</span>
                      <h6>Equipos</h6>
                      <hr class="hr">
                    </div>
                    <div class="col-md-12">
                      <span class="cuadro-3-linea-2" style="font-size: 300;">{{$total_pais_proyectados}}</span>
                      <h6>Países</h6>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row caja-derecha margen-superior dona-2 empresas_clientes">
                <div class="col-md-12 caja-tab">
                <div class="titulo-division-5">Empresas & Clientes</div>
                <?php
                $empre_oport=0; $empre_ident=0; $empr_new=0; $clientes_totales=0; $clien_new=0;
                 foreach($empresas as $empresa){
                  $responsable_de_la_empresa=DB::select('SELECT * FROM users WHERE cargo = "Ejecutivo Comercial" AND `id` = "'.$empresa->responsable.'"');
                  if(count($responsable_de_la_empresa)>0){
                    $empre_ident++;
                    $info=DB::select('SELECT * FROM oportunidades WHERE empresa_id = '.$empresa->id.' ');
                      if(count($info)>0){
                        foreach($info as $inf){
                          $son_oportunidad=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$inf->id.' AND `key` = "ciclo_venta" ORDER BY id DESC');
                          if((count($son_oportunidad))>0){
                            if(($son_oportunidad[0]->value)!=0 && ($son_oportunidad[0]->value)!=90){
                              $empre_oport++;
                              break;
                            }
                          }
                        }

                      }
                      $fecha=explode(" ",$empresa->created_at);
                      $cal_new=strtotime(date("Y-m-d H:i:s"))-strtotime($fecha[0]);
                      $cal_new=round($cal_new/(60*60*24));
                      if($cal_new<20){
                        $empr_new++;
                      }
                    }
                  }
                  foreach ($clientes as $cliente) {
                    $responsable_del_cliente=DB::select('SELECT * FROM users WHERE cargo = "Ejecutivo Comercial" AND `id` = "'.$cliente->user_id.'"');

                    if(count($responsable_del_cliente)>0){
                      $clientes_totales++;
                        $fecha=explode(" ",$cliente->created_at);

                        $cal_new=strtotime(date("Y-m-d H:i:s"))-strtotime($fecha[0]);
                        $cal_new=round($cal_new/(60*60*24));
                        if($cal_new<20){

                          $clien_new++;
                        }
                    }
                  }
                  ?>
                    <div class="item2">
                      <label class="linea1-item2">{{$empre_oport}}
                      <span class="linea2-item2">Empresas con Oportunidades</span></label>
                    </div>
                   <div class="item2">
                      <label class="linea1-item2">{{$empre_ident}}
                      <span class="linea2-item2">Empresas Identificadas</span></label>
                    </div>
                    <div class="item2">
                      <label class="linea1-item2">{{$clientes_totales}}
                      <span class="linea2-item2">Clientes Registrados</span></label>
                    </div>
                    <div class="item2">
                      <label class="linea1-item2">{{$empr_new}}
                      <span class="linea2-item2">Empresas Nuevas</span></label>
                    </div>
                    <div class="item2">
                      <label class="linea1-item2">{{$clien_new}}
                      <span class="linea2-item2">Clientes Nuevos</span></label>
                    </div>
                </div>
              </div>
              </div>
              <!-- PANEL DOS -->

              <div role="tabpanel" class="tab-pane" id="comercial">
                <div class="row caja-derecha">
                <div class="col-md-3 caja-lineaspark gasto_ventas">
                  <div class="card">
                      <div class="card-content purple white-text" style="height: 8.1rem;">
                          <p class="card-stats-title" style="margin-top: 4px;font-size: 1.3rem;">Costo de Venta</p>
                          <!--<h4 class="card-stats-number">{$gasto_ventas}%</h4>-->
                          <h4 class="card-stats-number" style="font-size: 3.8rem;margin: 2rem 0 0.2rem 0;">{{$porcentaje_gastos}}%</h4>
                      </div>
                      <div class="card-action purple darken-2">
                          <div id="sales-compositebar" class="center-align"></div>
                      </div>
                  </div>
                </div>
<?php
$fecha=strtotime("-1 Month",strtotime(date("Y-m")));
$mes_anterior=date("m",$fecha);

$ano_actual=date("Y",$fecha);
$mes_actual=date('m');
$total_presupuesto=0;
$total_presupuesto_a=0;
$total_presupuesto_m=0;
$ano_actual_real = date('Y');
foreach($presupuesto_cumplimiento_presupuestal as $presupuesto){
  if(((int)$presupuesto->mes)==((int)$mes_anterior) && ((int)$presupuesto->anio)==((int)$ano_actual)){
    $total_presupuesto+=((int)(str_replace(",", "", $presupuesto->alimentacion)))+((int)(str_replace(",", "", $presupuesto->transporte_interno)))+((int)(str_replace(",", "", $presupuesto->transporte_intermunicipal)))+((int)(str_replace(",", "", $presupuesto->tiquete_aereo)))+((int)(str_replace(",", "", $presupuesto->papeleria)))+((int)(str_replace(",", "", $presupuesto->invitacion_cliente)))+((int)(str_replace(",", "", $presupuesto->alquiler_vehiculo)))+((int)(str_replace(",", "", $presupuesto->gasolina_pasaje)))+((int)(str_replace(",", "", $presupuesto->hotel)))+((int)(str_replace(",", "", $presupuesto->otros)));
  }
  if(((int)$presupuesto->anio)==((int)$ano_actual_real) && ((int)$presupuesto->mes)<=((int)$mes_actual)){
    $total_presupuesto_a+=((int)(str_replace(",", "", $presupuesto->alimentacion)))+((int)(str_replace(",", "", $presupuesto->transporte_interno)))+((int)(str_replace(",", "", $presupuesto->transporte_intermunicipal)))+((int)(str_replace(",", "", $presupuesto->tiquete_aereo)))+((int)(str_replace(",", "", $presupuesto->papeleria)))+((int)(str_replace(",", "", $presupuesto->invitacion_cliente)))+((int)(str_replace(",", "", $presupuesto->alquiler_vehiculo)))+((int)(str_replace(",", "", $presupuesto->gasolina_pasaje)))+((int)(str_replace(",", "", $presupuesto->hotel)))+((int)(str_replace(",", "", $presupuesto->otros)));
  }
  if(((int)$presupuesto->mes)==((int)$mes_actual) && ((int)$presupuesto->anio)==((int)$ano_actual_real)){
     $total_presupuesto_m+=((int)(str_replace(",", "", $presupuesto->alimentacion)))+((int)(str_replace(",", "", $presupuesto->transporte_interno)))+((int)(str_replace(",", "", $presupuesto->transporte_intermunicipal)))+((int)(str_replace(",", "", $presupuesto->tiquete_aereo)))+((int)(str_replace(",", "", $presupuesto->papeleria)))+((int)(str_replace(",", "", $presupuesto->invitacion_cliente)))+((int)(str_replace(",", "", $presupuesto->alquiler_vehiculo)))+((int)(str_replace(",", "", $presupuesto->gasolina_pasaje)))+((int)(str_replace(",", "", $presupuesto->hotel)))+((int)(str_replace(",", "", $presupuesto->otros)));
  }
}
if($total_presupuesto>0){
  $e_p_u=$g['gastos_ultimo_mes']/$total_presupuesto*100;
}else{
  $e_p_u=0;
}
$e_p_u=number_format($e_p_u, 1, '.', ' ');

if($total_presupuesto_a>0){
  $e_p_a=$g['gastos_ano_actual']/$total_presupuesto_a*100;
}else{
  $e_p_a=0;
}
$e_p_a=number_format($e_p_a, 1, '.', ' ');

if($total_presupuesto_m>0){
  $p_m_e=$g['gastos_mes_actual']/$total_presupuesto_m*100;
}else{
  $p_m_e=0;
}
$p_m_e=number_format($p_m_e, 1, '.', ' ');
?>
                <div class="col-md-9 cumplimiento_presupuestal">
                  <div class="tab-titulo-division-2">Cumplimiento Presupuestal</div>
                  <div class="col-md-4 division-caja-tab-2 marron-1"><span class="tab-cuadro-3-linea-2">{{$e_p_u}}%</span><span class="tab-cuadro-3-linea-1">Cumplimiento Presupuesto Mes Anterior (Sin Ajuste) {{$meses[(((int)$mes_anterior)-1)]}}</span></div>
                  <div class="col-md-4 division-caja-tab-2 marron-2"><span class="tab-cuadro-3-linea-2">{{$p_m_e}}%</span><span class="tab-cuadro-3-linea-1" style="font-size: 9px !important; margin-bottom: 2%;">Cumplimiento Parcial Presupuesto <br>Mes Actual {{$meses[(((int)(date('m')))-1)]}}</span></div>
                  <div class="col-md-4 division-caja-tab-2 marron-3"><span class="tab-cuadro-3-linea-2">{{$data['e_p_a']}}%</span><span class="tab-cuadro-3-linea-1" style="font-size: 10px !important;"> Cumplimiento Presupuesto Acumulado (Con Ajuste) del año actual</span></div>
                  <!--<div class="col-md-3 division-caja-tab-2 marron-4"><span class="tab-cuadro-3-linea-2">{{$g['p_a_e']}}%</span><span class="tab-cuadro-3-linea-1" >Presupuesto Anual Ejecutado</span></div>-->
                </div>
              </div>
<!-----------------###########################################################################################################################3#------------>
             <div class="row ml-0 mr-0">
                <label class="absoluto padding-7 estilo_modificado">Uso del Tiempo</label>
                 <div class="col-md-6 col-sm-12">
                     <div id="uso_tiempo_chart"></div>
                 </div>
                 <div class="col-md-6 col-sm-12">
                 <ul class="list-group list-group-flush mt-5">
                  <li class="list-group-item p-0">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-3 list-head-child"><h2>90%</h2></div>
                              <div class="col-md-3 list-head-child">
                                  <h4>
                                      1150<br>
                                      <small class="text-muted">Horas</small>
                                    </h4>
                              </div>
                              <div class="col-md-6 list-head-child">
                                   <h4>
                                      Gestion Comercial<br>
                                      <small class="text-muted">Apertura del mercado</small>
                                    </h4>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item p-0">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-2 list-body-child">
                                  <h5>45%</h5>
                              </div>
                              <div class="col-md-3 list-body-child">
                                  <h5 class="font-weight-normal">1000 horas</h5>
                              </div>
                              <div class="col-md-3">
                                  <h5>Gestion Comercial</h5>
                              </div>
                              <div class="col-md-4">
                                  <h5 class="font-weight-normal">Planeacion</h5>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item p-0">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-2 list-body-child">
                                  <h5>45%</h5>
                              </div>
                              <div class="col-md-3 list-body-child">
                                  <h5 class="font-weight-normal">1000 horas</h5>
                              </div>
                              <div class="col-md-3">
                                  <h5>Gestion Comercial</h5>
                              </div>
                              <div class="col-md-4">
                                  <h5 class="font-weight-normal">Planeacion</h5>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item p-0">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-2 list-body-child">
                                  <h5>45%</h5>
                              </div>
                              <div class="col-md-3 list-body-child">
                                  <h5 class="font-weight-normal">1000 horas</h5>
                              </div>
                              <div class="col-md-3">
                                  <h5>Gestion Comercial</h5>
                              </div>
                              <div class="col-md-4">
                                  <h5 class="font-weight-normal">Planeacion</h5>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item p-0">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-2 list-body-child">
                                  <h5>45%</h5>
                              </div>
                              <div class="col-md-3 list-body-child">
                                  <h5 class="font-weight-normal">1000 horas</h5>
                              </div>
                              <div class="col-md-3">
                                  <h5>Gestion Comercial</h5>
                              </div>
                              <div class="col-md-4">
                                  <h5 class="font-weight-normal">Planeacion</h5>
                              </div>
                          </div>
                      </div>
                  </li>
                </ul>
                 </div>
             </div>

              <div class="caja-tab caja-derecha margen-superior distin-fondo actividades_pendientes">
              <label class="titulo-pendientes" style="color: #fff; background-color: #014eda; width: 100%;">Gestión de Actividades Pendientes</label>
              <div class="width-100 caja-contenedora">
                <div class="width-20 subcaja">
                  <span class="blue tamano-30">{{$gestion['hechas_tiempo']}} %</span>
                  <div class="tres-dos"><span class="linea-menor">Actividades Hechas a Tiempo</span></div>
                </div>
                <div class="width-20 subcaja">
                  <span class="blue tamano-30">{{$gestion['pendientes_semana']}}</span>
                  <div class="tres-dos"><span class="linea-menor">Actividades Pendientes Semana</span></div>
                </div>
                <div class="width-20 subcaja">
                  <span class="blue tamano-30">{{$gestion['pendientes_mes']}}</span>
                  <div class="tres-dos"><span class="linea-menor">Actividades Pendientes Mes</span></div>
                </div>
                <div class="width-20 subcaja">
                  <span class="blue tamano-30">{{$gestion['aplazadas_mes']}}</span>
                  <div class="tres-dos"><span class="linea-menor">Actividades Aplazadas</span></div>
                </div>
              </div>
              </div>
              <div class="row caja-tab caja-derecha margen-superior cumplimiento_llenado">
                <div class="col-md-12" id="cumplimiento_llenado_contenido">
                  <img src="{{url('/')}}/images/configuracion_crm/configuracion.gif">
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        @if($data['permiso_guardar_cierre_ano']=="Si")
        <div class="row">
            <div class="col-md-12">
                <a href="#" class="snip1489 btn_save_comparacion"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
            </div>
        </div>
        @endif
    </div>

    <!-- INICIO DE MODALES PAGINA UNO -->
<div class="modal fade" id="VENTAS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 10%; max-height: 600px;">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header margen-titulo i-ventas" style="margin-top: 25%;">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
            <h4 class="modal-title" id="myModalLabel">Ventas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row margen-inferior">
              <div class="col-md-12 borde-derecho" style="height: 425px" id="graf1">

              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="height: 445px" id="graf2">

              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="height: 650px" id="graf3">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="modal fade" id="PROYECTADAS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 10%; max-height: 600px;">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header margen-titulo i-proyectadas" style="margin-top: 28%;">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
            <h4 class="modal-title" id="myModalLabel">Proyectadas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row margen-inferior">
              <div class="col-md-12 borde-derecho" style="height: 425px" id="graf4">

              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="height: 445px" id="graf5">

              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="height: 1296px" id="graf6">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<div class="modal fade" id="negocios_cerrados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header margen-titulo">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
            <h4 class="modal-title" id="myModalLabel">Negocios Cerrados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row margen-inferior">
              <div class="col-md-7 borde-derecho">
                <h6 id="h6"><span class="blue">Monto</span> de Negocios Cerrados</h6>
                <span id="millones">(Millones)</span>
                <div id="lista-comercial"></div>
              </div>
              <div class="col-md-5">
               <div class="col-md-12 centrar-contenido borde-inferior">
               <h6 id="h6"><span class="blue">Cantidad</span> de Negocios Cerrados</h6>
                 <div id="barra-horizontal"></div>
               </div>
               <div class="col-md-12 centrar-contenido">
                 <h6 id="h6"><span class="blue">Promedio</span> Cierre Negocios</h6>
                 <span id="millones">(Meses)</span>
                 <div id="pie"></div>
               </div>
               </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<div class="modal fade" id="oportunidad_perdida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header margen-titulo" style="margin-top: 3%;">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Oportunidades Perdidas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row margen-inferior">
            <div class="col-md-4 borde-derecho">
              <h6 id="h6"><span class="blue">Monto</span> de Negocios Perdidos</h6>
              <span id="millones">(Millones)</span>
              <div id="negocio_perdido"></div>
            </div>
            <div class="col-md-4 borde-derecho">
              <h6 id="h6"><span class="blue">Oportunidades</span> Perdidas</h6>
              <span>(Porcentaje)</span>
              <div id="oportunidad_perdida_sub">
              <div id="contenedor-perdida">
                  <div class="lista-caja borde-inferior">
                  <?php
                  $valor=0; $x=0; $i=0; $mayor=0;
                  foreach ($usuarios as $usuario) {

                    if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){

                      $h=0;
                      $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');
                      foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                        if(($oportunidad_buscar[0]->value)==$usuario->id){
                          $fecha_oc_per=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');
                          if((date("Y", strtotime($fecha_oc_per[0]->updated_at)))==(date('Y'))){
                            $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" order by id');
                            $contador=count($historial);
                            if($contador>0){
                              if($historial[$contador-1]->value==0){
                                $h++;
                                $x++;
                              }
                            }
                          }
                        }
                      }

                      $vector[$i]=$h;
                      if($vector[$i]>$mayor){
                        $mayor=$vector[$i];
                      }
                      $i++;
                    }
                  }
                  $p=0;

                  foreach ($usuarios as $usuario) {
                    $total_oportunidades=0;
                    if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
                      $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND (`value` = "0" OR `value` = "90"))');
                      foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                        if(($oportunidad_buscar[0]->value)==$usuario->id){
                          $fecha_oc_per1=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" order by id DESC LIMIT 0,1');
                          if((date("Y", strtotime($fecha_oc_per1[0]->updated_at)))==(date('Y'))){
                            $total_oportunidades++;
                          }
                        }
                      }

                      $nombres=explode(" ",$usuario->nombres);
                      if($total_oportunidades>0){
                        $total_porc_perdida=($vector[$p]*100)/$total_oportunidades;
                        $total_porc_perdida=round($total_porc_perdida, 1);
                      }else{
                        $total_porc_perdida=0;
                      }
                      /*$total_porc_perdida1=($vector[$p]*100)/$mayor;*/

                      /*Crear variables para guardarlas en localstorage parte 5*/
                          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades perdidas";
                          $localstorage[$contador_ls]['campo'] = "porcentaje perdidas";
                          $localstorage[$contador_ls]['valor_campo'] = $total_porc_perdida;
                          $contador_ls++;
                     /*Crear variables para guardarlas en localstorage parte 5*/
                 ?>
                    <div class="row" style="margin-bottom: 2%;">
                      <div class="col-md-3" style="text-align: left;">
                            <p class="collections-title"><?php echo $nombres[0]; ?></p>
                        </div>
                        <div class="col-md-3" style="text-align: left; background-color: #0093c9 !important; border-radius: 5px;">
                            <span class="task-cat pink accent-2" style="font-size: 1.8rem !important; width: 63.5px; background-color: transparent !important;">{{$total_porc_perdida}}%</span>
                        </div>
                        <div class="col-md-6">
                            <div class="progress" style="cursor: pointer;" title="{{$vector[$p]}} @if($vector[$p]==1) oportunidad perdida @else oportunidades perdidas @endif">
                                 <div class="determinate" style="width: {{$total_porc_perdida}}%"></div>
                            </div>
                        </div>
                    </div>
                  <?php $p++; } } ?>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 centrar-contenido">
              <h6 id="h6"><span class="blue">Cantidad</span> de Negocios Perdidos</h6>
              <div id="cantidad-perdida"></div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="modal fade" id="oportunidad_identificada" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="margin-top: 11%;">
        <div class="modal-header" style="margin-top: 4.5%">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Oportunidades Identificadas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row margen-inferior">
            <div class="col-md-12">
              <div class="titulo-graficas">
                <!--<div class="linea-titulo-grafica"><span class="blue">Corto</span> Plazo (Millones)</div>
                <div class="linea-titulo-grafica"><span class="blue">Mediano</span> Plazo (Millones)</div>
                <div class="linea-titulo-grafica"><span class="blue">Largo</span> Plazo (Millones)</div>-->
                <div class="row">
                <div style="width:22%"></div>
                  <div style="width: 23.3%">
                    <span class="blue">Corto</span> Plazo (Millones)
                  </div>
                  <div style="width: 23.3%">
                    <span class="blue">Mediano</span> Plazo (Millones)
                  </div>
                  <div style="width: 23.3%">
                    <span class="blue">Largo</span> Plazo (Millones)
                  </div>
                </div>
              </div>
            </div>
            <?php
            $y=0;
            foreach($usuarios as $usuario){
              if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
                $nombres=explode(" ",$usuario->nombres);
                if($usuario->foto==""){
                  $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                }else{
                  $foto=url('/')."/images/file/clientes/".$usuario->foto;
                }
              ?>
            <div class="col-md-12 margen-contenedor">
              <div class="borde-inferior-2" style="padding-bottom: 35px;">
              <div class="divimagen" style="background-image: url('{{$foto}}');"><span class="blue nombre-avatar">{{$nombres[0]}}</span></div>
              <div class="cuadro">
                <div class="cuadro-color-azul"></div><div class="titulo-color">Alta</div>
                <div class="cuadro-color-verde"></div><div class="titulo-color">Media</div>
                <div class="cuadro-color-gris"></div><div class="titulo-color">Baja</div>
              </div>
              <div class="graficas" style="width: 70%">
                <div class="graficas-alta" id="graficas-alta{{$y}}"></div>
                <div class="graficas-mediano" id="graficas-mediano{{$y}}"></div>
                <div class="graficas-corto" id="graficas-corto{{$y}}"></div>
              </div>
            </div>
            </div>
            <?php $y++; } } ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ciclo_venta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="margin-top: 1.2%;">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Ciclos de Ventas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body tabla-padding">
          <table class="tabla">
            <tr>
              <td colspan="1">

              </td>
              <?php foreach($ciclos as $ciclo){
                if($ciclo->id != 12){
                $nombres_ciclos=explode(" ",$ciclo->nombre);
               ?>
              <td class="borde-azul">

                <?php if($ciclo->imagen!=""){ ?>
                  <img src="{{url('/')}}/{{$ciclo->imagen}}">
                <?php }else{?>
                  <img src="{{ url('/') }}/components/amchart/images/img1.png">
                <?php }?>
                <span class="linea4-item">{{$ciclo->porcentaje}}%</span>
                @if(isset($nombres_ciclos[2]))<span class="linea5-item">{{$nombres_ciclos[2]}}</span>@endif
              </td>
              <?php } } ?>
            </tr>
            <?php $o=0;
            foreach($usuarios as $usuario){
              if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
              $nombres=explode(" ",$usuario->nombres);
              if($usuario->foto==""){
                  $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                }else{
                  $foto=url('/')."/images/file/clientes/".$usuario->foto;
                }
              ?>
              <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('{{$foto}}');"><span class="blue nombre-avatar-2"><?php echo $nombres[0]; ?></span></div>
              </td>
              <?php $k=0;
                  $cantidad_ls=0;
                  $monto_ls=0;
              foreach($ciclos as $ciclo){
                if($ciclo->id != 12){
                $q=0; $t=0; $valorq=0; $ttalindividual=0;
                $nombre_ciclo=explode(" ",$ciclo->nombre);

                foreach($oportunidades as $key){
                  $responsable=DB::select('SELECT * FROM historial_oportunidades WHERE `key` = "responsable" AND `oportunidad_id` = '.$key->id.' ORDER BY id DESC');
                  $ciclo_op=DB::select('SELECT * FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `oportunidad_id` = '.$key->id.' ORDER BY id DESC');
                  if(($responsable[0]->value)==($usuario->id)){
                      if($ciclo_op[0]->value == 0){
                          $f_perdida = DB::select('SELECT updated_at FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND oportunidad_id = '.$key->id.' ORDER BY id DESC LIMIT 0,1');
                          if((date("Y", strtotime($f_perdida[0]->updated_at)))==(date('Y'))){
                              $t++;
                              if(($ciclo_op[0]->value)==($ciclo->porcentaje)){
                                    $q++;
                                    if($ciclo_op[0]->value != 0 && $ciclo_op[0]->value != 90){
                                        $cantidad_ls++;
                                    }
                                    $valor_ciclo_venta=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "val_pesos" ORDER BY `id` DESC');
                                    if(isset($valor_ciclo_venta[0]->value)){
                                      $valorq+=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor_ciclo_venta[0]->value)))));
                                    }
                                }
                          }
                      }else if($ciclo_op[0]->value == 90){
                          $f_vendida = DB::select('SELECT value FROM historial_oportunidades WHERE `key` = "fecha_oc" AND oportunidad_id = '.$key->id.' ORDER BY id DESC LIMIT 0,1');
                          if((date("Y", strtotime($f_vendida[0]->value)))==(date('Y'))){
                              $t++;
                              if(($ciclo_op[0]->value)==($ciclo->porcentaje)){
                                    $q++;
                                    if($ciclo_op[0]->value != 0 && $ciclo_op[0]->value != 90){
                                        $cantidad_ls++;
                                    }
                                    $valor_ciclo_venta=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "val_pesos" ORDER BY `id` DESC');
                                    if(isset($valor_ciclo_venta[0]->value)){
                                      $valorq+=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor_ciclo_venta[0]->value)))));
                                    }
                                }
                          }
                      }else{
                          $t++;
                          if(($ciclo_op[0]->value)==($ciclo->porcentaje)){
                                $q++;
                                if($ciclo_op[0]->value != 0 && $ciclo_op[0]->value != 90){
                                    $cantidad_ls++;
                                }
                                $valor_ciclo_venta=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "val_pesos" ORDER BY `id` DESC');
                                if(isset($valor_ciclo_venta[0]->value)){
                                  $valorq+=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor_ciclo_venta[0]->value)))));
                                }
                            }
                      }
                  }

                  }
                  if($ciclo_op[0]->value != 0 && $ciclo_op[0]->value != 90){
                        $monto_ls+=$valorq;
                  }

                  $valorq=round($valorq/1000000);
                  if($t!=0){
                    $porc_q=round(($q/$t)*100);
                  }else{
                    $porc_q=0;
                  }

                ?>
                <?php if($o%2==0){ ?>
                <td class="<?php if($k%2==0){ ?> celda-azul <?php }else{ ?> celda-celeste <?php } ?>">
                  <label class="linea3-item2"><?php echo $porc_q; ?>%</label>
                  <label class="linea2-item"><span class="blue"><?php echo number_format($valorq,"0",",","."); ?></span> Millones</label>
                  <label class="linea1-item"><span class="blue"><?php echo $q; ?></span> Oport.</label>
                </td>
                <?php }else{ ?>
                <td class="<?php if($k%2==0){ ?> celda-celeste <?php }else{ ?>  <?php } ?>">
                  <label class="linea3-item2"><?php echo $porc_q; ?>%</label>
                  <label class="linea2-item"><span class="blue"><?php echo number_format($valorq,"0",",","."); ?></span> Millones</label>
                  <label class="linea1-item"><span class="blue"><?php echo $q; ?></span> Oport.</label>
                </td>
                <?php }
                  $k++;
                }
              }
                  /*Crear variables para guardarlas en localstorage parte 6*/
                  $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades vigentes por comercial";
                  $localstorage[$contador_ls]['campo'] = "funcionario";
                  $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
                  $contador_ls++;
                  $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades vigentes por comercial";
                  $localstorage[$contador_ls]['campo'] = "cantidad";
                  $localstorage[$contador_ls]['valor_campo'] = $cantidad_ls;
                  $contador_ls++;
                  $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades vigentes por comercial";
                  $localstorage[$contador_ls]['campo'] = "monto";
                  $localstorage[$contador_ls]['valor_campo'] = $monto_ls;
                  $contador_ls++;
                /*Crear variables para guardarlas en localstorage parte 6*/
                ?>
            </tr>
            <?php $o++; } }  ?>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="empresas_clientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="margin-top: 2%">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Empresas & Clientes</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <table class="width-100">
                <tr>
                  <td></td>
                  <td colspan="2" class="cabecera-tabla numero-titulo">{{$empre_ident}}<span class="letra-titulo">Empresas identificadas</span></td>
                  <td colspan="2" class="cabecera-tabla numero-titulo">{{$clientes_totales}}<span class="letra-titulo">Clientes registrados</span></td>
                </tr>
                <tr >
                  <td></td>
                  <td colspan="1" class="subcabecera-tabla">Total</td>
                  <td colspan="1" class="subcabecera-tabla">Nuevos</td>
                  <td colspan="1" class="subcabecera-tabla">Total</td>
                  <td colspan="1" class="subcabecera-tabla">Nuevos</td>
                </tr>
                <?php
                foreach($usuarios as $usuario){
                  if(($usuario->cargo)=='Ejecutivo Comercial') {
                  $nombres=explode(" ",$usuario->nombres);
                  if($usuario->foto==""){
                    $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                  }else{
                    $foto=url('/')."/images/file/clientes/".$usuario->foto;
                  }
                  $empre_ttal=0; $empr_newi=0; $clien_newi=0; $clien_ttal=0;


                 foreach($empresas as $empresa){

                  if($empresa->responsable==$usuario->id){
                    $historial=DB::select('SELECT * FROM oportunidades WHERE empresa_id = '.$empresa->id.'');
                      $empre_ttal++;
                      /*if(count($historial)<1){
                        $empre_ttal++;
                      }*/

                      $fecha=explode(" ",$empresa->created_at);
                      $cal_new=strtotime(date("Y-m-d"))-strtotime($fecha[0]);
                      $cal_new=round($cal_new/(60*60*24));
                      if($cal_new<20){
                        $empr_newi++;
                      }

                    }
                  }

                  foreach ($clientes as $cliente) {
                    $fecha=explode(" ",$cliente->created_at);
                      if($cliente->user_id==$usuario->id){
                      $clien_ttal++;
                      $cal_new=strtotime(date("Y-m-d"))-strtotime($fecha[0]);
                      $cal_new=round($cal_new/(60*60*24));
                      if($cal_new<20){
                        $clien_newi++;
                      }
                    }
                   }
                   /*Crear variables para guardarlas en localstorage parte 10*/
                      $localstorage[$contador_ls]['tipo_informacion'] = "Clientes y contactos";
                      $localstorage[$contador_ls]['campo'] = "funcionario";
                      $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
                      $contador_ls++;
                      $localstorage[$contador_ls]['tipo_informacion'] = "Clientes y contactos";
                      $localstorage[$contador_ls]['campo'] = "cliente";
                      $localstorage[$contador_ls]['valor_campo'] = $empre_ttal;
                      $contador_ls++;
                      $localstorage[$contador_ls]['tipo_informacion'] = "Clientes y contactos";
                      $localstorage[$contador_ls]['campo'] = "contactos";
                      $localstorage[$contador_ls]['valor_campo'] = $clien_ttal;
                      $contador_ls++;
                    /*Fin Crear variables para guardarlas en localstorage parte 10*/
                ?>
                <tr>
                  <td colspan="1" class="td-85">
                    <div class="divimagen-2" style="background-image: url('{{$foto}}');"><span class="blue nombre-avatar-2">{{$nombres[0]}}</span>
                    <div class="bandera" style="background-image: url('{{url('/')}}/images/icons/{{$usuario->principal}}')";></div></div>
                  </td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">{{$empre_ttal}}</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">{{$empr_newi}}</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">{{$clien_ttal}}</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">{{$clien_newi}}</td>
                </tr>
                <?php } } ?>
              </table>
            </div>
            <div class="col-md-6">
            <h6 class="h6"><span class="blue">% de Empresas</span> por cada Comercial</h6>
              <div id="dona-2">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="cono" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header margen-titulo">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Cono de Oportunidades</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12 derecha">
                <div class="width-30 centrar-contenido"><span class="blue">Alta</span></label>
                </div>
                <div class="width-30 centrar-contenido"><span class="blue">Media</span></label>
                </div>
                <div class="width-30 centrar-contenido"><span class="blue">Baja</span></label>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="width-10 height-200"><label class="absoluto bottom-0"><span class="blue">Corto</span> Plazo <br><span class="blue" id="total_corto"></span></label></div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-1"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-2"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-3"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="width-10 height-200"><label class="absoluto bottom-0"><span class="blue">Mediano</span> Plazo <br><span class="blue" id="total_mediano"></span></label></div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-4"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-5"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-6"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="width-10 height-200"><label class="absoluto bottom-0"><span class="blue">Largo</span> Plazo <br><span class="blue" id="total_largo"></span></label></div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-7"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-8"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-9"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="negocios_cerrados2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="margin-top: 2.5%;">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Oportunidades Identificadas <span>($ {{number_format(round($total_oportunidades_identificadas/1000000), 0, '.', ',')}} Millones)</span></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
         <div class="row">
        <div class="col-md-4 centrar-contenido">
          <label>(Millones)<span class="blue bloque">por Comercial</span></label>
        </div>
        <div class="col-md-4 centrar-contenido">
          <label>(Millones)<span class="blue bloque">Probabilidad Alta/Media/Baja</span></label>
        </div>
        <div class="col-md-4 centrar-contenido">
          <label>(Millones)<span class="blue bloque">Corto/Mediano/Largo Plazo</span></label>
        </div>
        </div>
        <div class="row">
        <div class="col-md-4">
          <div class="dona" id="dona-3"></div>
        </div>
        <div class="col-md-4">
          <div class="dona" id="dona-4"></div>
        </div>
        <div class="col-md-4">
          <div class="dona" id="dona-5"></div>
        </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- INICIO DE MODALES PAGINA DOS -->
  <?php
    $modulo=2;
    $acceso = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Modal Gastos de ventas" AND `id_permisomodulo`="'.$modulo.'"');
    if(isset($acceso[0]->id)){
      $cargo = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
      if(isset($cargo[0]->id)){
        $permiso = Illuminate\Support\Facades\DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
        if(isset($permiso[0]->permiso)){
          if($permiso[0]->permiso == "Si"){ ?>
            <div class="modal fade" id="gasto_ventas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="margin-top: 1.2%;">
                  <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
                    <h4 class="modal-title" id="myModalLabel">Costo de Venta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                   <div class="row">
                   <?php foreach($gasto_x_usuario as $g){ ?>
                   <div class="col-md-3">
                    <div class="caja-avatar">
                      <div class="divimagen-2 centrar-contenido" style="background-image: url('{{ url('/') }}/images/file/clientes/{{ $g['foto'] }}');">
                      <img src="{{url('/')}}/images/icons/{{$g['pais']}}" class="bandera"></div>
                    </div>
                      <div class="card">
                          <div class="card-content blue-sky white-text">
                              <h4 class="titulo-card">{{$g['nombre']}}</h4>
                              <p class="card-stats-title" style="margin-top: 6%;font-size: 0.9rem;"><span class="texto-sombra" style="font-size: 60px;">{{$g['porcentaje']}} %</span> Costo de Venta</p>
                              <h4 class="card-stats-number" style="font-size: 1.1rem;margin: 0.5rem 0 0.2rem 0;">Gastos Totales <span>$ {{$g['gasto']}}</span></h4>
                          </div>
                          <div class="card-action1 blue-sky darken-2">
                              <div class="sales-compositebar1 center-align"></div>
                          </div>
                      </div>
                    </div>
                    <?php
                    /*Crear variables para guardarlas en localstorage parte 12*/
                      $localstorage[$contador_ls]['tipo_informacion'] = "Costo venta";
                      $localstorage[$contador_ls]['campo'] = "funcionario";
                      $localstorage[$contador_ls]['valor_campo'] = $g['id_user'];
                      $contador_ls++;
                      $localstorage[$contador_ls]['tipo_informacion'] = "Costo venta";
                      $localstorage[$contador_ls]['campo'] = "porcentaje";
                      $localstorage[$contador_ls]['valor_campo'] = $g['porcentaje'];
                      $contador_ls++;
                    /*Fin Crear variables para guardarlas en localstorage parte 12*/

                    } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php
          }
        }
      }
    }
  ?>


<div class="modal fade" id="cumplimiento_presupuestal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="margin-top: 1.2%">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Cumplimiento Presupuestal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body mt-5">
        <table class="tabla-2">
            <tr>
              <td colspan="2">

              </td>
              <td class="cabecera-marron-1">
                <!--<span class="titulo-marron-1">Ejecución Presupuesto Ultimo Mes</span>-->
                  <span class="titulo-marron-1">Cumplimiento Presupuesto Mes Anterior (Sin Ajuste) {{$meses[(((int)$mes_anterior)-1)]}}</span>
              </td>
              <td class="cabecera-marron-2">
                <!--<span class="titulo-marron-1">Ejecución Presupuesto Acumulado</span>-->
                <span class="titulo-marron-1">Cumplimiento Parcial Presupuesto Mes Actual {{$meses[(((int)date('m'))-1)]}}</span>
              </td>
              <!--<td class="cabecera-marron-3">
                <span class="titulo-marron-1">Ejecucion Parcial presupuesto {{$meses[(((int)date('m'))-1)]}}</span>
              </td>-->
              <td class="cabecera-marron-4">
                <!--<span class="titulo-marron-1">Presupuesto Anual Ejecutado</span>-->
                <span class="titulo-marron-1">Cumplimiento Presupuesto Acumulado (Con Ajuste) del año actual</span>
              </td>
            </tr>
            <?=$data['modal_cumplimiento_presupuestal']?>

          </table>
        </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="uso_tiempo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="margin-top: 1%">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Uso del Tiempo</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <table class="tabla-3">
            <tr>
              <td colspan="1">

              </td>
              <?php foreach ($tipo_actividades as $t_a) { ?>
                <td class="relativo tamano-110" style="cursor: pointer;" title="{{$t_a->concepto}}">
                  @if($t_a->foto == '')
                  <img src="{{url('/')}}/images/icons/sin_imagen.png" class="img-caja" style="width: 35px;">
                  @else
                  <img src="{{url('/')}}/images/icons/{{$t_a->foto}}" class="img-caja" style="width: 35px;">
                  @endif
                  <div class="caja-negra"><span>{{$t_a->concepto}}</span></div>
                </td>
              <?php } ?>

            </tr>
            <?php foreach ($usuarios as $usuario) {
              if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
              $nombre=explode(' ', $usuario->name);
                  $mayor_porcentaje=0;
                  $mayor_porcentaje_concepto='';
              ?>
              <tr>
                <td colspan="1" class="td-85">
                  <div class="divimagen-2" style="background-image: url('{{ url('/') }}/images/file/clientes/{{$usuario->foto}}');"><span class="blue nombre-avatar-2">{{$nombre[0]}}</span></div>
                </td>
                <?php
                $mis_actividades=DB::select('SELECT `tipo_actividad`, SUM(`tiempo_fin` - `tiempo_inicio`) AS `horas` FROM `actividades` WHERE `user_id` = "'.$usuario->id.'" GROUP BY `tipo_actividad`');
                $total_horas_actividad_x_usuario=0;
                foreach ($mis_actividades as $m_ac) {
                  $total_horas_actividad_x_usuario+=$m_ac->horas;
                }

                foreach($tipo_actividades as $t_a){
                  $numero_tipo_actividad_x_usuario=DB::select('SELECT `tipo_actividad`, SUM(`tiempo_fin` - `tiempo_inicio`) AS `horas` FROM `actividades` WHERE `user_id` = '.$usuario->id.' AND `tipo_actividad` = "'.$t_a->concepto.'" GROUP BY `tipo_actividad` ORDER BY `horas` DESC');
                 ?>
                <td class="borde-derecho" id="{{$usuario->id}}_{{$t_a->id}}">
                  <label class="blue linea3-item3">@if($total_horas_actividad_x_usuario>0 && isset($numero_tipo_actividad_x_usuario[0]->horas)) {{number_format((round($numero_tipo_actividad_x_usuario[0]->horas/$total_horas_actividad_x_usuario*100)),"0","."," ")}} <?php $valoresU[$usuario->id][$t_a->id]=(int)(number_format((round($numero_tipo_actividad_x_usuario[0]->horas/$total_horas_actividad_x_usuario*100)),"0","."," ")); $idU[$usuario->id][$t_a->id]=$usuario->id.'_'.$t_a->id; if($mayor_porcentaje<$valoresU[$usuario->id][$t_a->id]){ $mayor_porcentaje = $valoresU[$usuario->id][$t_a->id]; $mayor_porcentaje_concepto = $t_a->concepto; } ?> @else 0 @endif %</label>
                </td>
                <?php } ?>

              </tr>
            <?php
               /*Crear variables para guardarlas en localstorage parte 11*/
                  $localstorage[$contador_ls]['tipo_informacion'] = "Uso del tiempo";
                  $localstorage[$contador_ls]['campo'] = "funcionario";
                  $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
                  $contador_ls++;
                  $localstorage[$contador_ls]['tipo_informacion'] = "Uso del tiempo";
                  $localstorage[$contador_ls]['campo'] = "porcentaje";
                  $localstorage[$contador_ls]['valor_campo'] = $mayor_porcentaje;
                  $contador_ls++;
                  $localstorage[$contador_ls]['tipo_informacion'] = "Uso del tiempo";
                  $localstorage[$contador_ls]['campo'] = "concepto";
                  $localstorage[$contador_ls]['valor_campo'] = $mayor_porcentaje_concepto;
                  $contador_ls++;
                /*Fin Crear variables para guardarlas en localstorage parte 11*/
              } }  ?>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="actividades_pendientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="margin-top: 4%;">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Gestión de Actividades Pendientes</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-12 derecha">
              <div class="width-100 centrar-contenido"><span class="blue">Mes/Semana</span></label>
              </div>
            </div>
        </div>
        <div class="row">
          <dv class="col-md-12">
          <div id="linea-pendiente">

          </div>
          </dv>
        </div>
        <div class="row">
            <div class="col-md-6 derecha">
              <div class="width-100 centrar-contenido">% <span class="blue">Hechas a Tiempo</span></label>
              </div>
            </div>
            <div class="col-md-6 derecha">
              <div class="width-100 centrar-contenido"><span class="blue">Aplazadas</span> por cada comercial</label>
              </div>
            </div>
        </div>
        <div class="row">
        <dv class="col-md-6">
          <div id="radar-pendiente">

          </div>
        </dv>
        <dv class="col-md-6">
          <div id="radar2-pendiente">

          </div>
        </dv>
        </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="cumplimiento_llenado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Cumplimiento Llenado</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" style="height: 450px;">
          <div class="row">
              <div class="col-md-12 derecha">
                <div class="width-30 centrar-contenido">% <span class="blue">Llenado de bitacora</span></label>
                </div>
                <div class="width-30 centrar-contenido">% <span class="blue">Llenado semanal</span></label>
                </div>
                <div class="width-30 centrar-contenido">% <span class="blue">Presupuesto Mensual</span></label>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="dona" id="detallado-llenado-bitacora"></div> <!-- Antigua  -->
            </div>
            <div class="col-md-4">
              <div class="dona" id="detallado-cumplimiento-semanal"></div>
            </div>
            <div class="col-md-4">
              <div class="dona" id="detallado-presupuesto-mensual"></div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>

 <?php
    $altaalta=0;
    $altamedia=0;
    $altacorta=0;
    $mediaalta=0;
    $mediamedia=0;
    $mediacorta=0;
    $bajaalta=0;
    $bajamedia=0;
    $bajacorta=0;
  foreach ($oportunidades as $key) {
    $fecha="";
    $valor=0;
    $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
    $contador=count($historial);
    if($contador>0){
      if($historial[$contador-1]->value>0&&$historial[$contador-1]->value<90){

        $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "fecha_oc" order by id DESC');
        if(count($plazo)>0){
          $dias=(strtotime($plazo[0]->value))-(strtotime(date('Y-m-d')));

          $dias=(int)$dias;
          $dias=$dias/86400;
          $dias=(int)$dias;

          $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "probabilidad" order by id DESC');
          $valor_1=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "val_pesos" order by id DESC');
		  if(count($valor_1)>0){
			$valor_total=(int)(str_replace(",", "", str_replace(".", "", str_replace("$ ", "", $valor_1[0]->value))));
		  }else{
			$valor_total=0;
		  }
          if($dias<=($metas1->corto_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $bajaalta+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $bajamedia+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $bajacorta+=$valor_total;
              }
            }
          }elseif($dias<=($metas1->medio_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $mediaalta+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $mediamedia+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $mediacorta+=$valor_total;
              }
            }
          }elseif($dias>($metas1->medio_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $altaalta+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $altamedia+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $altacorta+=$valor_total;
              }
            }
          }
        }

      }
    }

  }
?>
<?php $probabilidades_list=["Alta","Media","Baja"]?>

@endsection

@section('scripts')

<!-- amCharts javascript sources -->

<script src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
<script src="{{ url('/') }}/components/amchart/js/funnel.js"></script>
<script src="{{ url('/') }}/components/amchart/js/serial.js"></script>
<script src="{{ url('/') }}/components/amchart/js/pie.js"></script>
<script src="{{ url('/') }}/components/amchart/js/gauge.js"></script>
<script src="{{ url('/') }}/components/amchart/js/radar.js"></script>
<script src="{{ url('/') }}/components/amchart/js/export.min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/light.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/morris.min.js"></script>
<!--<script type="text/javascript" src="{{ url('/')}}/js/material/morris-script.js"></script>-->
<script src="{{ url('/') }}/components/amchart/js/raphael-min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/morris.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/sparkline-script.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/dark.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
    console.log( screen.width );
    if(screen.width <= 1366){
       $('.i-proyectadas').css('margin-top','33%');
       $('.i-ventas').css('margin-top','17%');
    }else if(screen.width <= 1920){
       $('.i-proyectadas').css('margin-top','24%');
       $('.i-ventas').css('margin-top','13%');
    }

    AmCharts.makeChart("uso_tiempo_chart",
        {
            "type": "pie",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "innerRadius": 0,
            "baseColor": "#196DAA",
            "hoverAlpha": 0.21,
            "labelColorField": "#0000FF",
            "labelTickColor": "#0000FF",
            "titleField": "country",
            "valueField": "litres",
            "color": "#0000FF",
            "fontFamily": "roboto",
            "autoMargins": false,
            "theme": "default",
            "allLabels": [],
            "balloon": {
                "borderAlpha": 0,
                "color": "#0000FF"
            },
            "titles": [],
            "dataProvider": [
                {
                    "country": "Empresas",
                    "litres": "300"
                },
                {
                    "country": "Oportunidades",
                    "litres": 131.1
                },
                {
                    "country": "Gestión comercial",
                    "litres": 115.8
                },
                {
                    "country": "Gestión interna ESSI",
                    "litres": 109.9
                }
            ]
        }
    );

});
<?php

foreach ($usuarios as $usuario) {
  $mas_alto[0]=0; $mas_alto[1]=0; $mas_alto[2]=0;
  $mas_alto1[0]=''; $mas_alto1[1]=''; $mas_alto1[2]='';
  if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
    foreach($tipo_actividades as $t_a){
      if(isset($valoresU[$usuario->id][$t_a->id])){
        if($mas_alto[0]<$valoresU[$usuario->id][$t_a->id]){
            $mas_alto[2]=$mas_alto[1];
            $mas_alto1[2]=$mas_alto1[1];
            $mas_alto[1]=$mas_alto[0];
            $mas_alto1[1]=$mas_alto1[0];
          $mas_alto[0]=$valoresU[$usuario->id][$t_a->id];
          $mas_alto1[0]=$idU[$usuario->id][$t_a->id];

        }else if($mas_alto[1]<$valoresU[$usuario->id][$t_a->id]){
            $mas_alto[2]=$mas_alto[1];
            $mas_alto1[2]=$mas_alto1[1];
          $mas_alto[1]=$valoresU[$usuario->id][$t_a->id];
          $mas_alto1[1]=$idU[$usuario->id][$t_a->id];
        }else if($mas_alto[2]<$valoresU[$usuario->id][$t_a->id]){
          $mas_alto[2]=$valoresU[$usuario->id][$t_a->id];
          $mas_alto1[2]=$idU[$usuario->id][$t_a->id];
        }
      }

    } ?>
$('#{{$mas_alto1[0]}}').css('background-color','#62dcf3');
$('#{{$mas_alto1[1]}}').css('background-color','#62dcf3');
$('#{{$mas_alto1[2]}}').css('background-color','#62dcf3');
<?php
  }
}
?>

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});


$('body').on('click','.tabs',function(){
  $('.tabs').removeClass('active');
  $('.tabs a').removeClass('active');
  id=$(this).attr('id');
  $('#'+id).addClass('active');
  $('#'+id+' a').addClass('active');
})


 $("body").on("click",".negocios_cerrados",function(e){
  $("#negocios_cerrados").modal();

 })

 $("body").on("click",".negocios_cerrados2",function(e){
  $("#negocios_cerrados2").modal();
  $('.dona').html("");
  setTimeout(function(){
    Morris.Donut({
          element: 'dona-3',
          data: [
              <?php foreach($usuario1 as $usr){ ?>
                {label: "<?php echo $usr[0]; ?>", value: <?php echo (int)$usr[1]; ?>},
              <?php } ?>
            ]
        });
      Morris.Donut({
          element: 'dona-4',
          data: [
            {label: "Alta", value: <?php echo (int)$donas[1]['Alta']; ?>},
            {label: "Media", value: <?php echo (int)$donas[1]['Media']; ?>},
            {label: "Baja", value: <?php echo (int)$donas[1]['Baja']; ?>},
          ]
        });
      Morris.Donut({
          element: 'dona-5',
          data: [

            {label: "Corto Plazo", value: <?php echo (int)$donas[0]['corto']; ?>},
            {label: "Mediano Plazo", value: <?php echo (int)$donas[0]['mediano']; ?>},
            {label: "Largo Plazo", value: <?php echo (int)$donas[0]['largo']; ?>},
          ]
        });
    },2000);
})

<?php
/*Crear variables para guardarlas en localstorage parte 8*/
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "plazo";
  $localstorage[$contador_ls]['valor_campo'] = 'corto';
  $contador_ls++;
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "tiempo";
  $localstorage[$contador_ls]['valor_campo'] = $metas1->corto_dias;
  $contador_ls++;
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "cantidad";
  $localstorage[$contador_ls]['valor_campo'] = $donas[0]['corto'];
  $contador_ls++;

  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "plazo";
  $localstorage[$contador_ls]['valor_campo'] = 'mediano';
  $contador_ls++;
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "tiempo";
  $localstorage[$contador_ls]['valor_campo'] = $metas1->medio_dias;
  $contador_ls++;
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "cantidad";
  $localstorage[$contador_ls]['valor_campo'] = $donas[0]['mediano'];
  $contador_ls++;

  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "plazo";
  $localstorage[$contador_ls]['valor_campo'] = 'largo';
  $contador_ls++;
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "tiempo";
  $localstorage[$contador_ls]['valor_campo'] = $metas1->largo_dias;
  $contador_ls++;
  $localstorage[$contador_ls]['tipo_informacion'] = "Meta de identificación de oportunidades";
  $localstorage[$contador_ls]['campo'] = "cantidad";
  $localstorage[$contador_ls]['valor_campo'] = $donas[0]['largo'];
  $contador_ls++;
/*Crear variables para guardarlas en localstorage parte 8*/
?>

 $("body").on("click",".oportunidad_perdida",function(e){
  $("#oportunidad_perdida").modal();

 })

 $("body").on("click",".oportunidad_identificada",function(e){
  $("#oportunidad_identificada").modal();
})

$("body").on("click",".cumplimiento_presupuestal",function(e){
  $("#cumplimiento_presupuestal").modal();
})
 $("body").on("click",".empresas_clientes",function(e){
  $("#empresas_clientes").modal();
  $("#dona-2").empty();
    setTimeout(function(){
      Morris.Donut({
          element: 'dona-2',
          data: [
          <?php foreach($usuarios as $usuario){
                  if(($usuario->cargo)=='Ejecutivo Comercial') {
                $nombres=explode(" ",$usuario->nombres);
                $empre_ttald=0;
                 foreach($empresas as $empresa){
                  if($empresa->responsable==$usuario->id){
                      $empre_ttald++;
                  }
                }
              ?>
            {label: "% <?php echo $nombres[0] ?> ", value: <?php echo round(($empre_ttald*100)/count($empresas)); ?>},
            <?php } } ?>
          ]
        });
    },2000);
 })

$("body").on("click",".ciclo_venta",function(e){
  $("#ciclo_venta").modal();
})

cont_ventas=0;
$("body").on("click",".VENTAS_ABRIR",function(e){
  $("#VENTAS").modal();
  if(cont_ventas==0){

    $('#graf1').html(`<iframe src="{{url('/')}}/graficasventas" style="width: 100%; height: 250%; border: 0;"></iframe>`);
    $('#graf2').html(`<iframe src="{{url('/')}}/graficasventas1" style="width: 100%; height: 250%; border: 0;"></iframe>`);
    $('#graf3').html(`<iframe src="{{url('/')}}/graficasventas2" style="width: 100%; height: 250%; border: 0;"></iframe>`);
    cont_ventas++;
  }
});
cont_proyectadas=0;
$("body").on("click",".PROYECTADAS_ABRIR",function(e){
  $("#PROYECTADAS").modal();

  if(cont_proyectadas==0){
    $('#graf4').html(`<iframe src="{{url('/')}}/graficasproyectadas" style="width: 100%; height: 250%; border: 0;"></iframe>`);
    $('#graf5').html(`<iframe src="{{url('/')}}/graficasproyectadas1" style="width: 100%; height: 250%; border: 0;"></iframe>`);
    $('#graf6').html(`<iframe src="{{url('/')}}/graficasproyectadas2" style="width: 100%; height: 250%; border: 0;"></iframe>`);
    cont_proyectadas++;
  }
})

$("body").on("click",".gasto_ventas",function(e){
  $("#gasto_ventas").modal();
})

$("body").on("click",".uso_tiempo",function(e){
  $("#uso_tiempo").modal();
});
contadd=0;
$("body").on("click",".cumplimiento_llenado",function(e){
  if(contadd==0){
    $('#detallado-llenado-bitacora').html('<iframe src="{{url("/")}}/llenado_bitacora" style="width: 100%; height: 420px; border: 0;"></iframe>');
    $('#detallado-cumplimiento-semanal').html('<iframe src="{{url("/")}}/llenado_semanal" style="width: 100%; height: 420px; border: 0;"></iframe>');
    $('#detallado-presupuesto-mensual').html('<iframe src="{{url("/")}}/llenado_presupuesto" style="width: 100%; height: 420px; border: 0;"></iframe>');
    contadd++;
  }

  $("#cumplimiento_llenado").modal();

})


$("body").on("click",".actividades_pendientes",function(e){
      $("#actividades_pendientes").modal();
      $("#radar2-pendiente").empty();
      $("#radar-pendiente").empty();
      $("#linea-pendiente").empty();
      setTimeout(function(){
            var chart = AmCharts.makeChart("linea-pendiente", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "horizontalGap": 10,
                    "maxColumns": 1,
                    "position": "right",
                    "useGraphSettings": true,
                    "markerSize": 10
                },
                "dataProvider": [
                <?php
                    foreach($usuarios as $usuario){
                        if(($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Partner Solution'){
                            $nombres=explode(' ', ($usuario->name)); ?>
                              {
                                  "year": "{{$nombres[0]}}",
                                  "europe": {{$gestion1[$usuario->id]['pendientes_mes']}},
                                  "namerica": {{$gestion1[$usuario->id]['pendientes_semana']}}
                              },
                <?php } } ?>],
                "valueAxes": [{
                    "stackType": "regular",
                    "axisAlpha": 0.3,
                    "gridAlpha": 0
                }],
                "graphs": [{
                    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                    "fillAlphas": 0.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Mes",
                    "type": "column",
                    "color": "#000000",
                    "valueField": "europe"
                },
                           {
                    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                    "fillAlphas": 0.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Semana",
                    "type": "column",
                    "color": "#000000",
                    "valueField": "namerica"
                }],
                "categoryField": "year",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "position": "left"
                },
                "export": {
                    "enabled": false
                }
            });
            var chart = AmCharts.makeChart( "radar-pendiente", {
                "type": "radar",
                "theme": "light",
                "dataProvider": [
                  <?php foreach ($usuarios as $usuario) {
                  if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
                    $nombres=explode(' ', $usuario->name);?>
                    {
                        "country": "{{ $nombres[0] }}",
                        "litres": {{$gestion_usuario[$usuario->id]['hechas_tiempo']}}
                      },
            <?php } } ?>
              ],
                "valueAxes": [{
                "axisTitleOffset": 20,
                "minimum": 0,
                "axisAlpha": 0.15
              }],
                "startDuration": 2,
                "graphs": [ {
                    "balloonText": "[[value]] %",
                    "bullet": "round",
                    "lineThickness": 2,
                    "valueField": "litres"
                }],
                "categoryField": "country",
                "export": {
                    "enabled": false
                }
            });
            var chart = AmCharts.makeChart( "radar2-pendiente", {
                "type": "radar",
                "theme": "light",
                "dataProvider": [
                    <?php foreach ($usuarios as $usuario) {
                    if(($usuario->cargo=='Ejecutivo Comercial' || $usuario->cargo=='Gerente Comercial' || $usuario->cargo=='Partner Solution') && $usuario->tipo_user!='otro'){
                        $nombres=explode(' ', $usuario->name); ?>
                        {
                          "direction": "{{$nombres[0]}}",
                          "value": {{$gestion1[$usuario->id]['aplazadas_mes']}}
                        },
                <?php } } ?>
                ],
                "valueAxes": [ {
                    "gridType": "circles",
                    "minimum": 0,
                    "autoGridCount": false,
                    "axisAlpha": 0.2,
                    "fillAlpha": 0.05,
                    "fillColor": "#FFFFFF",
                    "gridAlpha": 0.08,
                    "guides": [ {
                        "angle": 225,
                        "fillAlpha": 0.3,
                        "fillColor": "#fff",
                        "tickLength": 0,
                        "toAngle": 315,
                        "toValue": 14,
                        "value": 0,
                        "lineAlpha": 0,

                    },
                                {
                      "angle": 45,
                      "fillAlpha": 0.3,
                      "fillColor": "#fff",
                      "tickLength": 0,
                      "toAngle": 135,
                      "toValue": 14,
                      "value": 0,
                      "lineAlpha": 0,
                } ],
                    "position": "left"
                } ],
                "startDuration": 1,
                "graphs": [ {
                    "balloonText": "[[category]]: [[value]] m/s",
                    "bullet": "round",
                    "fillAlphas": 0.3,
                    "valueField": "value"
                } ],
                "categoryField": "direction",
                "export": {
                    "enabled": false
                }
            });
          },2000);
})

$("body").on("click",".cono",function(e){
    setTimeout(abrir_cono, 1300);
});

abrir_cono = function(){
    $("#cono").modal();
    <?php
    foreach($metas as $meta){
      foreach ($participaciones as $participacion) {
        # code...
        $corto_alta=round(str_replace(".", ",", str_replace(",", "",$meta->corto_valor))*($participacion->alta/100));
        $corto_media=round(str_replace(".", ",", str_replace(",", "",$meta->corto_valor))*($participacion->media/100));
        $corto_baja=round(str_replace(".", ",", str_replace(",", "",$meta->corto_valor))*($participacion->baja/100));


        $medio_alta=round(str_replace(".", ",", str_replace(",", "",$meta->medio_valor))*($participacion->alta/100));
        $medio_media=round(str_replace(".", ",", str_replace(",", "",$meta->medio_valor))*($participacion->media/100));
        $medio_baja=round(str_replace(".", ",", str_replace(",", "",$meta->medio_valor))*($participacion->baja/100));


        $largo_alta=round(str_replace(".", ",", str_replace(",", "",$meta->largo_valor))*($participacion->alta/100));
        $largo_media=round(str_replace(".", ",", str_replace(",", "",$meta->largo_valor))*($participacion->media/100));
        $largo_baja=round(str_replace(".", ",", str_replace(",", "",$meta->largo_valor))*($participacion->baja/100));

       }
    }

     $porcentaje_corto_alta=round(($bajaalta*100)/$corto_alta);
     $porcentaje_corto_media=round(($bajamedia*100)/$corto_media);
     $porcentaje_corto_baja=round(($bajacorta*100)/$corto_baja);
     $total_corto=$bajaalta+$bajamedia+$bajacorta;

     $porcentaje_medio_alta=round(($mediaalta*100)/$medio_alta);
     $porcentaje_medio_media=round(($mediamedia*100)/$medio_media);
     $porcentaje_medio_baja=round(($mediacorta*100)/$medio_baja);
     $total_mediano=$mediaalta+$mediamedia+$mediacorta;

     $porcentaje_largo_alta=round(($altaalta*100)/$largo_alta);
     $porcentaje_largo_media=round(($altamedia*100)/$largo_media);
     $porcentaje_largo_baja=round(($altacorta*100)/$largo_baja);
     $total_largo=$altaalta+$altamedia+$altacorta;
     if($porcentaje_corto_alta>100){
      $porcentaje_corto_alta=100;
     }
     if($porcentaje_corto_media>100){
      $porcentaje_corto_media=100;
     }
     if($porcentaje_corto_baja>100){
      $porcentaje_corto_baja=100;
     }

     if($porcentaje_medio_alta>100){
      $porcentaje_medio_alta=100;
     }
     if($porcentaje_medio_media>100){
      $porcentaje_medio_media=100;
     }
     if($porcentaje_medio_baja>100){
      $porcentaje_medio_baja=100;
     }

     if($porcentaje_largo_alta>100){
      $porcentaje_largo_alta=100;
     }
     if($porcentaje_largo_media>100){
      $porcentaje_largo_media=100;
     }
     if($porcentaje_largo_baja>100){
      $porcentaje_largo_baja=100;
     }

     $listadogauge[0]["Porcentaje"]=$porcentaje_corto_alta;
     $listadogauge[0]["Valor"]=number_format(($bajaalta/1000000),"0",",",".")." Millones";
     $listadogauge[1]["Porcentaje"]=$porcentaje_corto_media;
     $listadogauge[1]["Valor"]=number_format(($bajamedia/1000000),"0",",",".")." Millones";
     $listadogauge[2]["Porcentaje"]=$porcentaje_corto_baja;
     $listadogauge[2]["Valor"]=number_format(($bajacorta/1000000),"0",",",".")." Millones";
     $listadogauge[3]["Porcentaje"]=$porcentaje_medio_alta;
     $listadogauge[3]["Valor"]=number_format(($mediaalta/1000000),"0",",",".")." Millones";
     $listadogauge[4]["Porcentaje"]=$porcentaje_medio_media;
     $listadogauge[4]["Valor"]=number_format(($mediamedia/1000000),"0",",",".")." Millones";
     $listadogauge[5]["Porcentaje"]=$porcentaje_medio_baja;
     $listadogauge[5]["Valor"]=number_format(($mediacorta/1000000),"0",",",".")." Millones";
     $listadogauge[6]["Porcentaje"]=$porcentaje_largo_alta;
     $listadogauge[6]["Valor"]=number_format(($altaalta/1000000),"0",",",".")." Millones";
     $listadogauge[7]["Porcentaje"]=$porcentaje_largo_media;
     $listadogauge[7]["Valor"]=number_format(($altamedia/1000000),"0",",",".")." Millones";
     $listadogauge[8]["Porcentaje"]=$porcentaje_largo_baja;
     $listadogauge[8]["Valor"]=number_format(($altacorta/1000000),"0",",",".")." Millones";
    ?>
    var colors=["#268c1f","#7db85e","#60df12","#ffbb00","#ffd640","#ffe27e","#f52b2b","#ff6155","#fb928c"];
    setTimeout(function(){
        <?php for($i=1; $i<=count($listadogauge);$i++){ ?>
        $("#meter-"+<?php echo $i ?>).empty();
        var chart = AmCharts.makeChart("meter-"+<?php echo $i ?>, {
            "theme": "light",
            "type": "gauge",
            "axes": [{
              "topTextFontSize": 15,
              "topTextYOffset": 70,
              "axisColor": "#31d6ea",
              "axisThickness": 1,
              "endValue": 100,
              "gridInside": true,
              "inside": true,
              "radius": "50%",
              "valueInterval": 25,
              "tickColor": "#67b7dc",
              "startAngle": -90,
              "endAngle": 90,
              "unit": "%",
              "bandOutlineAlpha": 0,
              "bands": [{
                "color": "#ccc",
                "endValue": 100,
                "innerRadius": "105%",
                "radius": "170%",
                "gradientRatio": [0.5, 0, -0.5],
                "startValue": 0
              }, {
                "color": colors[<?php echo $i ?>-1],
                "endValue": 0,
                "innerRadius": "105%",
                "radius": "170%",
                "gradientRatio": [0.5, 0, -0.5],
                "startValue": 0,
              }]
            }],
            "arrows": [{
              "alpha": 1,
              "innerRadius": "35%",
              "nailRadius": 0,
              "radius": "170%",
            }]
        });
        setInterval(randomValue(chart,<?php echo $listadogauge[$i-1]["Porcentaje"]; ?>, "<?php echo $listadogauge[$i-1]["Valor"]; ?>"), 500);
        <?php } ?>
    },2000);

    function randomValue(chart,porcentaje,valor) {
      chart.arrows[0].setValue(porcentaje);
      chart.axes[0].setTopText("$"+valor+" ("+porcentaje+"%)");
      chart.axes[0].bands[1].setEndValue(porcentaje);
}
    $('#total_corto').html('$ <?php echo number_format(($total_corto/1000000),"0",",",".")." Millones" ?>');
    $('#total_mediano').html('$ <?php echo number_format(($total_mediano/1000000),"0",",",".")." Millones" ?>');
    $('#total_largo').html('$ <?php echo number_format(($total_largo/1000000),"0",",",".")." Millones" ?>');
}

Morris.Bar({
  element: 'morris-bar',
  data: [
    <?php

    $corto[0]=0;$corto[1]=0;$corto[2]=0;
    $medio[0]=0;$medio[1]=0;$medio[2]=0;
    $largo[0]=0;$largo[1]=0;$largo[2]=0;
    $cortoprecio[0]=0;$cortoprecio[1]=0;$cortoprecio[2]=0;
    $medioprecio[0]=0;$medioprecio[1]=0;$medioprecio[2]=0;
    $largoprecio[0]=0;$largoprecio[1]=0;$largoprecio[2]=0;
    foreach ($oportunidades as $key) {

    $fecha="";
    $valor=0;
    $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');
    $contador=count($historial);
    if($contador>0){
      if($historial[$contador-1]->value>0&&$historial[$contador-1]->value<90){
        $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "fecha_oc" order by id DESC');
        if(count($plazo)>0){
          $dias=(strtotime($plazo[0]->value))-(strtotime(date('Y-m-d')));

          $dias=(int)$dias;
          $dias=$dias/86400;
          $dias=(int)$dias;

          $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "probabilidad" order by id DESC');

          $valor_1=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "val_pesos" order by id DESC');

          if(isset($valor_1[0]->value)){
              $valor_total=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor_1[0]->value)))));
          }
          /*$valor_1=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "valor_oportunidad" order by id DESC');
          $moneda=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "moneda" order by id DESC');

          if(($moneda[0]->value)!='COP'){
              $cantidad=str_replace(",", "", $valor_1[0]->value);
              $valor_total=moneda($cantidad,$moneda[0]->value,$dolar,$euro);
              $valor_total=(int)$valor_total;
            }else{
              $valor_total=$valor_1[0]->value;
              $valor_total=str_replace(",", "", $valor_total);

              $valor_total=(int)$valor_total;
            }

            $valor_total=round($valor_total/1000000);
            $valor_total=(int)$valor_total;*/

          if($dias<=($metas1->corto_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $corto[0]++;
                $cortoprecio[0]+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $corto[1]++;
                $cortoprecio[1]+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $corto[2]++;
                $cortoprecio[2]+=$valor_total;
              }
            }
          }elseif($dias<=($metas1->medio_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $medio[0]++;
                $medioprecio[0]+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $medio[1]++;
                $medioprecio[1]+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $medio[2]++;
                $medioprecio[2]+=$valor_total;
              }
            }
          }elseif($dias>($metas1->medio_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $largo[0]++;
                $largoprecio[0]+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $largo[1]++;
                $largoprecio[1]+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $largo[2]++;
                $largoprecio[2]+=$valor_total;
              }
            }
          }
        }
      }
    }
  }

    ?>
    { y: 'Corto Plazo', a: {{((int)(round($cortoprecio[0]/1000000)))}},  b: {{((int)(round($cortoprecio[1]/1000000)))}}, c: {{((int)(round($cortoprecio[2]/1000000)))}} },
    { y: 'Mediano Plazo', a: {{((int)(round($medioprecio[0]/1000000)))}},  b: {{((int)(round($medioprecio[1]/1000000)))}}, c: {{((int)(round($medioprecio[2]/1000000)))}} },
    { y: 'Largo Plazo', a: {{((int)(round($largoprecio[0]/1000000)))}}, b: {{((int)(round($largoprecio[1]/1000000)))}}, c: {{((int)(round($largoprecio[2]/1000000)))}} },

  ],
  xkey: 'y',
  ykeys: ['a', 'b', 'c'],
  labels: ['Alta Probabilidad Millones', 'Mediana Probabilidad Millones', 'Baja Probabilidad Millones'],
  barColors: ['#7df6ff','#00c7dc','#016064']
});

Morris.Donut({
  element: 'donut-example',
  data: [
  <?php foreach($usuario1 as $usr){ ?>
    {label: "<?php echo $usr[0]; ?>", value: <?php echo (int)$usr[1]; ?>},
  <?php } ?>
  ]
});

var chart = AmCharts.makeChart( "chartdiv", {
  "type": "funnel",
  "theme": "light",
  "dataProvider": [
  <?php
    $altaalta=0;
    $altamedia=0;
    $altacorta=0;
    $mediaalta=0;
    $mediamedia=0;
    $mediacorta=0;
    $bajaalta=0;
    $bajamedia=0;
    $bajacorta=0;
    $cortoprecio[0]=0;$cortoprecio[1]=0;$cortoprecio[2]=0;
    $medioprecio[0]=0;$medioprecio[1]=0;$medioprecio[2]=0;
    $largoprecio[0]=0;$largoprecio[1]=0;$largoprecio[2]=0;
  foreach ($oportunidades as $key) {
    $fecha="";
    $valor=0;
    $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$key->id.' AND `key` = "ciclo_venta" order by id');

    $contador=count($historial);
    if($contador>0){
      if($historial[$contador-1]->value>0&&$historial[$contador-1]->value<90){

        $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "fecha_oc" order by id DESC');
        if(count($plazo)>0){
          $dias=(strtotime($plazo[0]->value))-(strtotime(date('Y-m-d')));

          $dias=(int)$dias;
          $dias=$dias/86400;
          $dias=(int)$dias;

          $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "probabilidad" order by id DESC');
          $valor_1=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$key->id.' AND `key` = "val_pesos" order by id DESC');
		  if(count($valor_1)>0){
			$valor_total=str_replace(",", "", str_replace('.', '', str_replace('$ ', '', $valor_1[0]->value)));
		  }else{
			$valor_total=0;
		  }

            $valor_total=(int)$valor_total;

          if($dias<=($metas1->corto_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $corto[0]++;
                $cortoprecio[0]+=$valor_total;
                $bajaalta+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $corto[1]++;
                $cortoprecio[1]+=$valor_total;
                $bajamedia+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $corto[2]++;
                $cortoprecio[2]+=$valor_total;
                $bajacorta+=$valor_total;
              }
            }
          }elseif($dias<=($metas1->medio_dias)){

            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $medio[0]++;
                $medioprecio[0]+=$valor_total;
                $mediaalta+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $medio[1]++;
                $medioprecio[1]+=$valor_total;
                $mediamedia+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                $medio[2]++;
                $medioprecio[2]+=$valor_total;
                $mediacorta+=$valor_total;
              }
            }
          }elseif($dias>($metas1->medio_dias)){
            if(count($probabilidad)>0){
              if(($probabilidad[0]->value)=='Alta'){
                $largo[0]++;
                $largoprecio[0]+=$valor_total;
                $altaalta+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Media'){
                $largo[1]++;
                $largoprecio[1]+=$valor_total;
                $altamedia+=$valor_total;
              }elseif(($probabilidad[0]->value)=='Baja'){
                //var_dump($key->id.' $'.number_format($valor_total));
                $largo[2]++;
                $largoprecio[2]+=$valor_total;
                $altacorta+=$valor_total;
              }
            }
          }
        }
      }
    }
  }


   $diferenciabaja=round((str_replace(".", ",", str_replace(",", "",$metas[0]->corto_valor))-$bajaalta-$bajamedia-$bajacorta)/1000000);
   $diferenciamedia=round((str_replace(".", ",", str_replace(",", "",$metas[0]->medio_valor))-$mediaalta-$mediamedia-$mediacorta)/1000000);
   $diferenciaalta=round((str_replace(".", ",", str_replace(",", "",$metas[0]->largo_valor))-$altaalta-$altamedia-$altacorta)/1000000);
   if($diferenciabaja<0){
    $diferenciabaja=0;
   }
   if($diferenciamedia<0){
    $diferenciamedia=0;
   }
   if($diferenciaalta<0){
    $diferenciaalta=0;
   }
   $altaalta=round($altaalta/1000000);
   $altamedia=round($altamedia/1000000);
   $altacorta=round($altacorta/1000000);
   $mediaalta=round($mediaalta/1000000);
   $mediamedia=round($mediamedia/1000000);
   $mediacorta=round($mediacorta/1000000);
   $bajaalta=round($bajaalta/1000000);
   $bajamedia=round($bajamedia/1000000);
   $bajacorta=round($bajacorta/1000000);
?>
  {
    "title": "LP Diferencia meta",
    "value": <?php echo $diferenciaalta ?>
  }, {
    "title": "LP Baja",
    "value": <?php echo $altacorta; ?>
  }, {
    "title": "LP Media",
    "value": <?php echo $altamedia; ?>
  }, {
    "title": "LP Alta",
    "value": <?php echo $altaalta; ?>
  }, {
    "title": "MP Diferencia meta",
    "value": <?php echo $diferenciamedia ?>
  }, {
    "title": "MP Baja",
    "value": <?php echo $mediacorta; ?>
  }, {
    "title": "MP Media",
    "value": <?php echo $mediamedia; ?>
  }, {
    "title": "MP Alta",
    "value": <?php echo $mediaalta; ?>
  }, {
    "title": "CP Diferencia meta",
    "value": <?php echo $diferenciabaja ?>
  }, {
    "title": "CP Baja",
    "value": <?php echo $bajacorta; ?>
  } , {
    "title": "CP Media",
    "value": <?php echo $bajamedia; ?>
  } , {
    "title": "CP Alta",
    "value": <?php echo $bajaalta; ?>
  }  ],
  "balloon": {
    "fixedPosition": true,
  },
  "valueField": "value",
  "titleField": "title",
  "marginRight": 240,
  "marginLeft": 50,
  "startX": -500,
  "depth3D": 100,
  "angle": 40,
  "outlineAlpha": 1,
  "outlineColor": "#FFFFFF",
  "outlineThickness": 2,
  "colors":["#be0203","#f52b2b","#ff6155","#f9928b","#ffa901","#ffbb00","#ffd640","#ffe27e","#00690c","#268c1f","#7db85e","#5fe014"],
  "labelPosition": "right",
  "labelText":"[[title]]: [[value]] millones",
  "balloonText": "[[title]]: [[value]] Millones",
  "export": {
    "enabled": false
  }
} );

var chart = AmCharts.makeChart("lista-comercial",{
    "type": "serial",
    "theme": "light",
    "dataProvider": [
    <?php
    $masalto=0;
    foreach($usuarios as $usuario){
      if( ($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution' || ($usuario->cargo)=='Director General' ){
        $nombres=explode(" ",$usuario->nombres);
        $valor_usuario=0; $h=0;
        $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

        foreach ($mis_oportunidades as $m_o) {
          $fecha_cierre_m_o=DB::select('SELECT * FROM historial_oportunidades WHERE `key`="fecha_oc" AND `oportunidad_id`="'.$m_o->oportunidad_id.'" ORDER BY `id` DESC');

          if((date("Y", strtotime($fecha_cierre_m_o[0]->value)))==(date('Y'))){
            $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');

            if(($oportunidad_buscar[0]->value)==$usuario->id){

              $valoresoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');
              $valor_usuario+=(int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $valoresoportunidades[0]->value))));


              /*$valoresoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "valor_oportunidad" order by id');

              $convalores=count($valoresoportunidades);
              if($convalores>0){
                $cantidad=str_replace(",", "",($valoresoportunidades[$convalores-1]->value));
                $cantidad=(int)$cantidad;

              }
              $monedaoportunidades=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "moneda" order by id');
              $conmoneda=count($monedaoportunidades);
              if($conmoneda>0){
                $moneda_origen=$monedaoportunidades[$conmoneda-1]->value;
              }
              if($moneda_origen!='COP'){
                  $valor=moneda($cantidad,$moneda_origen,$dolar,$euro);
                  $valor_usuario+=((int)$valor);
              }else{
                  $valor_usuario+=$cantidad;
              }*/
            }
          }
        }
        /*Crear variables para guardarlas en localstorage parte 1*/
          $localstorage[$contador_ls]['tipo_informacion'] = "Negocios cerrados";
          $localstorage[$contador_ls]['campo'] = "funcionario";
          $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Negocios cerrados";
          $localstorage[$contador_ls]['campo'] = "monto";
          $localstorage[$contador_ls]['valor_campo'] = $valor_usuario;
          $contador_ls++;
        /*Crear variables para guardarlas en localstorage parte 1*/
        $valor_usuario=round($valor_usuario/1000000);
        if($masalto<$valor_usuario){
          $masalto=$valor_usuario;
        }
        if($usuario->foto==""){
          $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
        }else{
          $foto=url('/')."/images/file/clientes/".$usuario->foto;
        }

       ?>
          {
              "name": "<?php echo $nombres[0]; ?>",
              "points": <?php echo $valor_usuario; ?>,
              "color": "#009cff",
              "bullet": "<?php echo $foto; ?>"
          },
        <?php } } ?>
    ],
    "valueAxes": [{
        "maximum": {{ $masalto+($masalto*0.1) }},
        "minimum": 0,
        "axisAlpha": 0,
        "dashLength": 4,
        "position": "left"
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]] Millones</b></span>",
        "bulletOffset": 10,
        "bulletSize": 52,
        "colorField": "color",
        "cornerRadiusTop": 8,
        "customBulletField": "bullet",
        "fillAlphas": 0.8,
        "lineAlpha": 0,
        "type": "column",
        "valueField": "points"
    }],
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "categoryField": "name",
    "categoryAxis": {
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "export": {
      "enabled": false
     }
});

 var chart = AmCharts.makeChart("barra-horizontal", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [
        <?php
            foreach($usuarios as $usuario){
                if($usuario->cargo=='Ejecutivo Comercial' || $usuario->cargo=='Gerente Comercial' || $usuario->cargo=='Partner Solution' || $usuario->cargo=='Director General' ){
                    $nombres=explode(" ",$usuario->nombres);
                    $valor_usuario=0; $h=0;
                    $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `key` = "responsable" AND `value` = "'.$usuario->id.'" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90") ORDER BY `historial_oportunidades`.`oportunidad_id` ASC');
                    foreach ($mis_oportunidades as $m_o) {
                        $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                        if($oportunidad_buscar[0]->value==$usuario->id){
                            $fecha_cierre=DB::select('SELECT value FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC LIMIT 0,1');
                            if(date('Y',strtotime($fecha_cierre[0]->value)) == date('Y')){
                                $h++;
                            }
                        }
                    }
                    $valor_usuario=round($valor_usuario/1000000);
                    if($usuario->foto==""){
                      $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
                    }else{
                      $foto=url('/')."/images/file/clientes/".$usuario->foto;
                    }
                    /*Crear variables para guardarlas en localstorage parte 2*/
                      $localstorage[$contador_ls]['tipo_informacion'] = "Negocios cerrados";
                      $localstorage[$contador_ls]['campo'] = "cantidad";
                      $localstorage[$contador_ls]['valor_campo'] = $h;
                      $contador_ls++;
                    /*Crear variables para guardarlas en localstorage parte 2*/
                   ?>
                    {
                    "year": "<?php echo $nombres[0]?>",
                    "income": <?php echo $h; ?>,
                    "bullet": "<?php echo $foto; ?>"
                    },
                    <?php
                }
            }
        ?>
    ],
    "graphs": [{
        "balloonText": "Negocio(s) Cerrado(s) [[category]]:[[value]]",
        "bulletOffset": 10,
        "bulletSize": 33,
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "customBulletField": "bullet",
        "title": "Income",
        "type": "column",
        "valueField": "income"
    }],
    "depth3D": 20,
    "angle": 30,
    "rotate": true,
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left"
    },
    "export": {
      "enabled": false
     }
});

var chart = AmCharts.makeChart( "pie", {
  "type": "radar",
  "theme": "light",
  "dataProvider": [
        <?php
           foreach($usuarios as $usuario){
            $tiempo=0;
            $promedioindividual=0;
            $totaloportunidadindividual=0;
            if( ($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution' || ($usuario->cargo)=='Director General' ){
            $nombres=explode(" ",$usuario->nombres);
                  $valor_usuario=0; $h=0;
                  $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');
                  foreach ($mis_oportunidades as $m_o) {
                    $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
                    if(($oportunidad_buscar[0]->value)==$usuario->id){

                      $historial1=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" ORDER BY `id` DESC');
                      $fecha_fin=0;
                      $fecha_inicio=0;
                      $contador=count($historial1);
                      if($contador>0){
                        if($historial1[0]->value>=90){

                          $historial2=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_ff" ORDER BY `id` DESC');
                          if(count($historial2)>0){
                            $fecha_inicio=strtotime($historial2[0]->value);
                          }
                          $historial3=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "fecha_oc" ORDER BY `id` DESC');
                          if(count($historial3)>0){
                            $fecha_fin=strtotime($historial3[0]->value);
                          }
                            if(date('Y',strtotime($historial3[0]->value)) == date('Y')){
                                $totaloportunidadindividual++;
                            }
                        }
                      }
                      if(date('Y',strtotime($historial3[0]->value)) == date('Y')){
                        $tiempo+=$fecha_fin - $fecha_inicio;
                      }
                    }
                  }

            if($totaloportunidadindividual>0){
              $dias=$tiempo / 86400;
              $promedioindividual=($dias/$totaloportunidadindividual)/30;
              $promedioindividual=number_format($promedioindividual,0,".","");
            }else{
              $promedioindividual=0;
            }

            $valor_usuario=round($valor_usuario/1000000);
            //$ttal_prom_cierre=round($suma_meses/$h,1);
            if($usuario->foto==""){
              $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
            }else{
              $foto=url('/')."/images/file/clientes/".$usuario->foto;
            }
            /*Crear variables para guardarlas en localstorage parte 3*/
              $localstorage[$contador_ls]['tipo_informacion'] = "Negocios cerrados";
              $localstorage[$contador_ls]['campo'] = "promedio";
              $localstorage[$contador_ls]['valor_campo'] = $promedioindividual;
              $contador_ls++;
            /*Crear variables para guardarlas en localstorage parte 3*/
           ?>
          {
            "country": "<?php echo $nombres[0]?>",
            "litres": <?php echo $promedioindividual; ?>
          },
        <?php } } ?> ],
  "valueAxes": [ {
    "axisTitleOffset": 20,
    "minimum": 0,
    "axisAlpha": 0.15
  } ],
  "startDuration": 2,
  "graphs": [ {
    "balloonText": "[[value]] Promedio de cierre",
    "bullet": "round",
    "lineThickness": 2,
    "valueField": "litres"
  } ],
  "categoryField": "country",
  "export": {
    "enabled": false
  }
} );


 var chart = AmCharts.makeChart("negocio_perdido", {
    "theme": "light",
    "type": "serial",
    "startDuration": 2,
    "dataProvider": [
    <?php
      $valor=0; $h=0;
      foreach ($usuarios as $usuario) {
        if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
        $nombres=explode(" ",$usuario->nombres);

        $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE user_id = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');

        foreach ($mis_oportunidades as $m_o) {
          $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
          if(($oportunidad_buscar[0]->value)==$usuario->id){
            $fecha_cierre_ind_perdida=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" ORDER BY `id` DESC LIMIT 0,1');
            if((date("Y", strtotime($fecha_cierre_ind_perdida[0]->updated_at)))==(date('Y'))){
              $h++;

              $info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "val_pesos" ORDER BY `id` DESC');
              $valor+=((int)(str_replace(',', '', str_replace('.', '', str_replace('$ ', '', $info[0]->value)))));
              /*$info=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "valor_oportunidad" ORDER BY `id` DESC');
              $con=count($info);
              if($con>0){
                $cantidad=$info[0]->value;
                $cantidad=(int)(str_replace(",", "",$cantidad));
                $info1=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "moneda" ORDER BY `id` DESC');
                $con1=count($info1);
                if($con1>0){
                  $moneda_origen=$info1[0]->value;
                }
                if($moneda_origen=='COP'){
                  $valor+=$cantidad;
                }else{
                  $valor+=moneda($cantidad,$moneda_origen,$dolar,$euro);
                }
              }*/
            }
          }
        }
    /*Crear variables para guardarlas en localstorage parte 4*/
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades perdidas";
          $localstorage[$contador_ls]['campo'] = "funcionario";
          $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades perdidas";
          $localstorage[$contador_ls]['campo'] = "monto";
          $localstorage[$contador_ls]['valor_campo'] = $valor;
          $contador_ls++;
    /*Crear variables para guardarlas en localstorage parte 4*/
      $valor=round($valor/1000000);
       if($usuario->foto==""){
          $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
        }else{
          $foto=url('/')."/images/file/clientes/".$usuario->foto;
        }
     ?>
    {
        "country": "<?php echo $nombres[0];?>",
        "visits": <?php echo $valor; ?>,
        "color": "<?php echo $usuario->color; ?>",
        "bullet": "<?php echo $foto; ?>"
    },
    <?php } } ?>
    ],
    "valueAxes": [{
        "position": "left",

    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b> Millones",
        "bulletSize": 33,
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits",
        "customBulletField": "bullet",
    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 0,
        "fontSize": 0
    },
    "export": {
      "enabled": false
     }

});

 var chart = AmCharts.makeChart("cantidad-perdida", {
    "theme": "light",
    "type": "serial",
    "startDuration": 2,
    "dataProvider": [
      <?php
        $valor=0; $x=0;
        foreach ($usuarios as $usuario) {
          if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
            $h=0;

            $nombres=explode(" ",$usuario->nombres);
            $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE value = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0")');

            foreach ($mis_oportunidades as $m_o) {
              $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');

              if(($oportunidad_buscar[0]->value)==$usuario->id){
                $fecha_perdida=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "ciclo_venta" ORDER BY id DESC LIMIT 0,1');
                if((date("Y", strtotime($fecha_perdida[0]->updated_at)))==(date('Y'))){
                    $historial=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "ciclo_venta" order by id');
                    $contador=count($historial);
                    if($contador>0){
                      if($historial[$contador-1]->value==0){
                          $h++;
                      }
                    }
                }
              }
            }

          if($usuario->foto==""){
            $foto="https://placeholdit.imgix.net/~text?txtsize=45&txt=".$nombres[0]."&w=200&h=200";
          }else{
            $foto=url('/')."/images/file/clientes/".$usuario->foto;
          }
       ?>
    {
        "country": "<?php echo $nombres[0]; ?>",
        "visits": <?php echo $h; ?>,
        "color": "<?php echo $usuario->color; ?>",
        "bullet": "<?php echo $foto; ?>"
    },
    <?php } } ?>],
    "valueAxes": [{
        "position": "left",
        "axisAlpha":0,
        "gridAlpha":0
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "bulletSize": 33,
        "colorField": "color",
        "fillAlphas": 0.85,
        "lineAlpha": 0.1,
        "type": "column",
        "topRadius":1,
        "valueField": "visits",
        "customBulletField": "bullet",
    }],
    "depth3D": 40,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha":0,
        "gridAlpha":0,
        "fontSize": 0

    },
    "export": {
      "enabled": false
     }

}, 0);

<?php  $y=0; foreach($usuarios as $usuario){

  $corto[0]=0;$corto[1]=0;$corto[2]=0;
  $medio[0]=0;$medio[1]=0;$medio[2]=0;
  $largo[0]=0;$largo[1]=0;$largo[2]=0;
  $cortoprecio[0]=0;$cortoprecio[1]=0;$cortoprecio[2]=0;
  $medioprecio[0]=0;$medioprecio[1]=0;$medioprecio[2]=0;
  $largoprecio[0]=0;$largoprecio[1]=0;$largoprecio[2]=0;

     if((($usuario->cargo)=='Ejecutivo Comercial' || ($usuario->cargo)=='Gerente Comercial' || ($usuario->cargo)=='Partner Solution') && ($usuario->tipo_user)!='otro'){
       $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE value = '.$usuario->id.' AND `key` = "responsable" AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

        foreach ($mis_oportunidades as $m_o) {
          $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
          if(($oportunidad_buscar[0]->value)==$usuario->id){
            $plazo=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "fecha_oc" order by id DESC');
            if(count($plazo)>0){
              $dias=(strtotime($plazo[0]->value))-(strtotime(date('Y-m-d')));
              $dias=$dias/86400;
              $dias=(int)$dias;
              $probabilidad=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "probabilidad" order by id DESC');
              $valor=DB::select('SELECT * FROM historial_oportunidades WHERE `oportunidad_id` = '.$oportunidad_buscar[0]->oportunidad_id.' AND `key` = "val_pesos" order by id DESC');

              if(isset($valor[0]->value)){
                  $valor_total=((int)(str_replace('$ ', '', str_replace('.', '', str_replace(',', '', $valor[0]->value)))));
              }

              if($dias<=($metas1->corto_dias)){
                if(count($probabilidad)>0){
                  if(($probabilidad[0]->value)=='Alta'){
                    $corto[0]++;
                    $cortoprecio[0]+=$valor_total;
                  }elseif(($probabilidad[0]->value)=='Media'){
                    $corto[1]++;
                    $cortoprecio[1]+=$valor_total;
                  }elseif(($probabilidad[0]->value)=='Baja'){
                    $corto[2]++;
                    $cortoprecio[2]+=$valor_total;
                  }
                }
              }elseif($dias<=($metas1->medio_dias)){
                if(count($probabilidad)>0){
                  if(($probabilidad[0]->value)=='Alta'){
                    $medio[0]++;
                    $medioprecio[0]+=$valor_total;
                  }elseif(($probabilidad[0]->value)=='Media'){
                    $medio[1]++;
                    $medioprecio[1]+=$valor_total;
                  }elseif(($probabilidad[0]->value)=='Baja'){
                    $medio[2]++;
                    $medioprecio[2]+=$valor_total;
                  }
                }
              }elseif($dias>($metas1->medio_dias)){
                if(count($probabilidad)>0){
                  if(($probabilidad[0]->value)=='Alta'){
                    $largo[0]++;
                    $largoprecio[0]+=$valor_total;
                  }elseif(($probabilidad[0]->value)=='Media'){
                    $largo[1]++;
                    $largoprecio[1]+=$valor_total;
                  }elseif(($probabilidad[0]->value)=='Baja'){
                    $largo[2]++;
                    $largoprecio[2]+=$valor_total;
                  }
                }
              }
            }
          }
        }
        /*Calcular corto-media-largo de todos los usuarios*/
         $corto[$usuario->id]=$cortoprecio[0] + $cortoprecio[1] + $cortoprecio[2];
         $media[$usuario->id]=$medioprecio[0] + $medioprecio[1] + $medioprecio[2];
         $largo[$usuario->id]=$largoprecio[0] + $largoprecio[1] + $largoprecio[2];
        /*fin Calcular corto-media-largo de todos los usuarios*/

         /*Crear variables para guardarlas en localstorage parte 8*/
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según plazo";
          $localstorage[$contador_ls]['campo'] = "funcionario";
          $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según plazo";
          $localstorage[$contador_ls]['campo'] = "corto";
          $localstorage[$contador_ls]['valor_campo'] = $corto[$usuario->id];
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según plazo";
          $localstorage[$contador_ls]['campo'] = "mediano";
          $localstorage[$contador_ls]['valor_campo'] = $media[$usuario->id];
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según plazo";
          $localstorage[$contador_ls]['campo'] = "largo";
          $localstorage[$contador_ls]['valor_campo'] = $largo[$usuario->id];
          $contador_ls++;
        /*Fin Crear variables para guardarlas en localstorage parte 8*/

        /*Calcular alta-media-baja de todos los usuarios*/
         $alta[$usuario->id]=$cortoprecio[0]+$medioprecio[0]+$largoprecio[0];
         $medio[$usuario->id]=$cortoprecio[1]+$medioprecio[1]+$largoprecio[1];
         $baja[$usuario->id]=$cortoprecio[2]+$medioprecio[2]+$largoprecio[2];
        /*Fin Calcular alta-media-baja de todos los usuarios*/

        /*Crear variables para guardarlas en localstorage parte 9*/
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según probabilidad";
          $localstorage[$contador_ls]['campo'] = "funcionario";
          $localstorage[$contador_ls]['valor_campo'] = $usuario->id;
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según probabilidad";
          $localstorage[$contador_ls]['campo'] = "alta";
          $localstorage[$contador_ls]['valor_campo'] = $alta[$usuario->id];
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según probabilidad";
          $localstorage[$contador_ls]['campo'] = "media";
          $localstorage[$contador_ls]['valor_campo'] = $medio[$usuario->id];
          $contador_ls++;
          $localstorage[$contador_ls]['tipo_informacion'] = "Oportunidades identificadas según probabilidad";
          $localstorage[$contador_ls]['campo'] = "baja";
          $localstorage[$contador_ls]['valor_campo'] = $baja[$usuario->id];
          $contador_ls++;
        /*Fin Crear variables para guardarlas en localstorage parte 9*/

        $cortoprecio[0]=((int)(round($cortoprecio[0]/1000000)));
        $cortoprecio[1]=((int)(round($cortoprecio[1]/1000000)));
        $cortoprecio[2]=((int)(round($cortoprecio[2]/1000000)));
        $medioprecio[0]=((int)(round($medioprecio[0]/1000000)));
        $medioprecio[1]=((int)(round($medioprecio[1]/1000000)));
        $medioprecio[2]=((int)(round($medioprecio[2]/1000000)));
        $largoprecio[0]=((int)(round($largoprecio[0]/1000000)));
        $largoprecio[1]=((int)(round($largoprecio[1]/1000000)));
        $largoprecio[2]=((int)(round($largoprecio[2]/1000000)));

    ?>
var chart = AmCharts.makeChart("graficas-alta{{$y}}", {
  "type": "serial",
  "columnWidth": 0.96,
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": {{$cortoprecio[0]}},
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": {{$cortoprecio[1]}},
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": {{$cortoprecio[2]}},
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-mediano{{$y}}", {
  "type": "serial",
  "columnWidth": 0.96,
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": {{$medioprecio[0]}},
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": {{$medioprecio[1]}},
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": {{$medioprecio[2]}},
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-corto{{$y}}", {
  "type": "serial",
  "columnWidth": 0.96,
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": {{$largoprecio[0]}},
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": {{$largoprecio[1]}},
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": {{$largoprecio[2]}},
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
<?php $y++; } }  ?>

var chart = AmCharts.makeChart( "radar", {
  "type": "radar",
  "theme": "light",
  "fontSize": 10,
  "radius": "35%",
  "dataProvider": [
<?php $i=0; foreach($numero_tipo_actividad as $n_t_a){ if($i==5){ break; }else{ ?>
  {
    "country": "{{$n_t_a->tipo_actividad}}",
    "litres": {{$n_t_a->horas}}
  },
<?php $i++; } } ?>
  ],
  "valueAxes": [ {
    "axisTitleOffset": 20,
    "minimum": 0,
    "axisAlpha": 0.15
  } ],
  "startDuration": 2,
  "graphs": [ {
    "balloonText": "[[value]] horas usadas",
    "bullet": "round",
    "lineThickness": 2,
    "valueField": "litres"
  } ],
  "categoryField": "country",
  "export": {
    "enabled": false
  }
} );

contad=0;
$('body').on('click','#pestana2',function(){
  if(contad==0){
    contad++;
    $.ajax({
      url: "{{url('/')}}/dashboardconsultacumplimiento",
      cache: false,
      contentType: false,
      processData: false,
      type: 'GET',
      success: function(e){
        html=`<div class="tab-titulo-division-3">
                    Cumplimiento Llenado
                  </div>
                  <div class="borde-inferior">
                    <div class="row" style="text-align: left;">
                      <div class="col-md-4">
                            <img src="{{url('/')}}/images/icons/bitacora.png">
                            <p class="collections-title2">Bitácora</p>
                        </div>
                        <div class="col-md-2 padding-top-4">
                            <span class="task-cat2 pink accent-2">`+e.cumplimiento['bitacora']+`%</span>
                        </div>
                        <div class="col-md-6">
                            <div class="progress2">
                                 <div class="determinate" style="width: `+e.cumplimiento['bitacora']+`%"></div>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="borde-inferior">
                    <div class="row" style="text-align: left;">
                      <div class="col-md-4">
                            <img src="{{url('/')}}/images/icons/panel_semanal.png">
                            <p class="collections-title2">Plan Semanal</p>
                        </div>
                        <div class="col-md-2 padding-top-4">
                            <span class="task-cat2 pink accent-2">`+e.porcentajecumplimientosemanal+`%</span>
                        </div>
                        <div class="col-md-6">
                            <div class="progress2">
                                 <div class="determinate" style="width: `+e.porcentajecumplimientosemanal+`%"></div>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="borde-inferior">
                    <div class="row" style="text-align: left;">
                      <div class="col-md-4">
                          <img src="{{url('/')}}/images/icons/presupuesto_mensual.png">
                          <p class="collections-title2">Presupuesto Mensual</p>
                      </div>
                      <div class="col-md-2 padding-top-4">
                          <span class="task-cat2 pink accent-2">`+e.porcentajecumplimientopresupuesto+`%</span>
                      </div>
                      <div class="col-md-6">
                          <div class="progress2">
                               <div class="determinate" style="width: `+e.porcentajecumplimientopresupuesto+`%"></div>
                          </div>
                      </div>
                    </div>
                  </div>`;
        $('#cumplimiento_llenado_contenido').html(html);
      }
    });
  }
});

//************** INICIO DE SCRIPTS SEGUNDO PANEL**********************//
/*Local Storage*/
    var datos_local = {};
    <?php foreach($localstorage as $index=>$value){
            $contenido='';
            foreach($value as $index2=>$value2){
                if($contenido!=''){
                    $contenido.= ', '.$index2.': "'.$value2.'"';
                }else{
                    $contenido.= $index2.': "'.$value2.'"';
                }
            } ?>
            datos_local[<?=$index?>] = {<?=$contenido?>};
    <?php
        } ?>

    $(document).on('click','.btn_save_comparacion',function(){
       swal(
          'Esta seguro?',
          'Desea guardar los valores del dashboard hasta la fecha',
          'question'
        ).then(function () {
           var CSRF_TOKEN = $('input[name=_token]').val();
           $.ajax({
                url: "{{ url('/') }}/dashboard-action",
                cache: false,
                type: 'POST',
                data:{
                    _token : CSRF_TOKEN,
                    datos:datos_local,
                    action:0
                },
                success: function(e){
                  swal(
                      'Guardado!',
                      e.data['msj'],
                      'success'
                    );
                }
            });
       });
    });
/*Fin local Storage*/
</script>

<?php
function moneda($cantidad,$moneda_origen,$dolar,$euro)
{
    if(($moneda_origen )=='EUR'){
      $valor_conversion=$cantidad*$euro;
    }elseif(($moneda_origen)=='USD'){
      $valor_conversion=$cantidad*$dolar;
    }
    return($valor_conversion);
}
?>
@endsection
