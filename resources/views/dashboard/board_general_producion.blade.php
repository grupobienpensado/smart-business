@extends('template.app') @php date_default_timezone_set("America/Bogota");
@endphp @section('title', 'Ajuste')
@section('content')
<link rel="stylesheet" href="/css/dashboard/board_general_production.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<!-- Contenedor general-->
<div class="container">
    {{csrf_field()}}
    <input type="hidden" id="URL" value="{{url('/')}}">
    <div class="row">
        <div class="col-md-4 col-sm-12 bg-white rounded p-0 shadow-2">
            <div class="col-md-12 px-0">
                <h4 class="text-white mt-0 p-3 rounded-top bg-primary-darken">Cono de Oportunidades</h4>
            </div>
            <div id="chart_cono" onclick="abrir_cono()"></div>
            <canvas id="myCanvas"></canvas>
            <div class="col-md-12 px-4 cuadro-cono">
              <div class="cuadro-color-rojo-cono"></div><div class="titulo-color">LP - Largo Plazo</div><br>
              <div class="cuadro-color-amarillo-cono"></div><div class="titulo-color">MP - Mediano Plazo</div><br>
              <div class="cuadro-color-verde-cono"></div><div class="titulo-color">CP - Corto Plazo</div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12" id="Usage">
            <nav class="nav nav-tabs nav-justified bg-primary-darken">
                <a class="nav-item nav-link active text-white" id="pills-oportunidad-tab" data-toggle="pill" href="#pills-oportunidad" role="tab" aria-controls="pills-oportunidad" aria-selected="true">Gestión de oportunidades</a>
                <a class="nav-item nav-link text-white" onclick="cargar_segunda_parte()" id="pills-comercial-tab" data-toggle="pill" href="#pills-comercial" role="tab" aria-controls="pills-comercial" aria-selected="false">Gestión Comercial</a>
                @if($data['permiso_guardar_cierre_ano']=="Si")
                <a class="nav-item nav-link text-white" data-toggle="pill" onclick="save_comparativo()" href="#" role="tab" aria-controls="pills-comercial" aria-selected="false"><i class="fa fa-save"></i> Guardar</a>
                @endif
            </nav>
            <div class="tab-content py-0 mt-3 shadow-2" id="pills-tabContent">
                <!--Start first tab-->
                <div class="tab-pane fade show active" id="pills-oportunidad" role="tabpanel" aria-labelledby="pills-oportunidad-tab">
                    <div class="container bg-white rounded py-4">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="row" id="neg_cerrados" onclick="abrir_negocios_cerrados()">
                                    <div class="col-md-6 col-sm-12 pr-md-2 pr-sm-4">
                                        <div class="card shadow-2  border-radius-top-0">
                                            <div class="card-body">
                                                <h5 class="card-title">Negocios cerrados</h5>
                                                <h2 class="text-info my-2" id="numero_negocios_cerrados">2</h2>
                                                <hr class="m-2">
                                                <h2 class="m-0 text-info">$<span id="valor_negocios_cerrados">2,068</span> M</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 pr-md-0 px-sm-4">
                                        <div class="card shadow-2  border-radius-top-0">
                                            <div class="card-body">
                                                <h5 class="card-title">Promedio de cierre</h5>
                                                <h2 class="text-info" id="promedio_cierre">18.1</h2>
                                                <h5 class="card-title">Meses</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12">
                               <div class="card" onclick="abrir_oportunidades_perdidas()">
                                  <div class="text-white rounded-top card-header bg-info">
                                    <h4 class="m-0">Oportunidades perdidas</h4>
                                  </div>
                                  <div class="container px-0">
                                  <div class="row" id="op_perdidas">
                                    <div class="col-md-4 col-sm-12 col-sm-6 pr-md-0 pr-sm-4">
                                        <div class="card shadow-2  rounded-0">
                                            <div class="card-body pb-2">
                                                <h2 class="mt-md-2 mt-xl-3 opo-per">60%</h2>
                                                <p class="card-text">Respecto al total</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-sm-6 px-md-0 px-sm-4">
                                        <div class="card shadow-2  rounded-0">
                                            <div class="card-body pb-2">
                                                <h2 class="mt-md-2 mt-xl-3 opo-per">3</h2>
                                                <p class="card-text">Oportunidades</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-sm-6 pl-md-0 pl-sm-4">
                                        <div class="card shadow-2  rounded-0">
                                            <div class="card-body pb-2">
                                                <h2 class="mt-md-2 mt-xl-3 opo-per">$<span>4.849</span></h2>
                                                <p class="card-text">Millones</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="op_identificadas">
                            <div class="col-md-5 mt-3 pr-md-0 pr-sm-4">
                                <div class="card" onclick="abrir_oportunidades_identificadas()">
                                    <div class="card-header bg-white border-0">
                                        <h4 class="">Oportunidades Identificadas</h4>
                                        <h6 class="text-muted text-center" id="total_oportunidadesidentificadas">($ <span>102,063 </span>Millones)</h6>
                                    </div>
                                    <div class="card-body p-0">
                                       <div class="chartwrapper">
                                           <div id="donutchart"></div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 mt-3">
                                <div class="card border-radius-top-0  border-top-0" onclick="abrir_oportunidades_identificadas2()">
                                    <div class="text-white rounded-top card-header bg-info border-right-0 border-left-0">
                                        <h4 class="m-0">Oportunidades identificadas</h4>
                                    </div>
                                    <div class="card-body">
                                        <div id="columnchart_material" class="w-100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-3">
                              <div class="card" onclick="abrir_ciclos_venta()">
                                  <div class="card-header text-white rounded-top bg-success border-right-0 border-left-0">
                                        <h4 class="m-0">Ciclos de venta</h4>
                                    </div>
                                  <div class="container py-0">
                                  <div class="row" id="content_ciclos_ventas">
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">3</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">4.849</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">3</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">20</span>%</h5>
                                              <h6 class="card-title">Perdida</h6>
                                              </div>
                                            </div>
                                      </div>
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">48</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">36.723</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">42</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">10</span>%</h5>
                                              <h6 class="card-title">Detección</h6>
                                              </div>
                                            </div>
                                      </div>
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">48</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">36.723</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">42</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">20</span>%</h5>
                                              <h6 class="card-title">Presentación</h6>
                                              </div>
                                            </div>
                                      </div>
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">48</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">36.723</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">42</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">40</span>%</h5>
                                              <h6 class="card-title">Propuesta</h6>
                                              </div>
                                            </div>
                                      </div>
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">48</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">36.723</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">42</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">60</span>%</h5>
                                              <h6 class="card-title">Negociación</h6>
                                              </div>
                                            </div>
                                      </div>
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">48</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">36.723</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">42</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">80</span>%</h5>
                                              <h6 class="card-title">Gestión</h6>
                                              </div>
                                            </div>
                                      </div>
                                      <div class="col px-0">
                                          <div class="card  border-radius-top-0">
                                              <div class="card-body px-0">
                                              <h5 class="card-title"><span class="text-info">48</span> Oportunidades</h5>
                                              <h5 class="card-title"><span class="text-info">36.723</span> Millones</h5>
                                              <h5 class="card-title"><span class="text-info">42</span>%</h5>
                                              <i class="fa fa-area-chart fa-3x"></i>
                                              <h5 class="card-title"><span class="text-success">90</span>%</h5>
                                              <h6 class="card-title">Vendida</h6>
                                              </div>
                                            </div>
                                      </div>
                                  </div>
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6 mt-3 pr-md-2 pr-sm-4">
                               <div class="row" onclick="abrir_ventas()">
                                   <div class="col pr-0">
                                       <div class="card  text-white rounded-top bg-success">
                                        <div class="card-body">
                                            <h4 class="mt-5">Ventas</h4>
                                        </div>
                                    </div>
                                   </div>
                                   <div class="col px-0">
                                       <div class="card  rounded-top">
                                        <div class="card-body">
                                            <h2 class="my-2" id="total_equipos">2</h2>
                                            <h5 class="card-title my-2">Equipos</h5>
                                        </div>
                                    </div>
                                   </div>
                                   <div class="col pl-0">
                                       <div class="card  rounded-top">
                                        <div class="card-body">
                                            <h2 class="my-2" id="total_paises">2</h2>
                                            <h5 class="card-title my-2">Paises</h5>
                                        </div>
                                    </div>
                                   </div>
                               </div>
                            </div>
                            <div class="col-md-6 mt-3 pl-md-2 pl-sm-4">
                                <div class="row" onclick="abrir_proyecciones()">
                                   <div class="col pr-0">
                                       <div class="card  text-white rounded-top bg-warning">
                                        <div class="card-body">
                                            <h4 class="mt-5">Proyecciones</h4>
                                        </div>
                                    </div>
                                   </div>
                                   <div class="col px-0">
                                       <div class="card  rounded-top">
                                        <div class="card-body">
                                            <h2 class="my-2" id="total_equiposproyecciones">2</h2>
                                            <h5 class="card-title my-2">Equipos</h5>
                                        </div>
                                    </div>
                                   </div>
                                   <div class="col pl-0">
                                       <div class="card  rounded-top">
                                        <div class="card-body">
                                            <h2 class="my-2" id="total_paisesproyecciones">2</h2>
                                            <h5 class="card-title my-2">Paises</h5>
                                        </div>
                                    </div>
                                   </div>
                               </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12 mt-3">
                           <div class="card" onclick="abrir_empresas_clientes()">
                              <div class="card-header text-white rounded-top bg-info border-right-0 border-left-0">
                              <h4 class="m-0">Empresas & Clientes</h4>
                              </div>
                              <div class="container">
                              <div class="row">
                                  <div class="col px-0">
                                      <div class="card  border-radius-top-0">
                                      <div class="card-body">
                                      <h2 class="m-2" id="total_empresa_oportunidades">84</h2>
                                      <h6>Empresas con Oportunidades</h6>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col px-0">
                                      <div class="card  border-radius-top-0">
                                      <div class="card-body">
                                      <h2 class="m-2" id="total_empresa_identificadas">312</h2>
                                      <h6>Empresas identificadas</h6>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col px-0">
                                      <div class="card  border-radius-top-0">
                                      <div class="card-body">
                                      <h2 class="m-2" id="total_clientes_registrados">318</h2>
                                      <h6>Clientes registrados</h6>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col px-0">
                                      <div class="card  border-radius-top-0">
                                      <div class="card-body">
                                      <h2 class="m-2" id="total_empresas_nuevas">0</h2>
                                      <h6>Empresas nuevas</h6>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col px-0">
                                      <div class="card  border-radius-top-0">
                                      <div class="card-body">
                                      <h2 class="m-2" id="total_clientes_nuevos">1</h2>
                                      <h6>Cliente nuevo</h6>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!--End first tab-->
                <!--Start second tab-->
                <div class="tab-pane fade" id="pills-comercial" role="tabpanel" aria-labelledby="pills-comercial-tab">
                    <div class="container bg-white rounded py-4">
                        <div class="row">
                            <div class="col-md-4 pr-md-0 pr-sm-4" onclick="abrircostoventa()">
                                <div class="card text-white" style="height:100%;">
                                    <div class="card-header bg-secondary">
                                        Costo de venta
                                        <h2 id="costo_venta">0.26%</h2>
                                    </div>
                                    <div class="card-body bg-dark p-0">
                                        <img src="images/graphic.svg" class="mt-1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row" onclick="abrircumplimientopresupuestal()">
                                    <div class="col-md-12 brown-gradient-parent">
                                        <h4 class="bg-brown-gradient text-white rounded-top card-header mt-0">Cumplimiento Presupuestal</h4>
                                    </div>
                                    <div class="col-md-4 col-sm-12 brown-gradient-parent pr-md-0 pr-sm-4">
                                        <div class="bg-brown-gradient" id="ejecucion_presupuesto_ultimo_mes">
                                            <h2>121%</h2>
                                            <p class="text-white mt-5 font-weight-normal">Cumplimiento Presupuesto Marzo<br>Segun Bitacora</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 brown-gradient-parent px-md-0 pr-sm-4">
                                        <div class="bg-brown-gradient" id="gasto_parcial_mes_actual">
                                            <h2>89%</h2>
                                            <p class="text-white mt-5 font-weight-normal">Gasto Parcial de presupuesto Abril<br>Segun Bitacora</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 brown-gradient-parent pl-md-0 pl-sm-4">
                                        <div class="bg-brown-gradient" id="gasto_parcial_ano_actual">
                                            <h2>89%</h2>
                                            <p class="text-white mt-5 font-weight-normal">Gasto Parcial de presupuesto 2018<br>Incluye datos ajuste</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" onclick="abrirusotiempo()">
                            <div class="col-md-12">
                                <h4 class="text-white p-2 bg-primary-darken">Uso del tiempo</h4>
                            </div>
                            <div class="col-md-6 col-sm-12 px-0">
                                <div id="uso_tiempo_chart" class=" w-100"></div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <ul class="list-group list-group-flush my-2" id="first_list">
                                    <li class="list-group-item p-0">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-3 list-head-child">
                                                    <h2>90%</h2>
                                                </div>
                                                <div class="col-md-3 list-head-child">
                                                    <h4>
                                                        1150<br>
                                                        <small class="text-muted">Horas</small>
                                                    </h4>
                                                </div>
                                                <div class="col-md-6 list-head-child">
                                                    <h4>
                                                        Gestion Comercial<br>
                                                        <small class="text-muted">Apertura del mercado</small>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item p-0">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-2 list-body-child">
                                                    <h5>45%</h5>
                                                </div>
                                                <div class="col-md-3 list-body-child">
                                                    <h5 class="font-weight-normal">1000 horas</h5>
                                                </div>
                                                <div class="col-md-3">
                                                    <h5>Gestion Comercial</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5 class="font-weight-normal">Planeacion</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item p-0">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-2 list-body-child">
                                                    <h5>45%</h5>
                                                </div>
                                                <div class="col-md-3 list-body-child">
                                                    <h5 class="font-weight-normal">1000 horas</h5>
                                                </div>
                                                <div class="col-md-3">
                                                    <h5>Gestion Comercial</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5 class="font-weight-normal">Planeacion</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item p-0">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-2 list-body-child">
                                                    <h5>45%</h5>
                                                </div>
                                                <div class="col-md-3 list-body-child">
                                                    <h5 class="font-weight-normal">1000 horas</h5>
                                                </div>
                                                <div class="col-md-3">
                                                    <h5>Gestion Comercial</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5 class="font-weight-normal">Planeacion</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item p-0">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-2 list-body-child">
                                                    <h5>45%</h5>
                                                </div>
                                                <div class="col-md-3 list-body-child">
                                                    <h5 class="font-weight-normal">1000 horas</h5>
                                                </div>
                                                <div class="col-md-3">
                                                    <h5>Gestion Comercial</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5 class="font-weight-normal">Planeacion</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row" onclick="abrirgestion()">
                            <div class="col-md-12">
                                <h4 class="text-white bg-primary-darken p-2">Gestión de Actividades Pendientes</h4>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-title text-primary" id="porcentaje_hecha_tiempo">89%</h2>
                                        <p class="card-text text-dark">Actividades hechas a tiempo</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-title text-primary" id="pendientes_semana">4</h2>
                                        <p class="card-text text-dark">Actividades pendientes semana</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-title text-primary" id="pendientes_mes">11</h2>
                                        <p class="card-text text-dark">Actividades pendientes mes</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-title text-primary" id="aplazadas_mes">20</h2>
                                        <p class="card-text text-dark">Actividades Aplazadas</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" onclick="abrircumplimientollenado()">
                            <div class="col-md-12">
                                <h4 class="text-white bg-primary-darken p-2">Cumplimiento llenado</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4"><img src="/images/icons/bitacora.png" class="custom-img">
                                        <p class="custom-badge">Bitacora</p>
                                    </div>
                                    <div class="col-md-4"><span class="badge badge-secondary"><h5 class="my-1" id="cumplimiento_bitacora">93.7%</h5></span></div>
                                    <div class="col-md-4">
                                        <div class="progress">
                                            <div class="progress-bar cumplimiento_bitacora" role="progressbar" style="width: 93%" aria-valuenow="93" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4"><img src="/images/icons/panel_semanal.png" class="custom-img">
                                        <p class="custom-badge">Plan semanal</p>
                                    </div>
                                    <div class="col-md-4"><span class="badge badge-secondary"><h5 class="my-1" id="porcentajecumplimientosemanal">69.7%</h5></span></div>
                                    <div class="col-md-4">
                                        <div class="progress">
                                            <div class="progress-bar porcentajecumplimientosemanal" role="progressbar" style="width: 69%" aria-valuenow="69" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4"><img src="/images/icons/presupuesto_mensual.png" class="custom-img">
                                        <p class="custom-badge">Presupuesto mensual</p>
                                    </div>
                                    <div class="col-md-4"><span class="badge badge-secondary"><h5 class="my-1" id="porcentajecumplimientopresupuesto">91.7%</h5></span></div>
                                    <div class="col-md-4">
                                        <div class="progress">
                                            <div class="progress-bar porcentajecumplimientopresupuesto" role="progressbar" style="width: 91%" aria-valuenow="91" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Modal
</button>

<button type="button" class="btn btn-primary" id="do_test" onclick="test_canvas()">
  test canvas
</button>-->

<div class="container">
    <div class="row">
        <div class="col-md-12" id="test_container">
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-wide fade p-4" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg mw-100 w-75" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo_modal">Título dinámico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-0">
      <div class="container">
      <!--Charts Modal Negocios Cerrados-->
      <div class="row informacion-modal" id="modal_negocios_cerrados" style="display:none;">
         <div class="col-md-12">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <p class="h5 text-center">Seleccione los cargos por los que desea filtrar <em>(Presione la tecla Ctrl para seleccionar varios)</em></p>
                    <select class="custom-select form-control" id="filtro_cargos_modal_negocios" multiple>
                      <?php foreach($data['cargos'] as $cargo){ ?>
                      <option value="<?=$cargo->cargo?>" <?php if($cargo->cargo == "Ejecutivo Comercial" || $cargo->cargo == "Gerente Comercial" || $cargo->cargo == "Partner Solution" || $cargo->cargo == "Director General"){?>selected<?php } ?> ><?=$cargo->cargo?></option>
                      <?php } ?>
                    </select>
                    <p class="bg-success h4" style="cursor:pointer;" onclick="filtrar_cargos()"><a class="text-white"><i class="fa fa-search"></i>  Buscar</a></p>
                </div>
            </div>
         </div>
          <div class="col-md-6 m">
             <h4>Monto Negocios cerrados</h4>
             <div class="chartwrapper">
              <div id="neg_cerr_chart"></div>
              </div>
          </div>
          <div class="col-md-6">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Promedio cierre de negocios</h4>
                          <div class="chartwrapper"><div id="chart_radar"></div></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12">
              <h4>Cantidad de Negocios cerrados</h4>
              <div class="container">
                  <div class="row justify-content-md-center" id="cantidad_negocios_cerrados_modal">

                  </div>
              </div>
          </div>
      </div>
      <!--Charts Cumplimiento llenado-->
      <div class="row informacion-modal" id="modal_cumplimiento_llenado" style="display:none;">
          <div class="col-md-4">
              <h4 class="text-info">% Llenado bitacora</h4>
              <div class="chartwrapper"><div id="top_x_div1" class=""></div></div>
          </div>
          <div class="col-md-4">
              <h4 class="text-info">% Llenado plan semanal</h4>
              <div class="chartwrapper"><div id="top_x_div2" class=""></div></div>
          </div>
          <div class="col-md-4">
              <h4 class="text-info">% Llenado Presupuesto mensual</h4>
              <div class="chartwrapper"><div id="top_x_div3" class=""></div></div>
          </div>
      </div>
      <!--Cards Gastos de Ventas-->
      <!--<div class="row">
          <div class="col-md-2">
                <div class="mb-3">
                   <img src="http://via.placeholder.com/200x200" class="rounded-circle w-50 h-50 shadow-2 invisible">
               </div>
              <div class="card  custom-card">
                  <div class="card-body">
                    <h1 class="text-info float-none">12,1 %</h1>
                    <hr class="my-1">
                    <h3 class="text-info">$45'234.212</h3>
                  </div>
                  <div class="card-footer p-2">
                  <h3 class="my-1">Total</h3>
                  </div>
                </div>
          </div>
          <div class="col-md-2">
             <div class="mb-3">
                   <img src="http://via.placeholder.com/200x200" class="rounded-circle w-50 h-50 shadow-2">
                   <img src="/images/icons/honduras.png" class="rounded-circle w-25 position-absolute img-thumbnail country-thumbnail">
               </div>
              <div class="card  custom-card">
                  <div class="card-body gastos-ventas-box text-white">
                    <h2 class="card-title mt-3">12,1 %</h2>
                    <h4 class="text-dark">Gastos totales:</h4>
                    <h4 class="">$45'234.212</h4>
                  </div>
                  <div class="card-footer p-2 gastos-ventas-box">
                  <h3 class="my-1 text-white">Mayra</h3>
                  </div>
                </div>
          </div>
          <div class="col-md-2">
             <div class="mb-3">
                   <img src="http://via.placeholder.com/200x200" class="rounded-circle w-50 h-50 shadow-2">
                   <img src="/images/icons/honduras.png" class="rounded-circle w-25 position-absolute img-thumbnail country-thumbnail">
               </div>
              <div class="card  custom-card">
                  <div class="card-body gastos-ventas-box text-white">
                    <h2 class="card-title mt-3">12,1 %</h2>
                    <h4 class="text-dark">Gastos totales:</h4>
                    <h4 class="">$45'234.212</h4>
                  </div>
                  <div class="card-footer p-2 gastos-ventas-box">
                  <h3 class="my-1 text-white">Mayra</h3>
                  </div>
                </div>
          </div>
          <div class="col-md-2">
             <div class="mb-3">
                   <img src="http://via.placeholder.com/200x200" class="rounded-circle w-50 shadow-2">
                   <img src="/images/icons/honduras.png" class="rounded-circle w-25 position-absolute img-thumbnail country-thumbnail">
               </div>
              <div class="card  custom-card">
                  <div class="card-body gastos-ventas-box text-white">
                    <h2 class="card-title mt-3">12,1 %</h2>
                    <h4 class="text-dark">Gastos totales:</h4>
                    <h4 class="">$45'234.212</h4>
                  </div>
                  <div class="card-footer p-2 gastos-ventas-box">
                  <h3 class="my-1 text-white">Mayra</h3>
                  </div>
                </div>
          </div>
      </div>-->
      <!--Charts Oportunidades Perdidas-->
      <div class="row informacion-modal" id="modal_oportunidades_perdidas" style="display: none;">
          <div class="col-md-12">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <p class="h5 text-center">Seleccione los cargos por los que desea filtrar <em>(Presione la tecla Ctrl para seleccionar varios)</em></p>
                    <select class="custom-select form-control" id="filtro_cargos_modal_oportunidadesperdidas" multiple>
                      <?php foreach($data['cargos'] as $cargo){ ?>
                      <option value="<?=$cargo->cargo?>" <?php if($cargo->cargo == "Ejecutivo Comercial" || $cargo->cargo == "Gerente Comercial" || $cargo->cargo == "Partner Solution"){?>selected<?php } ?> ><?=$cargo->cargo?></option>
                      <?php } ?>
                    </select>
                    <p class="bg-success h4" style="cursor:pointer;" onclick="filtrar_cargos_perdidas()"><a class="text-white"><i class="fa fa-search"></i>  Buscar</a></p>
                </div>
            </div>
          </div>
          <div class="col col-md-4">
            <h4 class="">Monto de negocios perdidos</h4>
            <h6 class="text-muted text-center text-capitalize">(<span> Millones </span>)</h6>
            <div class="chartwrapper" style="height: 400px;">
                <div id="m_negocios_perdidos"></div>
            </div>
          </div>
          <div class="col col-md-4">
             <h4 class="">Oportunidades perdidas</h4>
             <h6 class="text-muted text-center text-capitalize">(<span> Porcentaje </span>)</h6>
             <ul class="list-group" id="modal_oportunidades_perdidas_porcentaje">

             </ul>
          </div>
          <div class="col col-md-4">
             <h4 class="">Cantidad de negocios perdidos</h4>
             <div class="row" id="modal_oportunidades_perdidas_cantidad">
                 <div class="col-md-6 col-sm-12">
                     <img src="/images/file/clientes/595ea6fc8ec24.png" class="img-thumbnail rounded-circle w-25 d-inline">
                    <div class="card d-inline ">
                      <div class="card-body d-inline">
                        <span>Diego</span>
                      </div>
                    </div>
                    <div class="card d-inline bg-info text-white rounded-0">
                      <div class="card-body d-inline">
                        <span>25</span>
                      </div>
                    </div>
                 </div>
                 <div class="col-md-6 col-sm-12">
                     <img src="/images/file/clientes/595ea6fc8ec24.png" class="img-thumbnail rounded-circle w-25 d-inline">
                    <div class="card d-inline rounded-0">
                      <div class="card-body d-inline">
                        <span>Diego</span>
                      </div>
                    </div>
                    <div class="card d-inline bg-info text-white rounded-0">
                      <div class="card-body d-inline">
                        <span>25</span>
                      </div>
                    </div>
                 </div>
             </div>
          </div>
      </div>
      <!--Datos modal uso del tiempo-->
      <div class="row palette-parent informacion-modal" id="modal_uso_tiempo" style="display: none;">
            <div class="col-md-2 pr-md-0 pr-sm-4">
                <img src="/images/file/clientes/5a61398e7cd0a.png" class="rounded-circle img-thumbnail shadow-2" width="40%">
                <h4 class="text-info my-2">Mayra</h4>
            </div>

            <div class="col-md-2 col-sm-12">
                <div class="container card px-0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12 px-0 palette text-white h-50">
                                    <h2 class="mt-3">88%</h2>
                                </div>
                                <div class="col-md-12 px-0 bg-secondary text-white h-50">
                                    <h4>1000 <small class="text-white ml-2">Horas</small></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 pl-2">
                            <h5 class="">Gestión comercial</h5>
                            <h5 class="font-weight-normal">Gestión Mejoramiento continuo</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 col-sm-12">
                <div class="container card px-0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12 px-0 palette text-white h-50">
                                    <h2 class="mt-3">88%</h2>
                                </div>
                                <div class="col-md-12 px-0 bg-secondary text-white h-50">
                                    <h4>1000 <small class="text-white ml-2">Horas</small></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 pl-2">
                            <h5 class="">Gestión comercial</h5>
                            <h5 class="font-weight-normal">Gestión Mejoramiento continuo</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 col-sm-12">
                <div class="container card px-0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12 px-0 palette text-white h-50">
                                    <h2 class="mt-3">88%</h2>
                                </div>
                                <div class="col-md-12 px-0 bg-secondary text-white h-50">
                                    <h4>1000 <small class="text-white ml-2">Horas</small></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 pl-2">
                            <h5 class="">Gestión comercial</h5>
                            <h5 class="font-weight-normal">Gestión Mejoramiento continuo</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 col-sm-12">
                <div class="container card px-0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12 px-0 palette text-white h-50">
                                    <h2 class="mt-3">88%</h2>
                                </div>
                                <div class="col-md-12 px-0 bg-secondary text-white h-50">
                                    <h4>1000 <small class="text-white ml-2">Horas</small></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 pl-2">
                            <h5 class="">Gestión comercial</h5>
                            <h5 class="font-weight-normal">Gestión Mejoramiento continuo</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 col-sm-12">
                <div class="container card px-0">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12 px-0 palette text-white h-50">
                                    <h2 class="mt-3">88%</h2>
                                </div>
                                <div class="col-md-12 px-0 bg-secondary text-white h-50">
                                    <h4>1000 <small class="text-white ml-2">Horas</small></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 pl-2">
                            <h5 class="">Gestión comercial</h5>
                            <h5 class="font-weight-normal">Gestión Mejoramiento continuo</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!--MODALES VIEJOS-->
      <!--Gestión de actividades pendientes-->
      <div class="row informacion-modal" id="modal_gestion" style="display: none;">
          <div class="col-md-12">
              <div id="ges_act_pen_1"></div>
          </div>
          <div class="col-md-12">
              <div class="row">
                  <div class="col-md-6">
                       <h4>Porcentaje hechas a tiempo</h4>
                        <div id="ges_act_pen_2" class="mt-5"></div>
                      </div>
                  <div class="col-md-6">
                         <h4>Aplazadas por cada comercial</h4>
                          <div id="ges_act_pen_3" class="mt-5"></div>
                  </div>
                  </div>
              </div>
          </div>
      <!--Oportunidades Oportunidades identificadas modal-->
      <div class="row informacion-modal" id="modal_oportunidades_identificadas" style="display: none;">
          <div class="col-md-4 col-sm-12">
            <p class="h4 text-center">(Millones)</p>
            <p class="h5 text-center text-info">Por Comercial</p>
            <div class="chartwrapper">
               <div class="" id="opor_per_mdl_1"></div>
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <p class="h4 text-center">(Millones)</p>
            <p class="h5 text-center text-info">Probabilidad Alta/Media/Baja</p>
            <div class="chartwrapper">
               <div id="opor_per_mdl_2"></div>
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <p class="h4 text-center">(Millones)</p>
            <p class="h5 text-center text-info">Corto/Mediano/Largo Plazo</p>
            <div class="chartwrapper">
               <div id="opor_per_mdl_3"></div>
           </div>
          </div>
      </div>
      <!--Oportunidades Identificadas 2 -->
      <div class="row informacion-modal" id="modal_oportunidades_identificadas2" style="display: none;">
          <div class="col-md-12">
              <div class="row">
                    <div style="width:22%"></div>
                    <div style="width: 23.3%">
                        <span class="text-info">Corto</span> Plazo (Millones)
                    </div>
                    <div style="width: 23.3%">
                        <span class="text-info">Mediano</span> Plazo (Millones)
                    </div>
                    <div style="width: 23.3%">
                        <span class="text-info">Largo</span> Plazo (Millones)
                    </div>
              </div>
          </div>
          <div class="col-md-12" id="usuarios_oportunidades_identificadas2">

          </div>
      </div>
      <!--Cono de oportunidades -->
      <div class="row informacion-modal" id="modal_cono_oportunidades" style="display: none;">
         <div class="col-md-2">

         </div>
         <div class="col-md-10">
             <div class="row">
                 <div class="col-md-4">
                     <p class="h4">Alta</p>
                 </div>
                 <div class="col-md-4">
                     <p class="h4">Media</p>
                 </div>
                 <div class="col-md-4">
                     <p class="h4">Baja</p>
                 </div>
             </div>
         </div>
         <div class="col-md-2">
              <div class="row" style="margin-top: 87px;">
                 <div class="col-md-12">
                      <p class="h5">Corto Plazo</p>
                      <p class="h6 text-info total-open-cono"></p>
                 </div>
              </div>
              <div class="row" style="margin-top: 243px;">
                 <div class="col-md-12">
                      <p class="h5">Mediano Plazo</p>
                      <p class="h6 text-info total-open-cono"></p>
                 </div>
              </div>
              <div class="row" style="margin-top: 210px;">
                 <div class="col-md-12">
                      <p class="h5">Lago Plazo</p>
                      <p class="h6 text-info total-open-cono"></p>
                 </div>
              </div>
           </div>
         <div class="col-md-10 text-center">
             <div class="row">
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-1"></div>
                     <p class="h5 text-info" id="total_corto_alta"></p>
                 </div>
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-2"></div>
                     <p class="h5 text-info" id="total_corto_media"></p>
                 </div>
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-3"></div>
                     <p class="h5 text-info" id="total_corto_baja"></p>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-4"></div>
                     <p class="h5 text-info" id="total_mediano_alta"></p>
                 </div>
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-5"></div>
                     <p class="h5 text-info" id="total_mediano_media"></p>
                 </div>
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-6"></div>
                     <p class="h5 text-info" id="total_mediano_baja"></p>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-7"></div>
                     <p class="h5 text-info" id="total_largo_alta"></p>
                 </div>
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-8"></div>
                     <p class="h5 text-info" id="total_largo_media"></p>
                 </div>
                 <div class="col-md-4">
                     <div class="text-center cono-graficas" id="meter-9"></div>
                     <p class="h5 text-info" id="total_largo_baja"></p>
                 </div>
             </div>
           </div>
      </div>
      <!--Ciclos de ventas-->
      <div class="row informacion-modal" id="modal_ciclos_venta" style="display:none;">

      </div>
      <!--Ventas Realizadas Año Actual-->
      <div class="row informacion-modal mx-3" id="modal_ventas" style="display:none;">

      </div>
      <!--Proyecciones Año Actual-->
      <div class="row informacion-modal mx-3" id="modal_proyecciones" style="display:none;">

      </div>
      <!-- Empresa & Cliente-->
      <div class="row informacion-modal mx-3" id="modal_empresas_clientes" style="display:none;">

      </div>
      <!--Costo de Venta-->
      <div class="row informacion-modal mx-3" id="modal_costo_venta" >

      </div>
      <!--Cumplimiento Presupuestal-->
      <div class="row informacion-modal mx-3" id="modal_cumplimiento_presupuestal" >

      </div>
     </div>
  </div>
 </div>
</div>
<form id="save-form" method="post">
    {{csrf_field()}}
    <input type="hidden" name="action" value="28">
</form>
      <!--Footer oculto(hidden="") por si necesita acciones (guardar, cerrar, cancelar)-->
      <div class="modal-footer" id="mdl_footer" hidden="">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>

@endsection @section('scripts')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script><!--Aceleradores-->
<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/assets/ArchivosDashboardNew/amcharts.js" type="text/javascript"></script><!--<script src="https://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>-->
<script src="/assets/ArchivosDashboardNew/responsive.min.js" type="text/javascript"></script><!--<script src="https://www.amcharts.com/lib/3/plugins/responsive/responsive.min.js"></script>-->
<script src="/assets/ArchivosDashboardNew/smoothCustomBullets.min.js" type="text/javascript"></script><!--<script src="https://www.amcharts.com/lib/3/plugins/tools/smoothCustomBullets/smoothCustomBullets.min.js"></script>-->
<script src="/assets/ArchivosDashboardNew/funnel.js" type="text/javascript"></script><!--<script src="https://www.amcharts.com/lib/3/funnel.js" type="text/javascript"></script>-->
<script src="https://cdn.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
<script src="/assets/ArchivosDashboardNew/radar.js" type="text/javascript"></script><!--<script src="https://www.amcharts.com/lib/3/radar.js"></script>-->
<script src="/assets/ArchivosDashboardNew/serial.js" type="text/javascript"></script><!--<script src="https://www.amcharts.com/lib/3/serial.js"></script>-->
<script src="/components/amchart/js/gauge.js"></script>
<script src="/assets/ArchivosDashboardNew/html2canvas.min.js" type="text/javascript"></script><!--<script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>-->
<script src="/assets/ArchivosDashboardNew/rgbcolor.js" type="text/javascript"></script><!--<script type="text/javascript" src="https://canvg.github.io/canvg/rgbcolor.js"></script>-->
<script src="/assets/ArchivosDashboardNew/StackBlur.js" type="text/javascript"></script><!--<script type="text/javascript" src="https://canvg.github.io/canvg/StackBlur.js"></script>-->
<script src="/assets/ArchivosDashboardNew/canvg.js" type="text/javascript"></script><!--<script type="text/javascript" src="https://canvg.github.io/canvg/canvg.js"></script>-->
<!--Filtro fechas-->
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="/js/dashboard/board_general_production.js"></script>
<script>
 // targetElem is a jquery obj
function take (targetElem) {
    var nodesToRecover = [];
    var nodesToRemove = [];

    var svgElem = targetElem.find('svg');

    svgElem.each(function(index, node) {
        var parentNode = node.parentNode;
        var svg = parentNode.innerHTML;

        var canvas = document.createElement('canvas');

        canvg(canvas, svg);

        nodesToRecover.push({
            parent: parentNode,
            child: node
        });
        parentNode.removeChild(node);

        nodesToRemove.push({
            parent: parentNode,
            child: canvas
        });

        parentNode.appendChild(canvas);
    });

    html2canvas(targetElem, {
        onrendered: function(canvas) {
            var ctx = canvas.getContext('2d');
            ctx.webkitImageSmoothingEnabled = false;
            ctx.mozImageSmoothingEnabled = false;
            ctx.imageSmoothingEnabled = false;

            canvas.toBlob(function(blob) {
                nodesToRemove.forEach(function(pair) {
                    pair.parent.removeChild(pair.child);
                });

                nodesToRecover.forEach(function(pair) {
                    pair.parent.appendChild(pair.child);
                });
                saveAs(blob, 'screenshot_'+ moment().format('YYYYMMDD_HHmmss')+'.png');
            });
        }
    });
}
</script>
<script id="script_dinamico"></script>
<script id="dona_identificada"></script>
<script id="uso_tiempo_torta"></script>
<script id="graficas_gestion_actividades_pendientes"></script>
<script id="scriptmodal_cumplimiento_llenado"></script>
<script id="script_modal_empresa_cliente"></script>
@endsection
