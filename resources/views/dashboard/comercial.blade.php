@extends('template.app')

@section('title', 'dashboard')

<!--@section('sidebar')
    @parent    
@endsection-->

@section('content')

<style type="text/css">
          .tabs.active {
            margin: 0.5px;
            background-color: #5b5d5c;
            color: #fff;
            padding: 9px;
            background-image: -moz-linear-gradient( 90deg, rgb(81,80,80) 0%, rgb(133,133,133) 49%, rgb(185,185,185) 100%);
             background-image: -webkit-linear-gradient( 90deg, rgb(81,80,80) 0%, rgb(133,133,133) 49%, rgb(185,185,185) 100%);
             background-image: -ms-linear-gradient( 90deg, rgb(81,80,80) 0%, rgb(133,133,133) 49%, rgb(185,185,185) 100%);
             box-shadow: 0px 2px 5px 0px rgba(9, 42, 33, 0.6),inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);

        }
        .alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img{
          border:none !important;
        }
        .tabs{
            width: 24%;
            text-align: center;
            margin: 0.5px 3.5px !important;
            background-color: #1f63dd;
            color: #fff;
            padding: 5px;
            border: 4px solid #d1d1d1;
            border-radius: 50px;
            background-image: -moz-linear-gradient( 90deg, rgb(1,62,124) 0%, rgb(3,144,245) 100%);
             background-image: -webkit-linear-gradient( 90deg, rgb(1,62,124) 0%, rgb(3,144,245) 100%);
             background-image: -ms-linear-gradient( 90deg, rgb(1,62,124) 0%, rgb(3,144,245) 100%);
             box-shadow: 0px 2px 5px 0px rgba(9, 42, 33, 0.6),inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);

        }
        .tabs a {
            color: #fff;
        }

        .bandera-perfil{
          height: 80px
          width:80px;
        }
        .container2{padding:15px 30px;min-height:500px}
        .division-1{min-height:690px;position:relative;background-size:contain;background-repeat:no-repeat;background-position:calc(50%);z-index:0}
        .titulo-division-1{height:27px;background-color:#014eda;padding-top:2px;color:#fff;width:100%;text-align:center;position:absolute;overflow-x:hidden;top:15px}
        .titulo-division-2{height:27px;background-color:#00b0d2;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px}
        .tab-titulo-division-2{height:27px;background-color:#5f0101;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px}
        .tab-titulo-division-3{background-color:#1f628c;padding-top:2px;padding-left:5px;color:#fff;width:100%;font-size:14px;overflow-x:hidden}
        .collections-title2{margin-bottom:0!important;font-size:12px;padding:5px;color:#000}
        .padding-top-4{padding-top:1px}
        .task-cat2{padding:3px 7px;color:#fff;font-weight:700;font-size:.8rem;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;background-clip:padding-box}
        .titulo-division-3{height:27px;background-color:#004f9b;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);background-image:-webkit-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);background-image:-ms-linear-gradient(90deg,#0390f5 0%,#013e7c 100%)}
        .titulo-division-4{height:27px;background-color:#1e8000;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#c7eb1f 0%,#33691e 100%);background-image:-webkit-linear-gradient(90deg,#c7eb1f 0%,#33691e 100%);background-image:-ms-linear-gradient(90deg,#c7eb1f 0%,#33691e 100%)}
        .titulo-division-5{height:27px;background-color:#004f9b;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-image:-moz-linear-gradient(90deg,#92d1ff 0%,#013e7c 100%);background-image:-webkit-linear-gradient(90deg,#92d1ff 0%,#013e7c 100%);background-image:-ms-linear-gradient(90deg,#92d1ff 0%,#013e7c 100%)}
        #chartdiv{padding-top:110px;overflow:initial!important}
        .amcharts-chart-div a{display:none!important}
        .caja-tab{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:5px;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
        .caja-lineaspark{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:0!important;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
        .caja-tab-2{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:5px;float:left;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
        .margen-superior .col-md-8{-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
        .caja-derecha{margin-left:0!important}
        .cuadro-1-linea-1{display:block;text-align:center;font-size:11px}
        .cuadro-1-linea-2{text-align:center;display:block;font-size:29px;font-weight:initial;color:#00b0d2}
        .cuadro-1-linea-3{text-align:center;display:block;font-size:24px;font-weight:initial;color:#00b0d2}
        .hr{margin-bottom:0!important;margin-top:0!important}
        .cuadro-2-linea-2{text-align:center;display:block;font-size:35px;font-weight:initial;color:#00b0d2}
        .cuadro-3-linea-1{display:block;text-align:center;font-size:11px;color:#2657dc}
        .cuadro-3-linea-2{text-align:center;display:block;font-size:35px;font-weight:initial;color:#2657dc}
        .tab-cuadro-3-linea-1{display:block;text-align:center;font-size:11px;color:#FFF}
        .tab-cuadro-3-linea-2{text-align:center;display:block;font-size:35px;font-weight:initial;color:#FFF}
        .margen-superior{margin-top:5px}
        #donut-example{height:200px!important}
        #morris-bar{height:200px!important}
        .item{padding-top:4px;max-width:9%;min-width:9%;height:137px;display:inline-block;text-align:center;border:.5px solid #ccc;margin-top:5px}
        .linea1-item{font-size:9px;display:block;line-height:.8;color:#000}
        .linea2-item{font-size:9px;display:block;line-height:.8;color:#000}
        .linea3-item{font-size:17px;display:block;line-height:.8;color:#000}
        .linea4-item{font-size:14px;display:block;font-weight:700;color:#60b90b;line-height:.8}
        .linea5-item{font-size:9px;color:#305ddb}
        .blue{color:#305ddb}
        .centrar-contenido{left:0;right:0;text-align:center}
        .item2{width:24.8%;margin:.1%;background-color:#e8e8e8;float:left;text-align:center;border:.5px solid #ccc;padding-top:4px}
        .linea1-item2{font-size:33px;color:#305ddb;display:block;font-weight:700;line-height:.8;position:relative;height:46px}
        .linea2-item2{font-size:13px;color:#305ddb;display:block;padding:5px;z-index:9;position:absolute;right:0;left:0;line-height:70px;top:-3px}
        .item2 label:hover,.item2 span:hover{color:#868686}
        .select-mes{position:absolute;right:0;margin-right:19px;height:27px;width:209px;font-weight:100;font-size:12px;border:1px solid #306fdf}
        .logo-essi{margin-top:32px}
        #lista-comercial{width:100%;height:500px}
        .margen-inferior{margin-bottom:20px}
        .borde-derecho{border-right:1px solid #d7d9da;text-align:center}
        #h6{line-height:1px;margin-top:12px}
        #millones{font-size:12px}
        #barra-horizontal{width:100%;height:250px}
        #pie{width:100%;height:250px}
        #negocio_perdido{width:100%;height:500px}
        .borde-inferior{border-bottom:1px solid #d7d9da}
        .pink.accent-2{background-color:#0093c9!important}
        .task-cat{padding:6px 7px;color:#fff;font-weight:700;font-size:.8rem;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;background-clip:padding-box}
        .pink{background-color:#0093c9!important}
        .progress{background-color:rgba(255,64,129,0.22);position:relative;height:4px;display:block;width:100%;background-color:#ddd;border-radius:2px;margin:.5rem 0 1rem;overflow:hidden}
        .progress .determinate{position:absolute;background-color:inherit;top:0;left:0;bottom:0;background-color:#0093c9;-webkit-transition:width .3s linear;-moz-transition:width .3s linear;-o-transition:width .3s linear;-ms-transition:width .3s linear;transition:width .3s linear}
        .progress2 .determinate{position:absolute;background-color:inherit;top:0;left:0;bottom:0;background-color:#36a2e3;-webkit-transition:width .3s linear;-moz-transition:width .3s linear;-o-transition:width .3s linear;-ms-transition:width .3s linear;transition:width .3s linear}
        #contenedor-perdida{position:absolute;top:0;bottom:0;left:0;right:0;width:90%;height:30%;margin:auto}
        .lista-caja{padding:15px 0 0}
        #cantidad-perdida{width:100%;height:500px}
        .divimagen{width:110px;height:110px;background-size:cover;border-radius:100%;display:inline-block;position:relative;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:1px 1px 12px -1px rgba(0,0,0,0.56);margin-top:50px}
        .divimagen-2{width:55px;height:55px;background-size:cover;border-radius:100%;display:inline-block;position:relative;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:1px 1px 12px -1px rgba(0,0,0,0.56)}
        .margen-contenedor{padding:0 75px}
        .cuadro{display:inline-block;margin-left:50px;font-size:10px;margin-top:23px;width:65px;display:inline-block}
        .cuadro-color-azul{width:20px;height:8px;float:left;display:inline-block;background-color:#0061a6;margin-right:5px;margin-top:4px}
        .cuadro-color-verde{width:20px;height:8px;float:left;display:inline-block;background-color:#00a944;margin-right:5px;margin-top:4px}
        .cuadro-color-gris{width:20px;height:8px;float:left;display:inline-block;background-color:#6e91a4;margin-right:5px;margin-top:4px}
        .titulo-color{display:inline-block;float:left}
        .borde-inferior-2{padding-bottom:27px;border-bottom:1px solid #0012be}
        .nombre-avatar{position:absolute;top:124px;font-size:13px;right:0;left:0;text-align:center}
        .nombre-avatar-2{position:absolute;top:55px;font-size:13px;right:0;left:0;text-align:center}
        .celda-celeste{background-color:#e3edf6}
        .celda-azul{background-color:#cce1f2}
        .graficas{width:55%;text-align:right;float:right}
        .titulo-graficas{width:100%;text-align:right;float:right;padding:0 75px}
        .linea-titulo-grafica{width:19%;display:inline-block;text-align:center}
        .graficas-alta{height:200px;width:32%;float:left;margin-right:7px}
        .graficas-mediano{height:200px;width:32%;float:left;margin-right:7px}
        .graficas-corto{height:200px;width:32%;float:left}
        .tabla{width:100%;text-align:center}
        .td-85{height:85px}
        .tabla-padding{padding:15px 40px 40px 15px !important}
        .borde-azul{border-bottom:1px solid #0748c8}
        .linea3-item2{font-size:22px;display:block;line-height:.8;color:#66ae20}
        #dona-2{height:450px}
        .h6{text-align:center}
        .cabecera-tabla{background-color:#00629e;color:#fff}
        .subcabecera-tabla{text-align:center;background-color:#01356f;color:#FFF;font-size:12px}
        .numero-titulo{font-size:25px;text-align:center;background-image:-moz-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%);background-image:-webkit-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%);background-image:-ms-linear-gradient(90deg,#013e7c 0%,#92d1ff 100%)}
        .letra-titulo{display:block;font-size:13px}
        .width-100{width:100%}
        .bandera{position:absolute;bottom:0;right:0;margin-right:-13px;margin-bottom:-6px}
        .numero-tabla{font-size:25px;padding:7px}
        .width-10{width:10%;display:inline-block;position:relative}
        .width-30{width:29%;display:inline-block}
        .heigth-250{height:250px}
        .height-200{height:200px}
        .absoluto{position:absolute}
        .height-100{height:100%;width:100%}
        .relativo{position:relative}
        .bottom-0{bottom:0}
        .derecha{text-align:right}
        .dona{height:300px;width:100%}
        .bloque{display:block}
        .card-content{text-align:center}
        .card .card-content{border-radius:0 0 2px 2px}
        .white-text{color:#fff!important}
        .purple{background-color:#507e8d!important}
        .card-stats-title{font-size:1.2rem}
        .card .card-content p{margin:0;color:inherit}
        .card-stats-title i{font-size:1.2rem}
        .card-stats-number{font-size:1.8rem;line-height:2rem;color:#fff;margin:.2rem 0;font-weight:500}
        .card .card-action{padding:10px 14px;border-top:1px solid rgba(160,160,160,0.2);padding:20px}
        .purple.darken-2{background-color:#395469!important}
        .width-20{width:19%}
        .blue-sky{background-color:#2ecded!important}
        .blue-sky.darken-2{background-color:#0087be!important}
        .blue-king{background-color:#002365!important}
        .blue-king.darken-2{background-color:#0093d6!important}
        .blue-sky .card-stats-number,.blue-king .card-stats-number{font-size:.9rem;line-height:2rem;color:#fff;margin:.2rem 0;font-weight:500}
        .division-caja-tab-2{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;padding:11px;float:left;-webkit-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);-moz-box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56);box-shadow:2px 9px 8px -5px rgba(0,0,0,0.56)}
        .marron-1{background-color:#ae9189}
        .marron-2{background-color:#9a6e63}
        .marron-3{background-color:#845448}
        .marron-4{background-color:#5f3326}
        #radar{height:250px;width:100%;margin-top:15px}
        .padding-7{padding:7px}
        .caja-20{display:inline-block;border:.5px solid #d7d7d7;text-align:center;margin-top:36px;height:170px;position:relative}
        .linea-titulo{margin-top:20px;font-size:21px;line-height:1px}
        .nombre-caja{font-size:10px;color:#636e7f}
        .height-40{height:40px;bottom:0;position:absolute;width:100%}
        .caja-contenedora{text-align:center;right:0;left:0}
        .subcaja{display:inline-block;border:.2px solid #ccc;position:relative;height:70px;background-color:#fff}
        .distin-fondo{background:#f8f8f8}
        .tamano-30{font-size:30px}
        .titulo-pendientes{color:#000;font-size:14px;padding:0 8px}
        .linea-menor{font-size:11px;display:block}
        .tres-dos{position:absolute;bottom:0;height:32px;right:0;left:0;text-align:center}
        .progress2{position:relative;height:4px;display:block;width:100%;background-color:#395468;border-radius:2px;margin:.5rem 0 1rem;overflow:hidden}
        @media (max-width: 600px) {
        .container2{padding:0}
        .linea2-item2{font-size:13px;color:#305ddb;display:block;padding:5px;z-index:9;position:relative;right:0;left:0;line-height:10px;top:-3px}
        .item{padding-top:4px;max-width:90px;min-width:90px;height:130px;display:inline-block;text-align:center;border:.5px solid #ccc;margin-top:5px}
        .tabs a{font-size:13px;margin-left:0;margin-right:0}
        .select-mes{position:relative;right:0;height:27px;width:100%;font-weight:100;font-size:12px;border:1px solid #306fdf;margin-top:13px}
        #oportunidad_perdida{height:500px;display:block}
        }
        .card-action1{border-top:1px solid rgba(160,160,160,0.2);padding:10px 14px}
        .titulo-card{padding:10px;font-size:17px;color:#fff}
        .blue-sky .card-stats-title,.blue-king .card-stats-title{line-height:1.3px}
        .texto-sombra{font-size:35px;text-shadow:1px 2px 4px #636060}
        .blue-sky .card-stats-title,.blue-king .card-stats-title{font-size:.7rem}
        .caja-avatar{width:100%;position:relative;height:71px;text-align:center}
        .cabecera-marron-1{background-color:#ae9189;padding:12px;border-right:.5px solid #fff;text-align:center}
        .cabecera-marron-2{background-color:#9a6e63;padding:12px;border-right:.5px solid #fff;text-align:center}
        .cabecera-marron-3{background-color:#845448;padding:12px;border-right:.5px solid #fff;text-align:center}
        .cabecera-marron-4{background-color:#5f3326;padding:12px;border-right:.5px solid #fff;text-align:center}
        .titulo-marron-1{color:#fff}
        .td-marron{border-bottom:.5px solid #ccc;text-align:center}
        .td-marron .linea3-item2{font-size:28px;display:block;line-height:.8;color:#5d5d5d}
        .tabla-2 .td-85{height:85px;width:125px;text-align:center}
        .caja-negra{height:64px;width:100%;line-height:.9;background-color:#494949;margin-top:5px;position:absolute;bottom:0}
        .caja-negra span{font-size:8px;color:#bdbdbd;line-height:10px;white-space:pre-line}
        .tamano-110{height:110px;text-align:center}
        .img-caja{top:-30px;position:relative;left:0;right:0;text-align:center}
        .borde-derecho{border-right:1px solid #01c7ec}
        .fondo-gris{background-color:#e4e4e4}
        .linea3-item3{font-size:18px;display:block;line-height:.8}
        .tabla-3{width:100%;text-align:center}
        .sostenido{background-color:#d0eff4!important}
        #linea-pendiente{width:100%;height:450px}
        #radar-pendiente{width:100%;height:450px}
        #radar2-pendiente{width:100%;height:450px}
        </style>
        <style type="text/css">
        .nav>li>a{padding:0!important;background-color:transparent!important}
        .nav-tabs>li>a:hover{background-color:transparent!important;border:none!important}
        .nav-tabs>li.active>a,.nav-tabs>li.active>a:focus,.nav-tabs>li.active>a:hover{background-color:transparent!important;border:none!important;padding-top:3px}
        .segundo-cuadro{position:absolute;top:15px}
        .segundo-cuadro .nombre{font-weight:bolder;text-align:center}
        .segundo-cuadro .cargo{color:#2d2d2d;font-size:14px;text-align:left;width:100%}
        .bandera-area{height:50px;width:50px;display:inline-block;background-repeat:no-repeat;background-size:cover;margin:8px 5px 20px;background-size: 73%;background-position: center;}
        .nombre-bandera{font-size:10px;font-weight:700;color:#000;position:absolute;left:0;right:0;bottom:-26px}
        .caja-media{border-top:.5px solid #f1eded;background-color:#fff;border-left:.5px solid #f1eded;border-right:.5px solid #f1eded;border-bottom:1.5px solid #f1eded;height:230px;margin:10px}
        .titulo-caja-media{height:40px;background-color:#001862;padding-top:7px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:25px}
        .titulo-caja-media-2{height:40px;color:#001862;background-color:#73caff;padding-top:7px;width:100%;text-align:center;overflow-x:hidden;top:25px;font-weight:500}
        .numero-caja-media{text-align:center;display:block;font-size:35px;font-weight:initial;color:#001862;line-height:1.3;margin-top:4%}
        .letra-caja-media{display:block;text-align:center;font-size:15px;font-weight:500;color:#000}
        .tab-content{background-color:transparent!important}
        .padding-0{padding:0!important}
        .titulo-verde-celeste{height:27px;background-color:#01c3a9;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px}
        .cuerpo-verde-celeste{text-align:center;display:block;font-size:29px;font-weight:initial;color:#0271cb}
        .numero-verde-celeste{text-align:center;display:block;font-size:50px;font-weight:initial;color:#0271cb;line-height:1.3}
        .subtitulo-verde-celeste{display:block;text-align:center;font-size:15px;font-weight:500;color:#000}
        .negocios-perdidos{height:27px;padding-top:2px;color:#fff;width:100%;text-align:center;overflow-x:hidden;top:15px;background-color:#01c3a9}
        .negocios-perdidos-numero{text-align:center;display:block;font-size:50px;font-weight:initial;color:#0271cb;line-height:1.8}
        .icono{height:60px;background-color:#ffa600}
        .icono-agenda-semanal{height:60px;background-color:#00406c}
        .numero-naranja{height:124px;padding:20px 0}
        .numero-naranja-grande{text-align:center;display:block;font-size:60px;color:#ffa600;font-weight:bolder;line-height:1;margin-top:15px}
        .numero-naranja-letra{display:block;text-align:center;font-size:17px;color:#000}
        .titulo-agenda-semanal{height:60px;text-align:center;background-color:#fff;padding:15px;color:#000;font-size:20px}
        .margen-doble-superior{margin-top:30px}
        .semana-anterior{width:49.1%;height:120px;display:inline-block;background-color:#427594}
        .semana-actual{width:49.1%;height:120px;display:inline-block;background-color:#4aa0d1}
        .caja-tab-2.padding-0{border-bottom:0!important;background-color:transparent!important}
        .padding-0 .caja-tab-2{padding:0!important}
        .caja-tab{border-bottom:0!important}
        .padding-derecho-20 .caja-tab-2{background-color:transparent!important;border-bottom:0!important;box-shadow:none!important}
        .icono-presupuesto-mensual{height:60px;background-color:#570069!important}
        .titulo-agenda-mensual{height:60px;text-align:center;background-color:#fff;padding:15px;color:#570069}
        .titulo-agenda-mensual-2{height:60px;text-align:center;padding:15px;background-color:#a1a1a1;font-weight:700;color:#570069;font-size:21px;margin-bottom:0}
        .titulo-agenda-mensual-3{height:60px;text-align:center;padding:15px;background-color:#340040;color:#fff;padding:7px;font-weight:700;border-top:2px solid #fff;font-size:16px}
        .titulo-agenda-mensual-3 span{display:block}
        .icono-lateral{margin:17px}
        .tabs a{margin-left:5px!important;margin-right:5px!important}
        .col-md-1.cerrados{width:64.9px!important;max-width:64.9px!important;min-width:64.9px!important}
        .titulo-semana{color:#fff;display:block;text-align:center;font-size:20px;margin-top:13px}
        .cantidad-semana{text-align:center;font-size:19px;font-weight:700;color:#fff;margin-top:12px}
        .cantidad-semana span{display:block}
        .cuadro-menor{height:40px;width:50px;display:block;margin-top:13px;float:right;background-color:#003e6d}
        .cuadro-menor img{padding:12px}
        .cuadro-menor-2{height:40px;width:50px;display:block;float:right;background-color:#570069}
        .cuadro-menor-2 img{padding:12px}
        .bitacora-dia{display:inline-block;width:24%;text-align:center;color:#011c53;font-weight:700;font-size:18px}
        .dias-bitacora{background-color:#bbdaef;height:60px;padding:15px;color:#000}
        .contenedor-bitacora{width:100%;height:95px;display:inline-block;background-color:#0053a3}
        .contenido-contenedor{margin-top:10px;width:24%;text-align:center;color:#fff;display:inline-block}
        .contenido-contenedor span{display:block}
        .icono-contenedor{position:absolute;right:0;left:0;bottom:-26px}
        .bandera-nombre{height:70px;width:70px;display:inline-block;background-repeat:no-repeat;background-size:cover;margin:8px 5px 20px;position:absolute;left:-79px;z-index:99}
        .icono-agenda-bitacora{height:60px;background-color:#0053a3}
        #lineas-negras{width:100%;height:350px}
        .contedor-linea{background-color:#012d46;color:#fff}
        .contenedor-barras{background-color:#003d80;color:#fff;background-image:-moz-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);background-image:-webkit-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);background-image:-ms-linear-gradient(90deg,#0390f5 0%,#013e7c 100%);box-shadow:0 1px 8px 0 rgba(0,0,0,0.28)}
        #barras-panel2{width:100%;height:300px}
        .cumplimiento-presupuestal{background-color:#49b500;color:#fff;line-height:1;padding:9px}
        .cuadro-indicador{height:83px;padding:0;background-color:#fff;font-size:35px;font-weight:700;color:#49b500}
        .cuadro-indicador span{font-size:13px;display:block;font-weight:700;color:#000}
        .caja-verde{text-align:center;padding:0;margin-right:0;padding-right:5px}
        .sublinea{width:70%;border-bottom:.1px solid #ccc;margin-left:15%}
        .padding-left-0{padding-left:0!important}
        .botones_estaticos{width:24%;text-align:center;margin:.5px 3.5px!important;background-color:#1f63dd;color:#fff;padding:5px;border:4px solid #d1d1d1;border-radius:50px;background-image:-moz-linear-gradient(90deg,#013e7c 0%,#0390f5 100%);background-image:-webkit-linear-gradient(90deg,#013e7c 0%,#0390f5 100%);background-image:-ms-linear-gradient(90deg,#013e7c 0%,#0390f5 100%);box-shadow:0 2px 5px 0 rgba(9,42,33,0.6),inset 0 1px 0 0 rgba(255,255,255,0.5)}
        .botones_estaticos a{color:#fff;font-size:13px;margin-left:5px!important;margin-right:5px!important}
            .cumpli-presu-child{}

            .cumpli-presu-child:nth-child(1){
                background-color: #77b41b;
                color: white;
            }
    </style>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    
@php
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
@endphp
    <div class="container2 animated flipInX">
    <div class="row">
      <form id="formFilter" method="GET" action="{{ url('comercial') }}" style="width: 30%;">        
        <div class="form-group row">
        <?php 
        $modulo=1;
        $acceso = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="select responsable" AND `id_permisomodulo`="'.$modulo.'"');
        if(isset($acceso[0]->id)){
          $cargo = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
          if(isset($cargo[0]->id)){
            $permiso = Illuminate\Support\Facades\DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso[0]->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
            if(isset($permiso[0]->permiso)){
              if($permiso[0]->permiso == "Si"){ ?>
                <div class="col-8">
                   <select class="form-control" name="filtro" id="responsable"></select>  
                </div>
              <?php
              }
            }
          }          
        }
        ?>
          
        </div>
      </form>
      
    </div>
    <!--ERROR-->
       <div class="row">
        <div class="col-md-2 col-sm-12 cono padding-left-0">
          <div class="division-1" style="background-image: url('{{ url('/') }}/images/file/clientes/{{$usuario->foto_completa}}');">
          <div class="bandera-perfil"></div>
          </div>

        </div>
        <div class="col-md-2 col-sm-12 cono padding-left-0">
          <div class="division-1">
          <div class="bandera-nombre relativo" style="background-image: url('{{ url('/')}}/images/icons/{{$usuario->principal}}');"></div>
          <div class="segundo-cuadro">
            <h5 class="nombre">{{ucwords($usuario->nombres)}}</h5>
            <h5 class="nombre">{{ucwords($usuario->apellidos)}}</h5>
            <label class="cargo">{{ucwords($usuario->tipo)}}</label>
            <div>
              <label style="text-align: left;">Responsable en</label>
              @php
              $contador=0;
              $paises=App\Users_paise::where('user',$usuario->id)->get();
              @endphp
              @foreach($paises as $pais)
                @if($pais->user==$usuario->id)
                  @if($contador == 0)
                    <div class="relativo text-center">
                  @elseif(($contador%3)==0)
                    </div>
                    <div class="relativo text-center">
                  @endif
                <div class="bandera-area relativo" style="background-image: url('{{ url('/')}}/images/icons/{{$pais->pais}}');"><span class="nombre-bandera">{{$pais->nombre}}</span></div>
                <?php $contador++; ?>
                @endif
              @endforeach
              </div>
            </div>
            <div class="caja-media" style="width: 191px;">
              @php $list=explode(" ",$usuario->nombres); $conopor=0; $suma=0; $suma_total_o=0; $generali=0; @endphp
              @foreach($oportunidades as $oportunidad)
                @if($oportunidad->user_id==$usuario->id)
                  <?php
                      $hola = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'ciclo_venta' AND oportunidad_id = '".$oportunidad->id."' ORDER BY id DESC");
                      if (count($hola)>0) {
                        if($hola[0]->value!=0&&$hola[0]->value!=90&&$hola[0]->value!=100){
                          $valor_historico = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'valor_oportunidad' AND oportunidad_id = '".$oportunidad->id."'");
                          $conopor++;
                          $suma=$suma+str_replace(".",",",str_replace(",","",$valor_historico[0]->value));
                        }
                      }
                  ?>
                @endif
              @endforeach

               @foreach($oportunidades as $oportunidad)
                  <?php
                      $largo = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'ciclo_venta' AND oportunidad_id = '".$oportunidad->id."' ORDER BY id DESC");
                      if (count($largo)>0) {
                        if($largo[0]->value!=0&&$largo[0]->value!=90&&$largo[0]->value!=100){
                          $valor_historico = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'valor_oportunidad' AND oportunidad_id = '".$oportunidad->id."'");
                          $generali++; 
                          if(isset($valor_historico[0]->value)){
                            $suma_total_o=$suma_total_o+str_replace(".",",",str_replace(",","",$valor_historico[0]->value));
                          }else{
                            $suma_total_o=$suma_total_o+0;
                          }
                        }
                      }
                  ?>
              @endforeach
              <div class="titulo-caja-media">{{ucwords($list[0])}}</div>
              <span class="numero-caja-media">{{$total_mis_oportunidades['numero']}}</span>
              <span class="letra-caja-media">Oportunidades</span>
              <hr class="hr">
              <span class="numero-caja-media">${{number_format($total_mis_oportunidades['valor'],"0",",",".")}}</span>
              <span class="letra-caja-media">Millones</span>
            </div>
            <div class="caja-media" style="width: 191px;">
              <div class="titulo-caja-media-2">ESSI General</div>
              <span class="numero-caja-media">{{$o_essi['numero']}}</span>
              <span class="letra-caja-media">Oportunidades</span>
              <hr class="hr">
              <span class="numero-caja-media">${{number_format($o_essi['valor'],"0",",",".")}}</span>
              <span class="letra-caja-media">Millones</span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              
            </div>
          </div>
          </div>
        </div>
        <div class="col-md-8 col-sm-12">
        <ul class="nav nav-tabs" role="tablist">
          <li class="li_tabs tabs active" id="Mi_Gestion" onclick="tab_mostrar('oportunidad')" style="cursor: pointer;"><a role="tab" data-toggle="tab"><img src="{{url('/')}}/images/icons/gestion_icon.png"> Mi Gestión</a></li>
          <li class="li_tabs tabs" id="Mi_Metas" onclick="tab_mostrar('comercial')" style="cursor: pointer;"><a role="tab" data-toggle="tab"><img src="{{url('/')}}/images/icons/metas_icon.png"> Mis Metas</a></li>
          <li class="botones_estaticos" id="Dashboard_Principal" style="cursor: pointer;"><a onclick="ir('dashboard')"><img src="{{url('/')}}/images/icons/dashboard_icon.png"> Dashboard Principal</a></li>
          <li class="botones_estaticos" id="Mega_Archivo" style="cursor: pointer;"><a onclick="ir('megaarchivo')"><img src="{{url('/')}}/images/icons/megaarch_icon.png"> Mega Archivo</a></li>
        </ul>

            <!-- PANEL NUMERO UNO -->
            <div class="tab-content">
              <div class="tab-pane fade in active cotnainer" id="oportunidad">
                <div class="row caja-derecha">
                  <?php $ttalcerrado=0; $sumacerrado=0;?>
                  @foreach($oportunidades as $oportunidad)
                    @if($oportunidad->user_id==$usuario->id)
                      <?php
                          $cerrados = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'ciclo_venta' AND `value`='90' AND oportunidad_id = '".$oportunidad->id."' ORDER BY id DESC");
                          if (count($cerrados)>0) {
                              $valor_historico = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'valor_oportunidad' AND oportunidad_id = '".$oportunidad->id."'");
                              $ttalcerrado++;
                              $sumacerrado=$sumacerrado+str_replace(".",",",str_replace(",","",$valor_historico[0]->value));
                          }
                      ?>
                    @endif
                  @endforeach
                  <div class="col-md-3 caja-tab padding-0">
                    <div class="titulo-verde-celeste">Negocios cerrados</div>
                    <span class="cuerpo-verde-celeste">{{$oportunidad_cerrada['numero']}}</span>
                    <hr class="hr">
                    <span class="cuerpo-verde-celeste">$ {{number_format($oportunidad_cerrada['valor'],"0",",",".")}} M</span>
                  </div>
                  <?php $promediomes=0; $timecierre=0; ?>
                  @foreach($oportunidades as $oportunidad)
                    @if($oportunidad->user_id==$usuario->id)
                      <?php
                          $cerrados = \Illuminate\Support\Facades\DB::select("SELECT * FROM historial_oportunidades WHERE `key` = 'ciclo_venta' AND `value`='90' AND oportunidad_id = '".$oportunidad->id."' ORDER BY id DESC");
                          if (count($cerrados)>0) {
                            
                              $resta= strtotime($cerrados[0]->created_at)-strtotime($oportunidad->created_at);
                              $fin =round($resta/(24*60*60*30));
                              $promediomes++;
                              $timecierre=$timecierre+$fin;
                          }
                      ?>
                    @endif
                  @endforeach
                  <div class="col-md-3 caja-tab padding-0 cerrados">
                      <div class="titulo-verde-celeste">Promedio de cierre</div>
                      <span class="numero-verde-celeste"><?php echo $promedioindividual ?></span>
                      <span class="subtitulo-verde-celeste">Meses</span>
                  </div>  
                  <div class="col-md-2 caja-tab padding-0 cerrados">
                      <div class="negocios-perdidos">Negocios Perdidos</div>
                      <span class="negocios-perdidos-numero">{{$total_porc_perdida}}%</span>
                  </div>
                  <div class="col-md-1 caja-tab padding-0 cerrados icono">
                    <img src="{{url('/')}}/images/icons/costo_icon.png" class="icono-lateral">
                  </div>   
                  <div class="col-md-3 padding-0">
                    <div class="col-md-12 caja-tab-2 numero-naranja">
                      <span class="numero-naranja-grande" style="font-size: 50px">{{$gasto_x_usuario_porcentaje}}%</span><br>
                      <span class="numero-naranja-letra">Costo de Venta</span>
                    </div>
                  </div>
              </div>
              <div class="row caja-derecha margen-doble-superior">
                <div class="col-md-1 caja-tab padding-0 cerrados icono-agenda-semanal">
                  <img src="{{url('/')}}/images/icons/agendasema_icon.png" class="icono-lateral">
                </div>   
                <div class="col-md-7 padding-0 padding-derecho-20">
                  <div class="col-md-12 caja-tab-2">
                  <?php

                  $fecha=date('Y-m-d') ; // fecha.

                  #separas la fecha en subcadenas y asignarlas a variables
                  #relacionadas en contenido, por ejemplo dia, mes y anio.

                  $diaa   = substr($fecha,8,2);
                  $mes = substr($fecha,5,2);
                  $anio = substr($fecha,0,4);

                  $dia = date('w',  mktime(0,0,0,$mes,$diaa,$anio));
                  //donde:
                          
                  #W (mayúscula) te devuelve el número de semana
                  #w (minúscula) te devuelve el número de día dentro de la semana (0=domingo, #6=sabado)

                  if((int)$dia==0){
                    $semana=date('W',  mktime(0,0,0,$mes,$diaa,$anio))+1;
                  }else{
                    $semana=date('W',  mktime(0,0,0,$mes,$diaa,$anio));
                  }                  
                  if((int)$dia<=1){
                    $semana_cerrada=$semana-1;
                    $semana_abierta=$semana;
                  }else{
                    $semana_cerrada=$semana;
                    $semana_abierta=$semana+1;
                  }
                  ?>
                  @php $sm=0; $ss=0; @endphp
                  @foreach($planes as $value)
                    @if($value->semana==$semana_cerrada)
                      @foreach($plan_descripciones as $key)
                        @if($key->plan_trabajo==$value->id)
                        @php 
                          $sm++;
                        @endphp
                        @endif
                      @endforeach
                    @endif

                    @if($value->semana==$semana_abierta)
                      @foreach($plan_descripciones as $key)
                        @if($key->plan_trabajo==$value->id)
                        @php 
                          $ss++;
                        @endphp
                        @endif
                      @endforeach
                    @endif
                  @endforeach

                    <div class="titulo-agenda-semanal">Agenda Semanal</div>
                      <div class="semana-anterior">
                        <div class="titulo-semana">Semana {{$semana_cerrada}}</div>

                        <div class="cantidad-semana">{{$sm}} Actividades <span>Cerrada</span></div>
                        <div class="cuadro-menor"><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/plantrabajo/calendario" @endif target="_blank" style="cursor: pointer;"><img src="{{url('/')}}/images/icons/view_icon.png"></a></div>
                      </div>
                      <div class="semana-actual">
                        <div class="titulo-semana">Semana {{$semana_abierta}}</div>
                        <div class="cantidad-semana">{{$ss}} Actividades<span>Abierto</span></div>
                        <div class="cuadro-menor"><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/plantrabajo/calendario" @endif target="_blank" style="cursor: pointer;"><img src="{{url('/')}}/images/icons/edit_icon.png"></a></div>
                      </div>
                  </div>
                </div>
                <div class="col-md-1 caja-tab padding-0 cerrados icono-presupuesto-mensual">
                  <img src="{{url('/')}}/images/icons/presupuestoMen_icon.png" class="icono-lateral">
                </div>   
                <div class="col-md-3 padding-0">
                  <div class="col-md-12 caja-tab-2">
                    <div class="titulo-agenda-mensual">Presupuesto Mensual</div>
                    @php 
                    $total_mes=0; 
                      $fecha_actual=strtotime(date("Y-m-")."01");
                      $mes_siguiente=strtotime("+1 Month",$fecha_actual);
                      $mes_siguiente=date("Y-m-d",$mes_siguiente);
                      list($anio_siguiente,$mes_sgt,$dia_siguiente)=explode("-",$mes_siguiente);
                    @endphp
                    @foreach($presupuestos as $index)
                      @if($index->user_id==$usuario->id&&(int)$mes_sgt==$index->mes&&$index->anio==$anio_siguiente)
                        @php 
                        $total_mes=$total_mes+str_replace(",","",$index->alimentacion)+str_replace(",","",$index->transporte_interno)+str_replace(",","",$index->transporte_intermunicipal)+str_replace(",","",$index->tiquete_aereo)+str_replace(",","",$index->papeleria)+str_replace(",","",$index->invitacion_cliente)+str_replace(",","",$index->alquiler_vehiculo)+str_replace(",","",$index->gasolina_pasaje)+str_replace(",","",$index->hotel)+str_replace(",","",$index->otros);
                        @endphp
                      @endif
                    @endforeach
                    <div class="titulo-agenda-mensual-2">{{$p_m['mes']}}</div>
                    <div class="titulo-agenda-mensual-3">${{number_format($p_m['valor'],"1",".",",")}} M<span>Abierto</span></div>
                  </div>
                  <div class="cuadro-menor-2" style="cursor: pointer;"><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/presupuesto" @endif target="_blank"><img src="{{url('/')}}/images/icons/edit_icon.png"></a></div>
                </div>
              </div>
              <div class="row caja-derecha margen-doble-superior">
                <div class="col-md-1 caja-tab padding-0 cerrados icono-agenda-bitacora">
                  <img src="{{url('/')}}/images/icons/bitacora_icon.png" class="icono-lateral">
                </div>   
                <div class="col-md-11 padding-0 padding-derecho-20">
                  <div class="col-md-12 caja-tab-2">
                    <div class="titulo-agenda-semanal">Bitacora</div>
                    <div class="dias-bitacora">
                      @php
                      $fecha_actual=strtotime(date("Y-m-d"));
                      $tresdias=strtotime("-3 Days",$fecha_actual);
                      $tresdias=date("Y-m-d",$tresdias);
                      $dosdias=strtotime("-2 Days",$fecha_actual);
                      $dosdias=date("Y-m-d",$dosdias);
                      $undias=strtotime("-1 Days",$fecha_actual);
                      $undias=date("Y-m-d",$undias);
                      list($anio_,$mes_,$dia_)=explode("-",$tresdias);
                      list($anio2_,$mes2_,$dia2_)=explode("-",$dosdias);
                      list($anio3_,$mes3_,$dia3_)=explode("-",$undias);
                      @endphp
                      <div class="bitacora-dia">{{$dia_." ".$meses[(int)$mes_-1]}}</div>
                      <div class="bitacora-dia">{{$dia2_." ".$meses[(int)$mes2_-1]}}</div>
                      <div class="bitacora-dia">{{$dia3_." ".$meses[(int)$mes3_-1]}}</div>
                      <div class="bitacora-dia">Hoy</div>
                    </div>
                    @php $m=0; $n=0; $o=0;$p=0; @endphp
                     @foreach($plan_descripciones as $key)
                      @if($key->user_id==$usuario->id&&$key->fecha==$tresdias)
                       @php $m++; @endphp
                      @endif
                    @endforeach
                     @foreach($plan_descripciones as $key)
                      @if($key->user_id==$usuario->id&&$key->fecha==$dosdias)
                       @php $n++; @endphp
                      @endif
                    @endforeach
                     @foreach($plan_descripciones as $key)
                      @if($key->user_id==$usuario->id&&$key->fecha==$undias)
                       @php $o++; @endphp
                      @endif
                    @endforeach
                     @foreach($plan_descripciones as $key)
                      @if($key->user_id==$usuario->id&&$key->fecha==date("Y-m-d"))
                       @php $p++; @endphp
                      @endif
                    @endforeach
                    <div class="contenedor-bitacora">
                      <div class="contenido-contenedor relativo">
                        Actividades<span>{{$actividades_registradas['hace3dias']}}</span>
                        <div><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/bitacora/calendario" @endif target="_blank"><img src="{{url('/')}}/images/icons/view_icon.png"></a></div>
                      </div>
                      <div class="contenido-contenedor">
                        Actividades<span>{{$actividades_registradas['antiayer']}}</span>
                        <div><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/bitacora/calendario" @endif target="_blank"><img src="{{url('/')}}/images/icons/view_icon.png"></a></div>
                      </div>
                      <div class="contenido-contenedor">
                        Actividades<span>{{$actividades_registradas['ayer']}}</span>
                        <div><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/bitacora/calendario" @endif target="_blank"><img src="{{url('/')}}/images/icons/view_icon.png"></a></div>
                      </div>
                      <div class="contenido-contenedor">
                        Actividades<span>{{$actividades_registradas['hoy']}}</span>
                        <div><a @if($data['permiso_hipervinculo'] == "Si") href="{{url('/')}}/bitacora/calendario" @endif target="_blank"><img src="{{url('/')}}/images/icons/view_icon.png"></a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              <!-- PANEL DOS -->
              <?php
              $meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
              ?>
              <div class="tab-pane fade" id="comercial">
                <div class="row">
                    <div class="col-md-12 contedor-linea">
                    <h6>Meta de Ventas {{date("Y")}}</h6>
                      <div id="lineas-negras"></div>
                    </div>
                </div>
                <?php try { ?>
                  
   <!----###################################################################################################################################################--->
                  <?php
                   $mes = intval(date("m")) - 2;
                           if($mes == -1){
                               $mes = 11;
                           }
                   ?>
                <div class="row">
                    <div class="col-md-3 mt-2">
                        <div class="row bg-white mr-4 h-100 shadow-2">
                            <div class="col-md-12 cumpli-presu-child">
                                <h5>Cumplimiento Presupuesto</h5>
                            </div>
                            <div class="col-md-12 cumpli-presu-child">
                                <h1 class="w-100"><?=$data['cumplimiento_presupuesto_mes_anterior']?>% <br><small class="text-muted font-weight-normal h5" style="text-transform: none !important;"><?=$meses[$mes]?></small></h1>
                            </div>
                            <div class="col-md-12"><hr style="width:120px"></div>
                            <div class="col-md-12 cumpli-presu-child">
                                <h1 class="w-100"><?=$data['cumplimiento_presupuesto_mes_actual']?>% <br><small class="text-muted font-weight-normal h5" style="text-transform: none !important;">Parcial <?=$meses[(intval(date("m")) - 1)]?></small></h1>
                            </div>
                        </div>
                    </div>
                    <style>
                        .dark-blue-1{ color: #001862; }
                        .dark-blue-2{ background-color: #001862; color: #fff; font-size: 20px;}
                        .border-r{border-right: 1px #c7c5c5 solid;}
                        .border-t{border-top: 1px #c7c5c5 solid;}
                        .h-100{ height: 100%; }
                        .cursor{cursor: pointer;}
                    </style>
                    <div class="col-md-9 bg-white mt-2 shadow-2">
                        <div class="row bg-info">
                            <div class="col-md-12">
                                <h4 class="text-white">Gastos de Gestiones {{date('Y')}}</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-primary">Oportunidad</h5>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <h4 class="mb-3 dark-blue-1">$<?php if(isset($data['sumatoria_oportrunidad'][0]->Total)){ echo number_format($data['sumatoria_oportrunidad'][0]->Total, 0, ',', '.'); } ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-primary">Empresa</h5>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <h4 class="mb-3 dark-blue-1">$<?=number_format($data['sumatoria_empresa'][0]->Total, 0, ',', '.')?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-primary">Gestión Comercial</h5>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <h4 class="mb-3 dark-blue-1">$<?=number_format($data['sumatoria_gestion_comercial'][0]->Total, 0, ',', '.')?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-primary">Gestión Interna</h5>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <h4 class="mb-3 dark-blue-1">$<?=number_format($data['sumatoria_gestion_interna'][0]->Total, 0, ',', '.')?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="gastos_oportunidades">
                            <?=$data['Gastos_oportunidades']?>
                        </div>
                    </div>
                </div>

    <!----###################################################################################################################################################--->
                <?php } catch (Exception $e) { } ?>
              </div>
        </div>
        </div>
    </div>
    <!--FIN ERROR-->

    <!-- INICIO DE MODALES PAGINA UNO -->
    <div class="modal fade" id="negocios_cerrados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
            <h4 class="modal-title" id="myModalLabel">Negocios Cerrados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row margen-inferior">
              <div class="col-md-7 borde-derecho">
                <h6 id="h6"><span class="blue">Monto</span> de Negocios Cerrados</h6>
                <span id="millones">(Millones)</span>
                <div id="lista-comercial"></div>
              </div>
              <div class="col-md-5">
               <div class="col-md-12 centrar-contenido borde-inferior">
               <h6 id="h6"><span class="blue">Cantidad</span> de Negocios Cerrados</h6>
                 <div id="barra-horizontal"></div>
               </div>
               <div class="col-md-12 centrar-contenido">
                 <h6 id="h6"><span class="blue">Promedio</span> Cierre Negocios</h6>
                 <span id="millones">(Meses)</span>
                 <div id="pie"></div>
               </div>
               </div> 
              </div>
            </div>
          </div>
        </div>
      </div>

  <div class="modal fade" id="oportunidad_perdida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Oportunidades Perdidas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row margen-inferior">
            <div class="col-md-4 borde-derecho">
              <h6 id="h6"><span class="blue">Monto</span> de Negocios Perdidos</h6>
              <span id="millones">(Millones)</span>
              <div id="negocio_perdido"></div>
            </div> 
            <div class="col-md-4 borde-derecho">
              <h6 id="h6"><span class="blue">Oportunidades</span> Perdidas</h6>
              <div id="oportunidad_perdida">
              <div id="contenedor-perdida">
                  <div class="lista-caja borde-inferior">
                  <div class="row">
                    <div class="col-md-4">
                          <p class="collections-title">Mayra</p>
                      </div>
                      <div class="col-md-2">
                          <span class="task-cat pink accent-2">86%</span>
                      </div>
                      <div class="col-md-6">
                          <div class="progress">
                               <div class="determinate" style="width: 86%"></div>   
                          </div>                                                
                      </div>
                  </div>
                  </div>
                  <div class="lista-caja borde-inferior">
                  <div class="row">
                    <div class="col-md-4">
                          <p class="collections-title">Silvia</p>
                      </div>
                      <div class="col-md-2">
                          <span class="task-cat pink accent-2">35%</span>
                      </div>
                      <div class="col-md-6">
                          <div class="progress">
                               <div class="determinate" style="width: 35%"></div>   
                          </div>                                                
                      </div>
                  </div>
                  </div>

                  <div class="lista-caja borde-inferior">
                  <div class="row">
                    <div class="col-md-4">
                          <p class="collections-title">Oscar</p>
                      </div>
                      <div class="col-md-2">
                          <span class="task-cat pink accent-2">50%</span>
                      </div>
                      <div class="col-md-6">
                          <div class="progress">
                               <div class="determinate" style="width: 50%"></div>   
                          </div>                                                
                      </div>
                  </div>
                  </div>
                  <div class="lista-caja borde-inferior">
                  <div class="row">
                    <div class="col-md-4">
                          <p class="collections-title">Diego</p>
                      </div>
                      <div class="col-md-2">
                          <span class="task-cat pink accent-2">15%</span>
                      </div>
                      <div class="col-md-6">
                          <div class="progress">
                               <div class="determinate" style="width: 15%"></div>   
                          </div>                                                
                      </div>
                  </div>
                  </div>
                  </div>
              </div>
            </div>
            <div class="col-md-4 centrar-contenido">
              <h6 id="h6"><span class="blue">Cantidad</span> de Negocios Perdidos</h6>
              <div id="cantidad-perdida"></div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<div class="modal fade" id="oportunidad_identificada" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Oportunidades Identificadas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row margen-inferior">
            <div class="col-md-12">
              <div class="titulo-graficas">
                <div class="linea-titulo-grafica"><span class="blue">Corto</span> Plazo</div>
                <div class="linea-titulo-grafica"><span class="blue">Mediano</span> Plazo</div>
                <div class="linea-titulo-grafica"><span class="blue">Largo</span> Plazo</div>
              </div>
            </div>
            <div class="col-md-12 margen-contenedor">
              <div class="borde-inferior-2">
              <div class="divimagen" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar">Mayra</span></div>
              <div class="cuadro">
                <div class="cuadro-color-azul"></div><div class="titulo-color">Alta</div>
                <div class="cuadro-color-verde"></div><div class="titulo-color">Media</div>
                <div class="cuadro-color-gris"></div><div class="titulo-color">Baja</div>
              </div>
              <div class="graficas">
                <div class="graficas-alta" id="graficas-alta1"></div>
                <div class="graficas-mediano" id="graficas-mediano1"></div>
                <div class="graficas-corto" id="graficas-corto1"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="row margen-inferior">
            <div class="col-md-12 margen-contenedor">
              <div class="borde-inferior-2">
              <div class="divimagen" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar">Oscar</span></div>
              <div class="cuadro">
                <div class="cuadro-color-azul"></div><div class="titulo-color">Alta</div>
                <div class="cuadro-color-verde"></div><div class="titulo-color">Media</div>
                <div class="cuadro-color-gris"></div><div class="titulo-color">Baja</div>
              </div>
              <div class="graficas">
                <div class="graficas-alta" id="graficas-alta2"></div>
                <div class="graficas-mediano" id="graficas-mediano2"></div>
                <div class="graficas-corto" id="graficas-corto2"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="row margen-inferior">
            <div class="col-md-12 margen-contenedor">
              <div class="borde-inferior-2">
              <div class="divimagen" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar">Silvia</span></div>
              <div class="cuadro">
                <div class="cuadro-color-azul"></div><div class="titulo-color">Alta</div>
                <div class="cuadro-color-verde"></div><div class="titulo-color">Media</div>
                <div class="cuadro-color-gris"></div><div class="titulo-color">Baja</div>
              </div>
              <div class="graficas">
                <div class="graficas-alta" id="graficas-alta3"></div>
                <div class="graficas-mediano" id="graficas-mediano3"></div>
                <div class="graficas-corto" id="graficas-corto3"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="row margen-inferior">
            <div class="col-md-12 margen-contenedor">
              <div class="borde-inferior-2">
              <div class="divimagen" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar">Diego</span></div>
              <div class="cuadro">
                <div class="cuadro-color-azul"></div><div class="titulo-color">Alta</div>
                <div class="cuadro-color-verde"></div><div class="titulo-color">Media</div>
                <div class="cuadro-color-gris"></div><div class="titulo-color">Baja</div>
              </div>
              <div class="graficas">
                <div class="graficas-alta" id="graficas-alta4"></div>
                <div class="graficas-mediano" id="graficas-mediano4"></div>
                <div class="graficas-corto" id="graficas-corto4"></div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ciclo_venta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Ciclos de Ventas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body tabla-padding">
          <table class="tabla">
            <tr>
              <td colspan="1">
                
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/perdida.png">
                <span class="linea4-item">0%</span>
                <span class="linea5-item">Perdida</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/eliminada.png">
                <span class="linea4-item">0%</span>
                <span class="linea5-item">Eliminada</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/deteccion.png">
                <span class="linea4-item">10%</span>
                <span class="linea5-item">Detecciòn</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/alcances.png">
                <span class="linea4-item">20%</span>
                <span class="linea5-item">Alcances</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/propuesta.png">
                <span class="linea4-item">40%</span>
                <span class="linea5-item">Propuesta</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/negociacion.png">
                <span class="linea4-item">60%</span>
                <span class="linea5-item">Negociaciòn</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/gestionOC.png">
                <span class="linea4-item">80%</span>
                <span class="linea5-item">Gestiòn OC</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/vendida.png">
                <span class="linea4-item">90%</span>
                <span class="linea5-item">Vendida</span>
              </td>
              <td class="borde-azul">
                <img src="{{url('/')}}/images/icons/facturada.png">
                <span class="linea4-item">100%</span>
                <span class="linea5-item">facturada</span>
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar-2">Mayra</span></div>
              </td>
              <td class="celda-celeste">
                <label class="linea3-item2">23%</label>
                <label class="linea2-item"><span class="blue">1.400</span> Millones</label>
                <label class="linea1-item"><span class="blue">20</span> Oportunidades</label>
              </td>
              <td>
                <label class="linea3-item2">23%</label>
                <label class="linea2-item"><span class="blue">1.400</span> Millones</label>
                <label class="linea1-item"><span class="blue">20</span> Oportunidades</label>
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                <label class="linea3-item2">23%</label>
                <label class="linea2-item"><span class="blue">1.400</span> Millones</label>
                <label class="linea1-item"><span class="blue">20</span> Oportunidades</label>
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Oscar</span></div>
              </td>
              <td class="celda-azul">
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                <label class="linea3-item2">23%</label>
                <label class="linea2-item"><span class="blue">1.400</span> Millones</label>
                <label class="linea1-item"><span class="blue">20</span> Oportunidades</label>
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/D02.png');"><span class="blue nombre-avatar-2">Silvia</span></div>
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td>
                
              </td>
              <td class="celda-celeste">
                
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/E01.png');"><span class="blue nombre-avatar-2">Diego</span></div>
              </td>
              <td class="celda-azul">
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                
              </td>
              <td class="celda-celeste">
                <label class="linea3-item2">23%</label>
                <label class="linea2-item"><span class="blue">1.400</span> Millones</label>
                <label class="linea1-item"><span class="blue">20</span> Oportunidades</label>
              </td>
              <td class="celda-azul">
                <label class="linea3-item2">23%</label>
                <label class="linea2-item"><span class="blue">1.400</span> Millones</label>
                <label class="linea1-item"><span class="blue">20</span> Oportunidades</label>
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                
              </td>
              <td class="celda-celeste">
                
              </td>
              <td class="celda-azul">
                
              </td>
            </tr>

          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="empresas_clientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Empresas & Clientes</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <table class="width-100">
                <tr>
                  <td></td>
                  <td colspan="2" class="cabecera-tabla numero-titulo">127<span class="letra-titulo">Empresas identificadas</span></td>
                  <td colspan="2" class="cabecera-tabla numero-titulo">431<span class="letra-titulo">Clientes registrados</span></td>
                </tr>
                <tr >
                  <td></td>
                  <td colspan="1" class="subcabecera-tabla">Total</td>
                  <td colspan="1" class="subcabecera-tabla">Nuevos</td>
                  <td colspan="1" class="subcabecera-tabla">Total</td>
                  <td colspan="1" class="subcabecera-tabla">Nuevos</td>
                </tr>
                <tr>
                  <td colspan="1" class="td-85">
                    <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Mayra</span>
                    <img src="{{url('/')}}/images/icons/argentina_flag.png" class="bandera"></div>
                  </td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                </tr>
                <tr>
                  <td colspan="1" class="td-85">
                    <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Oscar</span>
                    <img src="{{url('/')}}/images/icons/mexico_flag.png" class="bandera"></div>
                  </td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                </tr>
                <tr>
                  <td colspan="1" class="td-85">
                    <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Silvia</span>
                    <img src="{{url('/')}}/images/icons/ecuador_flag.png" class="bandera"></div>
                  </td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                </tr>
                <tr>
                  <td colspan="1" class="td-85">
                    <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Diego</span>
                    <img src="{{url('/')}}/images/icons/colombia_flag.png" class="bandera"></div>
                  </td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">25</td>
                  <td class="borde-inferior-2 centrar-contenido blue numero-tabla">7</td>
                </tr>
              </table>
            </div>
            <div class="col-md-6">
            <h6 class="h6"><span class="blue">% de Empresas</span> por cada Comercial</h6>
              <div id="dona-2">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="cono" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Cono de Oportunidades</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12 derecha">
                <div class="width-30 centrar-contenido"><span class="blue">Alta</span>
                </div>
                <div class="width-30 centrar-contenido"><span class="blue">Media</span>
                </div>
                <div class="width-30 centrar-contenido"><span class="blue">Baja</span>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="width-10 height-200"><label class="absoluto bottom-0"><span class="blue">Corto</span> Plazo</label></div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-1"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-2"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-3"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="width-10 height-200"><label class="absoluto bottom-0"><span class="blue">Mediano</span> Plazo</label></div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-4"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-5"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-6"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="width-10 height-200"><label class="absoluto bottom-0"><span class="blue">Largo</span> Plazo</label></div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-7"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-8"></div>
              </div>
              <div class="width-30 relativo heigth-250 borde-inferior-2">
                <div class="absoluto height-100" id="meter-9"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="negocios_cerrados2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Negocios Cerrados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
         <div class="row">
        <div class="col-md-4 centrar-contenido">
          <label>%<span class="blue bloque">por Comercial</span></label>
        </div>
        <div class="col-md-4 centrar-contenido">
          <label>%<span class="blue bloque">Alta/Media/Baja</span></label>
        </div>
        <div class="col-md-4 centrar-contenido">
          <label>%<span class="blue bloque">Corto Plazo/Mediano/Largo</span></label>
        </div>
        </div>
        <div class="row">
        <div class="col-md-4">
          <div class="dona" id="dona-3"></div>
        </div>
        <div class="col-md-4">
          <div class="dona" id="dona-4"></div>
        </div>
        <div class="col-md-4">
          <div class="dona" id="dona-5"></div>
        </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- INICIO DE MODALES PAGINA DOS -->

   <div class="modal fade" id="gasto_ventas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Gasto de Ventas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
         <div class="row">
          <div class="col-md-3">
          <div class="caja-avatar">
            <div class="divimagen-2 centrar-contenido" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');">
            <img src="{{url('/')}}/images/icons/mexico_flag.png" class="bandera"></div>
          </div>            
            <div class="card">
                <div class="card-content blue-sky white-text">
                    <h4 class="titulo-card">Mayra</h4>
                    <p class="card-stats-title"><span class="texto-sombra">12.1 %</span> Gasto de Venta</p>
                    <h4 class="card-stats-number">Gastos Totales <span>$ 4.342.445</span></h4>
                </div>
                <div class="card-action1 blue-sky darken-2">
                    <div class="sales-compositebar1 center-align"></div>
                </div>
            </div>
          </div>
          <div class="col-md-3">
          <div class="caja-avatar">
            <div class="divimagen-2 centrar-contenido" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');">
            <img src="{{url('/')}}/images/icons/colombia_flag.png" class="bandera"></div>
          </div>            
            <div class="card">
                <div class="card-content blue-king white-text">
                    <h4 class="titulo-card">Oscar</h4>
                    <p class="card-stats-title"><span class="texto-sombra">8.5 %</span> Gasto de Venta</p>
                    <h4 class="card-stats-number">Gastos Totales <span>$ 23.127.698</span></h4>
                </div>
                <div class="card-action1 blue-king darken-2">
                    <div class="sales-compositebar1 center-align"></div>
                </div>
            </div>
          </div>
          <div class="col-md-3">
          <div class="caja-avatar">
            <div class="divimagen-2 centrar-contenido" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');">
            <img src="{{url('/')}}/images/icons/argentina_flag.png" class="bandera"></div>
          </div>            
            <div class="card">
                <div class="card-content blue-sky white-text">
                    <h4 class="titulo-card">Silvia</h4>
                    <p class="card-stats-title"><span class="texto-sombra">16.3 %</span> Gasto de Venta</p>
                    <h4 class="card-stats-number">Gastos Totales <span>$ 99.548.212</span></h4>
                </div>
                <div class="card-action1 blue-sky darken-2">
                    <div class="sales-compositebar1 center-align"></div>
                </div>
            </div>
          </div>
          <div class="col-md-3">
          <div class="caja-avatar">
            <div class="divimagen-2 centrar-contenido" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');">
            <img src="{{url('/')}}/images/icons/colombia_flag.png" class="bandera"></div>
          </div>            
            <div class="card">
                <div class="card-content blue-king white-text">
                    <h4 class="titulo-card">Diego</h4>
                    <p class="card-stats-title"><span class="texto-sombra">2.4 %</span> Gasto de Venta</p>
                    <h4 class="card-stats-number">Gastos Totales <span>$ 15.234.254</span></h4>
                </div>
                <div class="card-action1 blue-king darken-2">
                    <div class="sales-compositebar1 center-align"></div>
                </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="cumplimiento_presupuestal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Gasto de Ventas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <table class="tabla-2">
            <tr>
              <td colspan="2">
                
              </td>
              <td class="cabecera-marron-1">
                <span class="titulo-marron-1">Ejecución Presupuesto Ultimo Mes</span>
              </td>
              <td class="cabecera-marron-2">
                <span class="titulo-marron-1">Ejecución Presupuesto Acumulado</span>
              </td>
              <td class="cabecera-marron-3">
                <span class="titulo-marron-1">Presupuesto Mensual Ejecutado</span>
              </td>
              <td class="cabecera-marron-4">
                <span class="titulo-marron-1">Presupuesto Anual Ejecutado</span>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar-2">Mayra</span></div>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">12 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">74 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">37 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">12 %</label>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Oscar</span></div>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">33 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">90 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">74 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">74 %</label>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/D02.png');"><span class="blue nombre-avatar-2">Silvia</span></div>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">22 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">45 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">23 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">88 %</label>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/E01.png');"><span class="blue nombre-avatar-2">Diego</span></div>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">12 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">54 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">12 %</label>
              </td>
              <td class="td-marron">
                <label class="linea3-item2">74 %</label>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="uso_tiempo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Uso del Tiempo</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <table class="tabla-3">
            <tr>
              <td colspan="1">
                
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/perdida.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Apertura de mercado</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/eliminada.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Identificación de oportunidades en clientes</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/deteccion.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Gestión de aprobación en ofertas</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/alcances.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Facturación</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/propuesta.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Ventas</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/negociacion.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>No aplica al cargo</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/gestionOC.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Planeación</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/vendida.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Mejoramiento continuo</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/facturada.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Soporte tecnico cliente</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/deteccion.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Desarrollo proveedores</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/alcances.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Estimación de inversiones y propuestas</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/propuesta.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Realización de dosier</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/negociacion.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Tramites de legalización y soliciud de viaticos</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/gestionOC.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Tramites administrativos internos ESSI</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/vendida.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Control y auditoria interna proyectos</span></div>
              </td>
              <td class="relativo tamano-110">
                <img src="{{url('/')}}/images/icons/facturada.png" class="img-caja" style="width: 35px;">
                <div class="caja-negra"><span>Soporte tecnico a otro funcionario ESSI</span></div>
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/C02.png');"><span class="blue nombre-avatar-2">Mayra</span></div>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho sostenido">
                <label class="blue linea3-item3 ">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho sostenido">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho sostenido">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td>
                <label class="blue linea3-item3">12 %</label>
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/A04.png');"><span class="blue nombre-avatar-2">Oscar</span></div>
              </td>
              <td class="borde-derecho fondo-gris sostenido">
                <label class="blue linea3-item3">12 %</label>
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris sostenido">
                <label class="blue linea3-item3 ">12 %</label>
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="fondo-gris">
                
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/D02.png');"><span class="blue nombre-avatar-2">Silvia</span></div>
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td class="borde-derecho">
                
              </td>
              <td>
                
              </td>
            </tr>
            <tr>
              <td colspan="1" class="td-85">
                <div class="divimagen-2" style="background-image: url('https://www.amcharts.com/lib/images/faces/E01.png');"><span class="blue nombre-avatar-2">Diego</span></div>
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="borde-derecho fondo-gris">
                
              </td>
              <td class="fondo-gris">
                
              </td>
            </tr>

          </table>
        </div>
      </div>
    </div>

  </div>

  <div class="modal fade" id="actividades_pendientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Gestión de Actividades Pendientes</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-12 derecha">
              <div class="width-100 centrar-contenido"><span class="blue">Mes/Semana</span>
              </div>
            </div>
        </div>
        <div class="row">
          <dv class="col-md-12">
          <div id="linea-pendiente">
            
          </div>
          </dv>
        </div>
        <div class="row">
            <div class="col-md-6 derecha">
              <div class="width-100 centrar-contenido">% <span class="blue">Hechas a Tiempo</span>
              </div>
            </div>
            <div class="col-md-6 derecha">
              <div class="width-100 centrar-contenido"><span class="blue">Propuestas</span> por cada comercial
              </div>
            </div>
        </div>
        <div class="row">
        <dv class="col-md-6">
          <div id="radar-pendiente">
            
          </div>
        </dv>
        <dv class="col-md-6">
          <div id="radar2-pendiente">
            
          </div>
        </dv>
        </div>
        </div>
      </div>
    </div>

  </div>

  <div class="modal fade" id="cumplimiento_llenado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi">
          <h4 class="modal-title" id="myModalLabel">Cumplimiento Llenado</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12 derecha">
                <div class="width-30 centrar-contenido">% <span class="blue">Llenado de bitacora</span>
                </div>
                <div class="width-30 centrar-contenido">% <span class="blue">Llenado semanal</span>
                </div>
                <div class="width-30 centrar-contenido">% <span class="blue">Presupuesto Mensual</span>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="dona" id="gira-1"></div>
            </div>
            <div class="col-md-4">
              <div class="dona" id="gira-2"></div>
            </div>
            <div class="col-md-4">
              <div class="dona" id="gira-3"></div>
            </div>
          </div>
        </div>
        </div>
      </div>


  </div>

  </div>
@endsection

@section('scripts')

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/funnel.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/gauge.js"></script>
<script src="https://www.amcharts.com/lib/3/radar.js"></script>
<script src="https://www.amcharts.com/lib/3/xy.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/black.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/morris.min.js"></script>
<!--<script type="text/javascript" src="{{ url('/')}}/js/material/morris-script.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/sparkline-script.js"></script>
<!-- FUNCIONES NUEVAS -->
<?php 

function burbuja($array)
{
    for($i=1;$i<count($array);$i++)
    {
        for($j=0;$j<count($array)-$i;$j++)
        {
            if($array[$j]["total"]<$array[$j+1]["total"])
            {
                $k=$array[$j+1];
                $array[$j+1]=$array[$j];
                $array[$j]=$k;
            }
        }
    }
 
    return $array;
}
?>
<script type="text/javascript">

$('body').on('click','.li_tabs',function(){
    $('.li_tabs').removeClass('active');
    $(this).addClass('active');
});
function tab_mostrar(id){
  $('.tab-pane').removeClass('show').removeClass('in').removeClass('active');
  $('#'+id).addClass('in').addClass('active');
}

$(function (){
  $('#responsable').select2({
    placeholder: "Seleccione un usuario",
    ciudad: true,
    tokenSeparators: [','],
    ajax: {
      dataType: 'json',
      url: '{{ url("usuariosdashboardcomercialpermisos") }}',
      delay: 250,
      data: function(params) {
          return {
              term: params.term
          }
      },
      processResults: function(data, page) {
        return {
          results: data
        };
      },
    }
  });

  $( "#responsable" ).change(function () {
    $( "#formFilter" ).submit();  
  });
});
function ir(link){
  if(link=='megaarchivo'){
    direc="http://megaarchivo.smartessi.com";
    window.open(direc, "_blank");
  }else{
    location.href="{{url('/')}}/"+link;
  }
  
}
var chart = AmCharts.makeChart( "barras-panel2", {
  "type": "serial",
  "theme": "light",
  "dataProvider": [ 
  <?php 

  $i=0; $array=[]; $mis_oportunidades=DB::select('SELECT DISTINCT oportunidad_id FROM historial_oportunidades WHERE value = '.$id_funcionario.' AND `key` = "responsable" AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "0") AND `oportunidad_id` NOT IN ( SELECT oportunidad_id FROM historial_oportunidades WHERE `key` = "ciclo_venta" AND `value` = "90")');

        foreach ($mis_oportunidades as $m_o) {
          $oportunidad_buscar=DB::select('SELECT * FROM historial_oportunidades WHERE oportunidad_id = '.$m_o->oportunidad_id.' AND `key` = "responsable" ORDER BY `id` DESC');
          $totalg=0; 
          $validacion=false;
          if(($oportunidad_buscar[0]->value)==$id_funcionario){
            
            $validacion=true;

            foreach ($actividades as $act) {
              if($act->oportunidad_id==$m_o->oportunidad_id){
                $totalg=$totalg+(int)(str_replace(",","",$act->total_general));
              }
            }
            
            $oportunidad_datos=DB::select('SELECT * FROM oportunidades WHERE id = '.$m_o->oportunidad_id.'');
            if($validacion==true){
                $array[$i]["total"]=$totalg;
                $array[$i]["oportunidad"]=$oportunidad_datos[0]->titulo;
                $i++;
            }
          }
        }
    $array=burbuja($array);
   for($o=0; $o<count($array);$o++){
    if($o<5){ ?>
    {
      "country": "<?php echo $array[$o]['oportunidad']?>",
      "visits": '<?php echo $array[$o]['total']; ?>'
    }, 
  <?php } } ?>
  ],
  "valueAxes": [ {
    "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
  } ],
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[category]]: <b>[[value]]</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits"
  } ],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20,
    "inside": true,
    "labelRotation":90,
  },
  "export": {
    "enabled": false
  }

} );

var chart = AmCharts.makeChart("lineas-negras", {
    "type": "serial",
    "theme": "black",
    "legend": {
        "useGraphSettings": true
    },
    "dataProvider": [
    <?php
      $meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
      for($i=0;$i<=11;$i++){
      ?>
    {
        "year": '<?php echo $meses[$i]; ?>',
        "germany": <?php echo $usuario_meta[$i]; ?>,
        "uk": <?php echo $ventas_x_meses[$i]; ?>
    }, 
    <?php } ?>],
    "valueAxes": [{
        "integersOnly": true,
        "minimum": 0,
        "reversed": false,
        "axisAlpha": 0,
        "dashLength": 5,
        "gridCount": 10,
        "position": "left",
    }],
    "startDuration": 0.5,
    "graphs": [{
        "balloonText": "Meta Ventas [[category]]: [[value]]",
        "bullet": "round",
        "title": "Meta Ventas anual",
        "lineColor": "#3feef3",
        "valueField": "germany",
    "fillAlphas": 0
    }, {
        "balloonText": "Real Venta [[category]]: [[value]] Millones",
        "bullet": "round",
        "title": "Real Ventas anual",
        "lineColor": "#df020b",
        "valueField": "uk",
    "fillAlphas": 0
    }],
    "chartCursor": {
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "fillAlpha": 0.09,
        "fillColor": "#000000",
        "gridAlpha": 0,
        "position": "bottom"
    },
    "export": {
      "enabled": false,
        "position": "bottom-right"
     }
});


</script>



<!-- FUNCIONES ANTERIOES-->
<script type="text/javascript">

$("body").on("click",".cumplimiento_llenado",function(e){

  setTimeout(function(){
    var gaugeChart = AmCharts.makeChart("gira-1", {
    "type": "gauge",
    "theme": "light",
    "axes": [{
      "axisAlpha": 0,
      "tickAlpha": 0,
      "labelsEnabled": false,
      "startValue": 0,
      "endValue": 100,
      "startAngle": 0,
      "endAngle": 270,
      "bands": [{
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "100%",
        "innerRadius": "85%"
      }, {
        "color": "#1c4597",
        "startValue": 0,
        "endValue": 80,
        "radius": "100%",
        "innerRadius": "85%",
        "balloonText": "90%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "80%",
        "innerRadius": "65%"
      }, {
        "color": "#0079f4",
        "startValue": 0,
        "endValue": 35,
        "radius": "80%",
        "innerRadius": "65%",
        "balloonText": "35%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "60%",
        "innerRadius": "45%"
      }, {
        "color": "#00d6fb",
        "startValue": 0,
        "endValue": 92,
        "radius": "60%",
        "innerRadius": "45%",
        "balloonText": "92%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "40%",
        "innerRadius": "25%"
      }, {
        "color": "#5ac3e1",
        "startValue": 0,
        "endValue": 68,
        "radius": "40%",
        "innerRadius": "25%",
        "balloonText": "68%"
      }]
    }],
    "allLabels": [{
      "text": "Diego",
      "x": "49%",
      "y": "5%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Mayra",
      "x": "49%",
      "y": "15%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Silvia",
      "x": "49%",
      "y": "24%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Oscar",
      "x": "49%",
      "y": "33%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }],
    "export": {
      "enabled": false
    }
  });

  var gaugeChart = AmCharts.makeChart("gira-2", {
    "type": "gauge",
    "theme": "light",
    "axes": [{
      "axisAlpha": 0,
      "tickAlpha": 0,
      "labelsEnabled": false,
      "startValue": 0,
      "endValue": 100,
      "startAngle": 0,
      "endAngle": 270,
      "bands": [{
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "100%",
        "innerRadius": "85%"
      }, {
        "color": "#1fdfea",
        "startValue": 0,
        "endValue": 80,
        "radius": "100%",
        "innerRadius": "85%",
        "balloonText": "90%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "80%",
        "innerRadius": "65%"
      }, {
        "color": "#016064",
        "startValue": 0,
        "endValue": 35,
        "radius": "80%",
        "innerRadius": "65%",
        "balloonText": "35%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "60%",
        "innerRadius": "45%"
      }, {
        "color": "#00c7dc",
        "startValue": 0,
        "endValue": 92,
        "radius": "60%",
        "innerRadius": "45%",
        "balloonText": "92%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "40%",
        "innerRadius": "25%"
      }, {
        "color": "#97f5fd",
        "startValue": 0,
        "endValue": 68,
        "radius": "40%",
        "innerRadius": "25%",
        "balloonText": "68%"
      }]
    }],
    "allLabels": [{
      "text": "Diego",
      "x": "49%",
      "y": "5%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Mayra",
      "x": "49%",
      "y": "15%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Silvia",
      "x": "49%",
      "y": "24%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Oscar",
      "x": "49%",
      "y": "33%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }],
    "export": {
      "enabled": false
    }
  });

  var gaugeChart = AmCharts.makeChart("gira-3", {
    "type": "gauge",
    "theme": "light",
    "axes": [{
      "axisAlpha": 0,
      "tickAlpha": 0,
      "labelsEnabled": false,
      "startValue": 0,
      "endValue": 100,
      "startAngle": 0,
      "endAngle": 270,
      "bands": [{
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "100%",
        "innerRadius": "85%"
      }, {
        "color": "#9ee0dc",
        "startValue": 0,
        "endValue": 80,
        "radius": "100%",
        "innerRadius": "85%",
        "balloonText": "90%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "80%",
        "innerRadius": "65%"
      }, {
        "color": "#00968a",
        "startValue": 0,
        "endValue": 35,
        "radius": "80%",
        "innerRadius": "65%",
        "balloonText": "35%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "60%",
        "innerRadius": "45%"
      }, {
        "color": "#49ccc6",
        "startValue": 0,
        "endValue": 92,
        "radius": "60%",
        "innerRadius": "45%",
        "balloonText": "92%"
      }, {
        "color": "#eee",
        "startValue": 0,
        "endValue": 100,
        "radius": "40%",
        "innerRadius": "25%"
      }, {
        "color": "#004d3d",
        "startValue": 0,
        "endValue": 68,
        "radius": "40%",
        "innerRadius": "25%",
        "balloonText": "68%"
      }]
    }],
    "allLabels": [{
      "text": "Diego",
      "x": "49%",
      "y": "5%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Mayra",
      "x": "49%",
      "y": "15%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Silvia",
      "x": "49%",
      "y": "24%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }, {
      "text": "Oscar",
      "x": "49%",
      "y": "33%",
      "size": 15,
      "bold": true,
      "color": "#1c4597",
      "align": "right"
    }],
    "export": {
      "enabled": false
    }
  });
  },2000);
})


$("body").on("click",".actividades_pendientes",function(e){
  $("#radar2-pendiente").empty();
  $("#radar-pendiente").empty();
  $("#linea-pendiente").empty();
  setTimeout(function(){
  var chart = AmCharts.makeChart("linea-pendiente", {
    "type": "serial",
    "theme": "light",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
    "useGraphSettings": true,
    "markerSize": 10
    },
    "dataProvider": [{
        "year": "Mayra",
        "europe": 2.5,
        "namerica": 2.5
    }, {
        "year": "Oscar",
        "europe": 2.6,
        "namerica": 2.7
    },{
        "year": "Silvia",
        "europe": 2.5,
        "namerica": 2.5
    }, {
        "year": "Diego",
        "europe": 2.6,
        "namerica": 2.7
    }],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Europe",
        "type": "column",
        "color": "#000000",
        "valueField": "europe"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "North America",
        "type": "column",
        "color": "#000000",
        "valueField": "namerica"
    }],
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "export": {
      "enabled": false
     }

});
    var chart = AmCharts.makeChart( "radar-pendiente", {
  "type": "radar",
  "theme": "light",
  "dataProvider": [ {
    "country": "Mayra",
    "litres": 156.9
  }, {
    "country": "Oscar",
    "litres": 131.1
  }, {
    "country": "Silvia",
    "litres": 115.8
  }, {
    "country": "Diego",
    "litres": 109.9
  } ],
  "valueAxes": [ {
    "axisTitleOffset": 20,
    "minimum": 0,
    "axisAlpha": 0.15
  } ],
  "startDuration": 2,
  "graphs": [ {
    "balloonText": "[[value]] litres of beer per year",
    "bullet": "round",
    "lineThickness": 2,
    "valueField": "litres"
  } ],
  "categoryField": "country",
  "export": {
    "enabled": false
  }
} );

    var chart = AmCharts.makeChart( "radar2-pendiente", {
  "type": "radar",
  "theme": "light",
  "dataProvider": [ {
    "direction": "Mayra",
    "value": 8
  }, {
    "direction": "Oscar",
    "value": 9
  }, {
    "direction": "Diego",
    "value": 4.5
  }, {
    "direction": "Silvia",
    "value": 3.5
  }],
  "valueAxes": [ {
    "gridType": "circles",
    "minimum": 0,
    "autoGridCount": false,
    "axisAlpha": 0.2,
    "fillAlpha": 0.05,
    "fillColor": "#FFFFFF",
    "gridAlpha": 0.08,
    "guides": [ {
      "angle": 225,
      "fillAlpha": 0.3,
      "fillColor": "#0066CC",
      "tickLength": 0,
      "toAngle": 315,
      "toValue": 14,
      "value": 0,
      "lineAlpha": 0,

    }, {
      "angle": 45,
      "fillAlpha": 0.3,
      "fillColor": "#CC3333",
      "tickLength": 0,
      "toAngle": 135,
      "toValue": 14,
      "value": 0,
      "lineAlpha": 0,
    } ],
    "position": "left"
  } ],
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[category]]: [[value]] m/s",
    "bullet": "round",
    "fillAlphas": 0.3,
    "valueField": "value"
  } ],
  "categoryField": "direction",
  "export": {
    "enabled": false
  }
} );
},2000);
  
})
  
  $("body").on("click",".cono",function(e){
    
    setTimeout(function(){
      for(i=1; i<10;i++){
        $("#meter-"+i).empty();
        var chart = AmCharts.makeChart("meter-"+i, {
        "theme": "light",
        "type": "gauge",
        "axes": [{
          "topTextFontSize": 15,
          "topTextYOffset": 70,
          "axisColor": "#31d6ea",
          "axisThickness": 1,
          "endValue": 100,
          "gridInside": true,
          "inside": true,
          "radius": "50%",
          "valueInterval": 20,
          "tickColor": "#67b7dc",
          "startAngle": -90,
          "endAngle": 90,
          "unit": "%",
          "bandOutlineAlpha": 0,
          "bands": [{
            "color": "#e23131",
            "endValue": 100,
            "innerRadius": "105%",
            "radius": "170%",
            "gradientRatio": [0.5, 0, -0.5],
            "startValue": 0
          }, {
            "color": "#f88100",
            "endValue": 0,
            "innerRadius": "105%",
            "radius": "170%",
            "gradientRatio": [0.5, 0, -0.5],
            "startValue": 0
          }]
        }],
        "arrows": [{
          "alpha": 1,
          "innerRadius": "35%",
          "nailRadius": 0,
          "radius": "170%"
        }]
      });
      setInterval(randomValue(chart), 500);
      }
  // set random value
},2000);

function randomValue(chart) {
  var value = Math.round(Math.random() * 100);
  chart.arrows[0].setValue(value);
  chart.axes[0].setTopText(value + " %");
  // adjust darker band to new value
  chart.axes[0].bands[1].setEndValue(value);
}
  })
 
  var chart = AmCharts.makeChart( "chartdiv", {
  "type": "funnel",
  "theme": "light",
  "dataProvider": [ {
    "title": "Diferencia meta",
    "value": 200
  }, {
    "title": "Baja",
    "value": 123
  }, {
    "title": "Media",
    "value": 98
  }, {
    "title": "Alta",
    "value": 72
  }, {
    "title": "Diferencia meta",
    "value": 35
  }, {
    "title": "Baja",
    "value": 35
  }, {
    "title": "Media",
    "value": 26
  }, {
    "title": "Alta",
    "value": 26
  }, {
    "title": "Diferencia meta",
    "value": 26
  }, {
    "title": "Baja",
    "value": 26
  } , {
    "title": "Media",
    "value": 26
  } , {
    "title": "Alta",
    "value": 26
  }  ],
  "balloon": {
    "fixedPosition": true
  },
  "valueField": "value",
  "titleField": "title",
  "marginRight": 240,
  "marginLeft": 50,
  "startX": -500,
  "depth3D": 100,
  "angle": 40,
  "outlineAlpha": 1,
  "outlineColor": "#FFFFFF",
  "outlineThickness": 2,
  "labelPosition": "right",
  "balloonText": "[[title]]: [[value]]n[[description]]",
  "export": {
    "enabled": false
  }
} );

  var chart = AmCharts.makeChart("lista-comercial",
{
    "type": "serial",
    "theme": "light",
    "dataProvider": [{
        "name": "Silvia",
        "points": 35654,
        "color": "#009cff",
        "bullet": "https://www.amcharts.com/lib/images/faces/A04.png"
    }, {
        "name": "Mayra",
        "points": 65456,
        "color": "#004dff",
        "bullet": "https://www.amcharts.com/lib/images/faces/C02.png"
    }, {
        "name": "Oscar",
        "points": 45724,
        "color": "#38a6b7",
        "bullet": "https://www.amcharts.com/lib/images/faces/D02.png"
    }, {
        "name": "Diego",
        "points": 13654,
        "color": "#0111be",
        "bullet": "https://www.amcharts.com/lib/images/faces/E01.png"
    }],
    "valueAxes": [{
        "maximum": 80000,
        "minimum": 0,
        "axisAlpha": 0,
        "dashLength": 4,
        "position": "left"
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
        "bulletOffset": 10,
        "bulletSize": 52,
        "colorField": "color",
        "cornerRadiusTop": 8,
        "customBulletField": "bullet",
        "fillAlphas": 0.8,
        "lineAlpha": 0,
        "type": "column",
        "valueField": "points"
    }],
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "categoryField": "name",
    "categoryAxis": {
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "export": {
      "enabled": false
     }
});

 var chart = AmCharts.makeChart("barra-horizontal", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "year": "Diego",
        "income": 23.5,
        "bullet": "https://www.amcharts.com/lib/images/faces/E01.png"
    }, {
        "year": "Oscar",
        "income": 26.2,
        "bullet": "https://www.amcharts.com/lib/images/faces/D02.png"
    }, {
        "year": "Mayra",
        "income": 30.1,
        "bullet": "https://www.amcharts.com/lib/images/faces/C02.png"
    }, {
        "year": "Silvia",
        "income": 29.5,
        "bullet": "https://www.amcharts.com/lib/images/faces/A04.png"
    }],
    "graphs": [{
        "balloonText": "Income in [[category]]:[[value]]",
        "bulletOffset": 10,
        "bulletSize": 33,
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "customBulletField": "bullet",
        "title": "Income",
        "type": "column",
        "valueField": "income"
    }],
    "depth3D": 20,
    "angle": 30,
    "rotate": true,
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left"
    },
    "export": {
      "enabled": false
     }
});

 var chart = AmCharts.makeChart("pie", {
    "type": "pie",
    "theme": "light",
    "innerRadius": "40%",
    "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
    "dataProvider": [{
        "country": "Diego",
        "litres": 501.9
    }, {
        "country": "Oscar",
        "litres": 301.9
    }, {
        "country": "Silvia",
        "litres": 201.1
    }, {
        "country": "Mayra",
        "litres": 165.8
    }],
    "balloonText": "[[value]]",
    "valueField": "litres",
    "titleField": "country",
    "balloon": {
        "drop": true,
        "adjustBorderColor": false,
        "color": "#023497",
        "fontSize": 16
    },
    "export": {
        "enabled": false
    }
});

 var chart = AmCharts.makeChart("negocio_perdido", {
    "theme": "light",
    "type": "serial",
  "startDuration": 2,
    "dataProvider": [{
        "country": "Diego",
        "visits": 4025,
        "color": "#FF0F00"
    }, {
        "country": "Oscar",
        "visits": 1882,
        "color": "#FF6600"
    }, {
        "country": "Mayra",
        "visits": 1809,
        "color": "#FF9E01"
    }, {
        "country": "Silvia",
        "visits": 1322,
        "color": "#FCD202"
    }],
    "valueAxes": [{
        "position": "left",
        
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits",

    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 0
    },
    "export": {
      "enabled": false
     }

});

 var chart = AmCharts.makeChart("cantidad-perdida", {
    "theme": "light",
    "type": "serial",
    "startDuration": 2,
    "dataProvider": [{
        "country": "Mayra",
        "visits": 8,
        "color": "#FF0F00"
    }, {
        "country": "Oscar",
        "visits": 4,
        "color": "#FF6600"
    }, {
        "country": "Diego",
        "visits": 3,
        "color": "#FF9E01"
    }, {
        "country": "Silvia",
        "visits": 10,
        "color": "#FCD202"
    }],
    "valueAxes": [{
        "position": "left",
        "axisAlpha":0,
        "gridAlpha":0
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "colorField": "color",
        "fillAlphas": 0.85,
        "lineAlpha": 0.1,
        "type": "column",
        "topRadius":1,
        "valueField": "visits"
    }],
    "depth3D": 40,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha":0,
        "gridAlpha":0

    },
    "export": {
      "enabled": false
     }

}, 0);

var chart = AmCharts.makeChart("graficas-alta1", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-mediano1", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-corto1", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});

var chart = AmCharts.makeChart("graficas-alta2", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-mediano2", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-corto2", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-alta3", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-mediano3", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-corto3", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});

var chart = AmCharts.makeChart("graficas-alta4", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-mediano4", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});
var chart = AmCharts.makeChart("graficas-corto4", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": [{
    "country": "Alta",
    "visits": 13,
    "color": "#0061a6"
  }, {
    "country": "Media",
    "visits": 8,
    "color": "#039b40"
  }, {
    "country": "Baja",
    "visits": 4,
    "color": "#668596"
  }],
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits",

  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 90,
    "ignoreAxisWidth":false,
    "inside":true,
  },
  "export": {
    "enabled": false
  }

});

var chart = AmCharts.makeChart( "radar", {
  "type": "radar",
  "theme": "light",
  "dataProvider": [ {
    "country": "Apertura de Mercado",
    "litres": 156.9
  }, {
    "country": "Soporte a ESSI",
    "litres": 131.1
  }, {
    "country": "Soporte Tecnico",
    "litres": 115.8
  }, {
    "country": "Facturación",
    "litres": 109.9
  }, {
    "country": "Negociación",
    "litres": 108.3
  } ],
  "valueAxes": [ {
    "axisTitleOffset": 20,
    "minimum": 0,
    "axisAlpha": 0.15
  } ],
  "startDuration": 2,
  "graphs": [ {
    "balloonText": "[[value]] litres of beer per year",
    "bullet": "round",
    "lineThickness": 2,
    "valueField": "litres"
  } ],
  "categoryField": "country",
  "export": {
    "enabled": false
  }
} );

//************** INICIO DE SCRIPTS SEGUNDO PANEL**********************//


</script>
@endsection
