@extends('template.app')

@section('title', 'Comparacion de años')


@section('content')
{{ csrf_field() }}
<style>
    .w-100{ width: 100%; }
    .color-tittle{ color: #5fc2ff; }
    .sombra{ webkit-box-shadow: -2px 3px 29px -4px rgba(0,0,0,0.75); -moz-box-shadow: -2px 3px 29px -4px rgba(0,0,0,0.75); box-shadow: -2px 3px 29px -4px rgba(0,0,0,0.75); }
    .encabezado{ background-color:  #2a3c48; }
    .color-encabezado a{ color: rgb(104, 210, 255) !important; }
    .br-ano{ border-right: 3px solid #2a3c48; }
    .date-select{ height: auto !important; font-size: 24px; border: 0;}
    .h-fijo{ height: 44px;}
    @media (min-width: 1366px) and (max-width: 1919px) { .date-select{ font-size: 19px; } h6{font-size: 10px !important;} }
    .fila-contenido:nth-child(odd){background-color: #2a3c483b;}
    .cabecera{background-color: #2a3c483b;}
</style>

<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="w-100 color-tittle">Comparación de años</h1>
    </div>
    <div class="col-md-12">
        <div class="row ml-5 mr-5 mt-5">
             <div class="col-md-12 mt-5">
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-10">
                        <div class="row h-fijo">
                            <div class="col-md-4 br-ano text-center filtro-fechas">
                                <select class="form-control date-select">
                                    <option value="">29 de Diciembre 2015</option>
                                </select>
                            </div>
                            <div class="col-md-4 br-ano filtro-fechas">
                                <select class="form-control date-select">
                                    <option value="">29 de Diciembre 2016</option>
                                </select>
                            </div>
                            <div class="col-md-4 filtro-fechas">
                                <select class="form-control date-select">
                                    <option value="">29 de Diciembre 2017</option>
                                </select>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row ml-5 mr-5">
            <div class="col-md-12">
                <div id="accordion" role="tablist">
                  <div class="card sombra">
                    <div class="card-header encabezado" role="tab" id="heading1">
                      <h4 class="mb-0 color-encabezado">
                        <a data-toggle="collapse" href="#collapse1" role="button" aria-expanded="true" aria-controls="collapse1">
                         <i class="fa fa-briefcase" aria-hidden="true"></i>
                          NEGOCIOS CERRADOS
                        </a>
                      </h4>
                    </div>
                    <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordion">
                      <div class="card-body">
                        <div class="row">
                            <div class="col-md-2 br-ano">
                                <h4>Funcionario</h4>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-4 br-ano">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Monto</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Cantidad</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Promedio Cierre</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 br-ano">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Monto</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Cantidad</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Promedio Cierre</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Monto</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Cantidad</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Promedio Cierre</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 br-ano">
                                <h6>Mayra Portilla</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-4 br-ano">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6>$130,000,000,000</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>2</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>2.3</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 br-ano">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6>$130,000,000,000</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>2</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>2.3</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6>$130,000,000,000</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>2</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>2.3</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-wide fade p-4" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal">Cargando...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body px-0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="fa-3x">
                               <img src="/images/carga.gif" style="width:80px;">
                                <p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
    var CSRF_TOKEN = $('input[name=_token]').val();
    $(document).ready(function($){
        select();
    });

    /**
     * Función para traer los select de las cabeceras
     */
    function select(){
         var jqxhr = $.ajax({
            url: "/comparacion-action",
            data: {
                _token: CSRF_TOKEN,
                action: 0
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('.filtro-fechas').eq(0).html(e.data['primer_select']);
                $('.filtro-fechas').eq(1).html(e.data['segundo_select']);
                $('.filtro-fechas').eq(2).html(e.data['tercer_select']);
                list($('.date-select').eq(0).val(),$('.date-select').eq(1).val(),$('.date-select').eq(2).val());
            }
         });
    }

    $(document).on('change','.date-select',function(){
        list($('.date-select').eq(0).val(),$('.date-select').eq(1).val(),$('.date-select').eq(2).val());
    });

    /**
     * Funcion para ver el listado de la información de dashboard almacenado
     * @param INT primera ID DEL PRIMER FILTRO DE LA TABLA comparaciones
     * @param INT segunda ID DEL SEGUNDO FILTRO DE LA TABLA comparaciones
     * @param INT tercera ID DEL TERCER FILTRO DE LA TABLA comparaciones
     */
    function list(primera,segunda,tercera){
        $('#exampleModal').modal('show');
         var jqxhr = $.ajax({
            url: "/comparacion-action",
            data: {
                _token: CSRF_TOKEN,
                action: 1,
                primera: primera,
                segunda: segunda,
                tercera: tercera
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#accordion').html(e.data['html']);
                $('#exampleModal').modal('hide');
            }
         });
    }
</script>
@endsection
