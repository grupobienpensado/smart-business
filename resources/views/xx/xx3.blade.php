@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Agenda Smart')

<?php $codigoHTML =   "";


$codigoHTML .=	'<form id="formAgendaSmart" enctype="multipart/form-data">
			                <div class="panel-title">
			                    <h2>AGENDA SMART DE TRABAJO</h2>
			                    <p id="parrafoIniFin"></p>';
					                if (Auth::user()->id === 1){
					                   	 $codigoHTML .= '<p id="validadorAdminDate"></p>';
					                }
					                $codigoHTML .= '
			                    <p>Agrega actividades a tu agenda smart</p>
			                    <div id="divImgEstado">
			                    	<p id="parrafoOk"></p>
			                    	<img id="imgStado" src="'.url('/').'/images/icons/checkedblue.png">
			                    	<input type="hidden" id="tiempofuera" name="fuera_tiempo" value="false">
			                    </div>
			                </div>
			                <hr>';
			                if (count($errors) > 0){
			                   	 $codigoHTML .= '<div class="alert alert-danger">
			                        <ul>';
			                            foreach ($errors->all() as $error){
			                                $codigoHTML .= '<li>'.$error.'</li>';
			                            }
			                        $codigoHTML .= '</ul>
			                    </div>';
			                }
			                $codigoHTML .= csrf_field().'
	                <div class="form-group row padreTraerdados">
                        <div class="col-8 hijoTraerdatos">
                            <select id="select_traer_datos" name="origen_datos" class="form-control" required>
                                <option value="">Seleccione el origen de los datos</option>
                                <option value="N">Actividad no planeada</option>
                                <option value="P">Plan de trabajo</option>
                                <option value="G">Agenda visita</option>
                            </select>
                        </div>
					        </div>
					        <div id="contenedorOculto" style="display: none !important;">
				                <div class="form-group row"  id="divAgendaVisita" style="display: none !important;">
				                	<div class="col-12" style="text-align: start;">
						    			<label style="text-align: start;">Agenda Visita</label>
						    			<input list="agenvisita" class="swal2-input" name="id_tipo" id="agendaVisita" placeholder="Ingrese una visita de cliente">
                                        <datalist id="agenvisita">';
                                                 foreach ($agendavisita as $tag) {
                                                    $agendaclient = App\Agenda_cliente::find($tag->agenda_clientes);
                                                    if(isset($agendaclient) && !empty($agendaclient)){
                                                        $oportagent   = App\Oportunidades::find($agendaclient->oportunidad);
	                                                    $codigoHTML .= '<option label="'.$oportagent->empresa.'/'.substr($tag->descripcion, 0, 100).'" value="'.$tag->id.'">';
                                                    }
	                                            }
	                                    $codigoHTML .='</datalist>

						            </div>
					            </div>

					            <div class="form-group row" id="divPlanTrabajo" style="display: none !important;">
						    		<div class="col-12" style="text-align: start;">
						    			<label style="text-align: start;">Plan de trabajo</label>
						    			<input list="planestrabajo" class="swal2-input" name="id_tipo" id="PlanTrabajo" placeholder="Ingrese un plan de trabajo">
                                        <datalist id="planestrabajo">';
	                                            foreach ($planestrabajo as $tag) {
	                                            	if ($tag->oportunidad == "Trabajos Generales ESSI") {
                                                        try{
                                                            $codigoHTML .= '<option label="Trabajos Generales ESSI/'.substr($tag->observaciones, 0, 100).'" value="'.$tag->id.'">';
                                                        }
                                                        catch (Exception $e){
                                                        }
	                                            	}else{
                                                        try{
                                                            $oportagent   = App\Oportunidades::find($tag->oportunidad);
	                                            		     $codigoHTML .= '<option label="'.$oportagent->empresa.'/'.substr($tag->observaciones, 0, 100).'" value="'.$tag->id.'">';
                                                        }
                                                        catch (Exception $e){
                                                        }
	                                            	}
	                                            }
	                                    $codigoHTML .='</datalist>
						            </div>
						        </div>

                    <div class="row top-grande">
                      <div class="col-md-3">
                        <p>Tipo de actividad</p>
                        <select name="tipo_actividad" class="swal2-input" id="tipo_actividad" required>
                            <option value="">Seleccione un tipo de actividad</option>
                            <option>Oportunidad</option>
                            <option>Empresa</option>
                            <option>Gestion comercial</option>
                            <option>Gestion interna ESSI</option>
                        </select>
                      </div>
                      <div class="col-md-3 contSubTipo"></div>
                      <div class="col-md-4 addOtro"></div>
                      <div class="col-md-2">
                        <p>Estado de la actividad</p>
                        <select name="estado_actividad" id="estado_actividad" class="swal2-input">
                            <option value="">Seleccione un estado de la actividad</option>';
                            foreach($estado_actividades as $e_a){
                                $codigoHTML .='<option value="'.$e_a->estado.'">'.$e_a->estado.'</option>';
                            }
                        $codigoHTML .='</select>
                      </div>
                    </div>

						        <div class="form-group row opprohtml"></div>
				                <div class="row fila_msj" style="display: none;">
				                    <div class="col-md-12">
				                        <div class="alert alert-success">
				                          <strong>Tipo Actividad: <a id="titulo_t_a"></a></strong><br> <a id="msj_ta">Es muy importante que leas este mensaje de alerta.</a>
				                        </div>
				                    </div>
				                </div>

				                <div class="form-group row">
				                    <label for="detalle_actividad" class="col-2 col-form-label">Detalle de actividad</label>
				                    <div class="col-10">
				                        <textarea class="form-control" id="detalle_actividad" name="detalle_actividad" rows="3" placeholder="Por favor ingrese un Detalle de la actividad"></textarea>
				                    </div>
				                </div>
				                <div class="row" style="margin-bottom: 1%">
				                    <div class="col-md-2 num_car_act_columna">

				                    </div>
				                    <div class="col-md-10 num_car_act_columna">
				                        <a id="num_car_act"></a>
				                    </div>
				                </div>
				                <div class="form-group row">
				                    <label for="detalle_resultado" class="col-2 col-form-label">Detalle de resultado</label>
				                    <div class="col-10">
				                        <textarea class="form-control" id="detalle_resultado" name="detalle_resultado" rows="3" placeholder="Por favor ingrese un Detalle de la actividad"></textarea>
				                    </div>
				                </div>
				                <div class="row" style="margin-bottom: 1%">
				                    <div class="col-md-2 num_car_resu_columna">

				                    </div>
				                    <div class="col-md-10 num_car_resu_columna">
				                        <a id="num_resu_act"></a>
				                    </div>
				                </div>
				                <div class="card" style="margin-bottom: 30px;">
				                    <h5 class="card-header"> PENDIENTES </h5>
				                    <div class="card-block">
				                        <div class="row">
				                          <div class="col-7">
				                            <div class="form-group">
				                                <label class="col-form-label">Descripcion de la actividad pendiente</label>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <label for="" class="col-form-label">Tipo de actividad</label>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <label class="col-form-label">Fecha de Cierre</label>
				                            </div>
				                          </div>
				                          <div class="col-1"></div>
				                        </div>
				                        <div id="pendientes"></div>
				                        <div id="pendienteshtml" class="row"></div>
				                        <div class="row">
				                          <div class="col-7">
				                            <div class="form-group">
				                                <textarea class="form-control detalle_pendiente" id="0" name="pendientes[0][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente"></textarea>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <select name="pendientes[0][tipo_pendiente]" id="tipo_pendiente0" class="form-control">
				                                    <option value="">Seleccione una tipo de actividad</option>';
				                                    foreach($tipos_actividades as $t_a){
				                                    	$codigoHTML .='<option value="'.$t_a->concepto.'">'.$t_a->concepto.'</option>';
				                                    }
				                                $codigoHTML .='</select>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <input class="form-control date" type="text" id="fechPendiente0" name="pendientes[0][fecha_pendiente]" placeholder="Fecha de Cierre">
				                            </div>
				                          </div>
				                          <div class="col-1">
				                            <button class="btn btn-outline-success pull-right btn-sm" type="button" id="agregarPendientes"><i class="fa fa-plus text-success"></i></button>
				                          </div>
				                        </div>
				                        <div class="row">
				                            <div class="col-md-7 num_pen_columna0">
				                                <a id="num_pen_act0"></a>
				                            </div>
				                            <div class="col-md-5 num_pen_columna0">

				                            </div>
				                        </div>
				                    </div>
				                </div>
				                <div class="card" style="margin-bottom: 30px;">
				                    <h5 class="card-header">MANO OBRA: '.Auth::user()->name.'</h5>
				                    <div class="card-block">
				                        <div class="row">
				                            <div class="col-md-4">
				                                <div>
				                                    <label class="col-form-label">Cargo</label>
				                                </div>
				                            </div>
				                            <div class="col-2">
				                                <div class="form-group">
				                                    <label class="col-form-label">Cantidad</label>
				                                </div>
				                            </div>
				                            <div class="col-2">
				                                <div class="form-group">
				                                    <label class="col-form-label">Horas</label>
				                                </div>
				                            </div>
				                            <div class="col-3">
				                                <div class="form-group">
				                                    <label class="col-form-label">Valor</label>
				                                </div>
				                            </div>
				                            <div class="col-1"></div>
				                        </div>
				                    </div>
				                    <div id="manoobrahtml" class="row">
				                          <input type="hidden" id="hiduser" name="hhombre[0][id_user]" value="'.Auth::user()->id.'">
					                      <div class="col-4" style="margin-left: 1%">
					                        <div class="form-group">
					                            <input type="hidden" id="hcargo" name="hhombre[0][cargo]" value="'.Auth::user()->cargo.'">
					                            <a style="font-size: x-large;">'.Auth::user()->cargo.'</a>
					                        </div>
					                      </div>
					                      <div class="col-2">
					                        <div class="form-group">
					                            <input type="hidden" name="hhombre[0][cantidad]" value="1">
					                            <a style="font-size: x-large;">1</a>
					                        </div>
					                      </div>
					                      <div class="col-2">
					                        <div class="form-group">
					                            <input type="hidden" class="mis_horas" name="hhombre[0][horas]" value="0">
					                            <a id="mis_horas" style="font-size: x-large;">0</a>
					                        </div>
					                      </div>
					                      <div class="col-3">
					                        <div class="form-group">
					                            <input type="hidden" id="total_mis_horas" name="hhombre[0][valor]" value="0">
					                            <a class="total_mis_horas" style="font-size: x-large;">$ 0</a>
					                        </div>
					                      </div>
				                    </div>
				                </div>
				                <div class="card" style="margin-bottom: 30px;">
				                    <h5 class="card-header"> HORAS HOMBRE </h5>
				                    <div class="card-block">
				                        <div class="row">
				                          <div class="col-4">
				                            <div class="form-group">
				                                <label class="col-form-label">Cargo</label>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <label class="col-form-label">Cantidad</label>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <label class="col-form-label">Horas</label>
				                            </div>
				                          </div>
				                          <div class="col-3">
				                            <div class="form-group">
				                                <label class="col-form-label">Valor</label>
				                            </div>
				                          </div>
				                          <div class="col-1"></div>
				                        </div>
				                        <div id="horashombrediv"></div>

				                        <div id="horashombrehtml" class="row"></div>

				                        <div class="row">
				                          <div class="col-4">
				                            <div class="form-group">
				                                <select name="hhombre[1][cargo]" id="textCargo1" class="form-control" onchange="calcularValorHH(1)">
				                                    <option value="">Seleccione un cargo</option>';
				                                    foreach($cargos as $cargo){
				                                    	$codigoHTML .='<option value="'.$cargo->nombre.'">'.$cargo->nombre.'</option>';
				                                    }
				                                $codigoHTML .='</select>
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <input type="number" id="numberCantidad1" name="hhombre[1][cantidad]" class="form-control" placeholder="0" onchange="calcularValorHH(1)">
				                            </div>
				                          </div>
				                          <div class="col-2">
				                            <div class="form-group">
				                                <input class="form-control" id="numberHoras1" type="number" name="hhombre[1][horas]" placeholder="0" onchange="calcularValorHH(1)">
				                            </div>
				                          </div>
				                          <div class="col-3">
				                            <div class="form-group">
				                                <input class="form-control sumarSueldo" id="numberValor1" type="text" name="hhombre[1][valor]" placeholder="$0" readonly>
				                            </div>
				                          </div>
				                          <div class="col-1">
				                            <button class="btn btn-outline-success pull-right btn-sm" type="button" id="agregarHorasHombre"><i class="fa fa-plus text-success"></i></button>
				                          </div>
				                        </div>
				                        <div class="form-group row">
				                            <label for="total" class="col-2 col-form-label">Total Horas Hombre</label>
				                            <div class="col-10">
				                                <input class="form-control" style="font-size: x-large;" type="text" name="total_h_hombre" placeholder="$0" id="total_h_hombre" readonly="true">
				                            </div>
				                        </div>
				                    </div>
				                </div>
				                <div class="card" style="margin-bottom: 30px;">
				                  <h5 class="card-header">COSTOS GENERADOS POR ACTIVIDADES </h5>
				                  <div class="card-block">
				                    <div class="row">
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="alimentacion" class="col-6 col-form-label">Alimentacion</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="alimentacion" placeholder="$0" id="s1" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="transportes_internos" class="col-6 col-form-label">Trasportes Internos</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="transportes_internos" placeholder="$0" id="s2" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                    </div>
				                    <div class="row">
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="transportes_intermunicipales" class="col-6 col-form-label">Trasportes Intermunicipales</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="transportes_intermunicipales" placeholder="$0" id="s3" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="tiquete_aereo" class="col-6 col-form-label">Tiquetes Aereos</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="tiquete_aereo" placeholder="$0" id="s4" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                    </div>
				                    <div class="row">
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="papeleria" class="col-6 col-form-label">Papeleria</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="papeleria" placeholder="$0" id="s5" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="invitacion_cliente" class="col-6 col-form-label">Invitacion Clientes</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="invitacion_cliente" placeholder="$0" id="s6" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                    </div>
				                    <div class="row">
				                        <div class="col-6">
				                            <div class="form-group row">
				                                <label for="alquiler_vehiculo" class="col-6 col-form-label">Alquiler de Vehiculo</label>
				                                <div class="col-6">
				                                    <input class="form-control dinero" type="text" name="alquiler_vehiculo" placeholder="$0" id="s7" onchange="funSumar()">
				                                </div>
				                            </div>
				                        </div>
				                        <div class="col-6">
				                            <div class="form-group row">
				                                <label for="gasolina_pasajes" class="col-6 col-form-label">Gasolina y pasajes</label>
				                                <div class="col-6">
				                                    <input class="form-control dinero" type="text" name="gasolina_pasajes" placeholder="$0" id="s8" onchange="funSumar()">
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row">
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="hotel" class="col-6 col-form-label">Hotel</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="hotel" placeholder="$0" id="s9" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group row">
				                            <label for="otros" class="col-6 col-form-label">Otros</label>
				                            <div class="col-6">
				                                <input class="form-control dinero" type="text" name="otros" placeholder="$0" id="s10" onchange="funSumar()">
				                            </div>
				                        </div>
				                      </div>
				                    </div>

				                    <div class="form-group row">
				                        <label for="total" class="col-2 col-form-label">Total</label>
				                        <div class="col-10">
				                            <input class="form-control" style="font-size: x-large;" type="text" name="total" placeholder="$0" id="total" readonly="true">
				                        </div>
				                    </div>

				                    <div class="form-group row">
				                        <label for="total" class="col-2 col-form-label">Total General</label>
				                        <div class="col-10">
				                            <input class="form-control" style="font-size: x-large;" type="text" name="total_general" placeholder="$0" id="total_general" readonly="true">
				                        </div>
				                    </div>
				                  </div>
				                </div>';
								if(Auth::user()->cargo=="Gerente Comercial"){
									$usuarios = App\User::where('cargo', 'like', 'Ejecutivo Comercial')
														  ->orWhere('cargo', 'like', 'Partner Solution')
														  ->orWhere('cargo', 'like', 'Gerente Comercial')
						                                  ->pluck('name', 'id');
					                $codigoHTML .='<div class="card" style="margin-bottom: 30px;">
					                  	<h5 class="card-header">ASIGNAR COSTOS DE ACTIVIDAD</h5>
					                  	<div class="card-block">
						                    <div class="row">
							                    <div class="col-7">
							                    	<label for="total" class="col-form-label">Seleccione un funcionario para asignar los gastos</label>
									                <select class="form-control" name="responsable_gasto" id="resGastosAsing">
									                	<option value="">Seleccione un funcionario</option>';
											            foreach ($usuarios as $id => $tag) {
															$codigoHTML .="<option value='".$id."'>".$tag."</option>";
														}
									                $codigoHTML .='</select>
									            </div>
								            </div>
						            	</div>
					            	</div>';
				            	}
				                $codigoHTML .='<div class="card" style="margin-bottom: 30px;">
					            	<h5 class="card-header">ADJUNTAR ARCHIVOS</h5>
					                <div id="divPegarArchivo"></div>
								</div>
							</div>
			            </form>
			           	<div class="imagesfile">
                            <div class="contentimg">

                            </div>
                        </div>

				        <div id="lisComentariosId"></div>
                        <div id="divRevisarButton"></div>
			            <div id="divEliminarButton"></div>

			            '; ?>
@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.min.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.print.min.css" rel='stylesheet' media='print' />

<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="{{ url('/') }}/css/bitacora.css">

<input type="hidden" id="cargo_user" value="{{Auth::user()->cargo}}">
<div class="card animated flipInX" id="list" style="margin-bottom: 30px;">
    <div class="card-block">
    	<div class="pull-left">
	        <div class="btn-group" role="group">
	          <button type="button" class="btn btn-sm btn-secondary" id="btnPrev"><i class="fa fa-chevron-left"></i> Anterior</button>
	          <button type="button" class="btn btn-sm btn-secondary" id="btnToday"><i class="fa fa-calendar-check-o"></i> Hoy</button>
	          <button type="button" class="btn btn-sm btn-secondary" id="btnNext">Siguiente <i class="fa fa-chevron-right"></i></button>
	        </div>
	    </div>
    	<div class="pull-right">
	        <div class="btn-group" role="group">
	          <button type="button" class="btn btn-sm btn-secondary active" onclick="funMostarDiv('calendar')" id="bcalendar"><i class="fa fa-calendar"></i> Ver Calendario</button>
	          <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('grafica')" id="bgrafica"><i class="fa fa-bar-chart"></i> Ver grafica</button>
	        </div>
	    </div>
    	<h2 id="tituloPrincipal">Bitacora</h2>
    	<div id="loaders" style="display: none;"><div class="loader"></div><small>Cargando registros...</small></div>
    	<div id='script-warning'>
			<code>Error!, no se han podido cargar los datos</code>
		</div>
		<div class="form-group row">
			<div class="col-2"></div>
            <div class="col-4">
            	<?php if($permiso_filtro=="Si"){ ?>
                <select class="form-control" id="responsable">
                	<option value="">Seleccione un funcionario</option>
                	<?php  $usuarios = App\User::where('cargo', 'like', 'Ejecutivo Comercial')
										  ->orWhere('cargo', 'like', 'Partner Solution')
										  ->orWhere('cargo', 'like', 'Gerente Comercial')
		              ->pluck('name', 'id');

		            foreach ($usuarios as $id => $tag) {
						echo "<option value='".$id."'>".$tag."</option>";
					}?>
                </select>
                <?php } ?>
            </div>
            <div class="col-4">
            	<?php if($permiso_filtro_semana=="Si"){ ?>
                <select class="form-control" id="semanas">
                	<option value="01">Seleccione una semana</option>
                	<option value="01">Semana 01</option>
                	<option value="02">Semana 02</option>
                	<option value="03">Semana 03</option>
                	<option value="04">Semana 04</option>
                	<option value="05">Semana 05</option>
                	<option value="06">Semana 06</option>
                	<option value="07">Semana 07</option>
                	<option value="08">Semana 08</option>
                	<option value="09">Semana 09</option>
                	<option value="10">Semana 10</option>
                	<option value="11">Semana 11</option>
                	<option value="12">Semana 12</option>
                	<option value="13">Semana 13</option>
                	<option value="14">Semana 14</option>
                	<option value="15">Semana 15</option>
                	<option value="16">Semana 16</option>
                	<option value="17">Semana 17</option>
                	<option value="18">Semana 18</option>
                	<option value="19">Semana 19</option>
                	<option value="20">Semana 20</option>
                	<option value="21">Semana 21</option>
                	<option value="22">Semana 22</option>
                	<option value="23">Semana 23</option>
                	<option value="24">Semana 24</option>
                	<option value="25">Semana 25</option>
                	<option value="26">Semana 26</option>
                	<option value="27">Semana 27</option>
                	<option value="28">Semana 28</option>
                	<option value="29">Semana 29</option>
                	<option value="30">Semana 30</option>
                	<option value="31">Semana 31</option>
                	<option value="32">Semana 32</option>
                	<option value="33">Semana 33</option>
                	<option value="34">Semana 34</option>
                	<option value="35">Semana 35</option>
                	<option value="36">Semana 36</option>
                	<option value="37">Semana 37</option>
                	<option value="38">Semana 38</option>
                	<option value="39">Semana 39</option>
                	<option value="40">Semana 40</option>
                	<option value="41">Semana 41</option>
                	<option value="42">Semana 42</option>
                	<option value="43">Semana 43</option>
                	<option value="44">Semana 44</option>
                	<option value="45">Semana 45</option>
                	<option value="46">Semana 46</option>
                	<option value="47">Semana 47</option>
                	<option value="48">Semana 48</option>
                	<option value="49">Semana 49</option>
                	<option value="50">Semana 50</option>
                	<option value="51">Semana 51</option>
                	<option value="52">Semana 52</option>
                </select>
                <?php } ?>
            </div>
        </div>
        <div id="calendarcontent">
			<div id='calendar'></div>
		</div>
		<div id="grafica" style="display: none">
	    	<div id="chartdiv"></div>
	    	<div id="chartdiv2"></div>
	    </div>
	</div>
</div>


<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev"><</a>
    <a class="next">></a>
    <a class="close">x</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">x</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="archivos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Archivo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <div class=" form-group row">
               <div class="col-md-12" id="modal_video">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	var $ = jQuery.noConflict();
	var urlList = '{{ url("listadoactividades") }}/{{ Auth::user()->id }}';
</script>
<script src='{{ url("/") }}/components/newfullcalendar/lib/moment.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.min.js" integrity="sha256-MDHWvW/uBfL2FEZwh9gqePZTrrxfCgNlQelyThMV8/c=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/locale-all.js" integrity="sha256-6cQXtrul38ChfKH+ojSity2Aifa8dSvrdRiYUiQrd2M=" crossorigin="anonymous"></script>

<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script src="{{ url('/') }}/js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<!--tooltip-->
<!--<script src="{{ url('/') }}/assets/js/king-common.js"></script>
<script src="{{ url('/') }}/demo-style-switcher/assets/js/deliswitch.js"></script>-->

<script src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
<script src="{{ url('/') }}/components/amchart/js/funnel.js"></script>
<script src="{{ url('/') }}/components/amchart/js/serial.js"></script>
<script src="{{ url('/') }}/components/amchart/js/pie.js"></script>
<script src="{{ url('/') }}/components/amchart/js/gauge.js"></script>
<script src="{{ url('/') }}/components/amchart/js/radar.js"></script>
<script src="{{ url('/') }}/components/amchart/js/light.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/morris.min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/raphael-min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/morris.min.js"></script>




<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>

<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>


<script>
	$(function() {
		moment.locale('es');
		$("#tituloPrincipal").html("Bitacora de la semana " + moment().weeks() + " <br>"+ moment().day(0).format("MMMM D") + " - " + moment().day(6).format("MMMM D"));
		$('#calendar').fullCalendar({
			header: {
				left: '',
				right:''
			},
			defaultView: 'agendaWeek',
			defaultDate: moment(),
			locale: 'es',
			timezoneParam: 'America/Bogota',
            allDaySlot: false,
			navLinks: true,
			selectable: true,
			selectHelper: true,
			axisFormat : "HH:mm",
    		agenda : "HH:mm",
			weekNumbers: true,
			weekNumbersWithinDays: true,
			firstDay:0,
			columnFormat: 'dddd D',
			editable: true,
			eventLimit: true,
			select: function(start, end) {
				<?php if($permiso_crear_actividad=="Si"){ ?>
				var diasemanaActual = moment().day();
				var semana = moment(start);
				var semanaActual = moment();
				var diasAntes = moment().subtract(4, 'day');
				console.log("semana-> ",semana.format('MMMM DD YYYY, h:mm:ss a'), " dias-> ",diasAntes.format('MMMM DD YYYY, h:mm:ss a'));
				if (semana >= moment().subtract(4, 'day') && semana <= semanaActual) {
					swal({
					  	title: 'Crear bitacora',
					  	animation: "slide-from-top",
					  	showCancelButton: true,
					  	confirmButtonText: 'Guardar',
					  	cancelButtonText: 'Cerrar',
					  	showLoaderOnConfirm: true,
					  	html:`<?php print $codigoHTML; ?>`,
					  	preConfirm: function () {
						    return new Promise(function (resolve, reject) {
						    	if($("form").parsley().validate()){
						    		actividad = $("#detalle_actividad").val();
									resultado = $("#detalle_resultado").val();
						    		if (actividad.length >= 50 && resultado.length >= 50) {
						    			var element = $("#formAgendaSmart").parent().parent().parent();

										comparadorStart = moment(start).format('DD');
										comparadorEnd = moment(end).format('DD');
										console.log("si en este caso muesta q es mayor-> "+comparadorEnd+" > "+comparadorStart+" - "+moment(end).format('H:mm')+" - "+moment(start).format('H:mm'));
										if (comparadorStart < comparadorEnd) {


											var fd = new FormData(document.getElementById("formAgendaSmart"));
											fd.append("fecha_actividad", moment(start).format('YYYY-MM-DD'));
											fd.append("tiempo_inicio", moment(start).format('H:mm'));
											fd.append("tiempo_fin", moment(end).format('23:59'));
											if (moment(start).format('H')>12) {
												fd.append("jornada", "T");
											}else{
												fd.append("jornada", "M");
											}

											var request = $.ajax({
											  url: "{{ url('bitacora') }}",
											  type: "POST",
											  data: fd,
											  processData: false,
											  contentType: false
											});

											request.done(function( msg ) {
											  	resolve(msg)
											});

											request.fail(function( jqXHR, textStatus ) {
												resolve(false)
											});
										}else{
											var fd = new FormData(document.getElementById("formAgendaSmart"));
											fd.append("fecha_actividad", moment(start).format('YYYY-MM-DD'));
											fd.append("tiempo_inicio", moment(start).format('H:mm'));
											fd.append("tiempo_fin", moment(end).format('H:mm'));
											if (moment(start).format('H')>12) {
												fd.append("jornada", "T");
											}else{
												fd.append("jornada", "M");
											}
											var request = $.ajax({
											  url: "{{ url('bitacora') }}",
											  type: "POST",
											  data: fd,
											  processData: false,
											  contentType: false
											});

											request.done(function( msg ) {
											  	resolve(msg)
											});

											request.fail(function( jqXHR, textStatus ) {
												resolve(false)
											});
										}

						    		}else{
						    			var element = $("#formAgendaSmart").parent().parent().parent();
									    $(element).addClass('animated rubberBand');
									    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
									      $(element).removeClass('animated rubberBand');
									    });
									    reject('Perdón!, Es necesario que el detalle de la actividad y el detalle de resultado superen los 50 caracteres.')
						    		}

								}else{
								    var element = $("#formAgendaSmart").parent().parent().parent();
								    $(element).addClass('animated rubberBand');
								    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
								      $(element).removeClass('animated rubberBand');
								    });
								    reject('Perdón!, Es necesario que ingrese una oportunidad.')
								}
						    })
					  	},
					  	onOpen: function () {
                $(document).on('change', '#tipo_actividad', function() {
                  var valor = $(this).val();
                  console.log("cambio", valor, '===',"Empresa");
                  $.ajax( "/subactividad?term="+valor )
                  .always(function(data) {
                    if(data){
                      var html = '<p>Subactividad</p><select id="subtipo" name="subtipo" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                      $.each( data, function( key, value ) {
                        html += '<option value="'+value.id+'">'+value.text+'</option>';
                      });
                      html += '</select>';
                      $(".addOtro").html("");
                      $(".contSubTipo").html(html);
                      console.log(data);
                    }
                  });
                  if (valor === "Empresa") {
                    $.ajax( "/empresa" )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa_id" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                        $.each( data, function( key, value ) {
                          html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                        });
                        html += '</datalist>';
                        $(".addOtro").html(html);
                        console.log(data);
                      }
                    });
                  }else if (valor === "Oportunidad") {
                    $.ajax( "/ssoportunidad" )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Oportunidad</p><input list="browsers" id="oportunidad" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                        $.each( data, function( key, value ) {
                          html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                        });
                        html += '</datalist>';
                        $(".addOtro").html(html);
                        console.log(data);
                      }
                    });
                  }
                });

					  		$('body').on('change','#select_traer_datos',function(){
						        if ($(this).val() === "") {
						        	$("#contenedorOculto").hide();
						        }else{
						        	$("#contenedorOculto").show();
						        	if ($(this).val() === "P") {
						        		$("#divAgendaVisita").hide();
										$("#divPlanTrabajo").show();
										$("#PlanTrabajo").attr("required", true);
										$("#agendaVisita").attr("required", false);
										$("#PlanTrabajo").attr("name", "id_tipo");
										$("#agendaVisita").attr("name", "");

						        	}else if($(this).val() === "G"){
						        		$("#divPlanTrabajo").hide();
										$("#divAgendaVisita").show();
										$("#PlanTrabajo").attr("required", false);
										$("#agendaVisita").attr("required", true);
										$("#PlanTrabajo").attr("name", "");
										$("#agendaVisita").attr("name", "id_tipo");
						        	}else{
						        		$("#PlanTrabajo").attr("required", false);
										$("#agendaVisita").attr("required", false);
										$("#PlanTrabajo").attr("name", "");
										$("#agendaVisita").attr("name", "");
						        		$("#divAgendaVisita").hide();
										$("#divPlanTrabajo").hide();
						        	}
						        }
						        $("#oportunidad").val("");
				            	$("#select_tipo_actividad").val("");
				            	$("#detalle_actividad").val("");
				            	$("#PlanTrabajo").val("");
				            	$("#agendaVisita").val("");
				            	$(".opprohtml").html("");
						    });

						    $('body').on('change','#PlanTrabajo',function(){
						    	$(".opprohtml").html("");
						        if ($(this).val() != "") {
						        	id = $(this).val();
						        	$.ajax({
							            url: "{{ url('/') }}/ajaxplantrabajo/"+id,
							            cache: false,
							            contentType: false,
							            processData: false,
							            type: 'GET',
							            success: function(e){
							            	$("#oportunidad").val(e.oportunidad);
							            	$("#select_tipo_actividad").val(e.tipo_actividad);
							            	$("#detalle_actividad").val(e.observaciones);
							            	funOportinidadhtml();
							              console.log(e);
							            }
							        });
						        }
						    });

						    $('body').on('change','#agendaVisita',function(){
						    	$(".opprohtml").html("");
						        if ($(this).val() != "") {
						        	id = $(this).val();
						        	$.ajax({
							            url: "{{ url('/') }}/ajaxagendavisita/"+id,
							            cache: false,
							            contentType: false,
							            processData: false,
							            type: 'GET',
							            success: function(e){
							            	$("#oportunidad").val(e.oportunidad);
							            	$("#select_tipo_actividad").val("");
							            	$("#detalle_actividad").val(e.detalle);
							              console.log(e);
							            }
							        });
						        }
						    });

				  			$("#divPegarArchivo").html(`
				            <div class="file-loading">
							    <input id="input-42" name="archivos[]" type="file" multiple accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.ms-powerpoint,image/*">
							</div>`);

						    $("#input-42").fileinput({
						    	language: "es",
						        maxFileCount: 10,
						         showUpload: false
						        //allowedFileExtensions: ["jpg", "gif", "png", "txt"]
						    });

					  		if (semana >= moment().subtract(2, 'day') && semana <= semanaActual) {
				            	$(".card-header").css('background-color', '#051d60');
				                $(".panel-title p").css('color', '#051d60');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
				                $("#tiempofuera").val(false);
				                $("#parrafoOk").text("Está¡ llenando una actividad en la fecha acordada.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#1d92af", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }else{
				                $(".card-header").css('background-color', '#f0ad4e');
				                $(".panel-title p").css('color', '#f0ad4e');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
				                $("#tiempofuera").val(true);
				                $("#parrafoOk").text("Está¡ llenando una actividad fuera de fecha.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#rgb(232, 72, 73);", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }
							hora_inicio = moment(start).format('H:mm');
					        hora_fin = moment(end).format('H:mm');
                            fecha_inicio = moment(start).format('YYYY-MM-DD');
					        fecha_fin = moment(end).format('YYYY-MM-DD');
                            if(fecha_fin != fecha_inicio){
                                console.log('si es mayor esto');
                                hora_fin = '23:59'
                            }
					        $("#parrafoIniFin").html("De "+hora_inicio+" a "+hora_fin);
					        if(hora_inicio!=='' && hora_fin!==''){
					            hora_inicio = (hora_inicio).split(":");
					            hora_fin = (hora_fin).split(":");
					            t1 = new Date();
					            t2 = new Date();
//                                if(){
//
//                                }
                                console.log('hora1 -> ',hora_fin[0], 'hora1 -> ', hora_fin[1]);
					            t1.setHours(hora_fin[0], hora_fin[1]);
					            t2.setHours(hora_inicio[0], hora_inicio[1]);
					            //Aqui hago la resta
					            t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes());

					            /* pasar los minutos a horas y sumarle las horas */
					            horas=0;
					            horas=(t1.getMinutes() ? (((t1.getMinutes()) / 60)+(t1.getHours())) : t1.getHours());
					            horas=horas.toFixed(1);

					            A="{{ str_replace(',', '', (Auth::user()->salario_basico)) }}";
					            A=parseInt(A);
					            B="{{ str_replace(',', '', (Auth::user()->bonificacion_fija)) }}";
					            B=parseInt(B);
					            C="{{ str_replace(',', '', (Auth::user()->aux_transporte)) }}";
					            C=parseInt(C);
					            D="{{ str_replace(',', '', (Auth::user()->aux_alimentacion)) }}";
					            D=parseInt(D);
					            E="{{ str_replace(',', '', (Auth::user()->aux_vivienda)) }}";
					            E=parseInt(E);
					            F="{{ str_replace(',', '', (Auth::user()->otros_auxilios)) }}";
					            F=parseInt(F);
					            P="{{ str_replace('%', '', ($parafiscal->porcentaje)) }}";
					            P=(parseInt(P))/100;
					            ValorMes=((A+B)*(1+P))+C+D+E+F;
					            ValorTotal=(ValorMes/26/8)*horas;
					            ValorTotal=parseInt(ValorTotal);
					            $('.mis_horas').val(horas);
					            $('#mis_horas').html(horas);
					            $('#total_mis_horas').val(ValorTotal);
					            /*sumar todos los totales*/
					            valSumaT=0;
					            $(".sumarSueldo").each(function() {
					                var valSuma = verificar(this.id);
					                valSumaT = parseInt(valSumaT) + parseInt(valSuma);
					            });
					            valSumaT = parseInt(valSumaT) + parseInt(ValorTotal);
					            valSumaT = valSumaT.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            valSumaT = valSumaT.split('').reverse().join('').replace(/^[\,]/,'');
					            $('#total_general').val(valSumaT);
					            /*fin sumar todos los totales*/
					            ValorTotal = ValorTotal.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            ValorTotal = ValorTotal.split('').reverse().join('').replace(/^[\,]/,'');
					            $('.total_mis_horas').html('$ '+ValorTotal);
					            /*diff="La diferencia es de: " + (t1.getHours() ? t1.getHours() + (t1.getHours() > 1 ? " horas" : " hora") : "") + (t1.getMinutes() ? ", " + t1.getMinutes() + (t1.getMinutes() > 1 ? " minutos" : " minuto") : "")*/
					        }

					        $('.detalle_pendiente').keyup(function() {
						        var chars = $(this).val().length;
						        id=$(this).attr('id');
						        if (chars>0) {
									$("#tipo_pendiente"+id).prop('required',true);
									$("#fechPendiente"+id).prop('required',true);
						        }else{
						        	$("#tipo_pendiente"+id).prop('required',false);
									$("#fechPendiente"+id).prop('required',false);
						        }
						        if(chars>=50){
						            $('.num_pen_columna'+id).css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_pen_columna'+id).css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_pen_act'+id).html(chars+' Caracteres '+icon);
						    });
						    /*fin detalle pendientes */


						    /*detalle de la resultado */
						    $('#detalle_resultado').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_resu_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_resu_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_resu_act').html(chars+' Caracteres '+icon);
						    });
						    /*fin detalles de la resultado*/

						    /*detalle de la actividad */
						    $('#detalle_actividad').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_act_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_act_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_car_act').html(chars+' Caracteres '+icon);
						    });
						    /*fin detalles de la actividad*/

						    /*Ayuda estado actividad */
						    $('body').on('change','#estado_actividad',function(){
						        @foreach($estado_actividades as $e_a)
						            if('{{ $e_a->estado }}'==($(this).val())){
						                des='{{ $e_a->descripcion }}';
						            }
						        @endforeach
						        $('#msj_estado_actividad').attr('data-original-title',des);
						    });
						    /*fin Ayuda estado actividad */

						    /* Ayuda tipo Actividad */
						    $('body').on('change','#select_tipo_actividad',function(){
						        id=$('#select_tipo_actividad').val();
						        if(id!=''){
						            $.ajax({
						            url: "{{ url('/') }}/ayudatipoactividad/"+id,
						            cache: false,
						            contentType: false,
						            processData: false,
						            type: 'GET',
						            success: function(e){
						              $('#msj_ta').html(e.msj);
						              $('#titulo_t_a').html(id);
						              $('.fila_msj').css('display','block');
						            }
						          });
						        }
						    })

						    $(".dinero").maskMoney();
					        $('.time').bootstrapMaterialDatePicker({
					            time: true,
					            date: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            format : 'HH:mm',
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar',
					            weekStart : 0
					        }).on('close', function(e){
					            var a = moment($("#timeend").val(), 'HH:mm');
					            var b = moment($(".time").val(), 'HH:mm');
					            c = a.diff(b);
					            if (c<0) {
					                $("#timeend").val("");
					                swal("La hora final no puede ser menor a la inicial");
					            }
					        });

					        $('#timeend').bootstrapMaterialDatePicker({
					                time: true,
					                date: false,
					                clearButton: true,
					                nowButton: true,
					                lang: 'es',
					                format : 'HH:mm',
					                cancelText : 'Cancelar',
					                okText: 'Aceptar',
					                nowText: 'Nuevo',
					                clearText: 'Limpiar',
					                weekStart : 0
					        }).on('close', function(e){
					            var a = moment($("#timeend").val(), 'HH:mm');
					            var b = moment($(".time").val(), 'HH:mm');
					            c = a.diff(b);
					            if (c<0) {
					                $("#timeend").val("");
					                $("#mis_horas").html("0");
					                $("#total_mis_horas").val("0");
					                $(".total_mis_horas").html("$ 0");
					                swal("La hora final no puede ser menor a la inicial");
					            }
					        });

					        $('.date').bootstrapMaterialDatePicker({
					            time: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            minDate : new Date(),
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar'
					        });

					        $('#fechaactividad').bootstrapMaterialDatePicker({
					            time: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            minDate : moment().subtract(20, 'day'),
					            maxDate : new Date(),
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar'
					        }).on('close', function(e){
					            var a = moment($("#fechaactividad").val(), 'YYYY-MM-DD');
					            var b = moment().subtract(2, 'day');
					            c = a.diff(b);
					            if (c<0) {
					                $(".card-header").css('background-color', '#f0ad4e');
					                $(".panel-title p").css('color', '#f0ad4e');
					                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
					                $("#tiempofuera").val(true);
					                swal("Está¡ ingresando una actividad fuera de fecha");
					            }else{
					                $(".card-header").css('background-color', '#051d60');
					                $(".panel-title p").css('color', '#051d60');
					                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
					                $("#tiempofuera").val(false);
					            }
					        });

					        var xx=1;
						    $( "#agregarHorasHombre" ).click(function() {
							  xx++;
						        var i=xx;
						        dato = `<div class="row animated rollIn" id="D`+i+`">
						                  <div class="col-4">
						                    <div class="form-group">
						                        <select name="hhombre[`+i+`][cargo]" id="textCargo`+i+`" class="form-control" onchange="calcularValorHH(`+i+`)">
						                            <option value="">Seleccione un cargo</option>
						                            @foreach($cargos as $cargo)
						                                <option value="{{ $cargo->nombre }}">{{ $cargo->nombre }}</option>
						                            @endforeach
						                        </select>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input type="number" id="numberCantidad`+i+`" name="hhombre[`+i+`][cantidad]" class="form-control" placeholder="0" onchange="calcularValorHH(`+i+`)">
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input class="form-control" id="numberHoras`+i+`" type="number" name="hhombre[`+i+`][horas]" placeholder="0" onchange="calcularValorHH(`+i+`)">
						                    </div>
						                  </div>
						                  <div class="col-3">
						                    <div class="form-group">
						                        <input class="form-control sumarSueldo" id="numberValor`+i+`" type="text" name="hhombre[`+i+`][valor]" placeholder="$0" readonly>
						                    </div>
						                  </div>
						                  <div class="col-1">
						                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarhHombre('#D`+i+`')"><i class="fa fa-minus text-danger"></i></button>
						                  </div>
						                </div>`;
						        $( "#horashombrediv" ).after(dato);
						        $('.date').bootstrapMaterialDatePicker({
						            time: false,
						            clearButton: true,
						            lang: 'es',
						            minDate : new Date()
						        });
							});
						    var i = 1;
							$( "#agregarPendientes" ).click(function() {
						        i++;
						        dato = `<div class="row animated rollIn" id="T`+i+`">
						                  <div class="col-7">
						                    <div class="form-group">
						                        <textarea class="form-control detalle_pendiente" id="`+i+`" name="pendientes[`+i+`][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente"></textarea>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <select name="pendientes[`+i+`][tipo_pendiente]" id="tipo_pendiente`+i+`" class="form-control">
						                            <option value="">Seleccione una tipo de actividad</option>
						                            <?php foreach($tipos_actividades as $t_a){ ?>
						                            <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
						                            <?php } ?>
						                        </select>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input class="form-control date" type="text" id="fechPendiente`+i+`" name="pendientes[`+i+`][fecha_pendiente]" placeholder="Fecha de Cierre">
						                    </div>
						                  </div>
						                  <div class="col-1">
						                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarPendientes('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
						                  </div>
						                </div>
						                <div class="row">
						                    <div class="col-md-7 num_pen_columna`+i+`">
						                        <a id="num_pen_act`+i+`"></a>
						                    </div>
						                    <div class="col-md-5 num_pen_columna`+i+`">

						                    </div>
						                </div>`;
						        $( "#pendientes" ).after(dato);
						        $('.date').bootstrapMaterialDatePicker({
						            time: false,
						            clearButton: true,
						            lang: 'es',
						            minDate : new Date()
						        });
						        /* detalle pendientes */
						        $('.detalle_pendiente').keyup(function() {
						            var chars = $(this).val().length;
						            id=$(this).attr('id');
						            if (chars>0) {
										$("#tipo_pendiente"+id).prop('required',true);
										$("#fechPendiente"+id).prop('required',true);
							        }else{
							        	$("#tipo_pendiente"+id).prop('required',false);
										$("#fechPendiente"+id).prop('required',false);
							        }
						            if(chars>=50){
						                $('.num_pen_columna'+id).css('background-color','lightgreen');
						                icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						            }else if(chars<=50){
						                $('.num_pen_columna'+id).css('background-color','yellow');
						                icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						            }
						            $('#num_pen_act'+id).html(chars+' Caracteres '+icon);
						        });
						        /*fin detalle pendientes */
						    });

						    funOportinidadhtml();
						}
					}).then(function (result) {
						if (result.success) {
							$('#calendar').fullCalendar('next');
							$('#calendar').fullCalendar('prev');
							swal("Exito!","Actividad guardado con exito.","success");
						}else{
							swal("Error!","Algo ha salido mal.","warning");
						}
					}).catch(swal.noop)
				}else{
					swal("Alto!","El tiempo de hacer cambios se ha agotado.","warning");
				}
				<?php }else{ ?>
					swal("Alto!","No tiene permisos para crear actividad.","warning");
				<?php } ?>
			},
			buttonIcons: true,  // allow "more" link when too many events
			events: function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(start).format('YYYY-MM-DD'),
		                end: moment(end).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  // a must be equal to b
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}

						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
					},
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    },
			timeFormat: 'HH:mm:ss',
			eventClick: function(event) {
				var diasemanaActual = moment().day();
				var semana = moment(event.start);
				var semanaActual = moment();
				if ((semana >= moment().subtract(4, 'day') && semana <= semanaActual) || {{ Auth::user()->id }} === 1 ) {
				    swal({
					  	title: 'Editar bitacora',
					  	animation: "slide-from-top",
					  	showCancelButton: true,
					  	confirmButtonText: 'Guardar',
					  	cancelButtonText: 'Cerrar',
					  	showLoaderOnConfirm: true,
					  	html:`<?php print $codigoHTML; ?>`,
					  	preConfirm: function () {
						    return new Promise(function (resolve, reject) {
						    	if($("form").parsley().validate()){
						    		actividad = $("#detalle_actividad").val();
										resultado = $("#detalle_resultado").val();
						    		if (actividad.length >= 50 && resultado.length >= 50) {
						    			var element = $("#formAgendaSmart").parent().parent().parent();
									    var fd = new FormData(document.getElementById("formAgendaSmart"));
										fd.append("fecha_actividad", moment(event.start).format('YYYY-MM-DD'));
										fd.append("tiempo_inicio", moment(event.start).format('H:mm'));
										fd.append("tiempo_fin", moment(event.end).format('H:mm'));
										fd.append("id", event.id);
										if (moment(event.start).format('H')>12) {
											fd.append("jornada", "T");
										}else{
											fd.append("jornada", "M");
										}
										var request = $.ajax({
										  url: "{{ url('bitacora/editar') }}",
										  type: "POST",
										  data: fd,
										  processData: false,
										  contentType: false
										});

										request.done(function( msg ) {
										  	resolve(msg)
										});

										request.fail(function( jqXHR, textStatus ) {
											resolve(false)
										});
						    		}else{
						    			var element = $("#formAgendaSmart").parent().parent().parent();
									    $(element).addClass('animated rubberBand');
									    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
									      $(element).removeClass('animated rubberBand');
									    });
									    reject('Perdón!, Es necesario que el detalle de la actividad y el detalle de resultado superen los 50 caracteres.')
						    		}
								}else{
								    var element = $("#formAgendaSmart").parent().parent().parent();
								    $(element).addClass('animated rubberBand');
								    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
								      $(element).removeClass('animated rubberBand');
								    });
								    reject('Perdón!, Es necesario que ingrese una oportunidad.')
								}
						    })
					  	},
					  	onOpen: function () {
                $(document).on('change', '#tipo_actividad', function() {
                  var valor = $(this).val();
                  console.log("cambio", valor, '===',"Empresa");
                  $.ajax( "/subactividad?term="+valor )
                  .always(function(data) {
                    if(data){
                      var html = '<p>Subactividad</p><select id="subtipo" name="subtipo" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                      $.each( data, function( key, value ) {
                        html += '<option value="'+value.id+'">'+value.text+'</option>';
                      });
                      html += '</select>';
                      $(".addOtro").html("");
                      $(".contSubTipo").html(html);
                      console.log(data);
                    }
                  });
                  if (valor === "Empresa") {
                    $.ajax( "/empresa" )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa_id" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                        $.each( data, function( key, value ) {
                          html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                        });
                        html += '</datalist>';
                        $(".addOtro").html(html);
                        console.log(data);
                      }
                    });
                  }else if (valor === "Oportunidad") {
                    $.ajax( "/ssoportunidad" )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Oportunidad</p><input list="browsers" id="oportunidad" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                        $.each( data, function( key, value ) {
                          html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                        });
                        html += '</datalist>';
                        $(".addOtro").html(html);
                        console.log(data);
                      }
                    });
                  }
                });

					  		if (event.origen_datos === null || event.origen_datos === "") {
					  			event.origen_datos = "N";
					  		}
					  		$("#select_traer_datos").val(event.origen_datos);
					  		function funTraerDatos() {
					  			if ($("#select_traer_datos").val() === "") {
						        	$("#contenedorOculto").hide();
						        }else{
						        	$("#contenedorOculto").show();
						        	if ($("#select_traer_datos").val() === "P") {
						        		$("#divAgendaVisita").hide();
										$("#divPlanTrabajo").show();
										$("#PlanTrabajo").attr("required", true);
										$("#agendaVisita").attr("required", false);
										$("#PlanTrabajo").attr("name", "id_tipo");
										$("#agendaVisita").attr("name", "");

						        	}else if($("#select_traer_datos").val() === "G"){
						        		$("#divPlanTrabajo").hide();
										$("#divAgendaVisita").show();
										$("#PlanTrabajo").attr("required", false);
										$("#agendaVisita").attr("required", true);
										$("#PlanTrabajo").attr("name", "");
										$("#agendaVisita").attr("name", "id_tipo");
						        	}else{
						        		$("#PlanTrabajo").attr("required", false);
										$("#agendaVisita").attr("required", false);
										$("#PlanTrabajo").attr("name", "");
										$("#agendaVisita").attr("name", "");
						        		$("#divAgendaVisita").hide();
										$("#divPlanTrabajo").hide();
						        	}
						        }
						        $("#oportunidad").val("");
				            	$("#select_tipo_actividad").val("");
				            	$("#detalle_actividad").val("");
				            	$("#PlanTrabajo").val("");
				            	$("#agendaVisita").val("");
				            	$(".opprohtml").html("");
					  		}

					  		funTraerDatos();

			            	if (event.origen_datos == "G") {
					  			$("#agendaVisita").val(event.id_tipo);
					  		}else if(event.origen_datos == "P"){
					  			$("#PlanTrabajo").val(event.id_tipo);
					  		}

					  		$('body').on('change','#select_traer_datos',function(){
						        funTraerDatos();
						    });
						    $('body').on('change','#PlanTrabajo',function(){
						    	$(".opprohtml").html("");
						        if ($(this).val() != "") {
						        	id = $(this).val();
						        	$.ajax({
							            url: "{{ url('/') }}/ajaxplantrabajo/"+id,
							            cache: false,
							            contentType: false,
							            processData: false,
							            type: 'GET',
							            success: function(e){
							            	$("#oportunidad").val(e.oportunidad);
							            	$("#select_tipo_actividad").val(e.tipo_actividad);
							            	$("#detalle_actividad").val(e.observaciones);
							            	funOportinidadhtml();
							              	console.log(e);
							            }
							        });
						        }
						    });

						    $('body').on('change','#agendaVisita',function(){
						    	$(".opprohtml").html("");
						        if ($(this).val() != "") {
						        	id = $(this).val();
						        	$.ajax({
							            url: "{{ url('/') }}/ajaxagendavisita/"+id,
							            cache: false,
							            contentType: false,
							            processData: false,
							            type: 'GET',
							            success: function(e){
							            	$("#oportunidad").val(e.oportunidad);
							            	$("#select_tipo_actividad").val("");
							            	$("#detalle_actividad").val(e.detalle);
							              console.log(e);
							            }
							        });
						        }
						    });

						    // aqui termina

					  		$("#divPegarArchivo").html(`
				            <div class="file-loading">
							    <input id="input-42" name="archivos[]" type="file" multiple>
							</div>`);

						    $("#input-42").fileinput({
						    	language: "es",
						        maxFileCount: 10,
						         showUpload: false
						        //allowedFileExtensions: ["jpg", "gif", "png", "txt"]
						    });

				            if (semana >= moment().subtract(2, 'day') && semana <= semanaActual) {
				            	$(".card-header").css('background-color', '#051d60');
				                $(".panel-title p").css('color', '#051d60');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
				                $("#tiempofuera").val(false);
				                $("#parrafoOk").text("Está¡ llenando una actividad en la fecha acordada.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#1d92af", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }else{
				                $(".card-header").css('background-color', '#f0ad4e');
				                $(".panel-title p").css('color', '#f0ad4e');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
				                $("#tiempofuera").val(true);
				                $("#parrafoOk").text("Está¡ llenando una actividad fuera de fecha.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#rgb(232, 72, 73);", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }

				            $("#resGastosAsing").val(event.responsable_gasto);
					  	// 	$("#oportunidad").val(event.oportunidad_id);
							// $("#select_tipo_actividad").val(event.tipo_actividad);
              console.log(event.tipo_actividad, "tipo actividad");
              $("#tipo_actividad").val(event.tipo_actividad);

              var valor = $("#tipo_actividad").val();
              $.ajax( "/subactividad?term="+valor )
              .always(function(data) {
                if(data){
                  var html = '<p>Subactividad</p><select id="subtipo" name="subtipo" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                  $.each( data, function( key, value ) {
                    html += '<option value="'+value.id+'">'+value.text+'</option>';
                  });
                  html += '</select>';
                  $(".addOtro").html("");
                  $(".contSubTipo").html(html);
                  $("#subtipo").val(event.subtipo);
                  console.log(data);
                }
              });
              if (valor === "Empresa") {
                $.ajax( "/empresa" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa_id" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#empresa_id").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }else if (valor === "Oportunidad") {
                $.ajax( "/ssoportunidad" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Oportunidad</p><input list="browsers" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#oportunidad").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }
							$("#estado_actividad").val(event.estado_actividad);
							$("#detalle_actividad").val(event.detalle_actividad);
							$("#detalle_resultado").val(event.detalle_resultado);
							funOportinidadhtml();
							$("#total_h_hombre").val(event.total_h_hombre);
							$("#s1").val(event.alimentacion);
							$("#s2").val(event.transportes_internos);
							$("#s3").val(event.transportes_intermunicipales);
							$("#s4").val(event.tiquete_aereo);
							$("#s5").val(event.papeleria);
							$("#s6").val(event.invitacion_cliente);
							$("#s7").val(event.alquiler_vehiculo);
							$("#s8").val(event.gasolina_pasajes);
							$("#s9").val(event.hotel);
							$("#s10").val(event.otros);
							$("#total").val(event.total);
							$("#total_general").val(event.total_general);

							j=1;
							i=1;
							html="";
							$.each(event.pendientes, function( index, value ) {
							  	i++;
								html =`
		                          <div class="col-7">
		                            <div class="form-group">
		                                <textarea class="form-control detalle_pendiente" id="`+i+`" name="pendientes[`+i+`][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente">`+value.detalle+`</textarea>
		                            </div>
		                          </div>
		                          <div class="col-2">
		                            <div class="form-group">
		                                <select name="pendientes[`+i+`][tipo_pendiente]" id="tipo_pendiente`+i+`" class="form-control">
		                                    <option value="">Seleccione una tipo de actividad</option>
		                                    <?php foreach($tipos_actividades as $t_a){ ?>
		                                    <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
		                                    <?php } ?>
		                                </select>
		                            </div>
		                          </div>
		                          <div class="col-2">
		                            <div class="form-group">
		                                <input class="form-control date" type="text" id="fechPendiente`+i+`" name="pendientes[`+i+`][fecha_pendiente]" placeholder="Fecha de Cierre" value="`+value.fecha_ejecucion+`">
		                            </div>
		                          </div>`;
								$("#pendienteshtml").append(html);
								$("#tipo_pendiente"+i).val(value.tipo);
								console.log(i, "  --   ", value.tipo);
							});


							html=`<div class="row justify-content-md-center"><a name="titulobservaciones"></a>
						          <div class="col-md-12">
						            <h5 class="card-header"> Observaciones </h5>
						          </div>
						          <div class="col-md-12 centrado">`;
						    moment.locale('es');
							$.each(event.comentario, function( index, value ) {
								if (value.user.foto === "" || value.user.foto === null) {
								fotod = `<img src="http://via.placeholder.com/40x40?text=`+value.user.name+`" class="media-object img-circle">`;
								}else{
								fotod = `<img src="{{url('/')}}/images/file/clientes/`+value.user.foto+`" alt="`+value.user.name+`" class="media-object img-circle" style="max-width:40px">`;
								}
								html += `
								        <div class="media clearfix header-bottom">
								  <div class="media-left">
								    `+fotod+`
								  </div>
								  <div class="media-body" style="text-align: start;">
								  	<label style="text-align: start;font-size: 14px !important;">
								      `+value.user.name+` `+moment(value.created_at).calendar()+` <div class="label label-info">`+value.estado+`</div>
									</label><br>
								    <p class="text-muted username soloprimer normalp" style="font-size: 15px;font-weight: 400;">`+value.detalle+`</p>
								  </div>
								</div>`;
					        });
							html += `</div>
						        </div>`;
						    $("#lisComentariosId").html(html);
							html="";
							$.each(event.archivos, function( index, value ) {

								if (value.extension === "GIF" || value.extension === "PNG" || value.extension === "JPG" || value.extension === "gif" || value.extension === "png" || value.extension === "jpg"){
                                    html += `<div class="espaciofile"><a data-gallery="" title="`+value.archivo+`" href="{{url('/')}}/images/file/activity/`+value.archivo+`"><img src="{{ url('/') }}/images/iconciclos/images.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`"><p style="font-size: 10px;">`+value.nombre+`</p></a></div>`;
                                }else if (value.extension === "AVI" || value.extension === "MOV" || value.extension === "MP4" || value.extension === "avi" || value.extension === "mov" || value.extension === "mp4") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/video.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openVideo('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else if (value.extension === "PDF" || value.extension === "pdf") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/pdf.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else if (value.extension === "DOCX" || value.extension === "docx" || value.extension === "DOC" || value.extension === "doc") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/word.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else if (value.extension === "XLSX" || value.extension === "xlsx" || value.extension === "XLSM" || value.extension === "xlsm" || value.extension === "XML" || value.extension === "xml") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/excell.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else{
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/word.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }

							});
							$(".contentimg").html(html);
							$('[data-toggle="tooltip"]').tooltip();



							usuariovalid = 0;
							$.each(event.hhombre, function( index, value ) {
							  	j++;
							  	/*valSumaT = ;
					            valSumaT = valSumaT.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            valSumaT = valSumaT.split('').reverse().join('').replace(/^[\,]/,'');*/

							  	if ($.isNumeric(value.id_user)) {
							  		usuariovalid = value.id_user;
							  		html=`<input type="hidden" name="hhombre[0][id_user]" value="`+value.id_user+`">
			                      <div class="col-4" style="margin-left: 1%">
			                        <div class="form-group">
			                            <input type="hidden" name="hhombre[0][cargo]" value="`+value.cargo+`">
			                            <a style="font-size: x-large;">`+value.cargo+`</a>
			                        </div>
			                      </div>
			                      <div class="col-2">
			                        <div class="form-group">
			                            <input type="hidden" name="hhombre[0][cantidad]" value="`+value.cantidad+`">
			                            <a style="font-size: x-large;">`+value.cantidad+`</a>
			                        </div>
			                      </div>
			                      <div class="col-2">
			                        <div class="form-group">
			                            <input type="hidden" class="mis_horas" name="hhombre[0][horas]" value="`+value.horas+`">
			                            <a id="mis_horas" style="font-size: x-large;">`+value.horas+`</a>
			                        </div>
			                      </div>
			                      <div class="col-3">
			                        <div class="form-group">
			                            <input type="hidden" id="total_mis_horas" name="hhombre[0][valor]" value="`+value.valor+`">
			                            <a class="total_mis_horas" style="font-size: x-large;">$ `+value.valor+`</a>
			                        </div>
			                      </div>`;
			                      $("#manoobrahtml").html(html);
			                  	}else{
			                  		html =`
			                          <div class="col-4">
			                            <div class="form-group">
			                                <select name="hhombre[`+j+`][cargo]" id="textCargo`+j+`" class="form-control" onchange="calcularValorHH(`+j+`)">
			                                    <option value="">Seleccione un cargo</option>
			                                    @foreach($cargos as $cargo)
			                                    <option value="{{ $cargo->nombre }}">{{ $cargo->nombre }}</option>
			                                    @endforeach
			                                </select>
			                            </div>
			                          </div>
			                          <div class="col-2">
			                            <div class="form-group">
			                                <input type="number" id="numberCantidad`+j+`" name="hhombre[`+j+`][cantidad]" class="form-control" placeholder="0" onchange="calcularValorHH(`+j+`)" value="`+value.cantidad+`">
			                            </div>
			                          </div>
			                          <div class="col-2">
			                            <div class="form-group">
			                                <input class="form-control" id="numberHoras`+j+`" type="number" name="hhombre[`+j+`][horas]" placeholder="0" onchange="calcularValorHH(`+j+`)" value="`+value.horas+`">
			                            </div>
			                          </div>
			                          <div class="col-4">
			                            <div class="form-group">
			                                <input class="form-control sumarSueldo" id="numberValor`+j+`" type="text" name="hhombre[`+j+`][valor]" placeholder="$0" readonly value="`+value.valor+`">
			                            </div>
			                          </div>`;
			                          $("#horashombrehtml").append(html);
			                          $("#textCargo"+j).val(value.cargo);
			                  	}
							});

									hora_inicio = moment(event.start).format('H:mm');
					        hora_fin = moment(event.end).format('H:mm');


					        $("#parrafoIniFin").html("De "+hora_inicio+" a "+hora_fin);
									if ({{ Auth::user()->id }} === 1){
											var created_at = moment(event.created_at).calendar();
											var fuera_fecha = "";
											if (event.fuera_tiempo === 'true') {
												fuera_fecha = " actividad fuera de tiempo";
											}else{
												fuera_fecha = " en el tiempo";
											}

											 $("#validadorAdminDate").html(created_at + fuera_fecha );
									}

					        if(hora_inicio!=='' && hora_fin!=='' && usuariovalid === {{ Auth::user()->id }}){
					            hora_inicio = (hora_inicio).split(":"),
					            hora_fin = (hora_fin).split(":"),
					            t1 = new Date(),
					            t2 = new Date();
					            t1.setHours(hora_fin[0], hora_fin[1]);
					            t2.setHours(hora_inicio[0], hora_inicio[1]);
					            //AquÃ­ hago la resta
					            t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes());

					            /* pasar los minutos a horas y sumarle las horas */
					            horas=0;
					            horas=(t1.getMinutes() ? (((t1.getMinutes()) / 60)+(t1.getHours())) : t1.getHours());
					            horas=horas.toFixed(1);

					            A="{{ str_replace(',', '', (Auth::user()->salario_basico)) }}";
					            A=parseInt(A);
					            B="{{ str_replace(',', '', (Auth::user()->bonificacion_fija)) }}";
					            B=parseInt(B);
					            C="{{ str_replace(',', '', (Auth::user()->aux_transporte)) }}";
					            C=parseInt(C);
					            D="{{ str_replace(',', '', (Auth::user()->aux_alimentacion)) }}";
					            D=parseInt(D);
					            E="{{ str_replace(',', '', (Auth::user()->aux_vivienda)) }}";
					            E=parseInt(E);
					            F="{{ str_replace(',', '', (Auth::user()->otros_auxilios)) }}";
					            F=parseInt(F);
					            P="{{ str_replace('%', '', ($parafiscal->porcentaje)) }}";
					            P=(parseInt(P))/100;
					            ValorMes=((A+B)*(1+P))+C+D+E+F;
					            ValorTotal=(ValorMes/26/8)*horas;
					            ValorTotal=parseInt(ValorTotal);
					            $('.mis_horas').val(horas);
					            $('#mis_horas').html(horas);
					            $('#total_mis_horas').val(ValorTotal);
					            /*sumar todos los totales*/
					            valSumaT=0;
					            $(".sumarSueldo").each(function() {
					                var valSuma = verificar(this.id);
					                valSumaT = parseInt(valSumaT) + parseInt(valSuma);
					            });
					            valSumaT = parseInt(valSumaT) + parseInt(ValorTotal);
					            valSumaT = valSumaT.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            valSumaT = valSumaT.split('').reverse().join('').replace(/^[\,]/,'');
					            $('#total_general').val(valSumaT);
					            /*fin sumar todos los totales*/
					            ValorTotal = ValorTotal.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            ValorTotal = ValorTotal.split('').reverse().join('').replace(/^[\,]/,'');
					            $('.total_mis_horas').html('$ '+ValorTotal);
					            /*diff="La diferencia es de: " + (t1.getHours() ? t1.getHours() + (t1.getHours() > 1 ? " horas" : " hora") : "") + (t1.getMinutes() ? ", " + t1.getMinutes() + (t1.getMinutes() > 1 ? " minutos" : " minuto") : "")*/
					        }

					        $('.detalle_pendiente').keyup(function() {
						        var chars = $(this).val().length;
						        id=$(this).attr('id');
						        if (chars>0) {
									$("#tipo_pendiente"+id).prop('required',true);
									$("#fechPendiente"+id).prop('required',true);
						        }else{
						        	$("#tipo_pendiente"+id).prop('required',false);
									$("#fechPendiente"+id).prop('required',false);
						        }
						        if(chars>=50){
						            $('.num_pen_columna'+id).css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_pen_columna'+id).css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_pen_act'+id).html(chars+' Caracteres '+icon);
						    });
						    /*fin detalle pendientes */
						    /*detalle de la resultado */
						    $('#detalle_resultado').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_resu_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_resu_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_resu_act').html(chars+' Caracteres '+icon);
						    });
						    /*fin detalles de la resultado*/
						    /*detalle de la actividad */
						    $('#detalle_actividad').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_act_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_act_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_car_act').html(chars+' Caracteres '+icon);
						    });
						    /*fin detalles de la actividad*/
						    /*Ayuda estado actividad */
						    $('body').on('change','#estado_actividad',function(){
						        @foreach($estado_actividades as $e_a)
						            if('{{ $e_a->estado }}'==($(this).val())){
						                des='{{ $e_a->descripcion }}';
						            }
						        @endforeach
						        $('#msj_estado_actividad').attr('data-original-title',des);
						    });

						    $("#divEliminarButton").html(`<button class="btn swal2-modal swal2-styled btn-danger" id="button-delete" name="`+event.id+`" data-toggle="tooltip" data-placement="top" title="Eliminar actividad">
			                      <i class="fa fa-trash"></i>
			                    </button>`);
						    <?php if ($permiso_aprobar == "Si") { ?>
						    	$("#divRevisarButton").html(`
						    		<textarea class="form-control" id="detalle_aprobar" rows="3" placeholder="Por favor ingrese un comentario"></textarea>
						    	<button class="btn swal2-modal swal2-styled btn-success button-aprobar" data-title="Aprobado" name="`+event.id+`" data-toggle="tooltip" data-placement="top" title="Aprobar actividad">
			                      <i class="fa fa-check"></i>
			                    </button>
			                    <button class="btn swal2-modal swal2-styled btn-primary button-aprobar" data-title="NoAprobado" name="`+event.id+`" data-toggle="tooltip" data-placement="top" title="Actividad no aprobada">
			                      <i class="fa fa-times"></i>
			                    </button>`);

			                    $("body").on('mouseup', '.button-aprobar', function () {
			                    	var id = $(this).attr("name");
			                    	var text = $(this).attr("data-title");
			                    	var detalle_aprobar = $("#detalle_aprobar").val();
			                    	console.log(detalle_aprobar, " - ", detalle_aprobar.length);
			                    	if (detalle_aprobar.length<=0) {
			                    		swal("Por favor ingrese un comentario para gestionar la actividad");
			                    	}else{
								    	swal({
									      title: '¿Estas seguro?',
									      html: $('<div>')
									        .addClass('some-class')
									        .text('Deseas cambiar el estado de esta oportunidad!'),
									      animation: false,
									      customClass: 'animated tada',
									      type: 'success',
									      showCancelButton: true,
									      confirmButtonColor: '#3085d6',
									      cancelButtonColor: '#d33',
									      confirmButtonText: 'Si, continuar!',
									      cancelButtonText: 'Cancelar',
									      closeOnConfirm: false,
									      showLoaderOnConfirm: true,
									      showLoaderOnConfirm: true,
									      preConfirm: function () {
									        return new Promise(function (resolve) {
									          $.post( "{{ url('/') }}/actividad/aprobar", { id: id, text: text, detalleaprobar: detalle_aprobar }, function( data ) {
									            if (data.success) {
									            	$('#calendar').fullCalendar('next');
													$('#calendar').fullCalendar('prev');
									              swal('Â¡Exito!','Su actividad ha sido gestionada con exito.','success');
									              resolve()
									            }else{
									              swal("Algo salio mal, vuelve a intentar",'warning');
									              resolve()
									            }
									          });
									        })
									      },
									      allowOutsideClick: false
									    });
								    }
			                    });
						    <?php } ?>

						    $('body').on('click','#button-delete',function(){
						    	var id = $(this).attr("name");
						    	swal({
							      title: '¿Estas seguro?',
							      html: $('<div>')
							        .addClass('some-class')
							        .text('No podrás revertir esto!'),
							      animation: false,
							      customClass: 'animated tada',
							      type: 'warning',
							      showCancelButton: true,
							      confirmButtonColor: '#3085d6',
							      cancelButtonColor: '#d33',
							      confirmButtonText: 'Si, borrarlo!',
							      cancelButtonText: 'Cancelar',
							      closeOnConfirm: false,
							      showLoaderOnConfirm: true,
							      showLoaderOnConfirm: true,
							      preConfirm: function () {
							        return new Promise(function (resolve) {
							          $.get( "{{ url('/') }}/actividad/eliminar/"+id, function( data ) {
							            if (data.success) {
							            	$('#calendar').fullCalendar('next');
											$('#calendar').fullCalendar('prev');
							              swal('Â¡Eliminado!','Su actividad ha sido eliminada.','success');
							              resolve()
							            }else{
							              swal("Algo salio mal, vuelve a intentar",'warning');
							              resolve()
							            }
							          });
							        })
							      },
							      allowOutsideClick: false
							    });
						    });

						    /*fin Ayuda estado actividad */
						    /* Ayuda tipo Actividad */
						    $('body').on('change','#select_tipo_actividad',function(){
						        id=$('#select_tipo_actividad').val();
						        if(id!=''){
						            $.ajax({
						            url: "{{ url('/') }}/ayudatipoactividad/"+id,
						            cache: false,
						            contentType: false,
						            processData: false,
						            type: 'GET',
						            success: function(e){
						              $('#msj_ta').html(e.msj);
						              $('#titulo_t_a').html(id);
						              $('.fila_msj').css('display','block');
						            }
						          });
						        }
						    })

						    $(".dinero").maskMoney();
					        $('.time').bootstrapMaterialDatePicker({
					            time: true,
					            date: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            format : 'HH:mm',
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar',
					            weekStart : 0
					        }).on('close', function(e){
					            var a = moment($("#timeend").val(), 'HH:mm');
					            var b = moment($(".time").val(), 'HH:mm');
					            c = a.diff(b);
					            if (c<0) {
					                $("#timeend").val("");
					                swal("La hora final no puede ser menor a la inicial");
					            }
					        });

					        $('#timeend').bootstrapMaterialDatePicker({
					                time: true,
					                date: false,
					                clearButton: true,
					                nowButton: true,
					                lang: 'es',
					                format : 'HH:mm',
					                cancelText : 'Cancelar',
					                okText: 'Aceptar',
					                nowText: 'Nuevo',
					                clearText: 'Limpiar',
					                weekStart : 0
					        }).on('close', function(e){
					            var a = moment($("#timeend").val(), 'HH:mm');
					            var b = moment($(".time").val(), 'HH:mm');
					            c = a.diff(b);
					            if (c<0) {
					                $("#timeend").val("");
					                $("#mis_horas").html("0");
					                $("#total_mis_horas").val("0");
					                $(".total_mis_horas").html("$ 0");
					                swal("La hora final no puede ser menor a la inicial");
					            }
					        });

					        $('.date').bootstrapMaterialDatePicker({
					            time: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            minDate : new Date(),
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar'
					        });

					        $('#fechaactividad').bootstrapMaterialDatePicker({
					            time: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            minDate : moment().subtract(20, 'day'),
					            maxDate : new Date(),
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar'
					        }).on('close', function(e){
					            var a = moment($("#fechaactividad").val(), 'YYYY-MM-DD');
					            var b = moment().subtract(2, 'day');
					            c = a.diff(b);
					            if (c<0) {
					                $(".card-header").css('background-color', '#f0ad4e');
					                $(".panel-title p").css('color', '#f0ad4e');
					                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
					                $("#tiempofuera").val(true);
					                swal("Esta ingresando una actividad fuera de fecha");
					            }else{
					                $(".card-header").css('background-color', '#051d60');
					                $(".panel-title p").css('color', '#051d60');
					                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
					                $("#tiempofuera").val(false);
					            }
					        });

					        var xx=1;
						    $( "#agregarHorasHombre" ).click(function() {
							  xx++;
						        var i=xx;
						        dato = `<div class="row animated rollIn" id="D`+i+`">
						                  <div class="col-4">
						                    <div class="form-group">
						                        <select name="hhombre[`+i+`][cargo]" id="textCargo`+i+`" class="form-control" onchange="calcularValorHH(`+i+`)">
						                            <option value="">Seleccione un cargo</option>
						                            @foreach($cargos as $cargo)
						                                <option value="{{ $cargo->nombre }}">{{ $cargo->nombre }}</option>
						                            @endforeach
						                        </select>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input type="number" id="numberCantidad`+i+`" name="hhombre[`+i+`][cantidad]" class="form-control" placeholder="0" onchange="calcularValorHH(`+i+`)">
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input class="form-control" id="numberHoras`+i+`" type="number" name="hhombre[`+i+`][horas]" placeholder="0" onchange="calcularValorHH(`+i+`)">
						                    </div>
						                  </div>
						                  <div class="col-3">
						                    <div class="form-group">
						                        <input class="form-control sumarSueldo" id="numberValor`+i+`" type="text" name="hhombre[`+i+`][valor]" placeholder="$0" readonly>
						                    </div>
						                  </div>
						                  <div class="col-1">
						                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarhHombre('#D`+i+`')"><i class="fa fa-minus text-danger"></i></button>
						                  </div>
						                </div>`;
						        $( "#horashombrediv" ).after(dato);
						        $('.date').bootstrapMaterialDatePicker({
						            time: false,
						            clearButton: true,
						            lang: 'es',
						            minDate : new Date()
						        });
							});
						    var i = 1;
							$( "#agregarPendientes" ).click(function() {
						        i++;
						        dato = `<div class="row animated rollIn" id="T`+i+`">
						                  <div class="col-7">
						                    <div class="form-group">
						                        <textarea class="form-control detalle_pendiente" id="`+i+`" name="pendientes[`+i+`][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente"></textarea>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <select name="pendientes[`+i+`][tipo_pendiente]" id="tipo_pendiente`+i+`" class="form-control">
						                            <option value="">Seleccione una tipo de actividad</option>
						                            <?php foreach($tipos_actividades as $t_a){ ?>
						                            <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
						                            <?php } ?>
						                        </select>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input class="form-control date" type="text" id="fechPendiente`+i+`" name="pendientes[`+i+`][fecha_pendiente]" placeholder="Fecha de Cierre">
						                    </div>
						                  </div>
						                  <div class="col-1">
						                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarPendientes('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
						                  </div>
						                </div>
						                <div class="row">
						                    <div class="col-md-7 num_pen_columna`+i+`">
						                        <a id="num_pen_act`+i+`"></a>
						                    </div>
						                    <div class="col-md-5 num_pen_columna`+i+`">

						                    </div>
						                </div>`;
						        $( "#pendientes" ).after(dato);
						        $('.date').bootstrapMaterialDatePicker({
						            time: false,
						            clearButton: true,
						            lang: 'es',
						            minDate : new Date()
						        });
						        /* detalle pendientes */
						        $('.detalle_pendiente').keyup(function() {
						            var chars = $(this).val().length;
						            id=$(this).attr('id');
						            if (chars>0) {
										$("#tipo_pendiente"+id).prop('required',true);
										$("#fechPendiente"+id).prop('required',true);
							        }else{
							        	$("#tipo_pendiente"+id).prop('required',false);
										$("#fechPendiente"+id).prop('required',false);
							        }
						            if(chars>=50){
						                $('.num_pen_columna'+id).css('background-color','lightgreen');
						                icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						            }else if(chars<=50){
						                $('.num_pen_columna'+id).css('background-color','yellow');
						                icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						            }
						            $('#num_pen_act'+id).html(chars+' Caracteres '+icon);
						        });
						        /*fin detalle pendientes */
						    });

						    funAgregarTotales();
							funSumar();
						}
					}).then(function (result) {
						if (result.success) {
							$('#calendar').fullCalendar('next');
							$('#calendar').fullCalendar('prev');
							swal("Exito!","Actividad guardado con exito.","success");
						}else{
							swal("Error!","Algo ha salido mal.","warning");
						}
					}).catch(swal.noop)
				}else{
					swal({
					  	title: 'Mostrar bitacora',
					  	animation: "slide-from-top",
					  	confirmButtonText: 'Aceptar',
					  	showLoaderOnConfirm: false,
					  	html:`<?php print $codigoHTML; ?>`,
					  	onOpen: function () {
					  		if (event.origen_datos === null || event.origen_datos === "") {
					  			event.origen_datos = "N";
					  		}
					  		$("#select_traer_datos").val(event.origen_datos);
					  		function funTraerDatos() {
					  			if ($("#select_traer_datos").val() === "") {
						        	$("#contenedorOculto").hide();
						        }else{
						        	$("#contenedorOculto").show();
						        	if ($("#select_traer_datos").val() === "P") {
						        		$("#divAgendaVisita").hide();
										$("#divPlanTrabajo").show();
										$("#PlanTrabajo").attr("required", true);
										$("#agendaVisita").attr("required", false);
										$("#PlanTrabajo").attr("name", "id_tipo");
										$("#agendaVisita").attr("name", "");

						        	}else if($("#select_traer_datos").val() === "G"){
						        		$("#divPlanTrabajo").hide();
										$("#divAgendaVisita").show();
										$("#PlanTrabajo").attr("required", false);
										$("#agendaVisita").attr("required", true);
										$("#PlanTrabajo").attr("name", "");
										$("#agendaVisita").attr("name", "id_tipo");
						        	}else{
						        		$("#PlanTrabajo").attr("required", false);
										$("#agendaVisita").attr("required", false);
										$("#PlanTrabajo").attr("name", "");
										$("#agendaVisita").attr("name", "");
						        		$("#divAgendaVisita").hide();
										$("#divPlanTrabajo").hide();
						        	}
						        }
						        $("#oportunidad").val("");
				            	$("#select_tipo_actividad").val("");
				            	$("#detalle_actividad").val("");
				            	$("#PlanTrabajo").val("");
				            	$("#agendaVisita").val("");
				            	$(".opprohtml").html("");
					  		}

					  		funTraerDatos();

			          if (event.origen_datos == "G") {
					  			$("#agendaVisita").val(event.id_tipo);
					  		}else if(event.origen_datos == "P"){
					  			$("#PlanTrabajo").val(event.id_tipo);
					  		}

					  		$('body').on('change','#select_traer_datos',function(){
						        funTraerDatos();
						    });

						    $('body').on('change','#PlanTrabajo',function(){
						    	$(".opprohtml").html("");
						        if ($(this).val() != "") {
						        	id = $(this).val();
						        	$.ajax({
							            url: "{{ url('/') }}/ajaxplantrabajo/"+id,
							            cache: false,
							            contentType: false,
							            processData: false,
							            type: 'GET',
							            success: function(e){
							            	$("#oportunidad").val(e.oportunidad);
							            	$("#select_tipo_actividad").val(e.tipo_actividad);
							            	$("#detalle_actividad").val(e.observaciones);
							            	funOportinidadhtml();
							              console.log(e);
							            }
							        });
						        }
						    });

						    $('body').on('change','#agendaVisita',function(){
						    	$(".opprohtml").html("");
						        if ($(this).val() != "") {
						        	id = $(this).val();
						        	$.ajax({
							            url: "{{ url('/') }}/ajaxagendavisita/"+id,
							            cache: false,
							            contentType: false,
							            processData: false,
							            type: 'GET',
							            success: function(e){
							            	$("#oportunidad").val(e.oportunidad);
							            	$("#select_tipo_actividad").val("");
							            	$("#detalle_actividad").val(e.detalle);
							              console.log(e);
							            }
							        });
						        }
						    });

					  		$("#oportunidad").val(event.oportunidad_id);
							$("#select_tipo_actividad").val(event.tipo_actividad);

              $("#tipo_actividad").val(event.tipo_actividad);

              var valor = $("#tipo_actividad").val();
              $.ajax( "/subactividad?term="+valor )
              .always(function(data) {
                if(data){
                  var html = '<p>Subactividad</p><select id="subtipo" name="subtipo" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                  $.each( data, function( key, value ) {
                    html += '<option value="'+value.id+'">'+value.text+'</option>';
                  });
                  html += '</select>';
                  $(".addOtro").html("");
                  $(".contSubTipo").html(html);
                  $("#subtipo").val(event.subtipo);
                  console.log(data);
                }
              });
              if (valor === "Empresa") {
                $.ajax( "/empresa" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa_id" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#empresa_id").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }else if (valor === "Oportunidad") {
                $.ajax( "/ssoportunidad" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Oportunidad</p><input list="browsers" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#oportunidad").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }

							$("#estado_actividad").val(event.estado_actividad);
							$("#detalle_actividad").val(event.detalle_actividad);
							$("#detalle_resultado").val(event.detalle_resultado);
							funOportinidadhtml();
							$("#total_h_hombre").val(event.total_h_hombre);
							$("#s1").val(event.alimentacion);
							$("#s2").val(event.transportes_internos);
							$("#s3").val(event.transportes_intermunicipales);
							$("#s4").val(event.tiquete_aereo);
							$("#s5").val(event.papeleria);
							$("#s6").val(event.invitacion_cliente);
							$("#s7").val(event.alquiler_vehiculo);
							$("#s8").val(event.gasolina_pasajes);
							$("#s9").val(event.hotel);
							$("#s10").val(event.otros);
							$("#total").val(event.total);
							$("#total_general").val(event.total_general);

							j=1;
							i=1;
							html="";
                            html=`<div class="row justify-content-md-center"><a name="titulobservaciones"></a>
						          <div class="col-md-12">
						            <h5 class="card-header"> Observaciones </h5>
						          </div>
						          <div class="col-md-12 centrado">`;
						    moment.locale('es');
							$.each(event.comentario, function( index, value ) {
								if (value.user.foto === "" || value.user.foto === null) {
								fotod = `<img src="http://via.placeholder.com/40x40?text=`+value.user.name+`" class="media-object img-circle">`;
								}else{
								fotod = `<img src="{{url('/')}}/images/file/clientes/`+value.user.foto+`" alt="`+value.user.name+`" class="media-object img-circle" style="max-width:40px">`;
								}
								html += `
								        <div class="media clearfix header-bottom">
								  <div class="media-left">
								    `+fotod+`
								  </div>
								  <div class="media-body" style="text-align: start;">
								  	<label style="text-align: start;font-size: 14px !important;">
								      `+value.user.name+` `+moment(value.created_at).calendar()+` <div class="label label-info">`+value.estado+`</div>
									</label><br>
								    <p class="text-muted username soloprimer normalp" style="font-size: 15px;font-weight: 400;">`+value.detalle+`</p>
								  </div>
								</div>`;
					        });
							html += `</div>
						        </div>`;
						    $("#lisComentariosId").html(html);
                            html="";

							$.each(event.pendientes, function( index, value ) {
							  	i++;
								html +=`
		                          <div class="col-7">
		                            <div class="form-group">
		                                <textarea class="form-control detalle_pendiente" id="`+i+`" name="pendientes[`+i+`][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente">`+value.detalle+`</textarea>
		                            </div>
		                          </div>
		                          <div class="col-2">
		                            <div class="form-group">
		                                <select name="pendientes[`+i+`][tipo_pendiente]" id="tipo_pendiente`+i+`" class="form-control">
		                                    <option value="">Seleccione una tipo de actividad</option>
		                                    <?php foreach($tipos_actividades as $t_a){ ?>
		                                    <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
		                                    <?php } ?>
		                                </select>
		                            </div>
		                          </div>
		                          <div class="col-2">
		                            <div class="form-group">
		                                <input class="form-control date" type="text" id="fechPendiente`+i+`" name="pendientes[`+i+`][fecha_pendiente]" placeholder="Fecha de Cierre" value="`+value.fecha_pendiente+`">
		                            </div>
		                          </div>`;
								$("#pendienteshtml").append(html);
								$("#tipo_pendiente"+i).val(value.tipo);
							});
							usuariovalid = 0;
							$.each(event.hhombre, function( index, value ) {
							  	j++;
							  	valSumaT = parseInt(value.valor);
					            valSumaT = valSumaT.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            valSumaT = valSumaT.split('').reverse().join('').replace(/^[\,]/,'');

							  	if ($.isNumeric(value.id_user)) {
							  		usuariovalid = value.id_user;
							  		html=`<input type="hidden" name="hhombre[0][id_user]" value="`+value.id_user+`">
			                      <div class="col-4" style="margin-left: 1%">
			                        <div class="form-group">
			                            <input type="hidden" name="hhombre[0][cargo]" value="`+value.cargo+`">
			                            <a style="font-size: x-large;">`+value.cargo+`</a>
			                        </div>
			                      </div>
			                      <div class="col-2">
			                        <div class="form-group">
			                            <input type="hidden" name="hhombre[0][cantidad]" value="`+value.cantidad+`">
			                            <a style="font-size: x-large;">`+value.cantidad+`</a>
			                        </div>
			                      </div>
			                      <div class="col-2">
			                        <div class="form-group">
			                            <input type="hidden" class="mis_horas" name="hhombre[0][horas]" value="`+value.horas+`">
			                            <a id="mis_horas" style="font-size: x-large;">`+value.horas+`</a>
			                        </div>
			                      </div>
			                      <div class="col-3">
			                        <div class="form-group">
			                            <input type="hidden" id="total_mis_horas" name="hhombre[0][valor]" value="`+value.valor+`">
			                            <a class="total_mis_horas" style="font-size: x-large;">$ `+value.valor+`</a>
			                        </div>
			                      </div>`;
			                      $("#manoobrahtml").html(html);
			                  	}else{
			                  		html =`
			                          <div class="col-4">
			                            <div class="form-group">
			                                <select name="hhombre[`+j+`][cargo]" id="textCargo`+j+`" class="form-control" onchange="calcularValorHH(`+j+`)">
			                                    <option value="">Seleccione un cargo</option>
			                                    @foreach($cargos as $cargo)
			                                    <option value="{{ $cargo->nombre }}">{{ $cargo->nombre }}</option>
			                                    @endforeach
			                                </select>
			                            </div>
			                          </div>
			                          <div class="col-2">
			                            <div class="form-group">
			                                <input type="number" id="numberCantidad`+j+`" name="hhombre[`+j+`][cantidad]" class="form-control" placeholder="0" onchange="calcularValorHH(`+j+`)" value="`+value.cantidad+`">
			                            </div>
			                          </div>
			                          <div class="col-2">
			                            <div class="form-group">
			                                <input class="form-control" id="numberHoras`+j+`" type="number" name="hhombre[`+j+`][horas]" placeholder="0" onchange="calcularValorHH(`+j+`)" value="`+value.horas+`">
			                            </div>
			                          </div>
			                          <div class="col-4">
			                            <div class="form-group">
			                                <input class="form-control sumarSueldo" id="numberValor`+j+`" type="text" name="hhombre[`+j+`][valor]" placeholder="$0" readonly value="`+valSumaT+`">
			                            </div>
			                          </div>`;
			                          $("#horashombrehtml").append(html);
			                          $("#textCargo"+j).val(value.cargo);
			                  	}
							});

							html="";
							$.each(event.archivos, function( index, value ) {

								if (value.extension === "GIF" || value.extension === "PNG" || value.extension === "JPG" || value.extension === "gif" || value.extension === "png" || value.extension === "jpg"){
                                    html += `<div class="espaciofile"><a data-gallery="" title="`+value.archivo+`" href="{{url('/')}}/images/file/activity/`+value.archivo+`"><img src="{{ url('/') }}/images/iconciclos/images.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`"><p style="font-size: 10px;">`+value.nombre+`</p></a></div>`;
                                }else if (value.extension === "AVI" || value.extension === "MOV" || value.extension === "MP4" || value.extension === "avi" || value.extension === "mov" || value.extension === "mp4") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/video.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openVideo('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else if (value.extension === "PDF" || value.extension === "pdf") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/pdf.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else if (value.extension === "DOCX" || value.extension === "docx" || value.extension === "DOC" || value.extension === "doc") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/word.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else if (value.extension === "XLSX" || value.extension === "xlsx" || value.extension === "XLSM" || value.extension === "xlsm" || value.extension === "XML" || value.extension === "xml") {
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/excell.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }else{
                                    html += `<div class="espaciofile"><img src="{{ url('/') }}/images/iconciclos/word.png" class="iconfiles" data-toggle="tooltip" data-placement="top" title="`+value.nombre+`" onclick="openDocument('`+value.archivo+`')"><p style="font-size: 10px;">`+value.nombre+`</p></div>`;
                                }

							});
							$(".contentimg").html(html);
							$('[data-toggle="tooltip"]').tooltip();

							hora_inicio = moment(event.start).format('H:mm');
					        hora_fin = moment(event.end).format('H:mm');
					        $("#parrafoIniFin").html("De "+hora_inicio+" a "+hora_fin);
					        if(hora_inicio!=='' && hora_fin!=='' && usuariovalid === {{ Auth::user()->id }}){
					            hora_inicio = (hora_inicio).split(":"),
					            hora_fin = (hora_fin).split(":"),
					            t1 = new Date(),
					            t2 = new Date();

					            t1.setHours(hora_fin[0], hora_fin[1]);
					            t2.setHours(hora_inicio[0], hora_inicio[1]);
					            //AquÃ­ hago la resta
					            t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes());

					            /* pasar los minutos a horas y sumarle las horas */
					            horas=0;
					            horas=(t1.getMinutes() ? (((t1.getMinutes()) / 60)+(t1.getHours())) : t1.getHours());
					            horas=horas.toFixed(1);

					            A="{{ str_replace(',', '', (Auth::user()->salario_basico)) }}";
					            A=parseInt(A);
					            B="{{ str_replace(',', '', (Auth::user()->bonificacion_fija)) }}";
					            B=parseInt(B);
					            C="{{ str_replace(',', '', (Auth::user()->aux_transporte)) }}";
					            C=parseInt(C);
					            D="{{ str_replace(',', '', (Auth::user()->aux_alimentacion)) }}";
					            D=parseInt(D);
					            E="{{ str_replace(',', '', (Auth::user()->aux_vivienda)) }}";
					            E=parseInt(E);
					            F="{{ str_replace(',', '', (Auth::user()->otros_auxilios)) }}";
					            F=parseInt(F);
					            P="{{ str_replace('%', '', ($parafiscal->porcentaje)) }}";
					            P=(parseInt(P))/100;
					            ValorMes=((A+B)*(1+P))+C+D+E+F;
					            ValorTotal=(ValorMes/26/8)*horas;
					            ValorTotal=parseInt(ValorTotal);
					            $('.mis_horas').val(horas);
					            $('#mis_horas').html(horas);
					            $('#total_mis_horas').val(ValorTotal);
					            /*sumar todos los totales*/
					            valSumaT=0;
					            $(".sumarSueldo").each(function() {
					                var valSuma = verificar(this.id);
					                valSumaT = parseInt(valSumaT) + parseInt(valSuma);
					            });
					            valSumaT = parseInt(valSumaT) + parseInt(ValorTotal);
					            valSumaT = valSumaT.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            valSumaT = valSumaT.split('').reverse().join('').replace(/^[\,]/,'');
					            $('#total_general').val(valSumaT);
					            /*fin sumar todos los totales*/
					            ValorTotal = ValorTotal.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
					            ValorTotal = ValorTotal.split('').reverse().join('').replace(/^[\,]/,'');
					            $('.total_mis_horas').html('$ '+ValorTotal);
					            /*diff="La diferencia es de: " + (t1.getHours() ? t1.getHours() + (t1.getHours() > 1 ? " horas" : " hora") : "") + (t1.getMinutes() ? ", " + t1.getMinutes() + (t1.getMinutes() > 1 ? " minutos" : " minuto") : "")*/
					        }

					        $('.detalle_pendiente').keyup(function() {
						        var chars = $(this).val().length;
						        id=$(this).attr('id');
						        if (chars>0) {
									$("#tipo_pendiente"+id).prop('required',true);
									$("#fechPendiente"+id).prop('required',true);
						        }else{
						        	$("#tipo_pendiente"+id).prop('required',false);
									$("#fechPendiente"+id).prop('required',false);
						        }
						        if(chars>=50){
						            $('.num_pen_columna'+id).css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_pen_columna'+id).css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_pen_act'+id).html(chars+' Caracteres '+icon);
						    });
						    /*fin detalle pendientes */
						    /*detalle de la resultado */
						    $('#detalle_resultado').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_resu_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_resu_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_resu_act').html(chars+' Caracteres '+icon);
						    });
						    /*fin detalles de la resultado*/
						    /*detalle de la actividad */
						    $('#detalle_actividad').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_act_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_act_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_car_act').html(chars+' Caracteres '+icon);
						    });
						    /*fin detalles de la actividad*/
						    /*Ayuda estado actividad */
						    $('body').on('change','#estado_actividad',function(){
						        @foreach($estado_actividades as $e_a)
						            if('{{ $e_a->estado }}'==($(this).val())){
						                des='{{ $e_a->descripcion }}';
						            }
						        @endforeach
						        $('#msj_estado_actividad').attr('data-original-title',des);
						    });
						    /*fin Ayuda estado actividad */
						    /* Ayuda tipo Actividad */
						    $('body').on('change','#select_tipo_actividad',function(){
						        id=$('#select_tipo_actividad').val();
						        if(id!=''){
						            $.ajax({
						            url: "{{ url('/') }}/ayudatipoactividad/"+id,
						            cache: false,
						            contentType: false,
						            processData: false,
						            type: 'GET',
						            success: function(e){
						              $('#msj_ta').html(e.msj);
						              $('#titulo_t_a').html(id);
						              $('.fila_msj').css('display','block');
						            }
						          });
						        }
						    })

						    $(".dinero").maskMoney();
					        $('.time').bootstrapMaterialDatePicker({
					            time: true,
					            date: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            format : 'HH:mm',
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar',
					            weekStart : 0
					        }).on('close', function(e){
					            var a = moment($("#timeend").val(), 'HH:mm');
					            var b = moment($(".time").val(), 'HH:mm');
					            c = a.diff(b);
					            if (c<0) {
					                $("#timeend").val("");
					                swal("La hora final no puede ser menor a la inicial");
					            }
					        });

					        $('#timeend').bootstrapMaterialDatePicker({
					                time: true,
					                date: false,
					                clearButton: true,
					                nowButton: true,
					                lang: 'es',
					                format : 'HH:mm',
					                cancelText : 'Cancelar',
					                okText: 'Aceptar',
					                nowText: 'Nuevo',
					                clearText: 'Limpiar',
					                weekStart : 0
					        }).on('close', function(e){
					            var a = moment($("#timeend").val(), 'HH:mm');
					            var b = moment($(".time").val(), 'HH:mm');
					            c = a.diff(b);
					            if (c<0) {
					                $("#timeend").val("");
					                $("#mis_horas").html("0");
					                $("#total_mis_horas").val("0");
					                $(".total_mis_horas").html("$ 0");
					                swal("La hora final no puede ser menor a la inicial");
					            }
					        });

					        $('.date').bootstrapMaterialDatePicker({
					            time: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            minDate : new Date(),
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar'
					        });

					        $('#fechaactividad').bootstrapMaterialDatePicker({
					            time: false,
					            clearButton: true,
					            nowButton: true,
					            lang: 'es',
					            minDate : moment().subtract(20, 'day'),
					            maxDate : new Date(),
					            cancelText : 'Cancelar',
					            okText: 'Aceptar',
					            nowText: 'Nuevo',
					            clearText: 'Limpiar'
					        }).on('close', function(e){
					            var a = moment($("#fechaactividad").val(), 'YYYY-MM-DD');
					            var b = moment().subtract(2, 'day');
					            c = a.diff(b);
					            if (c<0) {
					                $(".card-header").css('background-color', '#f0ad4e');
					                $(".panel-title p").css('color', '#f0ad4e');
					                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
					                $("#tiempofuera").val(true);
					                swal("EstÃ¡ ingresando una actividad fuera de fecha");
					            }else{
					                $(".card-header").css('background-color', '#051d60');
					                $(".panel-title p").css('color', '#051d60');
					                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
					                $("#tiempofuera").val(false);
					            }
					        });

					        var xx=1;
						    $( "#agregarHorasHombre" ).click(function() {
							  xx++;
						        var i=xx;
						        dato = `<div class="row animated rollIn" id="D`+i+`">
						                  <div class="col-4">
						                    <div class="form-group">
						                        <select name="hhombre[`+i+`][cargo]" id="textCargo`+i+`" class="form-control" onchange="calcularValorHH(`+i+`)">
						                            <option value="">Seleccione un cargo</option>
						                            @foreach($cargos as $cargo)
						                                <option value="{{ $cargo->nombre }}">{{ $cargo->nombre }}</option>
						                            @endforeach
						                        </select>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input type="number" id="numberCantidad`+i+`" name="hhombre[`+i+`][cantidad]" class="form-control" placeholder="0" onchange="calcularValorHH(`+i+`)">
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input class="form-control" id="numberHoras`+i+`" type="number" name="hhombre[`+i+`][horas]" placeholder="0" onchange="calcularValorHH(`+i+`)">
						                    </div>
						                  </div>
						                  <div class="col-3">
						                    <div class="form-group">
						                        <input class="form-control sumarSueldo" id="numberValor`+i+`" type="text" name="hhombre[`+i+`][valor]" placeholder="$0" readonly>
						                    </div>
						                  </div>
						                  <div class="col-1">
						                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarhHombre('#D`+i+`')"><i class="fa fa-minus text-danger"></i></button>
						                  </div>
						                </div>`;
						        $( "#horashombrediv" ).after(dato);
						        $('.date').bootstrapMaterialDatePicker({
						            time: false,
						            clearButton: true,
						            lang: 'es',
						            minDate : new Date()
						        });
							});
						    var i = 1;
							$( "#agregarPendientes" ).click(function() {
						        i++;
						        dato = `<div class="row animated rollIn" id="T`+i+`">
						                  <div class="col-7">
						                    <div class="form-group">
						                        <textarea class="form-control detalle_pendiente" id="`+i+`" name="pendientes[`+i+`][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente"></textarea>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <select name="pendientes[`+i+`][tipo_pendiente]" id="tipo_pendiente`+i+`" class="form-control">
						                            <option value="">Seleccione una tipo de actividad</option>
						                            <?php foreach($tipos_actividades as $t_a){ ?>
						                            <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
						                            <?php } ?>
						                        </select>
						                    </div>
						                  </div>
						                  <div class="col-2">
						                    <div class="form-group">
						                        <input class="form-control date" type="text" id="fechPendiente`+i+`" name="pendientes[`+i+`][fecha_pendiente]" placeholder="Fecha de Cierre">
						                    </div>
						                  </div>
						                  <div class="col-1">
						                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarPendientes('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
						                  </div>
						                </div>
						                <div class="row">
						                    <div class="col-md-7 num_pen_columna`+i+`">
						                        <a id="num_pen_act`+i+`"></a>
						                    </div>
						                    <div class="col-md-5 num_pen_columna`+i+`">

						                    </div>
						                </div>`;
						        $( "#pendientes" ).after(dato);
						        $('.date').bootstrapMaterialDatePicker({
						            time: false,
						            clearButton: true,
						            lang: 'es',
						            minDate : new Date()
						        });
						        /* detalle pendientes */
						        $('.detalle_pendiente').keyup(function() {
						            var chars = $(this).val().length;
						            id=$(this).attr('id');
						            if (chars>0) {
										$("#tipo_pendiente"+id).prop('required',true);
										$("#fechPendiente"+id).prop('required',true);
							        }else{
							        	$("#tipo_pendiente"+id).prop('required',false);
										$("#fechPendiente"+id).prop('required',false);
							        }
						            if(chars>=50){
						                $('.num_pen_columna'+id).css('background-color','lightgreen');
						                icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						            }else if(chars<=50){
						                $('.num_pen_columna'+id).css('background-color','yellow');
						                icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						            }
						            $('#num_pen_act'+id).html(chars+' Caracteres '+icon);
						        });
						        /*fin detalle pendientes */
						    });
						}
					}).then(function (result) {
					}).catch(swal.noop)
				}
		        return false;
		    },
		    loading: function(bool) {
				$('#loaders').toggle(bool);
				if (!bool) {
					$('#script-warning').hide();
				}
			}
		});

		var fechadato = $('#calendar').fullCalendar('getDate');
		var dateWeek = moment(fechadato).weeks();
		var dateWeek = moment().weeks(dateWeek);
		$("#tituloPrincipal").html("Bitacora de la semana " + moment(dateWeek).weeks() + " <br>"+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));
		$('#calendar').fullCalendar( 'removeEventSources');
		$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
	        $.ajax({
	            url: urlList,
	            dataType: 'json',
	            data: {
	                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
	                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
	            },
	            success: function(doc) {
	            	respal = "";
					var push = false
					var feature = false;
					var features = [];
					doc2 = doc.sort(function (a, b) {
					  if (a.tipo_actividad > b.tipo_actividad) {
					    return 1;
					  }
					  if (a.tipo_actividad < b.tipo_actividad) {
					    return -1;
					  }
					  return 0;
					});
					var comp = doc2.length - 1;
					$.each( doc2, function( key, value ) {
						var a = moment(value.start);
						var b = moment(value.end);
						c = b.diff(a);if (c<0) {c = c*(-1);}

						c = parseInt(c/3600000);

						if (feature) {
							if (value.tipo_actividad != respal) {
								push = true;
								features.push(feature);
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
							}else{
								push = false;
								feature.tiempo = c+feature.tiempo;
							}
						}else{
							push = false;
							respal = value.tipo_actividad;
							feature = new Object();
							feature.tiempo = c;
							feature.user = value.tipo_actividad;
							feature.colorGrp = "rgb(103, 183, 220)";
							respal = value.tipo_actividad;
						}

						if (comp === key) {
							features.push(feature);
						}
					});
					var charts = AmCharts.makeChart("chartdiv2", {
						"theme": "light",
						"type": "serial",
						"startDuration": 2,
						"dataProvider": features,
						"valueAxes": [{
							"position": "left",
							"title": "Uso de tiempo de las actividades",
							"gridAlpha": 0,
							"dashLength": 0
						}],
						"gridAboveGraphs": false,
						"graphs": [{
							"balloonText": "[[category]]: <b>[[value]]</b>",
							"fillColorsField": "colorGrp",
							"fillAlphas": 1,
							"lineAlpha": 0.1,
							"type": "column",
							"valueField": "tiempo"
						}],
						"depth3D": 20,
						"angle": 30,
						"chartCursor": {
							"categoryBalloonEnabled": false,
							"cursorAlpha": 0,
							"zoomable": false
						},
						"categoryField": "user",
						"categoryAxis": {
							"gridPosition": "start",
							"gridAlpha": 0,
							"tickPosition": "start",
							"tickLength": 20,
							"labelRotation": 45
						},
						"export": {
							"enabled": true
						}
					});

					var feature2 = false;
					var features2 = [];
					otrodoc = doc.sort(function (a, b) {
						return a.oportunidad.localeCompare(b.oportunidad);
					});
					$.each( otrodoc, function( key, value ) {
						var a = moment(value.start);
						var b = moment(value.end);
						c = b.diff(a);if (c<0) {c = c*(-1);}

						c = parseInt(c/3600000);

						if (feature2) {
							if (value.oportunidad != respal) {
								push = true;
								features2.push(feature2);
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
							}else{
								push = false;
								feature2.tiempo = c+feature2.tiempo;
							}
						}else{
							push = false;
							respal = value.oportunidad;
							feature2 = new Object();
							feature2.tiempo = c;
							feature2.user = value.oportunidad;
							feature2.colorGrp = "rgb(103, 183, 220)";
							respal = value.oportunidad;
						}
						var comp = otrodoc.length - 1;
						if (comp === key) {
							features2.push(feature2);
						}
					});
					var chart = AmCharts.makeChart( "chartdiv", {
						"type": "pie",
						"theme": "light",
						"titles": [ {
							"text": "Uso de tiempo con respecto a las oportunidades",
							"size": 16
						} ],
						"dataProvider": features2,
						"valueField": "tiempo",
						"titleField": "user",
						"startEffect": "elastic",
						"startDuration": 2,
						"labelRadius": 15,
						"innerRadius": "50%",
						"depth3D": 10,
						"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
						"angle": 15,
						"export": {
							"enabled": true
						}
					});
					callback(doc);
	            },
	            error: function() {
					$('#script-warning').show();
				}
	        });
	    });
    });

    openVideo = function(nombre) {
						        dir = "{{url('/')}}/images/file/activity/"+nombre;
						        video = `<div id="play_video_modal" class="caja">
						                    <video id="Video1" src="`+dir+`" loop preload="auto">
						                      Tu navegador no implementa el elemento <code>video</code>.
						                    </video>
						                    <div id="buttonbar">
						                        <button class="video_button" id="restart" onclick="restart();"><i class="fa fa-repeat" aria-hidden="true"></i></button>
						                        <button class="video_button" id="rew" onclick="skip(-10)"><i class="fa fa-backward" aria-hidden="true"></i></button>
						                        <button class="video_button" id="play" onclick="vidplay()"><i class="fa fa-play" aria-hidden="true"></i></button>
						                        <button class="video_button" id="fastFwd" onclick="skip(10)"><i class="fa fa-forward" aria-hidden="true"></i></button>
						                        <button class="video_button pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
						                        <button class="video_button pull-right" onclick="pantallaCompletaVideo()"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>
						                    </div>
						                <div>`;

						        $("#modal_video").html(video);
						        $("#archivos").modal();
						    }

						    openDocument = function(nombre) {
						        dir = "{{url('/')}}/images/file/activity/"+nombre;
						        contenido = `<iframe src="http://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:850px;" frameborder="0"></iframe>`;
						        $("#modal_video").html(contenido);
						        $("#archivos").modal();
						    }

						    function vidplay() {
						       var video = document.getElementById("Video1");
						       var button = $("#play");

						       if (video.paused) {
						          video.play();
						          button.html('<i class="fa fa-pause" aria-hidden="true"></i>');
						       } else {
						          video.pause();
						          button.html('<i class="fa fa-play" aria-hidden="true"></i>');
						       }
						    }

						    function restart() {
						        var video = document.getElementById("Video1");
						        video.currentTime = 0;
						    }

						    function skip(value) {
						        var video = document.getElementById("Video1");
						        video.currentTime += value;
						    }

    calcularValorHH = function (val) {
        cargo = $("#textCargo"+val).val();
        var valores = [
            @foreach($cargos as $cargo)
            {cargo:"{{ $cargo->nombre }}", sueldo:"{{ str_replace(',', '', $cargo->salario) }}"},
            @endforeach
        ];

        new_arr = $.grep(valores, function(n, i){
            return n.cargo == cargo;
        });

        if (new_arr) {
            valSueldo = new_arr[0].sueldo;
            var cantidad=verificar("numberCantidad"+val);
            var horas=verificar("numberHoras"+val);
            parafiscal = '{{ str_replace('%', '', $parafiscal->porcentaje) }}';
            parafiscal = (parseInt(parafiscal))/100;
            valorMes = (valSueldo*(1+parafiscal));
            valorHora = valorMes/26/8;
            valTotalHoras = valorHora*horas;
            valTotalAcomp = parseInt(valTotalHoras*cantidad);

            if(!isNaN(valTotalAcomp)){
                valTotalAcomp = valTotalAcomp.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                valTotalAcomp = valTotalAcomp.split('').reverse().join('').replace(/^[\,]/,'');
                document.getElementById("numberValor"+val).value = valTotalAcomp;

                funAgregarTotales();
            }
        }
    }

	funOportinidadhtml = function() {
		id = $("#oportunidad").val();
	  	var valores = [];
    	<?php
    		$valores = [];
    		class opproduc {
				public $id;
			  	public $oportunidad;
			 	public $producto;
			}
          	$valobj = new opproduc;
		  	$valobj->id = "Trabajos Generales ESSI";
		  	$valobj->oportunidad = "Trabajos Generales ESSI";
		  	$valobj->producto = "";
  		  	$valores[] = $valobj;

    		foreach ($oportunidades as $tag) {
    			$productos = App\OportunidadProducto::where("oportunidad_id",$tag->id)->get();
    			$html="<div class='col-12'><h4>".$tag->empresa.'/'.$tag->ciudad."</h4></div>";
    			foreach ($productos as $res) {
                    try {
                        if (isset($res->producto) && is_numeric($res->producto)) {
                            $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                            if (isset($res->referencia) && is_numeric($res->referencia)) {
                                $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                            }else{
                                $referencia->referencia = "Estandar";
                            }

                            if(!empty($referencia->foto) && $referencia->foto != Null){
                                $foto = url('/')."/images/file/productos/".$referencia->foto;
                            }else{
                              	$foto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Foto&w=100&h=100';
                            }
                            $html .= '
                            <div class="col-3 placeholder img-sedes">
                            	<div class="img-thumbnail img-sedes">
	                              <a href="">
	                                <img src="'.$foto.'" style="max-width: 50%;" class="img-fluid mx-auto d-block">
	                              </a>
                            	</div>
                            	<div class="text-muted centrado">
                            		<h6 style="margin-bottom: 0px;">'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.'</h6>
                            		<p>$ '.$res->total.'</p>
                            	</div>
                            </div>';
                        }
                    } catch (Exception $e) { }
                }
                $valobj = new opproduc;
				$valobj->id = $tag->id;
				$valobj->oportunidad = $tag->empresa.'/'.$tag->ciudad;
				$valobj->producto = $html;
          		$valores[] = $valobj;
			}
			//echo json_encode($valores);
        ?>
        var valores = <?php echo json_encode($valores); ?>;
        new_arr = $.grep(valores, function(n, i){
            return n.id == id;
        });

        if (new_arr) {
        	try {
        		$(".opprohtml").html(new_arr[0].producto);
        	} catch(err) {
			    console.log(err.message);
			}
        }
	}

	$('[data-toggle="tooltip"]').tooltip();
</script>
<script src="{{ url('/') }}/js/actividadescalendar.js"></script>
<script src="{{ url('/') }}/js/bitacora/bitacora.js"></script>
@endsection
