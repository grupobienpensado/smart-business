@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Plan de Trabajo')

@section('content')
<!--<link href='{{ url("/") }}/components/newfullcalendar/fullcalendar.min.css' rel='stylesheet' />-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.min.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.print.min.css" rel='stylesheet' media='print' />


<link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/morris.css">
<link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/export.css" type="text/css" media="all" />
<link rel="stylesheet" href="{{ url('/') }}/css/plandetrabajo.css">



<div class="card animated flipInX" id="list" style="margin-bottom: 30px;">
    <div class="card-block">
    	<div class="pull-left">
	        <div class="btn-group" role="group">
	          <button type="button" class="btn btn-sm btn-secondary" id="btnPrev"><i class="fa fa-chevron-left"></i> Anterior</button>
	          <button type="button" class="btn btn-sm btn-secondary" id="btnToday"><i class="fa fa-calendar-check-o"></i> Hoy</button>
	          <button type="button" class="btn btn-sm btn-secondary" id="btnNext">Siguiente <i class="fa fa-chevron-right"></i></button>
	        </div>
	    </div>
    	<div class="pull-right">
	        <div class="btn-group" role="group">
	          <button type="button" class="btn btn-sm btn-secondary active" onclick="funMostarDiv('calendar')" id="bcalendar"><i class="fa fa-calendar"></i> Ver Calendar</button>
	          <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('grafica')" id="bgrafica"><i class="fa fa-bar-chart"></i> Ver grafica</button>
	        </div>
	    </div>
    	<h2 id="tituloPrincipal">Plan de trabajo</h2>
    	<div id="loaders" style="display: none;"><div class="loader"></div><small>Cargando registros...</small></div>
    	<div id='script-warning'>
			<code>Error!, no se han podido cargar los datos</code>
		</div>
		<div class="form-group row">
			<div class="col-2"></div>
            <div class="col-4">
                @if($permiso_filtro == 'Si')
                <select class="form-control" id="responsable"></select>
                @endif
            </div>
            <div class="col-4">
                <select class="form-control" id="semanas">
                	<option value="01">Seleccione una semana</option>
                	<option value="01" <?php if(isset($semana) && $semana=="1"){ echo 'selected';} ?>>Semana 01</option>
                	<option value="02" <?php if(isset($semana) && $semana=="2"){ echo 'selected';} ?>>Semana 02</option>
                	<option value="03" <?php if(isset($semana) && $semana=="3"){ echo 'selected';} ?>>Semana 03</option>
                	<option value="04" <?php if(isset($semana) && $semana=="4"){ echo 'selected';} ?>>Semana 04</option>
                	<option value="05" <?php if(isset($semana) && $semana=="5"){ echo 'selected';} ?>>Semana 05</option>
                	<option value="06" <?php if(isset($semana) && $semana=="6"){ echo 'selected';} ?>>Semana 06</option>
                	<option value="07" <?php if(isset($semana) && $semana=="7"){ echo 'selected';} ?>>Semana 07</option>
                	<option value="08" <?php if(isset($semana) && $semana=="8"){ echo 'selected';} ?>>Semana 08</option>
                	<option value="09" <?php if(isset($semana) && $semana=="9"){ echo 'selected';} ?>>Semana 09</option>
                	<option value="10" <?php if(isset($semana) && $semana=="10"){ echo 'selected';} ?>>Semana 10</option>
                	<option value="11" <?php if(isset($semana) && $semana=="11"){ echo 'selected';} ?>>Semana 11</option>
                	<option value="12" <?php if(isset($semana) && $semana=="12"){ echo 'selected';} ?>>Semana 12</option>
                	<option value="13" <?php if(isset($semana) && $semana=="13"){ echo 'selected';} ?>>Semana 13</option>
                	<option value="14" <?php if(isset($semana) && $semana=="14"){ echo 'selected';} ?>>Semana 14</option>
                	<option value="15" <?php if(isset($semana) && $semana=="15"){ echo 'selected';} ?>>Semana 15</option>
                	<option value="16" <?php if(isset($semana) && $semana=="16"){ echo 'selected';} ?>>Semana 16</option>
                	<option value="17" <?php if(isset($semana) && $semana=="17"){ echo 'selected';} ?>>Semana 17</option>
                	<option value="18" <?php if(isset($semana) && $semana=="18"){ echo 'selected';} ?>>Semana 18</option>
                	<option value="19" <?php if(isset($semana) && $semana=="19"){ echo 'selected';} ?>>Semana 19</option>
                	<option value="20" <?php if(isset($semana) && $semana=="20"){ echo 'selected';} ?>>Semana 20</option>
                	<option value="21" <?php if(isset($semana) && $semana=="21"){ echo 'selected';} ?>>Semana 21</option>
                	<option value="22" <?php if(isset($semana) && $semana=="22"){ echo 'selected';} ?>>Semana 22</option>
                	<option value="23" <?php if(isset($semana) && $semana=="23"){ echo 'selected';} ?>>Semana 23</option>
                	<option value="24" <?php if(isset($semana) && $semana=="24"){ echo 'selected';} ?>>Semana 24</option>
                	<option value="25" <?php if(isset($semana) && $semana=="25"){ echo 'selected';} ?>>Semana 25</option>
                	<option value="26" <?php if(isset($semana) && $semana=="26"){ echo 'selected';} ?>>Semana 26</option>
                	<option value="27" <?php if(isset($semana) && $semana=="27"){ echo 'selected';} ?>>Semana 27</option>
                	<option value="28" <?php if(isset($semana) && $semana=="28"){ echo 'selected';} ?>>Semana 28</option>
                	<option value="29" <?php if(isset($semana) && $semana=="29"){ echo 'selected';} ?>>Semana 29</option>
                	<option value="30" <?php if(isset($semana) && $semana=="30"){ echo 'selected';} ?>>Semana 30</option>
                	<option value="31" <?php if(isset($semana) && $semana=="31"){ echo 'selected';} ?>>Semana 31</option>
                	<option value="32" <?php if(isset($semana) && $semana=="32"){ echo 'selected';} ?>>Semana 32</option>
                	<option value="33" <?php if(isset($semana) && $semana=="33"){ echo 'selected';} ?>>Semana 33</option>
                	<option value="34" <?php if(isset($semana) && $semana=="34"){ echo 'selected';} ?>>Semana 34</option>
                	<option value="35" <?php if(isset($semana) && $semana=="35"){ echo 'selected';} ?>>Semana 35</option>
                	<option value="36" <?php if(isset($semana) && $semana=="36"){ echo 'selected';} ?>>Semana 36</option>
                	<option value="37" <?php if(isset($semana) && $semana=="37"){ echo 'selected';} ?>>Semana 37</option>
                	<option value="38" <?php if(isset($semana) && $semana=="38"){ echo 'selected';} ?>>Semana 38</option>
                	<option value="39" <?php if(isset($semana) && $semana=="39"){ echo 'selected';} ?>>Semana 39</option>
                	<option value="40" <?php if(isset($semana) && $semana=="40"){ echo 'selected';} ?>>Semana 40</option>
                	<option value="41" <?php if(isset($semana) && $semana=="41"){ echo 'selected';} ?>>Semana 41</option>
                	<option value="42" <?php if(isset($semana) && $semana=="42"){ echo 'selected';} ?>>Semana 42</option>
                	<option value="43" <?php if(isset($semana) && $semana=="43"){ echo 'selected';} ?>>Semana 43</option>
                	<option value="44" <?php if(isset($semana) && $semana=="44"){ echo 'selected';} ?>>Semana 44</option>
                	<option value="45" <?php if(isset($semana) && $semana=="45"){ echo 'selected';} ?>>Semana 45</option>
                	<option value="46" <?php if(isset($semana) && $semana=="46"){ echo 'selected';} ?>>Semana 46</option>
                	<option value="47" <?php if(isset($semana) && $semana=="47"){ echo 'selected';} ?>>Semana 47</option>
                	<option value="48" <?php if(isset($semana) && $semana=="48"){ echo 'selected';} ?>>Semana 48</option>
                	<option value="49" <?php if(isset($semana) && $semana=="49"){ echo 'selected';} ?>>Semana 49</option>
                	<option value="50" <?php if(isset($semana) && $semana=="50"){ echo 'selected';} ?>>Semana 50</option>
                	<option value="51" <?php if(isset($semana) && $semana=="51"){ echo 'selected';} ?>>Semana 51</option>
                	<option value="52" <?php if(isset($semana) && $semana=="52"){ echo 'selected';} ?>>Semana 52</option>
                </select>
            </div>
        </div>
		<div id='calendar'></div>
		<div id="grafica" style="display: none">
	    	<div id="chartdiv"></div>
	    	<div id="chartdiv2"></div>
	    </div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	var j = jQuery.noConflict();
	var $ = jQuery.noConflict();
	var urlList = '{{ url("listadoplandetrabajo") }}/{{ Auth::user()->id }}';
	var features = [];
</script>
<script src='{{ url("/") }}/components/newfullcalendar/lib/moment.min.js'></script>
<!--<script src='{{ url("/") }}/components/newfullcalendar/lib/jquery.min.js'></script>
<script src='{{ url("/") }}/components/newfullcalendar/fullcalendar.min.js'></script>
<script src='{{ url("/") }}/components/newfullcalendar/locale-all.js'></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.min.js" integrity="sha256-MDHWvW/uBfL2FEZwh9gqePZTrrxfCgNlQelyThMV8/c=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/locale-all.js" integrity="sha256-6cQXtrul38ChfKH+ojSity2Aifa8dSvrdRiYUiQrd2M=" crossorigin="anonymous"></script>

<script src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
<script src="{{ url('/') }}/components/amchart/js/funnel.js"></script>
<script src="{{ url('/') }}/components/amchart/js/serial.js"></script>
<script src="{{ url('/') }}/components/amchart/js/pie.js"></script>
<script src="{{ url('/') }}/components/amchart/js/gauge.js"></script>
<script src="{{ url('/') }}/components/amchart/js/radar.js"></script>
<script src="{{ url('/') }}/components/amchart/js/light.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/morris.min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/raphael-min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/morris.min.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>

<script>
	<?php if(isset($semana)){ ?>
		setTimeout(consultar_semana, 4000);
	<?php } ?>
	function consultar_semana(){
		<?php
		if(isset($id_funcionario)){ ?>
			$('#responsable').val('{{$id_funcionario}}').trigger('change.select2');
		<?php }?>


		if ($('#semanas').val()) {
			var dateWeek = moment().weeks($('#semanas').val());
			$("#tituloPrincipal").html("Plan de trabajo de la semana " + moment(dateWeek).weeks() + " <br> "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));
			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar ('gotoDate', dateWeek);
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		}
	}

	$(function() {
		moment.locale('es');
		$("#tituloPrincipal").html("Plan de trabajo de la semana " + moment().weeks() + " <br> "+ moment().day(0).format("MMMM D") + " - " + moment().day(6).format("MMMM D"));
		$('#calendar').fullCalendar({
			header: {
				left: '',
				right:''
			},
			defaultView: 'agendaWeek',
			defaultDate: moment(),
			locale: 'es',
            allDaySlot: false,
			navLinks: true,
			selectable: true,
			selectHelper: true,
			axisFormat : "HH:mm",
    		agenda : "HH:mm",
			weekNumbers: true,
			weekNumbersWithinDays: true,
			firstDay:0,
			columnFormat: 'dddd D',
			editable: true,
			eventLimit: true,
			select: function(start2, end) {
				var diasemanaActual = moment().day();
				var semana = moment(start2).week();
				var semanaActual = moment().week();
				var anoActual = moment().year();
				var ano = moment(start2).year();
				if (semana > semanaActual || (semana === semanaActual && diasemanaActual < 2) || ano > anoActual) {
					swal({
					  title: 'Crear plan de trabajo',
					  animation: "slide-from-top",
					  showCancelButton: true,
					  confirmButtonText: 'Guardar',
					  cancelButtonText: 'Cerrar',
					  showLoaderOnConfirm: true,
					  html:
					    `
					    <form id="formPlantrabajo" enctype="multipart/form-data">
  					    	{{ csrf_field() }}
  					    	<div id="divImgEstado">
                  	<p id="parrafoOk"></p>
                  	<img id="imgStado" src="{{ url('/') }}/images/icons/checkedblue.png">
                  	<input type="hidden" id="tiempofuera" name="fuera_tiempo" value="false">
                  </div>
                  <div class="row top-grande">
                    <div class="col-md-4">
                      <p>Tipo de actividad</p>
                      <select name="tipo_actividad" class="swal2-input" id="tipo_actividad" required>
                          <option value="">Seleccione un tipo de actividad</option>
                          <option>Oportunidad</option>
                          <option>Empresa</option>
                          <option>Gestion comercial</option>
                          <option>Gestion interna ESSI</option>
                      </select>
                    </div>
                    <div class="col-md-4 contSubTipo"></div>
                    <div class="col-md-4 addOtro"></div>
                  </div>
  					    	<div class="form-group row opprohtml"></div>

  					    	<div class="form-group row">
  					          <div class="col-md-12">
  					          	  <label>Detalle exacto de las actividades</label>
  				              	<div id="output"></div>
  				                <textarea class="form-control" id="my-input" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones"></textarea>
  				                <div class="row" style="margin-bottom: 1%">
  				                    <div class="col-md-2 num_car_act_columna">

  				                    </div>
  				                    <div class="col-md-10 num_car_act_columna">
  				                        <a id="num_car_act"></a>
  				                    </div>
  				                </div>
  					          </div>
  					      </div>
				      </form>`,
					  preConfirm: function () {
					    return new Promise(function (resolve, reject) {
					    	var input = $('#my-input')
							  var count = input.val().length

                if($("form").parsley().validate()){
                  if(count < 50){
    								reject('Perdon!, Es necesario que ingrese un detalle mas amplio, el cual supere los 50 caracteres.')
    							}else{
    								if (moment(event.start).format('H')>12) {
    									jornada= "T";
    								}else{
    									jornada= "M";
    								}

    								$.post( "{{ url('plantrabajosave') }}", {
    									fecha: moment(start2).format('YYYY-MM-DD'),
    									hora_inicio: moment(start2).format('H:mm'),
    									hora_fin: moment(end).format('H:mm'),
    									observaciones: $('#my-input').val(),
    									tipo_actividad: $('#tipo_actividad').val(),
                      subtipo: $('#subtipo').val(),
                      empresa_id: $('#empresa_id').val(),
    									oportunidad: $('#oportunidad').val(),
    									_token: $("input[name='_token']").val(),
    									fuera_tiempo: $("#tiempofuera").val(),
    									semana: moment(start2).week(),
    									jornada		   : jornada
    								})
    								.fail(function() {
    									resolve(false)
    								})
    								.done(function(msg) {
    									resolve(msg)
    								});
    							}
                }else{
                    reject('Perdón!, Es necesario que complete los campos requeridos.');
                }

					    })
					  },
					  onOpen: function () {

                $(document).on('change', '#tipo_actividad', function() {
                  var valor = $(this).val();
                  console.log("cambio", valor, '===',"Empresa");
                  $.ajax( "/subactividad?term="+valor )
                  .always(function(data) {
                    if(data){
                      var html = '<p>Subactividad</p><select id="subtipo" name="subtipo_actividad" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                      $.each( data, function( key, value ) {
                        html += '<option value="'+value.id+'">'+value.text+'</option>';
                      });
                      html += '</select>';
                      $(".addOtro").html("");
                      $(".contSubTipo").html(html);
                      console.log(data);
                    }
                  });
                  if (valor === "Empresa") {
                    $.ajax( "/empresa" )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                        $.each( data, function( key, value ) {
                          html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                        });
                        html += '</datalist>';
                        $(".addOtro").html(html);
                        console.log(data);
                      }
                    });
                  }else if (valor === "Oportunidad") {
                    $.ajax( "/ssoportunidad" )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Oportunidad</p><input list="browsers" id="oportunidad" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                        $.each( data, function( key, value ) {
                          html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                        });
                        html += '</datalist>';
                        $(".addOtro").html(html);
                        console.log(data);
                      }
                    });
                  }
                });

						  	if (semana === semanaActual && diasemanaActual < 2) {
				                $(".card-header").css('background-color', '#f0ad4e');
				                $(".panel-title p").css('color', '#f0ad4e');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
				                $("#tiempofuera").val(true);
				                $("#parrafoOk").text("Esta¡ llenando una plan de trabajo fuera de fecha.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#rgb(232, 72, 73);", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }else{
				            	$(".card-header").css('background-color', '#051d60');
				                $(".panel-title p").css('color', '#051d60');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
				                $("#tiempofuera").val(false);
				                $("#parrafoOk").text("Esta¡ llenando una plan de trabajo en la fecha acordada.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#1d92af", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }

						  	funOportinidadhtml();
						  	$('#my-input').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_act_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_act_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_car_act').html(chars+' Caracteres '+icon);
						    });
					  }
					}).then(function (result) {
						if (result.success) {
							$('#calendar').fullCalendar('next');
							$('#calendar').fullCalendar('prev');
							swal("Exito!","Plan de trabajo guardado con exito.","success");
						}else{
							swal("Error!","Algo ha salido mal.","warning");
						}
					}).catch(swal.noop)
				}else{
					swal("Alto!","El tiempo de hacer cambios se ha agotado.","warning");
				}
			},
			buttonIcons: true,  // allow "more" link when too many events
			events: function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(start).format('YYYY-MM-DD'),
		                end: moment(end).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  // a must be equal to b
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);

							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
					},
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    },
			timeFormat: 'HH:mm:ss',
			eventClick: function(event) {
				var diasemanaActual = moment().day();
				var semana = moment(event.start).week();
				var semanaActual = moment().week();
				if (semana > semanaActual || (semana === semanaActual && diasemanaActual < 2) ) {
				    swal({
					  	title: 'Editar plan de trabajo',
					  	animation: "slide-from-top",
					  	showCancelButton: true,
					  	confirmButtonText: 'Guardar',
					  	cancelButtonText: 'Cerrar',
					  	showLoaderOnConfirm: true,
					  	html:
					    `
					    <form id="formPlantrabajo" enctype="multipart/form-data">
					    	{{ csrf_field() }}
					    	<div id="divImgEstado">
                	<p id="parrafoOk"></p>
                	<img id="imgStado" src="{{ url('/') }}/images/icons/checkedblue.png">
                	<input type="hidden" id="tiempofuera" name="fuera_tiempo" value="false">
                </div>
                <div class="row top-grande">
                  <div class="col-md-4">
                    <p>Tipo de actividad</p>
                    <select name="tipo_actividad" class="swal2-input" id="tipo_actividad" required>
                        <option value="">Seleccione un tipo de actividad</option>
                        <option>Oportunidad</option>
                        <option>Empresa</option>
                        <option>Gestion comercial</option>
                        <option>Gestion interna ESSI</option>
                    </select>
                  </div>
                  <div class="col-md-4 contSubTipo"></div>
                  <div class="col-md-4 addOtro"></div>
                </div>
                <div class="form-group row opprohtml"></div>

					    	<div class="form-group row">
					          <div class="col-md-12">
					          	  <label>Detalle exacto de las actividades</label>
				              	<div id="output"></div>
				                <textarea class="form-control" id="my-input" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones"></textarea>
				                <div class="row" style="margin-bottom: 1%">
				                    <div class="col-md-2 num_car_act_columna">

				                    </div>
				                    <div class="col-md-10 num_car_act_columna">
				                        <a id="num_car_act"></a>
				                    </div>
				                </div>
					          </div>
					      </div>
			       	</form>
              <div id="lisComentariosId"></div>
              <div id="divRevisarButton"></div>
              <div id="divEliminarButton"></div>`,
  						preConfirm: function () {
  						    return new Promise(function (resolve, reject) {
  						    	var input = $('#my-input')
    								var count = input.val().length
                    if($("form").parsley().validate()){
                        if(count < 50){
          								reject('Perdon!, Es necesario que ingrese un detalle mas amplio, el cual supere los 50 caracteres.')
          							}else{
          									if (moment(event.start).format('H')>12) {
          										jornada= "T";
          									}else{
          										jornada= "M";
          									}
          									$.post( "{{ url('plantrabajoedit') }}", {
          										fecha          : moment(event.start).format('YYYY-MM-DD'),
          										hora_inicio    : moment(event.start).format('H:mm'),
          										hora_fin       : moment(event.end).format('H:mm'),
          										observaciones  : $('#my-input').val(),
          										tipo_actividad : $('#tipo_actividad').val(),
                              subtipo        : $('#subtipo').val(),
                              empresa_id     : $('#empresa_id').val(),
            									oportunidad    : $('#oportunidad').val(),
          										_token         : $("input[name='_token']").val(),
          										semana         : moment(event.start).week(),
          										fuera_tiempo   : $("#tiempofuera").val(),
          										id             : event.id,
          										jornada		     : jornada

          									})
          									.fail(function() {
          										resolve(false)
          									})
          									.done(function(msg) {
        										resolve(msg)
        									});
                        }
                    }else{
                        reject('Perdón!, Es necesario que complete los campos requeridos.');
                    }
  						    })
  						},
					  	onOpen: function () {

                  $("#divEliminarButton").html(`<button class="btn swal2-modal swal2-styled btn-danger" id="button-delete" name="`+event.id+`" data-toggle="tooltip" data-placement="top" title="Eliminar plan trabajo">
                      <i class="fa fa-trash"></i>
                    </button>`);
                  $(document).on('change', '#tipo_actividad', function() {
                    var valor = $(this).val();
                    console.log("cambio", valor, '===',"Empresa");
                    $.ajax( "/subactividad?term="+valor )
                    .always(function(data) {
                      if(data){
                        var html = '<p>Subactividad</p><select id="subtipo" name="subtipo_actividad" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                        $.each( data, function( key, value ) {
                          html += '<option value="'+value.id+'">'+value.text+'</option>';
                        });
                        html += '</select>';
                        $(".addOtro").html("");
                        $(".contSubTipo").html(html);
                        console.log(data);
                      }
                    });
                    if (valor === "Empresa") {
                      $.ajax( "/empresa" )
                      .always(function(data) {
                        if(data){
                          var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                          $.each( data, function( key, value ) {
                            html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                          });
                          html += '</datalist>';
                          $(".addOtro").html(html);
                          console.log(data);
                        }
                      });
                    }else if (valor === "Oportunidad") {
                      $.ajax( "/ssoportunidad" )
                      .always(function(data) {
                        if(data){
                          var html = '<p>Oportunidad</p><input list="browsers" id="oportunidad" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                          $.each( data, function( key, value ) {
                            html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                          });
                          html += '</datalist>';
                          $(".addOtro").html(html);
                          console.log(data);
                        }
                      });
                    }
                  });

                  $('body').on('click','#button-delete',function(){
    						    	var id = $(this).attr("name");
    						    	swal({
    							      title: 'Estas seguro?',
    							      html: $('<div>')
    							        .addClass('some-class')
    							        .text('¡No podras revertir esto!'),
    							      animation: false,
    							      customClass: 'animated tada',
    							      type: 'warning',
    							      showCancelButton: true,
    							      confirmButtonColor: '#3085d6',
    							      cancelButtonColor: '#d33',
    							      confirmButtonText: 'Si­, borrarlo!',
    							      cancelButtonText: 'Cancelar',
    							      closeOnConfirm: false,
    							      showLoaderOnConfirm: true,
    							      showLoaderOnConfirm: true,
    							      preConfirm: function () {
    							        return new Promise(function (resolve) {
    							          $.get( "{{ url('/') }}/plantrabajo/eliminar/"+id, function( data ) {
    							            if (data.success) {
    							            	$('#calendar').fullCalendar('next');
    											$('#calendar').fullCalendar('prev');
    							              swal('¡Eliminado!','Su plan de trabajo ha sido eliminada.','success');
    							              resolve()
    							            }else{
    							              swal("Algo salio mal, vuelve a intentar",'warning');
    							              resolve()
    							            }
    							          });
    							        })
    							      },
    							      allowOutsideClick: false
    							    });
  						    });

                  <?php if ($permiso_aprobar == "Si") { ?>
						    	$("#divRevisarButton").html(`
                    <textarea class="form-control" id="detalle_aprobar" rows="3" placeholder="Por favor ingrese un comentario"></textarea>
                    <button class="btn swal2-modal swal2-styled btn-success button-aprobar" data-title="Aprobado" name="`+event.id+`" data-toggle="tooltip" data-placement="top" title="Aprobar actividad">
                      <i class="fa fa-check"></i>
                    </button>
                    <button class="btn swal2-modal swal2-styled btn-primary button-aprobar" data-title="NoAprobado" name="`+event.id+`" data-toggle="tooltip" data-placement="top" title="Actividad no aprobada">
                      <i class="fa fa-times"></i>
                    </button>`);

                    $("body").on('mouseup', '.button-aprobar', function () {
                    	var id = $(this).attr("name");
                    	var text = $(this).attr("data-title");
                    	var detalle_aprobar = $("#detalle_aprobar").val();
                    	console.log(detalle_aprobar, " - ", detalle_aprobar.length);
                    	if (detalle_aprobar.length<=0) {
                    		swal("Por favor ingrese un comentario para gestionar la actividad");
                    	}else{
  								    	swal({
  									      title: 'Estas seguro?',
  									      html: $('<div>')
  									        .addClass('some-class')
  									        .text('Deseas cambiar el estado de esta oportunidad!'),
  									      animation: false,
  									      customClass: 'animated tada',
  									      type: 'success',
  									      showCancelButton: true,
  									      confirmButtonColor: '#3085d6',
  									      cancelButtonColor: '#d33',
  									      confirmButtonText: 'Si­, continuar!',
  									      cancelButtonText: 'Cancelar',
  									      closeOnConfirm: false,
  									      showLoaderOnConfirm: true,
  									      showLoaderOnConfirm: true,
  									      preConfirm: function () {
  									        return new Promise(function (resolve) {
  									          $.post( "{{ url('/') }}/plantrabajo/aprobar", { id: id, text: text, detalleaprobar: detalle_aprobar }, function( data ) {
  									            if (data.success) {
  									            	$('#calendar').fullCalendar('next');
  													$('#calendar').fullCalendar('prev');
  									              swal('¡Exito!','Su actividad ha sido gestionada con exito.','success');
  									              resolve()
  									            }else{
  									              swal("Algo salio mal, vuelve a intentar",'warning');
  									              resolve()
  									            }
  									          });
  									        })
  									      },
  									      allowOutsideClick: false
  									    });
  								    }
                    });
			              <?php } ?>

                    html=`<div class="row justify-content-md-center"><a name="titulobservaciones"></a>
					          <div class="col-md-12">
					            <h5 class="card-header"> Observaciones </h5>
					          </div>
					          <div class="col-md-12 centrado">`;
						        moment.locale('es');
							$.each(event.comentario, function( index, value ) {
								if (value.user.foto === "" || value.user.foto === null) {
								fotod = `<img src="http://via.placeholder.com/40x40?text=`+value.user.name+`" class="media-object img-circle">`;
								}else{
								fotod = `<img src="{{url('/')}}/images/file/clientes/`+value.user.foto+`" alt="`+value.user.name+`" class="media-object img-circle" style="max-width:40px">`;
								}
								html += `
								        <div class="media clearfix header-bottom">
								  <div class="media-left">
								    `+fotod+`
								  </div>
								  <div class="media-body" style="text-align: start;">
								  	<label style="text-align: start;font-size: 14px !important;">
								      `+value.user.name+` `+moment(value.created_at).calendar()+` <div class="label label-info">`+value.estado+`</div>
									</label><br>
								    <p class="text-muted username soloprimer normalp" style="font-size: 15px;font-weight: 400;">`+value.detalle+`</p>
								  </div>
								</div>`;
					        });
							html += `</div>
						        </div>`;
						    $("#lisComentariosId").html(html);


					  		if (semana === semanaActual && diasemanaActual < 2) {
				                $(".card-header").css('background-color', '#f0ad4e');
				                $(".panel-title p").css('color', '#f0ad4e');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/warning.png");
				                $("#tiempofuera").val(true);
				                $("#parrafoOk").text("Esta¡ llenando una plan de trabajo fuera de fecha.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#rgb(232, 72, 73);", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }else{
				            	$(".card-header").css('background-color', '#051d60');
				                $(".panel-title p").css('color', '#051d60');
				                $("#imgStado").attr("src","{{ url('/') }}/images/icons/checkedblue.png");
				                $("#tiempofuera").val(false);
				                $("#parrafoOk").text("Esta¡ llenando una plan de trabajo en la fecha acordada.");
				                $("#parrafoOk").css({"font-size": "small", "color": "#1d92af", "float": "right", "top": "8px", "left": "12px", "position": "relative"});
				            }

                    console.log(event.tipo_actividad, event.oportunidad_id, event.subtipo);
							$("#tipo_actividad").val(event.tipo_actividad);

              var valor = $("#tipo_actividad").val();
              $.ajax( "/subactividad?term="+valor )
              .always(function(data) {
                if(data){
                  var html = '<p>Subactividad</p><select id="subtipo" name="subtipo_actividad" class="swal2-input" required><option value="">Seleccione Subactividad</option>';
                  $.each( data, function( key, value ) {
                    html += '<option value="'+value.id+'">'+value.text+'</option>';
                  });
                  html += '</select>';
                  $(".addOtro").html("");
                  $(".contSubTipo").html(html);
                  $("#subtipo").val(event.subtipo);
                  console.log(data);
                }
              });
              if (valor === "Empresa") {
                $.ajax( "/empresa" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa" placeholder="Ingrese una empresa" required><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#empresa_id").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }else if (valor === "Oportunidad") {
                $.ajax( "/ssoportunidad" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Oportunidad</p><input list="browsers" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#oportunidad").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }

							$("#my-input").val(event.observaciones);

							funOportinidadhtml();
							$('#my-input').keyup(function() {
						        var chars = $(this).val().length;
						        if(chars>=50){
						            $('.num_car_act_columna').css('background-color','lightgreen');
						            icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
						        }else if(chars<=50){
						            $('.num_car_act_columna').css('background-color','yellow');
						            icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
						        }
						        $('#num_car_act').html(chars+' Caracteres '+icon);
						    });
						}
					}).then(function (result) {
						if (result.success) {
							$('#calendar').fullCalendar('next');
							$('#calendar').fullCalendar('prev');
							swal("Exito!","Plan de trabajo guardado con exito.","success");
						}else{
							swal("Error!","Algo ha salido mal.","warning");
						}
					}).catch(swal.noop)
				}else{
					swal({
					  title: 'Plan de trabajo',
					  animation: "slide-from-top",
					  confirmButtonText: 'Aceptar',
					  showLoaderOnConfirm: true,
					  html:
					    `
                <div class="row top-grande">
                  <div class="col-md-4">
                    <p>Tipo de actividad</p>
                    <select name="tipo_actividad" class="swal2-input" id="tipo_actividad" required readonly>
                        <option value="">Seleccione un tipo de actividad</option>
                        <option>Oportunidad</option>
                        <option>Empresa</option>
                        <option>Gestion comercial</option>
                        <option>Gestion interna ESSI</option>
                    </select>
                  </div>
                  <div class="col-md-4 contSubTipo"></div>
                  <div class="col-md-4 addOtro"></div>
                </div>
                <div class="form-group row opprohtml"></div>
					    	<div class="form-group row">
					          <div class="col-md-12">
					          	<label>Detalle exacto de las actividades</label>
				              	<div id="output"></div>
				                <textarea class="form-control" id="my-input" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones" readonly></textarea>
				                <small class="char-count"></small>
					          </div>
					        </div>`,
					  onOpen: function () {
					    var count = 0;
  						var input = $('#my-input'), display = $('.char-count'), count = 0, limit = -50;
              $("#tipo_actividad").val(event.tipo_actividad);

              var valor = $("#tipo_actividad").val();
              $.ajax( "/subactividad?term="+valor )
              .always(function(data) {
                if(data){
                  var html = '<p>Subactividad</p><select id="subtipo" name="subtipo_actividad" class="swal2-input" required readonly><option value="">Seleccione Subactividad</option>';
                  $.each( data, function( key, value ) {
                    html += '<option value="'+value.id+'">'+value.text+'</option>';
                  });
                  html += '</select>';
                  $(".addOtro").html("");
                  $(".contSubTipo").html(html);
                  $("#subtipo").val(event.subtipo);
                  console.log(data);
                }
              });
              if (valor === "Empresa") {
                $.ajax( "/empresa" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Empresas</p><input list="browsers" id="empresa_id" class="swal2-input" name="empresa" placeholder="Ingrese una empresa" required readonly><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#empresa_id").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }else if (valor === "Oportunidad") {
                $.ajax( "/ssoportunidad" )
                .always(function(data) {
                  if(data){
                    var html = '<p>Oportunidad</p><input list="browsers" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required readonly><datalist id="browsers">';
                    $.each( data, function( key, value ) {
                      html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
                    });
                    html += '</datalist>';
                    $(".addOtro").html(html);
                    $("#oportunidad").val(event.oportunidad_id);
                    console.log(data);
                  }
                });
              }
  						funOportinidadhtml();
  						$("#select_tipo_actividad").val(event.tipo_actividad);
  						$("#my-input").val(event.observaciones);
  						count = input.val().length
  						var remaining = limit + count
  						update(remaining);

  						input.keyup(function(e) {
  							count = $(this).val().length;
  							remaining = limit + count;

  							update(remaining);
  						});

  						function update(count) {
  							var txt = ( Math.abs(count) === 1 ) ? count + ' Caracteres' :  count + ' Caracteres'
  							display.html(txt);
  						}
					  }
					})
				}
		        return false;
		    },
		    loading: function(bool) {
				$('#loaders').toggle(bool);

				if (!bool) {
					$('#script-warning').hide();
				}
			},
			eventDrop: function(event, delta){console.log(event,delta);},
			eventResize: function(event) {console.log(event);}
		});


		var fechadato = $('#calendar').fullCalendar('getDate');
		var dateWeek = moment(fechadato).weeks();
		var dateWeek = moment().weeks(dateWeek);
		$("#tituloPrincipal").html("Plan de trabajo de la semana " + moment(dateWeek).weeks() + "<br>"+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));
		$('#calendar').fullCalendar( 'removeEventSources');
		$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
	        $.ajax({
	            url: urlList,
	            dataType: 'json',
	            data: {
	                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
	                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
	            },
	            success: function(doc) {
	            	respal = "";
					var push = false
					var feature = false;
					var features = [];
					doc2 = doc.sort(function (a, b) {
					  if (a.tipo_actividad > b.tipo_actividad) {
					    return 1;
					  }
					  if (a.tipo_actividad < b.tipo_actividad) {
					    return -1;
					  }
					  return 0;
					});
					var comp = doc2.length - 1;
					$.each( doc2, function( key, value ) {
						var a = moment(value.start);
						var b = moment(value.end);
						c = b.diff(a);
						c = parseInt(c/3600000);
						if (feature) {
							if (value.tipo_actividad != respal) {
								push = true;
								features.push(feature);
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
							}else{
								push = false;
								feature.tiempo = c+feature.tiempo;
							}
						}else{
							push = false;
							respal = value.tipo_actividad;
							feature = new Object();
							feature.tiempo = c;
							feature.user = value.tipo_actividad;
							feature.colorGrp = "rgb(103, 183, 220)";
							respal = value.tipo_actividad;
						}

						if (comp === key) {
							features.push(feature);
						}
					});
					var charts = AmCharts.makeChart("chartdiv2", {
						"theme": "light",
						"type": "serial",
						"startDuration": 2,
						"dataProvider": features,
						"valueAxes": [{
							"position": "left",
							"title": "Uso de tiempo de las actividades",
							"gridAlpha": 0,
							"dashLength": 0
						}],
						"gridAboveGraphs": false,
						"graphs": [{
							"balloonText": "[[category]]: <b>[[value]]</b>",
							"fillColorsField": "colorGrp",
							"fillAlphas": 1,
							"lineAlpha": 0.1,
							"type": "column",
							"valueField": "tiempo"
						}],
						"depth3D": 20,
						"angle": 30,
						"chartCursor": {
							"categoryBalloonEnabled": false,
							"cursorAlpha": 0,
							"zoomable": false
						},
						"categoryField": "user",
						"categoryAxis": {
							"gridPosition": "start",
							"gridAlpha": 0,
							"tickPosition": "start",
							"tickLength": 20,
							"labelRotation": 45
						},
						"export": {
							"enabled": true
						}
					});

					var feature2 = false;
					var features2 = [];
					otrodoc = doc.sort(function (a, b) {
						return a.oportunidad.localeCompare(b.oportunidad);
					});
					$.each( otrodoc, function( key, value ) {
						var a = moment(value.start);
						var b = moment(value.end);
						c = b.diff(a);
						c = parseInt(c/3600000);
						if (feature2) {
							if (value.oportunidad != respal) {
								push = true;
								features2.push(feature2);
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
							}else{
								push = false;
								feature2.tiempo = c+feature2.tiempo;
							}
						}else{
							push = false;
							respal = value.oportunidad;
							feature2 = new Object();
							feature2.tiempo = c;
							feature2.user = value.oportunidad;
							feature2.colorGrp = "rgb(103, 183, 220)";
							respal = value.oportunidad;
						}
						var comp = otrodoc.length - 1;
						if (comp === key) {
							features2.push(feature2);
						}
					});
					var chart = AmCharts.makeChart( "chartdiv", {
						"type": "pie",
						"theme": "light",
						"titles": [ {
							"text": "Uso de tiempo con respecto a las oportunidades",
							"size": 16
						} ],
						"dataProvider": features2,
						"valueField": "tiempo",
						"titleField": "user",
						"startEffect": "elastic",
						"startDuration": 2,
						"labelRadius": 15,
						"innerRadius": "50%",
						"depth3D": 10,
						"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
						"angle": 15,
						"export": {
							"enabled": true
						}
					});
					callback(doc);
	            },
	            error: function() {
					$('#script-warning').show();
				}
	        });
	    });



		$('#semanas').select2();
        $('#responsable').select2({
            // Activamos la opcion "Tags" del plugin
             placeholder: "Seleccione una responsable",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("vendedores") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function(data, page) {
                  return {
                    results: data
                  };
                },
            }
        });


        $('#responsable').on('change', function() {
			if (this.value) {
				urlList = '{{ url("listadoplandetrabajo") }}/'+this.value;
				$('#calendar').fullCalendar( 'removeEventSources');
				$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
			        $.ajax({
			            url: urlList,
			            dataType: 'json',
			            data: {
			                start: moment(start).format('YYYY-MM-DD'),
			                end: moment(end).format('YYYY-MM-DD')
			            },
			            success: function(doc) {
			            	respal = "";
							var push = false
							var feature = false;
							var features = [];
							doc2 = doc.sort(function (a, b) {
							  if (a.tipo_actividad > b.tipo_actividad) {
							    return 1;
							  }
							  if (a.tipo_actividad < b.tipo_actividad) {
							    return -1;
							  }
							  // a must be equal to b
							  return 0;
							});
							var comp = doc2.length - 1;
							$.each( doc2, function( key, value ) {
								var a = moment(value.start);
								var b = moment(value.end);
								c = b.diff(a);
								c = parseInt(c/3600000);
								if (feature) {
									if (value.tipo_actividad != respal) {
										push = true;
										features.push(feature);
										respal = value.tipo_actividad;
										feature = new Object();
										feature.tiempo = c;
										feature.user = value.tipo_actividad;
										feature.colorGrp = "rgb(103, 183, 220)";
									}else{
										push = false;
										feature.tiempo = c+feature.tiempo;
									}
								}else{
									push = false;
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
									respal = value.tipo_actividad;
								}

								if (comp === key) {
									features.push(feature);
								}
							});
							var charts = AmCharts.makeChart("chartdiv2", {
								"theme": "light",
								"type": "serial",
								"startDuration": 2,
								"dataProvider": features,
								"valueAxes": [{
									"position": "left",
									"title": "Uso de tiempo de las actividades",
									"gridAlpha": 0,
									"dashLength": 0
								}],
								"gridAboveGraphs": false,
								"graphs": [{
									"balloonText": "[[category]]: <b>[[value]]</b>",
									"fillColorsField": "colorGrp",
									"fillAlphas": 1,
									"lineAlpha": 0.1,
									"type": "column",
									"valueField": "tiempo"
								}],
								"depth3D": 20,
								"angle": 30,
								"chartCursor": {
									"categoryBalloonEnabled": false,
									"cursorAlpha": 0,
									"zoomable": false
								},
								"categoryField": "user",
								"categoryAxis": {
									"gridPosition": "start",
									"gridAlpha": 0,
									"tickPosition": "start",
									"tickLength": 20,
									"labelRotation": 45
								},
								"export": {
									"enabled": true
								}
							});

							var feature2 = false;
							var features2 = [];
							otrodoc = doc.sort(function (a, b) {
								return a.oportunidad.localeCompare(b.oportunidad);
							});
							$.each( otrodoc, function( key, value ) {
								var a = moment(value.start);
								var b = moment(value.end);
								c = b.diff(a);
								c = parseInt(c/3600000);
								if (feature2) {
									if (value.oportunidad != respal) {
										push = true;
										features2.push(feature2);
										respal = value.oportunidad;
										feature2 = new Object();
										feature2.tiempo = c;
										feature2.user = value.oportunidad;
										feature2.colorGrp = "rgb(103, 183, 220)";
									}else{
										push = false;
										feature2.tiempo = c+feature2.tiempo;
									}
								}else{
									push = false;
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
									respal = value.oportunidad;
								}
								var comp = otrodoc.length - 1;
								if (comp === key) {
									features2.push(feature2);
								}
							});
							var chart = AmCharts.makeChart( "chartdiv", {
								"type": "pie",
								"theme": "light",
								"titles": [ {
									"text": "Uso de tiempo con respecto a las oportunidades",
									"size": 16
								} ],
								"dataProvider": features2,
								"valueField": "tiempo",
								"titleField": "user",
								"startEffect": "elastic",
								"startDuration": 2,
								"labelRadius": 15,
								"innerRadius": "50%",
								"depth3D": 10,
								"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
								"angle": 15,
								"export": {
									"enabled": true
								}
							});
							callback(doc);
			            },
			            error: function() {
							$('#script-warning').show();
						}
			        });
			    });
			}
		});

		$("#btnNext").click(function(){
			$('#calendar').fullCalendar('next');
			var fechadato = $('#calendar').fullCalendar('getDate');
			var dateWeek = moment(fechadato).weeks();
			var diahoy = moment().format('dddd');
            var semanahoy = dateWeek;
            if(diahoy === "domingo"){
               dateWeek = parseInt(dateWeek) - 1;
            }
			var dateWeek = moment().weeks(dateWeek);
			$("#tituloPrincipal").html("Plan de trabajo de la semana " + semanahoy + "<br>"+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));


			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		});
		$("#btnToday").click(function(){
			$('#calendar').fullCalendar('today');
			var fechadato = $('#calendar').fullCalendar('getDate');
			var dateWeek = moment(fechadato).weeks();
			var diahoy = moment().format('dddd');
            var semanahoy = dateWeek;
            if(diahoy === "domingo"){
               dateWeek = parseInt(dateWeek) - 1;
            }
			var dateWeek = moment().weeks(dateWeek);
			$("#tituloPrincipal").html("Plan de trabajo de la semana " + semanahoy + " <br> "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));
			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		});
		$("#btnPrev").click(function(){
			$('#calendar').fullCalendar('prev');
			var fechadato = $('#calendar').fullCalendar('getDate');
			var dateWeek = moment(fechadato).weeks();
			var diahoy = moment().format('dddd');
            var semanahoy = dateWeek;
            if(diahoy === "domingo"){
               dateWeek = parseInt(dateWeek) - 1;
            }
			var dateWeek = moment().weeks(dateWeek);
			$("#tituloPrincipal").html("Plan de trabajo de la semana " + semanahoy + " <br> "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));
			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		});

		$('#semanas').on('change', function() {
			if (this.value) {
					var dateWeek = moment().weeks(this.value);
					$("#tituloPrincipal").html("Plan de trabajo de la semana " + moment(dateWeek).weeks() + " <br> "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D"));
					$('#calendar').fullCalendar( 'removeEventSources');
					$('#calendar').fullCalendar ('gotoDate', dateWeek);
					$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
				        $.ajax({
				            url: urlList,
				            dataType: 'json',
				            data: {
				                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
				                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
				            },
				            success: function(doc) {
				            	respal = "";
								var push = false
								var feature = false;
								var features = [];
								doc2 = doc.sort(function (a, b) {
								  if (a.tipo_actividad > b.tipo_actividad) {
								    return 1;
								  }
								  if (a.tipo_actividad < b.tipo_actividad) {
								    return -1;
								  }
								  return 0;
								});
								var comp = doc2.length - 1;
								$.each( doc2, function( key, value ) {
									var a = moment(value.start);
									var b = moment(value.end);
									c = b.diff(a);
									c = parseInt(c/3600000);
									if (feature) {
										if (value.tipo_actividad != respal) {
											push = true;
											features.push(feature);
											respal = value.tipo_actividad;
											feature = new Object();
											feature.tiempo = c;
											feature.user = value.tipo_actividad;
											feature.colorGrp = "rgb(103, 183, 220)";
										}else{
											push = false;
											feature.tiempo = c+feature.tiempo;
										}
									}else{
										push = false;
										respal = value.tipo_actividad;
										feature = new Object();
										feature.tiempo = c;
										feature.user = value.tipo_actividad;
										feature.colorGrp = "rgb(103, 183, 220)";
										respal = value.tipo_actividad;
									}

									if (comp === key) {
										features.push(feature);
									}
								});
								var charts = AmCharts.makeChart("chartdiv2", {
									"theme": "light",
									"type": "serial",
									"startDuration": 2,
									"dataProvider": features,
									"valueAxes": [{
										"position": "left",
										"title": "Uso de tiempo de las actividades",
										"gridAlpha": 0,
										"dashLength": 0
									}],
									"gridAboveGraphs": false,
									"graphs": [{
										"balloonText": "[[category]]: <b>[[value]]</b>",
										"fillColorsField": "colorGrp",
										"fillAlphas": 1,
										"lineAlpha": 0.1,
										"type": "column",
										"valueField": "tiempo"
									}],
									"depth3D": 20,
									"angle": 30,
									"chartCursor": {
										"categoryBalloonEnabled": false,
										"cursorAlpha": 0,
										"zoomable": false
									},
									"categoryField": "user",
									"categoryAxis": {
										"gridPosition": "start",
										"gridAlpha": 0,
										"tickPosition": "start",
										"tickLength": 20,
										"labelRotation": 45
									},
									"export": {
										"enabled": true
									}
								});

								var feature2 = false;
								var features2 = [];
								otrodoc = doc.sort(function (a, b) {
									return a.oportunidad.localeCompare(b.oportunidad);
								});
								$.each( otrodoc, function( key, value ) {
									var a = moment(value.start);
									var b = moment(value.end);
									c = b.diff(a);
									c = parseInt(c/3600000);
									if (feature2) {
										if (value.oportunidad != respal) {
											push = true;
											features2.push(feature2);
											respal = value.oportunidad;
											feature2 = new Object();
											feature2.tiempo = c;
											feature2.user = value.oportunidad;
											feature2.colorGrp = "rgb(103, 183, 220)";
										}else{
											push = false;
											feature2.tiempo = c+feature2.tiempo;
										}
									}else{
										push = false;
										respal = value.oportunidad;
										feature2 = new Object();
										feature2.tiempo = c;
										feature2.user = value.oportunidad;
										feature2.colorGrp = "rgb(103, 183, 220)";
										respal = value.oportunidad;
									}
									var comp = otrodoc.length - 1;
									if (comp === key) {
										features2.push(feature2);
									}
								});
								var chart = AmCharts.makeChart( "chartdiv", {
									"type": "pie",
									"theme": "light",
									"titles": [ {
										"text": "Uso de tiempo con respecto a las oportunidades",
										"size": 16
									} ],
									"dataProvider": features2,
									"valueField": "tiempo",
									"titleField": "user",
									"startEffect": "elastic",
									"startDuration": 2,
									"labelRadius": 15,
									"innerRadius": "50%",
									"depth3D": 10,
									"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
									"angle": 15,
									"export": {
										"enabled": true
									}
								});
								callback(doc);
				            },
				            error: function() {
								$('#script-warning').show();
							}
				        });
				    });
				}
		});

		setTimeout(function(){
			$('#calendar').fullCalendar('next');
			$('#calendar').fullCalendar('prev');
			$('#calendar').fullCalendar('render');
		}, 1000);

    });

	funMostarDiv = function (valor) {
	    $(".active").removeClass('active');
	    $("#b"+valor).addClass('active');
	    $(".slideInDown").removeClass('slideInDown').addClass('rollOut');
	    if(valor==="grafica"){
	      $("#grafica").show();
	    }else{
	      $("#grafica").hide();
	    }
	    if(valor==="calendar"){
	      $("#calendar").show();
	      $("#calendar").fullCalendar('render');
	    }else{
	      $("#calendar").hide();
	    }
	    $("#"+valor).removeClass('rollOut').addClass('slideInDown');
	}

	funOportinidadhtml = function() {
		id = $("#oportunidad").val();
	  	var valores = [];
    	<?php
    		$valores = [];
    		class opproduc {
				public $id;
			  	public $oportunidad;
			 	public $producto;
			}
          	$valobj = new opproduc;
		  	$valobj->id = "Trabajos Generales ESSI";
		  	$valobj->oportunidad = "Trabajos Generales ESSI";
		  	$valobj->producto = "";
  		  	$valores[] = $valobj;

    		foreach ($oportunidades as $tag) {
    			$productos = App\OportunidadProducto::where("oportunidad_id",$tag->id)->get();
    			$html="<div class='col-12'><h4>".$tag->empresa.'/'.$tag->ciudad."</h4></div>";
    			foreach ($productos as $res) {
                    try {
                        if (isset($res->producto) && is_numeric($res->producto)) {
                            $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                            if (isset($res->referencia) && is_numeric($res->referencia)) {
                                $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                            }else{
                                $referencia->referencia = "Estandar";
                            }

                            if(!empty($referencia->foto) && $referencia->foto != Null){
                                $foto = url('/')."/images/file/productos/".$referencia->foto;
                            }else{
                              	$foto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Foto&w=100&h=100';
                            }
                            $html .= '
                            <div class="col-3 placeholder img-sedes">
                            	<div class="img-thumbnail img-sedes">
	                              <a href="">
	                                <img src="'.$foto.'" style="max-width: 50%;" class="img-fluid mx-auto d-block">
	                              </a>
                            	</div>
                            	<div class="text-muted centrado">
                            		<h6 style="margin-bottom: 0px;">'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.'</h6>
                            		<p>$ '.$res->total.'</p>
                            	</div>
                            </div>';
                        }
                    } catch (Exception $e) { }
                }
                $valobj = new opproduc;
				$valobj->id = $tag->id;
				$valobj->oportunidad = $tag->empresa.'/'.$tag->ciudad;
				$valobj->producto = $html;
          		$valores[] = $valobj;
			}
			//echo json_encode($valores);
        ?>
        var valores = <?php echo json_encode($valores); ?>;
        new_arr = $.grep(valores, function(n, i){
            return n.id == id;
        });

        if (new_arr) {
        	try {
        		$(".opprohtml").html(new_arr[0].producto);
        	} catch(err) {
			    console.log(err.message);
			}
        }
	}

	$('body').on('change', '#oportunidad', function() {
	  	funOportinidadhtml();
	});


</script>
@endsection
