@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Gastos')

@section('content')

<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
#chartdiv{
  width: 100%;
  height: 500px !important;
}

#chartdiv-2{
  width: 100%;
  height: 500px;
}

#chartdiv-3{
  width: 100%;
  height: 500px;
}
#chartdiv-4{
  width: 100%;
  height: 500px;
}
.amcharts-graph-g2 .amcharts-graph-stroke {
  stroke-dasharray: 3px 3px;
  stroke-linejoin: round;
  stroke-linecap: round;
  -webkit-animation: am-moving-dashes 1s linear infinite;
  animation: am-moving-dashes 1s linear infinite;
}

@-webkit-keyframes am-moving-dashes {
  100% {
    stroke-dashoffset: -31px;
  }
}
@keyframes am-moving-dashes {
  100% {
    stroke-dashoffset: -31px;
  }
}


.lastBullet {
  -webkit-animation: am-pulsating 1s ease-out infinite;
  animation: am-pulsating 1s ease-out infinite;
}
@-webkit-keyframes am-pulsating {
  0% {
    stroke-opacity: 1;
    stroke-width: 0px;
  }
  100% {
    stroke-opacity: 0;
    stroke-width: 50px;
  }
}
@keyframes am-pulsating {
  0% {
    stroke-opacity: 1;
    stroke-width: 0px;
  }
  100% {
    stroke-opacity: 0;
    stroke-width: 50px;
  }
}

.amcharts-graph-column-front {
  -webkit-transition: all .3s .3s ease-out;
  transition: all .3s .3s ease-out;
}
.amcharts-graph-column-front:hover {
  fill: #496375;
  stroke: #496375;
  -webkit-transition: all .3s ease-out;
  transition: all .3s ease-out;
}

.amcharts-graph-g3 {
  stroke-linejoin: round;
  stroke-linecap: round;
  stroke-dasharray: 500%;
  stroke-dasharray: 0 /;    /* fixes IE prob */
  stroke-dashoffset: 0 /;   /* fixes IE prob */
  -webkit-animation: am-draw 40s;
  animation: am-draw 40s;
}
@-webkit-keyframes am-draw {
    0% {
        stroke-dashoffset: 500%;
    }
    100% {
        stroke-dashoffset: 0%;
    }
}
@keyframes am-draw {
    0% {
        stroke-dashoffset: 500%;
    }
    100% {
        stroke-dashoffset: 0%;
    }
}
/* OVERWRITE OUR MAIN STYLE */
.demo-flipper-front.demo-panel-white, body {
  background-color: #161616;
}

.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}

.alert {
    -webkit-animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    margin-bottom: 10px;
}

.alert-dark {
    background-color: rgba(228, 104, 104, 0.9);
    border-color: rgba(0, 0, 0, 0.8);
    color: #fff;
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}
.table td{
    border: 0 !important;
}
.input-group select{
  padding: 8px !important;
}
.red{
  color: #ea122a;
}
.form-control.red:focus {
    color: #ea122a !important;
}
</style>
@php 
  $numero_dias = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
  $meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

@endphp
@php 
  $ajus=true;
  
  $resta= strtotime(date('Y-m-')."10")-strtotime(date('Y-m-d'));
  $fin =round($resta/(60*60*24));
  $msj=$fin;
   
@endphp
@foreach($ajustes as $ajuste)
  @if($ajuste->mes==date('m',strtotime("-1 month"))&&$ajuste->anio==date('Y',strtotime("-1 month"))&&$ajuste->user_id==$id||($ajuste->user_id==$id&&date('d')<10))
  @php $ajus=false; 
  
  @endphp
  @endif

@endforeach
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
       <div class="pull-right">
        <div class="btn-group">
        <?php if($fun->cargo != 'Gerente Comercial'){ if($fun->name != 'Administrador sistema'){ ?>
          <a href="{{url('/')}}/presupuestoanterior" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right" aria-hidden="true"></i> Presupuesto de {{$meses[(int)date("m")-1]}}</a>
        <?php } } ?>
          <a href="{{url('/')}}/presupuesto" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right" aria-hidden="true"></i> Presupuesto de {{$meses[(int)(date("m"))]}}</a>
        <?php if($fun->cargo != 'Gerente Comercial'){ if($fun->name != 'Administrador sistema'){ ?>
          <a href="{{url('/')}}/presupuestoanual" class="btn btn-sm btn-warning"><i class="fa fa-calendar" aria-hidden="true"></i> Presupuesto Año {{date("Y")}}</a>
        <?php } } ?>
          <a href="{{url('/')}}/presupuestoanualsiguiente" class="btn btn-sm btn-warning"><i class="fa fa-calendar" aria-hidden="true"></i> Presupuesto Año {{date("Y")+1}}</a>
          <a href="{{url('/')}}/ajustes" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Ajuste mes {{$meses[(int)date("m")-2]}}</a>
        </div>
      </div>
      <?php if($fun->cargo == 'Gerente Comercial' || $fun->name == 'Administrador sistema'){ ?>
      <form method="POST" action="{{ url('filtrargastosprueba') }}" enctype="multipart/form-data" >
      <div class="col-md-12">
        <div class="row" style="margin-top: 2.5%;">
          <div class="col-md-4">
            <select class="form-control filtro" name="usuario" id="filtro_usuarios" style="height: 100%">
              <option value="">Filtrar por usuario</option>
              <?php foreach($usuarios_fil as $usuario){ ?>
                <option value="{{$usuario->id}}" <?php if(isset($filtros['usuario_f'])){ if($filtros['usuario_f']==$usuario->id){ echo 'Selected'; }} ?> >{{$usuario->nombres.' '.$usuario->apellidos}}</option>
              <?php  } ?>
            </select>
          </div>
          <div class="col-md-4">
            <select class="form-control filtro" name="mes" id="filtro_mes" style="height: 100%">
              <option value="">Filtrar por mes</option>
              <?php 
              
              for($i=0;$i<=11;$i++){
                $fecha=strtotime ( '-'.$i.' month' , strtotime ( date('Y-m') ) ) ;
                $numero_mes=((int)(date('m', $fecha)))-1;
                $mes=$meses[$numero_mes];
                $ano=date('Y' , $fecha);
              ?>
              <option value="{{date ( 'Y-m' , $fecha )}}" <?php if(isset($filtros['mes_completo_f'])){ if($filtros['mes_completo_f']==date ( 'Y-m' , $fecha )){ echo 'Selected'; }} ?> >{{$mes}} de {{$ano}}</option>
            <?php
              }
              ?>
            </select>
          </div>
          <div class="col-md-3">
            <select class="form-control filtro" name="tipo" id="filtro_tipo" style="height: 100%;">
              <option value="">Filtrar por tipo</option>
              <option value="alimentacion" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="alimentacion"){ echo 'Selected'; }} ?>>Alimentacion</option>
              <option value="transporte_interno" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="transporte_interno"){ echo 'Selected'; }} ?>>Transporte Interno</option>
              <option value="transporte_intermunicipal" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="transporte_intermunicipal"){ echo 'Selected'; }} ?>>Transporte Intermunicipal</option>
              <option value="tiquete_aereo" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="tiquete_aereo"){ echo 'Selected'; }} ?>>Tiquete Aereo</option>
              <option value="papeleria" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="papeleria"){ echo 'Selected'; }} ?>>Papeleria</option>
              <option value="invitacion_cliente" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="invitacion_cliente"){ echo 'Selected'; }} ?>>Invitacion Cliente</option>
              <option value="alquiler_vehiculo" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="alquiler_vehiculo"){ echo 'Selected'; }} ?>>Alquiler Vehiculo</option>
              <option value="gasolina_pasaje" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="gasolina_pasaje"){ echo 'Selected'; }} ?>>Gasolina Pasaje</option>
              <option value="hotel" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="hotel"){ echo 'Selected'; }} ?>>Hotel</option>
              <option value="otros" <?php if(isset($filtros['tipo_f'])){ if($filtros['tipo_f']=="otros"){ echo 'Selected'; }} ?>>Otros</option>
            </select>
          </div>
          <div class="col-md-1">
            <button type="submit" id="btn_filtrar" class="btn btn-essi pull-right" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            <i class="fa fa-search" aria-hidden="true" style="cursor: pointer;" title="filtrar" id="aplicar_filtro"></i>
          </div>
        </div>
      </div>
      </form>
      <?php } ?>
         <div class="col-md-12 text-center inline-block">
            <h3>Grafico Comparativo de Gastos @if(isset($filtros['mes_completo_f']) && $filtros['mes_completo_f']!='') {{$meses[((int)date('m',strtotime($filtros['mes_completo_f'])))-1]}} @else {{$meses[(int)date('m',strtotime("-1 month"))]}} @endif de {{date('Y',strtotime("-1 month"))}}</h3>
          </div>
          <div class="col-md-12">
            <div id="chartdiv-3"></div>
          </div>
          <div class="col-md-12">
            @if(!$ajus)
              <div class="alert alert-dark bpx" role="alert" >
                <!--<strong>Atención!</strong> quedan {{$msj}} dias para poder agregar un ajuste al presupuesto para el mes de {{$meses[(int)date('m',strtotime("-2 month"))]}}-->
            </div>
            @endif
          </div>
        
          <div class="col-md-12 text-center inline-block">
            <h3>Grafico Comparativo de Gastos vs Presupuestado (Anual)</h3>
          </div>
          <div class="col-md-12">
            <div id="chartdiv-4"></div>
          </div>          
          
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script src="{{url('/')}}/components/amchart/js/amcharts.js"></script>
<script src="{{url('/')}}/components/amchart/js/serial.js"></script>
<script src="{{url('/')}}/components/amchart/js/light.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="{{url('/')}}/components/amchart/css/export.css" type="text/css" media="all" />
<script src="{{url('/')}}/components/amchart/js/light.js"></script>

<script type="text/javascript">
$("body").on("click","#aplicar_filtro",function(){
  $('#btn_filtrar').click();
  
});
$("body").on("keyup",".signo",function(){
  if($("."+$(this).attr("name")).val()=="-"){
    $(this).addClass("red");
  }else{
    $(this).removeClass("red");
  }

});
$("body").on("change","#filtro",function(e){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/filtrogastos/"+$(this).val(), 
        cache: false,
        type: 'GET',
        data:{"Calendario":'<?php echo $numero_dias ?>'},
        success: function(e){ 
            eval(e.gastos);
            eval(e.anual);
            eval(e.diario);
        }
    });
})
$("body").on("click",".guardar_ajuste",function(e){
  $("#guardar_ajuste").modal();
})


$(function () {
    $(".dinero").maskMoney();
  })

var chart = AmCharts.makeChart( "chartdiv-3", {
  "type": "serial",
  "legend": {
      "useGraphSettings": true
  },
  "addClassNames": true,
  "theme": "light",
  "autoMargins": false,
  "marginLeft": 90,
  "marginRight": 8,
  "marginTop": 10,
  "marginBottom": 26,
  "balloon": {
    "adjustBorderColor": false,
    "horizontalPadding": 10,
    "verticalPadding": 8,
    "color": "#ffffff"
  },

  "dataProvider": [
  <?php
    if(isset($filtros['mes_completo_f']) && $filtros['mes_completo_f']!=''){
      $fecha_filtro=$filtros['mes_completo_f'];
    }else{
      $fecha_filtro=date('Y-m-d');
    }

    if(isset($filtros['usuario_f']) && $filtros['usuario_f']!=''){
      $usuario_filtro=$filtros['usuario_f'];
    }else{
      $usuario_filtro=$id;
    }

  ?>
   @php $acumulado=0; $diario_presupuesto=0; @endphp
    @for($i=1;$i<=$numero_dias;$i++)
    @php $diario=0; @endphp
    @if($i<10)
    @php $i="0".$i; @endphp
    @endif
    @foreach($actividades as $key)
    @if($key->fecha_actividad==date('Y-m-', strtotime($fecha_filtro)).$i&&$key->user_id==$usuario_filtro)
    <?php 
    if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
      $diario=str_replace(",","",$key->alimentacion);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
      $diario=str_replace(",","",$key->transportes_internos);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
      $diario=str_replace(",","",$key->transportes_intermunicipales);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
      $diario=str_replace(",","",$key->tiquete_aereo);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
      $diario=str_replace(",","",$key->papeleria);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
      $diario=str_replace(",","",$key->invitacion_cliente);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
      $diario=str_replace(",","",$key->alquiler_vehiculo);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
      $diario=str_replace(",","",$key->gasolina_pasaje);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
      $diario=str_replace(",","",$key->hotel);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
      $diario=str_replace(",","",$key->otros);
    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
      $diario=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
    }
    ?>
    @php 

    $acumulado=$acumulado+$diario; @endphp
    @endif
    @endforeach
    <?php 
    foreach($mi_presupuesto as $m_p){     
      if($m_p->dia == $i){
        if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
          $diario_presupuesto+=str_replace(",","",$m_p->alimentacion);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
          $diario_presupuesto+=str_replace(",","",$m_p->transporte_interno);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
          $diario_presupuesto+=str_replace(",","",$m_p->transporte_intermunicipal);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
          $diario_presupuesto+=str_replace(",","",$m_p->tiquete_aereo);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
          $diario_presupuesto+=str_replace(",","",$m_p->papeleria);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
          $diario_presupuesto+=str_replace(",","",$m_p->invitacion_cliente);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
          $diario_presupuesto+=str_replace(",","",$m_p->alquiler_vehiculo);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
          $diario_presupuesto+=str_replace(",","",$m_p->gasolina_pasaje);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
          $diario_presupuesto+=str_replace(",","",$m_p->hotel);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
          $diario_presupuesto+=str_replace(",","",$m_p->otros);
        }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
          $diario_presupuesto+=str_replace(",","",$m_p->alimentacion)+str_replace(",","",$m_p->transporte_interno)+str_replace(",","",$m_p->transporte_intermunicipal)+str_replace(",","",$m_p->tiquete_aereo)+str_replace(",","",$m_p->papeleria)+str_replace(",","",$m_p->invitacion_cliente)+str_replace(",","",$m_p->alquiler_vehiculo)+str_replace(",","",$m_p->gasolina_pasaje)+str_replace(",","",$m_p->hotel)+str_replace(",","",$m_p->otros);
        }
      ?>

    <?php } } ?>

    {
    "year": "{{$i}}",
    "income": {{$acumulado}},
    "expenses": {{$diario_presupuesto}}
  }, 
  @endfor
  {
    "year": "{{$i}}",
    "income": {{$acumulado-$suma_ajuste}},
    "expenses": {{$diario_presupuesto}}
  },
    ],
  "valueAxes": [ {
    "axisAlpha": 0,
    "position": "left"
  } ],
  "startDuration": 1,
  "graphs": [ {
    "alphaField": "alpha",
    "balloonText": "<span style='font-size:12px;'>[[title]] en {{$meses[date("m")-1]}} [[category]]:<br><span style='font-size:20px;'>$ [[value]]</span> [[additional]]</span>",
    "fillAlphas": 1,
    "title": "Gastado según bitácora",
    "type": "column",
    "valueField": "income",
    "dashLengthField": "dashLengthColumn"
  }, {
    "id": "graph2",
    "balloonText": "<span style='font-size:12px;'>[[title]] en {{$meses[date("m")-1]}} [[category]]:<br><span style='font-size:20px;'>$ [[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Presupuesto mensual de gastos",
    "valueField": "expenses",
    "dashLengthField": "dashLengthLine"
  } ],
  "categoryField": "year",
  "categoryAxis": {
    "gridPosition": "start",
    "axisAlpha": 0,
    "tickLength": 0
  },
  "export": {
    "enabled": false
  }
} );



AmCharts.makeChart("chartdiv-4",
        {
          "type": "serial",
          "legend": {
              "useGraphSettings": true
          },
          "categoryField": "category",
          "autoMarginOffset": 40,
          "marginRight": 60,
          "marginTop": 60,
          "startDuration": 1,
          "fontSize": 13,
          "theme": "default",
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-1",
              "lineThickness": 3,
              "title": "Presupuesto anual",
              "type": "smoothedLine",
              "valueField": "column-1"
            },
            {
              
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-2",
              "lineThickness": 3,
              "title": "Gastos según bitácora",
              "type": "smoothedLine",
              "valueField": "column-2"
            },
            {
              
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-3",
              "lineThickness": 3,
              "title": "Presupuesto mensual de gastos",
              "type": "smoothedLine",
              "valueField": "column-3"
            },
            
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "titles": [],
          "dataProvider": [
          <?php
            $totalp=0;
            $totalpind=0;
            $totalg=0;

            for($i=0;$i<12;$i++){ 
                
                foreach ($anuales as $key) {
                  # code...
                  $o=$i+1;
                  if($o==$key->mes){
                    if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
                      $totalp+=str_replace(",","",$key->alimentacion);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
                      $totalp+=str_replace(",","",$key->transporte_interno);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
                      $totalp+=str_replace(",","",$key->transporte_intermunicipal);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
                       $totalp+=str_replace(",","",$key->tiquete_aereo);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
                      $totalp+=str_replace(",","",$key->papeleria);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
                      $totalp+=str_replace(",","",$key->invitacion_cliente);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
                      $totalp+=str_replace(",","",$key->alquiler_vehiculo);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
                      $totalp+=str_replace(",","",$key->gasolina_pasaje);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
                      $totalp+=str_replace(",","",$key->hotel);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
                      $totalp+=str_replace(",","",$key->otros);
                    }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
                      $totalp+=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transporte_interno)+str_replace(",","",$key->transporte_intermunicipal)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasaje)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
                    }
                  }
                } 
                foreach ($actividades as $key) {
                  # code...
                  $o=$i+1;
                  if($o<10){
                    $o="0".$o;
                  }
                  for($k=1;$k<=31;$k++){
                    if($k<10){
                      $k="0".$k;
                    }
                    if(date("Y", strtotime($fecha_filtro))."-".$o."-".$k==$key->fecha_actividad && $key->user_id==$usuario_filtro){
                      if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
                        $totalg+=str_replace(",","",$key->alimentacion);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
                        $totalg+=str_replace(",","",$key->transportes_internos);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
                        $totalg+=str_replace(",","",$key->transportes_intermunicipales);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
                         $totalg+=str_replace(",","",$key->tiquete_aereo);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
                        $totalg+=str_replace(",","",$key->papeleria);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
                        $totalg+=str_replace(",","",$key->invitacion_cliente);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
                        $totalg+=str_replace(",","",$key->alquiler_vehiculo);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
                        $totalg+=str_replace(",","",$key->gasolina_pasajes);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
                        $totalg+=str_replace(",","",$key->hotel);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
                        $totalg+=str_replace(",","",$key->otros);
                      }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
                        $totalg=$totalg+str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
                      }                      
                    }
                  }
                }
                if(isset($ajustemeses[$i+1])){
                  $totalg=$totalg-$ajustemeses[$i+1];
                }
              foreach ($individuales as $ind) {
                 if(date("Y", strtotime($fecha_filtro))==$ind->anio&&((int)$i+1)==$ind->mes&&$ind->user_id==$usuario_filtro){
                  if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alimentacion"){
                    $totalpind+=str_replace(",","",$ind->alimentacion);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_interno"){
                    $totalpind+=str_replace(",","",$ind->transporte_interno);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="transporte_intermunicipal"){
                    $totalpind+=str_replace(",","",$ind->transporte_intermunicipal);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="tiquete_aereo"){
                     $totalpind+=str_replace(",","",$ind->tiquete_aereo);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="papeleria"){
                    $totalpind+=str_replace(",","",$ind->papeleria);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="invitacion_cliente"){
                    $totalpind+=str_replace(",","",$ind->invitacion_cliente);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="alquiler_vehiculo"){
                    $totalpind+=str_replace(",","",$ind->alquiler_vehiculo);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="gasolina_pasaje"){
                    $totalpind+=str_replace(",","",$ind->gasolina_pasaje);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="hotel"){
                    $totalpind+=str_replace(",","",$ind->hotel);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']=="otros"){
                    $totalpind+=str_replace(",","",$ind->otros);
                  }else if(isset($filtros['tipo_f'])&&$filtros['tipo_f']==""){
                    $totalpind=$totalpind+str_replace(",","",$ind->alimentacion)+str_replace(",","",$ind->transporte_interno)+str_replace(",","",$ind->transporte_intermunicipal)+str_replace(",","",$ind->tiquete_aereo)+str_replace(",","",$ind->papeleria)+str_replace(",","",$ind->invitacion_cliente)+str_replace(",","",$ind->alquiler_vehiculo)+str_replace(",","",$ind->gasolina_pasaje)+str_replace(",","",$ind->hotel)+str_replace(",","",$ind->otros);
                  }                    
                }
              }
              if($i<10){
                $o="0".($i+1);
              }else{
                 $o=$i+1;
              }
              $dia=15;
              if($o==12){
                $dia=10;
              }
                ?>
                {
                  "category": "{{date('Y-').$o}}",
                  "column-1": {{$totalp}},
                  "column-2": {{$totalg}},
                  "column-3": {{$totalpind}}
                },
          <?php                
            }
          ?>
                        
          ]
        }
      );
</script>

@endsection