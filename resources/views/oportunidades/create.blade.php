<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Crear oportunidad')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
        margin: 25px;
    }

    .note-popover {
        display: none;
    }

    .custom-radio {
        font-weight: bold !important;
        font-size: small !important;
    }

    input#subtotalpro {
        height: 60px;
        font-size: 40px;
        text-align: center;
        font-weight: 700;
    }
</style>

    <div class="card animated flipInX" style="margin-bottom: 30px;">
        <div class="card-block">
            <form id="formulario" method="POST" action="{{ url('registraroportunidad2') }}" >
                <div class="panel-title">
                    <div class="pull-right">
                        <div class="btn-group">
                            <a href="{{ url('crearempresa') }}" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i> Añadir Empresa
                            </a>
                            <a href="{{ url('crearcliente') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i> Añadir Cliente
                            </a>
                        </div>
                    </div>
                    <h2>Crear nueva oportunidad</h2>
                    <p>Agrega una nueva oportunidad a tus registros</p>
                </div>
                <hr>
                <!-- Create Post Form -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- Fin Create Post Form -->
                {{ csrf_field() }}

                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label for="productos" class="col-form-label">Producto/s</label>
                        </div>
                        <div class="col-2">
                            <label for="productos" class="col-form-label">Referencia</label>
                        </div>
                        <div class="col-1">
                            <label for="productos" class="col-form-label">Cantidad</label>
                        </div>
                        <div class="col-2">
                            <label for="productos" class="col-form-label">Valor unitario</label>
                        </div>
                        <div class="col-2">
                            <label for="productos" class="col-form-label">Valor total</label>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="listProductos">
                    <div class="row">
                        <div class="col-5">
                            <select id="selectProducto0" name="productos[0][producto]" class="form-control productos0" onchange="buscarDimSelec2(0)" required></select>
                        </div>
                        <div class="col-2">
                            <select id="referencia0" name="productos[0][referencia]" class="form-control referencia0"></select>
                        </div>
                        <div class="col-1">
                            <input type="text" id="textCantidad0" class="col-12 form-control" name="productos[0][cantidad]" onkeyup="calcularValorTotal(0)" placeholder="0">
                        </div>
                        <div class="col-2">
                            <input type="text" id="textValor0" class="col-12 form-control dinero" name="productos[0][valor]" onkeyup="calcularValorTotal(0)" placeholder="$0">
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <input type="text" id="numberValor0" class="col-12 form-control sumarTotales" name="productos[0][total]" placeholder="$0" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarProductos()"><i class="fa fa-plus text-success"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                        </div>
                        <div class="col-2">
                            <h2>Total</h2>
                        </div>
                        <div class="col-4">
                            <input type="text" id="subtotalpro" class="col-12 form-control" placeholder="$0" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="observaciones" class="col-form-label">Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="2" placeholder="Por favor ingrese una  observacion o Detalle"></textarea>
                </div>

                <div class="form-group row">
                    <div class="col-3">
                        <label for="empresa" class="col-form-label">Empresa</label>
                        <select name="empresa" class="form-control" id="empresa" required></select>
                    </div>
                    <div class="col-2">
                        <label for="sede" class="col-form-label">Sede</label>
                        <select name="sede" class="form-control" id="sede" required></select>
                    </div>
                    <div class="col-3">
                        <label for="cliente" class="col-form-label">Cliente</label>
                        <select name="cliente" class="form-control" id="cliente" required></select>
                    </div>
                    <input type="hidden" name="ciclo_venta" value="10">
                    <div class="col-2">
                        <label for="probabilidad" class="col-form-label">Probabilidad</label>
                        <select name="probabilidad" class="form-control form-control-sm" id="probabilidad" required>
                            <option value="">Seleccione una opción</option>
                            <option value="Alta" data-image="https://placeholdit.imgix.net/~text?txtsize=33&bg=3c763d&w=350&h=150">Alta</option>
                            <option value="Media" data-image="https://placeholdit.imgix.net/~text?txtsize=33&bg=f0ad4e&w=350&h=150">Media</option>
                            <option value="Baja" data-image="https://placeholdit.imgix.net/~text?txtsize=33&bg=a94442&w=350&h=150">Baja</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <label for="margen" class="col-form-label">Margen de contribucion</label>
                        <div class="input-group">
                            <input class="form-control form-control-sm" type="number" name="margen" min="0" max="100" placeholder="0" id="margen" required>
                            <span class="input-group-addon form-control-sm">%</span>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-3">
                        <label class="col-form-label">Valor Oportunidad</label>
                        <input class="form-control form-control-sm" type="text" name="valor_oportunidad" id="valor_oportunidad" placeholder="Por favor ingrese un valor de oportunidad" required readonly>
                    </div>
                    <div class="col-2">
                        <label for="moneda" class="col-form-label">Moneda</label>
                        <select class="form-control" name="moneda" id="selec_moneda" required>
                            <option value="EUR">Euro (€)</option>
                            <option value="COP">Colombia Peso (COP)</option>
                            <option selected="selected" value="USD">US Dollar (USD)</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <label class="col-form-label">Tasa de cambio</label>
                        <input class="form-control form-control-sm dinero" type="text" id="tasa_cambio" name="tasa_cambio" placeholder="Por favor ingrese un valor de tasa de cambio" required>
                    </div>
                    <div class="col-4">
                        <label for="val_pesos" class="col-form-label">Valor en pesos</label>
                        <div class="input-group">
                            <input class="form-control form-control-sm" type="text" name="val_pesos" id="val_pesos" placeholder="0" required readonly>
                            <span class="input-group-addon form-control-sm">$</span>
                        </div>
                    </div>
                </div>

                <!-- datos de identificacion -->
                <div class="card" style="margin-bottom: 30px;">
                    <h5 class="card-header"> DATOS DE IDENTIFICACIÓN </h5>
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="identifico" class="col-form-label">Identifico</label>
                                <select name="identifico" class="form-control involucrado" onchange="identifica(this.value)" required>
                                    <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="responsable" class="col-form-label">Responsable</label>
                                <select name="responsable" class="form-control involucrado2" required>
                                    <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="fecha_ff" class="col-form-label">Fecha Identificacion</label>
                                <input class="form-control form-control-sm" type="text" name="fecha_ff" id="fecha_ff" placeholder="Por favor ingrese la fecha" required>
                            </div>
                            <div class="col-3">
                                <label for="fecha_oc" class="col-form-label">Fecha Cierre</label>
                                <input class="form-control form-control-sm" type="text" name="fecha_oc" id="fecha_oc" placeholder="Por favor ingrese la fecha" required>
                            </div>
                        </div>
                        <div class="form-group row select-otro" style="display: none;">
                            <div class="col-md-3">
                                <label for="identifico_otro" class="col-form-label">Identifico Otro</label>
                                <select name="identifico_otro" class="form-control involucrado_otro">

                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="button" style="margin-top: 25px !important;" class="btn btn-success btn-sm nuevo_user"><i class="fa fa-plus"></i> Nuevo</button>
                            </div>
                        </div>

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            <div class="form-group row formulario-user-otro" style="display: none; border-style: dashed; border-width: 4px;">
                                <center><h3>Nuevo Usuario</h3></center>
                                <div class="col-md-6">
                                    <label for="nombre_otro" class="col-form-label">Nombre</label>
                                    <input class="form-control otro-users" type="text" name="otro[nombre]" id="nombre_otro" placeholder="Nombre">
                                </div>
                                <div class="col-md-6">
                                    <label for="correo_otro" class="col-form-label">Correo</label>
                                    <input class="form-control otro-users" type="email" name="otro[correo]" id="email_otro" placeholder="Email">
                                </div>
                                <div class="col-md-6">
                                    <label for="apellido_otro" class="col-form-label">Apellido</label>
                                    <input class="form-control otro-users" type="text" name="otro[apellido]" id="apellido_otro" placeholder="Apellido">
                                </div>
                                <div class="col-md-6">
                                    <label for="telefono_otro" class="col-form-label">Telefono</label>
                                    <input class="form-control otro-users" type="text" name="otro[telefono]" id="telefono_otro" placeholder="Telefono">
                                </div>
                                <div class="col-md-6">
                                    <label for="empresa_otro" class="col-form-label">Empresa</label>
                                    <input class="form-control otro-users" type="text" name="otro[empresa]" id="empresa_otro" placeholder="Empresa">
                                </div>
                                <div class="col-md-6">
                                    <label for="pais_otro" class="col-form-label">Pais</label>
                                    <select name="otro[pais]" id="pais_otro" class="form-control pais">

                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="cargo_otro" class="col-form-label">Cargo</label>
                                    <input class="form-control otro-users" type="text" name="otro[cargo]" id="cargo_otro" placeholder="Cargo">
                                </div>
                                <div class="col-md-12" style="margin: 2%">
                                    <center><a class="btn btn-success guardar_otro" style="color: #fff;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                                    <a class="btn btn-warning cerrar_otro" style="color: #fff;"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</a></center>
                                </div>
                            </div>

                    </div>
                </div>
                <!-- fin datos de identificacion -->

                <!-- forma de pago -->
                <div class="card" style="margin-bottom: 30px;">
                    <h5 class="card-header"> FORMA DE PAGO </h5>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-3 form-group">
                                <label class="custom-control custom-radio">
                                    <input id="radio1" name="radio" type="radio" class="custom-control-input fpago" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Pago a cuotas</span>
                                </label>
                                <label class="custom-control custom-radio">
                                  <input id="radio2" name="radio" type="radio" class="custom-control-input fpago">
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description">Otra forma de pago</span>
                                </label>
                            </div>
                            <div class="col-2 form-group row ocultar_cuotas mostar_anticipo">
                                <label for="forma_pago" class="col-form-label">Anticipo de</label>
                                <div class="input-group">
                                    <input class="form-control form-control-sm" type="number" id="input_anticipo" name="forma_pago[0][anticipo]" min="0" max="100" placeholder="0">
                                    <span class="input-group-addon form-control-sm">%</span>
                                </div>
                            </div>
                            <div class="col-2 ocultar_cuotas">
                                <label for="forma_pago" class="col-form-label">Cantidad de cuotas</label>
                                <input class="form-control form-control-sm" type="number" id="num_cuotas" min="0" placeholder="0" onkeyup="comprobarCuotas()">
                            </div>
                            <div class="col-3 ocultar_cuotas">
                                <button type="button" class="btn btn-success btn-sm" style="margin-top: 25px !important;" onclick="añadircuotas()"><i class="fa fa-plus"></i> Añadir cuotas</button>
                            </div>
                        </div>
                        <div class="row" id="cuotas">
                        </div>
                        <div class="form-group" id="divcual" style="display: none;">
                            <label for="otra_forma_pago" class="col-form-label">Otro cual?</label>
                            <textarea class="form-control" name="otra_forma_pago" id="otra_forma_pago" rows="2" disabled placeholder="Por favor ingrese una  forma de pago"></textarea>
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Guardar</button>
            </form>
        </div>
    </div>
@endsection


@section('scripts')

<!--<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>-->
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="{{ url('/') }}/js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
    i=0;

    comprobarCuotas = function () {
        var valor = $("#num_cuotas").val();
        valor = parseInt(valor);
        if (valor>99) {
            swal('Error!', 'No es posible ingresar una cantidad de cuotas tan grande.','warning');
            $("#num_cuotas").val("");
        }
    }

    $('body').on('click','.guardar_otro',function(){
        if(($('#nombre_otro').val())!='' && ($('#apellido_otro').val())!='' && ($('#empresa_otro').val())!=''){
            swal({
              title: 'Esta seguro?',
              text: "Realmente desea guardar este usuario",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'No',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                var datos = new FormData($("#formulario")[0]);
                $.ajax({
                    url: "{{ url('/') }}/guardaruserotro",
                    data: datos,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(e){
                     $('.formulario-user-otro').css('display','none');
                     $('.otro-users').val('');
                     swal(
                          'Realizado!',
                          e.msj,
                          'success'
                        )
                    }
                });

            });
        }else{
            swal(
              'Alerta!',
              'Los campos de Nombre, Apellido y Empresa son obligatorios debe completarlos',
              'warning'
            );
        }
    });

    $('body').on('change','#selec_moneda', function() {
        if ($(this).val() === "COP") {
            $('#tasa_cambio').val(1);
            $("#val_pesos").val($("#valor_oportunidad").val());
            $('#tasa_cambio').prop('readonly', true);
        }else{
            $('#tasa_cambio').prop('readonly', false);
        }
    });

    $('body').on('change','#tasa_cambio', function() {
        cambio  = $(this).val();
        valor   = $("#valor_oportunidad").val();
        cambio  = cambio.replace(/,/g, '');
        valor   = valor.replace(/,/g, '');
        total   = valor*cambio;
        if(!isNaN(total)){
            total = total.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            total = total.split('').reverse().join('').replace(/^[\,]/,'');
            $("#val_pesos").val(total);
        }
    });

    $('body').on('click','.cerrar_otro',function(){
        $('.formulario-user-otro').css('display','none');
    });

    $('#telefono_otro').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    /*Pais*/
    $('.pais').select2({
            placeholder: "Seleccione un pais",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("pais") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    /*fin pais*/

    /*Nuevo Usuario*/
    $('body').on('click','.nuevo_user',function(){
        $('.formulario-user-otro').css('display','block');
    });
    /*Fin Nuevo Usuario */
    function identifica(valor){
        if(valor==0){
            $('.select-otro').css('display','block');
        }else{
            $('.select-otro').css('display','none');
        }
    }

    calcularValorTotal = function (val) {
        var cantidad=verificar("textCantidad"+val);
        var valor=verificar("textValor"+val);
        texValTotal = parseInt(valor*cantidad);

        if(!isNaN(texValTotal)){
            texValTotal = texValTotal.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            texValTotal = texValTotal.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("numberValor"+val).value = texValTotal;
        }

        var valSumaGeneral = 0;
        $(".sumarTotales").each(function() {
            var valSuma = verificar(this.id);
            valSumaGeneral = parseInt(valSumaGeneral) + parseInt(valSuma);
        });

        TotalhHombre = parseInt(valSumaGeneral);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        document.getElementById("subtotalpro").value = TotalhHombre;
        document.getElementById("valor_oportunidad").value = TotalhHombre;


    }

    añadircuotas = function () {
        $(".ocultar_cuotas").hide('slow/400/fast');
        $(".mostar_anticipo").show('slow');
        $("#cuotas").html(" ");
        var num = $("#num_cuotas").val();
        for (var j = 1; j <= num; j++) {
            cuotas=`
                <div class="col-12">
                    <div class="form-group row">
                        <label for="forma_pago " class="col-2 col-form-label">Cuota numero `+j+`</label>
                        <div class="input-group col-2 form-control-sm cuotas">
                            <input class="form-control form-control-sm" type="number" name="forma_pago[`+j+`][cuota_valor]" min="0" max="100" placeholder="0">
                            <span class="input-group-addon form-control-sm">%</span>
                        </div>
                        <div class="col-8 form-control-sm cuotas-text">
                            <textarea class="form-control form-control-sm" name="forma_pago[`+j+`][cuota_texto]" rows="1" placeholder="Por favor ingrese un detalle de cuota"></textarea>
                        </div>
                        <input type="hidden" name="forma_pago[`+j+`][cuota_numero]" value="`+j+`">
                    </div>
                </div>`;
            $("#cuotas").append(cuotas);
        }
    }

    $(function () {
        $("#valor_oportunidad").maskMoney();
        $(".dinero").maskMoney();

        $("#margen").change(function () {
            margen = $(this).val();
            if (margen > 100) {
                swal({
                  title: 'Error!',
                  text: 'No es posible ingresar un margen superior al 100%.',
                  timer: 2000
                }).then(function () {
                    $(this).val("");
                });
            }
        });
        $("#fecha_ff").change(function () {
            fecha = $(this).val();
            fecha2 = "{{ Date('Y-m-d') }}";
            if (fecha > fecha2) {
                swal({
                  title: 'Error!',
                  text: 'No es posible identificar una oportunidad en un día superior al de hoy.',
                  timer: 2000
                }).then(function () {
                    $("#fecha_ff").val("");
                });
            }
        });
        $("#fecha_oc").change(function () {
            fecha = $(this).val();
            fecha2 = $("#fecha_ff").val();
            if (fecha <= fecha2) {
                swal({
                  title: 'Error!',
                  text: 'No es posible cerrar una oportunidad en un día inferior o igual al que se identifico la oportunidad.',
                  timer: 2000
                }).then(function () {
                    $("#fecha_oc").val("");
                });
            }
        });

         $('#fecha_ff').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            nowButton: true,
            lang: 'es',
            maxDate : new Date(),
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar'
        }).on('close', function(e){
            $('#fecha_oc').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true,
                nowButton: true,
                lang: 'es',
                minDate : $("#fecha_ff").val(),
                cancelText : 'Cancelar',
                okText: 'Aceptar',
                nowText: 'Nuevo',
                clearText: 'Limpiar'
            })
        });



        $(".denero").maskMoney();
        $('#empresa').select2({
            placeholder: "Seleccione una empresa",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("empresa") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });


        $('#sede').select2({
            placeholder: "Seleccione una sede",
            sede: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sede") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#empresa').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#cliente').select2({
             placeholder: "Seleccione una cliente",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("cliente") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        dato:$("#sede").val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });


        $('.involucrado').select2({
            placeholder: "Seleccione una persona",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("vendedores") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('.involucrado2').select2({
            placeholder: "Seleccione una persona",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("vendedores2") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('.involucrado_otro').select2({
            placeholder: "Seleccione una persona",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("otros") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('.productos0').select2({
            // Activamos la opcion "Tags" del plugin

            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });

        $("#ciclo_venta").select2({
            templateResult: addUserPic,
            templateSelection: addUserPic
        });

        $("#probabilidad").select2({
            templateResult: addUserPic,
            templateSelection: addUserPic
        });

        function addUserPic (opt) {
            if (!opt.id) {
                return opt.text;
            }
            var optimage = $(opt.element).data('image');
            if(!optimage){
                return opt.text;
            } else {
                var $opt = $(
                '<span><img src="' + optimage + '" style="width: 30px;" /> ' + $(opt.element).text() + '</span>'
                );
                return $opt;
            }
        };

        $(".fpago").change(function(){ // bind a function to the change event
            if( $("#radio2").is(":checked") ){   // check if the radio is checked
                $("#divcual").show('slow'); // retrieve the value
                $("#otra_forma_pago").prop('disabled', false);
                $("#input_anticipo").prop('disabled', true);
                $(".ocultar_cuotas").hide('slow/400/fast');
                $("#cuotas").hide('slow/400/fast');
                $( ".cuotas" ).children("input").attr('disabled', true);
                $( ".cuotas-text" ).children("textarea").attr('disabled', true);
            } else {
                $("#divcual").hide('slow/400/fast');
                $("#input_anticipo").prop('disabled', false);
                $("#otra_forma_pago").prop('disabled', true);
                $(".ocultar_cuotas").show('slow');
                $("#cuotas").show('slow');
                $( ".cuotas" ).children("input").attr('disabled', false);
                $( ".cuotas-text" ).children("textarea").attr('disabled', false);
            }
        });
    });

    quitarProductos = function (id) {
      $(id).removeClass('rollIn').addClass('hinge');
      setTimeout(function(){
        $(id).remove();
        total();
        }, 1000);
    }

    total = function(){
        valSumaGeneral=0;
        $(".sumarTotales").each(function() {
            var valSuma = verificar(this.id);
            valSumaGeneral = parseInt(valSumaGeneral) + parseInt(valSuma);
        });
        valSumaGeneral = parseInt(valSumaGeneral);
        var valSumaGeneral = valSumaGeneral.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        valSumaGeneral = valSumaGeneral.split('').reverse().join('').replace(/^[\,]/,'');
        $('#subtotalpro').val(valSumaGeneral);
    }

    agregarProductos = function () {
        i++;
        dato = `
        <div class="row animated rollIn" id="listAddProductos`+i+`">
            <div class="col-5">
                <select id="selectProducto`+i+`" name="productos[`+i+`][producto]" class="form-control productos`+i+`" onchange="buscarDimSelec2(`+i+`)" required></select>
            </div>
            <div class="col-2">
                <select id="referencia`+i+`" name="productos[`+i+`][referencia]" class="form-control"></select>
            </div>
            <div class="col-1">
                <input type="text" id="textCantidad`+i+`" class="col-12 form-control" name="productos[`+i+`][cantidad]" onkeyup="calcularValorTotal(`+i+`)" placeholder="0">
            </div>
            <div class="col-2">
                <input type="text" id="textValor`+i+`" class="col-12 form-control dinero" name="productos[`+i+`][valor]" onkeyup="calcularValorTotal(`+i+`)" placeholder="$0">
            </div>
            <div class="col-2">
                <div class="input-group">
                    <input type="text" id="numberValor`+i+`" class="col-12 form-control sumarTotales" name="productos[`+i+`][total]" placeholder="$0" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarProductos('#listAddProductos`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                    </span>
                </div>
            </div>
        </div>`;
        $( "#listProductos" ).prepend(dato);
        $('.productos'+i).select2({
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });

        /*$('#referencia'+i).select2({
            placeholder: "Seleccione "+i,});*/
        $(".dinero").maskMoney();
    }


    buscarDimSelec2 = function (i) {
        console.log("entro aqui"+0);
        var ref = $.get( "{{ url('/') }}/referencia?term=&dato="+$("#selectProducto"+i).val());
        ref.done(function( data ) {
            console.log(data);
            $('#referencia'+i).html(" ");
            $.each(data, function (k, item) {
                $('#referencia'+i).append($('<option>', {
                    value: item.id,
                    text : item.text
                }));
            });
        });
    }


    verificar = function(id){
        var obj=document.getElementById(id);
        if(obj.value==""){
            value="0";
        }else{
            value=obj.value;
            value = value.replace(/,/g, "");
        }


        if(validate_importe(value,1))
        {
            // marcamos como erroneo
            obj.style.borderColor="#808080";
            return value;
        }else{
            // marcamos como erroneo
            obj.style.borderColor="#f00";
            return 0;
        }
    }

    validate_importe = function(value,decimal){
        if(decimal==undefined)
            decimal=0;

        if(decimal==1)
        {
            // Permite decimales tanto por . como por ,
            var patron=new RegExp("^[0-9]+((,|\.)[0-9]{1,2})?$");
        }else{
            // Numero entero normal
            var patron=new RegExp("^([0-9])*$")
        }

        if(value && value.search(patron)==0)
        {
            return true;
        }
        return false;
    }


</script>
@endsection
