@extends('template.app')

@section('title', 'Gestiones de oportunidades')

@section('content')
<link rel="stylesheet" href="/css/oportunidades/btnagregarcomentario.css">
<link rel="stylesheet" type="text/css" href="/components/3Dtimeline/css/style.css" />
<link rel="stylesheet" href="/css/oportunidades/gestiones.css">

<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">



<div class="widget" onscroll="scriprScroll">
    <!--menu-->
    <ul class="nav nav-pills" role="tablist"><input type="hidden" id="idop" value="{{ $id }}">
        <li><a href="/veroportunidadperdida/{{ $id }}"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
        <li><a href="/accionesoportunidadperdida/{{ $id }}"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
        <li><a href="/ver-oportunidadperdida-flujo/{{$id}}"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
        <li><a href="/verlecciones/{{ $id }}"><i class="fa fa-line-chart"></i> Lecciones aprendidas</a></li>
        <li class="active"><a href="/perdidas/gestionesoportunidad/{{ $id }}"><i class="fa fa-calendar"></i> Gestiones</a></li>
    </ul>
    <input type="hidden" id="observacioneshit">
    <div class="widget-header">
        <h4>Gestiones de oportunidad</h4>
    </div>
    <div class="widget-content">
        <div class="panel-flotando">
            <div class="panel1">
                <span>Historial cambios</span>
                <div class="color"></div>
            </div>
            <div class="panel2">
                <span>Pendientes</span>
                <div class="color"></div>
            </div>
            <div class="panel3">
                <span>Actividades</span>
                <div class="color"></div>
            </div>
            <div class="panel4">
                <span>Visita clientes</span>
                <div class="color"></div>
            </div>
            <div class="panel5">
                <span>Ciclo ventas</span>
                <div class="color"></div>
            </div>
        </div>
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="row calendarEdwin"></div>
            </div>
        </div>
        @if($data['permiso_crear_comentario'] == "Si")
        <div class="row flotante">
            <div class="col-md-12">
                <a class="snip1489" data-toggle="tooltip" data-placement="top" title="Agregar Comentario" onclick="crear_comentario()"><i class="fa fa-plus" aria-hidden="true"></i></a>
            </div>
        </div>
        @endif
    </div>
</div>


@endsection

@section('scripts')
<script src='/components/newfullcalendar/lib/moment.min.js'></script>
<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/assets/js/plugins/datatable/jquery.dataTables.min.js"></script>
<script src="/components/3Dtimeline/js/modernizr.custom.63321.js"></script>

<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script src="/assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
    var dias_eventos_guardar = '';
    function ordenar(a, b) {
        if (a.created_at > b.created_at) {
            return 1;
        }
        if (a.created_at < b.created_at) {
            return -1;
        }
        return 0;
    }
    $(function() {
        $("[data-toggle='tooltip']").tooltip();
        moment.locale('es');
        var id = $("#idop").val();
        $("body").on("click", ".menClick", function() {
            var classAdd = $(this).attr('name');
            var idAdd = $(this).attr('data-name');
            $(".menClick").removeClass("active");
            $(".menClick > .icon").removeClass("active");
            $(this).addClass("active");
            $("#line").removeClass("one");
            $("#line").removeClass("two");
            $("#line").removeClass("three");
            $("#line").removeClass("four");
            $("#line").removeClass("five");
            $("#line").addClass(classAdd);
            $(".menuContent").removeClass("active");
            $("#" + idAdd).addClass("active");
        })
        var jqxhr = $.ajax("/validargestionesop/" + id)
            .always(function(data) {
                if (data.success) {
                    $("#observacioneshit").val(data.observaciones);
                    data.modelo = data.modelo.sort(ordenar);
                    var ultimo = data.modelo.length;
                    var ultimoEvent = data.modelo[ultimo - 1]
                    funCrearCalendar(data.modelo, data.fecha, data.fechainit, data.fechafin, ultimoEvent);
                }
            });
        $("body").on('click', '.notificacion', function() {
            var fecha = $(this).attr('name');
            var html = funCrearHTML(id, fecha);
        });
    })
    window.addEventListener('scroll', function(e) {
        if ('pageXOffset' in window) {
            var scrollTop = window.pageYOffset;
        } else { // Internet Explorer before version 9
            var zoomFactor = GetZoomFactor();
            var scrollTop = Math.round(document.documentElement.scrollTop / zoomFactor);
        }
        if (scrollTop > 110) {
            $(".panel-flotando").addClass("panel-flotando-arriba").removeClass("panel-flotando");
        } else {
            $(".panel-flotando-arriba").addClass("panel-flotando").removeClass("panel-flotando-arriba");
        }
    });


    funCrearCalendar = function(datos, fecha, fechaini, fechafin, ultimoEvent) {
        var anoinit = moment(fechaini).format('YYYY');
        var anofin = "";
        var mesfin = "";

        if (moment(fechafin) > moment(ultimoEvent.created_at)) {
            anofin = parseInt(moment(ultimoEvent.created_at).format('YYYY-MM-DD'));
            mesfin = parseInt(moment(ultimoEvent.created_at).format('MM'));
            if (mesfin === 12) {
                mesfin = 1;
                anofin++;
            } else {
                mesfin++;
            }
        } else {
            anofin = moment(ultimoEvent.created_at).format('YYYY');
            mesfin = moment(ultimoEvent.created_at).format('M');
        }


        var mesVenta = moment(fecha.date).format('M');
        var anoVenta = moment(fecha.date).format('YYYY');
        var anoActual = moment().format('YYYY');
        var observaciones = $("#observacioneshit").val();
        var html = `
      <div class="col-md-4 mesesano" style="margin-top: 20px;">
        <div class="contencalborde">
          <h2 class="tith2" style="padding-top:15px;"><span>Identifico</span></h2>
          <h2 class="tith2">` + moment(fechaini).format('DD MMMM') + ` de ` + anoinit + `</h2>
          <div class="titconte">
            <span>` + observaciones + `</span>
          </div>
        </div>
      </div>
    `;
        $(".calendarEdwin").html(html);
        var conHTML = 0;
        for (var i = mesVenta; i <= 12; i++) {
            html = "";
            var diaSemana = moment(anoVenta + ' ' + i + ' 1').format('e');
            var ultimodia = moment(anoVenta + ' ' + i).daysInMonth();
            var mes = moment(anoVenta + ' ' + i).format('MMMM');
            html += `<div class="col-md-4 mesesano" id="` + anoVenta + `-` + i + `">
      <div class="contencalborde">
      <h2>` + mes + ` del ` + anoVenta + `</h2>
      <div class="titulo-semana"><span>D</span></div>
      <div class="titulo-semana"><span>L</span></div>
      <div class="titulo-semana"><span>M</span></div>
      <div class="titulo-semana"><span>M</span></div>
      <div class="titulo-semana"><span>J</span></div>
      <div class="titulo-semana"><span>V</span></div>
      <div class="titulo-semana"><span>S</span></div>`;
            if (diaSemana === '5') {
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
            }
            if (diaSemana === '4') {
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
            }
            if (diaSemana === '3') {
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
            }
            if (diaSemana === '2') {
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
            }
            if (diaSemana === '1') {
                html += ' <div class="valor-dia"><span></span></div> ';
                html += ' <div class="valor-dia"><span></span></div> ';
            }
            if (diaSemana === '0') {
                html += ' <div class="valor-dia"><span></span></div> ';
            }
            for (var j = 1; j <= ultimodia; j++) {
                var fecha_mostrar = "";
                var circuHtml = "";
                var fecha_comentario = anoVenta + '-' + i + '-' + j;
                var lado = seleccionar_dia(0,fecha_comentario);
                var bg_span = seleccionar_dia(1,fecha_comentario);
                $.each(datos, function(index, value) {
                    fecha_accion = moment(value.created_at).format('YYYY-M-D');
                    if (fecha_accion === anoVenta + '-' + i + '-' + j) {
                        fecha_mostrar = moment(value.created_at).format('YYYY-MM-DD');
                        if (value.tipo === "historial") {
                            circuHtml += `<span class="notificacion histop" name="` + fecha_mostrar + `"></span>`;
                        }
                        if (value.tipo === "actividades") {
                            circuHtml += `<span class="notificacion bitacora" name="` + fecha_mostrar + `"></span>`;
                        }
                        if (value.tipo === "pendientes") {
                            circuHtml += `<span class="notificacion pendientes" name="` + fecha_mostrar + `"></span>`;
                        }
                        if (value.tipo === "agendavisita") {
                            circuHtml += `<span class="notificacion visitclient" name="` + fecha_mostrar + `"></span>`;
                        }
                        if (value.tipo === "ciclo_venta") {
                            circuHtml += `<span class="notificacion ciclosvent" name="` + fecha_mostrar + `"></span>`;
                        }
                    }
                });
                onclic=lado!="" ? 'onclick="ver_comentario(\''+moment(fecha_comentario).format('YYYY-MM-DD')+'\')"' : "";
                if (circuHtml != "") {
                    html += ` <div class="valor-dia notificacion `+lado+`" id="` + fecha_mostrar + `" name="` + fecha_mostrar + `" ` + onclic + `>`;
                } else {
                    html += ` <div class="valor-dia `+lado+`" ` + onclic + `>`;
                }
                html += circuHtml;
                titulo = bg_span!="" ? 'data-toggle="tooltip" data-placement="top" title="Comentario Administrador CRM"' : "";
                html += `<span class="`+bg_span+`" ` + titulo + `>` + j + `</span>
                   </div> `;
            }
            html += '</div></div>';
            $(".calendarEdwin").append(html);
            $("[data-toggle='tooltip']").tooltip();
            mesfin = parseInt(mesfin);
            anofin = parseInt(anofin);
            if (i === mesfin && anoVenta === anofin) {
                break;
            }
            anoVenta = parseInt(anoVenta);
            i = parseInt(i);
            if (i === 12 && anofin > anoVenta) {
                i = 0;
                anoVenta++;
            }
        }
        var html = `
      <div class="col-md-4 mesesano" style="margin-top: 20px;">
        <div class="contencalborde">
          <h2 class="tith2" style="padding-top:15px;"><span>Cierre</span></h2>
          <h2 class="tith2">` + moment(fechafin).format('DD MMMM') + ` de ` + anofin + `</h2>
          <div class="titconte">
            <span>` + observaciones + `</span>
          </div>
        </div>
      </div>
    `;
        $(".calendarEdwin").append(html);
    }
    funCrearHTML = function(id, fecha) {
        var html = "";
        var agendacliente = "";
        var actbitacora = "";
        var menuHTML = "";
        var pendientes = "";
        var historial = "";
        var obserhisto = "";
        var ciclosventa = "";

        var jqxhr = $.ajax("/traerdatosop/" + id + "/" + fecha)
            .always(function(data) {
                if (data.success) {
                    if (data.historialop.length > 0) {
                        menuHTML += `<div class="menClick" name="one" data-name="HistorialCambiosCont">
                          <span><i class="fa fa-list"></i> Historial cambios</span>
                        </div>`;
                        $.each(data.historialop, function(index, value) {
                            if (value.key !== 'productos' && value.key !== 'radio' && value.key !== 'observaciones') {
                                historial += `
                  <div class="col-md-6 his-title">` + value.key + `</div>
                  <div class="col-md-3 his-valor">` + value.antvalue + `</div>
                  <div class="col-md-3 his-valor">` + value.value + `</div>
                `;
                            } else if (value.key === 'observaciones') {
                                obserhisto += `
                  <div class="col-md-12">
                    <div class="row title-hiscambios">
                      <div class="col-md-12">OBSERVACION</div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 his-valor">ANTERIOR</div>
                      <div class="col-md-6 his-valor">NUEVO</div>
                      <div class="col-md-6 his-valor">` + value.antvalue + `</div>
                      <div class="col-md-6 his-valor">` + value.value + `</div>
                    </div>
                  </div>
                `;
                            }
                        });
                    } else {
                        menuHTML += `<div class="href-disable">
                          <span><i class="fa fa-list"></i> Historial cambios</span>
                        </div>`;
                    }

                    if (data.pendientes.length > 0 || data.hist_pendientes.length > 0) {
                        menuHTML += `<div class="menClick" name="two" data-name="PendientesCont">
                            <span><i class="fa fa-check-square-o"></i> Pendientes</span>
                          </div>`;
                        $.each(data.pendientes, function(index, value) {
                            pendientes += `
                  <div class="col-md-12">
                    <div class="ticket">
                      <div class="stub">
                        <div class="top">
                          <span class="admit"><i>` + value.aplazadas + `</i> veces aplazada</span>
                          <span class="line"></span>
                          <span class="num">` + moment(value.fecha_ejecucion).format('DD MMM YYYY') + `</span>
                        </div>
                        <div class="number">` + value.estado + `</div>
                        <div class="tipo">
                          <p>` + value.tipo + `</p>
                        </div>
                      </div>
                      <div class="check">
                        <div class="responsable">
                          <div class="thumbUser" data-name="56" style="background-image: url(/images/file/clientes/` + value.fotoUser + `);background-repeat: no-repeat;background-position: center;background-size: 90px;"></div>
                          <p>` + value.nombreuser + `</p>
                        </div>
                        <div class="detalle">
                          <p>` + value.detalle + `</p>
                        </div>
                      </div>
                    </div>
                  </div>
                `;
                        });
                        $.each(data.hist_pendientes, function(index, value) {
                            pendientes += `
                  <div class="col-md-12">
                    <div class="ticket">
                      <div class="stub">
                        <div class="top">
                          <span class="admit"><i>` + value.aplazadas + `</i> veces aplazada</span>
                          <span class="line"></span>
                          <span class="num">` + moment(value.fecha_ejecucion).format('DD MMM YYYY') + `</span>
                        </div>
                        <div class="number">` + value.estado + `</div>
                        <div class="tipo">
                          <p>` + value.tipo + `</p>
                        </div>
                      </div>
                      <div class="check">
                        <div class="responsable">
                          <div class="thumbUser" data-name="56" style="background-image: url(/images/file/clientes/` + value.fotoUser + `);background-repeat: no-repeat;background-position: center;background-size: 90px;"></div>
                          <p>` + value.nombreuser + `</p>
                        </div>
                        <div class="detalle">
                          <p>` + value.detalle + `</p>
                        </div>
                        <div class="detalle">
                          <blockquote>` + value.comentario + `</blockquote>
                        </div>
                      </div>
                    </div>
                  </div>
                `;
                        });
                    } else {
                        menuHTML += `<div class="href-disable">
                          <span><i class="fa fa-check-square-o"></i> Pendientes</span>
                        </div>`;
                    }

                    if (data.actividades.length > 0) {
                        menuHTML += `<div class="menClick" name="three" data-name="ActividadesCont">
                          <span><i class="fa fa-calendar-check-o"></i> Actividades</span>
                        </div>`;
                        $.each(data.actividades, function(index, value) {
                            actbitacora += `
                <div class="col-md-12">
                  <h3>` + value.tipo_actividad + `</h3>
                  <div class="thumbUser" data-name="` + value.id + `" style="background-image: url(/images/file/clientes/` + value.fotoUser + `);background-repeat: no-repeat;background-position: center;background-size: 90px;"></div>
                  <div class="div-oculto" id="` + value.id + `">
                      <p class="subtitulo2">` + value.tiempo_inicio + ` - ` + value.tiempo_fin + ` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estado Actividad: <span class="badge indigo">` + value.estado_actividad + `</span></p>
                      <h5 class="card-header"> Detalle Actividad </h5>
                      <p class="subtitulo_coment">` + value.detalle_actividad + `</p>
                      <h5 class="card-header"> Resultado Actividad </h5>
                      <p class="subtitulo_coment">` + value.detalle_resultado + `</p>
                  </div>
                </div>`;
                        });
                    } else {
                        menuHTML += `<div class="href-disable">
                          <span><i class="fa fa-calendar-check-o"></i> Actividades</span>
                        </div>`;
                    }

                    if (data.agendacliente.length > 0) {
                        menuHTML += `<div class="menClick" name="four" data-name="VisitaClienteCont">
                          <span><i class="fa fa-handshake-o"></i> Visitas cliente</span>
                        </div>`;
                        agendacliente += `<div class="row">`;
                        $.each(data.agendacliente, function(index, value) {
                            agendacliente += `
                    <div class="col-md-12">
                        <div class="thumbUser" data-name="` + value.id + `" style="background-image: url(/images/file/clientes/` + value.fotoUser + `);background-repeat: no-repeat;background-position: center;background-size: 90px;"></div>
                        <h3>Visita</h3>
                        <div class="div-oculto" id="A` + value.id + `">
                            <p class="subtitulo">
                                <span class="badge indigo">` + value.countActividades + ` actividades</span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;` + moment(value.fecha_inicio).format('dddd DD MMM YYYY') + ` - ` + moment(value.fecha_final).format('dddd DD MMM YYYY') + `
                            </p>
                            <h5 class="card-header"> Acerca de la visita </h5>
                            <p class="subtitulo_coment">` + value.observaciones + `</p>`;
                            $.each(value.visitantes, function(key, model) {
                                if (!model.trClient) {
                                    model.trClient = 'Sr/a';
                                }
                                agendacliente += `<div class="thumbClient" style="background-image: url(/images/file/clientes/` + model.fotoClient + `);background-repeat: no-repeat;background-position: center;background-size: 90px;"><span>` + model.trClient + ' ' + model.nameClient + ` ` + model.cargoClient + `</span></div>`;
                            });
                            agendacliente += `
                        </div>
                    </div>`;
                        });
                        agendacliente += `</div>`;
                    } else {
                        menuHTML += `<div class="href-disable">
                          <span><i class="fa fa-handshake-o"></i> Visitas cliente</span>
                        </div>`;
                    }

                    if (data.ciclosventa.length > 0) {
                        menuHTML += `<div class="menClick" name="five" data-name="CicloVentaCont">
                            <span><i class="fa fa-sort-numeric-desc"></i> Ciclos venta</span>
                          </div>`;
                        $.each(data.ciclosventa, function(index, value) {
                            ciclosventa += `
                <div class="marco-flotando">
                  <div class="row">
                      <div class="izquierdo col-md-4">
                          <p style="border-bottom: solid 1px;">` + value.nombre + `</p>
                          <h2>` + value.value + `%</h2>
                      </div>
                      <div class="centro col-md-8">
                        <h2>Detalles del ciclo</h2>
                        <div>
                          ` + value.descripcion + `
                        </div>
                        <div class="thumbUserCiclos" data-name="` + value.id + `" style="background-image: url(/images/file/clientes/` + value.foto + `);"></div>
                      </div>

                  </div>
                </div>
              `;
                        });
                    } else {
                        menuHTML += `<div class="href-disable">
                          <span><i class="fa fa-sort-numeric-desc"></i> Ciclos venta</span>
                        </div>`;
                    }
                    html = `
            <div class="row">
              <div class="col-md-2">` + menuHTML + `</div>
              <div id="border" class="col-md-1">
                <div id="line" class="four"></div>
              </div>
              <div class="col-md-9">
                <div id="HistorialCambiosCont" class="menuContent">
                    <div class="col-md-12">
                      <div class="row title-hiscambios">
                        <div class="col-md-6">CAMBIO</div>
                        <div class="col-md-3">ANTERIOR</div>
                        <div class="col-md-3">NUEVO</div>
                      </div>
                      <div class="row">` + historial + `</div>
                    </div>
                    ` + obserhisto + `
                </div>

                <div id="PendientesCont" class="menuContent">
                  <div class="row">` + pendientes + `</div>
                </div>

                <div id="ActividadesCont" class="menuContent">
                  <div class="row">` + actbitacora + `</div>
                </div>

                <div id="VisitaClienteCont" class="menuContent active">` + agendacliente + `</div>

                <div id="CicloVentaCont" class="menuContent">` + ciclosventa + `</div>
            </div>
          `;
                    swal({
                        title: "Trazabilidad de gestión",
                        animation: "slide-from-top",
                        confirmButtonText: 'Aceptar',
                        html: html,
                        onOpen: function() {
                            $("[data-toggle='tooltip']").tooltip();
                            if ($('.datatable-pendientes').length > 0) {
                                var dtTableVigentes = $('.datatable-pendientes').DataTable({ // use DataTable, not dataTable
                                    language: {
                                        processing: "Tratamiento en curso ...",
                                        search: "Buscar&nbsp;:",
                                        loadingRecords: "Cargando...",
                                        emptyTable: "No hay datos disponibles en la tabla",
                                    },
                                    bPaginate: false,
                                });
                                dtTableVigentes.on('order.dt search.dt', function() {
                                    dtTableVigentes.column(0, {
                                        search: 'applied',
                                        order: 'applied'
                                    }).nodes().each(function(cell, i) {
                                        cell.innerHTML = i + 1;
                                    });
                                }).draw();
                            }
                        }
                    });
                }
            });
        //return html;

    }

    function seleccionar_dia(accion,fecha){

            var dia_seleccionado = 'No';
            fecha = moment(fecha).format("YYYY-MM-DD");
            switch(fecha) {
                <?php
                foreach($data['comentarios'] as $comentario){
                    $fechas = explode(' ',$comentario->value);
                    foreach($fechas as $fecha){ ?>
                case '<?=$fecha?>':
                    dia_seleccionado = 'Si';
                    break;
                    <?php
                    }
                }
                ?>
            }
            var clase = '';
            switch(accion) {
                case 0:
                    if(dia_seleccionado=='Si'){
                       clase = 'b-comentario';
                    }
                    break;
                case 1:
                    if(dia_seleccionado=='Si'){
                       clase = 'bg-comentario';
                    }
                    break;
            }
            return clase;

    }

    /**
     * Funcion para ver el comentario de ADMINISTRADOR CRM
     * @param {date} fecha LA FECHA EN LA QUE PICA UN USUARIO EN LA BANDERA
     */
    function ver_comentario(fecha){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/gestionesoportunidad-action",
            data: {
                _token: CSRF_TOKEN,
                action: 2,
                fecha: fecha,
                oportunidad_id:<?=$id?>
            },
            cache: false,
            type: 'POST',
            success: function(e){
                 swal({
                    title: "Ver Comentario",
                    confirmButtonText: 'ok',
                    html: e.data['contenido']
                });
                $('.swal2-modal').css('width','90%');
                $('button.swal2-confirm.swal2-styled').css('position','unset').css('left','0px');
            }
        });
    }
    /**
     * Funcion para crear formulario de comentario del administrador
     */
    function crear_comentario(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/gestionesoportunidad-action",
            data: {
                _token: CSRF_TOKEN,
                action: 0
            },
            cache: false,
            type: 'POST',
            success: function(e){
                formulario_comentario(e.data['contenido']);
            }
        });
    }


    var fecha1 = '';
    var fecha2 = '';
    var comentario_admin = '';
    /**
     * Funcion para crear el Sweet Alert con el html desde el controlador
     * @param string html HTML del sweet Alert
     */
    function formulario_comentario(html){
        swal({
            title: "Crear Comentario",
            confirmButtonText: 'Guardar',
            html: html,
            dir: "{{url('/')}}"
        }).then(function(){
            fecha1 = $('#fecha_ini').val();
            fecha2 = $('#fecha_fin').val();
            tinymce.remove();
            comentario_admin = $('#comentario_administrador').val();
            save_comentario(fecha1,fecha2,comentario_admin);
        });
        activar_calendario();
        activar_editor_texto();
        $('.swal2-modal').css('width','90%');
        $('button.swal2-confirm.swal2-styled').css('position','unset').css('left','0px');
    }

    function save_comentario(inicio,fin,comentario){
        if(inicio != '' && fin != '' && comentario != ''){
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "{{url('/')}}/gestionesoportunidad-action",
                data: {
                    _token: CSRF_TOKEN,
                    action: 1,
                    inicio: inicio,
                    fin: fin,
                    value: dias_eventos_guardar,
                    comentario: comentario,
                    oportunidad_id: <?=$id?>
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['guardo'] = "Si"){
                       swal(
                          'Guardado',
                          e.data['msj'],
                          'success'
                        ).then(function(){
                           location.reload();
                       });
                        $('button.swal2-confirm.swal2-styled').css('position','unset').css('left','0px');
                    }else{
                        swal(
                          'Error',
                          e.data['msj'],
                          'error'
                        );
                        $('button.swal2-confirm.swal2-styled').css('position','unset').css('left','0px');
                    }
                }
            });
        }else{
            swal(
              'Advertencia',
              'Hay campos vacios por favor verificar la información',
              'warning'
            ).then(function () {
                crear_comentario();
                setTimeout(function(){
                    $('#fecha_ini').val(inicio);
                    $('#fecha_fin').val(fin);
                    tinymce.remove();
                    $('#comentario_administrador').val(comentario);
                    activar_editor_texto();
                }, 1500);
            });
        }
    }

    /*Aqui se activa el calendario*/
    function activar_calendario(){
        $('#fecha_ini').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            nowButton: true,
            lang: 'es',
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar'
        }).on('close', function(e){
            $('#fecha_fin').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true,
                nowButton: true,
                lang: 'es',
                minDate : $("#fecha_ini").val(),
                cancelText : 'Cancelar',
                okText: 'Aceptar',
                nowText: 'Nuevo',
                clearText: 'Limpiar'
            })
        });
    }

    /**
     * Funcion para activar tinymce
     */
    function activar_editor_texto(){
        tinymce.remove();
        tinymce.init({
            selector:'textarea',
            height: 500,
            language : "es",
            theme: 'modern',
            plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern'
        });
    }

    /**
     * Evento para cada fecha inicio
     */
    $(document).on('change','#fecha_ini',function(){
        if($(this).val() != ''){
            ano_inicio = moment($(this).val()).format('YYYY');
            mes_inicio = parseInt(moment($(this).val()).format('MM'));
            if($('#'+ano_inicio+'-'+mes_inicio).length<=0){
               swal(
                  'Advertencia',
                  'El mes seleccionado no existe en esta oportunidad',
                  'warning'
                ).then(function () {
                    crear_comentario();
                });
           }
        }
    });
    /**
     * Evento para cada fecha fin
     */

    $(document).on('change','#fecha_fin',function(){
        numero_dias_evento = 0;
        dias_eventos = '';
        dias_eventos_guardar = '';
        if($(this).val() != ''){
            var fecha1 = moment($('#fecha_ini').val());
            var fecha2 = moment($(this).val());
            var dias = fecha2.diff(fecha1, 'days');
            for(i=0;i<=dias;i++){
                nueva = fecha1;
                nueva = moment(nueva).add(i, 'days');
                nueva = moment(nueva).format("YYYY-MM-DD");
                console.log(nueva);
                dias_eventos_guardar+=nueva+' ';
                if($('#'+nueva).length > 0){
                   numero_dias_evento++;
                   dias_eventos+=nueva+', ';
                }
            }
            if(numero_dias_evento>0){
               warning_event(dias_eventos,numero_dias_evento);
            }
        }
    });

    /**
     * Funcion para mostrar los eventos que ya existen en ese rango de fechas
     *  @param {STRING} dias_eventos LOS DIAS EN LOS QUE YA EXISTE UN EVENTO CON FORMATO (YYYY-MM-DD)
     * @param {INT} numero_dias_evento TOTAL DE DIAS EN LOS QUE HAY UN EVENTO
     */
    function warning_event(dias_eventos,numero_dias_evento){
        if(numero_dias_evento>1){
           msg1 = 'Los dias ';
           msg2 = 'ya tienen un evento y no se puede crear un comentario';
        }else{
            msg1 = 'El dia ';
            msg2 = 'ya tiene un evento y no se puede crear un comentario';
        }
        swal(
          'Advertencia',
          msg1 + dias_eventos + msg2,
          'warning'
        ).then(function () {
            crear_comentario();
        });
    }
</script>
@endsection
