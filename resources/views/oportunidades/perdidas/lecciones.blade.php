@extends('template.app')
@section('title', 'APU')

@section('content')
<style type="text/css">
	.soloprimer:first-letter,p:first-letter,span:first-letter{text-transform:uppercase!important}.capitalize{text-transform:capitalize!important}.text-muted{text-transform:lowercase}p.normalp{font-weight:400}.unalinea{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}.media-body{text-align:justify;font-size:larger}.media-body>a{color:#001862;font-weight:500}img.media-object.img-circle{width:50px}p{margin:0 0 10px;color:#7a5555;font-size:15px;font-family:Roboto;font-weight:400}
</style>

<?php header("Content-Type: text/plain"); 
$llenadas = 0;
$vacias   =0;
?>
<div class="main-content">
	<ul class="nav nav-pills" role="tablist">
        <li><a href="{{ url('veroportunidadperdida') }}/{{ $id }}"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
        <li><a href="{{ url('accionesoportunidadperdida') }}/{{ $id }}"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
        <li><a href="/ver-oportunidadperdida-flujo/{{$id}}"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
        <li class="active"><a href="#chart1" role="tab" data-toggle="tab"><i class="fa fa-line-chart"></i> Lecciones aprendidas</a></li>
        <li><a href="/perdidas/gestionesoportunidad/{{$id}}"><i class="fa fa-calendar"></i> Gestiones</a></li>
    </ul>
	<div class="widget">
		<div class="widget-content">			
			<h2>Lecciones aprendidas</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			@if(isset($dato->motivo_perdida) && !empty($dato->motivo_perdida))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Cual fue el motivo de la perdida de esta oportunidad?</a><br>
	                    <div ><?php $llenadas++; print( $dato->motivo_perdida ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->suministro_competencia) && !empty($dato->suministro_competencia))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿El suministro de equipos o el proyecto lo hará la competencia?</a><br>
	                    <div ><?php $llenadas++; print( $dato->suministro_competencia ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->empresa) && !empty($dato->empresa))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Que empresa realizará el proyecto?</a><br>
	                    <div ><?php $llenadas++; ?> @if(count($data['competencia'])>0) {{$data['competencia'][0]->nombre}} @else {{$dato->empresa}} @endif</div>
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->ventaja_competencia) && !empty($dato->ventaja_competencia))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Cual fue la ventaja competitiva o estrategia que permitió que la competencia ganara?</a><br>
	                    <div ><?php $llenadas++; print( $dato->ventaja_competencia ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->acciones_realizadas) && !empty($dato->acciones_realizadas))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Que acciones habría realizado para no perder la oportunidad?</a><br>
	                    <div ><?php $llenadas++; print( $dato->acciones_realizadas ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
		</div>
		<div class="col-md-6">
			@if(isset($dato->monto_oportuno) && !empty($dato->monto_oportuno))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Considera oportuno y necesario el monto y cantidad de recursos y tiempo invertido en esta oportunidad?</a><br>
	                    <div><?php $llenadas++; print( $dato->monto_oportuno ); ?>,  $ {{number_format($dato->gastos, 0, '.', ',')}} Gastos y {{$dato->horas}} Horas</div>
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->justificacion_monto_oportuno) && !empty($dato->justificacion_monto_oportuno))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Porqué?</a><br>
	                    <div ><?php $llenadas++; print( $dato->justificacion_monto_oportuno ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->percepcion_essi) && !empty($dato->percepcion_essi))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Cual es la percepción del cliente hacia ESSI?</a><br>
	                    <div ><?php $llenadas++; print( $dato->percepcion_essi ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif			
			@if(isset($dato->percepcion_maquina) && !empty($dato->percepcion_maquina))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Cual es la percepción del cliente hacia los equipos de ESSI?</a><br>
	                    <div ><?php $llenadas++; print( $dato->percepcion_maquina ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
			@if(isset($dato->futuras_oportunidades) && !empty($dato->futuras_oportunidades))
			<div class="widget widget-scrolling">
				<div class="widget-content">
                    <div class="media clearfix header-bottom">
	                  <div class="media-left">
	                    <img src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" class="media-object img-circle">
	                  </div>
	                  <div class="media-body">
	                    <a href="#">¿Cuales son las futuras oportunidades con este cliente?</a><br>
	                    <div ><?php $llenadas++; print( $dato->futuras_oportunidades ); ?></div>	                    
	                  </div>
	                </div>
				</div>
			</div>
			@else @php $vacias++ @endphp @endif
		</div>
	</div>
	<br>
	<br>
	<!--<div class="row">
		<div class="col-md-6">
			<div class="contextual-summary-info">
				<i class="fa fa-check-circle"></i>
				<p class="pull-right"><span>COMPLETADO</span> {{ $llenadas }} </p>
			</div>
		</div>
		<div class="col-md-6">
			<div class="contextual-summary-info">
				<i class="fa fa-warning yellow-font"></i>
				<p class="pull-right"><span>FALTANTES</span> {{ $vacias }} </p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="contextual-summary-info">
				<i class="fa fa-close red-font"></i>
				<p class="pull-right"><span>ERRORS</span>4</p>
			</div>
		</div>
	</div>
	<div class="bottom-30px"></div>-->
	<!--<div class="row">
		<div class="col-md-4">
			<div class="contextual-summary-info contextual-background green-bg">
				<i class="fa fa-check-circle"></i>
				<p class="pull-right"><span>SUCCESS</span> 35</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="contextual-summary-info contextual-background yellow-bg">
				<i class="fa fa-warning"></i>
				<p class="pull-right"><span>WARNINGS</span>12</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="contextual-summary-info contextual-background red-bg">
				<i class="fa fa-close"></i>
				<p class="pull-right"><span>ERRORS</span>4</div>
			</div>
		</div>
	</div>-->
</div>
@endsection
 
@section('scripts')
@endsection
