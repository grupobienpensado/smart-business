<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Ver Oportunidades')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/3Dtimeline/css/style.css" />
<style type="text/css">    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }   

    #form7 { 
        outline: none !important;
        border-color: #719ECE;
        box-shadow: 0 0 10px #719ECE;
        font-weight: bold;
        border: 0;
        background-color: gainsboro;
        border-bottom: #051d60 solid;
    }

    .subtitulo_coment{
        margin-bottom: 4px;
        font-size: small !important;
        color: #54575a;
    }

    p:first-letter {
    text-transform: uppercase !important;
  }

  span:first-letter {
    text-transform: uppercase !important;
  }

  .soloprimer:first-letter{
    text-transform: uppercase !important;
  }

  .uppercase {
    text-transform: uppercase !important;
  }

  .capitalize{
    text-transform: lowercase !important;
    text-transform: capitalize !important;
  }

  .text-inicial{
    font-variant:initial !important;
  }

  .text-muted{
    text-transform: lowercase;
  }
  p.normalp{
    font-weight: 400;
  }
  .unalinea{
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  .parrafo{
    color: #555;
    font-weight: 400;
    font-size: 16px;
  }
</style>

  

<div class="card animated flipInX" style="margin-bottom: 30px;">
    <div class="card-block"> 
        <ul class="nav nav-pills" role="tablist">
            <li class="active"><a href="#chart1" role="tab" data-toggle="tab"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
            <li><a href="{{ url('accionesoportunidadperdida') }}/{{ $id }}"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
            <li><a href="{{ url('verlecciones') }}/{{ $id }}"><i class="fa fa-line-chart"></i> Lecciones aprendidas</a></li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                <?php if($permiso_editar=="Si"){ ?>
                    <a href="{{ url('oportunidad/edit') }}/{{$oportunidad->id}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                <?php } ?>
                    <a href="#titulobservaciones" id="comentarbtn" class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Comentar</a>
                </div>
            </div>
        </div>
        <div class="profile-empresa">
            <img src="
               @if(!empty($empresa->logo))
                  {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
               @else
                  http://via.placeholder.com/250x250/fff/948e8e?text=Logo 
               @endif" class="img-fluid mx-auto d-block img-empresa">
        </div>
        <div class="row">
            <div class="col-6 col-md-6 centrado">
                <div  class="hijo">
                    <h2 class="titulo">{{ $empresa->nombre }}</h2>
                    <p><i class="fa fa-map-marker"></i><span class="soloPri">
                        <?php
                            setlocale(LC_ALL, "es_CO.UTF-8");
                            $optval = \Illuminate\Support\Facades\DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` LIKE "sede" AND `oportunidad_id` = '.$oportunidad->id.' ORDER BY `id` DESC LIMIT 1');  

                            $sede = App\EmpresaSedes::findOrFail($optval[0]->value); 
                            echo $sede->ciudad.", ".$sede->pais;
                        ?>
                    </span></p>
                    <p class="text-muted capitalize">{{ $empresa->nombre_p_a_cargo }}/{{ $empresa->cargo }}</p> 
                </div>
            </div>
            <div class="col-6 col-md-6 centrado" style="padding-top: 70px">    
                <p class="subtitulo">
                    <img src="{{ url('/') }}/images/form/eye.png">
                    <span class="subtitulo2">Identificado el </span>
                    <?php 
                        $otro = $datos->filter(function ($value, $key) {
                          return $value->key == "fecha_ff";                            
                        });

                        $respal = $otro;
                        $total = count($otro);
                    ?>
                    <input type="hidden" id="fecha_ff" value="{{ $respal }}"> 
                    <?php 
                        $casi = $otro->pop(); 
                        if (isset($casi->value)) {echo strftime("%d %B %Y", strtotime($casi->value));}
                    ?>
                    @if($total>1)
                    <a href="#" onclick="llenarTabla('fecha_ff')" class="histrialModal">
                        <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                    </a>
                    @endif
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="{{ url('/') }}/images/form/hand-shake.png">
                    <span class="subtitulo2">Por cerrar el </span>
                    <?php 
                        $otro = $datos->filter(function ($value, $key) {
                          return $value->key == "fecha_oc";                            
                        });

                        $respal = $otro;
                        $total = count($otro);
                    ?>
                    <input type="hidden" id="fecha_oc" value="{{ $respal }}"> 
                    <?php 
                        $casi = $otro->pop(); 
                        if (isset($casi->value)) {echo strftime("%d %B %Y", strtotime($casi->value));}
                    ?>
                    @if($total>1)
                    <a href="#" onclick="llenarTabla('fecha_oc')" class="histrialModal">
                        <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                    </a>
                    @endif
                </p>     
            </div>
            <div class="col-md-12">
                <div class="hijo2 row justify-content-md-center" style="
                                                border: solid 1px rgba(186, 190, 193, 0.55);
                                                padding: 20px;
                                                margin-top: 20px;
                                                margin-bottom: 20px;">
                        <?php 
                            //aqui son los eliominados corregir 
                            $productosRes = App\OportunidadProducto::where("oportunidad_id",$oportunidad->id)->get();
                            $productosDel = App\OportunidadProducto::onlyTrashed()->where("oportunidad_id",$oportunidad->id)->get();
                            $htmlrespal = '';
                            foreach ($productosDel as $res) { 
                            try {
                                if (isset($res->producto) && is_numeric($res->producto)) {
                                    //$imgproducto = App\Producto::where('name', '=', $res->producto)->pluck('foto');
                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop(); 
                                    if (isset($res->referencia) && is_numeric($res->referencia)) {
                                        $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                                    }else{
                                        $referencia->referencia = "Estandar";
                                    }

                                    $htmlrespal .= '<tr>
                                      <td>'.$res->created_at.'</td>
                                      <td>'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.'</td>
                                    </tr>';
                                }                              
                            } catch (Exception $e) { 
                                //$imgproducto = App\Producto::where('name', '=', $res)->pluck('foto');
                            } 
                          }
                            
                            //$respal = $productosDel;
                            $total = count($productosDel);
                        ?>
                        <input type="hidden" id="productos" value="{{ $htmlrespal }}">                        
                        <div class="col-3 col-sm-2 placeholder img-sedes"> 
                            <p class="subtitulo2">Oportunidad</p>
                            <p class="subtitulo uppercase">OP-{{ $oportunidad->id }}</p>
                        </div>
                        <?php
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "moneda";                            
                            });                      
                            $moneda = $otro->pop();                            

                          foreach ($productosRes as $res) { 
                            try {
                                if (isset($res->producto) && is_numeric($res->producto)) {
                                    //$imgproducto = App\Producto::where('name', '=', $res->producto)->pluck('foto');
                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop(); 
                                    if (isset($res->referencia) && is_numeric($res->referencia)) {
                                        $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop();
                                    }else{
                                        $referencia->referencia = "Estandar";
                                    } 
                                    $html = '<div class="col-3 col-sm-2 placeholder img-sedes"> 
                                    <div class="img-thumbnail img-sedes">
                                      <a href="">
                                        <img src="';
                                            if(!empty($referencia->foto) && $referencia->foto != Null){
                                                $html .= url('/')."/images/file/productos/".$referencia->foto;
                                            }else{
                                              $html .= 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Foto&w=100&h=100';
                                            }
                                            $html .='" style="max-width: 50%;" class="img-fluid  menu mx-auto d-block">
                                      </a>
                                    </div> 
                                    <div class="text-muted centrado"><h6 style="margin-bottom: 0px;">'.$res->cantidad.' '.$producto->name.' '.$referencia->referencia.'</h6><p>'.$res->total.' <span class="uppercase">'.$moneda->value.'</span></p>';
                                        if($total>1){
                                            $html .= '<a href="#" onclick="llenarTabla(\'productos\')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger">'.$total.'</span>
                                            </a>';
                                        }
                                    $html .= '</div>
                                    </div>';
                                    echo $html;
                                }                              
                            } catch (Exception $e) { 
                                //$imgproducto = App\Producto::where('name', '=', $res)->pluck('foto');
                            } 
                          }
                        ?>
                <!-- fin, aqui termina el error es por los productos -->
                </div>
            </div>
            <div class="col-md-12">
                <div class="hijo2 row justify-content-md-center">
                    <div class="table-responsive">
                        <table class="table product-table">
                            <thead>
                                <tr class="tr-table">
                                    <th class="centrado">Ciclo de Venta</th>
                                    <th class="centrado">Responsable</th>
                                    <th class="centrado">Identifico</th>
                                    <th class="centrado">Probabilidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado">
                                        <h2><strong>
                                        <?php 
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "ciclo_venta";                            
                                            });

                                            $respal = $otro;
                                            $total = count($otro); 
                                            $casi = $otro->pop(); 
                                        ?>
                                        <input type="hidden" id="ciclo_venta" value="{{ $respal }}">    
                                        <?php if (isset($casi->value)) {echo $casi->value."%";}?>
                                        @if($total>1)
                                        <a href="#" onclick="llenarTabla('ciclo_venta')" class="histrialModal">
                                            <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                        </a>
                                        @endif
                                        </strong></h2>
                                        <p style="color: #555;">
                                            <?php
                                                switch ($casi->value) {
                                                    case 0:
                                                        echo "Perdida";
                                                        break;
                                                    case 10:
                                                        echo "Detección de oportunidad";
                                                        break;
                                                    case 20:
                                                        echo "Presentación y evaluación";
                                                        break;
                                                    case 30:
                                                        echo "Especificación de productos";
                                                        break;
                                                    case 40:
                                                        echo "Propuesta / Cotización";
                                                        break;
                                                    case 60:
                                                        echo "ciación / Revisió";
                                                        break;
                                                    case 80:
                                                        echo "Gestión de orden de compra";
                                                        break;
                                                    case 90:
                                                        echo "Vendida con OC o Contrato";
                                                        break;
                                                    case 100:
                                                        echo "Facturación y entrega";
                                                        break;
                                                }
                                            ?>
                                        </p>
                                    </td>
                                    <td class="centrar-div">
                                        <?php 
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "responsable";                            
                                            });

                                            $respal = $otro;
                                            $total = count($otro);
                                            $casi = $otro->pop(); 
                                            $otroRespal = [];
                                            foreach ($respal as $valf) {
                                                if (is_numeric($valf->value)) {
                                                    $uderidenti = App\User::find($valf->value);
                                                }else{
                                                    $uderidenti = App\User::where('name', '=', $valf->value)->get()->pop();
                                                }
                                                $nombre   = explode(" ",$uderidenti->nombres);
                                                $apellido = explode(" ",$uderidenti->apellidos); 
                                                $valf->value = $nombre[0] ." ". $apellido[0];
                                                $otroRespal[] = $valf; 
                                            }
                                            $objRespal = json_encode($otroRespal);
                                        ?>
                                        <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">
                                        <?php 
                                            if (is_numeric($casi->value)) {
                                                $uderidenti = App\User::find($casi->value);
                                            }else{
                                                $uderidenti = App\User::where('name', '=', $casi->value)->get();
                                                $uderidenti = $uderidenti[0];
                                            }
                                        ?> 
                                            <?php if (isset($uderidenti) && !empty($uderidenti) ): ?>
                                            <a class="media-left waves-light">
                                                <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/{{ $uderidenti->foto }}" alt="Usuario">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">  
                                                    <input type="hidden" id="responsable" value="{{ $objRespal }}">    

                                                    <?php
                                                        $nombre   = explode(" ",$uderidenti->nombres);
                                                        $apellido = explode(" ",$uderidenti->apellidos); 
                                                        echo $nombre[0] ." ". $apellido[0]; 
                                                    ?>
                                                    @if($total>1)
                                                    <a href="#" onclick="llenarTabla('responsable')" class="histrialModal">
                                                        <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                                    </a>
                                                    @endif
                                                </h4>
                                                <p class="subtitulo">{{ $uderidenti->cargo}}</p>
                                            </div>
                                        <?php endif ?>
                                        </div>
                                    </td>                                    
                                    <td class="centrar-div">
                                        <?php 
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "identifico";                            
                                            });

                                            $respal = $otro;
                                            $total = count($otro);
                                            $casi = $otro->pop(); 
                                            $otroRespal = [];
                                            foreach ($respal as $valf) {
                                                if (is_numeric($valf->value)) {
                                                    $uderidenti = App\User::find($valf->value);
                                                }else{
                                                    $uderidenti = App\User::where('name', '=', $valf->value)->get()->pop();
                                                }
                                                if(isset($uderidenti) && !empty($uderidenti)){
                                                $nombre   = explode(" ",$uderidenti->nombres);
                                                $apellido = explode(" ",$uderidenti->apellidos); 
                                                $valf->value = $nombre[0] ." ". $apellido[0];
                                                $otroRespal[] = $valf; 
                                                }
                                            }
                                            $objRespal = json_encode($otroRespal);
                                        ?>
                                        <div class="media mb-1" style="position: relative;display: list-item !important;margin: auto;left: 10%;">

                                        <?php 
                                            if (is_numeric($casi->value)) {
                                                $uderidenti = App\User::find($casi->value);
                                            }else{
                                                $uderidenti = App\User::where('name', '=', $casi->value)->get();
                                                $uderidenti = $uderidenti[0];
                                            }
                                        ?> 
                                        <?php if (isset($uderidenti) && !empty($uderidenti) ): ?>
                                            <a class="media-left waves-light">
                                                <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/{{ $uderidenti->foto }}" alt="Usuario">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">  
                                                    <input type="hidden" id="identifico" value="{{ $objRespal }}">    

                                                    <?php
                                                        $nombre   = explode(" ",$uderidenti->nombres);
                                                        $apellido = explode(" ",$uderidenti->apellidos); 
                                                        echo $nombre[0] ." ". $apellido[0]; 
                                                    ?>
                                                    @if($total>1)
                                                    <a href="#" onclick="llenarTabla('identifico')" class="histrialModal">
                                                        <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                                    </a>
                                                    @endif
                                                </h4>
                                                <p class="subtitulo">{{ $uderidenti->cargo}}</p>
                                            </div>
                                        <?php endif ?>
                                        </div>
                                    </td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php 
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "probabilidad";                            
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                                $casi = $otro->pop();
                                            ?>
                                            <img src="{{ url('/') }}/images/<?php if (isset($casi->value)) {echo $casi->value;} ?>.png" style="width: 30px;">
                                            <input type="hidden" id="probabilidad" value="{{ $respal }}"> 
                                            <?php if (isset($casi->value)) {echo $casi->value;} ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('probabilidad')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>                                        
                                </tr>
                            </tbody>
                            <thead>
                                <tr class="tr-table">
                                    <th class="centrado">Valor Oportunidad</th>
                                    <th class="centrado">Tasa de cambio</th>
                                    <th class="centrado">Valor en Pesos</th>
                                    <th class="centrado">Margen</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php 
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "valor_oportunidad";                            
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                            ?>  

                                            <input type="hidden" id="valor_oportunidad" value="{{ $respal }}">    

                                            <?php 
                                                $valoracomvertir = $otro->pop(); 
                                                if (isset($valoracomvertir->value)) {echo $valoracomvertir->value;}
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('valor_oportunidad')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                            </a>
                                            @endif

                                            <?php 
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "moneda";                            
                                                });
                                                
                                                $respal = $otro;
                                                $total = count($otro);
                                            ?>  

                                            <input type="hidden" id="moneda" value="{{ $respal }}">    

                                            <?php 
                                                $moneda = $otro->pop(); 
                                                if (isset($moneda->value)) {echo $moneda->value;}
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('moneda')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                       <h2><strong>
                                            <?php 
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "tasa_cambio";                            
                                                }); 
                                                $respal = $otro;
                                                $total = count($otro);
                                                $casi = $otro->pop();
                                            ?> 

                                            <input type="hidden" id="tasa_cambio" value="{{ $respal }}"> 
                                            <?php 
                                                if (isset($casi->value)) {
                                                    $cantidad = str_replace(',','',$casi->value);
                                                    $cantidad = (int)$cantidad;
                                                    $cantidad = number_format($cantidad);
                                                    echo "$ ".$cantidad;
                                                }
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('tasa_cambio')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                            </a>
                                            @endif
                                        </strong></h2> 
                                    </td> 
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                       <h2><strong>
                                            <?php 
                                                $cantidad = str_replace(',','',$cantidad);
                                                $cantidad = (int)$cantidad;
                                                $cantidadValor = str_replace(',','',$valoracomvertir->value);
                                                $resultado = $cantidadValor * $cantidad;
                                                echo "$ ".number_format($resultado);
                                            ?>
                                        </strong></h2> 
                                    </td>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <h2><strong>
                                            <?php 
                                                $otro = $datos->filter(function ($value, $key) {
                                                  return $value->key == "margen";                            
                                                });

                                                $respal = $otro;
                                                $total = count($otro);
                                            ?>
                                            <input type="hidden" id="margen" value="{{ $respal }}">
                                            <?php 
                                                $casi = $otro->pop(); 
                                                if (isset($casi->value)) {echo $casi->value;}
                                            ?>%
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('margen')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                            </a>
                                            @endif
                                        </strong></h2>
                                    </td>                                      
                                </tr>
                            </tbody>                           
                        </table>
                        <table class="table product-table">
                            <thead>
                                <tr class="tr-table">
                                    <th class="centrado">Forma de pago</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="centrado" style="display: table-cell;vertical-align: middle;">
                                        <?php 
                                            $otro = $datos->filter(function ($value, $key) {
                                              return $value->key == "otra_forma_pago";                            
                                            });
                                            
                                            $respal = $otro;
                                            $total = count($otro);
                                            $casi = $otro->pop(); 
                                        ?> 
                                        <p style="font-size: larger;">
                                            <input type="hidden" id="otra_forma_pago" value="{{ $respal }}"> 
                                            <?php 
                                                if (isset($casi->value)) {echo $casi->value;}
                                            ?>
                                            @if($total>1)
                                            <a href="#" onclick="llenarTabla('otra_forma_pago')" class="histrialModal">
                                                <span class="badge badge-pill badge-danger soloprimer parrafo"><?php echo $total; ?></span>
                                            </a>
                                            @endif
                                        </p>

                                        <?php $formpag = App\OportunidadesFormapago::where('oportunidad_id','=', $oportunidad->id)->get();
                                            $i=0;
                                            echo '<table>';
                                            foreach ($formpag as $key => $value) {
                                                if ($value->key=="cuota_valor") {
                                                    $i++;
                                                    $value->key="Cuota ".$i;                                
                                                }
                                                echo '<tbody>
                                                        <tr>
                                                            <td style="min-width: 100px;text-align: left;"><p class="soloprimer parrafo">'.$value->key.'</p></th>
                                                            <td style="min-width: 100px;text-align: left;"><p class="soloprimer parrafo">'.$value->value.'%</p></th>
                                                            <td style="text-align: left;"><p class="soloprimer parrafo">'.$value->texto.'</p></th>
                                                        </tr>
                                                    </tbody>';
                                            }
                                            echo '</table>';
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                            <thead><a name="titulobservaciones"></a>
                                <tr class="tr-table">
                                    <th class="centrado">Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="" style="">
                                        <div class="media mb-1">
                                            <a class="media-left waves-light"><?php $user = App\User::findOrFail($oportunidad->user_id); ?>
                                                <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/{{ $user->foto }}" alt="Usuario">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading observaciones-title">
                                                <?php $nombre   = explode(" ",$user->nombres);
                                                $apellido = explode(" ",$user->apellidos); 
                                                echo $nombre[0] ." ". $apellido[0]; ?></h4>
                                                <p class="subtitulo_coment">
                                                    <?php 
                                                        $otro = $datos->filter(function ($value, $key) {
                                                          return $value->key == "observaciones";                            
                                                        });
                                                        
                                                        $respal = $otro;
                                                        $total = count($otro);
                                                    ?>  

                                                    <input type="hidden" id="observaciones" value="{{ $respal }}">    

                                                    <?php 
                                                        $casi = $otro->pop(); 
                                                        if (isset($casi->value)) {echo $casi->value;}
                                                    ?>
                                                    @if($total>1)
                                                    <a href="#" onclick="llenarTabla('observaciones')" class="histrialModal">
                                                        <span class="badge badge-pill badge-danger"><?php echo $total; ?></span>
                                                    </a>
                                                    @endif
                                                </p>
                                            </div>
                                      </div>
                                      <div id="lisComentariosId"></div>
                                      <form id="formComentarios">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="oportunidad_id" value="{{ $oportunidad->id }}">
                                        <div class="md-form" style="background-color: rgba(174, 219, 251, 0.71);margin-top: 50px;">
                                          <textarea type="text" id="form7" class="form-control" name="comentario" placeholder="Añadir comentario"></textarea>
                                        </div>
                                      </form>
                                      <div class="centrado mb-5">
                                        <a href="#" class="boton negro redondo" onclick="guardarComentario({{ $oportunidad->id }})"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                                      </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>   

<!--<div class="card animated flipInX" style="margin-bottom: 30px;">
    <div class="card-block"> 
        <ul class="nav nav-tabs nav-tabs-custom-colored" role="tablist">
            <li class="active"><a href="#colored21" role="tab" data-toggle="tab">
                Bitacora</a>
            </li>
            <li><a href="#colored22" role="tab" data-toggle="tab">
                Backlog</a>
            </li>
            <li><a href="#colored23" role="tab" data-toggle="tab">
                Visitas de clientes</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="colored21">
                <div class="container2">   
                    <section class="main2">
                        <ul class="timeline2">
                            @foreach($actividades as $actividad)
                            <li class="event2">
                                <input type="radio" class="clickea" data-name="{{$actividad->id}}" name="tl-group"/>
                                <label></label>
                                <?php $user = App\User::findOrFail($actividad->user_id); ?>
                                <div class="thumb" style="background-image: url({{ url('/') }}/images/file/clientes/{{ $user->foto }});background-repeat: no-repeat;background-position: center;background-size: 90px;">
                                    <span>{{ $actividad->fecha_actividad }}</span>
                                </div>
                                <div class="content-perspective">
                                    <div class="content2">
                                        <div class="content-inner2">
                                            <h3>{{ $actividad->tipo_actividad }}</h3>
                                            <div class="div-oculto" id="{{$actividad->id}}" style="display: none">
                                                <p class="subtitulo2">{{ $actividad->tiempo_inicio }} - {{ $actividad->tiempo_fin }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estado Actividad: <span class="badge indigo">{{ $actividad->estado_actividad }}</span></p>
                                                <h5 class="card-header"> Detalle Actividad </h5>
                                                <p class="subtitulo_coment">{{ $actividad->detalle_actividad }}</p>
                                                <h5 class="card-header"> Resultado Actividad </h5>
                                                <p class="subtitulo">{{ $actividad->detalle_resultado }}</p>
                                                <h5 class="card-header"> Actividades Pendientes </h5>
                                                <p class="subtitulo" style="text-align: center;"> 
                                                    <span class="badge indigo" style="margin-top: 10px;font-size: 20px">
                                                        <?php 
                                                            $datoac = json_decode($actividad->detalle_pendiente);
                                                            echo count($datoac);
                                                        ?>
                                                    </span>    
                                                </p>
                                            </div>                            
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </section>
                </div>
            </div>
            <div class="tab-pane fade" id="colored22">
                <div class="container2">  

                    <div class="table-responsive">            
                        <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                            <thead>
                                <tr>
                                    <th class="centrado">#</th> 
                                    <th class="centrado">Fecha pendiente</th> 
                                    <th class="centrado">Estado</th>  
                                    <th class="centrado">Veces aplazadas</th> 
                                    <th class="centrado">Funcionario</th> 
                                    <th class="centrado">Detalle de la actividad</th> 
                                    <th class="centrado">Observaciónes de la actividad</th>              
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $i = 1;
                                @endphp
                                @foreach($actidetalles as $actidetalle)
                                <?php 
                                    $user = App\User::findOrFail($actidetalle->user_id); 
                                    $aplazadas = App\Actividades_detalles_canceladas::where("actividades_detalles",$actidetalle->id)->get();
                                    $nombre   = explode(" ",$user->nombres);
                                    $apellido = explode(" ",$user->apellidos); 
                                ?>
                                <tr class="text-center dato">
                                    <td><span>{{ $i++ }}</span></td>
                                    <td><span>{{ $actidetalle->fecha_pendiente }}</span></td>
                                    <td><span>{{ $actidetalle->estado }}</span></td>
                                    <td><span>{{ count($aplazadas) }}</span></td>
                                    <td>{{ $nombre[0]." ".$apellido[0] }}</td>
                                    <td>{{ $actidetalle->detalle }}</td>
                                    <td>{{ $actidetalle->observaciones }}
                                        @foreach($aplazadas as $aplazada)
                                            {{ "&#10; * ".$aplazada->observaciones }}
                                        @endforeach
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="colored23">
                <div class="container2">   
                    <section class="main2">
                        <ul class="timeline2">
                            @foreach($agentclients as $agenda)
                            <li class="event2">
                                <input type="radio" class="clickea" data-name="A{{$agenda->id}}" name="tl-group"/>
                                <label></label>
                                <?php $user = App\User::findOrFail($agenda->user_id); ?>
                                <div class="thumb" style="background-image: url({{ url('/') }}/images/file/clientes/{{ $user->foto }});">
                                    <span style="position: inherit;top: 110px;">{{ $agenda->fecha_inicio }} - {{ $agenda->fecha_final }}</span>
                                </div>
                                <div class="content-perspective">
                                    <div class="content2">
                                        <div class="content-inner2">
                                            <h3>
                                                Visita cliente

                                                <?php 
                                                    $agentclientact = App\Agenda_clientes_visitante::where('agenda_clientes', $agenda->id)->get();
                                                    foreach ($agentclientact as $key => $value) {
                                                        $client = App\Cliente::findOrFail($value->cliente);
                                                        echo $client->cliente.", ";
                                                    }
                                                ?>
                                            </h3>
                                            <div class="div-oculto" id="A{{$agenda->id}}" style="display: none">
                                                <p class="subtitulo2">
                                                    <span class="badge indigo">
                                                        <?php 
                                                            $agentclientact = App\Agenda_clientes_actividade::where('agenda_clientes', $agenda->id)->get(); 
                                                            echo count($agentclientact)." Actividades";
                                                        ?>
                                                    </span>
                                                </p>
                                                <h5 class="card-header"> Observaciones </h5>
                                                <p class="subtitulo_coment">{{ $agenda->observaciones }}</p>
                                                <?php if (count($agentclientact)>0): ?>
                                                <div class="table-responsive">
                                                    <table class="table product-table">
                                                        <thead>
                                                            <tr class="tr-table">
                                                                <th class="centrado">Fecha</th>
                                                                <th class="centrado">H. Inicio</th>
                                                                <th class="centrado">H. Fin</th>
                                                                <th class="centrado">Descripcion</th>
                                                                <th class="centrado">Responsable</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($agentclientact as $key => $value) { ?>
                                                            <tr>
                                                                <td class="centrado">
                                                                <?php 
                                                                    try {
                                                                        \Carbon\Carbon::setLocale('es');
                                                                        
                                                                        $dt2 = \Carbon\Carbon::createFromFormat('Y-m-d', $value->fecha);
                                                                        echo ($dt2->format('d \d\e M \d\e\l Y'));
                                                                    } catch (Exception $e) {}
                                                                ?>
                                                                </td>
                                                                <td class="centrado">
                                                                <?php  
                                                                try {                             
                                                                    $dt2 = \Carbon\Carbon::createFromFormat('H:i', $value->hora_inicio);
                                                                    echo ($dt2->format('H:i'));
                                                                } catch (Exception $e) {}
                                                                ?>
                                                                </td>  
                                                                <td class="centrado">
                                                                <?php   
                                                                try {                              
                                                                    $dt2 = \Carbon\Carbon::createFromFormat('H:i', $value->hora_fin);
                                                                    echo ($dt2->format('H:i'));
                                                                } catch (Exception $e) {}
                                                                ?>
                                                                </td> 
                                                                <td><?php echo $value->descripcion; ?></td>
                                                                <td class="centrado">
                                                                <?php 
                                                                    if (!empty($value->user_id)) {
                                                                        $useren = App\User::findOrFail($value->user_id); 
                                                                        echo $useren->nombres." ".$useren->apellidos;
                                                                    }else{
                                                                        echo "Ninguno";
                                                                    }
                                                                    
                                                                ?>                                                    
                                                                </td>                                      
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php endif ?>
                                                <p class="subtitulo">Estado: 
                                                    <span class="badge indigo">
                                                        {{ $agenda->estado }}
                                                    </span>    
                                                </p>
                                            </div>                            
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>-->
    

    <!-- modal de cambios -->
    <div class="modal fade" id="exampleModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Historial de cambios</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="table-responsive">            
                <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                  <thead>
                    <tr>
                      <th class="centrado">Fecha</th>
                      <th class="centrado">Valor</th>                 
                    </tr>
                  </thead>
                  <tbody id="idTable">                 
                    
                  </tbody>
                </table>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- fin modal de cambios -->

    <!-- modal de cambios -->
    <div class="modal fade" id="detalleModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Detalle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" >
            <span style="display:block;text-align:center;" >
            <!--<img src="{{ url('/') }}/images/empresas/kMbwrX5R.jpeg" style="height: 140px;width: 140px;">-->
            </span>
            <div id="contenbodyModal">
                
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- fin modal de cambios -->

@endsection


@section('scripts')

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script src="{{ url('/') }}/components/3Dtimeline/js/modernizr.custom.63321.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script> 

<script type="text/javascript">
    direccion="{{ url('/') }}";
    $(function() {
        getComentarios({{ $oportunidad->id }});
        $("body").on("click",".clickea",function(e){
            id=$(this).attr("data-name");
            $(".div-oculto").hide("clip");
            $("#"+id).show("clip");
        });
        $( "#comentarbtn" ).click(function() {
          setTimeout(function(){ $("#form7").focus() }, 1000);
        });
    });
   
    guardarComentario = function (id) {
        event.preventDefault();    
        if(true === $("#formComentarios").parsley().validate()){
          console.log("llega"); 
          var datos = new FormData($("#formComentarios")[0]);
          $.ajax({
            url: "{{ url('savecomenoport') }}",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
              if (data.success) {
                $('#formComentarios')[0].reset();
                getComentarios(id);
              }else{
                swal("Algo salio mal, vuelve a intentar");
              }
            }
        }); 
        }else{
          swal("Faltan campos por completar!");
        }
    }

    $("body").on("click",".histrialModal",function(e){
      $("#exampleModal").modal();
    })

    getComentarios = function (id) {
        $.ajax({
          url: "{{ url('listcomenoport') }}/"+id,
          type: 'GET',
          success: function(data){
            if (data.success) {
              console.log(data.datos);
              var html3 = "";
              moment.locale('es');
              for(i in data.datos){
                comentario = data.datos[i];
                html3 += `
                <div class="media mb-1">
                  <a class="media-left waves-light">
                      <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/`+comentario.user.foto+`" alt="Usuario">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading observaciones-title">`+comentario.user.nombres+` `+comentario.user.apellidos+` <small>`+moment(comentario.created_at).calendar()+`</small> </h4>
                      <p class="subtitulo_coment">`+comentario.comentario+`</p>
                  </div>
                </div>`;
              }
              $("#lisComentariosId").html(html3);
            }else{

            }
          }
        });
    }

    llenarTabla =  function (Datos) {
        if (Datos === "productos") {
            $("#idTable").html($("#"+Datos).val());
        }else{
            html = "";
            //console.log(Datos);
            Datos = $("#"+Datos).val();
            Datos = JSON.parse(Datos);

            $.each(Datos, function( index, element ) {
                html += `<tr>
                  <td>`+element["created_at"]+`</td>
                  <td>`+element["value"]+`</td>
                </tr>`;
            });
            $("#idTable").html(html);
        }
    }

    verCliente = function () {
        dato = [];
        dato["cliente"] = "{{ $cliente->cliente }}";
        dato["empresa"] = "{{ $cliente->empresa }}";
        dato["unidad_negocio"] = "{{ $cliente->unidad_negocio }}";
        <?php $cliente->perfil = str_replace(array("\n", "\r", "\n\r"), " <br> ", $cliente->perfil); ?>
        dato["perfil"] = `{{ $cliente->perfil }}`;
        dato["cargo"] = "{{ $cliente->cargo }}";
        dato["jefe_inmediato"] = "{{ $cliente->jefe_inmediato }}";
        dato["n_a_pesos"] = "{{ $cliente->n_a_pesos }}";
        dato["pais"] = "{{ $cliente->pais }}";
        dato["departamento"] = "{{ $cliente->departamento }}";
        dato["ciudad"] = "{{ $cliente->ciudad }}";

        html = `<label class="col-form-label fondo_title">Cliente</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["cliente"]+`
                </p>
                <label class="col-form-label fondo_title">Empresa</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["empresa"]+`
                </p>
                <label class="col-form-label fondo_title">Unidad de negocio</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["unidad_negocio"]+`
                </p>
                <label class="col-form-label fondo_title">Perfil</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["perfil"]+`
                </p>
                <label class="col-form-label fondo_title">Cargo</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["cargo"]+`
                </p>
                <label class="col-form-label fondo_title">Jefe inmediato</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["jefe_inmediato"]+`
                </p>
                <label class="col-form-label fondo_title">Valor aprovado</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    $ `+dato["n_a_pesos"]+`
                </p>
                <label class="col-form-label fondo_title">Ubicacion</label>
                <p class="letra-gris" style="margin-bottom: 30px;">
                    `+dato["pais"]+`, `+dato["ciudad"]+` - `+dato["departamento"]+`
                </p>`;

        $("#contenbodyModal").html(html);
    }

    verEmpresa = function() {
        nombre = "{{ $empresa->nombre }}";
        nit = "{{ $empresa->nit }}";
        pagina_web = "{{ $empresa->pagina_web }}";
        telefono = "{{ $empresa->telefono }}";
    }
</script>
@endsection

