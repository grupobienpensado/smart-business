@extends('template.app')
@section('title', 'Calendario de cierre finalizados')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/oportunidades/calendariocierre.css" />
<style>
    .ano{ cursor: pointer; }
    .activo{ color: red !important; }
</style>
{{ csrf_field() }}
<div class="content">
    <div class="main-header">
        <h2>Calendario</h2>
        <em>Calendario de negocios cerrados</em>
    </div>
    <div class="main-content">
        <!-- INVOICE -->
        <div class="invoice">
            <!-- invoice header -->
            <div class="invoice-header">
                <div class="row">
                    <div class="col-lg-3 col-print-3 text-left font-calendar">
                     <a><i class="calendar-close fa fa-calendar" aria-hidden="true"></i></a><a id="anos_ventas"></a>
                    </div>
                    <div class="col-lg-9 col-print-9">
                        <ul class="list-inline">
                            <li>Mes cierre mas alto: <strong id="mes_alto">Enero</strong></li>
                            <li>Mes cierre mas bajo: <strong id="mes_bajo">Diciembre</strong></li>
                            <li>Total: <strong id="total_ano">$0 Millones</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-5 m-25">
    <div class="col-md-12">
        <select class="form-control" name="filtro" id="producto"></select><a class="c-p text-danger eliminar-filtro ocultar" data-toggle="tooltip" data-placement="right" title="Eliminar filtro"><i class="fa fa-times-circle fa-2" aria-hidden="true"></i></a>
    </div>
</div>
<div class="row active-with-click" id="calendario">

</div>
<div class="alert alert-success bordes-redondos alert-filtro scale-in-hor-right ocultar">
  <strong>Filtro Realizado!</strong> El filtro se realizo correctamente!.
</div>

<div class="modal fade viewmonth-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-round" style="margin-top: 0%">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Mes Año</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <img src="{{url('/')}}/images/icons/essi_logo.png" class="logo-essi">
            </div>
            <div class="modal-body">
                <div class="row cont-view-month">
                    <div class="col-md-4 border-card">
                        <div class="row mr-5">
                            <div class="col-md-12">
                                <div class="triangulo"></div>
                                <div class="row banda-blue mt-5">
                                    <div class="col-md-12 mt-2">
                                      <h4 class="mt-4">Llave en mano - Ingeniería de detalle y cálculos</h4>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-md-6 mt-5">
                                        <img class="img-view" src="{{url('/')}}/images/file/productos/59d65225d258b.png">
                                    </div>
                                    <div class="col-md-6 mt-5">
                                        <p class="color-view h1"><strong>3</strong></p>
                                        <p class="color-view h4"><i class="fa fa-database" aria-hidden="true"></i> Unidades</p>
                                        <p class="color-view h1"><strong>230</strong></p>
                                        <p class="color-view h4"><i class="fa fa-usd" aria-hidden="true"></i> Millones</p>
                                    </div>
                                    <div class="col-md-12 mb-5">
                                        <div class="row mb-5">
                                            <div class="col-md-4">
                                                <img class="img-view2" src="{{url('/')}}/images/file/empresas/principal/59847ad7d683f.png">
                                                <p class="h5 color-view">TANILAC</p>
                                                <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i>COLOMBIA</p>
                                                <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i>01/12/2018</p>
                                                <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i>3</p>
                                            </div>
                                            <div class="col-md-4">
                                                <img class="img-view2" src="{{url('/')}}/images/file/empresas/principal/59847ad7d683f.png">
                                                <p class="h5 color-view">TANILAC</p>
                                                <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i>COLOMBIA</p>
                                                <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i>01/12/2018</p>
                                                <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i>3</p>
                                            </div>
                                            <div class="col-md-4">
                                                <img class="img-view2" src="{{url('/')}}/images/file/empresas/principal/59847ad7d683f.png">
                                                <p class="h5 color-view">TANILAC</p>
                                                <p class="h6 color-view"><i class="fa fa-map-marker icon-view" aria-hidden="true"></i>COLOMBIA</p>
                                                <p class="h6 color-view"><i class="fa fa-calendar-check-o icon-view" aria-hidden="true"></i>01/12/2018</p>
                                                <p class="h6 color-view"><i class="fa fa-cogs icon-view" aria-hidden="true"></i>3</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="triangulo2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script>
    var CSRF_TOKEN = $('input[name=_token]').val();
    var medida = 0;
    $(document).ready(function(){
        moment.locale('es');
        calcular_anos(moment().format('YYYY'));
        mes_alto_menor(moment().format('YYYY'),'');
        var ventana_ancho = $(window).width();
        var ventana_alto = $(window).height();
        if(ventana_ancho>=1366 && ventana_ancho<=1919){
            medida=1;
        }
        calendario(medida,moment().format('YYYY'),'');
        filtro_maquinas();
    });

    /**
     * Mensaje de alerta y mostrar eliminar
     * @param {STRING} maquina VARIABLE CON EL FILTRO APLICADO
     */
    function alert_filtro(maquina){
        if(maquina!=''){
            $('.alert-filtro').removeClass('ocultar');
            setTimeout(function(){ $('.alert-filtro').addClass('ocultar'); }, 2000);
            $('.eliminar-filtro').removeClass('ocultar');
        }
    }

    /**
     * Funcion para armar el calendario
     * @param {INT} medida  VARIABLE CON QUE SE IDENTIFICA LA MEDIDA DE LA PANTALLA
     * @param {INT} ano     VARIABLE DONDE SE ENVIA EL AÑO QUE SE QUIERE PREGUNTAR
     * @param {INT} maquina VARIABLE CON EL ID DE LA MAQUINA POR LA CUAL SE VA A FILTRAR
     */
    function calendario(medida,ano,maquina){
        var jqxhr = $.ajax({
            url: "{{url('/')}}/calendario-action-cerrados",
            data: {
                _token: CSRF_TOKEN,
                medida: medida,
                ano: ano,
                maquina: maquina,
                action: 2
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#calendario').html(e.data['html']);
                activar_card();
                alert_filtro(maquina);
            }
        });
    }

    /**
     * Funcion para activar los efectos de las card
     */
    function activar_card(){
        /*Activae el tooltip*/
        $("[data-toggle='tooltip']").tooltip();
        /*Activador de card*/
        $('.material-card > .mc-btn-action').click(function () {
            var card = $(this).parent('.material-card');
            var icon = $(this).children('i');
            icon.addClass('fa-spin-fast');

            if (card.hasClass('mc-active')) {
                card.removeClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-arrow-left')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-bars');

                }, 800);
            } else {
                card.addClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-bars')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-arrow-left');

                }, 800);
            }
        });
    }

    /**
     * Funcion para traer los años en los que hay cierres
     */
    function calcular_anos(ano_activo){
        $('#anos_ventas').html('');
        var jqxhr = $.ajax({
            url: "{{url('/')}}/calendario-action-cerrados",
            data: {
                _token: CSRF_TOKEN,
                action: 0
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $.each( e.data['ano_cierre'], function( index, value ){
                    clase = ano_activo==value? "activo":"";
                    ano = ` <a class="ano `+clase+`">`+value+`</a>`;
                    $('#anos_ventas').html(ano + $('#anos_ventas').html());
                });
                if(e.data['msj']){
                    $('#anos_ventas').html(e.data['msj']);
                }
            }
        });
    }

    /**
     * Función para traer los meses con mayor y menor valor en ventas
     * @param {INT} ano_activo VARIABLE AÑO SELECCIONADO
     */
    function mes_alto_menor(ano_activo,maquina){
        var jqxhr = $.ajax({
            url: "{{url('/')}}/calendario-action-cerrados",
            data: {
                _token: CSRF_TOKEN,
                action: 1,
                ano: ano_activo,
                maquina: maquina
            },
            cache: false,
            type: 'POST',
            success: function(e){
                if(e.data != "No hay información"){
                    $('#mes_alto').html(e.data['mayor']['mes']);
                    $('#mes_bajo').html(e.data['menor']['mes']);
                    $('#total_ano').html('$ '+e.data['total']+' Millones');
                }else{
                    $('#mes_alto').html('No existe');
                    $('#mes_bajo').html('No existe');
                     $('#total_ano').html('$ 0 Millones');
                }
            }
        });
    }

    /**
     * Al seleccionar un año traer la información correspondiente a ese año
     */
    $(document).on('click','.ano',function(){
       $(".activo").each(function(){
        	$(this).removeClass('activo');
        });
        $(this).addClass('activo');
        mes_alto_menor($(this).html(),'');
        calendario(medida,$(this).html(),'');
        $('.eliminar-filtro').click();
    });

    /**
     * Traer el listado de las
     */
    function filtro_maquinas(){
        $('#producto').select2({
        placeholder: "Seleccione una maquina",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
          dataType: 'json',
          url: "{{url('/')}}/sproducto",
          delay: 250,
          data: function(params) {
              return {
                  term: params.term
              }
          },
          processResults: function(data, page) {
            return {
              results: data
            };
          },
        }
      });
    }

    /**
     * Al seleccionar un producto cambiar la busqueda
     */
    $(document).on('change','#producto',function(){
        var ano = $('.activo').html();
        calendario(medida,ano,$(this).val());
        mes_alto_menor(ano,$(this).val());
    });

    /**
     * Al oprimir la X se reinicia la busqueda solo con el filtro del año eliminando la del producto
     */
    $(document).on('click','.eliminar-filtro',function(){
        var ano = $('.activo').html();
        calendario(medida,ano,'');
        mes_alto_menor(ano,'');
        $(this).addClass('ocultar');
         $('.alert-filtro').removeClass('ocultar');
         setTimeout(function(){ $('.alert-filtro').addClass('ocultar'); }, 2000);
        $('#producto').html('');
    });

    /**
     * Función para ver las maquinas en un modal
     * @param {INT} ano EL AÑO QUE SE ESTA BUSCANDO
     * @param {INT} mes EL MES QUE SE ESTA BUSCANDO
     */
    function ver_mes(ano,mes){
        var cargando = `<div class="col-md-12 text-center"><img class="border-not" src="{{url('/')}}/images/configuracion_crm/configuracion.gif">`;
        $('.cont-view-month').html(cargando);
        $('.modal-content').css('margin-top','0%');
        $('#myLargeModalLabel').html('mes año');
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/calendario-action-cerrados",
            data: {
                _token: CSRF_TOKEN,
                mes: mes,
                ano: ano,
                action: 3,
                maquina: $('#producto').val(),
                url:"{{url('/')}}"
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#myLargeModalLabel').html(e.data['mes']);
                if((e.data['margin-top'])==0){
                   e.data['margin-top']=6;
                }else if((e.data['margin-top'])==19.16){
                    e.data['margin-top']=27;
                }
                $('.modal-content').css('margin-top',e.data['margin-top']+'%')
                $('.cont-view-month').html('');
                $.each(e.data['html'], function( k, v ) {
                  $('.cont-view-month').prepend(v);
                    $('#contador_'+k).html(e.data['contador_maquina'][k]);
                    $('#total_'+k).html(e.data['total_maquina'][k]);
                });
                $.each(e.data['cantidad_total_empresa'], function( k, v ) {
                    $('#'+k).html('cantidad: '+v);
                });
            }
        });
        $('.viewmonth-modal-lg').modal('show');
    }
</script>
@endsection
