@extends('template.app')

@section('title', 'Ver Oportunidades Flujo de caja')

@section('content')
<!-- Styles -->
<style>
#chartdiv,#chartdiv1 { width: 100%; height: 500px !important; font-size: 11px; }
 .filtro a{ font-size: x-large; color: #3dbcef !important; }
    .img-res{ border-radius: 200px 200px 200px 200px; -moz-border-radius: 200px 200px 200px 200px; -webkit-border-radius: 200px 200px 200px 200px; border: 0px solid #000000; width: 100px;}
</style>
<!--Calendario-->
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div class="card animated flipInX" style="margin-bottom: 30px;">
    <ul class="nav nav-pills" role="tablist">
        <?php if(!isset($data['menu'])){ ?>
        <!--<li><a href="{{ url('/')}}/oportunidad/{{ $data['id'] }}"><i class="fa fa-rocket"></i> Oportunidad</a></li>
        <li><a href="{{ url('acciones')}}/{{ $data['id'] }}"><i class="fa fa-check-square-o"></i> Cambiar ciclo</a></li>
        <li class="active"><a href="{{ url('/')}}/ver-oportunidad-flujo/{{ $data['id'] }}"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
        <li><a href="{{ url('/')}}/gestionesoportunidad/{{ $data['id'] }}"><i class="fa fa-calendar"></i> Gestiones</a></li>-->
          <li><a href="/oportunidad/<?=$data['id']?>"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
          @if($data['permiso_btn_ciclo'] == "Si")<li><a href="/acciones/<?=$data['id']?>"><i class="fa fa-pie-chart"></i> Cambiar Ciclo</a></li>@endif
          <li><a href="/oportunidad/acta/<?=$data['id']?>"><i class="fa fa-line-chart"></i> Actas reunion</a></li>
          <li class="active"><a href="/ver-oportunidad-flujo/<?=$data['id']?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
          <li><a href="/gestionesoportunidad/<?=$data['id']?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
        <?php }else{
            print($data['menu']);
         } ?>
    </ul>

    <div class="card-block">
        <div class="profile-empresa">
            <img src="
               @if(!empty($data['empresa']->logo))
                  {{ url('/') }}/images/file/empresas/principal/{{ $data['empresa']->logo }}
               @else
                  http://via.placeholder.com/250x250/fff/948e8e?text=Logo
               @endif" class="img-fluid mx-auto d-block img-empresa">
        </div>
    </div>

    <div class="row">
        <div class="col-6 col-md-6 centrado">
            <div class="hijo">
                <h2 class="titulo">{{ $data['empresa']->nombre }}</h2>
                <p><i class="fa fa-map-marker"></i><span class="soloPri">
                    <?php
                        setlocale(LC_ALL, "es_CO.UTF-8");
                        $optval = \Illuminate\Support\Facades\DB::select('SELECT * FROM `historial_oportunidades` WHERE `key` LIKE "sede" AND `oportunidad_id` = '.$data['id'].' ORDER BY `id` DESC LIMIT 1');

                        $sede = App\EmpresaSedes::findOrFail($optval[0]->value);
                        echo $sede->ciudad.", ".$sede->pais;
                    ?>
                </span></p>
                <p class="text-muted capitalize">{{ $data['empresa']->nombre_p_a_cargo }}/{{ $data['empresa']->cargo }}</p>
            </div>
        </div>
        <div class="col-6 col-md-6 centrado" style="padding-top: 70px">
            <div class="row">
                <div class="col-md-6 text-right">
                    <img class="img-res" src="{{url('/')}}/images/file/clientes/{{$data['Responsable'][0]->foto}}">
                </div>
                <div class="col-md-6 text-left">
                    <p class="h3">{{$data['Responsable'][0]->name}}</p>
                    <p class="h6">Responsable</p>
                </div>
            </div>
            <p class="subtitulo">
                <img src="{{ url('/') }}/images/form/eye.png">
                <span class="subtitulo2">Identificado el </span>
                {{formt_fecha($data['fecha_identificacion'][0]->value)}}

                <img src="{{ url('/') }}/images/form/hand-shake.png">
                <span class="subtitulo2">Por cerrar el </span>
                {{formt_fecha($data['fecha_cerrar'][0]->value)}}
            </p>
        </div>
    </div>
    <?php if(!isset($data['no_datos'])){ ?>
    <form method="POST" action="{{ url('ver-oportunidad-flujo-action') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="accion" value="0">
    <input type="hidden" name="id" value="{{$data['id']}}">
    <?php if(isset($data['id_menu_solo_vendida'])){ ?> <input type="hidden" name="id_menu_solo_vendida" value="{{$data['id_menu_solo_vendida']}}"> <?php } ?>
    <?php if(isset($data['menu'])){ ?> <input type="hidden" name="flujoventas" value="si"><?php } ?>
    <div class="row">

        <div class="col-md-12">
            <div class="row filtro mt-5">

                <div class="col-md-1 text-right">
                    <a><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-5">
                    <input class="form-control form-control-sm filtro" value="@if(isset($data['filtro_fecha_inicio_seleccionado'])) {{$data['filtro_fecha_inicio_seleccionado']}} @else {{$data['filtro_fecha_inicio']}} @endif" type="text" name="fecha_inicio" id="fecha_ff" placeholder="Por favor ingrese la fecha inicio" required>
                </div>
                <div class="col-md-1 text-right">
                    <a><i class="fa fa-calendar" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-5">
                    <input class="form-control form-control-sm filtro" value="@if(isset($data['filtro_fecha_fin_seleccionado'])) {{$data['filtro_fecha_fin_seleccionado']}} @else {{$data['filtro_fecha_fin']}} @endif" type="text" name="fecha_fin" id="fecha_oc" placeholder="Por favor ingrese la fecha fin" required>
                </div>
            </div>
        </div>
    </div>
    <button id="btn-filtro" style="display: none;">filtrar</button>
    </form>
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>Flujo de caja diario</h3>
            <!-- HTML -->
            <div id="chartdiv"></div>
        </div>
        <div class="col-md-12">
            <h2>Total $ <?=number_format($data['valortotal'], 0, '', ',')?></h2>
        </div>
        <div class="col-md-12 text-center mt-5">
            <h3>Flujo de caja acumulado</h3>
            <div id="chartdiv1" ></div>
        </div>
        <div class="col-md-12">
            <h2>Total $ <?=number_format($data['valortotal_anterior'], 0, '', ',')?></h2>
        </div>
    </div>
    <?php }else{ ?>
    <div class="row my-5">
        <div class="col-md-12 my-5">
            <h3>No existen gastos</h3>
            <h2>Total $ 0</h2>
        </div>
    </div>
    <?php } ?>
</div>


<?php
function formt_fecha($fecha){
    $dias= array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
    $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
    $dia=$dias[intval(date('w', strtotime($fecha)))];
    $mes=$meses[intval(date('m', strtotime($fecha)))];
    return $dia.' '.date('d', strtotime($fecha)).' de '.$mes.' del '.date('Y', strtotime($fecha));
}
?>
@endsection

@section('scripts')
<!--calendario-->
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<!-- Chart code -->

<?=$data['javascript']?>
<script>
$('#fecha_ff').bootstrapMaterialDatePicker({
    time: false,
    clearButton: true,
    nowButton: true,
    lang: 'es',
    minDate : "{{$data['filtro_fecha_inicio']}}",
    maxDate : "{{$data['filtro_fecha_fin']}}",
    cancelText : 'Cancelar',
    okText: 'Aceptar',
    nowText: 'Nuevo',
    clearText: 'Limpiar'
});
$('#fecha_oc').bootstrapMaterialDatePicker({
    time: false,
    clearButton: true,
    nowButton: true,
    lang: 'es',
    minDate : "{{$data['filtro_fecha_inicio']}}",
    maxDate : "{{$data['filtro_fecha_fin']}}",
    cancelText : 'Cancelar',
    okText: 'Aceptar',
    nowText: 'Nuevo',
    clearText: 'Limpiar'
});

    $(document).on('change','.filtro',function(){
        $('#btn-filtro').click();
    })
</script>
@endsection
