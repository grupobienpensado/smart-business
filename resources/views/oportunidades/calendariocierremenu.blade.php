@extends('template.app')
@section('title', 'Menu Calendario de cierre')

@section('content')
<style>
    .w-imag{ height: 180px; content:url("{{url('/')}}/images/mesesano/calendario_cierres_realizados.svg"); }
    .bg-close{ background-color:  #0091C1; cursor: pointer; color: #fff;}
    .bg-close:hover{ background-color:  #fff; color: #0091C1 !important; }
    .bg-close{transition: all 0.5s ease-in; }
    .w-imag{transition: all 1s ease-in; }
    .bg-close .w-imag:hover{ content:url("{{url('/')}}/images/mesesano/calendario_cierres_realizados_hover.svg"); }
    .w-imag-open{ height: 180px; content:url("{{url('/')}}/images/mesesano/calendario_cierres.svg"); }
    .bg-open{ background-color: #F8481F; cursor: pointer; color: #fff;}
    .bg-open:hover{ background-color:  #fff; color: #F8481F !important; }
    .bg-open{transition: all 0.5s ease-in; }
    .w-imag-open{transition: all 1s ease-in; }
    .bg-open .w-imag-open:hover{ content:url("{{url('/')}}/images/mesesano/calendario_cierres_hover.svg"); }
    .bordes-redondos { border-radius: 10px 10px 10px 10px !important; -moz-border-radius: 10px 10px 10px 10px !important; -webkit-border-radius: 10px 10px 10px 10px !important; border: 0.5px solid #9e9e9e5c !important; }
    .mt-9{ margin-top: 9rem!important; }
    .mt-8{ margin-top: 8.8rem; }
    .h47{ font-size: 47px; }
</style>
<div class="main-header">
    <p class="h47 mt-8">Calendario de Cierre de Negocios</p>
</div>
<div class="row mt-9">
    <div class="col-sm-12 col-md-6">
        <div class="container bg-close bordes-redondos">
            <img class="form-control w-imag my-5">
            <p class="h3">Ver Equipos Vendidos</p>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="container bg-open bordes-redondos">
            <img class="form-control w-imag-open my-5">
            <p class="h3">Ver Proyección de Ventas</p>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).on('click','.bg-close',function(){
        <?php if($data['permiso_calendariovendidos_acceder']=="Si"){ ?>
       location.href = "{{url('/')}}/calendario-cierre-cerrados";
        <?php }else{ ?>
        swal('Advertencia','Usted no tiene permiso para acceder','warning');
        <?php } ?>
    });
    $(document).on('click','.bg-open',function(){
        <?php if($data['permiso_calendarioproyectados_acceder']=="Si"){ ?>
        location.href = "{{url('/')}}/calendario-cierre";
        <?php }else{ ?>
        swal('Advertencia','Usted no tiene permiso para acceder','warning');
        <?php } ?>
    });
</script>
@endsection
