<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Listado de oportunidades')

@section('content') 
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<style type="text/css">
    .tamano-col{width:8%!important}
    .tooltip-inner{white-space:pre-wrap}
    .unalinea{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}
    .mas-pequeno{line-height:1.5;font-size:small;color:#636363;font-weight:400;display:block!important;margin:0!important}
    .centrar-vertical{vertical-align:middle!important}
    .justify{text-align:justify}
    .text-flow-box{ fill: rgb(231, 231, 231);font-family: "Covered By Your Grace";font-size: 18px;opacity: 1;text-anchor: middle;}
    #example_filter{
        margin-left: 1.5rem;
    }
</style>
    <div class="container-fluid animated slideInDown">
      <div class="row">
        <div class="col-sm-12  panel-view">
        <div class="contenido ">
            <div class="pull-right form-inline">
              <div class="btn-group">
                @if($data['permiso_crear'] == "Si")
                <a href="{{ url('registraroportunidad') }}" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Añadir oportunidad</a>
                @endif
                <a href="{{ url('oportunidadesperdidas') }}" target="_blank" class="btn btn-danger btn-sm">
                  <i class="fa fa-list" aria-hidden="true"></i> 
                  Oportunidades perdidas
                </a>
                <a href="{{ url('ventas') }}" class="btn btn-info btn-sm">
                  <i class="fa fa-list" aria-hidden="true"></i> 
                  Oportunidades vendidas
                </a>
                <!--<a href="{{ url('/calendario/oportunidades') }}" class="btn btn-essi btn-sm">
                  <i class="fa fa-calendar-o" aria-hidden="true"></i> 
                  Oportunidades calendario
                </a>-->
              </div>
            </div>

            <h3>Oportunidades (<span id="contpro"></span> registros - <span id="subtotalpro"></span>)</h3>

            <table cellpadding="3" cellspacing="0" border="0" style="width: 40%;" class="pull-left">
              <thead>
                  <tr>
                      <th colspan="3"><h4>Filtros de busqueda</h4></th>
                  </tr>
              </thead>
              <tbody>
                  <tr id="filter_col3" class="justify">
                      <td>Valor oportunidad</td>
                      <td align="center"><input type="text" class="form-control input-sm dinero" placeholder="Valor minimo" id="min" name="min"></td>
                      <td align="center"><input type="text" class="form-control input-sm dinero" placeholder="Valor maximo" id="max" name="max"></td>
                  </tr>
                  <tr id="filter_col6" class="justify">
                      <td>Fecha identificación</td>
                      <td align="center"><input type="text" class="form-control input-sm date" placeholder="Fecha inicial" id="minIdenti" name="min"></td>
                      <td align="center"><input type="text" class="form-control input-sm date" placeholder="Fecha final" id="maxIdenti" name="max"></td>
                  </tr>
                  <tr id="filter_col7" class="justify">
                      <td>Fecha cierre</td>
                      <td align="center"><input type="text" class="form-control input-sm date" placeholder="Fecha inicial" id="minCierre" name="min"></td>
                      <td align="center"><input type="text" class="form-control input-sm date" placeholder="Fecha final" id="maxCierre" name="max"></td>
                  </tr>
              </tbody>
            </table>

            <table cellpadding="3" cellspacing="0" border="0" style="width: 40%;" class="pull-right">
              <thead>
                  <tr>
                      <th></th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  <tr id="filter_col1" class="justify" data-column="2">
                      <td>Empresa/Ciudad</td>
                      <td align="center"><input type="text" class="column_filter form-control input-sm" placeholder="Ver todo" id="col2_filter"></td>
                  </tr>
                  <tr id="filter_col2" class="justify" data-column="3">
                      <td>Productos</td>
                      <td align="center"><input type="text" class="column_filter form-control input-sm" placeholder="Ver todo" id="col3_filter"></td>
                  </tr>
                  <tr id="filter_col4" class="justify" data-column="5">
                      <td>Responsable</td>
                      <td align="center"><input type="text" class="column_filter form-control input-sm" placeholder="Ver todo" id="col5_filter"></td>
                  </tr>
                  <tr id="filter_col5" class="justify" data-column="6">
                      <td>Pais</td>
                      <td align="center"><input type="text" class="column_filter form-control input-sm" placeholder="Ver todo" id="col6_filter"></td>
                  </tr>
                  <tr id="filter_col8" class="justify" data-column="9">
                      <td>Ciclo de venta</td>
                      <td align="center"><input type="text" class="column_filter form-control input-sm" placeholder="Ver todo" id="col9_filter"></td>
                  </tr>
                  <tr id="filter_col9" class="justify" data-column="10">
                      <td>Probabilidad</td>
                      <td align="center"><input type="text" class="column_filter form-control input-sm" placeholder="Ver todo" id="col10_filter"></td>
                  </tr>
              </tbody>
            </table>


          <div class="table-responsive" style="padding-top: 30px;">            
            <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
              <thead>
                <tr>
                  <th class="centrado tamano-col">Item</th>
                  <th class="centrado tamano-col">Codigo</th>
                  <th class="centrado">Empresa<br>Ciudad</th>
                  <th class="centrado tamano-col">Productos</th>
                  <th class="centrado tamano-col">Valor Oportunidad</th>
                  <th class="centrado tamano-col" data-toggle="tooltip" data-placement="top" title="Responsable">Res</th>
                  <th class="centrado tamano-col">Pais</th>
                  <th class="centrado tamano-col">Identificada</th>
                  <th class="centrado tamano-col" style="min-width: 49px !important;">Por cerrar</th>
                  <th class="centrado tamano-col">Ciclo de Venta</th>
                  <th class="centrado tamano-col" data-toggle="tooltip" data-placement="top" title="Probabilidad">Prob</th>
                  <th class="centrado tamano-col" style="min-width: 104px !important;">Acciones</th>
                </tr>
              </thead>
              <tbody>
                 <?php $i=1; ?>
                @foreach($datos as $dato)
                <tr class="text-center dato">
                  <td class="centrar-vertical"><a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a"><p class="mas-pequeno">{{ $i++ }}</p></a></td>
                  <td class="centrar-vertical"><a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a"><p class="mas-pequeno">OP{{ $dato["oportunidad"]->id }}</p></a></td>

                  <td class="centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a"><p class="mas-pequeno">{{ $dato["oportunidad"]->empresa }} <br> {{ $dato["oportunidad"]->ciudad }}</p></a>
                  </td>

                                
                  <td class="unalinea centrar-vertical" style="text-align: center;">
                    <?php 
                      $productosRes = App\OportunidadProducto::where("oportunidad_id",$dato["oportunidad"]->id)->get();
                      $tootilProductos = 0;
                      $final = '';
                      $abreviado = '';
                      foreach ($productosRes as $res) { 
                        try { 
                          if (!empty($res->producto)) { 
                            if (is_numeric($res->producto)) {
                              $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                            }else{
                              $producto->name = "Otro";
                            }
                            if (is_numeric($res->referencia)) {
                              $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop(); 
                            }else{
                              $referencia->referencia = "Estandar";
                            }
                            $text_productos = $res->cantidad.' '.$producto->name.' '.$referencia->referencia;
                            $final.=$text_productos.'&#10;';
                            if(strlen($text_productos) > 17){
                                $text_productos_mostrar = substr($text_productos, 0, 15);
                                $text_productos_mostrar.=' ...';
                            }else{
                                $text_productos_mostrar = $text_productos;
                            }
                            $abreviado.=$text_productos_mostrar.', <br>';
                          } 
                        } catch (Exception $e) {} 
                      } 
                    ?>
                      <p class="mas-pequeno" data-toggle="tooltip" data-placement="top" title="<?=$final?>"><?=$abreviado?></p>
                  </td>

                  <td class="unalinea centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                      <p class="mas-pequeno">
                      <?php 
                        try {
                          $otro = $dato["historial"]->filter(function ($value, $key) {
                            return $value->key == "val_pesos";                            
                          });
                          $valpesosoportunidad = $otro->pop(); 
                          if(isset($valpesosoportunidad->value)){
                            echo "$ ".$valpesosoportunidad->value;
                          }else{
                            echo "$ 0";
                          }
                        } catch (Exception $e) { }
                      ?>
                      </p>
                    </a>
                  </td>

                  <td class="centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                    <p class="mas-pequeno">
                    <?php 
                      try {
                        $otro = $dato["historial"]->filter(function ($value, $key) {
                          return $value->key == "responsable";                            
                        });
                        $casi = $otro->pop(); 
                        if(isset($casi->value) && !empty($casi->value)){
                          $nombreU = App\User::find($casi->value);
                          if (isset($nombreU)) {
                            $nombre   = explode(" ",$nombreU->nombres);
                            $apellido = explode(" ",$nombreU->apellidos); 
                            echo $nombre[0] ."<br>". $apellido[0];
                          }else{
                            echo $casi->value;
                          }
                        } 
                      } catch (Exception $e) {
                        
                      }
                    ?>
                    </p>
                    </a>
                  </td>

                  <td class="centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a"><p class="mas-pequeno">{{ $dato["oportunidad"]->pais}}</p></a>
                  </td> 

                  <td class="centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                    <p class="mas-pequeno">
                    <?php 
                      $otro = $dato["historial"]->filter(function ($value, $key) {
                        return $value->key == "fecha_ff";                            
                      });
                      $casi = $otro->pop(); 
                      if (isset($casi->value)) {echo date($casi->value);} 
                    ?>
                    </p>
                    </a>
                  </td>
                  
                  <td class="centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                    <p class="mas-pequeno">
                    <?php 
                      $otro = $dato["historial"]->filter(function ($value, $key) {
                        return $value->key == "fecha_oc";                            
                      });
                      $casi = $otro->pop(); 
                      if (isset($casi->value)) {echo $casi->value;}
                    ?>
                    </p>
                    </a>
                  </td>                   

                  <td class="centrar-vertical">
                    <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a">
                    <p class="mas-pequeno">
                    <?php 
                      $otro = $dato["historial"]->filter(function ($value, $key) {
                        return $value->key == "ciclo_venta";                            
                      });
                      $casi = $otro->pop(); 
                      if (isset($casi->value)) {echo $casi->value." %";} 
                    ?>
                    </p>
                    </a>
                  </td>

                  <?php 
                    $otro = $dato["historial"]->filter(function ($value, $key) {
                      return $value->key == "probabilidad";                            
                    });
                    $casi = $otro->pop();
                  ?>

                  <td class="centrar-vertical" style="<?php if (isset($casi->value) && $casi->value=="Alta"){ echo 'background: #85b586;font-weight: bold;';}elseif (isset($casi->value) && $casi->value=="Media"){ echo 'background: #f1c87c;font-weight: bold;';}elseif (isset($casi->value) && $casi->value=="Baja"){ echo 'background: #e26b68;font-weight: bold;';} 
                              ?>">
                    @if(isset($dato['oportunidad']->id))
                     <a href="{{ url('oportunidad/'.$dato['oportunidad']->id) }}" target="_blank" class="normal_a" style="color: #fff" data-toggle="tooltip" data-placement="top" @if(isset($casi->value)) title="<?=$casi->value?>" @endif >
                     @if(isset($casi->value))
                      <?php if (isset($casi->value)) {echo substr($casi->value, 0, 1);} ?>
                     @endif
                    </a>
                    @endif
                  </td>
                  <td class="text-center centrar-vertical">
                    <a href="{{ url('/') }}/oportunidad/{{ $dato['oportunidad']->id }}" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver oportunidad">
                      <i class="fa fa-eye"></i>
                    </a>
                    @if($data['permiso_btn_ciclo'] == "Si")
                    <a href="{{ url('/') }}/acciones/{{ $dato['oportunidad']->id }}" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Gestionar ciclo de venta">
                      <i class="fa fa-sort-numeric-desc"></i>
                    </a>
                    @endif
                    <a href="{{ url('/') }}/oportunidad/edit/{{ $dato['oportunidad']->id }}" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar oportunidad">
                      <i class="fa fa-pencil"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          

        </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts') 
    <script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
    <script src="{{ url('/') }}/js/plugins/material.min.js"></script>
    <script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
    <script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
    <script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>
    <script src="{{ url('/') }}/js/jquery.maskMoney.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script> 
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">

      sumarTotales = function() {
        var suma = 0;
        var cont = 0;
        $('#example tr.dato').each(function(){
          cont++;
          var cadena = $(this).find('td').eq(4).text();
          cadena = cadena.replace("$", "");
          cadena = cadena.replace(/\./g, "");
          cadena = cadena.replace(/\,/g, "");
          cadena = cadena.replace(/ /g, "");
          suma += parseInt(cadena||0,10) 
        });
        suma = suma.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        suma = suma.split('').reverse().join('').replace(/^[\,]/,'');
        $('#subtotalpro').html("$ "+suma);
        $('#contpro').html(cont);
      }

      function filterColumn( i ) {
          $('#example').DataTable().column(i).search(
              $('#col'+i+'_filter').val()
          ).draw();
          sumarTotales();
      }

      

      $.fn.dataTable.ext.search.push(
          function( settings, data, dataIndex ) {
              var min = $('#min').val();
              var max = $('#max').val();              
              min = min.replace(/\,/g, "");
              max = max.replace(/\,/g, "");
              min = parseInt(min, 10 );
              max = parseInt(max, 10 );
              var age = data[4] || 0; // use data for the age column
              age = age.replace("$", "");
              age = age.replace(/\./g, "");
              age = age.replace(/\,/g, "");
              age = age.replace(/ /g, "");

              var minIdenti = $('#minIdenti').val();
              var maxIdenti = $('#maxIdenti').val();              
              var date = data[7] || 0; // use data for the age column
              date = moment(date).format("x");
              maxIdenti = moment(maxIdenti).format("x");
              minIdenti = moment(minIdenti).format("x");

              var minCierre = $('#minCierre').val();
              var maxCierre = $('#maxCierre').val();              
              var dateCierre = data[8] || 0; 
              //console.log("minCierre=> ", minCierre, "maxCierre=> ",maxCierre, "dateCierre=> ", dateCierre);
              dateCierre = moment(dateCierre).format("x");
              maxCierre = moment(maxCierre).format("x");
              minCierre = moment(minCierre).format("x");
              //console.log("minCierre=> ", minCierre, "maxCierre=> ",maxCierre, "dateCierre=> ", dateCierre);

              var retornado;
              if ( ( isNaN( min ) && isNaN( max ) ) || ( isNaN( min ) && age <= max ) || ( min <= age   && isNaN( max ) ) || ( min <= age   && age <= max ) ){
                  retornado = true;
              }else{
                  retornado = false;
              }  
              
              if ((retornado === true) && ((isNaN(maxIdenti)) || (isNaN(minIdenti) && date <= maxIdenti) || (minIdenti <= date   && isNaN(maxIdenti)) || (minIdenti <= date   && date <= maxIdenti))){
                  retornado = true;
              }else{
                retornado = false;
              }

              if ((retornado === true) && ((isNaN(maxCierre)) || (isNaN(minCierre) && dateCierre <= maxCierre) || (minCierre <= dateCierre   && isNaN(maxCierre)) || (minCierre <= dateCierre   && dateCierre <= maxCierre))){
                  retornado = true;
              }else{
                retornado = false;
              }          
              return retornado;
          }
          
      );

      
      $(function () {
        $('.date').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            nowButton: true,
            lang: 'es',
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar'
        });

        $(".dinero").maskMoney();
        $('#responsable').select2({
          placeholder: "Seleccione una responsable",
          ciudad: true,
          tokenSeparators: [','],
          ajax: {
            dataType: 'json',
            url: '{{ url("vendedores") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function(data, page) {
              return {
                results: data
              };
            },
          }
        });

        $( "#responsable" ).change(function () {
          $( "#formFilter" ).submit();  
        });

        $("[data-toggle='tooltip']").tooltip();

        $('#productos').select2({
          placeholder: "Filtrar por productos",
          productos: true
        });

        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                <?php if($data['permiso_exportar'] == "Si"){ ?>
                'excelHtml5',
                'csvHtml5',
                <?php } ?>
            ],
          language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ oportunidades",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ oportunidades",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 oportunidades",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          },
          order: [ [8,'asc'] ],
          bPaginate: false,
        });

        var t = $('#example').DataTable();
     
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        

        $('input.column_filter').on( 'keyup click', function () {
          filterColumn( $(this).parents('tr').attr('data-column') );
        });

        var table = $('#example').DataTable();
     
        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max, #minIdenti, #maxIdenti, #minCierre, #maxCierre').on( 'keyup click change', function() {
            table.draw();
            sumarTotales();
        });

        
        sumarTotales();
      });
    </script>
@endsection
