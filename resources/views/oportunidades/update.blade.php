@extends('template.app')

@section('title', 'Editar oportunidad')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }  

    .custom-radio {
        font-weight: bold !important;
        font-size: small !important;
    } 

    input#subtotalpro {
        height: 60px;
        font-size: 40px;
        text-align: center;
        font-weight: 700;
    }
</style>

    <div class="card animated flipInX" style="margin-bottom: 30px;">
        <div class="card-block">
            <form method="POST" action="{{ url('editaroportunidad') }}" >
                <input type="hidden" name="id" value="{{ $oportunidad->id }}">
                <input type="hidden" id="comentariomodificacion" name="comentario_modificacion" value="">
                <div class="panel-title">
                    <div class="pull-right">
                        <div class="btn-group">
                            <a href="{{ url('crearempresa') }}" class="btn btn-primary btn-sm" target="popup" onClick="window.open(this.href, this.target, 'width=1050,height=920'); return false;">
                                <i class="fa fa-plus" aria-hidden="true"></i> Añadir Empresa
                            </a>
                            <a href="{{ url('crearcliente') }}" class="btn btn-success btn-sm" target="popup" onClick="window.open(this.href, this.target, 'width=1050,height=920'); return false;">
                                <i class="fa fa-plus" aria-hidden="true"></i> Añadir Cliente
                            </a>
                        </div>
                    </div>
                    <h2>Editar oportunidad</h2>
                </div>
                <hr>
                <!-- Create Post Form -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- Fin Create Post Form -->
                {{ csrf_field() }}
                <div id="output"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label for="productos" class="col-form-label">Producto/s</label> 
                        </div>
                        <div class="col-2">
                            <label for="productos" class="col-form-label">Referencia</label>
                        </div> 
                        <div class="col-1">
                            <label for="productos" class="col-form-label">Cantidad</label>
                        </div> 
                        <div class="col-2">
                            <label for="productos" class="col-form-label">Valor unitario</label>
                        </div> 
                        <div class="col-2">
                            <label for="productos" class="col-form-label">Valor total</label>
                        </div> 
                    </div>                   
                </div> 

                <div class="form-group productos-anteriores" id="listProductos">
                    <div class="row">
                        <div class="col-5">
                            <select id="selectProducto0" name="productos[0][producto]" class="form-control productos0" onchange="buscarDimSelec2(0)"></select> 
                        </div>
                        <div class="col-2">
                            <select id="referencia0" name="productos[0][referencia]" class="form-control referencia0"></select> 
                        </div> 
                        <div class="col-1">
                            <input type="text" id="textCantidad0" class="col-12 form-control" name="productos[0][cantidad]" onkeyup="calcularValorTotal(0)" placeholder="0">
                        </div> 
                        <div class="col-2">
                            <input type="text" id="textValor0" class="col-12 form-control dinero" name="productos[0][valor]" onkeyup="calcularValorTotal(0)" placeholder="$0">
                        </div> 
                        <div class="col-2">
                            <div class="input-group">
                                <input type="text" id="numberValor0" class="col-12 form-control sumarTotales" name="productos[0][total]" placeholder="$0" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarProductos()"><i class="fa fa-plus text-success"></i></button>
                                </span>
                            </div>
                        </div> 
                    </div>                   
                </div>
                    <?php 
                        $productosRes = App\OportunidadProducto::where("oportunidad_id",$oportunidad->id)->get();
                           
                        $i = 0;
                     
                        foreach ($productosRes as $res) { 
                            try {                                
                                $i++;
                                if (!empty($res->producto)) { 
                                  if (is_numeric($res->producto)) {
                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                                  }else{
                                    $producto->name = "Otro";
                                  }
                                  if (is_numeric($res->referencia)) {
                                    $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop(); 
                                  }else{
                                    $referencia->referencia = "Estandar";
                                  }
                                } 
                        ?>
                                    <div class="form-group productos-anteriores">
                                    <div class="row animated rollIn" id="listAddProductos{{ $i }}">
                                        <div class="col-5">
                                            <select id="selectProducto{{ $i }}" name="productos[{{ $i }}][producto]" class="form-control productos0" onchange="buscarDimSelec2({{ $i }})" required>
                                                <option value="{{ $res->producto }}">{{ $producto->name }}</option>
                                            </select> 
                                        </div>
                                        <div class="col-2">
                                            <select id="referencia{{ $i }}" name="productos[{{ $i }}][referencia]" class="form-control referencia{{ $i }}">
                                                <option value="{{ $res->referencia }}">{{ $referencia->referencia }}</option>
                                            </select> 
                                        </div> 
                                        <div class="col-1">
                                            <input type="text" id="textCantidad{{ $i }}" class="col-12 form-control" name="productos[{{ $i }}][cantidad]" onkeyup="calcularValorTotal({{ $i }})" placeholder="0" value="<?php if (isset($res->cantidad) && !empty($res->cantidad)) { echo $res->cantidad; } ?>">
                                        </div> 
                                        <div class="col-2">
                                            <input type="text" id="textValor{{ $i }}" class="col-12 form-control dinero" name="productos[{{ $i }}][valor]" onkeyup="calcularValorTotal({{ $i }})" placeholder="$0" value="<?php if (isset($res->valor) && !empty($res->valor)) { echo $res->valor; } ?>">
                                        </div> 
                                        <div class="col-2">
                                            <div class="input-group">
                                                <input type="text" id="numberValor{{ $i }}" class="col-12 form-control sumarTotales" name="productos[{{ $i }}][total]" placeholder="$0" value="<?php if (isset($res->total) && !empty($res->total)) { echo $res->total; } ?>" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarProductos('#listAddProductos{{ $i }}')"><i class="fa fa-minus text-danger"></i></button>
                                                </span>
                                            </div>
                                        </div> 
                                    </div> 
                                    </div>  
                                    <?php
                            } catch (Exception $e) { 
                            } 
                        }
                    ?>  

                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                        </div>
                        <div class="col-2">
                            <h2>Total</h2>
                        </div> 
                        <div class="col-4">
                            <input type="text" id="subtotalpro" class="col-12 form-control" placeholder="$0" readonly>
                        </div> 
                    </div>                   
                </div>
                <div class="form-group">
                    <?php 
                        $otro = $datos->filter(function ($value, $key) {
                          return $value->key == "observaciones";                            
                        });
                        $casi = $otro->pop(); 
                    ?>
                    <label for="observaciones" class="col-form-label">Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="2" placeholder="Por favor ingrese una  observacion o Detalle"><?php if (isset($casi->value)) {echo $casi->value;} ?></textarea>
                </div>

                <div class="form-group row">
                    <div class="col-3">
                        <label for="empresa" class="col-form-label">Empresa</label>
                        <select name="empresa" class="form-control" id="empresa" required>
                            <option value="{{ $empresa->id }}">{{ $empresa->nombre }}</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "sede";                            
                            });
                            $casi = $otro->pop(); 
                            $sede = App\EmpresaSedes::findOrFail($casi->value); 
                        ?>
                        <label for="sede" class="col-form-label">Sede</label>
                        <select name="sede" class="form-control" id="sede" required>
                            <option value="{{ $sede->id }}">{{ $empresa->nombre."/".$sede->ciudad }}</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <label for="cliente" class="col-form-label">Cliente</label>
                        <select name="cliente" class="form-control" id="cliente" required>
                        <?php if (isset($cliente) && !empty($cliente)) {?>
                            <option value="{{ $cliente->id }}">{{ $cliente->cliente }}</option>
                        <?php } ?>
                        </select>
                    </div>                    
                    <div class="col-2">
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "probabilidad";                            
                            });
                            $casi = $otro->pop(); 
                        ?>
                        <label for="probabilidad" class="col-form-label">Probabilidad</label>
                        <select name="probabilidad" class="form-control form-control-sm" id="probabilidad" required>
                            <option value="">Seleccione una opción</option>
                            <option value="Alta" data-image="https://placeholdit.imgix.net/~text?txtsize=33&bg=3c763d&w=350&h=150" <?php if ($casi->value === "Alta"){echo "selected"; } ?>>Alta</option>
                            <option value="Media" data-image="https://placeholdit.imgix.net/~text?txtsize=33&bg=f0ad4e&w=350&h=150" <?php if ($casi->value === "Media"){echo "selected"; } ?>>Media</option>
                            <option value="Baja" data-image="https://placeholdit.imgix.net/~text?txtsize=33&bg=a94442&w=350&h=150" <?php if ($casi->value === "Baja"){echo "selected"; } ?>>Baja</option>
                        </select>
                    </div> 
                    <div class="col-2"> 
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "margen";                            
                            });
                            $casi = $otro->pop(); 
                        ?>              
                        <label for="margen" class="col-form-label">Margen de contribucion</label>
                        <div class="input-group">
                            <input class="form-control form-control-sm" type="number" name="margen" min="0" max="100" placeholder="0" id="margen" value="{{ $casi->value }}" required>
                            <span class="input-group-addon form-control-sm">%</span>
                        </div> 
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-3">
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "valor_oportunidad";                            
                            });
                            $casi = $otro->pop(); 
                        ?>
                        <label for="valor_oportunidad" class="col-form-label">Valor Oportunidad</label>
                        <input class="form-control form-control-sm" type="text" name="valor_oportunidad" id="valor_oportunidad" placeholder="Por favor ingrese un valor de oportunidad" value="<?php if(isset($casi->value)){ echo $casi->value; }?>" required readonly>
                    </div>
                    <div class="col-2">
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "moneda";                            
                            });
                            $casi = $otro->pop(); 
                        ?>
                        <label for="moneda" class="col-form-label">Moneda</label>
                        <select class="form-control" name="moneda" id="selec_moneda" required>
                            <option value="EUR" <?php if ($casi->value === "EUR"){echo "selected"; } ?>>Euro (€)</option>
                            <option value="COP" <?php if ($casi->value === "COP"){echo "selected"; } ?>>Colombia Peso (COP)</option>
                            <option value="USD" <?php if ($casi->value === "USD"){echo "selected"; } ?>>US Dollar (USD)</option>
                        </select>  
                    </div>
                    <div class="col-3">
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "tasa_cambio";                            
                            });
                            $casi = $otro->pop(); 
                            if (isset($casi->value) && !empty($casi->value)) {
                                $cantidad = str_replace(',','',$casi->value);
                                $cantidad = (int)$cantidad;
                                $cantidad = number_format($cantidad);
                            }else{
                                $cantidad = 0;
                            }
                            
                        ?>
                        <label class="col-form-label">Tasa de cambio</label>
                        <input class="form-control form-control-sm dinero" type="text" id="tasa_cambio" name="tasa_cambio" value="{{ $cantidad }}" placeholder="Por favor ingrese un valor de tasa de cambio" required>
                    </div>
                    <div class="col-4">
                        <?php 
                            $otro = $datos->filter(function ($value, $key) {
                              return $value->key == "val_pesos";                            
                            });
                            $casi = $otro->pop(); 
                        ?>
                        <label for="val_pesos" class="col-form-label">Valor en pesos</label>                              
                        <div class="input-group">                            
                            <span class="input-group-addon form-control-sm">$</span>
                            <input class="form-control dinero form-control-sm" type="text" name="val_pesos" id="val_pesos" placeholder="0" value="<? if(isset($casi->value)){ echo $casi->value; }else{ echo '0'; } ?>" required readonly>
                        </div> 
                    </div>
                </div>                




<!-- datos de identificacion -->
                <div class="card" style="margin-bottom: 30px;">
                    <h5 class="card-header"> DATOS DE IDENTIFICACIÓN </h5>
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-3">
                                <?php
                                    $otro = $datos->filter(function ($value, $key) {
                                      return $value->key == "identifico";
                                    });
                                    $casi = $otro->pop();
                                    if (isset($casi->value)) {
                                        $nombre = App\User::find($casi->value);
                                    }
                                ?>
                                <label for="identifico" class="col-form-label">Identifico</label>
                                <select name="identifico" class="form-control involucrado" required>
                                    <?php if (isset($nombre)): ?>
                                        <option value="{{ $nombre->id }}">{{ $nombre->name }}</option>
                                    <?php endif ?>
                                </select>
                            </div>
                            <div class="col-3">
                                <?php
                                    $otro = $datos->filter(function ($value, $key) {
                                      return $value->key == "responsable";
                                    });
                                    $casi = $otro->pop();
                                    if (isset($casi->value)) {
                                        $nombre = App\User::find($casi->value);
                                    }
                                ?>
                                <label for="responsable" class="col-form-label">Responsable</label>
                                <select name="responsable" class="form-control involucrado" required>
                                    <?php if (isset($nombre)): ?>
                                        <option value="{{ $nombre->id }}">{{ $nombre->name }}</option>
                                    <?php endif ?>
                                </select>
                            </div>
                            <div class="col-3">
                                <?php
                                    $otro = $datos->filter(function ($value, $key) {
                                      return $value->key == "fecha_ff";
                                    });
                                    $casi = $otro->pop();
                                ?>
                                <label for="fecha_ff" class="col-form-label">Fecha Identificacion</label>
                                <input class="form-control form-control-sm datepicker" type="text" name="fecha_ff" id="fecha_ff" placeholder="Por favor ingrese la fecha" value="{{ $casi->value }}" required>
                            </div>
                            <div class="col-3">
                                <?php
                                    $otro = $datos->filter(function ($value, $key) {
                                      return $value->key == "fecha_oc";
                                    });
                                    $casi = $otro->pop();
                                ?>
                                <label for="fecha_oc" class="col-form-label">Fecha Cierre</label>
                                <input class="form-control form-control-sm datepicker" type="text" name="fecha_oc" id="fecha_oc" placeholder="Por favor ingrese la fecha" value="<?php if(isset($casi->value) && $casi->value != 'Invalid date'){ echo $casi->value; } ?>" required>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin datos de identificacion -->

                <!-- forma de pago -->
                <div class="card" style="margin-bottom: 30px;">
                    <h5 class="card-header"> FORMA DE PAGO </h5>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-3 form-group">
                                <label class="custom-control custom-radio">
                                    <input id="radio1" name="radio" type="radio" class="custom-control-input fpago" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Pago a cuotas</span>
                                </label>
                                <label class="custom-control custom-radio">
                                  <?php
                                        $otro = $datos->filter(function ($value, $key) {
                                          return $value->key == "otra_forma_pago";
                                        })->pop();
                                  ?>
                                  <input id="radio2" name="radio" type="radio" class="custom-control-input fpago" value="{{ $otro }}">
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description">Otra forma de pago</span>
                                </label>
                            </div>



                            <div class="col-2 form-group row ocultar_cuotas mostar_anticipo">
                               <?php
                                    $formpag = App\OportunidadesFormapago::where('oportunidad_id','=', $oportunidad->id)->get();
                                    $otro = $formpag->filter(function ($value, $key) {
                                      return $value->key == "anticipo";
                                    })->pop();
                                  ?>
                                <label for="forma_pago" class="col-form-label">Anticipo de</label>
                                 <div class="input-group">
                                    <input class="form-control form-control-sm" type="number" id="input_anticipo" name="forma_pago[0][anticipo]" min="0" max="100" placeholder="0" @if(!empty($otro->value)) value="{{ $otro->value }}" @endif>
                                    <span class="input-group-addon form-control-sm">%</span>
                                </div>

                            </div>
                            <div class="col-2 ocultar_cuotas">
                                <label for="forma_pago" class="col-form-label">Cantidad de cuotas</label>
                                <input class="form-control form-control-sm" type="number" id="num_cuotas" min="0" placeholder="0" onkeyup="comprobarCuotas()" @if(!empty($otro->value)) value="{{ count($formpag)-1 }}" @endif>
                            </div>
                            <div class="col-3 ocultar_cuotas">
                                <button type="button" class="btn btn-success btn-sm" style="margin-top: 25px !important;" onclick="añadircuotas()"><i class="fa fa-plus"></i> Añadir cuotas</button>
                            </div>
                        </div>

                        <div class="form-group" id="divcual" style="display: none;">
                            <label for="otra_forma_pago" class="col-form-label">Otro cual?</label>
                            <textarea class="form-control" name="otra_forma_pago" id="otra_forma_pago" rows="2" disabled placeholder="Por favor ingrese una  forma de pago"></textarea>
                        </div>
                    </div>
                </div>

                 <!-- forma de pago -->

                <div class="row" id="cuotas">
                             <?php
                                $i=0;
                                $html = "";
                                if(isset($formpag) && !empty($formpag)){

                                foreach ($formpag as $key => $value) {
                                    if($value->key != "anticipo"){
                                        $i++;
                                        $html .= '

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <label for="forma_pago " class="col-2 col-form-label">Cuota numero '.$i.'</label>
                                                    <div class="input-group col-2 form-control-sm cuotas">
                                                        <input class="form-control form-control-sm" type="number" name="forma_pago['.$i.'][cuota_valor]" min="0" max="100" placeholder="0" value="'.$value->value.'">
                                                        <span class="input-group-addon form-control-sm">%</span>
                                                    </div>
                                                    <div class="col-8 form-control-sm cuotas-text">
                                                        <textarea class="form-control form-control-sm" name="forma_pago['.$i.'][cuota_texto]" rows="1" placeholder="Por favor ingrese un detalle de cuota">'.$value->texto.'</textarea>
                                                    </div>
                                                    <input type="hidden" name="forma_pago['.$i.'][cuota_numero]" value="'.$i.'">
                                                </div>
                                            </div>';
                                    }
                                }
                                echo($html);
                                }
                            ?>
                        </div>

                <a class="btn btn-essi pull-right btn-submit" style="color: white;" onclick="editarOportunidad()">
                    <i class="fa fa-pencil" aria-hidden="true"></i> Editar
                </a>
                <button type="submit" id="btn_editar" style="display: none;" class="btn btn-essi pull-right btn-submit">
                    Editar
                </button>
            </form>
        </div>
    </div>
@endsection


@section('scripts')
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="{{ url('/') }}/js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script type="text/javascript">
    function editarOportunidad(){
        swal({
          title: 'Escriba el por que realiza la modificación',
          html:
            `<textarea class="mt-4 form-control" id="comentario_cambio" rows="8"></textarea>`+
            `<div class="bg-warning" id="caracteres_comentario"><h4 class="py-2" >0 de 250 Caracteres minimos</h4></div>`
        }).then(function () {
                $('#comentariomodificacion').val($('#comentario_cambio').val());
                $('#btn_editar').click();
            });
    }

    $(document).on('keyup','#comentario_cambio',function(){
       caracteres = $(this) .val().length;
        $('#caracteres_comentario h4').html(caracteres+' de 250 Caracteres minimos');
        if(caracteres >= 250){
            $('#caracteres_comentario').removeClass('bg-warning').addClass('text-white').addClass('bg-success');
        }else{
            $('#caracteres_comentario').removeClass('bg-success').addClass('bg-warning').removeClass('text-white');
        }
    });

    $('.btn-submit').click(function(e){
      if(true === $("form").parsley().validate()){        
        var element = $(this).parent().parent().parent().parent().parent();
      }else{
        e.preventDefault();
        var element = $(this).parent().parent().parent().parent().parent();
        $(element).addClass('animated rubberBand');  
        $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
          $(element).removeClass('animated rubberBand');
        });

        $("#output").removeClass(' alert alert-success');
        $("#output").addClass("alert alert-danger animated flip").html("Perdón!, Es necesario que complete todos los campos");
        $("#output").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
          $("#output").removeClass('animated flip');
        });
      }
    });

    i={{ $i }};

    /*fun_moneda = function() {
        $("#val_pesos").val("Cargando...");
        var val = $("#valor_oportunidad").val();
        if ($("#selec_moneda").val() != "COP") {            
            val = parseFloat(val.replace(/,/g, ''));
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
            var jqxhr = $.ajax({ 
                url: "{{ url('/') }}/cambiarmoneda", 
                data: {
                    _token: CSRF_TOKEN,
                    cantidad:val,
                    moneda_origen:$("#selec_moneda").val(),
                    moneda_destino:"COP"
                },
                cache: false,
                type: 'POST',
                success: function(e){  
                    e = parseInt(e);
                    var num = e;
                    if(!isNaN(num)){
                    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
                    num = num.split('').reverse().join('').replace(/^[\.]/,'');
                    e = num;
                    }
                    $("#val_pesos").val(e);
                }
            });
        }else{
            $("#val_pesos").val(val);
        }
    }*/

    mostarValorTotal = function () {
        var valSumaGeneral = 0;
        $(".sumarTotales").each(function() {
            var valSuma = verificar(this.id);
            valSumaGeneral = parseInt(valSumaGeneral) + parseInt(valSuma);                       
        });

        TotalhHombre = parseInt(valSumaGeneral);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        document.getElementById("subtotalpro").value = TotalhHombre;
        document.getElementById("valor_oportunidad").value = TotalhHombre;

        funtasaCalculo();
    }

    calcularValorTotal = function (val) {
        var cantidad=verificar("textCantidad"+val);
        var valor=verificar("textValor"+val);
        texValTotal = parseInt(valor*cantidad);
        
        if(!isNaN(texValTotal)){
            texValTotal = texValTotal.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            texValTotal = texValTotal.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("numberValor"+val).value = texValTotal;              
        }  

        mostarValorTotal();     
    }    

    añadircuotas = function () {
        $(".ocultar_cuotas").hide('slow/400/fast');
        $(".mostar_anticipo").show('slow');
        $("#cuotas").html(" ");
        var num = $("#num_cuotas").val();
        for (var j = 1; j <= num; j++) {
            cuotas=`
                <div class="col-12">
                    <div class="form-group row">
                        <label for="forma_pago " class="col-2 col-form-label">Cuota numero `+j+`</label>
                        <div class="input-group col-2 form-control-sm cuotas">
                            <input class="form-control form-control-sm" type="number" name="forma_pago[`+j+`][cuota_valor]" min="0" max="100" placeholder="0">
                            <span class="input-group-addon form-control-sm">%</span>
                        </div>
                        <div class="col-8 form-control-sm cuotas-text">
                            <textarea class="form-control form-control-sm" name="forma_pago[`+j+`][cuota_texto]" rows="1" placeholder="Por favor ingrese un detalle de cuota"></textarea>
                        </div>
                        <input type="hidden" name="forma_pago[`+j+`][cuota_numero]" value="`+j+`">
                    </div>
                </div>`;
            $("#cuotas").append(cuotas);
        }
    }

    funtasaCalculo = function() {
        if ($('#selec_moneda').val() === "COP") {
            $('#tasa_cambio').val(1);
            $("#val_pesos").val($("#valor_oportunidad").val());
            $('#tasa_cambio').prop('readonly', true);            
        }else{
            $('#tasa_cambio').prop('readonly', false);
        }

        cambio  = $("#tasa_cambio").val();
        valor   = $("#valor_oportunidad").val();
        cambio  = cambio.replace(/,/g, '');
        valor   = valor.replace(/,/g, '');
        total   = valor*cambio;
        if(!isNaN(total)){
            total = total.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            total = total.split('').reverse().join('').replace(/^[\,]/,'');
            $("#val_pesos").val(total);             
        }
    }

    $(function () {
            

        $(".fpago").change(function(){ // bind a function to the change event
            if( $("#radio2").is(":checked") ){   // check if the radio is checked
                $("#divcual").show('slow'); // retrieve the value
                $("#otra_forma_pago").prop('disabled', false);
                $("#input_anticipo").prop('disabled', true);
                $(".ocultar_cuotas").hide('slow/400/fast');
                $("#cuotas").hide('slow/400/fast');
                $( ".cuotas" ).children("input").attr('disabled', true);
                $( ".cuotas-text" ).children("textarea").attr('disabled', true);
            } else {
                $("#divcual").hide('slow/400/fast');
                $("#input_anticipo").prop('disabled', false);
                $("#otra_forma_pago").prop('disabled', true);
                $(".ocultar_cuotas").show('slow');
                $("#cuotas").show('slow');
                $( ".cuotas" ).children("input").attr('disabled', false);
                $( ".cuotas-text" ).children("textarea").attr('disabled', false);
            }
        });

        setTimeout(function(){ 
            mostarValorTotal();
            
        }, 3000); 
        
        /*$( "body" ).dblclick(function(e) {
            var target = $(e.target);
            if($(target).prop("disabled")){
                $(target).prop("disabled", false);
            }else{
                $(target).prop("disabled", true);
            }
          console.log( "Hello World!");
        });*/

        $('body').on('change','#selec_moneda', function() {
            if ($(this).val() === "COP") {
                $('#tasa_cambio').val(1);
                $("#val_pesos").val($("#valor_oportunidad").val());
                $('#tasa_cambio').prop('readonly', true);            
            }else{
                $('#tasa_cambio').prop('readonly', false);
            }
        });

        $('body').on('change','#tasa_cambio', function() {
            cambio  = $(this).val();
            valor   = $("#valor_oportunidad").val();
            cambio  = cambio.replace(/,/g, '');
            valor   = valor.replace(/,/g, '');
            total   = valor*cambio;
            if(!isNaN(total)){
                total = total.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                total = total.split('').reverse().join('').replace(/^[\,]/,'');
                $("#val_pesos").val(total);             
            }
        });

        $("#valor_oportunidad").maskMoney();
        $(".dinero").maskMoney();

        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            lang: 'es'
        });

        $("#margen").change(function () {
            margen = $(this).val();
            if (margen > 100) {
                swal({
                  title: 'Error!',
                  text: 'No es posible ingresar un margen superior al 100%.',
                  timer: 2000
                }).then(function () {
                    $(this).val("");
                });
            }
        });
        $("#fecha_ff").change(function () {
            fecha = $(this).val();
            fecha2 = "{{ Date('Y-m-d') }}";
            if (fecha > fecha2) {
                swal({
                  title: 'Error!',
                  text: 'No es posible identificar una oportunidad en un día superior al de hoy.',
                  timer: 2000
                }).then(function () {
                    $("#fecha_ff").val("");
                });
            }
        });
        $("#fecha_oc").change(function () {
            fecha = $(this).val();
            fecha2 = $("#fecha_ff").val();
            if (fecha <= fecha2) {
                swal({
                  title: 'Error!',
                  text: 'No es posible cerrar una oportunidad en un día inferior o igual al que se identifico la oportunidad.',
                  timer: 2000
                }).then(function () {
                    $("#fecha_oc").val("");
                });
            }
        });

        $(".denero").maskMoney();
        $('#empresa').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una empresa",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("empresa") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });


        $('#sede').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una sede",
            sede: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sede") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#empresa').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#cliente').select2({
            // Activamos la opcion "Tags" del plugin
             placeholder: "Seleccione una cliente",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("cliente") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        dato:$("#sede").val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });


        $('.involucrado').select2({
            placeholder: "Seleccione una persona",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("vendedores") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('.productos0').select2({
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });

        $("#ciclo_venta").select2({
            templateResult: addUserPic,
            templateSelection: addUserPic
        });

        $("#probabilidad").select2({
            templateResult: addUserPic,
            templateSelection: addUserPic
        });
    
        function addUserPic (opt) {
            if (!opt.id) {
                return opt.text;
            }               
            var optimage = $(opt.element).data('image'); 
            if(!optimage){
                return opt.text;
            } else {
                var $opt = $(
                '<span><img src="' + optimage + '" style="width: 30px;" /> ' + $(opt.element).text() + '</span>'
                );
                return $opt;
            }
        };

        $(".fpago").change(function(){ // bind a function to the change event
            if( $("#radio2").is(":checked") ){   // check if the radio is checked
                $("#divcual").show('slow'); // retrieve the value
                $("#otra_forma_pago").prop('disabled', false);
                $("#input_anticipo").prop('disabled', true);
                $(".ocultar_cuotas").hide('slow/400/fast');
                $("#cuotas").hide('slow/400/fast');
                $( ".cuotas" ).children("input").attr('disabled', true);
                $( ".cuotas-text" ).children("textarea").attr('disabled', true);
            } else {
                $("#divcual").hide('slow/400/fast');
                $("#input_anticipo").prop('disabled', false);
                $("#otra_forma_pago").prop('disabled', true);
                $(".ocultar_cuotas").show('slow');
                $("#cuotas").show('slow');
                $( ".cuotas" ).children("input").attr('disabled', false);
                $( ".cuotas-text" ).children("textarea").attr('disabled', false);
            }
        });
    });
    
    quitarProductos = function (id) {
      $(id).removeClass('rollIn').addClass('hinge');
      setTimeout(function(){ $(id).remove(); mostarValorTotal(); }, 1000);
    }
    
    i = $('.productos-anteriores').length;
    agregarProductos = function () {
      dato = `
        <div class="row animated rollIn" id="listAddProductos`+i+`">
            <div class="col-5">
                <select id="selectProducto`+i+`" name="productos[`+i+`][producto]" class="form-control productos0" onchange="buscarDimSelec2(`+i+`)" required></select>
            </div>
            <div class="col-2">
                <select id="referencia`+i+`" name="productos[`+i+`][referencia]" class="form-control"></select> 
            </div> 
            <div class="col-1">
                <input type="text" id="textCantidad`+i+`" class="col-12 form-control" name="productos[`+i+`][cantidad]" onkeyup="calcularValorTotal(`+i+`)" placeholder="0">
            </div> 
            <div class="col-2">
                <input type="text" id="textValor`+i+`" class="col-12 form-control dinero" name="productos[`+i+`][valor]" onkeyup="calcularValorTotal(`+i+`)" placeholder="$0">
            </div> 
            <div class="col-2">
                <div class="input-group">
                    <input type="text" id="numberValor`+i+`" class="col-12 form-control sumarTotales" name="productos[`+i+`][total]" placeholder="$0" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarProductos('#listAddProductos`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                    </span>
                </div>
            </div> 
        </div>`;
        $( "#listProductos" ).prepend(dato);
        $('.productos0').select2({
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });
        
        $(".dinero").maskMoney();
        i++;
    }


    buscarDimSelec2 = function (i) {
        console.log("entro aqui"+0);
        var ref = $.get( "{{ url('/') }}/referencia?term=&dato="+$("#selectProducto"+i).val());
        ref.done(function( data ) {
            console.log(data);
            $('#referencia'+i).html(" ");
            $.each(data, function (k, item) {
                $('#referencia'+i).append($('<option>', { 
                    value: item.id,
                    text : item.text 
                }));
            });
        });
    }


    verificar = function(id){
        var obj=document.getElementById(id);        
        if(!obj.value){
            value="0";
        }else{
            value=obj.value;
            value = value.replace(/,/g, "");
        }

        if(validate_importe(value,1))
        {
            // marcamos como erroneo
            obj.style.borderColor="#808080";
            return value;
        }else{
            // marcamos como erroneo
            obj.style.borderColor="#f00";
            return 0;
        }        
    }

    validate_importe = function(value,decimal){
        if(decimal==undefined)
            decimal=0;
 
        if(decimal==1)
        {
            // Permite decimales tanto por . como por ,
            var patron=new RegExp("^[0-9]+((,|\.)[0-9]{1,2})?$");
        }else{
            // Numero entero normal
            var patron=new RegExp("^([0-9])*$")
        }
 
        if(value && value.search(patron)==0)
        {
            return true;
        }
        return false;
    }

    añadircuotas = function () {
        $(".ocultar_cuotas").hide('slow/400/fast');
        $(".mostar_anticipo").show('slow');
        $("#cuotas").html(" ");
        var num = $("#num_cuotas").val();
        for (var j = 1; j <= num; j++) {
            cuotas=`
                <div class="col-12">
                    <div class="form-group row">
                        <label for="forma_pago " class="col-2 col-form-label">Cuota numero `+j+`</label>
                        <div class="input-group col-2 form-control-sm cuotas">
                            <input class="form-control form-control-sm" type="number" name="forma_pago[`+j+`][cuota_valor]" min="0" max="100" placeholder="0">
                            <span class="input-group-addon form-control-sm">%</span>
                        </div>
                        <div class="col-8 form-control-sm cuotas-text">
                            <textarea class="form-control form-control-sm" name="forma_pago[`+j+`][cuota_texto]" rows="1" placeholder="Por favor ingrese un detalle de cuota"></textarea>
                        </div>
                        <input type="hidden" name="forma_pago[`+j+`][cuota_numero]" value="`+j+`">
                    </div>
                </div>`;
            $("#cuotas").append(cuotas);
        }
    }

    
</script>
@endsection
