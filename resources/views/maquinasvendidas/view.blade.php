<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')
@section('title', 'Ver maquina vendida')
<!--@section('sidebar')
    @parent    
    @endsection-->
@section('content') 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="{{ url('/') }}/MDB/MDB Free/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="{{ url('/') }}/MDB/MDB Free/css/style.css" rel="stylesheet">
<style type="text/css">
  .btn.btn-sm {
    padding: .5rem 0.6rem;
    margin: 0px;
  }

  .titulo{
    font-weight: bold;
    color: #051d60;
    font-size: 2.5rem;
  }

  .titulo1{
    font-weight: bold;
    color: #051d60;
    font-size: 1.5rem;
  }

  .subtitulo{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: small;
    color: #54575a;
  }

  .subtitulo2{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: 14px;
    color: #051d60;
    font-weight: bold;
  }

  .subtitulo3{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: 14px;
    color: #54575a;
    font-weight: bold;
  }

  .hijo{
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
    margin-bottom: auto;
    width: 50%;
    text-align: left;    
  }

  .hijo2{
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
    margin-bottom: auto;
    width: 80%;
    text-align: left;    
  }

  .tex-hijo{
    display:inline-block;
    vertical-align:middle;
    line-height:normal;
  }

  .alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -ms-border-radius: 2px;
    -o-border-radius: 2px;
    border-radius: 0px;
    border: solid 1px rgba(0, 0, 0, 0.09);
  }

  .list-group-item{
    padding-top: 5px;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }

  .panel-view{
    -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    margin-top: 20px;
    margin-bottom: 20px;
    background-color: #fff;
    width: 80%;
  }

  .observaciones-img{
    width: 60px;
  }

  .observaciones-p{
    font-size: small;
  }

  .observaciones-title{
    font-size: larger;    
    font-weight: bold;
  }

  .boton {
    zoom: 2;
    vertical-align: baseline;
    cursor: pointer;
    text-decoration: none;
    font: 12px Arial;
    padding: .5em;
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
  }
  .boton:hover { text-decoration: none; }
   
  /* Redondez */
   
  .redondo {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
  }
   
  /* Color */
   
  .negro {
    color: #051d60;
    border: solid 1px #d7d7d7;
    background: #fff;
  }
  .negro:hover {
    background: #000;
    background: -webkit-gradient(linear, left top, left bottom, from(#444), to(#000));
    background: -moz-linear-gradient(top,  #444,  #000);
    filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#000000');
  }

  #chartdiv {
    height: 100%;
  }
  /* Optional: Makes the sample page fill the window. */
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .profile-empresa{
    margin-right: auto!important;
    margin-left: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    width: 250px;
    height: 250px;
    -webkit-border-radius: 200px 200px 200px 200px;
    top: -100px !important;
    position: relative;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  .img-empresa{
    padding: 30px;
    border: solid 0px;
    line-height: 140;
    position: absolute;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center;
    top: 50%;
    bottom: 50%;
  }

  .mx-auto {
    margin-right: auto!important;
    margin-left: auto!important;
    margin-bottom: auto;
    margin-top: auto;
  }

  .btn-amarillo {
    background: rgba(212, 194, 59, 0.87);
  }

  .tr-table{
      background-color: #063F9C;
      color: aliceblue;
  }

</style>

<div class="card animated flipInX" style="margin-bottom: 30px;">
        <div class="card-block"> 
    <?php $empresa = App\Empresa::find($model->empresa); ?>
    <?php $producto = App\Producto::where('name', 'LIKE', $model->producto)->get()->pop(); ?>
      <img src="
         @if(!empty($empresa->principal))
            {{ url('/') }}/images/file/empresas/principal/{{ $empresa->principal }}
         @else
            https://placeholdit.imgix.net/~text?txtsize=33&txt=Principal&w=350&h=150 
         @endif" style="width: 100%;height: 300px;margin-top: 12px;">
      <div class="profile-empresa">
        <img src="
           @if(!empty($empresa->logo))
              {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
           @else
              https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
           @endif" class="img-fluid mx-auto d-block img-empresa">
      </div>

      <div>
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <a href="{{ url('editarempresa') }}/{{ $model->id }}" class="btn btn-amarillo btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Maquina</a>
              <a href="#titulobservaciones" class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Comentar</a>
            </div>
          </div>
          <div class="col-6 col-md-6 centrado">
            <div  class="hijo">

              <div class="imgs-thumbnail img-sedes">
                <img src="
                  @if(!empty($producto->foto))
                    {{ url('/') }}/images/file/productos/{{ $producto->foto }}
                  @else
                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
                  @endif" style="max-width: 50%;" class="img-fluid menu biblioteca-img d-block">
              </div>
              <h2 class="titulo">{{ $empresa->nombre }}</h2>
              <h2 class="titulo1">
              <?php 
                if (isset($model->cliente) && !empty($model->cliente)) {
                  $cliente = App\Cliente::find($model->cliente); 
                  echo $cliente->tratamiento.". ".$cliente->cliente; 
                }
              ?></h2>
              <p class="subtitulo">
                <i class="fa fa-map-marker"></i>
                {{ $model->ciudad.", ".$model->pais }}
              </p>
            </div>
          </div>
          <div class="col-6 col-md-6 centrado" style="padding-top: 70px">  
            <div class="hijo">                    
              <p class="subtitulo">
                <i class="fa fa-star-o" aria-hidden="true"></i>
                Creada el {{ date("d M Y",strtotime($model->created_at)) }}
              </p>
              <p class="subtitulo2">
                <i class="fa fa-repeat" aria-hidden="true"></i>
                Actualizada el {{ date("d M Y",strtotime($model->update_at)) }}
              </p>
            </div>   
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="hijo2 row justify-content-md-center">
              <div class="table-responsive">
                <table class="table product-table">                                
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Maquina</th>
                        <th class="centrado">Valor venta en pesos</th>
                        <th class="centrado">Año</th>
                        <th class="centrado">Vendedor</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h2><strong>{{ $model->producto }}</strong></h2>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h5><strong><?php echo "$ ".$model->valor_venta." ="; ?></strong></h5>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h5><strong>{{ $model->ano_venta }}</strong></h5>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <div class="media mb-1">
                              <a class="media-left waves-light">
                                  <img class="rounded-circle observaciones-img" src="https://mdbootstrap.com/img/Photos/Avatars/avatar-13.jpg" alt="Generic placeholder image">
                              </a>
                              <div class="media-body">
                                <?php if (isset($model->vendedor_id) && !empty($model->vendedor_id)) { ?>
                                  <h4 class="media-heading observaciones-title">
                                    <?php
                                      $user = App\User::find($model->vendedor_id); echo $user->nombres ." ". $user->apellidos;
                                    ?>
                                  </h4>
                                  <p class="subtitulo">
                                      {{ $user->cargo }}
                                  </p>
                                <?php }?>
                              </div>
                          </div>                          
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="table-responsive">
                <table class="table product-table">      
                  <thead>
                      <tr class="tr-table">
                          <th class="centrado">Observaciones</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td class="centrado">
                              <h5><strong>{{ $model->observaciones }}</strong></h5>
                          </td>
                      </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        
      </div>
   </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/tether.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/mdb.min.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<!--sweetalert -->
<script type="text/javascript" src="{{ url('/') }}/js/plugins/sweetalert/sweetalert.min.js"></script>  

<script>
  direccion="{{ url('/') }}";
  var image = "";
  var contentString = "";

  $(function() {
    getComentarios({{ $model->id }});
  });


  guardarComentario = function (id) {
    event.preventDefault();    
    if(true === $("#formComentarios").parsley().validate()){
      console.log("llega"); 
      var datos = new FormData($("#formComentarios")[0]);
      $.ajax({
        url: "{{ url('savecomencliente') }}",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
          if (data.success) {
            $('#formComentarios')[0].reset();
            getComentarios(id);
          }else{
            swal("Algo salio mal, vuelve a intentar");
          }
        }
    }); 
    }else{
      swal("Faltan campos por completar!");
    }
  }

  getComentarios = function (id) {
      $.ajax({
          url: "{{ url('listcomencliente') }}/"+id,
          type: 'GET',
          success: function(data){
            if (data.success) {
              console.log(data.datos);
              var html3 = "";
              for(i in data.datos){
                comentario = data.datos[i];
                html3 += `
                <div class="media mb-1">
                  <a class="media-left waves-light">
                      <img class="rounded-circle observaciones-img" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(18)-mini.jpg" class="rounded-circle z-depth-1-half" alt="Generic placeholder image">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading observaciones-title">`+comentario.user.nombres+` `+comentario.user.apellidos+` <small>`+comentario.created_at+`</small> </h4>
                      <p class="subtitulo">`+comentario.comentario+`</p>
                  </div>
                </div>`;
              }
              $("#lisComentariosId").html(html3);
            }else{

            }
          }
      });
    }
</script>
@endsection

