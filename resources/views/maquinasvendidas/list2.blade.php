@extends('template.app')

@section('title', 'Maquinas Vendidas')

@section('content')
<style>
#map-vendidas { height: 712px; }.color-blue-b{color: #4092c1;}.color-blue-c{color: #0056b3;}.line-gray{display: inline-block;width: 100%;background: #e8d6d6;height: 1px;}.modal-dialog{width:90%!important;max-width:100%}.profile-empresa3{margin:auto!important;-webkit-border-radius:50% 50% 50% 50%;position:relative;background-color:#fff;overflow:hidden;top:-90px!important;left:130px!important;width:140px;height:140px;-webkit-box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important;-moz-box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important;box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important}.img-empresa{padding:5px!important}#example_wrapper .row{width:100%}
</style>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
  integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
  crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
  integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
  crossorigin=""></script>
  {{csrf_field()}}
 <div class="jumbotron">
  <div class="panel-title">
    <div class="pull-right">
      <a href="/crearmaquinavendida" class="btn btn-sm btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Crear maquina vendida</a>
      <div class="btn-group" role="group">
        <button type="button" class="btn btn-sm btn-secondary active" onclick="funMostarDiv('divmapas')" id="bdivmapa">
          <i class="fa fa-map"></i>
          Ver mapa
        </button>
        <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('list')" id="blist">
          <i class="fa fa-list"></i>
          Ver lista
        </button>
    </div>
    </div>
    <h2>Presencia mundial ESSI</h2>
  </div>
</div>

<div class="card animated flipInX" id="list" style="margin-bottom: 30px;display: none;">
  <div class="card-block">
    @if(count($data['datos']) > 0)
     <div class="table-responsive">
      <table id="example" cellspacing="0" class="table table-striped table table-striped table-bordered display" style="width: 100%">
        <thead>
          <tr>
            <th class="centrado">Item</th>
            <th class="centrado">Foto</th>
            <th class="centrado">Logo</th>
            <th class="centrado">Empresa</th>
            <th class="centrado">Sede</th>
            <th class="centrado">Maquina</th>
            <th class="centrado">Referencia</th>
            <th class="centrado" style="width: 150px">Valor venta en pesos</th>
            <th class="centrado">Año</th>
            <th class="centrado">Vendedor</th>
            <th class="centrado">Observaciones</th>
            <th class="centrado">Acciones</th>
          </tr>
        </thead>
        <tbody>
           <?php $i = 1; ?>
          @foreach($data['datos'] as $dato)
          <tr>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $i++ }}</p></td>
            <td>
                <img src="
                  @if(!empty($dato->foto))
                    /images/file/productos/{{ $dato->foto }}
                  @else
                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100
                  @endif" style="max-width: 50px;width: 50px;" class="imgs-thumbnail d-block">
            </td>
            <td>
                <img src="
                  @if(!empty($dato->logo))
                    /images/file/empresas/principal/{{ $dato->logo }}
                  @else
                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100
                  @endif" style="max-width: 50px;width: 50px;" class="imgs-thumbnail d-block">
            </td>
            <td class="centrado centrar-vertical" style="text-transform: uppercase !important;"><p class="mas-pequeño">{{ $dato->nombre }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->ciudad }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->name }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->referencia }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño"><?php echo "$ ".$dato->valor_venta; ?></p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->ano_venta }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">
              <?php
                $nombreuser ="";
                if (isset($dato->nombres) && !empty($dato->nombres)) {
                  $nombre   = explode(" ",$dato->nombres);
                  if (isset($nombre[2])) {
                    $nombreuser = $nombre[0]." ".$nombre[2];
                  }else{
                    $nombreuser = $nombre[0]." ".$nombre[1];
                  }
                  echo $nombreuser;
                }

              ?>
            </p></td>
            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top" title="{{ $dato->observaciones }}"><p class="mas-pequeño">Ver</p></td>
            <td class="text-center">
                <button onclick="modalVerMaquina({{ $dato->id }})" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ver maquina vendida">
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </button>
                <?php if($data['permiso_editar']=="Si"){ ?>
                <a href="{{ url('maquinavendida/editar') }}/{{ $dato->id }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editar maquina vendida">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <?php } ?>
                <?php if($data['permiso_eliminar']=="Si"){ ?>
                <button class="btn btn-danger btn-sm" data-toggle="tooltip" onclick="eliminarDato({{ $dato->id }}, this)" data-placement="top" title="Eliminar maquina vendida">
                  <i class="fa fa-trash-o" aria-hidden="true" style="color: #fff;"></i>
                </button>
                <?php } ?>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @endif
  </div>
</div>

<div class="row" id="divmapas">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="row mb-5" style="margin-left: 25%;margin-right: 25%;">
          <div class="col-md-10">
               <input type="text" class="form-control" id="filtro" placeholder="Escribe la maquina a filtrar">
          </div>
          <div class="col-md-2 bg-success" style="cursor:pointer;">
            <p class="h4" onclick="filtro()"><a class="text-white text-center"><i class="fa fa-search"></i> Filtrar</a></p>
          </div>
      </div>
   </div>
    <div class="col-xs-12 col-sm-12 col-md-9">
        <div class="map-vendidas" id="map-vendidas" style="z-index:1!important;">

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 bg-white" id="listado_productos">

    </div>
</div>

<!-- Template mapa marker popup -->
  <template id="popupMarker">
      <div>
        <div class="content-popup">
          <div class="col-xl-12">
            <div class="row">
              <div class="col-xl-3">
                <div class="row justify-content-center">
                  <div class="col-xl-12 p-0">
                    <img src="" alt="" data-logo style="width:100%;">
                  </div>
                  <div class="col-xl-12 p-0">
                    <img src="" alt="" data-fotomaquina style="width:100%;">
                  </div>
                </div>
              </div>
              <div class="col-xl-9">
                <div class="row">
                  <div class="col-xl-12">
                    <span class="color-blue-b f-400">Empresa:</span> <span class="color-blue-c f-600" data-empresa></span>
                  </div>
                  <div class="col-xl-12">
                    <span class="color-blue-b f-400">Año Venta:</span> <span class="color-blue-c f-500" data-anoventa></span>
                  </div>
                </div>
                <div class="row">
                  <div class="line-gray"></div>
                </div>
                <div class="row">
                  <div class="col-xl-12 color-blue-c f-600 m-t-5" data-producto></div>
                </div>
                <div class="row">
                  <div class="col-xl-12">
                    <div class="row m-t-5">
                      <div class="col-xl-6">
                        <div class="color-blue-b f-400">Referencia</div>
                      </div>
                      <div class="col-xl-6">
                        <div class="color-blue-c f-600" data-productoreferencia></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-12">
                    <div class="row m-t-5">
                      <div class="col-xl-6">
                        <div class="color-blue-b f-400">Vendedor</div>
                      </div>
                      <div class="col-xl-6">
                        <div class="color-blue-c f-600" data-vendedor></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </template>
  <!--End Template mapa marker popup -->

<div class="modal modal-wide fade p-4" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal">Cargando...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body px-0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="fa-3x">
                               <img src="/images/carga.gif" style="width:80px;">
                                <p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalvermaquina">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Maquina vendida</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="bodyMaquinaVendida">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="/js/generales/objMap.js" ></script>
<script type="text/javascript" src="/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/components/datatable/js/dataTables.bootstrap4.min.js"></script>
<script>
  var CSRF_TOKEN = $('input[name=_token]').val();
    $(document).ready(function(){
        lista_producos();
        var filtro = $("#filtro").val();
        var params = {}
        params.name_category = filtro;
        lista_maquinasvendidas(params);
    });

    /**
     * Listado de iconos de los producto ESSI
     */
    function lista_producos(){
        var jqxhr = $.ajax({
              url: "/listmaquinasvendidas-mapa-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 0,
                  filtro: $("#filtro").val()
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#listado_productos').html(e.data['html']);
              }
        });
    }

    function filtro(){
        var filtro = $("#filtro").val();
        var params = {}
        params.name_category = filtro;
        lista_maquinasvendidas(params);
        lista_producos();
    }

    /**
     * Listado de maquinas vendidas en el mundo
     */
    var load = false;
    var mymap;
    function lista_maquinasvendidas(params){
        $('#exampleModal').modal('show');
        if(!load){
            mymap = new MapStreep();
             mymap.init({
            center:[4.6311141,-78.8375682],
            zoom:4,
            layer:'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', //terreno del mapa,
            element: 'map-vendidas' //elemento div html ejemplo <div id="mymap"></div>
          }); //inicializar el mapa con el centro correspondiente
          load = true;
        }
        mymap.clearAllMarkers();

          var params = {
              url: "/listmaquinasvendidasJSON",
              data:{
                filtro:params
              } //parametros para el server
            }
          mymap.requestData(params, function(data, addMarkerMap){ //ejecutar petición y traer la data tipo JSON, addMarkerMap es una funcioón callback
            $.each( data.markers, function( i, marker ) {
              var IconMaquina = L.icon({
                  iconUrl: '/images/iconosmapamaquinasvendidas/'+marker.IDPRODUCTO+'.png',
                  iconSize:     [30, 30], // size of the icon
                  iconAnchor:   [15, 30], // point of the icon which will correspond to marker's location
                  popupAnchor:  [-0, -30] // point from which the popup should open relative to the iconAnchor
              });

              var tpl = $("#popupMarker").html();
              var $temp = $(tpl);
              $temp.find("[data-logo]").attr("src", marker.EMPRESALOGO);
              $temp.find("[data-fotomaquina]").attr("src", marker.PRODUCTOFOTO);
              $temp.find("[data-anoventa]").html(marker.ANOVENTA);
              $temp.find("[data-empresa]").html(marker.empresa);
              $temp.find("[data-producto]").html(marker.PRODUCTO);
              $temp.find("[data-productoreferencia]").html(marker.PRODUCTOREFERENCIA);
              $temp.find("[data-vendedor]").html(marker.CORTORESPONSABLE);

              var params = {
                tpl:$temp, //se pasa el template armado
                icon:IconMaquina, //se pasa el icono correspondiente al marker actual,
                marker:marker //se pasa el marker para agregar al mapa renderizado
              }
             addMarkerMap(params); //ejecutamos el callback para responder con el template del popup, el marker y el icono correspondiente
            });
              setTimeout(function(){ $('#exampleModal').modal('hide'); }, 1000);
          }); //traer markers del server

    }

      /** Edwin:Codigo de List.blade.php
       * Función para cambiar la vista
       * @param STRING valor LISTA - MAPA
       */
      funMostarDiv = function (valor) {
        $(".active").removeClass('active');
        $("#b"+valor).addClass('active');
        $(".slideInDown").removeClass('slideInDown').addClass('rollOut');
        if(valor==="list"){
          $("#list").show();
        }else{
          $("#list").hide();
        }
        if(valor==="divmapas"){
          $("#divmapas").show();
        }else{
          $("#divmapas").hide();
        }
        $("#"+valor).removeClass('rollOut').addClass('slideInDown');
      }

      /**
       * Función para ver el modal
       * @param INT id IDENTIFICACIÓN DE LA MAQUINA VENDIDA
       */
      modalVerMaquina = function(id) {
    url = "{{ url('maquinavendida') }}/"+id;
    var jqxhr = $.get( url )
    .done(function(data) {
      console.log( "second success", data );
      if (data.success) {
        if(data.referencia.foto != ""){
          foto = `/images/file/productos/`+data.referencia.foto;
        }else{
          foto = `https://placeholdit.imgix.net/~text?txtsize=33&txt=Maquina&w=100&h=100`;
        }


        if(data.empresa.logo != ""){
          fotoE = `/images/file/empresas/principal/`+data.empresa.logo;
        }else{
          fotoE = `http://via.placeholder.com/250x250/fff/948e8e?text=Logo`;
        }

        html =`
          <div id="bodyContent">
            <a href="#">
              <img src="`+foto+`" style="max-width: 90%" class="img-fluid mx-auto d-block">
            </a>
            <div class="profile-empresa3">
                <img src="`+fotoE+`"  style="max-width: 140px;"  class="img-fluid mx-auto d-block img-empresa">
            </div>
          </div>
          <h2 class="centrado titulo" style="margin-top: -50px;"></h2>
          <div class="table-responsive">
                <table class="table product-table">
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Empresa</th>
                        <th class="centrado">Sede</th>
                        <th class="centrado">Cliente</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.empresa != null) {
                            html += `<h4><strong>`+data.empresa.nombre+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra empresa</h4>`
                          }

                      html += `</td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.sede != null) {
                            html += `<h4><strong>`+data.sede.ciudad+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra sede</h4>`
                          }

                      html += `</td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.cliente != null) {
                            html += `<h4><strong>`+data.cliente.cliente+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra cliente</h4>`
                          }

                      html += `</td>
                    </tr>
                  </tbody>
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Valor venta en pesos</th>
                        <th class="centrado">Año</th>
                        <th class="centrado">Comercial</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.model.valor_venta != null) {
                            html += `<h4><strong>$ `+data.model.valor_venta+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra valor de venta</h4>`
                          }

                      html += `</td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h4><strong>`+data.model.ano_venta+`</strong></h4>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <div class="media mb-1">
                              <div class="media-body">`;
                                if (data.vendedor != null) {
                                  html += `<h4 class="media-heading observaciones-title">`+data.vendedor.nombres+` `+data.vendedor.apellidos+` </h4>`
                                }else{
                                  html += `<h4 class="media-heading observaciones-title"> No registra comercial </h4>`
                                }

                              html += `</div>
                          </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="table-responsive">
                <table class="table product-table">
                  <thead>
                      <tr class="tr-table">
                          <th class="centrado">Observaciones</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td class="centrado">`;
                          if (data.model.observaciones != null) {
                            html += `<h5><strong>`+data.model.observaciones+`</strong></h5>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra observaciones</h4>`
                          }

                      html += `</td>
                      </tr>
                  </tbody>
                </table>
              </div>`;
        $("#bodyMaquinaVendida").html(html);
        $("#modalvermaquina").modal();
        $(".modal-title").html(data.producto.name+` `+data.referencia.referencia);
      }
    })
    .fail(function(error) {
      console.log( "error",error );
    });
  }

      $(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#example').DataTable({
          language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ maquinas vendidas",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ maquinas vendidas",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 maquinas vendidas",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          }
        });
      });

  eliminarDato = function(id, input) {
    swal({
      title: '¿Estás seguro?',
      html: $('<div>')
        .addClass('some-class')
        .text('¡No podrás revertir esto!'),
      animation: false,
      customClass: 'animated tada',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo!',
      cancelButtonText: 'Cancelar',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      showLoaderOnConfirm: true,
      preConfirm: function () {
        return new Promise(function (resolve) {
          var elemento =$(input).parent().parent();
          $.get( "{{ url('maquinavendida/eliminar') }}/"+id, function( data ) {
            if (data.success) {
              //$('#formArchivo')[0].reset();
              $( elemento ).remove();
              swal('¡Eliminado!','Su archivo ha sido eliminado.','success');
              resolve()
            }else{
              swal("Algo salio mal, vuelve a intentar",'warning');
              resolve()
            }
          });
        })
      },
      allowOutsideClick: false
    });
  }
</script>
@endsection
