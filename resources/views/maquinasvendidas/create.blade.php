<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Agregar maquina vendida')

@section('content')   

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
   
</style>

<div class="card animated flipInX" id="list" style="margin-bottom: 30px;">
  <div class="card-block">  
    <form method="POST" action="{{ url('crearmaquinavendida') }}">
      <div class="panel-title">
          <h2>Agregar Maquina Vendida</h2>
          <p>Agrega una nueva maquina vendida a los registros</p>
      </div>
      <hr>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      {{ csrf_field() }}
      <div class="form-group" id="listProductos">
          <div class="row">
              <div class="col-6">
                  <select id="selectProducto" name="producto" class="form-control productos" onchange="buscarDimSelec2()"></select> 
              </div>
              <div class="col-4">
                  <select id="referencia" name="referencia" class="form-control referencia"></select> 
              </div>  
          </div>
      </div>

      <div class="form-group row">
        <div class="col-4">
            <label for="empresa" class="col-form-label">Empresa</label>
            <select name="empresa" class="form-control" id="empresa" required></select>
        </div>
        <div class="col-4">
            <label for="sede" class="col-form-label">Sede</label>
            <select name="sede" class="form-control" id="sede" required></select>
        </div>
        <div class="col-4">
            <label for="cliente" class="col-form-label">Cliente</label>
            <select name="cliente" class="form-control" id="cliente"></select>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-3">
            <label class="col-form-label">Año de venta</label>
            <input class="form-control form-control-sm" type="number" name="ano_venta" placeholder="Ej: 1992">
        </div>
        <div class="col-3">
          <label class="col-form-label">Valor Venta</label>
          <input class="form-control form-control-sm" type="text" name="valor_venta" id="valor" placeholder="Por favor ingrese un valor de venta">
        </div>
        <div class="col-3">               
            <label for="margen" class="col-form-label">Margen de Utilidad</label>
            <div class="input-group">
                <input class="form-control form-control-sm" type="number" name="margen" min="0" max="100" placeholder="0" id="margen">
                <span class="input-group-addon form-control-sm">%</span>
            </div> 
        </div>
        <div class="col-3">
            <label class="col-form-label">Comercial</label>
            <select name="vendedor_id" class="form-control" id="vendedores"></select>
        </div>
      </div>

      <div class="form-group">
        <label for="observaciones" class="col-form-label">Observaciones</label>
        <textarea class="form-control" name="observaciones" rows="5" placeholder="Por favor ingrese una observaciones"></textarea>
      </div>
      <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
    </form>
  </div>
</div>
@endsection
@section('scripts')
  <script src="js/jquery.maskMoney.min.js"></script>
  <script>

    $(function () {
      $("[data-toggle='tooltip']").tooltip();
   
      
      $("#valor").maskMoney();

      $("#margen").change(function () {
          margen = $(this).val();
          if (margen > 100) {
              swal({
                title: 'Error!',
                text: 'No es posible ingresar un margen superior al 100%.',
                timer: 2000
              }).then(function () {
                  $(this).val("");
              });
          }
      });

      $('.productos').select2({
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sproducto") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            },
            allowClear: true,
            placeholder: "Seleccione los productos",
            productos: true,
            tags: "true",
            language: "es",
            selectOnClose: true,
        });


      $('#empresa').select2({
          // Activamos la opcion "Tags" del plugin
          placeholder: "Seleccione una empresa",
          ciudad: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("empresa") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });

      $('#vendedores').select2({
          // Activamos la opcion "Tags" del plugin
          placeholder: "Seleccione una vendedor",
          ciudad: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("vendedores") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });

      $('#sede').select2({
          // Activamos la opcion "Tags" del plugin
          placeholder: "Seleccione una sede",
          sede: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("sede") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term,
                      state: $('#empresa').val()
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });

      $('#cliente').select2({
          // Activamos la opcion "Tags" del plugin
           placeholder: "Seleccione una cliente",
          ciudad: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("cliente") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term,
                      dato:$("#sede").val()
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });
    });


    buscarDimSelec2 = function () {
        var ref = $.get( "{{ url('/') }}/referencia?term=&dato="+$("#selectProducto").val());
        ref.done(function( data ) {
            console.log(data);
            $('#referencia').html(" ");
            $.each(data, function (k, item) {
                $('#referencia').append($('<option>', { 
                    value: item.id,
                    text : item.text 
                }));
            });
        });
    }
  </script>
@endsection

