<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Maquinas Vendidas')

@section('content')
<link rel="stylesheet" type="text/css" href="/components/datatable/css/dataTables.material.min.css">
<style type="text/css">
.map-marker{margin-left:-8px;margin-top:-8px}.map-marker.map-clickable{cursor:pointer}.pulse{width:10px;height:10px;border:5px solid #f7f14c;-webkit-border-radius:30px;-moz-border-radius:30px;border-radius:30px;background-color:#716f42;z-index:10;position:absolute}.map-marker .dot{border:10px solid #fff601;background:0 0;-webkit-border-radius:60px;-moz-border-radius:60px;border-radius:60px;height:50px;width:50px;-webkit-animation:pulse 3s ease-out;-moz-animation:pulse 3s ease-out;animation:pulse 3s ease-out;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;animation-iteration-count:infinite;position:absolute;top:-20px;left:-20px;z-index:1;opacity:0}@-moz-keyframes pulse{0%{-moz-transform:scale(0);opacity:0}25%{-moz-transform:scale(0);opacity:.1}50%{-moz-transform:scale(.1);opacity:.3}75%{-moz-transform:scale(.5);opacity:.5}100%{-moz-transform:scale(1);opacity:0}}@-webkit-keyframes pulse{0%{-webkit-transform:scale(0);opacity:0}25%{-webkit-transform:scale(0);opacity:.1}50%{-webkit-transform:scale(.1);opacity:.3}75%{-webkit-transform:scale(.5);opacity:.5}100%{-webkit-transform:scale(1);opacity:0}}.observaciones-img{width:50px!important;height:60px!important}.jumbotron{height:100px;padding-top:15px}#chartdiv,body,html{height:100%}.row,div#example_wrapper{width:100%}.img-sedes{max-width:50%;max-height:50%;min-height:125px;min-width:125px}body,html{margin:0;padding:0}.tooltip-inner{white-space:pre-wrap}.subtitulo{font-size:10px!important;color:#54575a!important;padding:0!important;margin:0!important;line-height:.9;text-transform:capitalize}.biblioteca-img{border:0 solid;position:absolute;top:50%;bottom:50%;left:25%;margin-bottom:auto;margin-top:auto;margin-right:auto!important;margin-left:auto!important}.imgs-thumbnail{max-width:100%;position:relative;overflow:hidden;background-color:#fff}.unalinea{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}img{border:0!important}.main-menu>li{line-height:10px!important}.profile-empresa{left:-30px!important;margin:auto!important;width:80px;height:80px;-webkit-border-radius:80px 80px 80px 80px;position:relative;-webkit-box-shadow:6px 7px 38px -4px rgba(5,29,96,.48)!important;-moz-box-shadow:6px 7px 38px -4px rgba(5,29,96,.48)!important;box-shadow:6px 7px 38px -4px rgba(5,29,96,.48)!important;overflow:hidden;background-color:#fff;max-width:140px;max-height:140px}.profile-empresa2,.profile-empresa3{margin:auto!important;-webkit-border-radius:50% 50% 50% 50%;position:relative;background-color:#fff;overflow:hidden}.profile-empresa2{top:-50px!important;left:50px!important;width:60px;height:60px;-webkit-box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important;-moz-box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important;box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important}.profile-empresa3{top:-90px!important;left:130px!important;width:140px;height:140px;-webkit-box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important;-moz-box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important;box-shadow:0 0 28px -4px rgba(5,29,96,.48)!important}.img-empresa{padding:5px!important}.mas-pequeño{line-height:1.5;font-size:small;color:#636363;font-weight:400;display:block!important;margin:0!important}.centrar-vertical{vertical-align:middle!important}h5.card-header{font-weight:900;background-color:#051d60;border-color:#051d60;color:#fff;font-size:11px;padding:2px;margin:1px!important}.titulo{font-weight:700!important;color:#051d60!important;font-size:12px!important}div#content{overflow:hidden}.modal-dialog{width:90%!important;max-width:100%}
</style>

<div class="jumbotron">
  <div class="panel-title">
    <div class="pull-right">
      <a href="/crearmaquinavendida" class="btn btn-sm btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Crear maquina vendida</a>
      <div class="btn-group" role="group">
        <button type="button" class="btn btn-sm btn-secondary active" onclick="funMostarDiv('divmapas')" id="bdivmapa">
          <i class="fa fa-map"></i>
          Ver mapa
        </button>
        <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('list')" id="blist">
          <i class="fa fa-list"></i>
          Ver lista
        </button>
    </div>
    </div>
    <h2>Maquinas Vendidas</h2>
  </div>
</div>

<div class="card animated flipInX" id="divmapas" style="margin-bottom: 30px;">
  <div class="card-block">
    <div id="divmapa" style="height: 700px !important" class=" animated rollIn"></div>
  </div>
</div>

<div class="card animated flipInX" id="list" style="margin-bottom: 30px;display: none;">
  <div class="card-block">
    @if(count($datos) > 0)
     <div class="table-responsive">
      <table id="example" cellspacing="0" class="table table-striped table table-striped table-bordered display" style="width: 100%">
        <thead>
          <tr>
            <th class="centrado">Item</th>
            <th class="centrado">Foto</th>
            <th class="centrado">Logo</th>
            <th class="centrado">Empresa</th>
            <th class="centrado">Sede</th>
            <th class="centrado">Maquina</th>            
            <th class="centrado">Referencia</th>
            <th class="centrado" style="width: 150px">Valor venta en pesos</th>
            <th class="centrado">Año</th>
            <th class="centrado">Vendedor</th>
            <th class="centrado">Observaciones</th>
            <th class="centrado">Acciones</th>
          </tr>
        </thead>
        <tbody>
           <?php $i = 1; ?>
          @foreach($datos as $dato)
          <tr>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $i++ }}</p></td>
            <td>  
                <img src="
                  @if(!empty($dato->foto))
                    /images/file/productos/{{ $dato->foto }}
                  @else
                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
                  @endif" style="max-width: 50px;width: 50px;" class="imgs-thumbnail d-block">
            </td>
            <td>  
                <img src="
                  @if(!empty($dato->logo))
                    /images/file/empresas/principal/{{ $dato->logo }}
                  @else
                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
                  @endif" style="max-width: 50px;width: 50px;" class="imgs-thumbnail d-block">
            </td>
            <td class="centrado centrar-vertical" style="text-transform: uppercase !important;"><p class="mas-pequeño">{{ $dato->nombre }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->ciudad }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->name }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->referencia }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño"><?php echo "$ ".$dato->valor_venta; ?></p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->ano_venta }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">
              <?php 
                $nombreuser ="";
                if (isset($dato->nombres) && !empty($dato->nombres)) {
                  $nombre   = explode(" ",$dato->nombres);
                  if (isset($nombre[2])) {
                    $nombreuser = $nombre[0]." ".$nombre[2];
                  }else{
                    $nombreuser = $nombre[0]." ".$nombre[1];
                  }
                  echo $nombreuser;
                }
               
              ?>
            </p></td>
            <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top" title="{{ $dato->observaciones }}"><p class="mas-pequeño">Ver</p></td>
            <td class="text-center">
                <button onclick="modalVerMaquina({{ $dato->id }})" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ver maquina vendida">
                  <i class="fa fa-eye" aria-hidden="true"></i> 
                </button> 
                <?php if($permiso_editar=="Si"){ ?>
                <a href="{{ url('maquinavendida/editar') }}/{{ $dato->id }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editar maquina vendida">
                  <i class="fa fa-pencil" aria-hidden="true"></i> 
                </a> 
                <?php } ?>
                <?php if($permiso_eliminar=="Si"){ ?>
                <button class="btn btn-danger btn-sm" data-toggle="tooltip" onclick="eliminarDato({{ $dato->id }}, this)" data-placement="top" title="Eliminar maquina vendida">
                  <i class="fa fa-trash-o" aria-hidden="true" style="color: #fff;"></i> 
                </button>
                <?php } ?>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @endif
  </div>
</div>

<div class="modal fade" id="modalvermaquina">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Maquina vendida</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="bodyMaquinaVendida">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/components/datatable/js/dataTables.bootstrap4.min.js"></script>



<script>
  var image = "";
  var contentString = "";
  function initMap() {

    var styledMapType = new google.maps.StyledMapType(
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8ec3b9"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1a3646"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#64779e"
            }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#4b6878"
            }
          ]
        },
        {
          "featureType": "landscape",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#334e87"
            }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6f9ba5"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3C7680"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#304a7d"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2c6675"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#255763"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#b0d5ce"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3a4762"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#0e1626"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#4e6d70"
            }
          ]
        }
      ],
      {name: 'Styled Map'}
    );

    var map = new google.maps.Map(document.getElementById('divmapa'), {
      zoom: 3,
      center: {lat: 7.064034827633218, lng: -73.1567105784718},
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
      }
    });

    image = {
      url: '/images/iconos_empresa/maquinas_vendidas.png',
      size: new google.maps.Size(40, 50),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 50)
    };
    
    var markers = [];
    function addMarker(feature) {
      console.log(JSON.stringify(feature.position["lat"]));          
      var marker = new google.maps.Marker({
        position: feature.position,
        icon: image,
        title: feature.type,
        map: map,
        draggable:true
      });
      marker.setAnimation(google.maps.Animation.BOUNCE);
      
      var infowindow = new google.maps.InfoWindow({
        content: feature.contenido,
        maxWidth: 350
      });

      marker.addListener('mouseover', function() {
        contentString = feature.contenido;
        marker.setAnimation(google.maps.Animation.BOUNCE);
        infowindow.open(map, marker);
      });

      marker.addListener('mouseout', function() {
        infowindow.close();
      });

      markers.push(marker);
    }

    var features = [
      <?php try {  foreach($datos as $dato){ 
      if (!empty($dato->lat)) { 
        $nombre = trim($dato->name, " \t\n\r\0\x0B"); 
        $nombre  = str_replace(' ', '', $nombre ); 
        $nombre = preg_replace('([^A-Za-z0-9])', '', $nombre);?>
        {
          position: new google.maps.LatLng({{ $dato->lat }}, {{ $dato->lng }}),
          type: '<?php echo $nombre; ?>',
          contenido:  `<div id="content">
                        <div id="siteNotice">
                        </div>  
                        <div class="row">
                          <div class="col-6">
                            <div id="bodyContent">
                              <a href="/maquinavendida/{{ $dato->id }}">
                                <img src="
                                  @if(!empty($dato->foto))
                                    /images/file/productos/{{ $dato->foto }}
                                  @else
                                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Maquina&w=100&h=100 
                                  @endif" style="max-width: 90%;" class="img-fluid mx-auto d-block">
                              </a>
                              <div class="profile-empresa2">
                                  <img src="
                                   @if(!empty($dato->logo))
                                      /images/file/empresas/principal/{{ $dato->logo }}
                                   @else
                                      http://via.placeholder.com/250x250/fff/948e8e?text=Logo 
                                   @endif"  style="max-width: 60px;"  class="img-fluid mx-auto d-block img-empresa">
                              </div>
                            </div>
                            <h2 class="centrado titulo" style="margin-top: -50px;">{{ $dato->name }} {{ $dato->referencia }}</h2>
                          </div>
                          <div class="col-6">
                            <h5 class="card-header centrado"> Empresa </h5>
                            <p class="subtitulo centrado" style="font-size: 18px;">{{ $dato->nombre }}</p>
                            <h5 class="card-header centrado"> Año venta </h5>
                            <p class="subtitulo centrado" style="font-size: 18px;">{{ $dato->ano_venta }}</p>
                            <h5 class="card-header centrado"> Valor venta </h5>
                            <p class="subtitulo centrado" style="font-size: 18px;">{{ "$ ".$dato->valor_venta }}</p>
                            <h5 class="card-header centrado"> Vendedor </h5>
                            <p class="subtitulo">
                            <?php 
                              $nombreuser ="";
                              if (isset($dato->nombres) && !empty($dato->nombres)) {
                                $nombre   = explode(" ",$dato->nombres);
                                if (isset($nombre[2])) {
                                  $nombreuser = $nombre[0]." ".$nombre[2];
                                }else{
                                  $nombreuser = $nombre[0]." ".$nombre[1];
                                }
                                echo $nombreuser;
                              }                         
                            ?>
                            </p>
                          </div>
                      </div>
                      </div>`
        },
      <?php } } } catch (Exception $e) { }?>           
    ];

    for (var i = 0, feature; feature = features[i]; i++) {
      addMarker(feature);
    }
    var markerCluster = new MarkerClusterer(map, markers, { maxZoom: 12, gridSize: 20, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
  }
  
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSg3ZOwMbQvB0xjXAFmr2Ch-7IKV98gno&callback=initMap"></script>
<script type="text/javascript" charset="utf-8">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ maquinas vendidas",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ maquinas vendidas",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 maquinas vendidas",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      }
    });
  });

  eliminarDato = function(id, input) {
    swal({
      title: '¿Estás seguro?',
      html: $('<div>')
        .addClass('some-class')
        .text('¡No podrás revertir esto!'),
      animation: false,
      customClass: 'animated tada',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo!',
      cancelButtonText: 'Cancelar',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      showLoaderOnConfirm: true,
      preConfirm: function () {
        return new Promise(function (resolve) {
          var elemento =$(input).parent().parent();
          $.get( "{{ url('maquinavendida/eliminar') }}/"+id, function( data ) {
            if (data.success) {
              //$('#formArchivo')[0].reset();
              $( elemento ).remove();
              swal('¡Eliminado!','Su archivo ha sido eliminado.','success');
              resolve()
            }else{
              swal("Algo salio mal, vuelve a intentar",'warning');
              resolve()
            }
          });
        })
      },
      allowOutsideClick: false
    });
  }

  modalVerMaquina = function(id) {    
    url = "{{ url('maquinavendida') }}/"+id;
    var jqxhr = $.get( url )
    .done(function(data) {
      console.log( "second success", data );
      if (data.success) {
        if(data.referencia.foto != ""){
          foto = `/images/file/productos/`+data.referencia.foto;
        }else{
          foto = `https://placeholdit.imgix.net/~text?txtsize=33&txt=Maquina&w=100&h=100`; 
        }


        if(data.empresa.logo != ""){
          fotoE = `/images/file/empresas/principal/`+data.empresa.logo;
        }else{
          fotoE = `http://via.placeholder.com/250x250/fff/948e8e?text=Logo`; 
        }

        html =`
          <div id="bodyContent">
            <a href="#">
              <img src="`+foto+`" style="max-width: 90%" class="img-fluid mx-auto d-block">
            </a>
            <div class="profile-empresa3">
                <img src="`+fotoE+`"  style="max-width: 140px;"  class="img-fluid mx-auto d-block img-empresa">
            </div>
          </div>
          <h2 class="centrado titulo" style="margin-top: -50px;"></h2>
          <div class="table-responsive">
                <table class="table product-table">
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Empresa</th>
                        <th class="centrado">Sede</th>
                        <th class="centrado">Cliente</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.empresa != null) {
                            html += `<h4><strong>`+data.empresa.nombre+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra empresa</h4>`
                          }
                          
                      html += `</td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.sede != null) {
                            html += `<h4><strong>`+data.sede.ciudad+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra sede</h4>`
                          }
                          
                      html += `</td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.cliente != null) {
                            html += `<h4><strong>`+data.cliente.cliente+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra cliente</h4>`
                          }
                          
                      html += `</td>
                    </tr>
                  </tbody>                              
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Valor venta en pesos</th>
                        <th class="centrado">Año</th>
                        <th class="centrado">Comercial</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">`;
                          if (data.model.valor_venta != null) {
                            html += `<h4><strong>$ `+data.model.valor_venta+`</strong></h4>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra valor de venta</h4>`
                          }
                          
                      html += `</td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h4><strong>`+data.model.ano_venta+`</strong></h4>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <div class="media mb-1">
                              <div class="media-body">`;
                                if (data.vendedor != null) {
                                  html += `<h4 class="media-heading observaciones-title">`+data.vendedor.nombres+` `+data.vendedor.apellidos+` </h4>`
                                }else{
                                  html += `<h4 class="media-heading observaciones-title"> No registra comercial </h4>`
                                }
                                
                              html += `</div>
                          </div>                          
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="table-responsive">
                <table class="table product-table">      
                  <thead>
                      <tr class="tr-table">
                          <th class="centrado">Observaciones</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td class="centrado">`;
                          if (data.model.observaciones != null) {
                            html += `<h5><strong>`+data.model.observaciones+`</strong></h5>`
                          }else{
                            html += `<h4 class="media-heading observaciones-title">No registra observaciones</h4>`
                          }
                          
                      html += `</td>
                      </tr>
                  </tbody>
                </table>
              </div>`;
        $("#bodyMaquinaVendida").html(html);
        $("#modalvermaquina").modal();
        $(".modal-title").html(data.producto.name+` `+data.referencia.referencia);
      }
    })
    .fail(function(error) {
      console.log( "error",error );
    });
  }

  funMostarDiv = function (valor) {
    $(".active").removeClass('active');
    $("#b"+valor).addClass('active');
    $(".slideInDown").removeClass('slideInDown').addClass('rollOut');
    if(valor==="list"){
      $("#list").show();
    }else{
      $("#list").hide();
    }
    if(valor==="divmapas"){
      $("#divmapas").show();
    }else{
      $("#divmapas").hide();
    }

    
    $("#"+valor).removeClass('rollOut').addClass('slideInDown');
    initMap();
  }
</script>
@endsection
