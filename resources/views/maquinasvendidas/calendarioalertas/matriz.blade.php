<div class="row" style="margin-top:90px;">
    <div class="col-md-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-flag"></i><a href="">Equipos Vendidos</a></li>
            <li class="active">Matriz</li>
        </ul>
    </div>
    <div class="col-md-8 ">
        <div class="top-content">
            <ul class="list-inline mini-stat">
                <li>
                    <div class="chart-block" style="padding:28px">
                       <h5>VENDIDOS <span class="stat-value stat-color-orange" id="total_vendidos"><i class="fa fa-plus-circle"></i> 118</span></h5>
                       <div id="column1" style="vertical-align: middle;display: inline-block; width: 110px; height: 30px;"></div>
                    </div>
                </li>
                <li>
                    <h5>AÑO ACTUAL <span class="stat-value stat-color-blue" id="total_anoactual"><i class="fa fa-plus-circle"></i> 20</span></h5>
                    <div id="column2" style="vertical-align: middle;display: inline-block; width: 110px; height: 30px;"></div>
                </li>
                <li>
                    <h5>AÑOS ANTERIORES <span class="stat-value stat-color-seagreen" id="total_anoanteriores"><i class="fa fa-plus-circle"></i> 98</span></h5>
                    <div id="column3" style="vertical-align: middle;display: inline-block; width: 110px; height: 30px;"></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row mt-5 matriz">
    <div class="col-md-12">
        <div class="content">
            <div class="row mx-5 bg-white" id="contenido_productos">

            </div>
        </div>
    </div>
</div>


<!-- Styles -->
<style>
#chartdiv {
 width: 100%;
  height: 500px;
}
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<!-- Chart code -->
<script>
/**
 * Column Chart #1
 */
AmCharts.makeChart( "column1", {
  "type": "serial",
  "dataProvider": [ {
    "day": 1,
    "value": 5
  }, {
    "day": 2,
    "value": 3
  }, {
    "day": 3,
    "value": 7
  }, {
    "day": 4,
    "value": 3
  }, {
    "day": 5,
    "value": 3
  }],
  "categoryField": "day",
  "autoMargins": false,
  "marginLeft": 0,
  "marginRight": 0,
  "marginTop": 0,
  "marginBottom": 0,
  "graphs": [ {
    "valueField": "value",
    "type": "column",
    "fillAlphas": 1,
    "lineColor": "#CE7B11",
    "showBalloon": false
  } ],
  "valueAxes": [ {
    "gridAlpha": 0,
    "axisAlpha": 0
  } ],
  "categoryAxis": {
    "gridAlpha": 0,
    "axisAlpha": 0
  }
} );

AmCharts.makeChart( "column2", {
  "type": "serial",
  "dataProvider": [ {
    "day": 1,
    "value": 5
  }, {
    "day": 2,
    "value": 3
  }, {
    "day": 3,
    "value": 7
  }, {
    "day": 4,
    "value": 3
  }, {
    "day": 5,
    "value": 3
  }],
  "categoryField": "day",
  "autoMargins": false,
  "marginLeft": 0,
  "marginRight": 0,
  "marginTop": 0,
  "marginBottom": 0,
  "graphs": [ {
    "valueField": "value",
    "type": "column",
    "fillAlphas": 1,
    "lineColor": "#1D92AF",
    "showBalloon": false
  } ],
  "valueAxes": [ {
    "gridAlpha": 0,
    "axisAlpha": 0
  } ],
  "categoryAxis": {
    "gridAlpha": 0,
    "axisAlpha": 0
  }
} );

AmCharts.makeChart( "column3", {
  "type": "serial",
  "dataProvider": [ {
    "day": 1,
    "value": 5
  }, {
    "day": 2,
    "value": 3
  }, {
    "day": 3,
    "value": 7
  }, {
    "day": 4,
    "value": 3
  }, {
    "day": 5,
    "value": 3
  }],
  "categoryField": "day",
  "autoMargins": false,
  "marginLeft": 0,
  "marginRight": 0,
  "marginTop": 0,
  "marginBottom": 0,
  "graphs": [ {
    "valueField": "value",
    "type": "column",
    "fillAlphas": 1,
    "lineColor": "#3F7577",
    "showBalloon": false
  } ],
  "valueAxes": [ {
    "gridAlpha": 0,
    "axisAlpha": 0
  } ],
  "categoryAxis": {
    "gridAlpha": 0,
    "axisAlpha": 0
  }
} );
</script>
