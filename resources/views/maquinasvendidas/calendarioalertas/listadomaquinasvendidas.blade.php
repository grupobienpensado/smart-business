<div class="row mt-5 calendario">
   <div class="col-md-12">
      <div class="row" style="margin-left:25%;margin-right:25%;">
          <div class="col-md-12">
              <input class="form-control" id="filtro_tabla" placeholder="Buscar">
          </div>
      </div>
   </div>
    <div class="col-md-12 mt-5">
        <div class="table-responsive-sm">
            <table class="table">
                <thead>
                    <tr style="background-color:#2f3e5d;color:#6198bf;">
                        <th>Item</th>
                        <th>Empresa</th>
                        <th>Vendedor</th>
                        <th>Producto</th>
                        <th>Referencia</th>
                        <th>Pais</th>
                        <th>Año</th>
                        <th>Alertas</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody id="contenido_tabla"  class="bg-white">

                </tbody>
            </table>
        </div>
    </div>
</div>
<template id="row">
    <tr class="h5">
        <td data-item>1</td>
        <td data-empresa><img src="/images/file/empresas/principal/59847ed8922be.png" class="img-avatar"><br>PRODALECC <br> Latacunga</td>
        <td data-vendedor><img src="/images/file/clientes/essi_admon.jpg" class="img-avatar"><br><p class="h6"><em>Jose Gutierrez</em></p></td>
        <td data-producto>BAGGGER TREEPACK</td>
        <td data-referencia>ESSI A3</td>
        <td data-pais>Colombia</td>
        <td data-ano>2018</td>
        <td data-alertas><i class="fa fa-bell"></i><p class="h6"><em>3</em></p></td>
        <td data-accion><a class="text-success" data-toggle="tooltip" data-placement="top" title="Agregar Alerta"><i class="fa fa-plus"></i></a></td>
    </tr>
</template>
