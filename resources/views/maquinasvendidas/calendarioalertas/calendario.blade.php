<input type="hidden" id="nombre_producto_seleccionado" value="">
<div class="row calendario">
    <div class="col-md-12">
       <div class="container">
           <div class="row" style="margin-left:25%;margin-right:25%;">
               <div class="col-md-12">
                   <?php $ano = intval(date('Y')); ?>
                   <input type="text" class="form-control date-select text-center" id="ano_filtro" min="{{$ano-10}}" max="{{$ano+30}}" list="anos" style="font-size: 40px;" placeholder="Ingrese Año" value="2018">
                   <datalist id="anos">
                     <?php for($i=($ano-10);$i<=($ano+30);$i++){ ?>
                      <option value="{{$i}}">
                      <?php } ?>
                    </datalist>
               </div>
           </div>
       </div>
    </div>
</div>
<div class="row mt-3 calendario" id="calendar-alertas">
    <div class="col-md-12 bg-white">
        <div class="row calendar-add-dias"></div>
    </div>
</div>

<div class="modal modal-wide fade p-4" id="crearAlerta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_crearAlerta"><a class="text-danger"><i class="fa fa-bell"></i></a> Crear Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body px-0">
               <form method="POST" action="" id="formulario" enctype="multipart/form-data">
               {{ csrf_field() }}
               <input type="hidden" name="action" value="2">
               <input type="hidden" id="id_alerta" name="id_alerta" value="">
               <input type="hidden" id="tipo_alerta" name="formulario[tipo]" value="">
               <input type="hidden" id="id_oportunidad_producto" name="formulario[id_oportunidad_producto]" value="">
               <input type="hidden" id="id_maquinas_vendidas" name="formulario[id_maquinas_vendidas]" value="">
               <input type="hidden" name="formulario[id_user_creador]" value="{{Auth::user()->id}}">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="h4">
                                <label>Fecha Alerta</label>
                                <input type="text" class="form-control" id="fecha_alerta" onchange="fecha_seleccionada(this.id,'#fecha_mostrar',this.value)" name="formulario[fecha_ejecucion]" placeholder="Seleccione una fecha" required>
                                <div class="row" style="display: none;" id="fecha_mostrar">
                                    <div class="col-md-11">
                                        <input class="form-control" type="text" readonly style="cursor: no-drop;">
                                    </div>
                                    <div class="col-md-1">
                                        <a class="text-warning editar-fecha" onclick="editar_fecha('#fecha_mostrar','#fecha_alerta')"><i class="fa fa-pencil"></i></a>
                                    </div>
                                </div>
                            </p>
                        </div>
                        <div class="col-md-12 text-center">
                            <p class="h4">
                                <label>Titulo</label>
                                <input type="text" class="form-control" name="formulario[titulo]" id="titulo_alerta" placeholder="Ingrese un titulo" onkeyup="max_campo(this.value,100,'#texto_titulo_alerta',this.id)" required>
                                <p class="text-right invisible" id="texto_titulo_alerta"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                            </p>
                        </div>
                        <div class="col-md-12 text-center">
                            <p class="h4">
                                <label>Descripción de la Alerta</label>
                                <textarea class="form-control" name="formulario[descripcion]" id="descripcion_alerta" onkeyup="max_campo(this.value,500,'#texto_descripcion',this.id)" rows="10"></textarea>
                                <p class="text-right invisible" id="texto_descripcion"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                            </p>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="guardar()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!--Modal de ver alertas -->

<div class="modal modal-wide fade p-4" id="VerAlerta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_crearAlerta"><a class="text-danger"><i class="fa fa-bell"></i></a> Listado de Alertas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="listado_alertas">

            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Cerrar</button>
            </div>
        </div>
    </div>
</div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/es.js"></script>
  <script>
   funCrearCalendar = function(ano,eventos) {
       moment.locale('es');
       console.log(moment.locale());
        $('.calendar-add-dias').html('');
        var anoVenta = ano;
        html = `<div class="ano">
                  <h2>`+anoVenta+`</h2>
                </div>`;
        for (var i = 1; i <= 12; i++) {
            var diaSemana = moment(anoVenta + ' ' + i + ' 1').format('e');
            var ultimodia = moment(anoVenta + ' ' + i).daysInMonth();
            var mes = moment(anoVenta + ' ' + i).format('MMM');
            html += `
            <div class="meses">
              <div class="mes">
                <h3>`+mes+`</h3>
              </div>
              <div class="dias">
                <div class="title"><p class="h5">Do</p></div>
                <div class="title"><p class="h5">Lu</p></div>
                <div class="title"><p class="h5">Ma</p></div>
                <div class="title"><p class="h5">Mi</p></div>
                <div class="title"><p class="h5">Ju</p></div>
                <div class="title"><p class="h5">Vi</p></div>
                <div class="title"><p class="h5">Sa</p></div>`;
            if (diaSemana === '5') {
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
            }
            if (diaSemana === '4') {
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
            }
            if (diaSemana === '3') {
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
            }
            if (diaSemana === '2') {
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
            }
            if (diaSemana === '1') {
                html += '<div class="numd"><p></p></div>';
                html += '<div class="numd"><p></p></div>';
            }
            if (diaSemana === '0') {
                html += '<div class="numd"><p></p></div>';
            }
            if(eventos == ''){
                for (var j = 1; j <= ultimodia; j++) {
                    var fecha_mostrar = "";
                    var circuHtml = "";
                    var bg_span ="";
                    var fecha = anoVenta+'-'+i+'-'+j;
                    var fechaCalendar = moment(fecha).format('YYYY-MM-DD');
                    html += `<div class="numd" data-fecha="`+fechaCalendar+`"><p>`+j+`</p></div>`;
                }
            }else{
                for (var j = 1; j <= ultimodia; j++) {
                    var fecha_mostrar = "";
                    var circuHtml = "";
                    var bg_span ="";
                    var fecha = anoVenta+'-'+i+'-'+j;
                    var fechaCalendar = moment(fecha).format('YYYY-MM-DD');
                    var contador = 0;
                    var arryResult = eventos.filter(function(obj) {
                      if (obj.fecha_ejecucion === fechaCalendar){
                        contador++;
                      }
                    });
                    if(contador > 0){
                        html += `<div class="numd alerta-dia" data-fecha="`+fechaCalendar+`"><p class="bg-danger text-white">`+j+`</p></div>`;
                    }else{
                        html += `<div class="numd" data-fecha="`+fechaCalendar+`"><p>`+j+`</p></div>`;
                    }
                }
            }
            html += '</div></div>';
        }
        $(".calendar-add-dias").append(html);
    }
   /**
     * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
     * @param STRING texto  TEXTO INGRESADO EN EL CAMPO
     * @param INT max_long NUMERO MAXIMO DE CARACTERES PARA EL CAMPO
     * @param STRING id  IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
     * @param STRING id_campo  IDENTIFICACION DEL CAMPO EDITADO
     */
    function max_campo(texto,max_long,id,id_campo){
        numeroCaracteres = texto.length;
        $(id).removeClass('invisible');
        if(numeroCaracteres > max_long){
            $('#'+id_campo).val(texto.substr(0,max_long));
            $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
        }else{
            $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
        }
    }

      function alerta_dia(producto){
          var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 6,
                  producto:producto
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  funCrearCalendar(moment().format('YYYY'),e.data['alertas']);
              }
        });
      }
    </script>
