@extends('template.app')
@section('title', 'Maquinas Vendidas - Calendario Alertas')
@section('content')
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<style>
#titulo {overflow-x:hidden;}
#titulo{width:100%;height:100%;position:fixed;background-color:transparent}#titulo .content{position:absolute;top:11%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);height:160px;overflow:hidden;font-family:Lato,sans-serif;font-size:35px;line-height:40px;color:#5fc2ff}#titulo .content__container{font-weight:600;overflow:hidden;height:40px;padding:0 40px}#titulo .content__container:before{content:'[';left:0}#titulo .content__container:after{content:']';right:0}#titulo .content__container:after,#titulo .content__container:before{position:absolute;top:0;color:#051d60;font-size:42px;line-height:40px;-webkit-animation-name:opacity;-webkit-animation-duration:2s;-webkit-animation-iteration-count:infinite;animation-name:opacity;animation-duration:2s;animation-iteration-count:infinite}#titulo .content__container__text{display:inline;float:left;margin:0}#titulo .content__container__list{margin-top:0;padding-left:286px;text-align:left;list-style:none;-webkit-animation-name:change;-webkit-animation-duration:10s;-webkit-animation-iteration-count:infinite;animation-name:change;animation-duration:10s;animation-iteration-count:infinite}#titulo .content__container__list li:nth-child(1){color:#9acd32}#titulo .content__container__list li:nth-child(2){color:green}#titulo .content__container__list li:nth-child(3){color:red}#titulo .content__container__list__item{line-height:40px;margin:0}@-webkit-keyframes opacity{0%,100%{opacity:0}50%{opacity:1}}@-webkit-keyframes change{0%,100%,12.66%{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}16.66%,29.32%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}33.32%,45.98%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}49.98%,62.64%{-webkit-transform:translate3d(0,-75%,0);transform:translate3d(0,-75%,0)}66.64%,79.3%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}83.3%,95.96%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}}@keyframes opacity{0%,100%{opacity:0}50%{opacity:1}}@keyframes change{0%,100%,12.66%{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}16.66%,29.32%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}33.32%,45.98%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}49.98%,62.64%{-webkit-transform:translate3d(0,-75%,0);transform:translate3d(0,-75%,0)}66.64%,79.3%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}83.3%,95.96%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}}/*Sombra productos*/.bg-producto{-webkit-box-shadow: 2px 4px 21px 3px rgba(59,54,214,0.5);-moz-box-shadow: 2px 4px 21px 3px rgba(59,54,214,0.5);box-shadow: 2px 4px 21px 3px rgba(104, 210, 255, 0.44);}/*Fin Sombra productos*/ /*Demo purposes only*/@import url(https://fonts.googleapis.com/css?family=Raleway:400,500,700);@import url(https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css);figure.snip1477{font-family:Raleway,Arial,sans-serif;position:relative;float:left;overflow:hidden;margin:10px 1%;min-width:230px;max-width:315px;width:100%;color:#fff;text-align:center;font-size:16px;background-color:#2a3c48}figure.snip1477 *,figure.snip1477 :after,figure.snip1477 :before{-webkit-box-sizing:border-box;box-sizing:border-box;-webkit-transition:all .55s ease;transition:all .55s ease}figure.snip1477 img{max-width:100%;backface-visibility:hidden;vertical-align:top;opacity:.9}figure.snip1477 .title{position:absolute;top:58%;left:25px;padding:5px 10px 10px}figure.snip1477 .title:after,figure.snip1477 .title:before{height:2px;width:400px;position:absolute;content:'';background-color:#fff}figure.snip1477 .title:before{top:0;left:10px;-webkit-transform:translateX(100%);transform:translateX(100%)}figure.snip1477 .title:after{bottom:0;right:10px;-webkit-transform:translateX(-100%);transform:translateX(-100%)}figure.snip1477 .title div:after,figure.snip1477 .title div:before{width:2px;height:300px;position:absolute;content:'';background-color:#fff}figure.snip1477 .title div:before{top:10px;right:0;-webkit-transform:translateY(100%);transform:translateY(100%)}figure.snip1477 .title div:after{bottom:10px;left:0;-webkit-transform:translateY(-100%);transform:translateY(-100%)}figure.snip1477 h2,figure.snip1477 h4{margin:0;text-transform:uppercase}figure.snip1477 h2{font-weight:400}figure.snip1477 h4{display:block;font-weight:700;background-color:#fff;padding:5px 10px;color:#000}figure.snip1477 figcaption{position:absolute;bottom:42%;left:25px;text-align:left;opacity:0;padding:5px 60px 5px 10px;font-size:.8em;font-weight:500;letter-spacing:1.5px}figure.snip1477 figcaption p{margin:0}figure.snip1477 a{position:absolute;top:0;bottom:0;left:0;right:0}figure.snip1477.hover img,figure.snip1477:hover img{zoom:1;filter:alpha(opacity=35);-webkit-opacity:.35;opacity:.35}figure.snip1477.hover .title div:after,figure.snip1477.hover .title div:before,figure.snip1477.hover .title:after,figure.snip1477.hover .title:before,figure.snip1477:hover .title div:after,figure.snip1477:hover .title div:before,figure.snip1477:hover .title:after,figure.snip1477:hover .title:before{-webkit-transform:translate(0,0);transform:translate(0,0)}figure.snip1477.hover .title:after,figure.snip1477.hover .title:before,figure.snip1477:hover .title:after,figure.snip1477:hover .title:before{-webkit-transition-delay:.15s;transition-delay:.15s}figure.snip1477.hover figcaption,figure.snip1477:hover figcaption{opacity:1;-webkit-transition-delay:.2s;transition-delay:.2s}/* Demo purposes only */ /*Calendario Edwin*/#calendar-alertas .numd,#calendar-alertas .title{width:14.2%;text-align:center;max-width:35px!important;float:left}#calendar-alertas .ano h2{font-weight:400;font-size:40px;writing-mode:vertical-rl;text-orientation:upright;margin:0;border:1px solid #ccc;color:#0033b3}#calendar-alertas .numd{min-height:30px;position:relative}#calendar-alertas .title{color:#0033b3;font-weight:500}#calendar-alertas .dias{float:left;width:80%;max-width:245px!important}#calendar-alertas .mes{width:10%!important;float:left;padding-top:50px}#calendar-alertas .meses{width:30%;padding-top:20px;max-width:285px!important}#calendar-alertas .mes h3{color:#626e7a;text-transform:uppercase;writing-mode:vertical-rl;text-orientation:initial;transform:rotate(180deg);margin:0!important;padding:0!important;font-weight:300}#calendar-alertas .ano{position:absolute;left:0;transform:translateY(-50%)!important;top:50%!important}#calendar-alertas .calendar-add-dias{padding-left:70px!important}#calendar-alertas .numd div{width:44%;height:13px;float:left;margin:1px 2%}#calendar-alertas .abajoderecha{ position: absolute;  bottom: 0px;  left: 0px;}#calendar-alertas .numd div { width: 44%;height: 13px;float: left;margin: 1px 2%;}#calendar-alertas .numd {float: left;width: 14.2%;text-align: center;min-height: 30px;position: relative;max-width: 35px !important;}/*Fin Calendario Edwin*/
.calendario,.out{display:none}.alerta-dia,.breadcrumb li{cursor:pointer}.h-fijo{height:44px}.date-select{height:auto!important;font-size:24px;border:0;background-color:transparent}.btn:hover{color:#fff!important}.img-avatar{width:50px;border-radius:200px;-moz-border-radius:200px;-webkit-border-radius:200px 200px 200px 200px;border:0 solid #000}.table thead tr th{text-align:center}.table tr:nth-child(odd){background-color:#051d6033}.table tr:nth-child(even){background-color:#f1f1f1}.sombra{-webkit-box-shadow:4px 5px 27px -1px rgba(77,98,186,1);-moz-box-shadow:4px 5px 27px -1px rgba(77,98,186,1);box-shadow:4px 5px 27px -1px rgba(77,98,186,1);width:100%}.btn-acciones a{font-size:1.6rem;cursor:pointer}/*Eliminar alerta que sale en el editor de texto*/.mce-notification.mce-has-close {display: none;}/*Fin Eliminar alerta que sale en el editor de texto*/
</style>
{{csrf_field()}}
<div class="row">
    <div class="row" id="titulo">
        <div class="col-md-12 visible-md visible-lg">
            <div class="content">
              <div class="content__container">
                <p class="content__container__text">
                  Equipos Vendidos
                </p>
                <ul class="content__container__list text-center">
                  <li class="content__container__list__item">Calendario</li>
                  <li class="content__container__list__item">Usuario Comercial</li>
                  <li class="content__container__list__item">Alertas</li>
                  <li class="content__container__list__item">Comentarios</li>
                </ul>
              </div>
            </div>
        </div>
    </div>
</div>
@include('maquinasvendidas.calendarioalertas.matriz')
@include('maquinasvendidas.calendarioalertas.calendario')
@include('maquinasvendidas.calendarioalertas.listadomaquinasvendidas')

<div class="modal modal-wide fade p-4" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal">Cargando...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body px-0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="fa-3x">
                               <img src="/images/carga.gif" style="width:80px;">
                                <p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="/js/parsley.min.js"></script>
<script src="/assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
    var CSRF_TOKEN = $('input[name=_token]').val();
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip()
        lista_productos();
        activar_editor_texto();
    });
    /**
     * Función para traer el listado de productos
     */
    function lista_productos(){
        $('#exampleModal').modal('show');
        var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 0
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#contenido_productos').html(e.data['html']);
                  $('#total_vendidos').html(`<i class="fa fa-plus-circle"></i> `+e.data['total']);
                  $('#total_anoactual').html(`<i class="fa fa-plus-circle"></i> `+e.data['total_ano_actual']);
                  $('#total_anoanteriores').html(`<i class="fa fa-plus-circle"></i> `+e.data['total_ano_anterior']);
                  $('#exampleModal').modal('hide');
              }
        });
    }
    /*Año Seleccionado*/
    $('#fecha_alerta').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        time: false,
        clearButton: true,
        nowButton: true,
        lang: 'es',
        cancelText : 'Cancelar',
        okText: 'Aceptar',
        nowText: 'Nuevo',
        clearText: 'Limpiar'
    });
    $(document).on('change','#ano_filtro',function(){
       var ano_filtro = $(this).val();

        var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 6,
                  producto:$('#nombre_producto_seleccionado').val()
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  funCrearCalendar(ano_filtro,e.data['alertas']);
              }
        });
    });

    /**
     * Función para abrir el calendario del producto
     * @param STRING producto PRODUCTO SELECCIONADO
     */
    function abrir_producto(producto){
        $('.calendario').fadeIn(2000);
        $('.matriz').fadeOut(1000);
        var html = `<li><i class="fa fa-flag"></i><a href="">Equipos Vendidos</a></li><li href="#Matriz" onclick="abrir_matriz()">Matriz</li><li class="active" href="#Calendario">Calendario</li>`;
        $('.breadcrumb').html(html);
        $('#nombre_producto_seleccionado').val(producto);
        alerta_dia(producto);
        list_maquinas_vendidas();
    }
    /**
     * Función para volver a la matriz de productos
     */
    function abrir_matriz(){
        $('.matriz').fadeIn(2000);
        $('.calendario').fadeOut(1000);
        var html = `<li><i class="fa fa-flag"></i><a href="">Equipos Vendidos</a></li><li class="active" href="#Matriz">Matriz</li>`;
        $('.breadcrumb').html(html);
    }

    /**
     * Función para traer todos los productos vendidos
     */
    function list_maquinas_vendidas(){
        var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 1
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  insertar_datos(e.data['maquinas_vendidas'],e.data['permiso_crear_alerta'])
              }
        });
    }

    function insertar_datos(datos,permiso){
        var i = 0;
        $('#contenido_tabla').html('');
        $.each(datos, function(key, value){
            if(value.PRODUCTO == $('#nombre_producto_seleccionado').val()){
                i++;
                 var template = $("#row").html();
                 var $template = $(template);
                 $template.find("[data-item]").html(i);
                 $template.find("[data-item]").attr( 'data-item', i );
                 if(value.EMPRESALOGO === null){
                   FotoEmpresa = 'https://placeholdit.imgix.net/~text?txtsize=8&txt=Sin%20Logo&w=50&h=50';
                 }else{
                   FotoEmpresa = value.EMPRESALOGO;
                 }
                 $template.find("[data-empresa]").html('<img src="'+FotoEmpresa+'" class="img-avatar"><br>'+value.EMPRESA);
                 $template.find("[data-empresa]").attr( 'data-empresa', value.EMPRESA);
                 if(value.FOTOVENDEDOR === null || value.FOTOVENDEDOR == "http://via.placeholder.com/50x50"){
                   FotoVendedor = 'https://placeholdit.imgix.net/~text?txtsize=8&txt=Sin%20Foto&w=50&h=50';
                 }else{
                   FotoVendedor = '/images/file/clientes/'+value.FOTOVENDEDOR;
                 }
                 if(value.VENDEDOR == null){
                   value.VENDEDOR = "Vendedor no registrado";
                 }
                 $template.find("[data-vendedor]").html('<img src="'+FotoVendedor+'" class="img-avatar"><br>'+value.VENDEDOR);
                 $template.find("[data-vendedor]").attr( 'data-vendedor', value.VENDEDOR);
                 $template.find("[data-producto]").html(value.PRODUCTO);
                 $template.find("[data-producto]").attr( 'data-producto', value.PRODUCTO );
                 $template.find("[data-referencia]").html(value.REFERENCIA);
                 $template.find("[data-referencia]").attr( 'data-referencia', value.REFERENCIA );
                 $template.find("[data-pais]").html(value.PAIS);
                 $template.find("[data-pais]").attr( 'data-pais', value.PAIS );
                 $template.find("[data-ano]").html(value.ANO);
                 $template.find("[data-ano]").attr( 'data-ano', value.ANO );
                 $template.find("[data-alertas]").html('<a class="text-danger" onclick="ver_alerta('+value.TIPO+','+value.ID+')"><i class="fa fa-bell"></i></a><p class="h6"><em>'+value.ALERTAS+'</em></p>');
                 $template.find("[data-alertas]").attr( 'data-alertas', value.ALERTAS );
                 /*Permiso Crear alerta*/
                 if(permiso=='Si'){
                  $template.find("[data-accion]").html('<a class="text-success" data-toggle="tooltip" data-placement="top" title="Agregar Alerta" onclick="add_alerta('+value.TIPO+','+value.ID+')"><i class="fa fa-plus"></i></a>');
                }else{
                  $template.find("[data-accion]").html('<a class="text-success" data-toggle="tooltip" data-placement="top" title="No tiene permiso para agregar alerta" style="opacity:0.5;cursor:no-drop"><i class="fa fa-plus"></i></a>');
                }
                 $template.appendTo($(".table tbody"));
            }
       });
        $('[data-toggle="tooltip"]').tooltip();
    }

    $(document).on('keyup','#filtro_tabla',function(){
        $('#contenido_tabla tr').each(function(){
            $(this).addClass('out');
            /*Empresa*/
            filtro = $('#filtro_tabla').val();
            if(filtro != ""){

                buscar = filtro.toUpperCase();
                empresa = $(this).context.cells[1].dataset.empresa;
                empresa = empresa.toUpperCase();
                encontradas = empresa.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }

            /*Vendedor*/

                vendedor = $(this).context.cells[2].dataset.vendedor;
                vendedor = vendedor.toUpperCase();
                encontradas = vendedor.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }

            /*Producto*/

                producto = $(this).context.cells[3].dataset.producto;
                producto = producto.toUpperCase();
                encontradas = producto.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }

            /*Referencia*/

                referencia = $(this).context.cells[4].dataset.referencia;
                referencia = referencia.toUpperCase();
                encontradas = referencia.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }

            /*Pais*/

                pais = $(this).context.cells[5].dataset.pais;
                pais = pais.toUpperCase();
                encontradas = pais.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }

            /*Año*/


                ano = $(this).context.cells[6].dataset.ano;
                encontradas = ano.search(filtro);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }
            }else{
                $(this).removeClass("out");
            }
        })
    })

    /**
     * Función para agregar una alerta
     */
    function add_alerta(tipo,id){
        $('#crearAlerta').modal('show');
        /*lleno los campos ocultos del formulario*/
        $('#id_alerta').val('');
        $('#tipo_alerta').val(tipo);
        if(tipo == 0){
            $('#id_oportunidad_producto').val(id);
            $('#id_maquinas_vendidas').val('');
        }else{
            $('#id_oportunidad_producto').val('');
            $('#id_maquinas_vendidas').val(id);
        }
    }

    /**
     * Función para mostrar la fecha en formato dia/Nombre del mes/año
     * @param STRING id_campo   IDENTIFICACION DEL CAMPO DE FECHA
     * @param STRING id_mostrar IDENTIFICACIÓN DEL CAMPO DONDE SE MUESTRA
     * @param DATE valor FECHA SELECCIONADA
     */
    function fecha_seleccionada(id_campo,id_mostrar,valor){
        var fecha = valor.split("-");
        var meses = ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        var fecha_formato = fecha[2]+'/'+meses[parseInt(fecha[1])]+'/'+fecha[0];
        $(id_mostrar+' input').val(fecha_formato);
        $(id_mostrar).css('display','block');
        $('#'+id_campo).css('display','none');
    }

    /**
     * Funcion para editar la fecha
     * @param STRING id_esconder IDENTIFICACION DEL CAMPO QUE ESTA MOSTRANDO LA FECHA CON FORMATO DD/MES/YYYY
     * @param STRING id_mostrar  IDENTIFICACION DEL CAMPO QUE CAPTURA LA FECHA
     */
    function editar_fecha(id_esconder, id_mostrar){
        $(id_mostrar).css('display','block');
        $(id_mostrar).val('');
        $(id_esconder).css('display','none');
    }

    /**
     * Función para guardar una alerta
     */
    function guardar(){
        event.preventDefault();
        if(true === $("#formulario").parsley().validate()){
            swal({
                title: 'Esta seguro?',
                text: "Desea crear una alerta",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, guardar!'
                }).then(function () {
                    tinymce.remove();
                    var jqxhr = $.post("/maquinasvendidas-calendario-action", $("#formulario").serialize())
                    .done(function(e) {
                        if(e.data['msj'] == "Guardado correctamente!"){
                            swal({
                                title: 'Guardado',
                                text: "Guardado correctamente",
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'OK'
                            }).then(function () {
                                list_maquinas_vendidas();
                                document.getElementById("formulario").reset();
                                $('#filtro_tabla').val('');
                                $('#crearAlerta').modal('hide');
                                $('.editar-fecha').click();
                                activar_editor_texto();
                                alerta_dia($('#nombre_producto_seleccionado').val());
                            });
                        }else{
                            swal(
                                  'Error!',
                                  e.data['msj'],
                                  'error'
                                );
                            activar_editor_texto();
                        }
                    })
                    .fail(function(e) {
                        console.error(e)
                    })
                    .always(function(e) {
                      console.log(e)
                    });
                });
        }else{
            swal('Faltan Campos por llenar');
        }
    }

    /**
     * Función para ver las alertas del producto
     * @param INT tipo 0:PERTENECE A UNA OPORTUNIDAD SMART 1:PERTENECE A LA TABLA maquinas_vendidas
     * @param INT id   IDENTIFICACION DEL PRODUCTO
     */
    function ver_alerta(tipo,id){
        $('#VerAlerta').modal('show');
        $('#listado_alertas').html(`<div class="container"><div class="row"><div class="col-md-12 text-center"><div class="fa-3x"><img src="/images/carga.gif" style="width:80px;"><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div></div></div>`);
        var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 3,
                  tipo: tipo,
                  id: id
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#listado_alertas').html(e.data['html']);
              }
        });
    }

    $(document).on('click','.alerta-dia',function(){
       var fecha = $(this).attr('data-fecha');
        ver_alerta_fecha(fecha);
    });

    /**
     * Función para ver las alertas del dia seleccionado en el calendario
     * @param DATE fecha LA FECHA SELECCIONADA
     */
    function ver_alerta_fecha(fecha){
        $('#VerAlerta').modal('show');
        $('#listado_alertas').html(`<div class="container"><div class="row"><div class="col-md-12 text-center"><div class="fa-3x"><img src="/images/carga.gif" style="width:80px;"><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div></div></div>`);
        var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 7,
                  fecha: fecha,
                  producto: $('#nombre_producto_seleccionado').val()
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#listado_alertas').html(e.data['html']);
              }
        });
    }

    var comentario = '';
    $(document).on('keyup','#comentario_alerta',function(){
        comentario = $(this).val();
    });

    /**
     * Función para ejecutar las subfuncioes (EDITAR, ELIMINAR, COMENTAR)
     * @param STRING accion (EDITAR, ELIMINAR, COMENTAR)
     * @param INT id     IDENTIFICACIÓN DE LA TABLA oportunidades_alertas
     */
    function accion_alerta(accion,id){
        switch(accion){
            case 'editar':
                $('#crearAlerta').modal('show');
                $('#VerAlerta').modal('hide');
                var jqxhr = $.ajax({
                      url: "/maquinasvendidas-calendario-action",
                      data: {
                          _token: CSRF_TOKEN,
                          action: 4,
                          id: id
                      },
                      cache: false,
                      type: 'POST',
                      success: function(e){
                          tinymce.remove();
                          $('#id_alerta').val(e.data['alerta']['id']);
                          $('#tipo_alerta').val(e.data['alerta']['tipo']);
                          $('#id_oportunidad_producto').val(e.data['alerta']['id_oportunidad_producto']);
                          $('#id_maquinas_vendidas').val(e.data['alerta']['id_maquinas_vendidas']);
                          $('#id_user_creador').val(e.data['alerta']['id_user_creador']);
                          $('#fecha_alerta').val(e.data['alerta']['fecha_ejecucion']);
                          $('#titulo_alerta').val(e.data['alerta']['titulo']);
                          $('#descripcion_alerta').val(e.data['alerta']['descripcion']);
                          activar_editor_texto();
                      }
                });
                break;
            case 'eliminar':
                swal({
                      title: 'Esta seguro?',
                      text: "Desea eliminar el funcionario",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Si, Eliminar!'
                    }).then(function(){
                        var jqxhr = $.ajax({
                                url: "/maquinasvendidas-calendario-action",
                                data: {
                                    _token: CSRF_TOKEN,
                                    action: 5,
                                    id: id
                                },
                                cache: false,
                                type: 'POST',
                                success: function(e){
                                    if(e.data['msj'] == 'Eliminado correctamente!'){
                                       swal(
                                          'Eliminado!',
                                           e.data['msj'],
                                          'success'
                                        ).then(function(){
                                           ver_alerta(e.data['tipo'],e.data['id']);
                                           list_maquinas_vendidas();
                                       });
                                    }else{
                                         swal(
                                          'Error!',
                                           e.data['msj'],
                                          'error'
                                        );
                                    }
                                }
                        });
                    });
                break;
            case 'comentario':
                var jqxhr = $.ajax({
                      url: "/maquinasvendidas-calendario-action",
                      data: {
                          _token: CSRF_TOKEN,
                          action: 8,
                          id: id
                      },
                      cache: false,
                      type: 'POST',
                      success: function(e){
                          $('#VerAlerta').modal('hide');
                          comentario = '';
                          swal({
                              title: 'Comentarios',
                              type: 'info',
                              html:e.data['html'],
                              showCloseButton: true,
                              showCancelButton: true,
                              confirmButtonText:
                                '<i class="fa fa-save"></i> Guardar!',
                              cancelButtonText:
                              '<a class="Cerrar-alertas"><i class="fa fa-close"></i> Cancelar</a>'
                            }).then(function(){
                              if(comentario == ''){
                                 swal(
                                      'Advertencia',
                                      'Debe ingresar un comentario',
                                      'warning'
                                    ).then(function(){
                                     accion_alerta(accion,id);
                                 });
                                 }else{
                                     guardarcomentario(comentario,id);

                                 }
                          });
                          $('.swal2-info').html('<i class="fa fa-comment"></i>');
                        $('.swal2-show').css('width','800px');
                      }
                });
                break;
        }
    }

    /**
     * Función para guardar el comentario
     * @param STRING comentario COMENTARIO A GUARDAR
     * @param INT id         IDENTIFICACIÓN DE LA ALERTA
     */
    function guardarcomentario(comentario,id){
        var jqxhr = $.ajax({
              url: "/maquinasvendidas-calendario-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 9,
                  comentario: comentario,
                  id:id
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  if(e.data['msj'] == 'Guardado correctamente!'){
                     swal(
                          'Guardado',
                          e.data['msj'],
                          'success'
                        ).then(function(){
                         accion_alerta("comentario",id);
                     });
                  }else{
                      swal(
                          'Error',
                          e.data['msj'],
                          'error'
                        ).then(function(){
                         accion_alerta("comentario",id);
                     });
                  }
              }
        });
    }
    $(document).on('click','.Cerrar-alertas',function(){
        $('#VerAlerta').modal('show');
    });
    /**
     * Funcion para activar tinymce
     */
    function activar_editor_texto(){
        tinymce.remove();
        tinymce.init({
            selector:'#descripcion_alerta',
            height: 500,
            language : "es",
            theme: 'modern',
            branding: false,
            plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern'
        });
    }
</script>
@endsection
