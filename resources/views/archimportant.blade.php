@extends('template.app')
@section('title', 'Archivos Importantes')

@section('content')
<!--<link href="{{ url('/') }}/js/plugins/sweetalert/sweetalert.css" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/css/mdb.min.css" crossorigin="anonymous">-->
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
<style type="text/css">
	.jumbotron{
	    height: 100px;
	    padding-top: 10px;
  	}

  	.pull-right{
    	padding-top: 15px;
  	}

  	.thumbnail {
	    position: relative;
	    padding: 0px;
	    margin-bottom: 20px;
	}

	.thumbnail > h4 {
	    padding: 7px 5px 0px;
	    white-space: nowrap;
	    overflow: hidden;
	    text-overflow: ellipsis;
	}

	.thumbnail h4 .info {
	    position: absolute;
	    top: 0px;
	    right: 0px;
	    font-size: 0.6em;
	    padding-left: 15px;
	    border-top-right-radius: 3px;
	    border-bottom-left-radius: 4px;
	    border-radius: 0px;
	    border-bottom-left-radius: 5px;
	    cursor:  pointer;
	}

	.thumbnail h4 .info > span {
	    margin-right: 10px;   
	}

	.thumbnail img {
	    width: 100%;
	}

	.thumbnail a.btn {
	    border-top-left-radius: 0px;
	    border-top-right-radius: 0px;
	}

	.imgmen{
		max-width: 100px;
	    max-height: 100px;
	    min-width: 100px;
	    min-height: 100px;
	}

	.text-muted{
		text-align: center;
	}

	.mdb-feed .news .excerpt {
	    display: block;
	    -webkit-box-flex: 1;
	    -webkit-flex: 1 1 auto;
	    -ms-flex: 1 1 auto;
	    flex: 1 1 auto;
	    -webkit-align-self: stretch;
	    -ms-flex-item-align: stretch;
	    -ms-grid-row-align: stretch;
	    align-self: stretch;
	    word-wrap: break-word;
	    margin: 0 0 1.2rem 1.2rem;
	}

	.mdb-feed .news .excerpt .brief {
	    padding-bottom: .5rem;
	    font-weight: 500;
	}

	.mdb-feed .news .excerpt .added-text {
	    margin-bottom: .6rem;
	}

	.mdb-feed .news .label {
	    display: block;
	    -webkit-box-flex: 0;
	    -webkit-flex: 0 0 auto;
	    -ms-flex: 0 0 auto;
	    flex: 0 0 auto;
	    -webkit-align-self: stretch;
	    -ms-flex-item-align: stretch;
	    -ms-grid-row-align: stretch;
	    align-self: stretch;
	    width: 2.5rem;
	}
	.titulogeneral {
	    text-align: left;
    	padding-left: 20px;
    	color: #555;
	}

	.form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }

    #imgStado{
        border: 0 !important;
        position: absolute;
        right: 16px;
        top: 16px;
    }

    .form-control.file-caption.kv-fileinput-caption {
	    height: 54px;
	}
    .ndfHFb-c4YZDc-Wrql6b {
        display: none !important;
    }
</style>
<div class="widget">
    <div class="pull-right">
      <button type="button" class="btn-sm btn btn-secondary" onclick="getCategorias()"><i class="fa fa-th-large"></i> Ver Categorias</button>
    </div>
    <h2 class="titulogeneral">Archivos Importantes</h2>
    <p class="letra-gris titulogeneral" id="subtitlecat" style="margin-bottom: 0px;">Este es un espacio para alojar archivos importantes de consulta abierta</p>


</div>
<div class="widget">
<div class="container">
    <div class="row" id="htmlArchi"></div>
</div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <img src="{{ url('/') }}/images/logo.png" class="logo-essi-video"/>
    <div id="modal_video" class="modal-content" style="border: 0; background: rgba(240, 248, 255, 0);">
    </div>
  </div>
</div>

<div class="modal fade" id="modalArte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
				<h4 class="modal-title ml-5 mt-3" id="myModalLabel">Estado del arte</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body" >
				<div class="form-group row block" >
					<div class="col-md-12">
						<div class="panel-title" id="contenArte"></div>
                        <hr><br>
                        <!--Section: Social Newsfeed-->
						<section>
						    <div class="row">
						        <div class="col-md-12" id="lisComentariosId" style="text-align: left;">
						        </div>
						    </div>
						</section>
						<!--/Section: Social Newsfeed-->
						<hr><br>
						
						<form class="form-inline" id="formComentarios"></form>
					</div>
				</div>
			</div>    
		</div>
	</div>
</div>

<div class="modal fade" id="modalCategorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
				<h4 class="modal-title" id="myModalLabel">Categorias</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body" >
				<div class="form-group row block" >
					<div class="col-md-12">
						<form method="POST" id="formCategoria" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group row">
		                        <div class="col-6" style="justify-content: center;">
		                            <label for="nombre" class="col-form-label">Imagen de la categoria</label>
						            <div class="dropzone" data-width="320" data-height="180" data-ajax="false" data-originalsave="true" style="width: 320px;height: 180px;min-width: 320px !important;min-height: 180px !important;max-width: 320px !important;max-height: 180px !important;">
				                        <input type="file" name="foto" accept="image/gif, image/jpeg, image/png">
				                    </div>
		                            <!--<input id="input-1" type="file" class="file" name="foto" accept="image/gif, image/jpeg, image/png">-->
		                        </div>
		                        <div class="col-6" style="justify-content: center;">
		                            <label for="nombre" class="col-form-label">Categoria</label>
		                            <input class="form-control" type="text" name="nombre" placeholder="Por favor ingrese el nombre de la categoria" required>
		                        </div>
		                    </div>
		                    <button type="button" class="btn btn-essi pull-right" onclick="guardarCategoria()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
						</form>
					</div>
				</div>
			</div>    
		</div>
	</div>
</div>

<div class="modal fade" id="modalUploadfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
				<h4 class="modal-title" id="myModalLabel">Archivos</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body" >
				<div class="form-group row block" >
					<div class="col-md-12">
						<form method="POST" id="formArchivo" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group row">
		                        <div class="col-6" style="justify-content: center;">
		                            <label for="nombre" class="col-form-label">Archivo</label>
									<input id="input-1" type="file" class="file" name="file" required>
		                        </div>
		                        <div class="col-6" style="justify-content: center;">
		                            <input type="hidden" name="cat_id" id="txtCategoria" required>
		                        	<label for="nombre" class="col-form-label">Nombre de archivo</label>
		                            <input type="text" name="nombre" class="form-control" placeholder="Nombre" required>
		                            <label for="nombre" class="col-form-label">Comentario</label>
		                            <textarea class="form-control" name="comentario" rows="5" placeholder="Comentario" required></textarea>
		                        </div>
		                    </div>
		                    <button type="button" class="btn btn-essi pull-right" onclick="guardarArchivo()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
						</form>
					</div>
				</div>
			</div>    
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/js/mdb.min.js" crossorigin="anonymous"></script>-->
	<!--<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
  	<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
  	<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
  	<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
  	<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>-->
  	<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>

  	<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
	<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
	<script src="https://momentjs.com/downloads/moment-with-locales.js"></script> 
  	<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
	<!--sweetalert -->
	<!--<script type="text/javascript" src="{{ url('/') }}/js/plugins/sweetalert/sweetalert.min.js"></script> --> 

  	<script type="text/javascript">
  		direccion="{{ url('/') }}";
        $("#input-1").fileinput({
                language: "es",
                maxFileCount: 10,
                showUpload: false
                //allowedFileExtensions: ["jpg", "gif", "png", "txt"]
            });
  		$(function() {
  			 $('.dropzone').html5imageupload();
  			getCategorias();
  		});
  		abrirmodal = function(item) {
  			$($(item).data("target")).modal();
  			 $('.dropzone').html5imageupload();
  		}
  		getCategorias = function () {
  			$.ajax({
			    url: "{{ url('listcatarchivos') }}",
			    type: 'GET',
			    success: function(data){
			    	if (data.success) {
			    		var html = "";
			    		
			    		for(i in data.datos){
			    			html += `<div class="col-sm-4 col-md-3">
						            <div class="thumbnail">
						                <div class="view hm-zoom">`;
			                if (data.datos[i].foto !== null) {
			                	html +=`<img src="{{ url('/')}}/images/file/categoriaarchivo/`+data.datos[i].foto+`" class="img-fluid " alt="">`;
			                }else{
			                	html +=`<img src="http://via.placeholder.com/320x180?text=`+data.datos[i].nombre+`" class="img-fluid " alt="">`;
			                }
						                
						                html +=`</div>
						                <button class="btn btn-primary col-xs-12" role="button" onclick="getArchivos(`+ data.datos[i].id +`)"><div class="pull-left">`+ data.datos[i].nombre +`</div> <div class="pull-right label label-default">`+ data.datos[i].archivo +` archivos</div></button>
						                <div class="clearfix"></div>
						            </div>
						        </div>`;
			    		}
			    		
			    		$('#htmlArchi').html(html);
			    	}else{
			    		swal("Algo salio mal, vuelve a intentar");
			    	}
			    }
			});
  		}

  		guardarComentario = function (id) {
		  event.preventDefault();    
		  if(true === $("#formComentarios").parsley().validate()){
		  	console.log("llega"); 
	    	var datos = new FormData($("#formComentarios")[0]);
	    	$.ajax({
			    url: "{{ url('savefilecomen') }}",
			    data: datos,
			    cache: false,
			    contentType: false,
			    processData: false,
			    type: 'POST',
			    success: function(data){
			    	if (data.success) {
			    		$('#formComentarios')[0].reset();
			    		getComentarios(id);
			    	}else{
			    		swal("Algo salio mal, vuelve a intentar");
			    	}
			    }
			}); 
		  }else{
		    swal("Faltan campos por completar!");
		  }
		}

  		guardarCategoria = function () {
		  event.preventDefault();    
		  if(true === $("#formCategoria").parsley().validate()){
		  	console.log("llega");
		    swal({   title: "Esta Seguro?",   
		      	text: "Va a guardar esta información",   
		      	type: "info",  
		      	showCancelButton: true,   
		      	showLoaderOnConfirm: true,
		      	animation: "slide-from-top",
			  	confirmButtonText: 'Guardar',
			  	cancelButtonText: 'Cerrar',
		      	preConfirm: function () {
				    return new Promise(function (resolve, reject) {
					    var datos = new FormData($("#formCategoria")[0]);
		    	
				    	var request = $.ajax({
						    url: "{{ url('savearchivosimportantes') }}",
						    data: datos,
						    cache: false,
						    contentType: false,
						    processData: false,
						    type: 'POST'
						});		
									 
						request.done(function( msg ) {console.log("ya");
						  	resolve(msg.success)
						});
						 
						request.fail(function( jqXHR, textStatus ) {
							resolve(false)
						});
				    })
			  	},
		    }).then(function(data){ 
		    	if (data) {
		    		$('#formCategoria')[0].reset();
		    		$("#modalCategorias").modal('hide');
		    		getCategorias();
		    		swal("Categoria guardada con exito");		    		
		    	}else{
		    		swal("Algo salio mal, vuelve a intentar");
		    	}
		    }).catch(swal.noop);  
		  }else{
		    swal("Faltan campos por completar!");

		  }
		}

        getArchivos = function(id) {
            console.log('getArchivos ',id);
			$.ajax({
			    url: "{{ url('listarchivos') }}/"+id,
			    type: 'GET',
			    success: function(data){
			    	if (data.success) {
			    		var html = "";
			    		var images = "";
						var videos = "";
						var presentacion = "";
						var pdf = "";
						var documentos = "";
						var excel = "";
						var otro = "";
						$("#subtitlecat").html(data.cate.nombre);
			    		
			    		for(i in data.datos){
			    			var file = data.datos[i].file;
			    			var nombre = data.datos[i].nombre;
			    			var contenido = data.datos[i];
			                total = file.length;
			                c=total-1;
			                b = c-1;
			                a = b-1;
			                extension = file.substr(a,b,c);
			                if (extension === "BMP" || extension === "GIF" || extension === "PNG" || extension === "JPG" || extension === "PEG" || extension === "TIF" || extension === "bmp" || extension === "gif" || extension === "png" || extension === "jpg" || extension === "peg" || extension === "tif") {
			                    images += ` <div class="col-3 col-sm-2 placeholder">                                
						                      	<img src="{{url('/')}}/aimportant/`+file+`" class="img-fluid mx-auto d-block imgmen rotate" data-toggle="modal" data-target="#modalArte" onclick="abrirmodal(this),openArte('`+contenido.nombre+`', '`+contenido.file+`', '`+contenido.comentario.replace(/[^a-zA-Z 0-9.]+/g,' ')+`', '`+contenido.created_at+`', '`+contenido.user_id+`', '{{url('/')}}/aimportant/`+file+`', `+contenido.id+`, true)">
						                      <div class="text-muted">`+nombre+` <span class="badge element-bg-color-green">`+ contenido.comentarios +` <small>Comentarios</small></span></div>
						                    </div>`;
			                }

			               	if (extension === "AVI" || extension === "MOV" || extension === "WMV" || extension === "FLv" || extension === "MP4" || extension === "avi" || extension === "mov" || extension === "wmv" || extension === "flv" || extension === "mp4") {
			                    videos += `<div class="col-3 col-sm-2 placeholder">  
	                                         <img src="../images/video.png" class="img-fluid mx-auto d-block imgmen" alt="Icono Generico" data-toggle="modal" data-target="#modalArte" onclick="openArte('abrirmodal(this), `+contenido.nombre+`', '`+contenido.file+`', '`+contenido.comentario.replace(/[^a-zA-Z 0-9.]+/g,' ')+`', '`+contenido.created_at+`', '`+contenido.user_id+`', '../images/video.png', `+contenido.id+`)">
	                                         <div class="text-muted">`+nombre+` <span class="badge element-bg-color-green">`+ contenido.comentarios +` <small>Comentarios</small></span></div>
		                    				</div>`;
			                }

			                if (extension === "PTX" || extension === "PSX" || extension === "PSM" || extension === "ptx" || extension === "psx" || extension === "psm") {
			                    presentacion += `<div class="col-3 col-sm-2 placeholder">
	                                                 <img src="../images/powerpoint.png" class="img-fluid mx-auto d-block imgmen" alt="Icono Generico" data-target="#modalArte" onclick="abrirmodal(this), openArte('`+contenido.nombre+`', '`+contenido.file+`', '`+contenido.comentario.replace(/[^a-zA-Z 0-9.]+/g,' ')+`', '`+contenido.created_at+`', '`+contenido.user_id+`', '../images/powerpoint.png', `+contenido.id+`)">
	                                                 <div class="text-muted">`+nombre+` <span class="badge element-bg-color-green">`+ contenido.comentarios +` <small>Comentarios</small></span></div>
			                    				</div>`;
			                }

			                if (extension === "PDF" || extension === "pdf"){
			                    pdf += `<div class="col-3 col-sm-2 placeholder">  
	                                         <img src="../images/pdf.png" class="img-fluid mx-auto d-block imgmen" alt="Icono Generico" data-toggle="modal" data-target="#modalArte" onclick="abrirmodal(this), openArte('`+contenido.nombre+`', '`+contenido.file+`', '`+contenido.comentario.replace(/[^a-zA-Z 0-9.]+/g,' ')+`', '`+contenido.created_at+`', '`+contenido.user_id+`', '../images/pdf.png', `+contenido.id+`)">
	                                         <div class="text-muted">`+nombre+` <span class="badge element-bg-color-green">`+ contenido.comentarios +` <small>Comentarios</small></span></div>
			                            </div>`;
			                }

			                if (extension === "OCX" || extension === "ocx" || extension === "DOC" || extension === "doc"){
			                    documentos += `<div class="col-3 col-sm-2 placeholder">           
	                                                <img src="../images/word.png" class="img-fluid mx-auto d-block imgmen" alt="Icono Generico" data-toggle="modal" data-target="#modalArte" onclick="abrirmodal(this), openArte('`+contenido.nombre+`', '`+contenido.file+`', '`+contenido.comentario.replace(/[^a-zA-Z 0-9.]+/g,' ')+`', '`+contenido.created_at+`', '`+contenido.user_id+`', '../images/word.png', `+contenido.id+`)">
	                                                <div class="text-muted">`+nombre+` <span class="badge element-bg-color-green">`+ contenido.comentarios +` <small>Comentarios</small></span></div>
			                                    </div>`;
			                }

			                if (extension === "LSX" || extension === "lsx" || extension === "LSM" || extension === "lsm" || extension === "XML" || extension === "xml"){
			                    excel += `<div class="col-3 col-sm-2 placeholder">  
	                                        <img src="../images/excel.png" class="img-fluid mx-auto d-block imgmen" alt="Icono Generico" data-toggle="modal" data-target="#modalArte" onclick="abrirmodal(this), openArte('`+contenido.nombre+`', '`+contenido.file+`', '`+contenido.comentario.replace(/[^a-zA-Z 0-9.]+/g,' ')+`', '`+contenido.created_at+`', '`+contenido.user_id+`', '../images/excel.png', `+contenido.id+`)">
	                                        <div class="text-muted">`+nombre+` <span class="badge element-bg-color-green">`+ contenido.comentarios +` <small>Comentarios</small></span></div>
			                             </div>`;
			                }
			    		}
			    		otro = `<div class="col-3 col-sm-2 placeholder">
			    					<img src="https://cdn2.iconfinder.com/data/icons/edit/100/edit-set-1-512.png" class="img-fluid mx-auto d-block imgmen" alt="Icono Generico" <?php if($data['permiso_agregar'] == "Si"){ ?> data-toggle="modal" data-target="#modalUploadfile" onclick="abrirmodal(this), addIdCat(`+id+`)"<?php }else{ ?>onclick="no_permiso('No tiene permisos para agregar un archivo')"<?php } ?>>
	    							<div class="text-muted">Nuevo Archivo</div>
	                            </div>`;
			    		$('#htmlArchi').html(images+videos+presentacion+pdf+documentos+excel+otro);
			    	}else{
			    		swal("Algo salio mal, vuelve a intentar");
			    	}
			    }
			});
		}

		guardarArchivo = function () {
		  event.preventDefault();
		  if(true === $("#formArchivo").parsley().validate()){
		  	console.log("llega");
		    swal({
		    	title: "Esta Seguro?",
		      	text: "Va a guardar esta información",
		      	type: "info",
		      	showCancelButton: true,
		      	showLoaderOnConfirm: true,
		      	animation: "slide-from-top",
			  	confirmButtonText: 'Guardar',
			  	cancelButtonText: 'Cerrar',
		      	preConfirm: function () {
				    return new Promise(function (resolve, reject) {
					    var datos = new FormData($("#formArchivo")[0]);
				    	var request = $.ajax({
						    url: "{{ url('savefileimportantes') }}",
						    data: datos,
						    cache: false,
						    contentType: false,
						    processData: false,
						    type: 'POST',
						});

						request.done(function( msg ) {console.log("ya");
						  	resolve(msg.success)
						});

						request.fail(function( jqXHR, textStatus ) {
							resolve(false)
						});
				    })
			  	},
		    }).then(function(data){
		    	if (data) {
		    		getArchivos($("#txtCategoria").val());
		    		$('#formArchivo')[0].reset();
		    		$('#modalUploadfile').modal('hide');
		    		swal("Archivo guardado con exito");
		    	}else{
		    		swal("Algo salio mal, vuelve a intentar");
		    	}
		    }).catch(swal.noop);
		  }else{
		    swal("Faltan campos por completar!");
		  }
		}


		openArte = function (nombre, file, comentario, created_at, user_id, foto, id, imagen) {
			console.log(nombre, file, comentario, created_at, user_id, id, imagen);
			var html = "";
			$.ajax({
			    url: "{{ url('user') }}/"+user_id,
			    type: 'GET',
			    success: function(data){
			    	if (data.success) {
			    		console.log(data.user);
			    		if (data.user.foto === "" || data.user.foto === null) {
			    			data.user.foto = `<img src="http://via.placeholder.com/320x180?text=foto" class="mx-auto d-block rounded-circle" style="height: 120px;width: 120px;">`;
			    		}else{
			    			data.user.foto = `<img src="{{url('/')}}/images/file/clientes/`+data.user.foto+`" class="mx-auto d-block rounded-circle" style="height: 120px;width: 120px;">`;
			    		}
			    		html=`<div class="pull-right" style="margin-top: 0;padding-top: 0;width: 40%;">
								`+data.user.foto+`                       
			                    <h5 style="text-align: center;"><span>
			                        `+data.user.name+`
			                    </span></h5>
			                    <p class="letra-gris" style="margin-bottom: 0px;font-size: medium;text-align: center;">
			                        `+data.user.tipo+`
			                    </p>                      
			                </div>
							<img src="`+foto+`" style="float: left;height: 140px;width: 140px;">
			                <h2><span>`+nombre+` </span></h2>                        
			                <div class="row">
			                    <label class="letra-gris">`+created_at+`</label>
			                </div>
			                <div class="row">                            
			                    <label class="letra-gris">`+comentario+`</label>
			                </div>`;
			                $("#contenArte").html(html);
			    	}else{
			    		swal("Algo salio mal, vuelve a intentar");
			    	}			    	
	    		}
	    	});

			if (!imagen) {
				html2 = `<div class="md-form form-group" style="width: 30%;margin-right: 0;">
					    <button type="button" class="btn btn-outline-default btn-rounded waves-effect" data-toggle="modal" data-target="#myModal" onclick="abrirmodal(this), openDocument('aimportant','`+file+`')"><i class="fa fa-eye"></i> Ver Archivo</button>
                        <button type="button" class="btn btn-outline-danger btn-rounded waves-effect button-delete" name="`+id+`"><i class="fa fa-trash"></i> Eliminar</button>
					</div>`;
			}else{
				html2 = `<div class="md-form form-group" style="width: 30%;margin-right: 0;">
						<a data-gallery="" title="`+file+`" href="{{url('/')}}/aimportant/`+file+`" class="btn btn-outline-default btn-rounded waves-effect"><i class="fa fa-eye"></i> Ver Archivo</a>
                        <button type="button" class="btn btn-outline-danger btn-rounded waves-effect button-delete" name="`+id+`"><i class="fa fa-trash"></i> Eliminar</button>
					</div>`;
			}

			html2 += `<div class="md-form form-group" style="width: 60%;margin-right: 0;">
						<input type="hidden" name="arch_id" value="`+id+`">
					    <input type="text" id="form92" name="comentario" class="form-control validate" placeholder="Ingrese un comentario" style="width: 100%">
					</div>

					<div class="md-form form-group" style="width: 10%;margin-right: 0;">
					    <button class="btn btn-outline-default btn-rounded waves-effect" onclick="guardarComentario(`+id+`)"><i class="fa fa-paper-plane"></i></button>
					</div>`;
			$("#formComentarios").html(html2);
			getComentarios(id);
		}

		getComentarios = function (id) {
			$.ajax({
			    url: "{{ url('listcomenarchivos') }}/"+id,
			    type: 'GET',
			    success: function(data){
			    	if (data.success) {
			    		console.log(data.datos);
			    		var html3 = "";
			    		moment.locale('es');
			    		for(i in data.datos){
			    			comentario = data.datos[i];
			    			if (comentario.user.foto === "" || comentario.user.foto === null) {
				    			comentario.user.foto = `<img src="http://via.placeholder.com/40x40?text=`+comentario.user.name+`" class="media-object img-circle">`;
				    		}else{
				    			comentario.user.foto = `<img src="{{url('/')}}/images/file/clientes/`+comentario.user.foto+`" alt="`+comentario.user.name+`" class="media-object img-circle" style="max-width:40px">`;
				    		}
				    		html3 += `
					                <div class="media clearfix header-bottom">
										<div class="media-left">
											`+comentario.user.foto+`
										</div>
										<div class="media-body">
											<a href="#">
												`+comentario.user.name+` `+moment(comentario.created_at).calendar()+`<br>
												<span class="text-muted username">`+comentario.comentario+`</span>
											</a>
										</div>
									</div>`;
		                }
		                $("#lisComentariosId").html(html3);
			    	}else{

			    	}
			    }
			});
		}

		openVideo = function(url, nombre) {
		   	dir = direccion+"/"+url+"/"+nombre;
		    video = `<div id="play_video_modal" class="caja">
		                <video id="Video1" src="`+dir+`" loop preload="auto">
		                  Tu navegador no implementa el elemento <code>video</code>.
		                </video>
		                <div id="buttonbar">
		                    <button class="video_button" id="restart" onclick="restart();"><i class="fa fa-repeat" aria-hidden="true"></i></button> 
		                    <button class="video_button" id="rew" onclick="skip(-10)"><i class="fa fa-backward" aria-hidden="true"></i></button>
		                    <button class="video_button" id="play" onclick="vidplay()"><i class="fa fa-play" aria-hidden="true"></i></button>
		                    <button class="video_button" id="fastFwd" onclick="skip(10)"><i class="fa fa-forward" aria-hidden="true"></i></button>
		                    <button class="video_button pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
		                    <button class="video_button pull-right" onclick="pantallaCompletaVideo()"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>
		                </div> 
		            <div>`;

		    $("#modal_video").html(video);
		}

		openDocument = function(url, nombre) {
			$('#modalArte').modal('hide');
		    dir = direccion+"/"+url+"/"+nombre;
		    contenido = `<iframe id="visor_pdf" src="https://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>`;
		    $("#modal_video").html(contenido);
            desactivarVentanaExterna();
		}

		function desactivarVentanaExterna(){
            $('.ndfHFb-c4YZDc-Wrql6b').remove();
        }
		           
		function vidplay() {
		   var video = document.getElementById("Video1");
		   var button = $("#play");

		   if (video.paused) {
		      video.play();
		      button.html('<i class="fa fa-pause" aria-hidden="true"></i>');
		   } else {
		      video.pause();
		      button.html('<i class="fa fa-play" aria-hidden="true"></i>');
		   }
		}

		function restart() {
		    var video = document.getElementById("Video1");
		    video.currentTime = 0;
		}

		function skip(value) {
		    var video = document.getElementById("Video1");
		    video.currentTime += value;
		}  

		$(function () {
            $('body').on('click','.button-delete',function(){
                <?php if($data['permiso_eliminar']=='Si'){ ?>
                var id = $(this).attr("name");
                swal({
                  title: '¿Estás seguro?',
                  html: $('<div>')
                    .addClass('some-class')
                    .text('¡No podrás revertir esto!'),
                  animation: false,
                  customClass: 'animated tada',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sí, borrarlo!',
                  cancelButtonText: 'Cancelar',
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true,
                  preConfirm: function () {
                    return new Promise(function (resolve) {
                      $.get( "{{ url('/') }}/archivosimportantes/eliminar/"+id, function( data ) {
                        if (data.success) {
                            $('#modalArte').modal('toggle');
                            getArchivos($("#txtCategoria").val());
                          swal('¡Eliminado!','Su Información ha sido eliminada.','success');
                          resolve()
                        }else{
                          swal("Algo salio mal, vuelve a intentar",'warning');
                          resolve()
                        }
                      });
                    })
                  },
                  allowOutsideClick: false
                });
                <?php }else{ ?>
                no_permiso('No tiene permisos para eliminar archivo');
                <?php } ?>
            });

		    $('#myModal').on('hidden.bs.modal', function (e) {
		        var video = document.getElementById("Video1");
		        var button = $("#play");
		          video.pause();
		          button.html('<i class="fa fa-play" aria-hidden="true"></i>');
		    });
		}); 

		addIdCat = function(id) {
			$("#txtCategoria").val(id);
		}

        function no_permiso(msj){
            swal('Advertencia',msj,'warning');
        }
  	</script>

@endsection




