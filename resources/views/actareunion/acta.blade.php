@extends('template.app')
@section('title', 'Actas')

@section('content')
@php
date_default_timezone_set("America/Bogota");
use Illuminate\Support\Facades\DB;
/*Permiso para acceder APU*/
$permiso_apu_acceder='No';
$modulo=4;
$acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="APU - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
if(isset($acceso1[0]->id)){
  $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
  if(isset($cargo1[0]->id)){
    $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
    if(isset($permiso1[0]->permiso)){
      if($permiso1[0]->permiso == "Si"){
        $permiso_apu_acceder='Si';
      }
    }
  }
}

/*Permiso para Acceder Actas*/
$acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Acceder" AND `id_permisomodulo`="'.$modulo.'"');
if(isset($acceso1[0]->id)){
  $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
  if(isset($cargo1[0]->id)){
    $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
    if(isset($permiso1[0]->permiso)){
      if($permiso1[0]->permiso == "No"){
        echo '<script language="javascript">location="/venta/'.$id.'"</script>';
      }
    }
  }
}

/*Permiso para acceder Actas*/
$permiso_acta_crear_editar='No';
$modulo=4;
$acceso1 = DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="Actas - Crear y Editar" AND `id_permisomodulo`="'.$modulo.'"');
if(isset($acceso1[0]->id)){
  $cargo1 = DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
  if(isset($cargo1[0]->id)){
    $permiso1 = DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso1[0]->id.'" AND `id_cargo`="'.$cargo1[0]->id.'" ORDER BY `id` DESC;');
    if(isset($permiso1[0]->permiso)){
      if($permiso1[0]->permiso == "Si"){
        $permiso_acta_crear_editar='Si';
      }
    }
  }
}
/*Fin Permiso para acceder Actas*/
@endphp
<script language="javascript">var permiso_acta_crear_editar = "<?=$permiso_acta_crear_editar?>"</script>
<link href="{{ url('/') }}/css/actasreunion/acta.css" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<input type="hidden" value="{{url('/')}}" id="url_basico" >
<input type="hidden" value="{{date('Y-m-d H:i:s')}}" id="fecha_actual" >

<div class="container-fluid animated slideInDown">
    <div class="row todas-ventanas" id="listado_actas" >
        <div class="col-md-12 panel-view">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-pills" role="tablist">
                        <li><a href="{{ url('venta') }}/{{ $id }}"><i class="fa fa-area-chart"></i> Oportunidad</a></li>
                        <li><a href="{{ url('ventafileimportant') }}/{{ $id }}"><i class="fa fa-bar-chart"></i> Archivos</a></li>
                        @if($permiso_apu_acceder=="Si")<li><a href="{{ url('apu') }}/{{ $id }}"><i class="fa fa-line-chart"></i> APU</a></li>@endif
                        <li><a href="{{ url('ventaciclos') }}/{{ $id }}"><i class="fa fa-pie-chart"></i> Ciclos venta</a></li>
                        <li class="active"><a href="{{ url('actasreunion') }}/{{ $id }}"><i class="fa fa-line-chart"></i> Actas reunion</a></li>
                        <li><a href="/ver-oportunidadvendida-flujo/<?=$id?>"><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
                        <li><a href="/venta/gestionesoportunidad/<?=$data['venta']->oportunidad?>"><i class="fa fa-calendar"></i> Gestiones</a></li>
                    </ul>
                </div>
            </div>
            <br>
            <div class="main-header">
                <h2>Actas</h2>
                <em>Actas de reunion y Listado de acciones</em>
            </div>
            <div class="row con-new-acta">
                <div class="col-md-2 new-acta">
                    <h2 class="new-acta-text">Crear Acta</h2>
                    <h3>+</h3>
                </div>
                @foreach($actas as $acta)
                <div class="col-md-2 {{$acta->estado}}">
                    @if(($acta->id)>9) @if(($acta->id)>99) @if(($acta->id)>999) @php $numero=$acta->id; @endphp @else @php $numero='0'.$acta->id; @endphp  @endif  @else @php $numero='00'.$acta->id; @endphp @endif  @else @php $numero='000'.$acta->id; @endphp @endif
                    <h5 id="numero_acta_{{$acta->id}}">AC-{{$numero}}</h5>
                    <h4 class="new-acta-text" data-toggle="tooltip" data-placement="top" title="<?=$acta->nombre?>">{{$acta->nombre_abreviado}}</h4>
                    @if($acta->estado == 'realizada')
                    <h5>{{date("d/m/Y (h:i:s A)",strtotime($acta->fecha_inicio_real))}}</h5>
                    <a class="text-warning"  onclick="ver_acta(<?=$acta->id?>)" title="Ver Reunion"><i class="fa fa-eye fa-2"></i></a>
                    <a class="text-info"  onclick="enviar_info(<?=$acta->id?>)" title="Enviar información a correo"><i class="fa fa-envelope fa-2"></i></a>
                    @endif

                    @if($acta->estado == 'citada')
                    <h5>{{date("d/m/Y (h:i:s A)",strtotime($acta->fecha))}}</h5>
                    <a class="text-warning"  onclick="ver_acta(<?=$acta->id?>)" title="Ver Reunion"><i class="fa fa-eye fa-2"></i></a>
                    <a class="iniciar" onclick="iniciar_reunion({{$acta->id}})" title="Iniciar Reunion"><i class="fa fa-check-circle-o fa-2"></i></a>
                    <a class="editar" onclick="editar_acta({{$acta->id}})" title="Editar"><i class="fa fa-pencil-square fa-2"></i></a>
                    <a class="eliminar" onclick="eliminar_citada({{$acta->id}})" title="Eliminar"><i class="fa fa-times-circle-o fa-2"></i></a>
                    @endif
                </div>
                @endforeach
            </div>
            <div class="main-content">
                <!-- TABS -->
                <div class="widget">
                    <div class="widget-header">
                        <h3>Listado Acciones</h3>
                    </div>
                    <div class="widget-content">
                        <h4>Listado de Acciones (Vigentes, Realizadas, Canceladas)</h4>
                        <ul class="nav nav-tabs nav-tabs-right">
                            <li class="active"><a href="#tabitem1" data-toggle="tab" aria-expanded="true">Vigentes <span class="badge element-bg-color-orange">{{$data['numero_vigentes']}}</span></a></li>
                            <li class=""><a href="#tabitem2" data-toggle="tab" aria-expanded="false">Realizados <span class="badge element-bg-color-green">{{$data['numero_realizado']}}</span></a></li>
                            <li class=""><a href="#tabitem3" data-toggle="tab" aria-expanded="false">Canceladas <span class="badge element-bg-color-red">{{$data['numero_cancelado']}}</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tabitem1">
                                <div class="row fila-encabezado">
                                    <div class="col-md-1">
                                        <div class="row">
                                            <div class="col-md-4 encabezado">
                                                <h6><strong>Item</strong></h6>
                                            </div>
                                            <div class="col-md-8 encabezado">
                                                <h6><strong>Acta</strong></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-6 encabezado">
                                                <h6><strong>Fecha Creación</strong></h6>
                                            </div>
                                            <div class="col-md-6 encabezado">
                                                <h6><strong>Fecha Ejecución</strong></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 encabezado">
                                        <h6><strong>Responsable</strong></h6>
                                    </div>
                                    <div class="col-md-3 encabezado">
                                        <h6><strong>Descripción</strong></h6>
                                    </div>
                                    <div class="col-md-1 encabezado">
                                        <h6><strong>Aplazó</strong></h6>
                                    </div>
                                    <div class="col-md-2 encabezado">
                                        <h6><strong>Acciones</strong></h6>
                                    </div>
                                </div>
                                <?=$data['vigentes']?>
                                @if($data['vigentes'] == '')
                                <div class="row">
                                    <div class="col-md-12">
                                        No hay registros
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="tabitem2">
                                <div class="row fila-encabezado">
                                    <div class="col-md-1">
                                        <div class="row">
                                            <div class="col-md-4 encabezado">
                                                <h6><strong>Item</strong></h6>
                                            </div>
                                            <div class="col-md-8 encabezado">
                                                <h6><strong>Acta</strong></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-4 encabezado">
                                                <h6><strong>Fecha Creación</strong></h6>
                                            </div>
                                            <div class="col-md-4 encabezado">
                                                <h6><strong>Fecha Ejecución</strong></h6>
                                            </div>
                                            <div class="col-md-4 encabezado">
                                                <h6><strong>Fecha Realizado</strong></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 encabezado">
                                        <h6><strong>Responsable</strong></h6>
                                    </div>
                                    <div class="col-md-3 encabezado">
                                        <h6><strong>Descripción</strong></h6>
                                    </div>
                                    <div class="col-md-1 encabezado">
                                        <h6><strong>Aplazó</strong></h6>
                                    </div>
                                    <div class="col-md-1 encabezado">
                                        <h6><strong>Acción</strong></h6>
                                    </div>
                                </div>
                                <?=$data['realizados']?>
                                @if($data['numero_realizado'] == 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        No hay registros
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="tabitem3">
                                <div class="row fila-encabezado">
                                    <div class="col-md-1">
                                        <div class="row">
                                            <div class="col-md-4 encabezado">
                                                <h6><strong>Item</strong></h6>
                                            </div>
                                            <div class="col-md-8 encabezado">
                                                <h6><strong>Acta</strong></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-6 encabezado">
                                                <h6><strong>Fecha Creación</strong></h6>
                                            </div>
                                            <div class="col-md-6 encabezado">
                                                <h6><strong>Fecha Ejecución</strong></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 encabezado">
                                        <h6><strong>Responsable</strong></h6>
                                    </div>
                                    <div class="col-md-4 encabezado">
                                        <h6><strong>Descripción</strong></h6>
                                    </div>
                                    <div class="col-md-1 encabezado">
                                        <h6><strong>Aplazó</strong></h6>
                                    </div>
                                    <div class="col-md-1 encabezado">
                                        <h6><strong>Acción</strong></h6>
                                    </div>

                                </div>
                                <?=$data['cancelados']?>
                                @if($data['numero_cancelado'] == 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        No hay registros
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row todas-ventanas" id="crear_acta">
        <div class="col-md-1 center">
            <img src="{{url('/')}}/images/atras.png" class="img-atras" title="Atras">
        </div>
        <div class="col-md-10">
            <h2>Crear Cita</h2>
        </div>
        <div class="col-md-1">
            <h2 class="lado">lado</h2>
        </div>
        <div class="col-md-12">
        <form id="formulario_cita" method="POST" action="{{url('/')}}/guardarcita" enctype="multipart/form-data">
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          {{ csrf_field() }}
          <input type="hidden" value="{{$id}}" id="id_oportunidad" name="id_oportunidad" >
          <input type="hidden" id="invitados" value="" name="invitados" >
          <div class="form-group">
            <p>Titulo </p>
            <label for="recipient-name" class="form-control-label"></label>
            <input type="text" class="form-control" id="acta[titulo]" name="titulo" maxlength="250" required>
          </div>
          <div class="form-group">
            <p>Descripción </p>
            <label for="message-text" class="form-control-label"></label>
            <textarea class="form-control" id="acta[descripcion]" name="descripcion" rows="5" required="required"></textarea>
          </div>
          <p>Fecha de la reunión - <small> hora de la reunión</small></p>
          <div class="row">
              <div class="col-md-12">
                  <input class="form-control2 form-control datatime" type="text" step="1800" placeholder="Fecha y hora reunión" name="fecha" required>
              </div>
          </div>
          <div class="row margen-top">
              <div class="col-md-12">
                  <div class="form-group">
                    <p>Lugar </p>
                    <label for="message-text" class="form-control-label"></label>
                    <input type="text" class="form-control" name="lugar" maxlength="250" required>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <dl class="dl-horizontal">
                    <dt>Invitados:</dt>
                    <dd>
                        <ul class="list-inline team-list" id="listado_invitados">
                            <li class="team-add">
                                <i class="icon ion-person"></i>
                                <a href="#" id="añadir_invitados"><i class="fa fa-plus-circle"></i> Añadir </a>
                            </li>
                        </ul>
                    </dd>
                </dl>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                <p>Listado de invitados externos </p>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12 crear-inv-adi">
                <a class="crear-inv"><i class="fa fa-plus-circle fa-4" aria-hidden="true" title="Crear"></i></a>
            </div>
          </div>
          <div id="contenedor-invi-exter">
              <div class="row">
                  <div class="col-md-12">
                      <p class="h5"><strong><em>Para agregar un usuario externo de click en más</em></strong></p>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <div class="col-sm-12">
                          <button type="submit" class="btn_guardar">Guardar</button>
                        <a  class="btn btn-success guardar"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                      </div>
                  </div>
              </div>
          </div>
      </form>
        </div>
    </div>
    <div class="row todas-ventanas" id="iniciar_reunion">
        <div class="col-md-1 center">
            <img src="{{url('/')}}/images/atras.png" class="img-atras" title="Atras">
        </div>
        <div class="col-md-10">
            <h2>Iniciar reunión</h2>
        </div>
        <div class="col-md-1">
            <h2 class="lado">lado</h2>
        </div>
        <div class="col-md-12">
            <form id="formulario_iniciar" method="POST" action="{{url('/')}}/iniciarreunion" enctype="multipart/form-data">
              @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
              {{ csrf_field() }}
              <input type="hidden" value="{{$id}}" name="id_oportunidad" >
              <input type="hidden" id="id_reunion_iniciar" name="id" value="">
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <p>Fecha y hora de inicio </p>
                          <label for="recipient-name" class="form-control-label"></label>
                          <input type="hidden" name="fecha" value="{{date('Y-m-d H:i:s')}}">
                          <input type="text" class="form-control fecha-inicio"  value="{{date('d/m/Y (h:i:s A)')}}" disabled required>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12 contenido-iniciar-reunion">
                      <div class="row fila-asistencia">
                          <div class="col-md-12">
                              <p>Asistencia</p>
                          </div>
                          <div class="col-md-3"></div>
                          <div class="col-md-6">
                            <div class="row" id="contenedor_asistencia">

                            </div>
                          </div>
                          <div class="col-md-3"></div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="widget">
                                <div class="widget-header">
                                    <h3>Temas</h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 crear-inv-adi">
                                        <a class="crear-tema"><i class="fa fa-plus-circle fa-4" aria-hidden="true" title="Crear"></i></a>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title titulo-tema">
                                                            <a class="collapsed titulo-tema0" data-toggle="collapse" data-parent="#accordion" href="#collapse1"><b>Tema #<span id="num_tema1">1</span></b><i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1" class="panel-collapse collapse">
                                                       <input type="hidden" id="id_tema0" name="temas[0][id]" value="">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <p>Titulo </p>
                                                                        <label for="recipient-name" class="form-control-label"></label>
                                                                        <input class="form-control titulo-tema" id="titulo-tema0" name="temas[0][titulo]" required="" maxlength="250" type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <p>Descripción </p>
                                                                        <label for="message-text" class="form-control-label"></label>
                                                                        <textarea class="form-control" name="temas[0][descripcion]" rows="5" required="required"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                              <div class="widget">
                                <div class="widget-header">
                                    <h3>Pendientes</h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 crear-inv-adi">
                                        <a class="crear-pendiente"><i class="fa fa-plus-circle fa-4" aria-hidden="true" title="Crear"></i></a>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel-group" id="accordion0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title titulo-pendiente">
                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_pen1"><b>Pendiente #<span id="num_pen1">1</span></b> <i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_pen1" class="panel-collapse collapse">
                                                       <input type="hidden" id="id_pendiente0" name="pendiente[0][id]" value="">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <p>Fecha ejecución </p>
                                                                        <label for="recipient-name" class="form-control-label"></label>
                                                                        <input type="hidden" name="pendiente[0][fecha_creacion]" value="{{date('Y-m-d H:i:s')}}">
                                                                        <input class="form-control2 form-control datatime" step="1800" placeholder="Fecha y hora Ejecución" name="pendiente[0][fecha_ejecucion]" data-dtp="dtp_jx0jc" type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group" id="select_responsable">
                                                                        <p>Responsable </p>
                                                                        <label for="message-text" class="form-control-label"></label>
                                                                        <select class="form-control responsable" name="pendiente[0][responsable]">
                                                                            <option value="">Seleccione un responsable</option>
                                                                            <script>  html_responsable='';   </script>
                                                                            @foreach($usuarios as $usuario)
                                                                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                                                                            <script>   html_responsable+='<option value="{{$usuario->id}}">{{$usuario->name}}</option>'; </script>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                     <div class="form-group">
                                                                        <p>Descripción </p>
                                                                        <label for="message-text" class="form-control-label"></label>
                                                                        <textarea class="form-control" name="pendiente[0][descripcion]" rows="5"></textarea>
                                                                     </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                    <button type="submit" class="btn_guardar">Guardar</button>
                    <!--<a class="btn btn-success text-white" onclick="guardar_continuar()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar y continuar</a>-->
                    <a  class="btn btn-success guardar2"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar y Cerrar</a>
                  </div>
              </div>
          </form>
        </div>
    </div>
    <div class="row todas-ventanas" id="editar_acta">
        <div class="col-md-1 center">
            <img src="{{url('/')}}/images/atras.png" class="img-atras" title="Atras">
        </div>
        <div class="col-md-10">
            <h2>Editar Cita</h2>
            <h3 id="numero_Acta_editar"></h3>
        </div>
        <div class="col-md-1">
            <h2 class="lado">lado</h2>
        </div>
        <div class="col-md-12">
        <form id="formulario_cita_editar" method="POST" action="{{url('/')}}/editarcita" enctype="multipart/form-data">
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          {{ csrf_field() }}
          <input type="hidden" id="id_cita" name="id_cita" >
          <input type="hidden" value="{{$id}}" id="id_oportunidad_editar" name="id_oportunidad" >
          <input type="hidden" id="invitados_editar" name="invitados" >
          <div class="form-group">
            <p>Titulo </p>
            <label for="recipient-name" class="form-control-label"></label>
            <input type="text" class="form-control" id="acta_editar_titulo" name="titulo" required>
          </div>
          <div class="form-group">
            <p>Descripción </p>
            <label for="message-text" class="form-control-label"></label>
            <textarea class="form-control" id="acta_editar_descripcion" name="descripcion" rows="5" required="required"></textarea>
          </div>
          <p>Fecha de la reunión - <small> hora de la reunión</small></p>
          <div class="row">
              <div class="col-md-12">
                  <input class="form-control2 form-control datatime" type="text" step="1800" placeholder="Fecha y hora reunión" id="fecha_editar" name="fecha" required>
              </div>
          </div>
          <div class="row margen-top">
              <div class="col-md-12">
                  <div class="form-group">
                    <p>Lugar </p>
                    <label for="message-text" class="form-control-label"></label>
                    <input type="text" class="form-control" id="lugar_editar" name="lugar" required>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <dl class="dl-horizontal">
                    <dt>Invitados:</dt>
                    <dd>
                        <ul class="list-inline team-list" id="listado_invitados_editar">
                            <li class="team-add">
                                <i class="icon ion-person"></i>
                                <a href="#" id="añadir_invitados_editar"><i class="fa fa-plus-circle"></i> Añadir </a>
                            </li>
                        </ul>
                    </dd>
                </dl>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                <p>Listado de invitados externos</p>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12 crear-inv-adi">
                <a class="crear-inv-editar"><i class="fa fa-plus-circle fa-4" aria-hidden="true" title="Crear"></i></a>
            </div>
          </div>
          <div id="contenedor-invi-exter-editar">

          </div>
          <div class="row guardar-editar-cita">
              <div class="col-md-12">
                  <div class="form-group">
                      <div class="col-sm-12">
                        <button type="submit" class="btn_guardar_editar">Guardar</button>
                        <a  class="btn btn-success guardar_editar"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                      </div>
                  </div>
              </div>
          </div>
      </form>
        </div>
    </div>
</div>


<!--modal añadir -->
<div class="modal fade añadir-invitados" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content anadir-invitados" style="width:120%;">
      <div class="modal-header">
        <h5 class="modal-title">Añadir Invitados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              @foreach($usuarios as $usuario)
                @php
                $nombre=explode(" ",$usuario->nombres);
                $apellido=explode(" ",$usuario->apellidos);
                @endphp

                @if($usuario->foto == '')
                    @php    $foto='essi_admon.jpg' @endphp
                @else
                     @php   $foto=$usuario->foto @endphp
                @endif
              <div class="col-md-3 col-user">
                  <label class="control-inline fancy-checkbox">
                        <input type="checkbox" class="lista-usuarios" id="user{{$usuario->id}}">
                        <span>{{$nombre[0]}} {{$apellido[0]}}</span>
                        <img  src="{{url('/')}}/images/file/clientes/{{$foto}}" class="img-circle imagen-agregar" alt="Avatar">
                  </label>
              </div>
              @endforeach
          </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default Guardar-Invi" data-dismiss="modal">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!--modal añadir editar-->
<div class="modal fade añadir-invitados-editar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content anadir-invitados">
      <div class="modal-header">
        <h5 class="modal-title">Añadir Invitados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row" id="funcionarios_editar">
              @foreach($usuarios as $usuario)
                @php
                $nombre=explode(" ",$usuario->nombres);
                $apellido=explode(" ",$usuario->apellidos);
                @endphp

                @if($usuario->foto == '')
                    @php    $foto='essi_admon.jpg' @endphp
                @else
                     @php   $foto=$usuario->foto @endphp
                @endif
              <div class="col-md-3 col-user">
                  <label class="control-inline fancy-checkbox">
                        <input type="checkbox" class="lista-usuarios-editar" id="user_editar{{$usuario->id}}">
                        <span>{{$nombre[0]}} {{$apellido[0]}}</span>
                        <img  src="{{url('/')}}/images/file/clientes/{{$foto}}" class="img-circle imagen-agregar" alt="Avatar">
                  </label>
              </div>
              @endforeach
          </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default Guardar-Invi" data-dismiss="modal">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!--modal aplazar -->
<div class="modal fade modificarfecha">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <h5 class="modal-title mf-header">Aplazar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Elija una nueva fecha</p>
          <input type="hidden" value="" id="id_aplazar">
        <div class="row">
          <div class="col-md-12">
              <input class="form-control2 form-control datatime" type="text" step="1800" placeholder="Fecha y hora" id="nuevafecha" required>
          </div>
        </div>
          <div class="row">
          <div class="col-md-12">
              <textarea class="form-control" id="comentarioaplazado" placeholder="Ingrese un comentario" rows="8"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn_guardar_aplazar">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!--modal comentarios -->
<div class="modal fade comentarios">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <h5 class="modal-title mf-header">Comentarios <a id="acta-comentario">AC-0000</a></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body contenido_comentarios">
        <div class="row">
            <div class="col-md-3">
                <span class="label label-medium">Realizada</span>
                <span class="label label-emergency">Aplazada</span>
                <span class="label label-critical">Cancelada</span>
            </div>
            <div class="col-md-6">
                <p><a href="#">Michael</a> has achieved 80% of his completed tasks <br><span class="timestamp hace-comentario">34 minutes ago</span></p>
            </div>
            <div class="col-md-3">
                <img src="/images/file/clientes/595ea6fc8ec24.png" class="img-circle img-comentario" alt="Avatar">
            </div>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<div class="alert alert-success" role="alert" style="position: fixed;width: 250px;right: 20px;top: 36%;display: none;">
  Guardado!
</div>
<div class="row flotante todas-ventanas" id="btn_guardar_continuar" style="display:none;">
   <div class="col-md-12" id="contador_tiempo">
       <div style="margin: 0px auto; text-align: center; width: 150px;"><img src="http://lh4.ggpht.com/_0eC4K-qZ7AM/TO1RSmqk-lI/AAAAAAAAMX0/-7OHYYDkfco/1.gif" /><img src="http://lh5.ggpht.com/_0eC4K-qZ7AM/TO1RS7xsylI/AAAAAAAAMX4/_uQfIddo6OA/2.gif" /><img src="http://lh3.ggpht.com/_0eC4K-qZ7AM/TO1RTOyFacI/AAAAAAAAMX8/i-DnYANb0kE/3.gif" /><img src="http://lh3.ggpht.com/_0eC4K-qZ7AM/TO1RTUc4nVI/AAAAAAAAMYA/bj0mqGH8xLM/4.gif" /><img src="http://lh3.ggpht.com/_0eC4K-qZ7AM/TO1RTuO79eI/AAAAAAAAMYE/-Y2w89jmaig/5.gif" /></div>
   </div>
    <div class="col-md-12">
        <a class="btn btn-success text-white" onclick="guardar_continuar()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar y continuar</a>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{url('/')}}/js/actasreunion/acta.js"></script>
<script src="{{url('/')}}/js/parsley.min.js"></script>

<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
@endsection
