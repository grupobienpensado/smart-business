@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Oportunidad Perdida')

@section('content')

<style type="text/css">
.estirar{
  width: 100%;
}
.radio label{
  margin-right: 50px
}
</style>
<link rel="stylesheet" href="{{url('/')}}/js/plugins/ckeditor/samples/css/samples.css">
<link rel="stylesheet" href="{{url('/')}}/js/plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
<div class="container animated slideInDown">
  <div class="row">
    <main class="col-sm-12 col-md-12 pt-5">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>Detalles Oportunidad Perdida</h3>
			</div>
        
      	 <div class="col-md-12 form-group">
         <label>¿Cual fue el motivo de la perdida de esta oportunidad? </label>
          <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->motivo_perdida) ?></span>
         </div> 
         <div class="col-md-12 form-group">
         <label>¿El suministro de equipos o el proyecto lo hará la competencia? </label>
          <span class="block"><?php print($datos->suministro_competencia) ?></span>
         </div> 
         <div class="col-md-12 form-group competencia" @if($datos->suministro_competencia=="No") style="display: none" @endif>
         <label>¿Que empresa realizará el proyecto? </label>
          <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->empresa) ?></span>
         </div>
         <div class="col-md-12 form-group competencia" @if($datos->suministro_competencia=="No") style="display: none" @endif>
         <label>Describa los equipos que suministrará: </label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->equipos_suministrados) ?></span>
          
         </div> 
         <div class="col-md-12 form-group competencia" @if($datos->suministro_competencia=="No") style="display: none" @endif>
         <label>¿Cual fue la ventaja competitiva o estrategia que permitió que la competencia ganara: </label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->ventaja_competencia) ?></span>
         </div> 
         <div class="col-md-12 form-group">
         <label>¿Que acciones habría realizado para no perder la oportunidad: </label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->acciones_realizadas) ?></span>
         </div> 
         <div class="col-md-12 form-group">
         <label>¿Considera oportuno y necesario el monto y cantidad de recursos y tiempo invertido en esta oportunidad? </label>
         <span class="block"><?php print($datos->monto_oportuno) ?></span>
         <label>¿Porqué?</label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->justificacion_monto_oportuno) ?></span>
        
         </div>
         <div class="col-md-12 form-group">
         <label>¿Cual es la percepción del cliente hacia ESSI? </label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->percepcion_essi) ?></span>
         </div>
         <div class="col-md-12 form-group">
         <label>¿Cual es la percepción del cliente hacia los equipos de ESSI? </label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc"><?php print($datos->percepcion_maquina) ?></span>
         </div>
         <div class="col-md-12 form-group">
         <label>¿Cuales son las futuras oportunidades con este cliente? </label>
         <span class="block" style="text-align: justify; padding: 5px;border-bottom: .5px solid #ccc;"><?php print($datos->futuras_oportunidades) ?></span>
         </div>
    </main>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="{{url('/')}}/js/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" charset="utf-8">

</script>

@endsection