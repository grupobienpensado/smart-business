<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Registrar Cliente')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">

<link rel="stylesheet" href="{{ url('/') }}/css/ripples.min.css"/>
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
   
</style>

<div class="container-fluid" id="list">
    <div class="row justify-content-md-center">
        <div class="col-12 col-md-auto panel-view">  
            <div class="pull-right">
                <div class="btn-group">
                  <a href="{{url('/')}}/clientes" class="btn btn-sm btn-success"><i class="fa fa-list" aria-hidden="true"></i> Listado Clientes</a>
                </div>
              </div>
            <form method="POST" action="{{ url('editarcliente') }}"  enctype="multipart/form-data">
                <div class="panel-title">
                    <h2>Editar cliente</h2>
                </div>
                <hr>

                <!-- Create Post Form -->

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <!-- Fin Create Post Form -->

                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $model->id }}">
                <div class="form-group row">
                    <div class="col-4">
                      <p style="color: #9e9e9e">Foto del cliente (10.6 %)</p>
                      <div class="dropzone" data-width="300" data-height="400" data-ajax="false" data-originalsave="true" style="width: 300px;height: 400px;min-width: 300px !important;min-height: 400px !important;background-image: url('{{ url('/') }}/images/file/clientes/{{ $model->foto }}');">
                        <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                      </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="nombres" class="col-form-label">Nombres (2.12 %)</label>
                                <input class="form-control form-control-sm" type="text" name="nombres" value="{{ $model->nombres }}" placeholder="Por favor ingrese los nombre del cliente">
                            </div>
                            <div class="col-6">
                                <label for="apellidos" class="col-form-label">Apellidos (2.12 %)</label>
                                <input class="form-control form-control-sm" type="text" name="apellidos" value="{{ $model->apellidos }}" placeholder="Por favor ingrese los apellidos del cliente">
                            </div>
                        </div>
                        <div class="form-group row">
                            <?php 
                                if (isset($model->empresa_sede_id) && !empty($model->empresa_sede_id)) {
                                    $sede = App\EmpresaSedes::find($model->empresa_sede_id);
                                    if (isset($sede->empresa_id) && !empty($sede->empresa_id)) {
                                        $empresa = App\Empresa::find($sede->empresa_id);
                                    }
                                    
                                }
                                
                                $user = App\User::find($model->user_id);
                            ?>
                            <div class="col-4">
                                <label for="empresa" class="col-form-label">Empresa (2.12 %)</label>
                                <select name="empresa" class="form-control" id="empresa">
                                    <?php if (isset($empresa) && !empty($empresa)): ?>
                                        <option value="<?php echo $empresa->id; ?>"><?php echo $empresa->nombre; ?></option>
                                    <?php endif ?>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for="empresa_sede_id" class="col-form-label">Sede (2.12 %)</label>
                                <select name="empresa_sede_id" class="form-control" id="sede" required> 
                                    <?php if (isset($empresa) && !empty($empresa) && isset($sede) && !empty($sede)): ?>
                                        <option value="{{ $sede->id }}"><?php echo $empresa->nombre."/".$sede->ciudad; ?></option>
                                    <?php endif ?>
                                </select>
                            </div> 
                            <div class="col-4">
                                <label class="col-form-label">Responsable (2.12 %)</label>
                                <select name="responsable" class="form-control" id="responsable" required>
                                    <option value="{{ $user->id }}"><?php echo $user->name; ?></option>
                                </select>
                            </div>                          
                        </div>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="profesion" class="col-form-label">Profesión (4,24 %)</label>
                                <input class="form-control form-control-sm" type="text" name="profesion" value="{{ $model->profesion }}" placeholder="Por favor ingrese la profesión">
                            </div>
                            <div class="col-4">
                                <label for="jefe_inmediato" class="col-form-label">Jefe Inmediato (2.12 %)</label>
                                <select name="jefe_inmediato" class="form-control" id="cliente">
                                    <option value="{{ $model->cliente }}">{{ $model->cliente }}</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for="n_a_pesos" class="col-form-label">Nivel de autorización en pesos (4,24 %)</label>
                                <input class="form-control form-control-sm dinero" type="text" name="n_a_pesos" value="<?php 
                              try{
                                echo number_format($model->n_a_pesos); 
                              } catch (Exception $e) {
                                echo $model->n_a_pesos; 
                              }  
                            ?>" placeholder="Por favor ingrese el nivel autorizado en pesos">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="tratamiento" class="col-form-label">Trato (2.12 %)</label>
                                <select name="tratamiento" class="form-control">
                                    <option value="">Seleccione una opcion</option>
                                    <option value="Sr"<?php if ($model->tratamiento == "Sr") { echo "selected"; } ?>>Señor</option>
                                    <option value="Sra" <?php if ($model->tratamiento == "Sra") { echo "selected"; } ?>>Señora</option>
                                    <option value="Dra" <?php if ($model->tratamiento == "Dra") { echo "selected"; } ?>>Doctora</option>
                                    <option value="Dr" <?php if ($model->tratamiento == "Dr") { echo "selected"; } ?>>Doctor</option>
                                    <option value="Lcda" <?php if ($model->tratamiento == "Lcda") { echo "selected"; } ?>>Licenciada</option>
                                    <option value="Lcdo" <?php if ($model->tratamiento == "Lcdo") { echo "selected"; } ?>>Licenciado</option>
                                    <option value="Ing" <?php if ($model->tratamiento == "Ing") { echo "selected"; } ?>>Ingeniero</option>
                                    <option value="Srta" <?php if ($model->tratamiento == "Srta") { echo "selected"; } ?>>Señorita</option>
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="estado_civil" class="col-form-label">Estado civil (2.12 %)</label>
                                <select name="estado_civil" class="form-control">
                                    <option value="">Seleccione una opcion</option>
                                    <option value="Soltero" <?php if ($model->estado_civil == "Soltero") { echo "selected"; } ?>>Soltero</option>
                                    <option value="Casado" <?php if ($model->estado_civil == "Casado") { echo "selected"; } ?>>Casado</option>
                                    <option value="Divorciado" <?php if ($model->estado_civil == "Divorciado") { echo "selected"; } ?>>Divorciado</option>
                                    <option value="Viudo" <?php if ($model->estado_civil == "Viudo") { echo "selected"; } ?>>Viudo</option>
                                    <option value="Union libre" <?php if ($model->estado_civil == "Union libre") { echo "selected"; } ?>>Union libre</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="cargo" class="col-form-label">Cargo (2.12 %)</label>
                                <input class="form-control form-control-sm" type="text" name="cargo" value="{{ $model->cargo }}" placeholder="Por favor ingrese el cargo">
                            </div>
                            <div class="col-3">
                                <label for="fecha_nacimiento" class="col-form-label">Fecha nacimiento (8.48 %)</label>
                                <input id="output" class="form-control form-control-sm c-datepicker-btn" type="text" value="{{ $model->fecha_nacimiento }}" name="fecha_nacimiento" placeholder="Fecha nacimiento">
                            </div>
                            <div class="col-2">
                                <label for="fecha_nacimiento" class="col-form-label">Numero de hijos (2.12 %)</label>
                                <input class="form-control form-control-sm" type="text" maxlength="1" name="num_hijos" size="1" placeholder="Numero de hijos" value="{{ $model->num_hijos }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="pais" class="col-form-label">Pais nacimiento (2.12 %)</label>
                                <select name="pais" class="form-control" id="pais">
                                    <option value="{{ $model->pais }}">{{ $model->pais }}</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for="departamento" class="col-form-label">Departamento nacimiento (2.12 %)</label>
                                <select name="departamento" class="form-control" id="estados">
                                    <option value="{{ $model->departamento }}">{{ $model->departamento }}</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for="ciudad" class="col-form-label">Ciudad nacimiento (2.12 %)</label>
                                <select name="ciudad" class="form-control" id="ciudad">
                                    <option value="{{ $model->ciudad }}">{{ $model->ciudad }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>                
                

                <div class="form-group row" > 
                    <?php 
                        try{
                          $telefonosCliente = App\ClienteTelefonos::where("cliente_id", $model->id)->get();
                          $i=0;
                          foreach ($telefonosCliente as $value) {
                            $i++;
                    ?>
                          
                    <div class="col-6">
                      <label class="col-form-label">Telefono Personal (6.36 %)</label>

                        <input type="hidden" name="telefono[{{ $i }}][id]" value="{{ $value->id }}">
                      <div class="input-group">
                          <select name="telefono[{{ $i }}][telefono_tipo]" class="col-2 form-control" id="codteltipo{{ $i }}" onchange="funcambiarinput(this.id, {{ $i }})">
                              <option value="Telefono" <?php if ($value->telefono_tipo === "Telefono") { echo "selected";} ?>>Telefono</option>
                              <option value="Celular" <?php if ($value->telefono_tipo === "Celular") { echo "selected";} ?>>Celular</option>
                              <option value="Radio" <?php if ($value->telefono_tipo === "Radio") { echo "selected";} ?>>Radio</option>
                          </select>
                          <input type="text" name="telefono[{{ $i }}][telefono_indp]" class="col-2 form-control form-control-sm centrado" placeholder="IND P" id="codtelindp{{ $i }}" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom" value="{{ $value->telefono_indp }}">
                          <input type="number" name="telefono[{{ $i }}][telefono_indc]" class="col-2 form-control form-control-sm centrado" placeholder="IND C" id="codtelindc{{ $i }}" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom" value="{{ $value->telefono_indc }}">
                          <input type="number" name="telefono[{{ $i }}][telefono_numero]" class="col-4 form-control form-control-sm centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum{{ $i }}" value="{{ $value->telefono_numero }}">
                          <input type="number" name="telefono[{{ $i }}][telefono_ext]" class="col-2 form-control form-control-sm centrado" placeholder="EXT" id="codtelext{{ $i }}" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom" value="{{ $value->telefono_ext }}">
                      </div>
                    </div>
                    <?php
                            }
                        }catch (Exception $e){

                        }
                    ?> 
                    <div class="col-6" id="mastelefonos">
                      <label class="col-form-label">Telefono Coorporativo (6.36 %)</label>
                      <div class="input-group">
                          <select name="telefono[0][telefono_tipo]" class="col-2 form-control" id="codteltipo0" onchange="funcambiarinput(this.id, 0)">
                              <option value="Telefono">Telefono</option>
                              <option value="Celular">Celular</option>
                              <option value="Radio">Radio</option>
                          </select>
                          <input type="text" name="telefono[0][telefono_indp]" class="col-2 form-control form-control-sm centrado" placeholder="IND P" id="codtelindp0" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom">
                          <input type="number" name="telefono[0][telefono_indc]" class="col-2 form-control form-control-sm centrado" placeholder="IND C" id="codtelindc0" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom">
                          <input type="number" name="telefono[0][telefono_numero]" class="col-4 form-control form-control-sm centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum0" >
                          <input type="number" name="telefono[0][telefono_ext]" class="col-2 form-control form-control-sm centrado" placeholder="EXT" id="codtelext0" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom">
                          <span class="input-group-btn">
                              <!--<button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarTelefono()"><i class="fa fa-plus text-success"></i></button>-->
                          </span>
                      </div>
                    </div>  
                </div>

                <div class="form-group row" >  
                    <?php 
                        try{
                            $mails = App\ClienteMails::where("cliente_id", $model->id)->get();
                            foreach ($mails as $value) {
                                $i++;
                    ?>
                    <div class="col-6">
                        <label for="n_a_pesos" class="col-form-label">Correo Personal (6.36 %)</label>
                        <input type="hidden" name="correo[{{ $i }}][id]" value="{{ $value->id }}">
                        <input class="form-control form-control-sm" type="email" name="correo[{{ $i }}][mail]" placeholder="Por favor ingrese un correo" value="{{ $value->mail }}">
                    </div>
                    <?php
                            }
                        }catch (Exception $e){

                        }
                    ?>
                    <div class="col-6" id="correo">
                        <label for="n_a_pesos" class="col-form-label">Correo Corporativo (6.36 %)</label>
                        <div class="input-group">
                          <input class="form-control form-control-sm" type="email" name="correo[0][mail]" placeholder="Por favor ingrese un correo">
                          <span class="input-group-btn">
                            <!--<button class="btn btn-secondary btn-sm" type="button" onclick="agregarCorreo()"><i class="fa fa-plus text-success"></i></button>-->
                          </span>
                        </div>
                    </div>  
                </div>

               
 

                <div>
                    <?php 
                        try{
                          $redessociales = App\ClienteRedessociales::where("cliente_id", $model->id)->get();
                          foreach ($redessociales as $value) {
                            $i++;
                    ?>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Redes Sociales (8.48 %)</label>
                        <div class="col-lg-10">
                            <div class="input-group"><input type="hidden" name="redsocial[{{ $i }}][id]" value="{{ $value->id }}">
                                <select name="redsocial[{{ $i }}][tipo]" class="col-2 form-control">
                                    <option value="Facebook" <?php if ($value->tipo === "Facebook") { echo "selected";} ?>>Facebook</option>
                                    <option value="Instagran" <?php if ($value->tipo === "Instagran") { echo "selected";} ?>>Instagran</option>
                                    <option value="Twitter" <?php if ($value->tipo === "Twitter") { echo "selected";} ?>>Twitter</option>
                                    <option value="Linkedin" <?php if ($value->tipo === "Linkedin") { echo "selected";} ?>>Linkedin</option>
                                    <option value="Otra" <?php if ($value->tipo === "Otra") { echo "selected";} ?>>Otra</option>
                                </select>
                                <input type="text" class="col-10 form-control form-control-sm" name="redsocial[{{ $i }}][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social" value="{{ $value->valor }}">
                            </div>
                        </div>
                    </div>
                    <?php
                            }
                        }catch (Exception $e){

                        }
                    ?>
                </div>

                <div id="redessociales">
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Redes Sociales (8.48 %)</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <select name="redsocial[0][tipo]" class="col-2 form-control">
                                    <option value="Facebook">Facebook</option>
                                    <option value="Instagran">Instagran</option>
                                    <option value="Twitter">Twitter</option>
                                    <option value="Linkedin">Linkedin</option>
                                    <option value="Otra">Otra</option>
                                </select>
                                <input type="text" class="col-10 form-control form-control-sm" name="redsocial[0][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social">
                                <span class="input-group-btn">
                                    <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarRedesSociales()"><i class="fa fa-plus text-success"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="perfil" class="col-form-label">Perfil (10.6 %)</label>
                    <textarea class="form-control" name="perfil" rows="5" placeholder="Por favor ingrese un Detalle del Perfil del Cliente">{{ $model->perfil }}</textarea>
                </div>
                <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </form>
        </div>
      </div>
    </div>
@endsection

@section('scripts')

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>

<script src="{{ url('/') }}/js/jquery.maskMoney.min.js"></script>

<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>





<script type="text/javascript">
    $(function() {
        $(".dinero").maskMoney();
       $('.dropzone').html5imageupload();
       $(".dropdown-menu select").dropdown();

       $('.c-datepicker-btn').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            lang: 'es'
        });

        $('#responsable').select2({
            // Activamos la opcion "Tags" del plugin
             placeholder: "Seleccione una responsable",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("vendedores") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    });

    i={{ $i }};
    j=0;
    k=0;
    h=0;
    agregarTelefono = function() {
        i++;
        h++;
        dato = `<div class="col-6 animated rollIn" id="T`+i+`">
                    <label class="col-form-label">Telefono `+h+`</label>
                        <div class="input-group">
                            <select name="telefono[`+i+`][telefono_tipo]" class="col-2 form-control" id="codteltipo`+i+`" onchange="funcambiarinput(this.id, `+i+`)">
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Radio">Radio</option>
                            </select>
                            <input type="text" name="telefono[`+i+`][telefono_indp]" class="col-2 form-control form-control-sm centrado" placeholder="IND P" id="codtelindp`+i+`" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <input type="number" name="telefono[`+i+`][telefono_indc]" class="col-2 form-control form-control-sm centrado" placeholder="IND C" id="codtelindc`+i+`" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <input type="number" name="telefono[`+i+`][telefono_numero]" class="col-4 form-control form-control-sm centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum`+i+`">
                            <input type="number" name="telefono[`+i+`][telefono_ext]" class="col-2 form-control form-control-sm centrado" placeholder="EXT" id="codtelext`+i+`" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                </div>`;
        $( "#mastelefonos" ).after(dato);
        $("[data-toggle='tooltip']").tooltip();
    }

    quitar = function (id) {
        $(id).removeClass('rollIn').addClass('hinge');
        setTimeout(function(){ $(id).remove(); }, 1000);            
    }

    agregarCorreo = function () {
        j++;
        i++;
        correo =   `<div class="col-6 animated rollIn" id="C`+j+`">
                        <label for="n_a_pesos" class="col-form-label">Correo `+j+`</label>
                        <div class="input-group">
                          <input class="form-control form-control-sm" type="email" name="correo[`+i+`][mail]" placeholder="Por favor ingrese un correo">
                          <span class="input-group-btn">
                            <button class="btn btn-secondary btn-sm" type="button" onclick="quitar('#C`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                          </span>
                        </div>
                    </div>`; 
        $( "#correo" ).after(correo);
    }
   
    agregarRedesSociales = function() {
        i++;
        dato = `<div class="form-group row animated rollIn" id="R`+i+`">
                    <label class="col-2 col-form-label">Redes Sociales `+i+`</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <select name="redsocial[`+i+`][tipo]" class="col-2 form-control">
                                <option value="Facebook">Facebook</option>
                                <option value="Instagran">Instagran</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Linkedin">Linkedin</option>
                            </select>
                            <input type="text" class="col-10 form-control form-control-sm" name="redsocial[`+i+`][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social">
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#R`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                    </div>
                </div>`;
        $( "#redessociales" ).after(dato);
    }
    $(function () {
        // inicializamos el plugin
        $('#pais').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Pais",
            pais: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("pais") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#estados').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Departamento",
            estados: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("estados") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#pais').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#ciudad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una Ciudad",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("ciudades") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        estado: $('#estados').val(),
                        pais: $('#pais').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#empresa').select2({
            // Activamos la opcion "Tags" del plugin
             placeholder: "Seleccione una empresa",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("empresa") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#sede').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una sede",
            sede: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sede") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#empresa').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#cliente').select2({
            // Activamos la opcion "Tags" del plugin
             placeholder: "Seleccione una cliente",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("cliente") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        dato:$("#sede").val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $("[data-toggle='tooltip']").tooltip();
    });

    funcambiarinput = function (id, val) {
      valor = $("#"+id).val();
      if (valor==="Celular") {
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-4 col-10').addClass('col-8');            
      }else if(valor==="Telefono"){
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).show();
          $("#codtelext"+val).show();
          $("#codtelnum"+val).removeClass('col-8 col-10').addClass('col-4'); 
      }else if(valor==="Radio"){
          $("#codtelindp"+val).hide();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-4 col-8').addClass('col-10');
      }
    }

</script>
@endsection
