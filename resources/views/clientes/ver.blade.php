<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')
@section('title', 'Ver Cliente')
<!--@section('sidebar')
    @parent    
    @endsection-->
@section('content') 
<?php setlocale(LC_ALL, "es_CO.UTF-8"); ?>
<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }

    .backgroud{
      background: linear-gradient(rgba(0,0,0,0) 71%, rgba(0,0,0, .53));
    }
   
</style>
<style type="text/css">

  .observaciones-img{
    width: 60px;
  }

  .observaciones-p{
    font-size: small;
  }

  .observaciones-title{
    font-size: larger;    
    font-weight: bold;
  }

  #chartdiv {
    height: 100%;
  }

  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .img-empresa{
    padding: 30px;
    border: solid 0px;
    line-height: 140;
    position: absolute;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center;
    top: 50%;
    bottom: 50%;
  }

  .mx-auto {
    margin-right: auto!important;
    margin-left: auto!important;
    margin-bottom: auto;
    margin-top: auto;
  }

  .card-header {
    margin: 20px 0px;
  }
  
  .marco_principal{
    display: flex;
    position: relative;
    overflow: hidden;
    text-decoration: none;
  }

  .profile-empresa{    
    left: -80px !important;
    margin: auto!important;
    width: 250px;
    height: 250px;
    -webkit-border-radius: 200px 200px 200px 200px;
    top: -100px !important;
    position: relative;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  .profile-empresa2{
    top: -250px !important;
    left: 50px !important;
    margin: auto!important;
    width: 150px;
    height: 150px;
    -webkit-border-radius: 200px 200px 200px 200px;
    position: relative;
    -webkit-box-shadow: 0px 0px 28px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 0px 0px 28px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 0px 0px 28px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  p:first-letter {
    text-transform: uppercase !important;
  }

  span:first-letter {
    text-transform: uppercase !important;
  }

  .soloprimer:first-letter{
    text-transform: uppercase !important;
  }

  .capitalize{
    text-transform: capitalize !important;
  }

  
  p.normalp{
    font-weight: 400;
  }
  .unalinea{
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  .mas-pequeño{
    line-height: 1.5;
    font-size: small;
    color: #636363;
    font-weight: 400;
    display: table-row !important;
  }

  .centrar-vertical{
    vertical-align: middle !important;
  }

  .parrafo{
    color: #555;
    font-weight: 400;
    font-size: 16px;
    text-align: justify;
    line-height: 1.2;
  }
  .my-6{ margin-left: 25%; margin-right: 50%;}
</style>
<div class="container-fluid animated flipInX" id="list">
  <div class="row justify-content-md-center">
    <div class="col-12 col-md-auto panel-view">
      <div class="marco_principal">
        <img src="
         @if(!empty($sede->principal))
            {{ url('/') }}/images/file/empresas/sede/{{ $sede->principal }}
         @else
            https://placeholdit.imgix.net/~text?txtsize=33&txt=Principal&w=350&h=150 
         @endif" style="width: 1100px;height: 300px;margin: auto;">
      </div>
      <div class="profile-empresa">
        <img src="
           @if(!empty($model->foto))
              {{ url('/') }}/images/file/clientes/{{ $model->foto }}
           @else
              http://via.placeholder.com/250x250/fff/948e8e?text=Foto 
           @endif" class="img-fluid mx-auto d-block img-empresa">
      </div>
      <div class="profile-empresa2">
          <img src="
           @if(!empty($empresa->logo))
              {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
           @else
              http://via.placeholder.com/250x250/fff/948e8e?text=Logo 
           @endif" class="img-fluid mx-auto d-block img-empresa">
      </div>

      <div style="top: -350px !important; position: relative;">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
            <?php if($permiso_editar=="Si"){ ?>
              <a href="{{ url('editarcliente') }}/{{ $model->id }}" class="btn btn-amarillo btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Cliente</a>
            <?php } ?>
              <a href="#titulobservaciones" id="comentarbtn" class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Comentar</a>
            </div>
          </div>
          <div class="col-6 col-md-6 centrado">
            <div  class="hijo">
              <h2 class="titulo">{{ $empresa->nombre }}</h2>
              <h2 class="titulo1">{{ $model->tratamiento.". ".$model->cliente }}</h2>
              <p class="">{{ $model->cargo }}</p>
              <p class="">{{ $model->profesion }}</p>   
              <p class=""><i class="fa fa-map-marker"></i> {{ $model->ciudad.", ".$model->pais }}</p>
            </div>
          </div>
          <div class="col-6 col-md-6 centrado" style="padding-top: 70px">  
              <div class="row">
                <div class="col-md-12">
                  <div class="hijo">
                      <h6>
                      <i class="fa fa-star-o pull-left" aria-hidden="true"></i>
                      <p class="subtitulo pull-left">
                        Creada el <span class="capitalize">{{ strftime("%d %B %Y", strtotime($model->created_at)) }}</span>
                      </p>
                      </h6>
                      <h6 style="padding-top: 20px;">
                      <i class="fa fa-repeat pull-left" aria-hidden="true"></i>
                      <p class="subtitulo2 pull-left">
                        Actualizada el <span class="capitalize">{{ strftime("%d %B %Y", strtotime($model->updated_at)) }}</span>
                      </p>
                      </h6>
                      <h6 style="padding-top: 20px;">
                          <i class="fa fa-percent pull-left" aria-hidden="true"></i>
                          <p class="subtitulo2 pull-left">
                            Datos llenado al <span class="capitalize"><?=$data['porcentaje'][0]->Porcentaje?> %</span>
                          </p>
                      </h6>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="progress my-6">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$data['porcentaje'][0]->Porcentaje?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$data['porcentaje'][0]->Porcentaje?>%"></div>
                    </div>
                  </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="hijo2 row justify-content-md-center">
                <table class="table product-table">                                
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Jefe inmediato</th>
                        <th class="centrado">Valor autorizado en pesos</th>
                        <th class="centrado">Edad</th>
                        <th class="centrado">Numero de hijos</th>
                        <th class="centrado">Estado civil</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h2><strong>
                          <?php 
                            try{
                              $client =App\Cliente::findOrFail($model->jefe_inmediato);
                              echo $client->tratamiento.". ".$client->cliente ; 
                            } catch (Exception $e) {
                            }  
                          ?>
                          </strong></h2>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h3><strong>
                            <?php 
                              try{
                                echo "$ ".number_format($model->n_a_pesos); 
                              } catch (Exception $e) {
                                echo "$ ".$model->n_a_pesos; 
                              }  
                            ?>
                          </strong></h3>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h3><strong>
                          <?php
                            try {
                              if (!empty($model->fecha_nacimiento)) {
                                $date2 = date('Y-m-d');//
                                $diff = abs(strtotime($date2) - strtotime($model->fecha_nacimiento));
                                $years = floor($diff / (365*60*60*24));
                                echo $years;
                              }

                            } catch (Exception $e) {
                            }
                          ?>
                          </strong></h3>
                          <small class="capitalize">
                          {{ strftime("%A %d %B", strtotime($model->fecha_nacimiento)) }}
                          </small>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h3><strong>{{ $model->num_hijos }}</strong></h3>
                      </td>
                      <td class="centrado" style="display: table-cell;vertical-align: middle;">
                          <h3><strong>{{ $model->estado_civil }}</strong></h3>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class="table product-table">                                
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Telefonos</th>
                        <th class="centrado">Correos</th>
                        <th class="centrado">Redes Sociales</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td  style="display: table-cell;vertical-align: middle;">
                        <div style="width: 50%;text-align: justify;margin: 0 auto;">
                        <?php 
                        try{
                          $telefonosCliente = App\ClienteTelefonos::where("cliente_id", $model->id)->get();
                          foreach ($telefonosCliente as $value) {
                            if ($value->telefono_tipo === "Telefono") {
                              echo "<h5><strong>Telefono: </strong> +(".$value->telefono_indp.") (".$value->telefono_indc.") ".$value->telefono_numero."  ext ".$value->telefono_ext."</h5>";
                            }elseif ($value->telefono_tipo === "Celular") {
                              echo "<h5><strong>Celular: </strong>+(".$value->telefono_indp.") ".$value->telefono_numero."</h5>";
                            }elseif ($value->telefono_tipo === "Radio") {
                              echo "<h5><strong>Radio: </strong>".$value->telefono_numero."</h5>";
                            }
                          }
                        }catch (Exception $e){

                        }
                        ?>
                        </div>  
                      </td>
                      <td style="display: table-cell;vertical-align: middle;">
                        <div style="width: 50%;text-align: justify;margin: 0 auto;">
                        <?php 
                        try{
                          $telefonosCliente = App\ClienteMails::where("cliente_id", $model->id)->get();
                          foreach ($telefonosCliente as $value) {
                              echo "<h5><strong>".$value->mail."</strong></h5>";
                            
                          }
                        }catch (Exception $e){

                        }
                        ?>
                        </div>
                      </td>
                      <td style="display: table-cell;vertical-align: middle;">
                        <div style="width: 50%;text-align: justify;margin: 0 auto;">
                        <?php 
                        try{
                          $redes = App\ClienteRedessociales::where('cliente_id','=', $model->id)->get();
                          foreach ($redes as $key => $value) {
                            echo '<a target="_blank" href="'.$value->valor.'">';
                            if ($value->tipo === "Facebook") {
                              echo '<img src="'.url("/").'/images/icons_redes/facebook_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">';
                            }else if ($value->tipo === "Instagran") {
                              echo '<img src="'.url("/").'/images/icons_redes/instagram_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">';
                            }else if ($value->tipo === "Twitter") {
                              echo '<img src="'.url("/").'/images/icons_redes/twitter_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">';
                            }else if ($value->tipo === "Linkedin") {
                              echo '<img src="'.url("/").'/images/icons_redes/linkedin_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">';
                            }else {
                              echo '<img src="'.url("/").'/images/icons_redes/share.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">';
                            }
                            echo '</a>';
                          }
                        }catch (Exception $e){

                        }
                        ?>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class="table product-table">      
                  <thead>
                      <tr class="tr-table">
                          <th class="centrado">Perfil</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td class="centrado" style="display: table-cell;vertical-align: middle;">
                              <p class="parrafo">{{ $model->perfil }}</p>
                          </td>
                      </tr>
                  </tbody>
                </table>
                <table class="table product-table" style="margin-top: 50px;">
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Oportunidades</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="centrado">
                        <?php $opts = \Illuminate\Support\Facades\DB::select('SELECT * FROM historial_oportunidades WHERE `value` = '.$model->id.'  AND `key` = "cliente"');?>
                        @if (isset($opts) && !empty($opts) && count($opts)>0)
                          <div class="table-responsive">            
                            <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                              <thead>
                                <tr>
                                  <th class="centrado centrar-vertical">Item</th>
                                  <th class="centrado centrar-vertical">Ciudad</th>
                                  <th class="centrado centrar-vertical">Productos</th>
                                  <th class="centrado centrar-vertical">Valor Oportunidad</th>
                                  <th class="centrado centrar-vertical">Responsable</th>
                                  <th class="centrado centrar-vertical">Pais</th>
                                  <th class="centrado centrar-vertical">Fecha identificación</th>
                                  <th class="centrado centrar-vertical">Fecha cierre</th>
                                  <th class="centrado centrar-vertical">Ciclo de Venta</th>
                                  <th class="centrado centrar-vertical">Probabilidad</th>
                                  <th class="centrado centrar-vertical">Acciones</th>                  
                                </tr>
                              </thead>
                              <tbody>
                                <?php $i=1; ?>
                                @foreach($opts as $datoA) 

                                  <?php $dato = App\Oportunidades::findOrFail($datoA->oportunidad_id); if (isset($dato->id) && !empty($dato->id)) { $historia = App\HistorialOportunidades::where('oportunidad_id', $dato->id)->get();?>
                                  <tr class="text-center dato">
                                      <td class="centrar-vertical"><a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $i++ }}</p></a></td>
                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato->ciudad }}</p></a>
                                      </td>

                                      <?php 
                                        $productosRes = App\OportunidadProducto::where("oportunidad_id",$dato->id)->get();
                                        $tootilProductos = "";                     
                                        foreach ($productosRes as $res) { 
                                          try { 
                                            if (!empty($res->producto)) { 
                                              if (is_numeric($res->producto)) {
                                                $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                                              }else{
                                                $producto->name = "Otro";
                                              }
                                              if (is_numeric($res->referencia)) {
                                                $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop(); 
                                              }else{
                                                $referencia->referencia = "Estandar";
                                              }
                                              $tootilProductos .=$res->cantidad.' '.$producto->name.' '.$referencia->referencia.', '; 
                                            } 
                                          } catch (Exception $e) { } 
                                        } 
                                      ?>              
                                      <td class="centrar-vertical" style="text-align: center;">
                                        <a href="{{ url('registraroportunidad') }}" target="_blank" class="normal_a" style="font-size: smaller; padding: 0px !important;margin: 0px !important;"><p class="mas-pequeño">{{ $tootilProductos }}</p></a>
                                      </td>

                                      <td class="unalinea centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                                        <p class="mas-pequeño">
                                          <?php 
                                            try { 
                                              $otro = $historia->filter(function ($value, $key) {
                                                  return $value->key == "val_pesos";                            
                                              });
                                              $valpesosoportunidad = $otro->pop(); 
                                              if(isset($valpesosoportunidad->value)){
                                                  echo "$ ".$valpesosoportunidad->value;
                                              }else{
                                                  echo "$ 0";
                                              }
                                            } catch (Exception $e) { }
                                          ?>
                                        </p>
                                        </a>
                                      </td>

                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                                        <p class="mas-pequeño">
                                        <?php 
                                          try {
                                            $otro = $historia->filter(function ($value, $key) {
                                              return $value->key == "responsable";                            
                                            });
                                            $casi = $otro->pop(); 
                                            if(isset($casi->value) && !empty($casi->value)){
                                              $nombreU = App\User::find($casi->value);
                                              if (isset($nombreU)) {
                                                $nombre   = explode(" ",$nombreU->nombres);
                                                $apellido = explode(" ",$nombreU->apellidos); 
                                                echo $nombre[0] ." ". $apellido[0]; 
                                              }else{
                                                echo $casi->value;
                                              }
                                            } 
                                          } catch (Exception $e) {
                                            
                                          }
                                        ?>
                                        </p>
                                        </a>
                                      </td>

                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato->pais}}</p></a>
                                      </td> 

                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                                        <p class="mas-pequeño">
                                        <?php 
                                          $otro = $historia->filter(function ($value, $key) {
                                            return $value->key == "fecha_ff";                            
                                          });
                                          $casi = $otro->pop(); 
                                          if (isset($casi->value)) {echo date($casi->value);} 
                                        ?>
                                        </p>
                                        </a>
                                      </td>
                                      
                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                                        <p class="mas-pequeño"> 
                                        <?php 
                                          $otro = $historia->filter(function ($value, $key) {
                                            return $value->key == "fecha_oc";                            
                                          });
                                          $casi = $otro->pop(); 
                                          if (isset($casi->value)) {echo $casi->value;}
                                        ?>
                                        </p>
                                        </a>
                                      </td>                   

                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                                        <p class="mas-pequeño">
                                        <?php 
                                          $otro = $historia->filter(function ($value, $key) {
                                            return $value->key == "ciclo_venta";                            
                                          });
                                          $casi = $otro->pop(); 
                                          $casi->value = intval($casi->value);
                                          if (isset($casi->value)) {
                                            if ($casi->value === 0) {
                                              echo "Perdida";
                                            }else if ($casi->value >= 90) {
                                              echo "Vendida";
                                            }else{
                                              echo $casi->value." %";
                                            }                                  
                                          } 
                                        ?>
                                        </p>
                                        </a>
                                      </td>

                                      <?php 
                                        $otro = $historia->filter(function ($value, $key) {
                                          return $value->key == "probabilidad";                            
                                        });
                                        $casi = $otro->pop();
                                      ?>

                                      <td class="centrar-vertical">
                                        <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                                        <p class="mas-pequeño"> 
                                          <?php if (isset($casi->value)) {echo $casi->value;} ?>
                                        </p>
                                        </a>
                                      </td>
                                      <td class="text-center unalinea centrar-vertical">
                                        <a href="{{ url('/') }}/oportunidad/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver oportunidad">
                                          <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ url('/') }}/acciones/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Gestionar ciclo de venta">
                                          <i class="fa fa-sort-numeric-desc"></i>
                                        </a>
                                        <a href="{{ url('/') }}/oportunidad/edit/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar oportunidad">
                                          <i class="fa fa-pencil"></i>
                                        </a>
                                      </td>
                                  </tr>
                                  <?php } ?>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        @else
                          <li class="list-group-item justify-content-between">No se encontraron oportunidades relacionadas con esta empresa</li>
                        @endif

                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class="table product-table" style="margin-top: 50px;"><a name="titulobservaciones"></a>
                  <thead>
                    <tr class="tr-table">
                        <th class="centrado">Observaciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="col-md-12">
                            <div id="lisComentariosId"></div>
                            <form id="formComentarios">
                              {{ csrf_field() }}
                              <input type="hidden" name="cliente_id" value="{{ $model->id }}">
                              <div class="md-form" style="background-color: rgba(174, 219, 251, 0.71);margin-top: 50px;">
                                <textarea type="text" id="form7" class="md-textarea" name="comentario"></textarea>
                                <label for="form7" style="padding-left: 8px;">Añadir comentario</label>
                              </div>
                            </form>
                            <div class="centrado">
                              <a href="#" class="boton negro redondo" onclick="guardarComentario({{ $model->id }})"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                            </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
        
      </div>
    </div>
   </div>
</div>
@endsection


@section('scripts')
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/mdb.min.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<!--sweetalert -->
<script type="text/javascript" src="{{ url('/') }}/js/plugins/sweetalert/sweetalert.min.js"></script>  

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>

<script>
  direccion="{{ url('/') }}";
  var image = "";
  var contentString = "";

  $(function() {
    getComentarios({{ $model->id }});
    $( "#comentarbtn" ).click(function() {
      setTimeout(function(){ $("#form7").focus() }, 1000);
    });
  });
  


  guardarComentario = function (id) {
    event.preventDefault();    
    if(true === $("#formComentarios").parsley().validate()){
      console.log("llega"); 
      var datos = new FormData($("#formComentarios")[0]);
      $.ajax({
        url: "{{ url('savecomencliente') }}",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
          if (data.success) {
            $('#formComentarios')[0].reset();
            getComentarios(id);
          }else{
            swal("Algo salio mal, vuelve a intentar");
          }
        }
    }); 
    }else{
      swal("Faltan campos por completar!");
    }
  }

  getComentarios = function (id) {
      $.ajax({
          url: "{{ url('listcomencliente') }}/"+id,
          type: 'GET',
          success: function(data){
            if (data.success) {
              console.log(data.datos);
              var html3 = "";
              moment.locale('es');
              for(i in data.datos){
                comentario = data.datos[i];
                html3 += `
                <div class="media mb-1 comentariosImg">
                  <a class="media-left waves-light">
                      <img class="rounded-circle observaciones-img" src="{{ url('/') }}/images/file/clientes/`+comentario.user.foto+`" class="rounded-circle z-depth-1-half" alt="Generic placeholder image">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading observaciones-title">`+comentario.user.nombres+` `+comentario.user.apellidos+` <small>`+moment(comentario.created_at).calendar()+`</small> </h4>
                      <p class="parrafo">`+comentario.comentario+`</p>
                  </div>
                </div>`;
              }
              $("#lisComentariosId").html(html3);
            }else{

            }
          }
      });
    }
</script>
@endsection

