<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>AgendaSmart - @yield('title')</title>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="author" content="Edwin Ferreira Martinez">
        <meta name="description" content="xxxxxxxx">
        <meta name="keywords" content="xx, xx, xx, xx, xx xx xx">
 <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Secular+One" rel="stylesheet">

        <link rel="icon" href="http://bigdata.hes.com.co/images/favicon_essi.png" sizes="32x32">
        <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css" crossorigin="anonymous">
        <link href="{{ url('/') }}/css/app1.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.css">
        <link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/') }}/css/calendar.css">
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.bootstrap4.min.css"/>
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/bootstrap-select/css/bootstrap-select.min.css"/>
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/sweetalert/sweetalert2.min.css"/>
        <link href="{{ url('/') }}/components/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="{{ url('/') }}/components/file/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/3Dtimeline/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,500" rel="stylesheet">

        <style>
        #chartdiv {
          width: 100%;
          height: 500px;
        }     
        *{
          font-family: 'Raleway', sans-serif;
          font-weight: 400;
        }
        th{
          font-weight: bold;
        }
        .inline-block{
          display: inline-block;
        }
        .table td{
          line-height: 2;
          font-size: 15px !important;
          padding: .7px !important;
          vertical-align: middle;
        }
        #archivos img{
          margin: 5px;
        }
        .display-line{
              display: inline;
        }
        .logo-essi-modal{
          margin-top: 32px;
        }
        .white{
          color: #ffffff;
        }
        .margen-1{
          margin: 1px;
        }
        .tab-content{
          background-color: #ffffff;
          padding: 5px;
          position: relative;
      }
      .relativo{
        position: relative;
      }
      .tabs{
        margin: 0.5px;
        background-color: #fff;
        color: #1f63dd;
        padding: 5px;
        border: 1px solid #1f63dd;
      }

      .tabs.active{
        margin: 0.5px;
        background-color: #001662;
        color: #fff;
        padding: 5px;
      }
      .tabs a{
        font-size:13px;
        margin-left: 25px;
        margin-right: 25px; 
      }
      .tabs.active a{
        color: #fff;
      }
      .tabs a{
        color: #1f63dd;
      }
      li[aria-expanded='false']{
        margin: 0.5px;
        background-color: #fff;
        color: #1f63dd;
        padding: 5px;
        border: 1px solid #1f63dd;
      }
      li[aria-expanded='true']{
        margin: 0.5px;
        background-color: #001662;
        color: #fff;
        padding: 5px;
      }
        </style>
        <!-- Styles -->
        <style>

          /*.select2-container--default .select2-results>.select2-results__options {
            max-height: 400px !important;
          }*/

          .btn-essi {
              color: #fff;
              background-color: #051d60;
              border-color: #051d60;
          }

          .normal_a{
            color: #636b6f;
            text-decoration: none !important;
          }

          .contenido{
            background: #fff;
            border-radius: 15px 15px 15px 15px;
            -moz-border-radius: 15px 15px 15px 15px;
            -webkit-border-radius: 15px 15px 15px 15px;
            border: 0px solid #fff;
            margin-top: 20px;
          }

          .centrado{
            text-align: center;
          }

          .letra-gris{
            color: #54575a;
            font-size: larger;
          }
            
          .block{
            display: block;
          }
          .select2 {             
            width: 100% !important;
          }

          /*.select2-container--default .select2-selection--single .select2-selection__arrow{
            top: 20% !important;
          }*/

          /*.select2-container--default .select2-selection--single{
            display: block;
            padding: .5rem .75rem;
              font-size: 1rem;
              line-height: 1.25;
              color: #464a4c;
              background-color: #fff;
              background-image: none;
              -webkit-background-clip: padding-box;
              background-clip: padding-box;
              border: 1px solid rgba(0,0,0,.15);
              border-radius: .25rem;
              -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
              transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
              -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
              transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
              transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
          }*/

          /*.select2-container .select2-selection--single{
            height: auto !important;
          }*/

          #chartdiv {
              width   : 100%;
              height  : 500px;
          }

          #chartdiv2 {
              width   : 100%;
              height  : 500px;
          }

          #chartdiv3 {
              width   : 100%;
              height  : 500px;
          }

          #chartdiv4 {
            width: 100%;
            height: 500px;
            margin: auto;
          } 
          .fondo_title {
            padding: 5px;
            min-width: 100%;
            border: 0px solid #e6e6e6;
            border-radius: 26px 26px 26px 26px;
            -moz-border-radius: 26px 26px 26px 26px;
            -webkit-border-radius: 26px 26px 26px 26px;
            border: 0px solid #e6e6e6;
            background-color: #e6e6e6;
          }

          .input-disable {
            /*line-height: 1.2 !important;*/
            color: #3a3b3b;
            background-color: #ddd !important;
            background-clip: padding-box !important;
            border: 2px solid rgba(0,0,0,.15) !important;
            border-radius: .0rem !important;
          } 

          .txtr3 {
            white-space: pre-wrap;
            word-wrap: break-word;
            height: auto;
            font-size: 17px;
            font-weight: 600;
            font-family: 'PT Serif', serif !important;
            text-align: left;
          } 

          /*body{
            background: #000;
            padding-top: 10px;
          }*/

          /*.p-texto {
            color: #051d60; 
            font-family: "Courier";
            font-size: 12px;
            margin: 10px 0 0 10px;
            white-space: nowrap;
            overflow: hidden;
            width: 100%;
            animation: type 0s steps(60, end); 
          }

          .p-texto:nth-child(2){
            animation: type2 20s steps(60, end);
          }

          .span-letras{
            animation: blink 1s infinite;
          }

          .logo-essi{
            animation: blink 3s infinite;
            transition: all .2s ease-in-out;  
          }

          .logo-essi:hover{  
            transform: scale(1.8); 
          }*/

          @keyframes type{ 
            from { width: 0; } 
          } 

          @keyframes type2{
            0%{width: 0;}
            50%{width: 0;}
            100%{ width: 100; } 
          } 

          @keyframes blink{
            to{opacity: .0;}
          }

          ::selection{
            background: black;
          }

          /*body { 
              font-family: monospace; 
              background: tomato;
          }*/
          .contenedor {
              margin: auto;
              display: table;
          }

          /*.p-texto { 
              position: relative; 
              float: left;
              background: tomato;
              overflow: hidden;
              color: #fff;
              font-size: 2.5em;
          }*/

          h1 { 
            position: relative; 
            float: left;
            background: #051d60;
            color: #fff;    
            font-size: 18px;
          }

          h1 span {
            position:absolute;
            right:0;
            width:0;
            background: #051d60;
            border-left: 1px solid #000;
            animation: escribir 10s steps(88) infinite alternate;
          }

          @keyframes escribir {
            from { width: 100% }
            to { width: 0 }
          }

          .logo-essi{
            animation: blink 3s infinite;
            transition: all .2s ease-in-out;  
          }

          .logo-essi:hover{  
            transform: scale(1.8); 
          }

          .logo-essi2{
            animation: blink 1s infinite;
            transition: all .2s ease-in-out;  
          }

          .logo-essi2:hover{  
            transform: scale(1.8); 
          }
          img.img-fluid.rounded-circle.menu {
              max-width: 25%;
          }

</style>

<style type="text/css">
  body,html{
    height:100%; /*Siempre es necesario cuando trabajamos con alturas*/
  }

  #inferior{
    color: #FFF;
    background: #051d60;
    font-size:22px;
    position:absolute; /*El div será ubicado con relación a la pantalla*/
    left:0px; /*A la derecha deje un espacio de 0px*/
    right:0px; /*A la izquierda deje un espacio de 0px*/
    bottom:0px; /*Abajo deje un espacio de 0px*/
    height:50px; /*alto del div*/
    z-index:0;
    font-family: 'Secular One', sans-serif;
  }

 .magnify {
      position: relative;
      cursor: none
  }

  .magnify-large {
      position: absolute;
      display: none;
      width: 500px;
      height: 500px;

      -webkit-box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.55), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
         -moz-box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.55), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
              box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.55), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
      
      -webkit-border-radius: 100%;
         -moz-border-radius: 100%;
               border-radius: 100%
  }

  .logo-essi-video{
    position: fixed;
    z-index: 1;
    right: 12%;
    top: 3%;
    opacity: 0.7;
    width: 10%;
  }







#loader{
  z-index:999999;
  display:block;
  position:fixed;
  top:0;
  left:0;
  width:100%;
  height:100%;
  background:url("{{ url('/') }}/images/logo.png") 50% 50% no-repeat #cccccc; 
}

body {
  background-image: url("{{ url('/') }}/images/fondo.png");
}
</style>

<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>

<style>
      html, body {
        margin: 0;
        padding: 0;
      }
      #map {
        position: relative;
        height: 400px;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
      }

      .coordinates {
        position: relative;
        bottom: 21px;
        left: 50%;
        width: 499px;
        height: 40px;
        margin: 0 0 0 -250px;
        text-align: center;
        line-height: 50px;
        color: #fff;
        z-index: 1;
      }

      .coordinates em.lat {
          left: 0;
      }

      .coordinates em {
          position: absolute;
          top: -20px;
          width: 249px;
          height: 20px;
          background: rgba(0,0,0,0.75);
          color: #fff;
          font-style: normal;
          letter-spacing: 1px;
          font-size: 12px;
          line-height: 21px;
          text-transform: uppercase;
          font-weight: normal;
      }

      #map .coordinates em.lon {
          right: 0;
      }
      #map .coordinates em {
          position: absolute;
          top: -20px;
          width: 249px;
          height: 20px;
          background: rgba(0,0,0,0.75);
          color: #fff;
          font-style: normal;
          letter-spacing: 1px;
          font-size: 12px;
          line-height: 21px;
          text-transform: uppercase;
          font-weight: normal;
      }

      .coordinates #lng {
          float: right;
      }
     
      .coordinates input {
          display: block;
          float: left;
          width: 249px;
          font-size: 18px;
          line-height: 40px;
          background: #000;
          text-align: center;
          color: #fff;
          border: none;
          -webkit-transition: all .2s;
          transition: all .2s;
      }


      /* Semáforo realista CSS */

      ul.semaforo {
        top: -25px;
        float: left;
        position: relative;
        width: 45px;
        height: 110px;
        margin: 0 5px;
        padding: 5px;
        list-style-type: none;
        border: 3px double grey;
        background: black;
        background: linear-gradient( -90deg, #45484d 0%, #000000 100%);
        border-radius: 10%;
        box-shadow: 0px 0px 0 7px rgba(0,0,0,.8);
      }
      ul.semaforo li {
        position: relative;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background: lightgrey;
        margin: 1px 0;
        background: linear-gradient(top, rgba(187,187,187,1) 0%,rgba(119,119,119,1) 99%);
        box-shadow: inset 0 -5px 15px rgba(255,255,255,0.4),
        inset -2px -1px 40px rgba(0,0,0,0.4),
        0 0 1px #000;
      }
      .Baja li:nth-of-type(3) {
        background: #cc0000;
        background: radial-gradient(center, ellipse cover, #ff0000 1%, #cc0000 100%);
      }
      .Media li:nth-of-type(2) {
        background: #FF9326;
      }
      .Alta li:nth-of-type(1) {
        background: #00ff00;
      }

      .logo-empresa-fotante{
        border-radius: 50%;
        width: 200px;
        height: 200px;
        top: -100px !important;
        position: relative;
        border-color: white;
        -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
        -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
        box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
      }

 

    </style>
    </head>
    <body onload="myFunction()">
      <div id="loader"></div>

        <nav class="navbar navbar-toggleable-md navbar-inverse bg-faded" style="background-color: #051d60">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand" href="{{url('/')}}/comercial"><img src="{{ url('/') }}/images/logo.png" class="img-peque logo-essi"/> <span>Comercial</span></a>

          <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-md-0">
              <!--<li class="nav-item active">
                <a class="nav-link" href="#">Inicio</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('informe') }}">Informes</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('oportunidades') }}">Oportunidades</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('calendario') }}">Bitacora</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('productos') }}">Productos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('crearcliente') }}">Clientes</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('crearempresa') }}">Empresas</a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="{{ url('backlog/principal') }}">Backlog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('plantrabajo') }}">Plan De Trabajo</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('funcionarios') }}">Usuarios</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('archivosimportantes') }}">Arch. Importantes</a>
              </li>-->
              <li class="nav-item">
                <a class="nav-link" id="menu_mega" style="cursor: pointer;">Menu</a>
              </li>
              @if(isset($usuario_megarchivo)) @if(($usuario_megarchivo->tipousuario)=='Administrador Sistema')
              <li class="nav-item" >
                <a class="nav-link" id="tabl_mega" style="cursor: pointer;" >Tablero Megarchivo</a>
              </li>
              <li class="nav-item" >
                <a class="nav-link" id="configuracion_mega" style="cursor: pointer; display: none;" >Configuración</a>
              </li>
              @endif
              @endif
            </ul>
            <!--<div class="form-inline my-2 my-lg-0">
              <input  id="search_input" class="form-control mr-sm-2" type="text" placeholder="Buscar">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </div>-->
            <ul class="nav navbar-nav navbar-right" id="usuario">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="#">Cambia contraseña</a>
                      <a class="dropdown-item" href="{{ url('/logout') }}">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
            @if(isset($tiempo))
            <ul class="nav navbar-right">
              <li class="user-details cyan darken-2" style="width: 226px;height: 45px; text-align: right;">
                <img src="{{ url('/')}}/storage/FUNCIONARIO/{{ $usuario_megarchivo->foto }}" style="max-width: 75px; max-height: 95px;" class="img-circle">
              </li>
              <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      @if(isset($usuario_megarchivo)) {{ $usuario_megarchivo->tipousuario }} @else {{ Auth::user()->name }} @endif
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="{{ url('/megaarchivo') }}">Cerrar sesión</a>
                    </div>
              </li>
            </ul>
            @endif
          </div>
        </nav>
        <form action="{{ url('/') }}/dashboardmegarchivo" method="POST" class="col s12" enctype="multipart/form-data" style="display: none;"> 
          {{ csrf_field() }}
          <input type="hidden" name="tiempo_dash" id="tiempo_dash" value="">
          <button type="submit" class="btn btn-info mb-2" id="btn-dashboard">Ingresar<i class="fa fa-send ml-1"></i></button>
        </form>
        <form action="{{ url('/') }}/megaarchivo/tipoingreso" method="POST" class="col s12" enctype="multipart/form-data" style="display: none;"> 
          {{ csrf_field() }}
          <input type="hidden" name="tiempoid" id="tiempo_menu" value="">
          <button type="submit" class="btn btn-info mb-2" id="btn-menumega">Ingresar<i class="fa fa-send ml-1"></i></button>
        </form>
        <form action="{{ url('/') }}/megaarchivo/configuracion" method="POST" class="col s12" enctype="multipart/form-data" style="display: none;"> 
          {{ csrf_field() }}
          <input type="hidden" name="tiempoid" id="tiempo_conf" value="">
          <button type="submit" class="btn btn-info mb-2" id="btn-confmega">Ingresar<i class="fa fa-send ml-1"></i></button>
        </form>
        @yield('content') 

       <?php
          if (!isset($gridster)) { ?>
              <script src="{{ url('/') }}/js/jquery-3.1.1.slim.min.js" crossorigin="anonymous"></script>        
              <script src="{{ url('/') }}/js/jquery.min.js"></script>
          <?php
          }
         ?>
        <script src="{{ url('/') }}/js/tether.min.js" crossorigin="anonymous"></script>
        <script src="{{ url('/') }}/js/bootstrap.min.js" crossorigin="anonymous"></script>
        
        <script src="{{ url('/') }}/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ url('/') }}/js/bootstrap-datepicker.es.min.js"></script>
        <script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
        <script src="{{ url('/') }}/js/select2.min.js"></script>
        <script src="{{ url('/') }}/components/sweetalert/sweetalert2.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script type="text/javascript">
            $(function() { 
                /*$("body").attr({
                    oncontextmenu: 'return false',
                    onkeydown: 'return false'
                });

                $(document).bind("contextmenu",function(e){
                    return false;
                });

                window.frames["miframe"].document.oncontextmenu = function(){ return false; };

                $('.miiframeblog').contents().find('a').click(function(event) {
                    event.preventDefault();

                }); 

                var Element = window.parent.document.getElementById('tdFrame');
                if (Element == null) {
                    document.write('<b style="font-family:Arial;font-size:20pt;">Not Authorized to View this Page</b>');
                }*/
            });

            myFunction = function () {
              $("#loader").hide();
            }

            $('.tabs a').click(function (e) {
              e.preventDefault()
              $(this).tab('show')
            })
            $('body').on('click','#tabl_mega',function(){
              @if(isset($tiempo))
              id_tiempo='{{ $tiempo }}'; id_tiempo=parseInt(id_tiempo);
              @else
              id_tiempo='';
              @endif
              $('#tiempo_dash').val(id_tiempo);
              $('#btn-dashboard').click();
            });
            $('body').on('click','#menu_mega',function(){
               @if(isset($tiempo))
              id_tiempo='{{ $tiempo }}'; id_tiempo=parseInt(id_tiempo);
              @else
              id_tiempo='';
              @endif
              $('#tiempo_menu').val(id_tiempo);
              $('#btn-menumega').click();
            });
            $('body').on('click','#configuracion_mega',function(){
                @if(isset($tiempo))
                id_tiempo='{{ $tiempo }}'; id_tiempo=parseInt(id_tiempo);
                @else
                id_tiempo='';
                @endif
                $('#tiempo_conf').val(id_tiempo);
                $('#btn-confmega').click();
            })
        </script>
        @yield('scripts')
        
    </body>
</html>
