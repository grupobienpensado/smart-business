<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="es-Es" xmlns="http://www.w3.org/1999/xhtml">
<head>
      <title>SmartBusiness - @yield('title')</title>
      <meta name="robots" content="noindex,nofollow" />
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      <meta Cache-Control: max-age>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta name="author" content="Edwin Ferreira Martinez">
      <meta name="description" content="CRM">
      <meta name="keywords" content="crm, essi, colombia, grupo bien pensado, smart business">
      <!--<meta charset="UTF-8">-->

      <!--Bootstrap 4-->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

      <!--<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css">-->
      <link href="{{ url('/') }}/css/app.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/animate.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/calendar.css">
      <link href="{{ url('/') }}/components/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="{{ url('/') }}/css/sweetalert2.min.css">

        <!-- tema dashboard -->
        <!--Bootstrap 3-->
      <link href="{{ url('/') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{ url('/') }}/assets/css/main.css" rel="stylesheet" type="text/css">
      <link href="{{ url('/') }}/demo-style-switcher/assets/css/style-switcher.css" rel="stylesheet" type="text/css">
      <!-- fin tema dashboard -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
      <!-- Fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('/') }}/assets/ico/kingadmin-favicon144x144.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ url('/') }}/assets/ico/kingadmin-favicon114x114.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('/') }}/assets/ico/kingadmin-favicon72x72.png">
      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ url('/') }}/assets/ico/kingadmin-favicon57x57.png">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
      <link rel="shortcut icon" href="{{ url('/') }}/assets/ico/kingadmin-favicon144x144.png">
      <link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet">
      <link rel="icon" href="{{ url('/') }}/images/favicon.ico" sizes="32x32">
      <style type="text/css">
          .dropzone img{position:absolute!important}
          .text-muted{text-transform:lowercase;white-space:normal!important;text-align:justify}
          span.caret{display:none}
          .top-bar.navbar-fixed-top{min-height:49px!important}
          .top-bar .logged-user .dropdown-menu{left:40px!important;top:31px;padding:0}
          ul.main-menu > li a{padding:8px 0 8px 4px !important}
          ul.main-menu > li .toggle-icon{top:8px!important}
          .js-sub-menu-toggle >img{position:relative;right:-6px}
          .total-conferencia{font-size:10px;color:#fff!important;background-color:#011d62;border-radius:7px;border:2px solid #011d62;position:absolute;top:5.3%;left:24.1%;z-index:1}
          .total-galeria{font-size:10px;color:#fff!important;background-color:#011d62;border-radius:7px;border:2px solid #011d62;position:absolute;top:5.3%;left:11.5%;z-index:1}
          .total-invitados{font-size:10px;color:#fff!important;background-color:#011d62;border-radius:7px;border:2px solid #011d62;position:absolute;top:5.3%;left:17.5%;z-index:1}
          @media screen and (max-width: 1367px) {
          .total-conferencia{font-size:10px;color:#fff!important;background-color:#011d62;border-radius:7px;border:2px solid #011d62;position:absolute;top:6.6%;left:33.5%;z-index:1}
          .total-galeria{font-size:10px;color:#fff!important;background-color:#011d62;border-radius:7px;border:2px solid #011d62;position:absolute;top:6.6%;left:16.2%;z-index:1}
          .total-invitados{font-size:10px;color:#fff!important;background-color:#011d62;border-radius:7px;border:2px solid #011d62;position:absolute;top:6.6%;left:24.6%;z-index:1}
          }
          .dtp div.dtp-picker{padding:0!important}
          .dtp div.dtp-date,.dtp div.dtp-time{background:#051d60!important}
          .dtp .p10 > a{color:#fff!important}
          .dtp div.dtp-actual-year{color:#fff!important}
          .dtp > .dtp-content > .dtp-date-view > header.dtp-header{background:#06298c!important}
          .dtp table.dtp-picker-days tr > td > a.selected{background:#051d60!important}
          .main-menu li a:hover{ background-color: #051d60 !important; color: #fff;}
          .main-menu li a span:hover{ background-color: #051d60 !important; }
          .activo-menu{background-color: #051d60 !important;color: white !important;}
          .shadow-1{box-shadow:0 1px 3px rgba(0,0,0,0.12),0 1px 2px rgba(0,0,0,0.24);transition:all .3s cubic-bezier(.25,.8,.25,1)}
          .shadow-1:hover{box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22)}
          .shadow-2{box-shadow:0 3px 6px rgba(0,0,0,0.16),0 3px 6px rgba(0,0,0,0.23)}
          .shadow-3{box-shadow:0 10px 20px rgba(0,0,0,0.19),0 6px 6px rgba(0,0,0,0.23)}
          .shadow-4{box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22)}
          .shadow-5{box-shadow:0 19px 38px rgba(0,0,0,0.30),0 15px 12px rgba(0,0,0,0.22)}
          .main-menu li{height: 26px;}
          .main-menu li a{ font-size: 1.4rem;height: 26px; }
          .left-sidebar.minified {z-index: 11;}
      </style>

    </head>
    <body onload="myFunction()" class="topnav-fixed">
      <div id="loader"></div>


      <div id="wrapper" class="wrapper">
        <!-- TOP BAR -->
        <div class="top-bar navbar-fixed-top">
            <div class="clearfix">
              <!-- logo -->
              <div class="pull-left left">
                <div class="title-menu"><img src="{{ url('/') }}/images/sb_logo.svg" style="position: absolute;
                  max-height: 47px;
                  top: 1px;
                  float: left;
                  max-width: 270px;"></div>
              </div>
              <!-- end logo -->
              <div class="pull-right right">
                <div class="top-bar-right">
                  <div class="logged-user">
                    <div class="btn-group">
                      <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                          @if(!empty(Auth::user()->foto))
                        <img src="{{ url('/') }}/images/file/clientes/{{ Auth::user()->foto }}" alt="User Avatar" class="img-circle" style="    max-height: 30px;" />
                          @else
                          <img src="https://placeholdit.imgix.net/~text?txtsize=65&txt=Sin%20Perfil&w=200&h=200" alt="User Avatar" class="img-circle" style="    max-height: 30px;" />
                          @endif
                        <span class="name" style="text-transform: capitalize !important;">
                          <?php  
                            try{
                              $nombre   = explode(" ",Auth::user()->nombres);
                              $apellido = explode(" ",Auth::user()->apellidos); 
                              echo $nombre[0] ." ". $apellido[0]; 
                            }catch (Exception $e) {}
                          ?>
                        </span> <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="{{ url('/logout') }}">
                            <i class="fa fa-power-off"></i>
                            <span class="text">Cerrar sesión</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- end logged user and the menu -->
                </div>
                <!-- end top-bar-right -->
              </div>
            </div>
        </div>
        <!-- END TOP BAR -->
        <!-- LEFT SIDEBAR -->
        <div id="left-sidebar" class="left-sidebar  minified" style="position: fixed;">
          <div class="sidebar-minified js-toggle-minified" id="cabecera_menu" onclick="abrirMenuSidebar()">
            <i class="fa fa-exchange"></i>
          </div>
          <!-- main-nav -->
          <div class="sidebar-scroll">
            <nav class="main-nav">
              <ul class="main-menu">
                <?php                  
                $menu = Illuminate\Support\Facades\DB::select('SELECT * FROM `menu_crms` WHERE `sub_item` IS NULL ORDER BY `orden` ASC');
                foreach($menu as $m){ 
                  $permiso_usuario_menu="No";
                  $cargo = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                    if(isset($cargo[0]->id)){
                      $permiso = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_menu` WHERE `id_menu`="'.$m->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                      if(isset($permiso[0]->permiso)){
                        if($permiso[0]->permiso == "Si"){
                          $permiso_usuario_menu="Si";
                        }
                      }
                    } 
                    if($permiso_usuario_menu=="Si"){
                      $submenu = Illuminate\Support\Facades\DB::select('SELECT * FROM `menu_crms` WHERE `sub_item`="'.$m->id.'" ORDER BY `id` ASC');
                      ?>
                      <?php if (isset($m->url) && !empty($m->url)): ?>
                        <?php if (isset($m->img) && !empty($m->img)): ?>
                            <li><a href="{{url('/')}}/{{$m->url}}" class="js-sub-menu-toggle" id="menu_{{$m->id}}"><img src="{{url('/')}}/images/iconos menu y modal/{{$m->img}}"><span class="text">
                        <?php else: ?>
                            <li><a href="{{url('/')}}/{{$m->url}}" class="js-sub-menu-toggle" id="menu_{{$m->id}}"><i class="{{$m->logo}}"></i><span class="text">
                        <?php endif ?> 
                      <?php else: ?>
                        <?php if (isset($m->img) && !empty($m->img)): ?>
                            <li><a href="#" class="js-sub-menu-toggle" id="menu_{{$m->id}}"><img src="{{url('/')}}/images/iconos menu y modal/{{$m->img}}"><span class="text">
                        <?php else: ?>
                            <li><a href="#" class="js-sub-menu-toggle" id="menu_{{$m->id}}"><i class="{{$m->logo}}"></i><span class="text">
                        <?php endif ?>                        
                      <?php endif ?>
                      {{$m->item}}</span>
                        <i class="toggle-icon fa fa-angle-left"></i></a>
                          <?php if(count($submenu) > 0){ ?>
                          <ul class="sub-menu mt-4">
                            <?php 
                            foreach($submenu as $s){ 
                              $permiso_usuario_submenu="No";
                              $permiso1 = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_menu` WHERE `id_menu`="'.$s->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                              if(isset($permiso1[0]->permiso)){
                                if($permiso1[0]->permiso == "Si"){
                                  $permiso_usuario_submenu="Si";
                                }
                              }
                              if($permiso_usuario_submenu=="Si"){
                              ?>

                              <?php if (isset($s->url) && !empty($s->url)): ?>
                                <li><a href="{{ url('/') }}/{{$s->url}}"><span class="text">{{$s->item}}</span></a></li>
                              <?php else: ?>
                                <li><a href="#"><span class="text">{{$s->item}}</span></a></li>
                              <?php endif ?>                              
                            <?php 
                            }
                            }
                            ?>
                          </ul>
                         <?php } ?>
                      </li>
                <?php 
                }
                }
                ?>
              </ul>
            </nav>
            <!-- /main-nav -->
          </div>
        </div>
        <!-- END LEFT SIDEBAR -->

        <div id="main-content-wrapper" class="content-wrapper expanded">
          <div class="content">
            @yield('content')
          </div>
          <!-- /main -->
        </div>
      </div>

        <script src="{{ url('/') }}/js/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/js/plugins/modernizr/modernizr.js" async></script>
        <script src="{{ url('/') }}/assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js" async></script>
        <script src="{{ url('/') }}/assets/js/jquery-ui/jquery-ui-1.10.4.custom.min.js" async></script>
        <!--<script src="{{ url('/') }}/assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js" async></script> -->

        <script src="{{ url('/') }}/js/tether.min.js"></script>
        <!--<script src="{{ url('/') }}/js/bootstrap.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous" type="text/javascript"></script>

        
        
        <script src="{{ url('/') }}/js/sweetalert2.min.js"></script>
        <!-- Fin Tema dashboard 
        <script src="{{ url('/') }}/assets/js/king-common.js" async></script>
        <script src="{{ url('/') }}/demo-style-switcher/assets/js/deliswitch.js" async></script>-->
        <script src="{{ url('/') }}/js/select2.min.js" ></script>
        
<script type="text/javascript">
    $( document ).ready(function() {
        <?php if(isset($data['item_menu'])): ?>
        MenuState('#<?=$data['item_menu']?>');
        <?php endif ?>
    });
    $(function() {
        /*$("body").attr({
            oncontextmenu: 'return false',
            onkeydown: 'return false'
        });

        $(document).bind("contextmenu",function(e){
            return false;
        });

        window.frames["miframe"].document.oncontextmenu = function(){ return false; };

        $('.miiframeblog').contents().find('a').click(function(event) {
            event.preventDefault();

        });

        var Element = window.parent.document.getElementById('tdFrame');
        if (Element == null) {
            document.write('<b style="font-family:Arial;font-size:20pt;">Not Authorized to View this Page</b>');
        }*/
        $('.main-menu .js-sub-menu-toggle').on('click', function(e) {
            var bob = jQuery(this).attr("href");
            if (bob == "#") {
                e.preventDefault();
                $li = $(this).parent('li');
                if (!$li.hasClass('active')) {
                    $li.find(' > a .toggle-icon').removeClass('fa-angle-left').addClass('fa-angle-down');
                    $li.addClass('active');
                    $li.find('ul.sub-menu').slideDown(300);
                } else {
                    $li.find(' > a .toggle-icon').removeClass('fa-angle-down').addClass('fa-angle-left');
                    $li.removeClass('active');
                    $li.find('ul.sub-menu').slideUp(300);
                }
            }
        });

    });
    abrirMenuSidebar = function() {
        var validar = $("#left-sidebar").hasClass("minified");
        if (validar) {
            $("#left-sidebar").removeClass("minified");
            $("#main-content-wrapper").removeClass("expanded");
        } else {
            $("#left-sidebar").addClass("minified");
            $("#main-content-wrapper").addClass("expanded");

            $li = $('.main-menu .js-sub-menu-toggle').parent('li');
            $li.find(' > a .toggle-icon').removeClass('fa-angle-down').addClass('fa-angle-left');
            $li.removeClass('active');
            $li.find('ul.sub-menu').slideUp(300);
        }
    }

    myFunction = function() {
        $("#loader").hide();
        $("img").css("background", "none");
    }

    $('.tabs a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    })

    function MenuState(el){
        $(el).toggleClass('activo-menu');
    }

    //Eduard: Menu para esos usuarios personalizados como el 97 Guillermo Suarez
    <?php if(Auth::user()->id == 97){ ?>
    var URLactual = window.location;
    if(URLactual != 'https://essi.gestionsmart.com/personalizado/oportunidadesvendidas' && URLactual != 'https://essi.gestionsmart.com/actasreunion/35' && URLactual != 'https://essi.gestionsmart.com/actasreunion/41' && URLactual != 'https://essi.gestionsmart.com/venta/35' && URLactual != 'https://essi.gestionsmart.com/venta/41' && URLactual != 'https://essi.gestionsmart.com/guardarcita' && URLactual != 'https://essi.gestionsmart.com/editarcita' && URLactual != 'https://essi.gestionsmart.com/iniciarreunion'){
        location.href ="https://essi.gestionsmart.com/personalizado/oportunidadesvendidas";
    }else{
        var item_menu_personalizado = `<li><a href="/personalizado/oportunidadesvendidas" class="js-sub-menu-toggle" id="menu_7"><i class="fa fa-rocket"></i><span class="text">Oportunidades</span><i class="toggle-icon fa fa-angle-left"></i></a></li>`;
        $('.main-menu').html(item_menu_personalizado);
        setTimeout(function(){ $('#menu_7').addClass("activo-menu"); }, 3000);
    }
    <?php } ?>
    //Fin
</script>
        @yield('scripts')
        
    </body>
</html>
