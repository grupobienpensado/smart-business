<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
      <title>AgendaSmart - @yield('title')</title>
      
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta name="author" content="Edwin Ferreira Martinez">
      <meta name="description" content="CRM">
      <meta name="keywords" content="xx, xx, xx, xx, xx xx xx">
<!-- Fonts -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>

     
      <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css" crossorigin="anonymous">
      <link href="{{ url('/') }}/css/app1.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/calendar.css">
      <link href="{{ url('/') }}/components/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

      
      <!--<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
      <link href="{{ url('/') }}/MDB/MDB Free/css/style.css" rel="stylesheet">-->

        <!-- tema dashboard -->
      <link href="{{ url('/') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{ url('/') }}/assets/css/main.css" rel="stylesheet" type="text/css">
      <link href="{{ url('/') }}/assets/css/my-custom-styles.css" rel="stylesheet" type="text/css">
      <link href="{{ url('/') }}/demo-style-switcher/assets/css/style-switcher.css" rel="stylesheet" type="text/css">
      <!-- fin tema dashboard -->
      <!-- Fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('/') }}/assets/ico/kingadmin-favicon144x144.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ url('/') }}/assets/ico/kingadmin-favicon114x114.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('/') }}/assets/ico/kingadmin-favicon72x72.png">
      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ url('/') }}/assets/ico/kingadmin-favicon57x57.png">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
      <link rel="shortcut icon" href="{{ url('/') }}/assets/ico/kingadmin-favicon144x144.png">

      <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.6.2/sweetalert2.min.css" integrity="sha256-1Dd1LfK9ogDaOMgl1HC3rF4/7NwlG4w5K0brANd/WXQ=" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.6.2/sweetalert2.css" integrity="sha256-ibGXtY0TktQWA3HtAQ4/EkcNedMA13mK66MwP7gCeJU=" crossorigin="anonymous">
      <link rel="icon" href="{{ url('/') }}/images/favicon.ico" sizes="32x32">


      <style type="text/css">
        /*div#lisComentariosId {
            position: relative;
            height: auto;
            min-height: 40px;
        }*/

        .jumbotron {
            background-color: white !important;
        }

        div#map {
            height: 300px !important;
        }
        .table-responsive {
            max-width: 100% !important;
            min-width: calc(100%) !important;
            width: 100% !important;
        }
        .mdl-grid {
            width: 100%;
        }
        .rounded-circle {
            border-radius: 50% !important;
        }

        #chartdiv {
            -webkit-box-sizing: content-box !important;
            box-sizing: content-box !important;
        }
        /*.comentariosImg {
            margin-top: 0;
            max-width: 100% !important;
            width: 100%;
            position: absolute;
        }*/

        .hijo2, .table {
            overflow: hidden;
        }

        img.img-fluid {
            border: 0px !important;
        }

        .caret {
          display: none;
        }

        .jumbotron{
            text-align: initial !important;
            height: 100% !important;
        }

        .btn.btn-flat.btn-sm {
            color: #323030 !important;
            font-weight: bold !important;
        }

        .btn.btn-sm {
          padding: .5rem 0.6rem !important;
          margin: 0px !important;
          font-size: 12px !important;
          font-weight: 400 !important;
          font-family: Roboto !important;
        }

        label {
          font-family: Roboto;
          font-size: 1.5rem !important;
          font-weight: 400;
        }

        .btn.btn-sm .fa {
            font-size: 1rem !important;
        }

        .titulo{
          font-weight: bold !important;
          color: #051d60 !important;
          font-size: 2.5rem !important;
        }

        .titulo1{
          font-weight: bold !important;
          color: #051d60 !important;
          font-size: 1.5rem !important;
        }

        .subtitulo{
          text-transform: capitalize !important;
          margin-bottom: 4px !important;
          font-size: small !important;
          color: #54575a !important;
        }

        .subtitulo2{
          text-transform: capitalize !important;
          margin-bottom: 4px !important;
          font-size: 14px !important;
          color: #051d60 !important;
          font-weight: bold !important;
        }

        .subtitulo3{
          text-transform: capitalize !important;
          margin-bottom: 4px !important;
          font-size: 14px !important;
          color: #54575a !important;
          font-weight: bold !important;
        }

        .hijo{
          margin-left: auto !important;
          margin-right: auto !important;
          margin-top: auto !important;
          margin-bottom: auto !important;
          width: 50% !important;
          text-align: left !important;
        }

        .hijo2{
          margin-left: auto !important;
          margin-right: auto !important;
          margin-top: auto !important;
          margin-bottom: auto !important;
          width: 80% !important;
          text-align: left !important;
        }

        .tex-hijo{
          display:inline-block !important;
          vertical-align:middle !important;
          line-height:normal !important;
        }

        .alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img {
          -webkit-border-radius: 2px !important;
          -moz-border-radius: 2px !important;
          -ms-border-radius: 2px !important;
          -o-border-radius: 2px !important;
          border-radius: 0px !important;
          border: solid 1px rgba(0, 0, 0, 0.09) !important;
        }

        .list-group-item{
          padding-top: 5px !important;
          padding-bottom: 5px !important;
          margin-bottom: 5px !important;
        }

        .panel-view{
          -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75) !important;
          -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75) !important;
          box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75) !important;
          margin-bottom: 20px !important;
          background-color: #fff !important;
          width: 100% !important;
          margin: 12px !important;
          border-radius: 10px 10px 10px 10px !important;
          -moz-border-radius: 10px 10px 10px 10px !important;
          -webkit-border-radius: 10px 10px 10px 10px !important;
          position: absolute;
          overflow: hidden !important;
        }

        .observaciones-img{
          width: 60px !important;
        }

        .observaciones-p{
          font-size: small !important;
        }

        .observaciones-title{
          font-size: larger;    !important;
          font-weight: bold !important;
        }

        .boton {
          zoom: 2 !important;
          vertical-align: baseline !important;
          cursor: pointer !important;
          text-decoration: none !important;
          font: 12px Arial !important;
          padding: .5em !important;
          text-shadow: 0 1px 1px rgba(0,0,0,.3) !important;
          -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2) !important;
          -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2) !important;
          box-shadow: 0 1px 2px rgba(0,0,0,.2) !important;
        }
        .boton:hover { text-decoration: none !important; }
         
        /* Redondez */
         
        .redondo {
          -webkit-border-radius: 50% !important;
          -moz-border-radius: 50% !important;
          border-radius: 50% !important;
        }
         
        /* Color */
         
        .negro {
          color: #051d60 !important;
          border: solid 1px #d7d7d7 !important;
          background: #fff !important;
        }
        .negro:hover {
          background: #000 !important;
          background: -webkit-gradient(linear, left top, left bottom, from(#444), to(#000)) !important;
          background: -moz-linear-gradient(top,  #444,  #000) !important;
          filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#000000') !important;
        }

        #chartdiv {
          height: 100% !important;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
          height: 100% !important;
          margin: 0 !important;
          padding: 0 !important;
        }

        .profile-empresa{
          margin-right: auto!important;
          margin-left: auto!important;
          margin-top: auto!important;
          margin-bottom: auto!important;
          width: 250px !important;
          height: 250px !important;
          -webkit-border-radius: 200px 200px 200px 200px !important;
          position: relative !important;
          -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
          -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
          box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
          overflow: hidden !important;
          background-color: #fff !important;
        }

        .img-empresa{
          padding: 30px !important;
          border: solid 0px !important;
          line-height: 140 !important;
          position: absolute !important;
          display: table-cell !important;
          vertical-align: middle !important;
          text-align: center !important;
          top: 50% !important;
          bottom: 50% !important;
        }

        .mx-auto {
          margin-right: auto!important;
          margin-left: auto!important;
          margin-bottom: auto !important;
          margin-top: auto !important;
        }

        .btn-amarillo {
          background: rgba(212, 194, 59, 0.87) !important;
        }

        .tr-table{
            background-color: #063F9C !important;
            color: aliceblue !important;
            font-size: 1.3em !important;
        }

        .profile-empresa-arriba{
          margin-right: auto !important;
          margin-left: auto !important;
          margin-top: auto !important;
          margin-bottom: auto !important;
          width: 200px !important;
          height: 200px !important;
          -webkit-border-radius: 200px 200px 200px 200px !important;
          top: -38px !important;
          left: -28px !important;
          position: absolute !important;
          -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
          -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
          box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
          overflow: hidden !important;
          background-color: #fff !important;
        }

        .img-empresa-arriba{
          padding: 30px !important;
          border: solid 0px !important;
          line-height: 140 !important;
          position: absolute !important;
          display: table-cell !important;
          vertical-align: middle !important;
          text-align: center !important;
          top: 50% !important;
          bottom: 50% !important;
        }

        .btn-floating.waves-effect.waves-light {
            padding: 5px;
            color: aliceblue;
            margin: 2px;
            opacity: 1;
            background-color: #000;
        }

        .col-form-label {
            font-size: 1rem !important;
        }
        .main-nav{
            padding: 0px;
        }
        .main-menu>li {
            text-transform: capitalize;
        }
      </style>


      
      <link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet">
      <!--<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">      
      <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.bootstrap4.min.css"/>
      <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/bootstrap-select/css/bootstrap-select.min.css"/>
      
      <link href="{{ url('/') }}/components/file/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>-->
      
      <style>
        .title-menu{
          padding-left: 10px;
          color: aliceblue;
          font-weight: 900;
        }

        ul.main-menu > li a {
          font-weight: 400 !important;
        }

        .top-bar .logged-user .btn {
          -webkit-box-shadow: 0px 0px 0px 0px;
          -moz-box-shadow: 0px 0px 0px 0px;
          box-shadow: 0px 0px 0px 0px;
        }

        .top-bar .logged-user .btn .name {
            font-weight: 500 !important;
        }

        .top-bar .logged-user .btn {
          font-size: 1em !important;
          color: #fff !important;
        }
          .main-menu li a:hover{ background-color: #051d60 !important; color: #fff;}
          .main-menu li a span:hover{ background-color: #051d60 !important; }
          .activo-menu{background-color: #051d60 !important;color: white !important;}
      </style>

    

    </head>
    <body onload="myFunction()" class="topnav-fixed">
      <div id="loader"></div>


      <div id="wrapper" class="wrapper">
        <!-- TOP BAR -->
        <div class="top-bar navbar-fixed-top" style="height: 49px;">
            <div class="clearfix">
              <!-- logo -->
              <div class="pull-left left">
                <div class="title-menu"><img src="{{ url('/') }}/images/sb_logo.svg" style="position: absolute;
    max-height: 47px;
    top: 1px;
    left: 10px;
    float: left;
    max-width: 270px;"></div>
              </div>
              <!-- end logo -->
              <div class="pull-right right">
                <!-- search box -->
                <!--<div class="searchbox">
                  <div id="tour-searchbox" class="input-group">
                    <input type="search" class="form-control" placeholder="enter keyword here...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>-->
                <!-- end search box -->
                <!-- top-bar-right -->
                <div class="top-bar-right">
                  <!--<div class="notifications">
                    <ul>-->
                      <!-- notification: inbox -->
                      <!--<li class="notification-item inbox">
                        <div class="btn-group">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope"></i><span class="count">2</span>
                            <span class="circle"></span>
                          </a>
                          <ul class="dropdown-menu" role="menu">
                            <li class="notification-header">
                              <em>You have 2 unread messages</em>
                            </li>
                            <li class="inbox-item clearfix">
                              <a href="#">
                                <div class="media">
                                  <div class="media-left">
                                    <img class="media-object" src="assets/img/user1.png" alt="Antonio">
                                  </div>
                                  <div class="media-body">
                                    <h5 class="media-heading name">Antonius</h5>
                                    <p class="text">The problem just happened this morning. I can't see ...</p>
                                    <span class="timestamp">4 minutes ago</span>
                                  </div>
                                </div>
                              </a>
                            </li>
                            <li class="inbox-item unread clearfix">
                              <a href="#">
                                <div class="media">
                                  <div class="media-left">
                                    <img class="media-object" src="assets/img/user2.png" alt="Antonio">
                                  </div>
                                  <div class="media-body">
                                    <h5 class="media-heading name">Michael</h5>
                                    <p class="text">Hey dude, cool theme!</p>
                                    <span class="timestamp">2 hours ago</span>
                                  </div>
                                </div>
                              </a>
                            </li>
                            <li class="notification-footer">
                              <a href="#">View All Messages</a>
                            </li>
                          </ul>
                        </div>
                      </li>-->
                      <!-- end notification: inbox -->
                      <!-- notification: general -->
                      <!--<li class="notification-item general">
                        <div class="btn-group">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i><span class="count">8</span>
                            <span class="circle"></span>
                          </a>
                          <ul class="dropdown-menu" role="menu">
                            <li class="notification-header">
                              <em>You have 8 notifications</em>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-shopping-cart red-font"></i>
                                <span class="text">4 new sales order</span>
                                <span class="timestamp">4 hours ago</span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-edit yellow-font"></i>
                                <span class="text">3 product reviews awaiting moderation</span>
                                <span class="timestamp">1 day ago</span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-warning red-font"></i>
                                <span class="text red-font">Low disk space!</span>
                                <span class="timestamp">Oct 11</span>
                              </a>
                            </li>
                            <li class="notification-footer">
                              <a href="#">View All Notifications</a>
                            </li>
                          </ul>
                        </div>
                      </li>-->
                      <!-- end notification: general -->
                    <!--</ul>
                  </div>-->
                  <!-- logged user and the menu -->
                  <div class="logged-user">
                    <div class="btn-group">
                      <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url('/') }}/images/file/clientes/{{ Auth::user()->foto }}" alt="User Avatar" class="img-circle" style="    max-height: 30px;" />
                        <span class="name" style="text-transform: capitalize !important;">
                          <?php  
                            try{
                              $nombre   = explode(" ",Auth::user()->nombres);
                              $apellido = explode(" ",Auth::user()->apellidos); 
                              echo $nombre[0] ." ". $apellido[0]; 
                            }catch (Exception $e) {}
                          ?>
                        </span> <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="#">
                            <i class="fa fa-cog"></i>
                            <span class="text">Configuración</span>
                          </a>
                        </li>
                        <li>
                          <a href="{{ url('/logout') }}">
                            <i class="fa fa-power-off"></i>
                            <span class="text">Cerrar sesión</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- end logged user and the menu -->
                </div>
                <!-- end top-bar-right -->
              </div>
            </div>
        </div>
        <!-- END TOP BAR -->
        <!-- LEFT SIDEBAR -->
        <div id="left-sidebar" class="left-sidebar  minified">
          <div class="sidebar-minified js-toggle-minified">
            <i class="fa fa-exchange"></i>
          </div>
          <!-- main-nav -->
          <div class="sidebar-scroll">
            <nav class="main-nav">
              <ul class="main-menu">
                <?php                  
                $menu = Illuminate\Support\Facades\DB::select('SELECT * FROM `menu_crms` WHERE `sub_item` IS NULL ORDER BY `orden` ASC');
                foreach($menu as $m){ 
                  $permiso_usuario_menu="No";
                  $cargo = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
                    if(isset($cargo[0]->id)){
                      $permiso = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_menu` WHERE `id_menu`="'.$m->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                      if(isset($permiso[0]->permiso)){
                        if($permiso[0]->permiso == "Si"){
                          $permiso_usuario_menu="Si";
                        }
                      }
                    } 
                    if($permiso_usuario_menu=="Si"){
                      $submenu = Illuminate\Support\Facades\DB::select('SELECT * FROM `menu_crms` WHERE `sub_item`="'.$m->id.'" ORDER BY `id` ASC');
                      ?>
                      <li><a href="{{url('/')}}/{{$m->url}}" class="js-sub-menu-toggle" id="menu_{{$m->id}}"><i class="{{$m->logo}}"></i><span class="text">{{$m->item}}</span>
                        <i class="toggle-icon fa fa-angle-left"></i></a>
                          <ul class="sub-menu ">
                            <?php 
                            foreach($submenu as $s){ 
                              $permiso_usuario_submenu="No";
                              $permiso1 = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_menu` WHERE `id_menu`="'.$s->id.'" AND `id_cargo`="'.$cargo[0]->id.'" ORDER BY `id` DESC;');
                              if(isset($permiso1[0]->permiso)){
                                if($permiso1[0]->permiso == "Si"){
                                  $permiso_usuario_submenu="Si";
                                }
                              }
                              if($permiso_usuario_submenu=="Si"){
                              ?>
                              <li><a href="{{ url('/') }}/{{$s->url}}"><span class="text">{{$s->item}}</span></a></li>
                            <?php 
                            }
                            }
                            ?>
                          </ul>
                      </li>
                <?php 
                }
                }
                ?>
              </ul>
            </nav>
            <!-- /main-nav -->
          </div>
        </div>
        <!-- END LEFT SIDEBAR -->
        
        

        <div id="main-content-wrapper" class="content-wrapper expanded">
          <!-- top general alert -->
          
          <!-- end top general alert 
          <div class="row">
            <div class="col-lg-4 ">
              <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><a href="#">Navigations</a></li>
                <li class="active">Normal Sidebar</li>
              </ul>
            </div>
            <div class="col-lg-8 ">
              <div class="top-content">
                <ul class="list-inline quick-access">
                  <li>
                    <a href="charts-statistics-interactive.html">
                      <div class="quick-access-item bg-color-green">
                        <i class="fa fa-bar-chart-o"></i>
                        <h5>CHARTS</h5><em>basic, interactive, real-time</em>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="page-inbox.html">
                      <div class="quick-access-item bg-color-blue">
                        <i class="fa fa-envelope"></i>
                        <h5>INBOX</h5><em>inbox with gmail style</em>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="tables-dynamic-table.html">
                      <div class="quick-access-item bg-color-orange">
                        <i class="fa fa-table"></i>
                        <h5>DYNAMIC TABLE</h5><em>tons of features and interactivity</em>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
           main -->
          <div class="content">
            @yield('content')
          </div>
          <!-- /main -->
        </div>
      

         
      </div>

        <!--<script src="{{ url('/') }}/js/tether.min.js" crossorigin="anonymous"></script>
   
        <script src="{{ url('/') }}/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ url('/') }}/js/bootstrap-datepicker.es.min.js"></script>
        <script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
        
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->

        <!-- Tema dashboard -->
        <?php if(isset($step)){ ?>
        <script src="{{ url('/') }}/images/configuracion_crm/step/js/jquery-2.2.4.min.js"></script>
        <script src="{{ url('/') }}/images/configuracion_crm/step/js/TweenMax.min.js"></script>
        <?php }else{ ?>
        <?php
          if (!isset($gridster)) { ?>
              <script src="{{ url('/') }}/js/jquery-3.1.1.slim.min.js" crossorigin="anonymous"></script>        
              <script src="{{ url('/') }}/js/jquery.min.js"></script>
          <?php
          }
         ?>
        <script src="{{ url('/') }}/assets/js/jquery/jquery-2.1.0.min.js"></script>
        <?php } ?>
        
        <script src="{{ url('/') }}/assets/js/bootstrap/bootstrap.js"></script>
        <script src="{{ url('/') }}/assets/js/plugins/modernizr/modernizr.js"></script>
        <script src="{{ url('/') }}/assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!--<script src="{{ url('/') }}/assets/js/king-common.js"></script>-->
        <!--<script src="{{ url('/') }}/demo-style-switcher/assets/js/deliswitch.js"></script>-->
        <script src="{{ url('/') }}/assets/js/jquery-ui/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="{{ url('/') }}/assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>  


 

        <script src="{{ url('/') }}/js/tether.min.js" crossorigin="anonymous"></script>
        <script src="{{ url('/') }}/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script src="{{ url('/') }}/js/select2.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/sweetalert2/6.6.2/sweetalert2.min.js" integrity="sha256-5DSTcjlCWf/JPl/ULIJojOlV9Rbzk0xgD3abApqBGIY=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/sweetalert2/6.6.2/sweetalert2.js" integrity="sha256-S1FK7FLfPfRWlF8ZovsiUoPcnoTf6Xs7myqab+x/X6A=" crossorigin="anonymous"></script>
        <!-- Fin Tema dashboard -->


        <script type="text/javascript">
             $( document ).ready(function() {
                <?php if(isset($data['item_menu'])): ?>
                MenuState('#<?=$data['item_menu']?>');
                <?php endif ?>
            });
            $(function() { 
                /*$("body").attr({
                    oncontextmenu: 'return false',
                    onkeydown: 'return false'
                });

                $(document).bind("contextmenu",function(e){
                    return false;
                });

                window.frames["miframe"].document.oncontextmenu = function(){ return false; };

                $('.miiframeblog').contents().find('a').click(function(event) {
                    event.preventDefault();

                }); 

                var Element = window.parent.document.getElementById('tdFrame');
                if (Element == null) {
                    document.write('<b style="font-family:Arial;font-size:20pt;">Not Authorized to View this Page</b>');
                }*/


            });

            myFunction = function () {
              $("#loader").hide();
            }

            $('.tabs a').click(function (e) {
              e.preventDefault()
              $(this).tab('show')
            });
            function MenuState(el){
                $(el).toggleClass('activo-menu');
            }
        </script>
        @yield('scripts')
        
    </body>
</html>
