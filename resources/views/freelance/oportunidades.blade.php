@extends('template.app')
@section('title', 'Listado Oportunidades')
@section('content')
<style>
    #titulo {overflow-x:hidden;}
#titulo{width:100%;height:100%;position:fixed;background-color:transparent}#titulo .content{position:absolute;top:11%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);height:160px;overflow:hidden;font-family:Lato,sans-serif;font-size:35px;line-height:40px;color:#5fc2ff}#titulo .content__container{font-weight:600;overflow:hidden;height:40px;padding:0 40px}#titulo .content__container:before{content:'[';left:0}#titulo .content__container:after{content:']';right:0}#titulo .content__container:after,#titulo .content__container:before{position:absolute;top:0;color:#051d60;font-size:42px;line-height:40px;-webkit-animation-name:opacity;-webkit-animation-duration:2s;-webkit-animation-iteration-count:infinite;animation-name:opacity;animation-duration:2s;animation-iteration-count:infinite}#titulo .content__container__text{display:inline;float:left;margin:0}#titulo .content__container__list{margin-top:0;padding-left:242px;text-align:left;list-style:none;-webkit-animation-name:change;-webkit-animation-duration:10s;-webkit-animation-iteration-count:infinite;animation-name:change;animation-duration:10s;animation-iteration-count:infinite}#titulo .content__container__list li:nth-child(1){color:#9acd32}#titulo .content__container__list li:nth-child(2){color:green}#titulo .content__container__list li:nth-child(3){color:red}#titulo .content__container__list__item{line-height:40px;margin:0}@-webkit-keyframes opacity{0%,100%{opacity:0}50%{opacity:1}}@-webkit-keyframes change{0%,100%,12.66%{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}16.66%,29.32%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}33.32%,45.98%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}49.98%,62.64%{-webkit-transform:translate3d(0,-75%,0);transform:translate3d(0,-75%,0)}66.64%,79.3%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}83.3%,95.96%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}}@keyframes opacity{0%,100%{opacity:0}50%{opacity:1}}@keyframes change{0%,100%,12.66%{-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}16.66%,29.32%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}33.32%,45.98%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}49.98%,62.64%{-webkit-transform:translate3d(0,-75%,0);transform:translate3d(0,-75%,0)}66.64%,79.3%{-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}83.3%,95.96%{-webkit-transform:translate3d(0,-25%,0);transform:translate3d(0,-25%,0)}}.caja{background-color: transparent;border: 0px solid transparent;border-bottom: 1px solid #5fc2ff;text-align: center;color: #051d60;}.caja:focus{background-color: transparent; color: #051d60;} th{text-align: center;}#contenido_tabla tr td{ vertical-align: inherit;color: #051d60;}.img-avatar{width: 50px;border-radius: 200px 200px 200px 200px;-moz-border-radius: 200px 200px 200px 200px;-webkit-border-radius: 200px 200px 200px 200px;border: 0px solid #000000;}#contenido_tabla tr td a{cursor: pointer;}.table tr:nth-child(odd) {background-color: #cccccc;} .table tr:nth-child(even){background-color: #f1f1f1;}.out{display: none;}
</style>
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-12">
        <p class="h2 visible-sm visible-xs" style="color: #051d60;">Oportunidades Freelance</p>
    </div>
</div>
<div class="row" id="titulo">
    <div class="col-md-12 visible-md visible-lg">
        <div class="content">
          <div class="content__container">
            <p class="content__container__text">
              Oportunidades
            </p>
            <ul class="content__container__list">
              <li class="content__container__list__item">Vigentes</li>
              <li class="content__container__list__item">Vendidas</li>
              <li class="content__container__list__item">Perdidas</li>
              <li class="content__container__list__item">Freelance</li>
            </ul>
          </div>
        </div>
    </div>
</div>
<div class="row visible-md visible-lg" style="margin-top: 86px;background-color: #f1f1f1;">
   <div class="col-xs-12 col-sm-12 col-md-3">
       <p class="h4 text-left" style="color: #051d60;">
           <strong><i class="fa fa-search"></i> Filtros para especificar busqueda</strong>
       </p>
   </div>
   <div class="col-md-9 visible-sm visible-md visible-lg">
       <hr style="border-top-color: #051d60;">
   </div>
</div>
<div class="row visible-md visible-lg" style="background-color: #f1f1f1;">
    <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Valor Oportunidad:</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-4">
               <input type="text" class="form-control caja" id="filtro_valor_minimo" onkeyup="formato_numero(this.value,this.id)" placeholder="Valor Minimo">
           </div>
           <div class="col-xs-12 col-sm-12 col-md-4">
               <input type="text" class="form-control caja" id="filtro_valor_maximo" onkeyup="formato_numero(this.value,this.id)" placeholder="Valor Maximo">
           </div>
       </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Empresa / Ciudad</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-8">
               <input type="text" class="form-control caja" id="filtro_empresa" list="list_empresas" placeholder="Empresa / Ciudad">
               <datalist id="list_empresas">
                  <?php foreach($data['empresas'] as $empresa){ ?>
                  <option value="<?=$empresa->empresa?>">
                  <?php } ?>
               </datalist>
           </div>
       </div>
   </div>
</div>
<div class="row visible-md visible-lg" style="background-color: #f1f1f1;">
    <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Fecha Identificación:</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-4">
               <input type="text" class="form-control caja fecha-filtro" id="filtro_fechaidentificacion_minima" placeholder="Fecha Minima">
           </div>
           <div class="col-xs-12 col-sm-12 col-md-4">
               <input type="text" class="form-control caja fecha-filtro" id="filtro_fechaidentificacion_maxima" placeholder="Fecha Maxima">
           </div>
       </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Productos</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-8">
               <input type="text" class="form-control caja" list="list_productos" id="filtro_productos" placeholder="Producto">
               <datalist id="list_productos">
                  <?php foreach($data['productos'] as $producto){ ?>
                  <option value="<?=$producto->name?>">
                  <?php } ?>
               </datalist>
           </div>
       </div>
   </div>
</div>
<div class="row visible-md visible-lg" style="background-color: #f1f1f1;">
    <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Fecha Cierre:</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-4">
               <input type="text" class="form-control caja fecha-filtro" id="filtro_fechacierre_minima" placeholder="Fecha Minima">
           </div>
           <div class="col-xs-12 col-sm-12 col-md-4">
               <input type="text" class="form-control caja fecha-filtro" id="filtro_fechacierre_maxima" placeholder="Fecha Maxima">
           </div>
       </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Responsable</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-8">
               <input type="text" class="form-control caja" list="list_freelance" id="filtro_freelance" placeholder="Responsable">
               <datalist id="list_freelance">
                  <?php foreach($data['freelance'] as $freelance){ ?>
                  <option value="<?=$freelance->NOMBRECORTOFREELANCE?>">
                  <?php } ?>
               </datalist>
           </div>
       </div>
   </div>
</div>
<div class="row visible-md visible-lg" style="background-color: #f1f1f1;">
    <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Pais</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-7">
               <input type="text" class="form-control caja" list="list_paises" id="filtro_paises" placeholder="Pais">
               <datalist id="list_paises">
                  <?php foreach($data['paises'] as $pais){ ?>
                  <option value="<?=$pais->name?>">
                  <?php } ?>
               </datalist>
           </div>
       </div>
   </div>
   <div class="col-xs-12 col-sm-12 col-md-6">
       <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-3">
               <p class="h5" style="color: #051d60;"><strong>Ciclo de venta</strong></p>
           </div>
           <div class="col-xs-12 col-sm-12 col-md-8">
               <input type="text" class="form-control caja" id="filtro_ciclo" placeholder="Ciclo de venta">
           </div>
       </div>
   </div>
</div>
<div class="row mt-5">
    <div class="col-md-12">
        <div class="table-responsive-sm">
            <table class="table">
                <thead>
                    <tr style="background-color:#2f3e5d;color:#6198bf;">
                        <th>Item</th>
                        <th>Codigo</th>
                        <th>Empresa / Ciudad</th>
                        <th>Productos</th>
                        <th>Valor Oportunidad</th>
                        <th>Freelance</th>
                        <th>Pais</th>
                        <th>Identificada</th>
                        <th>Por Cerrar</th>
                        <th>Ciclo</th>
                        <th>Probabilidad</th>
                    </tr>
                </thead>
                <tbody id="contenido_tabla">

                </tbody>
            </table>
        </div>
    </div>
</div>

<template id="row">
    <tr class="h5">
        <td data-item>1</td>
        <td data-codigo><p class="py-3" style="border:  1px solid #9E9E9E;"><a><strong>OP</strong>240</a></p></td>
        <td data-empresa><img src="/images/file/empresas/principal/59847ed8922be.png" class="img-avatar"><br>PRODALECC <br> Latacunga</td>
        <td data-producto>1 Homogenizador<br>2 Esterilizadores<br>1 Bagger</td>
        <td data-valor>$ 171,959,650</td>
        <td data-freelance><img src="/storage/Freelance/users/5ac3aa389b5e3.jpg" class="img-avatar"><br><p class="h6"><em>Jose Gutierrez</em></p></td>
        <td data-pais>Alemania</td>
        <td data-identificacion><a class="text-primary"><i class="fa fa-calendar"></i></a><br>12 Marzo 2018</td>
        <td data-cierre><a class="text-primary"><i class="fa fa-calendar"></i></a><br>25 Octubre 2018</td>
        <td data-ciclo><a class="text-danger" data-toggle="tooltip" data-placement="top" title="Perdida"><i class="fa fa-times"></i></a><a class="text-primary" data-toggle="tooltip" data-placement="top" title="Vigente"><i class="fa fa-check"></i></a><a class="text-success" data-toggle="tooltip" data-placement="top" title="Vendida"><i class="fa fa-check-circle"></i></a><p class="py-3" style="border:  1px solid #9E9E9E;"><a><strong>50</strong>%</a></p></td>
        <td data-probabilidad><a class="text-success"><i class="fa fa-arrow-up"></i></a> <br>Alta</td>
    </tr>
</template>

@endsection
@section('scripts')
<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script>
var CSRF_TOKEN = $('input[name=_token]').val();
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    fecha();
    lista();
});
    /**
     * Consulta por ajax para traer la lista de oportunidades
     */
    function lista(){
        var jqxhr = $.ajax({
            url: "/freelance/oportunidades-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 0
            },
            cache: false,
            type: 'POST',
            success: function(e){
                crearoportunidades(e.data['oportunidades']);
            }
        });
    }

    /**
     * Función para armar el listado
     * @param STRING lista LISTADO DE OPORTUNIDADES
     */
    function crearoportunidades(lista){
        var i = 0;
        $('#contenido_tabla').html('');
        $.each(lista, function(key, value){
            i++;
             var template = $("#row").html();
             var $template = $(template);
             $template.find("[data-item]").html(i);
             $template.find("[data-item]").attr( 'data-item', i );
             $template.find("[data-codigo]").html('<p class="py-3" style="border:1px solid #9E9E9E;"><a><strong>OP</strong>'+value.id+'</a></p>');
             $template.find("[data-codigo]").attr( 'data-codigo', value.id );
             if(value.EMPRESALOGO === null){
               FotoEmpresa = 'http://via.placeholder.com/50x50';
             }else{
               FotoEmpresa = value.EMPRESALOGO;
             }
             $template.find("[data-empresa]").html('<img src="'+FotoEmpresa+'" class="img-avatar"><br>'+value.empresa+' <br> '+value.ciudad);
             $template.find("[data-empresa]").attr( 'data-empresa', value.empresa+' '+value.ciudad );
             productos = (value.PRODUCTOSOPORTUNIDAD).replace(/,/g, "<br>");
             $template.find("[data-producto]").html(productos);
             $template.find("[data-producto]").attr( 'data-producto', productos );
             $template.find("[data-valor]").html(formato_numero(value.VALOR,''));
             $template.find("[data-valor]").attr( 'data-valor', value.VALOR );
            if(value.FOTOFREELANCE === null){
               FotoFreelance = 'http://via.placeholder.com/50x50';
             }else{
               FotoFreelance = value.FOTOFREELANCE;
             }
             $template.find("[data-freelance]").html('<img src="'+FotoFreelance+'" class="img-avatar"><br><p class="h6"><em>'+value.NOMBRECORTOFREELANCE+'</em></p>');
             $template.find("[data-freelance]").attr( 'data-freelance', value.NOMBRECORTOFREELANCE );
             $template.find("[data-pais]").html(value.pais);
             $template.find("[data-pais]").attr( 'data-pais', value.pais );
             $template.find("[data-identificacion]").html(fechas(value.FECHAIDENTIFICACION));
             $template.find("[data-identificacion]").attr( 'data-identificacion', value.FECHAIDENTIFICACION );
             $template.find("[data-cierre]").html(fechas(value.FECHACIERRE));
             $template.find("[data-cierre]").attr( 'data-cierre', value.FECHACIERRE );
             if(value.CICLO == 0){
               ciclo = '<a class="text-danger" data-toggle="tooltip" data-placement="top" title="Perdida"><i class="fa fa-times"></i></a><a class="text-primary" style="opacity:0.5;"><i class="fa fa-check"></i></a><a class="text-success" style="opacity:0.5;"><i class="fa fa-check-circle"></i></a><p class="py-3" style="border:  1px solid #9E9E9E;"><a><strong>'+value.CICLO+'</strong>%</a></p>';
               }else if(value.CICLO == 90){
                   ciclo = '<a class="text-danger" style="opacity:0.5;"><i class="fa fa-times"></i></a><a class="text-primary" style="opacity:0.5;"><i class="fa fa-check"></i></a><a class="text-success" data-toggle="tooltip" data-placement="top" title="Vendida"><i class="fa fa-check-circle"></i></a><p class="py-3" style="border:  1px solid #9E9E9E;"><a><strong>'+value.CICLO+'</strong>%</a></p>';
               }else{
                   ciclo = '<a class="text-danger" style="opacity:0.5;"><i class="fa fa-times"></i></a><a class="text-primary" data-toggle="tooltip" data-placement="top" title="Vigente"><i class="fa fa-check"></i></a><a class="text-success" style="opacity:0.5;"><i class="fa fa-check-circle"></i></a><p class="py-3" style="border:  1px solid #9E9E9E;"><a><strong>'+value.CICLO+'</strong>%</a></p>'
               }
             $template.find("[data-ciclo]").html(ciclo);
             $template.find("[data-ciclo]").attr( 'data-ciclo', value.CICLO );
             if(value.PROBABILIDAD == 'Baja'){
                probabilidad = '<a class="text-danger"><i class="fa fa-arrow-down"></i></a> <br>'+value.PROBABILIDAD;
             }else if(value.PROBABILIDAD == 'Media'){
                probabilidad = '<a class="text-warning"><i class="fa fa-arrow-right"></i></a> <br>'+value.PROBABILIDAD;
             }else{
                probabilidad = '<a class="text-success"><i class="fa fa-arrow-up"></i></a> <br>'+value.PROBABILIDAD;
             }
             $template.find("[data-probabilidad]").html(probabilidad);
            $template.find("[data-probabilidad]").attr( 'data-probabilidad', value.PROBABILIDAD );
             $template.appendTo($(".table tbody"));
       });
        $('[data-toggle="tooltip"]').tooltip();
    }

     /**
     * Función para dar formato a la moneda
     */
    function formato_numero(valor,id){
        valor = replaceAll(valor, "$ ", "" );
        if(valor != ""){
           valor = replaceAll(valor, ",", "" );
           valor = parseFloat(valor);

           if(isNaN(valor)){
               if(id!=''){
                  $('#'+id).val('');
                  }else{
                      return '$ 0';
                  }
            }else{
                valor = number_format(valor,0);
                if(id!=''){
                  $('#'+id).val("$ "+valor);
                  }else{
                      return "$ "+valor;
                  }
            }
        }else{
            if(id!=''){
              $('#'+id).val('');
              }else{
                  return "$ 0";
              }
        }
    }
    function number_format(amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }
    function replaceAll( text, busca, reemplaza ){
      while (text.toString().indexOf(busca) != -1)
          text = text.toString().replace(busca,reemplaza);
      return text;
    }

    /**
     * Función para darle formato a las fechas
     * @param DATE f FECHA A DARLE FORMATO
     */
    function fechas(f){
        var fecha = f.split("-");
        var meses = ["","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        return fecha[2]+' de '+meses[parseInt(fecha[1])]+' '+fecha[0];
    }

    /**
     * Funcion activar las fechas del filtro
     */
    function fecha(){
        $('.fecha-filtro').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
    }

    /*Función para los filtros*/
    $(document).on('keyup','.caja',function(){
        aplicar_filtro();
    });

    $(document).on('change','.caja',function(){
        aplicar_filtro();
    });
    function aplicar_filtro(){
        var vacio = 0;
        $('#contenido_tabla tr').each(function(){
            $(this).addClass('out');
            /*Ciclo de venta*/
            buscar = $('#filtro_ciclo').val();
            if(buscar != ""){
                buscar = buscar.toUpperCase();
                ciclo = $(this).context.cells[9].dataset.ciclo;
                ciclo = ciclo.toUpperCase();
                encontradas = ciclo.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }
            }
            /*Responsable*/
            buscar = $('#filtro_freelance').val();
            if(buscar != ""){
                buscar = buscar.toUpperCase();
                freelance = $(this).context.cells[5].dataset.freelance;
                freelance = freelance.toUpperCase();
                encontradas = freelance.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }
            }
            /*Productos*/
            buscar = $('#filtro_productos').val();
            if(buscar != ""){
                buscar = buscar.toUpperCase();
                producto = $(this).context.cells[3].dataset.producto;
                producto = producto.toUpperCase();
                encontradas = producto.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }
            }
            /*Empresa / Ciudad*/
            buscar = $('#filtro_empresa').val();
            if(buscar != ""){
                buscar = buscar.toUpperCase();
                empresa_ciudad = $(this).context.cells[2].dataset.empresa;
                empresa_ciudad = empresa_ciudad.toUpperCase();
                encontradas = empresa_ciudad.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }
            }
            /*Pais*/
            buscar = $('#filtro_paises').val();
            if(buscar != ""){
                buscar = buscar.toUpperCase();
                pais = $(this).context.cells[6].dataset.pais;
                pais = pais.toUpperCase();
                encontradas = pais.search(buscar);
                if(encontradas>=0){
                   $(this).removeClass("out");
                }
            }
            /*Valor Oportunidad*/
            valor_minimo = parseInt((($('#filtro_valor_minimo').val()).replace("$ ", "")).replace(/,/g, ""));
            valor_maximo = parseInt((($('#filtro_valor_maximo').val()).replace("$ ", "")).replace(/,/g, ""));
            valor = $(this).context.cells[4].dataset.valor;
            if(valor_minimo>=0 && valor_maximo>=0){
                if(valor>=valor_minimo && valor<=valor_maximo){
                   $(this).removeClass("out");
                }
            }else if(valor_minimo>=0){
                 if(valor>=valor_minimo){
                   $(this).removeClass("out");
                }
            }else if(valor_maximo>=0){
                if(valor<=valor_maximo){
                   $(this).removeClass("out");
                }
            }
            /*Fecha identificacion*/
            fecha_minima = $('#filtro_fechaidentificacion_minima').val();
            fecha_maxima = $('#filtro_fechaidentificacion_maxima').val();
            fecha = $(this).context.cells[7].dataset.identificacion;
            if(fecha_minima!='' && fecha_maxima!=''){
                if(fecha>=fecha_minima && fecha<=fecha_maxima){
                   $(this).removeClass("out");
                }
            }else if(fecha_minima!=""){
                 if(fecha>=fecha_minima){
                   $(this).removeClass("out");
                }
            }else if(fecha_maxima!=""){
                if(fecha<=fecha_maxima){
                   $(this).removeClass("out");
                }
            }
            /*Fecha cierre*/
            fecha_minima = $('#filtro_fechacierre_minima').val();
            fecha_maxima = $('#filtro_fechacierre_maxima').val();
            fecha = $(this).context.cells[8].dataset.cierre;
            if(fecha_minima!='' && fecha_maxima!=''){
                if(fecha>=fecha_minima && fecha<=fecha_maxima){
                   $(this).removeClass("out");
                }
            }else if(fecha_minima!=""){
                 if(fecha>=fecha_minima){
                   $(this).removeClass("out");
                }
            }else if(fecha_maxima!=""){
                if(fecha<=fecha_maxima){
                   $(this).removeClass("out");
                }
            }
        });

        /*Mostrar todo si no hay filtros*/
        if($('#filtro_valor_minimo').val() == "" && $('#filtro_valor_maximo').val() == "" && $('#filtro_fechaidentificacion_minima').val() == "" && $('#filtro_fechaidentificacion_maxima').val() == "" && $('#filtro_fechacierre_minima').val() == "" && $('#filtro_fechacierre_maxima').val() == "" && $('#filtro_paises').val() == "" && $('#filtro_empresa').val() == "" && $('#filtro_productos').val() == "" && $('#filtro_freelance').val() == "" && $('#filtro_ciclo').val() == ""){
            $('#contenido_tabla tr').each(function(){
                $(this).removeClass('out');
            });
        }
        var i=0;
        $('#contenido_tabla tr').each(function(){
            if(!$(this).hasClass('out')){
                i++;
                $(this).context.cells[0].innerHTML=i;
            }
        });
    }
</script>
@endsection
