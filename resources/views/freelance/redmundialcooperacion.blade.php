@extends('template.app')
@section('title', 'Nuevos Mercados')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css">
<style>
    .bg-light-2{background-color:#e2e2e2}#global_chart{width:90%;height:40rem;background-color:#1C2232;border-radius:10px}.globlal-btns{width:17rem;color:#fff;margin-top:.5rem}.globlal-btns:first-child{background-color:#00a8dd}.globlal-btns:last-child{background-color:#0054c9}.bg-primary-2{background-color:#008ccc}.border-custom{border:1px solid #6c757d;border-right:0;border-left:0}.border-custom:last-child{border-right:1px solid #6c757d}.bg-secondary{background-color:#5d5d5d!important}.card{border-radius:.5rem}.swiper-button-next,.swiper-button-prev{background-size:27px 27px;-webkit-filter:grayscale(100%);filter:grayscale(100%)}.swiper-button-prev,.swiper-container-rtl .swiper-button-next{left:1px}.swiper-button-next,.swiper-container-rtl .swiper-button-prev{right:1px}.swiper-container{width:100%;height:100%}.swiper-slide{text-align:center;display:inline-flex;-webkit-justify-content:center;justify-content:center;align-items:center}@media (min-width:768px) and (max-width:1366px){.globlal-btns:first-child{margin-top:0}.col-2.custom-col{max-width:14.666667%;flex:0 0 14.666667%}.col-1.custom-col{max-width:10.333333%;flex:0 0 10.333333%}}
</style>
  {{ csrf_field() }}
   <h2 class="text-center">Red Mundial de Cooperación</h2>
    <div class="card px-3 py-2 shadow-2">
       <div class="container">
        <div class="row no-gutters">
            <div class="col-md-6 col-sm-12">
                <div class="card bg-secondary">
                   <div class="container" id="inf_aliados">
                    <div class="row no-gutters">
                        <div class="col col-2 custom-col">
                            <h4 class="text-white bg-info w-100 h-100 m-0 d-flex align-items-center justify-content-center">Aliados</h4>
                        </div>
                        <div class="col col-2 custom-col">
                            <h2 class="text-white">14</h2>
                        </div>
                        <div class="col col-2 custom-col">
                            <h4 class="text-white bg-info w-100 h-100 m-0 d-flex align-items-center justify-content-center">Países</h4>
                        </div>
                        <div class="col col-2 custom-col">
                            <h2 class="text-white">15</h2>
                        </div>
                        <div class="col col-1 py-2 custom-col">
                           <div class="border">
                            <h5 class="text-white">Gold</h5>
                            <h4 class="text-info">8</h4>
                            </div>
                        </div>
                        <div class="col col-1 py-2 custom-col">
                           <div class="border">
                            <h5 class="text-white">VIP</h5>
                            <h4 class="text-info">11</h4>
                            </div>
                        </div>
                        <div class="col col-1 py-2 custom-col">
                           <div class="border">
                            <h5 class="text-white">A+</h5>
                            <h4 class="text-info">32</h4>
                            </div>
                        </div>
                        <div class="col col-1 py-2 pr-md-2 custom-col">
                           <div class="border">
                            <h5 class="text-white">A</h5>
                            <h4 class="text-info">57</h4>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide"><img src="/images/icons/colombia.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/mexico.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/bolivia.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/brasil.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/canada.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/chile.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/argentina.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/argentina.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/argentina.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/argentina.png" alt="" width="50%"></div>
                        <div class="swiper-slide"><img src="/images/icons/argentina.png" alt="" width="50%"></div>
                    </div>
                        <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-8 col-sm-6">
            <div class="container">
                <div class="row justify-content-end" id="info_oportunidades">
                    <div class="col col-4 rounded-left bg-primary-2 d-flex align-items-center justify-content-center">
                        <h3 class="text-white"><img src="/images/icons/rocket_oport.svg" alt="Rocket" class="mr-3">Oportunidades</h3>
                    </div>
                    <div class="col col-2 border-custom">
                        <h5 class="text-info">Vigentes</h5>
                        <h3 class="my-2">4</h3>
                        <h5>23.456 Millones</h5>
                    </div>
                    <div class="col col-2 border-custom">
                        <h5 class="text-info">Vendidas</h5>
                        <h3 class="my-2">11</h3>
                        <h5>11.872 Millones</h5>
                    </div>
                    <div class="col col-2 border-custom">
                        <h5 class="text-info">Perdidas</h5>
                        <h3 class="my-2">2</h3>
                        <h5>1.247 Millones</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 d-flex align-items-center">
           <div class="container">
               <a type="button" class="btn btn-lg rounded globlal-btns" href="/freelance/oportunidades"><i class="fa fa-list fa-fw" aria-hidden="true"></i>Oportunidades</a>
               <a type="button" class="btn btn-lg rounded globlal-btns" href="/freelance/listado"><i class="fa fa-list fa-fw" aria-hidden="true"></i>Freelance</a>
           </div>
        </div>
    </div>

    <div class="row my-5">
        <div class="col-12" id="Grafica">
            <div id="global_chart" class="position-relative mx-auto"></div>
        </div>
    </div>

    <form class="form-row mt-5 pt-4 justify-content-center no-gutters">
        <div class="col-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="date" id="fecha_inicio" class="form-control filtro">
            </div>
        </div>
        <div class="col-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="date" id="fecha_fin" class="form-control filtro">
            </div>
        </div>
    </form>

    <div class="container mt-5">
    <div class="row justify-content-center" id="Gastos_Ventas">
        <div class="col-md-4 col-sm-12">
           <h4 class="text-primary">Gastos</h4>
            <ol class="text-left bg-light-2">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                        <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                        <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                    </div>
                    <div class="col-md-6">
                        <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                        <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                        <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                    </div>
                </div>
            </ol>
        </div>
        <div class="col-md-4 col-sm-12">
            <h4 class="text-primary">Ingresos</h4>
            <ol class="text-left bg-light-2">
               <div class="row no-gutters">
                    <div class="col-md-6">
                <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                   </div>
                   <div class="col-md-6">
                <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                <li class="my-2">Jorge Armando Arias <span class="ml-3 text-info">34.201.562</span></li>
                </div>
                </div>
            </ol>
        </div>
    </div>
    </div>

@endsection
@section('scripts')
<!-- amCharts javascript sources -->
<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
<script>
    var CSRF_TOKEN = $('input[name=_token]').val();
    $(document).ready(function() {
        aliados();
        oportunidades();
        presupuesto_gastos();
        gastos_ingresos();
        /*AmCharts.makeChart("global_chart",
				{
					"type": "serial",
					"categoryField": "category",
					"columnWidth": 0,
					"startDuration": 1,
					"startEffect": "easeOutSine",
					"backgroundColor": "#1C2232",
					"color": "#FFFFFF",
					"theme": "light",
					"categoryAxis": {
						"gridPosition": "start",
						"axisAlpha": 1,
						"axisColor": "#FFFFFF",
						"fillColor": "#46C6FF"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonColor": "#009EB5",
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"bulletBorderColor": "undefined",
							"color": "#012D46",
							"fillColors": "#009EB5",
							"id": "AmGraph-1",
							"lineColor": "#009EB5",
							"title": "Presupuesto Anual",
							"valueField": "column-1"
						},
						{
							"balloonColor": "#88E006",
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "square",
							"color": "#000000",
							"fillColors": "#FF8000",
							"id": "AmGraph-2",
							"lineColor": "#88E006",
							"periodSpan": 2,
							"precision": -2,
							"title": "Gastos Anual",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisAlpha": 1,
							"axisColor": "#FFFFFF",
							"color": "#FFFFFF",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {
						"showBullet": true
					},
					"legend": {
						"enabled": true,
						"color": "#FFFFFF",
						"rollOverColor": "#46C6FF",
						"useGraphSettings": true
					},
					"titles": [
						{
							"color": "#FFFFFF",
							"id": "Title-1",
							"size": 12,
							"text": "Presupuesto vs Gasto"
						}
					],
					"dataProvider": [
						{
							"category": "Enero",
							"column-1": "2200",
							"column-2": "1100"
						},
						{
							"category": "Febrero",
							"column-1": "3250",
							"column-2": "2850"
						},
						{
							"category": "Marzo",
							"column-1": "4587",
							"column-2": "4894"
						},
						{
							"category": "Abril",
							"column-1": "5241",
							"column-2": "6897"
						},
						{
							"category": "Mayo",
							"column-1": "9352",
							"column-2": "12587"
						},
						{
							"category": "Junio",
							"column-1": "12784",
							"column-2": "15247"
						},
						{
							"category": "Julio",
							"column-1": "15478",
							"column-2": "12478"
						},
						{
							"category": "Agosto",
							"column-1": "19658",
							"column-2": "16985"
						},
						{
							"category": "Septiembre",
							"column-1": "21847",
							"column-2": "22000"
						},
						{
							"category": "Octubre",
							"column-1": "23568",
							"column-2": "27358"
						},
						{
							"category": "Noviembre",
							"column-1": "25417",
							"column-2": "35241"
						},
						{
							"category": "Diciembre",
							"column-1": "30254",
							"column-2": "42036"
						}
					]
				}
			);*/
    });

    /**
     * Función para inicializar elementos (swiper)
     */
    function inicializacion(){
        //initialize swiper when document ready
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 7,
            centeredSlides: true,
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }

    /**
     * Función para traer datos reales de los aliados cuadro superior
     */
    function aliados(){
        var jqxhr = $.ajax({
                url: "/freelance/dashboard-accion",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 0
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#inf_aliados .custom-col:nth-child(2) h2').html(e.data['aliados']);
                    $('#inf_aliados .custom-col:nth-child(4) h2').html(e.data['paises']);
                    $('#inf_aliados .custom-col:nth-child(5) h4').html(e.data['categorias'][0]['GOLD']);
                    $('#inf_aliados .custom-col:nth-child(6) h4').html(e.data['categorias'][0]['VIP']);
                    $('#inf_aliados .custom-col:nth-child(7) h4').html(e.data['categorias'][0]['APOSITIVO']);
                    $('#inf_aliados .custom-col:nth-child(8) h4').html(e.data['categorias'][0]['A']);
                    $('.swiper-wrapper').html(e.data['html']);
                    inicializacion();
                }
            });
    }

    /**
     * Función para traer la información de todas las oportunidades que pertenecen a los Freelance
     */
    function oportunidades(){
        var jqxhr = $.ajax({
                url: "/freelance/dashboard-accion",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 1
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#info_oportunidades .border-custom:nth-child(2) h3').html(e.data['oportunidades_vigentes']);
                    $('#info_oportunidades .border-custom:nth-child(2) h5:nth-child(3)').html(e.data['oportunidades_vigentes_valor']);
                    $('#info_oportunidades .border-custom:nth-child(3) h3').html(e.data['oportunidades_vendidas']);
                    $('#info_oportunidades .border-custom:nth-child(3) h5:nth-child(3)').html(e.data['oportunidades_vendidas_valor']);
                    $('#info_oportunidades .border-custom:nth-child(4) h3').html(e.data['oportunidades_perdidas']);
                    $('#info_oportunidades .border-custom:nth-child(4) h5:nth-child(3)').html(e.data['oportunidades_perdidas_valor']);
                }
            });
    }

    /**
     * Función para traer los datos de la grafica Presupuesto vs Gastos
     */
    function presupuesto_gastos(){
        var jqxhr = $.ajax({
                url: "/freelance/dashboard-accion",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 2
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#Grafica').html(e.data['html']);
                }
            });
    }

    /**
     * Función para llenar ultima tabla de Gastos e Ingresos
     */
    function gastos_ingresos(){
        var jqxhr = $.ajax({
            url: "/freelance/dashboard-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 3
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#Gastos_Ventas').html(e.data['html']);
            }
        });
    }

    /*filtro*/
    $(document).on('change','.filtro',function(){
        id=$(this).attr('id');
        if(id=="fecha_inicio"){
           var fechainicio = $(this).val();
           var fechafin = $('#fecha_fin').val();
            if(fechafin!="" && fechainicio!="" && fechainicio>=fechafin){
                swal(
                      'Advertencia',
                      'Debe elegir una fecha de inicio menor a la fecha fin',
                      'warning'
                    );
                $(this).val('');
            }else if(fechafin!="" && fechainicio!=""){
                filtrar(fechainicio,fechafin);
            }else{
                gastos_ingresos();
            }
        }else{
           var fechainicio = $('#fecha_inicio').val();
           var fechafin = $(this).val();
            if(fechainicio!="" && fechafin!="" && fechainicio>=fechafin){
                swal(
                      'Advertencia',
                      'Debe elegir una fecha fin mayor a la fecha de inicio',
                      'warning'
                    );
                $(this).val('');
            }else if(fechainicio!="" && fechafin!=""){
                filtrar(fechainicio,fechafin);
            }else{
                gastos_ingresos();
            }
        }
    });

    function filtrar(fechainicio,fechafin){
        var jqxhr = $.ajax({
            url: "/freelance/dashboard-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 3,
                fecha_inicio:fechainicio,
                fecha_fin:fechafin
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#Gastos_Ventas').html(e.data['html']);
            }
        });
    }
</script>
@endsection
