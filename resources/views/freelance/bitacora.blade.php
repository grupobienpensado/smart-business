@extends('template.app')
@section('title', 'Bitacora Freelance')
@section('content')
<link rel="stylesheet" href="/css/freelance/btnguardar.min.css">
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/assets/DataTable/jquery.dataTables.css">
<style>
    .freelancer-container{background-color:#FFF;width:100%;height:100%;border-radius:10px} .encabezado th:first-child{background-color: #051d60 !important;color: #fff;text-align: center;border-radius: 10px 0px 0px 10px;-moz-border-radius: 10px 0px 0px 10px;-webkit-border-radius: 10px 0px 0px 10px;border: 0px solid #000000;}.encabezado th:last-child{background-color: #051d60;color: #fff;text-align: center;border-radius: 0px 10px 10px 0px;-moz-border-radius: 0px 10px 10px 0px;-webkit-border-radius: 0px 10px 10px 0px;border: 0px solid #000000;} .encabezado th:nth-child(n+2){background-color: #051d60 !important;color: #fff;text-align: center;}.img-redonda{border-radius: 200px 200px 200px 200px;-moz-border-radius: 200px 200px 200px 200px;-webkit-border-radius: 200px 200px 200px 200px;border: 0px solid #000000;width:60px;}.seleccionado{cursor:no-drop;}.no-seleccionado{cursor:pointer;opacity: 0.5;}
/*Eliminar alerta que sale en el editor de texto*/
.mce-notification.mce-has-close {display: none;}.flotante {position:fixed;bottom:0;right:0;}.mt-20 {margin-top: 20rem;}
</style>
<div class="container">
    <div class="jumbotron">
        <div class="panel-title">
            <div class="pull-right">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" onclick="anadiractividad(this.id)" id="btn_anadiractividad" class="btn btn-success"><i class="fa fa-plus"></i> Añadir Actividad</button>
                    <button type="button" onclick="accion(2)" class="btn btn-info" data-action="1" data-toggle="tooltip" data-placement="top" title="Tipos de Gastos"><i class="fa fa-plus"></i> Tipos de gastos</button>
                </div>
            </div>
            <h2>Freelance Bitacora</h2>
            <p class="text-muted text-capitalize">Listado de actividades freelance</p>
        </div>
    </div>
    <div class="freelancer-container" id="listado_actividades">
        <table id="actividades" class="display">
            <thead>
                <tr class="encabezado">
                   <th>Item</th>
                    <th>Freelance</th>
                    <th>Tipo Bitacora</th>
                    <th>Oportunidad</th>
                    <th>Fecha Incio</th>
                    <th>Fecha Fin</th>
                    <th>Tipo de Bitacora</th>
                    <th>Creado</th>
                </tr>
            </thead>
            <tbody id="lista">

            </tbody>
        </table>
    </div>
    <div class="container bg-white" id="nueva_actividad" style="display:none;">
       <form method="POST" action="" id="formulario" enctype="multipart/form-data">
       {{ csrf_field() }}
       <input type="hidden" value="2" name="action">
        <div class="row">
            <div class="col-md-12">
                <h3>Crear Actividad</h3>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <label for="fecha_inicio">Fecha de inicio</label>
                <input class="form-control fecha-inicio" onchange="fecha_seleccionada(this.id,'#fecha_mostrar',this.value)" type="text" name="formulario[fecha_inicio]" id="fecha_inicio" value="" placeholder="Fecha Inicio" required>
                <div class="row" style="display: none;" id="fecha_mostrar">
                    <div class="col-md-10">
                        <input class="form-control" type="text" readonly style="cursor: no-drop;">
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="Editar Fecha" onclick="editar_fecha('#fecha_mostrar','#fecha_inicio')"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <label for="hora_inicio">Hora de inicio</label>
                <input class="form-control hora-inicio" onchange="horainicio_seleccionada(this.id,'#horainicio_mostrar',this.value)" type="text" name="formulario[hora_inicio]" id="hora_inicio" value="" placeholder="Hora Inicio" required>
                <div class="row" style="display: none;" id="horainicio_mostrar">
                    <div class="col-md-10">
                        <input class="form-control" type="text" readonly style="cursor: no-drop;">
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="Editar Hora" onclick="editar_horainicio('#horainicio_mostrar','#hora_inicio')"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <label for="fecha_fin">Fecha de fin</label>
                <input class="form-control fecha-inicio" onchange="fecha_seleccionada(this.id,'#fechafin_mostrar',this.value)" type="text" name="formulario[fecha_final]" id="fecha_fin" value="" placeholder="Fecha Fin" required>
                <div class="row" style="display: none;" id="fechafin_mostrar">
                    <div class="col-md-10">
                        <input class="form-control" type="text" readonly style="cursor: no-drop;">
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="Editar Fecha" onclick="editar_fecha('#fechafin_mostrar','#fecha_fin')"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <label for="hora_fin">Hora de fin</label>
                <input class="form-control hora-inicio" onchange="horainicio_seleccionada(this.id,'#horafin_mostrar',this.value)" type="text" name="formulario[hora_final]" id="hora_fin" value="" placeholder="Hora fin" required>
                <div class="row" style="display: none;" id="horafin_mostrar">
                    <div class="col-md-10">
                        <input class="form-control" type="text" readonly style="cursor: no-drop;">
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="Editar Hora" onclick="editar_horainicio('#horafin_mostrar','#hora_fin')"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-12 col-md-4">
                <label for="tipo_bitacora">Tipo de Bitacora</label>
                <select class="form-control" name="formulario[tipo_bitacora]" id="tipo_bitacora" onchange="accion_tipo_bitacora(this.value)" style="height:37px;" required="required">
                    <option>Seleccione una opcion</option>
                    <option value="0">Gestion Freelance Oportunidad</option>
                    <option value="1">Gestion Freelance Actividad</option>
                    <option value="2">Informe Administrador</option>
                </select>
            </div>
            <div class="col-sm-12 col-md-8" id="seccion_freelance">
                <label for="id_freelance_user">Freelance</label>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-redonda" id="user_elejido" src="http://via.placeholder.com/60x60">
                    </div>
                    <div class="col-md-9 elegir-user-freelance">
                       <input type="text" class="form-control" list="UserFreelance" id="id_freelance_user" name="formulario[id_freelance_user]" onchange="elegir_freelance(this.value)">
                        <datalist id="UserFreelance">
                            @foreach($data['usuarios'] as $usuario)
                            <option value="<?=$usuario->id?>"><?=$usuario->nombres.' '.$usuario->apellidos?></option>
                            @endforeach
                        </datalist>
                    </div>
                    <div class="col-md-9 mostrar-user-freelance" style="display:none;">

                    </div>
                </div>
            </div>
            <!--Esta seccion esta oculta hasta que se seleccione el tipo de bitacora 0=Gestión Freelance Oportunidad-->
            <div class="col-sm-12 col-md-4" id="oportunidad_capturar" style="display:none">
                <label for="id_oportunidad">Oportunidad</label>
                <input type="text" class="form-control" id="id_oportunidad" onchange="buscarnombre_oportunidad(this.value)" name="formulario[id_oportunidad]" list="lista_oportunidades" placeholder="Seleccione la oportunidad">
                <datalist id="lista_oportunidades">
                    @foreach($data['oportunidades'] as $oportunidad)
                    <?php if($oportunidad->VALOR > 0 && $oportunidad->VALOR < 90){ ?>
                    <option value="{{$oportunidad->id}}">{{$oportunidad->titulo}}</option>
                    <?php } ?>
                    @endforeach
                </datalist>
            </div>
            <div class="col-sm-12 col-md-4" id="oportunidad_mostrar" style="display:none;">
                <label for="id_oportunidad">Oportunidad</label>
                <div class="row">
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="nombre_oportunidad" placeholder="Seleccione la oportunidad" style="cursor:no-drop" readonly>
                    </div>
                    <div class="col-md-2">
                        <a class="text-warning" onclick="modificar_oportunidad()" style="cursor: pointer; font-size: 2rem;" data-toggle="tooltip" data-placement="top" title="Editar oportunidad"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <label>Gastos de la bitacora</label><br>
                <a class="text-success" style="font-size: 5rem; cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Agregar un Gasto" onclick="accion(1)"><i class="fa fa-plus"></i></a>
            </div>
            <div class="col-sm-12 col-md-5">
                Tipo de Gasto
            </div>
            <div class="col-sm-12 col-md-5">
                Valor del gasto
            </div>
            <div class="col-sm-12 col-md-2">
                Acción
            </div>
        </div>
        <div class="row" id="lista_gasto">
            <div class="col-sm-12 col-md-5 mt-3 G-0">
                <select class="form-control" name="gastos[0][id_tipo_gasto]" style="height:33px;">
                    <option value="">Selccione un tipo de gasto</option>
                    @foreach($data['tipo_gasto'] as $tipo)
                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-12 col-md-5 mt-3 G-0">
                <input type="text" class="form-control" id="valorG_0" name="gastos[0][valor]" onkeyup="formato_numero(this.value,this.id)" Placeholder="$ 0">
            </div>
            <div class="col-sm-12 col-md-2 mt-3 G-0">
                <a class="text-danger" style="font-size:2rem;" onclick="eliminar_item_gasto('.G-0')"><i class="fa fa-trash"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <label for="comentario_bitacora">Comentario Bitacora</label>
                <textarea id="comentario_bitacora" name="formulario[descripcion]"></textarea>
            </div>
        </div>
        <div class="row flotante">
            <div class="col-md-12">
                <a class="snip1489" data-toggle="tooltip" data-placement="top" title="Guardar Uusario" onclick="guardar()"><i class="fa fa-save"></i></a>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf8" src="/assets/DataTable/jquery.dataTables.js"></script>
<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="/assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/js/parsley.min.js"></script>
<script>
    var numerogasto = 0;
    $(document).ready(function(){
       accion(0);
       fecha();
       activar_editor_texto();
       $('[data-toggle="tooltip"]').tooltip();
    });

    /**
     * Función para ver la oportunidad
     * @param INT id IDENTIFICACIÓN DE LA ACTIVIDAD
     */
    function ver_actividad(id){
        alert(id);
    }
    /**
     * Función para dar formato a la moneda
     */
    function formato_numero(valor,id){
        valor = replaceAll(valor, "$ ", "" );
        if(valor != ""){
           valor = replaceAll(valor, ",", "" );
           valor = parseFloat(valor);

           if(isNaN(valor)){
                $('#'+id).val('$ 0');
            }else{
                valor = number_format(valor,0);
                $('#'+id).val("$ "+valor);
            }
        }else{
           $('#'+id).val('$ 0');
        }
    }
    function number_format(amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    function replaceAll( text, busca, reemplaza ){
      while (text.toString().indexOf(busca) != -1)
          text = text.toString().replace(busca,reemplaza);
      return text;
    }
    /**
     * Función para eliminar un item del gasto
     * @param STRING clase CLASE A ELIMINAR
     */
    function eliminar_item_gasto(clase){
        $(clase).remove();
    }
    /**
     * Función para mostrar el listado de oportunidades dependiendo el tipo de oportunidad
     * @param INT valor VALOR DEL TIPO DE OPORTUNIDAD (0=Gestión Freelance Oportunidad 1=Gestión Freelance Actividad 2=Informe Administrador)
     */
    function accion_tipo_bitacora(valor){
        if(valor == 0){
           $('#seccion_freelance').removeClass('col-md-8');
           $('#seccion_freelance').addClass('col-md-4');
           $('#oportunidad_capturar').css('display','block');

        }else{
           $('#seccion_freelance').removeClass('col-md-4');
           $('#seccion_freelance').addClass('col-md-8');
           $('#oportunidad_capturar').css('display','none');

        }
         $('#id_oportunidad').val('');
         $('#oportunidad_mostrar').css('display','none');
    }
    /**
     * Función para mostrar el formulario para crear una nueva actividad
     * @param INT id IDENTIFICACIÓN DE UNA ACTIVIDAD
     */
    function anadiractividad(id){
        if($('#listado_actividades').css('display') == 'none' ){
            $('#listado_actividades').css('display','block');
            $('#nueva_actividad').css('display','none');
            $('#'+id).html('<i class="fa fa-plus"></i> Añadir Actividad');
        }else{
            $('#listado_actividades').css('display','none');
            $('#nueva_actividad').css('display','block');
            $('#'+id).html('<i class="fa fa-plus"></i> Listado Actividades');
        }
    }
    /**
     * Función para realizar las acciones correspondientes a listar, crear, editar, eliminar
     * @param INT action ACCION A REALIZAR
     * @param INT id     IDENTIFICACION DE LA TIPO DE ACTIVIDAD A (ELIMINAR O EDITAR).
     */
    function accion(action,id){
        switch(action){
            /*Lista*/
            case 0:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/bitacora-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: action,
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            try {
                                 tabla.destroy();
                            }
                            catch(err) {
                               console.log(err.message);
                            }
                            $('#lista').html(e.data['html']);
                            ejecutartabla();
                        }
                });
                break;
            /*Función para agregar un gasto al formulario de actividad*/
            case 1:
                numerogasto++;
                 var CSRF_TOKEN = $('input[name=_token]').val();
                 var jqxhr = $.ajax({
                        url: "/freelance/bitacora-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 4,
                            numero: numerogasto
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            $('#lista_gasto').prepend(e.data['html']);
                        }
                });
                break;
            /*Listado de tipos de gastos*/
            case 2:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/bitacora-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 5
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            listado_tipos_gastos(e.data['html']);
                        }
                });
                break;
            /*Formulario nuevo tipo de gasto*/
            case 3:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/bitacora-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 6
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            crear_tipos_gastos(e.data['html']);
                        }
                });
                break;
            case 4:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/bitacora-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 8,
                            id: id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            editar_tipos_gastos(e.data['html']);
                        }
                });
                break;
            case 5:
                swal({
                      title: 'Eliminar?',
                      text: "Esta seguro de eliminar el tipo de oportunidad!",
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Si, Eliminar',
                      cancelButtonText: 'No'
                    }).then(function(){
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                            url: "/freelance/bitacora-accion",
                            data: {
                                _token: CSRF_TOKEN,
                                action: 10,
                                id: id
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                                if(e.data['msj'] == 'Eliminado correctamente!'){
                                   swal(
                                      'Eliminado',
                                      e.data['msj'],
                                      'success'
                                    ).then(function(){
                                       accion(2);
                                   });
                                }else{
                                    swal(
                                      'Error',
                                      e.data['msj'],
                                      'error'
                                    ).then(function(){
                                       accion(2);
                                   });
                                }
                            }
                    });
                });
                break;
        }
    }
    function ejecutartabla(){
      tabla = $('#actividades').DataTable({
            dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                ],
              language: {
                processing:     "Tratamiento en curso ...",
                search:         "Buscar&nbsp;:",
                lengthMenu:     "Mostrar _MENU_ oportunidades",
                info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ actividades",
                infoEmpty:      "Ver de elemento 0 al 0 de 0 actividades",
                infoPostFix:    "",
                loadingRecords: "Cargando...",
                zeroRecords:    "Ningun archivo",
                emptyTable:     "No hay datos disponibles en la tabla",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Ultimo"
                },
                aria: {
                    sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                    sortDescending: ": habilitado para ordenar la columna en orden descendente"
                }
              },
              bPaginate: false,
        });
    }

    /**
     * Función para mostrar la fecha en formato dia/Nombre del mes/año
     * @param STRING     id_campIDENTIFICACION DEL CAMPO DE FECHA
     * @param STRING     id_mostIDENTIFICACIÓN DEL CAMPO DONDE SE MUESTRA
     * @param DATE       valor  FECHA SELECCIONADA
     */
    function fecha_seleccionada(id_campo,id_mostrar,valor){
        var fecha = valor.split("-");
        var meses = ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        var fecha_formato = fecha[2]+'/'+meses[parseInt(fecha[1])]+'/'+fecha[0];
        $(id_mostrar+' input').val(fecha_formato);
        $(id_mostrar).css('display','block');
        $('#'+id_campo).css('display','none');
    }
    /**
     * Funcion para editar la fecha
     * @param STRING id_esconder IDENTIFICACION DEL CAMPO QUE ESTA MOSTRANDO LA FECHA CON FORMATO DD/MES/YYYY
     * @param STRING id_mostrar  IDENTIFICACION DEL CAMPO QUE CAPTURA LA FECHA
     */
    function editar_fecha(id_esconder, id_mostrar){
        $(id_mostrar).css('display','block');
        $(id_mostrar).val('');
        $(id_esconder).css('display','none');
    }

     /**
     * Función para mostrar la hora en formato Hora:Minutos:Segundos JORANDA (AM. PM.)
     * @param STRING     id_campIDENTIFICACION DEL CAMPO DE FECHA
     * @param STRING     id_mostIDENTIFICACIÓN DEL CAMPO DONDE SE MUESTRA
     * @param DATE       valor  HORA SELECCIONADA
     */
    function horainicio_seleccionada(id_campo,id_mostrar,valor){
        var hora = valor.split(":");
        if(hora[0]>12){
            H=parseInt(hora[0])-12;
            Jornada = 'P.M';
           }else{
               H=parseInt(hora[0]);
               Jornada = 'P.M';
           }
        horafinal = H+':'+hora[1]+':00 '+Jornada;
        $(id_mostrar+' input').val(horafinal);
        $(id_mostrar).css('display','block');
        $('#'+id_campo).css('display','none');
    }
    /**
     * Funcion para editar la hora
     * @param STRING id_esconder IDENTIFICACION DEL CAMPO QUE ESTA MOSTRANDO LA HORA CON FORMATO HORA:MINUTO:SEGUNDO JORNADA
     * @param STRING id_mostrar  IDENTIFICACION DEL CAMPO QUE CAPTURA LA HORA
     */
    function editar_horainicio(id_esconder, id_mostrar){
        $(id_mostrar).css('display','block');
        $(id_mostrar).val('');
        $(id_esconder).css('display','none');
    }
    /**
     * Función para inicializar las fechas
     */
    function fecha(){
        $('.fecha-inicio').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          maxDate : moment().format('YYYY-MM-DD'),
          format : 'YYYY-MM-DD',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
      $('.hora-inicio').bootstrapMaterialDatePicker({
          time: true,
          date: false ,
          format : 'HH:mm',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
    }
    /**
     * Funcion para activar tinymce
     */
    function activar_editor_texto(){
        tinymce.remove();
        tinymce.init({
            selector:'#comentario_bitacora',
            height: 500,
            language : "es",
            theme: 'modern',
            branding: false,
            plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern'
        });
    }

    /**
     * Funcion para seleccionar el usuario
     * @param INT id IDENTIFICACION DEL FREELANCE
     * @param STRING elemento IDENTIFICACION DE LA IMAGEN
     */
   /* function user_freelance(id,elemento){
        $(".seleccionado").each(function(){
            $(this).removeClass('seleccionado');
            $(this).addClass('no-seleccionado');
        });
        $('#'+elemento).removeClass('no-seleccionado');
        $('#'+elemento).addClass('seleccionado');
        $('#id_freelance_user').val(id);
    }*/

    /**
     * Función para modificar la fotografia de un freelance
     * @param INT id IDENTIFICACION DE UN FREELANCE
     */
    function elegir_freelance(id){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/bitacora-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 3,
                    id:id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    crear_listado_oportunidades(id);
                    if(e.data['html']=='no existe'){
                        swal(
                              'Error!',
                              'El usuario no existe',
                              'error'
                            );
                        $('#id_freelance_user').val('');
                    }else{
                        $('#user_elejido').attr('src',e.data['FOTOPERFIL']);
                        $('.elegir-user-freelance').css('display','none');
                        $('.mostrar-user-freelance').html(e.data['html']);
                        $('.mostrar-user-freelance').css('display','block');
                    }
                }
        });
    }

    /**
     * Función para crear el listado de oportunidades que pertenece al usuario FREELANCE que seleccione
     * @param INT id IDENTIFICACIÓN DEL FREELANCE
     */
    function crear_listado_oportunidades(id){


        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/bitacora-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 11,
                    id:id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#lista_oportunidades').html(e.data['html']);
                }
        });
    }

    /**
     * Función para editar el usuario de freelance
     */
    function modificar_usuario(){
        $('#id_freelance_user').val('');
        $('#user_elejido').attr('src','http://via.placeholder.com/60x60');
        $('.elegir-user-freelance').css('display','block');
        $('.mostrar-user-freelance').html('');
        $('.mostrar-user-freelance').css('display','none');
    }

    /**
     * Función para obtener el nombre de la oportunidad
     * @param INT id IDENTIFICACIÓN DE LA OPORTUNIDAD
     */
    function buscarnombre_oportunidad(id){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/bitacora-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 1,
                    id:id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['nombre_oportunidad'] == 'No existe!'){
                        swal(
                              'Error',
                              'Debe seleccionar una oportunidad valida',
                              'error'
                            );
                        $('#id_oportunidad').val('');
                    }else{
                        $('#nombre_oportunidad').val(e.data['nombre_oportunidad']);
                        $('#oportunidad_capturar').css('display','none');
                        $('#oportunidad_mostrar').css('display','block');
                    }
                }
        });
    }

    /**
     * Función para modificar la oportunidad
     */
    function modificar_oportunidad(){
        $('#oportunidad_capturar').css('display','block');
        $('#oportunidad_mostrar').css('display','none');
        $('#id_oportunidad').val('');
        $('#nombre_oportunidad').val('');
    }

    /**
     * Función para guardar la actividad de bitacora
     */
    function guardar(){
        if(true === $("#formulario").parsley().validate()){
            swal({
                title: "Esta Seguro?",
                text: "Esta a punto de guardar esta información ",
                type: "warning",
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonColor: "#3CBC8D",
                confirmButtonText: "Si, Guardar!",
                cancelButtonText: "No, Déjame comprobar!",
                closeOnConfirm: false
            }).then(function(){
                tinymce.remove();
                var formu = document.getElementById("formulario");
                var data = new FormData(formu);
                $.ajax({
                url:"/freelance/bitacora-accion",
                type:"POST",
                data: data,
                contentType:false,
                processData:false,
                cache:false
                }).success(function(e){
                    if(e.data['msj'] == "Guardado correctamente!"){
                        swal({
                            title: "Guardado",
                            text: e.data['msj'],
                            type: "success",
                            allowOutsideClick: false,
                            focusConfirm: false,
                            showCancelButton: false
                            }).then(function(){
                                activar_editor_texto();
                                /*Refrescar la lista y mostrarla*/
                                accion(0);
                                $('#btn_anadiractividad').click();
                            });
                       }else{
                          activar_editor_texto();
                          swal({
                            title: "Error",
                            text: e.data['msj'],
                            type: "error",
                            allowOutsideClick: false,
                            showCancelButton: false
                            });
                       }
              });
            });
        }
    }

    /**
     * Función para crear el listado de tipos de gastos
     * @param STRING html HTML CON LA ESTRUCTURA DEL LISTADO
     */
    function listado_tipos_gastos(html){
        swal({
              title: 'Tipos de Gastos',
              html:html,
              showConfirmButton: false,
              showCancelButton: true,
              focusConfirm: false,
              cancelButtonColor: '#d33',
              cancelButtonText:'<i class="fa fa-times"></i> Cerrar'
            });
        $('.swal2-modal').css('width','50%');
    }

    /**
     * Función para el formulario de crear un tipo de gasto
     * @param STRING html HTML QUE TIENE EL CAMPO PARA LLENAR EL TIPO DE GASTO
     */
    function crear_tipos_gastos(html){
        swal({
              title: 'Crear Gasto',
              html:html,
              showConfirmButton: true,
              showCancelButton: false,
              focusConfirm: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText:'<i class="fa fa-save"></i> Guardar'
            }).then(function(){
            var nombre_gasto = $('#nombre_gasto').val();
            if(nombre_gasto == ""){
                swal(
                      'Error!',
                      'El tipo de gasto no puede ser vacio!',
                      'error'
                    ).then(function(){
                    crear_tipos_gastos(html);
                });
            }else{
                save_tipo_gasto(nombre_gasto);
            }
        });
    }

    /**
     * Función para guardar el tipo de gasto que el usuario ingreso y ya esta validado
     * @param STRING nombre VARIABLE QUE CONTIENE EL NOMBRE DEL NUEVO TIPO DE GASTO
     */
    function save_tipo_gasto(nombre){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/bitacora-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 7,
                    nombre: nombre
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == "Guardado correctamente!"){
                           swal(
                              'Guardado!',
                              e.data['msj'],
                              'success'
                            ).then(function(){
                               accion(2);
                           });
                       }else{
                           swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            ).then(function(){
                               accion(2);
                           });
                       }
                }
        });
    }

    /**
     * Función para el formulario de editar un tipo de gasto
     * @param STRING html HTML QUE TIENE EL CAMPO PARA LLENAR EL TIPO DE GASTO
     */
    function editar_tipos_gastos(html){
        swal({
              title: 'Editar Gasto',
              html:html,
              showConfirmButton: true,
              showCancelButton: false,
              focusConfirm: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText:'<i class="fa fa-save"></i> Guardar'
            }).then(function(){
            var nombre_gasto = $('#editar_nombre_gasto').val();
            var id_gasto = $('#editar_id_gasto').val();
            if(nombre_gasto == ""){
                swal(
                      'Error!',
                      'El tipo de gasto no puede ser vacio!',
                      'error'
                    ).then(function(){
                    editar_tipos_gastos(html);
                });
            }else{
                update_tipo_gasto(nombre_gasto,id_gasto);
            }
        });
    }

     /**
     * Función para guardar el tipo de gasto que el usuario ingreso y ya esta validado
     * @param STRING nombre VARIABLE QUE CONTIENE EL NOMBRE DEL NUEVO TIPO DE GASTO
     * @param INT id VARIABLE QUE CONTIENE IDENTIFICACION DEL TIPO DE IDENTIFICACIÓN
     */
    function update_tipo_gasto(nombre,id){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/bitacora-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 9,
                    nombre: nombre,
                    id:id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == "Guardado correctamente!"){
                           swal(
                              'Guardado!',
                              e.data['msj'],
                              'success'
                            ).then(function(){
                               accion(2);
                           });
                       }else{
                           swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            ).then(function(){
                               accion(2);
                           });
                       }
                }
        });
    }

    /**
     * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
     * @param STRING texto  TEXTO INGRESADO EN EL CAMPO
     * @param INT max_long NUMERO MAXIMO DE CARACTERES PARA EL CAMPO
     * @param STRING id  IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
     * @param STRING id_campo  IDENTIFICACION DEL CAMPO EDITADO
     */
    function max_campo(texto,max_long,id,id_campo){
        numeroCaracteres = texto.length;
        $(id).removeClass('invisible');
        if(numeroCaracteres > max_long){
            $('#'+id_campo).val(texto.substr(0,max_long));
            $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
        }else{
            $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
        }
    }
</script>
@endsection
