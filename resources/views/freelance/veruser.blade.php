@extends('template.app')
@section('title', 'Ver Freelance')
@section('content')
<link rel="stylesheet" href="/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    #chartdiv,#chartdiv1 { width: 100%; height: 500px !important; font-size: 11px; }.oportunidad-user-freelance .onoffswitch-inner:after{content: "No" !important;}.oportunidad-user-freelance .onoffswitch-inner:before{padding-left: inherit !important;content: "Si" !important;}.user-no{cursor: pointer;opacity: 0.5;}.user-si{cursor: no-drop;opacity: 1;}.nav li{ cursor: pointer } .imagen-redonda{border-radius: 200px 200px 200px 200px; -moz-border-radius: 200px 200px 200px 200px; -webkit-border-radius: 200px 200px 200px 200px; border: 0px solid #000000;}.bg-ESSI{background-color: #051d60;}.pais-p{width: 150px;height: 140px;margin-left: 45%;}.pais-s{width: 150px;height: 100%;}.col-archivo{max-width:100%;position:relative;overflow:hidden;max-height:203px;min-width:203px;min-height:203px;margin:10px;padding:0}.file{visibility: hidden;position: absolute;}.col-archivo a img{max-width: 100%;min-width: 100%;}.menu{color:#1d92af;}.loader-cylon{position:relative;height:5px;overflow:hidden;margin-top:-.6%}@media (min-width:1260px) and (max-width:1600px){.loader-cylon{margin-top:-1.1%}}.loader-cylon:after{content:'';display:block;height:100%;width:100%;position:absolute;background-color:#0a78da;-webkit-animation:cylon 3s ease-out infinite;animation:cylon 3s ease-out infinite}@-webkit-keyframes cylon{0%,100%{left:-100%}50%{left:100%}}@keyframes cylon{0%,100%{left:-100%}50%{left:100%}}/*comentario*/.comments,textarea{font-family:"PT Sans","Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;-webkit-font-smoothing:antialiased}textarea{font-size:1.5rem;outline:0;border:none;display:block;margin:0;padding:0;color:#555f77}input::-webkit-input-placeholder,textarea::-webkit-input-placeholder{color:#ced2db}input::-moz-placeholder,textarea::-moz-placeholder{color:#ced2db}input:-moz-placeholder,textarea:-moz-placeholder{color:#ced2db}input:-ms-input-placeholder,textarea:-ms-input-placeholder{color:#ced2db}.comments{overflow-y:scroll;background-color:#f0f2fa;width:100%;height:600px;padding-bottom:1rem;padding-top:1rem;border-top-left-radius:10px;border-top-right-radius:10px}.comment-wrap{margin-bottom:1.25rem;display:table;width:100%;min-height:5.3125rem}.photo{padding:1.5rem;display:table-cell;width:3.5rem}.photo .avatar{height:6.25rem;width:6.25rem;border-radius:50%;background-size:cover}.comment-block{padding:1rem;background-color:#fff;display:table-cell;vertical-align:top;border-radius:.1875rem;box-shadow:0 1px 3px 0 rgba(0,0,0,.08)}.comment-block textarea{width:100%;resize:none}.comment-text{font-size:1.5rem;margin-bottom:1.25rem;color:#000;font-weight:400}.bottom-comment{color:#acb4c2}.comment-date{float:left}.comment-actions{float:right}.comment-actions li{display:inline;margin:-2px;cursor:pointer}.comment-actions li.complain{padding-right:.75rem;border-right:1px solid #e1e5eb}.comment-actions li.reply{padding-left:.75rem;padding-right:.125rem}.comment-actions li:hover{color:#0095ff}/*Fin comentario*/ /*filtros de flujo de caja*/.margin-filtro-l{margin-left:35%}.margin-filtro-r{margin-right:35%}@media (min-width:1366px) and (max-width:1600px){.margin-filtro-l{margin-left:30%}.margin-filtro-r{margin-right:30%}}.filtro-new{border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px 10px 10px 10px;border:1px solid #0000002e;background-color:#fff}.fecha-filtro{background-color:transparent;border:0;font-size:12px}.btn-filtrar{border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px 10px 10px 10px;border:1px solid #75BB11;background-color:#75BB11;color:#fff;cursor:pointer}.btn-filtrar:hover{background-color:transparent;color:#75BB11}/*filtros de flujo de caja*/
</style>
<div class="loader-cylon" style="display: none;"></div>
<div class="main-header mt-5">
    <h2>Usuario Freelance</h2>
    <em>Ver datos e información del usuario freelance</em>
</div>
<div class="container bg-white">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills" role="tablist">
                <li class="menu active" onclick="accion_menu(0)"><a><i class="fa fa-info-circle"></i> General</a></li>
                <li class="menu" onclick="accion_menu(1)"><a><i class="fa fa-rocket"></i> Oportunidad</a></li>
                <li class="menu" onclick="accion_menu(2)"><a><i class="fa fa-line-chart"></i> Flujo de caja</a></li>
                <li class="menu" onclick="accion_menu(3)"><a><i class="fa fa-money-bill"></i> Presupuesto</a></li>
            </ul>
        </div>
    </div>
    {{ csrf_field() }}
    <input type="hidden" id="id_user" value="{{$data['id']}}">
    <div class="row mt-5" id="contenido">

    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;" />
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="{{url('/')}}" id="URL" >
<div class="modal fade bd-example-modal-lg" id="documento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <iframe id="ver_pdf" src="https://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>
<!--Modal para ñadir oportunidades -->
<div class="modal fade bd-example-modal-lg" id="anadir_oportunidad" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>
<!--Template para agregar nuevo concepto -->
<template id="tem_presupuesto">
    <div class="col-md-12 text-left">
        <div class="row">
            <div class="col-md-2">
                <p class="h4"><strong data-concepto>Concepto</strong></p>
            </div>
            <div class="col-md-10">
                <div class="row">
                   <?php for($i=0;$i<12;$i++){ ?>
                    <div class="col-md-1"><p class="h6"><strong class="valor_presupuesto" data-valormes<?=$i?>>$0</strong></p></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</template>
<datalist id="lista_conceptos">

</datalist>
<!--Template del formulario para capturar un nuevo concepto -->
<template id="formulario_presupuesto">
    <div class="row">
        <div class="col-md-12">
            <label>Concepto</label>
            <input type="text" class="form-control" id="concepto" list="lista_conceptos" data-nombre="Campo: Concepto">

        </div>
    </div>
    <div class="row">
       <?php
        $meses_ano = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        for($i=0;$i<12;$i++){ ?>
        <div class="col-md-6">
            <label><?=$meses_ano[$i]?></label>
            <input type="text" class="form-control" id="mes_<?=$i+1?>" value="$ 0" onkeyup="formato_numero(this.value,this.id)" data-nombre="Campo: <?=$meses_ano[$i]?>">
        </div>
        <?php } ?>
    </div>
    <div class="row mt-5">
        <div class="col-md-12">
            <p class="bg-success" style="height:24px;cursor:pointer;" onclick="presupuesto('guardar')"><a class="text-white"><i class="fa fa-save"></i> Guardar</a></p>
        </div>
    </div>
</template>
@endsection
@section('scripts')
<script src="/js/jquery.blueimp-gallery.min.js"></script>
<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="/js/parsley.min.js"></script>
<!-- Resources FLUJO DE CAJA-->
<script src="/assets/grafica_flujo_caja_freelance/amcharts.js"></script>
<script src="/assets/grafica_flujo_caja_freelance/serial.js"></script>
<script src="/assets/grafica_flujo_caja_freelance/export.min.js"></script>
<link rel="stylesheet" href="/assets/grafica_flujo_caja_freelance/export.css" type="text/css" media="all" />
<script src="/assets/grafica_flujo_caja_freelance/light.js"></script>
<!--FIN Resources FLUJO DE CAJA-->
<script>
    var oportunidades = Array(<?=$data['oportunidades'][0]->id?>);
</script>
<script src="/js/freelance/veruser.js"></script>
@endsection

