@extends('template.app')
@section('title', 'Editar Freelance')
@section('content')
<link rel="stylesheet" href="/css/freelance/btnguardar.min.css">
<link rel="stylesheet" href="/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/html5imageupload.css?v1.3" rel="stylesheet">
<style>
    .pais-p{ width: 10%; height: 100%; } .pais-s{ width: 50%; height: 100%; } .imagen-perfil{ height: 200px; width: 200px; max-height: 200px; min-height: 200px; @if(!empty($data['usuario']->foto)) background-image: url('{{$data["usuario"]->FOTOPERFIL}}'); @endif } @if(!empty($data['usuario']->foto)) .imagen-perfil:after{ content: '' !important; } @endif .foto-completa{ width: 206px;height: 616px;max-height: 616px;min-width: 206px;min-height: 616px; @if(!empty($data['usuario']->foto_completa)) background-image:  url('{{$data["usuario"]->FOTOCOMPLETA}}'); @endif } @if(!empty($data['usuario']->foto_completa)) .foto-completa:after{ content: '' !important; } @endif .bg-ESSI{ background-color: #051d60; } .h-auto{ height: auto !important; } /*CSS para pais-departamento-ciudad*/ @for($i=1; $i<=2; $i++) <?php if($i == 2){ $m = $i; }else{ $m = ""; } ?> .ciudad_mostrar<?=$m?>{ cursor: no-drop; } .fila-ciudad<?=$m?>{ display: none; } .no-cursor<?=$m?>{ cursor: no-drop; } .departamento_mostrar<?=$m?>{ cursor: no-drop; } .fila-departamento<?=$m?>{ display: none; } .cambiar-ciudad<?=$m?>{ cursor: pointer; } .cambiar-departamento<?=$m?>{ cursor: pointer; } .cambiar-pais<?=$m?>{ cursor: pointer; } .fila-pais<?=$m?>{ display: none; } .pais_mostrar<?=$m?>{ cursor: no-drop; } @endfor /*FIN CSS para pais-departamento-ciudad*/
</style>
<div class="container">
    <div class="main-header mt-5">
        <h2>Editar Usuario Freelance</h2>
        <em>Aqui puedes editar información del usuario {{$data['usuario']->NOMBRECORTO}}</em>
    </div>
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-address-book-o"></i><a href="/freelance/listado">Listado</a></li>
                <li class="active">Editar</li>
            </ul>
        </div>
    </div>
    <form method="POST" action="" id="formulario" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" value="4" name="action">
        <input type="hidden" value="{{$data['usuario']->id}}" name="id">
        <div class="row bg-white">
            <div class="col-sm-12 col-md-4 col-lg-4 mt-3 mb-3">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Imagen</h3>
                    </div>
                    <div class="col-md-12">
                        <div class="dropzone imagen-perfil" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                            <input type="file" name="foto" accept="image/gif, image/jpeg, image/png">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Imagen Cuerpo Completo</h3>
                    </div>
                    <div class="col-md-12">
                        <div class="dropzone foto-completa" data-width="206" data-height="616" data-ajax="false" data-originalsave="true">
                            <input type="file" name="foto_completa" accept="image/gif, image/jpeg, image/png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8 mt-3 mb-3">
                <div class="row">
                    <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                        <h3>Información Personal</h3>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="nombres">Nombres</label>
                        <input class="form-control" type="text" name="formulario[nombres]" id="nombres" placeholder="Nombres" onkeyup="max_campo(this.value,30,'#texto_nombre',this.id)"  value="{{$data['usuario']->nombres}}" required>
                        <p class="text-right invisible" id="texto_nombre"><a class="text-success"><i class="fa fa-pencil"></i>... 1/30</a></p>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="apellidos">Apellidos</label>
                        <input class="form-control" type="text" name="formulario[apellidos]" id="apellidos" placeholder="Apellidos" value="{{$data['usuario']->apellidos}}" onkeyup="max_campo(this.value,30,'#texto_apellido',this.id)" required>
                        <p class="text-right invisible" id="texto_apellido"><a class="text-success"><i class="fa fa-pencil"></i>... 1/30</a></p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="tipo_documento">Tipo documento</label>
                        <select name="formulario[tipo_documento]" id="tipo_documento" class="form-control h-auto" required>
                          <option>Seleccione una opción</option>
                          <option value="0" <?php if($data['usuario']->tipo_documento == 0){ ?> Selected <?php } ?> >Cedula Ciudadania</option>
                          <option value="1" <?php if($data['usuario']->tipo_documento == 1){ ?> Selected <?php } ?> >Pasaporte</option>
                        </select>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="documento">Numero Documento</label>
                        <input class="form-control" id="documento" type="text" name="formulario[numero_documento]" placeholder="Numero Documento" onkeyup="max_campo(this.value,20,'#texto_numero',this.id)" value="{{$data['usuario']->numero_documento}}" required>
                        <p class="text-right invisible" id="texto_numero"><a class="text-success"><i class="fa fa-pencil"></i>... 1/20</a></p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="fecha_nacimiento">Fecha Nacimiento</label>
                        <input class="form-control fecha-nacimiento" style="display: none;" onchange="fecha_seleccionada(this.id,'#fecha_mostrar',this.value)" type="text" name="formulario[fecha_nacimiento]" id="fecha_nacimiento" value="{{$data['usuario']->fecha_nacimiento}}" placeholder="Fecha Nacimiento" required>
                        <div class="row" id="fecha_mostrar">
                            <div class="col-md-11">
                                <input class="form-control" type="text" readonly style="cursor: no-drop;" value="{{$data['usuario']->FECHANACIMIENTO}}">
                            </div>
                            <div class="col-md-1">
                                <a class="text-warning" onclick="editar_fecha('#fecha_mostrar','#fecha_nacimiento')"><i class="fa fa-pencil"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="tipo_sangre">Tipo de sangre</label>
                        <select name="formulario[tipo_sangre]" id="tipo_sangre" class="form-control h-auto" required>
                          <option value="">Seleccione una opcion</option>
                          <option value="0" <?php if($data['usuario']->tipo_sangre == 0){ ?> Selected <?php } ?> >A+</option>
                          <option value="1" <?php if($data['usuario']->tipo_sangre == 1){ ?> Selected <?php } ?> >A-</option>
                          <option value="2" <?php if($data['usuario']->tipo_sangre == 2){ ?> Selected <?php } ?> >O+</option>
                          <option value="3" <?php if($data['usuario']->tipo_sangre == 3){ ?> Selected <?php } ?> >O-</option>
                          <option value="4" <?php if($data['usuario']->tipo_sangre == 4){ ?> Selected <?php } ?> >B+</option>
                          <option value="5" <?php if($data['usuario']->tipo_sangre == 5){ ?> Selected <?php } ?> >B-</option>
                          <option value="6" <?php if($data['usuario']->tipo_sangre == 6){ ?> Selected <?php } ?> >AB+</option>
                          <option value="7" <?php if($data['usuario']->tipo_sangre == 7){ ?> Selected <?php } ?> >AB-</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="telefono_personal">Telefono Personal</label>
                        <input class="form-control" id="telefono_personal" type="text" name="formulario[telefono_personal]" placeholder="Telefono Personal" onkeyup="max_campo(this.value,30,'#texto_telefono_personal',this.id)" value="{{$data['usuario']->telefono_personal}}" required>
                        <p class="text-right invisible" id="texto_telefono_personal"><a class="text-success"><i class="fa fa-pencil"></i>... 1/30</a></p>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="texto_telefono_corporativo">Telefono Corporativo</label>
                        <input class="form-control" id="telefono_corporativo" type="text" name="formulario[telefono_corporativo]" placeholder="Telefono Corporativo" onkeyup="max_campo(this.value,30,'#texto_telefono_corporativo',this.id)" value="{{$data['usuario']->telefono_corporativo}}" required>
                        <p class="text-right invisible" id="texto_telefono_corporativo"><a class="text-success"><i class="fa fa-pencil"></i>... 1/30</a></p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="correo_personal">Correo Personal</label>
                        <input class="form-control" id="correo_personal" type="email" name="formulario[correo_personal]" placeholder="Correo Personal" onkeyup="max_campo(this.value,60,'#texto_correo_personal',this.id)" value="{{$data['usuario']->correo_personal}}" required>
                        <p class="text-right invisible" id="texto_correo_personal"><a class="text-success"><i class="fa fa-pencil"></i>... 1/60</a></p>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="correo_corporativo">Correo corporativo</label>
                        <input class="form-control" type="email" id="correo_corporativo" name="formulario[correo_corporativo]" placeholder="Correo Corporativo" onkeyup="max_campo(this.value,60,'#texto_correo_corporativo',this.id)" value="{{$data['usuario']->correo_corporativo}}" required>
                        <p class="text-right invisible" id="texto_correo_corporativo"><a class="text-success"><i class="fa fa-pencil"></i>... 1/60</a></p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="profesion">Profesión</label>
                        <input class="form-control" type="text" id="profesion" name="formulario[profesion]" placeholder="Profesión" value="{{$data['usuario']->profesion}}" onkeyup="max_campo(this.value,50,'#texto_profesion',this.id)" required>
                        <p class="text-right invisible" id="texto_profesion"><a class="text-success"><i class="fa fa-pencil"></i>... 1/50</a></p>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="pais">Pais Nacimiento</label>
                        <div class="row fila-pais" style="display: block;">
                            <div class="col-md-11">
                                <input type="text" id="pais_mostrar" class="form-control pais_mostrar" value="{{$data['usuario']->PAISNACIMIENTO}}" readonly>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-pencil cambiar-pais" aria-hidden="true" title="Cambiar de pais"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control" style="display: none;" id="pais" list="lista_paises" placeholder="Seleccione un pais">
                        <datalist id="lista_paises">
                          @foreach($data['paises'] as $pais)
                          <option value="{{$pais->id}}" label="{{$pais->name}}">
                          @endforeach
                        </datalist>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="departamento">Departamento Nacimiento</label>
                        <div class="row fila-departamento" style="display: block;">
                            <div class="col-md-11">
                                <input type="text" id="departamento_mostrar" class="form-control departamento_mostrar" value="{{$data['usuario']->ESTADONACIMIENTO}}" readonly>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-pencil cambiar-departamento" aria-hidden="true" title="Cambiar de departamento"></i>
                            </div>
                        </div>
                        <input type="text" style="display: none;" class="form-control no-cursor" id="departamento" list="lista_departamentos" placeholder="Seleccione un pais" readonly>
                        <datalist id="lista_departamentos">
                            @foreach($data['estados'] as $departamento)
                            <option value="{{$departamento->id}}" label="{{$departamento->name}}">
                            @endforeach
                        </datalist>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                        <label for="ciudad">Ciudad Nacimiento</label>
                        <div class="row fila-ciudad" style="display: block;">
                            <div class="col-md-11">
                                <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" value="{{$data['usuario']->CIUDADNACIMIENTO}}" readonly>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-pencil cambiar-ciudad" aria-hidden="true" title="Cambiar de ciudad"></i>
                            </div>
                        </div>
                        <input type="text" style="display: none" class="form-control no-cursor" id="ciudad" name="formulario[id_ciudad_nacimiento]" list="lista_ciudades" placeholder="Seleccione un pais" value="{{$data['usuario']->IDCIUDADNACIMIENTO}}">
                        <datalist id="lista_ciudades">
                            @foreach($data['ciudades'] as $ciudad)
                            <option value="{{$ciudad->id}}" label="{{$ciudad->name}}">
                            @endforeach
                        </datalist>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-12 col-lg-12 text-left">
                         <label for="descripcion">Descripcion de perfil</label>
                         <textarea class="form-control" type="text" id="descripcion" name="formulario[descripcion_perfil]" rows="15" onkeyup="max_campo(this.value,2000,'#texto_descripcion',this.id)" required="required">{{$data['usuario']->descripcion_perfil}}</textarea>
                         <p class="text-right invisible" id="texto_descripcion"><a class="text-success"><i class="fa fa-pencil"></i>... 1/2000</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-3">
                <div class="row mt-4">
                    <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                        <h3>Información Ubicación</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                <label for="pais2">Pais Ubicación</label>
                <div class="row fila-pais2" style="display: block;">
                    <div class="col-md-11">
                        <input type="text" id="pais_mostrar2" class="form-control pais_mostrar2" value="{{$data['usuario']->PAISUBICACION}}" readonly>
                    </div>
                    <div class="col-md-1">
                        <i class="fa fa-pencil cambiar-pais2" aria-hidden="true" title="Cambiar de pais"></i>
                    </div>
                </div>
                <input type="text" class="form-control" id="pais2" list="lista_paises2" placeholder="Seleccione un pais" style="display: none;">
                <datalist id="lista_paises2">
                  @foreach($data['paises'] as $pais)
                  <option value="{{$pais->id}}" label="{{$pais->name}}">
                  @endforeach
                </datalist>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                <label for="departamento2">Departamento Ubicación</label>
                <div class="row fila-departamento2" style="display: block;">
                    <div class="col-md-11">
                        <input type="text" id="departamento_mostrar2" class="form-control departamento_mostrar2" value="{{$data['usuario']->ESTADOUBICACION}}" readonly>
                    </div>
                    <div class="col-md-1">
                        <i class="fa fa-pencil cambiar-departamento2" aria-hidden="true" title="Cambiar de departamento"></i>
                    </div>
                </div>
                <input type="text" class="form-control no-cursor" id="departamento2" list="lista_departamentos2" placeholder="Seleccione un pais" readonly style="display: none;">
                <datalist id="lista_departamentos2">
                    @foreach($data['estados2'] as $departamento)
                    <option value="{{$departamento->id}}" label="{{$departamento->name}}">
                    @endforeach
                </datalist>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                <label for="ciudad2">Ciudad Ubicación</label>
                <div class="row fila-ciudad2" style="display: block;">
                    <div class="col-md-11">
                        <input type="text" id="ciudad_mostrar2" class="form-control ciudad_mostrar2" value="{{$data['usuario']->CIUDADUBICACION}}" readonly>
                    </div>
                    <div class="col-md-1">
                        <i class="fa fa-pencil cambiar-ciudad2" aria-hidden="true" title="Cambiar de ciudad"></i>
                    </div>
                </div>
                <input type="text" class="form-control no-cursor" id="ciudad2" name="formulario[id_ciudad_ubicacion]" list="lista_ciudades2" placeholder="Seleccione un pais" value="{{$data['usuario']->id_ciudad_ubicacion}}" style="display: none;">
                <datalist id="lista_ciudades2">
                    @foreach($data['ciudades2'] as $ciudad)
                    <option value="{{$ciudad->id}}" label="{{$ciudad->name}}">
                    @endforeach
                </datalist>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                <label for="direcion">Dirección de Residencia</label>
                <input class="form-control" type="text" id="direcion" name="formulario[direccion_ubicacion]" value="{{$data['usuario']->direccion_ubicacion}}" placeholder="Dirección de Residencia" onkeyup="max_campo(this.value,100,'#texto_direcion',this.id)" required>
                <p class="text-right invisible" id="texto_direcion"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div id="map"></div>
                <div class="coordinates">
                    <em class="lat">Latitud</em>
                    <em class="lon">Longitud</em>
                    <input type="text" id="lat" name="lat">
                    <input type="text" id="lng" name="lng">
                </div>
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-3">
                <div class="row mt-4">
                    <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                        <h3>Estudios Realizados</h3>
                    </div>
                </div>
                <div class="row mt-2 add-study">
                    <div class="col-md-12 text-center">
                        <a class="text-info" style="font-size: 4rem; cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Agregar Estudios" onclick="AgregarEstudio()"><i class="fa fa-plus-square"></i></a>
                    </div>
                </div>
                <?=$data['html_estudios']?>
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-sm-12 col-md-12 col-lg-12 mt-3 mb-3">
                <div class="row mt-4">
                    <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                        <h3>Vinculación a ESSI</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                <label for="fecha_vinculacion">Fecha vinculación a ESSI</label>
                <input class="form-control fecha-vinculacion" style="display: none;" type="text" id="fecha_vinculacion" name="formulario[fecha_vinculacion_essi]" onchange="fecha_seleccionada(this.id,'#fecha_mostrar2',this.value)" value="{{$data['usuario']->fecha_vinculacion_essi}}" required>
                <div class="row" id="fecha_mostrar2">
                    <div class="col-md-11">
                        <input class="form-control" type="text" readonly style="cursor: no-drop;" value="{{$data['usuario']->FECHAVINCULACION}}">
                    </div>
                    <div class="col-md-1">
                        <a class="text-warning" onclick="editar_fecha('#fecha_mostrar2','#fecha_vinculacion')"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                <label for="cargo">Cargo</label>
                <input type="text" class="form-control no-cursor" value="Freelance" readonly>
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-md-12 mt-4 text-left text-white bg-ESSI shadow-2">
                <h3>País Principal</h3>
            </div>
        </div>
        <div class="row bg-white mt-2 mb-5">
            <div class="col-sm-12 col-md-12 col-lg-12 mt-3 img-pais-principal">

            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 text-left mt-3">
                <label for="principal">Seleccione un pais principal</label>
                <select name="formulario[id_pais_principal]" id="principal" class="form-control h-auto">
                  <option>Seleccione una opción</option>
                  @foreach($data['paises'] as $pais)
                  @if($pais->imagen!="")
                  <option value="{{$pais->id}}" <?php if($pais->id == $data['usuario']->id_pais_principal){ ?>Selected<?php } ?> >{{$pais->name}}</option>
                  @endif
                  @endforeach
                </select>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 mt-3 img-paises-secundarios">

            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 text-left mt-3">
                <label for="secundarios">Seleccione paises secundarios</label><br>
                <label class="col-form-label">Nota: Para seleccionar varios paises secundarios debe oprimir la tecla Ctrl y hacer click en el pais a adicionar</label>
                <select type="text" class="form-control" id="secundarios" name="paises_secundarios[]" multiple="multiple" role="multiselect">
                  @foreach($data['paises'] as $pais)
                    @if($pais->imagen!="")
                  <option value="{{$pais->id}}" data-icon="glyphicon-picture" <?php foreach($data['paisesSecundarios'] as $secundario){ if($secundario->id_pais==$pais->id){ echo 'Selected'; } } ?> >{{$pais->name}}</option>
                    @endif
                  @endforeach
                </select>
            </div>
        </div>
        <div class="row flotante">
            <div class="col-md-12">
                <a class="snip1489" data-toggle="tooltip" data-placement="top" title="Guardar Uusario" onclick="guardar()"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
            </div>
        </div>
    </form>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="/js/html5imageupload.js?v1.4.3"></script>
<script src="/js/plugins/moment-with-locales.js"></script>
<script src="/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="{{ url('/') }}/assets/js/tinyColorPicker-master/colors.js"></script>
<script src="{{ url('/') }}/assets/js/tinyColorPicker-master/jqColorPicker.min.js"></script>
<script src="/js/parsley.min.js"></script>
<script>
    var marker;
    var map;
    var geocoder;
    var infoWindow = {};
    m=1;
    var pais = "";
    var departamento = "";
    var ciudad = "";
    var direccion = "";
    $( document ).ready(function() {
       paises_secundarios();
       setTimeout(function(){ ciudad2(); }, 3000);
        $('[data-toggle="tooltip"]').tooltip();
       $('.dropzone').html5imageupload();
       $('.fecha-nacimiento').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD',
          maxDate : moment().subtract(18, 'years').format('YYYY-MM-DD'),
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
        $('.fecha-vinculacion').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD',
          maxDate : new Date(),
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
    });

    /**
     * Función para guardar el usuario nuevo
     */
    function guardar(){
        event.preventDefault();
            if(true === $("#formulario").parsley().validate()){
                swal({
                    title: 'Esta seguro?',
                    text: "Va a editar el Funcionario {{$data['usuario']->NOMBRECORTO}}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                    }).then(function () {
                        var jqxhr = $.post("/freelance/crear-accion", $("#formulario").serialize())
                        .done(function(e) {
                            if(e.data['msj'] == "Guardado correctamente!"){
                                swal({
                                    title: 'Guardado',
                                    text: "Guardado correctamente",
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'OK'
                                }).then(function () {
                                    location.href = '/freelance/listado';
                                });
                            }else{
                                swal(
                                      'Error!',
                                      e.data['msj'],
                                      'error'
                                    );
                            }
                        })
                        .fail(function(e) {
                            console.error(e)
                        })
                        .always(function(e) {
                          console.log(e)
                        });
                    });
            }else{
                swal('Faltan Campos por llenar');
            }
    }
    /**
     * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
     * @param STRING texto  TEXTO INGRESADO EN EL CAMPO
     * @param INT max_long NUMERO MAXIMO DE CARACTERES PARA EL CAMPO
     * @param STRING id  IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
     * @param STRING id_campo  IDENTIFICACION DEL CAMPO EDITADO
     */
    function max_campo(texto,max_long,id,id_campo){
        numeroCaracteres = texto.length;
        $(id).removeClass('invisible');
        if(numeroCaracteres > max_long){
            $('#'+id_campo).val(texto.substr(0,max_long));
            $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
        }else{
            $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
        }
    }

    /**
     * Función para mostrar la fecha en formato dia/Nombre del mes/año
     * @param STRING id_campo   IDENTIFICACION DEL CAMPO DE FECHA
     * @param STRING id_mostrar IDENTIFICACIÓN DEL CAMPO DONDE SE MUESTRA
     * @param DATE valor FECHA SELECCIONADA
     */
    function fecha_seleccionada(id_campo,id_mostrar,valor){
        var fecha = valor.split("-");
        var meses = ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        var fecha_formato = fecha[2]+'/'+meses[parseInt(fecha[1])]+'/'+fecha[0];
        $(id_mostrar+' input').val(fecha_formato);
        $(id_mostrar).css('display','block');
        $('#'+id_campo).css('display','none');
    }

    /**
     * Funcion para editar la fecha
     * @param STRING id_esconder IDENTIFICACION DEL CAMPO QUE ESTA MOSTRANDO LA FECHA CON FORMATO DD/MES/YYYY
     * @param STRING id_mostrar  IDENTIFICACION DEL CAMPO QUE CAPTURA LA FECHA
     */
    function editar_fecha(id_esconder, id_mostrar){
        $(id_mostrar).css('display','block');
        $(id_mostrar).val('');
        $(id_esconder).css('display','none');
    }

   @for($i=1; $i<=2; $i++)
    <?php if($i == 2){ $m = $i; }else{ $m = ""; } ?>
    /*Pais*/
    $('body').on('change','#pais{{$m}}',function(){
       valor=$(this) .val();
        valor=parseInt(valor);
        if(isNaN(valor)){
            swal(
              'Oops...',
              'No eligio pais!',
              'warning'
            );
            $('#pais{{$m}}').val('');
            $('#departamento{{$m}}').val('');
            $('#ciudad{{$m}}').val('');

        }else{
            $.ajax({
                url: "{{url('/')}}/consultarpais/"+valor,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    $('#pais_mostrar{{$m}}').val(e.pais.name);
                    $('.fila-pais{{$m}}').css('display','block');
                    $('#pais{{$m}}').css('display','none');
                    $('#lista_departamentos{{$m}}').html(e.html);
                    $('#departamento{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
                    $('#ciudad{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
                    $('#departamento{{$m}}').removeClass('no-cursor');
                    $('#departamento{{$m}}').removeAttr("readonly");
                    @if($m==2) pais2(); @endif
                }
            });
        }
    });
    $('body').on('click','.cambiar-pais{{$m}}',function(){
        $('.fila-pais{{$m}}').css('display','none');
        $('#pais{{$m}}').val('');
        $('#pais_mostrar{{$m}}').val('');
        $('#pais{{$m}}').css('display','block');
        $('.fila-departamento{{$m}}').css('display','none');
        $('#departamento{{$m}}').css('display','block');
        $('#lista_departamentos{{$m}}').html('');
        $('#departamento{{$m}}').val('');
        $('#departamento_mostrar{{$m}}').val('');
        $('#departamento{{$m}}').addClass('no-cursor');
        $('#departamento{{$m}}').attr("readonly","readonly");
        $('.fila-ciudad{{$m}}').css('display','none');
        $('#ciudad{{$m}}').css('display','block');
        $('#lista_ciudades{{$m}}').html('');
        $('#ciudad{{$m}}').val('');
        $('#ciudad_mostrar{{$m}}').val('');
        $('#ciudad{{$m}}').addClass('no-cursor');
        $('#ciudad{{$m}}').attr("readonly","readonly");

        $('#departamento{{$m}}').attr('placeholder','Seleccione un pais');
        $('#ciudad{{$m}}').attr('placeholder','Seleccione un pais');

    });

    /*Departamento*/
    $('body').on('change','#departamento{{$m}}',function(){
       valor=$(this).val();
        valor=parseInt(valor);
        if(isNaN(valor)){
            swal(
              'Oops...',
              'No eligio Departamento!',
              'warning'
            );
            $('#departamento{{$m}}').val('');
            $('#ciudad{{$m}}').val('');
            @if($m==2) departamento2(); @endif
        }else{
            $.ajax({
                url: "{{url('/')}}/consultardepartamento/"+valor,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    $('#departamento_mostrar{{$m}}').val(e.departamento.name);
                    $('.fila-departamento{{$m}}').css('display','block');
                    $('#departamento{{$m}}').css('display','none');
                    $('#lista_ciudades{{$m}}').html(e.html);
                    $('#ciudad{{$m}}').attr('placeholder','Seleccione una ciudad (municipio)');
                    $('#ciudad{{$m}}').removeClass('no-cursor');
                    $('#ciudad{{$m}}').removeAttr("readonly");
                    @if($m==2) departamento2(); @endif
                }
            });
        }
    });
    $('body').on('click','.cambiar-departamento{{$m}}',function(){
        $('.fila-departamento{{$m}}').css('display','none');
        $('#departamento{{$m}}').val('');
        $('#departamento_mostrar{{$m}}').val('');
        $('#departamento{{$m}}').css('display','block');
        $('#departamento{{$m}}').removeAttr("readonly");
        $('#departamento{{$m}}').removeClass('no-cursor');
        $('.fila-ciudad{{$m}}').css('display','none');
        $('#ciudad{{$m}}').css('display','block');
        $('#lista_ciudades{{$m}}').html('');
        $('#ciudad{{$m}}').val('');
        $('#ciudad_mostrar{{$m}}').val('');
        $('#ciudad{{$m}}').addClass('no-cursor');
        $('#ciudad{{$m}}').attr("readonly","readonly");

        $('#departamento{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
        $('#ciudad{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
        @if($m==2) departamento2(); @endif
    });

    /*ciudad*/
    $('body').on('change','#ciudad{{$m}}',function(){
       valor=$(this).val();
        valor=parseInt(valor);
        if(isNaN(valor)){
            swal(
              'Oops...',
              'No eligio Ciudad!',
              'warning'
            );
            $('#ciudad{{$m}}').val('');
            @if($m==2) ciudad2(); @endif
        }else{
            $.ajax({
                url: "{{url('/')}}/consultarciudad/"+valor,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    $('#ciudad_mostrar{{$m}}').val(e.ciudad.name);
                    $('.fila-ciudad{{$m}}').css('display','block');
                    $('#ciudad{{$m}}').css('display','none');
                    @if($m==2) ciudad2(); @endif
                }
            });
        }
    });
    $('body').on('click','.cambiar-ciudad{{$m}}',function(){
        $('.fila-ciudad{{$m}}').css('display','none');
        $('#ciudad{{$m}}').val('');
        $('#ciudad_mostrar{{$m}}').val('');
        $('#ciudad{{$m}}').css('display','block');
        $('#ciudad{{$m}}').removeClass('no-cursor');
        $('#ciudad{{$m}}').removeAttr("readonly");

        $('#ciudad{{$m}}').attr('placeholder','Seleccione una ciudad (municipio)');
        @if($m==2) ciudad2(); @endif
    });
    @endfor

    /*MAPAS */

    $('#direccion').change(function () {
        direccion = "";
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        console.log(dir);
        codeAddress(dir);
      });

      function pais2() {
        pais = $('#pais_mostrar2').val();
        dir="";
        ciudad = $("#ciudad_mostrar2").val();
        departamento = $("#departamento_mostrar2").val();
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        codeAddress(dir);
      }

      function departamento2() {
        departamento = $('#departamento_mostrar2').val();
        dir="";
        pais = $("#pais_mostrar2").val();
        ciudad = $("#ciudad_mostrar2").val();
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      }

      function ciudad2() {
        ciudad = $('#ciudad_mostrar2').val();
        dir="";
        pais = $("#pais_mostrar2").val();
        departamento = $("#departamento_mostrar2").val();
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      }

    funcambiarinput = function (id, val) {
      valor = $("#"+id).val();
      if (valor==="Celular") {
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-10').addClass('col-9');
      }else if(valor==="Telefono"){
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).show();
          $("#codtelext"+val).show();
          $("#codtelnum"+val).removeClass('col-9 col-10').addClass('col-7');
      }else if(valor==="Radio"){
          $("#codtelindp"+val).hide();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-9').addClass('col-10');
      }
    }

    function setMapOnAll(map) {
      for (var i = 0; i < marker.length; i++) {
        marker[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.

    function clearMarkers() {
      setMapOnAll(null);
    }

    function initMap() {
      geocoder = new google.maps.Geocoder();
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 59.325, lng: 18.070}
      });

      infoWindow = new google.maps.InfoWindow({map: map});

      funGeolocation();
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    }

    function attachSecretMessage() {
        $("#lat").val(marker.position.lat);
        $("#lng").val(marker.position.lng);
    }

    function funGeolocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $("#lat").val(pos.lat);
          $("#lng").val(pos.lng);
          marker.setMap(map);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: pos
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function codeAddress(dir) {
      if (marker) {
        marker.setMap(null);
      }

      var address = dir;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);

          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: results[0].geometry.location
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          $("#lat").val(results[0].geometry.location.lat);
          $("#lng").val(results[0].geometry.location.lng);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

/*FIN MAPAS*/

    $(document).on('change','#principal',function(){
        pais = $(this).val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/freelance/crear-accion",
            data: {
                _token: CSRF_TOKEN,
                pais: pais,
                action: 0
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('.img-pais-principal').html(e.data['cuerpo']);
            }
        });
    });
<?php
if($data['usuario']->id_pais_principal > 0){ ?>
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/freelance/crear-accion",
        data: {
            _token: CSRF_TOKEN,
            pais: <?=$data['usuario']->id_pais_principal?>,
            action: 0
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.img-pais-principal').html(e.data['cuerpo']);
        }
    });
<?php
    }
?>
    $(document).on('click','#secundarios',function(){
        paises_secundarios();
    });

    /**
     * Función para listar los paises secundarios
     */
    function paises_secundarios(){
        paises = $('#secundarios').val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/crear-accion",
                data: {
                    _token: CSRF_TOKEN,
                    paises: paises,
                    action: 1
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.img-paises-secundarios').html(e.data['cuerpo']);
                }
        });
    }

    /**
     * Funcion para agregar un nuevo item de estudio
     */
    function AgregarEstudio(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/crear-accion",
                data: {
                    _token: CSRF_TOKEN,
                    estudios: $('.info-titulo').length,
                    action: 3
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.add-study').after(e.data['cuerpo']);
                    $('[data-toggle="tooltip"]').tooltip();
                }
        });
    }

    /**
     * Funcion para eliminar fila de estudio
     * @param INT id NUMERO DE FILA DE ESTUDIO A ELIMINAR
     */
    function EliminarEstudio(id){
        $('#fila'+id).remove();

        setTimeout(function(){ $('.tooltip.show').remove(); }, 500);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbw8hYpmFO_VWUZLufrAL1qfnPFQp4JaM&callback=initMap"></script>
@endsection
