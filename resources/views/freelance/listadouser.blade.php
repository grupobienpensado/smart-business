@extends('template.app')
@section('title', 'Listado Freelance')
@section('content')
<link href="/css/html5imageupload.css?v1.3" rel="stylesheet">
   <style>
    .card{box-shadow:0 8px 20px rgba(0,0,0,0.19),0 6px 6px rgba(0,0,0,0.23);border: none;}
    .card:hover{box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22);transition:all .3s cubic-bezier(.25,.8,.25,1)}
    .card .avatar{position:relative;top:-90px;margin-bottom:-70px}
    .card .avatar img{width:15rem;-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;-webkit-transition:-webkit-box-shadow .3s ease;transition:box-shadow .3s ease;-webkit-box-shadow:0 0 0 8px rgba(0,0,0,0.06);box-shadow:0 0 0 8px rgba(0,0,0,0.06)}
    .img-hover{-webkit-box-shadow:0 0 0 12px rgba(0,0,0,0.1)!important;box-shadow:0 0 0 12px rgba(0,0,0,0.1)!important}
    .card .cardheader{height:135px}
    .dropdown-toggle::after{display:none}
    .open > .dropdown-menu{-webkit-transform:scale(1,1);transform:scale(1,1);opacity:1}
    .freelancer-container{background-color:#FFF;width:100%;height:100%;border-radius:10px}
    .modal-content{width:538px;margin-top:20%}
    .comp-frm-header{min-width:536px!important;max-width:536px!important;min-height:135px!important;max-height:135px!important;z-index:0}
    .comp-frm-avatar{min-width:200px!important;max-width:200px!important;min-height:200px!important;max-height:200px!important;border-radius:50%;top:-65px;z-index:1;-webkit-box-shadow:0 0 0 8px rgba(0,0,0,0.06);box-shadow:0 0 0 8px rgba(0,0,0,0.06)}
    .comp-frm-avatar:after{content:"Haga click para añadir un logo";position:relative;font-size:20px;color:#868686;text-align:center;top:30%}
    .comp-frm-header:after{content:"Haga click para añadir un banner";position:relative;font-size:20px;color:#868686;text-align:center}
    canvas#canvas_c_banner{height:auto!important}
    .essi-color-text{color:#051d60 !important; }
    ul.social-network{list-style:none;display:inline;margin-left:0!important;padding:0}
    ul.social-network li{display:inline;margin:0 5px}
    .social-network a.red:hover{background-color:#051d60}
    .social-network a.red:hover i{color:#fff}
    a.socialIcon:hover,.socialHoverClass{color:#44BCDD}
    .social-circle li a{display:inline-block;position:relative;margin:0 auto;-moz-border-radius:50%;-webkit-border-radius:50%;border-radius:50%;text-align:center;width:40px;height:40px;font-size:20px}
    .social-circle li i{margin:0;line-height:40px;text-align:center}
    .social-circle li a:hover i,.triggeredHover{-moz-transform:rotate(360deg);-webkit-transform:rotate(360deg);-ms--transform:rotate(360deg);transform:rotate(360deg);-webkit-transition:all .2s;-moz-transition:all .2s;-o-transition:all .2s;-ms-transition:all .2s;transition:all .2s}
    .social-circle i{color:#fff;-webkit-transition:all .8s;-moz-transition:all .8s;-o-transition:all .8s;-ms-transition:all .8s;transition:all .8s}
    ul.social-network.social-circle li a{background-color:#D3D3D3}
    .comp-bottom-separator{border-color: #051d60;border-width: 5px;}
    .container h4{text-transform: capitalize;}
    #sweet_tipo_freelance{ display: none;}
    .imagen-redonda{border-radius: 200px 200px 200px 200px; -moz-border-radius: 200px 200px 200px 200px; -webkit-border-radius: 200px 200px 200px 200px; border: 0px solid #000000;}
    .logo-empresa{ height: 200px; width: 200px; max-height: 200px; min-height: 200px; }
    .logo-categoria{ height: 200px; width: 200px; max-height: 200px; min-height: 200px; }
    .b-b{border-bottom: 1px solid #00000045; }
    .list-tipos .row:nth-child(odd) { background: #E6E6E6; }
    .list-categorias .row:nth-child(odd) { background: #E6E6E6; }
</style>
{{ csrf_field() }}
<div class="container">
    <div class="jumbotron">
        <div class="panel-title">
            <div class="pull-right">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" onclick="anadirusuario()" class="btn btn-success" data-action="1" data-toggle="tooltip" data-placement="top" title="Añadir Usuarios"><i class="fa fa-plus"></i> Añadir usuario</button>
                    <button type="button" onclick="tipofreelance()" class="btn btn-info" data-action="1" data-toggle="tooltip" data-placement="top" title="Administrar tipos de freelance"><i class="fa fa-plus"></i> Tipos freelance</button>
                    <button type="button" onclick="categoria()" class="btn btn-info" data-action="1" data-toggle="tooltip" data-placement="top" title="Administrar Categorias"><i class="fa fa-plus"></i> Categorias</button>
                </div>
            </div>
            <h2>Freelance</h2>
            <p class="text-muted text-capitalize">Listado de usuarios freelance</p>
        </div>
    </div>
    <div class="freelancer-container">
        <div class="row" id="listado">

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/js/html5imageupload.js?v1.4.3"></script>
<script>
    /*Variables temporales que se usan en la función acciontipo()->case 3*/
    var temporal_nombre_tipo = '';
    var temporal_descripcion_tipo = '';
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        lista();
    });
    $(document).on('mouseenter', '.card', function() {
        $(this).find('.img-avatar-card').addClass('img-hover');
    });
    $(document).on('mouseleave', '.card', function() {
        $(this).find('.img-avatar-card').removeClass('img-hover');
    });
    /**
     * Funcion para redireccionar a añadir un usuario de freelance
     */
    function anadirusuario(){
        location.href = "/freelance/crear";
    }

    /**
     * Función para listar los usuarios de Freelance
     */
    function lista(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/listar-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 0
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#listado').html(e.data['html_listado']);
                }
        });
    }

    /**
     * Función  para eliminar un usuario FREELANCE
     * @param INT id IDENTIFICACIÓN DEL USUARIO
     */
    function eliminar(id){
        swal({
          title: 'Esta seguro?',
          text: "Desea eliminar el funcionario",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!'
        }).then(function(){
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                    url: "/freelance/listar-accion",
                    data: {
                        _token: CSRF_TOKEN,
                        action: 1,
                        id: id
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj'] == 'Eliminado correctamente!'){
                           swal(
                              'Eliminado!',
                               e.data['msj'],
                              'success'
                            ).then(function(){
                               lista();
                           });
                        }else{
                             swal(
                              'Error!',
                               e.data['msj'],
                              'error'
                            );
                        }
                    }
            });
        });
    }

    /**
     * Función para traer el listado de tipo de Freelance
     */
    function tipofreelance(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/listar-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 2
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    swal({
                          title: 'Tipo Freelance',
                          html:e.data['html_tipofreelance'],
                          showCloseButton: true,
                          showCancelButton: false,
                          focusConfirm: false,
                          confirmButtonText:'Cerrar',
                          confirmButtonColor: '#d33',
                          allowOutsideClick: false
                        });
                    $('.swal2-modal').css('width','80%');
                }
        });
    }

    /**
     * Función para realizar las tres acciones (Crear, Editar, Eliminar)
     * @param INT accion NUMERO QUE DETERMINA QUE ACCION REALIZAR
     * @param INT id     IDENTIFICACIÓN DEL TIPO DE FREELANCE
     */
    function acciontipo(accion,id){
        switch(accion){
            /*Crear Tipo*/
            case 0:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 3
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal({
                                  title: 'Crear Tipo Freelance',
                                  html:e.data['html_creartipo'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'Guardar',
                                  confirmButtonColor: '#088A08',
                                  allowOutsideClick: false
                                }).then(function(){
                                    var jqxhr = $.post("/freelance/listar-accion", $("#formulario").serialize())
                                    .done(function(e) {
                                        if(e.data['msj'] == "Guardado correctamente!"){
                                            swal({
                                                title: 'Guardado',
                                                text: "Guardado correctamente",
                                                type: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                confirmButtonText: 'OK'
                                            }).then(function () {
                                                tipofreelance();
                                            });
                                        }else{
                                            swal(
                                                  'Error!',
                                                  e.data['msj'],
                                                  'error'
                                                ).then(function () {
                                                    tipofreelance();
                                                });
                                        }
                                    })
                                    .fail(function(e) {
                                        console.error(e)
                                    })
                                    .always(function(e) {
                                      console.log(e)
                                    });
                            });
                            $('.swal2-modal').css('width','50%');
                            llenarformulariocreartipo();
                        }
                });
                break;
            /*Editar*/
            case 1:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 11,
                            id: id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            console.log(e.data['html_editartipo']);
                            swal({
                                  title: 'Editar Tipo Freelance',
                                  html:e.data['html_editartipo'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'Guardar',
                                  confirmButtonColor: '#088A08',
                                  allowOutsideClick: false
                                }).then(function(){
                                    var jqxhr = $.post("/freelance/listar-accion", $("#formulario").serialize())
                                    .done(function(e) {
                                        if(e.data['msj'] == "Guardado correctamente!"){
                                            swal({
                                                title: 'Guardado',
                                                text: "Guardado correctamente",
                                                type: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                confirmButtonText: 'OK'
                                            }).then(function () {
                                                tipofreelance();
                                            });
                                        }else{
                                            swal(
                                                  'Error!',
                                                  e.data['msj'],
                                                  'error'
                                                ).then(function () {
                                                    tipofreelance();
                                                });
                                        }
                                    })
                                    .fail(function(e) {
                                        console.error(e)
                                    })
                                    .always(function(e) {
                                      console.log(e)
                                    });
                            });
                            $('.swal2-modal').css('width','50%');
                            llenarformulariocreartipo();
                        }
                });
                break;
            /*Eliminar*/
            case 2:
                swal({
                  title: 'Esta seguro?',
                  text: "Desea eliminar el tipo de freelance",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si, Eliminar!'
                }).then(function(){
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                            url: "/freelance/listar-accion",
                            data: {
                                _token: CSRF_TOKEN,
                                action: 13,
                                id: id
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                                if(e.data['msj'] == 'Eliminado correctamente!'){
                                   swal(
                                      'Eliminado!',
                                       e.data['msj'],
                                      'success'
                                    ).then(function(){
                                       tipofreelance();
                                   });
                                }else{
                                     swal(
                                      'Error!',
                                       e.data['msj'],
                                      'error'
                                    ).then(function(){
                                       tipofreelance();
                                   });
                                }
                            }
                    });
                });
                break;
            /*Lista Empresas*/
            case 3:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 4
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            temporal_nombre_tipo = $('#nombre_tipo').val();
                            temporal_descripcion_tipo = $('#descripcion_tipo').val();
                            temporal_empresa_tipo = $('#empresa_tipo').val();
                            swal({
                                  title: 'Empresas Freelance',
                                  html:e.data['html_listaempresa'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'<i class="fa fa-arrow-left"></i> Atras!',
                                  confirmButtonColor: '#d33',
                                  allowOutsideClick: false
                                }).then(function(){
                                    acciontipo(0);
                                });
                            $('.swal2-modal').css('width','50%');
                        }
                });
                break;
            /*Crear Empresa*/
            case 4:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 5
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal({
                                  title: 'Empresas Freelance',
                                  html:e.data['html_crearempresa'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'Guardar',
                                  confirmButtonColor: '#088A08',
                                  allowOutsideClick: false
                                }).then(function(){
                                    var jqxhr = $.post("/freelance/listar-accion", $("#formulario").serialize())
                                    .done(function(e) {
                                        if(e.data['msj'] == "Guardado correctamente!"){
                                            swal({
                                                title: 'Guardado',
                                                text: "Guardado correctamente",
                                                type: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                confirmButtonText: 'OK'
                                            }).then(function () {
                                                acciontipo(3);
                                            });
                                        }else{
                                            swal(
                                                  'Error!',
                                                  e.data['msj'],
                                                  'error'
                                                ).then(function () {
                                                    acciontipo(3);
                                                });
                                        }
                                    })
                                    .fail(function(e) {
                                        console.error(e)
                                    })
                                    .always(function(e) {
                                      console.log(e)
                                    });
                            });
                            $('.swal2-modal').css('width','50%');
                            $('.dropzone').html5imageupload();
                        }
                });
                break;
            /*Editar Empresa*/
            case 5:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 7,
                            id: id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal({
                                  title: 'Editar Empresa Freelance',
                                  html:e.data['html_editarempresa'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'Guardar',
                                  confirmButtonColor: '#088A08',
                                  allowOutsideClick: false
                                }).then(function(){
                                    var jqxhr = $.post("/freelance/listar-accion", $("#formulario").serialize())
                                    .done(function(e) {
                                        if(e.data['msj'] == "Guardado correctamente!"){
                                            swal({
                                                title: 'Guardado',
                                                text: "Guardado correctamente",
                                                type: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                confirmButtonText: 'OK'
                                            }).then(function () {
                                                acciontipo(3);
                                            });
                                        }else{
                                            swal(
                                                  'Error!',
                                                  e.data['msj'],
                                                  'error'
                                                ).then(function () {
                                                    acciontipo(3);
                                                });
                                        }
                                    })
                                    .fail(function(e) {
                                        console.error(e)
                                    })
                                    .always(function(e) {
                                      console.log(e)
                                    });
                            });
                            $('.swal2-modal').css('width','50%');
                            $('.dropzone').html5imageupload();
                        }
                });
                break;
            /*Eliminar Empresa*/
            case 6:
                swal({
                  title: 'Esta seguro?',
                  text: "Desea eliminar la empresa",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si, Eliminar!'
                }).then(function(){
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                            url: "/freelance/listar-accion",
                            data: {
                                _token: CSRF_TOKEN,
                                action: 9,
                                id: id
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                                if(e.data['msj'] == 'Eliminado correctamente!'){
                                   swal(
                                      'Eliminado!',
                                       e.data['msj'],
                                      'success'
                                    ).then(function(){
                                       acciontipo(3);
                                   });
                                }else{
                                     swal(
                                      'Error!',
                                       e.data['msj'],
                                      'error'
                                    ).then(function(){
                                       acciontipo(3);
                                   });
                                }
                            }
                    });
                });
                break;
               }
    }

    /**
     * Función para volver a llenar el formulario de crear
     */
    function llenarformulariocreartipo(){
        $('#nombre_tipo').val(temporal_nombre_tipo);
        $('#descripcion_tipo').val(temporal_descripcion_tipo);
        temporal_nombre_tipo = '';
        temporal_descripcion_tipo = '';
    }

    /**
     * Funcion para esconder y mostrar el listado de empresas de freelance
     * @param INT valor VALOR DEL SELECT EMPRESA SI ES 0 = "si" 1 = "no"
     */
    function empresatipo(valor){
        if(valor==0){
            $('.id_empresa_tipo').css('display','block');
        }else{
            $('.id_empresa_tipo').css('display','none');
        }
    }

    /**
     * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
     * @param STRING texto  TEXTO INGRESADO EN EL CAMPO
     * @param INT max_long NUMERO MAXIMO DE CARACTERES PARA EL CAMPO
     * @param STRING id  IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
     * @param STRING id_campo  IDENTIFICACION DEL CAMPO EDITADO
     */
    function max_campo(texto,max_long,id,id_campo){
        numeroCaracteres = texto.length;
        $(id).removeClass('invisible');
        if(numeroCaracteres > max_long){
            $('#'+id_campo).val(texto.substr(0,max_long));
            $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
        }else{
            $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
        }
    }

    /**
     * Función para modificar el tipo de freelance
     * @param INT id IDENTIFICACIÓN DEL FREELANCE
     * @param STRING nombre NOMBRE DEL FREELANCE
     */
    function tipo_freelance(id, nombre){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/listar-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 14,
                    id: id,
                    nombre:nombre
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    swal({
                          title: 'Seleccione el Tipo Freelance ',
                          html:e.data['html'],
                          showCloseButton: true,
                          showCancelButton: false,
                          focusConfirm: false,
                          confirmButtonText:'Guardar',
                          confirmButtonColor: '#088A08',
                          allowOutsideClick: false
                        }).then(function(){
                        var jqxhr = $.ajax({
                                url: "/freelance/listar-accion",
                                data: {
                                    _token: CSRF_TOKEN,
                                    action: 15,
                                    id:id,
                                    tipo: $('#tipo_freelance_modificar').val()
                                },
                                cache: false,
                                type: 'POST',
                                success: function(e){
                                    swal(
                                          'Guardado!',
                                          e.data['msj'],
                                          'success'
                                        ).then(function(){
                                        lista();
                                    });
                                }
                        });
                    });
                }
        });
    }

    /**
     * Función para traer las categorias
     */
    function categoria(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/listar-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 16
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    swal({
                          title: 'Listado de categorias',
                          html:e.data['html'],
                          showCloseButton: true,
                          showCancelButton: false,
                          focusConfirm: false,
                          confirmButtonText:'Salir',
                          confirmButtonColor: 'red',
                          allowOutsideClick: false
                        });
                        $('.swal2-modal').css('width','80%');
                }
        });
    }

    /**
     * Acciones a realizar para las categorias de freelance
     * @param INT accion ACCIÓN A REALIZAR (1=CREAR, 3=EDITAR, 4=ELIMINAR)
     *@param INT id IDENTIFICACION DE LA CATEGORIA
     */
    function accioncategoria(accion,id){
        switch(accion){
            /*Formulario para Crear una categoria*/
            case 0:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 17
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal({
                                  title: 'Crear categoria',
                                  html:e.data['html'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'Guardar',
                                  confirmButtonColor: '#088A08',
                                  allowOutsideClick: false
                                }).then(function(){
                                accioncategoria(1);
                            });
                                $('.swal2-modal').css('width','80%');
                                $('.dropzone').html5imageupload();
                        }
                });
                break;
            /*Guardar una categoria*/
            case 1:
                var jqxhr = $.post("/freelance/listar-accion", $("#formulario").serialize())
                .done(function(e) {
                    if(e.data['msj'] == "Guardado correctamente!"){
                        swal({
                            title: 'Guardado',
                            text: "Guardado correctamente",
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        }).then(function () {
                            categoria();
                        });
                    }else{
                        swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            ).then(function () {
                                categoria();
                            });
                    }
                })
                .fail(function(e) {
                    console.error(e)
                })
                .always(function(e) {
                  console.log(e)
                });
                break;
            /*Formulario de editar categoria*/
            case 2:
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/freelance/listar-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            action: 19,
                            id:id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal({
                                  title: 'Editar categoria',
                                  html:e.data['html'],
                                  showCloseButton: true,
                                  showCancelButton: false,
                                  focusConfirm: false,
                                  confirmButtonText:'Guardar',
                                  confirmButtonColor: '#088A08',
                                  allowOutsideClick: false
                                }).then(function(){
                                accioncategoria(3);
                            });
                                $('.swal2-modal').css('width','80%');
                                $('.dropzone').html5imageupload();
                        }
                });
                break;
            /*Editar una categoria*/
            case 3:
                var jqxhr = $.post("/freelance/listar-accion", $("#formulario").serialize())
                .done(function(e) {
                    if(e.data['msj'] == "Guardado correctamente!"){
                        swal({
                            title: 'Guardado',
                            text: "Guardado correctamente",
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        }).then(function () {
                            categoria();
                        });
                    }else{
                        swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            ).then(function () {
                                categoria();
                            });
                    }
                })
                .fail(function(e) {
                    console.error(e)
                })
                .always(function(e) {
                  console.log(e)
                });
                break;
            /*Eliminar una categoria*/
            case 4:
                swal({
                  title: 'Esta seguro?',
                  text: "Desea eliminar la categoria",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si, Eliminar!'
                }).then(function(){
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                            url: "/freelance/listar-accion",
                            data: {
                                _token: CSRF_TOKEN,
                                action: 21,
                                id: id
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                                if(e.data['msj'] == 'Eliminado correctamente!'){
                                   swal(
                                      'Eliminado!',
                                       e.data['msj'],
                                      'success'
                                    ).then(function(){
                                       categoria();
                                   });
                                }else{
                                     swal(
                                      'Error!',
                                       e.data['msj'],
                                      'error'
                                    ).then(function(){
                                       categoria();
                                   });
                                }
                            }
                    });
                });
                break;
        }
    }
    /**
     * Función para modificar la categoria de freelance
     * @param INT id IDENTIFICACIÓN DEL FREELANCE
     * @param STRING nombre NOMBRE DEL FREELANCE
     */
    function categoria_freelance(id,nombre){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "/freelance/listar-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 22,
                    id: id,
                    nombre:nombre
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    swal({
                          title: 'Seleccione la categoria Freelance ',
                          html:e.data['html'],
                          showCloseButton: true,
                          showCancelButton: false,
                          focusConfirm: false,
                          confirmButtonText:'Guardar',
                          confirmButtonColor: '#088A08',
                          allowOutsideClick: false
                        }).then(function(){
                        var jqxhr = $.ajax({
                                url: "/freelance/listar-accion",
                                data: {
                                    _token: CSRF_TOKEN,
                                    action: 23,
                                    id:id,
                                    categoria: $('#categoria_freelance_modificar').val()
                                },
                                cache: false,
                                type: 'POST',
                                success: function(e){
                                    swal(
                                          'Guardado!',
                                          e.data['msj'],
                                          'success'
                                        ).then(function(){
                                        lista();
                                    });
                                }
                        });
                    });
                }
        });
    }
</script>
@endsection
