@extends('template.app')

@section('title', 'Backlog')

@section('content')
<style type="text/css">
.alert, .badge, .breadcrumb, .card, .card .card-header, .dropdown-menu, .file-custom, .input-group-addon, .jumbotron, .list-group .list-group-item, .nav .nav-link, .nav-tabs, .navbar, .navbar-toggler, .page-item:first-child .page-link, .page-item:last-child .page-link, .pagination-lg .page-item:first-child .page-link, .pagination-lg .page-item:last-child .page-link, .pagination-sm .page-item:first-child .page-link, .pagination-sm .page-item:last-child .page-link, .popover, .tooltip-inner, img {
    border: solid 0px rgba(0, 0, 0, 0.09) !important;
}
.comentarios{
	padding: 10px;
    margin: 10px;
    border: 0.5px solid #ccc;
    border-radius: 5px;
    font-size: 12px;
    box-shadow: 1px 1px 1px;
    text-align: justify;
}
.comentarios-danger{
	padding: 10px;
    margin: 10px;
    border: 0.5px solid #e6a4a4;
    border-radius: 5px;
    font-size: 12px;
    box-shadow: 3px 3px 3px #e6a4a4;
    text-align: justify;
}
.comentarios-success {
    padding: 10px;
    margin: 10px;
    border: 0.5px solid #62af64;
    border-radius: 5px;
    font-size: 12px;
    box-shadow: 3px 3px 3px #62af64;
    text-align: justify;
}
.usuario{
	display: block;
    color: #011662;
    font-weight: bold;
    font-size: 14px;
}

.usuario span{
	color: #000;
    font-size: 13px;
}
.relativo{
	position: relative !important;
}
.titulo-inicial{
	background-color: #2b2e84;
    color: #fff;
    font-size: 15px;
}
.pop-over{
	display: none;
	position: absolute;
    border: 1px solid #ccc;
    top: 30px;
    width: 250px;
    z-index: 99;
    background-color: #fff;
    border-radius: 10px;
    padding: 3px;
    left: -50px;
}
.tamano-20-p{
	width: 20% !important;
}
.dataTables_wrapper.form-inline.dt-bootstrap4.no-footer .row{
	display: block;
	width: 100%;
}
#calendar{
	margin-top: 50px;
	margin-bottom: 50px;
	max-height: calc(100% - 100px);
}


</style>

<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/js/fullcalendar-scheduler-1.6.2/lib/fullcalendar.min.css" />
<div class="container-fluid animated slideInDown">
  	<div class="row">
	    <div class="col-md-12 panel-view">
	      	<div id="calendar"></div>
	    </div>
  	</div>
</div>

<div class="modal fade" id="listado-actividad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 	<div class="modal-dialog modal-lg" role="document">
	  	<div class="modal-content">
		    <div class="modal-header">
		    	<img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
		      	<h4 class="modal-title" id="myModalLabel-aprobar"></h4>
		      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    </div>
		    <div class="modal-body">
			    <div class="text-center">
			    	<img src="{{ url('/')}}/images/icons/loading.gif" style="display: none" class="load">	
			    </div>
			    <div id="lista">
			    	
			    </div>
		    </div>
	    </div>
  	</div>
</div>
@endsection
 
@section('scripts')
<script type="text/javascript" src="{{url('/')}}/assets/js/fullcalendar-scheduler-1.6.2/lib/moment.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/assets/js/fullcalendar-scheduler-1.6.2/lib/fullcalendar.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/assets/js/fullcalendar-scheduler-1.6.2/lib/locale/es.js"></script>
<script type="text/javascript" charset="utf-8">
$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title'
			},
			defaultView: 'month',
			locale: 'es',
			defaultDate: moment(),
            allDaySlot: false,
			selectHelper: true,
			axisFormat : "HH:mm",
    		agenda : "HH:mm",
			weekNumbers: true,
			weekNumbersWithinDays: true,
			firstDay:0,			
			columnFormat: 'dddd',
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			eventTextColor:"#FFFFFF",
			events: [
				<?php 
				for($mes=1;$mes<=12;$mes++){
					$numero = cal_days_in_month(CAL_GREGORIAN, $mes, date("Y"));
					if($mes<10){
						$mes="0".$mes;
					}
				for($i=1;$i<=$numero;$i++){
					$cancelado=0;
					$finalizado=0;
					$pendiente=0; 
					if($i<10){
						$i="0".$i;
					}
					foreach($datos as $dato){ 
						/*$valida=false;
						foreach ($aplazadas as $key) {
							if($key->actividades_detalles==$dato->id){
								$fecha=$key->fecha_ejecucion;
								$valida=true;
							}
						}
						if($valida){
							if(date("Y-").$mes."-".$i==$fecha){
								if($dato->estado=="cancelado"){
									$cancelado++;
								}
								if($dato->estado=="finalizado"){
									$finalizado++;
								}
								if($dato->estado=="pendiente"){
									$pendiente++;
								}
							}
						}else{*/
							
								if($dato->estado=="cancelado"){
									if(date("Y-").$mes."-".$i==$dato->fecha_pendiente){
										$cancelado++;
									}
								}
								if($dato->estado=="finalizado"){
									if(date("Y-").$mes."-".$i==$dato->fecha_pendiente){
										$finalizado++;
									}
								}
								if($dato->estado=="pendiente"){
									if(date("Y-").$mes."-".$i==$dato->fecha_pendiente){
										$pendiente++;
									}
								}

						//}
					?>
				<?php } ?>
				<?php if($finalizado>0){ ?>
				{
					title: '{{$finalizado}} actividades realizadas',
					start: '<?php echo date("Y-").$mes."-".$i; ?>',
					color: '#0db53f'
				},
				<?php } if($pendiente>0){ ?>
				{
					title: '{{$pendiente}} actividades pendientes',
					start: '<?php echo date("Y-").$mes."-".$i; ?>',
					color: '#0d39b5'
				},
				<?php 
					}
					if($cancelado>0){
				?>
				{
					title: '{{$cancelado}} actividades canceladas',
					start: '<?php echo date("Y-").$mes."-".$i; ?>',
					color: '#de1f22'
				},
				<?php } ?>
				<?php } } ?>
				
			],
		dayClick: function(date, jsEvent, view) {
			$("#listado-actividad").modal()
	        $(".load").show();
	        $("#lista").empty();
	        
	        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
		    var jqxhr = $.ajax({ 
		        url: "{{ url('/') }}/listadodiario/"+date.format(), 
		        cache: false,
		        type: 'GET',
		        success: function(e){ 
		        	$(".load").hide();
		        	$("#myModalLabel-aprobar").html("Actividades para "+e.fecha);
		        	$("#lista").html(e.contenido);
		        }
		    });
	    }
		});

</script>
@endsection