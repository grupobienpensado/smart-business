@extends('template.app')

@section('title', 'Backlog')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">
.comentarios{
	padding: 10px;
    margin: 10px;
    border: 0.5px solid #ccc;
    border-radius: 5px;
    font-size: 12px;
    box-shadow: 1px 1px 1px;
    text-align: justify;
}
.comentarios-danger{
	padding: 10px;
    margin: 10px;
    border: 0.5px solid #e6a4a4;
    border-radius: 5px;
    font-size: 12px;
    box-shadow: 3px 3px 3px #e6a4a4;
    text-align: justify;
}
.comentarios-success {
    padding: 10px;
    margin: 10px;
    border: 0.5px solid #62af64;
    border-radius: 5px;
    font-size: 12px;
    box-shadow: 3px 3px 3px #62af64;
    text-align: justify;
}
.usuario{
	display: block;
    color: #011662;
    font-weight: bold;
    font-size: 14px;
}

.usuario span{
	color: #000;
    font-size: 13px;
}
.relativo{
	position: relative !important;
}
.pop-over{
	display: none;
	position: absolute;
    border: 1px solid #ccc;
    top: 30px;
    width: 250px;
    z-index: 99;
    background-color: #fff;
    border-radius: 10px;
    padding: 3px;
    left: -50px;
}

.modal-dialog {
    height: 90%;
    top: 40px;
}

.tamano-20-p{
	width: 20% !important;
}
.dataTables_wrapper.form-inline.dt-bootstrap4.no-footer .row{
	display: block;
	width: 100%;
}
.pixeles input{
	width: 50px !important;
	max-width:50px !important;
}
.fecha input{
	width: 90px !important;
	max-width:90px !important;
}

select.form-control:not([size]):not([multiple]) {
    height: auto !important;
}

.form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

  .mas-pequeño{
    line-height: 1.5;
    font-size: small;
    color: #636363;
    font-weight: 400;
    display: block !important;
    margin: 0px !important;
  }

  .centrar-vertical{
    vertical-align: middle !important;
  }

  .unalinea{
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  p:first-letter {
    text-transform: uppercase !important;
  }

  span:first-letter {
    text-transform: uppercase !important;
  }

  .soloprimer:first-letter{
    text-transform: uppercase !important;
  }

  .capitalize{
    text-transform: capitalize !important;
  }

  .text-muted{
    text-transform: lowercase;
  }
  p.normalp{
    font-weight: 400;
  }
</style>
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.css"/>
<link rel="stylesheet" type="text/css" href="{{url('/')}}/js/material/upload-file/css/component.css" />
<div class="container-fluid animated slideInDown">
      <div class="row">
        <div class="col-md-12 panel-view">
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ url('/') }}/backlog/calendario" class="btn btn-sm btn-success"><i class="fa fa-calendar" aria-hidden="true"></i> Ver calendario</a>
              <?php if($permiso_crear=="Si"){ ?>
              <a href="#" class="btn btn-sm btn-success" id="crearPendiente"><i class="fa fa-plus" aria-hidden="true"></i> Crear pendiente</a>
              <?php } ?>
            </div>
          </div>
          <ul class="nav nav-tabs" role="tablist">
			<li class="<?php if(!isset($aprobarActiv)){ echo("active"); } ?>"><a href="#pendientes" role="tab" data-toggle="tab">Actividades Pendientes</a></li>
			<li class="<?php if(isset($aprobarActiv)){ echo("active"); } ?>"><a href="#aprobadas" role="tab" data-toggle="tab">Actividades Por Aprobar</a></li>
			<li><a href="#realizadas" role="tab" data-toggle="tab">Actividades Realizadas</a></li>
			<li><a href="#canceladas" role="tab" data-toggle="tab">Actividades Canceladas</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade <?php if(!isset($aprobarActiv)){ echo(" in active"); } ?>" id="pendientes">
			<h4>Listado de Pendientes</h4>
		          <div class="table-responsive">
		            <table id="datatable-column-filter" cellspacing="0" width="100%" class="table table-sorting table-striped table-hover datatable dataTable no-footer">
		              <thead>
		                <tr>
		                  <th class="text-center pixeles">Item</th>
		                  <th class="text-center fecha">Fecha creación</th>
		                  <th class="text-center fecha">Fecha ejecución</th>
		                  <th class="text-center">Oportunidad</th>
		                  <th class="text-center">Tipo Actividad</th>
		                  <th class="text-center">Responsable</th>
		                  <th class="text-center fecha">Aplazadas</th>
		                  <th class="text-center">Detalle pendiente</th>
		                  <th class="text-center">Acciones</th>
		                </tr>
		              </thead>
		              <tbody>
		                @php 
		                	$i=1; 

		                @endphp
		                @foreach($datos as $dato)
		                @php $h=0; @endphp     
		               	 @if($dato->estado=="pendiente")
		                @php 
			                $num_aplazadas=false;
			                list($uno,$dos)=explode(" ",$dato->created_at);
			            @endphp



		                <tr>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $i }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $uno }}</p></td>
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  		<?php 
			                  		$aplaza = App\Actividades_detalles_canceladas::where("actividades_detalles",$dato->id)->get();
			                  		$j = count($aplaza);
			                  		$aplaza = $aplaza->pop();
			                  		echo $dato->fecha_pendiente;
			                  	?>
		                  	</p>
		                  </td>
		                  @foreach($oportunidades as $oportunidad)
		                  @php $nombre_oportunidad='';
		                  $titulo_oportunidad=''; 
		                  $precio_o=''; 
		                  @endphp
			                  @if($oportunidad->id==$dato->oportunidad_id)
				                  @php 
				                  $nombre_oportunidad=$oportunidad->empresa;
				                  $titulo_oportunidad=$oportunidad->titulo;

				                  $precios=App\HistorialOportunidades::all();
				                  foreach($precios as $precio){
				                  	if($precio->oportunidad_id==$dato->oportunidad_id&&$precio->key=="valor_oportunidad"){
				                  		$precio_o=$precio->value;
				                  	}
				                  }
				                  break;
				                  @endphp
			                  @endif

		                  @endforeach
		                  <td class="text-center pop relativo centrar-vertical" name="{{$i}}"><a href="{{url('/')}}/oportunidad/{{$dato->oportunidad_id}}" target="_blank"><p class="mas-pequeño">{{ $nombre_oportunidad }}</p></a></td>
		                  @foreach($actividades as $actividad)
		                  @php $nombre_actividad=''; @endphp
			                  @if($actividad->id==$dato->actividad)
				                  @php 
				                  $nombre_actividad=$actividad->tipo_actividad;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $nombre_actividad }}</p></td>
		                  @foreach($usuarios as $usuario)
		                  @php $nombre_usuario=''; @endphp
			                  @if($usuario->id==$dato->responsable)
				                  @php 
				                  $nombre_usuario=$usuario->name;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  	<?php 
				                $nombreuser ="";
				                if (isset($nombre_usuario) && !empty($nombre_usuario)) {
				                  $nombre   = explode(" ",$nombre_usuario);
				                  if (isset($nombre[2])) {
				                    $nombreuser = $nombre[0]." ".$nombre[2];
				                  }else{
				                    $nombreuser = $nombre[0]." ".$nombre[1];
				                  }
				                  echo $nombreuser;
				                }
				               
				            ?>
				            </p>
		                  </td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $j }} veces</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->detalle }}</p></td>
		                  <td class="text-center centrar-vertical" width="145">
		                  	<a class="btn btn-sm btn-success display-line  margen-1 comentarios-abrir" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Ver detalle">
		                  		<i class="fa fa-eye white" aria-hidden="true"></i>
		                  	</a>
		                  	<?php if($permiso_botones=="Si"){ ?>
		                  	<a class="btn btn-sm btn-primary display-line terminar margen-1" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Confirmar realizado">
		                  		<i class="fa fa-check-square-o white" aria-hidden="true"></i>
		                  	</a>
		                  	<a class="btn btn-sm btn-warning display-line aplazar margen-1" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Aplazar">
		                  		<i class="fa fa-share white" aria-hidden="true"></i>
		                  	</a>		                  	
		                  	<a class="btn btn-sm btn-danger display-line cancelar margen-1" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Cancelar">
		                  		<i class="fa fa-times white" aria-hidden="true"></i>
		                  	</a>
		                  	<?php } ?>
		                  </td>
		                </tr>
		                @php $i++; @endphp
		                @endif
		                @endforeach
		              </tbody>
		            </table>
		          </div>	
			</div>
			<div class="tab-pane fade <?php if(isset($aprobarActiv) && !empty($aprobarActiv)){ echo($aprobarActiv); } ?>" id="aprobadas">
				<h4>Listado de actividades pendientes por aprobar</h4>
		          <div class="table-responsive">
		            <table id="tabla-1" cellspacing="0" width="100%" class="table table-sorting table-striped table-hover datatable dataTable no-footer">
		              <thead>
		                <tr>
		                  <th class="text-center pixeles">Item</th>
		                  <th class="text-center fecha">Fecha creación</th>
		                  <th class="text-center fecha">Fecha ejecución</th>
		                  <th class="text-center">Oportunidad</th>
		                  <th class="text-center">Tipo Actividad</th>
		                  <th class="text-center">Responsable</th>
		                  <th class="text-center">Aplazadas</th>
		                  <th class="text-center">Detalle pendiente</th>
		                  <th class="text-center">Gestion realizada</th>
		                  <th class="text-center">Acciones</th>
		                </tr>
		              </thead>
		              <tbody>
		              	
		                @php $i=1; @endphp
		                @foreach($datos as $dato)
		                @if($dato->estado=="finalizado")
		                @php $num_aplazadas=false; $j=0; list($uno,$dos)=explode(" ",$dato->created_at); @endphp
		                <tr>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $i }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $uno }}</p></td>
		                  <td class="text-center centrar-vertical">
			                  <p class="mas-pequeño">
			                  	<?php 
			                  		$aplaza = App\Actividades_detalles_canceladas::where("actividades_detalles",$dato->id)->get();
		                  			$j = count($aplaza);
		                  			$aplaza = $aplaza->pop();
			                  		echo $dato->fecha_pendiente;
			                  	?>
			                  </p>
		                  </td>
		                  @foreach($oportunidades as $oportunidad)
		                  @php $nombre_oportunidad=''; @endphp
			                  @if($oportunidad->id==$dato->oportunidad_id)
				                  @php 
				                  $nombre_oportunidad=$oportunidad->empresa;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical" >
		                  	<a href="{{url('/')}}/oportunidad/{{$dato->oportunidad_id}}" target="_blank">
		                  		<p class="mas-pequeño">{{ $nombre_oportunidad }}</p>
		                  	</a>
		                  </td>
		                  @foreach($actividades as $actividad)
		                  @php $nombre_actividad=''; @endphp
			                  @if($actividad->id==$dato->actividad)
				                  @php 
				                  $nombre_actividad=$actividad->tipo_actividad;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $nombre_actividad }}</p></td>
		                  @foreach($usuarios as $usuario)
		                  @php $nombre_usuario=''; @endphp
			                  @if($usuario->id==$dato->responsable)
				                  @php 
				                  $nombre_usuario=$usuario->name;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  	<?php 
				                $nombreuser ="";
				                if (isset($nombre_usuario) && !empty($nombre_usuario)) {
				                  $nombre   = explode(" ",$nombre_usuario);
				                  if (isset($nombre[2])) {
				                    $nombreuser = $nombre[0]." ".$nombre[2];
				                  }else{
				                    $nombreuser = $nombre[0]." ".$nombre[1];
				                  }
				                  echo $nombreuser;
				                }
				               
				            ?>
				           	</p>
				          </td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $j }} veces</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->detalle }}</p></td>		                  
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->observaciones }}</p></td>
		                  <td class="text-center centrar-vertical" width="145">
		                  	<a class="btn btn-sm btn-success display-line  margen-1 comentarios-abrir" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Ver detalle">
		                  		<i class="fa fa-eye white" aria-hidden="true"></i>
		                  	</a>
		                  	<?php if($permiso_botones_aprobar=="Si"){ ?>
		                  	<a class="btn btn-sm btn-info display-line margen-1 aprobacion" data-name="Aprobar" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Aprobar">
		                  		<i class="fa fa-check-square-o white" aria-hidden="true"></i>
		                  	</a>
		                  	<a class="btn btn-sm btn-danger display-line margen-1 aprobacion" data-name="Rechazar" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Rechazar">
		                  		<i class="fa fa-times white" aria-hidden="true"></i>
		                  	</a>
		                  	<?php } ?>
		                  </td>
		                </tr>
		                @php $i++; @endphp
		                @endif
		                @endforeach
		              </tbody>
		            </table>
		          </div>
			</div>
			<div class="tab-pane fade" id="realizadas">
				<h4>Listado de Realizados</h4>
		          <div class="table-responsive">
		            <table id="tabla-2" cellspacing="0" width="100%" class="table table-sorting table-striped table-hover datatable dataTable no-footer">
		              <thead>
		                <tr>
		                  <th class="text-center pixeles">Item</th>
						  <th class="text-center fecha">Fecha creación</th>
		                  <th class="text-center fecha">Fecha ejecución</th>
		                  <th class="text-center">Oportunidad</th>
		                  <th class="text-center">Tipo Actividad</th>
		                  <th class="text-center">Responsable</th>
		                  <th class="text-center">Aplazadas</th>
		                  <th class="text-center">Detalle pendiente</th>
		                  <th class="text-center">Gestion realizada</th>
		                  <th class="text-center">Aprobación</th>
		                  <th class="text-center">Acciones</th>
		                </tr>
		              </thead>
		              <tbody>
		              	
		                @php $i=1; @endphp
		                @foreach($datos as $dato)
		                @if($dato->estado=="aprobado")
		                @php $num_aplazadas=false; $j=0; list($uno,$dos)=explode(" ",$dato->created_at); @endphp	                
		                
		                <tr>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $i }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $uno }}</p></td>
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  		<?php 
			                  		$aplaza = App\Actividades_detalles_canceladas::where("actividades_detalles",$dato->id)->get();
		                  			$j = count($aplaza);
		                  			$aplaza = $aplaza->pop();
			                  		echo $dato->fecha_pendiente;
			                  	?>
		                  	</p>
		                  </td>
		                  @foreach($oportunidades as $oportunidad)
		                  @php $nombre_oportunidad=''; @endphp
			                  @if($oportunidad->id==$dato->oportunidad_id)
				                  @php 
				                  $nombre_oportunidad=$oportunidad->empresa;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical" >
		                  	<a href="{{url('/')}}/oportunidad/{{$dato->oportunidad_id}}" target="_blank">
		                  		<p class="mas-pequeño">{{ $nombre_oportunidad }}</p>
		                  	</a>
		                  </td>
		                  @foreach($actividades as $actividad)
		                  @php $nombre_actividad=''; @endphp
			                  @if($actividad->id==$dato->actividad)
				                  @php 
				                  $nombre_actividad=$actividad->tipo_actividad;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $nombre_actividad }}</p></td>
		                  @foreach($usuarios as $usuario)
		                  @php $nombre_usuario=''; @endphp
			                  @if($usuario->id==$dato->responsable)
				                  @php 
				                  $nombre_usuario=$usuario->name;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  		<?php 
					                $nombreuser ="";
					                if (isset($nombre_usuario) && !empty($nombre_usuario)) {
					                  $nombre   = explode(" ",$nombre_usuario);
					                  if (isset($nombre[2])) {
					                    $nombreuser = $nombre[0]." ".$nombre[2];
					                  }else{
					                    $nombreuser = $nombre[0]." ".$nombre[1];
					                  }
					                  echo $nombreuser;
					                }					               
					            ?>
		                  	</p>
		                  </td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $j }} veces</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->detalle }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->observacion_final }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->observaciones }}</p></td>
		                  <td class="text-center centrar-vertical" width="145">
		                  	<span data-toggle="modal" data-target="#comentarios">
							    <a class="btn btn-sm btn-primary display-line  margen-1 comentarios-abrir" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Ver actividad">
			                  		<i class="fa fa-eye white" aria-hidden="true"></i>
			                  	</a>
							</span>		                  	
		                  </td>
		                </tr>
		                @php $i++; @endphp
		                @endif
		                @endforeach
		              </tbody>
		            </table>
		          </div>
			</div>
			<div class="tab-pane fade" id="canceladas">
				<h4>Listado de Cancelados</h4>
		          <div class="table-responsive">
		            <table id="tabla-3" cellspacing="0" width="100%" class="table table-sorting table-striped table-hover datatable dataTable no-footer">
		              <thead>
		                <tr>
		                  <th class="text-center pixeles">Item</th>
		                  <th class="text-center fecha">Fecha creación</th>
		                  <th class="text-center fecha">Fecha ejecución</th>
		                  <th class="text-center">Oportunidad</th>
		                  <th class="text-center">Tipo Actividad</th>
		                  <th class="text-center">Responsable</th>
		                  <th class="text-center">Aplazadas</th>
		                  <th class="text-center fecha">Fecha cancelacion</th>
		                  <th class="text-center">Detalle pendiente</th>
		                  <th class="text-center">Gestion realizada</th>
		                  <th class="text-center">Acciones</th>
		                </tr>
		              </thead>
		              <tbody>
		                @php $i=1; @endphp
		                @foreach($datos as $dato)
		                @if($dato->estado=="cancelado")
		                @php $num_aplazadas=false; $k=0; list($uno,$dos)=explode(" ",$dato->created_at); @endphp
		                <tr>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $i }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $uno }}</p></td>
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  		<?php 
			                  		$aplaza = App\Actividades_detalles_canceladas::where("actividades_detalles",$dato->id)->get();
		                  			$j = count($aplaza);
		                  			$aplaza = $aplaza->pop();
			                  		echo $dato->fecha_pendiente;
			                  	?>
		                  	</p>
		                  </td>
		                  @foreach($oportunidades as $oportunidad)
		                  @php $nombre_oportunidad=''; @endphp
			                  @if($oportunidad->id==$dato->oportunidad_id)
				                  @php 
				                  $nombre_oportunidad=$oportunidad->empresa;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical">
		                  	<a href="{{url('/')}}/oportunidad/{{$dato->oportunidad_id}}" target="_blank">
		                  		<p class="mas-pequeño">{{ $nombre_oportunidad }}</p>
		                  	</a>
		                  </td>
		                  @foreach($actividades as $actividad)
		                  @php $nombre_actividad=''; @endphp
			                  @if($actividad->id==$dato->actividad)
				                  @php 
				                  $nombre_actividad=$actividad->tipo_actividad;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $nombre_actividad }}</p></td>
		                  @foreach($usuarios as $usuario)
		                  @php $nombre_usuario=''; @endphp
			                  @if($usuario->id==$dato->responsable)
				                  @php 
				                  $nombre_usuario=$usuario->name;
				                  break;
				                  @endphp
			                  @endif
		                  @endforeach
		                  <td class="text-center centrar-vertical">
		                  	<p class="mas-pequeño">
		                  		<?php 
					                $nombreuser ="";
					                if (isset($nombre_usuario) && !empty($nombre_usuario)) {
					                  $nombre   = explode(" ",$nombre_usuario);
					                  if (isset($nombre[2])) {
					                    $nombreuser = $nombre[0]." ".$nombre[2];
					                  }else{
					                    $nombreuser = $nombre[0]." ".$nombre[1];
					                  }
					                  echo $nombreuser;
					                }
					               
					            ?>
		                  	</p>
		                  </td>
		                  	@php
		                	list($uno,$dos)=explode(" ",$dato->updated_at);
		                	@endphp
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $j }} veces</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->fecha_finalizacion }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->detalle }}</p></td>
		                  <td class="text-center centrar-vertical"><p class="mas-pequeño">{{ $dato->observacion_final }}</p></td>
		                  <td class="text-center centrar-vertical" width="145">
		                  	<span data-toggle="modal" data-target="#comentarios">
							    <a class="btn btn-sm btn-success display-line  margen-1 comentarios-abrir" name="{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Ver actividad">
			                  		<i class="fa fa-eye white" aria-hidden="true"></i>
			                  	</a>
							</span>
		                  </td>
		                </tr>
		                @php $i++; @endphp
		                @endif
		                @endforeach
		              </tbody>
		            </table>
		          </div>
			</div>
		</div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalPendiente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	  	<div class="modal-content">
		    <div class="modal-header">
		    	<img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
		      	<h4 class="modal-title">Crear pendiente</h4>
		      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    </div>
		    <div class="modal-body">
		    	<form method="POST" action="{{ url('crearpendiente') }}">
		    		{{ csrf_field() }}
		    		<div class="row">  
		    			<div class="col-12"> 
		    				<label class="col-form-label">Oportunidad</label>
                        	<!--<select name="oportunidad" class="form-control" id="oportunidad"></select>-->
                            <input type="text" class="form-control" name="oportunidad" list="nuevaoportunidad">
                            <datalist id="nuevaoportunidad">
                              @foreach($oportunidades as $o)
                                <option value="{{$o->id}}">{{$o->titulo}}</option>
                              @endforeach
                            </datalist>
                        </div>

                      	<div class="col-6">
                      		<label class="col-form-label">Tipo actividad</label>
                            <select name="tipo_pendiente" class="form-control">
                                <option value="">Seleccione una tipo de actividad</option>
                                <?php foreach($tipos_actividades as $t_a){ ?>
                                <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
                                <?php } ?>
                            </select>
                      	</div>
                      	<div class="col-6">
                      		<label class="col-form-label">Fecha pendiente</label>
                            <input class="form-control date" type="text" name="fecha_pendiente" placeholder="Fecha de Cierre" required>
                      	</div> 
                      	<div class="col-12">
                      		<label class="col-form-label">Detalle</label>
                            <textarea class="form-control detalle_pendiente" name="detalle_pendiente" rows="4" placeholder="Por favor ingrese una descripcion de la actividad pendiente"></textarea>
                      	</div>
                      	<div class="col-6">
                      		<button type="submit" class="btn btn-essi pull-right">
	                      		<i class="fa fa-floppy-o" aria-hidden="true"></i> 
	                      		Guardar
	                      	</button> 
                      	</div>                       	                          
                    </div>
		    	</form>
		    </div>
	    </div>
	</div>
</div>

<div class="modal fade" id="terminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Marcar Actividad Como Realizada</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('finalizaractividad') }}" enctype="multipart/form-data">
    	{{ csrf_field() }}
    	<input type="hidden" name="id" id="id_actividad">
    	<input type="hidden" name="estado" value="finalizado">
	    <div class="form-group row">
	          <div class="col-md-12">
	          	<label>Observaciones</label>
                <textarea class="form-control" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones"></textarea>
	          </div>
	       </div>
	       <div class=" form-group row">
	          <div class="col-md-12">
	          	<div class="box">
					<input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} Archivos seleccionados" multiple />
					<label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Cargar Archivos&hellip;</span></label>
				</div>
	          </div>
	      </div>
	      <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
	    </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="aplazar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Aplazar Actividad</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('aplazaractividad') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    	<input type="hidden" name="id" id="id_actividad-2">
    	<div class="form-group row">
    		<div class="col-12">
                <label for="fecha_ejecucion" class="col-form-label">Proxima fecha de realización</label>                   
                <input class="form-control date" type="text" name="fecha_ejecucion" required="true">
            </div>
    	</div>
	    <div class="form-group row">
	          <div class="col-md-12">
	          	<label>Observaciones</label>
                <textarea class="form-control" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones por el cual aplaza la realización de esta actividad"></textarea>
	          </div>
	       </div>
	      <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
	    </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Cancelar Actividad</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body">
    <form method="POST" action="{{ url('cancelaractividad') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    	<input type="hidden" name="id" id="id_actividad-3">
	    <div class="form-group row">
	          <div class="col-md-12">
	          	<label>Observaciones</label>
                <textarea class="form-control" name="observacion_final" required rows="3" placeholder="Escriba aqui las observaciones por el cual cancela esta actividad"></textarea>
	          </div>
	       </div>
	      <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
	    </form>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="comentarios" role="dialog">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Comentarios sobre la actividad</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
    <div class="text-center">
    	<img src="{{ url('/')}}/images/icons/loading.gif" style="display: none" class="load">	
    </div>
    <div id="cuerpo_comentarios">
    	
    </div>
    <div class="row" >
    	<div class="col-md-12 text-center" id="archivos">
    		
    	</div>
    </div>
    </div>
    </div>
  </div>
</div>
<div class="modal fade" id="aprobar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel-aprobar"></h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
    <form method="POST" action="{{ url('aprobarrechazar') }}" enctype="multipart/form-data">
    	{{ csrf_field() }}
    	<input type="hidden" name="id" id="id_actividad-aprobar">
    	<input type="hidden" name="estado" id="estado-apro">
	    <div class="form-group row">
	    	  <div class="col-md-12" style="display: none" id="fecha_volver">
	          	<label>Fecha Recomendada para realizar</label>
                <input class="form-control date" id="fecha_recomendada" name="fecha_ejecucion" type="text" required>
	          </div>
	          <div class="col-md-12">
	          	<label>Observaciones</label>
                <textarea class="form-control" name="observaciones" required rows="3" placeholder="Escriba aqui algunas observaciones"></textarea>
	          </div>
	       </div>
	      <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enviar</button>
	    </form>
    </div>
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" charset="utf-8">
$("#crearPendiente").click(function(){
        $("#modalPendiente").modal();
    });

$("body").on("mouseover",".pop",function(e){
	id=$(this).attr("name");
	vd=$("#pop-over"+id).is(":visible");
	$(".pop-over").hide("slow");
	if(!vd){
		$("#pop-over"+id).show("slow");
	}else{
		
	}
})

$("body").on("click",".aprobacion",function(e){
	if($(this).attr("data-name")!="Aprobar"){
		$("#fecha_volver").show();
		$("#fecha_recomendada").attr("required",true);
	}else{
		$("#fecha_volver").hide();
		$("#fecha_recomendada").attr("required",false);
	}
	$("#aprobar").modal();
	$("#myModalLabel-aprobar").html($(this).attr("data-name")+" realización de actividad");
	$("#id_actividad-aprobar").val($(this).attr("name"));
	$("#estado-apro").val($(this).attr("data-name"));

});
$(document).ready(function() {

	$('.date').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        nowButton: true,
        lang: 'es',
        minDate : new Date(),
        cancelText : 'Cancelar',
        okText: 'Aceptar',
        nowText: 'Nuevo',
        clearText: 'Limpiar'
    });

    $('#oportunidad').select2({
        // Activamos la opcion "Tags" del plugin
        placeholder: "Seleccione un oportunidad",
        involucrado: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("soportunidad") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

	 $("[data-toggle='tooltip']").tooltip();
    /*$('#datatable-column-filter thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );*/
    var table = $('#datatable-column-filter').DataTable({
    	language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ actividades pendientes",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ actividades pendientes",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 actividades pendientes",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "No se encontraron registros",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          }
      });
    /*table.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    $('#tabla-1 thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );*/
    var table = $('#tabla-1').DataTable({
    	language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ actividades por aprobar",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ actividades por aprobar",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 actividades por aprobar",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "No se encontraron registros",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          }
      });
    /*table.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    $('#tabla-2 thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );*/
    var table = $('#tabla-2').DataTable({
    	language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ actividades realizadas",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ actividades realizadas",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 actividades realizadas",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "No se encontraron registros",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          }
      });
    /*table.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    $('#tabla-3 thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );*/
    var table = $('#tabla-3').DataTable({
    	language: {
            processing:     "Tratamiento en curso ...",
            search:         "Buscar&nbsp;:",
            lengthMenu:     "Mostrar _MENU_ actividades canceladas",
            info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ actividades canceladas",
            infoEmpty:      "Ver de elemento 0 al 0 de 0 actividades canceladas",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "No se encontraron registros",
            emptyTable:     "No hay datos disponibles en la tabla",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": habilitado para ordenar la columna en orden descendente"
            }
          }
      });
    /*table.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );*/
    /*setTimeout(function(){
    	$("#datatable-column-filter_filter").hide();
    	$("#tabla-1_filter").hide();
    	$("#tabla-2_filter").hide();
    	$("#tabla-3_filter").hide();
    },900)*/
} );
</script>
<script src="{{url('/')}}/js/material/upload-file/js/custom-file-input.js"></script>
<script type="text/javascript">

$("body").on("click",".terminar",function(e){
	$("#terminar").modal();
	$("#id_actividad").val($(this).attr("name"));
})

$("body").on("click",".aplazar",function(e){
	$("#aplazar").modal();
	$("#id_actividad-2").val($(this).attr("name"));
})

$("body").on("click",".cancelar",function(e){
	$("#cancelar").modal();
	$("#id_actividad-3").val($(this).attr("name"));
})

$("body").on("click",".comentarios-abrir",function(e){
	$("#comentarios").modal();
	var id=$(this).attr("name");
	$(".load").show();
	$("#cuerpo_comentarios").html('');
	$("#archivos").html('');

	$.ajax({
	  	url: "{{ url('/') }}/consultarcomentarios/"+id,
	    type: "GET",
	    dataType: "json",
	    timeout: 4000,
	    cache: false
	}).done(function(e) {
	  	if (e.success) {
    		$(".load").hide();
    		$("#cuerpo_comentarios").html(e.contenido);
    		$("#archivos").html(e.archivos);
    	}else{
    		//$("#comentarios").modal('hide');
    		swal("Error!","Algo ha salido mal.","warning");
    	}
	}).fail(function(jqXHR, textStatus, errorThrown) {
	    if(textStatus==="timeout") {  
            //$("#comentarios").modal('toggle');
    		swal("Error!","El servidor se está demorando en responder, espera un momento e intenta de nuevo.","warning");
        } else {
            //$("#comentarios").modal('toggle');
    		swal("Error!","Algo ha salido mal.","warning");
        }
	});
	
})

</script>

@endsection
