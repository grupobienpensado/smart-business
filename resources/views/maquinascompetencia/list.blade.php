<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'dsfdfdsfds')

@section('content')
<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
 <!-- MODAL TIPO -->
<div class="jumbotron">
  <div class="panel-title">
    <div class="pull-right">
      <a href="{{ url('/') }}/crearmaquinavendida" class="btn btn-sm btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Crear maquina competencia</a>
      <a href="#" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
      <div class="btn-group" role="group">
        <button type="button" class="btn btn-sm btn-secondary active" onclick="funMostarDiv('divmapa2a')" id="bdivmapa2a"><i class="fa fa-map"></i></button>
        <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('list')" id="blist"><i class="fa fa-list"></i></button>
    </div>
    </div>
    <h2>Maquinas competencia</h2>
  </div>
</div>

<div class="container-fluid" id="list" style="display: none">
  <div class="row justify-content-md-center" style="background-color: #fff">
    <div class="col-12 col-md-auto">
      @if(count($datos) > 0)
       <div class="table-responsive">
        <table id="example" cellspacing="0" class="table table-striped table table-striped table-bordered display" style="width: 100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Maquina</th>
              <th style="width: 150px">Valor venta en pesos</th>
              <th>Año</th>
              <th>Modelo</th>
              <th>Marca</th>
              <th>Referencia</th>
              <th>Capacidad equipo</th>
              <th>Consumo energia</th>
              <th>Consumo aire</th>
              <th>Consumo agua</th>
              <th>Consumo vapor</th>
              <th>Observaciones</th>
              <th>Detalle</th>
              <th>Observaciones cliente</th>
            </tr>
          </thead>
          <tbody>
             
            @foreach($datos as $dato)
            <tr>
              <td class="centrado">{{ $dato->id }}</td>
              <td class="centrado">{{ $dato->name }}</td>
              <td class="centrado"><?php echo "$ ".$dato->valor_venta." ="; ?></td>
              <td class="centrado">{{ $dato->ano_venta }}</td>
              <td class="centrado">{{ $dato->modelo }}</td>
              <td class="centrado">{{ $dato->marca }}</td>
              <td class="centrado">{{ $dato->referencia }}</td>
              <td class="centrado">{{ $dato->capacidad_equipo }}</td>
              <td class="centrado"><?php echo $dato->energia; ?> Kw</td>
              <td class="centrado"><?php echo $dato->aire; ?> Cfm</td>
              <td class="centrado"><?php echo $dato->agua; ?> Lt</td>
              <td class="centrado"><?php echo $dato->vapor; ?> Kg/h</td>
              <td class="centrado" data-toggle="tooltip" data-placement="top" title="{{ $dato->observaciones }}">Observaciones</td>
              <td class="centrado" data-toggle="tooltip" data-placement="top" title="{{ $dato->detalle }}">Detalle</td>
              <td class="centrado" data-toggle="tooltip" data-placement="top" title="{{ $dato->observaciones_cliente }}">Observaciones cliente</td>
              <td></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      @endif

    </div>
  </div>
</div>

<div style="width: 100%;background-color: #fff;top: 190px;position: absolute;" id="divmapa2a">
  <div id="divmapa" style="height: 700px"></div>
</div>


@endsection

@section('scripts')

<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.bootstrap4.min.js"></script>   

<script>
  var image = "";
  var contentString = "";
  function initMap() {

    var styledMapType = new google.maps.StyledMapType(
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8ec3b9"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1a3646"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#64779e"
            }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#4b6878"
            }
          ]
        },
        {
          "featureType": "landscape",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#334e87"
            }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6f9ba5"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3C7680"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#304a7d"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2c6675"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#255763"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#b0d5ce"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3a4762"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#0e1626"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#4e6d70"
            }
          ]
        }
      ],
      {name: 'Styled Map'}
    );



    var map = new google.maps.Map(document.getElementById('divmapa'), {
      zoom: 3,
      center: {lat: 7.064034827633218, lng: -73.1567105784718},
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
      }
    });


    image = {
      url: '{{ url("/") }}/images/iconos_empresa/maquina_competencia.svg',
      size: new google.maps.Size(50, 40),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 40)
    };
    var markers = [];
    function addMarker(feature) {          
      var marker = new google.maps.Marker({
        position: feature.position,
        icon: image,
        title: feature.type,
        map: map,
        draggable:true
      });
      marker.setAnimation(google.maps.Animation.BOUNCE);
      
      var infowindow = new google.maps.InfoWindow({
        content: feature.contenido,
        maxWidth: 200
      });

      marker.addListener('mouseover', function() {
        contentString = feature.contenido;
        marker.setAnimation(google.maps.Animation.BOUNCE);
        infowindow.open(map, marker);
      });
      markers.push(marker);
    }

    var features = [
      <?php foreach($datos as $dato){ 
      if (!empty($dato->lat)) { 
        $nombre = trim($dato->name, " \t\n\r\0\x0B"); 
        $nombre  = str_replace(' ', '', $nombre ); 
        $nombre = preg_replace('([^A-Za-z0-9])', '', $nombre);?>
        {
          position: new google.maps.LatLng({{ $dato->lat }}, {{ $dato->lng }}),
          type: '<?php echo $nombre; ?>',
          contenido:  `<div id="content">
                        <div id="bodyContent">
                        <?php $empresa = App\Empresa::where('id', '=', $dato->empresa)->get(); $empresa = $empresa[0]; ?>
                              <a href="{{ url('/') }}/maquinacompetencia/{{ $dato->id }}">
                                <img src="
                                  @if(!empty($empresa->logo))
                                    {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
                                  @else
                                    https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
                                  @endif" style="max-width: 50%;" class="img-fluid mx-auto d-block">
                              </a>
                            </div>
                        <h2 class="titulo2">{{ $empresa->nombre }}</h2>
                        <h2 class="centrado titulo">{{ $dato->name }}</h2>
                        <h5 class="card-header centrado"> Valor venta </h5>
                        <p class="subtitulo centrado" style="font-size: 18px;">{{ "$ ".$dato->valor_venta." =" }}</p>
                        <h5 class="card-header centrado"> Año venta </h5>
                        <p class="subtitulo centrado" style="font-size: 18px;">{{ $dato->ano_venta }}</p>
                        <h5 class="card-header centrado"> Descripcion </h5>
                        <p class="subtitulo">{{ $dato->detalle }}</p>
                        <h5 class="card-header centrado"> Observaciones </h5>
                        <p class="subtitulo">{{ $dato->observaciones }}</p>
                        <h5 class="card-header centrado"> Observaciones Cliente </h5>
                        <p class="subtitulo">{{ $dato->observaciones_cliente }}</p>
                      </div>`
        },
      <?php } } ?>           
    ];

    for (var i = 0, feature; feature = features[i]; i++) {
      addMarker(feature);
    }
    var markerCluster = new MarkerClusterer(map, markers, { maxZoom: 4, gridSize: 80, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
  }
  var locations = [
    @foreach($datos as $dato)
      <?php if (!empty($dato->lat)) {?>
      {
      location: { lat: {{ $dato->lat }}, lng: {{ $dato->lng }} },
      images: image
      },
    <?php } ?> 
    @endforeach 
  ]
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSg3ZOwMbQvB0xjXAFmr2Ch-7IKV98gno&callback=initMap"></script>
<script type="text/javascript" charset="utf-8">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ oportunidades",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ 2 oportunidades",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 oportunidades",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      }
    });
  });

  funMostarDiv = function (valor) {
    $(".active").removeClass('active');
    $("#b"+valor).addClass('active');
    console.log("ccc", valor);
    if(valor==="list"){
      $("#list").show();
    }else{
      $("#list").hide();
    }
    if(valor==="divmapa2a"){
      console.log("entra");
      $("#divmapa2a").show();
      initMap();
    }else{
      $("#divmapa2a").hide();
    }
  }
</script>
@endsection