<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Agregar Sede')

<!--@section('sidebar')
    @parent

    
@endsection-->

@section('content')   

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
   
</style>

<div class="container animated flipInX">
  <div class="row">
    <main class="col-sm-12 col-md-12 pt-6">  
      <form method="POST" action="{{ url('crearmaquinacompetencia') }}">
        <div class="panel-title">
            <h2>Agregar Maquina Competencia</h2>
            <p>Agrega una nueva maquina de competencia a los registros</p>
        </div>
        <hr>
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group row">
            <label class="col-2 col-form-label">Producto</label>
            <div class="col-10">
                <input class="form-control form-control-sm" type="text" name="name" placeholder="Ingrese el nombre del producto">
            </div>
        </div> 

        <div class="form-group row">
          <div class="col-3">
              <label class="col-form-label">Año de venta</label>
              <input class="form-control form-control-sm" type="number" name="ano_venta" placeholder="Ej: 1992">
          </div>
          <div class="col-3">
            <label class="col-form-label">Valor Venta</label>
            <input class="form-control form-control-sm" type="text" name="valor_venta" id="valor" placeholder="Por favor ingrese un valor de venta" required>
          </div>
          <div class="col-3">
            <label class="col-form-label">Modelo</label>
            <input class="form-control form-control-sm" type="text" name="modelo" placeholder="Por favor ingrese el modelo" required>
          </div>
          <div class="col-3">
            <label class="col-form-label">Marca</label>
            <input class="form-control form-control-sm" type="text" name="marca" placeholder="Por favor ingrese la marca" required>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-3">
              <label class="col-form-label">Capacidad equipo</label>
              <input class="form-control form-control-sm" type="text" name="capacidad_equipo" placeholder="Por favor ingrese la capacidad del equipo">
          </div>
          <div class="col-3">
            <label class="col-form-label">Referencia</label>
            <input class="form-control form-control-sm" type="text" name="referencia" placeholder="Por favor ingrese la referencia del equipo" required>
          </div>
          <div class="col-6">
            <label class="col-form-label">Consumo de Energia</label>
            <div class="md-form input-group">
              <input type="number" class="form-control form-control-sm" name="energia" placeholder="Ingresar el consumo de Energia">
              <span class="input-group-addon form-control-sm"> Kw</span>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-4">
            <label class="col-form-label">Consumo de Aire</label>
            <div class="md-form input-group">
              <input type="number" class="form-control form-control-sm" name="aire" placeholder="Ingresar el consumo de Aire">
              <span class="input-group-addon form-control-sm"> Cfm</span>
            </div>
          </div>
          <div class="col-4">
            <label class="col-form-label">Consumo de Agua</label>
            <div class="md-form input-group">
              <input type="number" class="form-control form-control-sm" name="agua" placeholder="Ingresar el consumo de Agua">
              <span class="input-group-addon form-control-sm"> Lt</span>
            </div>
          </div>
          <div class="col-4">
            <label class="col-form-label">Consumo de Vapor</label>
            <div class="md-form input-group">
              <input type="number" class="form-control form-control-sm" name="vapor" placeholder="Ingresar el consumo de Vapor">
              <span class="input-group-addon form-control-sm"> Kg/h</span>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-4">
              <label for="empresa" class="col-form-label">Empresa</label>
              <select name="empresa" class="form-control" id="empresa" required></select>
          </div>
          <div class="col-4">
              <label for="sede" class="col-form-label">Sede</label>
              <select name="sede" class="form-control" id="sede" required></select>
          </div>
          <div class="col-4">
              <label for="cliente" class="col-form-label">Cliente</label>
              <select name="cliente" class="form-control" id="cliente" required></select>
          </div>
        </div>        

        <div class="form-group row">
          <div class="col-4">
            <label for="pais" class="col-form-label">Pais</label>
            <select name="pais" class="form-control" id="pais" required></select>
          </div>
          <div class="col-4">                     
            <label for="departamento" class="col-form-label">Departamento</label>
            <select name="departamento" class="form-control" id="estados" required></select>
          </div>
          <div class="col-4">
            <label for="ciudad" class="col-form-label">Ciudad</label>
            <select name="ciudad" class="form-control" id="ciudad" required></select> 
          </div>
        </div>

        <div class="form-group row">
          <div class="col-12">
              <label for="direccion" class="col-form-label">Direccion exacta</label>
              <input class="form-control form-control-sm" type="text" name="direccion" placeholder="Por favor ingrese la direccion exacta" id="direccion">
          </div>                                      
        </div>

        <div class="form-group">
          <div id="map"></div>
          <div class="coordinates">
            <em class="lat">Latitud</em>
            <em class="lon">Longitud</em>
            <input type="text" id="lat" name="lat">
            <input type="text" id="lng" name="lng">
          </div>
        </div>

        <div class="form-group">
          <label for="observaciones" class="col-form-label">Observaciones</label>
          <textarea class="form-control" name="observaciones" rows="5" placeholder="Por favor ingrese una observaciones"></textarea>
        </div>
        <div class="form-group">
          <label for="observaciones" class="col-form-label">Observaciones Cliente</label>
          <textarea class="form-control" name="observaciones_cliente" rows="5" placeholder="Por favor ingrese una observaciones"></textarea>
        </div>
        <div class="form-group">
          <label for="observaciones" class="col-form-label">Detalle</label>
          <textarea class="form-control" name="detalle" rows="5" placeholder="Por favor ingrese una observaciones"></textarea>
        </div>
        <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
      </form>
    </main>
  </div>
</div>
@endsection
@section('scripts')
  <script src="js/jquery.maskMoney.min.js"></script>
  <script>

    var marker;
    var map;
    var geocoder;
    var infoWindow = {};
    var pais = "";
    var departamento = "";
    var ciudad = "";
    var direccion = "";
    
    $("#valor").maskMoney();

    $('#direccion').change(function () {
      direccion = "";
      dir="";
      if (direccion != "") {
        dir = direccion+", ";
      }

      if (ciudad != "") {
        dir += ciudad+" - ";
      }

      if (departamento != "") {
        dir += departamento+", ";
      }

      if (pais != "") {
        dir += pais;
      }
      console.log(dir);
      codeAddress(dir);
    });

    $('#pais').change(function () {
      pais = $(this).val();
      dir="";
      if (direccion != "") {
        dir = direccion+", ";
      }

      if (ciudad != "") {
        dir += ciudad+" - ";
      }

      if (departamento != "") {
        dir += departamento+", ";
      }

      if (pais != "") {
        dir += pais;
      }
      codeAddress(dir);
    });

    $('#estados').change(function () {
      departamento = $(this).val();
      dir="";
      if (direccion != "") {
        dir = direccion+", ";
      }

      if (ciudad != "") {
        dir += ciudad+" - ";
      }

      if (departamento != "") {
        dir += departamento+", ";
      }

      if (pais != "") {
        dir += pais;
      }

      codeAddress(dir);
    });

    $('#ciudad').change(function () {
      ciudad = $(this).val();
      dir="";
      if (direccion != "") {
        dir = direccion+", ";
      }

      if (ciudad != "") {
        dir += ciudad+" - ";
      }

      if (departamento != "") {
        dir += departamento+", ";
      }

      if (pais != "") {
        dir += pais;
      }

      codeAddress(dir);
    });

    $("#margen").change(function () {
        margen = $(this).val();
        if (margen > 100) {
            swal({
              title: 'Error!',
              text: 'No es posible ingresar un margen superior al 100%.',
              timer: 2000
            }).then(function () {
                $(this).val("");
            });
        }
    });

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < marker.length; i++) {
        marker[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    function initMap() {
      geocoder = new google.maps.Geocoder();
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 59.325, lng: 18.070}
      });

      infoWindow = new google.maps.InfoWindow({map: map});

      funGeolocation();
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    }

    function attachSecretMessage() {      
        $("#lat").val(marker.position.lat);
        $("#lng").val(marker.position.lng);    
    }

    function funGeolocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $("#lat").val(pos.lat);
          $("#lng").val(pos.lng); 
          marker.setMap(map);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: pos
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function codeAddress(dir) {
      if (marker) {
        marker.setMap(null);
      }
      
      var address = dir;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
          
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: results[0].geometry.location
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          $("#lat").val(results[0].geometry.location.lat);
          $("#lng").val(results[0].geometry.location.lng);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

    $('#pais').select2({
        // Activamos la opcion "Tags" del plugin
        placeholder: "Seleccione un Pais",
        pais: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("pais") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $('#estados').select2({
        // Activamos la opcion "Tags" del plugin
        placeholder: "Seleccione un Departamento",
        estados: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("estados") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term,
                    state: $('#pais').val()
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $('#ciudad').select2({
        // Activamos la opcion "Tags" del plugin
        placeholder: "Seleccione una Ciudad",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("ciudades") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term,
                    dato: $('#estados').val()
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $('#productos').select2({
      tokenSeparators: [','],
      ajax: {
          dataType: 'json',
          url: '{{ url("sproducto") }}',
          delay: 250,
          data: function(params) {
              return {
                  term: params.term
              }
          },
          processResults: function (data, page) {
            return {
              results: data
            };
          },
      },
      allowClear: true,
      placeholder: "Seleccione los productos",
      productos: true,
      tags: "true",
      language: "es",
      selectOnClose: true,
    });

    $('#empresa').select2({
        // Activamos la opcion "Tags" del plugin
        placeholder: "Seleccione una empresa",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("empresa") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    
    $('#sede').select2({
        // Activamos la opcion "Tags" del plugin
        placeholder: "Seleccione una sede",
        sede: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("sede") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term,
                    state: $('#empresa').val()
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $('#cliente').select2({
        // Activamos la opcion "Tags" del plugin
         placeholder: "Seleccione una cliente",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("cliente") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term,
                    dato:$("#sede").val()
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $(function () {
      $("[data-toggle='tooltip']").tooltip();
    });

  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbw8hYpmFO_VWUZLufrAL1qfnPFQp4JaM&callback=initMap"></script>
@endsection

