<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Productos')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">  
<style type="text/css">
.kv-avatar{
  height: 240px;
  min-height: 240px;
  max-height: 240px;
}
  .kv-avatar .file-input{
    text-align: center;
    position: absolute;
    right: 27%;
  }
  p{
    font-weight: 100 !important;
    color: #000 !important;
    border-bottom: 0.5px solid #ccc;
  }
  p span{
    font-weight: bold;
  }
  .titulo{
    color: #000;
    font-weight: 100;
  }

  .titulo span{
    font-weight: bold;

  }
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
   
</style>

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
    
    .jumbotron{
        height: 100px;
        padding-top: 15px;
        background-color: #ffffff !important;
    }
    .mdl-grid,.dt-table{
      min-width: 100% !important;
    }

    .tooltip-inner {
      white-space: pre-wrap;
    }
</style>

<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('producto/crear') }}" class="btn btn-primary btn-sm"><i class="fa fa-list" aria-hidden="true"></i> Crear producto</a>
      </div>
      <h2>Listar producto</h2>
    </div>
</div>

  <div class="card animated slideInDown" style="margin-bottom: 30px;">
    <div class="card-block">  
        

        @if(count($datos) > 0)
        <div class="table-responsive">
          <table id="example" width="100%" class="table table-striped table table-striped table-bordered display">
            <thead>
              <tr>
                <th class="centrado">Item</th>
                <th class="centrado">Producto</th>
                <th class="centrado">Referencias</th>
                <th class="centrado">Creada</th>
                <th class="centrado">Editada</th>
                <th class="centrado">Acciones</th>
              </tr>
            </thead>
            <tbody>               
              @foreach($datos as $dato)
              <tr>
                <td class="text-center"></td>
                <td>{{ $dato->name }}</td>
                <?php $referencias = App\ProductoReferencias::where("producto_id",$dato->id)->get(); 
                  setlocale(LC_ALL, "es_CO.UTF-8");
                  $texto = "";
                  foreach ($referencias as $key => $value) {
                    $texto .=" ".$value->referencia.",&#10;"; 
                  }
                ?>
                <td class="text-center" data-toggle="tooltip" data-placement="top" title="{{ $texto }}"><?php echo count($referencias); ?></td>
                <td class="text-center">{{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->created_at)) }}</td>
                <td class="text-center">{{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->updated_at)) }}</td>
                <td class="text-center">
                  <a href="{{ url('producto/editar') }}/{{ $dato->id }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editar producto">
                    <i class="fa fa-pencil" aria-hidden="true"></i> 
                  </a>
                  <a href="{{ url('producto/ver') }}/{{ $dato->id }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ver producto">
                    <i class="fa fa-eye" aria-hidden="true"></i> 
                  </a> 
                </td>
              </tr>
              
              @endforeach
            </tbody>
          </table>
        </div>
        @endif
    </div>
  </div>

  <div class="modal fade" id="guardar_evento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
          <h4 class="modal-title" id="myModalLabel">Información del producto <span id="nombre"></span></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="cuerpo"></div>    
      </div>
    </div>
  </div>
@endsection


@section('scripts')
<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>

<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>

<script>
  $("body").on("click",".ver",function(e){
    $("#nombre").html($(this).attr("id"));
  id=$(this).attr("name");
  
  $("#cuerpo").html('');
  setTimeout(function(e){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/producto/"+id, 
        cache: false,
        type: 'GET',
        success: function(e){ 
          $("#cuerpo").html(e.tabla);
        }
    });},2000)

})
  $("#logo").fileinput({
      language: "es",
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      showBrowse: false,
      browseOnZoneClick: true,
      removeLabel: '',
      removeIcon: '<i class="fa fa-times" aria-hidden="true"></i>',
      removeTitle: 'Cancelar o restablecer cambios',
      elErrorContainer: '#logo-errors',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: '<img src="{{ url('/') }}/images/machine-press.svg" alt="Logo de la empresa" style="width:160px"><h6 class="text-muted">Clic para seleccionar foto</h6>',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif"]
  });
  $("#logo-1").fileinput({
      language: "es",
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      showBrowse: false,
      browseOnZoneClick: true,
      removeLabel: '',
      removeIcon: '<i class="fa fa-times" aria-hidden="true"></i>',
      removeTitle: 'Cancelar o restablecer cambios',
      elErrorContainer: '#logo-errors',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: '<img src="{{ url('/') }}/images/machine-press.svg" alt="Logo de la empresa" style="width:160px"><h6 class="text-muted">Clic para seleccionar foto</h6>',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif"]
  });

  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ productos",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ productos",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 productos",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      },
      order: [ [3,'desc'] ],
    });
    var t = $('#example').DataTable();
     
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
  });

  
</script>
@endsection
