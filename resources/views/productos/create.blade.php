@extends('template.app')

@section('title', 'Crear producto')

@section('content')
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
    
    .jumbotron{
        height: 100px;
        padding-top: 15px;
        background-color: #ffffff !important;
    }
</style>

<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('productos') }}" class="btn btn-primary btn-sm"><i class="fa fa-list" aria-hidden="true"></i> Listar producto</a>
      </div>
      <h2>Agregar producto</h2>
      <p class="letra-gris" style="margin-bottom: 0px;">Agrega un nuevo producto a tus registros</p>
    </div>
</div>

<div class="card" style="margin-bottom: 30px;">
    <div class="card-block">  
    	<form method="POST" action="{{ url('productos') }}" enctype="multipart/form-data">
			<!-- Create Post Form -->
			@if (count($errors) > 0)
			  <div class="alert alert-danger">
			      <ul>
			          @foreach ($errors->all() as $error)
			              <li>{{ $error }}</li>
			          @endforeach
			      </ul>
			  </div>
			@endif
			{{ csrf_field() }}
			<div id="output"></div>
			<div class="form-group row">			  
			  <div class="col-md-6">
			      <div class="form-group">
			        <label for="name" class="col-form-label">Categoria</label>
			        <select class="form-control" name="categoria" required>
			          <option value="">Seleccione una opción</option>
			          <option value="Envasadoras">Envasadoras</option>
			          <option value="Equipos de procesos">Equipos de procesos</option>
			          <option value="Finales de linea">Finales de linea</option>
			        </select>
			       </div>
		      </div>
		      <div class="col-md-6">
			      <div class="form-group">
			        <label for="name" class="col-form-label">Nombre del producto</label>
			        <input class="form-control" type="text" name="name" placeholder="Por favor ingrese el nombre del producto" required>
			      </div>
		      </div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4">
				      	<label class="col-form-label">Referencia</label>
				    </div>
				    <div class="col-4">
				      	<label class="col-form-label">Descripción</label> 
					</div>
					<div class="col-4" style="display: flex;justify-content: center;">
					    <p style="color: #9e9e9e">Foto de la maquina</p>              
					</div>
				</div>
			</div>
			<div class="form-group" id="productosMas">
				<div class="row animated rollIn" id="rowProductos1">
					<div class="col-4">
						<input type="hidden" id="count" value="1">
						<div class="input-group">
							<span class="input-group-btn">
								<button id="btn1" class="btn btn-primary add-more" type="button" onclick="addRowProducts()">+</button>
							</span>
							<input autocomplete="off" class="input form-control" id="field1" name="ref[1][referencia]" type="text" placeholder="Por favor ingrese la referencia" required>						
						</div>
				    </div>
				    <div class="col-4">
				      	<textarea class="form-control" placeholder="Por favor ingrese el nombre del producto" name="ref[1][descripcion]" rows="6"></textarea>
					</div>
					<div class="col-4" style="display: flex;justify-content: center;">
				        <div class="dropzone" data-width="300" data-height="300" data-ajax="false" data-originalsave="true" style="width: 300px;height: 300px;min-width: 300px !important;;min-height: 300px !important;">
				            <input type="file" name="ref[1][logo]" accept="image/gif, image/jpeg, image/png">
				        </div>               
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-essi pull-right btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
	<script type="text/javascript">

		$('.btn-submit').click(function(e){
		  if(true === $("form").parsley().validate()){		  
		    var element = $(this).parent().parent().parent().parent().parent();
		  }else{
		    e.preventDefault();
		    var element = $(this).parent().parent().parent().parent().parent();
		    $(element).addClass('animated rubberBand');  
		    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		      $(element).removeClass('animated rubberBand');
		    });

		    $("#output").removeClass(' alert alert-success');
		    $("#output").addClass("alert alert-danger animated flip").html("Perdón!, Es necesario que complete todos los campos");
		    $("#output").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		      $("#output").removeClass('animated flip');
		    });
		  }
		});

		
    	
		addRowProducts = function(){
			// change attribute for new item

			$count = $('#count').val();
			$count++;

			$("#productosMas").find('.btn')
				.prop('onclick',null)
				.off('click')
				.removeClass('add-more btn-primary')
				.addClass('btn-danger')
				.text('-')
				.on('click', function(){
					var element = $(this).parents('.row');
					element.removeClass('rollIn').addClass('hinge');
					setTimeout(function(){ element.remove(); }, 1000); 
				});
			


			$("#productosMas").prepend(`<div class="row input-appendable-wrapper animated rollIn"  id="rowProductos`+$count+`">
					<div class="col-4">
						<div class="input-group input-group-appendable">
							<span class="input-group-btn">
								<button id="btn`+$count+`" class="btn add-more btn-primary" type="button" onclick="addRowProducts()">+</button>
							</span>
							<input autocomplete="off" class="input form-control" id="field`+$count+`" name="ref[`+$count+`][referencia]" type="text" placeholder="Por favor ingrese la referencia">						
						</div>
				    </div>
				    <div class="col-4">
				      	<textarea class="form-control" placeholder="Por favor ingrese el nombre del producto" name="ref[`+$count+`][descripcion]" rows="6"></textarea>
					</div>
					<div class="col-4" style="display: flex;justify-content: center;">
				        <div class="dropzone" data-width="300" data-height="300" data-ajax="false" data-originalsave="true" style="width: 300px;height: 300px;min-width: 300px !important;;min-height: 300px !important;">
				            <input type="file" name="ref[`+$count+`][logo]" accept="image/gif, image/jpeg, image/png">
				        </div>               
					</div>
				</div>`);
			

			$("#productosMas").find('#count').val($count);
			$('.dropzone').html5imageupload();
				

		};      
        $('.dropzone').html5imageupload();
    </script>
@endsection
