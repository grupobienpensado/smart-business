@extends('template.app')

@section('title', 'Ver producto')

@section('content')
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
    
    .jumbotron{
        height: 100px;
        padding-top: 15px;
        background-color: #ffffff !important;
    }
</style>

<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('productos') }}" class="btn btn-primary btn-sm"><i class="fa fa-list" aria-hidden="true"></i> Listar producto</a>
      </div>
      <h2>Ver producto</h2>
    </div>
</div>

<div class="card" style="margin-bottom: 30px;">
    <div class="card-block">      	
			<div class="form-group row">			  
			  <div class="col-md-6">
			        <label for="name" class="col-form-label">Categoria</label>
			        <h3>{{ $model->categoria }}</h3>
		      </div>
		      <div class="col-md-6">
			      <div class="form-group">
			        <label for="name" class="col-form-label">Nombre del producto</label>
			        <h3>{{ $model->name }}</h3>
			      </div>
		      </div>
			</div>
			
			<?php $i = 0; ?>			
			<div class="form-group row">
				@foreach($referencia as $ref)
				<?php $i++; ?>			  
			  	<div class="col-md-3">
					<h5 class="card-header" style="text-align: center;"> {{ $ref->referencia }} </h5>
					<div class="profile-empresa" style="-webkit-border-radius: 0 !important;">
		        	   <img src="
			           @if(!empty($ref->foto))
			              {{ url('/') }}/images/file/productos/{{ $ref->foto }}
			           @else
			              http://via.placeholder.com/250x250/fff/948e8e?text={{ $ref->referencia }} 
			           @endif" class="img-fluid mx-auto d-block img-empresa">
		    		</div>
					<p>{{ $ref->descripcion }}</p>
			  	</div>
				@endforeach
			</div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
