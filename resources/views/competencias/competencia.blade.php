@extends('template.app')
@section('title', 'Competencia')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ url('/') }}/css/html5imageupload.css" rel="stylesheet">
<style>
.card-img-top{height:250px;background-position:center;background-repeat:no-repeat;background-size:cover}
.wrapper{margin-bottom:3.5rem}
.big-circle{background-color:rgba(0,0,0,0.2);border-radius:50%;width:260px;height:260px;position:absolute;top:15%;left:50%;transform:translate(-50%,-50%);transition:.3s}
.big-circle:hover{width:318px;height:318px}
.big-circle .fa{color:#fff!important;position:absolute;transform:translate(-50%,-50%)}
.big-circle .fa:hover{color:#f1f1f1}
.fa-archive{margin-top:30px;top:50%}
.fa-industry{margin-top:30px;bottom:50%}
.fa-user-plus{top:50%;left:50%}
.fa-comment{top:50%}
.small-circle{width:240px;height:240px;background-image:url('<?php echo url('/').'/storage/competencias_files/profile_images/'.$data['perfil']->logo; ?>');background-position:center;background-repeat:no-repeat;background-size:cover;border-radius:50%;display:inline-block;position:absolute;top:50%;left:50%;margin:-120px;transition:.3s}
.overlay{display:block;position:fixed;width:100%;height:100%;left:0;top:0;z-index:99;background:rgba(29,29,31,0.6);visibility:hidden;opacity:0}
.profile-open .overlay,.overlay{-webkit-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-moz-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-o-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);transition:all .5s cubic-bezier(0.645,0.045,0.355,1)}
.nav-slide{display:block;position:fixed;width:45rem;height:auto;background:#1D1D1F;top:0;bottom:0;overflow:hidden;min-height:500px;z-index:99;margin-top:0!important;-webkit-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-moz-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-o-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-webkit-transform:translate3d(-45rem,0,0);-moz-transform:translate3d(-45rem,0,0);-ms-transform:translate3d(-45rem,0,0);-o-transform:translate3d(-45rem,0,0);transform:translate3d(-45rem,0,0)}
.nav-slide ul{padding:0;margin-top:40px}
.nav-slide li{list-style:none;margin-bottom:20px;font-size:12px}
.nav-slide.side-profile{right:0;left:auto;color:#000;background:#FFF;-webkit-transform:translate3d(45rem,0,0);-moz-transform:translate3d(45rem,0,0);-ms-transform:translate3d(45rem,0,0);-o-transform:translate3d(45rem,0,0);transform:translate3d(45rem,0,0)}
body.no-scroll{overflow:hidden}
body.profile-open .transform-container{-webkit-transform:translate3d(-45rem,0,0);-moz-transform:translate3d(-45rem,0,0);-ms-transform:translate3d(-45rem,0,0);-o-transform:translate3d(-45rem,0,0);transform:translate3d(-45rem,0,0)}
.profile-open .overlay,.menu-open .overlay{visibility:visible;opacity:1}
.profile-open .side-profile{-webkit-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-moz-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-o-transition:all .5s cubic-bezier(0.645,0.045,0.355,1);transition:all .5s cubic-bezier(0.645,0.045,0.355,1);-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);-o-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}
.comment-wrap #messages_box textarea{font-size:1.5rem;outline:none;border:none;display:block;margin:0;padding:0;-webkit-font-smoothing:antialiased;color:#555f77}
.comment-wrap #messages_box textarea::-webkit-input-placeholder{color:#ced2db}
.comment-wrap #messages_box textarea::-moz-placeholder{color:#ced2db}
.comment-wrap #messages_box textarea:-moz-placeholder{color:#ced2db}
.comment-wrap #messages_box textarea:-ms-input-placeholder{color:#ced2db}
.comments{overflow-y:scroll;background-color:#f0f2fa;-webkit-font-smoothing:antialiased;width:100%;height:80vh;padding-bottom:1rem;padding-top:1rem;border-top-left-radius:10px;border-top-right-radius:10px}
.comment-wrap{margin-bottom:1.25rem;display:table;width:100%;min-height:5.3125rem}
.photo{padding:1.5rem;display:table-cell;width:3.5rem}
.photo .avatar{height:7rem;width:7rem;border-radius:50%;background-size:cover}
.comment-block{padding:1rem;background-color:#fff;display:table-cell;vertical-align:top;border-radius:.1875rem;box-shadow:0 1px 3px 0 rgba(0,0,0,0.08)}
.comment-block textarea{width:100%;resize:none}
.comment-text{font-size:1.5rem;margin-bottom:1.25rem;color:#000;font-weight:400}
.bottom-comment{color:#acb4c2}
.comment-date{float:left}
.comment-actions{float:right}
.comment-actions li{display:inline;margin:-2px;cursor:pointer}
.comment-actions li.complain{padding-right:.75rem;border-right:1px solid #e1e5eb}
.comment-actions li.reply{padding-left:.75rem;padding-right:.125rem}
.comment-actions li:hover{color:#0095ff}
    .ftd{
        min-width: 240px!important;
        max-width: 240px!important;
        min-height: 240px!important;
        max-height: 240px!important;
    }
    #map, #mapa_2 {
        height: 100%;
    }
    .modal-content{
        margin-top: 15%;
    }
    #caja_comentarios{
        padding-top: 12%;
    }
    .card-img-top{
        background-position: top;
    }
    .divider {
        height: 1px;
        overflow: hidden;
        background-color: #e0e0e0;
    }
    #mini_menu{
        top: 11.5rem;
    }
    .card-5 {
        box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
        border: none;
    }
    .func{
        width: 100%;
        height: 50%;
    }
    .avatar{
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: 22%;
    }
</style>
<input type="hidden" id="id_comp" value="{{ $data['id_competencia'] }}">
<div class="overlay"></div>
  <div class="nav-slide side-profile">
  <div class="row">
        <div class="comments col-md-12" id="caja_comentarios">
        </div>
        <div class="col-md-12 pt-4">
            <div class="comment-wrap">
                <div class="photo">
                    <div class="avatar" style="background-image: url('<?php echo url('/').'/storage/competencias_files/profile_images/'.$data['perfil']->logo; ?>')"></div>
                </div>
                <div class="comment-block">
                    <form autocomplete="off" id="messages_box">
                       <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..."></textarea>
                    </form>
                    <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="do_comments()" id="send_btn">Enviar <i class="fa fa-paper-plane"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt-3">
    <div class="card card-5">
    <div class="card-img-top" style="background-image: url('<?php echo url('/').'/storage/competencias_files/profile_images/'.$data['perfil']->banner; ?>')"></div>
        <div class="wrapper">
            <div class="big-circle">
                <a class="fa fa-user-plus fa-lg fa-2x" data-toggle="modal" data-target="#mdl_funcionarios"></a>
                <a class="fa fa-archive fa-lg fa-2x"></a>
                <a class="fa fa-industry fa-lg fa-2x" data-toggle="modal" data-target="#nueva_sede"></a>
                <a class="fa fa fa-comment fa-lg fa-2x" id="add_comment"></a>
                <div class="small-circle">
                </div>
            </div>
        </div>
        <center>
        <div class="btn-group" role="group" id="mini_menu">
          <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#mdl_funcionarios">Funcionarios</button>
          <button type="button" class="btn btn-secondary">Productos</button>
        </div>
        </center>
        <h1 style="margin-top: 12rem;">
        <?php echo $data['perfil']->nombre; ?><br>
        <small class="text-muted text-capitalize"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>Bucaramanga / Santander</small>
        </h1>
        <div class="card-body container pt-5">
            <div class="row">
                <div class="col-md-12">
                <p class=""><?php echo $data['perfil']->descripcion; ?></p>
                </div>
                <div class="col-md-12">
                <h1 class="w-100">Sedes</h1>
                <div class="divider"></div>
                <div class="container" id="lista_sedes">
                <!--Lista de sedes-->
                </div>
                <h1 class="w-100">Ubicacion</h1>
                </div>
                <div class="col-md-12 mt-5" id="mapa_2" style="height: 500px">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Funcionarios-->
<div class="modal fade" id="mdl_funcionarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Ver funcionarios</h5>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="card">
                                <img class="func card-img-top" src="http://via.placeholder.com/250x250" alt="Card image cap">
                                <div class="avatar"><img alt="" src="http://via.placeholder.com/100x100" class="rounded-circle"></div>
                                <div class="card-body">
                                    <h5 class="card-title mt-5">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal agregar sede-->
<div class="modal fade" id="nueva_sede" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg w-100" role="document">
        <div class="modal-content mt-5">
            <div class="modal-header">
                <h5 class="modal-title">Añadir sede competencia</h5>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="nueva_sede_frm">
                    <input type="hidden" name="comp_id" value="{{ $data['id_competencia'] }}">
                    <h4 class="">Foto de la planta</h4>
                    <div class="dropzone ftd mb-4" data-width="240" data-height="240" data-resize="false" data-ajax="false">
                        <input type="file" name="foto_planta" id="foto_planta">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="cant_empleados" name="cant_empleados" placeholder="Cantidad de empleados">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo de contácto">
                    </div>
                    <div class="form-group">
                        <label for="responsable">Funcionario responsable</label>
                        <select id="responsable" name="responsable">
                    </select>
                    </div>
                    <div class="form-group <?php echo $sede_flag = ($data['flag'])? "d-none" : "" ?>">
                        <input type="checkbox" class="form-check-input" id="sede_principal" name="sede_principal" value="1">
                        <label class="form-check-label ml-1" for="sede_principal">¿Es la sede principal?</label>
                    </div>
                    <div class="container p-0">
                       <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Latitud" id="lat" name="lat">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Longitud" id="lng" name="lng">
                        </div>
                    </div>
                        <div id="map"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" onclick="GuardarSede()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal agregar funcionario-->
<div class="modal fade" id="mdl_funcionarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Añadir funcionario</h5>
      </div>
      <div class="modal-body">
      <form enctype="multipart/form-data" id="add_funcionario_frm">
         <input type="hidden" name="comp_id" value="{{ $data['id_competencia'] }}">
         <h4 class="">Foto funcionario</h4>
           <div class="dropzone ftd mb-4" data-width="240" data-height="240" data-resize="false" data-ajax="false" id="foto_funcionario_dz">
              <input type="file" name="foto_funcionario" id="foto_funcionario">
            </div>
          <div class="form-group">
            <input type="text" class="form-control" id="nom_fun" name="nom_fun" placeholder="Nombre del funcionario">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1">Fecha de nacimiento</label>
            <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="cargo" name="cargo" placeholder="Cargo">
          </div>
          <div class="form-group">
           <label for="exampleInputEmail1">Fecha de ingreso</label>
           <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso">
          </div>
          <div class="form-group">
            <input type="number" class="form-control" id="salario" name="salario" placeholder="Salario">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="jefe_inmediato" name="jefe_inmediato" placeholder="Jefe inmediato">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="estado_civil" name="estado_civil" placeholder="Estado civil">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="profesion" name="profesion" placeholder="Profesión">
          </div>
          <div class="form-group">
            <textarea class="form-control" id="perfil" name="perfil" rows="3" placeholder="Perfil"></textarea>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="oportunidades" name="oportunidades" placeholder="Oportunidades">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="GuardarFuncionario()">Guardar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSg3ZOwMbQvB0xjXAFmr2Ch-7IKV98gno"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        list_comments();
        ListarFuncionarios();
        ListarSedes();
        ListarSedesMapa();
        $('.dropzone').html5imageupload();
        $('#foto_funcionario_dz').html5imageupload();

        $(".big-circle").on('mouseenter', function() {
            $(".fa-user-plus").animate({
                "left": "23px"
            }, "slow");

            $(".fa-archive").animate({
                "top": "-5px"
            }, "slow");

            $(".fa-comment").animate({
                "right": "-3px"
            }, "slow");

            $(".fa-industry").animate({
                "bottom": "4px"
            }, "slow");
        });

        $(".big-circle").on('mouseleave', function() {
            $(".fa-user-plus").css('left', '130px');
            $(".fa-archive").css('top', '50%');
            $(".fa-comment").css('right', '104px');
            $(".fa-industry").css('bottom', '111px');
        });

        $('#add_comment').on('click', function() {
            $('body').addClass('no-scroll profile-open');
        });

        $('.overlay').on('click', function() {
            $('body').removeClass('no-scroll menu-open profile-open');
        });

    });

    $('#nueva_sede').on('show.bs.modal', function(event){
        setTimeout(function(){
            initMap();
        }, 500);
    });

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 6
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        //Add listener
        google.maps.event.addListener(map, "click", function (event) {
            var latitude = event.latLng.lat();
            var longitude = event.latLng.lng();
            $("#lat").val(latitude);
            $("#lng").val(longitude);
            console.log( latitude + ', ' + longitude );
        });

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }


    function list_comments() {
        $.ajax({
            url: "{{ url('/competencias/comentarios-competencia') }}",
            cache: false,
            method: "POST",
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id_comp: $("#id_comp").val()
            },
            success: function(suss) {
                if (suss.res) {
                    $("#caja_comentarios").html(suss.data);
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    function do_comments(){
        $.ajax({
            url: "{{ url('/competencias/comentar-competencia') }}",
            cache: false,
            method: "POST",
            timeout: 10000,
            beforeSend: function() {
                $("#send_btn").prop('disabled', true);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                comentario: $("#chat_box").val(),
                id_comp: $("#id_comp").val()
            },
            success: function(suss) {
                console.log(suss);
                if (suss.res) {
                    $("#send_btn").prop('disabled', false);
                    $("#messages_box")[0].reset();
                    list_comments();
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    /**
     * Listar proveedores para select2
     * @returns {object|string} devuelve las options para el input
     */
    function ListarFuncionarios() {
        $("#responsable").select2({
            ajax: {
                url: '{{ url("/competencias/listar-funcionarios") }}',
                method: 'POST',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term // search term
                    };
                },
                processResults: function(data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.nombre,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            minimumInputLength: 2,
            language: {
                inputTooShort: function() {
                    return 'Por favor, introduzca 2 caracteres mínimo';
                }
            },
            placeholder: "Ej: José Pérez",
            allowClear: true
        });
    }

    function GuardarSede(){
        var datos = new FormData($("#nueva_sede_frm")[0]);
        $.ajax({
            url: '{{ url("/competencias/guardar-sede") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            contentType: false,
            processData: false,
            data: datos,
            success: function(suss){
                if(suss.res){
                    $("#nueva_sede_frm")[0].reset();
                    initMap();
                    $('#responsable').val(null).trigger('change');
                    $('#nueva_sede').modal('hide');
                    $('div.tools.final .btn-del').click();

                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    }

    function GuardarFuncionario(){
        var datos = new FormData($("#add_funcionario_frm")[0]);
        $.ajax({
            url: '{{ url("/competencias/guardar-funcionario") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            contentType: false,
            processData: false,
            data: datos,
            success: function(suss){
                if(suss.res){
                    $("#add_funcionario_frm")[0].reset();
                    $('div.tools.final .btn-del').click();
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    }

    function ListarSedes(){
        $.ajax({
            url: '{{ url("/competencias/listar-sedes") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: {id_comp : $("#id_comp").val()},
            success: function(suss){
                if(suss.res){
                    $("#lista_sedes").html(suss.data);
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    }

    function ListarSedesMapa(){
        $.ajax({
            url: '{{ url("/competencias/listar-sedes-geo") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: {id_comp : $("#id_comp").val()},
            success: function(suss){
                if(suss.res){
                    var sedes = [], temp;
                    for(var i = 0; i < suss.data.length; i++){
                        temp = suss.data[i].ubicacion.split("%%");
                        sedes.push({lat: parseFloat(temp[0]), lng: parseFloat(temp[1])})
                    }
                    var map = new google.maps.Map(document.getElementById('mapa_2'), {
                      zoom: 3,
                      center: {lat: -28.024, lng: 140.887}
                    });

                    // Create an array of alphabetical characters used to label the markers.
                    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                    // Add some markers to the map.
                    // Note: The code uses the JavaScript Array.prototype.map() method to
                    // create an array of markers based on a given "locations" array.
                    // The map() method here has nothing to do with the Google Maps API.
                    var markers = sedes.map(function(location, i) {
                      return new google.maps.Marker({
                        position: location,
                        label: labels[i % labels.length]
                      });
                    });

                    // Add a marker clusterer to manage the markers.
                    var markerCluster = new MarkerClusterer(map, markers,
                        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    }


    function initMap2(){

        var map = new google.maps.Map(document.getElementById('mapa_2'), {
          zoom: 3,
          center: {lat: -28.024, lng: 140.887}
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }

</script>
@endsection
