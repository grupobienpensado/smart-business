@extends('template.app')
@section('title', 'Competencias')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ url('/') }}/css/html5imageupload.css" rel="stylesheet">
<style>
    .card{box-shadow:0 8px 20px rgba(0,0,0,0.19),0 6px 6px rgba(0,0,0,0.23);border: none;}
    .card:hover{box-shadow:0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22);transition:all .3s cubic-bezier(.25,.8,.25,1)}
    .card .avatar{position:relative;top:-90px;margin-bottom:-70px}
    .card .avatar img{width:15rem;-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;-webkit-transition:-webkit-box-shadow .3s ease;transition:box-shadow .3s ease;-webkit-box-shadow:0 0 0 8px rgba(0,0,0,0.06);box-shadow:0 0 0 8px rgba(0,0,0,0.06)}
    .img-hover{-webkit-box-shadow:0 0 0 12px rgba(0,0,0,0.1)!important;box-shadow:0 0 0 12px rgba(0,0,0,0.1)!important}
    .card .cardheader{height:135px}
    .dropdown-toggle::after{display:none}
    .open > .dropdown-menu{-webkit-transform:scale(1,1);transform:scale(1,1);opacity:1}
    .competencias-container{background-color:#FFF;width:100%;height:100%;border-radius:10px}
    .modal-content{width:538px;margin-top:20%}
    .comp-frm-header{min-width:536px!important;max-width:536px!important;min-height:135px!important;max-height:135px!important;z-index:0}
    .comp-frm-avatar{min-width:200px!important;max-width:200px!important;min-height:200px!important;max-height:200px!important;border-radius:50%;top:-65px;z-index:1;-webkit-box-shadow:0 0 0 8px rgba(0,0,0,0.06);box-shadow:0 0 0 8px rgba(0,0,0,0.06)}
    .comp-frm-avatar:after{content:"Haga click para añadir un logo";position:relative;font-size:20px;color:#868686;text-align:center;top:30%}
    .comp-frm-header:after{content:"Haga click para añadir un banner";position:relative;font-size:20px;color:#868686;text-align:center}
    canvas#canvas_c_banner{height:auto!important}
    .essi-color-text{color:#051d60}
    ul.social-network{list-style:none;display:inline;margin-left:0!important;padding:0}
    ul.social-network li{display:inline;margin:0 5px}
    .social-network a.red:hover{background-color:#051d60}
    .social-network a.red:hover i{color:#fff}
    a.socialIcon:hover,.socialHoverClass{color:#44BCDD}
    .social-circle li a{display:inline-block;position:relative;margin:0 auto;-moz-border-radius:50%;-webkit-border-radius:50%;border-radius:50%;text-align:center;width:40px;height:40px;font-size:20px}
    .social-circle li i{margin:0;line-height:40px;text-align:center}
    .social-circle li a:hover i,.triggeredHover{-moz-transform:rotate(360deg);-webkit-transform:rotate(360deg);-ms--transform:rotate(360deg);transform:rotate(360deg);-webkit-transition:all .2s;-moz-transition:all .2s;-o-transition:all .2s;-ms-transition:all .2s;transition:all .2s}
    .social-circle i{color:#fff;-webkit-transition:all .8s;-moz-transition:all .8s;-o-transition:all .8s;-ms-transition:all .8s;transition:all .8s}
    ul.social-network.social-circle li a{background-color:#D3D3D3}
    .comp-bottom-separator{border-color: #051d60;border-width: 5px;}
</style>
<div class="container">

    <div class="jumbotron">
        <div class="panel-title">
            <div class="pull-right">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-success <?php if($data['permiso_crear']=="Si"){ ?>opn-mdl<?php } ?>" data-action="1" <?php if($data['permiso_crear']=="No"){ ?>onclick="no_permiso('Usted no tiene permisos para crear una competencia')"<?php } ?> ><i class="fa fa-plus"></i> Añadir competencia</button>
                </div>
            </div>
            <h2>Competencias</h2>
            <p class="text-muted text-capitalize">Gestión de competencias</p>
        </div>
    </div>
    <div class="competencias-container p-5">
    <div class="row" id="lst_comp_cont">
    </div>
    </div>
</div>

<!--MODALS-->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="competencias_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <form id="CE_competencia_frm" enctype="multipart/form-data">
                    <input type="hidden" name="action" id="action" value="">
                    <input type="hidden" name="id" id="id" value="">
                    <div class="dropzone comp-frm-header" data-width="830" data-height="360" data-resize="true" data-ajax="false" data-resize="true">
                        <input type="file" name="c_banner" id="c_banner">
                    </div>
                    <div class="dropzone comp-frm-avatar" data-width="200" data-height="200" data-resize="true" data-ajax="false" data-resize="true">
                        <input type="file" name="c_logo" id="c_logo">
                    </div>
                    <div class="form-group pr-5 pl-5">
                        <input type="text" class="form-control mt-4" id="nombre" name="nombre" placeholder="Nombre o razón social" data-parsley-required>
                        <input type="number" class="form-control mt-4" id="nit" name="nit" placeholder="NIT" data-parsley-validate data-parsley-required>
                        <div class="form-group mt-4">
                            <label class="text-muted text-capitalize text-left" for="fecha_fundacion">Fecha de fundación</label>
                            <input type="date" class="form-control" id="fecha_fundacion" name="fecha_fundacion" placeholder="Fecha de fundación">
                        </div>
                        <input type="text" class="form-control mt-4" id="pagina_web" name="pagina_web" placeholder="Página web" data-parsley-required>
                        <input type="email" class="form-control mt-4" id="correo_contacto" name="correo_contacto" placeholder="Correo" data-parsley-validate data-parsley-required>
                        <input type="tel" class="form-control mt-4" id="telefono_contacto" name="telefono_contacto" placeholder="Teléfono de contácto" data-parsley-validate data-parsley-required>
                        <textarea class="form-control mt-4" id="detail" name="detail" rows="7" placeholder="Breve descripción de la competencia" data-parsley-required></textarea>
                    </div>
                    <label class="text-muted text-capitalize text-left">Redes sociales</label>
                    <div class="form-group pr-5 pl-5" id="socials_select">
                       <div class="clonedInput" id="socials_0">
                        <select class="social-select form-control h-100" name="socials[]">
                        </select>
                        <input class="form-control" name="socials_link[]" placeholder="Link del perfil">
                        <div class="actions">
                        <a class="btn clone fa fa-plus text-success" aria-hidden="true"></a>
                        <a class="btn remove fa fa-minus text-danger" aria-hidden="true"></a>
                        </div>
                       </div>
                    </div>
                </form>
                <div class="progress mt-4" id="progress_compe">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="AccionesCompetencias(1)">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="LimpiarFormulario()">Cerrar</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        ListarCompetencias();
        $("#progress_compe").hide();

        $(document).on('click', '.comp-frm-header', function() {
            $(this).css('background-image', 'none');
        });

        $(document).on('click', '.comp-frm-avatar', function() {
            $(this).css('background-image', 'none');
        });

        $(document).on('mouseenter', '.card', function() {
            $(this).find('.img-avatar-card').addClass('img-hover');
        });

        $(document).on('mouseleave', '.card', function() {
            $(this).find('.img-avatar-card').removeClass('img-hover');
        });

        $(document).on('click', '.action-del', function() {
            AccionesCompetencias(3, $(this).data('cid'));
        });

        $(document).on('click', '.action-edit', function() {
            var datos = ListarCompIndividual($(this).data('cid'));
            $("#action").val(2);
            $("#id").val(datos[0].id);
            $(".comp-frm-header").css('background-image', 'none');
            $(".comp-frm-avatar").css('background-image', 'none');
            $(".comp-frm-header").css('background-image', 'url("{{ url("/") }}/storage/competencias_files/profile_images/' + datos[0].banner + '")');
            $(".comp-frm-header").css('background-size', 'cover');
            $(".comp-frm-avatar").css('background-image', 'url("{{ url("/") }}/storage/competencias_files/profile_images/' + datos[0].logo + '")');
            $(".comp-frm-avatar").css('background-size', 'cover');
            $("#nombre").val(datos[0].nombre);
            $("#detail").val(datos[0].descripcion);
            $("#nit").val(datos[0].nit);
            $("#fecha_fundacion").val(datos[0].fecha_fundacion);
            $("#pagina_web").val(datos[0].pagina_web);
            $("#correo_contacto").val(datos[0].correo_contacto);
            $("#telefono_contacto").val(datos[0].telefono_contacto);
            $("#socials_select").html(datos[1]);
            $('#competencias_modal').modal('show');
        });

        $(document).on('click', '.opn-mdl', function() {
            $("#action").val($(this).data('action'));
            LimpiarFormulario();
            ListarSocial();
            $('#competencias_modal').modal('show');
        });

        $(document).on("click", "a.clone", clone);

        $(document).on("click", "a.remove", remove);

        $("#competencias_modal").on('shown.bs.modal', function() {
            $('.comp-frm-avatar').html5imageupload();
            $('.comp-frm-header').html5imageupload();
        });

    });

    /**
     * Acciones individuales para las competencias
     * @param {number} action = null Acción a realizar
     * @param {number} id = null     ID del elemento a ejecutar
     */
    function AccionesCompetencias(action = null, id = null) {
        var form = $("#CE_competencia_frm").parsley();
        if (form.validate()) {
            if (action === 1 || action === 2) {
                var datos = new FormData($("#CE_competencia_frm")[0]);
                $.ajax({
                    url: '/competencias/accion',
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: datos,
                    beforeSend: function() {
                        $("#progress_compe").show();
                    },
                    success: function(suss) {
                        if (suss.res) {
                            $('#competencias_modal').modal('hide');
                            LimpiarFormulario();
                            ListarCompetencias();
                        } else {
                            console.log(suss.msj);
                        }
                    },
                    error: function(err) {
                        console.log(err);
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        //Upload Progress
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                $('#progress_compe > .progress-bar').css({
                                    "width": percentComplete + "%"
                                });
                            }
                        }, false);
                        //Upload progress
                        xhr.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = (evt.loaded / evt.total) * 100;
                                    $("#upload_bar_sol > .progress-bar").css({
                                        "width": percentComplete + "%"
                                    });
                                }
                            },
                            false);
                        return xhr;
                    }
                });
            }
        }
        if (action === 3) {
                $.ajax({
                    url: '/competencias/accion',
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    cache: false,
                    data: {
                        action: action,
                        id: id
                    },
                    success: function(suss) {
                        if (suss.res) {
                            ListarCompetencias();
                        } else {
                            console.log(suss.msj);
                        }
                    },
                    error: function(err) {
                        console.log(err);
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
    }

    /**
     * Listar todas las competencias
     */
    function ListarCompetencias() {
        $.ajax({
            url: '/competencias/listar',
            method: 'POST',
            cache: false,
            dataType: 'json',
            timeout: 10000,
            success: function(suss) {
                if (suss.res) {
                    $("#lst_comp_cont").html(suss.data);
                }
            },
            error: function(err) {
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /**
     * Traer la información de la competencia para editar
     * @param {number} id ID de la competencia
     */
    function ListarCompIndividual(id) {
        var datos;
        $.ajax({
            url: '/competencias/listar-individual',
            method: 'POST',
            cache: false,
            dataType: 'json',
            async: false,
            timeout: 10000,
            data: {
                id: id
            },
            success: function(suss) {
                if (suss.res) {
                    datos = suss.data;
                }
            },
            error: function(err) {
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        return datos;
    }

    /**
     * limpiar el formulario en el modal
     */
    function LimpiarFormulario(){
        $("#CE_competencia_frm")[0].reset();
        $("div.tools.final .btn-del").click();
        $(".comp-frm-header").css('background-image', 'none');
        $(".comp-frm-avatar").css('background-image', 'none');
        $("#progress_compe").hide();
        $('#progress_compe > .progress-bar').css('width', '0%');
    }

    var regex = /^(.+?)(\d+)$/i;
    var cloneIndex = $(".clonedInput").length;
    /**
     * Clonar select de redes sociales
     */
    function clone(){
        $(this).parents(".clonedInput").clone()
            .appendTo("#socials_select")
            .attr("id", "socials_" +  cloneIndex)
            .find("*")
            .each(function() {
                var id = this.id || "";
                var match = id.match(regex) || [];
                if (match.length == 3) {
                    this.id = match[1] + (cloneIndex);
                }
            });
        cloneIndex++;
    }

    /**
     * Eliminar una red social añadida o sólo el campo en el form
     */
    function remove(){
        var count_inputs = $(".clonedInput").children(".social-select").length;
        var element = $(this);
        if(count_inputs > 1){
            if(element.data('inedit')){
                swal({
                    title: "Atención",
                    text: "¿Está seguro que desea eliminar?",
                    type: "question",
                    showCancelButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: "Cancelar",
                    cancelButtonColor: '#d33'
                }).then(
                    function() {
                        EliminarSocialIndividual(element.data('redid'), element);
                    },
                    function() {
                        return false;
                    });
            }else{
                element.parents(".clonedInput").remove();
            }
        }
    }

    function ListarSocial(selector = ".social-select"){
        $.ajax({
            url: '{{ url("/redes-sociales/listar") }}',
            method: 'POST',
            dataType: 'json',
            timeout: 10000,
            cache: true,
            success: function(suss){
                if(suss.res){
                    $(selector).html(suss.data);
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    function EliminarSocialIndividual(rs_id, element){
        $.ajax({
            url: '{{ url("/competencias/eliminar-red-social") }}',
            method: 'POST',
            dataType: 'json',
            timeout: 10000,
            cache: true,
            data: {red_id : rs_id},
            success: function(suss){
                if(suss.res){
                    element.parents(".clonedInput").remove();
                    ListarCompetencias();
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
