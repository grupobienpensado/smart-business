@extends('template.app2')
@section('title', 'Tipo Ingreso-Mega Archivo')

@section('content')

@if(!isset($usuario))
	@php
		header('Location: '.url('/').'/megaarchivo');
	@endphp
@else

<style type="text/css">
	.container{
	  display: block;
	  position: relative;
	  margin: 40px auto;
	  height: auto;
	  width: 500px;
	  padding: 20px;
	}
	h2 {
		color: #AAAAAA;
	}

	.container ul{
	  list-style: none;
	  margin: 0;
	  padding: 0;
		overflow: auto;
	}

	ul li{
	  color: #AAAAAA;
	  display: block;
	  position: relative;
	  float: left;
	  width: 100%;
	  height: 100px;
		border-bottom: 1px solid #333;
	}

	ul li input[type=radio]{
	  position: absolute;
	  visibility: hidden;
	}

	ul li label{
	  display: block;
	  position: relative;
	  font-weight: 300;
	  font-size: 1.35em;
	  padding: 25px 25px 25px 80px;
	  margin: 10px auto;
	  height: 30px;
	  z-index: 9;
	  cursor: pointer;
	  -webkit-transition: all 0.25s linear;
	}

	ul li:hover label{
		color: #000;
	}

	ul li .check{
	  display: block;
	  position: absolute;
	  border: 5px solid #AAAAAA;
	  border-radius: 100%;
	  height: 25px;
	  width: 25px;
	  top: 30px;
	  left: 20px;
		z-index: 5;
		transition: border .25s linear;
		-webkit-transition: border .25s linear;
	}

	ul li:hover .check {
	  border: 5px solid #000;
	}

	ul li .check::before {
	  display: block;
	  /*position: absolute;*/
		content: '';
	  border-radius: 100%;
	  height: 15px;
	  width: 15px;
	  top: 5px;
		left: 5px;
	  margin: auto;
		transition: background 0.25s linear;
		-webkit-transition: background 0.25s linear;
	}

	input[type=radio]:checked ~ .check {
	  border: 5px solid #000;
	}

	input[type=radio]:checked ~ .check::before{
	  background: #000;
	}

	input[type=radio]:checked ~ label{
	  color: #000;
	}

	.signature {
		margin: 10px auto;
		padding: 10px 0;
		width: 100%;
	}

	.signature p{
		text-align: center;
		font-family: Helvetica, Arial, Sans-Serif;
		font-size: 0.85em;
		color: #AAAAAA;
	}

	.signature .much-heart{
		display: inline-block;
		position: relative;
		margin: 0 4px;
		height: 10px;
		width: 10px;
		background: #AC1D3F;
		border-radius: 4px;
		-ms-transform: rotate(45deg);
	    -webkit-transform: rotate(45deg);
	    transform: rotate(45deg);
	}

	.signature .much-heart::before, 
	.signature .much-heart::after {
		  display: block;
	  content: '';
	  position: absolute;
	  margin: auto;
	  height: 10px;
	  width: 10px;
	  border-radius: 5px;
	  background: #AC1D3F;
	  top: -4px;
	}

	.signature .much-heart::after {
		bottom: 0;
		top: auto;
		left: -4px;
	}

	.signature a {
		color: #AAAAAA;
		text-decoration: none;
		font-weight: bold;
	}


	/* Styles for alert... 
	by the way it is so weird when you look at your code a couple of years after you wrote it XD */

	.alert {
		box-sizing: border-box;
		background-color: #000;
		width: 100%;
		position: relative; 
		top: 0;
		left: 0;
		z-index: 300;
		padding: 20px 40px;
		color: #333;
	}

	.alert h2 {
		font-size: 22px;
		color: #232323;
		margin-top: 0;
	}

	.alert p {
		line-height: 1.6em;
		font-size:18px;
	}

	.alert a {
		color: #232323;
		font-weight: bold;
	}
</style>

<!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ url('/') }}/MDB/MDB Free/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ url('/') }}/MDB/MDB Free/css/style.css" rel="stylesheet">
 <!-- MODAL TIPO -->

 	<link href="{{ url('/') }}/select2/select2.min.css" rel="stylesheet">

<!--Modal: Contact form-->
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header light-blue darken-3 white-text">
                <h4 class="title" style="margin-top: -3%"><i class="fa fa-sign-in"></i> Tipo Ingreso</h4>
            </div>
            <!--Body-->
            <form id="formulario_reemplazararchivos" action="{{ url('megaarchivo/tipoingreso') }}" method="POST" enctype="multipart/form-data" >
            	{{ csrf_field() }}
	            <div class="modal-body mb-0">
	                <div class="md-form form-sm">
	                    
	                    <select id="form19" class="form-control tipo" name="tipo">
	                    	<option value="no" selected disabled>Seleccione un tipo</option>
	                    	<option value="oportunidad">Oportunidad creada</option>
	                    	<option value="estudios">Estudio de archivos comerciales</option>
	                    </select>
	                   
	                </div>
	                <input type="hidden" name="oportunidad" id="oportunidad" value="">
	                <input type="hidden" name="usuario" value="{{ $usuario }}">
	                <div class="content-oportunidad" style="display: none;">
	                <!--<select id="oportunidad_s" name="oportunidad_s2">
		                
	                </select>-->
	                <input type="text" id="oportunidad_s" name="oportunidad_s2" list="listado_oportunidades">
	                <datalist id="listado_oportunidades">
	                  <?php 
	                  	foreach($oportunidades as $oportunidad){ ?>
	                  	<option value="<?php echo $oportunidad->id ?>"><?php echo $oportunidad->titulo ?></option>
	                  	<?php
	                  	}
	                   ?>
					</datalist>            
	                </div>
	                <ul class="" style="display: none;">
	                <?php $con=0; ?>
	                  @foreach($oportunidades as $o)
					  <li>
					    <input type="radio" id="f-option{{ $con }}" class="tipo_opo" name="selector" value="{{ $o->id }}">
					    <label for="f-option{{ $con }}">{{ $o->titulo }}</label>
					    <div class="check"></div>
					  </li>
					  <?php $con++; ?>
					  @endforeach
					</ul>

	                <div class="text-center mt-1-half">
	                	<button type="submit" class="btn btn-info mb-2 ingresar_validao" style="display: none;">Ingresar<i class="fa fa-send ml-1"></i></button>
	                    <a class="btn btn-info mb-2 ingresar">Ingresar<i class="fa fa-send ml-1"></i></a>
	                </div>
	            </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Contact form-->


 <!-- MODAL TIPO -->


@endsection
@section('scripts')
<!-- Central Modal Medium Success-->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/mdb.min.js"></script>

    <script src="{{ url('/') }}/select2/select2.min.js"></script>

<script type="text/javascript" id="code">
$('#modalContactForm').modal('show');

$('body').on('change','.tipo',function(){
	if($(this).val() === "oportunidad"){
		$('.content-oportunidad').css('display','block');
	}else{
		$('.content-oportunidad').css('display','none');
	}
})

$('body').on('click','.tipo_opo',function(){
	$('#oportunidad').val($(this).val());
})

$('body').on('click','.ingresar',function(){
	valor=$('#form19').val();
	if(valor===null){
		swal("Error!", "Debe seleccionar un tipo", "error");
	}else if(valor==='oportunidad'){
		if($('#oportunidad_s').val()===''){
			swal("Error!", "Debe seleccionar una Oportunidad", "error");
		}else{
			$( ".ingresar_validao" ).click();
		}
	}else{
			$( ".ingresar_validao" ).click();
	}
})

	// inicializamos el plugin
	/*$('#oportunidad_s').select2({
	    // Activamos la opcion "Tags" del plugin
	    placeholder: "Seleccione una oportunidad",
	    pais: true,
	    tokenSeparators: [','],
	    ajax: {
	        dataType: 'json',
	        url: '{{ url("ssoportunidad") }}',
	        delay: 250,
	        data: function(params) {
	            return {
	                term: params.term
	            }
	        },
	        processResults: function (data, page) {
	          return {
	            results: data
	          };
	        },
	    }
	});*/
</script>

@endsection
@endif