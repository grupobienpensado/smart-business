@extends('template.app2')

@section('content')
<style>
  .blueimp-gallery {
    z-index: 1;
  }
  .text-muted{
    color: #032fac !important;
    font-size: 17px !important;
    margin-right: 8%;
  }
    .seleccionado{
        border-radius: 37px 37px 37px 37px;
        -moz-border-radius: 37px 37px 37px 37px;
        -webkit-border-radius: 37px 37px 37px 37px;
        border: 0px solid #000000;
    }
    input[type=range] {
      height: 25px;
      -webkit-appearance: none;
      margin: 10px 0;
      width: 90%;
    }
    input[type=range]:focus {
      outline: none;
    }
    input[type=range]::-webkit-slider-runnable-track {
      width: 90%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      box-shadow: 0px 0px 0px #000000;
      background: #D3D3D3;
      border-radius: 1px;
      border: 0px solid #000000;
    }
    input[type=range]::-webkit-slider-thumb {
      box-shadow: 0px 0px 0px #000000;
      border: 1px solid #2497E3;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #A1D0FF;
      cursor: pointer;
      -webkit-appearance: none;
      margin-top: -7px;
    }
    input[type=range]:focus::-webkit-slider-runnable-track {
      background: #D3D3D3;
    }
    input[type=range]::-moz-range-track {
      width: 90%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      box-shadow: 0px 0px 0px #000000;
      background: #2497E3;
      border-radius: 1px;
      border: 0px solid #000000;
    }
    input[type=range]::-moz-range-thumb {
      box-shadow: 0px 0px 0px #000000;
      border: 1px solid #2497E3;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #A1D0FF;
      cursor: pointer;
    }
    input[type=range]::-ms-track {
      width: 90%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: transparent;
      border-color: transparent;
      color: transparent;
    }
    input[type=range]::-ms-fill-lower {
      background: #2497E3;
      border: 0px solid #000000;
      border-radius: 2px;
      box-shadow: 0px 0px 0px #000000;
    }
    input[type=range]::-ms-fill-upper {
      background: #2497E3;
      border: 0px solid #000000;
      border-radius: 2px;
      box-shadow: 0px 0px 0px #000000;
    }
    input[type=range]::-ms-thumb {
      margin-top: 1px;
      box-shadow: 0px 0px 0px #000000;
      border: 1px solid #2497E3;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #A1D0FF;
      cursor: pointer;
    }
    input[type=range]:focus::-ms-fill-lower {
      background: #2497E3;
    }
    input[type=range]:focus::-ms-fill-upper {
      background: #2497E3;
    }

    .texto{
            border-radius: 23px;
        }
    input.texto {
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        margin-bottom: 1%;
    }
    .rounded-circle {
        border-radius: 0%;
        cursor: pointer;
    }
    img.img-fluid.rounded-circle.menu {
        max-width: 100%;
    }
    .img-circle {
      border-radius: 50%;
      -webkit-box-shadow: 2px 2px 5px #999;
  }
  .sombra{
    max-height: 200px;
    min-height: 200px;
    overflow: hidden;
    cursor: pointer;
    -webkit-box-shadow: 5px 5px 30px 2px rgba(5,29,96,0.68);
    -moz-box-shadow: 5px 5px 30px 2px rgba(5,29,96,0.68);
    box-shadow: 5px 5px 30px 2px rgba(5,29,96,0.68);
  }
  .titulo-imagen{
    margin: 2%;
    margin-bottom: 8%;
  }
  .letra-imagen{
    font-family: 'Raleway', sans-serif;
    font-size: 17px;
  }
</style>
@if(!isset($tiempo))
  @php
    header('Location: /megaarchivo');
  @endphp
@else
<script> id_tiempo='{{ $tiempo }}'; id_tiempo=parseInt(id_tiempo); </script>

<ol class="breadcrumb">@if(isset($html_cod))
@php $html_cod=str_replace("%", "'", $html_cod); $html_cod=str_replace("°", '"', $html_cod); @endphp @php print $html_cod; @endphp
@endif</ol>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: -8%;">
  <div class="modal-dialog modal-lg" role="document">
    <img src="{{ url('/') }}/images/logo.png" class="logo-essi-video"/>
    <div id="modal_video" class="modal-content" style="border: 0; background: rgba(240, 248, 255, 0);">
    </div>
  </div>
</div>

<input type="hidden" name="archivo" id="archivo-ruta" value="{{ $carpeta }}">
<div class="container-fluid">
    <div class="row">
        <div class="col-s12 col-md-12" style="text-align: center;">
            <h4 id="titulo">@if(isset($titulo)) {{ $titulo }} @else {{ $carpeta }} @endif</h4>
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-12" style="text-align: center; margin-bottom: 2%">
            <a class="btn btn-success" style="cursor: pointer;" onclick="cargar()">
                <i class="fa fa-plus" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Agregar" style="color: white;"></i>
            </a>
        </div>
    </div>

    <div class="row" style="margin-bottom: 2%">
        <div class="col-2 col-md-1">

        </div>
        <div class="col-2 col-md-2 no_seleccionado" id="Elemento1" onclick="mostrar(this.id,'Elemento2','section_videos')" style="cursor: pointer; display: none;">
            <img src="{{ url('/') }}/images/explorador/Play.png" style="margin: 3%;"><a id="numero_videos" style="color: #959699; font-size: 18.54;">  Videos () </a>

        </div>
        <div class="col-2 col-md-2 seleccionado" id="Elemento2" style="cursor: pointer; background-color: rgb(116, 146, 232)">
            <img src="{{ url('/') }}/images/explorador/play_blanco.png" style="margin: 3%;"><a id="numero_videos_seleccionado" style="color: #fff; font-size: 18.54;">  Videos () </a>
        </div>
        <div class="col-2 col-md-2 no_seleccionado" id="Elemento3" onclick="mostrar(this.id,'Elemento4','galeria_images_plano')" style="cursor: pointer;">
            <img src="{{ url('/') }}/images/explorador/Img_gris.png" style="margin: 3%;"><a id="numero_imagenes" style="color: #959699; font-size: 18.54;">  Imagenes () </a>
        </div>
        <div class="col-2 col-md-2 seleccionado" id="Elemento4" style="cursor: pointer; background-color: rgb(116, 146, 232); display: none;">
            <img src="{{ url('/') }}/images/explorador/img_blanco.png" style="margin: 3%;"><a id="numero_imagenes_seleccionado" style="color: #fff; font-size: 18.54;">  Imagenes () </a>
        </div>
        <div class="col-2 col-md-2 no_seleccionado" id="Elemento5" onclick="mostrar(this.id,'Elemento6','section_documentos')" style="cursor: pointer;">
            <img src="{{ url('/') }}/images/explorador/carpeta_gris.png" style="margin: 3%;"><a id="numero_documentos" style="color: #959699; font-size: 18.54;">  Documentos () </a>
        </div>
        <div class="col-2 col-md-2 seleccionado" id="Elemento6" style="cursor: pointer; background-color: rgb(116, 146, 232); display: none;">
            <img src="{{ url('/') }}/images/explorador/carpeta_blanca.png" style="margin: 5%;"><a id="numero_documentos_seleccionado" style="color: #fff; font-size: 18.54;">  Documentos () </a>
        </div>
        <div class="col-2 col-md-2" style="display: none;">
           <img src="{{ url('/') }}/images/explorador/min.png" style="margin-top: -7.5%"><input type="range" min="0" max="100" /><img src="{{ url('/') }}/images/explorador/max.png" style="margin-top: -7.5%">
        </div>
        <div class="col-2 sol-md-2" style="display: none;">
            <input type="text" class="form-control texto" id="buscar" name="buscar" placeholder='&#xf002;'>
        </div>
    </div>

  <div class="row">

    <div class="col-md-12">
        <section id="section_archive" class="row text-center placeholders"></section> <hr/>
        <section id="section_videos" class="row text-center placeholders secciones-mostrar" style="margin-bottom: 5%;"></section>
        <section id="section_documentos" style="display: none; margin-bottom: 5%;" class="row text-center placeholders secciones-mostrar"></section>

        <div id="con-img" class="secciones-mostrar" style="margin-top: 5%; display: none; margin-bottom: 5%;"></div>

    </div>
    <div class="col-md-12 secciones-mostrar" id="galeria_images_plano" style="margin-bottom: 5%; display: none;">
      <div class="row" style="margin: 2%;" id="galeria_imagenes">
        <div class="col-md-2">
          <div class="row">
            <div class="col-md-10 sombra">
              <img class="img-fotos" src="/images/file/productos/597f2ff3f2199.png" style="width: 100%;">
<<<<<<< HEAD
            </div>          
=======
            </div>
>>>>>>> origin/master
            <div class="col-md-2">
              <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
              <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
              <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
            </div>
          </div>
          <div class="row titulo-imagen">
            <div class="col-md-10">
              <h4 class="letra-imagen">Maquina ESSI T12</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <iframe id="galeria_images_3d" class="secciones-mostrar" style="display: none; border: 0 !important; height: 430px !important;" src="{{ url('/') }}/components/3d_carusel/index.html" name="mipagina" width="100%" height="300" framedorber="1">Tu navegador no soporta iframes</iframe>
    </div>

  </div>

</div>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-primary waves-effect waves-light abrir-modal" data-toggle="modal" data-target="#cargararchivo" style="display: none;">Small Modal</button>
<!-- Central Modal Small -->
<div class="modal fade show" id="cargararchivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <!--Content-->
        <div class="modal-content" style="background-color: #fff;">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel">Cargar Archivo</h4>
            </div>
            <!--Body-->



            <div class="modal-body">
                <form id="formulario_archivos" action="{{ url('creararchivomega') }}" method="POST" class="col s12 formValidate" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="url" id="url" value="">
                <input type="hidden" id="titulo" value="@if(isset($titulo)) {{ $titulo }} @else {{ $carpeta }} @endif">
                <input type="hidden" name="codigo_html" id="codigo_html" value="">
                <input type="hidden" name="tiempo" id="tiempo-new" value="{{ $tiempo }}">
                <input type="hidden" name="usuario" value="{{$usuario_megarchivo->id}}">
                  <div class="file-field">
                      <div class="btn btn-primary btn-sm">
                          <span>Archivo</span>
                          <input type="file" class="form-control" name="archivo" required="required">
                      </div>
                  </div>
                <!--Footer-->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light" data-dismiss="modal">Cerrar</button>
                      <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Guardar</button>
                  </div>
                </form>
            </div>

        </div>
        <!--/.Content-->
    </div>
</div>


<button type="button" class="btn btn-primary waves-effect waves-light abrir-modal2" data-toggle="modal" data-target="#reemplazararchivo" style="display: none;">Small Modal</button>
<!-- Central Modal Small -->
<div class="modal fade show" id="reemplazararchivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <!--Content-->
        <div class="modal-content" style="background-color: #fff;">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel">Reemplazar Archivo</h4>
            </div>
            <!--Body-->
            <div class="modal-body">
                <form id="formulario_reemplazararchivos" action="{{ url('reemplazararchivomega') }}" method="POST" class="col s12 formValidate" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <input type="hidden" name="url2" id="url2" value="">
                <input type="hidden" name="url3" id="url3" value="">
                <input type="hidden" name="tiempo" id="tiempo-reemplazar" value="{{ $tiempo }}">
                <input type="hidden" name="nombre_archivo" id="nombre_archivo" value="">
                <input type="hidden" name="extencion" id="extencion" value="">
                <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
                  <div class="file-field">
                      <div class="btn btn-primary btn-sm">
                          <span>Archivo</span>
                          <input type="file" class="form-control" name="archivo" required="required">
                      </div>
                  </div>
                <!--Footer-->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light" data-dismiss="modal">Cerrar</button>
                      <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Guardar</button>
                  </div>
                </form>
            </div>

        </div>
        <!--/.Content-->
    </div>
</div>
  <!-- Modal -->
  <div id="visor_imagenes" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1 !important; background-color: rgba(0, 0, 0, 0.61);">

    <div class="modal-dialog" style="max-width: 100%; margin-top: 5% !important;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="top: -2%; color: #fff;">&times;</button>
      <!-- Modal content-->
      <div class="modal-content" style="background-color: transparent; border: 0;">

        <div class="modal-body">
          <iframe id="visualizador_imagenes" style="border: 0 !important;" src="" name="mipagina" width="100%" height="800" framedorber="1">Tu navegador no soporta iframes</iframe>
        </div>

      </div>

    </div>
  </div>
<div id="inferior" style="bottom: 0; position: fixed;"><MARQUEE>ESSI, Empresa numero uno en soluciones de empaque flexible para procesos asepticos UTH.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{ url('/') }}/images/logo.png" class="img-peque logo-essi2"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ESSI, Empresa numero uno en soluciones de empaque flexible para procesos asepticos UTH.</MARQUEE></div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/contador.js"></script>
    <script src="{{ url('/') }}/js/main1.js"></script>

    <script type="text/javascript">


    function abrir_visor(rut, url, nombre){
      $('#visor_imagenes').modal('show');
      $('#visualizador_imagenes').attr('src','{{ url("/") }}/visualizador_imagenes/'+rut);
      abrir_imagen(url, nombre);
    }
    //$('#galeria_images_3d').attr('src','{{ url("/") }}/carruselimagenes/'+$('#archivo-ruta').val());
    $('#usuario').css('display','none');

        function launchFullScreen(element) {
            if(element.requestFullScreen) {
                element.requestFullScreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            }
        }
        pantallaCompletaVideo = function () {
            launchFullScreen(document.getElementById("play_video_modal"));
        }
        // Tooltips Initialization
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });

        direccion="{{ url('/') }}"
        carpeta="{{ $carpeta }}";
        @if(isset($re))
          re="{{$re}}";
        @else
        re="";
        @endif
        $(function () {
            buscarfolderandfile(carpeta,'A0','',re);
        });
        $(function(){
          guardarruta(carpeta,id_tiempo);
        });

        mostrar = function(id, id2, seccion){
            $('.seleccionado').css('display','none');
            $('.no_seleccionado').css('display','block');
            $('#'+id).css('display','none');
            $('#'+id2).css('display','block');

            $('.secciones-mostrar').css('display','none');
            $('#'+seccion).css('display','block');
        }

        $( ".img-fotos" ).hover(
          function() {
            $( this ).addClass( "animated" ).addClass( "bounce" );
          }, function() {
            $( this ).removeClass( "animated" ).removeClass( "bounce" );
          }
        );

    </script>
@endsection

@endif
