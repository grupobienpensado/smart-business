<!DOCTYPE html>
<html >
<style type="text/css">
	body{
		background-color: transparent !important;
	}
</style>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css" crossorigin="anonymous">
  <link rel='stylesheet prefetch' href='https://raw.githubusercontent.com/JohnBlazek/codepen-resources/master/3d-carousel/css/stylesheet.css'>

  <link rel="stylesheet" href="{{ url('/') }}/components/3d_carusel/css/style.css">
<script>
  $(document).ready(function(){
    $('.carousel').carousel({
      interval: 2000
    });
  });
</script>
  
</head>

<body>
	
	<div id="contentContainer" class="trans3d"> 
	<section id="carouselContainer" class="trans3d">
		{{ print($html) }}	
	</section>
	</div>

  <script src='https://raw.githubusercontent.com/JohnBlazek/codepen-resources/master/3d-carousel/js/libs.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="{{ url('/') }}/components/3d_carusel/js/index.js"></script>
<script src="{{ url('/') }}/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script type="text/javascript">
function abrir_visor(ru){
  window.parent.abrir_visor(ru);
}
	
</script>
</body>
</html>
