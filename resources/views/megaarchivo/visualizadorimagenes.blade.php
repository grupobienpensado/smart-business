<!DOCTYPE html>
<?php
$nombre=str_replace("¬", "/", $ru);

$carpeta=explode("/",$nombre);
        
$ruta_carpeta='';
for($i=0;$i<=((count($carpeta)) - 2);$i++){
    $ruta_carpeta.=$carpeta[$i].'/';
}
$archivo=str_replace($ruta_carpeta, "", $nombre);
$ruta=substr($ruta_carpeta, 0, -1);
?>
<html lang="en">
    <head>
        <title>Responsive Image Gallery</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
        <meta name="description" content="Responsive Image Gallery with jQuery" />
        <meta name="keywords" content="jquery, carousel, image gallery, slider, responsive, flexible, fluid, resize, css3" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/css/style.css" />
		<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/css/elastislide.css" />
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css' />
		<noscript>
			<style>
				.es-carousel ul{
					display:block;
				}
				.rg-caption-wrapper{
					display: none;
				}
			</style>
		</noscript>
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				
				<div class="rg-image" style="height: 500px;"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>
    </head>
    <body>
		<div class="container">
						
			<div class="content">
				
				<div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">
								<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel" style="height: 20%">
								<ul>
									<li><a href="#" onclick="abrir_imagen_visualizador('{{$ruta}}','{{$archivo}}')"><img src="{{ url('/') }}/storage/{{ $nombre }}" data-large="{{ url('/') }}/storage/{{ $nombre }}" alt="image01" data-description="" /></a></li>
									{{ print($html) }}
								</ul>
							</div>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div><!-- rg-thumbs -->
				</div><!-- rg-gallery -->
				
			</div><!-- content -->
		</div><!-- container -->
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/js/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/js/jquery.elastislide.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/visualizador/ResponsiveImageGallery/js/gallery.js"></script>
    </body>
</html>

<script type="text/javascript">
	function abrir_imagen_visualizador(url, nombre){
	  window.parent.abrir_imagen(url, nombre);
	}
</script>