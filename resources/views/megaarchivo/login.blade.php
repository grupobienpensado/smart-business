@extends('template.app2')

@section('title', 'Login-Mega Archivo')

@section('content')
<style type="text/css">
	#menu_mega{
		display: none;
	}
	.cont-inicio{
		border-radius: 10px;
	}
	.texto{
		border-radius: 10px;
	}
	input.texto {
	    font-family: FontAwesome;
	    font-style: normal;
	    font-weight: normal;
	    text-decoration: inherit;
	    margin-bottom: 1%;
	}
	button.boton{
		margin-top: 4%;
		border-style: solid;
		border-width: 1px;
		border-color: rgb(186, 186, 186);
		border-radius: 5px;
		background-image: -moz-linear-gradient( 90deg, rgb(3,144,245) 0%, rgb(1,62,124) 100%);
		  background-image: -webkit-linear-gradient( 90deg, rgb(3,144,245) 0%, rgb(1,62,124) 100%);
		  background-image: -ms-linear-gradient( 90deg, rgb(3,144,245) 0%, rgb(1,62,124) 100%);
		  cursor: pointer;
	}

	.bg-danger{
		border-radius: 10px 10px 10px 10px;
		-moz-border-radius: 10px 10px 10px 10px;
		-webkit-border-radius: 10px 10px 10px 10px;
		border: 0px solid #000000;
	}
	#inferior{
		position: absolute;
		bottom: 0;
	}
</style>

<div class="row" style="width: 98%">
	<div class="col s12 m12 l12">
		<div align="center" class="embed-responsive embed-responsive-16by9" style="position: fixed; background-color: black;">
		    <video autoplay loop class="embed-responsive-item" muted>
		        <source src="{{ url('/') }}/storage/MEGAARCHIVO/mac17.mp4" type="video/mp4">
		    </video>
		</div>
	</div>
</div>
<div class="row">
	<div class="col s12 m12 l12 cont-inicio animated slideInUp" style="background-color: #010d24; max-width: 242px; height: 290px; position: absolute; right: 2%; bottom: 0; opacity: 0.64; text-align: center;">
		<form id="login_megaarchivo" method="POST" action="{{ url('loginmegaarchivo') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<img id="imagen_essi" src="{{ url('/') }}/images/megaarchivo/essi_mac.png" style="width: 40%">
			<h4 class="texto-inicio" style="font: 'Raleway Regular'; margin-top: 5%; color: #a1cbfa">Iniciar Sesión</h4>			
			<!--<img src="{{ url('/') }}/images/megaarchivo/essi_mac_17.png" style="width: 33%; margin-left: 6%;">-->
			<input type="text" class="form-control texto" id="usuario" name="usuario" placeholder='&#xf007;' required="required">
			<input type="password" class="form-control texto" id="pw" name="contrasena" placeholder="&#xf023;" required="required">
			@if(isset($msj))
			<p class="bg-danger">{{ $msj }}</p>
			
			@endif
			<button type="submit" class="btn btn-primary boton">Entrar</button>		
		</form>
	</div>
</div>
@endsection

@section('scripts')
<script>

setTimeout('quitar_animacion()',2000);
quitar_animacion=function(){
	$('.cont-inicio').removeClass('animated').removeClass('slideInUp');
}

@if(isset($msj))
$('#imagen_essi').css('width','30%');
@endif
</script>
@endsection