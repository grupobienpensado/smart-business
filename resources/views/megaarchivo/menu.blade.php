<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app2')

@section('title', 'Menú-Mega Archivo')

@section('content')

@php 

$gridster="existe";

@endphp


@if(!isset($tiempo))
	@php
		header('Location: '.url('/').'/megaarchivo');
	@endphp
@else
<script> id_tiempo='{{ $tiempo }}'; id_tiempo=parseInt(id_tiempo); </script>
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/gridster/demos/assets/css/demo.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/gridster/dist/jquery.gridster.min.css">
    <script src="{{ url('/') }}/css/gridster/lib/jquery.min.js"></script>
    <script src="{{ url('/') }}/css/gridster/dist/jquery.gridster.min.js" type="text/javascript" charset="utf-8"></script>
<!-- CSS para switch -->
<style type="text/css">
	.onoffswitch {
    position: relative; width: 133px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
	    display: none;
	}
	.onoffswitch-label {
	    display: block; overflow: hidden; cursor: pointer;
	    border: 2px solid #999999; border-radius: 20px;
	}
	.onoffswitch-inner {
	    display: block; width: 200%; margin-left: -100%;
	    transition: margin 0.3s ease-in 0s;
	}
	.onoffswitch-inner:before, .onoffswitch-inner:after {
	    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
	    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
	    box-sizing: border-box;
	}
	.onoffswitch-inner:before {
	    content: "Diseño";
	    padding-left: 8px;
	    background-color: #051D60; color: #FFFFFF;
	}
	.onoffswitch-inner:after {
	    content: "configuración";
	    padding-right: 8px;
	    background-color: tranparent; color: #051D60;
	    text-align: right;
	}
	.onoffswitch-switch {
	    display: block; width: 12px; margin: 9px;
	    background: #FFFFFF;
	    position: absolute; top: 0; bottom: 0;
	    right: 99px;
	    border: 2px solid #999999; border-radius: 20px;
	    transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
	    margin-left: 0;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
	    right: 0px; 
	}
</style>
<style>

	.gridster .gs-w {
		background: transparent;
	}
	#div1 {
	    position: relative;
	    height: 150px;
	    width: 150px;
	    margin: 50px;
	    padding: 10px;
	    /*border: 1px solid black;*/
	    -webkit-perspective: 150px; /* Chrome, Safari, Opera  */
	    perspective: 150px;
	}

	#div2 {
	    padding: 80px;
	    width: 150px;
	    position: absolute;
	    /*border: 1px solid black;*/
	    background-image: -moz-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -webkit-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -ms-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(-15deg);
	     box-shadow: 2px 48px 9px #999;
	     -webkit-box-shadow: 2px 48px 9px #999;
	     -moz-box-shadow: 2px 48px 9px #999;
	}
	#div3 {
	    
	    width: 160px;
	    height: 80px;
	    position: absolute;
	    bottom: -85px;
	    background-color: #051d60;
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(-15deg);
	    color: white;
	    font-weight: bold;
	    font-size: 25px;
	}

	#div4 {
	    padding: 80px;
	    width: 150px;
	    position: absolute;
	    /*border: 1px solid black;*/
	    background-image: -moz-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -webkit-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -ms-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(15deg);
	     box-shadow: 2px 48px 9px #999;
	     -webkit-box-shadow: 2px 48px 9px #999;
	     -moz-box-shadow: 2px 48px 9px #999;
	}

	#div5 {
	    
	    width: 160px;
	    height: 80px;
	    position: absolute;
	    bottom: -85px;
	    background-color: #051d60;
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(15deg);
	    color: white;
	    font-weight: bold;
		    font-size: 25px;
		}
		body{
			background-attachment: fixed;
			background-image: url({{url('/')}}/storage/MEGAARCHIVO/fondomega.jpg);
			background-repeat: no-repeat;
			-webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
			background-position: center center;
		}

		.logo_essi{

		  -webkit-animation-duration: 3s;
		  -webkit-animation-delay: 2s;
		  -webkit-animation-iteration-count: infinite;

		}
		.animated{
			cursor: pointer;
		}
		.contenido-menu{
			width: 80%;
			margin-left: 9%;
		}
</style>
<style type="text/css">
	.btn-outline-primary:hover {
		color: #fff !important;
	}
	.flotante-btn {
	    position: fixed;
	    padding-top: 15px;
	    margin-bottom: 0;
	    z-index: 998;
	    bottom: 45px;
	    right: 24px;
	    cursor: pointer;
	}

	#div10 {
	    position: relative;
	    height: 150px;
	    width: 150px;
	    margin: 0px;
	    padding: 10px;
	    /*border: 1px solid black;*/
	    -webkit-perspective: 150px; /* Chrome, Safari, Opera  */
	    perspective: 150px;
	    margin-left: -40%; 
	    margin-top: 12%
	}

	#div20 {
	    padding: 50px;
	    width: 130px;
	    height: 140px;
	    position: absolute;
	    /*border: 1px solid black;*/
	    background-image: -moz-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -webkit-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -ms-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(-15deg);
	     box-shadow: 2px 48px 9px #999;
	     -webkit-box-shadow: 2px 48px 9px #999;
	     -moz-box-shadow: 2px 48px 9px #999;
	}
	#div30 {
	    
	    width: 130px;
	    height: 60px;
	    position: absolute;
	    bottom: -42px;
	    background-color: #051d60;
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(-15deg);
	    color: white;
	    font-weight: bold;
	    font-size: 25px;
	    left: 50%;
	}

	#div40 {
	    padding: 80px;
	    width: 150px;
	    position: absolute;
	    /*border: 1px solid black;*/
	    background-image: -moz-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -webkit-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
		  background-image: -ms-linear-gradient( 90deg, rgb(189,189,189) 0%, rgb(222,222,222) 24%, rgb(255,255,255) 100%);
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(15deg);
	     box-shadow: 2px 48px 9px #999;
	     -webkit-box-shadow: 2px 48px 9px #999;
	     -moz-box-shadow: 2px 48px 9px #999;
	}

	#div50 {
	    
	    width: 160px;
	    height: 80px;
	    position: absolute;
	    bottom: -85px;
	    background-color: #051d60;
	    -webkit-transform: rotateX(45deg); /* Chrome, Safari, Opera  */
	    transform: rotateY(15deg);
	    color: white;
	    font-weight: bold;
		    font-size: 25px;
		}
		body{
			background-attachment: fixed;
			background-image: url({{url('/')}}/storage/MEGAARCHIVO/fondomega.jpg);
			background-repeat: no-repeat;
			-webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
			background-position: center center;
		}
	.img-circle {
	    border-radius: 50%;
	    -webkit-box-shadow: 2px 2px 5px #999;
	}
</style>
<style type="text/css">
	.diseno_viejo .gs-w{
		-webkit-box-shadow: 0 0 0px rgba(0, 0, 0, 0.3) !important;
		box-shadow: 0 0 0px rgba(0, 0, 0, 0.3) !important;
	}
</style>
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/hover.css">
{{ csrf_field() }} 

<div id="cerrarlasesion" style="background: #fff; position: absolute; right: 2%; top: 6%; width: 240px; height: 36px; display: none;">
  <a class="dropdown-item" href="{{ url('/')}}/megaarchivo" style="color: #636b6f; font-size: 15px; margin-top: 3%;">Cerrar sesión</a>
</div>
<div class="contenido-menu" style="display: none;">
	<div class="row">
		<div class="col s6 m6 l6">
			<div class="row">
				<div class="col s6 m6 l6" style="color: transparent;">
					Lado izquierdo
				</div>
				<div class="col s3 m3 l3 animated fadeInDownBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','A2')"><a>
					  <div id="div4" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/MEGAARCHIVO/A2.png" style="position: fixed; top: 15%; right: 18%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div5"> <div style="margin: 30%; margin-top: 15%; text-align: center;">A2</div> </div>
					  </a>
					</div>		
				</div>
				<div class="col s3 m3 l3 animated fadeInUpBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','BAGGER')">
					  <div id="div2" class="hvr-sweep-to-right"><img src="{{ url('/') }}/storage/MEGAARCHIVO/BAGGER.png" style="position: fixed; top: 5%; right: 20%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div3"> <div style="margin: 30%; margin-top: 15%; text-align: center;">Bagger</div> </div>
					</div>		
				</div>

			</div>
		</div>
		<div class="col s6 m6 l6">
			<div class="row">
				<div class="col s3 m3 l3 animated fadeInDownBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','CIP AUTOMATICO')">
					  <div id="div4" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/MEGAARCHIVO/CIP.png" style="position: fixed; top: 15%; right: 18%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div5"> <div style="margin: 10%; margin-top: 5%; text-align: center;">CIP Automatico</div> </div>
					</div>
				</div>
				<div class="col s3 m3 l3 animated fadeInUpBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','A3 PLUS')">
					  <div id="div2" class="hvr-sweep-to-right"><img src="{{ url('/') }}/storage/MEGAARCHIVO/A3.png" style="position: fixed; top: 4%; right: 12%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div3"> <div style="margin: 10%; margin-top: 15%; text-align: center;">A3 PLUS</div> </div>
					</div>
				</div>
				<div class="col s12 m12 l12" style="color: transparent;">
					Lado Derecho
				</div>
			</div>
					
		</div>
	</div>

	<div class="row">
		<div class="col s4 m4 l4">
			<div class="row" style="margin-top: 5%">
				
				<div class="col s3 m3 l3 animated fadeInLeftBig" style="margin-left: 15%">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','BAGGER')">
					  <div id="div4" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/MEGAARCHIVO/BAGGER.png" style="position: fixed; top: 9%; right: 35%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div5"> <div style="margin: 20%; margin-top: 15%; text-align: center;">Bagger</div> </div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="col s4 m4 l4 logo_essi animated bounceIn" style="margin-top: 10%">
			<img src="{{ url('/') }}/storage/MEGAARCHIVO/essi_menu.png" class="mx-auto d-block" alt="...">
		</div>
		<div class="col s4 m4 l4">
			<div class="row" style="margin-top: 5%">
				<div class="col s3 m3 l3 animated fadeInRightBig" style="margin-left: 33%">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','CIP AUTOMATICO')">
					  <div id="div4" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/MEGAARCHIVO/CIP.png" style="position: fixed; top: 15%; right: 18%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div5"> <div style="margin: 10%; margin-top: 5%; text-align: center;">CIP Automatico</div> </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s6 m6 l6">
			<div class="row">
				<div class="col s6 m6 l6" style="color: transparent;">
					Lado izquierdo
				</div>
				<div class="col s3 m3 l3 animated fadeInUpBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','CIP AUTOMATICO')">
					  <div id="div2" class="hvr-sweep-to-right"><img src="{{ url('/') }}/storage/MEGAARCHIVO/CIP.png" style="position: fixed; top: 15%; right: 18%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div3"> <div style="margin: 10%; margin-top: 5%; text-align: center;">CIP Automatico</div> </div>
					</div>		
				</div>
				<div class="col s3 m3 l3 animated fadeInDownBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','CIP AUTOMATICO')">
					   <div id="div4" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/MEGAARCHIVO/CIP.png" style="position: fixed; top: 15%; right: 18%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div5"> <div style="margin: 10%; margin-top: 5%; text-align: center;">CIP Automatico</div> </div>
					</div>		
				</div>

			</div>
		</div>
		<div class="col s6 m6 l6">
			<div class="row">
				<div class="col s3 m3 l3 animated fadeInUpBig">
					<div id="div1" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','BAGGER')">
					 <div id="div2" class="hvr-sweep-to-right"><img src="{{ url('/') }}/storage/MEGAARCHIVO/BAGGER.png" style="position: fixed; top: 5%; right: 20%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div3"> <div style="margin: 30%; margin-top: 15%; text-align: center;">Bagger</div> </div>
					</div>
				</div>
				<div class="col s3 m3 l3 animated fadeInDownBig">
					<div id="div1" class="view overlay hm-red-strong" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','A2')">
					  <div id="div4" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/MEGAARCHIVO/A2.png" style="position: fixed; top: 15%; right: 18%;" class="mx-auto d-block" alt="..."></div>
					  <div id="div5"> <div style="margin: 30%; margin-top: 15%; text-align: center;">A2</div> </div>
					</div>
				</div>
				<div class="col s12 m12 l12" style="color: transparent;">
					Lado Derecho
				</div>
			</div>
					
		</div>
	</div>
</div>

	@if($func->tipousuario == 'Administrador Sistema')

	@php $display='none'; $displayuno=''; @endphp

	@else

	@php $display=''; $displayuno='none'; @endphp

	@endif

<div class="row" style="margin-top: 2%; display: {{ $displayuno }}">
	<div class="col s6 m6 l6">

	</div>
	<div class="col s6 m6 l6" style="text-align: center;">
		<div class="onoffswitch">
		    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="diseno" checked>
		    <label class="onoffswitch-label" for="myonoffswitch">
		        <span class="onoffswitch-inner"></span>
		        <span class="onoffswitch-switch"></span>
		    </label>
		</div>
	</div>
</div>


	<div class="gridster diseno_viejo" style="margin-top: 2%"> 

	</div>  

	<div class="gridster" id="grilla" style="display: none;">
	    <ul style="background-color: transparent;" class="cuadros">

	    </ul>
	</div>
<div class="flotante-btn" style="display: none;">
    <a class="btn btn-outline-primary btn-rounded waves-effect" style="color: #0275d8;" onclick="guardar()">Guardar</a>
 </div>

<form action="{{ url('/') }}/megaarchivo/documentos" method="POST" class="col s12" enctype="multipart/form-data" style="display: none;"> 
	{{ csrf_field() }}
	<input type="hidden" name="carpeta" id="carpeta-documentos" value="">
	<input type="hidden" name="tiempo" id="tiempo-documentos" value="">
	<button type="submit" class="btn btn-info mb-2" id="btn-documentos">Ingresar<i class="fa fa-send ml-1"></i></button>
</form>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/js/contador.js"></script>
<script type="text/javascript" id="code">
desplegar=0;
$('body').on('click','.dropdown-toggle',function(){
	if(desplegar==0){
		$('#cerrarlasesion').css('display','block');
		desplegar=1;
	}else{
		$('#cerrarlasesion').css('display','none');
		desplegar=0;
	}
});

$('#usuario').css('display','none');

tamano_pantalla=screen.width;
nuemro_columnas=(tamano_pantalla/170);
numero_columnas=Math.trunc(nuemro_columnas);

imagen=['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''];
nombre=['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''];
<?php $i=1; foreach ($datos as $dato) { ?>
	imagen[<?php echo $i ?>]='<?php echo $dato["imagen"] ?>';
	nombre[<?php echo $i ?>]='<?php echo $dato["nombre"] ?>';
	
<?php $i++; } ?>

html='';
j=1;
centro=0;
for(f=1;f<=5;f++){
	for(c=1;c<=numero_columnas;c++){
		if((imagen[j]!="" || nombre[j]!="") && (typeof(imagen[j]) != "undefined" || typeof(nombre[j]) != "undefined")){
			/*html+=`<li data-row="`+f+`" data-col="`+c+`" data-sizex="1" data-sizey="1">
		        	<div class="animated fadeInDownBig">
						<div id="div1" style="margin: 0px; margin-left: -40%; margin-top: 12%"><a >
						  <div id="div4" style="padding: 70px; width: 150px;" class="hvr-sweep-to-left"><img src="{{ url('/') }}/storage/`+imagen[j]+`" style="position: fixed; top: 15%; right: 28%; max-width: 98px; max-height: 100px;" class="mx-auto d-block" alt="..."></div>
						  <div id="div5" style="left: 50%; width:151px; height:60px; bottom: -42px;"> <div style="margin-top: 10%; text-align: center;">`+nombre[j]+`</div> </div>
						  </a>
						</div>		
					</div>
		        </li>`;*/
			html+=`<li data-row="`+f+`" data-col="`+c+`" data-sizex="1" data-sizey="1">
			        	<div class="animated fadeInUpBig" >
							<div id="div10">
							  <div id="div20" class="hvr-sweep-to-right" onclick="documentos_dir('{{ url('/') }}/megaarchivo/documentos','`+nombre[j]+`')"><img src="{{ url('/') }}/storage/MEGAARCHIVO/`+imagen[j]+`" style="position: fixed; top: 15%; right: 18%; max-width: 98px; max-height: 100px;" class="mx-auto d-block" alt="..."></div>
							  <div id="div30"> <div style="font-size:17px; margin-top: 10%; text-align: center;">`+nombre[j]+`</div> </div>
							</div>		
					</div>
			        </li>`;
		}else{
			if(centro==0){
				html+=`<li data-row="`+f+`" data-col="`+c+`" data-sizex="1" data-sizey="1"><div class="logo_essi animated bounceIn" style="margin-top: 40%">
							<img src="{{ url('/') }}/storage/MEGAARCHIVO/essi_menu.png" class="mx-auto d-block" alt="...">
						</div></li>`;
				centro++;
			}else{
				html+=`<li data-row="`+f+`" data-col="`+c+`" data-sizex="1" data-sizey="1"></li>`;	
			}
		}
		j++;
	}
}
$('.cuadros').html(html);

$(function(){ //DOM Ready

    $(".gridster ul").gridster({
        widget_margins: [10, 10],
        widget_base_dimensions: [160, 260]
    });

});


guardar = function () {

  event.preventDefault();    
  	menu=$('#grilla').html();
 	menu = menu.replace(/"/gi, '%%');
 	menu = menu.replace(/'/gi, "°°");  
  
    swal({   title: "Esta Seguro?",   
      text: "Va a guardar el menu",   
      type: "info",   showCancelButton: true,   
      closeOnConfirm: false,   
      showLoaderOnConfirm: true, 
    }).then(function(){   
      /*var jqxhr = $.post("{{ url('menuguardar') }}", { html: menu})
      .fail(function(e) {
        alert( "error" );
      })
      .always(function(e) {
        console.log(e);
        swal(e.mensaje);
               
          setTimeout(function(){window.location.replace("{{ url('megaarchivo/menu') }}");}, 4000);           
        
      });*/
     	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	    var jqxhr = $.ajax({ 
	        url: "{{ url('/') }}/menuguardar", 
	        cache: false,
	        type: 'POST',
	        data:{"html":menu,"_token":CSRF_TOKEN},
	        success: function(e){ 
	        	console.log(e.msg);
	        },
	        fail: function(e){ 
	        	swal({ title: "Error"});
	        }
	    });
    });  
  
}

$('body').on('click','#myonoffswitch',function(){
	if($(this).val() == 'diseno'){
		$(this).val('configuracion');
		$('.diseno_viejo').css('display','none');
		$('#grilla').css('display','');
		$('.flotante-btn').css('display','');
	}else{
		$(this).val('diseno');
		$('.diseno_viejo').css('display','');
		$('#grilla').css('display','none');
		$('.flotante-btn').css('display','none');
	}
	//alert($(this).val());
})



function documentos_dir(direc,carpeta){
	$('#carpeta-documentos').val(carpeta);
	$('#tiempo-documentos').val(id_tiempo);
	$('#btn-documentos').click();
}

@foreach($menu as $diseno_anterior)
	@php
		$diseno_anterior->menu=str_replace("%%", '"', $diseno_anterior->menu);
		$diseno_anterior->menu=str_replace("°°", "'", $diseno_anterior->menu);
	@endphp
	
	diseno_ant=`<?php print $diseno_anterior->menu; ?>`;
	
@endforeach
setTimeout(function(){
  $('.diseno_viejo').html(diseno_ant);
}, 2000);


</script>
@endsection

@endif