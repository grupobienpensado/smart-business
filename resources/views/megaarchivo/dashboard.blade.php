@extends('template.app2')

@section('content')

@if(!isset($tiempo))
	@php
		header('Location: '.url('/').'/megaarchivo');
	@endphp
@else
<script type="text/javascript">
	tiempo='{{ $tiempo }}';
</script>
<?php
function dateDiff($start, $end) {

	$start_ts = strtotime($start);

	$end_ts = strtotime($end);

	$diff = $end_ts - $start_ts;

	return round($diff / 3600);

}
?>
<style type="text/css">
	.img-circle {
	    border-radius: 50%;
	    -webkit-box-shadow: 2px 2px 5px #999;
	}
	tspan {
	    font-size: small;
	    color: #000;
	}	
	text {
		fill: #063cd2;
	}
	.amcharts-scrollbar-horizontal{
		display: none;
	}
	#chartdiv text{
		fill:#fff;
	}
	.amExportButton{
		display: none;
	}
</style>
<script type="text/javascript"> fecha_actual='{{ date("Y-m-d") }}'; </script>
@if(!isset($fi)) @php $fi=date('Y-m-01'); @endphp @endif
@if(!isset($ff)) @php $ff=date('Y-m-d'); @endphp @endif
<div class="row" style="margin-bottom: 2%; margin-top: 2%;">
	<div class="col-md-4" style="color: #fff">
		Lado Izquierdo
	</div>
	<div class="col-md-2">
		<label for="form5">Fecha Inicio</label>
		<input placeholder="Placeholder" name="fechainicio" id="fechainicio" type="date" class="form-control" value="{{ $fi }}">
	</div>
	<div class="col-md-2">
		<label for="form5">Fecha Fin</label>
		<input placeholder="Placeholder" name="fechafin" id="fechafin" type="date" class="form-control" value="{{ $ff }}">
	</div>
	<div class="col-md-4" style="color: #fff">
		Lado Derecho
	</div>
</div>
<div class="row">
	<div class="col-md-2" style="background-color: #002c4d; margin-left:3%; margin-right: 1%; height: 450px;">
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/carpeta.png">
			</div>
			<div class="col-md-5" style="margin: 1%; margin-top: 3%; color: #fff;">
				<a>Carpetas</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_carpetas">18</a>
			</div>
		</div>
		<div class="row" style="text-align: center;">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-6">
				<img src="{{ url('/') }}/images/icons_dashboard/linea_separadora.png">	
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/video.png">
			</div>
			<div class="col-md-5" style="margin: 1%;  margin-top: 3%; color: #fff;">
				<a>Videos</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_videos">18</a>
			</div>
		</div>
		<div class="row" style="text-align: center;">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-6">
				<img src="{{ url('/') }}/images/icons_dashboard/linea_separadora.png">	
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/imagenes.png">
			</div>
			<div class="col-md-5" style="margin: 1%;  margin-top: 3%; color: #fff;">
				<a>Imágenes</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_imagenes">18</a>
			</div>
		</div>
		<div class="row" style="text-align: center;">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-6">
				<img src="{{ url('/') }}/images/icons_dashboard/linea_separadora.png">	
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/pdf.png">
			</div>
			<div class="col-md-5" style="margin: 1%;  margin-top: 3%; color: #fff;">
				<a>Pdf</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_pdf">18</a>
			</div>
		</div>
		<div class="row" style="text-align: center;">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-6">
				<img src="{{ url('/') }}/images/icons_dashboard/linea_separadora.png">	
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/word.png">
			</div>
			<div class="col-md-5" style="margin: 1%;  margin-top: 3%; color: #fff;">
				<a>Word</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_word">18</a>
			</div>
		</div>
		<div class="row" style="text-align: center;">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-6">
				<img src="{{ url('/') }}/images/icons_dashboard/linea_separadora.png">	
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/excel.png">
			</div>
			<div class="col-md-5" style="margin: 1%;  margin-top: 3%; color: #fff;">
				<a>Excel</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_excel">18</a>
			</div>
		</div>
		<div class="row" style="text-align: center;">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-6">
				<img src="{{ url('/') }}/images/icons_dashboard/linea_separadora.png">	
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="margin: 1%; margin-top: 3%;">
				<img src="{{ url('/') }}/images/icons_dashboard/otros.png">
			</div>
			<div class="col-md-5" style="margin: 1%; margin-bottom: 3%;  margin-top: 3%; color: #fff;">
				<a>Otros</a>
			</div>
			<div class="col-md-3" style="margin: 1%; margin-top: 3%; color: #0087ff !important;">
				<a id="total_otros">18</a>
			</div>
		</div>
		<div class="row" style="background-color: #0087ff;">
			<div class="col-md-12" style="color: #fff; text-align: center; margin: 3%;">
				<a id="tamano">18.6 Gb Información</a>
			</div>
		</div>						
	</div>
	<div class="col-md-7" style="background-color: #1c1c28; height: 450px;">
		<div id="chartdiv" style="height: 453px;"></div>
	</div>
	<div class="col-md-2">
		@foreach($usuarios as $usuario)
		<div class="row" style="margin-bottom: 6%; cursor: pointer;" onclick="abrir_detalle({{ $usuario->id }})">
			<div class="col-md-4">
				<img src="{{ url('/') }}/storage/FUNCIONARIO/{{ $usuario->foto }}" style="max-width: 75px; max-height: 95px;" class="img-circle">
			</div>
			<div class="col-md-8" style="margin-top: 4%;">
				<a style="font-size: 17px; color: #063cd2;"><strong>{{ $usuario->nombre }}</strong></a><br>
				@php
				$entro=0;
				$tiempo_duracion=0;
				@endphp
				@foreach($tiempomegaarchivos as $ti)
					@if(($usuario->id)==($ti->id_user))
						@php
						$entro++;
						$tiempo_duracion+=dateDiff($ti->fecha_ingreso, $ti->fecha_retiro);
						@endphp
					@endif
				@endforeach
				<a style="font-size: 17px; color: #5f5f5f;">{{ $entro }} Accesos - {{ $tiempo_duracion }} Horas</a>
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="row" style="margin-top: 2%;">
	<div class="col-md-6" style="background-color: #fff; margin-left:3%;">
		<center><h3>Los diez archivos mas consultados</h3></center>
		<div id="chartdiv1" style="height: 453px;"></div>
	</div>
	<div class="col-md-5" style="background-color: #fff; margin-left:1%;">
		<center><h3>Las cinco carpetas mas consultadas</h3></center>
		<div id="chartdiv2" style="height: 453px;"></div>
	</div>
</div>
<!-- Large modal -->
<button type="button" style="display: none;" class="btn btn-primary abrir_modal" data-toggle="modal" data-target=".detalle_fun">Large modal</button>

<div class="modal fade detalle_fun" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="row" style="margin-top: 2%;">
      	<div class="col-md-4">
      		
      	</div>
      	<div class="col-md-4" id="imagen_funcionario">
      		
      	</div>
      	<div class="col-md-4">
      		
      	</div>
      </div>
      <center><h3><strong id="nombre_funcionario"></strong></h3></center>
      <div class="row" style="margin-bottom: 2%;">
      	<div class="col-md-1">
      		
      	</div>
      	<div class="col-md-10">
      		<!--Accordion wrapper-->
			<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
			    
			</div>
			<!--/.Accordion wrapper-->
      	</div>
      	<div class="col-md-1">
      		
      	</div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ url('/') }}/js/main2.js"></script>
<script src="{{ url('/') }}/js/dashboardmega/amcharts.js"></script>
<script src="{{ url('/') }}/js/dashboardmega/serial.js"></script>
<script src="{{ url('/') }}/js/dashboardmega/export.min.js"></script>
<link rel="stylesheet" href="{{ url('/') }}/js/dashboardmega/export.css" type="text/css" media="all" />
<script src="{{ url('/') }}/js/dashboardmega/dark.js"></script>
<script src="{{ url('/') }}/js/dashboardmega/light.js"></script>
<script src="{{ url('/') }}/js/dashboardmega/pie.js"></script>

<script type="text/javascript">

$('#usuario').css('display','none');

	abrir_detalle = function(id){
		f_inicio=$('#fechainicio').val();
		f_fin=$('#fechafin').val();
		$('#imagen_funcionario').html('<center>Cargando....</center>');
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	    var jqxhr = $.ajax({ 
	        url: "{{ url('/') }}/dashboardmega/funcionario", 
	        cache: false,
	        type: 'POST',
	        data:{"id":id,"f_inicio":f_inicio, "f_fin":f_fin,"_token":CSRF_TOKEN},
	        success: function(e){ 
	        	$('#nombre_funcionario').html(e.funcionario_consultado);
	        	$('#imagen_funcionario').html('<center><img src="{{ url('/') }}/storage/FUNCIONARIO/'+e.foto+'" style="max-width: 75px; max-height: 95px;" class="img-circle"></center>');
	        	$('#accordion').html(e.html);
	        	
	        },
	        fail: function(e){ 
	        	swal({ title: "Error"});
	        }
	    });
	    $('.abrir_modal').click();
	}

	function consultar_funcionario1(id,id2){
		$('#collapse'+id2).html('Cargando........');
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		var jqxhr = $.ajax({ 
	        url: "{{ url('/') }}/dashboardmega/funcionario1", 
	        cache: false,
	        type: 'POST',
	        data:{"id":id,"_token":CSRF_TOKEN},
	        success: function(e){ 
	        	$('#collapse'+id2).html(e.html);	        	
	        },
	        fail: function(e){ 
	        	swal({ title: "Error"});
	        }
	    });
	}

	function abrir_col(id){
		 if ($('#'+id).hasClass('show')){
		 	$('#'+id).removeClass('show');
		 }else{
		 	$('#'+id).addClass('show');
		 }
	}
	
</script>

<script type="text/javascript">
	var chart = AmCharts.makeChart("chartdiv", {
	  "type": "serial",
	  "theme": "dark",
	  "precision": 2,
	  "valueAxes": [{
	    "id": "v1",
	    "title": "",
	    "position": "left",
	    "autoGridCount": false,
	    "labelFunction": function(value) {
	      return  Math.round(value);
	    }
	  }, {
	    "id": "v2",
	    "title": "",
	    "gridAlpha": 0,
	    "position": "right",
	    "autoGridCount": false
	  }],
	  "graphs": [{
	    "id": "g3",
	    "valueAxis": "v1",
	    "lineColor": "#d2fcd3",
	    "fillColors": "#d2fcd3",
	    "fillAlphas": 1,
	    "type": "column",
	    "title": "# Estudio",
	    "valueField": "sales2",
	    "clustered": false,
	    "columnWidth": 0.5,
	    "legendValueText": "[[value]]",
	    "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
	  }, {
	    "id": "g4",
	    "valueAxis": "v1",
	    "lineColor": "#76ff03",
	    "fillColors": "#76ff03",
	    "fillAlphas": 1,
	    "type": "column",
	    "title": "# Clientes",
	    "valueField": "sales1",
	    "clustered": false,
	    "columnWidth": 0.3,
	    "legendValueText": "[[value]]",
	    "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
	  }, {
	    "id": "g1",
	    "valueAxis": "v2",
	    "bullet": "round",
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "bulletSize": 10,
	    "hideBulletsCount": 50,
	    "lineThickness": 2,
	    "lineColor": "#20acd4",
	    "type": "smoothedLine",
	    "title": "Hora Estudio",
	    "useLineColorForBulletBorder": true,
	    "valueField": "market1",
	    "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
	  }, {
	    "id": "g2",
	    "valueAxis": "v2",
	    "bullet": "round",
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "bulletSize": 10,
	    "hideBulletsCount": 50,
	    "lineThickness": 2,
	    "lineColor": "#ff0000",
	    "type": "smoothedLine",
	    "dashLength": 5,
	    "title": "Hora Cliente",
	    "useLineColorForBulletBorder": true,
	    "valueField": "market2",
	    "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
	  }],
	  "chartScrollbar": {
	    "graph": "g1",
	    "oppositeAxis": false,
	    "offset": 30,
	    "scrollbarHeight": 50,
	    "backgroundAlpha": 0,
	    "selectedBackgroundAlpha": 0.1,
	    "selectedBackgroundColor": "#888888",
	    "graphFillAlpha": 0,
	    "graphLineAlpha": 0.5,
	    "selectedGraphFillAlpha": 0,
	    "selectedGraphLineAlpha": 1,
	    "autoGridCount": true,
	    "color": "#AAAAAA"
	  },
	  "chartCursor": {
	    "pan": true,
	    "valueLineEnabled": true,
	    "valueLineBalloonEnabled": true,
	    "cursorAlpha": 0,
	    "valueLineAlpha": 0.2
	  },
	  "categoryField": "date",
	  "categoryAxis": {
	    "parseDates": false,
	    "dashLength": 1,
	    "minorGridEnabled": true
	  },
	  "legend": {
	    "useGraphSettings": true,
	    "position": "top"
	  },
	  "balloon": {
	    "borderThickness": 1,
	    "shadowAlpha": 0
	  },
	  "export": {
	   "enabled": true
	  },
	  "dataProvider": [
	   <?php 
		$cont=1; 
		
		foreach($usuarios as $u){ 
			$entro_oportunidad=0;
			$tiempo_duracion_oportunidad=0;
			$entro_estudiar=0;
			$tiempo_duracion_estudiar=0;
			
			foreach($tiempomegaarchivos as $t){ 
				if(($u->id)==($t->id_user)){
					if($t->tipo_ingreso === 'oportunidad'){
						$entro_oportunidad++;
						$tiempo_duracion_oportunidad+=dateDiff($t->fecha_ingreso, $t->fecha_retiro);
					}else{
						$entro_estudiar++;
						$tiempo_duracion_estudiar+=dateDiff($t->fecha_ingreso, $t->fecha_retiro);
					}
				} 
			} 
	if($cont!=1){ echo ','; } ?>
	   {
	   	/* {{ $u->nombre }} */
	    "date": "{{ $u->nombre }}",
	    "market1": <?php echo $tiempo_duracion_estudiar ?>,
	    "market2": <?php echo $tiempo_duracion_oportunidad ?>,
	    "sales1": <?php echo $entro_oportunidad ?>,
	    "sales2": <?php echo $entro_estudiar ?>
	  }
	   <?php $cont++; } ?>
	  /*{
	    "date": "2013-01-16",
	    "market1": 71,
	    "market2": 75,
	    "sales1": 5,
	    "sales2": 8
	  }, {
	    "date": "2013-01-17",
	    "market1": 74,
	    "market2": 78,
	    "sales1": 4,
	    "sales2": 6
	  }, {
	    "date": "2013-01-18",
	    "market1": 78,
	    "market2": 88,
	    "sales1": 5,
	    "sales2": 2
	  }, {
	    "date": "2013-01-19",
	    "market1": 85,
	    "market2": 89,
	    "sales1": 8,
	    "sales2": 9
	  }*/]
	});

</script>
<script type="text/javascript">
	var chart = AmCharts.makeChart("chartdiv1", {
	    "theme": "light",
	    "type": "serial",
		"startDuration": 2,
	    "dataProvider": [
	    <?php
	    	function sort_by_orden ($a, $b) {
			    return $a[1] - $b[1];
			}
	    ?>

	    <?php usort($diez, 'sort_by_orden'); for($y=((count($diez))-1); $y>=((count($diez))-10); $y--){ if($y!=((count($diez))-1)){ echo ','; } 
	    if(isset($diez[$y][0])){
	    ?>
	    	{
		        "country": "{{ $diez[$y][0] }}",
		        "visits": {{ $diez[$y][1] }},
		        "color": "#01579b"
		    }
	    <?php 
		}
	    }
	    	?>
	   /*{
	        "country": "archivo1",
	        "visits": 34,
	        "color": "#01579b"
	    }, {
	        "country": "archivo2",
	        "visits": 22,
	        "color": "#0277bd"
	    }, {
	        "country": "archivo3",
	        "visits": 19,
	        "color": "#0288d1"
	    }, {
	        "country": "archivo4",
	        "visits": 15,
	        "color": "#039be5"
	    }, {
	        "country": "archivo5",
	        "visits": 12,
	        "color": "#03a9f4 "
	    }, {
	        "country": "archivo6",
	        "visits": 10,
	        "color": "#29b6f6"
	    }, {
	        "country": "archivo7",
	        "visits": 10,
	        "color": "#4fc3f7"
	    }, {
	        "country": "archivo8",
	        "visits": 8,
	        "color": "#90caf9"
	    }, {
	        "country": "archivo9",
	        "visits": 6,
	        "color": "#81d4fa"
	    }, {
	        "country": "archivo10",
	        "visits": 4,
	        "color": "#b3e5fc"
	    }*/],
	    "valueAxes": [{
	        "position": "left",
	        "title": ""
	    }],
	    "graphs": [{
	        "balloonText": "[[category]]: <b>[[value]]</b>",
	        "fillColorsField": "color",
	        "fillAlphas": 1,
	        "lineAlpha": 0.1,
	        "type": "column",
	        "valueField": "visits"
	    }],
	    "depth3D": 40,
		"angle": 50,
	    "chartCursor": {
	        "categoryBalloonEnabled": false,
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "country",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "labelRotation": 20
	    },
	    "export": {
	    	"enabled": false
	     }

	});

</script>
<script type="text/javascript">
	var chart = AmCharts.makeChart( "chartdiv2", {
		  "type": "pie",
		  "theme": "dark",
		  "titles": [ {
		    "text": "",
		    "size": 16
		  } ],
		  "dataProvider": [ 
		   <?php $i=0; foreach($rutasusadas as $ruta){ if($i!=0){ echo ','; } ?>
	    	{
		        "country": "{{ $ruta->archivo }}",
		    	"visits": {{ $ruta->contador }}
		    }
	    	<?php $i++; } ?>
		  /*{
		    "country": "Carpeta1",
		    "visits": 75
		  }, {
		    "country": "Carpeta2",
		    "visits": 66
		  }, {
		    "country": "Carpeta3",
		    "visits": 48
		  }, {
		    "country": "Carpeta4",
		    "visits": 30
		  }, {
		    "country": "Carpeta5",
		    "visits": 18
		  }*/ ],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 2,
		  "labelRadius": 15,
		  "innerRadius": "60%",
		  "depth3D": 20,
		  "balloonText": "[[title]]<br><span style='font-size:16px'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 35,
		  "export": {
		    "enabled": false
		  }
		} );

</script>
@endsection

@endif
