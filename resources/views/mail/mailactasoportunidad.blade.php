
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php
    $acta = App\OportunidadActa::find($id);
    $vista = App\ViewOportunidades::find($acta->id_oportunidad);
    $maquinas = DB::SELECT('SELECT CONCAT("/images/file/productos/",R.foto) AS FOTO FROM oportunidad_productos OP INNER JOIN producto_referencias R ON OP.referencia = R.id WHERE OP.deleted_at IS NULL AND OP.oportunidad_id = '.$acta->id_oportunidad);
    $invitados = DB::SELECT('SELECT I.tipo, IF(I.tipo="usuario externo" ,"/images/file/clientes/essi_admon.jpg", CONCAT("/images/file/clientes/",(SELECT U.foto FROM users U WHERE U.id = I.id_usuario))) AS IMAGEN, IF(I.tipo="usuario externo",I.nombre,(SELECT CONCAT(SUBSTRING_INDEX(U.nombres, " ", 1), " ", SUBSTRING_INDEX(U.apellidos, " ", 1)) FROM users U WHERE U.id = I.id_usuario)) AS USUARIO FROM oportunidad_actas_invitados I WHERE I.id_acta = '.$id.' AND I.deleted_at IS NULL');
    $fecha_reunion = date("d-m-Y",strtotime($acta->fecha));
    $fecha_reunion_array = explode('-',$fecha_reunion);
    $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
    $fecha = $fecha_reunion_array[0].' '.$meses[intval($fecha_reunion_array[1])].' del '.$fecha_reunion_array[2];
    ?>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Reunión</title>
  <style type="text/css">
  body {margin: 0; padding: 0; min-width: 100%!important;}
  img {height: auto;}
  .content {width: 100%; max-width: 600px;}
      .header {padding: 40px 30px 20px 30px; background-image:url("{{url('/')}}/correo/banner.jpg"); background-repeat: no-repeat; height:232px;}
  .innerpadding {padding: 30px 30px 30px 30px;}
  .borderbottom {border-bottom: 1px solid #f2eeed;}
  .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
  .h1, .h2, .bodycopy {color: #0a1291; font-family: sans-serif;}
  .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
  .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
  .bodycopy {font-size: 16px; line-height: 22px;}
  .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
  .button a {color: #ffffff; text-decoration: none;}
  .footer {padding: 20px 30px 15px 30px;}
  .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
  .footercopy a {color: #ffffff; text-decoration: underline;}

  @media only screen and (max-width: 360px), screen and (max-device-width: 360px) {
  body[yahoo] .hide {display: none!important;}
  body[yahoo] .buttonwrapper {background-color: transparent!important;}
  body[yahoo] .button {padding: 0px!important;}
  body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
  body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
  }

  </style>
</head>

<body yahoo bgcolor="#f6f8f1">
<table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>
    <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="header" style="padding-bottom:0px;">
        </td>
      </tr>
      <tr>
        <td class="innerpadding borderbottom" style="padding-top:0px; padding-bottom:0px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="h2" colspan="2" style="text-align:center; border-bottom: 1px solid #00000047; padding: 0;">
                Hola
              </td>
            </tr>
            <tr>
              <td class="bodycopy" colspan="2" style="padding: 15px 0 0 0; text-align: center; color: #000;">
                <strong>Te invitamos a asistir</strong>
              </td>
            </tr>
			<tr>
              <td class="bodycopy" colspan="2" style="text-align: center; color: #000; padding: 0 0 15px 0;">
                <?=$acta->nombre?>
              </td>
            </tr>
			<tr bgcolor="#212679">
              <td class="bodycopy" style="text-align: right; color: #fff; width: 50%; border-right: 10px solid #212679; padding: 7px 0 7px 0;">
                <img src="{{url('/')}}/correo/fast-forward.png" width="5%" /><a style="color:#006DF0">Hora:</a> <?=date("h:i A",strtotime($acta->fecha))?>
              </td>
			  <td class="bodycopy" style="text-align: left; color: #fff; width: 50%; padding: 7px 0 7px 0;">
                <a style="color:#006DF0">Fecha:</a> <?=$fecha?>
              </td>
            </tr>
			<tr bgcolor="#212679">
              <td class="bodycopy" colspan="2" style="text-align: center; color: #fff; width: 50%; border-right: 10px solid #212679; border-top: 1px solid #fff; padding: 7px 0 7px 0;">
                <img src="{{url('/')}}/correo/fast-forward.png" width="3%" /><a style="color:#006DF0">Lugar:</a> <?=$acta->lugar?>
              </td>
            </tr>
			<tr>
				<td class="bodycopy" style="padding: 15px 0 0 0; text-align: right;">
					<img src="{{url('/')}}<?=$vista->EMPRESALOGO?>" width="60%"/>
				</td>
				<td class="bodycopy" style="padding: 15px 0 0 0; text-align: center;">
					<p><strong><?=$vista->empresa?></strong></p>
					<p style="color: #00000080"><?=$vista->pais?></p>
				</td>
			</tr>
			<tr>

			</tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="innerpadding borderbottom" style="padding-top:0px;">
          <table align="left" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #00000047">
           <?php for($i=1;$i<=count($maquinas);$i++){ ?>
           <?php if($i == 1){ ?>
            <tr>
           <?php } ?>
              <td style="padding: 0 20px 20px 0;">
                <img src="{{url('/')}}<?=$maquinas[$i-1]->FOTO?>" width="80%">
              </td>
            <?php if($i%4 == 0){ ?>
            </tr>
            <?php if($i<count($maquinas)){ ?>
            <tr>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if(count($maquinas)%4 != 0){ ?>
              </tr>
            <?php } ?>
          </table>

        </td>
      </tr>
      <tr>
        <td class="innerpadding bodycopy">
		  <p><img src="{{url('/')}}/correo/fast-forward.png" width="5%"/><strong style="font-size: 23px;">Descripción de la reunión</strong></p>
          <p style="color: #00000094; text-align: justify;"><?=$acta->descripcion?></p>

        </td>
      </tr>
	  <tr>
        <td class="bodycopy" style="text-align: center !important;">
			<p><img src="{{url('/')}}/correo/fast-forward.png" width="2%"/><strong style="font-size: 12px;">Equipo de trabajo</strong></p>
		</td>
	  </tr>
	  <tr>
        <td class="bodycopy" style="text-align: center !important;">
        <?php foreach($invitados as $invitado){ if($invitado->tipo == "usuario sistema"){ ?>
			<img src="{{url('/')}}<?=$invitado->IMAGEN?>" width="10%" style="border-radius: 200px 200px 200px 200px;-moz-border-radius: 200px 200px 200px 200px;-webkit-border-radius: 200px 200px 200px 200px;border: 0px solid #000000;">
        <?php } } ?>
		</td>
	  </tr>
      <tr>
        <td class="bodycopy" style="text-align: center !important;">
			<p><img src="{{url('/')}}/correo/fast-forward.png" width="2%"/><strong style="font-size: 12px;">Invitados</strong></p>
		</td>
      </tr>
      <tr>
        <td class="bodycopy" style="text-align: center !important;">
           <p>
            <?php $i=1; foreach($invitados as $invitado){ if($invitado->tipo == "usuario externo"){ ?>
            <?=$i?>. <?=$invitado->USUARIO?><br>
            <?php $i++; } } ?>
            </p>
         </td>
      </tr>
      <tr>
        <td class="footer" style="background-image: url('{{url('/')}}/correo/pie.jpg'); background-repeat: no-repeat; height: 121px">

        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<!--analytics-->
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://tutsplus.github.io/github-analytics/ga-tracking.min.js"></script>
</body>
</html>
