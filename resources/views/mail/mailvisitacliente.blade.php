<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
<?php 
		$agenda = App\Agenda_cliente::find($id);
		$oportunidad = App\Oportunidades::find($agenda->oportunidad);
		$empresa = App\Empresa::find($oportunidad->empresa_id);
        $visitantes = App\Agenda_clientes_visitante::where("agenda_clientes", $id)->get();
        for ($i=0; $i < count($visitantes); $i++) { 
            $cliente = App\Cliente::find($visitantes[$i]->cliente);
            $visitantes[$i]->nombres = $cliente->nombres;
            $visitantes[$i]->apellidos = $cliente->apellidos;
            $visitantes[$i]->cargo = $cliente->cargo;
            $visitantes[$i]->tratamiento = $cliente->tratamiento;
            $visitantes[$i]->foto = $cliente->foto;
        }
        setlocale(LC_ALL, "es_CO.UTF-8"); 
?>
<title>{{ $oportunidad->empresa }}</title>
<style type="text/css">


#outlook a { padding:0; }
.ReadMsgBody { width:100%; }
.ExternalClass { width:100%; }
body { -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; -webkit-font-smoothing:antialiased;}
.yshortcuts, .yshortcuts a, .yshortcuts a:link, .yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {text-decoration:none !important; border-bottom:none !important; background:#000 !important;}
/*link style*/
a { color: #43a0dd; text-decoration:none; outline: none;}
a:hover { text-decoration:underline !important; }
a .img:hover{opacity:0.8;filter:alpha(opacity=80);}
.button a:hover { text-decoration:none !important; }
.sign-up a {text-decoration:none !important;}
.unsub a{ color:#929ba5; text-decoration:none !important;}
.unsub a:hover{ text-decoration:underline !important;}
@media only screen and (max-width: 640px) {

table[class~=wrap], table[class~=divider]{ width: 100% !important; }

}
@media only screen and (max-width: 600px) {

table[class~=header]{background-image: url({{ url('/') }}/images/mail/800-490.jpg) !important;}{background-size:800px 490px !important;}
table[class~=wrap], table[class~=divider] { width: 440px !important; }
table[class~=row] { width: 400px !important;}
table[class~=gray] { width:100% !important;}
table[class~=col2], table[class~=col3], table[class=bottom-right]{ width: 100% !important; }
table[class=bottom-right] td{ text-align:center !important;}
table[class~=mid] td { padding:10px !important;}
td[class~=general-td] { padding: 10px 10px 0 10px !important; }
td[class~=general-img-td] { padding: 10px !important;}
td[class~=general-banner-td] { padding:10px !important;}
table[class~=gray] .general-img-td { padding:30px !important;}
table[class~=mid] {float: none !important;margin-bottom: 20px !important;}
table[class~=logo], table[class~=menu] {width: 90% !important;float: none !important;margin: 0 auto 15px !important;}
table[class~=menu] .info { display: none !important; }
table[class~=menu] td {text-align: center !important;}
img { height: auto !important; }
img[class~=img] {width: 100% !important;height:auto !important;max-width: 100% !important;display: block !important;}
table[class~=header-img] h1{ font-size:35px !important; line-height:46px !important;}
table[class~=general-banner], table[class~=general-banner] table, table[class~=general-banner2], table[class~=general-banner2] table  { width:100% !important; height:auto !important;}
table[class~=gray] { background-size:100% 100% !important;}

}
@media only screen and (max-width: 439px) {

table[class~=img-rounded] { width:100% !important;}
table[class~=img-rounded] .in { padding:10px 0 !important;}
table[class~=header]{background-image: url({{ url('/') }}/images/mail/800-490.jpg) !important;}{background-size:800px 490px !important;}
table[class~=wrap], table[class~=divider], table[class~=row] { width: 100% !important;}
table[class~=logo] img { max-width: 100% !important; }
table[class~=logo], table[class~=menu] { width: 97% !important; }
table[class~=social-icon]{ width:100% !important;}
table[class~=social-icon] td{ padding:10px 2px !important;}

}
@media only screen and (max-width: 339px) {

table[class~=logo] img { max-width: 260px !important;}

}

table.wrap.header {
    background-repeat: no-repeat;
}
</style>
</head>

	<body style="margin:0;padding:0;width:100%;height:100%;">
		<div class="preheader" style="display:none; visibility:hidden; height:0px; font-size:0px; line-height:0px;">
			{{ $oportunidad->empresa }} 
		</div>
		<table class="BGtable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #fff;width: 100% !important;height: 100% !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td class="BGtable-inner" valign="top">
					<repeater>
						<!-- start ◆header-module-1s◆ -->
						<layout label="header-module-1s1">
							<table class="wrap header" style="
							    border-collapse: collapse;
							    background-color: #161616;
							    background-image: url({{ url('/')}}/images/mail/800-490.jpg);
							    background-repeat: no-repeat;
							    background-position: center;" localhost="" businessman="" html="" light="" images="800-490.jpg"  width:="650px"  margin="" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td class="in" style="padding: auto;">
										<!-- logo & menu -->
										<table class="header-top-bg" style="border-collapse: collapse;" align="center" background="{{ url('/') }}/images/mail/header-bg.png" border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td>
													<!--module-divider-->
													<table class="module-divider" style="border-collapse: collapse;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td height="30"><br></td>
														</tr>
													</table>
													<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
														<tr>
															<td class="general-img-td" style="padding: 10px;">
																<table class="logo" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="left" border="0" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" height="62"><img alt="logo" src="{{ url('/') }}/images/mail/logo.png" class="img-inline"  editable="true" label="logo" style="border: 0;display: inline;-ms-interpolation-mode: bicubic;" height="62" width="182">
																		</td>
																	</tr>
																	<tr>
																	<td height="20px">
																	<br>
																	</td>
																	</tr>
																</table>
																<table class="menu" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="right" border="0" cellpadding="0" cellspacing="0">
																	<tr>
																		<td class="info"  style="font-size: 11px;color: #cbcbcb;font-family: Tohama, Arial;" align="right" height="30" valign="middle"><singleline label="call me">Ingresa al Smart Business y conoce mas detalles de la Visita</singleline>
																		</td>
																	</tr>
																	<tr>
																		<td class="content home"  style="font-family: Tahoma,Arial; color: #71777f; font-size: 12px; line-height: 20px; font-weight: 400;" align="right" height="30" width="33%"><singleline label="home"></singleline>
																  		<span style="">
																				    <a style="overflow: hidden;
    text-decoration: none;
    color: #fff;
    background-color: #00c3ff;
    text-align: center;
    letter-spacing: .5px;
    font-size: 0.8rem;
    outline: 0;
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 26px;
    line-height: 26px;
    padding: 0 2rem;
    text-transform: uppercase;
    vertical-align: middle;" href="{{ url('login') }}">Iniciar Sesión</a></span></td>
																	</tr>
																	
																	
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<!-- header img -->
										<table class="header-img" style="border-collapse: collapse;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td class="in">
												<!--module-divider-->
													<table class="module-divider" style="border-collapse: collapse;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td height="30"><br></td>
														</tr>
													</table>
													<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
														<tr>
														  <td colspan="2" align="center" class="general-img-td"  style="padding: 10px; font-family: Impact, Haettenschweiler, 'Franklin Gothic Bold', 'Arial Black', sans-serif; font-size: 35px; color: #FFFFFF;"> Visita Cliente</td>
														  </tr>
														<tr>
											  				<td width="215" align="right" class="general-img-td"  style="padding: 10px;"><img src="@if(!empty($empresa->logo))
            {{ url('/') }}/images/file/empresas/principal/{{ $empresa->logo }}
         @else
            https://placeholdit.imgix.net/~text?txtsize=33&txt=Principal&w=350&h=150 
         @endif" width="149" height="146" alt="" style="border-radius: 50%;" /></td>
															<td width="385" align="left" valign="middle" class="general-img-td"  style="padding: 10px;"><h1 class="h1" style="font-family: Arial,Tohama;color: #fff;font-weight: 400;font-size: 40px;line-height: 30px;margin: 0 0 15px !important;">
											  					<singleline label="h1-title"><span style="font-size: 30px">{{ $oportunidad->empresa }}</span></singleline></h1>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table> 
						</layout>
						<!-- end - ◆header-module-1s◆ -->
						<!-- start ◆divider-40px-2as◆ -->
						<layout label="divider-40px-2as7">
							<table class="divider" style="border-collapse: collapse;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td class="wrap" style="background-color: #eeeeee; width: 650px; margin: 0px auto;" height="40"><img src="{{ url('/') }}/images/mail/spacer.gif"  editable="true" label="do not edit" style="border: 0;display: block;-ms-interpolation-mode: bicubic;" height="1" width="1">
									</td>
								</tr>
							</table> 
						</layout>
						<!-- end - ◆divider-40px-2as◆ -->
						<!-- start ◆full-content-6s◆ -->
						<layout label="full-content-6s1">
							<table class="wrap" style="border-collapse: collapse;background-color: #eee;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td>
										<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td class="general-img-td content"  style="font-family: Tahoma, Arial;color: #71777f;font-size: 13px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;padding: 10px;" align="center">
													<h3 class="h3" style="font-family: Arial,Tohama;color: #71777f;font-weight: 400;font-size: 25px;line-height: 37px;margin: 0 0 10px !important;">
														<singleline label="h3-title">Fecha inicio</singleline>
													</h3>
													<multiline label="content">
													<p style="font-family: Tahoma, Arial;color: #71777f;font-size: 23px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;margin: 0 0 10px !important;">
														{{ strftime("%d %B %Y", strtotime($agenda->fecha_inicio)) }}
													</p>
													</multiline>
												</td>
												<td class="general-img-td content"  style="font-family: Tahoma, Arial;color: #71777f;font-size: 13px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;padding: 10px;" align="center">
													<h3 class="h3" style="font-family: Arial,Tohama;color: #71777f;font-weight: 400;font-size: 25px;line-height: 37px;margin: 0 0 10px !important;">
														<singleline label="h3-title">Fecha fin</singleline>
													</h3>
													<multiline label="content">
													<p style="font-family: Tahoma, Arial;color: #71777f;font-size: 23px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;margin: 0 0 10px !important;">
													{{ strftime("%d %B %Y", strtotime($agenda->fecha_final)) }}</p></multiline>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table> 
						</layout>
						<!-- end - ◆full-content-6s◆ -->
						<!-- start ◆divider-40px-2as◆ -->
						<layout label="divider-40px-2as6">
							<table class="divider" style="border-collapse: collapse;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td class="wrap" style="background-color: #eeeeee; width: 650px; margin: 0px auto;" height="40"><img src="{{ url('/') }}/images/mail/spacer.gif"  editable="true" label="do not edit" style="border: 0;display: block;-ms-interpolation-mode: bicubic;" height="1" width="1">
									</td>
								</tr>
							</table> 
						</layout>
						<!-- end - ◆divider-40px-2as◆ -->
						<!-- start ◆banner-content-7s◆ -->
						<layout label="banner-content-7s1">
						<table class="wrap banner" style="border-collapse: collapse; background-color: #545252;background: #003b7d; background-repeat: no-repeat; background-position: center center; width: 650px; margin: 0px auto;" align="center" background="http://placehold.it/650x300/555/000&amp;text=650-300-img1" border="0" cellpadding="0" cellspacing="0" width="650">
						<tr>
						<td>
						<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
						<tr>
						<td height="15">
						<br>
						</td>
						</tr>
						<tr>
						<td class="general-img-td content s t"  style="font-family: Tahoma, Arial;color: #dfdfdf;font-size: 12px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;padding: 20px 30px;" align="center">
							<h2 class="h2" style="font-family: Arial,Tohama;color: #dfdfdf;font-weight: 400;font-size: 30px;line-height: 45px;margin: 0 0 15px !important;"><singleline label="h2-title">Acerca de la visita</singleline></h2><multiline label="content">
							<p style="font-family: Tahoma, Arial;color: #dfdfdf;font-size: 12px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;margin: 0 0 10px !important;">{{ $agenda->observaciones }}</p></multiline>
						</td>
						</tr>
						<tr>
						<td height="10">
						<br>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						</table> 
						</layout>
	<!-- end - ◆banner-content-7s◆ -->
	<layout label="divider-40px-2as7">
							<table class="divider" style="border-collapse: collapse;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td class="wrap" style="background-color: #eeeeee; width: 650px; margin: 0px auto;" height="40"><img src="{{ url('/') }}/images/mail/spacer.gif"  editable="true" label="do not edit" style="border: 0;display: block;-ms-interpolation-mode: bicubic;" height="1" width="1">
									</td>
								</tr>
							</table> 
						</layout>
						<!-- end - ◆divider-40px-2as◆ -->
						<!-- start ◆full-content-6s◆ -->
						<layout label="full-content-6s1">
							<table class="wrap" style="border-collapse: collapse;background-color: #eee;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td>
										<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr><?php foreach($visitantes as $visita){ ?>
												<td class="general-img-td content"  style="font-family: Tahoma, Arial;color: #71777f;font-size: 13px;line-height: 21px;font-weight: 400;-webkit-font-smoothing: antialiased;padding: 10px;" align="center">
																								
													<div class="media clearfix header-bottom custom-control-description">
						                        		<div class="media-left pull-left"><img src="@if(!empty($visita->foto))
              {{ url('/') }}/images/file/clientes/{{ $visita->foto }}
           @else
              http://via.placeholder.com/250x250/fff/948e8e?text=Foto 
           @endif" alt="Edinson Ossa" class="media-object img-circle" style="max-width:80px; border-radius: 50%;"></div>
								                        <div class="media-body">
								                            <a href="#">{{ $visita->tratamiento." ".$visita->nombres." ".$visita->apellidos }}</a><br> 
								                            <p class="text-muted username soloprimer normalp" style="padding: 0px !important;
    margin: 0px !important;">{{ $visita->cargo }}</p>                       
								                        </div>
						                    		</div>
				                    			
												</td>
												<?php } ?>
											</tr>
										</table>
									</td>
								</tr>
							</table> 
						</layout>
						<!-- end - ◆full-content-6s◆ -->
						<!-- start ◆divider-40px-2as◆ -->
						<layout label="divider-40px-2as6">
							<table class="divider" style="border-collapse: collapse;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
								<tr>
									<td class="wrap" style="background-color: #eeeeee; width: 650px; margin: 0px auto;" height="40"><img src="{{ url('/') }}/images/mail/spacer.gif"  editable="true" label="do not edit" style="border: 0;display: block;-ms-interpolation-mode: bicubic;" height="1" width="1">
									</td>
								</tr>
							</table> 
						</layout>
	<!-- start ◆divider-40px-2as◆ -->
	<layout label="divider-40px-2as5"></layout>
	<!-- end - ◆divider-40px-2as◆ -->
	<!-- start ◆col2-content-9s◆ -->
	<layout label="col2-content-9s4"></layout>
	<!-- end - ◆col2-content-9s◆ -->
	<!-- start ◆divider-80px-4s◆ -->
	<layout label="divider-80px-4s2"></layout>
	<!-- end - ◆divider-80px-4s◆ -->
	<!-- start ◆col2-content-9s◆ -->
	<layout label="col2-content-9s3">
	<table class="wrap" style="border-collapse: collapse;background-color: #eee;width: 650px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="650">
	<tr>
	<td>
	<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
	<td><table class="col2" style="border-collapse: collapse;width: 295px;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="right" border="0" cellpadding="0" cellspacing="0" width="295">
	  
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table> </layout>
	<!-- end - ◆col2-content-9s◆ -->
	<!-- start ◆divider-40px-2as◆ -->
	<layout label="divider-40px-2as4"></layout>
	<!-- end - ◆divider-40px-2as◆ -->
	<!-- start ◆banner-content-10s◆ -->
	<layout label="banner-content-10s2"></layout>
	<!-- end - ◆banner-content-10s◆ -->
	<!-- start ◆divider-40px-2as◆ -->
	<layout label="divider-40px-2as3"></layout>
	<!-- end - ◆divider-40px-2as◆ -->
	<!-- start ◆col2-content-9s◆ -->
	<layout label="col2-content-9s2"></layout>
	<!-- end - ◆col2-content-9s◆ -->
	<!-- start ◆divider-80px-4s◆ -->
	<layout label="divider-80px-4s1"></layout>
	<!-- end - ◆divider-80px-4s◆ -->
	<!-- start ◆col2-content-9s◆ -->
	<layout label="col2-content-9s1"></layout>
	<!-- end - ◆col2-content-9s◆ -->
	<!-- start ◆divider-40px-2as◆ -->
	<layout label="divider-40px-2as2"></layout>
	<!-- end - ◆divider-40px-2as◆ -->
	<!-- start ◆banner-content-10s◆ -->
	<layout label="banner-content-10s1"></layout>
	<!-- end - ◆banner-content-10s◆ -->
	<!-- start ◆divider-40px-2as◆ -->
	<layout label="divider-40px-2as1"></layout>
	<!-- end - ◆divider-40px-2as◆ -->
	<!-- start ◆full-title-12s◆ -->
	<layout label="full-title-12s1"></layout>
	<!-- end - ◆full-title-12s◆ -->
	<!-- start ◆full-content-23s◆ -->
	<layout label="full-content-23s1"></layout>
	<!-- end - ◆full-content-23s◆ -->
	<!-- start ◆divider-20px-2s◆ -->
	<layout label="divider-20px-2s1"></layout>
	<!-- end - ◆divider-20px-2s◆ -->
	<!-- start ◆footer-module-27s◆ -->
	<layout label="footer-module-27s1">
	<table class="wrap footer" style="border-collapse: collapse; background-color: #252525; background-image: url({{ url('/') }}/images/mail/lattice.png); background-repeat: repeat; background-position: center center; width: 650px; margin: 0px auto;" align="center" background="{{ url('/') }}/images/mail/lattice.png" border="0" cellpadding="0" cellspacing="0" width="650">
	<tr>
	<td>
	<table class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
	<td class="general-img-td" style="padding: 10px;" align="center">
	<!--module-divider-->

	<table class="bottom-right" style="border-collapse: collapse;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
	  <tr>
	<td class="general-img-td content"  style="font-family: Tahoma,Arial; color: #929ba5; font-size: 13px; line-height: 21px; font-weight: 400; padding: 10px;" align="center">
		<singleline label="address">Email: smart@essi.com.co</singleline><br style="line-height: 20px;">
		<singleline label="telephone">Telefono: (+57) 315 461 2700</singleline>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td class="general-img-td" style="padding: 10px;" align="center">&nbsp;</td>
	</tr>
	</table>
	</td>
	</tr>
	</table> </layout>
	<!-- end - ◆footer-module-27s◆ -->
	</repeater>
	</td>
	</tr>
	</table>
	</body>
</html>

