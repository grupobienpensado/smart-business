<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Visitas Clientes')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
    <style type="text/css">
      .tooltip-inner {
        white-space: pre-wrap;
      }
      .nuevo-tamano{
        width: 14%
      }
      .tamano30{
        width: 30px !important;
        max-width: 30px !important;
      }
      .unalinea{
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
      }

      .mas-pequeño{
        line-height: 1.5;
        font-size: small;
        color: #636363;
        font-weight: 400;
        display: block !important;
        margin: 0px !important;
      }

      .centrar-vertical{
        vertical-align: middle !important;
      }

      .justify{
        text-align: justify;
      }
    </style>
       <div class="container-fluid animated slideInDown">
      <div class="row">

        <div class="col-md-12 panel-view">
          <div class="pull-right">
            <div class="btn-group">
              <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
              <?php if($permiso_agendar=="Si"){ ?>
              <a href="{{url('/')}}/agendarvisita" class="btn btn-sm btn-success"><i class="fa fa-list" aria-hidden="true"></i> Agendar Reunión</a>
              <?php } ?>
            </div>
          </div>
          <h2>Listado de Visitas de Clientes</h2>
          <div class="table-responsive">
            <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
              <thead>
                <tr>                  
                  <th class="text-center tamano30">#</th>
                  <th class="text-center nuevo-tamano">cliente</th>
                  <th class="text-center nuevo-tamano">fecha inicio</th>
                  <th class="text-center nuevo-tamano">fecha final</th>
                  <th class="text-center nuevo-tamano">agendado desde</th>
                  <th class="text-center nuevo-tamano">agendado por </th>
                  <th class="text-center nuevo-tamano"># actividades </th>
                  <th class="text-center nuevo-tamano">acciones</th>
                </tr>
              </thead>
              <tbody>
                @php $i=1; $nombre='';  $nombre_usuario=''; @endphp 
                @foreach($lista as $dato)
                @foreach($oportunidades as $oportunidad)
                @php $cont=0; @endphp
                  @if($oportunidad->id==$dato->oportunidad)
                  @foreach($empresas as $empresa)
                    @if($empresa->id==$oportunidad->empresa_id)
                    @php
                    $nombre=$empresa->nombre;
                    break;
                    @endphp
                    @endif
                  @endforeach
                  @endif
                @endforeach
                @foreach($usuarios as $usuario)
                  @if($usuario->id==$dato->user_id)
                  @php
                  $nombre_usuario=$usuario->username;
                  break;
                  @endphp
                  @endif
                @endforeach
                 @foreach($visitantes as $visitante)
                  @if($visitante->agenda_clientes==$dato->id)
                  @php
                  $cont++;
                  @endphp
                  @endif
                @endforeach
                <tr>
                  <td class="text-center">{{ $i }}</td>
                  <td class="text-center">{{ $nombre }}</td>
                  <td class="text-center">{{ $dato->fecha_inicio }}</td>
                  <td class="text-center">{{ $dato->fecha_final }}</td>
                  <td class="text-center">{{ $dato->created_at }}</td>
                  <td class="text-center">{{ $nombre_usuario }}</td>
                  <td class="text-center">{{ $cont }} visitantes</td>
                  <td class="text-center"><a @if($dato->estado!="pendiente") href="{{ url('/') }}/agendacliente/{{ $dato->id }}" @else href="{{ url('/') }}/asignarresponsable/{{ $dato->id }}" @endif class="btn btn-sm btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                </tr>
                @php $i++; @endphp
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    
@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ visitas de clientes",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ visitas de clientes",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 visitas de clientes",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      }
    });
  });
</script>
@endsection