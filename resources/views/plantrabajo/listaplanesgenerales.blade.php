@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Plan de Trabajo')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/ripples.min.css"/>
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">
.form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }   

.row-2{
	padding: 50px;
}

.row-2 th{
	padding: .3rem !important;
}
.th{
	background-color: #eceeef;
	border-bottom: 2px solid #d2d3d4 !important;
}

.cuadro{
	width: 14.28%;
	min-width: 14.28%;
	background-color: #fff;
}
.minutero{
	text-align: right;
    border-bottom: 0.5px solid #ccc;
}
.actividad{
	margin: 2px;
    padding: 1px;
    text-align: center;
    border-radius: 5px;
    line-height: 1;
    color: #fff;
    z-index: 99;
}

.tr textarea{
	margin: 5px;
}
.alert {
    -webkit-animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    margin-bottom: 10px;
}

.alert-dark {
    background-color: rgba(228, 104, 104, 0.9);
    border-color: rgba(0, 0, 0, 0.8);
    color: #fff;
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}

.form-control[readonly] {
    background-color: #fff;
    opacity: 1;
}
.modal-body .form-group.row.block{
	margin-top: 65px;
}
</style>
@php


function fecha($str)
{
	$date = explode("-",$str);
	return $date[2] . "/". $date[1] ."/". $date[0];
}
$calculo=7-date("w");
$nuevafecha=strtotime("+".$calculo." Day",strtotime(date("Y-m-d")));
$domingo_siguiente = date ( 'Y-m-d' , $nuevafecha );

$nuevafecha2=strtotime("+6 Day",strtotime($domingo_siguiente));
$sabado_siguiente = date ( 'Y-m-d' , $nuevafecha2 );
$hora="00:30";

$expirar=false;

if(date('w')==6&&date("H:i:s")>'22:00:00'){
	$expirar=true;
}
@endphp
@for($o=0;$o<7; $o++)
@php
$vector=strtotime("+".$o." Day",strtotime($domingo_siguiente));
$vector = date ( 'Y-m-d' , $vector );

$dias[$o]=$vector;
@endphp	
@endfor
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 text-center inline-block">
      		<h3>Listado de actividades Semana {{date("W")+1}}</h3>
      		<h5>Semana del {{fecha($domingo_siguiente)." hasta el ".fecha($sabado_siguiente)}}</h5>
      	</div>
      	
      <div class="row-2">
      	<table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
          <thead>
          	<tr>
          		<th class="text-center th" width="50"> # </th>
          		<th class="text-center th">Fecha</th>
	          	<th class="text-center th">Hora inicio</th>
	          	<th class="text-center th">Hora final</th>
	          	<th class="text-center th">Oportunidad</th>
	          	<th class="text-center th">Tipo actividad</th>
              <th class="text-center th">Acciones</th>
          	</tr>
          </thead>
          <tbody>
           @php 
              $nombre_oportunidad="";
              $titulo_oportunidad="";
              $i=1;
              @endphp
          @foreach($descripciones as $value)
            @if($value->plan_trabajo==$id&&$value->user_id==$user)
            @foreach($oportunidades as $oportunidad)
                @if($oportunidad->id==$value->oportunidad)
                  @php 
                  $nombre_oportunidad=$oportunidad->empresa;
                  $titulo_oportunidad=$oportunidad->titulo;
                  break;
                  @endphp
                @endif
             @endforeach
          	<tr>
          		<td class="text-center">{{$i}}</td>
              <td class="text-center">{{$value->fecha}}</td>
              <td class="text-center">{{$value->hora_inicio}}</td>
              <td class="text-center">{{$value->hora_fin}}</td>
              <td class="text-center">{{$titulo_oportunidad}}</td>
              <td class="text-center">{{$value->tipo_actividad}}</td>
              <td class="text-center">
                <button class="btn btn-sm btn-warning" onclick="abrirModal({{$value->id}})"><i class="fa fa-pencil"></i></button>
              </td>
          	</tr>
            @php $i++; @endphp
            @endif
            @endforeach
          </tbody>
      	</table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guardar_evento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Editar actividad<span id="fecha"></span></h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
      <form method="POST" action="{{ url('editarplantrabajo') }}" enctype="multipart/form-data" id="form-modalEdit"></form>
    </div>    
    </div>
    </div>
  </div>

  <div class="modal fade" id="tiempo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Estadistica por Uso del Tiempo</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body ">
      <div class="row margen-10">
        <div class="col-md-12 mb" id="body-tiempo" style="background-color: #ddd">
          
        </div>
      </div>
    </div>    
    </div>
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script type="text/javascript" charset="utf-8">
$(function () {
  

  

$('.btn-submit').click(function(e){
     console.log("Entra -<> " + remaining);
  var element = $(this).parent().parent().parent();
  
  
  if(remaining < 0){
    e.preventDefault();
    // wigle and show notification
    // Wigle
    var element = $(this).parent().parent().parent();
    $(element).addClass('animated rubberBand');  
    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(element).removeClass('animated rubberBand');
    });
    
    $("#output").removeClass(' alert alert-success');
    $("#output").addClass("alert alert-danger animated flip").html("Perdón!, Es necesario que ingrese un detalle más amplio, el cual supere los 50 caracteres");
  }
});

  $('#oportunidad').select2({
      // Activamos la opcion "Tags" del plugin
      placeholder: "Seleccione un oportunidad",
      involucrado: true,
      tokenSeparators: [','],
      ajax: {
          dataType: 'json',
          url: '{{ url("soportunidad") }}',
          delay: 250,
          data: function(params) {
              return {
                  term: params.term
              }
          },
          processResults: function (data, page) {
            return {
              results: data
            };
          },
      }
  });

  $('.time').bootstrapMaterialDatePicker({
      time: true,
      date: false,
      clearButton: true,
      nowButton: true,
      lang: 'es',
      format : 'HH:mm',
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar',
      weekStart : 0 
  });
});  
function realizado(id){
		mo=$("#tr-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr-"+id).show("clip");
		}else{
			$("#tr-"+id).hide("clip");
		}
}

abrirModal = function(id) {
  $("#form-modalEdit").html(`<div id="loader"></div>`);
  $.ajax({
    url: "{{ url('buscarplantrabajo') }}/"+id,
    type: 'GET',
    success: function(data){
      if (data.success) {
        console.log(data.datos);
        actividadD = data.datos;
        html = `
          <input type="hidden" name="id" value="`+actividadD.id+`">
          <input type="hidden" name="idlist" value="{{ $id2 }}">
          
          {{ csrf_field() }}
          <div class="form-group row">
                <label for="oportunidad" class="col-2 col-form-label">Oportunidad</label>
                <div class="col-10">
                    <select name="oportunidad" class="form-control" id="oportunidad" required>
                      <option value="`+actividadD.oportunidad+`">`+data.titulo+`</option>
                    </select>
                </div>
          </div>
          <div class="form-group row">
              <div class="col-md-4">
                  <label for="hora_inicio" class="col-form-label">Hora de inicio</label>                   
                  <input class="form-control time" type="text" placeholder="00:00" name="hora_inicio" value="`+actividadD.hora_inicio+`">
              </div>
              <div class="col-md-4">
                  <label for="hora_final" class="col-form-label">Hora final</label>                   
                  <input class="form-control time" type="text" placeholder="00:00" name="hora_fin" value="`+actividadD.hora_fin+`">
              </div>
              <div class="col-md-4">
                  <label for="tipo" class="col-form-label">Tipo Actividad</label>
                  <select name="tipo" class="form-control" id="selectTipo" required>
                      <option value="">Seleccione una tipo de actividad</option>
                      <option value="Estimacion de inversiones y propuestas">Estimacion de inversiones y propuestas</option>
                      <option value="Identificacion de oportunidades en cliente">Identificacion de oportunidades en cliente</option>
                      <option value="Mejoramiento continuo">Mejoramiento continuo</option>
                      <option value="No aplica al cargo">No aplica al cargo</option>
                      <option value="Tramites de legalizacion y solicitud de viaticos">Tramites de legalizacion y solicitud de viaticos</option>
                      <option value="Ventas">Ventas</option>
                      <option value="Apertura de mercado">Apertura de mercado</option>
                      <option value="Gestion de aprobacion de ofertas">Gestion de aprobacion de ofertas</option>
                      <option value="Facturacion">Facturacion</option>
                      <option value="Planeacion">Planeacion</option>
                      <option value="Soporte tecnico cliente">Soporte tecnico cliente</option>
                      <option value="Desarrollo proveedores">Desarrollo proveedores</option>
                      <option value="Realizacion de Dosier">Realizacion de Dosier</option>
                      <option value="Tramites administrativos internos ESSI">Tramites administrativos internos ESSI</option>
                      <option value="Control y auditoria interna de proyectos">Control y auditoria interna de proyectos</option>
                      <option value="Soporte tecnico otro funcionario de Essi">Soporte tecnico otro funcionario de Essi</option>
                      <option value="Traslados">Traslados</option>
                      <option value="Permisos">Permisos</option>
                      <option value="Vacaciones">Vacaciones</option>
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-12">
                <label>Detalle exacto de las actividades</label>
                <div id="output"></div>
                  <textarea class="form-control" id="my-input" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones">`+actividadD.observaciones+`</textarea>
                  <small class="char-count"></small>
              </div>
          </div>
          <button type="submit" class="btn btn-essi pull-right btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Editar</button>`;
          
        $("#form-modalEdit").html(html);
        $("#selectTipo").val(actividadD.tipo_actividad);
        var count = 0;
        var input = $('#my-input'), display = $('.char-count'), count = 0, limit = -50;

        count = input.val().length
        var remaining = limit + count
        update(remaining);

        input.keyup(function(e) {
          count = $(this).val().length;
          remaining = limit + count;

          update(remaining);
        });

        function update(count) {
          var txt = ( Math.abs(count) === 1 ) ? count + ' Carácteres restantes' :  count + ' Carácteres restantes'
          display.html(txt);
        }

        $('#oportunidad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('.time').bootstrapMaterialDatePicker({
            time: true,
            date: false,
            clearButton: true,
            nowButton: true,
            lang: 'es',
            format : 'HH:mm',
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar',
            weekStart : 0 
        });
      }else{

      }
    }
  });



  

  $("#guardar_evento").modal();
}

</script>

@endsection