@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Plan de Trabajo')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/ripples.min.css"/>
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
.row-2{
	padding: 50px;
}

.row-2 th{
	padding: .3rem !important;
}
.th{
	background-color: #eceeef;
	border-bottom: 2px solid #d2d3d4 !important;
}

.cuadro{
	width: 14.28%;
	min-width: 14.28%;
	background-color: #fff;
}
.minutero{
	text-align: right;
    border-bottom: 0.5px solid #ccc;
}
.actividad{
	margin: 2px;
    padding: 1px;
    text-align: center;
    border-radius: 5px;
    line-height: 1;
    color: #fff;
    z-index: 99;
}

.tr td{
	padding: 13px !important;
}
.tr textarea{
	margin: 5px;
}
.alert {
    -webkit-animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    margin-bottom: 10px;
}

.alert-dark {
    background-color: rgba(228, 104, 104, 0.9);
    border-color: rgba(0, 0, 0, 0.8);
    color: #fff;
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}

.form-control[readonly] {
    background-color: #fff;
    opacity: 1;
}
.modal-body .form-group.row.block{
	margin-top: 65px;
}
#body-tiempo{
  height: 500px;
  width: 100%;
}
.amcharts-main-div a{
  display: none !important;
}
.mb { color: #fff; }

#body-tiempo text{
  font-size: 16px !important;
}
.margen-10{
  margin: 10px;
}
text tspan{
  x:10 !important;
}
</style>
@php
$expirar=false;
$id_semana=0;
function fecha($str)
{
  $date = explode("-",$str);
  return $date[2] . "/". $date[1] ."/". $date[0];
}
@endphp

@if(date("w")<2)
  @php
    $semana_gral=date("W");
    $nuevafecha=strtotime("-".date("w")." Day",strtotime(date("Y-m-d")));
    $domingo_siguiente = date ( 'Y-m-d' , $nuevafecha );

    $nuevafecha2=strtotime("+6 Day",strtotime($domingo_siguiente));
    $sabado_siguiente = date ( 'Y-m-d' , $nuevafecha2 );
      $expirar=true;
    
  @endphp
  @for($o=0;$o<7; $o++)
    @php
      $vector=strtotime("+".$o." Day",strtotime($domingo_siguiente));
      $vector = date ( 'Y-m-d' , $vector );

      $dias[$o]=$vector;
    @endphp	
  @endfor


@else 
@php
  $semana_gral=date("W")+1;
  $calculo=7-date("w");
  $nuevafecha=strtotime("+".$calculo." Day",strtotime(date("Y-m-d")));
  $domingo_siguiente = date ( 'Y-m-d' , $nuevafecha );

  $nuevafecha2=strtotime("+6 Day",strtotime($domingo_siguiente));
  $sabado_siguiente = date ( 'Y-m-d' , $nuevafecha2 );
@endphp
  @for($o=0;$o<7; $o++)
    @php
      $vector=strtotime("+".$o." Day",strtotime($domingo_siguiente));
      $vector = date ( 'Y-m-d' , $vector );

      $dias[$o]=$vector;
    @endphp 
  @endfor
@endif
<div class="container-fluid animated slideInDown">
<style type="text/css">
* {
  margin: 0;
  padding: 0;
}

*, *:before, *:after {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

.chart {
  display: -webkit-flex;
  display: flex;
  -webkit-align-items: flex-end;
  align-items: flex-end;
  height: 10em;
  margin: 0 auto;
  padding: 0 2em;
  position: relative;
  width: calc(100% - 4em);
}
.chart li {
  -webkit-flex: 1;
  flex: 1;
  list-style: none;
  position: relative;
  text-align: center;
}
.chart li:after {
  content: attr(data-label);
  color: white;
  display: block;
  margin: .75em 0 0 -2.75em;
  position: absolute;
  top: 100%;
  left: calc(40%);
  white-space: nowrap;
  -webkit-transform-origin: 0 0;
  -webkit-transform: skewy(15deg);
}
.chart:before {
  background-color: white;
  box-shadow: 0 2.5em 0 white, 0 5em 0 white, 0 7.5em 0 white, 0 10em 0 white;
  content: '';
  height: 1px;
  left: 0;
  opacity: .5;
  position: absolute;
  top: -1px;
  width: 100%;
}
.chart div {
  background-color: white;
  bottom: 0;
  display: inline-block;
  margin-left: -2em;
  -webkit-transform-origin: 0 0;
  transform-origin: 0 0;
  -webkit-transform: skewY(20deg);
  transform: skewY(20deg);
  width: 4em;
}
.chart div:before {
  background-color: #0083be;
  bottom: 100%;
  color: white;
  content: attr(data-value);
  line-height: 1.7em;
  left: 0;
  position: absolute;
  -webkit-transform-origin: 0 100%;
  transform-origin: 0 100%;
  -webkit-transform: skewX(-50deg);
  transform: skewX(-50deg);
  width: 4em;
}
.chart div:after {
  background-color: #0071a5;
  bottom: 0;
  content: '';
  height: 100%;
  left: 100%;
  position: absolute;
  width: 2em;
  -webkit-transform-origin: 0 0;
  transform-origin: 0 0;
  -webkit-transform: skewY(-40deg);
  transform: skewY(-40deg);
}
.chart div.grow {
  -webkit-animation: grow 1s 1 linear;
  animation: grow 1s 1 linear;
}

@-webkit-keyframes grow {
  0% {
    height: 0;
  }
}
@keyframes grow {
  0% {
    height: 0;
  }
}

.charts{
  background: #009fd6;
  padding: 100px;
}
.titulo-plan{
  color: #fff;
  margin-bottom: 15px;
}
</style>
@foreach ($planes as $value) 
        @if($value->semana==$semana_gral&&$value->user_id==Auth::user()->id)
          @php $id_semana=$value->id;
          break;
          @endphp
        @endif
@endforeach
      @php $valida='';
      $i=0;
      $array1=[];
      $cantidad=0; @endphp
      @foreach ($descripciones as $key)
      @if($key->user_id==$usuario) 
        @if($key->plan_trabajo==$id_semana)
          @if($key->tipo_actividad==$valida)
            @php
            list($hora,$min)=explode(":", $key->hora_inicio);
            list($horaf,$minf)=explode(":", $key->hora_fin);
            $ini=(($hora*60)*60)+($min*60);
            $fin=(($horaf*60)*60)+($minf*60);
            $dif=$fin-$ini;
            $cantidad=($dif/3600);
            //$array1[$i-1]["cantidad"]=$array1[$i-1]["cantidad"]+$cantidad;
            @endphp
          @else
          @php
            list($hora,$min)=explode(":", $key->hora_inicio);
            list($horaf,$minf)=explode(":", $key->hora_fin);
            $ini=(($hora*60)*60)+($min*60);
            $fin=(($horaf*60)*60)+($minf*60);
            $dif=$fin-$ini;
            $cantidad=($dif/3600);

            $valida=$key->tipo_actividad;
            $array1[$i]["actividad"]=$key->tipo_actividad;
            $array1[$i]["cantidad"]=$cantidad;
            $i++;
            @endphp
          @endif
        @endif

       @endif
      @endforeach
@php
$array1=burbuja($array1);
function burbuja($lista)
{
    for($i=1;$i<count($lista);$i++)
    {
        for($j=0;$j<count($lista)-$i;$j++)
        {
            if($lista[$j]["cantidad"]<$lista[$j+1]["cantidad"])
            {
                $k=$lista[$j+1];
                $lista[$j+1]=$lista[$j];
                $lista[$j]=$k;
            }
        }
    }
 
    return $lista;
}
@endphp
  <div class="row">
    <div class="col-sm-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
          <a href="javascript:location.href='{{url('/')}}/listaplanes'" class="btn btn-sm btn-warning"><i class="fa fa-arrow-right" aria-hidden="true"></i> Ver planes anteriores</a>
          <a href="javascript:location.href='{{url('/')}}/listaplangenerales/{{$semana_gral}}'" class="btn btn-sm btn-success"><i class="fa fa-list" aria-hidden="true"></i> Ver listado de actividades</a>
        </div>
      </div>
      	<div class="col-md-12 text-center inline-block titulo-plan">
      		<h3>Plan de Trabajo Semana {{$semana_gral}}</h3>
      		<h5>Semana del {{fecha($domingo_siguiente)." hasta el ".fecha($sabado_siguiente)}}</h5>
      		
      	</div>
      	<div class="charts">
            <ul class="chart">
              @for($o=0; $o<count($array1);$o++)
              <li data-label="{{$array1[$o]['actividad']}}"><div data-value="{{$array1[$o]['cantidad']}}"></div></li>
              @endfor
            </ul>
        </div>
      <div class="row-2">
      <div class="alert alert-dark bpx" role="alert" @if(!$expirar) style="display: none;" @endif>
            <strong>Advertencia!</strong> la hora de entrega del plan de trabajo ha expirado, esto afecta su indice de rendimiento
        </div>
      	<table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
          <thead>
          	<tr>
          		<th></th>
              <th class="text-center th">Domingo</th>
	          	<th class="text-center th">Lunes</th>
	          	<th class="text-center th">Martes</th>
	          	<th class="text-center th">Miercoles</th>
	          	<th class="text-center th">Jueves</th>
	          	<th class="text-center th">Viernes</th>
	          	<th class="text-center th">Sabado</th>
          	</tr>
          </thead>
          <tbody>
          	@php $color=''; $hora="00:30"; @endphp
          	@while($hora!="00:00")
          	<tr>
          		<td class="relativo minutero">
          			{{$hora}}
          		</td>
          		@for($l=0;$l<7;$l++)
             
          		@php $actividad=false; @endphp
          		<td class="cuadro guardar_evento" name="{{$dias[$l]}}" id="{{$hora}}">
	          			@php $nombre_oportunidad='';
		                  $titulo_oportunidad=''; 
		                  
		                  @endphp
	          			@foreach($descripciones as $value)
                  @if($value->user_id==$usuario)
	          			@if($value->fecha==$dias[$l]&&($value->hora_inicio<=$hora&&$value->hora_fin>=$hora))
	          				@foreach($oportunidades as $oportunidad)
                          @if($value->oportunidad=="Trabajos Generales ESSI")
                          @php 
                            $nombre_oportunidad=$value->tipo_actividad;
                            $titulo_oportunidad="No Aplica";
                            break;
                            @endphp
				                  @elseif($oportunidad->id==$value->oportunidad)
					                  @php 
					                  $nombre_oportunidad=$oportunidad->empresa;
					                  $titulo_oportunidad=$oportunidad->titulo;
					                  break;
					                  @endphp
				                  @endif
			                 @endforeach
	          				@php $actividad=true;
	          				$color=$value->color;
	          				$break;
	          				@endphp
	          			  @endif
                  @endif
	          			@endforeach
	          			<div @if($actividad) class="actividad" style="background-color: {{$color}};" @endif>{{$nombre_oportunidad}}</div>
                  
          		</td>
              
          		@endfor
          	</tr>
          	@php $nuevafecha=strtotime("+30 Minutes",strtotime($hora));
			       $hora = date ( 'H:i' , $nuevafecha ); @endphp
          	@endwhile
          </tbody>
      	</table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="guardar_evento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal" style="border: 0 !important">
      <h4 class="modal-title" id="myModalLabel">Guardar Nueva Actividad Para El Dia <span id="fecha"></span></h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
    	<form method="POST" action="{{ url('plantrabajosave') }}" enctype="multipart/form-data">
    	<input type="hidden" name="fecha" id="fecha_actividad">
    	<input type="hidden" name="semana" value="{{$semana_gral}}">
    	<input type="hidden" name="fecha_cierre" value="{{$sabado_siguiente}}">
      
    	{{ csrf_field() }}
    	<div class="form-group row">
            <label for="oportunidad" class="col-2 col-form-label">Oportunidad</label>
            <div class="col-10">
                <select name="oportunidad" class="form-control" id="oportunidad" required=""></select>
            </div>
        </div>
        <div class="row fila_msj" style="display: none;">
            <div class="col-md-12">
                <div class="alert alert-success">
                  <strong>Tipo Actividad</strong><br> <a id="msj_ta">Es muy importante que leas este mensaje de alerta.</a>
                </div>
            </div>
        </div>
    	<div class="form-group row">
	        <div class="col-md-4">
                <label for="hora_inicio" class="col-form-label">Hora de inicio</label>                   
                <input class="form-control" type="time" step="1800" name="hora_inicio" id="hora_inicio">
            </div>
            <div class="col-md-4">
                <label for="hora_final" class="col-form-label">Hora final</label>                   
                <input class="form-control time" type="text" placeholder="00:00" name="hora_fin">
            </div>
            <div class="col-md-3">
                <label for="tipo" class="col-form-label">Tipo Actividad</label>
                <select id="select_tipo_actividad" name="tipo" class="form-control" required="">
                    <option value="">Seleccione una tipo de actividad</option>
                    @foreach($tipos_actividades as $t_a)
                    @if(!empty($t_a->orden))
                    <option value="{{ $t_a->concepto }}">{{ $t_a->concepto }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <a class="tipo-act" data-toggle="tooltip" data-original-title="Ayuda tipo Actividad"><img src="{{ url('/') }}/images/pregunta.png" style="cursor: pointer; border: 0 !important; margin-top: 30%;"></a>
                <!--<a class="tipo-act" data-toggle="tooltip" data-original-title="Ayuda tipo Actividad"><i class="fa fa-question-circle" aria-hidden="true" style="cursor: pointer;"></i></a>-->
            </div>
	       </div>
	    	<div class="form-group row">
	          <div class="col-md-12">
	          	<label>Detalle exacto de las actividades</label>
              <div id="output"></div>
                <textarea class="form-control" id="my-input" required name="observaciones" rows="3" placeholder="Escriba aqui las observaciones"></textarea>
                <small class="char-count"></small>
	          </div>
	       </div>
	      <button type="submit" class="btn btn-essi pull-right btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
	    </form>
	    <div class="form-group row block" >
       		<div class="col-md-12" id="lista-actividades">
       			
       		</div>
       </div>
    </div>    
    </div>
    </div>
  </div>

  <div class="modal fade" id="tiempo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Estadistica por Uso del Tiempo</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body ">
      <div class="row margen-10">
        <div class="col-md-12 mb" id="body-tiempo" style="background-color: #ddd">
          
        </div>
      </div>
    </div>    
    </div>
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<!--tooltip-->
<script src="{{ url('/') }}/assets/js/king-common.js"></script>
<script src="{{ url('/') }}/demo-style-switcher/assets/js/deliswitch.js"></script>
<!--tooltip-->
<script type="text/javascript" charset="utf-8">

/* Ayuda tipo Actividad */
$('body').on('click','.tipo-act',function(){
    id=$('#select_tipo_actividad').val();
    if(id!=''){
        $.ajax({
        url: "{{ url('/') }}/ayudatipoactividad/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
          console.log(e.msj);
          $('#msj_ta').html(e.msj);
          $('.fila_msj').css('display','block');
        }
      });
    }    
})
/*Fin Ayuda Tipo Actividad */

var count = 0;
var input = $('#my-input'), display = $('.char-count'), count = 0, limit = -50;

  count = input.val().length
  var remaining = limit + count
  update(remaining);

  input.keyup(function(e) {
    count = $(this).val().length;
    remaining = limit + count;

    update(remaining);
  });

  function update(count) {
    var txt = ( Math.abs(count) === 1 ) ? count + ' Carácteres restantes' :  count + ' Carácteres restantes'
    display.html(txt);
  }

$('.btn-submit').click(function(e){
     console.log("Entra -<> " + remaining);
  var element = $(this).parent().parent().parent();
  
  
  if(remaining < 0){
    e.preventDefault();
    // wigle and show notification
    // Wigle
    var element = $(this).parent().parent().parent();
    $(element).addClass('animated rubberBand');  
    $(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(element).removeClass('animated rubberBand');
    });
    
    $("#output").removeClass(' alert alert-success');
    $("#output").addClass("alert alert-danger animated flip").html("Perdón!, Es necesario que ingrese un detalle más amplio, el cual supere los 50 caracteres");
  }
});

function realizado(id){
		mo=$("#tr-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr-"+id).show("clip");
		}else{
			$("#tr-"+id).hide("clip");
		}
}

$("body").on("click",".guardar_evento",function(e){
  $("#guardar_evento").modal();
})
function cancelar(id){
		mo=$("#tr2-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr2-"+id).show("clip");
		}else{
			$("#tr2-"+id).hide("clip");
		}
		
}

function ver(id){
		mo=$("#tr3-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr3-"+id).show("clip");
		}else{
			$("#tr3-"+id).hide("clip");
		}
		
}

$("body").on("click",".funcion",function(e){
  $("#tiempo").modal();
    id=$(this).attr("name");
  $("#lista-actividades").html('');
  setTimeout(function(e){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/listaplanesgenerales/"+id, 
        cache: false,
        type: 'GET',
        success: function(e){ 
          console.log(e.function)
          eval(e.function);
        }
    });},2000)

})

$("body").on("click",".cuadro",function(e){
	$("#fecha").html($(this).attr("name"));
	$("#fecha_actividad").val($(this).attr("name"));
	$("#hora_inicio").val($(this).attr("id"));
	fecha=$(this).attr("name");
	hora=$(this).attr("id");
	$("#lista-actividades").html('');
	setTimeout(function(e){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/consultarplanestrabajo/"+fecha, 
        cache: false,
        type: 'GET',
        data:{"hora":hora},
        success: function(e){ 
        	$("#lista-actividades").html(e.tabla);
        }
    });},2000)

})
$(function () {

  $('.time').bootstrapMaterialDatePicker({
      time: true,
      date: false,
      clearButton: true,
      nowButton: true,
      lang: 'es',
      format : 'HH:mm',
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar',
      weekStart : 0 
  });

  var $cheight = $(".chart").height();
  <?php if(count($array1)>0){ ?>
    $(".chart div").each(function(i) {
      var $value = $(this).data("value");
      $(this).css('height',$value * $cheight / {{$array1[0]["cantidad"]}}).addClass("grow");
    });
  <?php } ?>
    
        i = 0;
        $('#oportunidad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    })
</script>

@endsection