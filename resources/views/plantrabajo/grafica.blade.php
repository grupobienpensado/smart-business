@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Plan de Trabajo')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/morris.css">
<link rel="stylesheet" href="{{ url('/') }}/components/amchart/css/export.css" type="text/css" media="all" />
<style type="text/css">
	#chartdiv2 {
		float:left;
		width		: 100%;
		height		: 500px !important;
		font-size	: 11px;
	}
	#chartdiv {
		float:left;
		width		: 100%;
		height		: 500px !important;
		font-size	: 11px;
	}
</style>

<div class="card animated flipInX" id="list" style="margin-bottom: 30px;">
    <div class="card-block">
    	<div class="form-group row"> 
			<div class="col-2"></div>
            <div class="col-4">
                <select class="form-control" id="responsable"></select>
            </div> 
        </div>
    	<div id="chartdiv2"></div>
    	<div id="chartdiv"></div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
<script src="{{ url('/') }}/components/amchart/js/funnel.js"></script>
<script src="{{ url('/') }}/components/amchart/js/serial.js"></script>
<script src="{{ url('/') }}/components/amchart/js/pie.js"></script>
<script src="{{ url('/') }}/components/amchart/js/gauge.js"></script>
<script src="{{ url('/') }}/components/amchart/js/radar.js"></script>
<script src="{{ url('/') }}/components/amchart/js/light.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/material/morris.min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/raphael-min.js"></script>
<script src="{{ url('/') }}/components/amchart/js/morris.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>

<script type="text/javascript">
	var features = [];
	urlList = '{{ url("listadoplandetrabajo") }}/{{ Auth::user()->id }}?start=2017-07-21&end=2017-08-02';

	$(function () {
	    $('#responsable').select2({
	        // Activamos la opcion "Tags" del plugin
	         placeholder: "Seleccione una responsable",
	        ciudad: true,
	        tokenSeparators: [','],
	        ajax: {
	            dataType: 'json',
	            url: '{{ url("vendedores") }}',
	            delay: 250,
	            data: function(params) {
	                return {
	                    term: params.term
	                }
	            },
	            processResults: function(data, page) {
	              return {
	                results: data
	              };
	            },
	        }
	    });

	var jqxhr = $.get(urlList)
	.done(function() {
	})
	.fail(function() {
	})
	.always(function(doc) {
		respal = "";
    	var push = false
		var feature = false;
		var features = [];
		var feature2 = false;
		var features2 = [];

		doc = doc.sort(function (a, b) {
		  return a.tipo_actividad.localeCompare(b.tipo_actividad);
		});

		$.each( doc, function( key, value ) {
			var a = moment(value.start);
        	var b = moment(value.end);
        	c = b.diff(a);
        	c = parseInt(c/3600000);
        	if (feature) {
        		if (value.tipo_actividad != respal) {
        			push = true;
        			features.push(feature);
	        		respal = value.tipo_actividad;
	        		feature = new Object();
					feature.tiempo = c;
					feature.user = value.tipo_actividad;
					feature.colorGrp = "rgb(103, 183, 220)";
	        	}else{
	        		push = false;
					feature.tiempo = c+feature.tiempo;
	        	}
        	}else{
        		push = false;
        		respal = value.tipo_actividad;
        		feature = new Object();
				feature.tiempo = c;
				feature.user = value.tipo_actividad;
				feature.colorGrp = "rgb(103, 183, 220)";
				respal = value.tipo_actividad;
        	}
        	var comp = doc.length - 1;
        	if (comp === key && !push) {
        		features.push(feature);
        	}
		});	
		var charts = AmCharts.makeChart("chartdiv2", {
		    "theme": "light",
		    "type": "serial",
			"startDuration": 2,
		    "dataProvider": features,
		    "valueAxes": [{
		        "position": "left",
		        "title": "Uso de tiempo de las actividades",
		        "gridAlpha": 0,
		        "dashLength": 0
		    }],
		  "gridAboveGraphs": false,
		    "graphs": [{
		        "balloonText": "[[category]]: <b>[[value]]</b>",
		        "fillColorsField": "colorGrp",
		        "fillAlphas": 1,
		        "lineAlpha": 0.1,
		        "type": "column",
		        "valueField": "tiempo"
		    }],
		    "depth3D": 20,
			"angle": 30,
		    "chartCursor": {
		        "categoryBalloonEnabled": false,
		        "cursorAlpha": 0,
		        "zoomable": false
		    },
		    "categoryField": "user",
		    "categoryAxis": {
		    "gridPosition": "start",
		    "gridAlpha": 0,
		    "tickPosition": "start",
		    "tickLength": 20,
		    "labelRotation": 45
		  },
		    "export": {
		    	"enabled": true
		     }

		});


		otrodoc = doc.sort(function (a, b) {
		  return a.oportunidad.localeCompare(b.oportunidad);
		});


		$.each( otrodoc, function( key, value ) {
			var a = moment(value.start);
        	var b = moment(value.end);
        	c = b.diff(a);
        	c = parseInt(c/3600000);
        	if (feature2) {
        		if (value.oportunidad != respal) {
        			push = true;
        			features2.push(feature2);
	        		respal = value.oportunidad;
	        		feature2 = new Object();
					feature2.tiempo = c;
					feature2.user = value.oportunidad;
					feature2.colorGrp = "rgb(103, 183, 220)";
	        	}else{
	        		push = false;
					feature2.tiempo = c+feature2.tiempo;
	        	}
        	}else{
        		push = false;
        		respal = value.oportunidad;
        		feature2 = new Object();
				feature2.tiempo = c;
				feature2.user = value.oportunidad;
				feature2.colorGrp = "rgb(103, 183, 220)";
				respal = value.oportunidad;
        	}
        	var comp = otrodoc.length - 1;
        	if (comp === key && !push) {
        		features2.push(feature2);
        	}
		});	
		var chart = AmCharts.makeChart( "chartdiv", {
		  "type": "pie",
		  "theme": "light",
		  "titles": [ {
		    "text": "Uso de tiempo con respecto a las oportunidades",
		    "size": 16
		  } ],
		  "dataProvider": features2,
		  "valueField": "tiempo",
		  "titleField": "user",
		  "startEffect": "elastic",
		  "startDuration": 2,
		  "labelRadius": 15,
		  "innerRadius": "50%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});
	});

	$('#responsable').on('change', function() {
				if (this.value) {
					urlList = '{{ url("listadoplandetrabajo") }}/'+this.value+'?start=2017-07-21&end=2017-08-02';
					var jqxhr = $.get(urlList)
					.done(function() {
					})
					.fail(function() {
					})
					.always(function(doc) {
						respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  // a must be equal to b
						  return 0;
						});
						console.log("doc -> ",doc2);
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}
							
							console.log(comp, " === ", key, " && ", push, " -> ", feature);
							if (comp === key) {
								console.log("feature ", feature);
								features.push(feature);
							}
						});	
						console.log("features ->",features);
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);
							c = parseInt(c/3600000);
							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});	
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
					});		
				}
			});
});
	
</script>
@endsection