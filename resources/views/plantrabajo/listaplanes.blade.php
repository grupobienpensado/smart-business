@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Lstado Planes de Trabajo')

@section('content')

<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
.width-20{
  width: 16.5% !important;
}
#body-tiempo{
  height: 300px;
  width: 100%;
}
* {
  margin: 0;
  padding: 0;
}

*, *:before, *:after {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

.chart {
  display: -webkit-flex;
  display: flex;
  -webkit-align-items: flex-end;
  align-items: flex-end;
  height: 10em;
  margin: 0 auto;
  padding: 0 2em;
  position: relative;
  width: calc(100% - 4em);
}
.chart li {
  -webkit-flex: 1;
  flex: 1;
  list-style: none;
  position: relative;
  text-align: center;
}
.chart li:after {
  content: attr(data-label);
  color: white;
  display: block;
  margin: .75em 0 0 -2.75em;
  position: absolute;
  top: 100%;
  left: calc(40%);
  white-space: nowrap;
  -webkit-transform-origin: 0 0;
  -webkit-transform: skewy(15deg);
}
.chart:before {
  background-color: white;
  box-shadow: 0 2.5em 0 white, 0 5em 0 white, 0 7.5em 0 white, 0 10em 0 white;
  content: '';
  height: 1px;
  left: 0;
  opacity: .5;
  position: absolute;
  top: -1px;
  width: 100%;
}
.chart div {
  background-color: white;
  bottom: 0;
  display: inline-block;
  margin-left: -2em;
  -webkit-transform-origin: 0 0;
  transform-origin: 0 0;
  -webkit-transform: skewY(20deg);
  transform: skewY(20deg);
  width: 4em;
}
.chart div:before {
  background-color: #0083be;
  bottom: 100%;
  color: white;
  content: attr(data-value);
  line-height: 1.7em;
  left: 0;
  position: absolute;
  -webkit-transform-origin: 0 100%;
  transform-origin: 0 100%;
  -webkit-transform: skewX(-50deg);
  transform: skewX(-50deg);
  width: 4em;
}
.chart div:after {
  background-color: #0071a5;
  bottom: 0;
  content: '';
  height: 100%;
  left: 100%;
  position: absolute;
  width: 2em;
  -webkit-transform-origin: 0 0;
  transform-origin: 0 0;
  -webkit-transform: skewY(-40deg);
  transform: skewY(-40deg);
}
.chart div.grow {
  -webkit-animation: grow 1s 1 linear;
  animation: grow 1s 1 linear;
}

@-webkit-keyframes grow {
  0% {
    height: 0;
  }
}
@keyframes grow {
  0% {
    height: 0;
  }
}

.charts{
  background: #009fd6;
  padding: 100px 20px;
}
</style>
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>Listado de Planes de Trabajo</h3>
      	</div>
        <div class="col-md-12">
          <div class="table-responsive">
          	<table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
              <thead>
              	<tr>
              		<th class="text-center th">#</th>
    	          	<th class="text-center th width-20">funcionario</th>
    	          	<th class="text-center th width-20">tiempo</th>
    	          	<th class="text-center th width-20">num. actividades</th>
    	          	<th class="text-center th width-20">año</th>
                  <th class="text-center th width-20">Cumplimiento</th>
    	          	<th class="text-center th width-20">acciones</th>
              	</tr>
              </thead>
              <tbody>
              @php $i=1; @endphp
              @foreach($planes as $plan)
              @php $contador=0 ; @endphp
              @foreach($descripciones as $key)
                @if($key->plan_trabajo==$plan->id)
                  @php $contador++; @endphp
                @endif
              @endforeach
              @foreach($usuarios as $value)
                @if($value->id==$plan->user_id)
                  @php $funcionario=$value->name; @endphp
                @endif
              @endforeach
              	<tr>
              		<td class="text-center">{{$i}}</td>
                  <td class="text-center">{{$funcionario}}</td>
                  <td class="text-center"> Semana {{$plan->semana}}</td>
                  <td class="text-center">{{$contador}}</td>
                  <td class="text-center">{{substr($plan->created_at,0,4)}}</td>
                  <td class="text-center">
                    @if($plan->updated_at>$plan->fecha_cierre)
                      <img src="{{url('/')}}/images/icons/cancel.png" >
                    @else
                      <img src="{{url('/')}}/images/icons/checked.png" >
                    @endif
                  </td>
                  <td class="text-center">
                  <a class="btn btn-sm btn-primary display-line  margen-1 ver" name="{{$plan->id}}">
                    <i class="fa fa-eye white" aria-hidden="true"></i>
                  </a>
                  </td>
              	</tr>
                @php $i++; @endphp
              @endforeach
              </tbody>
          	</table>
          </div>  
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="actividades" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Actividades Semanales</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
	    <div class="form-group row block" >
          <div class="charts">
            <ul class="chart" id="graphs">
            </ul>
          </div>
          <hr>
       		<div class="col-md-12" id="lista-actividades">
       			
       		</div>
       </div>
    </div>    
    </div>
    </div>
  </div>
@endsection
 
@section('scripts')
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" charset="utf-8">
	
$("body").on("click",".ver",function(e){
  $("#graphs").html("");
  $("#actividades").modal();
	id=$(this).attr("name");
	$("#lista-actividades").html('');
	setTimeout(function(e){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/listarplanestrabajoindividual/"+id, 
        cache: false,
        type: 'GET',
        success: function(e){ 
          var $cheight = $(".chart").height();
          $("#graphs").html(e.funcion)
        	$("#lista-actividades").html(e.tabla);
          $(".chart div").each(function(i) {
            var $value = $(this).data("value");
            $(this).css('height',$value * $cheight / e.final).addClass("grow");
          });
        }
    });},2000)

})
function realizado(id){
    mo=$("#tr-"+id).is(":visible");
    $(".tr").hide("slow");
    if(!mo){
      $("#tr-"+id).show("slow");
    }else{
      $("#tr-"+id).hide("slow");
    }
}
 $(document).ready(function() {
  

    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ oportunidades",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ 2 oportunidades",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 oportunidades",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      }
    });
  });
</script>

@endsection