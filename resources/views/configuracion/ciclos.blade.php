<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Ciclos de Oportunidades')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
    text-align: left;
}
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
}
.imagen-perfil{
    height: 100px !important;
    width: 100px !important;
    max-height: 100px !important;
}
.dropzone.smalltext:after {
    font-size: 12px !important;
}
.dropzone:after{
  font-size: 12px !important;
}
.alta{
  text-align: center;
  border-radius: 5px;
  border-left: 3px solid #9be69b;
  border-top: 3px solid #9be69b;
  box-shadow: 1px 2px #ccc;
}
.media{
  text-align: center;
  border-radius: 5px;
  border-left: 3px solid #e8cc8f;
  border-top: 3px solid #e8cc8f;
  box-shadow: 1px 2px #ccc;
}
.baja{
  text-align: center;
  border-radius: 5px;
  border-left: 3px solid #f18061;
  border-top: 3px solid #f18061;
  box-shadow: 1px 2px #ccc;
}
.contenedor-input{
  width: 50%;
  display: inline-block;
  float: right;
}
.form-control{
  text-align: center;
}
</style>
<link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<div class="container-fluid animated flipInX">
  <div class="row">

    <div class="col-md-12 panel-view">
       <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:abrir()" class="btn btn-sm btn-success"><i class="fa fa-refresh" aria-hidden="true" ></i> Agregar Nuevo Ciclo</a>
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      <div class="col-md-12"><h4>Ciclos de Oportunidades</h4></div>
      <div class="col-md-12">
          <?php foreach($ciclos as $ciclo){
          $lis=explode(" ",$ciclo->nombre);
            if($ciclo->imagen!=""){
                $imagen=url('/').'/'.$ciclo->imagen;
            }else{
                $imagen="https://placeholdit.imgix.net/~text?txtsize=45&txt=".str_replace(" ", "%20", $ciclo->name)."&w=200&h=200";
            }
            ?>
            <div class="col-md-6" onclick="ver_nuevo({{$ciclo->id}})">
              <div style="margin: 2px;width: 100%;display: inline-block;border-radius: 10px;position: relative;height: 60px;padding: 5px;box-shadow: 0px 5px 3px #ccc;border: 0.5px solid #ccc;">
                <div style="width: 50px;float: left;height: 50px;display: inline-block;background-repeat: no-repeat;background-size: contain;margin-right: 10px; background-image: url({{$imagen}})"></div>
                <div style="text-align: left;width: 75%;float: left;">
                  <span style="display: block;font-size: 21px;font-weight: bold;">{{$lis[2]}}</span>
                  <span style="display: block;">Tiempo de transición: {{$ciclo->dias}} dias</span>                
                </div>
                <div style="width: 75px;position: absolute;right: 0;">
                  <span style="font-size: 33px;text-shadow: 3px 1px 2px #ccc;">{{$ciclo->porcentaje}}%</span>
                </div>
              </div>
              
            </div>
          <?php } ?>
      </div>
      
      <div class="col-md-12"><hr><h4>Participación según probabilidad</h4></div>
      <form action="{{'configoportunidades'}}" method="POST">
      {{ csrf_field() }}
      <div class="col-md-6">
      <?php if(count($probabilidades)>0){?> 
      <input type="hidden" name="id_2" value="{{$probabilidades[0]->id}}">
       <?php } ?>
          <div class="form-group col-md-12">
            <label>% Alta Probabilidad</label>
            <div class="contenedor-input">
              <input type="number" min="1" class="alta" name="alta" <?php if(count($probabilidades)>0){?> value="{{$probabilidades[0]->alta}}" <?php } ?>>
            </div>
          </div>
          <div class="form-group col-md-12">
            <label>% Mediana Probabilidad</label>
            <div class="contenedor-input">
              <input type="number" min-value="1" class="media" name="media" <?php if(count($probabilidades)>0){?> value="{{$probabilidades[0]->media}}" <?php } ?>>
            </div>
          </div>
          <div class="form-group col-md-12">
            <label>% Baja Probabilidad</label>
            <div class="contenedor-input">
              <input type="number" min="1" class="baja" name="baja" <?php if(count($probabilidades)>0){?> value="{{$probabilidades[0]->baja}}" <?php } ?>>
            </div>
          </div>
      </div>
      <div class="col-md-6">
        <table class="table product-table">
          <thead>
            <tr>
              <th colspan="3" class="text-center">Metas Oportunidades General</th>
            </tr>
            <tr>
              <th></th>
              <th class="text-center">Numero de dias</th>
              <th class="text-center">Valor</th>
            </tr>
          </thead>
          <tbody>
          <?php if(count($metas)>0){ ?>
          <input type="hidden" name="id" value="{{$metas[0]->id}}">
          <?php } ?>
            <tr>
              <td class="text-center">Corto</td>
              <td><input type="number" class="form-control form-control2 corto" required name="corto_dias" <?php if(count($metas)>0){?> value="{{$metas[0]->corto_dias}}" <?php } ?>></td>
              <td><input type="text" class="form-control dinero form-control2" required name="corto_valor" <?php if(count($metas)>0){?> value="{{$metas[0]->corto_valor}}" <?php } ?>></td>
            </tr>
            <tr>
              <td class="text-center">Mediano</td>
              <td><input type="number" class="form-control form-control2 mediano" <?php if(count($metas)>0){?> min="{{$metas[0]->corto_dias}}" <?php } ?> required name="medio_dias" <?php if(count($metas)>0){?> value="{{$metas[0]->medio_dias}}" <?php } ?>></td>
              <td><input type="text" class="form-control dinero form-control2" required name="medio_valor" <?php if(count($metas)>0){?> value="{{$metas[0]->medio_valor}}" <?php } ?>></td>
            </tr>
            <tr>
              <td class="text-center">Largo</td>
              <td><input type="text" class="form-control form-control2 alto" readonly="" name="largo_dias" <?php if(count($metas)>0){?> value="{{$metas[0]->largo_dias}}" <?php } ?>></td>
              <td><input type="text" class="form-control dinero form-control2" required name="largo_valor" <?php if(count($metas)>0){?> value="{{$metas[0]->largo_valor}}"> <?php } ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="form-group col-md-12">
        <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
      </div>
      </form>
    </div>
</div>
</div>
<div class="modal fade" id="ciclo-ed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
        <h4 class="modal-title" id="myModalLabel">Editar Ciclo de Oportunidad</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row margen-inferior">
         <div class="col-md-12">
              <form method="POST" action="{{ url('ciclo_editar') }}"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="ciclo_oportunidad">
                  
                </div>
                <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                </form>
          </div> 
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="banderas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
        <h4 class="modal-title" id="myModalLabel">Agregar Ciclo de Oportunidad</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row margen-inferior">
         <div class="col-md-12">
              <form method="POST" action="{{ url('ciclo_editar') }}"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="ciclo_oportunidad">
                  
                </div>
                <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                </form>
          </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
function abrir(){
    $("#banderas").modal()
    $('.dropzone').html5imageupload();
}
function ver_nuevo(id){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/ciclo/"+id, 
        cache: false,
        type: 'GET',
        success: function(e){ 
            $("#ciclo_oportunidad").html(e.contenido);
            $("#ciclo-ed").modal();
            $('.dropzone').html5imageupload();
        }
    });
}
$(function () {
  $(".dinero").maskMoney();
})

$("body").on("keyup",".corto",function(e){
  $(".mediano").attr("min",$(this).val());
})
$("body").on("keyup",".mediano",function(e){
  id="> "+String($(this).val());
  console.log(id);
  $(".alto").val(id);
})
</script>
@endsection