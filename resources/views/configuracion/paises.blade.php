<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Paises')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
    text-align: left;
}
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
}
.imagen-perfil{
    height: 200px !important;
    width: 200px !important;
    max-height: 200px !important;
}
</style>
<link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<div class="container-fluid animated flipInX">
  <div class="row">

    <div class="col-md-12 panel-view">
       <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:abrir()" class="btn btn-sm btn-success"><i class="fa fa-map" aria-hidden="true" ></i> Agregar Bandera</a>
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      <div class="col-md-12"><h4>Listado de Paises</h4></div>
      <div class="col-md-12">
          <?php foreach($paises as $pais){ 
            if($pais->imagen!=""){
                $imagen=url('/').'/images/icons/'.$pais->imagen;
            }else{
                $imagen="https://placeholdit.imgix.net/~text?txtsize=45&txt=".str_replace(" ", "%20", $pais->name)."&w=200&h=200";
            }
            ?>
          <div style="margin:2px; width: 112px;display: inline-block;text-align: center;position: relative;height: 140px;padding: 5px;box-shadow: 0px 5px 3px #ccc;">
              <div style="width: 100px; height: 100px;border-radius: 100%;border: 0.5px solid #ccc;position: absolute; background-repeat: no-repeat; background-size: contain; background-image: url({{$imagen}})">
                  <div style="position: absolute;bottom: -38px;left: 0;right: 0;height: 37px;white-space: normal;overflow-y: hidden;">
                      <span style="line-height: 0.3px">{{$pais->name}}</span>
                  </div>
              </div>
          </div>
          <?php } ?>
      </div>
    </div>
</div>
</div>
<div class="modal fade" id="banderas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
            <h4 class="modal-title" id="myModalLabel">Agregar Bandera</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row margen-inferior">
             <div class="col-md-12">
                  <form method="POST" action="{{ url('paises_save') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-12">
                        <label for="pais" class="col-form-label">Pais nacimiento</label>
                        <select name="pais" class="form-control" id="pais"></select>
                    </div>
                    <div class="col-md-12" style="text-align: center; margin: 10px">
                      <div class="dropzone imagen-perfil" style="height: 200px; width:200px; max-height: 200px; min-height: 200px;" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                        <input type="file" name="foto" required="required" accept="image/gif, image/jpeg, image/png">
                      </div>
                    </div>
                    <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                    </form>
              </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script type="text/javascript">
function abrir(){
    $("#banderas").modal()
    $('.dropzone').html5imageupload();
    $('#pais').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Pais",
            pais: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("pais") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
}
</script>
@endsection