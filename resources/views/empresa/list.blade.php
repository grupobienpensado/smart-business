<!-- Stored in resources/views/siervo.blade.php -->

@extends('template.app')

@section('title', 'Listado de empresas')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
<link rel="stylesheet" href="{{url('/')}}/components/jplist/normalize.css" />
<link rel="stylesheet" href="{{url('/')}}/components/jplist/styles.min.css?v=15" />
<link href="{{url('/')}}/components/jplist/jplist.demo-pages.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/components/jplist/jplist.core.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<!-- borrado-->

<style type="text/css">
    #lista_empresas_filter{
        margin-left: 1.5rem;
        margin-top: 0.3rem;
    }
.lato{font-family:'Lato',sans-serif}.jplist-reset-btn{width:75px}
.lato{font-family:'Lato',sans-serif}.jplist-panel .jplist-pagination{cursor:pointer;float:left;line-height:30px}.jplist-panel .jplist-pagination button{display:inline-block;color:#0381bb;height:30px;line-height:30px;padding:0 10px}.jplist-panel .jplist-pagination .jplist-current{font-weight:bold;color:#f03800}.jplist-panel .jplist-pagination .jplist-pagingprev,.jplist-panel .jplist-pagination .jplist-pagingmid,.jplist-panel .jplist-pagination .jplist-pagingnext{float:left}.jplist-panel .jplist-pagination .jplist-pagingprev button,.jplist-panel .jplist-pagination .jplist-pagingnext button{font-size:20px;font-family:"Courier New",Courier,monospace}.jplist-one-page{display:none}.jplist-empty{display:none}
.lato{font-family:'Lato',sans-serif}.jplist-panel .jplist-pagination{cursor:pointer;float:left;line-height:30px}.jplist-panel .jplist-pagination button{display:inline-block;color:#0381bb;height:30px;line-height:30px;padding:0 10px}.jplist-panel .jplist-pagination .jplist-current{font-weight:bold;color:#f03800}.jplist-panel .jplist-pagination .jplist-pagingprev,.jplist-panel .jplist-pagination .jplist-pagingmid,.jplist-panel .jplist-pagination .jplist-pagingnext{float:left}.jplist-panel .jplist-pagination .jplist-pagingprev button,.jplist-panel .jplist-pagination .jplist-pagingnext button{font-size:20px;font-family:"Courier New",Courier,monospace}.jplist-one-page{display:none}.jplist-empty{display:none}


.map-marker {
  margin-left: -8px;
  margin-top: -8px;
}

.map-marker.map-clickable {
  cursor: pointer;
}

.pulse {
  width: 10px;
  height: 10px;
  border: 5px solid #f7f14c;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
  background-color: #716f42;
  z-index: 10;
  position: absolute;
}
.map-marker .dot {
  border: 10px solid #fff601;
  background: transparent;
  -webkit-border-radius: 60px;
  -moz-border-radius: 60px;
  border-radius: 60px;
  height: 50px;
  width: 50px;
  -webkit-animation: pulse 3s ease-out;
  -moz-animation: pulse 3s ease-out;
  animation: pulse 3s ease-out;
  -webkit-animation-iteration-count: infinite;
  -moz-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  position: absolute;
  top: -20px;
  left: -20px;
  z-index: 1;
  opacity: 0;
  }
  @-moz-keyframes pulse {
   0% {
      -moz-transform: scale(0);
      opacity: 0.0;
   }
   25% {
      -moz-transform: scale(0);
      opacity: 0.1;
   }
   50% {
      -moz-transform: scale(0.1);
      opacity: 0.3;
   }
   75% {
      -moz-transform: scale(0.5);
      opacity: 0.5;
   }
   100% {
      -moz-transform: scale(1);
      opacity: 0.0;
   }
  }
  @-webkit-keyframes "pulse" {
   0% {
      -webkit-transform: scale(0);
      opacity: 0.0;
   }
   25% {
      -webkit-transform: scale(0);
      opacity: 0.1;
   }
   50% {
      -webkit-transform: scale(0.1);
      opacity: 0.3;
   }
   75% {
      -webkit-transform: scale(0.5);
      opacity: 0.5;
   }
   100% {
      -webkit-transform: scale(1);
      opacity: 0.0;
   }
  }

  .observaciones-img {
    width: 50px !important;
    height: 60px !important;
  }

  .jumbotron{
    height: 100px;
    padding-top: 15px;
  }

  div#example_wrapper,.row{
    width: 100%
  }

  .img-sedes{
    max-width: 50%;
    max-height: 50%;
    min-height: 125px;
    min-width: 125px;
  }

  #chartdiv {
    height: 100%;
  }
  /* Optional: Makes the sample page fill the window. */
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .tooltip-inner {
    white-space: pre-wrap;
  }

  .subtitulo{
    text-transform: capitalize;
    margin-bottom: 4px;
    font-size: small;
    color: #54575a;
  }

  .biblioteca-img{
    border: solid 0px;
    position: absolute;
    top: 50%;
    bottom: 50%;
    left: 25%;
    margin-bottom: auto;
    margin-top: auto;
    margin-right: auto!important;
    margin-left: auto!important;
  }

  .imgs-thumbnail{
    max-width: 100%;
    position: relative;
    overflow: hidden;
    background-color: #fff;
  }

  .unalinea{
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  img {
    border: 0px !important;
  }

  .main-menu > li {
    line-height: 10px !important;
  }

  .mas-pequeño{
    line-height: 1.5;
    font-size: small;
    color: #636363;
    font-weight: 400;
    display: inline-block !important;
  }

  .centrar-vertical{
    vertical-align: middle !important;
  }
  p.subtitulo.title {
      white-space: nowrap;
  }
</style>

<?php 
$modulo=5;
$permiso_crear_empresa='No';
$acceso3 = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_accesos` WHERE `acceso`="botón crear empresa en listar y ver" AND `id_permisomodulo`="'.$modulo.'"');
if(isset($acceso3[0]->id)){
  $cargo3 = Illuminate\Support\Facades\DB::select('SELECT * FROM `permiso_cargos` WHERE `cargo`="'.Auth::user()->cargo.'"');
  if(isset($cargo3[0]->id)){
    $permiso3 = Illuminate\Support\Facades\DB::select('SELECT * FROM `permisos` WHERE `id_acceso`="'.$acceso3[0]->id.'" AND `id_cargo`="'.$cargo3[0]->id.'" ORDER BY `id` DESC;');
    if(isset($permiso3[0]->permiso)){
      if($permiso3[0]->permiso == "Si"){ 
        $permiso_crear_empresa="Si";
      }else{
        $permiso_crear_empresa="No";
      }
    }
  }          
}
?>

  <div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
      <?php if($permiso_crear_empresa=="Si"){ ?>
        <a href="{{ url('/') }}/crearempresa" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Crear empresa</a>
      <?php } ?>
        <div class="btn-group" role="group">
          <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('chartdiv2')" id="bchartdiv"><i class="fa fa-map"></i> Ver mapa</button>
          <button type="button" class="btn btn-sm btn-secondary active" onclick="funMostarDiv('list')" id="blist"><i class="fa fa-list"></i> Ver lista</button>
          <button type="button" class="btn btn-sm btn-secondary" onclick="funMostarDiv('circulos')" id="bcirculos"><i class="fa fa-th-large"></i> Ver mosaico</button>
        </div>
      </div>
      <h2>Empresas <span id="contEmpresas"></span></h2>
      <p class="letra-gris" style="margin-bottom: 0px;">Listado de empresas</p>
    </div>
  </div>

          

  <div class="card animated slideInRight" id="list" style="margin-bottom: 30px;">
    <div class="card-block">  
      <div class="table-responsive">
        <table id="lista_empresas" cellspacing="0" class="table table-striped table table-striped table-bordered display" style="width: 100%">
          <thead>
            <tr>
              <th class="centrado">Item</th>
              <th class="centrado">Logo</th>
              <th class="centrado" style="width: 150px">Nombre</th>
              <th class="centrado">Pais</th>
              <th class="centrado">Redes Sociales</th>
              <th class="centrado">Sedes</th>
              <th class="centrado">Clientes</th>
              <th class="centrado">Cant. Opor.</th>
              <th class="centrado">Litros Dia</th>
              <th class="centrado">Responsable</th>
              <th class="centrado" data-toggle="tooltip" data-placement="top" title="Procentaje de llenado">Porcentaje de Datos</th>
              <th class="centrado">Creada</th>
              <th class="centrado">Editada</th>
              <th class="centrado">Acciones</th>
            </tr>
          </thead>
          <tbody id="listEmpresaTable"></tbody>
        </table>
      </div>
    </div>
  </div>



<!-- HTML -->
<div class="card animated slideInRight" id="chartdiv2" style="display: none;margin-bottom: 30px;">
  <div class="card-block">
    <div style="width: 100%;background-color: #fff;height: 700px">
      <div id="chartdiv" style="height: 700px" class=" animated rollIn"></div>
    </div>
  </div>
</div>

<div class="card animated slideInRight" id="circulos" style="display: none;margin-bottom: 30px;">
  <div id="demo" class="card-block">
    <div class="jplist-panel box panel-top">                    
                            
        <!-- reset button -->
        <button type="button" class="jplist-reset-btn" data-control-type="reset" data-control-name="reset" data-control-action="reset">
            Reiniciar &nbsp;<i class="fa fa-share"></i>
        </button>
                
        <!-- items per page dropdown -->
        <div 
          class="jplist-drop-down" 
          data-control-type="items-per-page-drop-down" 
          data-control-name="paging" 
          data-control-action="paging">
        
          <ul>
            <li><span data-number="50"> 50 por pagina </span></li>
            <li><span data-number="all" data-default="true"> Todas </span></li>
          </ul>
        </div>
                
        <!-- sort dropdown -->
        <div 
            class="jplist-drop-down" 
            data-control-type="sort-drop-down" 
            data-control-name="sort" 
            data-control-action="sort"
            data-datetime-format="{month}/{day}/{year} {hour}:{min}"> <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->
                    
            <ul>
                <li><span data-path="default">Ordenar por</span></li>
                <li><span data-path=".title" data-order="asc" data-type="text" data-default="true">Titulo A-Z</span></li>
                <li><span data-path=".title" data-order="desc" data-type="text">Titulo Z-A</span></li>
                <li><span data-path=".desc" data-order="asc" data-type="text">Pais A-Z</span></li>
                <li><span data-path=".desc" data-order="desc" data-type="text">Pais Z-A</span></li>
                <li><span data-path=".like" data-order="asc" data-type="number">Sedes asc</span></li>
                <li><span data-path=".like" data-order="desc" data-type="number">Sedes desc</span></li>
                <!--<li><span data-path=".date" data-order="asc" data-type="datetime">Fecha asc</span></li>
                <li><span data-path=".date" data-order="desc" data-type="datetime" data-default="true">Fecha desc</span></li>-->
                <li><span data-path=".litos" data-order="asc" data-type="number">Litros dia asc</span></li>
                <li><span data-path=".litos" data-order="desc" data-type="number">Litros dia desc</span></li>
            </ul>
        </div>
                
        <!-- filter by title -->
        <div class="text-filter-box">                
            <i class="fa fa-search jplist-icon"></i>
            <input data-path=".title" type="text" placeholder="Filtrar por empresa" 
                data-control-type="textbox" 
                data-control-name="title-filter" 
                data-control-action="filter"
            />
        </div>
                
        <!-- filter by description -->
        <div class="text-filter-box">                    
            <i class="fa fa-search jplist-icon"></i>
            <input data-path=".desc" type="text" placeholder="Filtrar por pais" 
                data-control-type="textbox" 
                data-control-name="desc-filter" 
                data-control-action="filter"
            />  
        </div>  
                
        <!-- pagination results -->
        <div 
            class="jplist-label" 
            data-type="Página {current} de {pages}" 
            data-control-type="pagination-info" 
            data-control-name="paging" 
            data-control-action="paging">
        </div>
                    
        <!-- pagination -->
        <div 
            class="jplist-pagination" 
            data-control-type="pagination" 
            data-control-name="paging" 
            data-control-action="paging">
        </div>  
    </div>
    <div id="listEmpresaMosaico" class="demo-tbl"></div> 
  </div>
</div>


@endsection

@section('scripts')

<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>

<script type="text/javascript">
  var image = "";
  var contentString = "";
  var features = [];

  function initMap2() {
      var jqxhr = $.get( "{{ url('/') }}/sedesjson")
      .fail(function() {
          swal("Error!","Algo salió mal, intenta más tarde o comunícate con el área de sistemas ","danger");
      })
      .always(function(data) {
        if (data.success) {
            var totalclient = data.model.length;
            $("#contEmpresas").text(" ( "+totalclient+" )");
            $.each(data.model, function (k, item) {
              if (item.logo != "" && item != null) {
                var contenidoimg = `<img src="{{ url('/') }}/images/file/empresas/principal/`+item.logo+`" style="max-width: 50%;" class="img-fluid mx-auto d-block">`;
              }else{
                var contenidoimg = `<img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100" style="max-width: 50%;" class="img-fluid mx-auto d-block">`;
              }
              
              var feature = new Object();
                  feature.position = new google.maps.LatLng(item.lat, item.lng);
                  feature.type = item.pais;
                  feature.contenido = `<div id="content"><div id="siteNotice"></div><div id="bodyContent"><a href="{{ url('/') }}/empresa/`+item.id+`">`+contenidoimg+`</a></div><h2 class="centrado">`+item.nombre+`</h2><p class="centrado">`+item.litros_dia+` Ltrs día</p><p class="centrado">`+item.pais+`/`+item.ciudad+`</p></div>`; 
              features.push(feature);      
            });
        }else{
          swal("Error!","Algo salió mal, intenta más tarde o comunícate con el área de sistemas ","warning");
        }
      });

      initMap()
  }

  function initMap() {
    var styledMapType = new google.maps.StyledMapType(
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8ec3b9"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1a3646"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#64779e"
            }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#4b6878"
            }
          ]
        },
        {
          "featureType": "landscape",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#334e87"
            }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6f9ba5"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3C7680"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#304a7d"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2c6675"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#255763"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#b0d5ce"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3a4762"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#0e1626"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#4e6d70"
            }
          ]
        }
      ],
      {name: 'Styled Map'}
    );
    
    image = {
      url: '{{ url("/") }}/images/iconos_empresa/empresas.png',
      size: new google.maps.Size(50, 40),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 40)
    };

    var map = new google.maps.Map(document.getElementById('chartdiv'), {
      zoom: 3,
      center: {lat: 7.064034827633218, lng: -73.1567105784718},
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
      }
    });

    var markers = [];
    function addMarker(feature) {  
      console.log(feature.position);
      var marker = new google.maps.Marker({
        position: feature.position,
        icon: image,
        title: feature.type,
        map: map,
        draggable:true
      });
      marker.setAnimation(google.maps.Animation.BOUNCE);
      
      var infowindow = new google.maps.InfoWindow({
        content: feature.contenido,
        maxWidth: 200
      });

      marker.addListener('mouseover', function() {
        contentString = feature.contenido;
        marker.setAnimation(google.maps.Animation.BOUNCE);
        infowindow.open(map, marker);
      });

      marker.addListener('mouseout', function() {
        infowindow.close();
      });

      markers.push(marker);
    }

    for (var i = 0, feature; feature = features[i]; i++) {
      addMarker(feature);
    }

    

    var markerCluster = new MarkerClusterer(map, markers, { maxZoom: 12, gridSize: 20, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
  }
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSg3ZOwMbQvB0xjXAFmr2Ch-7IKV98gno&callback=initMap2"></script><!--  -->

<!-- js     
<script src="http://jplist.com/content/js/vendor/jquery-1.10.0.min.js"></script>--> 
<script src="{{url('/')}}/components/jplist/modernizr.min.js"></script>
<script src="{{url('/')}}/components/jplist/jquery.lazyload.min.js"></script>        
<script src="{{url('/')}}/components/jplist/website.min.js?v=1"></script>
<script src="{{url('/')}}/components/jplist/jplist.core.min.js"></script>
<script src="{{url('/')}}/components/jplist/jplist.history-bundle.min.js"></script>
<script src="{{url('/')}}/components/jplist/jplist.pagination-bundle.min.js"></script>
<script src="{{url('/')}}/components/jplist/jplist.sort-bundle.min.js"></script>
<script src="{{url('/')}}/components/jplist/jplist.textbox-filter.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">

  $(function () {
    html = `<tr class="odd"><td valign="top" colspan="14" class="dataTables_empty">Cargando...</td></tr>`;
    $('#listEmpresaTable').html(html);
    var jqxhr = $.get( "{{ url('/') }}/empresasjson")
    .fail(function() {
        swal("Error!","Algo salió mal, intenta más tarde o comunícate con el área de sistemas ","danger");
    })
    .always(function(data) {
        if (data.success) {
            html=""; mosaico="";
            moment.locale('es');
            dataEmpresa = data.model.sort(function (a, b) {
              return new Date(b.updated_at) - new Date(a.updated_at);
            });
            $('#contEmpresas').html(data.contador);
            $.each(dataEmpresa, function (k, item) {
              i=k+1;
              html += `<tr>
                <td class="centrado centrar-vertical"><p class="mas-pequeño">`+i+`</p></td>
                <td class="centrar-vertical">
                  <a target="_blank" href="{{ url('/') }}/empresa/`+item.id+`">
                    <img src="`;
                    if(item.logo != null && item.logo != ""){
                      html +=  `{{ url('/') }}/images/file/empresas/principal/`+item.logo;
                    }else {
                      html +=  `https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100`;
                    }
                    html +=`" style="max-width: 50px;" class="img-fluid img-thumbnail mx-auto d-block">
                  </a>
                </td>
                <td class="centrado centrar-vertical"><p class="mas-pequeño" style="text-transform: uppercase !important;">`+item.nombre+`</p></td>
                <td class="centrado centrar-vertical"><p class="mas-pequeño">`+item.pais+`</p></td>
                <td class="centrado centrar-vertical"><div style="display: table;margin: 0 auto;">`;
                  $.each(item.redes, function (c, red) {
                    html += `<a target="_blank" href="`+red.valor+`">`;
                    if (red.tipo === "Facebook") {
                      html += `<img src="{{ url("/") }}/images/icons_redes/facebook_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">`;
                    }else if (red.tipo === "Instagran") {
                      html += `<img src="{{ url("/") }}/images/icons_redes/instagram_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">`;
                    }else if (red.tipo === "Twitter") {
                      html += `<img src="{{ url("/") }}/images/icons_redes/twitter_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">`;
                    }else if (red.tipo === "Linkedin") {
                      html += `<img src="{{ url("/") }}/images/icons_redes/linkedin_logo.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">`;
                    }else {
                      html += `<img src="{{ url("/") }}/images/icons_redes/share.svg" style="float:left;max-width: 30px;" class="img-fluid mx-auto d-block">`;
                    }
                    html += `</a>`;
                  });

                  texto = "";
                  $.each(item.sedes, function (c, sede) {
                    texto +=" "+sede.ciudad+",&#10;"; 
                  });

                  txtClient = "";
                  $.each(item.clientes, function (c, cliente) {
                    txtClient +=" "+cliente.tratamiento+" "+cliente.cliente+",&#10;"; 
                  });

                html += `</div></td>
                <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top" title="`+texto+`"><p class="mas-pequeño">`+item.nsedes+`</p></td>
                <td class="centrado centrar-vertical" data-toggle="tooltip" data-placement="top" title="`+txtClient+`"><p class="mas-pequeño">`+item.nclient+`</p></td>
                <td class="centrado centrar-vertical"><p class="mas-pequeño">`+item.numop+`</p></td>
                <td class="centrado centrar-vertical"><p class="mas-pequeño">`+item.tlitros_dia+`</p></td>
                <td class="centrado centrar-vertical"><p class="mas-pequeño" style="text-transform: uppercase !important;">`+item.name+`</p></td>
                <td class="centrado centrar-vertical unalinea" data-toggle="tooltip" data-placement="top" title="`+item.faltante+`" style="cursor: pointer;">
                    <p class="mas-pequeño">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: `+item.Porcentaje+`%" aria-valuenow="`+item.Porcentaje+`" aria-valuemin="0" aria-valuemax="100">`+item.Porcentaje+`%</div>
                        </div>
                    </p>
                </td>
                <td class="centrado centrar-vertical unalinea"><p class="mas-pequeño">`+moment(item.created_at).format("YYYY/MM/DD")+`</p></td>
                <td class="centrado centrar-vertical unalinea"><p class="mas-pequeño">`+moment(item.updated_at).format("YYYY/MM/DD")+`</p></td>
                <td class="unalinea centrar-vertical">
                  <a href="{{ url('/') }}/empresa/`+item.id+`" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver empresa">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>`;
                  if((data.permiso_editar)=="Si"){
                    html +=`<a href="{{ url('editarempresa') }}/`+item.id+`" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar empresa">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>`;
                  }
                  if((data.permiso_eliminar)=="Si"){
                    html +=`<button onclick="eliminarEmpresa(`+item.id+`, this)" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar empresa">
                      <i class="fa fa-trash"></i>
                    </button>`;
                  }
                html +=`</td>
              </tr>`;    

              mosaico += `
                <div class="col-3 col-sm-2 tbl-item placeholder img-sedes" style="
                -webkit-box-shadow: 6px 7px 38px -4px rgba(5,2,6,0.2) !important;
                -moz-box-shadow: 6px 7px 38px -4px rgba(5,2,6,0.2) !important;
                box-shadow: 6px 7px 38px -4px rgba(5,2,6,0.2) !important;
                padding-top: 35px;    max-height: 402px !important; overflow: hidden;">
                <div class="imgs-thumbnail img-sedes">
                  <a href="{{ url('/') }}/empresa/`+item.id+`">
                    <img src="`;
                      if(item.logo != null && item.logo != ""){
                        mosaico +=  `{{ url('/') }}/images/file/empresas/principal/`+item.logo;
                      }else {
                        mosaico +=  `https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100`;
                      }
                      mosaico +=`" style="max-width: 50%;" class="img-fluid menu biblioteca-img d-block">
                  </a>
                </div> 
                <div class="text-muted centrado">
                  <p class="date" style="top: 0px;position: absolute;right: 0px;font-size: 11px !important;color: #606365;">`+moment(item.updated_at).format("DD/MM/YYYY")+`</p>
                  <p class="subtitulo title">`+item.nombre+`</p>
                  <p class="subtitulo like" data-toggle="tooltip" data-placement="top" title="`+texto+`">`+item.nsedes+` Sedes</p>
                  <p class="subtitulo desc">`+item.pais+`</p> 
                  <p class="subtitulo litos">`+item.tlitros_dia+` Litros dia</p>  
                  <p class="subtitulo">
                    <a href="{{ url('/') }}/empresa/`+item.id+`" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver empresa">
                      <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>`;
                    if((data.permiso_editar)=="Si"){
                    mosaico +=`<a href="{{ url('editarempresa') }}/`+item.id+`" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar empresa">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>`;
                    }
                    if((data.permiso_eliminar)=="Si"){
                      mosaico +=`<button onclick="eliminarEmpresa(`+item.id+`, this)" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar empresa">
                        <i class="fa fa-trash"></i>
                      </button>`;
                    }
                  mosaico +=`</p>
                  </div>
                </div>
              `;            
            });
            $('#listEmpresaTable').html(html);
            $('#listEmpresaMosaico').html(mosaico);
            


            $('#demo').jplist({

                itemsBox: '.demo-tbl'
                , itemPath: '.tbl-item'
                , panelPath: '.jplist-panel'

                //save plugin state
                , storage: 'localstorage' //'', 'cookies', 'localstorage'           
                , storageName: 'jplist-tabl'
            });


            $('#lista_empresas').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    <?php if($data['permiso_exportar'] == "Si"){ ?>
                    'excelHtml5',
                    'csvHtml5',
                    <?php } ?>
                ],
                language: {
                    processing:     "Tratamiento en curso ...",
                    search:         "Buscar&nbsp;:",
                    lengthMenu:     "Mostrar _MENU_ Empresas",
                    info:           "Registros del  _START_ al _END_ de _TOTAL_ Empresas",
                    infoEmpty:      "Ver de elemento 0 al 0 de 0 Empresas",
                    infoPostFix:    "",
                    loadingRecords: "Cargando...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "No hay datos disponibles en la tabla",
                    paginate: {
                        first:      "Primero",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "Ultimo"
                    },
                    aria: {
                        sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                        sortDescending: ": habilitado para ordenar la columna en orden descendente"
                    }
              }
            });
            $('[data-toggle="tooltip"]').tooltip();
        }else{
          swal("Error!","Algo salió mal, intenta más tarde o comunícate con el área de sistemas ","warning");
        } 
    });      
  });

  eliminarEmpresa = function(id, input) {
    swal({
      title: '¿Estás seguro?',
      html: $('<div>')
        .addClass('some-class')
        .text('¡No podrás revertir esto!'),
      animation: false,
      customClass: 'animated tada',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo!',
      cancelButtonText: 'Cancelar',
      closeOnConfirm: false,   
      showLoaderOnConfirm: true,
      showLoaderOnConfirm: true,
      preConfirm: function () {
        return new Promise(function (resolve) {
          var elemento =$(input).parent().parent(); 
          $.get( "{{ url('/') }}/empresa/eliminar/"+id, function( data ) {
            if (data.success) {
              //$('#formArchivo')[0].reset();
              $( elemento ).remove();
              swal('¡Eliminado!','Su archivo ha sido eliminado.','success');
              resolve() 
            }else{
              swal("Algo salio mal, vuelve a intentar",'warning');
              resolve() 
            }              
          }); 
        })
      },
      allowOutsideClick: false
    });
  }

  funMostarDiv = function (valor) {
    $(".active").removeClass('active');
    $("#b"+valor).addClass('active');
    $(".slideInDown").removeClass('slideInDown').addClass('rollOut');
    if(valor==="list"){
      $("#list").show();
    }else{
      $("#list").hide();
    }
    if(valor==="circulos"){
      $("#circulos").show();
    }else{
      $("#circulos").hide();
    }
    if(valor==="chartdiv2"){
      $("#chartdiv2").show();
      initMap();
    }else{
      $("#chartdiv2").hide();
    }

    
    $("#"+valor).removeClass('rollOut').addClass('slideInDown');

    $('#demo').jplist({
        itemsBox: '.demo-tbl'
        , itemPath: '.tbl-item'
        , panelPath: '.jplist-panel'
        , storage: 'localstorage' //'', 'cookies', 'localstorage'           
        , storageName: 'jplist-tabl'
    });
  }

</script>
@endsection
