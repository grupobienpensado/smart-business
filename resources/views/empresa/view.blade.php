<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')
@section('title', 'Ver Empresa')
<!--@section('sidebar')
    @parent    
    @endsection-->
@section('content') 
<!-- Material Design Bootstrap -->
 
<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
<style type="text/css">

  .observaciones-img{
    width: 60px;
  }

  .observaciones-p{
    font-size: small;
  }

  .observaciones-title{
    font-size: larger;    
    font-weight: bold;
  }

  #chartdiv {
    height: 100%;
  }

  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .mas-pequeño{
    line-height: 1.5;
    font-size: small;
    color: #636363;
    font-weight: 400;
    display: table-row !important;
  }

  .centrar-vertical{
    vertical-align: middle !important;
  }

  .profile-empresa{
    margin-right: auto!important;
    margin-left: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    width: 250px;
    height: 250px;
    -webkit-border-radius: 200px 200px 200px 200px;
    top: -100px !important;
    position: relative;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  .img-empresa{
    padding: 30px;
    border: solid 0px;
    line-height: 140;
    position: absolute;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center;
    top: 50%;
    bottom: 50%;
  }

  .mx-auto {
    margin-right: auto!important;
    margin-left: auto!important;
    margin-bottom: auto;
    margin-top: auto;
  }

  .card-header {
    margin: 20px 0px;
  }
  
  .marco_principal{
    display: block;
    position: relative;
    overflow: hidden;
    text-decoration: none;
  }
  .liquidFillGaugeText { font-family: Helvetica; font-weight: bold; }
  #fillgauge5 {
    -webkit-border-radius: 50%;
    overflow: hidden;
    background-color: #178bca;
    margin: auto;
    padding: 0px;
  }
  .content_sedes {
    width: 96px;
    height: 96px;
    margin-right: auto!important;
    margin-left: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    -webkit-border-radius: 200px 200px 200px 200px !important;
    position: relative !important;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden !important;
    background-color: #fff !important;
  }
  .img_sedes {
    max-width: 275px;
    min-height: 75px;
    background: none;
    border: solid 0px !important;
    line-height: 140 !important;
    position: absolute !important;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center !important;
    top: 50% !important;
    bottom: 50% !important;
    left: -90% !important;
  }

  p:first-letter {
    text-transform: uppercase !important;
  }

  span:first-letter {
    text-transform: uppercase !important;
  }

  .soloprimer:first-letter{
    text-transform: uppercase !important;
  }

  .capitalize{
    text-transform: capitalize !important;
  }

  .text-muted{
    text-transform: lowercase;
  }
  p.normalp{
    font-weight: 400;
  }
  .unalinea{
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }
  .my-6{ margin-left: 25%; margin-right: 34%; }
</style>

<div class="container-fluid animated flipInX" id="list">
  <div class="row justify-content-md-center">
    <div class="col-12 col-md-auto panel-view">
    <div class="marco_principal">
      <img src="
         @if(!empty($model->principal))
            {{ url('/') }}/images/file/empresas/principal/{{ $model->principal }}
         @else
            https://placeholdit.imgix.net/~text?txtsize=33&txt=Principal&w=350&h=150 
         @endif" style="width: 1100px;height: 300px;margin-top: 12px;">
      </div>
      <div class="profile-empresa">
        <img src="
           @if(!empty($model->logo))
              {{ url('/') }}/images/file/empresas/principal/{{ $model->logo }}
           @else
              http://via.placeholder.com/250x250/fff/948e8e?text=Logo 
           @endif" class="img-fluid mx-auto d-block img-empresa">
      </div>

      <div style="top: -100px !important; position: relative;">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
            <?php if($permiso_anadir_cliente=="Si"){ ?>
              <a href="{{ url('crearcliente') }}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Añadir Cliente</a>
            <?php } ?>
            <?php if($permiso_anadir_sede=="Si"){ ?>
              <a href="{{ url('agregarsede') }}/{{ $model->id }}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Añadir Sede</a>
            <?php } ?>
              <?php if($permiso_editar=="Si"){ ?>
              <a href="{{ url('editarempresa') }}/{{ $model->id }}" class="btn btn-amarillo btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Empresa</a>
              <?php } ?>
              <a href="#titulobservaciones" id="comentarbtn" class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Comentar</a>
            </div>
          </div>
          <div class="col-8 col-md-8 centrado">
            <div  class="hijo">
              <h2 class="titulo">{{ $model->nombre }}</h2>
              <p class="">
                <i class="fa fa-map-marker"></i>
                <span class="soloPri">
                <?php 
                  try {
                    $paissede = \Illuminate\Support\Facades\DB::select('SELECT pais FROM empresa_sedes WHERE importancia_sede = "Principal" AND empresa_id = '.$model->id.' LIMIT 1');
                    echo $paissede[0]->pais;
                  } catch (Exception $e) {
                    
                  }
                ?>
                </span>
              </p>
              <p class="capitalize">  
                  <i class="fa fa-flag-o"></i> Fundada
                  <?php 
                  header("Content-Type: text/html;charset=utf-8");
                    try {                      
                      setlocale(LC_ALL, "es_CO.UTF-8");

                      if (isset($model->finauguracion) && !empty($model->finauguracion) && $model->finauguracion != "0000-00-00") {
                        echo strftime("%d %B %Y", strtotime($model->finauguracion));
                      }else{
                        echo "(No registra)";
                      }
                    } catch (Exception $e) {
                    }                    
                  ?>                                    
              </p>   
              
              <p class="subtitulo">
                <i class="fa fa-user" aria-hidden="true"></i>
                <?php 
                try {
                  $client = App\Cliente::where('cliente', '=', $model->nombre_p_a_cargo)->get();
                  $client = $client[0];
                ?>
                <span class="subtitulo2 capitalize">{{ $client->tratamiento }}. {{ $client->cliente }}</span><p class="subtitulo3">{{ $client->cargo }}</p>
                <?php
                } catch (Exception $e) {
                ?>
                  <span class="subtitulo2 capitalize">Sr. {{ $model->nombre_p_a_cargo }}</span><span class="subtitulo3">{{ $model->cargo }}</span>
                <?php } ?>
              </p>
            </div>
          </div>
          <div class="col-4 col-md-4 centrado" style="padding-top: 70px">  
            <div class="row">
                  <div class="col-md-12">
                      <div class="hijo">
                          <h6>
                            <i class="fa fa-star-o pull-left" aria-hidden="true"></i>
                            <p class="subtitulo pull-left">
                                Creada el <span class="capitalize">{{ strftime("%d %B %Y", strtotime($model->created_at)) }}</span>
                            </p>
                          </h6>
                          <h6 style="padding-top: 20px;">
                              <i class="fa fa-repeat pull-left" aria-hidden="true"></i>
                              <p class="subtitulo2 pull-left">
                                Actualizada el <span class="capitalize">{{ strftime("%d %B %Y", strtotime($model->updated_at)) }}</span>
                              </p>
                          </h6>
                          <h6 style="padding-top: 20px;">
                              <i class="fa fa-percent pull-left" aria-hidden="true"></i>
                              <p class="subtitulo2 pull-left">
                                Datos llenado al <span class="capitalize"><?=$data['porcentaje'][0]->Porcentaje?> %</span>
                              </p>
                          </h6>
                        </div>
                    </div>
                  <div class="col-md-12">
                    <div class="progress my-6">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$data['porcentaje'][0]->Porcentaje?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$data['porcentaje'][0]->Porcentaje?>%"></div>
                    </div>
                  </div>
              </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <h5 class="card-header"> Información de contacto </h5>
          </div>
          <div class="col-md-6" style="text-align: right;">
            <div style="padding-right: 30px">
              <p class="subtitulo"><a href="{{ $model->pagina_web }}"><i class="fa fa-globe" aria-hidden="true"></i> {{ $model->pagina_web }}</a></p>
              <p class="subtitulo">
                <?php 
                  try {
                    $redes = App\EmpresaRedessociales::where('empresas_id','=', $model->id)->get();
                    foreach ($redes as $key => $value) {
                      echo '<a target="_blank" href="'.$value->valor.'">';
                      if ($value->tipo === "Facebook") {
                        echo '<img src="'.url("/").'/images/icons_redes/facebook.svg" style="max-width: 30px;border: solid 0px;">';
                      }else if ($value->tipo === "Instagran") {
                        echo '<img src="'.url("/").'/images/icons_redes/instagram_logo.svg" style="max-width: 30px;border: solid 0px;">';
                      }else if ($value->tipo === "Twitter") {
                        echo '<img src="'.url("/").'/images/icons_redes/twitter_logo.svg" style="max-width: 30px;border: solid 0px;">';
                      }else if ($value->tipo === "Linkedin") {
                        echo '<img src="'.url("/").'/images/icons_redes/linkedin_logo.svg" style="max-width: 30px;border: solid 0px;">';
                      }else {
                        echo '<img src="'.url("/").'/images/icons_redes/share.svg" style="max-width: 30px;border: solid 0px;">';
                      }
                      echo '</a>';
                    }
                  } catch (Exception $e) {} 
                ?>
              </p>
            </div>
          </div>
          <div class="col-md-6" style="border-left: dashed 1px #54575a;text-align: left;">
            <div style="padding-left: 30px">
              <p class="subtitulo2"><i class="fa fa-envelope" aria-hidden="true"></i> Contacto</p>
              <p class="subtitulo"> {{ $model->email_contacto }}</p>
              <p class="subtitulo2"><i class="fa fa-envelope" aria-hidden="true"></i> Contabilidad</p>
              <p class="subtitulo"> {{ $model->email_contabilidad }}</p>
            </div>
          </div>
        </div>

        <div class="row justify-content-md-center">
          <?php  
            try {
              $totallitros=0; 
            } catch (Exception $e) {}
          ?>
          <div class="col-md-12">
            <h5 class="card-header"> <?php echo count($sedes); ?> Sedes </h5>
          </div>  

          <?php if (isset($sedes)): ?> 
          @foreach($sedes as $sede)            
            <?php 
              try {
                $sede->ciudad = trim($sede->ciudad, " \t\n\r\0\x0B");
              } catch (Exception $e) {}
            ?>
            <div class="col-3 col-sm-2 placeholder img-sedes"> 
              <a href="{{ url('empresasede') }}/{{ $sede->id }}">
              <div class="img-sedes content_sedes">
                <img src="@if(!empty($sede->principal)){{ url('/') }}/images/file/empresas/sede/{{ $sede->principal }} @else https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=1100&h=300 @endif" class="rounded-circle img_sedes menu mx-auto d-block">                
              </div> 
              <div class="text-muted centrado">
                <p class="subtitulo centrado">{{ $sede->ciudad }}</p>
                <p class="subtitulo2 centrado">
                <?php 
                  $s= str_replace('"', '', $sede->litros_dia); 
                  $s= str_replace(':', '', $s); 
                  $s= str_replace('.', '', $s); 
                  $s= str_replace(',', '', $s); 
                  $s= str_replace(';', '', $s); 
                  if (isset($s) && !empty($s)) {
                  $totallitros = $totallitros+$s; 
                  echo number_format($s); }?> Ltrs</p>
              </div>
              </a>
            </div>
          @endforeach
          <?php endif ?>

          <div class="col-3 col-sm-2 placeholder img-sedes"> 
            <div class="img-sedes">
              <a href="#">
                <svg id="fillgauge5" width="96" height="96"></svg>
              </a>
            </div> 
            <div class="text-muted centrado">
              <p class="subtitulo centrado">Total</p>
              <p class="subtitulo2 centrado"><?php echo number_format($totallitros); ?> Ltrs</p>
            </div>
          </div>
          <div class="col-3 col-sm-2 placeholder img-sedes"> 
            <div class="img-sedes">
              <a href="{{ url('/') }}/clientesfil/{{ $model->id }}">
                <img src="{{ url('/') }}/images/boton_cliente.png" style="max-width: 50%;" class="img-fluid rounded-circle menu mx-auto d-block">
              </a>
            </div> 
            <div class="text-muted centrado">
              <p class="subtitulo centrado">Clientes</p>
              <p class="subtitulo2 centrado">
              <?php 
              try {
                $clientes = App\Cliente::where('empresa_id', '=', $model->id)->get(); 
                echo count($clientes);
              } catch (Exception $e) {} 
              ?></p>
            </div>
          </div>
          
        </div>

        <div class="row justify-content-md-center">
          <div class="col-md-12">
            <h5 class="card-header"> Oportunidades </h5>
          </div>
          <div class="col-md-12 centrado">
            <div class="hijo2">
              <?php $opts = App\Oportunidades::where('empresa_id', '=', $model->id)->get(); ?>
              @if (isset($opts) && !empty($opts) && count($opts)>0)
                <div class="table-responsive">            
                  <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                    <thead>
                      <tr>
                        <th class="centrado centrar-vertical">Item</th>
                        <th class="centrado centrar-vertical">Ciudad</th>
                        <th class="centrado centrar-vertical">Productos</th>
                        <th class="centrado centrar-vertical">Valor Oportunidad</th>
                        <th class="centrado centrar-vertical">Responsable</th>
                        <th class="centrado centrar-vertical">Pais</th>
                        <th class="centrado centrar-vertical">Fecha identificación</th>
                        <th class="centrado centrar-vertical">Fecha cierre</th>
                        <th class="centrado centrar-vertical">Ciclo de Venta</th>
                        <th class="centrado centrar-vertical">Probabilidad</th>
                        <th class="centrado centrar-vertical">Acciones</th>                  
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; ?>
                      @foreach($opts as $dato) 
                        <?php if (isset($dato->id) && !empty($dato->id)) { $historia = App\HistorialOportunidades::where('oportunidad_id', $dato->id)->get();?>
                        <tr class="text-center dato">
                            <td class="centrar-vertical"><a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $i++ }}</p></a></td>
                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato->ciudad }}</p></a>
                            </td>

                            <?php 
                              $productosRes = App\OportunidadProducto::where("oportunidad_id",$dato->id)->get();
                              $tootilProductos = "";                     
                              foreach ($productosRes as $res) { 
                                try { 
                                  if (!empty($res->producto)) { 
                                    if (is_numeric($res->producto)) {
                                      $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                                    }else{
                                      $producto->name = "Otro";
                                    }
                                    if (is_numeric($res->referencia)) {
                                      $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop(); 
                                    }else{
                                      $referencia->referencia = "Estandar";
                                    }
                                    $tootilProductos .=$res->cantidad.' '.$producto->name.' '.$referencia->referencia.', '; 
                                  } 
                                } catch (Exception $e) { } 
                              } 
                            ?>              
                            <td class="centrar-vertical" style="text-align: center;">
                              <a href="{{ url('registraroportunidad') }}" target="_blank" class="normal_a" style="font-size: smaller; padding: 0px !important;margin: 0px !important;"><p class="mas-pequeño">{{ $tootilProductos }}</p></a>
                            </td>

                            <td class="unalinea centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                              <p class="mas-pequeño">
                                <?php 
                                  try { 
                                    $otro = $historia->filter(function ($value, $key) {
                                        return $value->key == "val_pesos";                            
                                    });
                                    $valpesosoportunidad = $otro->pop(); 
                                    if(isset($valpesosoportunidad->value)){
                                        echo "$ ".$valpesosoportunidad->value;
                                    }else{
                                        echo "$ 0";
                                    }
                                  } catch (Exception $e) { }
                                ?>
                              </p>
                              </a>
                            </td>

                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                              <p class="mas-pequeño">
                              <?php 
                                try {
                                  $otro = $historia->filter(function ($value, $key) {
                                    return $value->key == "responsable";                            
                                  });
                                  $casi = $otro->pop(); 
                                  if(isset($casi->value) && !empty($casi->value)){
                                    $nombreU = App\User::find($casi->value);
                                    if (isset($nombreU)) {
                                      $nombre   = explode(" ",$nombreU->nombres);
                                      $apellido = explode(" ",$nombreU->apellidos); 
                                      echo $nombre[0] ." ". $apellido[0]; 
                                    }else{
                                      echo $casi->value;
                                    }
                                  } 
                                } catch (Exception $e) {
                                  
                                }
                              ?>
                              </p>
                              </a>
                            </td>

                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato->pais}}</p></a>
                            </td> 

                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                              <p class="mas-pequeño">
                              <?php 
                                $otro = $historia->filter(function ($value, $key) {
                                  return $value->key == "fecha_ff";                            
                                });
                                $casi = $otro->pop(); 
                                if (isset($casi->value)) {echo date($casi->value);} 
                              ?>
                              </p>
                              </a>
                            </td>
                            
                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                              <p class="mas-pequeño"> 
                              <?php 
                                $otro = $historia->filter(function ($value, $key) {
                                  return $value->key == "fecha_oc";                            
                                });
                                $casi = $otro->pop(); 
                                if (isset($casi->value)) {echo $casi->value;}
                              ?>
                              </p>
                              </a>
                            </td>                   

                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                              <p class="mas-pequeño">
                              <?php 
                                $otro = $historia->filter(function ($value, $key) {
                                  return $value->key == "ciclo_venta";                            
                                });
                                $casi = $otro->pop(); 
                                $casi->value = intval($casi->value);
                                if (isset($casi->value)) {
                                  if ($casi->value === 0) {
                                    echo "Perdida";
                                  }else if ($casi->value >= 90) {
                                    echo "Vendida";
                                  }else{
                                    echo $casi->value." %";
                                  }                                  
                                } 
                              ?>
                              </p>
                              </a>
                            </td>

                            <?php 
                              $otro = $historia->filter(function ($value, $key) {
                                return $value->key == "probabilidad";                            
                              });
                              $casi = $otro->pop();
                            ?>

                            <td class="centrar-vertical">
                              <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                              <p class="mas-pequeño"> 
                                <?php if (isset($casi->value)) {echo $casi->value;} ?>
                              </p>
                              </a>
                            </td>
                            <td class="text-center unalinea centrar-vertical">
                              <a href="{{ url('/') }}/oportunidad/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver oportunidad">
                                <i class="fa fa-eye"></i>
                              </a>
                              <a href="{{ url('/') }}/acciones/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Gestionar ciclo de venta">
                                <i class="fa fa-sort-numeric-desc"></i>
                              </a>
                              <a href="{{ url('/') }}/oportunidad/edit/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar oportunidad">
                                <i class="fa fa-pencil"></i>
                              </a>
                            </td>
                        </tr>
                        <?php } ?>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              @else
                <li class="list-group-item justify-content-between">No se encontraron oportunidades relacionadas con esta empresa</li>
              @endif
            </div>
          </div>
        </div>

        <div class="row justify-content-md-center"><a name="titulobservaciones"></a>
          <div class="col-md-12">
            <h5 class="card-header"> Observaciones </h5>
          </div>
          <div class="col-md-12 centrado">
            <!--First review-->
            <div class="hijo2">
              <div id="lisComentariosId"></div>
              <form id="formComentarios">
                {{ csrf_field() }}
                <input type="hidden" name="empresa_id" value="{{ $model->id }}">
                <div class="md-form" style="background-color: rgba(174, 219, 251, 0.71);margin-top: 50px;">
                  <textarea type="text" id="form7" class="md-textarea" name="comentario"></textarea>
                  <label for="form7" style="padding-left: 8px;">Añadir comentario</label>
                </div>
              </form>
              <div class="centrado">
                <button class="boton negro redondo" onclick="guardarComentario({{ $model->id }})"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
              </div>
            </div>
          </div>
        </div>

        <div class="row justify-content-md-center">
          <div class="col-md-12">
            <h5 class="card-header"> Ubicación </h5>
          </div>
          <div class="col-md-12 centrado">
            <div class="hijo2">
              <div id="chartdiv" style="height: 700px !important;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
   </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/mdb.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<!--sweetalert
<script type="text/javascript" src="{{ url('/') }}/js/plugins/sweetalert/sweetalert.min.js"></script>  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/d3.v3.min.js" language="JavaScript"></script>
<script type="text/javascript" src="{{ url('/') }}/js/plugins/chart/liquidFillGauge.js"></script>
<script>
  var config4 = liquidFillGaugeDefaultSettings();

    config4.circleThickness = 0.05;
    config4.circleFillGap = 0.05;
    config4.circleColor = "#178bca";
    config4.textColor = "#fff";
    config4.waveTextColor = "#000";
    config4.waveColor = "#fff";
    config4.textVertPosition = 0.8;
    config4.waveAnimateTime = 900;
    config4.waveRiseTime = 900;
    config4.waveHeight = 0.05;
    config4.waveAnimate = true;
    config4.waveRise = true;
    config4.waveHeightScaling = true;
    config4.displayPercent = true;
    config4.valueCountUp = true;
    config4.waveOffset = 1;
    config4.textSize = 0.5;
    config4.waveCount = 1;
    config4.minValue = 0;
    config4.maxValue =  <?php  
                          if (isset($totallitros) && !empty($totallitros)) {
                            $bodytag = $totallitros;
                            $maslitros = $totallitros+$totallitros; 
                            echo $maslitros; 
                          }else{ echo "0";} ?>;
    var gauge5 = loadLiquidFillGauge("fillgauge5", <?php if (isset($bodytag) && !empty($bodytag)) {echo $bodytag;}else{echo "0";} ?>, config4);

    $(function() {
      setTimeout(function(){ $(".liquidFillGaugeText").html("<?php if (isset($bodytag) && !empty($bodytag)) {echo number_format($bodytag).' &#10; Lt';} ?>"); }, 2000);
      setInterval(function(){ $(".liquidFillGaugeText").html("<?php if (isset($bodytag) && !empty($bodytag)) {echo number_format($bodytag).' &#10; Lt';} ?>"); }, 6000);
      getComentarios({{ $model->id }});      
    });

  direccion="{{ url('/') }}";
  var image = "";
  var contentString = "";
  function initMap() {

    var styledMapType = new google.maps.StyledMapType(
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8ec3b9"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1a3646"
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#4b6878"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#64779e"
            }
          ]
        },
        {
          "featureType": "administrative.neighborhood",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#4b6878"
            }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#334e87"
            }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi",
          "stylers": [
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#283d6a"
            },
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6f9ba5"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3C7680"
            }
          ]
        },
        {
          "featureType": "road",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#304a7d"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2c6675"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#255763"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#b0d5ce"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "transit",
          "stylers": [
            {
              "visibility": "off"
            },
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3a4762"
            }
          ]
        },
        {
          "featureType": "water",
          "stylers": [
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#0e1626"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            },
            {
              "weight": 1.5
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#4e6d70"
            },
            {
              "weight": 1.5
            }
          ]
        }
      ],
      {name: 'Styled Map'}
    );



    var map = new google.maps.Map(document.getElementById('chartdiv'), {
      zoom: 3,
      center: {lat: 7.064034827633218, lng: -73.1567105784718},
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
      }
    });



    image = {
      url: '{{ url("/") }}/images/iconos_empresa/empresa_cliente_icono.svg',
      size: new google.maps.Size(50, 40),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 40)
    };
    var markers = [];
    function addMarker(feature) {          
      var marker = new google.maps.Marker({
        position: feature.position,
        icon: image,
        title: feature.type,
        map: map
      });
      
      var infowindow = new google.maps.InfoWindow({
        content: feature.contenido,
        maxWidth: 200
      });

      marker.addListener('mouseover', function() {
        contentString = feature.contenido;
        marker.setAnimation(google.maps.Animation.BOUNCE);
        infowindow.open(map, marker);
      });

      marker.addListener('mouseout', function() {
        infowindow.close();
      });
      markers.push(marker);
    }

    var features = [
      @foreach($sedes as $dato)
        <?php if (!empty($dato->lat)) { ?>
        {
          position: new google.maps.LatLng({{ $dato->lat }}, {{ $dato->lng }}),
          type: <?php 
                  $nombre = trim($model->nombre, " \t\n\r\0\x0B"); 
                  $nombre  = str_replace(' ', '', $nombre ); 
                  $nombre = preg_replace('([^A-Za-z0-9])', '', $nombre);

                  $s= str_replace('"', '', $dato->litros_dia); 
                  $s= str_replace(':', '', $s); 
                  $s= str_replace('.', '', $s); 
                  $s= str_replace(',', '', $s); 
                  $s= str_replace(';', '', $s); 
                  if (!isset($s) || empty($s)) {
                    $s=0;
                  }
                ?>'{{ $nombre }}',
          contenido:  `<div id="content">
                        <div id="siteNotice">
                        </div>                        
                        <div id="bodyContent">
                          <a href="{{ url('/') }}/empresa/{{ $model->id }}">
                            <img src="
                              @if(!empty($dato->principal))
                                {{ url('/') }}/images/file/empresas/sede/{{ $dato->principal }}
                              @else
                                https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
                              @endif" style="max-width: 100%;" class="img-fluid mx-auto d-block">
                          </a>
                        </div>
                        <h2 class="centrado">{{ $dato->ciudad }}</h2>
                        <p class="centrado">{{ number_format($s) }} Ltrs</p>
                      </div>`
        },
      <?php } ?> 
      @endforeach           
    ];

    for (var i = 0, feature; feature = features[i]; i++) {
      addMarker(feature);
    }
    var markerCluster = new MarkerClusterer(map, markers, { maxZoom: 12, gridSize: 20, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
  }
  

  $(function() {
    getComentarios({{ $model->id }});
    $( "#comentarbtn" ).click(function() {
      setTimeout(function(){ $("#form7").focus() }, 1000);
    });
  });


  guardarComentario = function (id) {
    event.preventDefault();    
    if(true === $("#formComentarios").parsley().validate()){
      console.log("llega"); 
      var datos = new FormData($("#formComentarios")[0]);
      $.ajax({
        url: "{{ url('savecomenempresa') }}",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
          if (data.success) {
            $('#formComentarios')[0].reset();
            getComentarios(id);
          }else{
            swal("Algo salio mal, vuelve a intentar");
          }
        }
    }); 
    }else{
      swal("Faltan campos por completar!");
    }
  }

  getComentarios = function (id) {
    $.ajax({
        url: "{{ url('listcomenempresa') }}/"+id,
        type: 'GET',
        success: function(data){
          if (data.success) {
            console.log(data.datos);
            var html3 = "";
            moment.locale('es');
            for(i in data.datos){
              comentario = data.datos[i];
              if (comentario.user.foto === "" || comentario.user.foto === null) {
                comentario.user.foto = `<img src="http://via.placeholder.com/40x40?text=`+comentario.user.name+`" class="media-object img-circle">`;
              }else{
                comentario.user.foto = `<img src="{{url('/')}}/images/file/clientes/`+comentario.user.foto+`" alt="`+comentario.user.name+`" class="media-object img-circle" style="max-width:40px">`;
              }
              html3 += `
                        <div class="media clearfix header-bottom">
                  <div class="media-left">
                    `+comentario.user.foto+`
                  </div>
                  <div class="media-body">
                    <a href="#">
                      `+comentario.user.name+` `+moment(comentario.created_at).calendar()+`<br>
                      <p class="text-muted username soloprimer normalp">`+comentario.comentario+`</p>
                    </a>
                  </div>
                </div>`;
            }
            $("#lisComentariosId").html(html3);
          }else{

          }
        }
    });
  }
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSg3ZOwMbQvB0xjXAFmr2Ch-7IKV98gno&callback=initMap"></script>
@endsection

