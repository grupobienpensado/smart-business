<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Editar Sede')

<!--@section('sidebar')
    @parent

    
@endsection-->

@section('content')   
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/components/datepicker/material-datetime-picker.css">
<style type="text/css">
  .panel-view{
    -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.75);
    margin-top: 20px;
    margin-bottom: 20px;
    background-color: #fff;
    width: 80%;
    overflow: hidden;
  }

  .profile-empresa{
    margin-right: auto!important;
    margin-left: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    width: 250px;
    height: 250px;
    -webkit-border-radius: 200px 200px 200px 200px;
    top: -100px !important;
    position: relative;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  .img-empresa{
    padding: 30px;
    border: solid 0px;
    line-height: 140;
    position: absolute;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center;
    top: 50%;
    bottom: 50%;
  }

  .mx-auto {
    margin-right: auto!important;
    margin-left: auto!important;
    margin-bottom: auto;
    margin-top: auto;
  }
</style>

<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
   
</style>

<div class="container-fluid animated flipInX">
  <div class="row justify-content-md-center">
    <div class="col-12 col-md-auto panel-view">       
      <form method="POST" action="{{ url('editarsede') }}" enctype="multipart/form-data">
        <div class="panel-title">
            <h2>Editar Sede</h2>
        </div>
        <hr>
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        {{ csrf_field() }}

        <div class="form-group row" style="display: flex;justify-content: center;">
          <div class="dropzone" data-width="1100" data-height="300" data-ajax="false" data-originalsave="true" style="width: 1100px !important;height: 300px !important;max-height: 300px !important;min-width: 1100px !important;min-height: 300px !important;background-image: url('{{ url('/') }}/images/file/empresas/sede/{{ $sede->principal }}');">
            <input type="file" name="principal" accept="image/gif, image/jpeg, image/png">
          </div>
        </div>
        <div class="profile-empresa">
          <img src="
             @if(!empty($model->logo))
                {{ url('/') }}/images/file/empresas/principal/{{ $model->logo }}
             @else
                http://via.placeholder.com/250x250/fff/948e8e?text=Logo; 
             @endif" class="img-fluid mx-auto d-block img-empresa">
        </div>

        <input type="hidden" name="id" value="{{ $sede->id }}">
        <input type="hidden" name="empresa_id" value="{{ $model->id }}">
        <div class="form-group row">
            <div class="col-4">
              <label for="pais" class="col-form-label">Pais</label>
              <select name="pais" class="form-control" id="pais" required>
                <option value="{{ $sede->pais }}">{{ $sede->pais }}</option>
              </select>
            </div>
            <div class="col-4">                     
              <label for="departamento" class="col-form-label">Departamento</label>
              <select name="departamento" class="form-control" id="estados">
                <option value="{{ $sede->departamento }}">{{ $sede->departamento }}</option>
              </select>
            </div>
            <div class="col-4">
              <label for="ciudad" class="col-form-label">Ciudad</label>
              <select name="ciudad" class="form-control" id="ciudad">
                <option value="{{ $sede->ciudad }}">{{ $sede->ciudad }}</option>
              </select> 
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12">
                <label for="direccion" class="col-form-label">Direccion exacta</label>
                <input class="form-control" type="text" name="direccion" value="{{ $sede->direccion }}" placeholder="Por favor ingrese la direccion exacta" id="direccion">
            </div>                                      
        </div>

        <div class="form-group row">            
            <div class="col-3">
                <label for="finauguracion" class="col-form-label">Fecha de inauguración</label>                   
                <input class="form-control" type="date" value="{{ $sede->finauguracion }}" name="finauguracion">
            </div> 
            <div class="col-3">
                <label for="importancia_sede" class="col-form-label">Importancia de sede</label>
                <select name="importancia_sede" class="form-control">
                    <option value="Principal" <?php if($sede->importancia_sede=="Principal"){echo "selected";} ?>>Principal</option>
                    <option value="Secundaria" <?php if($sede->importancia_sede=="Secundaria"){echo "selected";} ?>>Secundaria</option>
                </select>
            </div>
            <div class="col-3">
                <label for="tipo_sede" class="col-form-label">Tipo de sede</label>
                <select name="tipo_sede" class="form-control">
                    <option value="Administrativa" 
                      <?php if($sede->tipo_sede=="Administrativa"){echo "selected";} ?>>Administrativa</option>
                    <option value="Planta" <?php if($sede->tipo_sede=="Planta"){echo "selected";} ?>>Planta</option>
                    <option value="Administrativa y Planta" 
                      <?php if($sede->tipo_sede=="Administrativa y Planta"){echo "selected";} ?>>Administrativa y Planta</option>
                </select>
            </div>  
            <div class="col-3">
                <label for="litros_dia" class="col-form-label">Cantidad litros dia</label>
                <input class="form-control" type="text" name="litros_dia" value="{{ $sede->litros_dia }}" placeholder="Ingrese la cantidad de litros dia">
            </div>
        </div>

        <div class="form-group row" >
            <div class="col-6">
              <label for="encargado" class="col-form-label">Gerente o encargado</label>
              <select name="encargado" class="form-control" id="cliente">
                <option value="{{ $sede->encargado }}" selected>
                <?php 
                if (!empty($sede->encargado) && is_numeric($sede->encargado)) {
                    $cliente = App\Cliente::findOrFail($sede->encargado); 
                    echo $cliente->cliente; 
                }
                ?>
                </option>
              </select>
            </div>

            <div class="col-6" id="mastelefonos">
              <label class="col-form-label">Telefono de la sede</label>
              <div class="input-group">
                  <select name="telefono[0][telefono_tipo]" class="col-2 form-control" id="codteltipo0" onchange="funcambiarinput(this.id, 0)">
                      <option value="Telefono">Telefono</option>
                      <option value="Celular">Celular</option>
                      <option value="Radio">Radio</option>
                  </select>
                  <input type="text" name="telefono[0][telefono_indp]" class="col-2 form-control centrado" placeholder="IND P" id="codtelindp0" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom">
                  <input type="text" name="telefono[0][telefono_indc]" class="col-2 form-control centrado" placeholder="IND C" id="codtelindc0" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom">
                  <input type="text" name="telefono[0][telefono_numero]" class="col-4 form-control centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum0" >
                  <input type="text" name="telefono[0][telefono_ext]" class="col-2 form-control centrado" placeholder="EXT" id="codtelext0" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom">
                  <span class="input-group-btn">
                      <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarTelefono()"><i class="fa fa-plus text-success"></i></button>
                  </span>
              </div>
            </div>
            
            @foreach($telefonos as $key => $telefono)
            <div class="col-6" id="">
              <label class="col-form-label">Telefono de la sede</label>
              <div class="input-group">
                  <select name="telefono[{{ $key+1 }}][telefono_tipo]" class="col-2 form-control" id="codteltipo{{ $key+1 }}" onchange="funcambiarinput(this.id, {{ $key+1 }})">
                      <option value="Telefono" <?php if($telefono->telefono_tipo=="Telefono"){echo "selected";} ?>>Telefono</option>
                      <option value="Celular" <?php if($telefono->telefono_tipo=="Celular"){echo "selected";} ?>>Celular</option>
                      <option value="Radio" <?php if($telefono->telefono_tipo=="Radio"){echo "selected";} ?>>Radio</option>
                  </select>
                  <input type="text" name="telefono[{{ $key+1 }}][telefono_indp]" class="col-2 form-control centrado" placeholder="IND P" id="codtelindp{{ $key+1 }}" value="{{ $telefono->telefono_indp }}" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom">
                  <input type="text" name="telefono[{{ $key+1 }}][telefono_indc]" class="col-2 form-control centrado" placeholder="IND C" id="codtelindc{{ $key+1 }}" value="{{ $telefono->telefono_indc }}" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom">
                  <input type="text" name="telefono[{{ $key }}][telefono_numero]" class="col-4 form-control centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum{{ $key+1 }}" value="{{ $telefono->telefono_numero }}" >
                  <input type="text" name="telefono[{{ $key+1 }}][telefono_ext]" class="col-2 form-control centrado" placeholder="EXT" id="codtelext{{ $key+1 }}" value="{{ $telefono->telefono_ext }}" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom">
                  <span class="input-group-btn">
                      <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarTelefono()"><i class="fa fa-plus text-success"></i></button>
                  </span>
              </div>
            </div> 
            @endforeach 
        </div>
        <div class="form-group row">
            <div class="col-6">
                <label for="email_contacto" class="col-form-label">Correo electronico contacto, quejas, reclamos</label>
                <input class="form-control" type="email" name="email_contacto" value="{{ $sede->email_contacto }}" placeholder="Por favor ingrese un correo">
            </div>
            <div class="col-6">
                <label for="email_contabilidad" class="col-form-label">Correo electronico contabilidad</label>
                <input class="form-control" type="email" name="email_contabilidad" value="{{ $sede->email_contabilidad }}" placeholder="Por favor ingrese un correo">
            </div>
        </div>
        
        <div class="form-group">
          <div id="map"></div>
          <div class="coordinates">
            <em class="lat">Latitud</em>
            <em class="lon">Longitud</em>
            <input type="text" id="lat" name="lat" value="{{ $sede->lat }}">
            <input type="text" id="lng" name="lng" value="{{ $sede->lng }}">
          </div>
        </div>
        <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
      </form>
    </div>
  </div>
</div>
@endsection
@section('scripts')
  <script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
  <script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
  <script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
  <script>

    var marker;
    var map;
    var geocoder;
    var infoWindow = {};
    var pais = "";
    var departamento = "";
    var ciudad = "";
    var direccion = "";
    $(function () {
      $('#cliente').select2({
        placeholder: "Seleccione una cliente",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("cliente2") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
      });

      $("[data-toggle='tooltip']").tooltip();

      $('.dropzone').html5imageupload();

      $('#direccion').change(function () {
        direccion = "";
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        console.log(dir);
        codeAddress(dir);
      });

      $('#pais').change(function () {
        pais = $(this).val();
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        codeAddress(dir);
      });

      $('#estados').change(function () {
        departamento = $(this).val();
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      });

      $('#ciudad').change(function () {
        ciudad = $(this).val();
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      });

      $('#pais').select2({
          // Activamos la opcion "Tags" del plugin
          placeholder: "Seleccione un Pais",
          pais: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("pais") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });

      $('#estados').select2({
          // Activamos la opcion "Tags" del plugin
          placeholder: "Seleccione un Departamento",
          estados: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("estados") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term,
                      state: $('#pais').val()
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });

      $('#ciudad').select2({
          // Activamos la opcion "Tags" del plugin
          placeholder: "Seleccione una Ciudad",
          ciudad: true,
          tokenSeparators: [','],
          ajax: {
              dataType: 'json',
              url: '{{ url("ciudades") }}',
              delay: 250,
              data: function(params) {
                  return {
                      term: params.term,
                      estado: $('#estados').val(),
                      pais: $('#pais').val()
                  }
              },
              processResults: function (data, page) {
                return {
                  results: data
                };
              },
          }
      });
    });

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < marker.length; i++) {
        marker[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    function initMap() {
      geocoder = new google.maps.Geocoder();
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 59.325, lng: 18.070}
      });

      infoWindow = new google.maps.InfoWindow({map: map});

      funGeolocation();
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    }

    function attachSecretMessage() {      
        $("#lat").val(marker.position.lat);
        $("#lng").val(marker.position.lng);    
    }

    function funGeolocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $("#lat").val(pos.lat);
          $("#lng").val(pos.lng); 
          marker.setMap(map);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: pos
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function codeAddress(dir) {
      if (marker) {
        marker.setMap(null);
      }
      
      var address = dir;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
          
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: results[0].geometry.location
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          $("#lat").val(results[0].geometry.location.lat);
          $("#lng").val(results[0].geometry.location.lng);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

    i = 0;
    
    agregarTelefono = function() {
        i++;
        dato = `<div class="col-6 animated rollIn" id="T`+i+`">
                    <label class="col-form-label">Telefono `+i+`</label>
                        <div class="input-group">
                            <select name="telefono[`+i+`][telefono_tipo]" class="col-2 form-control" id="codteltipo`+i+`" onchange="funcambiarinput(this.id, `+i+`)">
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Radio">Radio</option>
                            </select>
                            <input type="text" name="telefono[`+i+`][telefono_indp]" class="col-2 form-control centrado" placeholder="IND P" id="codtelindp`+i+`" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <input type="text" name="telefono[`+i+`][telefono_indc]" class="col-2 form-control centrado" placeholder="IND C" id="codtelindc`+i+`" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <input type="text" name="telefono[`+i+`][telefono_numero]" class="col-4 form-control centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum`+i+`">
                            <input type="text" name="telefono[`+i+`][telefono_ext]" class="col-2 form-control centrado" placeholder="EXT" id="codtelext`+i+`" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                </div>`;
        $( "#mastelefonos" ).after(dato);
        $("[data-toggle='tooltip']").tooltip();
    }

    quitar = function (id) {
      $(id).removeClass('rollIn').addClass('hinge');
      setTimeout(function(){ $(id).remove(); }, 1000);            
    }

    funcambiarinput = function (id, val) {
      valor = $("#"+id).val();
      if (valor==="Celular") {
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-10').addClass('col-9');            
      }else if(valor==="Telefono"){
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).show();
          $("#codtelext"+val).show();
          $("#codtelnum"+val).removeClass('col-9 col-10').addClass('col-7'); 
      }else if(valor==="Radio"){
          $("#codtelindp"+val).hide();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-9').addClass('col-10');
      }
    }

  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbw8hYpmFO_VWUZLufrAL1qfnPFQp4JaM&callback=initMap"></script>
@endsection

