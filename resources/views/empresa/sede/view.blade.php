<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')
@section('title', 'Ver Sede')
<!--@section('sidebar')
    @parent    
    @endsection-->
@section('content') 
<!-- Material Design Bootstrap -->
<link href="{{ url('/') }}/MDB/MDB Free/css/mdb.min.css" rel="stylesheet">
<style type="text/css">
 

  .observaciones-img{
    width: 60px;
  }

  .observaciones-p{
    font-size: small;
  }

  .observaciones-title{
    font-size: larger;    
    font-weight: bold;
  }

  

  #chartdiv {
    height: 100%;
  }
  /* Optional: Makes the sample page fill the window. */
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .profile-empresa{
    margin-right: auto!important;
    margin-left: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    width: 250px;
    height: 250px;
    -webkit-border-radius: 200px 200px 200px 200px;
    top: -100px !important;
    position: relative;
    -webkit-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    -moz-box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    box-shadow: 6px 7px 38px -4px rgba(5,29,96,0.48) !important;
    overflow: hidden;
    background-color: #fff;
  }

  .img-empresa{
    padding: 30px;
    border: solid 0px;
    line-height: 140;
    position: absolute;
    display: table-cell !important;
    vertical-align: middle !important;
    text-align: center;
    top: 50%;
    bottom: 50%;
  }

  .mx-auto {
    margin-right: auto!important;
    margin-left: auto!important;
    margin-bottom: auto;
    margin-top: auto;
  }

  .card-header {
    margin: 20px 0px;
  }

  .liquidFillGaugeText { font-family: Helvetica; font-weight: bold; }

  #fillgauge5{
    -webkit-border-radius: 50%;
    top: 18px;
    right: 0px;
    position: absolute;
    overflow: hidden;
    background-color: #178bca;
    margin: auto;
    padding: 0px;
  }

  .titulo-img{
    position: absolute;
    font-weight: bold;
    color: #1c2331;
    top: 50px;
    left: 190px;
    font-family: 'Archivo Black', sans-serif;
  }

  .indigo {
    background-color: #063f9c!important;
  }  
  .subtitulo-msj{
    margin-bottom: 4px !important;
    font-size: small !important;
    color: #54575a !important;
  }

  .mas-pequeño{
    line-height: 1.5;
    font-size: small;
    color: #636363;
    font-weight: 400;
    display: table-row !important;
  }

  .centrar-vertical{
    vertical-align: middle !important;
  }
  .unalinea{
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  p:first-letter {
    text-transform: uppercase !important;
  }

  span:first-letter {
    text-transform: uppercase !important;
  }

  .soloprimer:first-letter{
    text-transform: uppercase !important;
  }

  .capitalize{
    text-transform: capitalize !important;
  }

  .text-muted{
    text-transform: lowercase;
  }
  p.normalp{
    font-weight: 400;
  }
</style>

<div class="container-fluid animated flipInX" id="list">
  <div class="row justify-content-md-center">
    <div class="col-12 col-md-auto panel-view">

      <img src="
         @if(!empty($model->principal))
            {{ url('/') }}/images/file/empresas/sede/{{ $model->principal }}
         @else
            http://via.placeholder.com/350x150/fff/948e8e?text=FotoSede 
         @endif" style="width: 100%;height: 300px;margin-top: 12px;">
      <div class="profile-empresa-arriba">
        <img src="
          <?php $empresa = App\Empresa::findOrFail($model->empresa_id);
            if(!empty($empresa->logo)){
              echo url('/').'/images/file/empresas/principal/'.$empresa->logo;
            }else{
              echo 'http://via.placeholder.com/250x250/fff/948e8e?text=Logo';
            } 
          ?>" class="img-fluid mx-auto d-block img-empresa-arriba">
      </div>
      <h2 class="titulo-img"><span class="badge indigo" style="font-size: -webkit-xxx-large;">{{ $model->ciudad }}</span></h2>
      <svg id="fillgauge5" width="300" height="300"></svg>

      <div style="padding-top: 100px;">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
            <?php if($permiso_anadir_cliente=="Si"){ ?>
              <a href="{{ url('crearcliente') }}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Añadir Cliente</a>
            <?php } ?>
            <?php if($permiso_editar_sede=="Si"){ ?>
              <a href="{{ url('editarsede') }}/{{ $model->id }}" class="btn btn-amarillo btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Sede</a>
            <?php } ?>
              <a href="#titulobservaciones" class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Comentar</a>
              <?php if($permiso_eliminar_sede=="Si"){ ?>
              <a class="btn btn-danger btn-sm" onclick="eliminar_sede({{ $model->id }}, {{ $model->empresa_id }})"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>
              <?php } ?>
            </div>
          </div>
          <div class="col-6 col-md-6 centrado">
            <div  class="hijo">
              <h2 class="titulo"><?php echo $empresa->nombre; ?></h2>
              <p class="subtitulo">
                <i class="fa fa-map-marker"></i>
                {{ $model->pais .", ". $model->ciudad }}
              </p>
              <p class="subtitulo"><small>{{ $model->direccion }}</small></p>
              <p class="subtitulo">                                
                <i class="fa fa-flag-o"></i> Fundada el  {{ date("d M Y",strtotime($model->finauguracion)) }}                               
              </p>   
              
              <p class="subtitulo2">
                <i class="fa fa-user" aria-hidden="true"></i>{{ $model->encargado }}
              </p>
            </div>
          </div>
          <div class="col-6 col-md-6 centrado" style="padding-top: 70px">  
            <div class="hijo"> 
              <h6><span class="badge indigo">Sede {{ $model->importancia_sede }}</span></h6>
              <p class="subtitulo">
                Tipo {{ $model->tipo_sede }}
              </p>                   
              <p class="subtitulo">
                <i class="fa fa-star-o" aria-hidden="true"></i>
                Creada el {{ date("d M Y",strtotime($model->created_at)) }}
              </p>
              <p class="subtitulo2">
                <i class="fa fa-repeat" aria-hidden="true"></i>
                Actualizada el {{ date("d M Y",strtotime($model->updated_at)) }}
              </p>
            </div>   
          </div>
          <div class="col-md-12">
            <div class="hijo2 row justify-content-md-center">
              <div class="table-responsive">
                <table class="table product-table" style="margin-top: 50px;">
                    <thead>
                        <tr class="tr-table">
                            <th class="centrado">Información de contacto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td class="">
                            <div class="col-md-12">
                              <p class="subtitulo4 centrado">
                                <i class="fa fa-envelope" aria-hidden="true"></i> 
                                Contacto: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="subtitulo">{{ $model->email_contacto }}</span> 
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <i class="fa fa-envelope" aria-hidden="true"></i> 
                                Contabilidad: &nbsp;&nbsp; <span class="subtitulo">{{ $model->email_contabilidad }}</span>
                              </p>
                            </div>                            
                          </td>
                        </tr>
                    </tbody>
                </table>
              </div>

              <div class="table-responsive">
                <table class="table product-table" style="margin-top: 50px;">
                    <thead>
                        <tr class="tr-table">
                            <th class="centrado">Oportunidades</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td class="centrado">
                            <?php $opts = App\Oportunidades::where('empresa_id', '=', $model->empresa_id)->where('ciudad', '=', $model->ciudad)->get(); ?>
                            @if (isset($opts) && !empty($opts) && count($opts)>0)
                              <div class="table-responsive">            
                                <table id="example" cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                                  <thead>
                                    <tr>
                                      <th class="centrado centrar-vertical">Item</th>
                                      <th class="centrado centrar-vertical">Ciudad</th>
                                      <th class="centrado centrar-vertical">Productos</th>
                                      <th class="centrado centrar-vertical">Valor Oportunidad</th>
                                      <th class="centrado centrar-vertical">Responsable</th>
                                      <th class="centrado centrar-vertical">Pais</th>
                                      <th class="centrado centrar-vertical">Fecha identificación</th>
                                      <th class="centrado centrar-vertical">Fecha cierre</th>
                                      <th class="centrado centrar-vertical">Ciclo de Venta</th>
                                      <th class="centrado centrar-vertical">Probabilidad</th>
                                      <th class="centrado centrar-vertical">Acciones</th>                  
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $i=1; ?>
                                    @foreach($opts as $dato) 
                                      <?php if (isset($dato->id) && !empty($dato->id)) { $historia = App\HistorialOportunidades::where('oportunidad_id', $dato->id)->get();?>
                                      <tr class="text-center dato">
                                          <td class="centrar-vertical"><a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $i++ }}</p></a></td>
                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato->ciudad }}</p></a>
                                          </td>

                                          <?php 
                                            $productosRes = App\OportunidadProducto::where("oportunidad_id",$dato->id)->get();
                                            $tootilProductos = "";                     
                                            foreach ($productosRes as $res) { 
                                              try { 
                                                if (!empty($res->producto)) { 
                                                  if (is_numeric($res->producto)) {
                                                    $producto = App\Producto::where('id',"=", $res->producto)->get()->pop();
                                                  }else{
                                                    $producto->name = "Otro";
                                                  }
                                                  if (is_numeric($res->referencia)) {
                                                    $referencia = App\ProductoReferencias::where('id',"=", $res->referencia)->get()->pop(); 
                                                  }else{
                                                    $referencia->referencia = "Estandar";
                                                  }
                                                  $tootilProductos .=$res->cantidad.' '.$producto->name.' '.$referencia->referencia.', '; 
                                                } 
                                              } catch (Exception $e) { } 
                                            } 
                                          ?>              
                                          <td class="centrar-vertical" style="text-align: center;">
                                            <a href="{{ url('registraroportunidad') }}" target="_blank" class="normal_a" style="font-size: smaller; padding: 0px !important;margin: 0px !important;"><p class="mas-pequeño">{{ $tootilProductos }}</p></a>
                                          </td>

                                          <td class="unalinea centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                                            <p class="mas-pequeño">
                                              <?php 
                                                try {
                                                  $otro = $historia->filter(function ($value, $key) {
                                                    return $value->key == "valor_oportunidad";                            
                                                  });
                                                  $valoracomvertir = $otro->pop();
                                                  $otro2 = $historia->filter(function ($value, $key) {
                                                    return $value->key == "moneda";                            
                                                  });
                                                  $moneda = $otro2->pop(); 
                                                
                                              
                                                  if ($moneda->value != 'COP') {
                                                    $cantidad = str_replace(',','',$valoracomvertir->value);
                                                    if(($moneda->value )=='EUR'){
                                                      $valor_conversion=$cantidad*$euro;
                                                    }elseif(($moneda->value )=='USD'){
                                                      $valor_conversion=$cantidad*$dolar;
                                                    }
                                                    $resultado=$valor_conversion;
                                                    echo "$ ".number_format($resultado);
                                                  }else{
                                                      echo "$ ".$valoracomvertir->value;
                                                  }
                                                } catch (Exception $e) { }
                                              ?>
                                            </p>
                                            </a>
                                          </td>

                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                                            <p class="mas-pequeño">
                                            <?php 
                                              try {
                                                $otro = $historia->filter(function ($value, $key) {
                                                  return $value->key == "responsable";                            
                                                });
                                                $casi = $otro->pop(); 
                                                if(isset($casi->value) && !empty($casi->value)){
                                                  $nombreU = App\User::find($casi->value);
                                                  if (isset($nombreU)) {
                                                    $nombre   = explode(" ",$nombreU->nombres);
                                                    $apellido = explode(" ",$nombreU->apellidos); 
                                                    echo $nombre[0] ." ". $apellido[0]; 
                                                  }else{
                                                    echo $casi->value;
                                                  }
                                                } 
                                              } catch (Exception $e) {
                                                
                                              }
                                            ?>
                                            </p>
                                            </a>
                                          </td>

                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"><p class="mas-pequeño">{{ $dato->pais}}</p></a>
                                          </td> 

                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                                            <p class="mas-pequeño">
                                            <?php 
                                              $otro = $historia->filter(function ($value, $key) {
                                                return $value->key == "fecha_ff";                            
                                              });
                                              $casi = $otro->pop(); 
                                              if (isset($casi->value)) {echo date($casi->value);} 
                                            ?>
                                            </p>
                                            </a>
                                          </td>
                                          
                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                                            <p class="mas-pequeño"> 
                                            <?php 
                                              $otro = $historia->filter(function ($value, $key) {
                                                return $value->key == "fecha_oc";                            
                                              });
                                              $casi = $otro->pop(); 
                                              if (isset($casi->value)) {echo $casi->value;}
                                            ?>
                                            </p>
                                            </a>
                                          </td>                   

                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a"> 
                                            <p class="mas-pequeño">
                                            <?php 
                                              $otro = $historia->filter(function ($value, $key) {
                                                return $value->key == "ciclo_venta";                            
                                              });
                                              $casi = $otro->pop(); 
                                              $casi->value = intval($casi->value);
                                              if (isset($casi->value)) {
                                                if ($casi->value === 0) {
                                                  echo "Perdida";
                                                }else if ($casi->value >= 90) {
                                                  echo "Vendida";
                                                }else{
                                                  echo $casi->value." %";
                                                }                                  
                                              } 
                                            ?>
                                            </p>
                                            </a>
                                          </td>

                                          <?php 
                                            $otro = $historia->filter(function ($value, $key) {
                                              return $value->key == "probabilidad";                            
                                            });
                                            $casi = $otro->pop();
                                          ?>

                                          <td class="centrar-vertical">
                                            <a href="{{ url('oportunidad/'.$dato->id) }}" target="_blank" class="normal_a">
                                            <p class="mas-pequeño"> 
                                              <?php if (isset($casi->value)) {echo $casi->value;} ?>
                                            </p>
                                            </a>
                                          </td>
                                          <td class="text-center unalinea centrar-vertical">
                                            <a href="{{ url('/') }}/oportunidad/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Ver oportunidad">
                                              <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ url('/') }}/acciones/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Gestionar ciclo de venta">
                                              <i class="fa fa-sort-numeric-desc"></i>
                                            </a>
                                            <a href="{{ url('/') }}/oportunidad/edit/{{ $dato->id }}" target="_blank" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Editar oportunidad">
                                              <i class="fa fa-pencil"></i>
                                            </a>
                                          </td>
                                      </tr>
                                      <?php } ?>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                            @else
                              <li class="list-group-item justify-content-between">No se encontraron oportunidades relacionadas con esta empresa</li>
                            @endif
                          </td>
                        </tr>
                    </tbody>
                </table>
              </div>

              <div class="table-responsive"><a name="titulobservaciones"></a>
                <table class="table product-table" style="margin-top: 50px;">
                    <thead>
                        <tr class="tr-table">
                            <th class="centrado">Observaciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>
                            <div class="col-md-12">
                                <div id="lisComentariosId"></div>
                                <form id="formComentarios">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="empresa_sede_id" value="{{ $model->id }}">
                                  <div class="md-form" style="background-color: rgba(174, 219, 251, 0.71);margin-top: 50px;">
                                    <textarea type="text" id="form7" class="md-textarea" name="comentario"></textarea>
                                    <label for="form7" style="padding-left: 8px;">Añadir comentario</label>
                                  </div>
                                </form>
                                <div class="centrado">
                                  <a href="#" class="boton negro redondo" onclick="guardarComentario({{ $model->id }})"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                                </div>
                            </div>
                          </td>
                        </tr>
                    </tbody>
                </table>
              </div>

              <div class="table-responsive">
                <table class="table product-table" style="margin-top: 50px;">
                    <thead>
                        <tr class="tr-table">
                            <th class="centrado">Ubicación</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td class="centrado">
                            <div id="chartdiv" style="height: 700px !important"></div>
                          </td>
                        </tr>
                    </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>      
    </div>
   </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/MDB/MDB Free/js/mdb.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script> 

<script src="https://d3js.org/d3.v3.min.js"></script>
<script type="text/javascript" src="/js/plugins/chart/liquidFillGauge.js"></script>

<script language="JavaScript">
$(document).ready(function(){
    setTimeout(function(){ getComentarios({{$model->id}}); }, 1000);
})
    var config4 = liquidFillGaugeDefaultSettings();

    config4.circleThickness = 0.05;
    config4.circleFillGap = 0.05;
    config4.circleColor = "#178bca";
    config4.textColor = "#fff";
    config4.waveTextColor = "#000";
    config4.waveColor = "#fff";
    config4.textVertPosition = 0.8;
    config4.waveAnimateTime = 900;
    config4.waveRiseTime = 900;
    config4.waveHeight = 0.05;
    config4.waveAnimate = true;
    config4.waveRise = true;
    config4.waveHeightScaling = true;
    config4.displayPercent = true;
    config4.valueCountUp = true;
    config4.waveOffset = 1;
    config4.textSize = 0.5;
    config4.waveCount = 1;
    config4.minValue = 0;
    config4.maxValue =  <?php 
                  $s= str_replace('"', '', $model->litros_dia); 
                  $s= str_replace(':', '', $s); 
                  $s= str_replace('.', '', $s); 
                  $s= str_replace(',', '', $s); 
                  $s= str_replace(';', '', $s); 
                  if (isset($s) && !empty($s)) {
                    $bodytag = $s;
                    $maslitros = $s+$s; 
                    echo $maslitros; 
                  }else{ echo "0"; } ?>;
    var gauge5 = loadLiquidFillGauge("fillgauge5", <?php if (isset($bodytag) && !empty($bodytag)) {echo $bodytag;}else{ echo "0"; } ?>, config4);

    $(function() {
      setTimeout(function(){ $(".liquidFillGaugeText").html("<?php if (isset($bodytag) && !empty($bodytag)) {echo number_format($bodytag).' &#10; Lt';} ?>"); }, 2000);
      setInterval(function(){ $(".liquidFillGaugeText").html("<?php if (isset($bodytag) && !empty($bodytag)) {echo number_format($bodytag).' &#10; Lt';} ?>"); }, 6000);
      getComentarios({{ $model->id }});      
    });

    direccion="{{ url('/') }}";
  var image = "";
  var contentString = "";
  function initMap() {

    var styledMapType = new google.maps.StyledMapType(
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8ec3b9"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1a3646"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "stylers": [
            {
              "color": "#ffffff"
            },
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#64779e"
            }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#4b6878"
            }
          ]
        },
        {
          "featureType": "landscape",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#334e87"
            }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6f9ba5"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3C7680"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#304a7d"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2c6675"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#255763"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#b0d5ce"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#023e58"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#98a5be"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1d2c4d"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#283d6a"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3a4762"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#0e1626"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#4e6d70"
            }
          ]
        }
      ],
      {name: 'Styled Map'}
    );

    var map = new google.maps.Map(document.getElementById('chartdiv'), {
      zoom: 3,
      center: {lat: 7.064034827633218, lng: -73.1567105784718},
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
      }
    });
 
    <?php  
      $nombre = trim($model->ciudad, " \t\n\r\0\x0B"); 
      $nombre  = str_replace(' ', '', $nombre ); 
      $nombre = preg_replace('([^A-Za-z0-9])', '', $nombre);
    ?>

    image = {
      url: '{{ url("/") }}/images/iconos_empresa/empresa_cliente_icono.svg',
      size: new google.maps.Size(50, 40),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 40)
    };
    var markers = [];
    function addMarker(feature) {          
      var marker = new google.maps.Marker({
        position: feature.position,
        icon: image,
        title: feature.type,
        map: map
      });

      var infowindow = new google.maps.InfoWindow({
        content: feature.contenido,
        maxWidth: 200
      });

      marker.addListener('mouseover', function() {
        contentString = feature.contenido;
        marker.setAnimation(google.maps.Animation.BOUNCE);
        infowindow.open(map, marker);
      });

      marker.addListener('mouseout', function() {
        infowindow.close();
      });
      markers.push(marker);
    }

    var features = [
      <?php if (!empty($model->lat)) { ?>
        {
          position: new google.maps.LatLng({{ $model->lat }}, {{ $model->lng }}),
          type: <?php 
                      $nombre = trim($model->nombre, " \t\n\r\0\x0B"); 
                      $nombre  = str_replace(' ', '', $nombre ); 
                      $nombre = preg_replace('([^A-Za-z0-9])', '', $nombre);
                      echo "'".$nombre."',";
                ?>
          contenido:  `<div id="content">
                        <div id="siteNotice">
                        </div>
                        
                        <div id="bodyContent">
                          <a href="{{ url('/') }}/empresa/{{ $model->id }}">
                            <img src="
                              @if(!empty($model->principal))
                                {{ url('/') }}/images/file/empresas/sede/{{ $model->principal }}
                              @else
                                https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100 
                              @endif" style="max-width: 100%;" class="img-fluid mx-auto d-block">
                          </a>
                        </div>
                        <h2 class="centrado">{{ $model->ciudad }}</h2>
                        <p class="centrado"><?php if (is_numeric($model->litros_dia)) {
                          echo number_format($model->litros_dia);
                        }else{ echo $model->litros_dia; } ?> Ltrs</p>
                      </div>`
        },
      <?php } ?>
    ];

    for (var i = 0, feature; feature = features[i]; i++) {
      addMarker(feature);
    }
    var markerCluster = new MarkerClusterer(map, markers, { maxZoom: 12, gridSize: 20, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
  }

  function guardarComentario(id) {
    event.preventDefault();    
    if(true === $("#formComentarios").parsley().validate()){
      var datos = new FormData($("#formComentarios")[0]);
      $.ajax({
        url: "{{ url('savecomensede') }}",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
          if (data.success) {
            $('#formComentarios')[0].reset();
            getComentarios(id);
          }else{
            swal("Algo salio mal, vuelve a intentar");
          }
        }
    }); 
    }else{
      swal("Faltan campos por completar!");
    }
  }

  function getComentarios(id) {
    $.ajax({
        url: "{{ url('listcomensede') }}/"+id,
        type: 'GET',
        success: function(data){
          if (data.success) {
            console.log(data.datos);
            var html3 = "";
            moment.locale('es');
            for(i in data.datos){
              comentario = data.datos[i];
              if (comentario.user.foto === "" || comentario.user.foto === null) {
                comentario.user.foto = `<img src="http://via.placeholder.com/40x40?text=`+comentario.user.name+`" class="media-object img-circle">`;
              }else{
                comentario.user.foto = `<img src="{{url('/')}}/images/file/clientes/`+comentario.user.foto+`" alt="`+comentario.user.name+`" class="media-object img-circle" style="max-width:40px">`;
              }
              html3 += `
                        <div class="media clearfix header-bottom">
                  <div class="media-left">
                    `+comentario.user.foto+`
                  </div>
                  <div class="media-body">
                    <a href="#">
                      `+comentario.user.name+` `+moment(comentario.created_at).calendar()+`<br>
                      <p class="text-muted username soloprimer normalp">`+comentario.comentario+`</p>
                    </a>
                  </div>
                </div>`;
            }
            $("#lisComentariosId").html(html3);
          }else{

          }
        }
    });
  }

  eliminar_sede = function(id, empresa_id) {
    swal({
      title: '¿Estás seguro?',
      html: $('<div>')
        .addClass('some-class')
        .text('¡No podrás revertir esto!'),
      animation: false,
      customClass: 'animated tada',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo!',
      cancelButtonText: 'Cancelar',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      showLoaderOnConfirm: true,
      preConfirm: function () {
        return new Promise(function (resolve) {
          $.get( "{{ url('/') }}/sede/eliminar/"+id, function( data ) {
            if (data.success) {
              swal('¡Eliminado!','Su sede ha sido eliminada.','success');
                window.location.href = "{{ url('empresa') }}/"+empresa_id;
              resolve()
            }else{
              swal("Algo salio mal, vuelve a intentar",'warning');
              resolve()
            }
          });
        })
      },
      allowOutsideClick: false
    });
  }
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSg3ZOwMbQvB0xjXAFmr2Ch-7IKV98gno&callback=initMap"></script>
@endsection

