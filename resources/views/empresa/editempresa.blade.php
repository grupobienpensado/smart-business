<!-- Stored in resources/views/child.blade.php -->

@extends('template.app')

@section('title', 'Crear empresa')

<!--@section('sidebar')
    @parent

    
@endsection-->

@section('content')
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">

<link rel="stylesheet" href="{{ url('/') }}/css/ripples.min.css"/>
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<style type="text/css">
    
    .form-control-sm, .btn-sm{
        z-index: inherit !important;
    }

    .content {
        text-align: -webkit-auto;
    }

    html {
        font-size: 13px !important;
    }

    form {
        font-size: 13px !important;
    }

    .note-popover {
        display: none;
    }
    
    .jumbotron{
        height: 100px;
        padding-top: 15px;
        background-color: #ffffff !important;
    }
    .banner_em > .dropzone:after { content: 'Suelte su imagen aquí o haga clic para agregar una! (11.52 %)' !important; }
    .foto_em > .dropzone:after { content: 'Suelte su imagen aquí o haga clic para agregar una! (19.2 %)' !important; }
</style>
    <div class="jumbotron">
        <div class="panel-title">
            <div class="pull-right">
                <div class="btn-group">
                    <a href="{{ url('empresas') }}" class="btn btn-primary btn-sm"><i class="fa fa-list" aria-hidden="true"></i> Listar empresa</a>
                </div>
            </div>
            <h2>Editar empresa</h2>
        </div>
    </div>

    <div class="card" style="margin-bottom: 30px;">
        <div class="card-block">  
                @if(!empty($model))
                <form method="POST" id="formCrearEmpresa" enctype="multipart/form-data">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{ csrf_field() }} 
                    <div class="form-group row banner_em" style="display: flex;justify-content: center;">
                      <div class="dropzone" data-width="1100" data-height="300" data-ajax="false" data-originalsave="true" style="width: 1100px !important;height: 300px !important;max-height: 300px !important;min-width: 1100px !important;min-height: 300px !important;background-image: url('{{ url('/') }}/images/file/empresas/principal/{{ $model->principal }}');">
                        <input type="file" name="principal" accept="image/gif, image/jpeg, image/png">
                      </div>
                    </div>

                    <div class="form-group row foto_em" style="display: flex;justify-content: center;height: 12px;margin-bottom: 90px;">
                      <div class="dropzone logo-empresa-fotante" data-width="200" data-height="200" data-ajax="false" data-originalsave="true" style="width: 200px !important;height: 200px !important;max-height: 200px !important;min-width: 200px !important;min-height: 200px !important;background-image: url('{{ url('/') }}/images/file/empresas/principal/{{ $model->logo }}');" >
                        <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
                      </div>
                    </div>  
                    
                    <div class="form-group row">
                        <input type="hidden" name="id" value="{{ $model->id }}">
                        <div class="col-6">
                            <label for="nombre" class="col-form-label">Empresa (3.84 %)</label>
                            <input class="form-control" type="text" name="nombre" placeholder="Por favor ingrese el Nombre de la Empresa" value="{{ $model->nombre }}" required>
                        </div>
                        <div class="col-6">
                            <label class="col-form-label">Identificación (3.84 %)</label>
                            <div class="input-group">
                                <select name="tipo_identificacion" class="col-3 form-control">
                                    <option value="NIT" <?php if ($model->tipo_identificacion === "NIT"):  echo "selected";  endif ?>>NIT</option>
                                    <option value="ID" <?php if ($model->tipo_identificacion === "ID"):  echo "selected";  endif ?>>ID</option>
                                    <option value="RUP" <?php if ($model->tipo_identificacion === "RUP"):  echo "selected";  endif ?>>RUP</option>
                                    <option value="CC" <?php if ($model->tipo_identificacion === "CC"):  echo "selected";  endif ?>>CC</option>
                                </select>
                                <?php 
                                    $s= str_replace('"', '', $model->nit); 
                                    $s= str_replace(':', '', $s); 
                                    $s= str_replace('.', '', $s); 
                                    $s= str_replace(',', '', $s); 
                                    $s= str_replace(';', '', $s); 
                                ?>
                                <input class="form-control col-9" type="number" name="nit" placeholder="Por favor ingrese el Nit" value="{{ $s }}">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                <input class="form-control col-3" type="number" name="dv" placeholder="D.V." value="{{ $model->dv }}">
                            </div>                            
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-6">
                            <label for="pagina_web" class="col-form-label">Pagina Web (3.84 %)</label>
                            <input class="form-control" type="text" name="pagina_web" placeholder="Por favor ingrese la Pagina Web" value="{{ $model->pagina_web }}">
                        </div>
                        <div class="col-2">
                            <label for="finauguracion" class="col-form-label">Fecha de inauguración (3.84 %)</label>
                            <input id="output" class="form-control date" type="text" name="finauguracion" placeholder="Fecha de inauguración" value="{{ $model->finauguracion }}">
                        </div> 
                        <?php  $user = App\User::find($model->responsable); ?>
                        <div class="col-4">
                            <label class="col-form-label">Responsable (3.84 %)</label>
                            <select name="responsable" class="form-control" id="responsable" required>
                                <option value="{{ $user->id }}"><?php echo $user->name; ?></option>
                            </select>
                        </div>                                      
                    </div>

                    <div class="form-group row">
                        <div class="col-6">
                            <label for="ciudad" class="col-form-label">Ciudad inauguración (3.84 %)</label>
                            <input class="form-control" type="text" name="ciudad" placeholder="Por favor ingrese la ciudad" value="{{ $model->ciudad }}">  
                        </div>                    
                        <div class="col-6">
                            <label for="cliente_id" class="col-form-label">Nombre persona de mas alto cargo (11.52 %)</label>
                            <select name="cliente_id" class="form-control" id="cliente">
                                <option value="{{ $model->cliente_id }}" selected>
                                <?php 
                                if (!empty($model->cliente_id)) {
                                    $cliente = App\Cliente::find($model->cliente_id); 
                                    if (isset($cliente) && !empty($cliente)) {
                                        echo $cliente->cliente;
                                    } 
                                }
                                ?>
                                </option>
                            </select>
                        </div>
                    </div> 

                    <div id="redessociales">
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Redes Sociales (11.52 %)</label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <select name="redsocial[0][tipo]" class="col-2 form-control">
                                        <option value="Facebook">Facebook</option>
                                        <option value="Instagran">Instagran</option>
                                        <option value="Twitter">Twitter</option>
                                        <option value="Linkedin">Linkedin</option>
                                        <option value="Otra">Otra</option>
                                    </select>
                                    <input type="text" class="col-10 form-control" name="redsocial[0][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social">
                                    <span class="input-group-btn">
                                        <button class="btn btn-outline-success pull-right btn-sm" type="button" onclick="agregarRedesSociales()"><i class="fa fa-plus text-success"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <?php $redes = App\EmpresaRedessociales::where('empresas_id','=', $model->id)->get();
                            foreach ($redes as $key => $value) { ?>
                            <div class="form-group row animated rollIn" id="R{{$key}}">
                                <label class="col-2 col-form-label">Redes Sociales (11.52 %)</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <select name="redsocial[{{$key}}][tipo]" class="col-2 form-control">
                                            <option value="Facebook" <?php if ($value->tipo === "Facebook") { echo "selected"; } ?>>Facebook</option>
                                            <option value="Instagran" <?php if ($value->tipo === "Instagran") { echo "selected"; } ?>>Instagran</option>
                                            <option value="Twitter" <?php if ($value->tipo === "Twitter") { echo "selected"; } ?>>Twitter</option>
                                            <option value="Linkedin" <?php if ($value->tipo === "Linkedin") { echo "selected"; } ?>>Linkedin</option>
                                            <option value="Otra" <?php if ($value->tipo === "Otra") { echo "selected"; } ?>>Otra</option>
                                        </select>
                                        <input type="text" class="col-10 form-control" name="redsocial[{{$key}}][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social" value="{{ $value->valor }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#R{{$key}}')"><i class="fa fa-minus text-danger"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                i = <?php echo $key; ?>;
                            </script>
                        <?php } ?>
                    </div>

                    <div class="form-group row">
                        <div class="col-6">
                            <label for="email_contacto" class="col-form-label">Correo electronico contacto, quejas, reclamos (3.84 %)</label>
                            <input class="form-control" type="email" name="email_contacto" placeholder="Por favor ingrese un correo" value="{{ $model->email_contacto }}">
                        </div>
                        <div class="col-6">
                            <label for="email_contabilidad" class="col-form-label">Correo electronico contabilidad (3.84 %)</label>
                            <input class="form-control" type="email" name="email_contabilidad" placeholder="Por favor ingrese un correo" value="{{ $model->email_contabilidad }}">
                        </div>
                    </div>                      
                    <button type="button" class="btn btn-essi pull-right" onclick="guardarEmpresa()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar cambios</button>
                </form>
                @else
                <h2>No hemos encontrado la empresa</h2>
                @endif
        </div>
    </div>

@endsection

@section('scripts')

<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<!--sweetalert -->

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script>
     $('.dropzone').html5imageupload();

        $(function () {
            $('.date').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true,
                nowButton: true,
                lang: 'es',
                cancelText : 'Cancelar',
                okText: 'Aceptar',
                nowText: 'Nuevo',
                clearText: 'Limpiar'
            });
        });

    $('#cliente').select2({
        placeholder: "Seleccione una cliente",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("cliente2") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $('#responsable').select2({
        // Activamos la opcion "Tags" del plugin
         placeholder: "Seleccione una responsable",
        ciudad: true,
        tokenSeparators: [','],
        ajax: {
            dataType: 'json',
            url: '{{ url("vendedores") }}',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
              return {
                results: data
              };
            },
        }
    });

    $('.dropzone').html5imageupload();       

    quitar = function (id) {
        $(id).removeClass('rollIn').addClass('hinge');
        setTimeout(function(){ $(id).remove(); }, 1000);            
    }

    j = 0;    
    
    agregarRedesSociales = function() {
        j++;
        dato = `<div class="form-group row animated rollIn" id="R`+j+`">
                    <label class="col-2 col-form-label">Redes Sociales `+j+`</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <select name="redsocial[`+j+`][tipo]" class="col-2 form-control">
                                <option value="Facebook">Facebook</option>
                                <option value="Instagran">Instagran</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Linkedin">Linkedin</option>
                                <option value="Otra">Otra</option>
                            </select>
                            <input type="text" class="col-10 form-control" name="redsocial[`+j+`][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social">
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#R`+j+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                    </div>
                </div>`;
        $( "#redessociales" ).after(dato);
    }

    funcambiarinput = function (id, val) {
        valor = $("#"+id).val();
        if (valor==="Celular") {
            $("#codtelindc"+val).hide();
            $("#codtelext"+val).hide();
            $("#codtelnum"+val).removeClass('col-7').addClass('col-9');            
        }else{
            $("#codtelindc"+val).show();
            $("#codtelext"+val).show();
            $("#codtelnum"+val).removeClass('col-9').addClass('col-7'); 
        }
    }

    guardarEmpresa = function () {
        event.preventDefault();    
        if(true === $("#formCrearEmpresa").parsley().validate()){
            console.log("llega");
            swal({   title: "Esta Seguro?",   
              text: "Va a guardar esta información",   
              type: "info",   
              showCancelButton: true,   
              //closeOnConfirm: false,   
              showLoaderOnConfirm: true, 
            }).then(function(){ 
                var datos = new FormData($("#formCrearEmpresa")[0]);
                $.ajax({
                    url: "{{ url('editarempresa') }}",
                    data: datos,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                        if (data.success) {
                            swal("Empresa guardada con exito");
                            location.href="{{ url('/')}}/empresa/{{ $model->id }}";
                        }else{
                            swal("Algo salio mal, vuelve a intentar");
                        }
                    }
                });
            });  
        }else{
            swal("Faltan campos por completar!");
        }
    }
</script>

@endsection

