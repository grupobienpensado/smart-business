@extends('template.app')
@section('title', 'Ver Feria')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/asistida.css">
<style>
    .profile-feria-arriba{width:200px!important;height:200px!important;-webkit-border-radius:200px 200px 200px 200px!important;top:148px!important;left:88px!important;position:absolute!important;-webkit-box-shadow:6px 7px 38px -4px rgba(5,29,96,0.48)!important;-moz-box-shadow:6px 7px 38px -4px rgba(5,29,96,0.48)!important;box-shadow:6px 7px 38px -4px rgba(5,29,96,0.48)!important;overflow:hidden!important;background-color:#fff!important;margin:auto!important}
.profile-feria-exponente{width:200px!important;height:200px!important;-webkit-border-radius:200px 200px 200px 200px!important;top:112px!important;right:111px!important;position:absolute!important;-webkit-box-shadow:6px 7px 38px -4px rgba(5,29,96,0.48)!important;-moz-box-shadow:6px 7px 38px -4px rgba(5,29,96,0.48)!important;box-shadow:6px 7px 38px -4px rgba(5,29,96,0.48)!important;overflow:hidden!important;background-color:#fff!important;margin:auto!important}
p.detalle{text-align:left;color:#000;background-color:#dbdbdb;padding:12px;border-radius:8px}
span.title{font-size:x-large;color:#051d60;font-weight:400;display:block;text-align:center}
.asistentes_ESSI{border-radius:4px;background-image:-moz-linear-gradient(91deg,#8ee0ff 0%,#4881b3 55%,#012266 100%);background-image:-webkit-linear-gradient(91deg,#8ee0ff 0%,#4881b3 55%,#012266 100%);background-image:-ms-linear-gradient(91deg,#8ee0ff 0%,#4881b3 55%,#012266 100%);padding:20px;padding-top:30px;padding-bottom:30px;float:left;color:#f0f8ff;font-size:50px;font-weight:600}
.title_asistentes{font-size:x-large;color:#051d60;font-weight:400;float:left;padding-left:10px;padding-top:25px}
.title_responsable{font-size:x-large;color:#051d60;font-weight:400;display:block;text-align:center}
.detalle_responsable{font-size:large;color:#795548;font-weight:400;display:block;text-align:center}
.text-muted{text-transform:initial;white-space:normal!important;text-align:center}
.tipo_feria{color:#051d60;position:absolute;top:425px!important;right:115px!important;display:none}
.plano_ubicacion{border-radius:4px;background-image:-moz-linear-gradient(91deg,#8ee0ff 0%,#4881b3 55%,#012266 100%);background-image:-webkit-linear-gradient(91deg,#8ee0ff 0%,#4881b3 55%,#012266 100%);background-image:-ms-linear-gradient(91deg,#8ee0ff 0%,#4881b3 55%,#012266 100%);padding:20px;float:left;color:#f0f8ff;font-size:50px;font-weight:600}
body{background:#ddd;font:14px/1.6em sans-serif}
.ribbon{width:241px;height:60px;margin:100px auto 0;position:relative;color:#fff;font:28px/60px sans-serif;text-align:center;text-transform:uppercase;background:#474992;-webkit-animation:main 250ms;-moz-animation:main 250ms;-ms-animation:main 250ms;animation:main 250ms}
.ribbon i{position:absolute}
.ribbon i:first-child,.ribbon i:nth-child(2){position:absolute;left:-20px;bottom:-20px;z-index:-1;border:20px solid transparent;border-right-color:#043140;-webkit-animation:edge 500ms;-moz-animation:edge 500ms;-ms-animation:edge 500ms;animation:edge 500ms}
.ribbon i:nth-child(2){left:auto;right:-20px;border-right-color:transparent;border-left-color:#043140}
.ribbon i:nth-child(3),.ribbon i:last-child{width:20px;bottom:-20px;left:-41px;z-index:-2;border:30px solid #474992;border-left-color:transparent;-webkit-animation:back 600ms;-moz-animation:back 600ms;-ms-animation:back 600ms;animation:back 600ms;-webkit-transform-origin:100% 0;-moz-transform-origin:100% 0;-ms-transform-origin:100% 0;transform-origin:100% 0}
.ribbon i:last-child{bottom:-20px;left:auto;right:-41px;border:30px solid #474992;border-right-color:transparent;-webkit-transform-origin:0 0;-moz-transform-origin:0 0;-ms-transform-origin:0 0;transform-origin:0 0}
#main-content-wrapper{overflow-x:hidden}
@-webkit-keyframes main {
0%{-webkit-transform:scaleX(0)}
100%{-webkit-transform:scaleX(1)}
}
@-webkit-keyframes edge {
0%,50%{-webkit-transform:scaleY(0)}
100%{-webkit-transform:scaleY(1)}
}
@-webkit-keyframes back {
0%,75%{-webkit-transform:scaleX(0)}
100%{-webkit-transform:scaleX(1)}
}
@-moz-keyframes main {
0%{-moz-transform:scaleX(0)}
100%{-moz-transform:scaleX(1)}
}
@-moz-keyframes edge {
0%,50%{-moz-transform:scaleY(0)}
100%{-moz-transform:scaleY(1)}
}
@-moz-keyframes back {
0%,75%{-moz-transform:scaleX(0)}
100%{-moz-transform:scaleX(1)}
}
@keyframes main {
0%{transform:scaleX(0)}
100%{transform:scaleX(1)}
}
@keyframes edge {
0%,50%{transform:scaleY(0)}
100%{transform:scaleY(1)}
}
@keyframes back {
0%,75%{transform:scaleX(0)}
100%{transform:scaleX(1)}
}
.cinta{z-index:5;position:absolute;top:191px;right:-738px}
@media screen and (max-width: 1600px) {
.cinta{right:-582px}
}
@media screen and (max-width: 1366px) {
.cinta{right:-469px}
}
.foto-inv{max-width:113px!important;max-height:113px!important;min-width:113px!important;min-height:113px!important}
.btn-outline-success:hover{color:#FFF}
.foto-inv{max-width:113px!important;max-height:113px!important;min-width:113px!important;min-height:113px!important}
#main-content-wrapper{overflow:hidden}
.ui-autocomplete{position:absolute;top:100%;left:0;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0;margin:2px 0 0;list-style:none;font-size:14px;text-align:left;background-color:#fff;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.15);border-radius:4px;-webkit-box-shadow:0 6px 12px rgba(0,0,0,0.175);box-shadow:0 6px 12px rgba(0,0,0,0.175);background-clip:padding-box}
.ui-autocomplete > li > div{display:block;padding:3px 20px;clear:both;font-weight:400;line-height:1.42857143;color:#333;white-space:nowrap}
.ui-state-hover,.ui-state-active,.ui-state-focus{text-decoration:none;color:#262626;background-color:#f5f5f5;cursor:pointer}
.ui-helper-hidden-accessible{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}
</style>
<?php setlocale(LC_ALL, "es_CO.UTF-8");  ?>
<input type="hidden" id="id_feria" value="{{$id}}">
<input type="hidden" id="URL" value="{{url('/')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="widget">
    <?php echo $menu; ?>
    <div class="widget-content">
        @if($feria->tipo === "Exponente") @if(isset($exponente->baner) && !empty($exponente->baner))
        <img src="/storage/ferias/{{ $exponente->baner }}" style="width: 100%; height: 300px; margin-top: 12px; background: none;"> @else
        <img src="/storage/ferias/header-bitacora.jpg" style="width: 100%; height: 300px; margin-top: 12px; background: none;"> @endif
        <h2 class="tipo_feria">Exponente</h2>
        @else
        <img src="/storage/ferias/header-bitacora.jpg" style="width: 100%; height: 300px; margin-top: 12px; background: none;">
        <h2 class="tipo_feria">Visitante</h2>
        @endif

        <div class="profile-feria-arriba">
            <img src="/storage/ferias/{{ $feria->logo }}" class="img-fluid mx-auto d-block img-empresa-arriba" style="background: none;">
        </div>
        <div class="profile-feria-exponente">
            <img src="/storage/ferias/icons/essi_logo.svg" class="img-fluid mx-auto d-block img-empresa-arriba" style="background: none;">
        </div>
        <h2 style="text-align: left;color: #051d60;">{{ $feria->nombre }} <span style="font-size: medium;">del  {{ strftime("%A %d", strtotime($feria->fecha_inicio)) }} al {{ strftime("%A %d de %B de %Y", strtotime($feria->fecha_fin)) }} <?php echo $feria->pais." / ".$feria->estado." / ".$feria->ciudad; ?></span></h2>

        <p class="detalle">@php echo($feria->descripcion); @endphp</p>
        @if(isset($productos) && !empty($productos))
        <h2 style="text-align: center;color: #051d60;">Equipos a Exhibir</h2>
        <div class="row">
            @php echo($productos); @endphp
        </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                <p><span class="asistentes_ESSI">{{ $feria->invitados_essi }}</span>
                    <span class="title_asistentes">Asistentes ESSI</span></p>
            </div>
            <div class="col-md-3">
                <p><span class="asistentes_ESSI">{{ $feria->invitados_total }}</span>
                    <span class="title_asistentes">Personas invitadas</span></p>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{url('/')}}/images/file/clientes/{{$responsable->foto}}" class="img-circle foto-inv" alt="Avatar">
                    </div>
                    <div class="col-md-9">
                        <p style="padding-top: 20px;"><span class="title_responsable">Responsable ESSI</span><br>
                            <span class="detalle_responsable">{{ $responsable->nombres.' '.$responsable->apellidos }}</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
               <?php if($feria->ubicacion_foto != ''){ ?>
                <p><span class="plano_ubicacion"><a data-gallery="" href="{{url('/')}}/storage/ferias/{{$feria->ubicacion_foto}}"><img src="/storage/ferias/icons/plano_ubicacion_icon.svg"></a></span>
                    <span class="title_asistentes">Plano de Ubicación</span></p>
               <?php }else{ ?>
               <p><span class="title_asistentes">No existe Plano de Ubicación</span></p>
               <?php } ?>
            </div>
        </div>
        <p class="detalle"><span class="title">Atuendo</span><br><?php if(isset($exponente->atuendo)){ ?> @php echo($exponente->atuendo); @endphp <?php }else{ echo 'No hay atuendo'; } ?></p>
        <div class="row">
            <div class="col-md-6">
                <p class="detalle"><span class="title">Alcances</span><br><?php if(isset($exponente->atuendo)){ ?> @php echo($exponente->alcances); @endphp <?php }else{ echo 'No hay alcances'; } ?></p>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p class="detalle"><span class="title">Objetivos</span><br><?php if(isset($exponente->atuendo)){ ?> @php echo($exponente->objetivos); @endphp <?php }else{ echo 'No hay objetivos'; } ?></p>
                    </div>
                    <div class="col-md-12">
                        <p class="detalle"><span class="title">Estrategia</span><br><?php if(isset($exponente->atuendo)){ ?> @php echo($exponente->estrategia); @endphp <?php }else{ echo 'No hay estrategias'; } ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                <div class="card-body">
                   <h3 class="card-title">Oportunidades identificadas</h3>
                    <p>
                      <button class="btn btn-outline-success mb-4" type="button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>data-toggle="collapse" data-target="#add_oportunidad"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para añadir oportunidades')"<?php } ?> aria-expanded="false" aria-controls="add_oportunidad">
                        Añadir oportunidad
                      </button>
                    </p>
                   <div class="collapse container" id="add_oportunidad">
                      <form id="add_opor_frm">
                        <div class="form-group">
                        <input type="hidden" name="action" id="action" value="1">
                         <input type="hidden" name="oportunidad_id" id="oportunidad_id">
                         <input type="hidden" name="id_feria" id="id_feria" value="{{$id}}">
                          <input type="text" class="form-control" id="quick_oportunidad" placeholder="Escriba una oportunidad">
                          <button class="btn btn-success mt-4">Guardar</button>
                          <button class="btn btn-danger mt-4" type="button" data-toggle="collapse" data-target="#add_oportunidad" aria-expanded="false" aria-controls="add_oportunidad">
                        Cancelar
                      </button>
                        </div>
                      </form>
                    </div>
                    <div class="container">
                   <div class="row w-100" id="oportunidades_container">
                   </div>
                   </div>
                </div>
            </div>
            </div>
               <div class="col-md-6">
                <div class="card">
                <div class="card-body">
                   <h3 class="card-title">Posibles clientes</h3>
                   <p>
                      <button class="btn btn-outline-success mb-4" type="button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>data-toggle="collapse" data-target="#add_cliente"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para añadir clientes')"<?php } ?> aria-expanded="false" aria-controls="add_cliente">
                        Añadir cliente
                      </button>
                    </p>
                   <div class="collapse container" id="add_cliente">
                      <form id="add_client_frm">
                        <div class="form-group">
                        <input type="hidden" name="action_c" id="action_c" value="1">
                         <input type="hidden" name="id_cliente_c" id="id_cliente_c">
                         <input type="hidden" name="id_feria_c" id="id_feria_c" value="{{$id}}">
                          <input type="text" class="form-control" id="quick_cliente" placeholder="Escriba el nombre de un cliente">
                          <button class="btn btn-success mt-4">Guardar</button>
                          <button class="btn btn-danger mt-4" type="button" data-toggle="collapse" data-target="#add_cliente" aria-expanded="false" aria-controls="add_cliente">
                        Cancelar
                      </button>
                        </div>
                      </form>
                    </div>
                   <div class="w-100" id="clientes_container"></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="container cinta">
    <div class="ribbon">
        {{$feria->tipo}}
        <i></i>
        <i></i>
        <i></i>
        <i></i>
    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;" />
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/asistida.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        ListarClientesOportunidades();

        $("#quick_oportunidad").autocomplete({
            source: function(request, response) {
                $.ajax({
                    type: "POST",
                    url: '{{ url("/") }}/feria/oportunidades/autocomplete',
                    data: request,
                    success: response,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            },
            minLength: 2,
            select: function(event, ui) {
                $('#oportunidad_id').val(ui.item.id);
            },
            position: {
                my: "center bottom",
                at: "center top",
                of: $("#add_oportunidad"),
                collision: "flip flip"
            }
        });

        $("#quick_cliente").autocomplete({
            source: function(request, response) {
                $.ajax({
                    type: "POST",
                    url: '{{ url("/") }}/feria/clientes/autocomplete',
                    data: request,
                    success: response,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            },
            minLength: 2,
            select: function(event, ui) {
                $('#id_cliente_c').val(ui.item.id);
            },
            position: {
                my: "center bottom",
                at: "center top",
                of: $("#add_cliente"),
                collision: "flip flip"
            }
        });

        $(document).on('submit', '#add_opor_frm', function(event) {
            event.preventDefault();
            $.ajax({
                url: '{{ url("/") }}/feria/oportunidades/acciones',
                method: 'POST',
                dataType: 'json',
                cache: false,
                timeout: 10000,
                data: $(this).serialize(),
                success: function(suss) {
                    if (suss.res) {
                        $("#add_opor_frm")[0].reset();
                        $("#add_oportunidad").collapse('hide');
                        ListarClientesOportunidades();
                    }
                },
                error: function(err) {
                    console.log(err);
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $(document).on('submit', '#add_client_frm', function(event) {
            event.preventDefault();
            $.ajax({
                url: '{{ url("/") }}/feria/clientes/acciones',
                method: 'POST',
                dataType: 'json',
                cache: false,
                timeout: 10000,
                data: $(this).serialize(),
                success: function(suss) {
                    if (suss.res) {
                        $("#add_client_frm")[0].reset();
                        $("#add_cliente").collapse('hide');
                        ListarClientesOportunidades();
                    }
                },
                error: function(err) {
                    console.log(err);
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('.ribbon').on('click', function() {
            var self = $(this),
                newone = self.clone(true);

            self.before(newone);
            $("." + self.attr("class") + ":last").remove();
        });
    });

    function ListarClientesOportunidades(){
        $.ajax({
            url: '{{ url("/") }}/feria/clientes-oportunidades',
            method: 'POST',
            cache: false,
            dataType: 'json',
            timeout: 10000,
            data: {id: {{$id}} },
            success: function(suss){
                if(suss.res){
                    $("#oportunidades_container").html(suss.oportunidades);
                    $("#clientes_container").html(suss.clientes);
                }
            },
            error: function(err){
                console.log(err);
            },
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
