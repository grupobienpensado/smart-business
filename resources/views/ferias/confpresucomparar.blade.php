@extends('template.app')
@section('title', 'Comparar Presupuesto Conferencia')

@section('content')
<style>
.alert-success{
    display: none;
    width: 215px;
    text-align: left;
    position: absolute;
    right: 0;
    top: 0;
    opacity: 0.5;
}
</style>
<div class="content" style="background-color: #fff;">
    <?=$data['menu']?>
    <div class="main-header mt-5">
        <h2>Conferencia</h2>
        <em>Comprar presupuesto</em>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="top-content">
                <ul class="list-inline quick-access">
                    <li>
                        <a href="#">
                            <div class="quick-access-item bg-color-blue">
                                <i class="fa fa-envelope"></i>
                                <h5>NOTA</h5><em>Los datos se almacenan automaticamente</em>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Comparar Presupuesto</h3>
        </div>
        <div class="widget-content">
            <ul class="list-unstyled activity-list">
                <li>
                    <i class="fa fa-bell-o activity-icon pull-left"></i>
                    <p class="text-left">
                        <a href="#">{{$data['nombre']}}</a> Recuerda que el presupuesto se debe llenar hasta del día <a href="#">{{$data['fecha_limite_presupuesto']}}</a> <span class="timestamp">@if(isset($data['vencio_presupuesto'])) Vencio hace {{$data['vencio_presupuesto']}} dias @else No ha vencido @endif</span>
                    </p>
                </li>
                <li>
                    <i class="fa fa-bell-o activity-icon pull-left"></i>
                    <p class="text-left">
                        <a href="#">{{$data['nombre']}}</a> Recuerda que el valor real se debe llenar hasta del día <a href="#">{{$data['fecha_limite_real']}}</a> <span class="timestamp">@if(isset($data['vencio_real'])) Vencio hace {{$data['vencio_real']}} dias @else No ha vencido @endif</span>
                    </p>
                </li>
            </ul>
            <form class="form-horizontal" role="form" id="formulario_basico" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$data['id']}}">
                <div class="row">
                    <div class="col-md-5">
                        <a>Concepto</a>
                    </div>
                    <div class="col-md-2">
                        <a>Presupuesto</a>
                    </div>
                    <div class="col-md-2">
                        <a>Valor Real</a>
                    </div>
                    <div class="col-md-2">
                        <a>Ajuste</a>
                    </div>
                </div>
                @php $i=0; $t_ajuste=0; @endphp
                @foreach($data['items'] as $concepto)
                <input type="hidden" value="{{$concepto->id}}" name="ajuste[{{$i}}][id_concepto]">
                <div class="row">
                    <div style="width: 2%;">
                        {{$i+1}}.
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label for="concepto{{$i}}" class="control-label sr-only">{{$concepto->concepto}}</label>
                            <input type="text" class="form-control detalle" name="presupuesto[{{$i}}][concepto]" id="concepto{{$i}}" value="{{$concepto->concepto}}" placeholder="Concepto" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="valor" class="control-label sr-only">Valor</label>
                            <input type="text" class="form-control valor" id="valor{{$i}}" name="ajuste[{{$i}}][valor]" @if($concepto->presupuesto != "") value="{{'$ '.number_format($concepto->presupuesto, 0, ',', '.')}}" @else value="$ 0" @endif id="valor" placeholder="Valor" @if($data['permiso_presupuesto'] =="No" || $data['permiso_agregar_editar_eliminar']=="Si") readonly style="cursor: no-drop" onclick="no_permiso('Usted no tiene permisos para agregar presupuesto')" @endif >
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" class="form-control valorreal" id="valorreal{{$i}}" name="ajuste[{{$i}}][real]" @if($concepto->real != "") value="{{'$ '.number_format($concepto->real, 0, ',', '.')}}" @else value="$ 0" @endif placeholder="Valor" @if($data['permiso_real'] =="No" || $data['permiso_agregar_editar_eliminar']=="Si") readonly style="cursor: no-drop" onclick="no_permiso('Usted no tiene permisos para agregar valor real')" @endif >
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php
                                if($concepto->ajuste<0){
                                    $estilo="color: red;";
                                    $ajuste=$concepto->ajuste*(-1);
                                    $ajuste=intval($ajuste);
                                    $ajuste = number_format($ajuste, 0, ',', '.');
                                    $ajuste ='- $ '.$ajuste;
                                }else{
                                    $estilo="";
                                    $ajuste = number_format($concepto->ajuste, 0, ',', '.');
                                    $ajuste ='$ '.$ajuste;
                                }
                              ?>
                            <input type="text" class="form-control" style="cursor: no-drop; {{$estilo}}" id="valorajuste{{$i}}" name="presupuesto[{{$i}}][valor]" value="{{$ajuste}}" placeholder="Valor" readonly>
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
                @endforeach
                <div class="row">
                    <div class="col-md-5 txt-total">
                        <h3><strong>Total</strong></h3>
                    </div>
                    <div class="col-md-2">
                        <h3><strong id="total">$ {{number_format($data['totales'][0]->Presupuesto, 0, ',', '.')}}</strong></h3>
                    </div>
                    <div class="col-md-2">
                        <h3><strong id="totalreal">$ {{number_format($data['totales'][0]->vReal, 0, ',', '.')}}</strong></h3>
                    </div>
                    <div class="col-md-2">
                        <?php
                        if($data['totales'][0]->Ajuste<0){
                            $estilo="color: red;";
                            $t_ajuste=$data['totales'][0]->Ajuste*(-1);
                            $t_ajuste=intval($t_ajuste);
                            $t_ajuste = number_format($t_ajuste, 0, ',', '.');
                            $t_ajuste ='- $ '.$t_ajuste;
                        }else{
                            $estilo="";
                            $t_ajuste = number_format($data['totales'][0]->Ajuste, 0, ',', '.');
                            $t_ajuste ='$ '.$t_ajuste;
                        }
                        ?>
                        <h3><strong style="{{$estilo}}" id="totalajuste">{{$t_ajuste}}</strong></h3>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="alert alert-success"><strong>¡Guardado!</strong><br> Se ha guardado correctamente.</div>
@endsection

@section('scripts')
<script>

function format(input){
    var num = input.replace(/\./g,'');
    hallado = num.indexOf("0");
    if(hallado == 0){
        num=num.slice(1);
    }
    if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        return '$ '+num;
    }else{
        return '$ '+0;
    }
}

$(document).on('keyup','.valorreal, .valor',function(){
    v=$(this).val();
    v=v.replace(/\./g,'');
    v=v.replace(/\$/g,'');
    v=parseInt(v);
    v=String(v);
    v=format(v);
    $(this).val(v);
    sumatotal();
    sumatotalpresupuesto();
    ajuste();
    guardar();
});

function sumatotal(){
    total=0;
    $(".valorreal").each(function(){
        valor=$(this).val();
        valor=valor.replace(/\./g,'');
        valor=valor.replace(/\$/g,'');
        total=total+parseInt(valor);
    });
    total=String(total);
    total=format(total);
    $('#totalreal').html(total);
}

function sumatotalpresupuesto(){
    total=0;
    $(".valor").each(function(){
        valor=$(this).val();
        valor=valor.replace(/\./g,'');
        valor=valor.replace(/\$/g,'');
        total=total+parseInt(valor);
    });
    total=String(total);
    total=format(total);
    $('#total').html(total);
}

    function ajuste(){
        suma_ajuste=0;
        $(".valor").each(function(){
            id_valor=$(this).attr('id');
            id=id_valor.replace('valor','');
            valor_presupuesto=$(this).val();
            valor_presupuesto=valor_presupuesto.replace('$ ','');
            valor_presupuesto=replaceAll(valor_presupuesto, ".", "" );
            valor_presupuesto=parseInt(valor_presupuesto);
            valor_real=$('#valorreal'+id).val();
            valor_real=valor_real.replace('$ ','');
            valor_real=replaceAll(valor_real, ".", "" );
            valor_real=parseInt(valor_real);
            valor_ajuste=valor_presupuesto-valor_real;
            suma_ajuste+=valor_ajuste;
            if(valor_ajuste<0){
                signo="-";
                valor_ajuste=valor_ajuste*(-1);
            }else{
                signo="+";
            }
            valor_ajuste=String(valor_ajuste);
            valor_ajuste=format(valor_ajuste);
            if(signo=="-"){
                $('#valorajuste'+id).val('- '+valor_ajuste);
                $('#valorajuste'+id).css('color','red');
            }else{
                $('#valorajuste'+id).val(valor_ajuste);
                $('#valorajuste'+id).css('color','#555');
            }

        });
        if(suma_ajuste<0){
            signo="-";
            suma_ajuste=suma_ajuste*(-1);
        }else{
            signo="+";
        }
        suma_ajuste=String(suma_ajuste);
        suma_ajuste=format(suma_ajuste);
        if(signo=="-"){
            $('#totalajuste').html('- '+suma_ajuste);
            $('#totalajuste').css('color','red');
        }else{
            $('#totalajuste').html(suma_ajuste);
            $('#totalajuste').css('color','#555');
        }
    }

    function replaceAll( text, busca, reemplaza ){
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca,reemplaza);
        return text;
    }

    function guardar(){
        var jqxhr = $.post("{{url('/')}}/guardarajusteconferencia", $("#formulario_basico").serialize())
        .done(function() {
            console.log("Guardado");
        })
        .fail(function(e) {
            console.error(e.msj)
        })
        .always(function(e) {
          console.log(e.msj)
        });
    }

    $('body').on('change','.valorreal',function(){
        $('.alert-success').css('display','block');
        setTimeout(function(){ $('.alert-success').css('display','none'); }, 1500);
    });

    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
