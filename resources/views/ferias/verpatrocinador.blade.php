@extends('template.app') @section('title', 'Detalles patrocinador') @section('content')
<link href="{{ url('/') }}/css/ferias/verpatrocinador.css" rel="stylesheet" type="text/css">
<meta name="csrf-token" content="{{ csrf_token() }}">


<div class="container">
    <input type="hidden" id="id_pat" value="{{$id_pat}}">
    <div class="row">
        <div class="card-container col-lg-10 col-sm-6 offset-lg-1">
            @foreach($datos_p as $dato)
            @php if(isset($dato->logo) && !empty($dato->logo)){
            $src = url('/').'/storage/ferias/patrocinadores/'.$dato->logo;
            }else{
            $src = 'https://www.shareicon.net/data/2016/09/01/822711_user_512x512.png';
            } @endphp
            <div class="card hovercard">
                <div class="row">
                    <div class="col-md-5">
                        <img alt="" src="{{ $src }}">
                        <h2 class="card-title text-uppercase">{{ $dato->nombre }}</h2>
                    </div>
                    <div class="col-md-7">
                        <h3>Información Básica</h3>
                        <div class="row mt-5">
                            <div class="col-md-4 col-md-offset-2 text-left">
                                <h3>NIT:</h3>
                                <h3>Tipo financiador:</h3>
                                <h3>Ubicación:</h3>
                            </div>
                            <div class="col-md-6 text-left">
                                <p class="h3 text-info">
                                    <?php echo $nit = ($dato->nit)? $dato->nit : "No especificado";   ?>
                                </p>
                                <p class="h3 text-info">{{ $dato->tipo_financiador }}</p>
                                <p class="h3 text-info">{{str_replace("%%", ", ", $dato->ubicacion)}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <ul class="nav nav-pills btn-group-justified" role="group" role="tablist">
                <li class="btn-group w-50" role="group">
                    <a id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab" role="tab">
                                   <i class="fa fa-user" aria-hidden="true"></i>
                                    <div class="hidden-xs">Detalles</div>
                                </a>
                </li>
                <li class="btn-group w-50" role="group">
                    <a id="favorites" class="btn btn-secondary" href="#tab2" data-toggle="tab" role="tab">
                           <i class="fa fa-comments" aria-hidden="true"></i>
                            <div class="hidden-xs">Comentarios</div>
                        </a>
                </li>
            </ul>
            <div class="well">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="stars">
                        <div class="row">
                            <div class=" col-md-12 col-lg-12">
                                @if($datos_r)
                                <table class="table table-hover table-responsive-lg">
                                    <thead>
                                        <tr>
                                            <h3>
                                                <?php echo $title = (count($datos_r) > 1)? "Responsables" : "Responsable" ?>
                                            </h3>
                                        </tr>
                                        <tr>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Cargo</th>
                                            <th scope="col">Teléfono</th>
                                            <th scope="col">Celular</th>
                                            <th scope="col">Correo</th>
                                            <th scope="col">Observaciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datos_r as $res)
                                        <tr>
                                            <th>{{ $res->nombre }}</th>
                                            <td>{{ $res->cargo }}</td>
                                            <td>{{ $res->telefono }}</td>
                                            <td>{{ $res->celular }}</td>
                                            <td>{{ $res->correo }}</td>
                                            <td class="text-justify">{{ $res->observaciones }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <p class="display-2 text-muted text-center">
                                    <i class="fa fa-info-circle text-info" aria-hidden="true"></i> No hay datos de responsables.
                                </p>
                                @endif
                            </div>
                            <div class="col-md-12">
                                @if($datos_sp)
                                <table class="table table-hover table-responsive-lg">
                                    <thead>
                                        <tr>
                                            <h3>
                                                <?php echo $title = (count($datos_sp) > 1)? "Solicitud" : "Solicitudes" ?>
                                            </h3>
                                        </tr>
                                        <tr>
                                            <th scope="col">Feria</th>
                                            <th scope="col">Fecha</th>
                                            <th scope="col">Responsable</th>
                                            <th scope="col">Valor Solicitado</th>
                                            <th scope="col">Valor Aprobado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datos_sp as $sp)
                                        <tr>
                                            <th>{{ $sp->f_nombre }}</th>
                                            <td>{{date("d/m/Y (h:i:s A)",strtotime($sp->created_at))}}</td>
                                            <td>{{ $sp->f_r_nombre }}</td>
                                            <td>{{ $sp->patrocinio_inicial }}</td>
                                            <td>
                                                <?php echo $final = (isset($sp->patrocinio_final))? $sp->patrocinio_final : 0;  ?>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <p class="display-2 text-muted text-center">
                                    <i class="fa fa-info-circle text-info" aria-hidden="true"></i> No hay datos de solicitudes.
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="tab2" role="tabpanel">
                        <h3>Comentarios</h3>
                        <div class="container">
                            <div class="row">
                                <div class="comments col-md-12 pt-5" id="caja_comentarios">
                                </div>
                                <div class="col-md-12 pt-4">
                                    <div class="comment-wrap">
                                        <div class="photo">
                                            <div class="avatar" style="background-image: url('<?php echo url('/').'/images/file/clientes/'.$img[0]->foto; ?>')"></div>
                                        </div>
                                        <div class="comment-block">
                                            <form autocomplete="off" id="messages_box">
                                               <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..."></textarea>
                                            </form>
                                            <div class="pull-right">
                                            <button type="button" class="btn btn-success" onclick="do_comments()" id="send_btn">Enviar <i class="fa fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('scripts')
<script src="{{url('/')}}/js/ferias/verpatrocinador.js"></script>
@endsection
