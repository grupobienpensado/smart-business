@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Dashboard Visitante')

@section('content')

<!-- Styles -->
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<style>
#chartdiv {
    background-color: black;
}

#chartdiv2 {
    background: white;
}

#chartdiv3 {
    background: #000833;
}

#chartdiv4 {
    background: #555;
}
path.amcharts-graph-column-front.amcharts-graph-column-element {
    fill: #5fc2ff;
}
.cajagraf {
    font-size: 11px;
    max-height: 400px;
    min-height: 400px;

}

.borde-separado{
    border-radius: 8px 8px 8px 8px;
    -moz-border-radius: 8px 8px 8px 8px;
    -webkit-border-radius: 8px 8px 8px 8px;
    border: 0px solid #000000;
    margin: 1%;
}

h4.card-header {
    background: rgba(240, 248, 255, 0);
}

    .visitantes h4{
        font-size: 25px;
        padding-top: 0px;
    }

    .visitantes{
        background-color: rgba(255, 152, 0, 0.87);
        color: aliceblue;
    }

    .paises{
        background-color: rgba(63, 81, 181, 0.92);
        color: aliceblue;
    }

    .paises h4{
        font-size: 25px;
        padding-top: 0px;
    }

    h1.cinta-title {
        width: 100%;
        color: aliceblue;
        background: #5fc2ff;
    }

/*path {
    fill: #387aba;
}*/

/*Botones de dashboard y registros*/
    /* footer social icons */
    #btn-nav ul.social-network {
        list-style: none;
        display: inline;
        margin-left:0 !important;
        padding: 0;
    }
    #btn-nav ul.social-network li {
        display: inline;
        margin: 0 5px;
    }


    /* footer social icons */

    #btn-nav .social-network a.icoRegistre:hover {
        background-color:#3B5998;
    }

    #btn-nav .social-network a.icoDashboard:hover {
        background-color:#007bb7;
    }
    #btn-nav .social-network a.icoRegistre:hover i, .social-network a.icoDashboard:hover i {
        color:#fff;
    }
    #btn-nav a.socialIcon:hover, .socialHoverClass {
        color:#44BCDD;
    }

    #btn-nav .social-circle li a {
        display:inline-block;
        position:relative;
        margin:0 auto 0 auto;
        -moz-border-radius:50%;
        -webkit-border-radius:50%;
        border-radius:50%;
        text-align:center;
        width: 50px;
        height: 50px;
        font-size:20px;
    }
    #btn-nav .social-circle li i {
        margin:0;
        line-height:50px;
        text-align: center;
    }

    #btn-nav .social-circle li a:hover i, .triggeredHover {
        -moz-transform: rotate(360deg);
        -webkit-transform: rotate(360deg);
        -ms--transform: rotate(360deg);
        transform: rotate(360deg);
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
        -o-transition: all 0.2s;
        -ms-transition: all 0.2s;
        transition: all 0.2s;
    }
    #btn-nav .social-circle i {
        color: #fff;
        -webkit-transition: all 0.8s;
        -moz-transition: all 0.8s;
        -o-transition: all 0.8s;
        -ms-transition: all 0.8s;
        transition: all 0.8s;
    }

    #btn-nav a {
     background-color: #D3D3D3;
    }

    .resultado{
        color: #051d60;
    }
    .bg-white{
        background-color: #fff;
    }
    .bordes-redondos{
        border-radius: 10px 10px 10px 10px;
        -moz-border-radius: 10px 10px 10px 10px;
        -webkit-border-radius: 10px 10px 10px 10px;
        border: 0px solid #000000;
    }
    .bordes-redondo-superior{
        border-radius: 20px 20px 0px 0px;
        -moz-border-radius: 20px 20px 0px 0px;
        -webkit-border-radius: 20px 20px 0px 0px;
        border: 0px solid #000000;
    }
    .bg-identificacion{
        background-color: #01BBCF;
    }
    .bg-identificacion2{
        background-color: #5684D1;
    }
    .border-u{
        border-left: 1px solid #bdbdbd;
        border-bottom: 1px solid #bdbdbd;
        border-right: 1px solid #bdbdbd;
    }
    .label-opotunidades{
        color: #0e46e2;
        border: 1px solid #bdbdbd;
        position: absolute;
        bottom: -12px;
        left: 21px;
        background-color: #fff;
        border-radius: 20px 20px 20px 20px;
        -moz-border-radius: 20px 20px 20px 20px;
        -webkit-border-radius: 20px 20px 20px 20px;
    }
    /*Alto variable*/
    .hv{
        width: 16%;
    }
    @media (min-width: 1366px) and (max-width: 1599px) {
        .label-opotunidades{
            left: 13px;
            bottom: -11px;
        }
        .label-opotunidades a{
            font-size: 14px !important;
        }
        /*Alto variable*/
        .hv{
            width: 25%;
        }
    }
    @media (min-width: 1600px) and (max-width: 1919px) {
        .label-opotunidades{
            left: 16px;
        }
        /*Alto variable*/
        .hv{
            width: 19%;
        }
    }

    .hm{
        min-height: 100px;
    }
    .hm h3{
        font-size: -webkit-xxx-large;
        margin-top: 33px;
        color: #01bbcf;
    }

    .hm2{
        min-height: 100px;
    }
    .hm2 h3{
        font-size: -webkit-xxx-large;
        margin-top: 33px;
        color: #5684D1;
    }

/*Fin Botones dashboard y registros*/

/*Css Presupuesto de feria*/
    .charts {
      width: 100%;
      height: 25rem !important;
    }

    .budget-header{
        width: 100%;
        height: 100%;
        border-radius: 5px;
    }
    .budget-header h3 {
        margin: 0;
    }
    .budget-wrap :nth-child(1) .budget-item .budget-header{

        background-color: rgba(100, 56, 171, 0.6);
    }
    .budget-wrap :nth-child(2) .budget-item .budget-header{
        background-color: rgba(62, 134, 176, 0.6);
    }

    .budget-wrap :nth-child(3) .budget-item .budget-header{
        background-color: rgba(213, 116, 59, 0.6);
    }
    .budget-item{
        border-radius: 5px;
        background-repeat:no-repeat;
        background-size:100%;
        background-position:center;
    }
    .budget-wrap :nth-child(1) .budget-item{
        background-image: url("{{url('/')}}/images/bg_real.png");
        background-size: cover;
       -moz-background-size: cover;
       -webkit-background-size: cover;
       -o-background-size: cover;
       -ms-background-size: cover;
    }
    .budget-wrap :nth-child(2) .budget-item{
        background-image: url("{{url('/')}}/images/bg_procumpli.png");
        background-size: cover;
       -moz-background-size: cover;
       -webkit-background-size: cover;
       -o-background-size: cover;
       -ms-background-size: cover;
    }
    .budget-wrap :nth-child(3) .budget-item{
        background-image: url("{{url('/')}}/images/bg_patrocinio.png");
        background-size: cover;
       -moz-background-size: cover;
       -webkit-background-size: cover;
       -o-background-size: cover;
       -ms-background-size: cover;
    }
    .pallets-img{
        width: 100%;
    }
    .gradient-aguamarina{
        background-image: -moz-linear-gradient( 153deg, rgb(6,106,124) 0%, rgb(14,171,162) 100%);
        background-image: -webkit-linear-gradient( 153deg, rgb(6,106,124) 0%, rgb(14,171,162) 100%);
        background-image: -ms-linear-gradient( 153deg, rgb(6,106,124) 0%, rgb(14,171,162) 100%);
        border-radius: 10px;
    }

    .gradient-azul{
        background-image: -moz-linear-gradient( 153deg, rgb(87,173,201) 0%, rgb(6,68,138) 100%);
        background-image: -webkit-linear-gradient( 153deg, rgb(87,173,201) 0%, rgb(6,68,138) 100%);
        background-image: -ms-linear-gradient( 153deg, rgb(87,173,201) 0%, rgb(6,68,138) 100%);
        border-radius: 10px;
    }
    @media (min-width: 1366px) and (max-width: 1599px) {
        .real{
            font-size: 32px;
        }
    }
/*FIN CSS presupuesto de feria*/
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/black.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/themes/patterns.js"></script>

<script src='{{ url("/") }}/components/newfullcalendar/lib/moment.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/locale-all.js" integrity="sha256-6cQXtrul38ChfKH+ojSity2Aifa8dSvrdRiYUiQrd2M=" crossorigin="anonymous"></script>

<div class="widget">
<?php echo $menu; ?>
</div>

<div class="widget-content">
        <div class="row">
            <div class="col-md-12 text-center mb-3" id="btn-nav">
                <ul class="social-network social-circle">

                    <li><a href="{{url('/')}}/feria/bitacora/{{$id}}" class="icoRegistre" title="Registros"><i class="fa fa-list-alt"></i></a></li>

                    <li><a href="#" class="icoDashboard" title="Dashboard"><i class="fa fa-dashboard"></i></a></li>
                </ul>
            </div>
        </div>
</div>
<div class="row" style="padding-top: 25px;">
    <div class="col-md-12">
        <h1 class="cinta-title">Dashboard Bitacora</h1>
    </div>
    <div class="col-md-4">
        <div class="card borde-separado visitantes">
          <h4 class="card-header total-visitantes">0 Visitantes</h4>
          <div class="card-body">
            <div id="chartdiv" class="cajagraf"></div>
          </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card borde-separado">
          <h4 class="card-header" style="color: #051d60;font-size: 22px;">Horarios de visita</h4>
          <div class="card-body">
            <div id="chartdiv2" class="cajagraf"></div>
          </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card borde-separado paises">
          <h4 class="card-header total-paises">0 Paises</h4>
          <div class="card-body">
            <div id="chartdiv3" class="cajagraf"></div>
          </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card borde-separado">
          <h4 class="card-header" style="color: #051d60;font-size: 22px;">Equipos de interes</h4>
          <div class="card-body">
            <div id="chartdiv4" class="cajagraf"></div>
          </div>
        </div>
    </div>
</div>


<div class="row bg-white">
    <div class="col-md-12 text-center bordes-redondos">
        <h2 class="resultado">Resultados</h2>
    </div>
    <div class="col-md-6">
        <div class="row ml-5 mr-5">
            <div class="col-md-12 bordes-redondo-superior text-center text-white bg-identificacion">
                <h3>Identificación de Oportunidades</h3>
            </div>
        </div>
        <div class="row ml-5 mr-5">
            <div class="col-md-4">
                <div class="row border-u mr-1">
                    <div class="col-md-12">
                        <img class="mt-5 mb-5" src="{{url('/')}}/images/iconosferias/visitante/oportunidades_icon.svg">
                        <div class="row">
                            <div class="col-md-10 label-opotunidades">
                                <a class="h4">Oportunidades</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-u mr-1">
                    <div class="col-md-12 hm">
                        <h3>{{$data['oportunidades']}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row border-u">
                    <div class="col-md-12">
                        <img class="mt-5 mb-5" src="{{url('/')}}/images/iconosferias/visitante/paises_icon.svg">
                        <div class="row">
                            <div class="col-md-10 label-opotunidades">
                                <a class="h4">Países</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-u">
                    <div class="col-md-12 hm">
                        <h3>{{$data['paises']}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row border-u ml-1">
                    <div class="col-md-12">
                        <img class="mt-5 mb-5" src="{{url('/')}}/images/iconosferias/visitante/millons_icon.svg">
                        <div class="row">
                            <div class="col-md-10 label-opotunidades">
                                <a class="h4">Millones</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-u ml-1">
                    <div class="col-md-12 hm">
                        <h3>{{$data['millones']}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ml-5 mr-5">
            <div class="col-md-12 bordes-redondo-superior text-center text-white bg-identificacion2">
                <h3>Identificación de Clientes</h3>
            </div>
        </div>
        <div class="row ml-5 mr-5">
            <div class="col-md-4">
                <div class="row border-u mr-1">
                    <div class="col-md-12">
                        <img class="mt-5 mb-5 hv" src="{{url('/')}}/images/iconosferias/visitante/empresa_icon.svg">
                        <div class="row">
                            <div class="col-md-10 label-opotunidades">
                                <a class="h4">Empresas</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-u mr-1">
                    <div class="col-md-12 hm2">
                        <h3>{{$data['empresas']}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row border-u">
                    <div class="col-md-12">
                        <img class="mt-5 mb-5" src="{{url('/')}}/images/iconosferias/visitante/clientes_icon.svg">
                        <div class="row">
                            <div class="col-md-10 label-opotunidades">
                                <a class="h4">Clientes</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-u">
                    <div class="col-md-12 hm2">
                        <h3>{{$data['clientes']}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row border-u ml-1">
                    <div class="col-md-12">
                        <img class="mt-5 mb-5" src="{{url('/')}}/images/iconosferias/visitante/paises_icon.svg">
                        <div class="row">
                            <div class="col-md-10 label-opotunidades">
                                <a class="h4">Paises</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-u ml-1">
                    <div class="col-md-12 hm2">
                        <h3>{{$data['paises_clientes']}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center bordes-redondos">
        <h2 class="resultado">Presupuesto de Feria</h2>
    </div>
    <div class="col-md-12">
        <div class="row budget-wrap mt-5 ml-5 mr-5">
            <div class="col-md-2 col-sm-12">
                <div class="container budget-item">
                <div class="row">
                    <div class="col-sm-12 budget-header p-2"><h3 class="text-white">Real</h3></div>
                    <div class="col-sm-12 p-3"><p class="text-white h1 real"><?php echo number_format(($data['real'] / 1000000), 1, ',', '.'); ?> <small class="text-white">Millones</small></p></div>
                </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
              <div class="container budget-item">
                  <div class="row">
                      <div class="col-sm-12 budget-header p-2">
                          <div class="row">
                              <div class="col-sm-6">
                                  <h3 class="text-white">Proyección</h3>
                              </div>
                              <div class="col-sm-6">
                                  <h3 class="text-white">Cumplimiento</h3>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                         <div class="row">
                              <div class="col-sm-6 p-3">
                                  <p class="text-white h1"><?php echo number_format(($data['proyeccion'] / 1000000), 1, ',', '.'); ?> <small class="text-white">Millones</small></p>
                              </div>
                              <div class="col-sm-6 p-3">
                                  <p class="text-white h1"><?php echo number_format($data['cumplimiento'], 1, ',', '.'); ?>%</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-5 col-sm-12">
              <div class="container budget-item">
              <div class="row">
                  <div class="col-sm-12 p-0 budget-header p-2">
                      <div class="row">
                          <div class="col-sm-6 p-0">
                              <h3 class="text-white">Patrocinio</h3>
                          </div>
                          <div class="col-sm-6 p-0">
                              <h3 class="text-white">Patrocinado</h3>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-12 p-0">
                     <div class="row">
                          <div class="col-sm-6 p-3">
                              <p class="text-white h1"><?php echo number_format(($data['patrocinio'] / 1000000), 1, ',', '.'); ?> <small class="text-white">Millones</small></p>
                          </div>
                          <div class="col-sm-6 p-3">
                              <p class="text-white h1"><?php echo number_format($data['patrocinado'], 1, ',', '.'); ?>%</p>
                          </div>
                      </div>
                  </div>
              </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
$(function(){
    moment.locale('es');
    $.get( "/json/visitantes/{{ $id }}", function( data ) {
      console.log( data );
        if(data.success){
         $(".total-visitantes").html(data.total+' Visitantes');
            var objs = [];
            $.each(data.list, function( index, value ) {
              var obj = {};
                obj.valor = value.num;
                obj.fecha = moment(value.fecha).format('D MMMM');
                objs.push(obj);
            });
            dibujarVisitas(objs);

        }
    });

    $.get( "/json/horarios/{{ $id }}", function( data ) {
      console.log( data );
        if(data.success){
            var objs = [];
            $.each(data.list, function( index, value ) {
                console.log(moment(value.hora).format('LT'));
                var hora = moment(value.hora).format('LT');
                var hora = moment("12-25-1995 "+value.hora, "MM-DD-YYYY HH:mm:ss").format('LT');
                console.log(hora);
                var obj = {};
                obj.valor = value.num;
                obj.hora = hora;
                objs.push(obj);
            });
            dibujarHoras(objs);

        }
    });

    $.get( "/json/pais/{{ $id }}", function( data ) {
  console.log( data );
    if(data.success){
     $(".total-paises").html(data.total+' Paises');
        var objs = [];
        $.each(data.list, function( index, value ) {
          var obj = {};
            obj.valor = value.num;
            obj.pais  = value.pais;
            obj.color = "#0d47a1";
            if(value.imagen){
                obj.imagen  = '/images/icons/'+value.imagen;
            }else{
                obj.imagen  = 'http://via.placeholder.com/150x150?text=Pais';
            }
            objs.push(obj);
        });
        dibujarPaises(objs);

    }
});

    $.get( "/json/productos/{{ $id }}", function( data ) {
        console.log( data );
        if(data.success){
            var objs = [];
            $.each(data.list, function( index, value ) {
                var obj = {};
                obj.valor = value.num;
                obj.producto = value.producto+' '+value.referencia;
                objs.push(obj);
            });
            dibujarProducto(objs);

        }
    });
});



dibujarVisitas = function(objs) {
    var chart = AmCharts.makeChart("chartdiv", {
                "theme": "black",
                "color": "#5fc2ff",
                "axisColor": "#5fc2ff",
                "type": "serial",
                "dataProvider": objs,
                "valueAxes": [{
                    "unit": "",
                    "position": "left",
                    "title": "",
                    "axisColor": "#5fc2ff",
                }],
                "startDuration": 1,
                "graphs": [{
                "balloonText": "[[value]] Visita/s",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "2005",
                "type": "column",
                "clustered":false,
                "columnWidth":0.5,
                "valueField": "valor"
            }],
            "plotAreaFillAlphas": 0.1,
            "categoryField": "fecha",
            "categoryAxis": {
                "gridPosition": "start"
            },
                "export": {
                    "enabled": true
                 }

            });
}

dibujarHoras = function(objs) {
    AmCharts.makeChart("chartdiv2",{
					"type": "serial",
					"categoryField": "hora",
					"autoMarginOffset": 40,
					"marginRight": 60,
					"marginTop": 60,
					"plotAreaBorderColor": "#FFFFFF",
					"plotAreaFillColors": "#0000FF",
					"startDuration": 1,
					"borderColor": "#009EB5",
					"color": "#0000FF",
					"fontFamily": "Roboto",
					"fontSize": 13,
					"theme": "patterns",
					"categoryAxis": {
						"gridPosition": "start",
						"axisColor": "#0000FF",
						"color": "#0000FF",
						"fillColor": "#0000FF",
						"gridColor": "#009EB5"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonColor": "#00CDFF",
							"balloonText": "[[title]] of [[hora]]:[[value]]",
							"bullet": "round",
							"bulletColor": "#0000FF",
							"bulletSize": 10,
							"fillColors": "#00CDFF",
							"id": "AmGraph-1",
							"lineAlpha": 1,
							"lineThickness": 3,
							"title": "",
							"type": "smoothedLine",
							"valueField": "valor"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisColor": "#0000FF",
							"color": "#0000FF",
							"fillColor": "#00CDFF",
							"gridColor": "#00CDFF",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {
						"borderColor": "#0000FF",
						"color": "#0000FF",
						"pointerOrientation": "up"
					},
					"titles": [],
					"dataProvider": objs
				});
}

dibujarPaises = function(objs) {
    var chart = AmCharts.makeChart("chartdiv3",{
    "type": "serial",
    "theme": "light",
    "dataProvider": objs,
    "startDuration": 1,
    "graphs": [{
        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
        "bulletOffset": 10,
        "bulletSize": 52,
        "colorField": "color",
        "cornerRadiusTop": 8,
        "customBulletField": "imagen",
        "fillAlphas": 0.8,
        "lineAlpha": 0,
        "type": "column",
        "valueField": "valor"
    }],
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "categoryField": "pais",
    "categoryAxis": {
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "export": {
        "enabled": true
     }
});

}

dibujarProducto = function(objs) {
    AmCharts.makeChart("chartdiv4",{
        "type": "serial",
        "categoryField": "producto",
        "rotate": true,
        "startDuration": 1,
        "backgroundColor": "#3B3B3B",
        "color": "#005710",
        "categoryAxis": {
            "gridPosition": "start",
            "axisColor": "#FFFFFF",
            "color": "#FFFFFF",
            "gridColor": "#005710"
        },
        "chartCursor": {
            "enabled": true,
            "color": "#000000",
            "cursorColor": "#ADDE64"
        },
        "chartScrollbar": {
            "enabled": true,
            "color": "#FF8000",
            "graphFillColor": "#FF8000",
            "graphLineColor": "#FF8000"
        },
        "trendLines": [],
        "graphs": [
            {
                "balloonColor": "undefined",
                "bulletColor": "#00CDFF",
                "color": "#FFFFFF",
                "fillAlphas": 1,
                "fillColors": "#ADDE64",
                "fixedColumnWidth": 2,
                "fontSize": 1,
                "id": "AmGraph-1",
                "lineColor": "#FFFFFF",
                "lineThickness": 2,
                "title": "graph 1",
                "type": "step",
                "valueField": "valor"
            }
        ],
        "guides": [],
        "valueAxes": [
            {
                "gridType": "circles",
                "id": "ValueAxis-1",
                "axisColor": "#555555",
                "color": "#FFFFFF",
                "gridAlpha": 0,
                "gridColor": "#FFFFFF",
                "title": ""
            }
        ],
        "allLabels": [],
        "balloon": {},
        "titles": [
            {
                "id": "Title-1",
                "size": 15,
                "text": ""
            }
        ],
        "dataProvider": objs
    });
}
</script>



        <script type="text/javascript">

</script>


@endsection
<!-- HTML -->
