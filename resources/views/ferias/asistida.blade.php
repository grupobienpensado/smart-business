@extends('template.app')
@section('title', 'Ferias asistidas')

@section('content')
<?php print $estilo ?>
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/asistida.css">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<input type="hidden" id="URL" value="{{url('/')}}">
<div class="row blocos">
<div class="col-md-12">
    <input id="filtro" type="text" class="form-control" placeholder="Busca Rápida">
</div>
 <?php $cont=-1; ?>
 @foreach($datos as $dato)
  @php
    $cont++;
    if(isset($dato->logo) && !empty($dato->logo)){
      $foto = url('/').'/storage/ferias/'.$dato->logo;
    }else {
      $foto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100';
    }
  @endphp
  <div class="col-md-6 bloco ver-asis" id="ver-asis{{$dato->id}}" style="cursor: pointer">
    <div class="promo-box promo_03" style="background: url(<?php echo $foto; ?>) left top no-repeat #fff;background-size: 200px 200px;">
      <div class="promo-content pull-right">

        <nav class="menuR{{$cont}}">
          <input type="checkbox" href="#" class="menu-openR{{$cont}}" name="menu-openR{{$cont}}" id="menu-openR{{$cont}}"/>
          <label class="menu-open-buttonR{{$cont}}" for="menu-openR{{$cont}}">
            <span class="hamburgerR{{$cont}} hamburger-1"></span>
            <span class="hamburgerR{{$cont}} hamburger-2"></span>
            <span class="hamburgerR{{$cont}} hamburger-3"></span>
          </label>

          <a href="#" class="menu-itemR{{$cont}} subir-ubicacion1" id="ubi_{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Cargar Ubicación"> <i class="fa fa-cloud-upload"></i> </a>
          <a href="{{url('/')}}/cargargaleria/{{$dato->id}}" class="menu-itemR{{$cont}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Galeria"> <i class="fa fa-cloud-upload"></i> </a>
          <a href="{{ url('/') }}/presupuesto/{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Crear Presupuesto" class="menu-itemR{{$cont}}"> <i class="fa fa-plus-circle"></i> </a>
          <a href="{{ url('/') }}/feria/patrocinador/{{$dato->id}}&{{$dato->nombre}}" data-toggle="tooltip" data-placement="top" title="Solicitudes Patrocinio" class="menu-itemR{{$cont}}"> <i class="fa fa-plus-square"></i> </a>
          <a href="{{ url('/') }}/feria/ver/{{$dato->id}}" data-toggle="tooltip" title="Ver feria" class="menu-itemR{{$cont}}"> <i class="fa fa-eye"></i> </a>
          <a href="{{ url('/') }}/editarexponente/{{$dato->id}}" data-toggle="tooltip" title="Editar feria" class="menu-itemR{{$cont}}"> <i class="fa fa-pencil"></i> </a>
        </nav>
          <!--
          @if($dato->ubicacion_foto != '')
          <a class="adjunto" data-toggle="tooltip" data-placement="top" title="Ya existe un archivo adjunto"><i class="fa fa-paperclip" aria-hidden="true" ></i></a>
          @endif
          <i class="fa fa-cloud-upload subir-ubicacion" id="ubi_{{$dato->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Ubicación"></i>
          <a href="{{url('/')}}/cargargaleria/{{$dato->id}}"><i class="fa fa-cloud-upload subir-asistidas" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cargar Galeria"></i></a>
          <a href="{{ url('/') }}/presupuesto/{{$dato->id}}" data-toggle="tooltip" data-placement="top" title="Crear Presupuesto"><i class="fa fa-plus-circle crear-presupuesto" aria-hidden="true"></i></a>
          <a href="{{ url('/') }}/feria/patrocinador/{{$dato->id}}&{{$dato->nombre}}" data-toggle="tooltip" title="Añadir Patrocinador">
              <i class="fa fa-plus-square new-sponsor" aria-hidden="true"></i>
          </a>
          <a href="{{ url('/') }}/feria/ver/{{$dato->id}}" data-toggle="tooltip" title="Ver feria">
              <i class="fa fa-eye btnVerFeria"></i>
          </a>-->
        @if($dato->tipo == "Exponente")
        <div class="label label-success">{{ $dato->tipo }}</div>
        @else
        <div class="label label-info">{{ $dato->tipo }}</div>
        @endif
        <h4>{{ $dato->nombre }} <span class="subUbicacion">({{ $dato->ubicacion }})</span></h4>
        <p>@php echo(substr($dato->descripcion, 0, 200)); @endphp...</p>
        <span class="date-init">{{ strftime("%d/%m/%Y", strtotime($dato->fecha_inicio)) }} - {{ strftime("%d/%m/%Y", strtotime($dato->fecha_fin)) }}</span>
      </div>
    </div>
  </div>
  @endforeach
</div>
<div class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cargar Ubicación</h5>
        <button type="button" class="close cerrar-modal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-cloud-upload"></i> Archivo Ubicación</h3></div>
            <div class="widget-content">
                <form class="dropzone" action="{{url('/')}}/cargarubicacion"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id_feria" value="">
                </form>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary cerrar-modal" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{url('/')}}/assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/asistida.js"></script>

@endsection
