@extends('template.app')
@section('title', 'Menu Feria')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/menu.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/vergaleria.css">
<link href="/css/html5imageupload.css?v1.3" rel="stylesheet">
<input type="hidden" value="{{url('/')}}" id="URL">
<div class="container">
    {{ csrf_field() }}
    @if(isset($feria))
    <div class="row p-5">
        <div class="col-md-12 bordes-redondos">
            <div class="row">
                <div class="col-md-12 text-center titulo">
                    <h3 class="mt-3"><strong>Feria Actual</strong></h3>
                </div>
                <button type="button" class="btn btn-primary botn-ingresar" id="{{$feria->id}}">Ingresar</button>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3 class="titulo"><strong>{{$feria->nombre}}</strong> <small> {{$fecha_ubicacion}}</small></h3>

                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-2">
                    <img src="{{url('/')}}/storage/ferias/{{$feria->logo}}" class="img-circle img-feia" alt="Avatar">
                </div>
                <div class="col-md-10 text-justify mt-5 color-parrafo">
                    <p>{{$feria->descripcion}}</p>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row mt-3 p-5">
        <div class="col-md-4 bordes-redondos crear text-center">
            <a class="text-white"><i class="fa fa-plus-circle fa-4 mt-5 mb-1" aria-hidden="true"></i></a><br>
            <a class="titulo-botones mb-4"><h3 class="mb-5">Crear nueva feria</h3></a>
        </div>
        <div class="col-md-4">
            <div class="row alto-min ml-4">

                <div class="col-lg-5 calendario bordes-redondos text-center">
                   <img src="{{url('/')}}/images/iconosferias/menu/calendar_icono.png">
                   <h3 class="text-white pie">Ver</h3>
                </div>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha text-center">
                    <p class="mt-5 h3">Calendario</p>
                    <div class="color-calendario calendario-pie">
                    @if(isset($proxima['nombre_lugar']))
                    <a><h6>{{$proxima['nombre_lugar']}}</h6></a>
                    <a><h6>{{$proxima['fecha']}}</h6></a>
                    @else
                    <a><h6>{{$proxima['no_hay']}}</h6></a><br>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="row alto-min ml-4">
                <a href="#ancla-final" class="col-lg-5 feriasfuturas bordes-redondos">
                   <img src="{{url('/')}}/images/iconosferias/menu/futuras_icono.png">
                   <h3 class="text-white pie">Ver</h3>
                </a>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Ferias futuras</h3></a>
                    <a><h2 class="for-numero">{{$data['ferias_futuras']}}</h2></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5 text-center">
            <div class="row alto-min">
                <a href="#ancla-final" class="col-lg-5 exponente bordes-redondos">
                   <img src="{{url('/')}}/images/iconosferias/menu/exponente.png">
                   <h3 class="text-white pie">Ver</h3>
                </a>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Asistente como <br> Exponente</h3></a>
                    <a><h2 class="for-numero">{{$data['ferias_exponente']}}</h2></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5 text-center">
            <div class="row alto-min ml-4">

                <div class="col-lg-5 visitante bordes-redondos">
                   <img src="{{url('/')}}/images/iconosferias/menu/visitante.png">
                   <h3 class="text-white pie">Ver</h3>
                </div>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Asistente como <br> Visitante</h3></a>
                    <a><h2 class="for-numero">{{$data['ferias_visitante']}}</h2></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5 text-center">
            <div class="row alto-min ml-4">

                <div class="col-lg-5 no-asistidas bordes-redondos">
                   <img src="{{url('/')}}/images/iconosferias/menu/no_asistidas.png">
                   <h3 class="text-white pie">Ver</h3>
                </div>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Ferias No Asistidas</h3></a>
                    <a><h2 class="for-numero">{{$data['ferias_noasistida']}}</h2></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5 text-center">
            <div class="row">
                <div class="col-lg-5 patrocinadores bordes-redondos">
                   <h3 class="text-white letra-btn-abajo">{{$data['ferias_patrocinadores']}}</h3>
                </div>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Patrocinadores</h3></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5 text-center">
            <div class="row ml-4">

                <div class="col-lg-5 proveedores bordes-redondos">
                   <h3 class="text-white letra-btn-abajo">{{$data['ferias_proveedores']}}</h3>
                </div>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Proveedores</h3></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5 text-center">
            <div class="row ml-4">

                <div class="col-lg-5 competencia bordes-redondos">
                   <h3 class="text-white letra-btn-abajo">0</h3>
                </div>
                <div class="col-lg-7 bg-white titulo bordes-redondos-derecha">
                    <a><h3 class="mt-5">Competencia</h3></a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <hr></hr>
        </div>
        <div class="col-md-12 area-listados text-center">

        </div>
        <a id="ancla-final"></a>
    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="documento" style="z-index: 3040" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 100%">
    <div class="modal-content" style="margin-top:3%; height: 700px; width: 90%;">
      <iframe id="ver_pdf" src="http://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>
<!--MODAL PARA CARGAR LA UBICACIÓN -->
<div class="modal fade" id="cargar_ubicacion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cargar Ubicación</h5>
        <button type="button" class="close cerrar-modal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-cloud-upload"></i> Archivo Ubicación</h3></div>
            <div class="widget-content">
                <form class="dropzone" action="/cargarubicacion"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id_feria" value="">
                </form>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary cerrar-modal" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
    var permiso_crear = "{{$data['permiso_crear']}}";
</script>
<script src="/js/parsley.min.js"></script>
<script type="text/javascript" src="/js/html5imageupload.js?v1.4.3"></script>
<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="/assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="/js/ferias/menu.js"></script>
@endsection
