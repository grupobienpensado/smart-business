@extends('template.app') @section('title', 'Crear patrocinador') @section('content')
<link href="{{ url('/') }}/css/ferias/crearpatrocinador.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<input type="hidden" id="URL" value="{{url('/')}}">
<div class="main-header">
    <h2>Patrocinador</h2>
    <em>Crear Patrocinador</em>
</div>
<div class="widget">
    <div class="widget-header">
        <h3><i class="fa fa-edit"></i> Crear Patrocinador</h3>
    </div>
    <div class="widget-content">
        <form id="formulario_patrocinador" method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="dropzone logo" data-ajax="false" data-originalsave="true">
                        <input type="file" name="logo" accept="image/gif, image/jpeg, image/png" required="required">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 izquierda">
                    <label>Nombre </label>
                    <input type="text" name="formulario[nombre]" class="form-control">
                </div>
                <div class="col-md-3 izquierda">
                    <label>Nit </label>
                    <input type="text" name="formulario[nit]" class="form-control">
                </div>
                <div class="col-md-3 izquierda">
                    <label>Tipo Financiador </label>
                    <select class="form-control" style="height: auto;" name="formulario[tipo_financiador]" required>
                    <option value="" seleted>Seleccione un tipo</option>
                    <option value="Administrable">Administrable</option>
                    <option value="Proveedor Grande">Proveedor Grande</option>
                    <option value="Proveedor Pequeno">Proveedor Pequeno</option>
                    <option value="Aliado">Aliado</option>
                    <option value="Materia Prima">Materia Prima</option>
                    <option value="Plastico">Plastico</option>
                </select>
                </div>
                <div class="col-md-3">
                   <label>Añadir una descripción</label>
                   <textarea class="form-control" name="formulario[descripcion]"></textarea>
                </div>
                <div class="col-md-4 izquierda mt-5">
                    <label for="pais">Pais</label>
                    <div class="row fila-pais">
                        <div class="col-md-10">
                            <input type="text" id="pais_mostrar" name="pais" class="form-control pais_mostrar" value="" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil lapiz cambiar-pais" aria-hidden="true" title="Cambiar de pais"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="pais" list="lista_paises" placeholder="Seleccione un pais" required="required">
                    <datalist id="lista_paises">
                  @foreach($paises as $pais)
                  <option value="{{$pais->id}}" label="{{$pais->name}}">
                  @endforeach
                </datalist>
                </div>
                <div class="col-md-4 izquierda mt-5">
                    <label for="departamento">Departamento <em>(Estado)</em></label>
                    <div class="row fila-departamento">
                        <div class="col-md-10">
                            <input type="text" id="departamento_mostrar" name="departamento" class="form-control departamento_mostrar" value="" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil lapiz cambiar-departamento" aria-hidden="true" title="Cambiar de departamento"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control no-cursor" id="departamento" list="lista_departamentos" placeholder="Seleccione un pais" required="required" readonly>
                    <datalist id="lista_departamentos">

                </datalist>
                </div>
                <div class="col-md-4 izquierda mt-5">
                    <label for="departamento">Ciudad <em>(Municipio)</em></label>
                    <div class="row fila-ciudad">
                        <div class="col-md-10">
                            <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" name="ciudad" value="" readonly>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-pencil lapiz cambiar-ciudad" aria-hidden="true" title="Cambiar de ciudad"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control no-cursor" id="ciudad" list="lista_ciudades" placeholder="Seleccione un pais" required="required" readonly>
                    <datalist id="lista_ciudades">

                </datalist>
                </div>
                <div class="col-md-12 text-center mt-5 pr-0 pl-0" id="res_container">
                    <h3>Responsable</h3>
                    <div class="actions text-center">
                          <a class="clone btn btn-success"><i class="fa fa-plus-square text-white"></i></a>
                          <a class="remove btn btn-danger"><i class="fa fa-minus-square text-white" aria-hidden="true"></i></a>
                    </div>
                    <div id="res_clone" class="clonedInput">
                        <div class="col-md-4 mt-sm-5">
                            <input type="text" class="form-control" name="responsable[nombre][]" value="" placeholder="Nombre del responsable" required>
                        </div>
                        <div class="col-md-4 mt-sm-5">
                            <input type="email" class="form-control" name="responsable[correo][]" placeholder="Correo del responsable" required>
                        </div>
                        <div class="col-md-4 mt-sm-5">
                            <input class="input form-control telefonofijo" name="responsable[telefono][]" type="text" placeholder="Teléfono">
                        </div>
                        <div class="col-md-4 mt-5">
                            <input class="input form-control telefonocelular" name="responsable[celular][]" type="text" placeholder="Celular">
                        </div>
                        <div class="col-md-4 mt-5">
                            <input class="input form-control" name="responsable[cargo][]" type="text" placeholder="Cargo">
                        </div>
                        <div class="col-md-4 mt-5">
                            <textarea placeholder="Observaciones..." name="responsable[observaciones][]" class="form-control"></textarea>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 mt-5">
                    <a href="#" class="btn btn-success btn-sm btn_guardar" data-toggle="tooltip" data-placement="top" title="Guardar">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                </a>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
    .new-attendant{
        margin-top: 11.5rem;
    }
</style>
@endsection
@section('scripts')
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/crearpatrocinadores.js"></script>
@endsection
