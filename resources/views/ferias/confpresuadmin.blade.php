@extends('template.app')
@section('title', 'Administrar Presupuesto Conferencia')

@section('content')

<link href="{{ url('/') }}/css/ferias/presupuesto.css" rel="stylesheet">
<input type="hidden" value="{{url('/')}}" id="URL">
<div class="content" style="background-color: #fff;">
    <?=$data['menu']?>
    <div class="main-header mt-5">
        <h2>Presupuesto</h2>
        <em>Items presupuesto conferencia administrable</em>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="top-content">
                <ul class="list-inline quick-access">
                    <li>
                        <a href="#">
                            <div class="quick-access-item bg-color-blue">
                                <i class="fa fa-envelope"></i>
                                <h5>NOTA</h5><em>Los datos se almacenan automaticamente</em>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Crear Items de presupuesto</h3>
        </div>
        <div class="widget-content">
            <a <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>class="crearitem"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para crear un item de presupuesto')"<?php } ?> data-toggle="tooltip" data-placement="top" title="Crear Item"><i class="fa fa-plus-circle agregar" aria-hidden="true"></i></a>
            <form class="form-horizontal" role="form" id="formulario_basico" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                @foreach($data['datos'] as $dato)
                <input type="hidden" name="presupuesto[{{$data['i']}}][id]" value="{{$dato->id}}">
                <div class="row">
                    <div class="mt-3" style="width: 2%;">
                        {{$data['i']+1}}.
                    </div>
                    <div class="col-sm-11">
                        <div class="form-group">
                            <label for="concepto{{$data['i']}}" class="control-label sr-only">{{$dato->concepto}}</label>
                            <input type="text" class="form-control detalle" name="presupuesto[{{$data['i']}}][concepto]" id="concepto{{$data['i']}}" value="{{$dato->concepto}}" placeholder="Concepto">
                        </div>
                    </div>
                    <i class="fa fa-trash text-danger <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>eliminar<?php } ?>" <?php if($data['permiso_agregar_editar_eliminar']=="No"){ ?>onclick="no_permiso('Usted no tiene permisos para eliminar un item de presupuesto')"<?php } ?> id="eliminar_{{$dato->id}}" aria-hidden="true"></i>
                </div>
                @php $data['i']++; @endphp
                @endforeach
                @if(count($data['datos']) == 0)
                <h3>No hay Registros</h3>
                @endif
                <input type="hidden" value="{{$data['i']}}" id="filas">
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>

    $('body').on('keyup','.detalle',function(){
        var jqxhr = $.post("{{url('/')}}/guardarferiaconferenciapresupuestoadmin", $("#formulario_basico").serialize())
        .done(function() {
            console.log("Guardado");
        })
        .fail(function(e) {
            console.error(e.msj)
        })
        .always(function(e) {
          console.log(e.msj)
        });
    });

    $('body').on('click','.eliminar',function(){
       id=$(this).attr('id');
       id=id.replace('eliminar_','');
        swal({
          title: 'Esta seguro?',
          text: "Va a eliminar un item de presupuesto",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
            $.ajax({
                url: "{{url('/')}}/eliminaritempresupuestoconferencia/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal(
                        'Eliminar!',
                        'Se elimino correctamente.',
                        'success'
                      ).then(function () {
                        parent.window.location.reload(true);
                    });
                }
            });
        });
    });
    fila=$('#filas').val();
    $('body').on('click','.crearitem',function(){
        $.ajax({
                url: "{{url('/')}}/crearitempresupuestoconferencia/{{$data['id']}}",
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    numero=parseInt(fila)+1;
                    html=`<input type="hidden" name="presupuesto[`+fila+`][id]" value="`+e.id+`">
                                <div class="row">
                                    <div class="mt-3" style="width: 2%;">
                                        `+numero+`.
                                    </div>
                                    <div class="col-sm-11">
                                        <div class="form-group">
                                            <label for="concepto`+fila+`" class="control-label sr-only">Concepto</label>
                                            <input type="text" class="form-control detalle" name="presupuesto[`+fila+`][concepto]" id="concepto`+fila+`"  placeholder="Concepto">
                                        </div>
                                    </div>
                                    <i class="fa fa-trash eliminar" id="eliminar_`+e.id+`" aria-hidden="true"></i>
                                </div>`;
                    $('#formulario_basico').append(html);
                    fila++;
                }
            });

    });
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
