@extends('template.app')
@section('title', 'Ver Galeria')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/vergaleria.css">
<input type="hidden" id="URL" value="{{url('/')}}">
<input type="hidden" id="id_feria" value="{{$id}}">
<div class="widget">
<?php echo $menu; ?>
	<div class="widget-header">
		<h2>Galeria</h2>
	</div>
	<div class="widget-content">
        <div class="row">
            <div class="col-md-1">

            </div>
            <div class="col-md-3 seleccionado">
                <img src="http://megaarchivo.smartessi.com/multimedia/explorador/play_blanco.png" class="video imagen"><a>Videos ({{$contador['videos']}})</a>
            </div>
            <div class="col-md-3 no_seleccionado">
                <img src="http://megaarchivo.smartessi.com/multimedia/explorador/Img_gris.png" class="foto imagen"><a>Imagenes ({{$contador['imagenes']}})</a>
            </div>
            <div class="col-md-3 no_seleccionado">
                <img src="http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_gris.png" class="carpeta imagen"><a>Documentos ({{$contador['archivos']}})</a>
            </div>
            <div class="col-md-1">

            </div>
        </div>
        <div class="row margen-top archivos" id="cont-videos">
            <?php echo $videos ?>
        </div>

        <div class="row margen-top archivos" id="cont-imagenes">
            <?php echo $imagenes ?>
        </div>

        <div class="row margen-top archivos" id="cont-archivos">
            <?php echo $archivos ?>
        </div>
    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade bd-example-modal-lg" id="documento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <iframe id="ver_pdf" src="http://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>
@endsection


@section('scripts')
<script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/vergaleria.js"></script>
@endsection
