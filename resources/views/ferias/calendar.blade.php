@extends('template.app')
@section('title', 'Calendario de Ferias')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.min.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.1/fullcalendar.print.min.css" rel='stylesheet' media='print' />
<link rel="stylesheet" href="{{ url('/') }}/css/ferias/calendar.css" />


<div class="card animated flipInX" id="list" style="margin-bottom: 30px;">
    <div class="card-block">
	    <div class="pull-right">
        <a href="{{ url('feriasperdidas') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias perdidas
        </a>
        <a href="{{ url('feriasposibles') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Posibles ferias
        </a>
        <a href="{{ url('feriasasistidas') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias asistidas
        </a>
        <a href="{{ url('crearferias') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Crear feria
        </a>
        <a href="{{ url('proveedores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Proveedores
        </a>
        <a href="{{ url('feria/patrocinadores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Patrocinadores
        </a>
      </div>
	    <div class="pull-left">
            <div style="width: 10px;height: 30px;background: #D95350;"><span class="position-left">No asistida</span></div>
            <div style="width: 10px;height: 30px;background: #D4AC0D;"><span class="position-left">Futura</span></div>
            <div style="width: 10px;height: 30px;background: #16A085;"><span class="position-left">ESSI Exponente</span></div>
            <div style="width: 10px;height: 30px;background: #016090;"><span class="position-left">ESSI Visitante</span></div>
	    </div>
    	<h2 id="tituloPrincipal">Calendario de ferias</h2>
    	<div id="loaders" style="display: none;"><div class="loader"></div><small>Cargando registros...</small></div>
    	<div id='script-warning'>
			<code>Error!, no se han podido cargar los datos</code>
		</div>
        <div id="calendarcontent">
			<div id='calendar'></div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src='{{ url("/") }}/components/newfullcalendar/lib/moment.min.js'></script>
<script src='{{ url("/") }}/components/newfullcalendar/fullcalendar.min.js'></script>
<script src='{{ url("/") }}/components/newfullcalendar/locale-all.js'></script>
<script src='{{ url("/") }}/js/ferias/calendar.js'></script>
@endsection
