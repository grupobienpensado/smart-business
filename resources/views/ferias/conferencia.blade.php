@extends('template.app')
@section('title', 'Ver Feria')

@section('content')
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="{{ url('/') }}/css/ferias/conferencia.css" rel="stylesheet">
<div class="widget">
<?php echo $menu; ?>

    <input type="hidden" id="id_feria" value="{{$dato->id}}">
    <input type="hidden" id="URL" value="{{url('/')}}">
	<div class="main-header">
        <h2>Conferencias</h2>
        <em>{{$dato->nombre}}</em>
       <div class="row">
            <div class="col-lg-12">
                <div class="top-content">
                    <ul class="list-inline quick-access">
                        <li>
                            <a href="{{url('/')}}/presupuestoconferencia/{{$dato->id}}">
                                <div class="quick-access-item bg-color-blue" style="background-color: #3d8b9e;">
                                    <i class="fa fa-sitemap"></i>
                                    <h5>Presupuesto</h5><em>(Tematica)</em>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}/presupuestoconferenciacomparar/{{$dato->id}}">
                                <div class="quick-access-item bg-color-blue">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <h5>Comparar</h5><em>presupuesto y valor real</em>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}/presupuestoconferenciacomentarios/{{$dato->id}}">
                                <div class="quick-access-item bg-color-blue" style="background-color: #75d0e6;">
                                    <i class="fa fa-comments-o"></i>
                                    <h5>Comentario</h5><em>Realizar comentario</em>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--<div class="row">
        <div class="col-md-12">
            <i class="fa fa-plus-circle crear_conferencia" id="feria_{{$dato->id}}" style="color: #001d64;font-size: -webkit-xxx-large;" title="Crear conferencia"></i>
        </div>
    </div>
	<!--<div class="widget-content">
        @foreach($conferencias as $conferencia)
        <div class="row cabecera">
            <div class="col-md-1">
                <div class="numero-conferencia">
                    <a>{{$i+1}}</a>
                </div>
            </div>
            <div class="col-md-4">
                <hr>
            </div>
            <div class="col-md-3">
                <h3 class="nombre-conferencia"><em><input type="text" class="form-control titulo" placeholder="Titulo" id="titulo_{{$conferencia->id}}" value="{{$conferencia->titulo}}"></em></h3>
            </div>
            <div class="col-md-4">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="contextual-summary-info contextual-background azul-bg conferencista" id="todos_conferencista{{$conferencia->id}}">
                    <i class="fa fa-plus-circle crear-conferencista" id="conferencista{{$conferencia->id}}"></i>
                    @php $conferencistas=explode('%%',$conferencia->conferencistas) @endphp
                    @php $foto_conferencista=explode('%%',$conferencia->foto_conferencista) @endphp
                    <p class="pull-right total-conferencistas{{$conferencia->id}}"><span>Conferencistas</span> @if($conferencia->conferencistas != "") {{count($conferencistas)}} @else 0 @endif</p>
                    @php $f=0; @endphp
                    @if($conferencia->conferencistas != "")
                        @foreach($conferencistas as $conferencista)
                        <div class="row fila-conferencista f-conferencista{{$conferencia->id}}" id="fila_conferencista{{$f}}_{{$conferencia->id}}">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">

                                @if($foto_conferencista[$f] == 'null')
                                <img src="{{url('/')}}/storage/ferias/conferencista/noImagen.png" alt="Avatar" class="avatar img-circle img-lista">
                                @else
                                <img src="{{url('/')}}/storage/ferias/conferencista/{{$foto_conferencista[$f]}}" alt="Avatar" class="avatar img-circle img-lista">
                                @endif
                            </div>
                            <div class="col-md-7 nombre-conferencista">
                                <a>{{$conferencista}}</a>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash eliminar_conferencista" aria-hidden="true" title="Eliminar {{$conferencista}}" onclick="eliminar_conferencista('{{$conferencista}}','{{$conferencia->id}}','{{$f}}')"></i>
                            </div>
                        </div>
                        @php $f++; @endphp
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="contextual-summary-info contextual-background azul1-bg conferencista">
                    <i class="fa fa-plus-circle crear-tema" id="tema{{$conferencia->id}}"></i>
                    @php $temas=explode('%%',$conferencia->temas); $t=0; @endphp
                    <p class="pull-right total-temas{{$conferencia->id}}"><span>Temas</span> @if($conferencia->temas != '') {{count($temas)}} @else 0 @endif</p>
                    <div id="contenedor_temas_{{$conferencia->id}}">
                      @if($conferencia->temas != '')
                        @foreach($temas as $tema)
                        <div class="row fila-conferencista" id="fila_tema_{{$t}}_{{$conferencia->id}}">
                            <div class="col-md-2"></div>
                            <div class="col-md-1 tema-conferencia">{{$t+1}}.</div>
                            <div class="col-md-7 tema-conferencia">
                                <a>{{$tema}}</a>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-trash eliminar_temas" aria-hidden="true" title="Eliminar {{$tema}}" onclick="eliminar_tema('{{$tema}}','{{$conferencia->id}}','fila_tema_{{$t}}_{{$conferencia->id}}')"></i>
                            </div>
                        </div>
                        @php $t++; @endphp
                        @endforeach
                      @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contextual-summary-info contextual-background azul-bg conferencista">
                    <i class="fa fa-check-circle"></i>
                    <p class="pull-right"><span>Datos</span> 3</p>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9 nombre-conferencista">
                            <input type="text" class="form-control datos" id="ubicacion_{{$conferencia->id}}" name="ubicacion" value="{{$conferencia->ubicacion}}" placeholder="Ubicación">
                            <input class="form-control2 form-control datatime datos" id="fecha_{{$conferencia->id}}" name="fecha" value="{{$conferencia->fecha}}" type="text" step="1800" placeholder="Fecha y hora" required>
                            @php $duracion=explode(':',$conferencia->duracion); @endphp
                            <a class="duracion">Duración Horas: <input type="number" class="form-control duracion_h_m" id="hora_{{$conferencia->id}}" name="hora" min="0" @if(isset($duracion[0])) value="{{$duracion[0]}}" @endif placeholder="Horas"></a>
                            <a class="duracion">Duración Minutos: <input type="number" class="form-control duracion_h_m" id="minuto_{{$conferencia->id}}" name="minutos" min="0" max="59" @if(isset($duracion[1])) value="{{$duracion[1]}}" @endif placeholder="minutos"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php $i++; @endphp
        @endforeach
    </div>-->
</div>

<div class="container">
    <div class="row border">
        <div class="col-md-10">
            <h2 class="encabezado">{{$dato->nombre}}</h2>
        </div>
        <div class="col-md-2 text-right crear-conferencia">
            <a class="h6" data-toggle="tooltip" data-placement="top" data-original-title="Crear conferencia" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> onclick="new_conference({{$dato->id}})" <?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para crear una conferencia')"<?php } ?> ><img src="{{url('/')}}/images/iconosferias/conferencia/crearconferencia_icon.svg"> Crear conferencia</a>
        </div>

    </div>
</div>
<!-- Modal -->
<div id="crear_conferencista" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close exis" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear conferencista</h4>
      </div>
      <div class="modal-body">
          <form id="formulario_conferencista" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
            <input type="hidden" id="id_conferencia" name="id" value="">
            <input type="hidden" name="direccion" value="{{url('/')}}">
            <input type="hidden" id="valor_f" name="f" value="">
            <div class="row">
                <div class="col-md-12">
                   <div class="dropzone logo" data-width="200" data-height="200" data-ajax="false" data-originalsave="true" style="max-height: 200px; max-width: 200px; min-height: 200px; min-width: 200px;">
                      <input type="file" name="fotoconferencista" accept="image/gif, image/jpeg, image/png">
                   </div>
                </div>
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Nombre Conferencista" name="conferencista" required="required">
                </div>
                <div class="col-md-12">
                    <a class="btn btn-success guardar_conferencista"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                </div>
             </div>
           </form>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="crear_temas" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close exis" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear tema</h4>
      </div>
      <div class="modal-body">
          <form id="formulario_tema" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
            <input type="hidden" id="id_conferencia_tema" name="id" value="">
            <div class="row">
                <div class="col-md-12">
                    <a class="mas-temas" title="Agregar Tema"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="row contenedor-de-temas">

                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Tema" name="tema[0]" required="required">
                </div>

             </div>
             <div class="row">
                 <div class="col-md-12">
                    <a class="btn btn-success guardar_tema"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                </div>
             </div>
           </form>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')

<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/conferencia.js"></script>
@endsection
