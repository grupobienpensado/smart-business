@extends('template.app')
@section('title', 'Ver Feria - Competencia')
@section('content')
<link rel="stylesheet" href="/css/blueimp-gallery.min.css">
<style>
    .img-round{border-radius: 200px 200px 200px 200px;-moz-border-radius: 200px 200px 200px 200px;-webkit-border-radius: 200px 200px 200px 200px;border: 0px solid #000000;}.no-select:hover{filter: opacity(.5);cursor: pointer;}.select{filter: opacity(.5);cursor: no-drop;}#btn_agregar_competencia{font-size:6rem; cursor:pointer;}.dropzone{border-style: dotted !important;border-width: 3px !important;border-color: #011d62 !important;}.col-archivo{max-width: 100%;position: relative;overflow: hidden;max-height: 203px;min-width: 203px;min-height: 203px;margin: 10px;padding: 0px;}img.icono-video {width: auto;position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;margin: auto;height: 100%;}
</style>
{{ csrf_field() }}
<div class="widget">
    <?php echo $data['menu']; ?>
    <div class="widget-header">
		<h2>Competencia de la feria <?=$data['feria']->nombre?></h2>
	</div>
    <div class="widget-content">
       <div class="row" id="contenido">

       </div>
       <div class="row my-5" id="archivos">

       </div>

    </div>
</div>
<!--Agregar Empresas-->
<div class="modal fade bd-example-modal-lg" id="agregar_empresas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel_2">Agregar Empresas a la feria</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row" id="agregar">

           </div>
      </div>
    </div>
  </div>
</div>
<!--Ver Imagenes-->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;" />
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Ver Video-->
<div class="modal fade bd-example-modal-lg" id="reproductor_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="reproductor" src="" allowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Ver Archivos-->
<div class="modal fade bd-example-modal-lg" id="documento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <iframe id="ver_pdf" src="http://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="/js/jquery.blueimp-gallery.min.js"></script>
<script src="/assets/js/plugins/dropzone/dropzone.min.js"></script>
<script>
    var CSRF_TOKEN = $('input[name=_token]').val();
    $(document).ready(function(){
        lista({{$data['id']}});
        lista_boton_add({{$data['id']}});
    });

    /**
     * Función para inicializar recursos que se usen en el archivo
     */
    function inicializacion(){
        $('[data-toggle="tooltip"]').tooltip();
        // if dropzone exist
        if( $('.dropzone').length > 0 ) {
            Dropzone.autoDiscover = false;

            $(".dropzone").dropzone({
                url: "/cargarmultimediacompetenciaferia",
                addRemoveLinks : true,
                maxFilesize: 30,
                maxFiles: 50,
                acceptedFiles: 'image/*, video/mp4, application/pdf, .txt',
                dictResponseError: 'File Upload Error.',
                success: function(e){
                    ver_archivos($('#id_ferias_competencias_carga').val(),$('#id_competencia_carga').val());
                }
            });
        } // end if dropzone exist
    }
    /**
     * Función para listar todas las competencias de la feria
     * @param INT id IDENTIFICACIÓN DE LA FERIA
     */
    function lista(id){
        var jqxhr = $.ajax({
            url: "/competencia-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 0,
                id: id
            },
            cache: false,
            type: 'POST',
            success: function(e){
               $('#contenido').html(e.data['html']);
                inicializacion();
            }
        });
    }

    /**
     * Función para traer todas las competencias registradas en la base de datos
     * @param INT id IDENTIFICACIÓN DE LA FERIA
     */
    function lista_boton_add(id){
        var jqxhr = $.ajax({
            url: "/competencia-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 1,
                id:id
            },
            cache: false,
            type: 'POST',
            success: function(e){
               $('#agregar').html(e.data['html']);
                inicializacion();
            }
        });
    }

    /**
     * Funcion para abrir el listado de competencias
     */
    function add_competencia(){
        $('#listado_competencias').fadeIn('1000');
        $('#btn_agregar_competencia').fadeOut('1000');
    }

    /**
     * Función para cerrar el listado de competencias
     */
    function close_competencia(){
        $('#listado_competencias').fadeOut('1000');
        $('#btn_agregar_competencia').fadeIn('1000');
    }

    /**
     * Función para agregar una competencia a la feria
     * @param INT id IDENTIFICACIÓN DE LA COMPETENCIA
     * @param STRING nombre NOMBRE DE LA COMPETENCIA
     */
    function competencia_agregar(id,nombre){
        swal({
              title: 'Seguro?',
              text: "Va agregar la competencia "+nombre,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'No',
              confirmButtonText: 'Si, agregar'
            }).then(function(){
                var jqxhr = $.ajax({
                    url: "/competencia-accion",
                    data: {
                        _token: CSRF_TOKEN,
                        accion: 2,
                        id_feria:<?=$data['id']?>,
                        id:id
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal(
                              'Agregada',
                              'Agregada correctamente!',
                              'success'
                            )
                       lista({{$data['id']}});
                       lista_boton_add({{$data['id']}});
                    }
                });
        });
    }

    /**
     * Función para ver los archivos cargados a la competencia
     * @param INT id IDENTIFICACIÓN DE LA TABLA ferias_competencias
     * @param INT id_competencia IDENTIFICACIÓN DE LA TABLA competencias
     */
    function ver_archivos(id,id_competencia){
        var jqxhr = $.ajax({
            url: "/competencia-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 3,
                id_feria:<?=$data['id']?>,
                id:id,
                id_competencia:id_competencia
            },
            cache: false,
            type: 'POST',
            success: function(e){
               $('#archivos').html(e.data['html']);
                inicializacion();
            }
        });
    }

    /**
     * Función para mostrar el sweetAlert con el dropzone que guarda los archivos
     * @param INT id IDENTIFICACIÓN DE LA TABLA ferias_competencias
     */
    function add_archivo(id){
        var jqxhr = $.ajax({
            url: "/competencia-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 4,
                id:id
            },
            cache: false,
            type: 'POST',
            success: function(e){
              swal({
                  title: 'Cargar Archivos',
                  html:e.data['html'],
                  showCloseButton: true,
                  showCancelButton: false,
                  confirmButtonColor: '#d33',
                  focusConfirm: false,
                  confirmButtonText:'<i class="fa fa-times"></i> Cerrar'
                });
                inicializacion();
                $('.swal2-modal').css('width','70%');
            }
        });
    }

    /**
     * Función para abrir el video
     * @param STRING url DIRECCIÓN DE VIDEO A REPRODUCIR
     */
    function ver_video(url){
        $('#reproductor').attr('src',url);
        $('#reproductor_video').modal('show');
    }

    /**
     * Función para ver los archivos PDFs de la competencia
     * @param STRING archivo RUTA DE ARCHIVO PDF
     */
    function ver_pdf(archivo){
        $('#ver_pdf').attr('src',archivo);
        $('#documento').modal('show');
    }

    /**
     * Función para eliminar los archivos cargados para la feria
     * @param INT id      IDENTIFICACION DE LA TABLA ferias_competencias_archivos
     * @param STRING archivo NOMBRE DEL ARCHIVO
     */
    function eliminar_archivo(id,archivo){
        swal({
          title: 'Esta seguro?',
          text: "Desea eliminar el archivo "+archivo,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'No',
          confirmButtonText: 'Si, Eliminar!'
        }).then(function() {
            eliminar_archivo2(id);
        });
    }

    /**
     * Funcion para eliminar el archivo
     * @param INT id      IDENTIFICACION DE LA TABLA ferias_competencias_archivos
     */
    function eliminar_archivo2(id){
        var jqxhr = $.ajax({
            url: "/competencia-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 5,
                id:id
            },
            cache: false,
            type: 'POST',
            success: function(e){
              if(e.data['msj'] == "Eliminado correctamente!"){
                 swal(
                      'Eliminado!',
                      e.data['msj'],
                      'success'
                    );
                  ver_archivos(e.data['id'],e.data['id_competencia']);
                 }else{
                     swal(
                      'Error',
                      e.data['msj'],
                      'success'
                    );
                 }
            }
        });
    }

    /**
     * Función para eliminar una competencia con todos sus archivos adjuntos
     * @param INT id IDENTIFICACIÓN DE LA TABLA ferias_competencias
     */
    function eliminar_competencia(id){
        swal({
          title: 'Esta seguro?',
          text: "Desea eliminar la competencia con todos los archivos cargados",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'No',
          confirmButtonText: 'Si, Eliminar!'
        }).then(function() {
            eliminar_competencia2(id);
        });
    }

    /**
     * Función para eliminar una competencia con todos sus archivos adjuntos
     * @param INT id IDENTIFICACIÓN DE LA TABLA ferias_competencias
     */
    function eliminar_competencia2(id){
        var jqxhr = $.ajax({
            url: "/competencia-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 6,
                id:id
            },
            cache: false,
            type: 'POST',
            success: function(e){
              if(e.data['msj'] == "Eliminado correctamente!"){
                 swal(
                      'Eliminado!',
                      e.data['msj'],
                      'success'
                    );
                    lista({{$data['id']}});
                    lista_boton_add({{$data['id']}});
                  $('#archivos').html('');
                 }else{
                     swal(
                      'Error',
                      e.data['msj'],
                      'success'
                    );
                 }
            }
        });
    }

    /**
     * Función para abrir modal de competencias
     */
    function abrir_modal_competencias(){
        $('#agregar_empresas').modal('show');
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
