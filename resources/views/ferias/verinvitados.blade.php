@extends('template.app')
@section('title', 'Ver invitados')

@section('content')
<input type="hidden" id="URL" value="{{url('/')}}">
<input type="hidden" id="id_feria" value="{{$data['datos']->id}}">
{{ csrf_field() }}
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/verinvitados.css">
<div class="widget">
<?php echo $menu; ?>

	<div class="main-header margin-top">
        <h2>Invitados</h2>
        <em>{{$data['datos']->nombre}}</em>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <a <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>href="{{url('/')}}/invitado/crear/{{$data['datos']->id}}"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para añadir un invitado')"<?php } ?>><i class="fa fa-plus-circle agregar-invitado" aria-hidden="true"></i></a>
    </div>
</div>
<div class="board">
    <div class="board-inner">
        <ul class="nav nav-tabs" id="board_tabs">
            <div class="liner"></div>
            <li class="active">
                <a href="#nuevos" data-toggle="tab" title="Invitados nuevos">
                  <span class="round-tabs one">
                          <i class="fa fa-user" aria-hidden="true"></i>
                  </span>
              </a>
            </li>
            <li><a href="#clientesinvitados" data-toggle="tab" title="Invitados clientes">
                 <span class="round-tabs three">
                      <i class="fa fa-address-book" aria-hidden="true"></i>
                 </span> </a>
            </li>
        </ul>
    </div>
    <div class="tab-content text-center">
        <div class="tab-pane fade in active" id="nuevos">
            <div class="container">

                <div class="row">
                    @foreach($data['invitados_nuevos'] as $invitado)
                    <div class="col-lg-3 col-sm-6 mt-3 mb-3">
                        <div class="card">
                            <div class="cardheader"></div>
                            <div class="avatar">
                                @if(!empty($invitado->foto))
                                <img alt="" src="{{url('/')}}/storage/ferias/invitados/{{$invitado->foto}}">
                                @else
                                <img alt="" src="https://www.shareicon.net/data/2016/09/01/822711_user_512x512.png">
                                @endif
                            </div>
                            <div class="card-body pr3 pl-3">
                                <h4 class="card-title">{{$invitado->nombre}}</h4>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">{{$invitado->empresa}}</li>
                                    <li class="list-group-item">{{$invitado->cargo}}</li>
                                    <li class="list-group-item">{{$invitado->correo}}</li>
                                    <li class="list-group-item">{{$invitado->telefono}}</li>
                                </ul>
                            </div>
                            <div class="card-footer text-center">
                                <div class="share-buttons">
                                    <a class="share-button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>href="/invitado/editar/{{$data['datos']->id}}&{{$invitado->id}}"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para editar un invitado')"<?php } ?> >
                                        <div class="share-button-secondary">
                                            <div class="share-button-secondary-content">
                                                Editar
                                            </div>
                                        </div>
                                        <div class="share-button-primary">
                                            <i class="share-button-icon fa fa-pencil"></i>
                                        </div>
                                    </a>
                                    <a class="share-button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="eliminar({{$invitado->id}})"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para eliminar un invitado')"<?php } ?>>
                                        <div class="share-button-secondary">
                                            <div class="share-button-secondary-content">
                                                Eliminar
                                            </div>
                                        </div>
                                        <div class="share-button-primary">
                                            <i class="share-button-icon fa fa-trash"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="clientesinvitados">
            <div class="container">
                <div class="row">
                    @foreach($data['invitados_clientes'] as $invitado)
                    <div class="col-lg-3 col-sm-6 mt-3 mb-3">
                        <div class="card">
                            <div class="cardheader"></div>
                            <div class="avatar">
                                @if(!empty($invitado->foto))
                                <img alt="" src="{{url('/')}}/images/file/clientes/{{$invitado->foto}}">
                                @else
                                <img alt="" src="https://www.shareicon.net/data/2016/09/01/822711_user_512x512.png">
                                @endif
                            </div>
                            <div class="card-body pr3 pl-3">
                                <h4 class="card-title">{{$invitado->nombre}}</h4>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">{{$invitado->empresa}}</li>
                                    <li class="list-group-item">{{$invitado->cargo}}</li>
                                    <li class="list-group-item">{{$invitado->correo}}</li>
                                    <li class="list-group-item">{{$t[$invitado->id]['telefonos']}}</li>
                                    <li class="list-group-item">{{$t[$invitado->id]['celulares']}}</li>
                                </ul>
                            </div>
                            <div class="card-footer text-center">
                                <div class="share-buttons">
                                    <a class="share-button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="modificar({{$invitado->id_principal}})"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para modificar un invitado')"<?php } ?>>
                                        <div class="share-button-secondary">
                                            <div class="share-button-secondary-content">
                                                Modificar
                                            </div>
                                        </div>
                                        <div class="share-button-primary">
                                            <i class="share-button-icon fa fa-pencil"></i>
                                        </div>
                                    </a>
                                    <a class="share-button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="eliminar({{$invitado->id_principal}})"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para eliminar un invitado')"<?php } ?>>
                                        <div class="share-button-secondary">
                                            <div class="share-button-secondary-content">
                                                Eliminar
                                            </div>
                                        </div>
                                        <div class="share-button-primary">
                                            <i class="share-button-icon fa fa-trash"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
    $('[data-toggle="tooltip"]').tooltip();
    url_basico = $('#URL').val();
    id_feria=$('#id_feria').val();
    function modificar(id_invitado){
        $.ajax({
            url: url_basico+"/consultarclientes",
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                swal({
                  title: '<i>Seleccione un cliente</i>',
                  type: 'info',
                  html:e.html,
                  focusConfirm: false,
                  confirmButtonText: '<i class="fa fa-paperclip"></i> Guardar!',
                  showCancelButton: false
                }).then(function () {
                    valor=$('.cliente').val();
                    motivo=$('#objetivo_invitacion').val();
                    if(valor == "" || motivo == ""){
                        swal({
                          title: 'Error...',
                          html:'Debe seleccionar un cliente y agregar un motivo de invitación!',
                          type: 'error',
                          allowOutsideClick: false
                        }).then(function () {
                            parent.window.location.reload(true);
                        });
                    }else{
                        id=$('.cliente').val();
                        var CSRF_TOKEN = $('input[name=_token]').val();
                        var jqxhr = $.ajax({
                            url: url_basico+"/editarinvitadocliente",
                            data: {
                                _token: CSRF_TOKEN,
                                id: id_invitado,
                                id_cliente: id,
                                id_feria: id_feria,
                                motivo: motivo
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                                swal(
                                    'Guardado!',
                                    e.msj,
                                    'success'
                                  ).then(function () {
                                    location.href = url_basico+"/invitados/ver/"+id_feria;
                                  });
                            }
                        });
                    }
                })
            }
        });
    }

    /*Parte de empresa sweet principal*/
    $('body').on('change','.empresa',function(){
        $('#empresa_mostrar').val('');
        $('.empresa').css('display','none');
        $('#empresa_mostrar').css('display','block');
        $('.cambiar-empresa').css('display','block');
        $.ajax({
            url: url_basico+"/consultarempresas/"+$(this).val(),
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#empresa_mostrar').val(e.data['nombre_empresa']);
                $('#clientes').html(e.data['content']);
            }
        });
    });

    $('body').on('click','.cambiar-empresa',function(){
        $('#clientes').html('');
        $('.empresa').val('');
        $('.empresa').css('display','block');
        $('#empresa_mostrar').css('display','none');
        $('.cambiar-empresa').css('display','none');

        $('.cliente').val('');
        $('.cliente').css('display','block');
        $('#cliente_mostrar').css('display','none');
        $('.cambiar-cliente').css('display','none');
    });
    /*Fin Parte de empresa sweet principal*/


    /*Parte de cliente sweet principal*/
    $('body').on('change','.cliente',function(){
        $('#cliente_mostrar').val('');
        $('.cliente').css('display','none');
        $('#cliente_mostrar').css('display','block');
        $('.cambiar-cliente').css('display','block');
        $.ajax({
            url: url_basico+"/consultarclientes2/"+$(this).val(),
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#cliente_mostrar').val(e.nombre);
            }
        });
    });

    $('body').on('click','.cambiar-cliente',function(){
        $('.cliente').val('');
        $('.cliente').css('display','block');
        $('#cliente_mostrar').css('display','none');
        $('.cambiar-cliente').css('display','none');
    });
    /*Fin Parte de cliente sweet principal*/

    function eliminar(id){
        swal({
          title: 'Eliminar?',
          text: "Desea realmente eliminar este invitado",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!',
          cancelButtonText: 'No'
        }).then(function () {
            $.ajax({
                url: url_basico+"/eliminarinvitado/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal(
                      'Eliminado!',
                      e.msj,
                      'success'
                    ).then(function (){
                        location.href = url_basico+"/invitados/ver/"+id_feria;
                    });
                }
            });
        });
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
