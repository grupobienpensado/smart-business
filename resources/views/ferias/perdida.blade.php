@extends('template.app')
@section('title', 'Ferias perdidas')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/listadoposibles.css">

<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('calendario/feria') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Calendario de ferias
        </a>
        <a href="{{ url('feriasposibles') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Posibles ferias
        </a>
        <a href="{{ url('feriasasistidas') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias asistidas
        </a>
        <a href="{{ url('crearferias') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Crear feria
        </a>
        <a href="{{ url('proveedores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Proveedores
        </a>
        <a href="{{ url('feria/patrocinadores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Patrocinadores
        </a>
      </div>
      <h2>Listado de ferias perdidas</h2>
    </div>
</div>

<div class="card animated slideInDown" style="margin-bottom: 30px;">
<div class="card-block">


    @if(count($datos) > 0)
    <div class="table-responsive">
      <table id="example" width="100%" class="table table-striped table table-striped table-bordered display">
        <thead>
          <tr>
            <th class="centrado">Item</th>
            <th class="centrado">Logo</th>
            <th class="centrado">Nombre</th>
            <th class="centrado">Ubicacion</th>
            <th class="centrado">Inicio</th>
            <th class="centrado">Fin</th>
            <th class="centrado">Creada</th>
            <th class="centrado">Editada</th>
            <th class="centrado">Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($datos as $dato)
          @php
            if(isset($dato->logo) && !empty($dato->logo)){
              $linkfoto = url('/').'/storage/ferias/'.$dato->logo;
            }else {
              $linkfoto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100';
            }
            $foto = '<img src="'.$linkfoto.'" style="max-width: 50px;" class="img-fluid img-thumbnail mx-auto d-block">';
          @endphp
          <tr>
            <td class="centrado centrar-vertical"><p class="mas-pequeño"></p></td>
            <td class="centrado centrar-vertical"><?php echo $foto; ?></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->nombre }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->ubicacion }}</p></td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y", strtotime($dato->fecha_inicio)) }}
              </p>
            </td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y", strtotime($dato->fecha_fin)) }}
              </p>
            </td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->created_at)) }}
              </p>
            </td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->updated_at)) }}
              </p>
            </td>
            <td class="text-center">
                <button class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ver feria" onclick="verFeria({{ $dato->id }})">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </button>
            </td>
          </tr>

          @endforeach
        </tbody>
      </table>
    </div>
    @endif
</div>
</div>

@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/ferias/listaposible.js"></script>
@endsection
