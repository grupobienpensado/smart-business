@extends('template.app')
@section('title', 'Listado de patrocinadores')
@section('content')
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<style>
    .charts {
      width: 100%;
      height: 25rem !important;
    }

    .budget-header{
        width: 100%;
        height: 100%;
        border-radius: 5px;
    }
    .budget-header h3 {
        margin: 0;
    }
    .budget-wrap :nth-child(1) .budget-item .budget-header{

        background-color: rgba(100, 56, 171, 0.6);
    }
    .budget-wrap :nth-child(2) .budget-item .budget-header{
        background-color: rgba(62, 134, 176, 0.6);
    }

    .budget-wrap :nth-child(3) .budget-item .budget-header{
        background-color: rgba(213, 116, 59, 0.6);
    }
    .budget-item{
        border-radius: 5px;
        background-repeat:no-repeat;
        background-size:100%;
        background-position:center;
    }
    .budget-wrap :nth-child(1) .budget-item{
        background-image: url("{{url('/')}}/images/bg_real.png");
        background-size: cover;
       -moz-background-size: cover;
       -webkit-background-size: cover;
       -o-background-size: cover;
       -ms-background-size: cover;
    }
    .budget-wrap :nth-child(2) .budget-item{
        background-image: url("{{url('/')}}/images/bg_procumpli.png");
        background-size: cover;
       -moz-background-size: cover;
       -webkit-background-size: cover;
       -o-background-size: cover;
       -ms-background-size: cover;
    }
    .budget-wrap :nth-child(3) .budget-item{
        background-image: url("{{url('/')}}/images/bg_patrocinio.png");
        background-size: cover;
       -moz-background-size: cover;
       -webkit-background-size: cover;
       -o-background-size: cover;
       -ms-background-size: cover;
    }
    .pallets-img{
        width: 100%;
    }
    .gradient-aguamarina{
        background-image: -moz-linear-gradient( 153deg, rgb(6,106,124) 0%, rgb(14,171,162) 100%);
        background-image: -webkit-linear-gradient( 153deg, rgb(6,106,124) 0%, rgb(14,171,162) 100%);
        background-image: -ms-linear-gradient( 153deg, rgb(6,106,124) 0%, rgb(14,171,162) 100%);
        border-radius: 10px;
    }

    .gradient-azul{
        background-image: -moz-linear-gradient( 153deg, rgb(87,173,201) 0%, rgb(6,68,138) 100%);
        background-image: -webkit-linear-gradient( 153deg, rgb(87,173,201) 0%, rgb(6,68,138) 100%);
        background-image: -ms-linear-gradient( 153deg, rgb(87,173,201) 0%, rgb(6,68,138) 100%);
        border-radius: 10px;
    }
    @media (min-width: 1366px) and (max-width: 1599px) {
        .real{
            font-size: 32px;
        }
        .patrocinio p{
            font-size: 32.44px !important;
        }
    }
    #board_tabs li.active:after,li:after{content:" ";bottom:0}
    .modal-title{width:80%}
    #dialog_mdl,#dialog_mdl>.modal-content,#doc_container{width:100%!important;height:100%!important;margin-top:2%}
    .action_btn,.comment_btn{cursor:pointer}
    .board{width:100%;margin:60px auto;height:100%;background:#fff}
    .board h4{margin:0;padding:20px 0 0}
    .board .nav-tabs{width:30%;display:inline-block;position:relative;margin:40px auto 0;box-sizing:border-box}
    .board>div.board-inner{background:url(http://subtlepatterns.com/patterns/geometry2.png) #fafafa;background-size:30%}
    p.narrow{width:60%;margin:10px auto}
    #board_tabs li.active:after,.liner,li:after{position:absolute;margin:0 auto}
    .liner{height:2px;background:#ddd;width:40%;left:0;right:0;top:50%;z-index:1}
    #board_tabs.nav-tabs>li.active>a,#board_tabs.nav-tabs>li.active>a:focus,#board_tabs.nav-tabs>li.active>a:hover{color:#555;cursor:default;border:0;border-bottom-color:transparent}
    span.round-tabs{width:70px;height:70px;line-height:70px;display:inline-block;border-radius:100px;background:#fff;z-index:2;position:absolute;left:0;text-align:center;font-size:25px}
    span.round-tabs.one{color:#22c222;border:2px solid #22c222}
    li.active span.round-tabs.one{background:#fff!important;border:2px solid #ddd;color:#22c222}
    span.round-tabs.two{color:#febe29;border:2px solid #febe29}
    li.active span.round-tabs.two{background:#fff!important;border:2px solid #ddd;color:#febe29}
    span.round-tabs.three{color:#3e5e9a;border:2px solid #3e5e9a}
    li.active span.round-tabs.three{background:#fff!important;border:2px solid #ddd;color:#3e5e9a}
    span.round-tabs.four{color:#f1685e;border:2px solid #f1685e}
    li.active span.round-tabs.four{background:#fff!important;border:2px solid #ddd;color:#f1685e}
    span.round-tabs.five{color:#999;border:2px solid #999}
    li.active span.round-tabs.five{background:#fff!important;border:2px solid #ddd;color:#999}
    #board_tabs.nav-tabs>li.active>a span.round-tabs{background:#fafafa}
    #board_tabs.nav-tabs>li{width:50%}
    li:after{left:45%;opacity:0;border:5px solid transparent;border-bottom-color:#ddd;transition:.1s ease-in-out}
    #board_tabs li.active:after{left:49%;opacity:1;border:10px solid transparent;border-bottom-color:#ddd}
    #board_tabs.nav-tabs>li a{width:70px;height:70px;margin:20px auto;border-radius:100%;padding:0}
    .nav-tabs>li a:hover{background:0 0}
    .tab-content .head{font-family:'Roboto Condensed',sans-serif;font-size:25px;text-transform:uppercase;padding-bottom:10px}
    .btn-outline-rounded{padding:10px 40px;margin:20px 0;border:2px solid transparent;border-radius:25px}
    .btn.green{background-color:#5cb85c;color:#fff}

    @media(max-width:585px) {
    .nav-tabs>li a,span.round-tabs{width:50px;height:50px;line-height:50px}
    span.round-tabs{font-size:16px}
    .tab-content .head{font-size:20px}
    li.active:after{content:" ";position:absolute;left:35%}
    .btn-outline-rounded{padding:12px 20px}
}

</style>

<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="widget"><?php echo $menu; ?></div>

<div class="jumbotron">
    <div class="panel-title">
        <div class="pull-right">
            <button type="button" class="btn btn-primary btn-block" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> data-toggle="collapse" data-target="#new_sponsorship" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para crear una nueva solicitud')" <?php } ?> >Nueva solicitud</button>
        </div>
        <div class="pull-right mr-4">
            <a class="btn btn-primary btn-block text-white" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> href="/feria/patrocinadores/crear" target="_blank" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para crear un patrocinador')" <?php } ?> >Nuevo patrocinador</a>
        </div>
        <h2>Patrocinios <span id="count_sponsors"></span></h2>
        <p class="letra-gris" style="margin-bottom: 0px;">Nueva solicitud</p>
    </div>
</div>

<!--Sección Nueva solicitud-->
<div class="widget collapse" id="new_sponsorship">

    <div class="widget-header">
        <h3><i class="fa fa-edit"></i>Nueva solicitud</h3>
    </div>
    <div class="widget-content">
        <form class="form-horizontal" role="form" id="nueva_solicitud" enctype="multipart/form-data">
            <fieldset>
                <legend>{{$nom_feria}}</legend>
                <input type="hidden" value="{{$id_feria}}" name="id_feria" id="id_feria">
                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                <div class="form-group">
                    <label for="ticket-priority" class="col-sm-3 control-label">Patrocinador</label>
                    <div class="col-sm-9" id="nombre_patrocinador_selec">
                        <input list="browsers" name="id_sponsor" id="id_sponsor" class="form-control" style="height: 35px;" placeholder="Escriba el nombre de un patrocinador...">
                        <datalist id="browsers">
                        <?php
                            foreach ($sponsor_data as $row){
                                echo "<option value='$row->id' class='respon'>$row->nombre</option>";
                                }
                            ?>
                      </datalist>
                    </div>
                    <div class="col-sm-9" id="nombre_patrocinador" style="display:none;">
                      <div class="row">
                          <div class="col-md-10">
                              <input type="text" id="nombre_patrocinador_txt" class="form-control" style="cursor:no-drop" readonly value="">
                          </div>
                          <div class="col-md-2">
                              <a class="text-warning" id="btn_edit_patrocinador" style="font-size:2rem;cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Editar Patrocinador"><i class="fa fa-edit"></i></a>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Responsable</label>
                    <div class="col-sm-9">
                        <select id="res_dep" name="res_dep" class="form-control" style="height: 35px;">
                            <option>Seleccione...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Valor</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                            <input type="text" class="form-control" id="valor_patrocinio" onkeyup="formato_numero(this.value,this.id)" placeholder="Valor inicial del patrocinio" name="vlr_inicial">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment_sol" class="col-sm-3 control-label">Comentario:</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" id="comment_sol" name="comment_sol"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="attached_file" class="col-sm-3 control-label">Adjuntar archivo (Opcional)</label>
                    <div class="col-sm-9">
                        <input type="file" class="form-control-file" accept="application/pdf" name="attached_file_sol" id="attached_file_sol">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                        <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#new_sponsorship">Cancelar</button>
                    </div>
                </div>
            </fieldset>
        </form>
        <div class="progress" id="upload_bar_sol">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
        </div>
    </div>
</div>

<!--Board-->
            <div class="board">
                <div class="board-inner">
                    <ul class="nav nav-tabs" id="board_tabs">
                        <div class="liner"></div>
                        <li class="active">
                            <a href="#lista" data-toggle="tab" title="Vista de Lista">
                              <span class="round-tabs one">
                                      <i class="fa fa-list" aria-hidden="true"></i>
                              </span>
                          </a>
                        </li>
                        <li><a href="#board" data-toggle="tab" title="Vista de Gráficos">
                             <span class="round-tabs three">
                                  <i class="fa fa-area-chart" aria-hidden="true"></i>
                             </span> </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content text-center">
                    <div class="tab-pane fade in active" id="lista">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#pendings" role="tab" data-toggle="tab" id="pend_tab">Pendientes</a></li>
                            <li><a href="#approved" role="tab" data-toggle="tab" id="approv_tab">Aprobados</a></li>
                            <li><a href="#cancel" role="tab" data-toggle="tab" id="cancel_tab">Rechazados</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="pendings">
                            </div>
                            <div class="tab-pane fade" id="approved">
                            </div>
                            <div class="tab-pane fade" id="cancel">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="board">
                        <div class="container">
                           <p class="h1">Patrocinios</p>
                            <div class="row mt-5">
                                <div class="col-md-6 col-sm-12">
                                   <div class="container gradient-aguamarina">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 patrocinio">
                                            <h3 class="text-white">Solicitados</h3>
                                            <p class="text-white h1"><?php echo number_format(($datos_board['patrocinio']['left'][0]->p_solicitado / 1000000), 1, ',', '.'); ?>  <small class="text-white">Millones</small></p>
                                            <img src="{{ url('/') }}/images/colombinas.svg" class="pallets-img">
                                        </div>
                                        <div class="col-md-4 col-sm-12 patrocinio">
                                            <h3 class="text-white">Logrados</h3>
                                            <p class="text-white h1"><?php echo number_format(($datos_board['patrocinio']['left'][0]->p_aprobado / 1000000), 1, ',', '.'); ?>  <small class="text-white">Millones</small></p>
                                            <img src="{{ url('/') }}/images/colombinas.svg" class="pallets-img">
                                        </div>
                                        <div class="col-md-4 col-sm-12 patrocinio">
                                            <h3 class="text-white">Efectividad</h3>
                                            <p class="text-white h1"><?php echo number_format(($datos_board['patrocinio']['left'][0]->por_efectividad), 1, ',', '.'); ?>%</p>
                                            <img src="{{ url('/') }}/images/colombinas.svg" class="pallets-img">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                   <div class="container gradient-azul">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 patrocinio">
                                        <h3 class="text-white">Solicitudes</h3>
                                        <p class="text-white h1"><?php echo $datos_board['patrocinio']['right'][0]->cant_solicitudes; ?></p>
                                        <img src="{{ url('/') }}/images/colombinas.svg" class="pallets-img">
                                        </div>
                                        <div class="col-md-4 col-sm-12 patrocinio">
                                        <h3 class="text-white">Aportantes</h3>
                                        <p class="text-white h1"><?php echo $datos_board['patrocinio']['right'][0]->cant_aportantes; ?></p>
                                        <img src="{{ url('/') }}/images/colombinas.svg" class="pallets-img">
                                        </div>
                                        <div class="col-md-4 col-sm-12 patrocinio">
                                        <h3 class="text-white">Efectividad</h3>
                                        <p class="text-white h1"><?php echo number_format(($datos_board['patrocinio']['right'][0]->sol_por_efectividad), 1, ',', '.'); ?>%</p>
                                        <img src="{{ url('/') }}/images/colombinas.svg" class="pallets-img">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <p class="h1">Presupuesto de feria</p>
                            <div class="row budget-wrap mt-5">
                                <div class="col-md-2 col-sm-12">
                                <div class="container budget-item">
                                <div class="row">
                                    <div class="col-sm-12 budget-header p-2"><h3 class="text-white">Gasto Real</h3></div>
                                    <div class="col-sm-12 p-3"><p class="text-white h1 real"><?php echo $datos_board['Patrocinador']['patrocinado'][0]->valor_real ?> <small class="text-white">Millones</small></p></div>
                                </div>
                                </div>
                                </div>
                                <div class="col-md-5 col-sm-12">
                                  <div class="container budget-item">
                                  <div class="row">
                                      <div class="col-sm-12 budget-header p-2">
                                          <div class="row">
                                              <div class="col-sm-6">
                                                  <h3 class="text-white">Proyección</h3>
                                              </div>
                                              <div class="col-sm-6">
                                                  <h3 class="text-white">Cumplimiento</h3>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-12">
                                         <div class="row">
                                              <div class="col-sm-6 p-3">
                                                  <p class="text-white h1"><?php echo $datos_board['presupuesto']['essi'][0]->proyectado; ?> <small class="text-white">Millones</small></p>
                                              </div>
                                              <div class="col-sm-6 p-3">
                                                 <?php
                                                  $real = isset($datos_board['Patrocinador']['patrocinado'][0]->valor_sin_comas_real)?$datos_board['Patrocinador']['patrocinado'][0]->valor_sin_comas_real:0;
                                                  $presupuesto = isset($datos_board['presupuesto']['essi'][0]->valor)?$datos_board['presupuesto']['essi'][0]->valor:0;
                                                  $efectividad_presupuesto = $presupuesto>0? ($real/$presupuesto)*100:0;
                                                  ?>
                                                  <p class="text-white h1"><?php echo number_format($efectividad_presupuesto, 1, ',', '.'); ?>%</p>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  </div>
                                </div>
                                <div class="col-md-5 col-sm-12">
                                  <div class="container budget-item">
                                  <div class="row">
                                      <div class="col-sm-12 p-0 budget-header p-2">
                                          <div class="row">
                                              <div class="col-sm-6 p-0">
                                                  <h3 class="text-white">Patrocinio</h3>
                                              </div>
                                              <div class="col-sm-6 p-0">
                                                  <h3 class="text-white">Patrocinado</h3>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-12 p-0">
                                         <div class="row">
                                              <div class="col-sm-6 p-3">
                                                  <p class="text-white h1">{{$datos_board['Patrocinador']['patrocinio'][0]->patrocinio}} <small class="text-white">Millones</small></p>
                                              </div>
                                              <div class="col-sm-6 p-3">
                                                  <p class="text-white h1">{{$datos_board['Patrocinador']['patrocinado'][0]->patrocinado}}%</p>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-md-6 col-sm-12">
                                <p class="h1">Costos de feria</p>
                                <div id="costo_f" class="charts"></div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                <p class="h1">Patrocinadores de feria</p>
                                <div id="patro_f" class="charts"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- MODAL DIALOG -->
<div class="modal fade" id="action_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Detalles</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="action_frm" enctype="multipart/form-data">
                    <input type="hidden" name="id_req" id="id_req">
                    <input type="hidden" name="action" id="action">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Presupuesto final:</label>
                        <input type="text" class="form-control" placeholder="$" name="total" id="total" onkeyup="formato_numero(this.value,this.id)">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Comentario:</label>
                        <textarea class="form-control" id="message-text" name="comment"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="attached_file">Adjuntar archivo (Opcional)</label>
                        <input type="file" class="form-control-file" accept="application/pdf" name="attached_file" id="attached_file">
                    </div>
                </form>
                <div class="progress" id="upload_bar">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel_action('#action_frm')"><i class="fa fa-times-circle"></i>Cancelar</button>
                <button type="button" class="btn btn-custom-primary" onclick="save_action()"><i class="fa fa-check-circle"></i>Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="doc_modal">
    <div class="modal-dialog modal-lg" id="dialog_mdl">
        <div class="modal-content">
            <iframe id="doc_container"></iframe>
        </div>
    </div>
</div>

<div class="modal fade" id="comments_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Agregar Comentario</h3>
            </div>
            <div class="modal-body">
                <form id="comment_frm" autocomplete="off">
                    <input type="hidden" name="id_req" id="id_req_comment">
                    <input type="hidden" name="action" id="action_comment">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Comentario:</label>
                        <textarea class="form-control" id="message-text" name="comment_body"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel_action('#comment_frm')"><i class="fa fa-times-circle"></i>Cancelar</button>
                <button type="button" class="btn btn-custom-primary" onclick="do_comment()"><i class="fa fa-check-circle"></i>Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="comments_list_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Comentarios</h3>
            </div>
            <div class="comments modal-body" id="comments_body_container">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i>Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DIALOG -->

@endsection @section('scripts')

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery.tinyscrollbar.js"></script>
<?php $colores = array('#558b2f','#FEC514','#DB4C3C','#607d8b'); ?>
<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();
    var chart_1 = AmCharts.makeChart("costo_f", {

        "type": "pie",
        "theme": "black",
        "dataProvider": [ {
            "country": "ESSI",
            "value": <?=($datos_board['Patrocinador']['patrocinado'][0]->valor_sin_comas_real)-($datos_board['Patrocinador']['patrocinio'][0]->valor_total)?>
          },{
            "country": "Patrocinios",
            "value": <?=$datos_board['Patrocinador']['patrocinio'][0]->valor_total?>
          } ],
        "valueField": "value",
        "titleField": "country",
        "outlineAlpha": 0.4,
        "depth3D": 25,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 30,
        "export": {
            "enabled": false
        }
});

    var chart_2 = AmCharts.makeChart("patro_f",
{
    "type": "serial",
    "theme": "black",
    "dataProvider": [
    <?php $i=0; $maximo=0; $color=0; foreach($datos_board['Patrocinador']['lista'] as $lista){ if($color>=4){ $color=0; } if($i>0){ ?> , <?php } ?>
    {
        "name": "<?=$lista->nombre?>",
        "points": <?=$lista->patrocinio_total?>,
        "color": "<?=$colores[$color]?>",
        "bullet": "{{url('/')}}/storage/ferias/patrocinadores/{{$lista->logo}}"
    }
    <?php $color++; $i++; $maximo=$lista->patrocinio_total; } ?>
    ],
    "valueAxes": [{
        "maximum": <?=$maximo?>,
        "minimum": 0,
        "axisAlpha": 0,
        "dashLength": 4,
        "position": "left"
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
        "bulletOffset": 10,
        "bulletSize": 52,
        "colorField": "color",
        "cornerRadiusTop": 8,
        "customBulletField": "bullet",
        "fillAlphas": 0.8,
        "lineAlpha": 0,
        "type": "column",
        "valueField": "points"
    }],
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "categoryField": "name",
    "categoryAxis": {
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "export": {
    	"enabled": false
     }
});






    (function(){
        $('a[title]').tooltip();
    });
    $(document).ready(function() {
        $("#upload_bar").hide();
        $("#upload_bar_sol").hide();
        list_requests();
    });

    $(document).on('click', '.list_comments', function() {
        list_comments_table($(this).data('idr'));
    });

    $(document).on('change', '#id_sponsor', function(){
        if($(this).val() && $(this).val() !== ""){
            list_responsible($(this).val());
        }
    });


    $(document).on('click', '.action_btn', function() {
        var action = $(this).data('action');

        $("#id_req").val($(this).data('idr'));
        $("#action").val($(this).data('action'));
        $('#action_modal').modal('show');

        if (action === 2) {
            $("#total").prop('readonly', true);
            $("#total").attr("placeholder", "0");
        } else {
            $("#total").prop('readonly', false);
            $("#total").attr("placeholder", "");
        }

    });

    /*Control de eventos click*/
    $(document).on('click', '.comment_btn', function() {
        $("#id_req_comment").val($(this).data('idr'));
        $("#action_comment").val($(this).data('action'));
        $('#comments_modal').modal('show');
    });

    $(document).on('click', '#pend_tab', function() {
        list_requests(3, "#pendings")
    });

    $(document).on('click', '#approv_tab', function() {
        list_requests(1, "#approved")
    });

    $(document).on('click', '#cancel_tab', function() {
        list_requests(2, "#cancel")
    });

    /*Nueva solicitud*/
    $(document).on('submit', '#nueva_solicitud', function(event) {

        event.preventDefault();
        var datos = new FormData($("#nueva_solicitud")[0]);
        var archivo = $("#attached_file_sol").val();
        var extensiones = archivo.substring(archivo.lastIndexOf("."));

        if (extensiones != ".pdf" && archivo != "") {
            swal('Sólo se admiten archivos PDF')
        } else {
            if ($("#attached_file_sol").val()) {
                $("#upload_bar_sol").show();
            }
        $.ajax({
            url: '/feria/savesponsorship',
            method: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data: datos,
            success: function(suss) {
                list_requests();
                $("#upload_bar_sol").hide();
                $("#nueva_solicitud")[0].reset();
                swal(
                      'Guardado',
                      'Guardado correctamente!',
                      'success'
                    )
            },
            error: function(err) {
                console.log(err);
            },
            xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    //Upload Progress
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            $('#upload_bar_sol > .progress-bar').css({
                                "width": percentComplete + "%"
                            });
                        }
                    }, false);
                    //Upload progress
                    xhr.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                $("#upload_bar_sol > .progress-bar").css({
                                    "width": percentComplete + "%"
                                });
                            }
                        },
                        false);
                    return xhr;
                }
        });
        }
    });

    /*Listar tabla de comentarios en el modal*/
    function list_comments_table(id) {
        $.ajax({
            url: '/feria/listarcomentarios',
            method: 'POST',
            cache: false,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id_req: id
            },
            success: function(suss) {
                if (suss.res === 1) {
                    $("#comments_body_container").html(suss.data);
                    $("#comments_list_modal").modal('show');
                } else {
                    swal(suss.msj);
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    /*Guardar comentario para la solicitud*/
    function do_comment() {
        $.ajax({
            url: '/feria/accionsolicitud',
            method: 'POST',
            cache: false,
            dataType: 'json',
            data: $("#comment_frm").serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(suss) {
                if (suss.res === 1) {
                    $("#comment_frm")[0].reset();
                    $('#comments_modal').modal('hide');
                    list_requests();
                    swal('Comentario guardado');
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    /*Aprovar o rechazar solicitud*/
    function save_action() {
        var datos = new FormData($("#action_frm")[0]);
        var archivo = $("#attached_file").val();
        var extensiones = archivo.substring(archivo.lastIndexOf("."));

        if (extensiones != ".pdf" && archivo != "") {
            swal('Sólo se admiten archivos PDF')
        } else {
            if ($("#attached_file").val()) {
                $("#upload_bar").show();
            }
            $.ajax({
                url: '/feria/accionsolicitud',
                method: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: datos,
                success: function(suss) {
                    list_requests();
                    $("#upload_bar").hide();
                    $('#action_modal').modal('hide');
                    $("#action_frm")[0].reset();
                    swal(
                      'Guardado',
                      'Guardado correctamente',
                      'success'
                    )
                },
                error: function(err) {
                    console.log(err);
                },
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    //Upload Progress
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            $('#upload_bar > .progress-bar').css({
                                "width": percentComplete + "%"
                            });
                        }
                    }, false);
                    //Upload progress
                    xhr.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                $("#upload_bar > .progress-bar").css({
                                    "width": percentComplete + "%"
                                });
                            }
                        },
                        false);
                    return xhr;
                }
            });
        }
    }

    function cancel_action(selector) {
        $(selector)[0].reset();
    }

    function show_doc(nombre) {
        $('#doc_container').attr('src', '/storage/ferias/solicitud_patrocinio/'+nombre);
        $('#doc_modal').modal('show');

        setTimeout(function(){ $('#doc_container>#download').css({'display':'none'}); }, 3000);
    }

    function list_requests(state = 3, selector = "#pendings") {
        var id = $("#id_feria").val();
        $.ajax({
            url: '/feria/listsponsorship',
            method: 'POST',
            cache: false,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id_feria: id,
                URL: "{{url('/')}}",
                estado: state
            },
            success: function(suss) {
                $(selector).html(suss.cuerpo_tabla);
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    function list_responsible(id){
        if(id){
            $.ajax({
            url: '/feria/patrocinador/listar-responsables',
            method: 'POST',
            cache: false,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id_responsable: id,
            },
            success: function(suss) {
                console.log(suss);
                $("#res_dep").html(suss.options);
            },
            error: function(err) {
                console.log(err);
            }
        });
        }
    }

    $(document).on('change','#id_sponsor',function(){
        var nombre = '';
        switch($(this).val()){
            <?php foreach($sponsor_data as $row){ ?>
            case '<?=$row->id?>':
                nombre = '<?=$row->nombre?>';
                break;
            <?php } ?>
            default:
                nombre='No hay patrocinador';
        }
        $('#nombre_patrocinador_txt').val(nombre);
        $('#nombre_patrocinador_selec').css('display','none');
        $('#nombre_patrocinador').css('display','block');
    });


    $(document).on('click','#btn_edit_patrocinador',function(){
        $('#nombre_patrocinador_selec').css('display','block');
        $('#nombre_patrocinador').css('display','none');
        $('#id_sponsor').val('');
    });
    /**
     * Función para dar formato a la moneda
     */
    function formato_numero(valor,id){
        valor = replaceAll(valor, "$ ", "" );
        if(valor != ""){
           valor = replaceAll(valor, ",", "" );
           valor = parseFloat(valor);

           if(isNaN(valor)){
                $('#'+id).val('$ 0');
            }else{
                valor = number_format(valor,0);
                $('#'+id).val("$ "+valor);
            }
        }else{
           $('#'+id).val('$ 0');
        }
    }
    function number_format(amount, decimals) {
        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }
    function replaceAll( text, busca, reemplaza ){
      while (text.toString().indexOf(busca) != -1)
          text = text.toString().replace(busca,reemplaza);
      return text;
    }

    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>


@endsection
