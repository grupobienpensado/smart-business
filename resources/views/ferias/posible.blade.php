@extends('template.app')
@section('title', 'Ferias posibles')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/components/datatable/css/dataTables.material.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/vergaleria.css">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/listadoposibles.css">

<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">

<input type="hidden" value="{{url('/')}}" id="URL">
<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('calendario/feria') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Calendario de ferias
        </a>
        <a href="{{ url('feriasperdidas') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias perdidas
        </a>
        <a href="{{ url('feriasasistidas') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Ferias asistidas
        </a>
        <a href="{{ url('crearferias') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Crear feria
        </a>
        <a href="{{ url('proveedores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Proveedores
        </a>
        <a href="{{ url('feria/patrocinadores') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Patrocinadores
        </a>
      </div>
      <h2>Listado de ferias posibles</h2>
    </div>
</div>

<div class="card animated slideInDown" style="margin-bottom: 30px;">
<div class="card-block">


    @if(count($datos) > 0)
    <div class="table-responsive" id="lista-mostrar">
      <table id="example" width="100%" class="table table-striped table table-striped table-bordered display">
        <thead>
          <tr>
            <th class="centrado">Item</th>
            <th class="centrado">Logo</th>
            <th class="centrado">Nombre</th>
            <th class="centrado">Ubicacion</th>
            <th class="centrado">Inicio</th>
            <th class="centrado">Fin</th>
            <th class="centrado">Creada</th>
            <th class="centrado">Editada</th>
            <th class="centrado">Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($datos as $dato)
          @php
            if(isset($dato->logo) && !empty($dato->logo)){
              $linkfoto = url('/').'/storage/ferias/'.$dato->logo;
            }else {
              $linkfoto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Logo&w=100&h=100';
            }
            $foto = '<img src="'.$linkfoto.'" style="max-width: 50px;" class="img-fluid img-thumbnail mx-auto d-block">';
          @endphp
          <tr>
            <td class="centrado centrar-vertical"><p class="mas-pequeño"></p></td>
            <td class="centrado centrar-vertical"><?php echo $foto; ?></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->nombre }}</p></td>
            <td class="centrado centrar-vertical"><p class="mas-pequeño">{{ $dato->ubicacion }}</p></td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y", strtotime($dato->fecha_inicio)) }}
              </p>
            </td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y", strtotime($dato->fecha_fin)) }}
              </p>
            </td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->created_at)) }}
              </p>
            </td>
            <td class="centrado centrar-vertical">
              <p class="mas-pequeño">
                  {{ strftime("%d/%m/%Y %l:%M %p", strtotime($dato->updated_at)) }}
              </p>
            </td>
            <td class="text-center">
                <a href="{{ url('cargargaleria') }}/{{ $dato->id }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Cargar Galeria">
                    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                </a>
                <a href="{{ url('editarferias') }}/{{ $dato->id }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editar feria">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="#" class="btn btn-success btn-sm activar-posible" id="posible_{{ $dato->id }}" data-toggle="tooltip" data-placement="top" title="Activar">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                </a>
                <button class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ver feria" onclick="verFeria({{ $dato->id }})">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </button>
            </td>
          </tr>

          @endforeach
        </tbody>
      </table>
    </div>
    @endif
</div>
</div>
<div id="contenedor-exponente">
    <div class="row" id="Exponente">
        <div class="col-md-12 titulo">
            <h2>Datos como Exponente</h2>
        </div>
    </div>
    <div class="container formulario_exponente">
        <form id="formulario_exponente" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id_feria" id="id_feria" >
            <div class="col-md-1">
            </div>
            <div class="col-md-10 crop">
                <div class="dropzone" style="min-width: 1024px !important; max-width: 1024px !important;min-height: 184px !important;max-height: 184px !important;margin-top: 2%;" data-ajax="false" data-originalsave="true">
                  <input type="file" name="baner" accept="image/gif, image/jpeg, image/png" required="required">
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-12">
                <h3>Equipos Exhibidos</h3>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#" class="btn btn-success btn-sm" id="agregar_equipo" data-toggle="tooltip" data-placement="top" title="Agregar Maquina">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar maquina
                        </a>
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" value="" name="referencias" id="referencias_agregadas">
                        <div class="row cont-imagenes">

                        </div>
                    </div>
                </div>
                <hr size="2px" color="black" />
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h6>Cantidad de personas ESSI en feria</h6>
                        <input type="hidden" class="form-control" id="cantidad_personas" name="cantidad_personas" required>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="#" class="btn btn-success btn-sm" id="agregar_invitado" data-toggle="tooltip" data-placement="top" title="Agregar invitado">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar invitado
                                </a>
                            </div>
                        </div>
                        <div class="row listado_funcionarios_invitados">

                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Responsable ESSI</h6>
                        <select class="form-control" name="responsable">
                            <option value="">Seleccione un responsable</option>
                            @foreach($responsables_essi as $r)
                            <option value="{{$r->id}}">{{$r->nombres.' '.$r->apellidos}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Objetivos</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-objetivo" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Objetivo"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Item #
                            </div>
                            <div class="col-md-11">
                                Objetivo
                            </div>
                        </div>
                        <div class="listado-objetivos">
                        <div class="row n_obj" id="n_obj0">
                            <div class="col-md-1 num-objetivo">
                                1
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="objetivos[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Alcances</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-alcance" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Alcance"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Item #
                            </div>
                            <div class="col-md-11">
                                Alcance
                            </div>
                        </div>
                        <div class="listado-alcances">
                        <div class="row n_alc" id="n_alc0">
                            <div class="col-md-1 num-alcance">
                                1
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="alcances[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Estrategias</h6>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-estrategia" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Estrategia"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Item #
                            </div>
                            <div class="col-md-11">
                                Estrategia
                            </div>
                        </div>
                        <div class="listado-estrategias">
                        <div class="row n_est" id="n_est0">
                            <div class="col-md-1 num-estrategia">
                                1
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="estrategia[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Atuento de asistentes</h6>
                        <textarea class="form-control" name="atuendo" rows="8" required></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-12 btn-guardar">
                <a href="#" class="btn btn-success btn-sm btn_guardar_expo" data-toggle="tooltip" data-placement="top" title="Guardar">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                </a>
            </div>
        </form>
    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="{{ url('/') }}/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;"/>
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="documento" style="z-index: 3040" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <iframe id="ver_pdf" src="http://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/ferias/listaposible.js"></script>
    <script src="{{ url('/') }}/js/jquery.blueimp-gallery.min.js"></script>
    <script src="{{url('/')}}/js/parsley.min.js"></script>

    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/components/datatable/js/dataTables.material.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>

@endsection
