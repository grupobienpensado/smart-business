@extends('template.app')
@section('title', 'Calendario de Ferias')

@section('content')
<?php
// Definimos nuestra zona horaria
date_default_timezone_set("America/Santiago");

// incluimos el archivo de funciones
    // Evaluar los datos que ingresa el usuario y eliminamos caracteres no deseados.
    function evaluar($valor)
    {
        $nopermitido = array("'",'\\','<','>',"\"");
        $valor = str_replace($nopermitido, "", $valor);
        return $valor;
    }

    // Formatear una fecha a microtime para añadir al evento, tipo 1401517498985.
    function _formatear($fecha)
    {
        return strtotime(substr($fecha, 6, 4)."-".substr($fecha, 3, 2)."-".substr($fecha, 0, 2)." " .substr($fecha, 10, 6)) * 1000;
    }

// incluimos el archivo de configuracion
    // Datos de conexion a la base de datos
    $servidor='localhost';
    $usuario='smartsis';
    $pass='CvIIYS3p7p';
    $bd='smartbusiness';


    // Nos conectamos a la base de datos
    $conexion = new mysqli($servidor, $usuario, $pass, $bd);

    // Definimos que nuestros datos vengan en utf8
    $conexion->set_charset('utf8');

    // verificamos si hubo algun error y lo mostramos
    if ($conexion->connect_errno) {
        echo "Error al conectar la base de datos {$conexion->connect_errno}";
    }

    // Url donde estara el proyecto, debe terminar con un "/" al final
    $base_url=url('/')."/calendario/";
?>
<style>
    .dh-no-asistida{
        background-color: #D95350 !important;
        color: #fff !important;
    }
    .no-asistida{
        background-color: #D95350 !important;
    }
    .dh-futura{
        background-color: #D4AC0D !important;
        color: #fff !important;
    }
    .futura{
        background-color: #D4AC0D !important;
    }
    .dh-exponente{
       background-color: #16A085 !important;
        color: #fff !important;
    }
    .exponente{
       background-color: #16A085 !important;
    }
    .dh-visitante{
        background-color: #016090 !important;
        color: #fff !important;
    }
    .visitante{
        background-color: #016090 !important;
    }
    .list-unstyled li{
        text-align: left !important;
    }
    .titulo {
        font-weight: bold !important;
        color: #051d60 !important;
        font-size: 2.5rem !important;
    }
    .list-unstyled li a{
        font-size: x-large !important;
    }
    .position-left{
        position: absolute;
        left: 30px;
    }
    #cal-slide-content a.event-item { font-weight: 100 !important; }
    .list-unstyled li a { font-size: initial !important; }
</style>
<head>
    <meta charset="utf-8">
    <title>Calendario</title>
    <link rel="stylesheet" href="{{url('/')}}/css/ferias/botonessuperioresferias.css" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$base_url?>css/calendar.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?=$base_url?>js/es-ES.js"></script>
    <script src="<?=$base_url?>js/jquery.min.js"></script>
    <script src="<?=$base_url?>js/moment.js"></script>
    <script src="<?=$base_url?>js/bootstrap.min.js"></script>
    <script src="<?=$base_url?>js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="<?=$base_url?>css/bootstrap-datetimepicker.min.css" />
   <script src="<?=$base_url?>js/bootstrap-datetimepicker.es.js"></script>
</head>

<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{url('feria/patrocinadores')}}" class="btn btn-sm btn-patrocinador">
            Patrocinadores | {{$boton['patrocinadores']}}
        </a>
        <a href="{{url('proveedores')}}" class="btn btn-sm btn-proveedor">
            Proveedores | {{$boton['proveedores']}}
        </a>
        <a href="{{url('ferias/menu')}}" class="btn btn-sm btn-feria">
            Feria
        </a>
      </div>
      <h2>Calendario</h2>
    </div>
</div>

<div class="container" style="background-color:#fff;">

    <div class="row">
            <div class="col-md-2">
                <div class="pull-left">
                    <div style="width: 10px;height: 30px;background: #D95350;"><span class="position-left">No asistida</span></div>
                    <div style="width: 10px;height: 30px;background: #D4AC0D;"><span class="position-left">Futura</span></div>
                    <div style="width: 10px;height: 30px;background: #16A085;"><span class="position-left">ESSI Exponente</span></div>
                    <div style="width: 10px;height: 30px;background: #016090;"><span class="position-left">ESSI Visitante</span></div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="page-header"><h2></h2></div>
                <div class="pull-left form-inline"><br>
                    <div class="btn-group">
                        <button class="btn btn-primary" data-calendar-nav="prev"><< Anterior</button>
                        <button class="btn" data-calendar-nav="today">Hoy</button>
                        <button class="btn btn-primary" data-calendar-nav="next">Siguiente >></button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-primary active" data-calendar-view="year">Año</button>
                        <button class="btn btn-primary" data-calendar-view="month">Mes</button>
                        <button class="btn btn-primary" data-calendar-view="week">Semana</button>
                    </div>
                </div>
            </div>
    </div><hr>

    <div class="row">
            <div id="calendar"></div> <!-- Aqui se mostrara nuestro calendario -->
            <br><br>
    </div>

    <!--ventana modal para el calendario-->
    <div class="modal fade" id="events-modal">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-body" style="height: 400px">
                            <p>One fine body&hellip;</p>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cerrar-modal" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="<?=$base_url?>js/underscore-min.js"></script>
<script src="<?=$base_url?>js/calendar.js"></script>
<script type="text/javascript">
    (function($) {
        //creamos la fecha actual
        var date = new Date();
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth() + 1).toString().length == 1 ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
        var dd = (date.getDate()).toString().length == 1 ? "0" + (date.getDate()).toString() : (date.getDate()).toString();
        //establecemos los valores del calendario
        var options = {
            // definimos que los eventos se mostraran en ventana modal
            /*modal: '#events-modal',*/
            // dentro de un iframe
            modal_type: 'iframe',
            //obtenemos los eventos de la base de datos
            events_source: '<?=$base_url?>obtener_eventos.php',
            // mostramos el calendario en el mes
            view: 'year',
            // y dia actual
            day: yyyy + "-" + mm + "-" + dd,
            // definimos el idioma por defecto
            language: 'es-ES',
            //Template de nuestro calendario
            tmpl_path: '<?=$base_url?>tmpls/',
            tmpl_cache: false,
            // Hora de inicio
            time_start: '00:00',
            // y Hora final de cada dia
            time_end: '23:00',
            // intervalo de tiempo entre las hora, en este caso son 30 minutos
            time_split: '30',
            // Definimos un ancho del 100% a nuestro calendario
            width: '100%',
            /*onAfterEventsLoad: function(events) {
                if (!events) {
                    return;
                }
                var list = $('#eventlist');
                list.html('');

                $.each(events, function(key, val) {
                    $(document.createElement('li'))
                        .html('<a href="' + val.url + '">' + val.nombre + '</a>')
                        .appendTo(list);
                });
            },*/
            onAfterViewLoad: function(view) {
                $('.page-header h2').text(this.getTitle());
                $('.btn-group button').removeClass('active');
                $('button[data-calendar-view="' + view + '"]').addClass('active');
            },
            classes: {
                months: {
                    general: 'label'
                }
            }
        };


        // id del div donde se mostrara el calendario
        var calendar = $('#calendar').calendar(options);

        $('.btn-group button[data-calendar-nav]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.navigate($this.data('calendar-nav'));
            });
        });

        $('.btn-group button[data-calendar-view]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.view($this.data('calendar-view'));
            });
        });

        $('#first_day').change(function() {
            var value = $(this).val();
            value = value.length ? parseInt(value) : null;
            calendar.setOptions({
                first_day: value
            });
            calendar.view();
        });
    }(jQuery));

    function ver_feria(id) {
        alert(id);
    }
</script>
@endsection
