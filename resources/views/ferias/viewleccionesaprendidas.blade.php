@extends('template.app')
@section('title', 'Lecciones Aprendidas')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/viewleccionesaprendidas.css">
<div class="container">
    <div class="row bg-white">
        <?=$data['menu']?>
    </div>
    <div class="row">
        <div class="col-md-12 down" id="menu-interno">
            <ul>
                <li id="preguntas">
                    <a href="#" title="Preguntas">
                        <div id="fb">
                            <div class="layer2">
                                <i class="fa fa-question-circle question" aria-hidden="true"></i>
                                <div class="nb">{{$data['respuestas']}}</div>
                            </div>
                            <div class="textlink">{{$data['respuestas']}}/8 Respuestas </div>
                        </div>
                    </a>
                </li>

                <li id="archivos">
                    <a href="#" title="">
                        <div id="tw">
                            <div class="layer2">
                                <i class="fa fa-file-pdf-o file-upload" aria-hidden="true"></i>
                                <div class="nb">{{$data['archivos']}}</div>
                            </div>
                            <div class="textlink">{{$data['archivos']}} Archivo(s) </div>
                        </div>
                    </a>
                </li>

                <li id="comentarios">
                    <a href="#" title="">
                        <div id="insta">
                            <div class="layer2">
                                <i class="fa fa-commenting commentary" aria-hidden="true"></i>
                                <div class="nb">{{$data['comentarios']}}</div>
                            </div>
                            <div class="textlink">{{$data['comentarios']}} Comentario(s)</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    {{ csrf_field() }}
    <div class="row" id="list">

    </div>
</div>

<!--Visualizador PDF -->

<div class="modal fade bd-example-modal-lg" id="documento" style="z-index: 3040" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 1030px">
    <div class="modal-content" style="margin-top:3%; height: 700px;">
      <iframe id="ver_pdf" src="http://docs.google.com/gview?url={{url('/')}}/storage/ferias/multimedia/1509743094___PropuestacomercialANDINAPACK.pdf&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script src="{{url('/')}}/assets/js/plugins/dropzone/dropzone.min.js"></script>
<script>
    $(document).on('click','#preguntas',function(){
        closelist();
        $('#menu-interno').removeClass('down');
        $('#menu-interno').addClass('up');
        setTimeout(function(){ $("#list").html('<div class="col-md-12 mt-3 ml-5 mr-5 text-center"><img src="{{url("/")}}/images/configuracion_crm/configuracion.gif"></div>'); listquestion(); }, 1500);
    });

    /**
     * Funcion para cerrar el div de lista con una transición lenta
     */
    function closelist(){
        $('#list').hide('slow');
         setTimeout(function(){ $('#list').html(''); $('#list').show(); },1000);
    }

    /**
     * [[Con esta funcion se listan todos las preguntas de la feria]]
     */
    function listquestion(){
        $.ajax({
            url: "{{url('/')}}/consultarlecciones/{{$data['id']}}",
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#list').html(e.content);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    $(document).on('click','.cerrar',function(){
        $("#list").html('');
        $('#menu-interno').removeClass('up').addClass('down');
        $('.tooltip').removeClass('show');
    });

    /**
     * Se edita la respuesta ya almacenada
     * @param {number} id [[Identificación de la feria]]
     */
    function editar_pregunta(id){
        $.ajax({
            url: "{{url('/')}}/consultarrespuesta/"+id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                swal({
                      title: 'Editar la respuesta',
                      type: 'info',
                      html:e.data['content'],
                      showCloseButton: true,
                      showCancelButton: true,
                      focusConfirm: false,
                      confirmButtonText:'Guardar',
                      cancelButtonText:'Cancelar'
                    }).then(function(){
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "{{url('/')}}/guardarrespuesta",
                        data: {
                            _token: CSRF_TOKEN,
                            tipo: "editar",
                            respuesta: $('#respuesta').val(),
                            id: id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal(
                                  'Guardado',
                                  e.msj,
                                  'success'
                                ).then(function(){
                                    listquestion();
                                    $('[data-toggle="tooltip"]').tooltip();
                            });
                        }
                    });
                });
            }
        });
    }

    /**
     * Se crea la respuesta
     * @param {number} id [[Identificación de la feria]]
     */
    function crear_pregunta(id){

    swal({
          title: 'Editar la respuesta',
          type: 'info',
          html:`<textarea class="form-control" rows="8" id="respuesta"></textarea>`,
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:'Guardar',
          cancelButtonText:'Cancelar'
        }).then(function(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/guardarrespuesta",
            data: {
                _token: CSRF_TOKEN,
                tipo: "crear",
                respuesta: $('#respuesta').val(),
                id_feria: "{{$data['id']}}",
                pregunta: id
            },
            cache: false,
            type: 'POST',
            success: function(e){
                swal(
                      'Guardado',
                      e.msj,
                      'success'
                    ).then(function(){
                        listquestion();
                        $('[data-toggle="tooltip"]').tooltip();
                });
            }
        });
    });
}

    $(document).on('click','#archivos',function(){
        closelist();
        $('#menu-interno').removeClass('down');
        $('#menu-interno').addClass('up');
        setTimeout(function(){ $("#list").html('<div class="col-md-12 mt-3 ml-5 mr-5 text-center"><img src="{{url("/")}}/images/configuracion_crm/configuracion.gif"></div>'); listdocumet(); }, 1500);
    });


    function listdocumet(){
        $.ajax({
            url: "{{url('/')}}/consultardocumentoslecciones/{{$data['id']}}",
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#list').html(e.data['content']);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    function subirarchivo(id){
        swal({
              title: 'Subir archivos',
              type: 'info',
              html:`<!-- FILE UPLOAD -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-cloud-upload"></i> Cargar</h3></div>
                        <div class="widget-content">

                            <form class="dropzone" action="{{url('/')}}/cargararchivolecciones"  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$data['id']}}">
                            </form>
                        </div>
                    </div>
                    <!-- END FILE UPLOAD -->`,
              allowOutsideClick: false,
              showCloseButton: false,
              showCancelButton: false,
              focusConfirm: false,
              confirmButtonText:'cerrar'
            }).then(function(){
                listdocumet();
        });
        $('.swal2-show').css('width','90%');
        rundropzone();
    }

    function rundropzone(){
        // if dropzone exist
        if( $('.dropzone').length > 0 ) {
            Dropzone.autoDiscover = false;

            $(".dropzone").dropzone({
                url: "{{url('/')}}/cargararchivolecciones",
                addRemoveLinks : true,
                maxFilesize: 30,
                maxFiles: 50,
                acceptedFiles: 'application/pdf',
                dictResponseError: 'File Upload Error.'
            });
        } // end if dropzone exist
    }

    function eliminar_archivo(id){
        swal({
          title: 'Esta seguro?',
          text: "Va a eliminar un archivo",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!',
          cancelButtonText:'Cerrar'
        }).then(function(){
            $.ajax({
                url: "{{url('/')}}/eliminararchivolecciones/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal(
                          'Eliminado!',
                          e.msj,
                          'success'
                        );
                    listdocumet();
                }
            });

        });
    }

    function ver_archivo(url){
        $('#ver_pdf').attr('src',url);
        $('#documento').modal('show');
    }

    $(document).on('click','#comentarios',function(){
        closelist();
        $('#menu-interno').removeClass('down');
        $('#menu-interno').addClass('up');
        setTimeout(function(){ $("#list").html('<div class="col-md-12 mt-3 ml-5 mr-5 text-center"><img src="{{url("/")}}/images/configuracion_crm/configuracion.gif"></div>'); listcoment(); }, 1500);
    });

    function listcoment(){
        $.ajax({
            url: "{{url('/')}}/consultarcomentarios/{{$data['id']}}",
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#list').html(e.content);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    function do_comments(){
        comentario = $('#chat_box').val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/comentarlecciones",
            data: {
                _token: CSRF_TOKEN,
                id_user: "{{Auth::user()->id}}",
                comentario: comentario,
                id_feria: "{{$data['id']}}"
            },
            cache: false,
            type: 'POST',
            success: function(e){
                listcoment();
                $('#chat_box').val('');
            }
        });
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>

@endsection
