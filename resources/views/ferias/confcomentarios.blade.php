@extends('template.app')
@section('title', 'Comentarios Presupuesto Conferencia')

@section('content')
<link href="{{ url('/') }}/css/ferias/verpatrocinador.css" rel="stylesheet" type="text/css">
<div class="content" style="background-color: #fff;">
    <?=$data['menu']?>
    <div class="main-header mt-5">
        <h2>Conferencia</h2>
        <em>Comentarios</em>
    </div>
    {{ csrf_field() }}
    <div class="container">
        <div class="row">
            <div class="comments col-md-12 pt-5" id="caja_comentarios">

            </div>
            <div class="col-md-12 pt-4">
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url('<?php echo url('/').'/images/file/clientes/'.Auth::user()->foto; ?>')"></div>
                    </div>
                    <div class="comment-block">
                        <form autocomplete="off" id="messages_box">
                           <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..."></textarea>
                        </form>
                        <div class="pull-right">
                        <button type="button" class="btn btn-success" onclick="do_comments()" id="send_btn">Enviar <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        listarcomentarios();
    });
    function listarcomentarios(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/listarcomentariosconferencia",
            data: {
                _token: CSRF_TOKEN,
                id: "{{$data['id']}}",
                url_basico: "{{url('/')}}"
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#caja_comentarios').html(e.content);
            }
        });
    }

    function do_comments(){
        comentario = $('#chat_box').val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/comentarconferencia",
            data: {
                _token: CSRF_TOKEN,
                id_user: "{{Auth::user()->id}}",
                comentario: comentario,
                id_feria: "{{$data['id']}}"
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#caja_comentarios').html(e.content);
            }
        });
        listarcomentarios();
        $('#chat_box').val('');
    }
</script>
@endsection
