@extends('template.app')
@section('title', 'Crear Presupuesto')

@section('content')
<link href="{{ url('/') }}/css/ferias/presupuesto.css" rel="stylesheet">
<input type="hidden" value="{{url('/')}}" id="URL">
<div class="container">
<div class="row bg-white">
    <div class="col-md-12 mb-5">
        <?php echo $menu; ?>
    </div>
</div>

</div>
<div class="content bg-white">
    <div class="main-header">
        <h2>Presupuesto</h2>
        <em>{{$dato->nombre}}</em>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="top-content">
                <ul class="list-inline quick-access">
                    <li>
                        <a href="{{url('/')}}/presupuestoconferencia">
                            <div class="quick-access-item bg-color-blue">
                                <i class="fa fa-envelope"></i>
                                <h5>NOTA</h5><em>Los datos se almacenan automaticamente</em>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a <?php if($data['permiso_agregar_editar_eliminar'] == "Si"){ ?> href="/presupuestoadministrable" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para crear items del presupuesto')" <?php } ?> >
                            <div class="quick-access-item bg-color-orange">
                                <i class="fa fa-table"></i>
                                <h5>Crear</h5><em>Haz clic aquí para crear un nuevo ítem de presupuesto</em>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/compararpresupuesto/{{$dato->id}}">
                            <div class="quick-access-item bg-color-red">
                                <i class="fa fa-money"></i>
                                <h5>Compara</h5><em>Haz clic aquí para Comparar real con presupuesto</em>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Formulario de Presupuesto</h3></div>
        <div class="widget-content">
            <ul class="list-unstyled activity-list">
                <li>
                    <i class="fa fa-bell-o activity-icon pull-left"></i>
                    <p class="text-left">
                        <a href="#">{{$data['nombre']}}</a> Recuerda que el presupuesto se debe llenar hasta del día <a href="#">{{$data['fecha_limite_presupuesto']}}</a> <span class="timestamp">@if(isset($data['vencio_presupuesto'])) Vencio hace {{$data['vencio_presupuesto']}} dias @else No ha vencido @endif</span>
                    </p>
                </li>
            </ul>
            <form class="form-horizontal" role="form" id="formulario_basico" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$dato->id}}">
                @foreach($conceptos as $concepto)
                <input type="hidden" value="{{$concepto->id}}" name="presupuesto[{{$i}}][id_concepto]">
                <div class="row">
                    <div style="width: 2%;">
                        {{$i+1}}.
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="concepto{{$i}}" class="control-label sr-only">{{$concepto->concepto}}</label>
                            <input type="text" class="form-control detalle" name="presupuesto[{{$i}}][concepto]" id="concepto{{$i}}" value="{{$concepto->concepto}}" placeholder="Concepto" readonly>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="valor" class="control-label sr-only">Valor</label>
                            <input type="text" class="form-control valor" name="presupuesto[{{$i}}][valor]" value="{{$concepto->valor}}" id="valor" placeholder="Valor" <?php if($data['permiso_presupuesto'] == "No" || $data['permiso_agregar_editar_eliminar'] == "No"){ ?> readonly style="cursor: no-drop" onclick="no_permiso('Usted no tiene permisos para editar presupuesto')" <?php } ?> >
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
                @endforeach
                <div class="row">
                    <div class="col-md-9 txt-total">
                        <h3><strong>Total</strong></h3>
                    </div>
                    <div class="col-md-3">
                        <h3><strong id="total">$ {{$total}}</strong></h3>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="alert alert-success"><strong>¡Guardado!</strong><br> Se ha guardado correctamente.</div>
@endsection

@section('scripts')
<script src="{{ url('/') }}/js/ferias/presupuesto.js"></script>

@endsection
