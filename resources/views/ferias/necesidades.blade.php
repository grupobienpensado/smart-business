@extends('template.app')
@section('title', 'Ferias perdidas')

@section('content')
<style>
    .card-header>h4.text-left{display:inline-block}
    .circle-flag{width:35px;height:35px}
    .circle-flag i{margin-top:1.1rem}
    .cc-selector{display:table;margin:0 auto}
    .cc-selector input{margin:0;padding:0;-webkit-appearance:none;-moz-appearance:none;appearance:none}
    .cc-selector input:active+.drinkcard-cc{opacity:.9}
    .cc-selector input:checked+.drinkcard-cc{-webkit-filter:none;-moz-filter:none;filter:none}
    .drinkcard-cc{cursor:pointer;background-size:contain;background-repeat:no-repeat;display:inline-block;width:14rem;height:14rem;-webkit-transition:all 100ms ease-in;-moz-transition:all 100ms ease-in;transition:all 100ms ease-in;-webkit-filter:brightness(1.8) grayscale(1) opacity(.7);-moz-filter:brightness(1.8) grayscale(1) opacity(.7);filter:brightness(1.8) grayscale(1) opacity(.7)}
    .drinkcard-cc:hover{-webkit-filter:brightness(1.1) grayscale(.5) opacity(.9);-moz-filter:brightness(1.1) grayscale(.5) opacity(.9);filter:brightness(1.1) grayscale(.5) opacity(.9)}
    .bg-white{background-color:#FFF}
    .comment-wrap #messages_box textarea{font-size:1.5rem;outline:none;border:none;display:block;margin:0;padding:0;-webkit-font-smoothing:antialiased;font-family:"PT Sans","Helvetica Neue","Helvetica","Roboto","Arial",sans-serif;color:#555f77}
    .comment-wrap #messages_box textarea::-webkit-input-placeholder{color:#ced2db}
    .comment-wrap #messages_box textarea::-moz-placeholder{color:#ced2db}
    .comment-wrap #messages_box textarea:-moz-placeholder{color:#ced2db}
    .comment-wrap #messages_box textarea:-ms-input-placeholder{color:#ced2db}
    .comments{overflow-y:scroll;background-color:#f0f2fa;-webkit-font-smoothing:antialiased;width:100%;max-height:600px;padding-bottom:1rem;padding-top:1rem}
    .comment-wrap{margin-bottom:1.25rem;display:table;width:100%;min-height:5.3125rem}
    .photo{padding:1.5rem;display:table-cell;width:3.5rem}
    .photo .avatar{height:6.25rem;width:6.25rem;border-radius:50%;background-size:cover}
    .comment-block{padding:1rem;background-color:#fff;display:table-cell;vertical-align:top;border-radius:.1875rem;box-shadow:0 1px 3px 0 rgba(0,0,0,0.08)}
    .comment-block textarea{width:100%;resize:none}
    .comment-text{font-size:1.5rem;margin-bottom:1.25rem;color:#000;font-weight:400}
    .bottom-comment{color:#acb4c2}
    .comment-date{float:left}
    .comment-actions{float:right}
    .comment-actions li{display:inline;margin:-2px;cursor:pointer}
    .comment-actions li.complain{padding-right:.75rem;border-right:1px solid #e1e5eb}
    .comment-actions li.reply{padding-left:.75rem;padding-right:.125rem}
    .comment-actions li:hover{color:#0095ff}
    .jumbotron{ padding-top: 0px !important;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="jumbotron">
    <div class="row bg-white">
        <?=$data['menu']?>
    </div>
    <div class="panel-title">
        <div class="pull-right">
            <a href="#" class="btn btn-primary btn-sm" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?> data-toggle="modal" data-target="#new_cot_mdl" <?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para crear una nueva cotización')" <?php } ?>>
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Nueva cotización
        </a>
        </div>
        <h2>Necesidades</h2>
        <h4 class="text-muted text-capitalize">Crear cotizaciones</h4>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-5">
        <div class="container bg-white rounded p-4" id="lista_necesidades">

        </div>
        </div>
        <div class="col-md-7">
        <div class="container bg-white rounded">
        <div class="row p-4">
            <div class="comments col-md-12" id="caja_comentarios">
            </div>
            <div class="col-md-12 mt-4 pr-0">
                <div class="comment-wrap">
                    <div class="photo">
                        <div class="avatar" style="background-image: url(<?php echo $data['user_img']; ?>)"></div>
                    </div>
                    <div class="comment-block">
                        <form autocomplete="off" id="messages_box">
                          <input type="hidden" value="" id="need_id_comments">
                           <textarea name="message" id="chat_box" cols="30" rows="3" placeholder="Agregar comentario..."></textarea>
                        </form>
                        <div class="pull-right">
                        <button type="button" class="btn btn-success" id="enviar_btn" onclick="needs_comments()">Enviar <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="new_cot_mdl" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nueva necesidad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">


                <form id="necesidades_frm">
                    <input type="hidden" value="{{ $data['id_feria'] }}" name="id_feria">
                    <div class="form-group">
                        <h4>Título</h4>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Ej: Cotización máquina x">
                    </div>
                    <div class="form-group">
                        <h4>Presupuesto</h4>
                        <input type="number" class="form-control" id="cantidad" name="cantidad" placeholder="Ej: 10'000,000">
                    </div>
                    <div class="form-group">
                        <h4>Proveedores posibles</h4>
                        <select id="lista_proveedores" name="proveedores[]" multiple="multiple">
                    </select>
                    </div>
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn-cancel btn btn-secondary" data-dismiss="modal" onclick="CancelarNecesidades()">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="GuardarNecesidades()">Guardar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>
<script>
    $(document).ready(function() {
        ListarProveedores();
        ListarNecesidades();

        $(document).on('submit', '.resultado-proveedor-frm', function(event){
            event.preventDefault();
            var form = $(this);
            if (form.parsley().validate()) {
                swal({
                    title: "Atención",
                    text: "¿Desea guardar el resultado?",
                    type: "question",
                    showCancelButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: "Cancelar",
                    cancelButtonColor: '#d33'
                }).then(
                    function() {
                        GuardarProveedorSelecionado(form.serialize());
                    },
                    function() {
                        return false;
                    });
            }
        });

        /**
         * Prevenir el envío de selección del proveedor final, confirmación
         */
        $(document).on('submit', '.select-proveedor-frm', function(event) {
            event.preventDefault();
            var form = $(this);
            if (form.parsley().validate()) {
                swal({
                    title: "Atención",
                    text: "¿Desea guardar la selección?",
                    type: "question",
                    showCancelButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: "Cancelar",
                    cancelButtonColor: '#d33'
                }).then(
                    function() {
                        GuardarProveedorSelecionado(form.serialize());
                    },
                    function() {
                        return false;
                    });
            }
        });

        $(document).on('click', '.needs-accordion', function(){
            $("#need_id_comments").val($(this).data('id'));
            list_comments($(this).data('id'));
        });

    });


    /**
     * Guardar necesidades
     */
    function GuardarNecesidades() {
        $.ajax({
            url: '{{ url("/") }}/feria/necesidades/guardar',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: $("#necesidades_frm").serialize(),
            success: function(suss) {
                console.log(suss);
                if (suss.res) {
                    CancelarNecesidades();
                    ListarNecesidades();
                    $('#new_cot_mdl').modal('hide');
                    swal(
                        'Crear necesidad',
                        suss.msj,
                        'success'
                    );
                }
            },
            error: function(err) {
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /**
     * Acciones del botón cancelar
     */
    function CancelarNecesidades() {
        $("#necesidades_frm")[0].reset();
        $('#lista_proveedores').val(null).trigger('change');
    }

    /**
     * Listar proveedores para select2
     * @returns {object|string} devuelve las options para el input
     */
    function ListarProveedores() {
        $("#lista_proveedores").select2({
            ajax: {
                url: '{{ url("/") }}/proveedores/todos',
                method: 'POST',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term // search term
                    };
                },
                processResults: function(data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.nombre,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            minimumInputLength: 2,
            language: {
                inputTooShort: function() {
                    return 'Por favor, introduzca 2 caracteres mínimo';
                }
            },
            placeholder: "Ej: Corferias",
            allowClear: true
        });
    }

    /**
     * Listar necesidades, desplegar los collapsibles
     */
    function ListarNecesidades() {
        $.ajax({
            url: '{{ url("/") }}/feria/necesidades/listar',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(suss) {
                if (suss.res) {
                    $("#lista_necesidades").html(suss.data.necesidades);
                    $('[data-toggle="tooltip"]').tooltip();
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    /**
     * Guardar la selección posterior del proveedor
     * @param {string} data = null formulario serializado
     */
    function GuardarProveedorSelecionado(data = null) {
        $.ajax({
            url: '{{ url("/") }}/feria/necesidades/guardar-seleccion',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(suss) {
                if(suss.res) {
                    ListarNecesidades();
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    /**
     * Función de la caja de comentarios
     */
    function needs_comments() {
        var id_need = $("#need_id_comments").val();
        $.ajax({
            url: "{{ url('/') }}/feria/necesidades/comentar",
            cache: false,
            method: "POST",
            timeout: 10000,
            beforeSend: function() {
                $("#enviar_btn").prop('disabled', true);
                $("#chat_box").val('');
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                comentario: $("#chat_box").val(),
                id_need: id_need
            },
            success: function(suss) {
                if (suss.res) {
                    $("#enviar_btn").prop('disabled', false);
                    list_comments(id_need);
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }

    /**
     * Listar los comentarios
     * @param {number} id ID de la necesidad para listar sus comentarios
     */
    function list_comments(id) {
        $.ajax({
            url: "{{ url('/') }}/feria/necesidades/comentarios",
            cache: false,
            method: "POST",
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id_need: parseInt(id)
            },
            success: function(suss) {
                if (suss.res) {
                    $("#caja_comentarios").html(suss.data.comentarios);
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>

@endsection
