@extends('template.app')
@section('title', 'Presupuesto Administrable')

@section('content')
<link href="{{ url('/') }}/css/ferias/presupuesto.css" rel="stylesheet">
<input type="hidden" value="{{url('/')}}" id="URL">
<div class="content">
    <div class="main-header">
        <h2>Presupuesto</h2>
        <em>Items de presupuesto administrable</em>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="top-content">
                <ul class="list-inline quick-access">
                    <li>
                        <a href="#">
                            <div class="quick-access-item bg-color-blue">
                                <i class="fa fa-envelope"></i>
                                <h5>NOTA</h5><em>Los datos se almacenan automaticamente</em>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:history.back()">
                            <div class="quick-access-item bg-color-orange">
                                <i class="fa fa-table"></i>
                                <h5>Listado</h5><em>Haz clic aquí para regresar al listado de ferias asistidas</em>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Crear Items de presupuesto</h3>
        </div>
        <div class="widget-content">
            <a class="crearitem" data-toggle="tooltip" data-placement="top" title="Crear Item"><i class="fa fa-plus-circle agregar" aria-hidden="true"></i></a>
            <form class="form-horizontal" role="form" id="formulario_basico" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                @foreach($datos as $dato)
                <input type="hidden" name="presupuesto[{{$i}}][id]" value="{{$dato->id}}">
                <div class="row">
                    <div style="width: 2%;">
                        {{$i+1}}.
                    </div>
                    <div class="col-sm-11">
                        <div class="form-group">
                            <label for="concepto{{$i}}" class="control-label sr-only">{{$dato->concepto}}</label>
                            <input type="text" class="form-control detalle" name="presupuesto[{{$i}}][concepto]" id="concepto{{$i}}" value="{{$dato->concepto}}" placeholder="Concepto">
                        </div>
                    </div>
                    <i class="fa fa-trash eliminar" id="eliminar_{{$dato->id}}" aria-hidden="true"></i>
                </div>
                @php $i++; @endphp
                @endforeach
                @if(count($datos) == 0)
                <h3>No hay Registros</h3>
                @endif
                <input type="hidden" value="{{$i}}" id="filas">
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ url('/') }}/js/ferias/presupuestoadministrable.js"></script>
@endsection
