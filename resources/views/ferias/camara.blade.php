@extends('template.app')
@section('title', 'Calendario de Ferias')

@section('content')
  <style>
      .embed-container {
            position: relative;
            padding-bottom: 56.25%;
            height: 0;
            overflow: hidden;
        }
        .embed-container iframe {
            position: absolute;
            top:0;
            left: 0;
            width: 100%;
            height: 100%;
        }
      .jumbotron{ padding-top: 0px !important;}
  </style>
  <div class="jumbotron">
      <div class="row bg-white">
        <?=$data['menu']?>
    </div>
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{ url('feria/visualizar/camara') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            Visualizar Camara
        </a>
        <a href="{{ url('feria/trasmitir/camara') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-list" aria-hidden="true"></i>
            Emitir Trasmisión
        </a>
        </div>
      </div>
    </div>
   <div class="embed-container">
        <iframe width="560" height="315" src="//:300/visualizar.html" frameborder="0" allowfullscreen></iframe>
    </div>
@endsection
@section('scripts')
@endsection
