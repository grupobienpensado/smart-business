@extends('template.app')
@section('title', 'Crear patrocinador')

@section('content')
<link href="{{ url('/') }}/css/ferias/crearpatrocinador.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<input type="hidden" id="URL" value="{{url('/')}}">
<div class="main-header">
    <h2>Patrocinador</h2>
    <em>Crear Patrocinador</em>
</div>
<div class="widget">
    <div class="widget-header">
        <h3><i class="fa fa-edit"></i> Editar Patrocinador</h3>
    </div>
    <div class="widget-content">
        <form id="formulario_patrocinador_editar" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$dato->id}}">
        <div class="row">
            <div class="col-md-12">
                <div class="dropzone logo" style="background-image: url('{{url('/')}}/storage/ferias/patrocinadores/{{$dato->logo}}')" data-ajax="false" data-originalsave="true">
                  <input type="file" name="logo" accept="image/gif, image/jpeg, image/png">
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 izquierda">
                <label>Nombre </label>
                <input type="text" name="formulario[nombre]" value="{{$dato->nombre}}" class="form-control" >
            </div>
            <div class="col-md-3 izquierda">
                <label>Nit </label>
                <input type="text" name="formulario[nit]" value="{{$dato->nit}}" class="form-control" >
            </div>
            <div class="col-md-3 izquierda">
                <label>Tipo Financiador </label>
                <select class="form-control" style="height: auto;" name="formulario[tipo_financiador]" required>
                    <option value="">Seleccione un tipo</option>
                    <option value="Administrable" @if($dato->tipo_financiador == "Administrable") Selected @endif >Administrable</option>
                    <option value="Proveedor Grande" @if($dato->tipo_financiador == "Proveedor Grande") Selected @endif >Proveedor Grande</option>
                    <option value="Proveedor Pequeno" @if($dato->tipo_financiador == "Proveedor Pequeno") Selected @endif >Proveedor Pequeno</option>
                    <option value="Aliado" @if($dato->tipo_financiador == "Aliado") Selected @endif >Aliado</option>
                    <option value="Materia Prima" @if($dato->tipo_financiador == "Materia Prima") Selected @endif >Materia Prima</option>
                    <option value="Plastico" @if($dato->tipo_financiador == "Plastico") Selected @endif >Plastico</option>
                </select>
            </div>
            <div class="col-md-3">
               <label>Añadir una descripción</label>
               <textarea class="form-control" name="formulario[descripcion]">{{$dato->descripcion}}</textarea>
            </div>
            <div class="col-md-4 izquierda margen-top">
                <label for="pais">Pais</label>
                <div class="row fila-pais" style="display: block;">
                    <div class="col-md-10">
                        <input type="text" id="pais_mostrar" name="pais" class="form-control pais_mostrar" value="{{$ubicacion['pais']}}" readonly>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-pencil lapiz cambiar-pais" aria-hidden="true" title="Cambiar de pais"></i>
                    </div>
                </div>
                <input type="text" style="display: none;" value="{{$ubicacion['pais']}}" class="form-control" id="pais" list="lista_paises" placeholder="Seleccione un pais" required="required">
                <datalist id="lista_paises">
                  @foreach($paises as $pais)
                  <option value="{{$pais->id}}" label="{{$pais->name}}">
                  @endforeach
                </datalist>
            </div>
            <div class="col-md-4 izquierda margen-top">
                <label for="departamento">Departamento <em>(Estado)</em></label>
                <div class="row fila-departamento" style="display: block;">
                    <div class="col-md-10">
                        <input type="text" id="departamento_mostrar" name="departamento" class="form-control departamento_mostrar" value="{{$ubicacion['departamento']}}" readonly>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-pencil lapiz cambiar-departamento" aria-hidden="true" title="Cambiar de departamento"></i>
                    </div>
                </div>
                <input type="text" style="display: none;" value="{{$ubicacion['departamento']}}" class="form-control" id="departamento" list="lista_departamentos" placeholder="Seleccione un pais" required="required">
                <datalist id="lista_departamentos">
                    @foreach($departamentos as $departamento)
                    <option value="{{$departamento->id}}" label="{{$departamento->name}}">
                    @endforeach
                </datalist>
            </div>
            <div class="col-md-4 izquierda margen-top">
                <label for="departamento">Ciudad <em>(Municipio)</em></label>
                <div class="row fila-ciudad" style="display: block;">
                    <div class="col-md-10">
                        <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" name="ciudad" value="{{$ubicacion['ciudad']}}" readonly>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-pencil lapiz cambiar-ciudad" aria-hidden="true" title="Cambiar de ciudad"></i>
                    </div>
                </div>
                <input type="text" style="display: none;" value="{{$ubicacion['ciudad']}}" class="form-control" id="ciudad" list="lista_ciudades" placeholder="Seleccione un pais" required="required" >
                <datalist id="lista_ciudades">
                    @foreach($ciudades as $ciudad)
                    <option value="{{$ciudad->id}}" label="{{$ciudad->name}}">
                    @endforeach
                </datalist>
            </div>
            <div class="col-md-12 text-center mt-5 pr-0 pl-0" id="res_container">
                    <h3>Responsables</h3>
<!--
                    <div class="actions text-center">
                          <a class="clone btn btn-success"><i class="fa fa-plus-square text-white"></i></a>
                          <a class="remove btn btn-danger"><i class="fa fa-minus-square text-white" aria-hidden="true"></i></a>
</div>
-->

                    <?php echo $responsables; ?>
                </div>
            <div class="col-md-12 margen-top">
                <a href="#" class="btn btn-success btn-sm btn_editar" data-toggle="tooltip" data-placement="top" title="Guardar">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                </a>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/crearpatrocinadores.js"></script>
@endsection
