@extends('template.app') @section('title', 'Planeación') @section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ url('/') }}/css/vis.min.css" rel="stylesheet">
<link href="{{ url('/') }}/css/vis-timeline-graph2d.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style type="text/css">
    .bg-white {
        background-color: #FFFFFF;
    }

    .dtp div.dtp-picker {
        padding: 0;
    }

    .vis-item {
        background-color: #bbd1e6;
        font-size: 15pt;
        color: #fff;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        border: 0px;
    }

    .vis-timeline{
        border-radius: 15px;
    }

    .vis-item .vis-item-overflow {
      overflow: visible;
    }
    .vis-item .vis-item-content {
      width: 100%;
      padding: 0;
    }
</style>

<?php echo $data['menu']; ?>
<div class="jumbotron">
    <div class="panel-title">
        <div class="pull-right">
            <button type="button" class="btn btn-primary btn-block" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>data-toggle="modal" data-target="#create_activity_mdl"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para agregar actividad')"<?php } ?>>Agregar actividad</button>
        </div>
        <h2>Planeación <span id="count_sponsors"></span></h2>
        <p class="letra-gris mb-0">Actividades</p>
    </div>
</div>

<div class="container bg-white rounded mt-5 w-100 h-100 p-5" id="timeline">
</div>

<!-- Modal -->
<div class="modal fade" id="create_activity_mdl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Añadir actividad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" role="form" autocomplete="off" id="new_activity">
       <input type="hidden" name="action" value="1">
       <input type="hidden" name="id" value="{{ $data['id'] }}">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <input type="datetime" class="form-control" id="fecha_inicio" name="fecha_inicio" placeholder="Fecha de inicio de la actividad" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <input type="datetime" class="form-control" id="fecha_fin" name="fecha_fin" placeholder="Fecha de finalización de la actividad" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input type="hidden" name="count" value="0">
                <div class="input-appendable-wrapper">
                    <input type="hidden" id="count" value="0">
                    <div class="input-group input-group-appendable" id="input-group-appendable1">
                        <input class="input form-control" id="responsable1" name="responsable[0]" type="text" placeholder="Añadir un responsable" required>
                        <span class="input-group-btn">
                        <button id="btn1" class="btn btn-primary add-more" type="button"><i class="fa fa-user-plus" aria-hidden="true"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título de la actividad" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <textarea class="form-control" name="descripcion" id="descripcion" rows="5" cols="30" placeholder="Añadir una descripción"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </form>
      </div>
    </div>
  </div>
</div>

@endsection @section('scripts')
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="{{url('/')}}/js/vis.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {

        redrawTL();

        $(document).on('submit', '#new_activity', function(event) {
            event.preventDefault();
            $.ajax({
                url: '{{ url("/") }}/feria/planeacion/activity-action',
                method: 'POST',
                dataType: 'json',
                cache: false,
                timeout: 10000,
                data: $(this).serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(suss) {
                    if (suss.res) {
                        $("#new_activity")[0].reset();
                        $("#create_activity_mdl").modal('hide');
                        redrawTL();
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        });

        $('#fecha_inicio').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm',
            lang: 'es',
            weekStart: 1,
            cancelText: 'Cancelar',
            okText: 'Aceptar'
        });

        $('#fecha_fin').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm',
            lang: 'es',
            weekStart: 1,
            cancelText: 'Cancelar',
            okText: 'Aceptar'
        });

        //*******************************************
        /*	RESPONSABLE DINÁMICO
        /********************************************/

        /*$("form.input-append").keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
            }
        });*/

        $(document).on('click', '.input-group-appendable .add-more', function() {
            $wrapper = $(this).parents('.input-appendable-wrapper');
            $lastItem = $wrapper.find('.input-group-appendable').last();

            $newInput = $lastItem.clone(true);

            // change attribute for new item
            $count = $wrapper.find('#count').val();
            $count++;

            // change text input and the button
            $newInput.attr('id', 'input-group-appendable' + $count);
            $newInput.find('input[type="text"]').attr({
                id: "responsable" + $count,
                name: "responsable[" + $count + "]"
            });

            $newInput.find('.btn').attr('id', 'btn' + $count);
            $newInput.appendTo($wrapper);

            //change the previous button to remove
            $lastItem.find('.btn')
                .removeClass('add-more btn-primary')
                .addClass('btn-danger')
                .html('<i class="fa fa-user-times" aria-hidden="true"></i>')
                .off()
                .on('click', function() {
                    $(this).parents('.input-group-appendable').remove();
                });

            $wrapper.find('#count').val($count);

        });


    });

    // Create a DataSet (allows two way data-binding)
    var items = new vis.DataSet();

    // DOM element where the Timeline will be attached
    var container = document.getElementById('timeline');

    // Configuration for the Timeline
    var options = {
        width: '100%',
        locales: {
        // create a new locale (text strings should be replaced with localized strings)
        mylocale: {
          current: 'current',
          time: 'time',
        }
        },
        // use the new locale
        locale: 'mylocale',
        editable: {
            add: false,
            remove: true,
            updateGroup: false,
            updateTime: true,
            overrideItems: false
        },
        onRemove: function(item, callback){
            console.log(item);
            swal({
                title: "¿Desea eliminar esta actividad?",
                text: "Esta acción eliminará los responsables asignados a la actividad",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Aceptar',
                cancelButtonText: "cancelar"
             }).then(
                   function (){
                       console.log("Confirmado");
                       $.ajax({
                           url: '{{ url("/") }}/feria/planeacion/activity-action',
                           method: 'POST',
                           dataType: 'json',
                           cache: false,
                           timeout: 10000,
                           headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {id: item.id, action: 3},
                        success: function(suss){
                            if(suss.res){
                                redrawTL();
                            }
                        },
                        error: function(err){
                            console.log(err);
                        }
                       });
                   },
                   function (){
                      redrawTL();
                   });

        }
    };


    // Create a Timeline
    var timeline = new vis.Timeline(container, items, options);

    items.on('update', function(event, datos){
        if(datos.data[0].id === datos.oldData[0].id && (datos.data[0].start !== datos.oldData[0].start || datos.data[0].end !== datos.oldData[0].end)){
            var id = datos.data[0].id;

            swal({
            title: '¿Desea Actualizar la fecha de la actividad?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
        }).then(
                function(json_data) {
            if(json_data){

                $.ajax({
                url: '/feria/planeacion/activity-action',
                method: 'POST',
                dataType: 'json',
                cache: false,
                timeout: 10000,
                data: {action: 2, fecha_inicio: datos.data[0].start, fecha_fin: datos.data[0].end, id: id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(suss) {
                    console.log(suss);
                    if(suss.res){
                        redrawTL();
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });

            }else{
                console.log(false);
            }
        },
            function(){
                redrawTL();
            });
        }
    });

    timeline.on('doubleClick', function(e) {
        swal({
            title: '¿Desea añadir una actividad?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
        }).then(function(json_data) {
            if(json_data){
                $("#create_activity_mdl").modal('show');
            }else{
                console.log(false);
            }
        });
    });

    function redrawTL() {
        $.ajax({
            method: "POST",
            url: "{{ url('/') }}/feria/planeacion/timeline",
            cache: false,
            dataType: 'json',
            timeout: 10000,
            data: {
                id: {{ $data['id'] }}
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(suss) {
                if (suss.res && suss.data) {
                    items.clear();
                    items.add(suss.data);
                    timeline.fit();
                }
                if (suss.res && suss.data == "") {
                    swal(
                        'Atención',
                        'No hay actividades para esta feria.',
                        'info'
                    );
                }

            },
            error: function(err) {
                console.log(err);
            }
        });
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
