@extends('template.app') @section('title', 'Ferias patrocinadores') @section('content')
<link rel="stylesheet" href="{{url('/')}}/css/ferias/botonessuperioresferias.css" type="text/css" media="all" />
<input type="hidden" value="{{url('/')}}" id="URL">
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/listadopatrocinadores.css">
<div class="jumbotron">
    <div class="panel-title">
        <div class="pull-right">
            <!--
            <a href="{{ url('calendario/feria') }}" class="btn btn-info btn-sm">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                Calendario de ferias
            </a>
                <a href="{{ url('feriasperdidas') }}" class="btn btn-danger btn-sm">
                <i class="fa fa-list" aria-hidden="true"></i>
                Ferias perdidas
            </a>
                <a href="{{ url('feriasposibles') }}" class="btn btn-warning btn-sm">
                <i class="fa fa-list" aria-hidden="true"></i>
                Posibles ferias
            </a>
                <a href="{{ url('proveedores') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-list" aria-hidden="true"></i>
                Proveedores
            </a>
                <a href="{{ url('/') }}/feria/patrocinadores/crear" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Crear Patrocinador
            </a>-->
            <a href="{{url('feria/patrocinadores')}}" class="btn btn-sm btn-patrocinador">
                Patrocinadores | {{$boton['patrocinadores']}}
            </a>
            <a href="{{url('proveedores')}}" class="btn btn-sm btn-proveedor">
                Proveedores | {{$boton['proveedores']}}
            </a>
            <a href="{{url('ferias/menu')}}" class="btn btn-sm btn-feria">
                Feria
            </a>
            <a <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>href="/feria/patrocinadores/crear"<?php }else{ ?> onclick="no_permiso('Usted no tiene permisos para editar la pregunta')" <?php } ?> class="btn btn-success btn-sm text-white">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Crear Patrocinador
            </a>
        </div>
        <h2>Listado de Patrocinadores</h2>
    </div>
</div>

<div class="container">
    <div class="row">
        @foreach($pdata as $dato)
        @php if(isset($dato->logo) && !empty($dato->logo)){
        $src = url('/').'/storage/ferias/patrocinadores/'.$dato->logo;
        }else{
        $src = 'https://www.shareicon.net/data/2016/09/01/822711_user_512x512.png';
        } @endphp
        <div class="col-lg-3 col-sm-6 mt-3 mb-3">
            <div class="card">
                <div class="cardheader" onclick="detalles({{ $dato->id }})"></div>
                <div class="avatar" onclick="detalles({{ $dato->id }})">
                    <img alt="" src="{{ $src }}">
                </div>
                <div class="card-body pr3 pl-3" onclick="detalles({{ $dato->id }})">
                    <h4 class="card-title"><a target="_blank" href="{{url('/')}}/feria/patrocinador/detalles-patrocinador/{{ $dato->id }}">{{$dato->nombre}}</a></h4>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{str_replace("%%", ", ", $dato->ubicacion)}}</li>
                        <li class="list-group-item">{{$dato->tipo_financiador}}</li>
                        <li class="list-group-item">
                            <h4>Solicitudes de patrocinio</h4>
                            <h5 class="text-success">Aprobadas <span class="badge badge-secondary">{{$dato->Aprobado}}</span></h5>
                            <h5 class="text-danger">Rechazadas <span class="badge badge-secondary">{{$dato->Rechazado}}</span></h5>
                            <h5 class="text-warning">Pendientes <span class="badge badge-secondary">{{$dato->Pendiente}}</span></h5>
                        </li>
                    </ul>
                </div>
                <div class="card-footer text-center">
                    <div class="share-buttons">
                        <a class="share-button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>href="/feria/patrocinadores/editar/{{ $dato->id }}"<?php }else{ ?>href="#" onclick="no_permiso('Usted no tiene permisos para editar el patrocinador')" <?php } ?>>
                            <div class="share-button-secondary">
                                <div class="share-button-secondary-content">
                                    Editar
                                </div>
                            </div>
                            <div class="share-button-primary">
                                <i class="share-button-icon fa fa-pencil"></i>
                            </div>
                        </a>
                        <a class="share-button" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="eliminar({{ $dato->id }})"<?php }else{ ?>href="#" onclick="no_permiso('Usted no tiene permisos para eliminar el patrocinador')" <?php } ?>>
                            <div class="share-button-secondary">
                                <div class="share-button-secondary-content">
                                    Eliminar
                                </div>
                            </div>
                            <div class="share-button-primary">
                                <i class="share-button-icon fa fa-trash"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection @section('scripts')
<script>
    function detalles(id){
        window.location.href = "{{url('/')}}/feria/patrocinador/detalles-patrocinador/"+id;
    }

    function verpatrocinador(id){
        $.ajax({
            url: "{{url('/')}}/verlistpatrocinador/" + id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e) {
                swal({
                      title: 'Patrocinador',
                      html: e.content,
                      showCloseButton: true,
                      showCancelButton: true,
                      showConfirmButton: false,
                      focusConfirm: false,
                      cancelButtonText:'Cerrar',
                    });
            }
        });
    }

    function eliminar(id) {
        swal({
            title: 'Esta seguro?',
            text: "Va a eliminar un patrocinador",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, guardar!'
        }).then(function() {
            $.ajax({
                url: "{{url('/')}}/eliminarpatrocinador/" + id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e) {
                    swal(
                        'Guardado',
                        e.msj,
                        'success'
                    ).then(function() {
                        location.href = "{{url('/')}}/feria/patrocinadores";
                    });
                }
            });
        });
    }
/*
    $('body').on('click', '.ver', function() {
        id = $(this).attr('id');
        id = id.replace('ver-', '');
        $('.ver-patrocinador-modal').modal('show');
        $.ajax({
            url: "{{url('/')}}/verpatrocinador/" + id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e) {
                $('.nombre-patrocinador').html(e.dato.nombre);
                $('#image_patrocinador').attr('src', '{{url("/")}}/storage/ferias/patrocinadores/' + e.dato.logo);
                $('#creado').html('<i class="fa fa-plus"></i> ' + formato(e.dato.created_at));
                $('#editado').html('<i class="fa fa-pencil"></i> ' + formato(e.dato.updated_at));
                $('#ubicacion').html(e.ubicacion);
                $('.contacto').html(e.dato.responsable);


                $('#responsable').html(e.dato.responsable);
                telefono = (e.dato.telefono).replace(/%%/g, ' ');
                $('#telefono').html(telefono);
                celular = (e.dato.celular).replace(/%%/g, ' ');
                $('#celular').html(celular);
                $('#correo').html(e.dato.correo);

            }
        });
    });*/

    function formato(texto) {
        fecha_completa = texto.split(' ');
        solo_fecha = fecha_completa[0].split('-');
        solo_hora = fecha_completa[1].split(':');
        fecha_real = solo_fecha[2] + '/' + solo_fecha[1] + '/' + solo_fecha[0];
        if (parseInt(solo_hora[0]) >= 13) {
            hora = parseInt(solo_hora[0]) - 12;
            fecha_real = fecha_real + ' ' + hora + ':' + solo_hora[1] + ':' + solo_hora[2] + ' P.M';
        } else {
            fecha_real = fecha_real + ' ' + solo_hora[0] + ':' + solo_hora[1] + ':' + solo_hora[2] + ' A.M';
        }
        return fecha_real;
    }
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
