@extends('template.app')
@section('title', 'Ferias asistidas')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/bitacora.css">
<input type="hidden" id="URL" value="{{url('/')}}">
<input type="hidden" id="id_feria" value="{{ $id }}">
<div class="widget">
	<?php echo $menu; ?>

	<div class="widget-header">
		<h2>Bitacora</h2>
		<input type="hidden" id="feria_id" value="{{ $id }}">
	</div>
	<div class="widget-content">
        <div class="row">
            <div class="col-md-12 text-center mb-3" id="btn-nav">
                <ul class="social-network social-circle">

                    <li><a href="#" class="icoRegistre" title="Registros"><i class="fa fa-list-alt"></i></a></li>

                    <li><a href="{{ url('/') }}/feria/bitacora/dashboard/{{ $id }}" class="icoDashboard" title="Dashboard"><i class="fa fa-dashboard"></i></a></li>
                </ul>
            </div>
        </div>
       <div class="card">
         <div class="header">
            <div class="color-overlay">
              <!--<div class="day-number">8</div>-->
              <div class="date-left">@php setlocale(LC_ALL, "es_CO.UTF-8"); @endphp
                <div class="day-name">{{ $model->nombre }}</div>
                <div class="fecha-evento">
                    {{ strftime("%A %d de %B de %Y a las %r", strtotime($model->fecha_inicio)).", ".$data['ubicacion'] }}
                </div>
                <div class="month">@php echo($model->descripcion); @endphp</div>
              </div>
            </div>
            <div class="actionbutton" <?php if($data['permiso_agregar_editar_eliminar']=="Si"){ ?>onclick="newBitacora({{ $id }})"<?php }else{ ?>onclick="no_permiso('Usted no tiene permisos para agregar bitacora o comentario')"<?php } ?>>+</div>
          </div>
          <ul class="timeline"></ul>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ url('/') }}/js/plugins/ripples.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/parsley.min.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/ferias/bitacora.js"></script>
@endsection
