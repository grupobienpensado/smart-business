@extends('template.app')
@section('title', 'Archivos')

@section('content')
<style>
    .dropzone{
        border-style: dotted !important;
        border-width: 3px !important;
        border-color: #011d62 !important;
    }
</style>
<link rel="stylesheet" href="/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="{{url('/')}}/css/ferias/botonessuperioresferias.css" type="text/css" media="all" />
<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{url('feria/patrocinadores')}}" class="btn btn-sm btn-patrocinador">
            Patrocinadores | {{$boton['patrocinadores']}}
        </a>
        <a href="{{url('proveedores')}}" class="btn btn-sm btn-proveedor">
            Proveedores | {{$boton['proveedores']}}
        </a>
        <a href="{{url('ferias/menu')}}" class="btn btn-sm btn-feria">
            Feria
        </a>
      </div>
       <div class="main-header">
            <h2>Ferias</h2>
            <em>Cargar archivos a la feria</em>
        </div>
    </div>
</div>
<input type="hidden" name="id" id="id">
<div class="main-content">
    <div class="row archivos_cargados">
        <?=$html?>
    </div>
    <!-- FILE UPLOAD -->
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-cloud-upload"></i> Cargar</h3></div>
        <div class="widget-content">

            <form class="dropzone" action="{{url('/')}}/cargarmultimedia"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$id}}">
            </form>
        </div>
    </div>
    <!-- END FILE UPLOAD -->
</div>
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <img src="/images/logo.png" style="position: fixed;z-index: 1;right: 20px;top: 20px;opacity: 0.5;" />
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{url('/')}}/assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript">
        // if dropzone exist
        if( $('.dropzone').length > 0 ) {
            Dropzone.autoDiscover = false;

            $(".dropzone").dropzone({
                url: "{{url('/')}}/cargarmultimedia",
                addRemoveLinks : true,
                maxFilesize: 30,
                maxFiles: 50,
                acceptedFiles: 'image/*, video/*, application/pdf, .txt',
                dictResponseError: 'File Upload Error.',
                success: function(e){
                    lista({{$id}});
                }
            });
        } // end if dropzone exist
    /**
     * Función para listar los archivos cargados
     * @param INT id IDENTIFICACIÓN DE LA FERIA
     */
    function lista(id){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/ferias-acciones",
            data: {
                _token: CSRF_TOKEN,
                accion: 2,
                id: id
            },
            cache: false,
            type: 'POST',
            success: function(e){
               $('.archivos_cargados').html(e.data['html']);
            }
        });
    }
    function rename(id,antiguo){
        swal({
              title: 'Renombrar',
              input: 'text',
              inputPlaceholder: 'Escriba el nuevo nombre',
              showCancelButton: true,
              inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                  if (value) {
                    resolve()
                  } else {
                    reject('Debe escribir un nombre!')
                  }
                })
              }
            }).then(function (name) {
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                    url: "{{url('/')}}/renombrararchivo",
                    data: {
                        _token: CSRF_TOKEN,
                        nuevonombre: name,
                        antiguo: antiguo,
                        id: id
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        $('.archivos_cargados').html(e.msj);
                        swal({
                            type: 'success',
                            title: 'Realizado nuevo nombre es: ' + name
                          })
                    }
                });
            });
    }

    function deletefile(id){
        swal({
          title: 'Eliminar',
          text: "Desee eliminar el archivo",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!',
          cancelButtonText: 'No',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(function () {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "{{url('/')}}/eliminararchivo",
                data: {
                    _token: CSRF_TOKEN,
                    id: id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.archivos_cargados').html(e.msj);
                    swal({
                        type: 'success',
                        title: 'Realizado'
                      })
                }
            });

        })
    }

    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING msj MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>

@endsection
