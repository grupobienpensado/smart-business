@extends('template.app')
@section('title', 'Editar invitado')

@section('content')
<input type="hidden" id="URL" value="{{url('/')}}">

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/crearinvitados.css">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<div class="widget">
	<ul class="nav nav-pills" role="tablist">
        <li><a href="{{url('/')}}/feria/ver/{{$id}}"><i class="fa fa-university"></i> Feria</a></li>
        <li><a href="{{url('/')}}/feria/galeria/{{$id}}"><i class="fa fa-picture-o"></i> Galeria</a></li>
        <li class="active" ><a role="tab" data-toggle="tab"><i class="fa fa-users"></i> Invitados</a></li>
        <li><a href="{{url('/')}}/feria/conferencia/{{$id}}"><i class="fa fa-desktop"></i> Conferencia</a> </li>
        <li><a href="#"><i class="fa fa-calendar"></i> Bitacora</a></li>
        <li><a href="#"><i class="fa fa-camera"></i> Camara</a></li>
        <li><a href="#"><i class="fa fa-hand-rock-o"></i> Competencia</a></li>
        <li><a href="#"><i class="fa fa-clock-o"></i> Planeacion</a></li>
        <li><a href="#"><i class="fa fa-money"></i> Presupuestos</a></li>
        <li><a href="#"><i class="fa fa-handshake-o "></i> Patrocinios</a></li>
        <li><a href="#"><i class="fa fa-check"></i> Lecciones</a></li>
    </ul>
	<div class="main-header margin-top">
        <h2>Editar invitado</h2>
        <em>{{$dato->nombre}}</em>
	</div>
    <div class="widget-content">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('/')}}/invitados/ver/{{$id}}" data-toggle="tooltip" data-placement="top" title="Regresar al listado"><i class="fa fa-undo regresar-invitado" aria-hidden="true"></i></a>
            </div>
        </div>
        <form id="formulario" method="POST" enctype="multipart/form-data">
            <input type="hidden" id="id_feria" name="id_feria" value="{{$id}}">
            <input type="hidden" name="id_invitado" value="{{$invitado->id}}">
            <div class="row">
                {{ csrf_field() }}
                <div class="col-md-12">
                     @if(!empty($invitado->foto))
                            <?php
                                $estilo='background-image: url("'.url("/").'/storage/ferias/invitados/'.$invitado->foto.'"); background-size: 100%;';
                            ?>
                            <div class="dropzone foto" style='{{$estilo}}' data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                      @else
                            <div class="dropzone foto" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                      @endif
                      <input type="file" name="foto" accept="image/gif, image/jpeg, image/png">
                    </div>
                </div>
                <div class="col-md-12">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="nombre" value="{{$invitado->nombre}}" required>
                </div>
                <div class="col-md-6">
                    <label>Empresa</label>
                    <input type="text" class="form-control" name="empresa" value="{{$invitado->empresa}}" required>
                </div>
                <div class="col-md-6">
                    <label>Cargo</label>
                    <input type="text" class="form-control" name="cargo" value="{{$invitado->cargo}}" required>
                </div>
                <div class="col-md-6">
                    <label>Correo</label>
                    <input type="email" class="form-control" name="correo" value="{{$invitado->correo}}" required>
                </div>
                <div class="col-md-6">
                    <label>Telefono</label>
                    <input type="text" class="form-control" name="telefono" value="{{$invitado->telefono}}" required>
                </div>
                <div class="col-md-12">
                    <label>Motivo de la invitación</label>
                    <textarea class="form-control" name="motivo_invitacion" rows="4" placeholder="Escriba el motivo de la invitación" required="required">{{$invitado->motivo_invitacion}}</textarea>
                </div>
                <div class="col-md-12 margin-top">
                    <a type="button" class="btn btn-success" id="guardar_invitado"><i class="fa fa-floppy-o"></i> Guardar</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection


@section('scripts')
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/ver_totales_pestana.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/editarinvitado.js"></script>
@endsection
