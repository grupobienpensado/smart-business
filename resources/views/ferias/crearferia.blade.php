@extends('template.app')
@section('title', 'Crear Feria')

@section('content')
<link rel="stylesheet" href="{{url('/')}}/css/ferias/botonessuperioresferias.css" type="text/css" media="all" />
<link href="{{ url('/') }}/css/ferias/crearferias.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<input type="hidden" id="fecha_actual" value="{{date('Y-m-d H:i:s')}}" >

<div class="jumbotron">
    <div class="panel-title">
      <div class="pull-right">
        <a href="{{url('feria/patrocinadores')}}" class="btn btn-sm btn-patrocinador">
            Patrocinadores | {{$boton['patrocinadores']}}
        </a>
        <a href="{{url('proveedores')}}" class="btn btn-sm btn-proveedor">
            Proveedores | {{$boton['proveedores']}}
        </a>
        <a href="{{url('ferias/menu')}}" class="btn btn-sm btn-feria">
            Feria
        </a>
      </div>
      <div class="main-header">
        <h2>Ferias</h2>
        <em>Crear una posible feria</em>
    </div>
    </div>
</div>

<div class="row formulario">
    <form id="formulario_basico" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" value="{{url('/')}}" id="URL">
       <div class="col-md-11 formulario1">
          <!-- FORMULARIO BASICO -->
          <div class="widget">
             <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Formulario Datos basicos</h3>
             </div>
             <div class="widget-content">
                <div class="form-horizontal">
                   <div class="form-group">
                      <label class="col-md-2 control-label">Nombre</label>
                      <div class="col-md-10">
                         <input type="text" class="form-control" name="nombre" placeholder="Nombre" required="required">
                      </div>
                   </div>
                   <div class="form-group">
                      <label class="col-md-2 control-label">Descripción</label>
                      <div class="col-md-10">
                         <textarea class="form-control" name="descripcion" placeholder="Descripción" rows="8" required="required"></textarea>
                      </div>
                   </div>
                   <!--<div class="form-group">
                      <label class="col-md-2 control-label">Ubicación</label>
                      <div class="col-md-10">
                         <input type="text" class="form-control" name="ubicacion" placeholder="Ubicación" required="required">
                      </div>
                   </div>-->
                   <div class="form-group">
                      <label class="col-md-2 control-label" for="pais">Pais</label>
                      <div class="col-md-10 datos margen-butom">
                        <div class="row fila-pais">
                            <div class="col-md-11">
                                <input type="text" id="pais_mostrar" class="form-control pais_mostrar" value="" readonly>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-pencil cambiar-pais" aria-hidden="true" title="Cambiar de pais"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control" id="pais" name="pais" list="lista_paises" placeholder="Seleccione un pais" required="required">
                        <datalist id="lista_paises">
                          @foreach($paises as $pais)
                          <option value="{{$pais->id}}" label="{{$pais->name}}">
                          @endforeach
                        </datalist>
                    </div>

                   <label class="col-md-2 control-label" for="departamento">Departamento <em>(Estado)</em></label>
                    <div class="col-md-10 margen-butom">
                        <div class="row fila-departamento">
                            <div class="col-md-11">
                                <input type="text" id="departamento_mostrar" class="form-control departamento_mostrar" value="" readonly>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-pencil cambiar-departamento" aria-hidden="true" title="Cambiar de departamento"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control no-cursor" id="departamento" name="departamento" list="lista_departamentos" placeholder="Seleccione un pais" required="required" readonly>
                        <datalist id="lista_departamentos">

                        </datalist>
                    </div><br>

                    <label class="col-md-2 control-label" for="departamento">Ciudad <em>(Municipio)</em></label>
                    <div class="col-md-10 margen-butom">
                        <div class="row fila-ciudad">
                            <div class="col-md-11">
                                <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" value="" readonly>
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-pencil cambiar-ciudad" aria-hidden="true" title="Cambiar de ciudad"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control no-cursor" id="ciudad" name="ciudad" list="lista_ciudades" placeholder="Seleccione un pais" required="required" readonly>
                        <datalist id="lista_ciudades">

                        </datalist>
                    </div>
                   </div>
                   <div class="form-group">
                      <label class="col-md-2 control-label">Fecha y hora de inicio</label>
                      <div class="col-md-10">
                         <input class="form-control2 form-control datatime" id="fecha_inicio" name="fecha_inicio" type="text" step="1800" placeholder="Fecha y hora" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label class="col-md-2 control-label">Fecha y hora de Fin</label>
                      <div class="col-md-10">
                         <input class="form-control2 form-control datatime" id="fecha_fin" name="fecha_fin" type="text" step="1800" placeholder="Fecha y hora" required>
                      </div>
                   </div>
                </div>
             </div>
             <div class="row">
                <div class="col-md-12">
                   <div class="dropzone logo" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                      <input type="file" name="logo" accept="image/gif, image/jpeg, image/png" required="required">
                   </div>
                </div>
             </div>
          </div>
          <!-- END FORMULARIO BASICO -->

          <div class="widget">
             <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Links </h3>
             </div>
             <div class="widget-content">
                <div class="row">
                   <div class="col-md-12">
                      <div class="input-appendable-wrapper">
                         <div class="input-group input-group-appendable">
                            <span class="input-group-addon" id="basic-addon3">https://ejemplo.com/ferias/</span>
                            <input autocomplete="off" class="input form-control direcciones_url" name="link[0][contenido]" type="text" required="required">
                            <span class="input-group-btn">
                            <button class="btn btn-primary add-more" type="button">+</button>
                            </span>
                         </div>
                      </div>
                      <br>
                      <p>Presione + para agregar un link</p>
                   </div>
                </div>
             </div>
          </div>

          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <div class="col-sm-12">
                        <a  class="btn btn-success guardar"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                      </div>
                  </div>
              </div>
          </div>
       </div>
   </form>
</div>

@endsection

@section('scripts')
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>

<script src="{{ url('/') }}/js/plugins/material.min.js"></script>
<script src="{{ url('/') }}/js/plugins/material2.min.js"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>

<script src="{{ url('/') }}/js/ferias/crearferia.js"></script>
@endsection
