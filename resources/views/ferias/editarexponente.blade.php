@extends('template.app')
@section('title', 'Editar feria exponente')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/ferias/editarexponente.css">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<input type="hidden" value="{{url('/')}}" id="URL">
<div id="contenedor-exponente">
    <div class="row" id="Exponente">
        <div class="col-md-12 titulo">
            <h2>Editar datos como Exponente</h2>
        </div>
    </div>
    <div class="container formulario_exponente">
        <form id="formulario_exponente" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="{{$datos[0]->id}}" name="id" >
            <input type="hidden" value="{{$feria->id}}" name="id_feria" id="id_feria" >
            <div class="col-md-1">
            </div>
            <div class="col-md-10 crop">
                <div class="dropzone" style="background-image:url('{{url("/")}}/storage/ferias/{{$datos[0]->baner}}'); background-size: 100%; min-width: 1024px !important; max-width: 1024px !important;min-height: 184px !important;max-height: 184px !important;margin-top: 2%;" data-width="1024" data-height="184" data-ajax="false" data-originalsave="true">
                  <input type="file" name="baner" accept="image/gif, image/jpeg, image/png">
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-12 text-left">
                <h3>Equipos Exhibidos</h3>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#" class="btn btn-success btn-sm" id="agregar_equipo" data-toggle="tooltip" data-placement="top" title="Agregar Maquina">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar maquina
                        </a>
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" value="{{$datos[0]->equipos}}" name="referencias" id="referencias_agregadas">
                        <div class="row cont-imagenes">

                            <?= $html['maquinas'] ?>

                        </div>
                    </div>
                </div>
                <hr size="2px" color="black" />
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h6>Cantidad de personas ESSI en feria</h6>
                        <input type="hidden" class="form-control" id="cantidad_personas" name="cantidad_personas" value="{{$datos[0]->cantidad_personas}}" required>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="#" class="btn btn-success btn-sm" id="agregar_invitado" data-toggle="tooltip" data-placement="top" title="Agregar invitado">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar invitado
                                </a>
                            </div>
                        </div>
                        <div class="row listado_funcionarios_invitados">
                            <?= $html['invitados'] ?>
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Responsable ESSI</h6>
                        <select class="form-control" name="responsable">
                            <option value="">Seleccione un responsable</option>
                            @foreach($responsables_essi as $r)
                            <option value="{{$r->id}}" @if($r->id == $datos[0]->responsable) Selected @endif >{{$r->nombres.' '.$r->apellidos}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Objetivos</h6>
                        <input type="hidden" id="objetivos_total" value="{{$html['objetivos_total']}}">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-objetivo" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Objetivo"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Item #
                            </div>
                            <div class="col-md-11">
                                Objetivo
                            </div>
                        </div>
                        <div class="listado-objetivos">
                        @if($datos[0]->objetivos=='')
                        <div class="row n_obj" id="n_obj0">
                            <div class="col-md-1 num-objetivo">
                                1
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="objetivos[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <?= $html['objetivos'] ?>
                        @endif
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Alcances</h6>
                        <input type="hidden" id="alcances_total" value="{{$html['alcances_total']}}">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-alcance" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Alcance"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Item #
                            </div>
                            <div class="col-md-11">
                                Alcance
                            </div>
                        </div>
                        <div class="listado-alcances">
                        @if($datos[0]->alcances=='')
                        <div class="row n_alc" id="n_alc0">
                            <div class="col-md-1 num-alcance">
                                1
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="alcances[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <?= $html['alcances'] ?>
                        @endif
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Estrategias</h6>
                        <input type="hidden" id="estrategias_total" value="{{$html['estrategias_total']}}">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="agregar-estrategia" style="color: green; cursor: pointer" data-toggle="tooltip" data-placement="top" title="Agregar Estrategia"><i class="fa fa-plus-circle fa-3" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Item #
                            </div>
                            <div class="col-md-11">
                                Estrategia
                            </div>
                        </div>
                        <div class="listado-estrategias">
                        @if($datos[0]->estrategia=='')
                        <div class="row n_est" id="n_est0">
                            <div class="col-md-1 num-estrategia">
                                1
                            </div>
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="estrategia[0]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <?= $html['estrategias'] ?>
                        @endif
                        </div>
                        <hr size="2px" color="black" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Atuento de asistentes</h6>
                        <textarea class="form-control" name="atuendo" rows="8" required>{{$datos[0]->atuendo}}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-12 btn-guardar">
                <a href="#" class="btn btn-success btn-sm btn_guardar_expo" data-toggle="tooltip" data-placement="top" title="Guardar">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ferias/editarexponente.js"></script>
@endsection
