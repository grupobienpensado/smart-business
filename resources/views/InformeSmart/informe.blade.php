{{ csrf_field() }}
<!-- Bootstrap core CSS -->
<link href="https://essi.gestionsmart.com/InformeSmart/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="https://essi.gestionsmart.com/InformeSmart/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://essi.gestionsmart.com/InformeSmart/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="https://essi.gestionsmart.com/InformeSmart/css/landing-page.min.css" rel="stylesheet">
<div class="row d-flex justify-content-center">
    <div class="col-md-3">
        <input type="date" class="form-control" id="inicio" onchange="filtro()">
    </div>
    <div class="col-md-3">
        <input type="date" class="form-control" id="fin" onchange="filtro()">
    </div>
</div>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col" colspan="7" class="text-center">Smart ESSI</th>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Fecha</th>
                <th scope="col">Actividades</th>
                <th scope="col">Clientes</th>
                <th scope="col">Oportunidades</th>
                <th scope="col">Plan de trabajo</th>
                <th scope="col">Total</th>
            </tr>
    </thead>
    <tbody id="content-informe">

    </tbody>
</table>
<template id="FilasIpsa">
    <tr>
      <th scope="row" data-numero>1</th>
      <td data-fecha>Mark</td>
      <td data-actividades>Otto</td>
      <td data-clientes>@mdo</td>
      <td data-oportunidades>@mdo</td>
      <td data-plantrabajo>@mdo</td>
      <td data-total class="bg-warning"></td>
    </tr>
</template>
<script src="https://essi.gestionsmart.com/InformeSmart/vendor/jquery/jquery.min.js"></script>
<script src="https://essi.gestionsmart.com/InformeSmart/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
    var fechaInicio='';
    var fechaFin='';
    $(document).ready(function(){
        fechaInicio = $('#inicio').val();
        fechaFin = $('#fin').val();
        Informe(fechaInicio,fechaFin);
    });
    function filtro(){
        fechaInicio = $('#inicio').val();
        fechaFin = $('#fin').val();
        Informe(fechaInicio,fechaFin);
    }
    function Informe(fechaInicio,fechaFin){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "https://essi.gestionsmart.com/informe-smart",
            data: {
                _token: CSRF_TOKEN,
                fechaInicio: fechaInicio,
                fechaFin: fechaFin
            },
            cache: false,
            type: 'POST',
            success: function(e){
                ArmarInformeIpsa(e.data['meses'],e.data['actividades'],e.data['clientes'],e.data['oportunidades'],e.data['plantrabajo']);
            }
        })
    }

        function ArmarInformeIpsa(meses,actividades,clientes,oportunidades,plantrabajo){

            $("#content-informe").html('');
            var i = 1;
            $.each(meses, function (key, value) {
                var template = $('#FilasIpsa').html();
                var $template = $(template);
                $template.find("[data-numero]").html(i);
                $template.find("[data-fecha]").html(formatFecha(value));
                var totalActividades = ActividadesESSI(actividades,value);
                $template.find("[data-actividades]").html(totalActividades);
                var totalClientes = ClientesESSI(clientes,value);
                $template.find("[data-clientes]").html(totalClientes);
                var totalOportunidades = OportunidadesESSI(oportunidades,value);
                $template.find("[data-oportunidades]").html(totalOportunidades);
                var totalPlan = PlantrabajoESSI(plantrabajo,value);
                $template.find("[data-plantrabajo]").html(totalPlan);

                var total  = totalActividades + totalClientes + totalOportunidades + totalPlan;
                $template.find("[data-total]").html(total);
                $template.appendTo($("#content-informe"));
                i++;
            });
        }

        function ActividadesESSI(actividades,mes){
            var total = 0;
            $.each(actividades, function (key, value) {
                if(value.MES==mes){
                   total = value.BITACORA;
                }
            });
            return total;
        }

        function ClientesESSI(clientes,mes){
            var total = 0;
            $.each(clientes, function (key, value) {
                if(value.MES==mes){
                   total = value.CLIENTES;
                }
            });
            return total;
        }

        function OportunidadesESSI(oportunidades,mes){
            var total = 0;
            $.each(oportunidades, function (key, value) {
                if(value.MES==mes){
                   total = value.OPORTUNIDADES;
                }
            });
            return total;
        }

        function PlantrabajoESSI(plantrabajo,mes){
            var total = 0;
            $.each(plantrabajo, function (key, value) {
                if(value.MES==mes){
                   total = value.PLANTRABAJO;
                }
            });
            return total;
        }
        /**
         * Función para dar formato a las fechas (01 Mes 2018)
         * @param   STRING   fecha FECHA A DAR FORMATO
         * @returns STRING FECHA CON FORMATO
         */
        function formatFecha(fecha){
            var solofecha = fecha.split(' ');
            var porciones = solofecha[0].split('-');
            var meses = ["","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            return meses[parseInt(porciones[1])]+' '+porciones[0];
        }

    </script>
