@extends('template.app')
@section('title', 'Ver Mercado')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css">
<style>
    #profile_img{shape-outside:circle(50%);float:left;border-radius:100%!important}
    .inputDnD .form-control-file{position:relative;width:100%;height:100%;min-height:6em;outline:none;visibility:hidden;cursor:pointer;-webkit-box-shadow:0 0 5px solid currentColor;box-shadow:0 0 5px solid currentColor}
    .inputDnD .form-control-file:before{content:attr(data-title);position:absolute;top:.5em;left:0;width:100%;min-height:9em;line-height:2em;padding-top:1.5em;opacity:1;visibility:visible;text-align:center;border:.25em dashed currentColor;-webkit-transition:all .3s cubic-bezier(0.25,0.8,0.25,1);transition:all .3s cubic-bezier(0.25,0.8,0.25,1);overflow:hidden}
    .inputDnD .form-control-file:hover:before{border-style:solid;-webkit-box-shadow:inset 0 0 0 .25em currentColor;box-shadow:inset 0 0 0 .25em currentColor}
    .btn-circle{width:2rem!important;height:2rem!important;text-align:center;font-size:12px;line-height:1.428571429;border-radius:50%!important;padding:6px 0;}
    .swiper-container{width:100%;height:40rem;}
    .swiper-slide{text-align:center;font-size:18px;background:#fff;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;-webkit-justify-content:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;-webkit-align-items:center;align-items:center;}
    .content-wrapper{z-index:1;}
    .ribon-1{position:absolute;background-color:#f1c40f;border-radius:3px;right:-15px;width:25rem;text-align:left;-webkit-transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out;-ms-transition:all .3s ease-in-out;-o-transition:all .3s ease-in-out;transition:all .3s ease-in-out;padding:10px;}
    .ribon-1::after{content:"";position:absolute;display:block;width:20px;height:20px;background-color:#A48505;transform:rotate(45deg);right:5px;top:6.2rem;z-index:-1;}
    .ribon-1:hover{width:35rem;}
    #change_button_wraper{display:none;}
    .btn{-webkit-transition:all .4s ease-in-out;transition:all .4s ease-in-out;border:0;border-radius:.125rem;cursor:pointer;white-space:normal;word-wrap:break-word;color:#fff!important;}
    #change_button_wraper .btn.btn-success{width:10.5rem;background-color:#00c851!important;color:#fff!important;}
    select.form-control:not([size]):not([multiple]){height:34px!important;}
    .img-redonda{ border-radius: 200px 200px 200px 200px;-moz-border-radius: 200px 200px 200px 200px;-webkit-border-radius: 200px 200px 200px 200px;border: 0px solid #000000;}
    /*Icono de Carga*/.cssload-loader{position:relative;left:calc(50% - 36px);width:72px;height:72px;border-radius:50%;-o-border-radius:50%;-ms-border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%;perspective:900px}.cssload-inner{position:absolute;width:100%;height:100%;box-sizing:border-box;-o-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;border-radius:50%;-o-border-radius:50%;-ms-border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%}.cssload-inner.cssload-one{left:0;top:0;animation:cssload-rotate-one .6s linear infinite;-o-animation:cssload-rotate-one .6s linear infinite;-ms-animation:cssload-rotate-one .6s linear infinite;-webkit-animation:cssload-rotate-one .6s linear infinite;-moz-animation:cssload-rotate-one .6s linear infinite;border-bottom:3px solid #0f3e9c}.cssload-inner.cssload-two{right:0;top:0;animation:cssload-rotate-two .6s linear infinite;-o-animation:cssload-rotate-two .6s linear infinite;-ms-animation:cssload-rotate-two .6s linear infinite;-webkit-animation:cssload-rotate-two .6s linear infinite;-moz-animation:cssload-rotate-two .6s linear infinite;border-right:3px solid #11bcc2}.cssload-inner.cssload-three{right:0;bottom:0;animation:cssload-rotate-three .6s linear infinite;-o-animation:cssload-rotate-three .6s linear infinite;-ms-animation:cssload-rotate-three .6s linear infinite;-webkit-animation:cssload-rotate-three .6s linear infinite;-moz-animation:cssload-rotate-three .6s linear infinite;border-top:3px solid #0a1ac9}@keyframes cssload-rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-o-keyframes cssload-rotate-one{0%{-o-transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{-o-transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-ms-keyframes cssload-rotate-one{0%{-ms-transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{-ms-transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes cssload-rotate-one{0%{-webkit-transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{-webkit-transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-moz-keyframes cssload-rotate-one{0%{-moz-transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{-moz-transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes cssload-rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-o-keyframes cssload-rotate-two{0%{-o-transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{-o-transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-ms-keyframes cssload-rotate-two{0%{-ms-transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{-ms-transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes cssload-rotate-two{0%{-webkit-transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{-webkit-transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-moz-keyframes cssload-rotate-two{0%{-moz-transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{-moz-transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes cssload-rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@-o-keyframes cssload-rotate-three{0%{-o-transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{-o-transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@-ms-keyframes cssload-rotate-three{0%{-ms-transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{-ms-transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@-webkit-keyframes cssload-rotate-three{0%{-webkit-transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{-webkit-transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@-moz-keyframes cssload-rotate-three{0%{-moz-transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{-moz-transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}/*Fin Icono de Carga*/

</style>
<div class="container bg-white rounded">
   <h2 class="text-center">Información del mercado</h2>
    <div class="row mt-5 justify-content-center">
        <div class="col-md-6 col-sm-12">
           <div class="container">
               <div class="row">
                   <div class="col-md-10 col-sm-12">
                     <h2 class="mt-2 mb-0 text-right text-info">{{ $data['profile']->titulo }}</h2>
                      <img src="{{ $data['profile']->foto }}" alt="" class="img-fluid img-thumbnail rounded-circle shadow-2 mr-4" id="profile_img">
                       <p class="text-justify text-dark mt-4">{{ $data['profile']->descripcion }}</p>
                   </div>
               </div>
           </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="ribon-1">
              <div class="container">
               <div class="row no-gutters align-items-center">
                   <div class="col-sm-7">
                      <h4 class="m-0 text-white" id="estado"><i class="fa fa-flag fa-fw" aria-hidden="true"></i>
                          <?php
                            switch($data['profile']->estado){
                                case 0:
                                    echo "Rechazado";
                                    break;
                                case 1:
                                    echo "Vigente";
                                    break;
                                case 2:
                                    echo "En Ejecución";
                                    break;
                            }
                          ?>
                      </h4>
                      <h4 class="mt-2"><i class="fa fa-refresh fa-fw" aria-hidden="true"></i>
                      <span id="last_updated_date_lbl">
                      <?php
                        $date = date_create($data['profile']->updated_at);
                        echo date_format($date, 'Y/m/d');
                        ?>
                        </span>
                      </h4>
                   </div>
                   <div class="col-sm-4" id="change_button_wraper">
                       <a class="float-right btn btn-success shadow-2" data-toggle="modal" data-target="#change_state_mdl"><i class="fa fa-exchange fa-fw" aria-hidden="true"></i>Cambiar</a>
                   </div>
               </div>
                </div>
            </div>
            <div class="row d-flex flex-row-reverse" style="margin-top: 15%;">
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <p><a class="text-info" style="cursor:pointer;font-size: 5rem;" data-toggle="tooltip" data-placement="bottom" title="Comentarios" onclick="comentarios()"><i class="fa fa-comments"></i></a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-11 mt-5">
            <hr>
        </div>
        <div class="col-sm-12 mt-5">
            <h2>Archivos adjuntos</h2>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                       <h4>Imágenes <a class="btn btn-success btn-circle p-0" data-attach="1" id="attach_video_files_btn"><i class="fa fa-plus m-0 align-bottom" aria-hidden="true"></i></a></h4>
                        <!-- Swiper -->
                          <div class="swiper-container" id="attach_img_files">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide"><img src="/storage/nuevos_mercados/attachment_files/5af05d3213e85.jpg"></div>
                              <div class="swiper-slide">Slide 2</div>
                              <div class="swiper-slide">Slide 3</div>
                              <div class="swiper-slide">Slide 4</div>
                              <div class="swiper-slide">Slide 5</div>
                              <div class="swiper-slide">Slide 6</div>
                              <div class="swiper-slide">Slide 7</div>
                              <div class="swiper-slide">Slide 8</div>
                              <div class="swiper-slide">Slide 9</div>
                              <div class="swiper-slide">Slide 10</div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                          </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                       <h4>Archivos <a class="btn btn-success btn-circle p-0" data-attach="2" id="attach_video_files_btn"><i class="fa fa-plus m-0 align-bottom" aria-hidden="true"></i></a></h4>
                        <!-- Swiper -->
                          <div class="swiper-container" id="attach_file_files">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">Slide 1</div>
                              <div class="swiper-slide">Slide 2</div>
                              <div class="swiper-slide">Slide 3</div>
                              <div class="swiper-slide">Slide 4</div>
                              <div class="swiper-slide">Slide 5</div>
                              <div class="swiper-slide">Slide 6</div>
                              <div class="swiper-slide">Slide 7</div>
                              <div class="swiper-slide">Slide 8</div>
                              <div class="swiper-slide">Slide 9</div>
                              <div class="swiper-slide">Slide 10</div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                          </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                       <h4>Vídeos <a class="btn btn-success btn-circle p-0" data-attach="3" id="attach_video_files_btn"><i class="fa fa-plus m-0 align-bottom" aria-hidden="true"></i></a></h4>
                        <!-- Swiper -->
                          <div class="swiper-container" id="attach_video_files">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">Slide 1</div>
                              <div class="swiper-slide">Slide 2</div>
                              <div class="swiper-slide">Slide 3</div>
                              <div class="swiper-slide">Slide 4</div>
                              <div class="swiper-slide">Slide 5</div>
                              <div class="swiper-slide">Slide 6</div>
                              <div class="swiper-slide">Slide 7</div>
                              <div class="swiper-slide">Slide 8</div>
                              <div class="swiper-slide">Slide 9</div>
                              <div class="swiper-slide">Slide 10</div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cambiar estado-->
<div class="modal fade" id="change_state_mdl" tabindex="-1" role="dialog" aria-labelledby="change_state_modal_lbl" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="change_state_modal_lbl">Cambiar estado de mercado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="change_state_frm">
             <input type="hidden" name="new_market_id" value="{{ $data['profile']->id }}">
              <div class="form-group">
                <label for="estado_nuevo_mercado_select">Estado</label>
                <select class="form-control" id="estado_nuevo_mercado_select" name="estado_mercado">
                  <option value="2" <?php echo $selected = ($data['profile']->estado == 2)? 'selected': ''; ?> >En Ejecución</option>
                  <option value="0" <?php echo $selected = ($data['profile']->estado == 0)? 'selected': ''; ?> >Rechazado</option>
                  <option value="1" <?php echo $selected = ($data['profile']->estado == 1)? 'selected': ''; ?> >Vigente</option>
                </select>
              </div>
              <div class="form-group">
                <label for="justificacion">Justificación</label>
                <textarea class="form-control" id="justificacion" rows="5" name="justificacion_nuevo_mercado"></textarea>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success rounded" onclick="GuardarJustificacion({{ $data['profile']->id }})">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Subir archivos-->
<div class="modal fade" id="upload_file_mdl" tabindex="-1" role="dialog" aria-labelledby="upload_file_lbl" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-dark" id="upload_file_lbl">Cargar archivo adjunto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="new_market_attach_file" enctype="multipart/form-data">
                    <input type="hidden" id="type_attachment" name="tipo_archivo">
                    <input type="hidden" id="id_new_market_attach" name="id_new_market_attach" value="{{ $data['profile']->id }}">
                    <div class="container">
                        <div class="row">
                            <div class="col col-12">
                               <div class="form-group mt-4">
                                   <input type="text" class="form-control rounded" id="custom_name_file" name="custom_name_file" aria-describedby="emailHelp" placeholder="Nombre para el archivo">
                                </div>
                                <div class="form-group inputDnD">
                                    <label class="sr-only" for="inputFile">File Upload</label>
                                    <input type="file" class="form-control-file text-secondary font-weight-bold" id="inputFile" name="attach_new_market" onchange="readUrl(this)" data-title="Click acá o arrastre su archivo">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger rounded" data-dismiss="modal" onclick="$('#new_market_attach_file')[0].reset()">Cerrar</button>
                <button type="button" class="btn btn-success rounded" onclick="GuardarAdjunto()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="documento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="margin-top:3%; height: 700px;" id="ver_archivo_adjunto">
      <iframe src="https://docs.google.com/gview?url=https://smartessi.com/storage/nuevos_mercados/attachment_files/5af1a8a0e139b.docx&amp;embedded=true&amp;toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>
    </div>
  </div>
</div>

<template id="comentarios">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="shadow-3 img-redonda" src="/images/file/clientes/essi_admon.jpg" style="width: 60px;" data-imagen>
                <p class="h6 py-2 bg-info text-white" data-usuario>Administrador Sistema</p>
            </div>
            <div class="col-md-8">
                <p class="h5 text-justify" data-descripcion>Lorem ipsum dolor sit amet consectetur adipiscing elit, penatibus quam sagittis aenean vestibulum platea condimentum volutpat, mollis tristique commodo euismod nulla lectus. Gravida commodo faucibus orci sed sollicitudin ante viverra libero, maecenas vestibulum nibh iaculis potenti purus congue dictumst parturient, ultrices ad nunc augue phasellus scelerisque himenaeos. Vitae habitasse convallis porttitor felis inceptos tempor, dictum dignissim in montes vehicula dapibus nunc, ligula velit urna a conubia.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="h6 text-right"><strong><em data-fecha>Creado: 07 de Mayo 2018</em></strong></p>
                <hr size="2px" color="black" />
            </div>
        </div>
    </div>
</template>
<template id="icono_carga">
    <div class="cssload-loader">
        <div class="cssload-inner cssload-one"></div>
        <div class="cssload-inner cssload-two"></div>
        <div class="cssload-inner cssload-three"></div>
    </div>
</template>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        lista_archivos();
    });

    function slider(){
        var swiper_img = new Swiper('#attach_img_files', {
          pagination: {
            el: '.swiper-pagination',
          },
        });

        var swiper_file = new Swiper('#attach_file_files', {
          pagination: {
            el: '.swiper-pagination',
          },
        });

        var swiper_video = new Swiper('#attach_video_files', {
          pagination: {
            el: '.swiper-pagination',
          },
        });
    }

    /*Eventos*/

    $(document).on('click', '#attach_img_files_btn, #attach_file_files_btn, #attach_video_files_btn', function(){
        $("#type_attachment").val($(this).data('attach'));
        $("#upload_file_mdl").modal('show');
    });

    $(document).on('mouseenter','.ribon-1', function(){
        $("#change_button_wraper").show('fast');
    });

    $(document).on('mouseleave','.ribon-1', function(){
        $("#change_button_wraper").hide('fast');
    });

    $(document).on('show.bs.modal', '#change_state_mdl, #upload_file_mdl, #documento', function (e) {
        $(document).find('#main-content-wrapper').css("z-index", 'inherit');
    });

    $(document).on('hide.bs.modal', '#change_state_mdl, #upload_file_mdl, #documento', function (e) {
        $(document).find('#main-content-wrapper').css("z-index", 1);
    });


    /*Funciones*/

    /*Función de "Última actualización"*/
    function LastUpdate(id){
        $.ajax({
            url: ' {{ url("/nuevos-mercados/last-update") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: {new_market_id: id},
            success: function(suss){
                if(suss.res){
                    $("#last_updated_date_lbl").html(suss.updated_date);
                }else{
                    swal(suss.msj);
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /*Falta terminar*/
    function GetUpdatedData(id){
        $.ajax({
            url: ' {{ url("/") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: {new_market_id: id},
            success: function(suss){
                console.log(suss);
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /*Guardar cambio de estado y justificación de cambio*/
    function GuardarJustificacion(id){
        $.ajax({
            url: ' {{ url("/nuevos-mercados/cambiar-estado") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: $("#change_state_frm").serialize(),
            success: function(suss){
                if(suss.res){
                    $("#change_state_frm")[0].reset();
                    $("#change_state_mdl").modal('hide');
                    LastUpdate({{ $data['profile']->id }});
                }
                if(suss.res == 0){
                   swal('Error',suss.msj,'error');
                }
                if(suss.res == 1){
                   swal('Guardado',suss.msj,'success');
                    cambiar_estado(suss.estado);
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /**
     * Función para cambiar el estado
     * @param INT estado NUMERO DEL ESTADO Vigente-1 Rechazadas-0 En Ejecución-2
     */
    function cambiar_estado(estado){
        if(estado == 0){
            $('#estado').html(`<i class="fa fa-flag fa-fw" aria-hidden="true"></i> Rechazado`);
        }else if(estado == 1){
            $('#estado').html(`<i class="fa fa-flag fa-fw" aria-hidden="true"></i> Vigente`);
        }else{
            $('#estado').html(`<i class="fa fa-flag fa-fw" aria-hidden="true"></i> En Ejecución`);
        }
    }
 
    /*Leer uri del archivo para mostrar nombre en el Uploader*/
    function readUrl(input) {
      if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = (e) => {
          let imgData = e.target.result;
          let imgName = input.files[0].name;
          input.setAttribute("data-title", imgName);
        }
        reader.readAsDataURL(input.files[0]);
      }

    }

    /*Guardar los archivos adjuntos al nuevo mercado*/    
    function GuardarAdjunto(){
        swal({
              title: 'Cargando...',
              html:$('#icono_carga').html(),
              showCloseButton: true,
              showCancelButton: false,
              showConfirmButton: false,
              focusConfirm: false
            });
         var data = new FormData($("#new_market_attach_file")[0]);
            $.ajax({
            url: '{{ url("/nuevos-mercados/guardar-adjunto") }}',
            method: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 10000,
            data: data,
            success: function(suss){
                if(suss.res){
                    $("#new_market_attach_file")[0].reset();
                    $("#upload_file_mdl").modal('hide');
                    swal('Cargado',suss.msj,'success').then(function(){ location.reload(); });
                    document.getElementById('new_market_attach_file').reset();
                }
                if(suss.res == 0){
                   swal('Error',suss.msj,'error');
                }
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /*Función para listar los archivos adjuntos por tipos, del nuevo mercado*/
    function ListarAdjuntos(){  
        $.ajax({
            url: ' {{ url("/nuevos-mercados/listar-adjuntos") }}',
            method: 'POST', 
            dataType: 'json',
            cache: false,  
            timeout: 10000, 
            data: {id_new_market: {{ $data['profile']->id }} },  
            success: function(suss){
                if(suss.res){ 
                    console.log(suss.data);
                }
            },  
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            } 
        });
    }

    /**
     * Función para mostrar los comentarios realizados
     */
    function comentarios(){
        $.ajax({
            url: ' {{ url("/nuevos-mercados-acciones") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: {accion: 0, id_nuevo_mercado: <?=$data['profile']->id?> },
            success: function(suss){
                comentarios_ver(suss.data['comentarios']);
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /**
     * Ver los comentarios en un sweet Alert
     * @param JSON lista LISTADO DE COMENTARIOS REALIZADOS AL NUEVO MERCADO
     */
    function comentarios_ver(lista){
        swal({
              title: 'Comentarios',
              type: 'info',
              html:$('#icono_carga').html(),
              showCloseButton: true,
              showCancelButton: false,
              focusConfirm: false,
              confirmButtonText:
                '<i class="fa fa-close"></i> Cerrar'
            });
        $('.swal2-info').html('<i class="fa fa-comment"></i>');
        $('.swal2-show').css('width','800px');

        $("#swal2-content").html('');
        $.each(lista, function(key, value){
                 var template = $("#comentarios").html();
                 var $template = $(template);
                 $template.find("[data-imagen]").attr('src',value.FOTOGRAFIA);
                 $template.find("[data-usuario]").html(value.NOMBREUSUARIO);
                 $template.find("[data-descripcion]").html(value.comentario);
                 $template.find("[data-fecha]").html(fecha(value.CREADO));
                 $template.appendTo($("#swal2-content"));
       });
    }

        function fecha(fecha){
            var meses = ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            var res = fecha.split("-");
            var fechaformateada = res[2]+' de '+meses[parseInt(res[1])]+' '+res[0];
            return fechaformateada;
        }

     /**
     * Función para Traer los archivos adjuntos
     */

    function lista_archivos(){
        $.ajax({
            url: ' {{ url("/nuevos-mercados-acciones") }}',
            method: 'POST',
            dataType: 'json',
            cache: false,
            timeout: 10000,
            data: {accion: 1, id_nuevo_mercado: <?=$data['profile']->id?> },
            success: function(suss){
                $('#attach_img_files').html(suss.data['html_imagenes']);
                $('#attach_file_files').html(suss.data['html_archivos']);
                $('#attach_video_files').html(suss.data['html_videos']);
                slider();
            },
            error: function(err){
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /**
     * Función para ver el adjunto
     */
    function ver_adjunto(){
        var ruta = $('#attach_file_files .swiper-slide-active').attr('data-ruta');
        dir = "{{url('/')}}"+ruta;
        $('#documento').modal('show');
        contenido = `<iframe src="https://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>`;
		$("#ver_archivo_adjunto").html(contenido);
        $('.ndfHFb-c4YZDc-Wrql6b').css('display','none');
    }

</script>
@endsection
