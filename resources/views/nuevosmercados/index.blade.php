@extends('template.app')
@section('title', 'Nuevos Mercados')
@section('content')
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .card{border: none;}
    .reveal-overlay{position:absolute;width:0;height:0;margin:auto;top:0;left:0;bottom:0;right:0;background-color:#051d60a8;color:#FFF;z-index:99;-webkit-transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out;-ms-transition:all .3s ease-in-out;-o-transition:all .3s ease-in-out;transition:all .3s ease-in-out;}
    .reveal-overlay a,div{display:none;}
    .reveal-overlay a{color:#ffffff;text-decoration: none;cursor: pointer;}
    .card:hover a,div{display:block;}
    .card:hover .reveal-overlay .container{border:1px solid #e9ecef;}
    .card:hover .reveal-overlay{width:100%;height:100%;}
    .bottom-text{position:absolute;bottom:1rem;right:2rem;}
    .fab-float{cursor:pointer;position:fixed;width:60px;height:60px;bottom:40px;right:40px;background-color:#0C9;color:#fff;border-radius:50px;text-align:center;box-shadow:2px 2px 3px #999;transition:all .2s ease-in-out;}
    .fab-float:hover{transform:scale(1.1);}
    .my-float{margin-top:22px;font-size:18px;}
    .img-circle-avatar{width:100%;}
    .nav-tabs{border-bottom:2px solid #DDD;}
    .nav-tabs > li > a.active,.nav-tabs > li.active > a:focus,.nav-tabs > li.active > a:hover{border-width:0;}
    .nav-tabs > li > a{border:none;color:#666;}
    .nav-tabs > li > a.active,.nav-tabs > li > a:hover{border:none;color:#4285F4!important;background:transparent;}
    .nav-tabs > li > a::after{content:"";background:#4285F4;height:2px;position:absolute;width:100%;left:0;bottom:-1px;transition:all 250ms ease 0;transform:scale(0);}
    .nav-tabs > li > a.active::after,.nav-tabs > li:hover > a::after{transform:scale(1);}
</style>
<div class="container">
    <ul class="nav nav-tabs nav-fill" id="nuevo_mercado_tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link font-weight-bold active" href="#vigentes" data-toggle="tab" role="tab" aria-controls="vigentes" aria-selected="true" id="vigentes_tab" data-market="1">Vigentes</a>
        </li>
        <li class="nav-item">
            <a class="nav-link font-weight-bold" href="#rechazadas" data-toggle="tab" role="tab" aria-controls="rechazadas" aria-selected="false" id="rechazadas_tab" data-market="0">Rechazadas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link font-weight-bold" href="#aceptadas" data-toggle="tab" role="tab" aria-controls="aceptadas" aria-selected="false" id="aceptadas_tab" data-market="2">En ejecución</a>
        </li>
    </ul>

    <div class="tab-content" id="nuevo_mercado_tabs_content">
        <!--Contenido para Vigentes-->
        <div class="tab-pane fade show active" id="vigentes" role="tabpanel" aria-labelledby="vigentes_tab">
            <div class="container bg-white rounded p-4">
                <div class="row" id="markets_vigentes">
                </div>
            </div>
        </div>
        <!--Contenido para Aceptadas-->
        <div class="tab-pane fade" id="aceptadas" role="tabpanel" aria-labelledby="aceptadas_tab">
            <div class="container bg-white rounded p-4">
                <div class="row" id="markets_aceptados">
                </div>
            </div>
        </div>
        <!--Contenido para Rechazadas-->
        <div class="tab-pane fade bg-white" id="rechazadas" role="tabpanel" aria-labelledby="rechazadas_tab">
            <div class="container bg-white rounded p-4">
                <div class="row" id="markets_rechazadas">
                </div>
            </div>
        </div>
    </div>
</div>
<!--FAB button create-->
<a class="fab-float open-new" <?php if($data['permiso_crear']=="Si"){ ?>data-toggle="modal" data-target="#create_new_market" <?php } ?>>
    <i class="fa fa-plus my-float text-white"></i>
</a>

<!-- Modal -->
<div class="modal fade" id="create_new_market" tabindex="-1" role="dialog" aria-labelledby="create_new_market_label" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-dark" id="create_new_market_label">Crear nuevo mercado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="new_markets_frm">
                   <div class="dropzone" data-width="360" data-height="360" data-resize="true" data-ajax="false" style="width: 360px;height: 360px;">
                        <input type="file" name="market_thumb" required="required"/>
                    </div>
                    <div class="form-group">
                        <label for="nw_mkt_title" class="float-left font-weight-bold">Título</label>
                        <input type="email" class="form-control" id="nw_mkt_title" name="market_title" placeholder="Mercado de prueba" onkeyup="max_campo(this.value,100,'#texto_nw_mkt_title',this.id)">
                        <p class="text-right invisible" id="texto_nw_mkt_title"><a class="text-success"><i class="fa fa-pencil"></i>... 1/100</a></p>
                    </div>
                    <div class="form-group">
                        <label for="nw_mkt_description" class="float-left font-weight-bold">Descripción</label>
                        <textarea class="form-control" id="nw_mkt_description" name="market_description" rows="3" onkeyup="max_campo(this.value,1000,'#texto_nw_mkt_description',this.id)"></textarea>
                        <p class="text-right invisible" id="texto_nw_mkt_description"><a class="text-success"><i class="fa fa-pencil"></i>... 1/1000</a></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success rounded" onclick="CreateMarket()">Crear mercado</button>
            </div>
        </div>
    </div>
</div>
<template id="cargando">
    <div class="col-md-12 text-center">
        <div class="fa-3x">
           <img src="/images/carga.gif" style="width:80px;">
            <p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p>
        </div>
    </div>
</template>
@endsection
@section('scripts')
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script>
    $(document).ready(function() {
        ListMarkets();
    });

    $(document).on('click', '#vigentes_tab', function() {
        ListMarkets(1);
    });

    $(document).on('click', '#aceptadas_tab', function() {
        ListMarkets(2);
    });

    $(document).on('click', '#rechazadas_tab', function() {
        ListMarkets(0);
    });


    $("#create_new_market").on('shown.bs.modal', function() {
        $('.dropzone').html5imageupload();
    });

    /*Funcion para guardar un nuevo mercado*/
    function CreateMarket(){
        swal({
          title: 'Esta seguro?',
          text: "Desea guardar un nuevo mercado!",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Guardar!',
          cancelButtonText: 'Cancelar'
        }).then(function(){
          CreateMarket_save();
        })
    }
    /*Crear nuevo mercado*/
    function CreateMarket_save() {
        $.ajax({
            url: "{{ url('nuevos-mercados/crear') }}",
            method: 'POST',
            dataType: 'json',
            timeout: 10000,
            cache: false,
            data: $("#new_markets_frm").serialize(),
            success: function(suss) {
                if (suss.res) {
                    var type = $("#nuevo_mercado_tabs .nav-item .nav-link.active").data('market');
                    if (type == 1) {
                        ListMarkets();
                    }
                    $("#new_markets_frm")[0].reset();
                    $(".btn-del").trigger('click');
                    $("#create_new_market").modal('hide');
                }
                if(suss.res == 0){
                   swal('Error',suss.msj,'error');
                }
                if(suss.res == 1){
                   swal('Guardado',suss.msj,'success');
                }
            },
            error: function(err) {
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /*Listar mercados*/
    function ListMarkets(flag = 1) {
        html = $('#cargando').html();
        $("#markets_rechazadas").html(html);
        $("#markets_vigentes").html(html);
        $("#markets_aceptados").html(html);
        $.ajax({
            url: "{{ url('nuevos-mercados/listar') }}",
            method: 'POST',
            dataType: 'json',
            timeout: 10000,
            cache: false,
            data: {
                type_market: flag
            },
            success: function(suss) {
                if (suss.res) {
                    switch (flag) {
                        case 0:
                            $("#markets_rechazadas").html(suss.data);
                            break;
                        case 1:
                            $("#markets_vigentes").html(suss.data);
                            break;
                        case 2:
                            $("#markets_aceptados").html(suss.data);
                            break;
                    }
                }
            },
            error: function(err) {
                console.log(err);
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    /*Eliminar mercado*/
    function DelMarket(id) {
        <?php if($data['permiso_eliminar']=="Si"){ ?>
        swal({
            title: '¿Está seguro?',
            html: 'Esta acción eliminará el nuevo mercado y todos sus datos.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#bd2130',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            customClass: 'Eliminar'
        }).then(function() {
            /*Eliminar*/
            $.ajax({
                url: "{{ url('nuevos-mercados/eliminar') }}",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                cache: false,
                data: {market_id: id},
                success: function(suss) {
                    if (suss.res) {

                    }
                    if(suss.res == 1){
                        ListMarkets();
                       swal('Eliminado',suss.msj,'success');
                    }
                },
                error: function(err) {
                    console.log(err);
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        }, function() {
            /*Cancelar*/
        });
        <?php }else{ ?>
        no_permiso('No tiene permiso para eliminar');
        <?php } ?>
    }

    /*Función para resetear el formulario de Nuevos Mercados*/
    $(document).on('click','.open-new',function(){
        <?php if($data['permiso_crear']=="Si"){ ?>
        document.getElementById('new_markets_frm').reset();
        $('.btn-del').trigger('click');
        $('#texto_nw_mkt_title').html(`<a class="text-success"><i class="fa fa-pencil"></i>... 0/100</a>`);
        $('#texto_nw_mkt_description').html(`<a class="text-success"><i class="fa fa-pencil"></i>... 0/1000</a>`);
        <?php }else{ ?>
        no_permiso('No tiene permiso para crear');
        <?php } ?>
    });

    /**
     * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
     * @param STRING texto  TEXTO INGRESADO EN EL CAMPO
     * @param INT max_long NUMERO MAXIMO DE CARACTERES PARA EL CAMPO
     * @param STRING id  IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
     * @param STRING id_campo  IDENTIFICACION DEL CAMPO EDITADO
     */
    function max_campo(texto,max_long,id,id_campo){
        numeroCaracteres = texto.length;
        $(id).removeClass('invisible');
        if(numeroCaracteres > max_long){
            $('#'+id_campo).val(texto.substr(0,max_long));
            $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
        }else{
            $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
        }
    }

    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING msj MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection
