<!DOCTYPE html>
<html>
	<head>
		<title>smoothed line | amCharts</title>
		<style type="text/css">
			.amcharts-chart-div a{
		        display: none !important;
		      }
		</style>
		<meta name="description" content="chart created using amCharts live editor" />
		
		<!-- amCharts javascript sources -->
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/serial.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/dark.js"></script>
		
		<?php $sum=0; ?>
		<!-- amCharts javascript code -->
		<script type="text/javascript">
			var chart = AmCharts.makeChart("detallado-cumplimiento-semanal-iframe",
			{
			    "type": "serial",
			    "theme": "light",
			    "dataProvider": [
			    <?php foreach($cumplimiento_x_usr1 as $c_x_u1){ ?>
			          {
			              "name": "<?php echo $c_x_u1['nombre'] ?>",
			              "points": <?php echo $c_x_u1['semanal'] ?>,
			              "color": "<?php echo $c_x_u1['color'] ?>",
			              "bullet": "<?php echo $c_x_u1['foto'] ?>"
			          },
			        <?php } ?>
			    ],
			    "valueAxes": [{
			        "maximum": {{ $mayor_cumplimiento1+($mayor_cumplimiento1*0.1) }},
			        "minimum": 0,
			        "axisAlpha": 0,
			        "dashLength": 4,
			        "position": "left"
			    }],
			    "startDuration": 1,
			    "graphs": [{
			        "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]] %</b></span>",
			        "bulletOffset": 10,
			        "bulletSize": 52,
			        "colorField": "color",
			        "cornerRadiusTop": 8,
			        "customBulletField": "bullet",
			        "fillAlphas": 0.8,
			        "lineAlpha": 0,
			        "type": "column",
			        "valueField": "points"
			    }],
			    "marginTop": 0,
			    "marginRight": 0,
			    "marginLeft": 0,
			    "marginBottom": 0,
			    "autoMargins": false,
			    "categoryField": "name",
			    "categoryAxis": {
			        "axisAlpha": 0,
			        "gridAlpha": 0,
			        "inside": true,
			        "tickLength": 0
			    },
			    "export": {
			      "enabled": false
			     }
			});
		</script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-12" style="background-color: #e8e8e8;">
				<div id="detallado-cumplimiento-semanal-iframe" style="width: 100%; height: 400px; background-color: #e8e8e8;" ></div>
			</div>
		</div>
	</body>
</html>