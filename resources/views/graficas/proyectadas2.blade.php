<!DOCTYPE html>
<html>
	<head>
		<title>bar and line | amCharts</title>
		<style type="text/css">
			.amcharts-chart-div a{
		        display: none !important;
		      }
		</style>
		<meta name="description" content="chart created using amCharts live editor" />
		
		<!-- amCharts javascript sources -->
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="https://cdn.amcharts.com/lib/3/serial.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/themes/dark.js"></script>
		

		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"rotate": true,
					"autoMarginOffset": 40,
					"maxSelectedSeries": {{count($nombre_productos_referencia)}},
					"marginRight": 60,
					"marginTop": 60,
					"startDuration": 1,
					"startEffect": "easeOutSine",
					"backgroundColor": "#02141A",
					"borderColor": "#FFFFFF",
					"color": "#FFC64F",
					"fontSize": 13,
					"theme": "dark",
					"categoryAxis": {
						"gridPosition": "start",
						"axisColor": "#00BFFF",
						"fillColor": "#FF8000"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "",
							"title": "Equipo: ",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisColor": "#00BFFF",
							"fillColor": "#00BFFF",
							"gridColor": "#00BFFF",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": [
						<?php 
							foreach($productos_referencia as $clave=>$valor){ ?>
							{
								"category": "{{$nombrecompleto_productos_referencia[$clave]}}",
								"column-1": "{{$productos_referencia[$clave]}}"
							},
						<?php
							}
						?>
					]
				}
			);
		</script>
	</head>
	<body>		
		<div class="row">
			<div class="col-md-12" style="background-color: #02141A;">
				<a style="color: #fff; font-size: 20px;"><center>Proyección de venta de equipos</center></a>
				<div id="chartdiv" style="width: 100%; height: calc(23.92px*{{count($nombre_productos_referencia)}}); background-color: #02141A;" ></div>
			</div>
		</div>
	</body>
</html>
