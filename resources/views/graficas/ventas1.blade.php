<!DOCTYPE html>
<html>
	<head>
		<title>chalk-column | amCharts</title>
		<style type="text/css">
			.amcharts-chart-div a{
		        display: none !important;
		      }
		</style>
		<meta name="description" content="chart created using amCharts live editor" />
		
		<!-- amCharts javascript sources -->
		<script src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
		<script src="{{ url('/') }}/components/amchart/js/serial.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/light.js"></script>
		

		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"columnSpacing": 3,
					"columnWidth": 0.69,
					"autoMarginOffset": 40,
					"marginRight": 70,
					"marginTop": 70,
					"startDuration": 1,
					"startEffect": "easeOutSine",
					"backgroundColor": "#001A4A",
					"color": "#E7E7E7",
					"fontSize": 12,
					"theme": "light",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonColor": "#00BFFF",
							"balloonText": "[[title]]  [[category]]:[[value]]",
							"color": "#FFFFFF",
							"fillAlphas": 0.9,
							"id": "AmGraph-1",
							"title": "",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"animationPlayed": true,
							"balloonColor": "undefined",
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"behindColumns": true,
							"fillAlphas": 0.9,
							"id": "AmGraph-2",
							"title": "graph 2",
							"type": "column",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisColor": "#0000FF",
							"axisThickness": 2,
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": [
					<?php 
                    	if(isset($nombre_pais_vendida)){
						foreach($nombre_pais_vendida as $nombre){ ?>
						{
							"category": "{{$nombre}}",
							"column-1": "{{$pais_vendida[$nombre]}}"
						},
					<?php
						} }
					?>
						
					]
				}
			);
		</script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-12" style="background-color: #001A4A;">
				<a style="color: #fff; font-size: 20px;"><center>Ventas por paises <?php echo date('Y') ?></center></a>
				<div id="chartdiv" style="width: 100%; height: 400px; background-color: #001A4A;" ></div>
			</div>
		</div>
	</body>
</html>
