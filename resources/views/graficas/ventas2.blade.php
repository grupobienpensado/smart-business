<!DOCTYPE html>
<html>
	<head>
		<title>bar and line | amCharts</title>
		<style type="text/css">
			.amcharts-chart-div a{
		        display: none !important;
		      }
		</style>
		<meta name="description" content="chart created using amCharts live editor" />
		
		<!-- amCharts javascript sources -->
		<script src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
		<script src="{{ url('/') }}/components/amchart/js/serial.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/dark.js"></script>

		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"rotate": true,
					"autoMarginOffset": 40,
					"maxSelectedSeries": @if(isset($nombre_productos_referencia)) {{count($nombre_productos_referencia)}} @else 0 @endif,
					"marginRight": 60,
					"marginTop": 60,
					"startDuration": 1,
					"startEffect": "easeOutSine",
					"backgroundColor": "#02141A",
					"borderColor": "#FFFFFF",
					"color": "#00BFFF",
					"fontSize": 13,
					"theme": "dark",
					"categoryAxis": {
						"gridPosition": "start",
						"axisColor": "#00BFFF",
						"fillColor": "#FF8000"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"labelText": "",
							"title": "Equipo: ",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisColor": "#00BFFF",
							"fillColor": "#00BFFF",
							"gridColor": "#00BFFF",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": [
					<?php 
                        if(isset($productos_referencia)){
						foreach($productos_referencia as $clave=>$valor){ ?>
						{
							"category": "{{$nombrecompleto_productos_referencia[$clave]}}",
							"column-1": "{{$productos_referencia[$clave]}}"
						},
					<?php
						}
                        }
					?>
						
					]
				}
			);
		</script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-12" style="background-color: #02141A;">
				<a style="color: #fff; font-size: 20px;"><center>Equipos vendidos <?php echo date('Y') ?></center></a>
				<div id="chartdiv" style="width: 100%; @if(isset($nombre_productos_referencia)) height: calc(63.25px*{{count($nombre_productos_referencia)}}); @else height:230px; @endif background-color: #02141A;" ></div>
			</div>
		</div>
	</body>
</html>
