<!DOCTYPE html>
<html>
	<head>
		<title>smoothed line | amCharts</title>
		<style type="text/css">
			.amcharts-chart-div a{
		        display: none !important;
		      }
		</style>
		<meta name="description" content="chart created using amCharts live editor" />
		
		<!-- amCharts javascript sources -->
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/serial.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/dark.js"></script>
		
		<?php $sum=0; ?>
		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"columnWidth": 0,
					"autoMarginOffset": 40,
					"marginRight": 60,
					"marginTop": 60,
					"zoomOutButtonColor": "#FFC800",
					"startDuration": 1,
					"startEffect": "easeInSine",
					"backgroundColor": "#252627",
					"color": "#FFC800",
					"theme": "dark",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"animationPlayed": true,
							"balloonColor": "#FFC800",
							"balloonText": "[[title]] de [[category]]:[[value]]",
							"behindColumns": true,
							"bullet": "diamond",
							"bulletBorderThickness": 1,
							"bulletColor": "#00FF30",
							"bulletSize": 13,
							"color": "#FFC800",
							"id": "AmGraph-1",
							"lineAlpha": 1,
							"lineColor": "#00CDFF",
							"title": "Mes ",
							"type": "smoothedLine",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 0.1,
						"fadeOutDuration": 0.06
					},
					"titles": [],
					"dataProvider": [
						{
							"category": "Enero",
							"column-1": "<?php if(isset($meses_ventas[1])){ $sum+=$meses_ventas[1]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Febrero",
							"column-1": "<?php if(isset($meses_ventas[2])){ $sum+=$meses_ventas[2]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Marzo",
							"column-1": "<?php if(isset($meses_ventas[3])){ $sum+=$meses_ventas[3]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Abril",
							"column-1": "<?php if(isset($meses_ventas[4])){ $sum+=$meses_ventas[4]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Mayo",
							"column-1": "<?php if(isset($meses_ventas[5])){ $sum+=$meses_ventas[5]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Junio",
							"column-1": "<?php if(isset($meses_ventas[6])){ $sum+=$meses_ventas[6]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Julio",
							"column-1": "<?php if(isset($meses_ventas[7])){ $sum+=$meses_ventas[7]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Agosto",
							"column-1": "<?php if(isset($meses_ventas[8])){ $sum+=$meses_ventas[8]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Septiembre",
							"column-1": "<?php if(isset($meses_ventas[9])){ $sum+=$meses_ventas[9]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Octubre",
							"column-1": "<?php if(isset($meses_ventas[10])){ $sum+=$meses_ventas[10]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Noviembre",
							"column-1": "<?php if(isset($meses_ventas[11])){ $sum+=$meses_ventas[11]; echo $sum; }else{ echo $sum; } ?>"
						},
						{
							"category": "Diciembre",
							"column-1": "<?php if(isset($meses_ventas[12])){ $sum+=$meses_ventas[12]; echo $sum; }else{ echo $sum; } ?>"
						}
					]
				}
			);
		</script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-12" style="background-color: #252627;">
				<a style="color: #fff; font-size: 20px;"><center>Ventas Proyectadas {{date("Y")}}</center></a>
				<div id="chartdiv" style="width: 100%; height: 400px; background-color: #252627;" ></div>
			</div>
		</div>
	</body>
</html>