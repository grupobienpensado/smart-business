<!DOCTYPE html>
<html>
	<head>
		<title>chalk-column | amCharts</title>
		<style type="text/css">
			.amcharts-chart-div a{
		        display: none !important;
		      }
		</style>
		<meta name="description" content="chart created using amCharts live editor" />
		
		<!-- amCharts javascript sources -->
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/amcharts.js"></script>
		<script type="text/javascript" src="{{ url('/') }}/components/amchart/js/serial.js"></script>
		

		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"columnSpacing": 3,
					"columnWidth": 0.68,
					"autoMarginOffset": 40,
					"marginRight": 70,
					"marginTop": 70,
					"plotAreaBorderColor": "#FFFFFF",
					"startDuration": 1,
					"startEffect": "easeInSine",
					"accessibleTitle": "",
					"backgroundColor": "#212121",
					"borderColor": "#FFFFFF",
					"color": "#E7E7E7",
					"fontSize": 12,
					"theme": "default",
					"categoryAxis": {
						"gridPosition": "start",
						"axisColor": "#00BFFF",
						"fillColor": "#00BFFF",
						"title": "",
						"titleBold": false,
						"titleColor": "#00BFFF",
						"titleFontSize": 19,
						"titleRotation": 0
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonColor": "#00BFFF",
							"balloonText": "[[title]]  [[category]]:[[value]]",
							"color": "#FFFFFF",
							"fillAlphas": 0.9,
							"id": "AmGraph-1",
							"title": "",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"animationPlayed": true,
							"balloonColor": "#FFFFFF",
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"behindColumns": true,
							"fillAlphas": 0.9,
							"id": "AmGraph-2",
							"stepDirection": "left",
							"title": "graph 2",
							"type": "column",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"axisThickness": 2,
							"tickLength": 4,
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {
						"shadowColor": "#FFFFFF"
					},
					"titles": [],
					"dataProvider": [
						<?php 
						foreach($nombre_pais_vendida as $nombre){ ?>
						{
							"category": "{{$nombre}}",
							"column-1": "{{$pais_vendida[$nombre]}}"
						},
					<?php
						}
					?>
					]
				}
			);
		</script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-12" style="background-color: #212121;">
				<a style="color: #fff; font-size: 20px;"><center>Proyección de ventas por paises</center></a>
				<div id="chartdiv" style="width: 100%; height: 400px; background-color: #212121;" ></div>
			</div>
		</div>
	</body>
</html>