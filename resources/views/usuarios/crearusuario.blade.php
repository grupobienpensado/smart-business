<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Registrar Usuario')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
<link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<link href="{{ url('/') }}/css/plugins/media-hover-effects.css" type="text/css" rel="stylesheet" media="screen,projection">
<style type="text/css">
.form-control-sm, .btn-sm{
    z-index: inherit !important;
}

.content {
    text-align: -webkit-auto;
}

html {
    font-size: 13px !important;
}

form {
    font-size: 13px !important;
}
.tamano{
    height: 616px;
    width: 206px !important;
}
.titulos{
    background-color: #ccc;
    color: #000;
    padding: 3px;
}
.titulos label{
    color: #000;
    margin-bottom: 0 !important;
}
.btn-add{
    position: absolute;
    vertical-align: middle;
    top: 20px;
}
.table td{
    line-height: 0 !important;
    border: 0 !important;
}
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}
.form-control3 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
}
#map{
    height: 340px;
    width: 100%;
    top: 10px;
}

.bandera-principal{
    width: 100px;
    height: 100px;
    background-size: cover;
    background-repeat: no-repeat;
    display: inline-block;
    margin: 10px;
}
.imagen-perfil{
    height: 300px;
    width: 300px;
    max-height: 300px;
    min-height: 300px;
}
.foto-completa{
    height: 616px;
    width: 300px;
}
.mes-meta{
    line-height: 2 !important;
}
</style>
@php
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
@endphp
<div class="widget">
    <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
    <form method="POST" action="{{ url('funcionariosave') }}"  enctype="multipart/form-data">
        <div class="panel-title">
            <h2> Crear nuevo usuario</h2>
        </div>
        <hr>
        {{ csrf_field() }}
        <div class="form-group row">
            <div class="col-3" style="text-align:center; justify-content: center;">
                  <div class="dropzone imagen-perfil" data-width="200" data-height="200" data-ajax="false" data-originalsave="true" style="width: 200px !important;height: 200px !important;max-height: 200px !important;min-width: 200px !important;min-height: 200px !important;">
                    <input type="file" name="foto" accept="image/gif, image/jpeg, image/png">
                  </div>
                <hr />
                  <div class="dropzone foto-completa" data-width="206" data-height="616" data-ajax="false" data-originalsave="true" style="width: 206px !important;height: 616px !important;max-height: 616px !important;min-width: 206px !important;min-height: 616px !important;">
                    <input type="file" name="principal" accept="image/gif, image/jpeg, image/png">
                  </div>
            </div>
            <div class="col-9">
                <div class="titulos"><label>Información Personal</label></div>
                <div class="form-group row">
                    <div class="col-6">
                        <label for="nombres" class="col-form-label">Nombres</label>
                        <input class="form-control form-control-sm" type="text" name="nombres" placeholder="Por favor ingrese los nombre del usuario">
                    </div>
                    <div class="col-6">
                        <label for="apellidos" class="col-form-label">Apellidos</label>
                        <input class="form-control form-control-sm" type="text" name="apellidos" placeholder="Por favor ingrese los apellidos del usuario">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-4">
                        <label for="tipo_documento" class="col-form-label">Tipo documento</label>
                        <select name="tipo_documento" class="form-control">
                          <option>Seleccione una opción</option>
                          <option value="Cedula Ciudadania">Cedula Ciudadania</option>
                          <option value="Pasaporte">Pasaporte</option>
                        </select>
                    </div>
                    <div class="col-4">
                        <label for="empresa_sede_id" class="col-form-label">Numero Documento</label>
                        <input class="form-control form-control-sm" type="text" name="documento" placeholder="Por favor ingrese el numero de identificacion">
                    </div>
                    <div class="col-4">
                        <label for="fecha_naicmiento" class="col-form-label">Fecha nacimiento</label>
                        <input class="form-control form-control-sm" type="date" name="fecha_nacimiento" placeholder="Por favor ingrese la fecha de nacimiento">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-4">
                        <label for="tipo_sangre" class="col-form-label">Tipo de sangre</label>
                        <select name="tipo_sangre" class="form-control">
                          <option value="">Seleccione una opcion</option>
                          <option value="O+">O+</option>
                          <option value="O-">O-</option>
                          <option value="A+">A+</option>
                          <option value="A-">A-</option>
                          <option value="B+">B+</option>
                          <option value="B-">B-</option>
                          <option value="AB+">AB+</option>
                          <option value="AB-">AB-</option>
                        </select>
                    </div>
                    <div class="col-4">
                        <label for="telefono" class="col-form-label">Telefono personal</label>
                        <input class="form-control form-control-sm" type="text" name="telefono_personal" placeholder="Por favor ingrese el numero de telefono">
                    </div>
                    <div class="col-4">
                        <label for="correo" class="col-form-label">Telefono corporativo</label>
                        <input class="form-control form-control-sm" type="text" name="telefono_corporativo" placeholder="Por favor ingrese el correo">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-4">
                        <label for="telefono" class="col-form-label">Correo personal</label>
                        <input class="form-control form-control-sm" type="text" name="correo_personal" placeholder="Por favor ingrese el numero de telefono">
                    </div>
                    <div class="col-4">
                        <label for="correo" class="col-form-label">Correo corporativo</label>
                        <input class="form-control form-control-sm" type="text" name="correo_corporativo" placeholder="Por favor ingrese el correo">
                    </div>
                    <div class="col-4">
                        <label for="profesion" class="col-form-label">Profesión</label>
                        <input class="form-control form-control-sm" type="text" name="profesion" placeholder="Por favor ingrese la profesión">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-4">
                        <label for="pais" class="col-form-label">Pais nacimiento</label>
                        <select name="pais_nacimiento" class="form-control" id="pais"></select>
                    </div>
                    <div class="col-4">
                        <label for="departamento" class="col-form-label">Departamento nacimiento</label>
                        <select name="departamento_nacimiento" class="form-control" id="estados"></select>
                    </div>
                    <div class="col-4">
                        <label for="ciudad" class="col-form-label">Ciudad nacimiento</label>
                        <select name="ciudad_nacimiento" class="form-control" id="ciudad"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 ">
                        <label for="pais_residencia" class="col-form-label">Descripcion de perfil</label>
                        <textarea class="form-control" type="text" name="descripcion" rowspan="6"></textarea>
                    </div>
                </div>
                <div class="titulos"><label>Información Ubicación</label></div>
                <div class="form-group row">
                    <div class="col-4">
                        <label for="pais_residencia" class="col-form-label">Pais residencia</label>
                        <select name="pais_residencia" class="form-control" id="pais_residencia"></select>
                    </div>
                    <div class="col-4">
                        <label for="departamento_residencia" class="col-form-label">Departamento residencia</label>
                        <select name="departamento_residencia" class="form-control" id="estados_residencia"></select>
                    </div>
                    <div class="col-4">
                        <label for="ciudad_residencia" class="col-form-label">Ciudad residencia</label>
                        <select name="ciudad_residencia" class="form-control" id="ciudad_residencia"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <label for="direccion" class="col-form-label">Direccion de residencia</label>
                        <input class="form-control form-control-sm" type="text" name="direcion" placeholder="Por favor ingrese la direccion de residencia">
                    </div>
                    <div class="col-12">
                     <div id="map"></div>
                      <div class="coordinates">
                        <em class="lat">Latitud</em>
                        <em class="lon">Longitud</em>
                        <input type="text" id="lat" name="lat">
                        <input type="text" id="lng" name="lng">
                      </div>
                      </div>
                </div>
                </div>
            <div class="col-md-12">
                <div class="titulos"><label>Vinculación ESSI</label></div>
                <div class="form-group row">
                    <div class="col-6">
                        <label for="pais_residencia" class="col-form-label">Fecha vinculación a ESSI</label>
                        <input class="form-control form-control-sm" type="date" name="fecha_vinculacion">
                    </div>
                    <div class="col-6">
                        <label for="cargo" class="col-form-label">Cargo</label>
                        <select name="cargo" class="form-control">
                          <option>Seleccione una opción</option>
                          @foreach($data['cargos'] as $cargo)
                          <option value="<?=$cargo->cargo?>"><?=$cargo->cargo?></option>
                          @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="titulos"><label>Contacto Emergencia</label></div>
                <div class="form-group row">
                    <div class="col-4">
                        <label for="pais_residencia" class="col-form-label">Parentesco</label>
                        <input class="form-control form-control-sm" type="text" placeholder="Digite aqui el parentesco" name="parentesco_emergencia">
                    </div>
                    <div class="col-4">
                        <label for="pais_residencia" class="col-form-label">Nombre</label>
                        <input class="form-control form-control-sm" type="text" placeholder="Digite aqui el nombre" name="nombre_emergencia" >
                    </div>
                    <div class="col-4">
                        <label for="pais_residencia" class="col-form-label">Telefono</label>
                        <input class="form-control form-control-sm" type="text" name="telefono_emergencia" placeholder="Digite aqui el telefono de contacto" name="telefono_emergencia">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="titulos"><label>Información Familiar</label></div>
                <div class="form-group row " id="familia">
                    <div class="col-6">
                        <label for="cargo" class="col-form-label">Estado Civil</label>
                        <select name="estado_civil" class="form-control familia">
                          <option value="Soltero">Soltero</option>
                          <option value="Casado">Casado</option>
                          <option value="Unión Libre">Unión Libre</option>
                          <option value="Divorciado/a">Divorciado/a</option>
                          <option value="Viudo/a">Viudo/a</option>
                        </select>
                    </div>
                    <div class="col-6">
                        <label for="pais_residencia" class="col-form-label">Numero de hijos</label>
                        <input class="form-control form-control-sm hijos" type="number" name="hijos" placeholder="Digite aqui el numero de hijos">
                    </div>
                    <div class="col-6 conyugue" style="display: none">
                        <label for="pais_residencia" class="col-form-label">Nombre del conyugue</label>
                        <input class="form-control form-control-sm" type="hidden" name="familia[0][parentesco]" value="Conyugue">
                        <input class="form-control form-control-sm" type="text" name="familia[0][nombre]" placeholder="Digite nombre del conyugue">
                    </div>
                    <div class="col-6 conyugue" style="display: none">
                        <label for="pais_residencia" class="col-form-label">Fecha de nacimiento</label>
                        <input class="form-control form-control-sm" type="date" name="familia[0][fecha_nacimiento]" placeholder="Digite fecha de nacimiento" >
                    </div>
                </div>
                <div class="form-group row " id="hijos">
                </div>
            </div>
            <div class="col-md-12">
                <div class="titulos"><label>Estudios Realizados</label></div>
                <div class="form-group row" id="estudios">
                    <div class="col-4 ">
                        <label for="pais_residencia" class="col-form-label">Titulo Obtenido</label>
                        <input class="form-control form-control-sm" type="text" name="educacion[0][titulo]" placeholder="Digite titulo obtenido">
                    </div>
                    <div class="col-3">
                        <label for="pais_residencia" class="col-form-label">Año</label>
                        <input class="form-control form-control-sm" type="number" name="educacion[0][anio]" placeholder="Digite año de culminación">
                    </div>
                    <div class="col-4 ">
                        <label for="pais_residencia" class="col-form-label">Institución</label>
                        <input class="form-control form-control-sm" type="text" name="educacion[0][institucion]" placeholder="Digite institucion">
                    </div>
                    <div class="col-1 relativo">
                        <a class="btn btn-primary btn-sm btn-add" href="javascript:agregar()"><i class="fa fa-plus white"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="titulos"><label>Salarios Mensuales</label></div>
                <div class="form-group row">
                    <div class="col-2 ">
                        <label for="pais_residencia" class="col-form-label">Salario Basico</label>
                        <input class="form-control form-control-sm dinero" type="text" name="salario_basico" placeholder="Digite salario basico mensual">
                    </div>
                    <div class="col-2">
                        <label for="pais_residencia" class="col-form-label">Bonificación Fija</label>
                        <input class="form-control form-control-sm dinero" type="text" name="bonificacion_fija" placeholder="Digite bonificación fija mensual">
                    </div>
                    <div class="col-2">
                        <label for="pais_residencia" class="col-form-label">Aux. Transporte</label>
                        <input class="form-control form-control-sm dinero" type="text" name="aux_transporte" placeholder="Digite auxilio de transporte">
                    </div>
                    <div class="col-2">
                        <label for="pais_residencia" class="col-form-label">Aux. Alimentación</label>
                        <input class="form-control form-control-sm dinero" type="text" name="aux_alimentacion" placeholder="Digite auxilio de alimentación">
                    </div>
                    <div class="col-2">
                        <label for="pais_residencia" class="col-form-label">Aux. Vivienda</label>
                        <input class="form-control form-control-sm dinero" type="text" name="aux_vivienda" placeholder="Digite auxilio de vivienda">
                    </div>
                    <div class="col-2">
                        <label for="pais_residencia" class="col-form-label">Otros Auxilios</label>
                        <input class="form-control form-control-sm dinero" type="text" name="otros_auxilios" placeholder="Digite otros auxilios">
                    </div>
                </div>
            </div>
             <div class="col-md-12">
                <div class="titulos"><label>Pais Principal</label></div>
                <div class="form-group row">
                    <div class="col-3">
                        <label for="principal" class="col-form-label">Seleccione un pais principal</label>
                        <select name="principal" class="form-control pais_principal">
                          <option>Seleccione una opción</option>
                          @foreach($paises as $pais)
                          @if($pais->imagen!="")
                          <option value="{{$pais->name}}">{{$pais->name}}</option>
                          @endif
                          @endforeach
                        </select>
                        <div class="bandera-principal" style="display: none" id="imagen">

                        </div>
                    </div>
                    <div class="col-6">
                        <label for="alternas" class="col-form-label" style="width: 100%">Seleccione paises alternativos</label>
                        <select name="alternas[]" multiple="multiple" id="multiselect3-all" class="multiselect form-control form-control-sm pais_alterno">
                          @foreach($paises as $pais)
                          @if($pais->imagen!="")
                          <option value="{{$pais->name}}">{{$pais->name}}</option>
                          @endif
                          @endforeach
                        </select>
                        <div style="display: none" id="imagenes">

                        </div>
                    </div>
                    <div class="col-3">
                        <label>Color Predeterminado</label>
                        <input id="mycolor" type="text" value="#B6BD79" class="color form-control" placeholder="Seleccione un color" name="color">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="titulos"><label>Metas</label></div>
                <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                <thead>
                    <tr>
                        <th></th>
                        <th>Oportunidades Identificadas</th>
                        <th>Montos Identificados</th>
                        <th>Negocios Cerrados</th>
                        <th>Montos Cerrados</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i=0;$i<12;$i++)
                    <tr>   <input type="hidden" name="meta[{{$i}}][mes]" value="{{$i+1}}">
                        <td class="mes-meta">{{$meses[$i]}}</td>
                        <td><input class="form-control form-control3 form-control-sm" placeholder="Digite aqui la cantidad de oportunidades" type="number" name="meta[{{$i}}][oportunidad_identificada]"></td>
                        <td><input class="form-control form-control2 form-control-sm dinero" placeholder="$ 0" type="text" name="meta[{{$i}}][monto_identificado]"></td>
                        <td><input class="form-control form-control3 form-control-sm" placeholder="Digite aqui la cantidad de oportunidades" type="number" name="meta[{{$i}}][oportunidad_concretada]"></td>
                        <td><input class="form-control form-control2 form-control-sm dinero" placeholder="$ 0" type="text" name="meta[{{$i}}][monto_concretado]"></td>
                    </tr>
                    @endfor
                </tbody>
                </table>
            </div>
        </div>
        <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
    </form>
</div>
@endsection

@section('scripts')

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>
<script src="{{ url('/') }}/assets/js/bootstrap-multiselect.js"></script>
<script src="{{ url('/') }}/assets/js/tinyColorPicker-master/colors.js"></script>
<script src="{{ url('/') }}/assets/js/tinyColorPicker-master/jqColorPicker.min.js"></script>
<script src="{{ url('/') }}/js/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
var marker;
var map;
var geocoder;
var infoWindow = {};
m=1;
var pais = "";
    var departamento = "";
    var ciudad = "";
var direccion = "";

$(function(){
  $('.color').colorPicker(); // that's it
});

$("body").on("change",".pais_principal",function(e){
    nombre=$(this).val()
    /*if(nombre!=""){

        nombre=nombre.toLowerCase().replace(" ","_")+".png";
        $("#imagen").show();
        $("#imagen").attr("style","background-image:url({{url('/')}}/images/icons/"+nombre+")");
    }*/
    $.ajax({
        url: "{{ url('/') }}/consultarpais/"+nombre,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $("#imagen").show();
            $("#imagen").attr("style","background-image:url({{url('/')}}/images/icons/"+e.imagen+")");
        }
      });
})

$("body").on("change",".pais_alterno",function(e){
    nombre=$(this).val();
    $("#imagenes").html("");
    /*if(nombre!=""){
         $("#imagenes").show()
        for(i=0; i<nombre.length;i++){
            nombre1=nombre[i].toLowerCase().replace(" ","_")+".png";
            $("#imagenes").append('<div class="bandera-principal" style="background-image:url({{url('/')}}/images/icons/'+nombre1+')"></div>')
        }
    }else{
        $("#imagenes").hide()
    }*/

    if(nombre!=""){
        $("#imagenes").show();
        for(i=0; i<nombre.length;i++){
            $.ajax({
                url: "{{ url('/') }}/consultarpais/"+nombre[i],
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                   $("#imagenes").append('<div class="bandera-principal" style="background-image:url({{url('/')}}/images/icons/'+e.imagen+')"></div>')
                }
            });
        }
    }else{
        $("#imagenes").hide()
    }


})

$('#multiselect3-all').multiselect({
    includeSelectAllOption: true,
    buttonClass: 'btn btn-primary'
});
function agregar(){
    //codeAddress(dir);
    $("#estudios").append('<div class="col-4 col'+m+'">'
                +'<label for="pais_residencia" class="col-form-label">Titulo Obtenido</label>'
                +'<input class="form-control form-control-sm" type="text" name="educacion['+m+'][titulo]">'
            +'</div>'
            +'<div class="col-3 col'+m+'">'
                +'<label for="pais_residencia" class="col-form-label">Año</label>'
                +'<input class="form-control form-control-sm" type="number" name="educacion['+m+'][anio]">'
            +'</div>'
            +'<div class="col-4 col'+m+'">'
                +'<label for="pais_residencia" class="col-form-label">Institución</label>'
                +'<input class="form-control form-control-sm" type="text" name="educacion['+m+'][institucion]">'
            +'</div>'
            +'<div class="col-1 relativo col'+m+'">'
                +'<a class="btn btn-danger btn-sm btn-add" href="javascript:eliminar('+m+')"><i class="fa fa-trash white"></i></a>'
            +'</div>');
    m++;
}

function eliminar(id){
    $(".col"+id).remove();
}
$("body").on("change",".familia",function(e){
    if($(this).val()!="Divorciado/a"&&$(this).val()!="Viudo/a"&&$(this).val()!="Soltero"){
        $(".conyugue").show();
    }else{
        $(".conyugue").hide();
    }
});

$("body").on("keyup",".hijos",function(){
    $("#hijos").html("");
    for(i=0;i<$(this).val();i++){
        $("#hijos").append('<div class="col-6">'
                    +'<input type="hidden" value="Hijo" name="hijos_inf['+i+'][parentesco]">'
                    +'<label for="pais_residencia" class="col-form-label">Nombre del hijo</label>'
                    +'<input class="form-control form-control-sm" type="text" name="hijos_inf['+i+'][nombre]">'
                +'</div>'
                +'<div class="col-6">'
                    +'<label for="pais_residencia" class="col-form-label">Fecha de nacimiento</label>'
                    +'<input class="form-control form-control-sm" type="date" name="hijos_inf['+i+'][fecha_nacimiento]">'
                +'</div>');
    }

});
 $('.dropzone').html5imageupload();
    $("#logo").fileinput({
        language: "es",
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="fa fa-times" aria-hidden="true"></i> Restablecer cambios',
        removeTitle: 'Cancelar o restablecer cambios',
        elErrorContainer: '#logo-errors',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ url('/') }}/images/file/avatar/man.svg" alt="Logo de la empresa" style="width:160px"><h6 class="text-muted">Clic para seleccionar foto</h6>',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

    i=1;
    j=0;
    k=0;
    h=0;

    $(function () {
        // inicializamos el plugin
        $(".dinero").maskMoney();
        $('#pais').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Pais",
            pais: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("pais") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#estados').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Departamento",
            estados: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("estados") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#pais').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#ciudad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una Ciudad",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("ciudades") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        estado: $('#estados').val(),
                        pais: $('#pais').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#pais_residencia').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Pais",
            pais: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("pais") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#estados_residencia').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Departamento",
            estados: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("estados") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#pais_residencia').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  }
                },
            }
        });

        $('#ciudad_residencia').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una Ciudad",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("ciudades") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        pais: $('#pais_residencia').val(),
                        estado: $('#estados_residencia').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#empresa').select2({
            placeholder: "Seleccione una empresa",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("empresa") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#sede').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una sede",
            sede: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sede") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#empresa').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
        $("[data-toggle='tooltip']").tooltip();

         $('#direccion').change(function () {
        direccion = "";
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        console.log(dir);
        codeAddress(dir);
      });

      $('#pais_residencia').change(function () {
        pais = $(this).val();
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        codeAddress(dir);
      });

      $('#estados_residencia').change(function () {
        departamento = $(this).val();
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      });

      $('#ciudad_residencia').change(function () {
        ciudad = $(this).val();
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      });
    });

    funcambiarinput = function (id, val) {
      valor = $("#"+id).val();
      if (valor==="Celular") {
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-10').addClass('col-9');
      }else if(valor==="Telefono"){
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).show();
          $("#codtelext"+val).show();
          $("#codtelnum"+val).removeClass('col-9 col-10').addClass('col-7');
      }else if(valor==="Radio"){
          $("#codtelindp"+val).hide();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-9').addClass('col-10');
      }
    }
function setMapOnAll(map) {
      for (var i = 0; i < marker.length; i++) {
        marker[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    function initMap() {
      geocoder = new google.maps.Geocoder();
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 59.325, lng: 18.070}
      });

      infoWindow = new google.maps.InfoWindow({map: map});

      funGeolocation();
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    }

    function attachSecretMessage() {
        $("#lat").val(marker.position.lat);
        $("#lng").val(marker.position.lng);
    }

    function funGeolocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $("#lat").val(pos.lat);
          $("#lng").val(pos.lng);
          marker.setMap(map);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: pos
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function codeAddress(dir) {
      if (marker) {
        marker.setMap(null);
      }

      var address = dir;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);

          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: results[0].geometry.location
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          $("#lat").val(results[0].geometry.location.lat);
          $("#lng").val(results[0].geometry.location.lng);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbw8hYpmFO_VWUZLufrAL1qfnPFQp4JaM&callback=initMap"></script>
@endsection
