@extends('template.app')
@section('title', 'Ver Usuario')

@section('content')
<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap-material-datetimepicker.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="{{ url('/') }}/css/html5imageupload.css?v1.3" rel="stylesheet">
<style>
.imagen-perfil{
   height: 200px;
    width: 200px;
    max-height: 200px;
    min-height: 200px;
    @if(!empty($data['funcionario']->foto))
    background-image: url('{{url("/")}}/images/file/clientes/{{$data["funcionario"]->foto}}');
    @endif
}

@if(!empty($data['funcionario']->foto))
.imagen-perfil:after{
    content: '' !important;
}
@endif

.foto-completa{
    width: 206px;height: 616px;max-height: 616px;min-width: 206px;min-height: 616px;
    @if(!empty($data['funcionario']->foto_completa))
    background-image: url('{{url("/")}}/images/file/clientes/{{$data["funcionario"]->foto_completa}}');
    @endif
}
@if(!empty($data['funcionario']->foto_completa))
.foto-completa:after{
    content: '' !important;
}
@endif
.bg-ESSI{
    background-color: #051d60;
}
.h-auto{
    height: auto !important;
}
.pais-p{
    width: 10%;
    height: 100%;
}
 .pais-s{
    width: 50%;
    height: 100%;
}
.b-r{border-right: 1px solid #000;}
.resaltar{background-color: #a9a9a940;}
.h-100{ height: 100%; }
.b-t{ background-color: transparent;}
/*CSS para pais-departamento-ciudad*/
@for($i=1; $i<=2; $i++)
<?php if($i == 2){ $m = $i; }else{ $m = ""; } ?>
.ciudad_mostrar<?=$m?>{
    cursor: no-drop;
}
.fila-ciudad<?=$m?>{
    display: none;
}
.no-cursor<?=$m?>{
    cursor: no-drop;
}
.departamento_mostrar<?=$m?>{
    cursor: no-drop;
}
.fila-departamento<?=$m?>{
    display: none;
}
.cambiar-ciudad<?=$m?>{
    cursor: pointer;
}
.cambiar-departamento<?=$m?>{
    cursor: pointer;
}
.cambiar-pais<?=$m?>{
    cursor: pointer;
}
.fila-pais<?=$m?>{
    display: none;
}
.pais_mostrar<?=$m?>{
    cursor: no-drop;
}
@endfor
/*FIN CSS para pais-departamento-ciudad*/

/*Mapa*/
#map{
    height: 340px;
    width: 100%;
    top: 10px;
}
/*Fin Mapa*/
.img-redonda{border-radius: 200px 200px 200px 200px;-moz-border-radius: 200px 200px 200px 200px;-webkit-border-radius: 200px 200px 200px 200px;border: 0px solid #000000;}
</style>

<div class="main-header mt-5">
    <h2>Ver Usuario</h2>
    <em>Aqui puedes editar los datos del usuario {{$data['nombre']}} </em>
</div>
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-address-book-o"></i><a href="{{url('/')}}/funcionarios">Funcionario</a></li>
            <li class="active">Ver</li>
        </ul>
    </div>
</div>
<form method="POST" action="" id="formulario"  enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{$data['funcionario']->id}}">
    <input type="hidden" name="action" value="5">
    <div class="row bg-white">
        <div class="col-sd-12 col-md-4 col-lg-4 mt-3 mb-3">
            <div class="row">
                <div class="col-md-12">
                    <h3>Imagen de Perfil</h3>
                </div>
                <div class="col-md-12">
                    <div class="dropzone imagen-perfil" data-width="200" data-height="200" data-ajax="false" data-originalsave="true">
                        <input type="file" name="foto" accept="image/gif, image/jpeg, image/png" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Imagen Cuerpo Completo</h3>
                </div>
                <div class="col-md-12">
                    <div class="dropzone foto-completa" data-width="206" data-height="616" data-ajax="false" data-originalsave="true">
                        <input type="file" name="principal" accept="image/gif, image/jpeg, image/png" disabled>
                      </div>
                </div>
            </div>
        </div>
        <div class="col-sd-12 col-md-8 col-lg-8 mt-3 mb-3">
            <div class="row">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Información Personal</h3>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="nombres">Nombres</label>
                    <input class="form-control" type="text" name="formulario[nombres]" id="nombres" placeholder="Nombres" value="{{$data['funcionario']->nombres}}" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="apellidos">Apellidos</label>
                    <input class="form-control" type="text" name="formulario[apellidos]" id="apellidos" placeholder="Apellidos" value="{{$data['funcionario']->apellidos}}" disabled>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="tipo_documento">Tipo documento</label>
                    <select name="formulario[tipo_documento]" id="tipo_documento" class="form-control h-auto" disabled>
                      <option>Seleccione una opción</option>
                      <option value="Cedula Ciudadania" <?php if($data['funcionario']->tipo_documento == "Cedula Ciudadania"){ echo 'selected'; }?> >Cedula Ciudadania</option>
                      <option value="Pasaporte" <?php if($data['funcionario']->tipo_documento == "Pasaporte"){ echo 'selected'; }?> >Pasaporte</option>
                    </select>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="documento">Numero Documento</label>
                    <input class="form-control" id="documento" type="text" name="formulario[documento]" placeholder="Numero Documento" value="{{$data['funcionario']->documento}}" disabled>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="fecha_ff">Fecha Nacimiento</label>
                    <input class="form-control fecha_ff" type="text" name="formulario[fecha_nacimiento]" id="fecha_ff" value="{{$data['funcionario']->fecha_nacimiento}}" placeholder="Fecha Nacimiento" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="tipo_sangre">Tipo de sangre</label>
                    <select name="formulario[tipo_sangre]" id="tipo_sangre" class="form-control h-auto" disabled>
                      <option value="">Seleccione una opcion</option>
                      <option value="O+" <?php if($data['funcionario']->tipo_sangre == "O+"){ echo 'selected'; }?>>O+</option>
                      <option value="O-" <?php if($data['funcionario']->tipo_sangre == "O-"){ echo 'selected'; }?>>O-</option>
                      <option value="A+" <?php if($data['funcionario']->tipo_sangre == "A+"){ echo 'selected'; }?>>A+</option>
                      <option value="A-" <?php if($data['funcionario']->tipo_sangre == "A-"){ echo 'selected'; }?>>A-</option>
                      <option value="B+" <?php if($data['funcionario']->tipo_sangre == "B+"){ echo 'selected'; }?>>B+</option>
                      <option value="B-" <?php if($data['funcionario']->tipo_sangre == "B-"){ echo 'selected'; }?>>B-</option>
                      <option value="AB+" <?php if($data['funcionario']->tipo_sangre == "AB+"){ echo 'selected'; }?>>AB+</option>
                      <option value="AB-" <?php if($data['funcionario']->tipo_sangre == "AB-"){ echo 'selected'; }?>>AB-</option>
                    </select>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="telefono_personal">Telefono Personal</label>
                    <input class="form-control" id="telefono_personal" type="text" name="formulario[telefono_personal]" placeholder="Telefono Personal" value="{{$data['funcionario']->telefono_personal}}" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="telefono_personal">Telefono Corporativo</label>
                    <input class="form-control" id="telefono_corporativo" type="text" name="formulario[telefono_corporativo]" value="{{$data['funcionario']->telefono_corporativo}}" placeholder="Telefono Corporativo" disabled>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="correo_personal">Correo Personal</label>
                    <input class="form-control" id="correo_personal" type="text" name="formulario[correo_personal]" value="{{$data['funcionario']->correo_personal}}" placeholder="Correo Personal" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="correo_corporativo">Correo corporativo</label>
                    <input class="form-control" type="text" id="correo_corporativo" name="formulario[correo_corporativo]" value="{{$data['funcionario']->correo_corporativo}}" placeholder="Correo Corporativo" disabled>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="profesion">Profesión</label>
                    <input class="form-control" type="text" name="formulario[profesion]" value="{{$data['funcionario']->profesion}}" placeholder="Profesión" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="pais">Pais Nacimiento</label>
                    <div class="row fila-pais" style="display: block;">
                        <div class="col-md-12">
                            <input type="text" id="pais_mostrar" class="form-control pais_mostrar" name="formulario[pais_nacimiento]" value="{{$data['funcionario']->pais_nacimiento}}" disabled>
                        </div>
                    </div>
                    <input type="text" class="form-control" style="display: none;" id="pais" list="lista_paises" placeholder="Seleccione un pais">
                    <datalist id="lista_paises">
                      @foreach($data['paises'] as $pais)
                      <option value="{{$pais->id}}" label="{{$pais->name}}">
                      @endforeach
                    </datalist>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="departamento">Departamento Nacimiento</label>
                    <div class="row fila-departamento" style="display: block;">
                        <div class="col-md-12">
                            <input type="text" id="departamento_mostrar" class="form-control departamento_mostrar" name="formulario[departamento_nacimiento]" value="{{$data['funcionario']->departamento_nacimiento}}" disabled>
                        </div>
                    </div>
                    <input type="text" style="display: none;" class="form-control no-cursor" id="departamento" list="lista_departamentos" placeholder="Seleccione un pais" readonly>
                    <datalist id="lista_departamentos">
                      @foreach($data['estados'] as $departamento)
                      <option value="{{$departamento->id}}" label="{{$departamento->name}}">
                      @endforeach
                    </datalist>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="ciudad">Ciudad Nacimiento</label>
                    <div class="row fila-ciudad" style="display: block;">
                        <div class="col-md-12">
                            <input type="text" id="ciudad_mostrar" class="form-control ciudad_mostrar" value="{{$data['funcionario']->ciudad_nacimiento}}" name="formulario[ciudad_nacimiento]" disabled>
                        </div>
                    </div>
                    <input type="text" style="display: none" class="form-control no-cursor" id="ciudad" list="lista_ciudades" placeholder="Seleccione un pais" readonly>
                    <datalist id="lista_ciudades">
                      @foreach($data['ciudades'] as $ciudad)
                      <option value="{{$ciudad->id}}" label="{{$ciudad->name}}">
                      @endforeach
                    </datalist>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-12 col-lg-12 text-left">
                     <label for="descripcion">Descripcion de perfil</label>
                     <textarea class="form-control" type="text" id="descripcion" name="formulario[descripcion]" rows="15" disabled><?=$data['funcionario']->descripcion?></textarea>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Información Ubicación</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="pais2">Pais Ubicación</label>
                    <div class="row fila-pais2" style="display: block;">
                        <div class="col-md-12">
                            <input type="text" id="pais_mostrar2" class="form-control pais_mostrar2" name="formulario[pais_residencia]" value="{{$data['funcionario']->pais_residencia}}" disabled>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="pais2" list="lista_paises2" placeholder="Seleccione un pais" style="display: none;">
                    <datalist id="lista_paises2">
                      @foreach($data['paises'] as $pais)
                      <option value="{{$pais->id}}" label="{{$pais->name}}">
                      @endforeach
                    </datalist>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="departamento2">Departamento Ubicación</label>
                    <div class="row fila-departamento2" style="display: block;">
                        <div class="col-md-12">
                            <input type="text" id="departamento_mostrar2" class="form-control departamento_mostrar2" name="formulario[departamento_residencia]" value="{{$data['funcionario']->departamento_residencia}}" disabled>
                        </div>
                    </div>
                    <input type="text" class="form-control no-cursor" id="departamento2" list="lista_departamentos2" placeholder="Seleccione un pais" readonly style="display: none;">
                    <datalist id="lista_departamentos2">
                      @foreach($data['estados2'] as $estado)
                      <option value="{{$estado->id}}" label="{{$estado->name}}">
                      @endforeach
                    </datalist>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="ciudad2">Ciudad Ubicación</label>
                    <div class="row fila-ciudad2" style="display: block;">
                        <div class="col-md-12">
                            <input type="text" id="ciudad_mostrar2" class="form-control ciudad_mostrar2" name="formulario[ciudad_residencia]" value="{{$data['funcionario']->ciudad_residencia}}" disabled>
                        </div>
                    </div>
                    <input type="text" class="form-control no-cursor" id="ciudad2" list="lista_ciudades2" placeholder="Seleccione un pais" readonly style="display: none;">
                    <datalist id="lista_ciudades2">
                      @foreach($data['ciudades2'] as $ciudad)
                      <option value="{{$ciudad->id}}" label="{{$ciudad->name}}">
                      @endforeach
                    </datalist>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="direcion">Dirección de Residencia</label>
                    <input class="form-control" type="text" id="direcion" name="formulario[direccion]" value="{{$data['funcionario']->direccion}}" placeholder="Dirección de Residencia" disabled>
                </div>
            </div>
            <div class="row">
                <div id="map"></div>
                <div class="coordinates">
                    <em class="lat">Latitud</em>
                    <em class="lon">Longitud</em>
                    <input type="text" id="lat" name="lat">
                    <input type="text" id="lng" name="lng">
                </div>
            </div>
        </div>
    </div>
    <div class="row bg-white">
        <div class="col-sd-12 col-md-12 col-lg-12 mt-3 mb-3">
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Vinculación a ESSI</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="fecha_vinculacion">Fecha vinculación a ESSI</label>
                    <input class="form-control fecha_ff" type="text" id="fecha_vinculacion" name="formulario[fecha_vinculacion]" value="{{$data['funcionario']->fecha_vinculacion}}" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="cargo">Cargo</label>
                    <select name="formulario[cargo]" id="cargo" class="form-control h-auto" disabled>
                      <option>Seleccione una opción</option>
                      <option value="Ejecutivo Comercial" <?php if($data['funcionario']->cargo == "Ejecutivo Comercial"){ echo "Selected"; } ?> >Ejecutivo Comercial</option>
                      <option value="Partner Solution" <?php if($data['funcionario']->cargo == "Partner Solution"){ echo "Selected"; } ?> >Partner Solution</option>
                      <option value="Gerente Comercial" <?php if($data['funcionario']->cargo == "Gerente Comercial"){ echo "Selected"; } ?> >Gerente Comercial</option>
                      <option value="Director General" <?php if($data['funcionario']->cargo == "Director General"){ echo "Selected"; } ?> >Director General</option>
                      <option value="Junta Directiva" <?php if($data['funcionario']->cargo == "Junta Directiva"){ echo "Selected"; } ?> >Junta Directiva</option>
                      <option value="Gerente Financiero" <?php if($data['funcionario']->cargo == "Gerente Financiero"){ echo "Selected"; } ?> >Gerente Financiero</option>
                      <option value="Equipo Directivo" <?php if($data['funcionario']->cargo == "Equipo Directivo"){ echo "Selected"; } ?> >Equipo Directivo</option>
                      <option value="Calidad" <?php if($data['funcionario']->cargo == "Calidad"){ echo "Selected"; } ?> >Calidad</option>
                      <option value="Visitante" <?php if($data['funcionario']->cargo == "Visitante"){ echo "Selected"; } ?> >Visitante</option>
                      <option value="Administrador" <?php if($data['funcionario']->cargo == "Administrador"){ echo "Selected"; } ?> >Administrador</option>
                      <option value="Administrador Smart" <?php if($data['funcionario']->cargo == "Administrador Smart"){ echo "Selected"; } ?> >Administrador Smart</option>
                    </select>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Contacto Emergencia</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                    <label for="parentesco_emergencia">Parentesco</label>
                    <input class="form-control" type="text" placeholder="Parentesco" id="parentesco_emergencia" name="contacto_emergencia[parentesco]" value="<?php if(isset($data['contacto_emergencia'][0]->parentesco)){ ?>{{$data['contacto_emergencia'][0]->parentesco}}<?php } ?>" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                    <label for="nombre_emergencia">Nombre</label>
                    <input class="form-control" type="text" placeholder="Nombre" id="nombre_emergencia" name="contacto_emergencia[nombre]" value="<?php if(isset($data['contacto_emergencia'][0]->nombre)){ ?>{{$data['contacto_emergencia'][0]->nombre}}<?php } ?>" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left">
                    <label for="telefono_emergencia">Telefono</label>
                    <input class="form-control" type="text" id="telefono_emergencia" name="contacto_emergencia[telefono]" placeholder="Telefono" value="<?php if(isset($data['contacto_emergencia'][0]->telefono)){ ?>{{$data['contacto_emergencia'][0]->telefono}}<?php } ?>" disabled>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Información Familiar</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="estado_civil">Estado Civil</label>
                    <select name="formulario[estado_civil]" id="estado_civil" class="form-control h-auto" disabled>
                      <option value="Soltero" <?php if($data['funcionario']->estado_civil == "Soltero"){ echo "Selected"; } ?>>Soltero</option>
                      <option value="Casado" <?php if($data['funcionario']->estado_civil == "Casado"){ echo "Selected"; } ?>>Casado</option>
                      <option value="Unión Libre" <?php if($data['funcionario']->estado_civil == "Unión Libre"){ echo "Selected"; } ?>>Unión Libre</option>
                      <option value="Divorciado/a" <?php if($data['funcionario']->estado_civil == "Divorciado/a"){ echo "Selected"; } ?>>Divorciado/a</option>
                      <option value="Viudo/a" <?php if($data['funcionario']->estado_civil == "Viudo/a"){ echo "Selected"; } ?>>Viudo/a</option>
                    </select>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="hijos">Numero de hijos</label>
                    <input class="form-control hijos" type="number" id="hijos" name="formulario[hijos]" value="{{$data['funcionario']->hijos}}" placeholder="Numero Hijos" disabled>
                </div>
            </div>
            <div class="row mt-2 info-conyugue">
                @if(count($data['Conyuge'])>0)
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="conyugue">Conyugue</label>
                    <input class="form-control" type="hidden" name="familia[0][parentesco]" value="Cónyuge">
                    <input class="form-control" type="text" id="conyugue" name="familia[0][nombre]" placeholder="Conyugue" value="<?php if(isset($data['Conyuge'][0]->nombre)){ echo $data['Conyuge'][0]->nombre; } ?>" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left">
                    <label for="conyugue_fecha_nacimiento">Fecha Nacimiento</label>
                    <input class="form-control fecha_ff" id="conyugue_fecha_nacimiento" name="familia[0][fecha_nacimiento]" placeholder="Fecha Nacimiento" value="<?php if(isset($data['Conyuge'][0]->fecha_nacimiento)){ echo $data['Conyuge'][0]->fecha_nacimiento; } ?>" disabled>
                </div>
                @endif
            </div>
            <div class="row mt-2 info-hijos">
                @if(count($data['hijos'])>0)
                <?php $con=0; foreach($data['hijos'] as $hijo){ ?>
                @if($con == 0)
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                </div>
                @endif
                <div class="col-sm-12 col-md-6 col-lg-6 text-left mt-3">
                    <label for="pais_residencia">Nombre del hijo</label>
                    <input type="hidden" value="Hijo" name="hijos_inf[{{$con}}][parentesco]">
                    <input class="form-control" type="text" name="hijos_inf[{{$con}}][nombre]" value="{{$hijo->nombre}}" disabled>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left mt-3">
                    <label for="pais_residencia">Fecha de nacimiento</label>
                    <input class="form-control fecha_ff" type="text" name="hijos_inf[{{$con}}][fecha_nacimiento]" value="{{$hijo->fecha_nacimiento}}" disabled>
                </div>
                <?php $con++; } ?>
                @endif
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Estudios Realizados</h3>
                </div>
            </div>
            <div class="row mt-2 info-titulo">
                <?php $con = 0; ?>
                @if(count($data['estudio']) > 0)
                @foreach($data['estudio'] as $estudio)
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="titulo">Titulo Obtenido</label>
                    <input class="form-control" type="text" name="educacion[{{$con}}][titulo]" placeholder="Titulo Obtenido" value="{{$estudio->titulo}}" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="ano">Año culminación</label>
                    <input class="form-control" type="number" name="educacion[{{$con}}][anio]" placeholder="Año culminación" value="{{$estudio->anio}}" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="institucion">Institución</label>
                    <input class="form-control" type="text" name="educacion[{{$con}}][institucion]" placeholder="Institución" value="{{$estudio->institucion}}" disabled>
                </div>
                <?php $con++; ?>
                @endforeach
                @else
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="titulo">Titulo Obtenido</label>
                    <input class="form-control" type="text" name="educacion[0][titulo]" placeholder="Titulo Obtenido" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="ano">Año culminación</label>
                    <input class="form-control" type="number" name="educacion[0][anio]" placeholder="Año culminación" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 text-left mt-3">
                    <label for="institucion">Institución</label>
                    <input class="form-control" type="text" name="educacion[0][institucion]" placeholder="Institución" disabled>
                </div>
                @endif
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Salarios Mensuales</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-4 col-lg-2 text-left mt-3">
                    <label for="salario_basico">Salario Basico</label>
                    <input class="form-control dinero" type="text" id="salario_basico" name="formulario[salario_basico]" value="{{$data['funcionario']->salario_basico}}" placeholder="Salario basico mensual" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 text-left mt-3">
                    <label for="bonificacion_fija">Bonificación Fija</label>
                    <input class="form-control dinero" id="bonificacion_fija" type="text" name="formulario[bonificacion_fija]" placeholder="Bonificación fija mensual" value="{{$data['funcionario']->bonificacion_fija}}" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 text-left mt-3">
                    <label for="aux_transporte">Aux. Transporte</label>
                    <input class="form-control dinero" id="aux_transporte" type="text" name="formulario[aux_transporte]" placeholder="Auxilio de transporte" value="{{$data['funcionario']->aux_transporte}}" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 text-left mt-3">
                    <label for="aux_alimentacion">Aux. Alimentación</label>
                    <input class="form-control dinero" id="aux_alimentacion" type="text" name="formulario[aux_alimentacion]" placeholder="Auxilio de alimentación" value="{{$data['funcionario']->aux_alimentacion}}" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 text-left mt-3">
                    <label for="aux_vivienda">Aux. Vivienda</label>
                    <input class="form-control dinero" id="aux_vivienda" type="text" name="formulario[aux_vivienda]" placeholder="Auxilio de Vivienda" value="{{$data['funcionario']->aux_vivienda}}" disabled>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 text-left mt-3">
                    <label for="otros_auxilios">Otros Auxilios</label>
                    <input class="form-control dinero" id="otros_auxilios" type="text" name="formulario[otros_auxilios]" placeholder="Otros Auxilios" value="{{$data['funcionario']->otros_auxilios}}" disabled>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>País Principal</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-12 col-lg-12 mt-3 img-pais-principal">
                    @if(!empty($data['funcionario']->principal) && $data['funcionario']->principal != "seleccione_una_opción.png")
                    <img class="form-control pais-p" src="{{url('/')}}/images/icons/<?=$data['funcionario']->principal?>">
                    @else
                    <img class="form-control pais-p img-redonda" src="https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Perfil&w=180&h=168">
                    @endif
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left mt-3">
                    <label for="principal">Seleccione un pais principal</label>
                    <select name="formulario[principal]" id="principal" class="form-control h-auto" disabled>
                      <option>Seleccione una opción</option>
                      @foreach($data['paises'] as $pais)
                      @if($pais->imagen!="")
                      <option value="{{$pais->name}}" <?php if(ucwords(str_replace(".png", "", $data['funcionario']->principal)) == $pais->name){ echo 'selected'; } ?> >{{$pais->name}}</option>
                      @endif
                      @endforeach
                    </select>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-left mt-3">
                    <label>Color Predeterminado</label>
                    <input id="mycolor" type="text" value="{{$data['funcionario']->color}}" class="color form-control" placeholder="Seleccione un color" name="formulario[color]" disabled>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 mt-3 img-paises-secundarios">

                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Metas para el año {{date('Y')}}</h3>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-2 col-lg-2 b-r">
                    <h3>Meses</h3>
                </div>
                <div class="col-sm-12 col-md-10 col-lg-10">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3 b-r">
                            <h3>Oportunidades Identificadas</h3>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 b-r">
                            <h3>Montos Identificados</h3>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 b-r">
                            <h3>Negocios Cerrados</h3>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 b-r">
                            <h3>Montos Cerrados</h3>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i=1; ?>
            @foreach($data['meses'] as $mes)
            <?php if(($i-1)%2 == 0){ $clase='resaltar'; }else{ $clase=''; } ?>
            <div class="row mt-2">
                <div class="col-sm-12 col-md-2 col-lg-2 <?=$clase?>">
                    <h4>{{$mes}}</h4>
                </div>
                <div class="col-sm-12 col-md-10 col-lg-10">
                    <div class="row h-100">
                        <div class="col-sm-12 col-md-3 col-lg-3 <?=$clase?>">
                            <input type="number" min="0" class="form-control b-t" name="metas[{{$i}}][oportunidad_identificada]" value="<?php if(count($data['meta']) > 0 && isset($data['meta'][$i-1]->oportunidad_identificada)){ echo $data['meta'][$i-1]->oportunidad_identificada; }else{ echo '0'; } ?>" disabled>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 <?=$clase?>">
                            <input type="text" class="form-control dinero b-t" name="metas[{{$i}}][monto_identificado]" value="<?php if(count($data['meta']) > 0 && isset($data['meta'][$i-1]->monto_identificado)){ echo $data['meta'][$i-1]->monto_identificado; }else{ echo '0'; } ?>" disabled>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 <?=$clase?>">
                            <input type="number" min="0" class="form-control b-t" name="metas[{{$i}}][oportunidad_concretada]" value="<?php if(count($data['meta']) > 0 && isset($data['meta'][$i-1]->oportunidad_concretada)){ echo $data['meta'][$i-1]->oportunidad_concretada; }else{ echo '0'; } ?>" disabled>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 <?=$clase?>">
                            <input type="text" class="form-control dinero b-t" name="metas[{{$i}}][monto_concretado]" value="<?php if(count($data['meta']) > 0 && isset($data['meta'][$i-1]->monto_concretado)){ echo $data['meta'][$i-1]->monto_concretado; }else{ echo '0'; } ?>" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; ?>
            @endforeach
            <div class="row mt-4">
                <div class="col-md-12 text-left text-white bg-ESSI shadow-2">
                    <h3>Datos de Acceso al Sistema</h3>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 text-left mt-3">
                    <label for="usuario">Usuario</label>
                    <input class="form-control" id="usuario" type="text" name="formulario[username]" placeholder="Usuario" value="{{$data['funcionario']->username}}" disabled>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col-sm-12 col-md-12 col-lg-12 alert-contrasena">

                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ url('/') }}/js/html5imageupload.js?v1.4.3"></script>
<script src="{{ url('/') }}/js/plugins/moment-with-locales.js"></script>
<script src="{{ url('/') }}/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="../js/jquery.maskMoney.min.js"></script>
<script src="{{ url('/') }}/assets/js/tinyColorPicker-master/colors.js"></script>
<script src="{{ url('/') }}/assets/js/tinyColorPicker-master/jqColorPicker.min.js"></script>
<script src="{{url('/')}}/js/parsley.min.js"></script>
<script>
    var marker;
    var map;
    var geocoder;
    var infoWindow = {};
    m=1;
    var pais = "";
    var departamento = "";
    var ciudad = "";
    var direccion = "";
    $( document ).ready(function() {
    setTimeout(function(){ ciudad2(); paises_secundarios() }, 3000);
    $("[data-toggle='tooltip']").tooltip();
    $('.dropzone').html5imageupload();
    $(".dinero").maskMoney();
    $('.color').colorPicker();
    $('.fecha_ff').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
});

    function save(){
        if($('#contrasena').val() == ''){
            guardar_formulario();
        }else{
            if($('#contrasena').val() == $('#v_contrasena').val()){
                guardar_formulario();
            }else{
                swal({
                      title: 'Error',
                      text: "Las contraseñas no coinciden",
                      type: 'error',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then(function () {

                    });
            }
        }
    }

    function guardar_formulario(){
        event.preventDefault();
            if(true === $("#formulario").parsley().validate()){
                swal({
                      title: 'Esta seguro?',
                      text: "Va a editar al Funcionario {{$data['funcionario']->name}}",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Si, guardar!'
                    }).then(function () {
                        var jqxhr = $.post("{{url('/')}}/editarfuncionario-action", $("#formulario").serialize())
                        .done(function() {
                           swal({
                              title: 'Guardado',
                              text: "Guardado correctamente",
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'OK'
                            })
                        })
                        .fail(function(e) {
                            console.error(e)
                        })
                        .always(function(e) {
                          console.log(e)
                        });
                    });
            }else{
                swal('Faltan Campos por llenar');
            }
    }

    verificada = 'no';
    $(document).on('keyup','.contrasena',function(){
        contrasena = $('#contrasena').val();
        v_contrasena = $('#v_contrasena').val();
        if(contrasena == v_contrasena){
           html=`<div class="alert alert-success" role="alert">La contraseña esta Verificada</div>`;
           $('.alert-contrasena').html(html);
           verificada="si";
        }else{
           html=`<div class="alert alert-danger" role="alert">La contraseña no es igual</div>`;
           $('.alert-contrasena').html(html);
           verificada="no";
        }
    });

    $(document).on('change','#secundarios',function(){
        paises_secundarios();
    });

    function paises_secundarios(){
        paises = $('#secundarios').val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "{{url('/')}}/editarfuncionario-action",
                data: {
                    _token: CSRF_TOKEN,
                    paises: paises,
                    url: "{{url('/')}}",
                    action: 4
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.img-paises-secundarios').html(e.data['cuerpo']);
                }
        });
    }

    $(document).on('change','#principal',function(){
        pais = $(this).val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "{{url('/')}}/editarfuncionario-action",
                data: {
                    _token: CSRF_TOKEN,
                    pais: pais,
                    url: "{{url('/')}}",
                    action: 3
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.img-pais-principal').html(e.data['cuerpo']);
                }
        });
    });

    function eliminar_titulo(clase){
        $(clase).remove();
    }

    numero_titulo = {{count($data['estudio'])}};
    function agregar_titulo(){
        if(numero_titulo == 0){
           numero_titulo++;
        }
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
                url: "{{url('/')}}/editarfuncionario-action",
                data: {
                    _token: CSRF_TOKEN,
                    numero: numero_titulo,
                    action: 2
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.info-titulo').prepend(e.data['cuerpo']);
                }
        });
        numero_titulo++;
    }

    function fecha(){
        $('.fecha_ff').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
    }
    /*Estado Civil del Usuario*/
    $(document).on('change','#estado_civil',function(){
       estado = $(this).val();
       if(estado == 'Casado' || estado == 'Unión Libre'){
           var CSRF_TOKEN = $('input[name=_token]').val();
           var jqxhr = $.ajax({
                url: "{{url('/')}}/editarfuncionario-action",
                data: {
                    _token: CSRF_TOKEN,
                    id: {{$data['funcionario']->id}},
                    action: 0
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.info-conyugue').html(e.data['cuerpo']);
                    fecha();
                }
            });
       }else{
        $('.info-conyugue').html('');
       }
    });

    $(document).on('keyup','#hijos',function(){
       hijos = $(this).val();
       if(hijos > 0){
           var CSRF_TOKEN = $('input[name=_token]').val();
           var jqxhr = $.ajax({
                url: "{{url('/')}}/editarfuncionario-action",
                data: {
                    _token: CSRF_TOKEN,
                    id: {{$data['funcionario']->id}},
                    hijos: hijos,
                    action: 1
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('.info-hijos').html(e.data['cuerpo']);
                    fecha();
                }
            });
       }else{
        $('.info-hijos').html('');
       }
    });
@for($i=1; $i<=2; $i++)
<?php if($i == 2){ $m = $i; }else{ $m = ""; } ?>
/*Pais*/
$('body').on('change','#pais{{$m}}',function(){
   valor=$(this) .val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio pais!',
          'warning'
        );
        $('#pais{{$m}}').val('');
        $('#departamento{{$m}}').val('');
        $('#ciudad{{$m}}').val('');

    }else{
        $.ajax({
            url: "{{url('/')}}/consultarpais/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#pais_mostrar{{$m}}').val(e.pais.name);
                $('.fila-pais{{$m}}').css('display','block');
                $('#pais{{$m}}').css('display','none');
                $('#lista_departamentos{{$m}}').html(e.html);
                $('#departamento{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
                $('#ciudad{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
                $('#departamento{{$m}}').removeClass('no-cursor');
                $('#departamento{{$m}}').removeAttr("readonly");
                @if($m==2) pais2(); @endif
            }
        });
    }
});

$('body').on('click','.cambiar-pais{{$m}}',function(){
    $('.fila-pais{{$m}}').css('display','none');
    $('#pais{{$m}}').val('');
    $('#pais_mostrar{{$m}}').val('');
    $('#pais{{$m}}').css('display','block');
    $('.fila-departamento{{$m}}').css('display','none');
    $('#departamento{{$m}}').css('display','block');
    $('#lista_departamentos{{$m}}').html('');
    $('#departamento{{$m}}').val('');
    $('#departamento_mostrar{{$m}}').val('');
    $('#departamento{{$m}}').addClass('no-cursor');
    $('#departamento{{$m}}').attr("readonly","readonly");
    $('.fila-ciudad{{$m}}').css('display','none');
    $('#ciudad{{$m}}').css('display','block');
    $('#lista_ciudades{{$m}}').html('');
    $('#ciudad{{$m}}').val('');
    $('#ciudad_mostrar{{$m}}').val('');
    $('#ciudad{{$m}}').addClass('no-cursor');
    $('#ciudad{{$m}}').attr("readonly","readonly");

    $('#departamento{{$m}}').attr('placeholder','Seleccione un pais');
    $('#ciudad{{$m}}').attr('placeholder','Seleccione un pais');

});

/*Departamento*/

$('body').on('change','#departamento{{$m}}',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Departamento!',
          'warning'
        );
        $('#departamento{{$m}}').val('');
        $('#ciudad{{$m}}').val('');
        @if($m==2) departamento2(); @endif
    }else{
        $.ajax({
            url: "{{url('/')}}/consultardepartamento/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#departamento_mostrar{{$m}}').val(e.departamento.name);
                $('.fila-departamento{{$m}}').css('display','block');
                $('#departamento{{$m}}').css('display','none');
                $('#lista_ciudades{{$m}}').html(e.html);
                $('#ciudad{{$m}}').attr('placeholder','Seleccione una ciudad (municipio)');
                $('#ciudad{{$m}}').removeClass('no-cursor');
                $('#ciudad{{$m}}').removeAttr("readonly");
                @if($m==2) departamento2(); @endif
            }
        });
    }
});

$('body').on('click','.cambiar-departamento{{$m}}',function(){
    $('.fila-departamento{{$m}}').css('display','none');
    $('#departamento{{$m}}').val('');
    $('#departamento_mostrar{{$m}}').val('');
    $('#departamento{{$m}}').css('display','block');
    $('#departamento{{$m}}').removeAttr("readonly");
    $('#departamento{{$m}}').removeClass('no-cursor');
    $('.fila-ciudad{{$m}}').css('display','none');
    $('#ciudad{{$m}}').css('display','block');
    $('#lista_ciudades{{$m}}').html('');
    $('#ciudad{{$m}}').val('');
    $('#ciudad_mostrar{{$m}}').val('');
    $('#ciudad{{$m}}').addClass('no-cursor');
    $('#ciudad{{$m}}').attr("readonly","readonly");

    $('#departamento{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
    $('#ciudad{{$m}}').attr('placeholder','Seleccione un departamento (estado)');
    @if($m==2) departamento2(); @endif
});

/*ciudad*/

$('body').on('change','#ciudad{{$m}}',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Ciudad!',
          'warning'
        );
        $('#ciudad{{$m}}').val('');
        @if($m==2) ciudad2(); @endif
    }else{
        $.ajax({
            url: "{{url('/')}}/consultarciudad/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#ciudad_mostrar{{$m}}').val(e.ciudad.name);
                $('.fila-ciudad{{$m}}').css('display','block');
                $('#ciudad{{$m}}').css('display','none');
                @if($m==2) ciudad2(); @endif
            }
        });
    }
});

$('body').on('click','.cambiar-ciudad{{$m}}',function(){
    $('.fila-ciudad{{$m}}').css('display','none');
    $('#ciudad{{$m}}').val('');
    $('#ciudad_mostrar{{$m}}').val('');
    $('#ciudad{{$m}}').css('display','block');
    $('#ciudad{{$m}}').removeClass('no-cursor');
    $('#ciudad{{$m}}').removeAttr("readonly");

    $('#ciudad{{$m}}').attr('placeholder','Seleccione una ciudad (municipio)');
    @if($m==2) ciudad2(); @endif
});
@endfor


/*MAPAS */

    $('#direccion').change(function () {
        direccion = "";
        dir="";
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        console.log(dir);
        codeAddress(dir);
      });

      function pais2() {
        pais = $('#pais_mostrar2').val();
        dir="";
        ciudad = $("#ciudad_mostrar2").val();
        departamento = $("#departamento_mostrar2").val();
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }
        codeAddress(dir);
      }

      function departamento2() {
        departamento = $('#departamento_mostrar2').val();
        dir="";
        pais = $("#pais_mostrar2").val();
        ciudad = $("#ciudad_mostrar2").val();
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      }

      function ciudad2() {
        ciudad = $('#ciudad_mostrar2').val();
        dir="";
        pais = $("#pais_mostrar2").val();
        departamento = $("#departamento_mostrar2").val();
        if (direccion != "") {
          dir = direccion+", ";
        }

        if (ciudad != "") {
          dir += ciudad+" - ";
        }

        if (departamento != "") {
          dir += departamento+", ";
        }

        if (pais != "") {
          dir += pais;
        }

        codeAddress(dir);
      }

    funcambiarinput = function (id, val) {
      valor = $("#"+id).val();
      if (valor==="Celular") {
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-10').addClass('col-9');
      }else if(valor==="Telefono"){
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).show();
          $("#codtelext"+val).show();
          $("#codtelnum"+val).removeClass('col-9 col-10').addClass('col-7');
      }else if(valor==="Radio"){
          $("#codtelindp"+val).hide();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-9').addClass('col-10');
      }
    }

    function setMapOnAll(map) {
      for (var i = 0; i < marker.length; i++) {
        marker[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    function initMap() {
      geocoder = new google.maps.Geocoder();
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 59.325, lng: 18.070}
      });

      infoWindow = new google.maps.InfoWindow({map: map});

      funGeolocation();
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    }

    function attachSecretMessage() {
        $("#lat").val(marker.position.lat);
        $("#lng").val(marker.position.lng);
    }

    function funGeolocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $("#lat").val(pos.lat);
          $("#lng").val(pos.lng);
          marker.setMap(map);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: pos
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function codeAddress(dir) {
      if (marker) {
        marker.setMap(null);
      }

      var address = dir;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);

          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            position: results[0].geometry.location
          });

          marker.addListener('dragend', function() {
            attachSecretMessage();
          });

          $("#lat").val(results[0].geometry.location.lat);
          $("#lng").val(results[0].geometry.location.lng);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

/*FIN MAPAS*/
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbw8hYpmFO_VWUZLufrAL1qfnPFQp4JaM&callback=initMap"></script>
@endsection
