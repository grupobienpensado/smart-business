@extends('template.app')
@section('title', 'Listado de Usuarios')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('/') }}/js/plugins/responsive-portfolio-gallery-with-jquery-tutorial/fdw-demo.css" media="all" />
<link rel="stylesheet" type="text/css" href="{{url('/') }}/js/plugins/responsive-portfolio-gallery-with-jquery-tutorial/style.css">
<style type="text/css">
	.div-imagen{
		width: 190px;
		height: 190px;
		background-repeat: no-repeat;
    	background-size: cover;
    	border: 0.5px solid #ccc;
	}
</style>
<div class="container animated flipInX">
  <div class="row">

    <main class="col-sm-12 col-md-12 pt-6">
        <div class="pull-right">
	        <div class="btn-group">
	          <a <?php if($data['permiso_agregar']=="Si"){ ?>href="{{url('/')}}/crearusuario"<?php }else{ ?> onclick="no_permiso('Usted no tiene permiso para agregar un funcionario')" <?php } ?> class="btn btn-sm btn-success text-white"><i class="fa fa-plus" aria-hidden="true"></i> Agregar Usuario</a>
	        </div>
         </div>
         <div class="panel-title">
             <h2>Listado Funcionarios</h2>
         </div>       
          <div class="form-group row">  
		    <div class="col-md-12">
			    <ul id="list" class="portfolio_list da-thumbs">
			    @foreach($lista as $key)
			    @php
			    $lista=explode(" ",$key->nombres);
			    $lista2=explode(" ",$key->apellidos);
			    @endphp
			    	<li>
			    		@if($key->foto!='')
			            <div class="div-imagen" style="background-image: url('{{url('/') }}/images/file/clientes/{{$key->foto}}'); "></div>
			            @else
			            <div class="div-imagen" style="background-image: url('https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Perfil&w=200&h=200'); "></div>
			            @endif
			            <article class="da-animate da-slideFromRight" style="display: block;">
			                <h3>{{$lista[0]}}</h3>
			                <em>{{$lista2[0]}}</em>
			                <span class="zoom"><a <?php if($data['permiso_ver']=="Si"){ ?>href="{{url('/') }}/funcionario/{{$key->id}}"<?php }else{ ?> onclick="no_permiso('Usted no tiene permiso para ver funcionarios')" <?php } ?>></a></span>
                            <span class="edit"><a <?php if($data['permiso_editar']=="Si"){ ?>href="{{url('/') }}/editarfuncionario/{{$key->id}}"<?php }else{ ?> onclick="no_permiso('Usted no tiene permiso para editar un funcionario')" <?php } ?>></a></span>
			            </article>
			        </li>
			        @endforeach
			    </ul>
		    </div>
          </div>
    </main>
  </div>
</div>		
@endsection
@section('scripts')
<script type="text/javascript" src="{{url('/') }}/js/plugins/responsive-portfolio-gallery-with-jquery-tutorial/js/jquery-hover-effect.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(function() {
		$('ul.da-thumbs > li').hoverdir();
	});
});
    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
</script>
@endsection

