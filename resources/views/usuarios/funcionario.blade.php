<!-- Stored in resources/views/child.blade.php -->
@extends('template.app')
@section('title', 'Datos de Usuario')
<!--@section('sidebar')
    @parent
@endsection-->
@section('content')
@php 
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
@endphp
<style type="text/css">
.posicion-foto {
    border-radius: 5px;
    border: 1px solid #ddd;
    padding: 5px;
    width: 250px;
    height: 250px;
    margin-bottom: 5px;
    background-repeat: no-repeat;
    background-size: cover;
}
.posicion-foto2 {
    border-radius: 5px;
    border: 1px solid #ddd;
    padding: 5px;
    width: 250px;
    height: 606px;
    margin-bottom: 5px;
    background-repeat: no-repeat;
    background-size: cover;
}
p{
    color: #000;
    border-bottom: 0.5px solid #ccc;
    font-weight: 400;
}
.titulos{
        background-color: #ccc;
        color: #000;
        padding: 3px;
    }
    .titulos label{
        color: #000;
        margin-bottom: 0 !important;
    }
</style>
    <div class="container animated flipInX">
      <div class="row">

        <main class="col-sm-12 col-md-12 pt-6">
            <div class="pull-right">
                <div class="btn-group">
                  <a href="{{ url('/') }}/funcionarios" class="btn btn-sm btn-primary"><i class="fa fa-list" aria-hidden="true"></i> Listado de funcionarios</a>
                </div>
              </div>
              <div class="panel-title">
                    <h2>Ver datos usuario</h2>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-4" style="text-align:center; justify-content: center;">
                        <div class="col-md-12">
                        @if($usuario->foto!="")
                           <div class="posicion-foto" style="background-image: url('{{ url('/') }}/images/file/clientes/{{$usuario->foto}}')">
                           </div>
                        @else
                            <div class="posicion-foto" style="background-image: url('https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Perfil&w=200&h=200')">
                            </div>
                        @endif
                        </div>
                        <hr />
                        <div class="col-md-12">
                        @if($usuario->foto_completa!="")
                           <div class="posicion-foto2" style="background-image: url('{{ url('/') }}/images/file/clientes/{{$usuario->foto_completa}}')">
                           </div>
                        @else
                            <div class="posicion-foto2" style="background-image: url('https://placeholdit.imgix.net/~text?txtsize=45&txt=Sin%20Perfil&w=200&h=600')">
                            </div>
                        @endif
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="titulos"><label>Información Personal</label></div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="nombres" class="col-form-label">Nombres</label>                    
                                <p>{{$usuario->nombres}}</p>
                            </div>
                            <div class="col-3">
                                <label for="apellidos" class="col-form-label">Apellidos</label>                    
                               <p>{{$usuario->apellidos}}</p>
                            </div>
                            <div class="col-6">
                                <label for="apellidos" class="col-form-label">Numero Documento</label>                    
                               <p>{{$usuario->tipo_documento." - ".$usuario->documento}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="fecha_naicmiento" class="col-form-label">Fecha nacimiento</label>
                                <p>{{$usuario->fecha_nacimiento}}</p>
                            </div>      
                            <div class="col-4">
                                <label for="fecha_naicmiento" class="col-form-label">Telefono personal</label>
                                <p>{{$usuario->telefono_personal}}</p>
                            </div> 
                            <div class="col-4">
                                <label for="fecha_naicmiento" class="col-form-label">Telefono corporativo</label>
                                <p>{{$usuario->telefono_corporativo}}</p>
                            </div>                     
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="telefono" class="col-form-label">Correo personal</label>
                                <p>{{$usuario->correo_personal}}</p>
                            </div> 
                            <div class="col-6">
                                <label for="correo" class="col-form-label">Correo corporativo</label>
                                <p>{{$usuario->correo_corporativo}}</p>
                            </div>                   
                        </div>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="profesion" class="col-form-label">Profesión</label>
                                <p>{{$usuario->profesion}}</p>
                            </div>  
                            <div class="col-2">
                                <label for="profesion" class="col-form-label">Tipo Sangre</label>
                                <p>{{$usuario->tipo_sangre}}</p>
                            </div> 
                            <div class="col-6">
                                <label for="profesion" class="col-form-label">Dirección</label>
                                <p>{{$usuario->direccion}}</p>
                            </div>                        
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="pais" class="col-form-label">Ciudad nacimiento</label>
                                <p>{{$usuario->ciudad_nacimiento." ".$usuario->departamento_nacimiento}}</p>
                            </div>
                            <div class="col-6">
                                <label for="departamento" class="col-form-label">Ciudad residencia</label>
                                <p>{{$usuario->ciudad_residencia." ".$usuario->departamento_residencia}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 ">
                                <label for="pais_residencia" class="col-form-label">Descripcion de perfil</label>
                                <p>{{$usuario->descripcion}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                           <div class="col-md-12">
                            <div class="titulos"><label>Vinculación ESSI</label></div>
                            <div class="form-group row">
                                <div class="col-6">
                                    <label for="pais_residencia" class="col-form-label">Fecha vinculación a ESSI</label>
                                    <p>{{$usuario->fecha_vinculacion}}</p>
                                </div>
                                <div class="col-6">
                                    <label for="cargo" class="col-form-label">Cargo</label>
                                    <p>{{$usuario->cargo}}</p>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                            <div class="titulos"><label>Contacto Emergencia</label></div>
                            <div class="form-group row">
                            @foreach($contacto as $contact)
                                @if($contact->users==$usuario->id)
                                <div class="col-4">
                                    <label for="pais_residencia" class="col-form-label">Parentesco</label>
                                    <p>{{$contact->parentesco}}</p>
                                </div>
                                <div class="col-4">
                                    <label for="pais_residencia" class="col-form-label">Nombre</label>
                                    <p>{{$contact->nombre}}</p>
                                </div>
                                <div class="col-4">
                                    <label for="pais_residencia" class="col-form-label">Telefono</label>
                                    <p>{{$contact->telefono}}</p>
                                </div>
                                @endif
                            @endforeach
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="titulos"><label>Información Familiar</label></div>
                        <div class="form-group row " id="familia">
                            <div class="col-6">
                                <label for="cargo" class="col-form-label">Estado Civil</label>
                                <p>{{$usuario->estado_civil}}</p>
                            </div>
                            <div class="col-6">
                                <label for="pais_residencia" class="col-form-label">Numero de hijos</label>
                                <p>{{$usuario->hijos}}</p>
                            </div>
                            @if($usuario->estado_civil=="Casado/a"||$usuario->estado_civil="Unión Libre")
                                @foreach($familias as $key)
                                    @if($key->users==$usuario->id&&$key->parentesco=="Conyugue")
                                    <div class="col-6 conyugue">
                                        <label for="pais_residencia" class="col-form-label">Nombre del conyugue</label>
                                        <input class="form-control form-control-sm" type="hidden" name="familia[0][parentesco]" value="Conyugue">
                                        <p>{{ $key->nombre }}</p>
                                    </div>
                                    <div class="col-6 conyugue">
                                        <label for="pais_residencia" class="col-form-label">Fecha de nacimiento</label>
                                        <p>{{ $key->fecha_nacimiento }}</p>
                                    </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group row">
                        @foreach($familias as $key)
                            @if($key->users==$usuario->id&&$key->parentesco=="Hijo")
                            <div class="col-6">
                                <label for="pais_residencia" class="col-form-label">Nombre del hijo</label>
                                <p>{{$key->nombre}}</p>
                            </div>
                            <div class="col-6">
                                <label for="pais_residencia" class="col-form-label">Fecha de nacimiento</label>
                                <p>{{$key->fecha_nacimiento}}</p>
                            </div>
                            @endif
                        @endforeach
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="titulos"><label>Estudios Realizados</label></div>
                        @foreach($estudios as $estudio)
                        @if($estudio->users==$usuario->id)
                        <div class="form-group row">
                            <div class="col-4 ">
                                <label for="pais_residencia" class="col-form-label">Titulo Obtenido</label>
                                <p>{{$estudio->titulo}}</p>
                            </div>
                            <div class="col-3">
                                <label for="pais_residencia" class="col-form-label">Año</label>
                                <p>{{$estudio->anio}}</p>
                            </div>
                            <div class="col-5 ">
                                <label for="pais_residencia" class="col-form-label">Institución</label>
                                <p>{{$estudio->institucion}}</p>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-12">
                        <div class="titulos"><label>Salarios Mensuales</label></div>
                        <div class="form-group row">
                            <div class="col-2 ">
                                <label for="pais_residencia" class="col-form-label">Salario Basico</label>
                                <p>$ {{$usuario->salario_basico}}</p>
                                
                            </div>
                            <div class="col-2">
                                <label for="pais_residencia" class="col-form-label">Bonificación Fija</label>
                                <p>$ {{$usuario->bonificacion_fija}}</p>
                            </div>
                            <div class="col-2">
                                <label for="pais_residencia" class="col-form-label">Aux. Transporte</label>
                                <p>$ {{$usuario->aux_transporte}}</p>
                            </div>
                            <div class="col-2">
                                <label for="pais_residencia" class="col-form-label">Aux. Alimentación</label>
                                <p>$ {{$usuario->aux_alimentacion}}</p>
                            </div>
                            <div class="col-2">
                                <label for="pais_residencia" class="col-form-label">Aux. Vivienda</label>
                                <p>$ {{$usuario->aux_vivienda}}</p>
                            </div>
                            <div class="col-2">
                                <label for="pais_residencia" class="col-form-label">Otros Auxilios</label>
                                <p>$ {{$usuario->otros_auxilios}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="titulos"><label>Metas</label></div>
                        <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Oportunidades Identificadas</th>
                                <th>Montos Identificados</th>
                                <th>Negocios Cerrados</th>
                                <th>Montos Cerrados</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=0;$i<12;$i++)
                                @foreach($metas as $meta)
                                    @if($meta->users==$usuario->id&&($i+1)==$meta->mes)
                                    <tr class="text-center">
                                        <td>{{$meses[$i]}}</td>
                                        <td>
                                        {{$meta->oportunidad_identificada}}</td>
                                        <td>
                                        {{$meta->monto_identificado}}</td>
                                        <td>
                                        {{$meta->oportunidad_concretada}}</td>
                                        <td>
                                        {{$meta->monto_concretado}}</td>
                                    </tr>
                                    @endif
                                @endforeach
                            @endfor
                        </tbody>
                        </table>
                    </div>
                </div>    
        </main>
      </div>
    </div>
@endsection

@section('scripts')

<script src="{{ url('/') }}/components/file/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/locales/es.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/themes/explorer/theme.js" type="text/javascript"></script>
<script src="{{ url('/') }}/components/file/js/plugins/purify.min.js"></script>

<script type="text/javascript">
    $("#logo").fileinput({
        language: "es",
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="fa fa-times" aria-hidden="true"></i> Restablecer cambios',
        removeTitle: 'Cancelar o restablecer cambios',
        elErrorContainer: '#logo-errors',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ url('/') }}/images/file/avatar/man.svg" alt="Logo de la empresa" style="width:160px"><h6 class="text-muted">Clic para seleccionar foto</h6>',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

    i=1;
    j=0;
    k=0;
    h=0;
    agregarTelefono = function() {
        i++;
        h++;
        dato = `<div class="col-6 animated rollIn" id="T`+i+`">
                    <label class="col-form-label">Telefono `+h+`</label>
                        <div class="input-group">
                            <select name="telefono[`+i+`][telefono_tipo]" class="col-2 form-control form-control-sm" id="codteltipo`+i+`" onchange="funcambiarinput(this.id, `+i+`)">
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Radio">Radio</option>
                            </select>
                            <input type="text" name="telefono[`+i+`][telefono_indp]" class="col-2 form-control form-control-sm centrado" placeholder="IND P" id="codtelindp`+i+`" data-original-title="Indicativo Pais" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <input type="number" name="telefono[`+i+`][telefono_indc]" class="col-2 form-control form-control-sm centrado" placeholder="IND C" id="codtelindc`+i+`" data-original-title="Indicativo Ciudad" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <input type="number" name="telefono[`+i+`][telefono_numero]" class="col-4 form-control form-control-sm centrado" aria-label="Ingrese el numero" placeholder="Ingrese el numero" id="codtelnum`+i+`">
                            <input type="number" name="telefono[`+i+`][telefono_ext]" class="col-2 form-control form-control-sm centrado" placeholder="EXT" id="codtelext`+i+`" data-original-title="Extension" data-container="body" data-toggle="tooltip" data-placement="bottom">
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                </div>`;
        $( "#mastelefonos" ).after(dato);
        $("[data-toggle='tooltip']").tooltip();
    }

    quitar = function (id) {
        $(id).removeClass('rollIn').addClass('hinge');
        setTimeout(function(){ $(id).remove(); }, 1000);            
    }

    agregarCorreo = function () {
        j++;
        correo =   `<div class="col-6 animated rollIn" id="C`+j+`">
                        <label for="n_a_pesos" class="col-form-label">Correo `+j+`</label>
                        <div class="input-group">
                          <input class="form-control form-control-sm" type="email" name="correo[]" placeholder="Por favor ingrese un correo">
                          <span class="input-group-btn">
                            <button class="btn btn-secondary btn-sm" type="button" onclick="quitar('#C`+j+`')"><i class="fa fa-minus text-danger"></i></button>
                          </span>
                        </div>
                    </div>`; 
        $( "#correo" ).after(correo);
    }
   
    agregarRedesSociales = function() {
        k++;
        dato = `<div class="form-group row animated rollIn" id="R`+k+`">
                    <label class="col-2 col-form-label">Redes Sociales `+k+`</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <select name="redsocial[`+k+`][tipo]" class="col-2 form-control form-control-sm">
                                <option value="Facebook">Facebook</option>
                                <option value="Instagran">Instagran</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Linkedin">Linkedin</option>
                            </select>
                            <input type="text" class="col-10 form-control form-control-sm" name="redsocial[`+k+`][valor]" aria-label="Ingrese la red social" placeholder="Ingrese la red social">
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitar('#R`+k+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                    </div>
                </div>`;
        $( "#redessociales" ).after(dato);
    }
    $(function () {
        // inicializamos el plugin
        $('#pais').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Pais",
            pais: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("pais") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#estados').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un Departamento",
            estados: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("estados") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#pais').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#ciudad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una Ciudad",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("ciudades") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        dato: $('#estados').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#empresa').select2({
            // Activamos la opcion "Tags" del plugin
             placeholder: "Seleccione una empresa",
            ciudad: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("empresa") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });

        $('#sede').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione una sede",
            sede: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("sede") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
                        state: $('#empresa').val()
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
        $("[data-toggle='tooltip']").tooltip();
    });

    funcambiarinput = function (id, val) {
      valor = $("#"+id).val();
      if (valor==="Celular") {
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-10').addClass('col-9');            
      }else if(valor==="Telefono"){
          $("#codtelindp"+val).show();
          $("#codtelindc"+val).show();
          $("#codtelext"+val).show();
          $("#codtelnum"+val).removeClass('col-9 col-10').addClass('col-7'); 
      }else if(valor==="Radio"){
          $("#codtelindp"+val).hide();
          $("#codtelindc"+val).hide();
          $("#codtelext"+val).hide();
          $("#codtelnum"+val).removeClass('col-7 col-9').addClass('col-10');
      }
    }

</script>
@endsection