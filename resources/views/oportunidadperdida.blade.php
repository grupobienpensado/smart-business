@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Oportunidad Perdida')

@section('content')

<style type="text/css">
    .btn-sm,.form-control-sm{z-index:inherit!important}
    .content{text-align:-webkit-auto}
    form,html{font-size:13px!important}
    .estirar{width:100%}
    .radio label{margin-right:50px}
</style>

<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-sm-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>Formulario De Oportunidad Perdida</h3>
			</div>
        <form method="POST" action="{{ url('saveperdida') }}" enctype="multipart/form-data" class="estirar">
        <input type="hidden" name="id" value="{{$datos->id}}">
        {{ csrf_field() }}
      	 <div class="col-md-12 form-group">
         <label>¿Cual fue el motivo de la perdida de esta oportunidad? </label>
          <textarea name="motivo_perdida" id="editor1" rows="10" cols="80" required="">
            </textarea>
         </div> 
         <div class="col-md-12 form-group">
         <label>¿El suministro de equipos o el proyecto lo hará la competencia? </label>
         <div class="radio">
          <label>
            <input type="radio" class="seleccione" name="suministro_competencia" id="optionsRadios1" value="Si" required="">
            Si
          </label>
          <label>
            <input type="radio" class="seleccione" name="suministro_competencia" id="optionsRadios2" value="No" required="">
            No
          </label>
        </div>
         </div> 
         <div class="col-md-12 form-group competencia" style="display: none">
         <label>¿Que empresa realizará el proyecto? </label>
         <select name="empresa" class="form-control" id="empresa">
           @foreach($data['competencia'] as $dato)
           <option value="{{$dato->id}}">{{$dato->nombre}}</option>
           @endforeach
         </select>
         </div>
         <div class="col-md-12 form-group competencia" style="display: none">
         <label>Describa los equipos que suministrará: </label>
          <textarea name="equipos_suministrados" id="editor2" rows="10" cols="80">
            </textarea>
         </div> 
         <div class="col-md-12 form-group competencia" style="display: none">
         <label>¿Cual fue la ventaja competitiva o estrategia que permitió que la competencia ganara: </label>
          <textarea name="ventaja_competencia" id="editor3" rows="10" cols="80">
            </textarea>
         </div> 
         <div class="col-md-12 form-group">
         <label>¿Que acciones habría realizado para no perder la oportunidad: </label>
          <textarea name="acciones_realizadas" id="editor4" rows="10" cols="80" required="">
            </textarea>
         </div> 
         <div class="col-md-12 form-group">
         <label>¿Considera oportuno y necesario el monto y cantidad de recursos y tiempo invertido en esta oportunidad? </label>
         <div class="row">
             <div class="col-md-3 col-sm-12">
                <div class="radio">
                  <label>
                    <input type="radio" name="monto_oportuno" id="optionsRadios1" value="Si" required="" >
                    Si
                  </label>
                  <label>
                    <input type="radio" name="monto_oportuno" id="optionsRadios2" value="No" required="">
                    No
                  </label>
                </div>
             </div>
             <div class="col-md-9 col-sm-12 text-left">
                 <input type="hidden" name="gastos" value="{{$data['Total']}}">
                 <input type="hidden" name="horas" value="{{$data['Horas_hombre'][0]->tiempo}}">
                <p class="h3">$ {{number_format($data['Total'], 0, '.', ',')}} Total de Gastos y {{$data['Horas_hombre'][0]->tiempo}} (Horas) Total tiempo</p>
             </div>
         </div>
        <label>¿Porqué?</label>
        <textarea name="justificacion_monto_oportuno" id="editor5" rows="10" cols="80" required="">
            </textarea>
         </div>
         <div class="col-md-12 form-group">
         <label>¿Cual es la percepción del cliente hacia ESSI? </label>
          <textarea name="percepcion_essi" id="editor6" rows="10" cols="80" required="">
            </textarea>
         </div>
         <div class="col-md-12 form-group">
         <label>¿Cual es la percepción del cliente hacia los equipos de ESSI? </label>
          <textarea name="percepcion_maquina" id="editor7" rows="10" cols="80" required="">
            </textarea>
         </div>
         <div class="col-md-12 form-group">
         <label>¿Cuales son las futuras oportunidades con este cliente? </label>
          <textarea name="futuras_oportunidades" id="editor8" rows="10" cols="80" required="">
            </textarea>
         </div>
         <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
      </form>
    </div>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="{{url('/')}}/js/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" charset="utf-8">

CKEDITOR.replace( 'editor1' );
CKEDITOR.replace( 'editor2' );
CKEDITOR.replace( 'editor3' );
CKEDITOR.replace( 'editor4' );
CKEDITOR.replace( 'editor5' );
CKEDITOR.replace( 'editor6' );
CKEDITOR.replace( 'editor7' );
CKEDITOR.replace( 'editor8' );

$("body").on("click",".seleccione",function(e){
  if($(this).val()=="Si"){
    $(".competencia").show();
    $('#empresa').select2({
             placeholder: "Seleccione una empresa",
        });
  }else{
    $(".competencia").hide();
  }
})
</script>

@endsection
