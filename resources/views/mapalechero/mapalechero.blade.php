@extends('template.app')
@section('title', 'Mapa Lechero')
@section('content')
<style>
    .card-front p,.img-avatar,.ul{position:absolute}.card-front{height:200px;border-radius:120px 10px 10px 120px;-moz-border-radius:120px 10px 10px 120px;-webkit-border-radius:120px 10px 10px 120px;border:0 solid #000;background-color:#263238;-webkit-box-shadow:-5px 6px 19px -4px rgba(38,50,56,1);-moz-box-shadow:-5px 6px 19px -4px rgba(38,50,56,1);box-shadow:-5px 6px 19px -4px rgba(38,50,56,1);cursor:pointer}.ul{right:0;height:100%;width:7%}.ul:hover{width:60%;-webkit-transition:width .5s;transition:width .5s}@media (min-width:1572px) and (max-width:1786px){.ul{width:8%}}@media (min-width:1405px) and (max-width:1571px){.ul{width:9%}}@media (min-width:1271px) and (max-width:1404px){.ul{width:10%}}@media (min-width:1162px) and (max-width:1270px){.ul{width:11%}}@media (min-width:1071px) and (max-width:1161px){.ul{width:12%}}@media (min-width:995px) and (max-width:1070px){.ul{width:13%}}@media (min-width:929px) and (max-width:994px){.ul{width:14%}}@media (min-width:871px) and (max-width:928px){.ul{width:15%}}@media (min-width:821px) and (max-width:870px){.ul{width:16%}}@media (min-width:777px) and (max-width:870px){.ul{width:17%}}@media (min-width:768px) and (max-width:776px){.ul{width:18%}}.card-front .ul div:nth-child(1){background-color:#27ae60}.card-front .ul div:hover:nth-child(1){background-color:#051d60}.card-front .ul div:nth-child(2){background-color:#f1c40f}.card-front .ul div:hover:nth-child(2){background-color:#051d60}.card-front .ul div:nth-child(3){background-color:#3498db}.card-front .ul div:hover:nth-child(3){background-color:#051d60}.card-front .ul div:nth-child(4){background-color:#9b59b6}.card-front .ul div:hover:nth-child(4){background-color:#051d60}.img-avatar{left:10%;top:25%;max-height:90px;background:0 0}.swing-in-top-fwd{-webkit-animation:swing-in-top-fwd 1.5s cubic-bezier(.175,.885,.32,1.275) both;animation:swing-in-top-fwd 1.5s cubic-bezier(.175,.885,.32,1.275) both}@-webkit-keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0);transform:rotateX(0);-webkit-transform-origin:top;transform-origin:top;opacity:1}}@keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0);transform:rotateX(0);-webkit-transform-origin:top;transform-origin:top;opacity:1}}.card-front p{font-family:Novecento,sans-serif;color:#fff;left:36%;width:220px;text-align:left}.card-front p:nth-child(2){top:9%}.card-front p:nth-child(3){top:24%}.card-front p:nth-child(4){top:54%}.card-front p:nth-child(5),.card-front p:nth-child(6){top:64%}.snip1489{cursor:pointer;position:relative;font-size:40px;color:green!important;width:75px;margin:40px;height:75px;line-height:75px;display:inline-block;text-align:center;-webkit-perspective:50em;perspective:50em;text-decoration:none;-webkit-box-sizing:border-box;box-sizing:border-box}.snip1489:after{position:absolute;top:0;bottom:0;left:0;right:0;border:1px solid transparent;content:'';z-index:-1;border-radius:50%}.snip1489:before{color:#fff}.snip1489:after,.snip1489:before{-webkit-transition:all .45s ease-in-out;transition:all .45s ease-in-out}.snip1489.hover,.snip1489:active,.snip1489:hover{color:green}.snip1489.hover:after,.snip1489:active:after,.snip1489:hover:after{border-color:transparent green;-webkit-transform:rotate(360deg);transform:rotate(360deg)}.flotante{position:fixed;bottom:0;right:0}.flotante-1{position:fixed;bottom:10%;right:0}
</style>
<div class="main-header">
    <h2>Mapa Lechero</h2>
    <em class="titulo">Listado de mapa lechero</em>
</div>
{{ csrf_field() }}
<div class="row" id="contenido">

</div>
<div class="row flotante">
    <div class="col-md-12">
        <a class="snip1489" data-toggle="tooltip" data-placement="top" title="Agregar Mapa Lechero" onclick="crear()"><i class="fa fa-plus" aria-hidden="true"></i></a>
    </div>
</div>
@if(isset($data['permiso_titulo']) && $data['permiso_titulo']=="Si")
<div class="row flotante-1">
    <div class="col-md-12">
        <a class="snip1489" data-toggle="tooltip" data-placement="top" title="Editar titulos" onclick="editar_titulo()"><i class="fa fa-tumblr" aria-hidden="true"></i></a>
    </div>
</div>
@endif
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
       list();//LLamar la lista
    });
    /*Variables globales*/
    var v = 0; //Total de versiones
    var p = 0; //Total de palabras
    var t = 0; //Total de titulos
    var a = 0; //Fecha de actualización
    /**
     * Funcion para hacer un listado de los mapas lecheros del sistema
     */
    function list(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "{{url('/')}}/mapa-lechero",
            data: {
                _token: CSRF_TOKEN,
                accion: 0
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#contenido').html(e.data['contenido']);
                v = e.data['versiones'];
                p = e.data['palabras'];
                t = e.data['titulos'];
                a = e.data['Actualizacion'];
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    $(document).on('mouseenter', 'div.row.ul', function(){
        id = $(this).attr('id');
        $(this).children(':nth-child(1)').children('h5').text('Tiene '+v[id]+' Versiones.');
        $(this).children(':nth-child(2)').children('h5').text('Tiene '+p[id]+' Palabras.');
        $(this).children(':nth-child(3)').children('h5').text(t[id]+' Titulos');
        $(this).children(':nth-child(4)').children('h5').text(a[id]+' Ultima actualización');
    });
    $(document).on('mouseleave', 'div.row.ul', function(){
        $(this).children(':nth-child(1)').children('h5').text('V');
        $(this).children(':nth-child(2)').children('h5').text('P');
        $(this).children(':nth-child(3)').children('h5').text('T');
        $(this).children(':nth-child(4)').children('h5').text('A');
    });

    /**
     * Eliminar un mapa lechero
     * @param INT id ESTE ES EL ID DEL MAPA LECHERO
     */
    function eliminar(id){
        swal({
          title: 'Esta seguro?',
          text: "Desea eliminar este mapa lechero",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar',
          cancelButtonText: 'Cancelar'
        }).then(function () {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero",
                data: {
                    _token: CSRF_TOKEN,
                    id: id,
                    accion: 1
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == "Eliminado correctamente!"){
                        swal(
                          'Exito!',
                          e.data['msj'],
                          'success'
                        );
                    }else{
                        swal(
                          'Error!',
                          e.data['msj'],
                          'error'
                        );
                    }
                    list(); //Refrescar el listado
                }
            });
        });
    }

    /**
     * Crear un mapa lechero
     */
    function crear(){
        swal({
          title: 'Ingrese el nombre del nuevo mapa lechero?',
          input: 'text',
          inputPlaceholder: 'Nombre Mapa Lechero',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          cancelButtonText: 'Cancelar'
        }).then(function(val){
            if(val != ''){
               var CSRF_TOKEN = $('input[name=_token]').val();
               var jqxhr = $.ajax({
                    url: "/mapa-lechero",
                    data: {
                        _token: CSRF_TOKEN,
                        accion: 2,
                        nombre: val
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj'] == "Guardado correctamente!"){
                            swal(
                              'Exito!',
                              e.data['msj'],
                              'success'
                            );
                        }else{
                            swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            );
                        }
                        list(); //Refrescar el listado
                    }
                });
            }else{
                swal(
                  'Error!',
                  'Debe ingresar el nombre del mapa lechero',
                  'error'
                ).then(function(val){
                    crear(); //Volvemos a intentar ingresar un mapa lechero
                });
            }
        });
    }

    /**
     * Funcion para editar los titulos
     */
    function editar_titulo(){
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/mapa-lechero",
            data: {
                _token: CSRF_TOKEN,
                accion: 3
            },
            cache: false,
            type: 'POST',
            success: function(e){
                swal({
                  title: 'Titulos',
                  type: 'info',
                  html:e.data['contenido'],
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText: 'Nuevo',
                  cancelButtonText: 'Cerrar'
                }).then(function(){
                    crear_titulo();
                });
            }
        });
    }

    /**
     * Funcion para editar los titulos
     * @param INT id IDENTIFICACION DEL TITULO
     */
    function editar_titulo_1(id,valor){
        swal({
          title: 'Editar titulo',
          input: 'text',
          inputPlaceholder: valor,
          showCancelButton: true,
          cancelButtonColor: '#d33',
          cancelButtonText: 'Cancelar'
        }).then(function(val){
            if(val != ''){
               var CSRF_TOKEN = $('input[name=_token]').val();
               var jqxhr = $.ajax({
                    url: "/mapa-lechero",
                    data: {
                        _token: CSRF_TOKEN,
                        accion: 4,
                        id:id,
                        titulo: val
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj'] == "Guardado correctamente!"){
                            swal(
                              'Exito!',
                              e.data['msj'],
                              'success'
                            ).then(function(){
                                editar_titulo();
                            });
                        }else{
                            swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            ).then(function(){
                                editar_titulo();
                            });
                        }
                    }
                });
            }else{
                swal(
                  'Error!',
                  'Debe ingresar el nombre del titulo',
                  'error'
                ).then(function(val){
                    editar_titulo(); //Volvemos a ver el listado de titulos
                });
            }
        });
    }

    /**
     * Eliminar un titulo exstente
     * @param INT id IDENTIFICACION DEL TITULO
     */
    function eliminar_titulo(id){
        swal({
          title: 'Esta seguro?',
          text: "Desea eliminar este titulo",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar',
          cancelButtonText: 'Cancelar'
        }).then(function () {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero",
                data: {
                    _token: CSRF_TOKEN,
                    id: id,
                    accion: 5
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == "Eliminado correctamente!"){
                        swal(
                          'Exito!',
                          e.data['msj'],
                          'success'
                        ).then(function(){
                            editar_titulo(); //Volvemos a ver el listado de titulos
                        });
                    }else{
                        swal(
                          'Error!',
                          e.data['msj'],
                          'error'
                        ).then(function(){
                            editar_titulo(); //Volvemos a ver el listado de titulos
                        });
                    }
                }
            });
        });
    }

    /**
     * Función para crear titulo
     */
    function crear_titulo(){
        swal({
          title: 'Crear titulo',
          input: 'text',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          cancelButtonText: 'Cancelar'
        }).then(function(val){
            if(val != ''){
               var CSRF_TOKEN = $('input[name=_token]').val();
               var jqxhr = $.ajax({
                    url: "/mapa-lechero",
                    data: {
                        _token: CSRF_TOKEN,
                        accion: 6,
                        titulo: val
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e.data['msj'] == "Guardado correctamente!"){
                            swal(
                              'Exito!',
                              e.data['msj'],
                              'success'
                            ).then(function(){
                                editar_titulo();
                            });
                        }else{
                            swal(
                              'Error!',
                              e.data['msj'],
                              'error'
                            ).then(function(){
                                editar_titulo();
                            });
                        }
                    }
                });
            }else{
                swal(
                  'Error!',
                  'Debe ingresar el nombre del titulo',
                  'error'
                ).then(function(val){
                    editar_titulo(); //Volvemos a ver el listado de titulos
                });
            }
        });
    }
</script>
@endsection
