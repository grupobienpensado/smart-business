@extends('template.app')
@section('title', 'Mapa Lechero - Versiones')
@section('content')
<link href="/css/preload/preload.min.css" rel="stylesheet">
<link rel="stylesheet" href="/css/mapalechero/btnguardar.css">
<style>
    .file {
      visibility: hidden;
      position: absolute;
    }

    .titulos{
        border-right: 1px solid #000;
    }
    .titulo-no{
        background-color: #0A78DA;
        color: #fff;
        cursor: pointer;
    }
    .titulo-activo{
        background-color: #051d60;
        color: #5fc2ff;
        cursor: no-drop;
    }
    .fx-10{
        font-size: 7rem;
        color: green;
        cursor: pointer;
        transition: font-size 0.5s;
    }
    .fx-10:hover{
        font-size: 5rem;
        -webkit-transition: font-size 0.5s; /* Safari */
        transition: font-size 0.5s;
    }
    .actual{
        font-size: 5rem;
        border-right: 1.5px solid #051d60;
        color: #051d60;
        cursor: pointer;
        margin-bottom: -11px;
    }
    .anterior{
        font-size: 5rem;
        border-right: 1.5px solid red;
        color: red;
        cursor: pointer;
        margin-bottom: -11px;
    }
    .main-header{
        margin-bottom: 10px;
    }
    .version p{
        -webkit-transition: font-size 0.5s; /* Safari */
        transition: font-size 0.5s;
    }
    .version p:hover{
        font-size: 7rem;
        -webkit-transition: font-size 0.5s; /* Safari */
        transition: font-size 0.5s;
    }
    .msj-informativo{
        position: fixed;
        top: 10%;
        right: 0;
        opacity: 0;
    }
    /*Eliminar alerta que sale en el editor de texto*/
    .mce-notification.mce-has-close {
        display: none;
    }
    .flotante {
        position:fixed;
        bottom:0;
        right:0;
    }
    .flotante-img {
        position:fixed;
        bottom:10%;
        right:0;
    }
    .mt-20 {
        margin-top: 20rem;
    }
    /*flechas*/
    body .arrows {
      position: relative;
      /**/
      position: absolute;
      top: 38%;
      left: 40%;
      /**/
      width: 30px;
      height: 30px;
      transform: translate(-50%, -50%);
      cursor: pointer;
    }
    body .arrows:before {
      content: '';
      position: absolute;
      width: 100%;
      height: 100%;
      border-left: 3.3333333333px solid rgba(0, 0, 0, 0.7);
      border-bottom: 10px solid rgba(0, 0, 0, 0.7);
      transform: translate(30px, 0) rotate(0deg);
      animation: arrows 6s linear infinite;
    }
    body .arrows:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 100%;
      border-left: 10px solid rgba(0, 0, 0, 0.7);
      border-bottom: 10px solid rgba(0, 0, 0, 0.7);
      transform: translate(30px, 0px) rotate(0deg);
      animation: arrows 6s linear infinite -3s;
    }

    @keyframes arrows {
      0% {
        border-left: 10px solid rgba(0, 0, 0, 0);
        border-bottom: 10px solid rgba(0, 0, 0, 0);
        transform: translate(-30px, 0) rotate(45deg);
      }
      10%, 90% {
        border-left: 10px solid rgba(0, 0, 0, 0);
        border-bottom: 10px solid rgba(0, 0, 0, 0);
      }
      50% {
        border-left: 10px solid rgba(0, 0, 0, 0.7);
        border-bottom: 10px solid rgba(0, 0, 0, 0.7);
        transform: translate(1px, 0px) rotate(45deg);
      }
      100% {
        border-left: 10px solid rgba(0, 0, 0, 0);
        border-bottom: 10px solid rgba(0, 0, 0, 0);
        transform: translate(0.6666666667px, 0) rotate(45deg);
      }
    }
    /*Flechas 2*/
    body .arrows-2 {
      position: relative;
      /**/
      position: absolute;
      top: 38%;
      right: 20%;
      /**/
      width: 30px;
      height: 30px;
      transform: translate(-50%, -50%);
      cursor: pointer;
    }
    body .arrows-2:before {
      content: '';
      position: absolute;
      width: 100%;
      height: 100%;
      border-left: 3.3333333333px solid rgba(0, 0, 0, 0.7);
      border-bottom: 10px solid rgba(0, 0, 0, 0.7);
      transform: translate(30px, 0) rotate(0deg);
      animation: arrows-2 6s linear infinite;
    }
    body .arrows-2:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 100%;
      border-left: 10px solid rgba(0, 0, 0, 0.7);
      border-bottom: 10px solid rgba(0, 0, 0, 0.7);
      transform: translate(30px, 0px) rotate(0deg);
      animation: arrows-2 6s linear infinite -3s;
    }

    @keyframes arrows-2 {
      0% {
        border-left: 10px solid rgba(0, 0, 0, 0);
        border-bottom: 10px solid rgba(0, 0, 0, 0);
        transform: translate(-30px, 0) rotate(-150deg);
      }
      10%, 90% {
        border-left: 10px solid rgba(0, 0, 0, 0);
        border-bottom: 10px solid rgba(0, 0, 0, 0);
      }
      50% {
        border-left: 10px solid rgba(0, 0, 0, 0.7);
        border-bottom: 10px solid rgba(0, 0, 0, 0.7);
        transform: translate(1px, 0px) rotate(-150deg);
      }
      100% {
        border-left: 10px solid rgba(0, 0, 0, 0);
        border-bottom: 10px solid rgba(0, 0, 0, 0);
        transform: translate(0.6666666667px, 0) rotate(-150deg);
      }
    }

    /*COMENTARIOS*/

    textarea {
        font-size: 1.5rem;
        outline: none;
        border: none;
        display: block;
        margin: 0;
        padding: 0;
        -webkit-font-smoothing: antialiased;
        font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
        color: #555f77;
    }

    input::-webkit-input-placeholder,
    textarea::-webkit-input-placeholder {
        color: #ced2db;
    }

    input::-moz-placeholder,
    textarea::-moz-placeholder {
        color: #ced2db;
    }

    input:-moz-placeholder,
    textarea:-moz-placeholder {
        color: #ced2db;
    }

    input:-ms-input-placeholder,
    textarea:-ms-input-placeholder {
        color: #ced2db;
    }

    .comments {
        overflow-y: scroll;
        background-color: #f0f2fa;
        font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
        -webkit-font-smoothing: antialiased;
        width: 100%;
        height: 600px;
        padding-bottom: 1rem;
        padding-top: 1rem;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }

    .comment-wrap {
        margin-bottom: 1.25rem;
        display: table;
        width: 100%;
        min-height: 5.3125rem;
    }

    .photo {
        padding: 1.5rem;
        display: table-cell;
        width: 3.5rem;
    }

    .photo .avatar {
        height: 6.25rem;
        width: 6.25rem;
        border-radius: 50%;
        background-size: cover;
    }

    .comment-block {
        padding: 1rem;
        background-color: #fff;
        display: table-cell;
        vertical-align: top;
        border-radius: 0.1875rem;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);
    }

    .comment-block textarea {
        width: 100%;
        resize: none;
    }

    .comment-text {
        font-size: 1.5rem;
        margin-bottom: 1.25rem;
        color: #000;
        font-weight: normal;
    }

    .bottom-comment {
        color: #acb4c2;
    }

    .comment-date {
        float: left;
    }

    .comment-actions {
        float: right;
    }

    .comment-actions li {
        display: inline;
        margin: -2px;
        cursor: pointer;
    }

    .comment-actions li.complain {
        padding-right: 0.75rem;
        border-right: 1px solid #e1e5eb;
    }

    .comment-actions li.reply {
        padding-left: 0.75rem;
        padding-right: 0.125rem;
    }

    .comment-actions li:hover {
        color: #0095ff;
    }
    /*FIN COMENTARIOS*/
</style>
<div class="loader-cylon" style="display: none;"></div>
<div class="main-header">
    <h2>Mapa Lechero</h2>
    <em class="titulo">{{$data['mapa']->nombre}}</em><br>
    <img src="/images/file/clientes/{{$data['user']->foto}}" alt="User Avatar" class="img-circle mt-4" style="max-height:50px; cursor: pointer" data-toggle="tooltip" data-placement="bottom" title="Creado por: {{$data['user']->name}}">
    <em class="h5">Creado el: {{$data['fecha_creado']}}</em>
</div>
{{ csrf_field() }}
<div class="row">
    <div class="col-sm-12 col-md-4 titulos">
        <h2>Capitulos de investigación</h2>
        <div class="row">
            @foreach($data['titulos'] as $titulo)
            <div class="col-md-12" onclick="editar({{$titulo->id}})">
                <div class="container">
                    <div class="row titulo-no mx-5 my-3 shadow-1 shadow-3">
                        <div class="col-md-12">
                            <p class="h4">{{$titulo->titulo}}</p>
                            <p class="h5">Palabras : {{$data['palabras'][$titulo->id]}}</p>
                            <p class="h5">Última modificación : {{$data['ultima_modificacion'][$titulo->id]}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-12 col-md-8">
        <h2>Versiones</h2>
        <div class="row" id="contenido">
            <div class="col-md-12 mt-5">
                <p class="h4 mt-5"><em>Debe seleccionar un titulo para ver las versiones</em></p>
            </div>
        </div>
        <div class="row" id="comentarios">

        </div>
        <div class="row flotante" id="btn_guardar">

        </div>
    </div>
</div>
<div class="row flotante-img">
    <div class="col-md-12">
         <a class="snip1489" data-toggle="tooltip" data-placement="top" title="" onclick="cargar_imagenes()" data-original-title="Cargar imagen"><i class="fa fa-cloud-upload"></i></a>
     </div>
</div>
<div class="alert alert-warning msj-informativo" role="alert">
    <i class="fa fa-exclamation-triangle"></i> No hay mas versiones recientes
</div>
@endsection

@section('scripts')
   <script src="/assets/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
        pentitle="SCSS Arrow Animation";
        anterior = 0;
        siguiente = 0;
        parar = 'no';
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

        /**
         * Funcion para reiniciar valores Universales
         */
        function reiniciar_valores(){
            anterior = 0;
            siguiente = 0;
            parar = 'no';
            $('#comentarios').html('<div class="col-md-12"><h2 class="mt-20"><em>Seleccione una versión</em></h2></div>');
            $('#btn_guardar').html('');
        }

        /**
         * Función para traer el listado de versiones por titulo
         * @param INT id IDENTIFICACIÓN DEL TITULO
         */
        function editar(id){
            reiniciar_valores();
            $('.loader-cylon').css('display','block');
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero/version",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 0,
                    titulo: id,
                    mapa: '{{$data["mapa"]->id}}'
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#contenido').html(e.data['contenido']);
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.loader-cylon').css('display','none');
                }
            });
        }

        /**
         /*Función para pasar el listado de versiones a las mas recientes
         * @param INT id IDENTIFICACION DEL TITULO
         */
        function anterior_pagina(id){
            if(anterior==0){
               show_mensaje('<i class="fa fa-exclamation-triangle"></i> No hay más versiones recientes');
            }else{
               siguiente--;
               anterior++;
               paginar(siguiente,id);
           }
        };

        /**
         * Mensaje de alerta para las versiones
         * @param STRING msj MENSAJE
         */
        function show_mensaje(msj){
            $('.msj-informativo').html(msj);
            $('.msj-informativo').fadeTo( "slow" , 1);
            setTimeout(function() {
                $('.msj-informativo').fadeTo( "slow" , 0);
              },2500);
        }

        /**
         * Función para consultar las versiones mas antiguas que tiene el titulo
         * @param INT id IDENTIFICACION DEL TITULO
         */
        function siguiente_pagina(id){
            if(parar=='si'){
               show_mensaje('<i class="fa fa-exclamation-triangle"></i> No hay más versiones anteriores');
           }else{
               siguiente++;
               anterior--;
               paginar(siguiente,id);
           }
        }

        /**
         * Función para paginar ls versiones
         * @param INT pagina La pagina que se quiere ver
         * @param INT id     IDENTIFICACIÓN DEL TITULO
         */
        function paginar(pagina,id){
            $('.loader-cylon').css('display','block');
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                    url: "/mapa-lechero/version",
                    data: {
                        _token: CSRF_TOKEN,
                        accion: 1,
                        paginado: pagina,
                        titulo: id,
                        mapa: '{{$data["mapa"]->id}}'
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        actualizar_versiones(e.data['contenido'],e.data['parar']);
                        $('[data-toggle="tooltip"]').tooltip();
                        $('.loader-cylon').css('display','none');
                    }
                });
        }

        /**
         * Funcion para actualizar las versiones cada vez que pagine
         * @param STRING html TODO EL CONTENIDO DE LAS NUEVAS VERSIONES
         * @param STRING p INFORMACION SI HAY MAS REGISTROS PARA PAGINAR
         */
        function actualizar_versiones(html,p){
            parar = p;
            $('.version').remove();
            $('.no-version').remove();
            $('#crear-version').after(html);
        }

        $(document).on('click','.titulo-no',function(){
            $(".titulo-activo").each(function() {
              $(this).removeClass('titulo-activo');
              $(this).addClass('titulo-no');
            });
           $(this).removeClass('titulo-no');
           $(this).addClass('titulo-activo');
        });

        /**
         * Función para crear nueva versión
         * @param INT mapa   IDENTIFICACION DEL MAPA
         * @param INT titulo IDENTIFICACION DEL TITULO
         */
        function crear_version(mapa,titulo){
            $('.loader-cylon').css('display','block');
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero/version",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 2,
                    titulo: titulo,
                    mapa: mapa
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#comentarios').html(e.data['comentario']);
                    $('#btn_guardar').html(e.data['btn_guardar']);
                    activar_editor_texto();
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.loader-cylon').css('display','none');
                }
            });
        }
        /**
         * Funcion para activar tinymce
         */
        function activar_editor_texto(){
            tinymce.remove();
            tinymce.init({
                selector:'#comentario_administrador',
                height: 500,
                language : "es",
                theme: 'modern',
                branding: false,
                plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern'
            });
        }

        /**
         * Función para guardar una nueva versión
         * @param INT mapa   IDENTIFICACION DEL MAPA
         * @param INT titulo IDENTIFICACION DEL TITULO
         */
        function guardar_version(mapa,titulo){
            comentario = $('#chat_box').val();
            tinymce.remove();
            contenido = $('#comentario_administrador').val();
            activar_editor_texto();
            if(comentario!='' && contenido!=''){
               guardar_version_1(mapa,titulo);
            }else{
                swal({
                  type: 'error',
                  title: 'Error',
                  text: 'Debe ingresar un comentario y un contenido'
                });
            }
        }

        /**
         * Función para guardar una nueva versión
         * @param INT mapa   IDENTIFICACION DEL MAPA
         * @param INT titulo IDENTIFICACION DEL TITULO
         */
        function guardar_version_1(mapa,titulo){
            $('.loader-cylon').css('display','block');
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero/version",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 3,
                    titulo: titulo,
                    mapa: mapa,
                    comentario: $('#chat_box').val(),
                    contenido: $('#comentario_administrador').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == 'Error al guardar!'){
                       swal({
                          type: 'error',
                          title: 'Error',
                          text: e.data['msj']
                        });
                    }else{
                        swal({
                          type: 'success',
                          title: 'Guardado',
                          text: e.data['msj']
                        });
                        $('.titulo-activo').click();
                    }
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.loader-cylon').css('display','none');
                }
            });
        }

        /**
         * Ver la versión
         * @param INT id IDENTIFICACION DE LA VERSION
         */
        function verversion(id){
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero/version",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 4,
                    version: id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#comentarios').html(e.data['contenido']);
                    $('#btn_guardar').html('');
                    activar_ver_texto();
                }
            });
        }

        function activar_ver_texto(){
            tinymce.remove();
            tinymce.init({
                selector:'#comentario_administrador',
                height: 500,
                language : "es",
                theme: 'modern',
                branding: false,
                menubar: false,
                toolbar: false,
                readonly : 1
            });
        }

        /**
         * Función para cargar imagenes
         */
        function cargar_imagenes(){
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/mapa-lechero/version",
                data: {
                    _token: CSRF_TOKEN,
                    accion: 5,
                    id:{{$data['mapa']->id}}
                },
                cache: false,
                type: 'POST',
                success: function(e){
                   swal({
                      title: 'Listado de Imagenes',
                      type: 'info',
                      html:e.data['contenido'],
                      showCancelButton: false,
                      focusConfirm: false,
                      confirmButtonColor: '#FF0000',
                      confirmButtonText: 'Cerrar'
                    });
                    $('.swal2-modal').css('width','70%');
                }
            });
        }

        /**
         * Funcion para copiar link
         */
        function getlink(url) {
            var aux = document.createElement("input");
            aux.setAttribute("value",url);
            document.body.appendChild(aux);
            aux.select();
            document.execCommand("copy");
            document.body.removeChild(aux);
        }

        function subir_imagen(){
            swal({
              title: 'Subir imagen',
              type: 'info',
              html:`<div class="container"> <div class="row"> <div class="col-md-12"> <form id="action_frm" enctype="multipart/form-data">{{ csrf_field() }} <div class="form-group"> <input type="hidden" name="id_mapa_lechero" value="{{$data['mapa']->id}}"><input type="hidden" name="accion" value="6"> <input type="file" name="imagen" id="attached_file" class="file" accept="image/jpg,image/png,image/jpeg"> <div class="input-group col-xs-12"> <span class="input-group-addon"><i class="fa fa-image"></i></span> <input type="text" class="form-control input-lg" disabled placeholder="Cargar Imagen"> <span class="input-group-btn"> <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Buscar</button> </span> </div></div></form> </div><div class="col-md-12"> <div class="progress" id="upload_bar"> <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div></div><div class="col-md-12"> <a class="text-success" onclick="save_img()" style="font-size: 2rem;cursor:pointer;"><i class="fa fa-save"></i> Guardar</a> </div></div></div>`,
              showCloseButton: true,
              showCancelButton: false,
              focusConfirm: false,
              showConfirmButton: false,
            });
            $('.swal2-modal').css('width','70%');
        }

        $(document).on('click', '.browse', function(){
          var file = $(this).parent().parent().parent().find('.file');
          file.trigger('click');
        });
        $(document).on('change', '.file', function(){
          $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
        });

        function save_img(){
            var datos = new FormData($("#action_frm")[0]);
                if ($("#attached_file").val()) {
                    $("#upload_bar").show();
                }
                $.ajax({
                    url: '/mapa-lechero/version',
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: datos,
                    success: function(suss) {
                        $("#upload_bar").hide();
                        $('#action_modal').modal('hide');
                        $("#action_frm")[0].reset();
                        cargar_imagenes();
                    },
                    error: function(err) {
                        console.log(err);
                    },
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        //Upload Progress
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                $('#upload_bar > .progress-bar').css({
                                    "width": percentComplete + "%"
                                });
                            }
                        }, false);
                        //Upload progress
                        xhr.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = (evt.loaded / evt.total) * 100;
                                    $("#upload_bar > .progress-bar").css({
                                        "width": percentComplete + "%"
                                    });
                                }
                            },
                            false);
                        return xhr;
                    }
                });
        }
    </script>
@endsection
