@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Gastos')

@section('content')

<style type="text/css">
.btn-sm,.form-control-sm{z-index:inherit!important}.content{text-align:-webkit-auto}form,html{font-size:13px!important}#chartdiv{width:100%;height:500px!important}#chartdiv-2,#chartdiv-3,#chartdiv-4{width:100%;height:500px}.amcharts-graph-g2 .amcharts-graph-stroke{stroke-dasharray:3px 3px;stroke-linejoin:round;stroke-linecap:round;-webkit-animation:am-moving-dashes 1s linear infinite;animation:am-moving-dashes 1s linear infinite}@-webkit-keyframes am-moving-dashes{100%{stroke-dashoffset:-31px}}@keyframes am-moving-dashes{100%{stroke-dashoffset:-31px}}.lastBullet{-webkit-animation:am-pulsating 1s ease-out infinite;animation:am-pulsating 1s ease-out infinite}@-webkit-keyframes am-pulsating{0%{stroke-opacity:1;stroke-width:0}100%{stroke-opacity:0;stroke-width:50px}}@keyframes am-pulsating{0%{stroke-opacity:1;stroke-width:0}100%{stroke-opacity:0;stroke-width:50px}}.amcharts-graph-column-front{-webkit-transition:all .3s .3s ease-out;transition:all .3s .3s ease-out}.amcharts-graph-column-front:hover{fill:#496375;stroke:#496375;-webkit-transition:all .3s ease-out;transition:all .3s ease-out}.amcharts-graph-g3{stroke-linejoin:round;stroke-linecap:round;stroke-dasharray:500%;stroke-dasharray:0/;stroke-dashoffset:0/;-webkit-animation:am-draw 40s;animation:am-draw 40s}@-webkit-keyframes am-draw{0%{stroke-dashoffset:500%}100%{stroke-dashoffset:0}}@keyframes am-draw{0%{stroke-dashoffset:500%}100%{stroke-dashoffset:0}}.demo-flipper-front.demo-panel-white,body{background-color:#161616}.form-control2{width:100%;padding:0!important;font-size:.9rem!important;line-height:inherit!important;color:#464a4c;-webkit-background-clip:padding-box;border-radius:0!important;border:.5px solid rgba(0,0,0,.15)!important;text-align:right!important}.alert-dark{background-color:rgba(228,104,104,.9);border-color:rgba(0,0,0,.8);color:#fff}.alert{-webkit-animation:slide-from-top 1s cubic-bezier(.2,.7,.5,1);animation:slide-from-top 1s cubic-bezier(.2,.7,.5,1);padding:.75rem 1.25rem;margin-bottom:1rem;border:1px solid transparent;border-radius:.25rem}.table td{border:0!important}.input-group select{padding:8px!important}.red{color:#ea122a}.form-control.red:focus{color:#ea122a!important}
</style>
<?php
$numero_dias = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
$ajus=true;
$resta= strtotime(date('Y-m-')."10")-strtotime(date('Y-m-d'));
$fin =round($resta/(60*60*24));
$msj=$fin;
foreach($ajustes as $ajuste){
    if($ajuste->mes==date('m',strtotime("-1 month"))&&$ajuste->anio==date('Y',strtotime("-1 month"))&&$ajuste->user_id==$id||($ajuste->user_id==$id&&date('d')<10)){
        $ajus=false;
    }
}
?>
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
       <div class="pull-right">
        <div class="btn-group">
          <a href="/presupuestoanterior" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right" aria-hidden="true"></i> Presupuesto de {{$meses[(int)date("m")-1]}}</a>
          <a href="/presupuesto" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right" aria-hidden="true"></i> Presupuesto de {{$meses[(int)(date("m"))]}}</a>
          <a href="/presupuestoanual" class="btn btn-sm btn-warning"><i class="fa fa-calendar" aria-hidden="true"></i> Presupuesto Año {{date("Y")}}</a>
          <a href="/presupuestoanualsiguiente" class="btn btn-sm btn-warning"><i class="fa fa-calendar" aria-hidden="true"></i> Presupuesto Año {{date("Y")+1}}</a>
        </div>
      </div>
         <div class="col-md-12 text-center inline-block">
            <h3>Grafico Comparativo de Gastos {{$meses[(int)date('m',strtotime("-1 month"))]}} de {{date('Y',strtotime("-1 month"))}}</h3>
          </div>
          <div class="col-md-12">
            <div id="chartdiv-3"></div>
          </div>
          <div class="col-md-12">
            @if(!$ajus)
              <div class="alert alert-dark bpx" role="alert"  >
                <strong>Atención!</strong> quedan {{$msj}} dias para poder agregar un ajuste al presupuesto para el mes de {{$meses[(int)date('m',strtotime("-2 month"))]}}
            </div>
            @endif
          </div>
          <div class="col-md-12 text-center inline-block">
            <h3>Grafico Comparativo de Gastos vs Presupuestado (Anual)</h3>
          </div>
          <div class="col-md-12">
            <div id="chartdiv-4"></div>
          </div>
          <div class="col-md-12 text-center inline-block">
            <h3>Indice de Rendimiento de Comerciales {{date("Y")}}</h3>
          </div>
          <div class="col-md-12">
            <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                 <thead>
                   <tr>
                     <th class="text-center">#</th>
                     <th class="text-center">Comercial</th>
                     <th class="text-center">Mes</th>
                     <th class="text-center">Año</th>
                     <th class="text-center">Cumplimiento</th>
                   </tr>
                 </thead>
                 <tbody>
                  <?php
                    $h=1;
                    for($mes=0;$mes<12;$mes++){
                        foreach($usuarios as $usuario){
                            if($usuario->tipo!="comercial"){
                                $user=true; $valida=true;$name_usuario=ucwords($usuario->nombres." ".$usuario->apellidos);
                                foreach($individuales as $ind){
                                    if($ind->anio==date("Y")&&$ind->user_id==$usuario->id&&$ind->mes==($mes+1)){
                                        list($uno,$dos)=explode(" ",$ind->updated_at);
                                        list($anio,$mes1,$dia)=explode("-",$uno);
                                        $user=false;
                                        if((int)$mes1>$ind->mes){
                                            $valida=false;
                                            break;
                                        }
                                    }
                                }
                                if(!$user){ ?>
                       <tr>
                         <td class="text-center">{{$h}}</td>
                         <td class="text-center">{{$name_usuario}}</td>
                         <td class="text-center">{{$meses[(int)($mes)]}}</td>
                         <td class="text-center">{{date("Y")}}</td>
                         <td class="text-center"><img @if($valida) src="{{url('/')}}/images/icons/checked.png" @else src="{{url('/')}}/images/icons/cancel.png"@endif/></td>
                       </tr>
                       <?php
                                    $h++;
                                }
                            }
                        }
                    }
                    ?>
                 </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                 <thead>
                   <tr>
                     <th class="text-center">#</th>
                     <th class="text-center">Comercial</th>
                     <th class="text-center">Mes</th>
                     <th class="text-center">Año</th>
                     <th class="text-center">Presupuestado</th>
                     <th class="text-center">Gastado</th>
                     <th class="text-center">Ajuste Mensual</th>
                     <th class="text-center">Efectividad</th>
                   </tr>
                 </thead>
                 <tbody>
                  <?php
                    $h=1;
                    for($mes=11;$mes>=0;$mes--){
                        foreach($usuarios as $usuario){
                            $totalp=0;
                            $totalg=0;
                            $totala=0;
                            if($usuario->tipo!="comercial"){
                                $user=true;
                                $valida=true;
                                $name_usuario=ucwords($usuario->nombres." ".$usuario->apellidos);
                                foreach($individuales as $ind){
                                    if($ind->anio==date("Y")&&$ind->user_id==$usuario->id&&$ind->mes==($mes+1)){
                                        $totalp=$totalp+str_replace(",","",$ind->alimentacion)+str_replace(",","",$ind->transporte_interno)+str_replace(",","",$ind->transporte_intermunicipal)+str_replace(",","",$ind->tiquete_aereo)+str_replace(",","",$ind->papeleria)+str_replace(",","",$ind->invitacion_cliente)+str_replace(",","",$ind->alquiler_vehiculo)+str_replace(",","",$ind->gasolina_pasaje)+str_replace(",","",$ind->hotel)+str_replace(",","",$ind->otros);
                                    }
                                }
                                foreach($actividades as $act){
                                    $list=explode("-",$act->fecha_actividad);
                                    if($list[0]==date("Y")&&$act->user_id==$usuario->id&&$list[1]==($mes+1)){
                                        $totalg=$totalg+str_replace(",","",$act->alimentacion)+str_replace(",","",$act->transportes_internos)+str_replace(",","",$act->transportes_intermunicipales)+str_replace(",","",$act->tiquete_aereo)+str_replace(",","",$act->papeleria)+str_replace(",","",$act->invitacion_cliente)+str_replace(",","",$act->alquiler_vehiculo)+str_replace(",","",$act->gasolina_pasajes)+str_replace(",","",$act->hotel)+str_replace(",","",$act->otros);
                                    }
                                }
                                foreach($ajustes as $ajuste){
                                    if($ajuste->anio==date("Y")&&$act->user_id==$usuario->id&&$ajuste->mes==($mes+1)){
                                        $totala=$totalg+str_replace(",","",$act->valor_alimentacion)+str_replace(",","",$act->valor_transporte_interno)+str_replace(",","",$act->valor_transporte_intermunicipal)+str_replace(",","",$act->valor_tiquete_aereo)+str_replace(",","",$act->valor_papeleria)+str_replace(",","",$act->valor_invitacion_cliente)+str_replace(",","",$act->valor_alquiler_vehiculo)+str_replace(",","",$act->valor_gasolina_pasaje)+str_replace(",","",$act->valor_hotel)+str_replace(",","",$act->valor_otros);
                                    }
                                }
                                if(($mes+1)<=(int)date("m")){
                                    if($totalp){
                                        $efectivo=(($totalg+$totala)*100)/$totalp;
                                    }else{
                                        $efectivo=($totalg+$totala)*100;
                                    } ?>
                       <tr>
                         <td class="text-center">{{$h}}</td>
                         <td class="text-center">{{$name_usuario}}</td>
                         <td class="text-center">{{$meses[(int)($mes)]}}</td>
                         <td class="text-center">{{date("Y")}}</td>
                         <td class="text-center">$ {{number_format($totalp,"0",",",".")}}</td>
                         <td class="text-center">$ {{number_format($totalg,"0",",",".")}}</td>
                         <td class="text-center">$ {{number_format($totala,"0",",",".")}}</td>
                         <td class="text-center <?php if($efectivo>100){ ?>red<?php } ?>">{{number_format(round($efectivo),"0",",",".")}}%</td>
                       </tr>
                       <?php
                                    $h++;
                                }
                            }
                        }
                    } ?>
                 </tbody>
            </table>
          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="guardar_ajuste" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <img src="{{ url('/')}}/images/icons/essi_logo.png" class="logo-essi-modal">
      <h4 class="modal-title" id="myModalLabel">Realizar Ajuste al Presupuesto Mes de {{$meses[(int)date('m',strtotime("-1 month"))-1]}}</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body" >
    <form method="POST" action="{{ url('ajustegastos') }}" enctype="multipart/form-data">
    <input type="hidden" name="anio" value="{{date('Y',strtotime("-1 month"))}}">
    <input type="hidden" name="mes" value="{{date('m',strtotime("-1 month"))}}">
    {{ csrf_field() }}
      <div class="form-group row block" >
            <div class="col-md-12">
              <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
                 <thead>
                   <tr>
                     <th class="text-center">Alimentación</th>
                     <th class="text-center">Transporte Interno</th>
                     <th class="text-center">Transporte Intermunicipal</th>
                     <th class="text-center">Tiquete Aereo</th>
                     <th class="text-center">Papeleria</th>
                     <th class="text-center">Invitación Cliente</th>
                     <th class="text-center">Alquiler Vehiculo</th>
                     <th class="text-center">Gasolina y Pasaje</th>
                     <th class="text-center">Hotel</th>
                     <th class="text-center">Otros</th>
                   </tr>
                 </thead>
                 <tbody>
                 <tr>
                   <td>
                     <div class="input-group">
                        <select name="signo_alimentacion" class="col-4 form-control form-control2 valor_alimentacion">
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_alimentacion" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_transporte_interno" class="col-4 form-control form-control2 valor_transporte" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_transporte_interno" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_transporte_intermunicipal" class="col-4 form-control form-control2 valor_transporte_intermunicipal" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_transporte_intermunicipal" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_tiquete_aereo" class="col-4 form-control form-control2 valor_tiquete_aereo" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_tiquete_aereo" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_papeleria" class="col-4 form-control form-control2 valor_papeleria" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_papeleria" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_invitacion_cliente" class="col-4 form-control form-control2 valor_invitacion_cliente" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_invitacion_cliente" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_alquiler_vehiculo" class="col-4 form-control form-control2 valor_alquiler_vehiculo" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo"  type="text" name="valor_alquiler_vehiculo" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_gasolina_pasaje" class="col-4 form-control form-control2 valor_gasolina_pasaje" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_gasolina_pasaje" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_hotel" class="col-4 form-control form-control2 valor_hotel" >
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_hotel" placeholder="$0">
                    </div>
                   </td>
                   <td>
                     <div class="input-group">
                        <select name="signo_otros" class="col-4 form-control form-control2 valor_otros">
                            <option value="+"> + </option>
                            <option value="-"> - </option>
                        </select>
                        <input class="form-control dinero col-8 form-control2 signo" type="text" name="valor_otros" placeholder="$0">
                    </div>
                   </td>
                 </tr>
                 </tbody>
            </table> 
            </div>
          </div>
          <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
       </form>
    </div>    
    </div>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="/js/jquery.maskMoney.min.js"></script>
<script src="/assets/presupuesto/amcharts.js"></script>
<script src="/assets/presupuesto/serial.js"></script>
<script src="/assets/presupuesto/light.js"></script>
<script src="/assets/presupuesto/export.min.js"></script>
<link rel="stylesheet" href="/assets/presupuesto/export.css" type="text/css" media="all" />
<script type="text/javascript">
$("body").on("keyup",".signo",function(){
  if($("."+$(this).attr("name")).val()=="-"){
    $(this).addClass("red");
  }else{
    $(this).removeClass("red");
  }
});
$("body").on("change","#filtro",function(e){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/filtrogastos/"+$(this).val(), 
        cache: false,
        type: 'GET',
        data:{"Calendario":'<?php echo $numero_dias ?>'},
        success: function(e){ 
            eval(e.gastos);
            eval(e.anual);
            eval(e.diario);
        }
    });
})
$("body").on("click",".guardar_ajuste",function(e){
  $("#guardar_ajuste").modal();
})
  var chartData = [
    <?php 
    $acumulado=0;
    $diario_presupuesto=0;
    for($i=1;$i<=$numero_dias;$i++){
        $diario=0;
        if($i<10){
            $i="0".$i;
        }
        foreach($actividades as $key){
            if($key->fecha_actividad==date('Y-m-').$i&&$key->user_id==$id){
                $diario=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
                $acumulado=$acumulado+$diario;
            }
        }
        foreach($mi_presupuesto as $m_p){
            if($m_p->dia == $i){
                $diario_presupuesto+=str_replace(",","",$m_p->alimentacion)+str_replace(",","",$m_p->transporte_interno)+str_replace(",","",$m_p->transporte_intermunicipal)+str_replace(",","",$m_p->tiquete_aereo)+str_replace(",","",$m_p->papeleria)+str_replace(",","",$m_p->invitacion_cliente)+str_replace(",","",$m_p->alquiler_vehiculo)+str_replace(",","",$m_p->gasolina_pasaje)+str_replace(",","",$m_p->hotel)+str_replace(",","",$m_p->otros);
            }
        }
      ?>
    {
    "date": "{{date('Y-m-').$i}}",
    "distance": {{$acumulado}},
    "duration": {{$diario_presupuesto}}
  }, 
  <?php } ?>
  ];
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "serial",
"theme": "light",

  "dataDateFormat": "YYYY-MM-DD",
  "dataProvider": chartData,

  "addClassNames": true,
  "startDuration": 1,
  "marginLeft": 0,

  "categoryField": "date",
  "categoryAxis": {
    "parseDates": true,
    "minPeriod": "DD",
    "autoGridCount": false,
    "gridCount": 50,
    "gridAlpha": 0.1,
    "gridColor": "#FFFFFF",
    "axisColor": "#555555",
    "dateFormats": [ {
      "period": 'DD',
      "format": 'DD'
    }, {
      "period": 'WW',
      "format": 'MMM DD'
    }, {
      "period": 'MM',
      "format": 'MMM'
    }, {
      "period": 'YYYY',
      "format": 'YYYY'
    } ]
  },

  "valueAxes": [ {
    "id": "a1",
    "title": "Gastos Diarios",
    "gridAlpha": 0,
    "axisAlpha": 0
  }, {
    "id": "a2",
    "position": "right",
    "gridAlpha": 0,
    "axisAlpha": 0,
    "labelsEnabled": false
  }, {
    "id": "a3",
    "title": "Presupuesto",
    "position": "right",
    "gridAlpha": 0,
    "axisAlpha": 0,
    "inside": true,
  } ],
  "graphs": [ {
    "id": "g1",
    "valueField": "distance",
    "title": "Gasto Diario",
    "type": "column",
    "fillAlphas": 0.9,
    "valueAxis": "a1",
    "balloonText": "[[value]]",
    "legendValueText": "[[value]]",
    "legendPeriodValueText": "total: [[value.sum]]",
    "lineColor": "#8aa326",
    "alphaField": "alpha"
  }, {
    "id": "g3",
    "title": "Presupuesto",
    "valueField": "duration",
    "type": "line",
    "valueAxis": "a3",
    "lineColor": "#ff5755",
    "balloonText": "[[value]]",
    "lineThickness": 1,
    "legendValueText": "[[value]]",
    "bullet": "square",
    "bulletBorderColor": "#ff5755",
    "bulletBorderThickness": 1,
    "bulletBorderAlpha": 1,
    "dashLengthField": "dashLength",
    "animationPlayed": true
  } ],

  "chartCursor": {
    "zoomable": false,
    "categoryBalloonDateFormat": "DD",
    "cursorAlpha": 0,
    "valueBalloonsEnabled": false
  },
  "legend": {
    "bulletType": "round",
    "equalWidths": false,
    "valueWidth": 120,
    "useGraphSettings": true,
  }
} );

$(function () {
    $(".dinero").maskMoney();
  })

var chart = AmCharts.makeChart( "chartdiv-3", {
  "type": "serial",
  "legend": {
      "useGraphSettings": true
  },
  "addClassNames": true,
  "theme": "light",
  "autoMargins": false,
  "marginLeft": 90,
  "marginRight": 8,
  "marginTop": 10,
  "marginBottom": 26,
  "balloon": {
    "adjustBorderColor": false,
    "horizontalPadding": 10,
    "verticalPadding": 8,
    "color": "#ffffff"
  },

  "dataProvider": [
   <?php
    $acumulado=0;
    $diario_presupuesto=0;
    for($i=1;$i<=$numero_dias;$i++){
        $diario=0;
        if($i<10){
            $i="0".$i;
        }
    foreach($actividades as $key){
        if($key->fecha_actividad==date('Y-m-').$i&&$key->user_id==$id){
            $diario=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
            $acumulado=$acumulado+$diario;
        }
    }
    foreach($mi_presupuesto as $m_p){     
        if($m_p->dia == $i){
            $diario_presupuesto+=str_replace(",","",$m_p->alimentacion)+str_replace(",","",$m_p->transporte_interno)+str_replace(",","",$m_p->transporte_intermunicipal)+str_replace(",","",$m_p->tiquete_aereo)+str_replace(",","",$m_p->papeleria)+str_replace(",","",$m_p->invitacion_cliente)+str_replace(",","",$m_p->alquiler_vehiculo)+str_replace(",","",$m_p->gasolina_pasaje)+str_replace(",","",$m_p->hotel)+str_replace(",","",$m_p->otros);
        }
    }
    ?>
    {
    "year": "{{$i}}",
    "income": {{$acumulado}},
    "expenses": {{$diario_presupuesto}}
  }, 
  <?php } ?>
    ],
  "valueAxes": [ {
    "axisAlpha": 0,
    "position": "left"
  } ],
  "startDuration": 1,
  "graphs": [ {
    "alphaField": "alpha",
    "balloonText": "<span style='font-size:12px;'>[[title]] en {{$meses[date("m")-1]}} [[category]]:<br><span style='font-size:20px;'>$ [[value]]</span> [[additional]]</span>",
    "fillAlphas": 1,
    "title": "Gastado según bitácora",
    "type": "column",
    "valueField": "income",
    "dashLengthField": "dashLengthColumn"
  }, {
    "id": "graph2",
    "balloonText": "<span style='font-size:12px;'>[[title]] en {{$meses[date("m")-1]}} [[category]]:<br><span style='font-size:20px;'>$ [[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Presupuesto mensual de gastos",
    "valueField": "expenses",
    "dashLengthField": "dashLengthLine"
  } ],
  "categoryField": "year",
  "categoryAxis": {
    "gridPosition": "start",
    "axisAlpha": 0,
    "tickLength": 0
  },
  "export": {
    "enabled": false
  }
} );
AmCharts.makeChart("chartdiv-4",
        {
          "type": "serial",
          "legend": {
              "useGraphSettings": true
          },
          "categoryField": "category",
          "autoMarginOffset": 40,
          "marginRight": 60,
          "marginTop": 60,
          "startDuration": 1,
          "fontSize": 13,
          "theme": "default",
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-1",
              "lineThickness": 3,
              "title": "Presupuesto anual",
              "type": "smoothedLine",
              "valueField": "column-1"
            },
            {
              
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-2",
              "lineThickness": 3,
              "title": "Gastos según bitácora",
              "type": "smoothedLine",
              "valueField": "column-2"
            },
            {
              
              "balloonText": "[[title]] de [[category]]:[[value]]",
              "bullet": "round",
              "bulletSize": 10,
              "id": "AmGraph-3",
              "lineThickness": 3,
              "title": "Presupuesto mensual de gastos",
              "type": "smoothedLine",
              "valueField": "column-3"
            },
            
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "titles": [],
          "dataProvider": [
          <?php
            $totalp=0;
            $totalpind=0;
            $totalg=0;
            for($i=0;$i<12;$i++){
                foreach ($anuales as $key) {
                  $o=$i+1;
                  if($o==$key->mes&&date("Y")==$key->anio&&$key->user_id==$id){
                    $totalp+=str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transporte_interno)+str_replace(",","",$key->transporte_intermunicipal)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasaje)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
                  }
                } 
                foreach ($actividades as $key) {
                  $o=$i+1;
                  if($o<10){
                    $o="0".$o;
                  }
                  for($k=1;$k<=31;$k++){
                    if($k<10){
                      $k="0".$k;
                    }
                    if(date("Y")."-".$o."-".$k==$key->fecha_actividad && $key->user_id==$id){
                      $totalg=$totalg+str_replace(",","",$key->alimentacion)+str_replace(",","",$key->transportes_internos)+str_replace(",","",$key->transportes_intermunicipales)+str_replace(",","",$key->tiquete_aereo)+str_replace(",","",$key->papeleria)+str_replace(",","",$key->invitacion_cliente)+str_replace(",","",$key->alquiler_vehiculo)+str_replace(",","",$key->gasolina_pasajes)+str_replace(",","",$key->hotel)+str_replace(",","",$key->otros);
                    }
                  }
                }
              foreach ($individuales as $ind) {
                 if(date("Y")==$ind->anio&&((int)$i+1)==$ind->mes){
                    $totalpind=$totalpind+str_replace(",","",$ind->alimentacion)+str_replace(",","",$ind->transporte_interno)+str_replace(",","",$ind->transporte_intermunicipal)+str_replace(",","",$ind->tiquete_aereo)+str_replace(",","",$ind->papeleria)+str_replace(",","",$ind->invitacion_cliente)+str_replace(",","",$ind->alquiler_vehiculo)+str_replace(",","",$ind->gasolina_pasajes)+str_replace(",","",$ind->hotel)+str_replace(",","",$ind->otros);
                  }
              }
              if($i<10){
                $o="0".($i+1);
              }else{
                 $o=$i+1;
              }
              $dia=15;
              if($o==12){
                $dia=10;
              }
                ?>
                {
                  "category": "{{date('Y-').$o}}",
                  "column-1": {{$totalp}},
                  "column-2": {{$totalg}},
                  "column-3": {{$totalpind}}
                },
          <?php } ?>
          ]
        }
      );
</script>

@endsection
