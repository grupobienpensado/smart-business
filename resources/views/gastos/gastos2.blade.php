@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Gastos')

@section('content')
<style type="text/css">
.btn-sm,.form-control-sm{z-index:inherit!important}.content{text-align:-webkit-auto}form,html{font-size:13px!important}#chartdiv{width:100%;height:500px!important}#chartdiv-2,#chartdiv-3,#chartdiv-4{width:100%;height:500px}.amcharts-graph-g2 .amcharts-graph-stroke{stroke-dasharray:3px 3px;stroke-linejoin:round;stroke-linecap:round;-webkit-animation:am-moving-dashes 1s linear infinite;animation:am-moving-dashes 1s linear infinite}@-webkit-keyframes am-moving-dashes{100%{stroke-dashoffset:-31px}}@keyframes am-moving-dashes{100%{stroke-dashoffset:-31px}}.lastBullet{-webkit-animation:am-pulsating 1s ease-out infinite;animation:am-pulsating 1s ease-out infinite}@-webkit-keyframes am-pulsating{0%{stroke-opacity:1;stroke-width:0}100%{stroke-opacity:0;stroke-width:50px}}@keyframes am-pulsating{0%{stroke-opacity:1;stroke-width:0}100%{stroke-opacity:0;stroke-width:50px}}.amcharts-graph-column-front{-webkit-transition:all .3s .3s ease-out;transition:all .3s .3s ease-out}.amcharts-graph-column-front:hover{fill:#496375;stroke:#496375;-webkit-transition:all .3s ease-out;transition:all .3s ease-out}.amcharts-graph-g3{stroke-linejoin:round;stroke-linecap:round;stroke-dasharray:500%;stroke-dasharray:0/;stroke-dashoffset:0/;-webkit-animation:am-draw 40s;animation:am-draw 40s}@-webkit-keyframes am-draw{0%{stroke-dashoffset:500%}100%{stroke-dashoffset:0}}@keyframes am-draw{0%{stroke-dashoffset:500%}100%{stroke-dashoffset:0}}.demo-flipper-front.demo-panel-white,body{background-color:#161616}.form-control2{width:100%;padding:0!important;font-size:.9rem!important;line-height:inherit!important;color:#464a4c;-webkit-background-clip:padding-box;border-radius:0!important;border:.5px solid rgba(0,0,0,.15)!important;text-align:right!important}.alert-dark{background-color:rgba(228,104,104,.9);border-color:rgba(0,0,0,.8);color:#fff}.alert{-webkit-animation:slide-from-top 1s cubic-bezier(.2,.7,.5,1);animation:slide-from-top 1s cubic-bezier(.2,.7,.5,1);padding:.75rem 1.25rem;margin-bottom:1rem;border:1px solid transparent;border-radius:.25rem}.table td{border:0!important}.input-group select{padding:8px!important}.red{color:#ea122a}.form-control.red:focus{color:#ea122a!important}.advertencia{text-align:left;color:#ff0;background:#000}#aplicar_filtro{border:none;background:#1e92af;color:#fff;font-size:12px;border-radius:5px;position:relative;box-sizing:border-box;transition:all .5s ease;cursor:pointer;padding:10px 35px;overflow:hidden}#aplicar_filtro:hover{background:rgba(0,0,0,0);color:#3a7999;box-shadow:inset 0 0 0 3px #3a7999}#aplicar_filtro:before{font-family:FontAwesome;content:"\f002";position:absolute;top:11px;left:-30px;transition:all .2s ease}#aplicar_filtro:hover:before{left:7px}
</style>
<?php
$numero_dias = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
$meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
?>
{{ csrf_field() }}
<div class="container-fluid animated slideInDown">
    <div class="row">
        <div class="col-md-12 panel-view">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="active"><a href="#"><i class="fa fa-area-chart"></i> Gafico Comparativo</a></li>
                        <li><a href="/presupuesto"><i class="fa fa-money"></i> Presupuesto Mensual</a></li>
                        <li><a href="/presupuestoanualsiguiente"><i class="fa fa-usd"></i> Presupuesto Anual</a></li>
                        <li><a href="/ajustes"><i class="fa fa-balance-scale"></i> Ajuste Mensual</a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <form method="POST" action="{{ url('filtrargastos') }}" enctype="multipart/form-data" class="mx-3" style="width:100%;" >
                  <div class="col-md-12">
                    <div class="row" style="margin-top: 2.5%;">
                      <div class="col-md-4">
                        <?php if($permiso_filtro_usuario=="Si"){ ?>
                          <select class="form-control filtro" name="usuario" id="filtro_usuarios" style="height: 100%">
                            <option value="">Filtrar por usuario</option>
                            <?php foreach($usuarios_fil as $usuario){ ?>
                              <option value="{{$usuario->id}}">{{$usuario->nombres.' '.$usuario->apellidos}}</option>
                            <?php  } ?>
                            <option value="sumatoria">Sumatoria General</option>
                          </select>
                        <?php } ?>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control filtro" name="mes" id="filtro_mes" style="height: 100%">
                          <option value="">Filtrar por mes</option>
                          <?php

                          for($i=0;$i<=11;$i++){
                            $fecha=strtotime ( '-'.$i.' month' , strtotime ( date('Y-m') ) ) ;
                            $numero_mes=((int)(date('m', $fecha)))-1;
                            $mes=$meses[$numero_mes];
                            $ano=date('Y' , $fecha);
                          ?>
                          <option value="{{date ( 'Y-m' , $fecha )}}">{{$mes}} de {{$ano}}</option>
                        <?php
                          }
                          ?>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <select class="form-control filtro" name="tipo" id="filtro_tipo" style="height: 100%;">
                          <option value="">Filtrar por tipo</option>
                          <option value="alimentacion">Alimentacion</option>
                          <option value="transporte_interno">Transporte Interno</option>
                          <option value="transporte_intermunicipal">Transporte Intermunicipal</option>
                          <option value="tiquete_aereo">Tiquete Aereo</option>
                          <option value="papeleria">Papeleria</option>
                          <option value="invitacion_cliente">Invitacion Cliente</option>
                          <option value="alquiler_vehiculo">Alquiler Vehiculo</option>
                          <option value="gasolina_pasaje">Gasolina Pasaje</option>
                          <option value="hotel">Hotel</option>
                          <option value="otros">Otros</option>
                        </select>
                      </div>
                      <div class="col-md-1" style="margin-top: 0.5%;">
                        <button type="submit" id="btn_filtrar" class="btn btn-essi pull-right" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>

                        <a id="aplicar_filtro">Aplicar Filtro</a>
                      </div>
                    </div>
                  </div>
                </form>
            </div>

            <div class="row my-5" id="content-graficas">
                <div class="col-md-12 text-center inline-block">
                    <h3>Grafico Comparativo de Gastos {{$meses[(int)date('m',strtotime("-1 month"))]}} de {{date('Y',strtotime("-1 month"))}}</h3>
                </div>
                <div class="col-md-12" id="grafico_comparativo_gastos">
                    <div id="chartdiv-3"></div>
                </div>
                <div class="col-md-12">
                    <?=$data['mensaje_ajuste']?>
                </div>
                <div class="col-md-12 text-center inline-block">
                    <h3>Grafico Comparativo de Gastos vs Presupuestado (Anual)</h3>
                </div>
                <div class="col-md-12" id="grafico_comparativo_gastos_vs_presupuesto">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/jquery.maskMoney.min.js"></script>
<script src="/assets/presupuesto/amcharts.js"></script>
<script src="/assets/presupuesto/serial.js"></script>
<script src="/assets/presupuesto/light.js"></script>
<script src="/assets/presupuesto/export.min.js"></script>
<link rel="stylesheet" href="/assets/presupuesto/export.css" type="text/css" media="all" />
<script>
    var CSRF_TOKEN = $('input[name=_token]').val();
    $(document).ready(function(){
        grafico_comparativo_gastos('','','');
        grafico_comparativo_gastos_vs_presupuesto('','','');
    });
    /**
     * Función para crear grafica comparativa de gastos
     */
    function grafico_comparativo_gastos(usuario,ano,mes,tipo){
        var jqxhr = $.ajax({
            url: "/gastos-comparativo-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 0,
                usuario:usuario,
                ano:ano,
                mes:mes,
                tipo:tipo
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#grafico_comparativo_gastos').html(e.data['html']);
                $('#content-graficas h3:eq(0)').html(e.data['titulo']);
            }
        });
    }

    function grafico_comparativo_gastos_vs_presupuesto(usuario,ano,tipo){
        var jqxhr = $.ajax({
            url: "/gastos-comparativo-accion",
            data: {
                _token: CSRF_TOKEN,
                accion: 1,
                usuario:usuario,
                ano:ano,
                tipo:tipo
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#grafico_comparativo_gastos_vs_presupuesto').html(e.data['html']);
            }
        });
    }
    $("body").on("click","#aplicar_filtro",function(){
      var fecha = ($('#filtro_mes').val()).split('-');
      var ano = fecha[0];
      var mes = fecha[1];
      var tipo = $('#filtro_tipo').val();
      grafico_comparativo_gastos($('#filtro_usuarios').val(),ano,mes,tipo);
      grafico_comparativo_gastos_vs_presupuesto($('#filtro_usuarios').val(),ano,tipo);
    });
    console.log(`<?=$data['Query']?>`);
</script>
@endsection
