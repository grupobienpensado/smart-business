@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Presupuesto')

@section('content')

<style type="text/css">
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 1.5rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}
.table td{
    line-height: 0;
    border: 0 !important;
}
.dia{
  line-height: 2 !important;
}
#chartdiv {
  width : 100%;
  height  : 500px !important;
}
</style>
@php

$semana=["Dom","Lun","Mar","Mie","Jue","Vie","Sab"];
$meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic"];
$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

@endphp
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>Ultimos Ajustes Al Presupuesto</h3>
			</div>
      	 <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th width="100">Mes</th>
                 <th class="text-center">Alimentación</th>
                 <th class="text-center">Transporte Interno</th>
                 <th class="text-center">Transporte Intermunicipal</th>
                 <th class="text-center">Tiquete Aereo</th>
                 <th class="text-center">Papeleria</th>
                 <th class="text-center">Invitación Cliente</th>
                 <th class="text-center">Alquiler Vehiculo</th>
                 <th class="text-center">Gasolina y Pasaje</th>
                 <th class="text-center">Hotel</th>
                 <th class="text-center">Otros</th>
               </tr>
             </thead>
             <tbody>
             @php $fecha=strtotime("-12 Month",strtotime(date("Y-m"))); 
             $fechacal=date("Y-m",$fecha);
             @endphp
             @for($i=0;$i<12;$i++)
             
             @php $fecha=strtotime("+".$i." Month",strtotime($fechacal)); 
             $fecha=date("Y-m",$fecha);
             $list=explode("-",$fecha);

             $sentencia = \Illuminate\Support\Facades\DB::select("SELECT * FROM ajuste_gastos WHERE `anio`='".$list[0]."' AND mes='".$list[1]."' AND user_id='".Auth::user()->id."' ");
             @endphp
               <tr>
                 <td class="dia">{{$meses2[(int)$list[1]-1]."/".$list[0]}}</td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo$sentencia[0]->valor_alimentacion?>" <?php if(substr($sentencia[0]->valor_alimentacion, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_transporte_interno?>" <?php if(substr($sentencia[0]->valor_transporte_interno, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_transporte_intermunicipal?>" <?php if(substr($sentencia[0]->valor_transporte_intermunicipal, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_tiquete_aereo?>" <?php if(substr($sentencia[0]->valor_tiquete_aereo, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_papeleria?>" <?php if(substr($sentencia[0]->valor_papeleria, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_invitacion_cliente?>" <?php if(substr($sentencia[0]->valor_invitacion_cliente, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_alquiler_vehiculo?>" <?php if(substr($sentencia[0]->valor_alquiler_vehiculo, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_gasolina_pasaje?>" <?php if(substr($sentencia[0]->valor_gasolina_pasaje, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_hotel?>" <?php if(substr($sentencia[0]->valor_hotel, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
                 <td><input class="form-control2 form-control" type="text" readonly="" <?php if(count($sentencia)>0){ ?> value="<?php echo $sentencia[0]->valor_otros?>" <?php if(substr($sentencia[0]->valor_otros, 0,1)=="-"){?> style="color:#ef0808" <?php } ?> <?php }else{ ?> value="0" <?php } ?> ></td>
               </tr>
               @endfor
             </tbody>
           </table>
         </div>
         <div class="col-md-12 inline-block">
          <?php 
            $nombre_mes=strtotime("-1 Month",strtotime(date("Y-m")));
            $nombre_mes=date("Y-m",$nombre_mes);
            $list2=explode("-",$nombre_mes);
            ?>
            <h3>Grafico Comparativo Ajustes de Gastos {{$meses2[(int)$list2[1]-1]}}</h3>
        </div>
         <div class="col-md-12">
           <div id="chartdiv"></div>
         </div>
    </div>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/none.js"></script>
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" charset="utf-8">

var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "none",
    "marginRight":30,
    "autoMarginOffset":20,
    "dataProvider": [
    <?php for($i=0;$i<12;$i++){
        $total_act=0;
       $fecha=strtotime("+".$i." Month",strtotime($fechacal)); 
       $fecha=date("Y-m",$fecha);
       $list=explode("-",$fecha);
       $sentencia = \Illuminate\Support\Facades\DB::select("SELECT * FROM ajuste_gastos WHERE `anio`='".$list[0]."' AND mes='".$list[1]."' AND user_id='".Auth::user()->id."' ");
       if(count($sentencia)>0){
        $sumajuste=str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_alimentacion))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_transporte_interno))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_transporte_intermunicipal))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_tiquete_aereo))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_papeleria))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_invitacion_cliente))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_gasolina_pasaje))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_hotel))+str_replace(",", "",str_replace(".", ",",$sentencia[0]->valor_otros));
       }else{
        $sumajuste=0;
       }
       for($o=1;$o<=31;$o++){
        if($o<10){
          $o="0".$o;
        }
          foreach($actividades as $actividad){
            $fecha_f=$fecha."-".$o;
            if($actividad->fecha_actividad==$fecha_f&&$actividad->user_id==Auth::user()->id){
              $total_act=$total_act+str_replace(",", "",str_replace(".", ",",$actividad->alimentacion))+str_replace(",", "",str_replace(".", ",",$actividad->transportes_internos))+str_replace(",", "",str_replace(".", ",",$actividad->transportes_intermunicipales))+str_replace(",", "",str_replace(".", ",",$actividad->tiquete_aereo))+str_replace(",", "",str_replace(".", ",",$actividad->papeleria))+str_replace(",", "",str_replace(".", ",",$actividad->invitacion_cliente))+str_replace(",", "",str_replace(".", ",",$actividad->gasolina_pasajes))+str_replace(",", "",str_replace(".", ",",$actividad->hotel))+str_replace(",", "",str_replace(".", ",",$actividad->otros));
            }
         }
       }
       $diferen=$total_act+$sumajuste;
     ?>
      {
        "date": "<?php echo $meses2[(int)$list[1]-1]."/".$list[0] ?>",
        "value": "<?php echo $total_act; ?>",
        "value1": "<?php echo $diferen; ?>",
        "value2": "<?php echo $sumajuste ?>"
      },
    <?php } ?>],
    "valueAxes": [{
        "axisAlpha": 0,
        "guides": [{
            "fillAlpha": 0.1,
            "fillColor": "#888888",
            "lineAlpha": 0,
            "toValue": 16,
            "value": 10
        }],
        "position": "left",
        "tickLength": 0
    }],
    "graphs": [{
        "balloonText": "Gastado en [[category]]<br><b><span style='font-size:14px;'>$[[value]]</span></b>",
        "bullet": "round",
        "dashLength": 3,
        "colorField":"color",
        "valueField": "value"
    },{
        "balloonText": "Valor Final en [[category]]<br><b><span style='font-size:14px;'>$[[value]]</span></b>",
        "bullet": "round",
        "dashLength": 3,
        "valueField": "value1"
    },{
        "balloonText": "Ajuste en [[category]]<br><b><span style='font-size:14px;'>$[[value]]</span></b>",
        "bullet": "round",
        "dashLength": 3,
        "valueField": "value2"
    }],
    "chartCursor": {
        "fullWidth":true,
        "valueLineEabled":true,
        "valueLineBalloonEnabled":true,
        "valueLineAlpha":0.5,
        "cursorAlpha":0
    },
    "categoryField": "date",
    "categoryAxis": {
        "axisAlpha": 0,
        "gridAlpha": 0.1,
        "minorGridAlpha": 0.1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": false
     }
});

</script>

@endsection