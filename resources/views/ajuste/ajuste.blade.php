@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Ajuste')

@section('content')
<style type="text/css">
	.cabecera{
		height: 120px;
		background-color: #1d4870; 
	}
	.avatar_ajuste{
		border-radius: 50%;
	    height: 120px;
	    width: 120px;
	    margin: -65px auto 0;
	    overflow: hidden;
	    border: 5px solid #fff;
	    margin-top: 2.5%;
	}
	.avatar_ajuste img{
		width: 100%;
	}
	.titulo{
		margin-top: 1%;
	}
	.cont_tabla{
		margin: 1%;
	}
	.cont_tabla .col-md-3{
		border: 1px solid #000;
	}
	.head{
		background-color: #ececec;
		font-size: x-large;
	}
	.total{
		font-size: x-large;	
	}
	.concepto{
		font-size: large;
	}
	.form-control2 {
	    width: 100%;
	    padding: 0px !important;
	    font-size: 1.5rem !important;
	    line-height: inherit !important;
	    color: #464a4c;
	    -webkit-background-clip: padding-box;
	    border-radius: 0 !important;
	    border: 0.5px solid rgba(0,0,0,.15) !important;
	    text-align: center !important;
	}
	.c_right{
		text-align: right;
	}
</style>

<?php 
$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre",""];
$meses3=["Diciembre","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
?>
<div class="container-fluid animated slideInDown">
  <div class="row">
  	<div class="col-md-12 panel-view">
	  	<div class="row">
	        <div class="col-md-12">
	          <ul class="nav nav-pills" role="tablist">
	              <li><a href="{{url('/')}}/gastos"><i class="fa fa-area-chart"></i> Gafico Comparativo</a></li>
	              <li><a href="{{url('/')}}/presupuesto"><i class="fa fa-money"></i> Presupuesto Mensual</a></li>
	              <li><a href="{{url('/')}}/presupuestoanualsiguiente"><i class="fa fa-usd"></i> Presupuesto Anual</a></li>
	              <li class="active"><a href="#"><i class="fa fa-balance-scale"></i> Ajuste Mensual</a></li>
	          </ul>
	        </div>
	    </div>
  		<!--<div class="pull-right">
	        <div class="btn-group">
	        <?php if($fun->cargo != 'Gerente Comercial'){ if($fun->name != 'Administrador sistema'){ ?>
	          <a href="{{url('/')}}/presupuestoanterior" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right" aria-hidden="true"></i> Presupuesto de {{$meses2[(int)date("m")-1]}}</a>
	        <?php } } ?>
	          <a href="{{url('/')}}/presupuesto" class="btn btn-sm btn-danger"><i class="fa fa-arrow-right" aria-hidden="true"></i> Presupuesto de {{$meses2[(int)(date("m"))]}}</a>
	        <?php if($fun->cargo != 'Gerente Comercial'){ if($fun->name != 'Administrador sistema'){ ?>
	          <a href="{{url('/')}}/presupuestoanual" class="btn btn-sm btn-warning"><i class="fa fa-calendar" aria-hidden="true"></i> Presupuesto Año {{date("Y")}}</a>
	        <?php } } ?>
	          <a href="{{url('/')}}/presupuestoanualsiguiente" class="btn btn-sm btn-warning"><i class="fa fa-calendar" aria-hidden="true"></i> Presupuesto Año {{date("Y")+1}}</a>
	          <a href="{{url('/')}}/gastos" class="btn btn-sm btn-success"><i class="fa fa-arrow-right" aria-hidden="true"></i> Gastos</a>
	        </div>
	    </div>-->
	    
	      <div class="col-md-12">
	        <div class="row" style="margin-top: 1.5%; margin-bottom: 1.5%;">
	          <div class="col-md-6">
	          	<?php if($permiso_filtro_usuario=="Si"){ ?>
	            <select class="form-control filtro" id="filtro_usuarios" style="height: 100%">
	              <option value="">Filtrar por usuario</option>
	              <?php foreach($usuarios_fil as $usuar){ ?>
	                <option value="{{$usuar->id}}">{{$usuar->nombres.' '.$usuar->apellidos}}</option>
	              <?php  } ?>
	              <option value="sumatoria">Sumatoria General</option>
	            </select>
	            <?php } ?>
	          </div>
	          <div class="col-md-6">
	          	<?php if($permiso_filtro_mes=="Si"){ ?>
	            <select class="form-control filtro" id="filtro_mes" style="height: 100%">
	              <option value="">Filtrar por mes</option>
	              <?php 
	              
	              for($i=1;$i<=12;$i++){
	                $fecha=strtotime ( '-'.$i.' month' , strtotime ( date('Y-m') ) ) ;
	                /*$fecha = date ( 'Y-m-j' , $fecha );*/
	                $numero_mes=((int)(date('m', $fecha)))-1;
	                
	                $mes=$meses2[$numero_mes];
	                $ano=date('Y' , $fecha);
	              ?>
	              <option value="{{date ( 'Y-m' , $fecha )}}">{{$mes}} de {{$ano}}</option>
	            <?php
	              }
	              ?>
	            </select>
	            <?php } ?>
	          </div>
	        </div>
	      </div>
	      
  	</div>
  	
  	<div class="col-md-12 panel-view" id="contenido">
  		<div class="row cabecera">
  			<div class="col-md-12">
  				<div class="avatar_ajuste">
  					<?php if($permiso_usr=="Ver Sumatoria"){ ?> <img src="{{url('/')}}/images/file/clientes/essi_admon.jpg" class="rounded-circle img-responsive"> <?php }else{ ?> <img src="{{url('/')}}/images/file/clientes/{{$usuario->foto}}" class="rounded-circle img-responsive"> <?php } ?>
                </div>
  			</div>  			
  		</div>
  		<div class="row titulo">
  			<div class="col-md-12">
  				<h3><?php if($permiso_usr=="Ver Sumatoria"){ echo "Sumatoria"; } ?> Ajuste del mes <?php echo $meses3[(((int)(date('m')))-1)] ?></h3>
  			</div>
  		</div>
  		<div class="row cont_tabla">
  			<div class="col-md-12">
  				<div class="row">
		  			<div class="col-md-3 head" style="text-align: right;">
		  				Concepto
		  			</div>
		  			<div class="col-md-3 head">
		  				Valor
		  			</div>
		  			<div class="col-md-3 head">
		  				Valor Real
		  			</div>
		  			<div class="col-md-3 head">
		  				Ajuste
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 total" style="text-align: right;">
		  				Total
		  			</div>
		  			<div class="col-md-3 total" id="total_valor">
		  				$ {{number_format($valor['total'])}}
		  			</div>
		  			<div class="col-md-3 total" id="total_valor_real">
		  				$ {{number_format($valor1['valorReal']['total'])}}
		  			</div>
		  			<div class="col-md-3 total" id="total_ajuste" style="color: <?php if($valor1['ajuste']['total'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['total'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Alimentación
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_alimentacion">
		  				$ {{number_format($valor['alimentacion'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_alimentacion" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['alimentacion'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_alimentacion" style="color: <?php if($valor1['ajuste']['alimentacion'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['alimentacion'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Transporte Interno
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_transporte_interno">
		  				$ {{number_format($valor['transporte_interno'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_transporte_interno" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['transporte_interno'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_transporte_interno" style="color: <?php if($valor1['ajuste']['transporte_interno'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['transporte_interno'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Transporte Intermunicipal
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_transporte_intermunicipal">
		  				$ {{number_format($valor['transporte_intermunicipal'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_transporte_intermunicipal" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['transporte_intermunicipal'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_transporte_intermunicipal" style="color: <?php if($valor1['ajuste']['transporte_intermunicipal'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['transporte_intermunicipal'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Tiquete Aereo
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_tiquete_aereo">
		  				$ {{number_format($valor['tiquete_aereo'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_tiquete_aereo" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['tiquete_aereo'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_tiquete_aereo" style="color: <?php if($valor1['ajuste']['tiquete_aereo'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['tiquete_aereo'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Papeleria
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_papeleria">
		  				$ {{number_format($valor['papeleria'])}}
		  			</div>
		  			<div class="col-md-3 concepto" >
		  				<input class="form-control2 form-control dinero valor_real" id="vr_papeleria" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['papeleria'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_papeleria" style="color: <?php if($valor1['ajuste']['papeleria'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['papeleria'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Invitación Cliente
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_invitacion_cliente">
		  				$ {{number_format($valor['invitacion_cliente'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_invitacion_cliente" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['invitacion_cliente'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_invitacion_cliente" style="color: <?php if($valor1['ajuste']['invitacion_cliente'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['invitacion_cliente'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Alquiler Vehiculo
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_alquiler_vehiculo">
		  				$ {{number_format($valor['alquiler_vehiculo'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_alquiler_vehiculo" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['alquiler_vehiculo'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_alquiler_vehiculo" style="color: <?php if($valor1['ajuste']['alquiler_vehiculo'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['alquiler_vehiculo'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Gasolina y Pasaje
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_gasolina_pasaje">
		  				$ {{number_format($valor['gasolina_pasaje'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_gasolina_pasaje" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['gasolina_pasaje'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_gasolina_pasaje" style="color: <?php if($valor1['ajuste']['gasolina_pasaje'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['gasolina_pasaje'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Hotel
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_hotel">
		  				$ {{number_format($valor['hotel'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_hotel" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['hotel'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_hotel" style="color: <?php if($valor1['ajuste']['hotel'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['hotel'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Otros
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_otros">
		  				$ {{number_format($valor['otros'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_otros" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['otros'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_otros" style="color: <?php if($valor1['ajuste']['otros'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['otros'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Salario Propio
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_salariopropio">
		  				$ {{number_format($valor['salariopropio'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_salariopropio" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['salariopropio'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_salariopropio" style="color: <?php if($valor1['ajuste']['salariopropio'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>">
		  				$ {{number_format($valor1['ajuste']['salariopropio'])}}
		  			</div>
		  		</div>
		  		<div class="row">
		  			<div class="col-md-3 concepto c_right">
		  				Salario Tercero
		  			</div>
		  			<div class="col-md-3 concepto VR" id="val_salariotercero">
		  				$ {{number_format($valor['salariotercero'])}}
		  			</div>
		  			<div class="col-md-3 concepto">
		  				<input class="form-control2 form-control dinero valor_real" id="vr_salariotercero" type="text" placeholder="$0" value="{{number_format($valor1['valorReal']['salariotercero'])}}" <?php if($permiso_usr=="Ver Sumatoria"){ ?> disabled style="cursor: no-drop;" <?php } ?> >
		  			</div>
		  			<div class="col-md-3 concepto valor_ajuste" id="aj_salariotercero" style="color: <?php if($valor1['ajuste']['salariotercero'] >=0 ){ echo 'green'; }else{ echo 'red'; } ?>" >
		  				$ {{number_format($valor1['ajuste']['salariotercero'])}}
		  			</div>
		  		</div>
  			</div>
  		</div>
  	</div>
  </div>
</div>
<div class="row" id="contenido_btn_guardar">
	<div class="col-md-12">
		<button class="btn btn-essi pull-right" id="btn_guardar"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
	</div>
</div>

<form method="POST" action="{{ url('ajusteguardar') }}" enctype="multipart/form-data" style="display: none;">
	<?php
	if(isset($valor1['valor']['id'])){ ?>
		<input type="hidden" name="valor[id]" value="{{$valor1['valor']['id']}}">
	<?php
	}
	?>
	<input type="hidden" name="valor[tipo]" value="valor">
	<input type="hidden" name="valor[valor_alimentacion]" id="primera_valor_alimentacion" value="0">
	<input type="hidden" name="valor[valor_transporte_interno]" id="primera_valor_transporte_interno" value="0">
	<input type="hidden" name="valor[valor_transporte_intermunicipal]" id="primera_valor_transporte_intermunicipal" value="0">
	<input type="hidden" name="valor[valor_tiquete_aereo]" id="primera_valor_tiquete_aereo" value="0">
	<input type="hidden" name="valor[valor_papeleria]" id="primera_valor_papeleria" value="0">
	<input type="hidden" name="valor[valor_invitacion_cliente]" id="primera_valor_invitacion_cliente" value="0">
	<input type="hidden" name="valor[valor_alquiler_vehiculo]" id="primera_valor_alquiler_vehiculo" value="0">

	<input type="hidden" name="valor[valor_gasolina_pasaje]" id="primera_valor_gasolina_pasaje" value="0">
	<input type="hidden" name="valor[valor_hotel]" id="primera_valor_hotel" value="0">
	<input type="hidden" name="valor[valor_otros]" id="primera_valor_otros" value="0">
	<input type="hidden" name="valor[valor_salariopropio]" id="primera_valor_salariopropio" value="0">
	<input type="hidden" name="valor[valor_salariotercero]" id="primera_valor_salariotercero" value="0">

	<?php
	if(isset($valor1['valorReal']['id'])){ ?>
		<input type="hidden" name="valorreal[id]" value="{{$valor1['valorReal']['id']}}">
	<?php
	}
	?>
	<input type="hidden" name="valorreal[tipo]" value="valorreal">
	<input type="hidden" name="valorreal[valor_alimentacion]" id="segunda_valor_alimentacion">
	<input type="hidden" name="valorreal[valor_transporte_interno]" id="segunda_valor_transporte_interno">
	<input type="hidden" name="valorreal[valor_transporte_intermunicipal]" id="segunda_valor_transporte_intermunicipal">
	<input type="hidden" name="valorreal[valor_tiquete_aereo]" id="segunda_valor_tiquete_aereo">
	<input type="hidden" name="valorreal[valor_papeleria]" id="segunda_valor_papeleria">
	<input type="hidden" name="valorreal[valor_invitacion_cliente]" id="segunda_valor_invitacion_cliente">
	<input type="hidden" name="valorreal[valor_alquiler_vehiculo]" id="segunda_valor_alquiler_vehiculo">

	<input type="hidden" name="valorreal[valor_gasolina_pasaje]" id="segunda_valor_gasolina_pasaje">
	<input type="hidden" name="valorreal[valor_hotel]" id="segunda_valor_hotel">
	<input type="hidden" name="valorreal[valor_otros]" id="segunda_valor_otros">
	<input type="hidden" name="valorreal[valor_salariopropio]" id="segunda_valor_salariopropio">
	<input type="hidden" name="valorreal[valor_salariotercero]" id="segunda_valor_salariotercero">	

	<?php
	if(isset($valor1['ajuste']['id'])){ ?>
		<input type="hidden" name="ajuste[id]" value="{{$valor1['ajuste']['id']}}">
	<?php
	}
	?>
	<input type="hidden" name="ajuste[tipo]" value="ajuste">
	<input type="hidden" name="ajuste[signo_alimentacion]" id="tercera_signo_alimentacion">
	<input type="hidden" name="ajuste[valor_alimentacion]" id="tercera_valor_alimentacion">
	<input type="hidden" name="ajuste[signo_transporte_interno]" id="tercera_signo_transporte_interno">
	<input type="hidden" name="ajuste[valor_transporte_interno]" id="tercera_valor_transporte_interno">
	<input type="hidden" name="ajuste[signo_transporte_intermunicipal]" id="tercera_signo_transporte_intermunicipal">
	<input type="hidden" name="ajuste[valor_transporte_intermunicipal]" id="tercera_valor_transporte_intermunicipal">
	<input type="hidden" name="ajuste[signo_tiquete_aereo]" id="tercera_signo_tiquete_aereo">
	<input type="hidden" name="ajuste[valor_tiquete_aereo]" id="tercera_valor_tiquete_aereo">
	<input type="hidden" name="ajuste[signo_papeleria]" id="tercera_signo_papeleria">
	<input type="hidden" name="ajuste[valor_papeleria]" id="tercera_valor_papeleria">
	<input type="hidden" name="ajuste[signo_invitacion_cliente]" id="tercera_signo_invitacion_cliente">
	<input type="hidden" name="ajuste[valor_invitacion_cliente]" id="tercera_valor_invitacion_cliente">
	<input type="hidden" name="ajuste[signo_alquiler_vehiculo]" id="tercera_signo_alquiler_vehiculo">
	<input type="hidden" name="ajuste[valor_alquiler_vehiculo]" id="tercera_valor_alquiler_vehiculo">

	<input type="hidden" name="ajuste[signo_gasolina_pasaje]" id="tercera_signo_gasolina_pasaje">
	<input type="hidden" name="ajuste[valor_gasolina_pasaje]" id="tercera_valor_gasolina_pasaje">
	<input type="hidden" name="ajuste[signo_hotel]" id="tercera_signo_hotel">
	<input type="hidden" name="ajuste[valor_hotel]" id="tercera_valor_hotel">
	<input type="hidden" name="ajuste[signo_otros]" id="tercera_signo_otros">
	<input type="hidden" name="ajuste[valor_otros]" id="tercera_valor_otros">
	<input type="hidden" name="ajuste[signo_salariopropio]" id="tercera_signo_salariopropio">
	<input type="hidden" name="ajuste[valor_salariopropio]" id="tercera_valor_salariopropio">
	<input type="hidden" name="ajuste[signo_salariotercero]" id="tercera_signo_salariotercero">
	<input type="hidden" name="ajuste[valor_salariotercero]" id="tercera_valor_salariotercero">
	<button type="submit" id="btn_guardar1" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
</form>
@endsection

@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">

entradas_filtro=0;
$('body').on('change','.filtro',function(){
  if(entradas_filtro==0){
    html_guardado=$('#contenido').html();
  }
  $('#contenido_btn_guardar').css('display','none');
  user='';
  mes='';
  $(".filtro").each(function(){
    if(($(this).attr('id'))=='filtro_usuarios'){
      user=$(this).val();
    }else if(($(this).attr('id'))=='filtro_mes'){
      mes=$(this).val();
    }
  });
  url="{{url('/')}}";
  csrf=`{{csrf_field()}}`;
  if(user==''&&mes==''){
    $('#contenido').html(html_guardado);
    $('#contenido_btn_guardar').css('display','block');
  }else{
    if(user==''){
        user='{{Auth::user()->id}}';
    }
    $.ajax({ 
        url: "{{ url('/') }}/filtrarajuste", 
        cache: false,
        type: 'POST',
        data:{"user":user,
              "mes" : mes,
              "url" : url,
              "csrf" : csrf},
        success: function(e){ 
          $('#contenido').html(e.html);
        }
    });
  }  
  entradas_filtro++;
});

$('body').on('click','#btn_guardar',function(){
	
	$(".VR").each(function(){
		id=$(this).attr('id');
		valor=$(this).html();
		id_nuevo=id.replace("val_","primera_valor_");
		valor_nuevo=parseInt(valor.replace("$ ","").replace(/,/g,""));
		$('#'+id_nuevo).val(valor_nuevo);
		
	});

	$(".valor_real").each(function(){
		id=$(this).attr('id');
		valor=$(this).val();
		id_nuevo=id.replace("vr_","segunda_valor_");
		valor_nuevo=parseInt(valor.replace("$ ","").replace(/,/g,""));
		$('#'+id_nuevo).val(valor_nuevo);
		
	});

	$(".valor_ajuste").each(function(){
		id=$(this).attr('id');
		valor=$(this).html();
		id_nuevo=id.replace("aj_","tercera_valor_");
		id_nuevo2=id.replace("aj_","tercera_signo_");
		valor_nuevo=parseInt(valor.replace("$ ","").replace(/,/g,""));
		if(valor_nuevo>0){
			$('#'+id_nuevo2).val("+");
		}else{
			$('#'+id_nuevo2).val("-");
		}
		$('#'+id_nuevo).val(valor_nuevo);
		
	});
	$('#btn_guardar1').click();
});

//Autor :  Roberto Herrero & Daniel//Web: http://www.indomita.org
//Asunto : Dar formato a un número
function dar_formato(num){
  var cadena = ""; var aux;
  var cont = 1,m,k;
  if(num<0) aux=1; else aux=0;
  num=num.toString();
  for(m=num.length-1; m>=0; m--){
   cadena = num.charAt(m) + cadena;
   if(cont%3 == 0 && m >aux)  cadena = "." + cadena; else cadena = cadena;
   if(cont== 3) cont = 1; else cont++;
  }
  cadena = cadena.replace(/.,/,",");
  return cadena;
}


$('body').on('keyup','.valor_real',function(){
  total=0;
  $(".valor_real").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_valor_real').html('$ '+total);

  valor=parseInt((($('#total_valor').html()).replace("$ ", "")).replace(/,/g, ""));
  valorreal=parseInt((($('#total_valor_real').html()).replace("$ ", "")).replace(/,/g, ""));
  ajuste=valor-valorreal;
  if(ajuste>0){
  	ajuste=(dar_formato(ajuste)).replace(/\./g, ",");
  	$('#total_ajuste').html('$ '+ajuste);
  	$('#total_ajuste').css('color','green');
  }else{
  	ajuste=(dar_formato(ajuste)).replace(/\./g, ",");
  	$('#total_ajuste').html('$ '+ajuste);
  	$('#total_ajuste').css('color','red');
  }

  identificacion=($(this).attr('id')).replace('vr_','');
  valor=parseInt(($('#val_'+identificacion).html()).replace("$ ", "").replace(/,/g, ""));
  valorreal=parseInt(($('#vr_'+identificacion).val()).replace(/,/g, ""));
  ajuste=valor-valorreal;
  if(ajuste>0){
  	ajuste=(dar_formato(ajuste)).replace(/\./g, ",");
  	$('#aj_'+identificacion).html('$ '+ajuste);
  	$('#aj_'+identificacion).css('color','green');
  }else{
  	ajuste=(dar_formato(ajuste)).replace(/\./g, ",");
  	$('#aj_'+identificacion).html('$ '+ajuste);
  	$('#aj_'+identificacion).css('color','red');
  }
});

	$(function () {
	   $(".dinero").maskMoney();
	        i = 0;
	        $('#oportunidad').select2({
	            // Activamos la opcion "Tags" del plugin
	            placeholder: "Seleccione un oportunidad",
	            involucrado: true,
	            tokenSeparators: [','],
	            ajax: {
	                dataType: 'json',
	                url: '{{ url("soportunidad") }}',
	                delay: 250,
	                data: function(params) {
	                    return {
	                        term: params.term
	                    }
	                },
	                processResults: function (data, page) {
	                  return {
	                    results: data
	                  };
	                },
	            }
	        });
	    })
</script>
@endsection
