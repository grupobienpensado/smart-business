@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Presupuesto')

@section('content')

<style type="text/css">
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 1.5rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}
.table td{
    line-height: 0;
    border: 0 !important;
}
.dia{
  line-height: 2 !important;
}

</style>
<?php
if(intval(date("m")+1)==13){
    $datemes=1;
}else{
    $datemes=date("m")+1;
}
?>
@php
$dias=cal_days_in_month ( CAL_GREGORIAN, $datemes, date("Y") );
$semana=["Dom","Lun","Mar","Mie","Jue","Vie","Sab"];
$meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic","Ene"];
$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre","Enero"];

if(date('m')==12){
  $anio=date('Y')+1;
}else{
  $anio=date("Y");
}
@endphp
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills" role="tablist">
              <li><a href="{{url('/')}}/gastos"><i class="fa fa-area-chart"></i> Gafico Comparativo</a></li>
              <li class="active"><a href="#"><i class="fa fa-money"></i> Presupuesto Mensual</a></li>
              <li><a href="{{url('/')}}/presupuestoanualsiguiente"><i class="fa fa-usd"></i> Presupuesto Anual</a></li>
              <li><a href="{{url('/')}}/ajustes"><i class="fa fa-balance-scale"></i> Ajuste Mensual</a></li>
          </ul>
        </div>
      </div>

      <div class="col-md-12">
        <div class="row" style="margin-top: 2.5%;">
          <div class="col-md-6">
            <?php if($permiso_filtro_usuario=="Si"){ ?>
            <select class="form-control filtro" id="filtro_usuarios" style="height: 100%">
              <option value="">Filtrar por usuario</option>
              <?php foreach($usuarios as $usuario){ ?>
                <option value="{{$usuario->id}}">{{$usuario->nombres.' '.$usuario->apellidos}}</option>
              <?php  } ?>
              <option value="sumatoria">Sumatoria General</option>
            </select>
            <?php } ?>
          </div>
          <div class="col-md-6">
            <select class="form-control filtro" id="filtro_mes" style="height: 100%">
              <option value="">Filtrar por mes</option>
              <?php 
              
              for($i=0;$i<=11;$i++){
                $fecha=strtotime ( '-'.$i.' month' , strtotime ( date('Y-m-d') ) ) ;
                /*$fecha = date ( 'Y-m-j' , $fecha );*/
                $numero_mes=((int)(date('m', $fecha)))-1;
                
                $mes=$meses2[$numero_mes];
                $ano=date('Y' , $fecha);
              ?>
              <option value="{{date ( 'Y-m' , $fecha )}}">{{$mes}} de {{$ano}}</option>
            <?php
              }
              ?>
            </select>
          </div>
        </div>
      </div>
      
      <div id="contenido">
      <div class="col-md-12 inline-block">
      		<h3><?php if($permiso!="Ver propio"){ ?> Sumatoria <?php } ?> Presupuesto Mes de {{$meses2[(int)date("m")]}} de {{$anio}}</h3>
          <h3><strong>($ {{$total['total']}})</strong></h3>
			</div>
        <form method="POST" action="{{ url('presupuestosave') }}" enctype="multipart/form-data">
        
        {{ csrf_field() }}
      	 <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th width="100"></th>
                 <th class="text-center">Alimentación</th>
                 <th class="text-center">Transporte Interno</th>
                 <th class="text-center">Transporte Intermunicipal</th>
                 <th class="text-center">Tiquete Aereo</th>
                 <th class="text-center">Papeleria</th>
                 <th class="text-center">Invitación Cliente</th>
                 <th class="text-center">Alquiler Vehiculo</th>
                 <th class="text-center">Gasolina y Pasaje</th>
                 <th class="text-center">Hotel</th>
                 <th class="text-center">Otros</th>
                 <th class="text-center">Salario Propio</th>
                 <th class="text-center">Salario Tercero</th>
               </tr>
             </thead>
             <tbody>
              <tr>
                 <td class="dia">Total</td>
                 <td id="total_alimentacion">$ {{number_format($total['alimentacion'])}}</td>
                 <td id="total_transporte_interno">$ {{number_format($total['transporte_interno'])}}</td>
                 <td id="total_transporte_intermunicipal">$ {{number_format($total['transporte_intermunicipal'])}}</td>
                 <td id="total_tiquete_aereo">$ {{number_format($total['tiquete_aereo'])}}</td>
                 <td id="total_papeleria">$ {{number_format($total['papeleria'])}}</td>
                 <td id="total_invitacion_cliente">$ {{number_format($total['invitacion_cliente'])}}</td>
                 <td id="total_alquiler_vehiculo">$ {{number_format($total['alquiler_vehiculo'])}}</td>
                 <td id="total_gasolina_pasaje">$ {{number_format($total['gasolina_pasaje'])}}</td>
                 <td id="total_hotel">$ {{number_format($total['hotel'])}}</td>
                 <td id="total_otros">$ {{number_format($total['otros'])}}</td>
                 <td id="total_salario_propio">$ {{number_format($total['salario_propio'])}}</td>
                 <td id="total_salario_tercero">$ {{number_format($total['salario_tercero'])}}</td>
              </tr>
             @for($i=1;$i<=$dias;$i++)
             @if($i<10)
             @php $i="0".$i; @endphp
             @endif
             @php  
             $nuevo=strtotime("+1 Month",strtotime(date("Y-m-")."01"));
             $nuevo = date ( 'Y-m' , $nuevo );
             @endphp
             <input type="hidden" name="datos[{{$i-1}}][mes]" value="{{date("m")+1}}">
             <input type="hidden" name="datos[{{$i-1}}][anio]" value="{{$anio}}">
             <input type="hidden" name="datos[{{$i-1}}][dia]" value="{{$i}}">
             @if(!empty($lista[$i-1]))
             <input type="hidden" name="datos[{{$i-1}}][id]" value="{{$lista[$i-1]->id}}">
             @endif
               <tr>
                 <td class="dia">{{$semana[date("w",strtotime($nuevo."-".$i))].", ".$i." ".$meses[(int)date("m")]}}</td>
                 <td><input class="form-control2 form-control dinero alimentacion" type="text" name="datos[{{$i-1}}][alimentacion]" placeholder="$0"  @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->alimentacion)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero transporte_interno" type="text" name="datos[{{$i-1}}][transporte_interno]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->transporte_interno)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero transporte_intermunicipal" type="text" name="datos[{{$i-1}}][transporte_intermunicipal]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->transporte_intermunicipal)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero tiquete_aereo" type="text" name="datos[{{$i-1}}][tiquete_aereo]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->tiquete_aereo)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero papeleria" type="text" name="datos[{{$i-1}}][papeleria]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->papeleria)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero invitacion_cliente" type="text" name="datos[{{$i-1}}][invitacion_cliente]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->invitacion_cliente)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero alquiler_vehiculo" type="text" name="datos[{{$i-1}}][alquiler_vehiculo]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->alquiler_vehiculo)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero gasolina_pasaje" type="text" name="datos[{{$i-1}}][gasolina_pasaje]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->gasolina_pasaje)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero hotel" type="text" name="datos[{{$i-1}}][hotel]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->hotel)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero otros" type="text" name="datos[{{$i-1}}][otros]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->otros)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero salario_propio" type="text" name="datos[{{$i-1}}][salario_propio]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->salario_propio)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero salario_tercero" type="text" name="datos[{{$i-1}}][salario_tercero]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->salario_tercero)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
               </tr>
            @endfor
             </tbody>
           </table>
           <?php if($permiso=="Ver propio"){ ?>
           <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
           <?php } ?>
         </div>
      </form>
      </div>
    </div>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" charset="utf-8">
entradas_filtro=0;
$('body').on('change','.filtro',function(){
  if(entradas_filtro==0){
    html_guardado=$('#contenido').html();
  }
  user="";
  mes="";
  $(".filtro").each(function(){
    if(($(this).attr('id'))=='filtro_usuarios'){
      user=$(this).val();
    }else if(($(this).attr('id'))=='filtro_mes'){
      mes=$(this).val();
    }
  });
  url="{{url('/')}}";
  csrf=`{{csrf_field()}}`;
  if(user==''&&mes==''){
    $('#contenido').html(html_guardado);
    $(".dinero").maskMoney();
  }else{
    $.ajax({ 
        url: "{{ url('/') }}/filtrarpresupuesto", 
        cache: false,
        type: 'POST',
        data:{"user":user,
              "mes" : mes,
              "url" : url,
              "csrf" : csrf},
        success: function(e){ 
          $('#contenido').html(e.html);
        }
    });
  }  
  entradas_filtro++;
});

//Autor :  Roberto Herrero & Daniel//Web: http://www.indomita.org
//Asunto : Dar formato a un número
function dar_formato(num){
  var cadena = ""; var aux;
  var cont = 1,m,k;
  if(num<0) aux=1; else aux=0;
  num=num.toString();
  for(m=num.length-1; m>=0; m--){
   cadena = num.charAt(m) + cadena;
   if(cont%3 == 0 && m >aux)  cadena = "." + cadena; else cadena = cadena;
   if(cont== 3) cont = 1; else cont++;
  }
  cadena = cadena.replace(/.,/,",");
  return cadena;
}


$('body').on('keyup','.alimentacion',function(){
  total=0;
  $(".alimentacion").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_alimentacion').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.transporte_interno',function(){
  total=0;
  $(".transporte_interno").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_transporte_interno').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.transporte_intermunicipal',function(){
  total=0;
  $(".transporte_intermunicipal").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_transporte_intermunicipal').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.tiquete_aereo',function(){
  total=0;
  $(".tiquete_aereo").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_tiquete_aereo').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.papeleria',function(){
  total=0;
  $(".papeleria").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_papeleria').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.invitacion_cliente',function(){
  total=0;
  $(".invitacion_cliente").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_invitacion_cliente').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.alquiler_vehiculo',function(){
  total=0;
  $(".alquiler_vehiculo").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_alquiler_vehiculo').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.gasolina_pasaje',function(){
  total=0;
  $(".gasolina_pasaje").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_gasolina_pasaje').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.hotel',function(){
  total=0;
  $(".hotel").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_hotel').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.otros',function(){
  total=0;
  $(".otros").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_otros').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.salario_propio',function(){
  total=0;
  $(".salario_propio").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_salario_propio').html('$ '+total);
  totaltotal();
});

$('body').on('keyup','.salario_tercero',function(){
  total=0;
  $(".salario_tercero").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_salario_tercero').html('$ '+total);
  totaltotal();
});

function realizado(id){
		mo=$("#tr-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr-"+id).show("clip");
		}else{
			$("#tr-"+id).hide("clip");
		}
}

function cancelar(id){
		mo=$("#tr2-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr2-"+id).show("clip");
		}else{
			$("#tr2-"+id).hide("clip");
		}
		
}

function ver(id){
		mo=$("#tr3-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr3-"+id).show("clip");
		}else{
			$("#tr3-"+id).hide("clip");
		}
		
}
$("body").on("click",".cuadro",function(e){
	$("#fecha").html($(this).attr("name"));
	$("#fecha_actividad").val($(this).attr("name"));
	$("#hora_inicio").val($(this).attr("id"));
	fecha=$(this).attr("name");
	hora=$(this).attr("id");
	$("#lista-actividades").html('');
	setTimeout(function(e){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/consultarplanestrabajo/"+fecha, 
        cache: false,
        type: 'GET',
        data:{"hora":hora},
        success: function(e){ 
        	$("#lista-actividades").html(e.tabla);
        }
    });},2000)

})

$(function () {
   $(".dinero").maskMoney();
        i = 0;
        $('#oportunidad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    });

    function totaltotal(){
        t_alimentacion = parseInt((($("#total_alimentacion").html()).replace(/,/g, "")).replace("$ ", ""));
        t_transporte_interno = parseInt((($("#total_transporte_interno").html()).replace(/,/g, "")).replace("$ ", ""));
        t_transporte_intermunicipal = parseInt((($("#total_transporte_intermunicipal").html()).replace(/,/g, "")).replace("$ ", ""));
        t_tiquete_aereo = parseInt((($("#total_tiquete_aereo").html()).replace(/,/g, "")).replace("$ ", ""));
        t_papeleria = parseInt((($("#total_papeleria").html()).replace(/,/g, "")).replace("$ ", ""));
        t_invitacion_cliente = parseInt((($("#total_invitacion_cliente").html()).replace(/,/g, "")).replace("$ ", ""));
        t_alquiler_vehiculo = parseInt((($("#total_alquiler_vehiculo").html()).replace(/,/g, "")).replace("$ ", ""));
        t_gasolina_pasaje = parseInt((($("#total_gasolina_pasaje").html()).replace(/,/g, "")).replace("$ ", ""));
        t_hotel = parseInt((($("#total_hotel").html()).replace(/,/g, "")).replace("$ ", ""));
        t_otros = parseInt((($("#total_otros").html()).replace(/,/g, "")).replace("$ ", ""));
        t_salario_propio = parseInt((($("#total_salario_propio").html()).replace(/,/g, "")).replace("$ ", ""));
        t_salario_tercero = parseInt((($("#total_salario_tercero").html()).replace(/,/g, "")).replace("$ ", ""));
        t_t = t_alimentacion + t_transporte_interno + t_transporte_intermunicipal + t_tiquete_aereo + t_papeleria + t_invitacion_cliente + t_alquiler_vehiculo + t_gasolina_pasaje + t_hotel + t_otros + t_salario_propio + t_salario_tercero;
        t_t = (dar_formato(t_t)).replace(/\./g, ",");
        $('#total_total').html('$ '+t_t);
    }
</script>

@endsection
