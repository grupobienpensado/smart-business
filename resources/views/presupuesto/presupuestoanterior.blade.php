@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Presupuesto')

@section('content')

<style type="text/css">
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 0.9rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}
.table td{
    line-height: 0 !important;
    border: 0 !important;
}

.alert {
    -webkit-animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    animation: slide-from-top 1000ms cubic-bezier(0.2, 0.7, 0.5, 1);
    margin-bottom: 10px;
}

.alert-dark {
    background-color: rgba(228, 104, 104, 0.9);
    border-color: rgba(0, 0, 0, 0.8);
    color: #fff;
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}
</style>
@php
$dias=cal_days_in_month ( CAL_GREGORIAN, date("m")-1, date("Y") );
$semana=["Dom","Lun","Mar","Mie","Jue","Vie","Sab"];
$meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic"];
$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

if(date('m')==1){
  $anio=date('Y')-1;
}else{
  $anio=date("Y");
}
@endphp
<div class="container animated slideInDown">
  <div class="row">
    <main class="col-sm-12 col-md-12 pt-5">
      <div class="pull-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
          <a href="{{url('/')}}/gastos" class="btn btn-sm btn-success"><i class="fa fa-arrow-right" aria-hidden="true"></i> Gastos</a>
        </div>
      </div>
      	<div class="col-md-12 inline-block">
      		<h3>Presupuesto Mes de {{$meses2[(int)date("m")-2]}} de {{$anio}}</h3>
			</div>
      	<div class="row">
        <form method="POST" action="{{ url('presupuestosave') }}" enctype="multipart/form-data">
        
        {{ csrf_field() }}
      	 <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th width="100"></th>
                 <th class="text-center">Alimentación</th>
                 <th class="text-center">Transporte Interno</th>
                 <th class="text-center">Transporte Intermunicipal</th>
                 <th class="text-center">Tiquete Aereo</th>
                 <th class="text-center">Papeleria</th>
                 <th class="text-center">Invitación Cliente</th>
                 <th class="text-center">Alquiler Vehiculo</th>
                 <th class="text-center">Gasolina y Pasaje</th>
                 <th class="text-center">Hotel</th>
                 <th class="text-center">Otros</th>
               </tr>
             </thead>
             <tbody>
             @for($i=1;$i<=$dias;$i++)
             @if($i<10)
             @php $i="0".$i; @endphp
             @endif
             @php  
             $nuevo=strtotime("-1 Month",strtotime(date("Y-m-")."01"));
             $nuevo = date ( 'Y-m' , $nuevo );
             @endphp
             <input type="hidden" name="datos[{{$i-1}}][mes]" value="{{date("m")-1}}">
             <input type="hidden" name="datos[{{$i-1}}][anio]" value="{{$anio}}">
             <input type="hidden" name="datos[{{$i-1}}][dia]" value="{{$i}}">
             @if(!empty($lista[$i-1]))
             <input type="hidden" name="datos[{{$i-1}}][id]" value="{{$lista[$i-1]->id}}">
             @else
             <input type="hidden" name="datos[{{$i-1}}][id]">
             @endif
               <tr>
                 <td>{{$semana[date("w",strtotime($nuevo."-".$i))].", ".$i." ".$meses[(int)date("m")-2]}}</td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][alimentacion]" placeholder="$0"  @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->alimentacion}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][transporte_interno]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->transporte_interno}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][transporte_intermunicipal]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->transporte_intermunicipal}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][tiquete_aereo]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->tiquete_aereo}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][papeleria]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->papeleria}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][invitacion_cliente]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->invitacion_cliente}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][alquiler_vehiculo]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->alquiler_vehiculo}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][gasolina_pasaje]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->gasolina_pasaje}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][hotel]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->hotel}}" @endif></td>
                 <td><input class="form-control2 form-control dinero" type="text" name="datos[{{$i-1}}][otros]" placeholder="$0" @if(!empty($lista[(int)$i])) value="{{$lista[$i-1]->otros}}" @endif></td>
               </tr>
               @endfor
             </tbody>
           </table>
           <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
         </div>
      </form>
      <div class="col-md-12">
        <div class="alert alert-dark bpx" role="alert"  >
            <strong>Atención!</strong> si realiza algun cambio en este presupuesto su indice de cumplimiento se verá afectado negativamente
        </div>
      </div>
      </div>

    </main>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" charset="utf-8">

function realizado(id){
		mo=$("#tr-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr-"+id).show("clip");
		}else{
			$("#tr-"+id).hide("clip");
		}
}

function cancelar(id){
		mo=$("#tr2-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr2-"+id).show("clip");
		}else{
			$("#tr2-"+id).hide("clip");
		}
		
}

function ver(id){
		mo=$("#tr3-"+id).is(":visible");
		$(".tr").hide("clip");
		if(!mo){
			$("#tr3-"+id).show("clip");
		}else{
			$("#tr3-"+id).hide("clip");
		}
		
}
$("body").on("click",".cuadro",function(e){
	$("#fecha").html($(this).attr("name"));
	$("#fecha_actividad").val($(this).attr("name"));
	$("#hora_inicio").val($(this).attr("id"));
	fecha=$(this).attr("name");
	hora=$(this).attr("id");
	$("#lista-actividades").html('');
	setTimeout(function(e){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/consultarplanestrabajo/"+fecha, 
        cache: false,
        type: 'GET',
        data:{"hora":hora},
        success: function(e){ 
        	$("#lista-actividades").html(e.tabla);
        }
    });},2000)

})
$(function () {
   $(".dinero").maskMoney();
        i = 0;
        $('#oportunidad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    })
</script>

@endsection