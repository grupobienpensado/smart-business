@extends('template.app')
@php date_default_timezone_set("America/Bogota"); @endphp
@section('title', 'Presupuesto')

@section('content')

<style type="text/css">
.form-control2 {
    width: 100%;
    padding: 0px !important;
    font-size: 1.5rem !important;
    line-height: inherit !important;
    color: #464a4c;
    -webkit-background-clip: padding-box;
    border-radius: 0 !important;
    border: 0.5px solid rgba(0,0,0,.15) !important;
    text-align: right !important;
}
.table td{
    line-height: 0;
    border: 0 !important;
}
</style>
@php
$meses=["Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic",""];
$meses2=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre",""];
@endphp
<div class="container-fluid animated slideInDown">
  <div class="row">
    <div class="col-md-12 panel-view">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills" role="tablist">
              <li><a href="{{url('/')}}/gastos"><i class="fa fa-area-chart"></i> Gafico Comparativo</a></li>
              <li><a href="{{url('/')}}/presupuesto"><i class="fa fa-money"></i> Presupuesto Mensual</a></li>
              <li class="active"><a href="#"><i class="fa fa-usd"></i> Presupuesto Anual</a></li>
              <li><a href="{{url('/')}}/ajustes"><i class="fa fa-balance-scale"></i> Ajuste Mensual</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-12">
        <div class="row" style="margin-top: 2.5%;">
          <div class="col-md-6">
            <?php if($permiso_filtros=="Si"){ ?>
            <select class="form-control filtro" id="filtro_usuarios" style="height: 100%">
              <option value="">Filtrar por usuario</option>
              <?php foreach($usuarios as $usuario){ ?>
                <option value="{{$usuario->id}}">{{$usuario->nombres.' '.$usuario->apellidos}}</option>
              <?php  } ?>
              <option value="sumatoria">Sumatoria General</option>
            </select>
            <?php } ?>
          </div>
          <div class="col-md-6">
            <?php if($permiso_filtros=="Si"){ ?>
            <select class="form-control filtro" id="filtro_ano" style="height: 100%">
              <option value="">Filtrar por año</option>
              <?php 
              for($i=0;$i<=11;$i++){
                $fecha=strtotime ( '-'.$i.' year' , strtotime ( date('Y-m-d') ) ) ;
                $ano=date('Y' , $fecha);
              ?>
              <option value="{{date ( 'Y' , $fecha )}}">Año {{$ano}}</option>
            <?php
              }
              ?>
            </select>
            <?php } ?>
          </div>
        </div>
      </div>
      
      <div id="contenido">
        <div class="col-md-12 inline-block">
          <h3><?php if($permiso!="Ver propio"){ ?> Sumatoria <?php } ?> Presupuesto de {{$data['ano']}}</h3>
          <h3><strong class="total-total">($ {{$total['total']}})</strong></h3>
      </div>
        <form method="POST" action="{{ url('presupuestoanualsave') }}" enctype="multipart/form-data">
        
        {{ csrf_field() }}
         <div class="col-md-12">
           <table cellspacing="0" width="100%" class="table table-striped table table-striped table-bordered display">
             <thead>
               <tr>
                 <th width="100"></th>
                 <th class="text-center">Alimentación</th>
                 <th class="text-center">Transporte Interno</th>
                 <th class="text-center">Transporte Intermunicipal</th>
                 <th class="text-center">Tiquete Aereo</th>
                 <th class="text-center">Papeleria</th>
                 <th class="text-center">Invitación Cliente</th>
                 <th class="text-center">Alquiler Vehiculo</th>
                 <th class="text-center">Gasolina y Pasaje</th>
                 <th class="text-center">Hotel</th>
                 <th class="text-center">Otros</th>
               </tr>
             </thead>
             <tbody>
             <tr>
               <td class="dia">Total</td>
               <td class="total-area" id="total_alimentacion">$ {{number_format($total['alimentacion'])}}</td>
               <td class="total-area" id="total_transporte_interno">$ {{number_format($total['transporte_interno'])}}</td>
               <td class="total-area" id="total_transporte_intermunicipal">$ {{number_format($total['transporte_intermunicipal'])}}</td>
               <td class="total-area" id="total_tiquete_aereo">$ {{number_format($total['tiquete_aereo'])}}</td>
               <td class="total-area" id="total_papeleria">$ {{number_format($total['papeleria'])}}</td>
               <td class="total-area" id="total_invitacion_cliente">$ {{number_format($total['invitacion_cliente'])}}</td>
               <td class="total-area" id="total_alquiler_vehiculo">$ {{number_format($total['alquiler_vehiculo'])}}</td>
               <td class="total-area" id="total_gasolina_pasaje">$ {{number_format($total['gasolina_pasaje'])}}</td>
               <td class="total-area" id="total_hotel">$ {{number_format($total['hotel'])}}</td>
               <td class="total-area" id="total_otros">$ {{number_format($total['otros'])}}</td>
            </tr>
             @for($i=1;$i<=12;$i++)
             <input type="hidden" name="datos[{{$i-1}}][mes]" value="{{$i}}">
             <input type="hidden" name="datos[{{$i-1}}][anio]" value="{{$data['ano']}}">
             @if(!empty($lista[$i-1]))
             <input type="hidden" name="datos[{{$i-1}}][id]" value="{{$lista[$i-1]->id}}">
             @else
             <input type="hidden" name="datos[{{$i-1}}][id]">
             @endif
               <tr>
                 <td>{{$meses[$i-1]." de ".$data['ano']}}</td>
                 <td><input class="form-control2 form-control dinero alimentacion" type="text" name="datos[{{$i-1}}][alimentacion]" placeholder="$0"  @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->alimentacion)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero transporte_interno" type="text" name="datos[{{$i-1}}][transporte_interno]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->transporte_interno)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero transporte_intermunicipal" type="text" name="datos[{{$i-1}}][transporte_intermunicipal]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->transporte_intermunicipal)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero tiquete_aereo" type="text" name="datos[{{$i-1}}][tiquete_aereo]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->tiquete_aereo)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero papeleria" type="text" name="datos[{{$i-1}}][papeleria]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->papeleria)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero invitacion_cliente" type="text" name="datos[{{$i-1}}][invitacion_cliente]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->invitacion_cliente)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero alquiler_vehiculo" type="text" name="datos[{{$i-1}}][alquiler_vehiculo]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->alquiler_vehiculo)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero gasolina_pasaje" type="text" name="datos[{{$i-1}}][gasolina_pasaje]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->gasolina_pasaje)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero hotel" type="text" name="datos[{{$i-1}}][hotel]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->hotel)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
                 <td><input class="form-control2 form-control dinero otros" type="text" name="datos[{{$i-1}}][otros]" placeholder="$0" @if(!empty($lista[((int)$i)-1])) value="{{number_format(((int)($lista[$i-1]->otros)), 0, '.', ',')}}" @endif <?php if($permiso!="Ver propio"){ ?> disabled <?php } ?> ></td>
               </tr>
               @endfor
             </tbody>
           </table>
           <?php if($permiso=="Ver propio"){ ?>
            <button type="submit" class="btn btn-essi pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
           <?php } ?>
         </div>
      </form>
      </div>
      </div>
  </div>
</div>

@endsection
 
@section('scripts')
<script src="../js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" charset="utf-8">
function calcular_total_ano(){
    total_total = 0;
    $(".total-area").each(function(){
        total = $(this).html();
        total = total.replace("$ ", "");
        total = parseInt(total.replace(/,/gi, ""));
        total_total += total;
    });
    total_total=dar_formato(total_total);
    total_total = total_total.replace(/\./g, ",");
    $('.total-total').html('$ '+total_total);
}
entradas_filtro=0;
$('body').on('change','.filtro',function(){
  if(entradas_filtro==0){
    html_guardado=$('#contenido').html();
  }
  $(".filtro").each(function(){
    if(($(this).attr('id'))=='filtro_usuarios'){
      user=$(this).val();
    }else if(($(this).attr('id'))=='filtro_ano'){
      ano=$(this).val();
    }
  });
  url="{{url('/')}}";
  csrf=`{{csrf_field()}}`;
  if(user==''&&ano==''){
    $('#contenido').html(html_guardado);
  }else{
    $.ajax({ 
        url: "{{ url('/') }}/filtrarpresupuestoanual", 
        cache: false,
        type: 'POST',
        data:{"user":user,
              "ano" : ano,
              "url" : url,
              "csrf" : csrf},
        success: function(e){ 
          $('#contenido').html(e.html);
        }
    });
  }  
  entradas_filtro++;
});
//Autor :  Roberto Herrero & Daniel//Web: http://www.indomita.org
//Asunto : Dar formato a un número
function dar_formato(num){
  var cadena = ""; var aux;
  var cont = 1,m,k;
  if(num<0) aux=1; else aux=0;
  num=num.toString();
  for(m=num.length-1; m>=0; m--){
   cadena = num.charAt(m) + cadena;
   if(cont%3 == 0 && m >aux)  cadena = "." + cadena; else cadena = cadena;
   if(cont== 3) cont = 1; else cont++;
  }
  cadena = cadena.replace(/.,/,",");
  return cadena;
}


$('body').on('keyup','.alimentacion',function(){
  total=0;
  $(".alimentacion").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_alimentacion').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.transporte_interno',function(){
  total=0;
  $(".transporte_interno").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_transporte_interno').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.transporte_intermunicipal',function(){
  total=0;
  $(".transporte_intermunicipal").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_transporte_intermunicipal').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.tiquete_aereo',function(){
  total=0;
  $(".tiquete_aereo").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_tiquete_aereo').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.papeleria',function(){
  total=0;
  $(".papeleria").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_papeleria').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.invitacion_cliente',function(){
  total=0;
  $(".invitacion_cliente").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_invitacion_cliente').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.alquiler_vehiculo',function(){
  total=0;
  $(".alquiler_vehiculo").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_alquiler_vehiculo').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.gasolina_pasaje',function(){
  total=0;
  $(".gasolina_pasaje").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_gasolina_pasaje').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.hotel',function(){
  total=0;
  $(".hotel").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_hotel').html('$ '+total);
    calcular_total_ano();
});

$('body').on('keyup','.otros',function(){
  total=0;
  $(".otros").each(function(){
    if(($(this).val())!=''){
      total+=parseInt(($(this).val()).replace(/,/g, ""));
    }
  });
  total=(dar_formato(total)).replace(/\./g, ",");
  $('#total_otros').html('$ '+total);
    calcular_total_ano();
});

function realizado(id){
    mo=$("#tr-"+id).is(":visible");
    $(".tr").hide("clip");
    if(!mo){
      $("#tr-"+id).show("clip");
    }else{
      $("#tr-"+id).hide("clip");
    }
}

function cancelar(id){
    mo=$("#tr2-"+id).is(":visible");
    $(".tr").hide("clip");
    if(!mo){
      $("#tr2-"+id).show("clip");
    }else{
      $("#tr2-"+id).hide("clip");
    }
    
}

function ver(id){
    mo=$("#tr3-"+id).is(":visible");
    $(".tr").hide("clip");
    if(!mo){
      $("#tr3-"+id).show("clip");
    }else{
      $("#tr3-"+id).hide("clip");
    }
    
}
$("body").on("click",".cuadro",function(e){
  $("#fecha").html($(this).attr("name"));
  $("#fecha_actividad").val($(this).attr("name"));
  $("#hora_inicio").val($(this).attr("id"));
  fecha=$(this).attr("name");
  hora=$(this).attr("id");
  $("#lista-actividades").html('');
  setTimeout(function(e){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var jqxhr = $.ajax({ 
        url: "{{ url('/') }}/consultarplanestrabajo/"+fecha, 
        cache: false,
        type: 'GET',
        data:{"hora":hora},
        success: function(e){ 
          $("#lista-actividades").html(e.tabla);
        }
    });},2000)

})
$(function () {
   $(".dinero").maskMoney();
        i = 0;
        $('#oportunidad').select2({
            // Activamos la opcion "Tags" del plugin
            placeholder: "Seleccione un oportunidad",
            involucrado: true,
            tokenSeparators: [','],
            ajax: {
                dataType: 'json',
                url: '{{ url("soportunidad") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
            }
        });
    })
</script>

@endsection
