url_basico=$('#URL').val();
if ( $("#numerolinks").length > 0 ) {
  // hacer algo aquí si el elemento existe
    link=$('#numerolinks').val();
}else{
    link=0;
}
$(function () {
    $('.datatime').bootstrapMaterialDatePicker({
          time: true,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD HH:mm',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
});

//imagen
$('.dropzone').html5imageupload();


$('body').on('click','.add-more',function(){
    link++;
   html=`<div class="input-group input-group-appendable link_`+link+`">
            <span class="input-group-addon" id="basic-addon3">https://ejemplo.com/ferias/</span>
            <input autocomplete="off" class="input form-control direcciones_url" name="link[`+link+`][contenido]" type="text" required="required">
            <span class="input-group-btn">
            <button class="btn btn-danger menos-link" id="link_`+link+`" type="button">-</button>
            </span>
         </div>`;
    $('.input-appendable-wrapper').prepend(html);
});

$('body').on('click','.menos-link',function(){
    eliminar=$(this).attr('id');
    $('.'+eliminar).remove();
});

$('body').on('click','.guardar',function(){
    event.preventDefault();
  if(true === $("#formulario_basico").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a guardar la feria",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                var jqxhr = $.post( url_basico+"/guardarferia", $("#formulario_basico").serialize())
                .done(function() {
                    console.log("Guardado");
                })
                .fail(function(e) {
                    console.error(e.msj)
                })
                .always(function(e) {
                  console.log(e.msj)
                    swal(
                      'Guardado',
                       e.msj,
                      'success'
                    ).then(function () {
                        location.href = url_basico+"/ferias/menu";
                    });
                });
            });
    }else{
        swal('Faltan campos por completar');
    }
});

$('body').on('click','.guardar_editar',function(){
    event.preventDefault();
  if(true === $("#formulario_basico_editar").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a editar la feria",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                var jqxhr = $.post( url_basico+"/editarferia", $("#formulario_basico_editar").serialize())
                .done(function() {
                    console.log("Editado");
                })
                .fail(function(e) {
                    console.error(e.msj)
                })
                .always(function(e) {
                  console.log(e.msj);
                    swal(
                      'Guardado',
                       'Guardado correctamente',
                      'success'
                    ).then(function () {
                        location.href = url_basico+"/ferias/menu";
                    });
                });
            });
    }else{
        swal('Faltan campos por completar');
    }
});

$('body').on('change','#pais',function(){
   valor=$(this) .val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio pais!',
          'warning'
        );
        $('#pais').val('');
        $('#departamento').val('');
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultarpais/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#pais_mostrar').val(e.pais.name);
                $('.fila-pais').css('display','block');
                $('#pais').css('display','none');
                $('#lista_departamentos').html(e.html);
                $('#departamento').attr('placeholder','Seleccione un departamento (estado)');
                $('#ciudad').attr('placeholder','Seleccione un departamento (estado)');
                $('#departamento').removeClass('no-cursor');
                $('#departamento').removeAttr("readonly");
            }
        });
    }
});

$('body').on('click','.cambiar-pais',function(){
    $('.fila-pais').css('display','none');
    $('#pais').val('');
    $('#pais').css('display','block');
    $('.fila-departamento').css('display','none');
    $('#departamento').css('display','block');
    $('#lista_departamentos').html('');
    $('#departamento').val('');
    $('#departamento').addClass('no-cursor');
    $('#departamento').attr("readonly","readonly");
    $('.fila-ciudad').css('display','none');
    $('#ciudad').css('display','block');
    $('#lista_ciudades').html('');
    $('#ciudad').val('');
    $('#ciudad').addClass('no-cursor');
    $('#ciudad').attr("readonly","readonly");

    $('#departamento').attr('placeholder','Seleccione un pais');
    $('#ciudad').attr('placeholder','Seleccione un pais');
});

/*Departamento*/

$('body').on('change','#departamento',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Departamento!',
          'warning'
        );
        $('#departamento').val('');
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultardepartamento/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#departamento_mostrar').val(e.departamento.name);
                $('.fila-departamento').css('display','block');
                $('#departamento').css('display','none');
                $('#lista_ciudades').html(e.html);
                $('#ciudad').attr('placeholder','Seleccione una ciudad (municipio)');
                $('#ciudad').removeClass('no-cursor');
                $('#ciudad').removeAttr("readonly");
            }
        });
    }
});

$('body').on('click','.cambiar-departamento',function(){
    $('.fila-departamento').css('display','none');
    $('#departamento').val('');
    $('#departamento').css('display','block');
    $('.fila-ciudad').css('display','none');
    $('#ciudad').css('display','block');
    $('#lista_ciudades').html('');
    $('#ciudad').val('');
    $('#ciudad').addClass('no-cursor');
    $('#ciudad').attr("readonly","readonly");

    $('#departamento').attr('placeholder','Seleccione un departamento (estado)');
    $('#ciudad').attr('placeholder','Seleccione un departamento (estado)');
});

/*ciudad*/

$('body').on('change','#ciudad',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Ciudad!',
          'warning'
        );
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultarciudad/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#ciudad_mostrar').val(e.ciudad.name);
                $('.fila-ciudad').css('display','block');
                $('#ciudad').css('display','none');
            }
        });
    }
});

$('body').on('click','.cambiar-ciudad',function(){
    $('.fila-ciudad').css('display','none');
    $('#ciudad').val('');
    $('#ciudad').css('display','block');

    $('#ciudad').attr('placeholder','Seleccione una ciudad (municipio)');
});

$('body').on('change','#fecha_inicio',function(){
    fecha_fin=$('#fecha_fin').val();
    fecha_actual=$('#fecha_actual').val();
    if(fecha_fin == ""){
       if($(this).val() < fecha_actual){
            swal(
              'Oops...',
              'La fecha de inicio no puede ser menor a la fecha actual',
              'warning'
            );
           $(this).val('');
        }
    }else{
        if($(this).val() < fecha_actual){
            swal(
              'Oops...',
              'La fecha de inicio no puede ser menor a la fecha actual',
              'warning'
            );
           $(this).val('');
        }else if($(this).val() >= fecha_fin){
            swal(
              'Oops...',
              'La fecha de inicio no puede ser mayor a la fecha fin',
              'warning'
            );
            $(this).val('');
        }
    }
});

$('body').on('change','#fecha_fin',function(){
    fecha_inicio=$('#fecha_inicio').val();
    if(fecha_inicio == ""){
       fecha_actual=$('#fecha_actual').val();
       if($(this).val() < fecha_actual){
            swal(
              'Oops...',
              'La fecha de fin no puede ser menor a la fecha actual',
              'warning'
            );
           $(this).val('');
        }
    }else{
        if($(this).val() <= fecha_inicio){
            swal(
              'Oops...',
              'La fecha de fin no puede ser menor a la fecha inicio',
              'warning'
            );
            $(this).val('');
        }
    }
});

$('body').on('change','.direcciones_url',function(){
   textoAreaDividido = ($(this).val()).split(" ");
   numeroPalabras = textoAreaDividido.length;
    if(numeroPalabras>1){
       swal(
          'Oops...',
          'La dirección del link no debe tener espacios',
          'warning'
        );
        $(this).val('');
    }
});
