//imagen
$('.dropzone').html5imageupload();
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

url_basico=$('#URL').val();

$('body').on('change','#pais',function(){
   valor=$(this) .val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio pais!',
          'warning'
        );
        $('#pais').val('');
        $('#departamento').val('');
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultarpais/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#pais_mostrar').val(e.pais.name);
                $('.fila-pais').css('display','block');
                $('#pais').css('display','none');
                $('#lista_departamentos').html(e.html);
                $('#departamento').attr('placeholder','Seleccione un departamento (estado)');
                $('#ciudad').attr('placeholder','Seleccione un departamento (estado)');
                $('#departamento').removeClass('no-cursor');
                $('#departamento').removeAttr("readonly");
            }
        });
    }
});

$('body').on('click','.cambiar-pais',function(){
    $('.fila-pais').css('display','none');
    $('#pais').val('');
    $('#pais').css('display','block');
    $('.fila-departamento').css('display','none');
    $('#departamento').css('display','block');
    $('#lista_departamentos').html('');
    $('#departamento').val('');
    $('#departamento').addClass('no-cursor');
    $('#departamento').attr("readonly","readonly");
    $('.fila-ciudad').css('display','none');
    $('#ciudad').css('display','block');
    $('#lista_ciudades').html('');
    $('#ciudad').val('');
    $('#ciudad').addClass('no-cursor');
    $('#ciudad').attr("readonly","readonly");

    $('#departamento').attr('placeholder','Seleccione un pais');
    $('#ciudad').attr('placeholder','Seleccione un pais');
});

/*Departamento*/

$('body').on('change','#departamento',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Departamento!',
          'warning'
        );
        $('#departamento').val('');
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultardepartamento/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#departamento_mostrar').val(e.departamento.name);
                $('.fila-departamento').css('display','block');
                $('#departamento').css('display','none');
                $('#lista_ciudades').html(e.html);
                $('#ciudad').attr('placeholder','Seleccione una ciudad (municipio)');
                $('#ciudad').removeClass('no-cursor');
                $('#ciudad').removeAttr("readonly");
            }
        });
    }
});

$('body').on('click','.cambiar-departamento',function(){
    $('.fila-departamento').css('display','none');
    $('#departamento').val('');
    $('#departamento').css('display','block');
    $('.fila-ciudad').css('display','none');
    $('#ciudad').css('display','block');
    $('#lista_ciudades').html('');
    $('#ciudad').val('');
    $('#ciudad').addClass('no-cursor');
    $('#ciudad').attr("readonly","readonly");

    $('#departamento').attr('placeholder','Seleccione un departamento (estado)');
    $('#ciudad').attr('placeholder','Seleccione un departamento (estado)');
});

/*ciudad*/

$('body').on('change','#ciudad',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Ciudad!',
          'warning'
        );
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultarciudad/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#ciudad_mostrar').val(e.ciudad.name);
                $('.fila-ciudad').css('display','block');
                $('#ciudad').css('display','none');
            }
        });
    }
});

$('body').on('click','.cambiar-ciudad',function(){
    $('.fila-ciudad').css('display','none');
    $('#ciudad').val('');
    $('#ciudad').css('display','block');

    $('#ciudad').attr('placeholder','Seleccione una ciudad (municipio)');
});

/*Agregar nuevo responsable*/

var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;

function clone(){
    $("#res_container").children(".clonedInput").clone()
        .appendTo("#res_container")
        .attr("id", "clonedInput" +  cloneIndex)
        .find("*")
        .each(function() {
            var id = this.id || "";
            var match = id.match(regex) || [];
            if (match.length == 3) {
                this.id = match[1] + (cloneIndex);
            }
        })
        .on('click', 'a.clone', clone)
        .on('click', 'a.remove', remove);
    cloneIndex++;
}
function remove(){
    if($(".clonedInput").size() > 1){
        $("#res_container").children(".clonedInput:last").remove();
    }
}
$("a.clone").on("click", clone);

$("a.remove").on("click", remove);


$('body').on('click','.btn_guardar',function(){
    if(true === $("#formulario_patrocinador").parsley().validate()){
           swal({
              title: 'Esta seguro?',
              text: "Va a guardar un patrocinador",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
              var jqxhr = $.post( url_basico+"/guardarpatrocinador", $("#formulario_patrocinador").serialize())
                .done(function(data, textStatus, jqXHR) {
                    console.log("Guardado");
                })
                .fail(function(e) {
                    console.error(e.msj)
                })
                .always(function(e) {
                  console.log(e.msj)
                    swal(
                      'Guardado',
                       e.msj,
                      'success'
                    ).then(function () {
                        location.href = url_basico+"/feria/patrocinadores";
                    });
                });
            });
    }else{
        swal(
            'Advertencia',
            'Debe llenar los campo obligatorios.',
            'warning'
          )
    }
});

$(document).on('click','.btn_editar',function(){
    if(true === $("#formulario_patrocinador_editar").parsley().validate()){
           swal({
              title: 'Esta seguro?',
              text: "Va a editar un patrocinador",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
              var jqxhr = $.post( url_basico+"/editarpatrocinador", $("#formulario_patrocinador_editar").serialize())
                .done(function() {
                    console.log("Guardado");
                })
                .fail(function(e) {
                    console.error(e.msj)
                })
                .always(function(e) {
                  console.log(e.msj)
                    swal(
                      'Guardado',
                       e.msj,
                      'success'
                    ).then(function () {
                        //alert();
                        //location.href = url_basico+"/feria/patrocinadores";
                    });
                });
            });
    }else{
        swal(
            'Advertencia',
            'Debe llenar los campo obligatorios.',
            'warning'
          );
    }
});
