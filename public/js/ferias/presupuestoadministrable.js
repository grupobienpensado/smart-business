url_basico=$('#URL').val();
$('body').on('keyup','.detalle',function(){
    var jqxhr = $.post( url_basico+"/guardarferiapresupuestoadmin", $("#formulario_basico").serialize())
    .done(function() {
        console.log("Guardado");
    })
    .fail(function(e) {
        console.error(e.msj)
    })
    .always(function(e) {
      console.log(e.msj)
    });
});

$('body').on('click','.eliminar',function(){
   id=$(this).attr('id');
   id=id.replace('eliminar_','');
    swal({
      title: 'Esta seguro?',
      text: "Va a eliminar un item de presupuesto",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then(function () {
        $.ajax({
            url: url_basico+"/eliminaritempresupuesto/"+id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                swal(
                    'Eliminar!',
                    'Se elimino correctamente.',
                    'success'
                  ).then(function () {
                    parent.window.location.reload(true);
                });
            }
        });
    });
});
fila=$('#filas').val();
$('body').on('click','.crearitem',function(){
    $.ajax({
            url: url_basico+"/crearitempresupuesto",
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                numero=parseInt(fila)+1;
                html=`<input type="hidden" name="presupuesto[`+fila+`][id]" value="`+e.id+`">
                            <div class="row">
                                <div style="width: 2%;">
                                    `+numero+`.
                                </div>
                                <div class="col-sm-11">
                                    <div class="form-group">
                                        <label for="concepto`+fila+`" class="control-label sr-only">Concepto</label>
                                        <input type="text" class="form-control detalle" name="presupuesto[`+fila+`][concepto]" id="concepto`+fila+`"  placeholder="Concepto">
                                    </div>
                                </div>
                                <i class="fa fa-trash eliminar" id="eliminar_`+e.id+`" aria-hidden="true"></i>
                            </div>`;
                $('#formulario_basico').append(html);
                fila++;
            }
        });

});
