var CSRF_TOKEN = $('input[name=_token]').val();
url_basico = $('#URL').val();
$('[data-toggle="tooltip"]').tooltip();

// if dropzone exist
if( $('.dropzone').length > 0 ) {
    Dropzone.autoDiscover = false;

    $(".dropzone").dropzone({
        url: "/cargarubicacion",
        addRemoveLinks : true,
        maxFilesize: 30,
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictResponseError: 'File Upload Error.',
        success: function(e){
            swal(
                  'Cargado',
                  'La ubicación se cargo correctamente',
                  'success'
                ).then(function(){
                $('#cargar_ubicacion').modal('hide');
            })
        }
    });
}
// end if dropzone exist

//Quitar mensaje dropzone
$('.dz-message > span').html('');

/*Scroll con animacion*/
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
/*Fin Scroll con animacion*/

$(document).on('click', '.crear', function () {
    if(permiso_crear=="Si"){
        window.location.href = url_basico + "/crearferias";
    }else{
        swal('Advertencia','Usted no tiene permisos para crear','warning');
    }
});
$(document).on('click', '.calendario', function () {
    window.location.href = url_basico + "/feria/calendario";
});
$(document).on('click', '.patrocinadores', function () {
    window.location.href = url_basico + "/feria/patrocinadores";
});
$(document).on('click', '.proveedores', function () {
    window.location.href = url_basico + "/proveedores";
});
$(document).on('click', '.competencia', function () {
    window.location.href = url_basico + "/competencias";
});
$(document).on('click', '.botn-ingresar', function () {
    id = $(this).attr('id');
    window.location.href = url_basico + "/feria/ver/" + id;
});
/*Lista de ferias futuras*/
$(document).on('click','.feriasfuturas',function(){
    cargando=`<img src="`+url_basico+`/images/configuracion_crm/configuracion.gif">`;
    $('.area-listados').html(cargando);
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasfuturas",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.area-listados').html(e.content);
        }
    });
});

$(document).on('keyup','#buscar_futura',function(){
    buscar = $(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasfuturasfiltro",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico,
            filtro: buscar
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.fila-feria-futura').remove();
            $('.area-busqueda').after(e.content);
        }
    });
});

verFeria = function(id){
    var jqxhr = $.get( url_basico+"/viewferia/"+id)
    .always(function(model) {
        if(model.success){
            ubicacion = model.u;
            total_link = model.total_link;
            total_archivo = model.total_archivo;
            model = model.model;
            console.log(model);
            moment.locale('es');
            var foto = 'http://via.placeholder.com/250x250/fff/948e8e?text=Logo';
            if(model.logo != null && model.logo != ""){
               foto = url_basico+'/storage/ferias/'+model.logo;
            }
            swal({
              title: 'Ver Feria',
              html: `
            <div class="profile-empresa">
                <img src="`+foto+`" class="img-fluid mx-auto d-block img-empresa">
            </div>
            <div class="row">
              <div class="col-8 col-md-8 centrado">
                <div  class="hijo">
                  <h2 class="titulo">`+model.nombre+`</h2>
                  <p class="capitalize">
                    <i class="fa fa-map-marker"></i>
                    <span class="soloPri">`+ubicacion+`</span>
                  </p>
                  <p class="capitalize">
                      <i class="fa fa-hourglass-start"></i> Inicio `+moment(model.fecha_inicio).calendar()+`
                  </p>
                  <p class="capitalize">
                      <i class="fa fa-hourglass-end"></i> Cierre `+moment(model.fecha_fin).calendar()+`
                  </p>
                </div>
              </div>
              <div class="col-4 col-md-4 centrado" style="padding-top: 70px">
                <div class="hijo">
                  <h6>
                  <p class="subtitulo pull-left">
                    <i class="fa fa-star-o pull-left" aria-hidden="true"></i>
                    Creada el <span class="capitalize">`+moment(model.created_at).calendar()+`</span>
                  </p>
                  </h6>
                  <h6 style="padding-top: 20px;">
                  <p class="subtitulo subtitulo2 pull-left">
                    <i class="fa fa-repeat pull-left" aria-hidden="true"></i>
                    Actualizada el <span class="capitalize">`+moment(model.updated_at).calendar()+`</span>
                  </p>
                  </h6>
                </div>
              </div>
              <div class="col-2 col-md-2"></div>
              <div class="col-10 col-md-8" style="padding-top: 70px;text-align: justify;">`+model.descripcion+`</div>
              <div class="col-2 col-md-2"></div>
              <div class="col-2 col-md-2"></div>
              <div class="col-8 col-md-8" style="padding-top: 70px;text-align: center;">
                <ul class="social">
                  <li class="galeria" id="ga_`+model.id+`"><a href="#" data-toggle="tooltip" data-placement="top" title="Ver Galeria (`+total_archivo+`)"><i class="fa fa-camera-retro fa-3x"></i></a></li>

                  <li class="links" id="li_`+model.id+`"><a href="#" data-toggle="tooltip" data-placement="top" title="Ver Links (`+total_link+`)"><i class="fa fa-link fa-3x"></i></a></li>

                </ul>
              </div>
              <div class="col-2 col-md-2"></div>
              <div class="col-md-2"></div>
              <div class="col-8 col-md-8" id="contenido_feria">

              </div>
            </div>`,
              showCloseButton: true,
              focusConfirm: false,
              confirmButtonText: 'Cerrar'
            });
            $('.swal2-modal').css('width','90%');
        }else{
            swal(
              'Vaya...',
              '¡Algo salió mal!',
              'error'
            );
        }
    });
}

$('body').on('click','.galeria',function(){
   id=$(this).attr('id');
   id=id.replace('ga_','');
    $.ajax({
        url: url_basico+"/consultargaleria/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#contenido_feria').html(e.content);
        }
    });
});

$('body').on('click','.links',function(){
   id=$(this).attr('id');
   id=id.replace('li_','');
    $.ajax({
        url: url_basico+"/consultarlinks/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#contenido_feria').html(e.content);
        }
    });
});

$('body').on('click','.no_seleccionado',function(){
    $('.seleccionado').removeClass('seleccionado').addClass('no_seleccionado');
    $(this).addClass('seleccionado').removeClass('no_seleccionado');

     $(".seleccionado .video").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/play_blanco.png');
     $(".seleccionado .foto").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/img_blanco.png');
     $(".seleccionado .carpeta").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_blanca.png');

     $(".no_seleccionado .video").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/Play.png');
     $(".no_seleccionado .foto").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/Img_gris.png');
     $(".no_seleccionado .carpeta").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_gris.png');

    /*esconder y mostrar contenedores*/
    if ( $(".seleccionado .video").length > 0 ) {
      // hacer algo aquí si el elemento existe
        $('.archivos').css('display','none');
        $('#cont-videos').css('display', 'block');
    }

    if ( $(".seleccionado .foto").length > 0 ) {
      // hacer algo aquí si el elemento existe
        $('.archivos').css('display','none');
        $('#cont-imagenes').css('display', 'block');
    }

    if ( $(".seleccionado .carpeta").length > 0 ) {
      // hacer algo aquí si el elemento exist
        $('.archivos').css('display','none');
        $('#cont-archivos').css('display', 'block');
    }
});

function abrir(tipo,nombre){
    url_basico=$('#URL').val();
    $('#documento').modal('show');
    $('#ver_pdf').attr('src',url_basico+'/storage/ferias/multimedia/'+nombre);
}

function cerrar_modal(){
    $('#documento').modal('hiden');
}

/**
 * Función para activar una feria ya sea (EXPONENTE o VISITANTE)
 * @param INT id IDENTIFICACIÓN DE LA FERIA
 */
function activar_feria(id){
    var jqxhr = $.ajax({
        url: "/ferias-acciones",
        data: {
            _token: CSRF_TOKEN,
            id: id,
            accion: 0
        },
        cache: false,
        type: 'POST',
        success: function(e){
            activar_feria_sw_1(e.data['html']);
        }
    });
}

/**
 * Función que muestra un mensaje con la decisión que el usuario debe tomar (VISITANTE O EXPONENTE)
 * @param STRING html CONTENIDO DEL MENSAJE
 */
function activar_feria_sw_1(html){
    swal({
      title: 'Activar como',
      html: html,
      showCloseButton: true,
      showConfirmButton: false,
      allowOutsideClick: false
    })
}

/**
 * Función para activar la feria que esta en el listado
 * @param INT id    IDENTIFICACIÓN DE LA FERIA ACTIVAR
 * @param STRING value TIPO DE ACTIVACIÓN (EXPONENTE O VISITANTE)
 */
function activar_la_feria(id, value){
    if (value === 'Visitante') {
            $.ajax({
                url: "/activar_posible/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal({
                        type: 'success',
                        html: 'Guardado correctamente como: ' + value
                      }).then(function () {
                        location.href ="/ferias/menu";
                    });
                }
            });
          } else {
              var html = '';
              /*Traer formulario para llenar como exponente*/
              var jqxhr = $.ajax({
                url: "/ferias-acciones",
                data: {
                    _token: CSRF_TOKEN,
                    id: id,
                    accion: 1
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    html = e.data['html'];
                }
            });
            /*Fin Traer formulario para llenar como exponente*/
              swal({
                    title: 'Advertencia',
                    type: 'warning',
                    html: 'Debe llenar el formulario para ser exponente',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Llenar',
                    cancelButtonText: 'Cancelar',
                    showCancelButton: 'true',
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        formulario_exponente(html);
                    },
                    allowOutsideClick: false
                  });
          }
}

/**
 * Función para mostrar el formulario de exponente
 * @param STRING html VARIABLE CON EL HTML DEL FORMULARIO
 */
function formulario_exponente(html){
    swal({
        html: html,
        confirmButtonColor: '#FF0000',
        confirmButtonText: 'Cerrar',
        allowOutsideClick: false
    });
    $('.dropzone').html5imageupload();
    $('.swal2-modal').css('width','90%');
}

/**
 * Función para guardar la feria exponente
 */
function guardar_exponente(){
    if(true === $("#formulario_exponente").parsley().validate()){
        $('#btn_save_exponente').css('display','none');
        $('#msj_save_exponente').css('display','block');
    }else{
        $("#msj_falta_campos_exponente").fadeIn(2000);
        setTimeout(function(){ $("#msj_falta_campos_exponente").fadeOut(2000); }, 4000);
    }
}

/**
 * Función para guardar o cancelar
 * @param STRING desicion (SI NO)
 */
function guardar_exponente_desicion(desicion){
    if(desicion == "si"){
       var jqxhr = $.post( "/guardarexponente", $("#formulario_exponente").serialize())
        .done(function() {
            console.log("Guardado");
        })
        .fail(function(e) {
            console.error(e.msj);
        })
        .always(function(e) {
          console.log(e.msj);
            swal({
                title:'Guardado',
                type: 'success',
                html: 'Guardado correctamente'
              }).then(function(){
                    location.href ="/ferias/menu";
                });
        });
    }else{
        $('#btn_save_exponente').css('display','block');
        $('#msj_save_exponente').css('display','none');
    }
}

/**
 * Función para buscar maquinas segun categoria
 * @param STRING tipo LA CATEGORIA QUE DESEO BUSCAR
 */
function formulario_exponente_tipo(tipo){
    $.ajax({
        url: "/buscarmaquina/"+tipo,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#maquina').html('<option value="" disabled selected>Seleccione una maquina</option>'+e.maquinas);
            $('#referencia').html('<option value="" disabled selected>Seleccione una maquina</option>');
        }
    });
}

/**
 * Función para buscar referencias segun la maquina
 * @param INT maquina IDENTIFICACIÓN DE LA MAQUINA
 */
function formulario_exponente_maquina(maquina){
    $.ajax({
        url: "/buscarreferencia/"+maquina,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#referencia').html('<option value="" disabled selected>Seleccione una referencia</option>'+e.referencia);
        }
    });
}

/**
 * Función para agregar una maquina a la feria
 */
function formulario_exponente_agregar_maquina(){
    id_referencia=$('#referencia').val();
    if(id_referencia!=null){
        referencias=$('#referencias_agregadas').val();
        console.log(referencias.indexOf(id_referencia));
        if(referencias.indexOf(id_referencia) == -1){ //if para saber si no existe la referencia
            $('#referencias_agregadas').val(referencias+'%%'+id_referencia);
            $.ajax({
                url: "/imagenmaquina/"+id_referencia,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    $('.cont-imagenes').prepend(e.imagen);
                    $("#mensaje_maquina_exito").fadeIn(2000);
                    setTimeout(function(){ $("#mensaje_maquina_exito").fadeOut(2000); }, 4000);
                }
            });
        }else{
            $("#mensaje_maquina_repetida").fadeIn(2000);
            setTimeout(function(){ $("#mensaje_maquina_repetida").fadeOut(2000); }, 4000);
        }
    }else{
        $("#mensaje_maquina_error").fadeIn(2000);
        setTimeout(function(){ $("#mensaje_maquina_error").fadeOut(2000); }, 4000);
    }
}

/**
 * Función para eliminar una maquina que ya halla seleccionado
 * @param STRING id         IDENTIFICACIÓN DE LA IMAGEN DE LA MAQUINA PARA ELIMINARLO DEL HTML
 * @param INT referencia IDENTIFICACIÓN DE LA REFERENCIA PARA ELIMINARLO DEL INPUT=>HIDDEN
 */
function eliminar_maquina(id,referencia){
    $("#mensaje_maquina_eliminado").fadeIn(1000);
    setTimeout(function(){ $("#mensaje_maquina_eliminado").fadeOut(1000); }, 3000);

    $('#'+id).remove();
    referencias=$('#referencias_agregadas').val();
    reemplazar="%%"+referencia;
    referencias=referencias.replace(reemplazar, "");
    $('#referencias_agregadas').val(referencias);
}

$(document).on('click','.activar-funcionario',function(){
   id=$(this).attr('id');
   id=id.replace('af_','');
   invitados_essi=$('#cantidad_personas').val();
   invitado=invitados_essi.split("%%");
   cont=0;
   $.each(invitado, function( k, v ) {
       if(v==id){
          cont++;
       }
   });
   if(cont==0){
       if(invitados_essi==""){
          $('#cantidad_personas').val(id);
        }else{
            $('#cantidad_personas').val(invitados_essi+'%%'+id);
        }
    }
    $(this).removeClass('activar-funcionario');
    $(this).addClass('desactivar-funcionario');
    $.ajax({
        url: "/anadiruserinv/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('.listado_funcionarios_invitados').append(e.content);
        }
    });
});

$(document).on('click','.desactivar-funcionario',function(){
    id=$(this).attr('id');
    id=id.replace('af_','');
    invitados_essi=$('#cantidad_personas').val();
    invitado=invitados_essi.split("%%");
    nuevo='';
    $.each(invitado, function( k, v ) {
       if(v!=id){
          if(nuevo==''){
            nuevo+=v;
          }else{
              nuevo+='%%'+v;
          }
       }
   });
    $('#cantidad_personas').val(nuevo);
    $(this).addClass('activar-funcionario');
    $(this).removeClass('desactivar-funcionario');
    $('#funcionario_invitado'+id).remove();
});

w=0;
$(document).on('click','.agregar-objetivo',function(){
    w++;
    numero=$('.n_obj').length;
    numero++;
    html=`<div class="row n_obj" id="n_obj`+w+`">
                <div class="col-md-1 num-objetivo">
                    <p class="h5"><strong>`+numero+`</strong></p>
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="objetivos[`+w+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminarobjetivo(`+w+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-objetivos').append(html);
});

function eliminarobjetivo(id){
    $('#n_obj'+id).remove();
    i=1;
    $(".num-objetivo").each(function(){
        $(this).html('<p class="h5"><strong>'+i+'</strong></p>');
        i++;
    });
}

al=0;
$(document).on('click','.agregar-alcance',function(){
    al++;
    numero=$('.n_alc').length;
    numero++;
    html=`<div class="row n_alc" id="n_alc`+al+`">
                <div class="col-md-1 num-alcance">
                    <p class="h5"><strong>`+numero+`</strong></p>
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="alcances[`+al+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminaralcance(`+al+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-alcances').append(html);
});
function eliminaralcance(id){
    $('#n_alc'+id).remove();
    i=1;
    $(".num-alcance").each(function(){
        $(this).html('<p class="h5"><strong>'+i+'</strong></p>');
        i++;
    });
}

es=0;
$(document).on('click','.agregar-estrategia',function(){
    es++;
    numero=$('.n_est').length;
    numero++;
    html=`<div class="row n_est" id="n_est`+es+`">
                <div class="col-md-1 num-estrategia">
                    <p class="h5"><strong>`+numero+`</strong></p>
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="estrategia[`+es+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminarestrategia(`+es+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-estrategias').append(html);
});
function eliminarestrategia(id){
    $('#n_est'+id).remove();
    i=1;
    $(".num-estrategia").each(function(){
        $(this).html('<p class="h5"><strong>'+i+'</strong></p>');
        i++;
    });
}
/*Fin Lista de ferias futuras*/


/* Lista de ferias Asistidas Exponente*/
$(document).on('click','.exponente',function(){
    cargando=`<img src="`+url_basico+`/images/configuracion_crm/configuracion.gif">`;
    $('.area-listados').html(cargando);
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasexponente",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.area-listados').html(e.content);
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});

$(document).on('keyup','#buscar_exponente',function(){
    buscar = $(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasexponentefiltro",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico,
            filtro: buscar
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.list-exponente').html(e.content);
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});

/* Fin Lista de ferias Asistidas Exponente*/

/* Lista de ferias Asistidas Visitante*/
$(document).on('click','.visitante',function(){
    cargando=`<img src="`+url_basico+`/images/configuracion_crm/configuracion.gif">`;
    $('.area-listados').html(cargando);
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasvisitante",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.area-listados').html(e.content);
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});

$(document).on('keyup','#buscar_visitante',function(){
    buscar = $(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasvisitantefiltro",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico,
            filtro: buscar
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.list-visitante').html(e.content);
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});
/*Fin Lista de ferias Asistidas Visitante*/


/* Lista de ferias Asistidas Visitante*/
$(document).on('click','.no-asistidas',function(){
    cargando=`<img src="`+url_basico+`/images/configuracion_crm/configuracion.gif">`;
    $('.area-listados').html(cargando);
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasnoasistidas",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.area-listados').html(e.content);
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});

$(document).on('keyup','#buscar_perdida',function(){
    buscar = $(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/consultarlistadoferiasnoasistidasfiltro",
        data: {
            _token: CSRF_TOKEN,
            url: url_basico,
            filtro: buscar
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.fila-feria-perdida').remove();
            $('.area-busqueda').after(e.content)
        }
    });
});
/*Fin Lista de ferias No-asistidas*/

$('body').on('click','.subir-ubicacion1',function(){
   $('#cargar_ubicacion').modal('show');
    id=$(this).attr('id');
    id=id.replace('ubi_','');
    $('#id_feria').val(id);
});

/**
 * Función para crear sweet alert de no tiene permiso
 * @param STRING msj MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
 */
function no_permiso(msj){
    swal('Advertencia',msj,'warning');
}
