url_basico=$('#URL').val();
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#example').DataTable({
      language: {
        processing:     "Tratamiento en curso ...",
        search:         "Buscar&nbsp;:",
        lengthMenu:     "Mostrar _MENU_ ferias posibles",
        info:           "Visualizacion de elementos del  _START_ al _END_ de _TOTAL_ ferias posibles",
        infoEmpty:      "Ver de elemento 0 al 0 de 0 ferias posibles",
        infoPostFix:    "",
        loadingRecords: "Cargando...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "No hay datos disponibles en la tabla",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Ultimo"
        },
        aria: {
            sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
            sortDescending: ": habilitado para ordenar la columna en orden descendente"
        }
      },
      order: [ [3,'desc'] ],
    });
    var t = $('#example').DataTable();

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    }).draw();
});

verFeria = function(id){
    var jqxhr = $.get( "/viewferia/"+id)
    .always(function(model) {
        if(model.success){
            total_link = model.total_link;
            total_archivo = model.total_archivo;
            model = model.model;
            console.log(model);
            moment.locale('es');
            var foto = 'http://via.placeholder.com/250x250/fff/948e8e?text=Logo';
            if(model.logo != null && model.logo != ""){
               foto = '/storage/ferias/'+model.logo;
            }
            swal({
              title: 'Ver Feria',
              html: `
            <div class="profile-empresa">
                <img src="`+foto+`" class="img-fluid mx-auto d-block img-empresa">
            </div>
            <div class="row">
              <div class="col-8 col-md-8 centrado">
                <div  class="hijo">
                  <h2 class="titulo">`+model.nombre+`</h2>
                  <p class="capitalize">
                    <i class="fa fa-map-marker"></i>
                    <span class="soloPri">`+model.ubicacion+`</span>
                  </p>
                  <p class="capitalize">
                      <i class="fa fa-hourglass-start"></i> Inicio `+moment(model.fecha_inicio).calendar()+`
                  </p>
                  <p class="capitalize">
                      <i class="fa fa-hourglass-end"></i> Cierre `+moment(model.fecha_fin).calendar()+`
                  </p>
                </div>
              </div>
              <div class="col-4 col-md-4 centrado" style="padding-top: 70px">
                <div class="hijo">
                  <h6>
                  <p class="subtitulo pull-left">
                    <i class="fa fa-star-o pull-left" aria-hidden="true"></i>
                    Creada el <span class="capitalize">`+moment(model.created_at).calendar()+`</span>
                  </p>
                  </h6>
                  <h6 style="padding-top: 20px;">
                  <p class="subtitulo subtitulo2 pull-left">
                    <i class="fa fa-repeat pull-left" aria-hidden="true"></i>
                    Actualizada el <span class="capitalize">`+moment(model.updated_at).calendar()+`</span>
                  </p>
                  </h6>
                </div>
              </div>
              <div class="col-2 col-md-2"></div>
              <div class="col-10 col-md-8" style="padding-top: 70px;text-align: justify;">`+model.descripcion+`</div>
              <div class="col-2 col-md-2"></div>
              <div class="col-2 col-md-2"></div>
              <div class="col-8 col-md-8" style="padding-top: 70px;text-align: center;">
                <ul class="social">
                  <li class="galeria" id="ga_`+model.id+`"><a href="#" data-toggle="tooltip" data-placement="top" title="Ver Galeria (`+total_archivo+`)"><i class="fa fa-camera-retro fa-3x"></i></a></li>

                  <li class="links" id="li_`+model.id+`"><a href="#" data-toggle="tooltip" data-placement="top" title="Ver Links (`+total_link+`)"><i class="fa fa-link fa-3x"></i></a></li>

                </ul>
              </div>
              <div class="col-2 col-md-2"></div>
              <div class="col-md-2"></div>
              <div class="col-8 col-md-8" id="contenido_feria">

              </div>
            </div>`,
              showCloseButton: true,
              focusConfirm: false,
              confirmButtonText: 'Cerrar'
            });
            $('.swal2-modal').css('width','90% !important');
        }else{
            swal(
              'Vaya...',
              '¡Algo salió mal!',
              'error'
            );
        }
    });
}

$('body').on('click','.galeria',function(){
   id=$(this).attr('id');
   id=id.replace('ga_','');
    $.ajax({
        url: url_basico+"/consultargaleria/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#contenido_feria').html(e.content);
        }
    });
});

$('body').on('click','.links',function(){
   id=$(this).attr('id');
   id=id.replace('li_','');
    $.ajax({
        url: url_basico+"/consultarlinks/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#contenido_feria').html(e.content);
        }
    });
});

$('body').on('click','.no_seleccionado',function(){
    $('.seleccionado').removeClass('seleccionado').addClass('no_seleccionado');
    $(this).addClass('seleccionado').removeClass('no_seleccionado');

     $(".seleccionado .video").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/play_blanco.png');
     $(".seleccionado .foto").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/img_blanco.png');
     $(".seleccionado .carpeta").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_blanca.png');

     $(".no_seleccionado .video").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/Play.png');
     $(".no_seleccionado .foto").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/Img_gris.png');
     $(".no_seleccionado .carpeta").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_gris.png');

    /*esconder y mostrar contenedores*/
    if ( $(".seleccionado .video").length > 0 ) {
      // hacer algo aquí si el elemento existe
        $('.archivos').css('display','none');
        $('#cont-videos').css('display', 'block');
    }

    if ( $(".seleccionado .foto").length > 0 ) {
      // hacer algo aquí si el elemento existe
        $('.archivos').css('display','none');
        $('#cont-imagenes').css('display', 'block');
    }

    if ( $(".seleccionado .carpeta").length > 0 ) {
      // hacer algo aquí si el elemento exist
        $('.archivos').css('display','none');
        $('#cont-archivos').css('display', 'block');
    }
});

function abrir(tipo,nombre){
    url_basico=$('#URL').val();
    $('#documento').modal('show');
    $('#ver_pdf').attr('src',url_basico+'/storage/ferias/multimedia/'+nombre);
}

function cerrar_modal(){
    $('#documento').modal('hiden');
}
/*
$('body').on('click','.activar-posible',function(){
    id=$(this).attr('id');
    id=id.replace("posible_", "");
    swal({
      title: 'Seleccione un tipo',
      input: 'select',
      inputOptions: {
        'Exponente': 'Exponente',
        'Visitante': 'Visitante'
      },
      inputPlaceholder: 'Seleccione un tipo',
      showCancelButton: true,
      inputValidator: function (value) {
        return new Promise(function (resolve, reject) {
          if (value === 'Visitante') {
            $.ajax({
                url: url_basico+"/activar_posible/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal({
                        type: 'success',
                        html: 'Guardado correctamente como: ' + value
                      }).then(function () {
                        location.href =url_basico+"/feriasasistidas";
                    });
                }
            });
          } else {
              $('#id_feria').val(id);
              $('.slideInDown').css('display','none');
              $('#contenedor-exponente').css('display','block');
              swal({
                    type: 'warning',
                    html: 'Debe llenar el formulario para ser exponente'
                  })
          }
        })
      }
    }).then(function (result) {
      swal({
        type: 'success',
        html: 'Se selecciona ' + result
      })
    });
});*/

$('body').on('click','.activar-posible',function(){
    id=$(this).attr('id');
    id=id.replace("posible_", "");
    swal({
      title: 'Activar como',
      html: `<div class="row"><div class="col-md-2"></div><div class="col-md-8 botones-activacion"><a href="#" onclick="activar_la_feria(`+id+`,'Visitante')" class="icon-button twitter2" title="Visitante"><i class="fa fa-male"></i><span></span></a>
            <a href="#" onclick="activar_la_feria(`+id+`,'exponente')" class="icon-button facebook2" title="Exponente"><i class="fa fa-users"></i><span></span></a>
            </div><div class="col-md-2"></div>`,
      showCloseButton: true,
      showConfirmButton: false
    })
});

function activar_la_feria(id, value){
    if (value === 'Visitante') {
            $.ajax({
                url: url_basico+"/activar_posible/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal({
                        type: 'success',
                        html: 'Guardado correctamente como: ' + value
                      }).then(function () {
                        location.href =url_basico+"/feriasasistidas";
                    });
                }
            });
          } else {
              $('#id_feria').val(id);
              $('.slideInDown').css('display','none');
              $('#contenedor-exponente').css('display','block');
              swal({
                    type: 'warning',
                    html: 'Debe llenar el formulario para ser exponente'
                  })
          }
}

$('body').on('change','#tipo',function(){
   tipo=$(this).val();
    $.ajax({
        url: url_basico+"/buscarmaquina/"+tipo,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#maquina').html('<option value="" disabled selected>Seleccione una maquina</option>'+e.maquinas);
            $('#referencia').html('<option value="" disabled selected>Seleccione una maquina</option>');
        }
    });
});

$('body').on('change','#maquina',function(){
   maquina=$(this).val();
    $.ajax({
        url: url_basico+"/buscarreferencia/"+maquina,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#referencia').html('<option value="" disabled selected>Seleccione una referencia</option>'+e.referencia);
        }
    });
});

$('body').on('click','#agregar_equipo',function(){
    $.ajax({
        url: url_basico+"/listadecategorias",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
              title: 'Agregar Maquina',
              html: e.content,
              showCloseButton: true,
              showConfirmButton: true,
              confirmButtonText: '<i class="fa fa-floppy-o"></i> Agregar',
              confirmButtonColor: '#4BA84B',
            }).then(function(){
                id_referencia=$('#referencia').val();
                if(id_referencia!=null){
                    referencias=$('#referencias_agregadas').val();
                    console.log(referencias.indexOf(id_referencia));
                    if(referencias.indexOf(id_referencia) == -1){ //if para saber si no existe la referencia
                        $('#referencias_agregadas').val(referencias+'%%'+id_referencia);
                        $.ajax({
                            url: url_basico+"/imagenmaquina/"+id_referencia,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'GET',
                            success: function(e){
                                $('.cont-imagenes').prepend(e.imagen);
                            }
                        });
                    }
                }else{
                    swal({
                        type: 'error',
                        html: 'Seleccione una referencia'
                      }).then(function(){
                        $('#agregar_equipo').click();
                    });
                }
            });
        }
    });

});

function eliminar_maquina(id,referencia){
    $('#'+id).remove();
    referencias=$('#referencias_agregadas').val();
    reemplazar="%%"+referencia;
    referencias=referencias.replace(reemplazar, "");
    $('#referencias_agregadas').val(referencias);
}

$('body').on('click','.btn_guardar_expo',function(){
    event.preventDefault();
  if(true === $("#formulario_exponente").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a guardar como exponente",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                var jqxhr = $.post( url_basico+"/guardarexponente", $("#formulario_exponente").serialize())
                .done(function() {
                    console.log("Guardado");
                })
                .fail(function(e) {
                    console.error(e.msj)
                })
                .always(function(e) {
                  console.log(e.msj);
                    swal({
                        title:'Guardado',
                        type: 'success',
                        html: 'Guardado correctamente'
                      }).then(function(){
                            location.href =url_basico+"/feriasasistidas";
                        });
                });
            });
    }else{
        swal('Faltan campos por completar');
    }
});

$(document).on('click','#agregar_invitado',function(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var cant_per = $('#cantidad_personas').val();
    var jqxhr = $.ajax({
        url: url_basico+"/listadefuncionarios",
        data: {
            _token: CSRF_TOKEN,
            personas: cant_per,
            url_basico: url_basico
        },
        cache: false,
        type: 'POST',
        success: function(e){
            swal({
              html: e.content,
              showCloseButton: true,
              showConfirmButton: false,
            }).then(function(){

            });
            $('.swal2-show').css('width','70%');
        }
    });
});

$(document).on('click','.activar-funcionario',function(){
   id=$(this).attr('id');
   id=id.replace('af_','');
   invitados_essi=$('#cantidad_personas').val();
   invitado=invitados_essi.split("%%");
   cont=0;
   $.each(invitado, function( k, v ) {
       if(v==id){
          cont++;
       }
   });
   if(cont==0){
       if(invitados_essi==""){
          $('#cantidad_personas').val(id);
        }else{
            $('#cantidad_personas').val(invitados_essi+'%%'+id);
        }
    }
    $(this).removeClass('activar-funcionario');
    $(this).addClass('desactivar-funcionario');
    $.ajax({
        url: url_basico+"/anadiruserinv/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('.listado_funcionarios_invitados').append(e.content);
        }
    });
});

$(document).on('click','.desactivar-funcionario',function(){
    id=$(this).attr('id');
    id=id.replace('af_','');
    invitados_essi=$('#cantidad_personas').val();
    invitado=invitados_essi.split("%%");
    nuevo='';
    $.each(invitado, function( k, v ) {
       if(v!=id){
          if(nuevo==''){
            nuevo+=v;
          }else{
              nuevo+='%%'+v;
          }
       }
   });
    $('#cantidad_personas').val(nuevo);
    $(this).addClass('activar-funcionario');
    $(this).removeClass('desactivar-funcionario');
    $('#funcionario_invitado'+id).remove();
});
w=0;
$(document).on('click','.agregar-objetivo',function(){
    w++;
    numero=$('.n_obj').length;
    numero++;
    html=`<div class="row n_obj" id="n_obj`+w+`">
                <div class="col-md-1 num-objetivo">
                    `+numero+`
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="objetivos[`+w+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminarobjetivo(`+w+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-objetivos').append(html);
});

function eliminarobjetivo(id){
    $('#n_obj'+id).remove();
    i=1;
    $(".num-objetivo").each(function(){
        $(this).html(i);
        i++;
    });
}


al=0;
$(document).on('click','.agregar-alcance',function(){
    al++;
    numero=$('.n_alc').length;
    numero++;
    html=`<div class="row n_alc" id="n_alc`+al+`">
                <div class="col-md-1 num-alcance">
                    `+numero+`
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="alcances[`+al+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminaralcance(`+al+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-alcances').append(html);
});

function eliminaralcance(id){
    $('#n_alc'+id).remove();
    i=1;
    $(".num-alcance").each(function(){
        $(this).html(i);
        i++;
    });
}

es=0;
$(document).on('click','.agregar-estrategia',function(){
    es++;
    numero=$('.n_est').length;
    numero++;
    html=`<div class="row n_est" id="n_est`+es+`">
                <div class="col-md-1 num-estrategia">
                    `+numero+`
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="estrategia[`+es+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminarestrategia(`+es+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-estrategias').append(html);
});

function eliminarestrategia(id){
    $('#n_est'+id).remove();
    i=1;
    $(".num-estrategia").each(function(){
        $(this).html(i);
        i++;
    });
}

$('.dropzone').html5imageupload();
