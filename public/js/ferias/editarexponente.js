//imagen
$('.dropzone').html5imageupload();
url_basico=$('#URL').val();
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

$('body').on('click','#agregar_equipo',function(){
    $.ajax({
        url: url_basico+"/listadecategorias",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
              title: 'Agregar Maquina',
              html: e.content,
              showCloseButton: true,
              showConfirmButton: true,
              confirmButtonText: '<i class="fa fa-floppy-o"></i> Agregar',
              confirmButtonColor: '#4BA84B',
            }).then(function(){
                id_referencia=$('#referencia').val();
                if(id_referencia!=null){
                    referencias=$('#referencias_agregadas').val();
                    console.log(referencias.indexOf(id_referencia));
                    if(referencias.indexOf(id_referencia) == -1){ //if para saber si no existe la referencia
                        $('#referencias_agregadas').val(referencias+'%%'+id_referencia);
                        $.ajax({
                            url: url_basico+"/imagenmaquina/"+id_referencia,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'GET',
                            success: function(e){
                                $('.cont-imagenes').prepend(e.imagen);
                                $('[data-toggle="tooltip"]').tooltip();
                            }
                        });
                    }
                }else{
                    swal({
                        type: 'error',
                        html: 'Seleccione una referencia'
                      }).then(function(){
                        $('#agregar_equipo').click();
                    });
                }
            });
        }
    });

});

$('body').on('change','#tipo',function(){
   tipo=$(this).val();
    $.ajax({
        url: url_basico+"/buscarmaquina/"+tipo,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#maquina').html('<option value="" disabled selected>Seleccione una maquina</option>'+e.maquinas);
            $('#referencia').html('<option value="" disabled selected>Seleccione una maquina</option>');
        }
    });
});

$('body').on('change','#maquina',function(){
   maquina=$(this).val();
    $.ajax({
        url: url_basico+"/buscarreferencia/"+maquina,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#referencia').html('<option value="" disabled selected>Seleccione una referencia</option>'+e.referencia);
        }
    });
});

function eliminar_maquina(id,referencia){
    $('#'+id).remove();
    referencias=$('#referencias_agregadas').val();
    reemplazar="%%"+referencia;
    referencias=referencias.replace(reemplazar, "");
    $('#referencias_agregadas').val(referencias);
    $('.tooltip').removeClass('show');
}

$(document).on('click','#agregar_invitado',function(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var cant_per = $('#cantidad_personas').val();
    var jqxhr = $.ajax({
        url: url_basico+"/listadefuncionarios",
        data: {
            _token: CSRF_TOKEN,
            personas: cant_per,
            url_basico: url_basico
        },
        cache: false,
        type: 'POST',
        success: function(e){
            swal({
              html: e.content,
              showCloseButton: true,
              showConfirmButton: false,
            }).then(function(){

            });
            $('.swal2-show').css('width','70%');
        }
    });
});

$(document).on('click','.activar-funcionario',function(){
   id=$(this).attr('id');
   id=id.replace('af_','');
   invitados_essi=$('#cantidad_personas').val();
   invitado=invitados_essi.split("%%");
   cont=0;
   $.each(invitado, function( k, v ) {
       if(v==id){
          cont++;
       }
   });
   if(cont==0){
       if(invitados_essi==""){
          $('#cantidad_personas').val(id);
        }else{
            $('#cantidad_personas').val(invitados_essi+'%%'+id);
        }
    }
    $(this).removeClass('activar-funcionario');
    $(this).addClass('desactivar-funcionario');
    $.ajax({
        url: url_basico+"/anadiruserinv/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('.listado_funcionarios_invitados').append(e.content);
        }
    });
});

$(document).on('click','.desactivar-funcionario',function(){
    id=$(this).attr('id');
    id=id.replace('af_','');
    invitados_essi=$('#cantidad_personas').val();
    invitado=invitados_essi.split("%%");
    nuevo='';
    $.each(invitado, function( k, v ) {
       if(v!=id){
          if(nuevo==''){
            nuevo+=v;
          }else{
              nuevo+='%%'+v;
          }
       }
   });
    $('#cantidad_personas').val(nuevo);
    $(this).addClass('activar-funcionario');
    $(this).removeClass('desactivar-funcionario');
    $('#funcionario_invitado'+id).remove();
});

w=$('#objetivos_total').val();
$(document).on('click','.agregar-objetivo',function(){
    w++;
    numero=$('.n_obj').length;
    numero++;
    html=`<div class="row n_obj" id="n_obj`+w+`">
                <div class="col-md-1 num-objetivo">
                    `+numero+`
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="objetivos[`+w+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminarobjetivo(`+w+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-objetivos').append(html);
});

function eliminarobjetivo(id){
    $('#n_obj'+id).remove();
    i=1;
    $(".num-objetivo").each(function(){
        $(this).html(i);
        i++;
    });
}


al=$('#alcances_total').val();
$(document).on('click','.agregar-alcance',function(){
    al++;
    numero=$('.n_alc').length;
    numero++;
    html=`<div class="row n_alc" id="n_alc`+al+`">
                <div class="col-md-1 num-alcance">
                    `+numero+`
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="alcances[`+al+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminaralcance(`+al+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-alcances').append(html);
});

function eliminaralcance(id){
    $('#n_alc'+id).remove();
    i=1;
    $(".num-alcance").each(function(){
        $(this).html(i);
        i++;
    });
}

es=$('#estrategias_total').val();
$(document).on('click','.agregar-estrategia',function(){
    es++;
    numero=$('.n_est').length;
    numero++;
    html=`<div class="row n_est" id="n_est`+es+`">
                <div class="col-md-1 num-estrategia">
                    `+numero+`
                </div>
                <div class="col-md-11 mt-1">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" class="form-control" name="estrategia[`+es+`]" required>
                        </div>
                        <div class="col-md-1 text-center">
                            <a class="eliminar" onclick="eliminarestrategia(`+es+`)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>`;
    $('.listado-estrategias').append(html);
});

function eliminarestrategia(id){
    $('#n_est'+id).remove();
    i=1;
    $(".num-estrategia").each(function(){
        $(this).html(i);
        i++;
    });
}

$('body').on('click','.btn_guardar_expo',function(){
    event.preventDefault();
  if(true === $("#formulario_exponente").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a guardar como exponente",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                var jqxhr = $.post( url_basico+"/editarexponente", $("#formulario_exponente").serialize())
                .done(function() {
                    console.log("Guardado");
                })
                .fail(function(e) {
                    console.error(e.msj)
                })
                .always(function(e) {
                  console.log(e.msj);
                    swal({
                        title:'Guardado',
                        type: 'success',
                        html: 'Guardado correctamente'
                      }).then(function(){
                            location.href =url_basico+"/feriasasistidas";
                        });
                });
            });
    }else{
        swal('Faltan campos por completar');
    }
});
