url_basico = $('#URL').val();
id_feria=$('#id_feria').val();
$(function() {
    $('.dropzone').html5imageupload();
    $('[data-toggle="tooltip"]').tooltip();
});

$('body').on('click','#guardar_invitado',function(){
    if(true === $("#formulario").parsley().validate()){
        swal({
          title: 'Esta seguro?',
          text: "Va a guardar un invitado",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var jqxhr = $.post( url_basico+"/editarinvitadonuevo", $("#formulario").serialize())
            .done(function() {
                console.log("Guardado");
            })
            .fail(function(e) {
                console.error(e.msj)
            })
            .always(function(e) {
              console.log(e.msj);
                swal(
                    'Guardado!',
                    e.msj,
                    'success'
                  ).then(function () {
                    location.href = url_basico+"/invitados/ver/"+id_feria;
                  });
            });
        });
    }
});
