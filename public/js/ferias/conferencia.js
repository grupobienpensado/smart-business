url_basico=$('#URL').val();
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.datatime').bootstrapMaterialDatePicker({
          time: true,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD HH:mm',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
    listar();
});

function tiempo(){
    $('.datatime2').bootstrapMaterialDatePicker({
          time: false,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD',
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
}

function listar(){
    id = $('#id_feria').val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
            url: url_basico+"/listadoconferenciasferias",
            data: {
                _token: CSRF_TOKEN,
                id_feria: id,
                ur_basico: url_basico
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('.crear-conferencia').after(e.content);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
}

function new_conference(id){
    swal({
          title: 'Ingrese el nombre de la conferencia',
          type: 'info',
          html:'<input type="text" id="name_new" class="form-control">',
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:
            'Crear',
          cancelButtonText:
          'Cancelar'
        }).then(function (){
            if(($('#name_new').val())!=""){
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: url_basico+"/crearconferenciasferias",
                        data: {
                            _token: CSRF_TOKEN,
                            id: id,
                            nombre: $('#name_new').val()
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            $('.item-conferen').remove();
                            listar();
                        }
                    });
            }else{
                swal(
                  'Oops...',
                  'Debe ingresar el nombre de la conferencia',
                  'error'
                ).then(function(){
                   new_conference(id);
                });
            }
        });
}

$(document).on('click','.crear-conferencista',function(){
    id=$(this).attr('id');
    id=id.replace('conferencista','');
    $('#id_conferencia').val(id);

    /*con=0;
    $(".f-conferencista"+id).each(function(){
        con++;
    });
    yaexiste(con,id);*/

   $('#crear_conferencista').modal('show');
});

function yaexiste(yaex,conf){
    if ( $("#fila_conferencista"+yaex+"_"+conf).length > 0 ) {
      // hacer algo aquí si el elemento existe
      yaex++;
      yaexiste(yaex);
    }else{
        yaex=parseInt(yaex);
        $('#valor_f').val(yaex);
        return yaex;
    }
}

//imagen
$('.dropzone').html5imageupload();

$('body').on('click','.guardar_conferencista',function(){
    if(true === $("#formulario_conferencista").parsley().validate()){
        swal({
          title: 'Esta seguro?',
          text: "Desea guardar el conferencista",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Guardar!'
        }).then(function () {
            var jqxhr = $.post( url_basico+"/guardarconferencista", $("#formulario_conferencista").serialize())
            .done(function() {
                console.log("Guardado");
            })
            .fail(function(e) {
                console.error(e.msj)
            })
            .always(function(e) {
              console.log(e.msj);
              swal(
                'Guardado!',
                e.msj,
                'success'
              ).then(function () {
                  $('.item-conferen').remove();
                  listar();
                  $('.close').click();
              });
            });
        })
    }else{
        swal(
            'Error',
            'Debe llenar los campos obligatorios.',
            'error'
          );
    }
});

function eliminar_conferencista(conferencista,conferencia,fila){
     swal({
          title: 'Esta seguro?',
          text: "Desea eliminar el conferencista",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!'
        }).then(function () {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                    url: url_basico+"/eliminarconferencista",
                    data: {
                        _token: CSRF_TOKEN,
                        id_conferencia: conferencia,
                        conferencista: conferencista
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal(
                            'Eliminado!',
                            e.msj,
                            'success'
                          ).then(function () {
                            $('.item-conferen').remove();
                            listar();
                          });
                    }
                });
        });
}

function eliminar_tema(tema,conferencia,fila){
    swal({
          title: 'Esta seguro?',
          text: "Desea eliminar el tema "+tema,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!'
        }).then(function () {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                    url: url_basico+"/eliminartema",
                    data: {
                        _token: CSRF_TOKEN,
                        id_conferencia: conferencia,
                        tema: tema
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal(
                            'Eliminado!',
                            e.msj,
                            'success'
                          ).then(function () {
                            $('.item-conferen').remove();
                            listar();
                          });
                    }
                });
        });
}
te=0;
$('body').on('click','.crear-tema',function(){
   id=$(this).attr('id');
   id=id.replace('tema','');
   $('#id_conferencia_tema').val(id);
    te=0;
    html=`<div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Tema" name="tema[0]" required="required">
                </div>`;
    $('.contenedor-de-temas').html(html);
    $('#crear_temas').modal('show');
});

$('body').on('click','.mas-temas',function(){
    te++;
    html=`<div class="col-md-12" style="margin-bottom: 2%;">
            <input type="text" class="form-control" placeholder="Tema" name="tema[`+te+`]" required="required">
        </div>`;
    $('.contenedor-de-temas').prepend(html);
});

$('body').on('click','.guardar_tema',function(){
    if(true === $("#formulario_tema").parsley().validate()){
        swal({
          title: 'Esta seguro?',
          text: "Desea guardar los temas",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Guardar!'
        }).then(function () {
            var jqxhr = $.post( url_basico+"/guardartemas", $("#formulario_tema").serialize())
            .done(function() {
                console.log("Guardado");
            })
            .fail(function(e) {
                console.error(e.msj)
            })
            .always(function(e) {
              console.log(e.msj);
              swal(
                'Guardado!',
                e.msj,
                'success'
              ).then(function () {
                   $('.item-conferen').remove();
                   listar();
                   $('.exis').click();
              });
            });
        })
    }else{
        swal(
            'Error',
            'Debe llenar los campos obligatorios.',
            'error'
          );
    }
});

$('body').on('change','.datos',function(){
    id=$(this).attr('id');
    var res = id.split("_");
    cambio=$(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
            url: url_basico+"/guardardatosconferencia",
            data: {
                _token: CSRF_TOKEN,
                campo: res[0],
                id_conferencia: res[1],
                cambio:cambio
            },
            cache: false,
            type: 'POST',
            success: function(e){

            }
        });
});


$('body').on('change','.duracion_h_m',function(){
    id=$(this).attr('id');
    var res = id.split("_");
    cambio=$(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
            url: url_basico+"/guardarduracionconferencia",
            data: {
                _token: CSRF_TOKEN,
                campo: res[0],
                id_conferencia: res[1],
                cambio:cambio
            },
            cache: false,
            type: 'POST',
            success: function(e){

            }
        });
});

$('body').on('keyup','.titulo',function(){
    id=$(this).attr('id');
    var res = id.split("_");
    cambio=$(this).val();
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
            url: url_basico+"/guardardatosconferencia",
            data: {
                _token: CSRF_TOKEN,
                campo: res[0],
                id_conferencia: res[1],
                cambio:cambio
            },
            cache: false,
            type: 'POST',
            success: function(e){

            }
        });
});

$('body').on('click','.crear_conferencia',function(){
    id=$(this).attr('id');
    id=id.replace('feria_','');
    id=parseInt(id);
    ruta=url_basico+"/crearconferencia/"+id;
    $.get( ruta, function( data ) {
          swal({
              title: 'Creando!',
              text: 'Creando conferencia',
              timer: 10000,
              onOpen: function () {
                swal.showLoading()
              }
            }).then(
              function () {},
              // handling the promise rejection
              function (dismiss) {
                if (dismiss === 'timer') {
                  console.log(e.msj);
                }
              }
            );
         parent.window.location.reload(true);
      });
});

$('body').on('click','#items-presupuesto',function(){
   direc=url_basico+"/presupuestoconferencia";
   window.open(direc, "_blank");
});

/**
 * [[Esta funcion se realiza para editar la ubicacion de la conferencia]]
 * @param {number} id [[Identificador deconferencia]]
 */
function edit_location(id){
    $.ajax({
        url: url_basico+"/consultarubicacionconferencia/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
                  title: 'Editar Ubicación',
                  type: 'info',
                  html:e.content,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:'Guardar',
                  cancelButtonText:'Cancelar',
                }).then(function(){
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                            url: url_basico+"/guardarubicacionconferencia",
                            data: {
                                _token: CSRF_TOKEN,
                                id_conferencia:id,
                                ubicacion: $('#ubicacion_conference').val(),
                                fecha: $('#fecha_conference').val(),
                                hora_inicio: $('#hora_inicio_conference').val(),
                                hora_fin: $('#hora_fin_conference').val(),
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                                swal(
                                    'Guardado!',
                                    e.msj,
                                    'success'
                                  ).then(function () {
                                      $('.item-conferen').remove();
                                      listar();
                                  });
                            }
                        });
            });
            tiempo();
        }
    });
}

/**
 * [[Funcion para eliminar la conferencia seleccionada con todo los datos]]
 * @param {number} id     [[El id de la conferencia]]
 * @param {string} titulo [[Nombre de la conferencia]]
 */
function eliminar_conferencia(id,titulo){
    swal({
          title: 'Esta seguro?',
          text: "Va a eliminar la conferencia "+titulo+"!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!',
          cancelButtonText: 'Cancelar',
        }).then(function(){
            $.ajax({
                url: url_basico+"/eliminarconferencia/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal(
                      'Eliminado!',
                      e.msj,
                      'success'
                    ).then(function(){
                        $('.item-conferen').remove();
                        listar();
                    });
                }
             });
        });
}

    /**
     * Función para crear sweet alert de no tiene permiso
     * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
     */
    function no_permiso(msj){
        swal('Advertencia',msj,'warning');
    }
