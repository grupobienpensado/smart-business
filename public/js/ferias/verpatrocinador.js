$(document).ready(function () {

    $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
        // $(".tab").addClass("active"); // instead of this do the below
        $(this).removeClass("btn-default").addClass("btn-primary");
    });

    list_comments();
});


function list_comments() {
    $.ajax({
        url: "/feria/patrocinador/comentarios-patrocinador",
        cache: false,
        method: "POST",
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id_pat: $("#id_pat").val()
        },
        success: function (suss) {
            if(suss.res){
            $("#caja_comentarios").html(suss.comments);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function do_comments(){
    $.ajax({
        url: "/feria/patrocinador/comentar-patrocinador",
        cache: false,
        method: "POST",
        timeout: 10000,
        beforeSend: function(){
            $("#send_btn").prop('disabled', true);
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {comentario: $("#chat_box").val(), id_pat_: $("#id_pat").val()},
        success: function (suss) {
            if(suss.res){
                $("#send_btn").prop('disabled', false);
                $("#messages_box")[0].reset();
                list_comments();
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
