url_basico = $('#URL').val();
id_feria=$('#id_feria').val();
$(function() {
    $('.dropzone').html5imageupload();
    $('[data-toggle="tooltip"]').tooltip();
    swal({
          title: 'Seleccione un tipo de invitado',
          input: 'select',
          inputOptions: {
            'cliente': 'Cliente',
            'Nuevo': 'Nuevo'
          },
          inputPlaceholder: 'Seleccione un tipo de invitado',
          showCancelButton: false,
          allowOutsideClick: false,
          inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
              if (value === 'cliente') {
                  $.ajax({
                        url: url_basico+"/consultarclientes",
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'GET',
                        success: function(e){
                            swal({
                              title: '<i>Seleccione un cliente</i>',
                              type: 'info',
                              html:e.html,
                              focusConfirm: false,
                              confirmButtonText: '<i class="fa fa-paperclip"></i> Guardar!',
                              showCancelButton: false,
                              allowOutsideClick: false,
                            }).then(function () {
                                valor=$('.cliente').val();
                                motivo=$('#objetivo_invitacion').val();
                                if(valor == "" || motivo == ""){
                                    swal({
                                      title: 'Error...',
                                      html:'Debe seleccionar un cliente y agregar un comentario!',
                                      type: 'error',
                                      allowOutsideClick: false
                                    }).then(function () {
                                        parent.window.location.reload(true);
                                    });
                                }else{
                                    id=$('.cliente').val();
                                    var CSRF_TOKEN = $('input[name=_token]').val();
                                    var jqxhr = $.ajax({
                                        url: url_basico+"/guardarinvitado",
                                        data: {
                                            _token: CSRF_TOKEN,
                                            id: id,
                                            id_feria: id_feria,
                                            motivo: motivo
                                        },
                                        cache: false,
                                        type: 'POST',
                                        success: function(e){
                                            swal(
                                                'Guardado!',
                                                e.msj,
                                                'success'
                                              ).then(function () {
                                                location.href = url_basico+"/invitados/ver/"+id_feria;
                                              });
                                        }
                                    });
                                }
                            })
                        }
                    });

              } else if(value === '') {
                reject('Debe seleccionar un tipo')
              } else if(value === 'Nuevo'){
                  swal(
                    'Información!',
                    'Debe llenar el siguiente formulario',
                    'info'
                  ).then(function () {

                  });
              }
            })
          }
        }).then(function (result) {

        })
});
/*Parte de empresa sweet principal*/
$('body').on('change','.empresa',function(){
    $('#empresa_mostrar').val('');
    $('.empresa').css('display','none');
    $('#empresa_mostrar').css('display','block');
    $('.cambiar-empresa').css('display','block');
    $.ajax({
        url: url_basico+"/consultarempresas/"+$(this).val(),
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#empresa_mostrar').val(e.data['nombre_empresa']);
            $('#clientes').html(e.data['content']);
        }
    });
});

$('body').on('click','.cambiar-empresa',function(){
    $('#clientes').html('');
    $('.empresa').val('');
    $('.empresa').css('display','block');
    $('#empresa_mostrar').css('display','none');
    $('.cambiar-empresa').css('display','none');

    $('.cliente').val('');
    $('.cliente').css('display','block');
    $('#cliente_mostrar').css('display','none');
    $('.cambiar-cliente').css('display','none');
});
/*Fin Parte de empresa sweet principal*/


/*Parte de cliente sweet principal*/
$('body').on('change','.cliente',function(){
    $('#cliente_mostrar').val('');
    $('.cliente').css('display','none');
    $('#cliente_mostrar').css('display','block');
    $('.cambiar-cliente').css('display','block');
    $.ajax({
        url: url_basico+"/consultarclientes2/"+$(this).val(),
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('#cliente_mostrar').val(e.nombre);
        }
    });
});

$('body').on('click','.cambiar-cliente',function(){
    $('.cliente').val('');
    $('.cliente').css('display','block');
    $('#cliente_mostrar').css('display','none');
    $('.cambiar-cliente').css('display','none');
});
/*Fin Parte de cliente sweet principal*/



$('body').on('click','#guardar_invitado',function(){
    if(true === $("#formulario").parsley().validate()){
        swal({
          title: 'Esta seguro?',
          text: "Va a guardar un invitado",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var jqxhr = $.post( url_basico+"/guardarinvitadonuevo", $("#formulario").serialize())
            .done(function() {
                console.log("Guardado");
            })
            .fail(function(e) {
                console.error(e.msj)
            })
            .always(function(e) {
              console.log(e.msj);
                swal(
                    'Guardado!',
                    e.msj,
                    'success'
                  ).then(function () {
                    location.href = url_basico+"/invitados/ver/"+id_feria;
                  });
            });
        });
    }
});
