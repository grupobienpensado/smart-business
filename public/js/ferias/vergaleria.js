$('body').on('click','.no_seleccionado',function(){
    $('.seleccionado').removeClass('seleccionado').addClass('no_seleccionado');
    $(this).addClass('seleccionado').removeClass('no_seleccionado');

     $(".seleccionado .video").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/play_blanco.png');
     $(".seleccionado .foto").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/img_blanco.png');
     $(".seleccionado .carpeta").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_blanca.png');

     $(".no_seleccionado .video").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/Play.png');
     $(".no_seleccionado .foto").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/Img_gris.png');
     $(".no_seleccionado .carpeta").attr('src','http://megaarchivo.smartessi.com/multimedia/explorador/carpeta_gris.png');

    /*esconder y mostrar contenedores*/
    if ( $(".seleccionado .video").length > 0 ) {
      // hacer algo aquí si el elemento existe
        $('.archivos').css('display','none');
        $('#cont-videos').css('display', 'block');
    }

    if ( $(".seleccionado .foto").length > 0 ) {
      // hacer algo aquí si el elemento existe
        $('.archivos').css('display','none');
        $('#cont-imagenes').css('display', 'block');
    }

    if ( $(".seleccionado .carpeta").length > 0 ) {
      // hacer algo aquí si el elemento exist
        $('.archivos').css('display','none');
        $('#cont-archivos').css('display', 'block');
    }
});

function abrir(tipo,nombre){
    url_basico=$('#URL').val();
    $('#documento').modal('show');
    $('#ver_pdf').attr('src',url_basico+'/storage/ferias/multimedia/'+nombre);
}

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
