$(function() {
    var urlList = '/jsoncalendarferias';
    $('#calendar').fullCalendar({
        defaultView: 'month',
        header: {
            center: 'month, listYear' // buttons for switching between views
        },
        defaultDate: moment(),
        locale: 'es',
        allDaySlot: false,
        navLinks: false,
        selectable: false,
        selectHelper: false,
        axisFormat : "HH:mm",
        agenda : "HH:mm",
        weekNumbers: true,
        weekNumbersWithinDays: true,
        columnFormat: 'dddd D',
        editable: false,
        eventLimit: false,
        events: function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(start).format('YYYY-MM-DD'),
		                end: moment(end).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
						callback(doc);
					},
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    },
        eventClick: function(event) {
            var diasemanaActual = moment().day();
            var semana = moment(event.start).week();
            var semanaActual = moment().week();
            model = event.dato;
            console.log(model);
            moment.locale('es');
            var foto = 'http://via.placeholder.com/250x250/fff/948e8e?text=Logo';
            if(model.logo != null && model.logo != ""){
               foto = '/storage/ferias/'+model.logo;
            }
            swal({
              title: 'Ver Feria',
              html: `
            <div class="profile-empresa">
                <img src="`+foto+`" class="img-fluid mx-auto d-block img-empresa">
            </div>
            <div class="row">
              <div class="col-8 col-md-8 centrado">
                <div  class="hijo">
                  <h2 class="titulo">`+model.nombre+`</h2>
                  <p class="capitalize">
                    <i class="fa fa-map-marker"></i>
                    <span class="soloPri">`+model.ubicacion+`</span>
                  </p>
                  <p class="capitalize">
                      <i class="fa fa-hourglass-start"></i> Inicio `+moment(model.fecha_inicio).calendar()+`
                  </p>
                  <p class="capitalize">
                      <i class="fa fa-hourglass-end"></i> Cierre `+moment(model.fecha_fin).calendar()+`
                  </p>
                </div>
              </div>
              <div class="col-4 col-md-4 centrado" style="padding-top: 70px">
                <div class="hijo">
                  <h6>
                  <p class="subtitulo pull-left">
                    <i class="fa fa-star-o pull-left" aria-hidden="true"></i>
                    Creada el <span class="capitalize">`+moment(model.created_at).calendar()+`</span>
                  </p>
                  </h6>
                  <h6 style="padding-top: 20px;">
                  <p class="subtitulo subtitulo2 pull-left">
                    <i class="fa fa-repeat pull-left" aria-hidden="true"></i>
                    Actualizada el <span class="capitalize">`+moment(model.updated_at).calendar()+`</span>
                  </p>
                  </h6>
                </div>
              </div>
              <div class="col-12 col-md-12" style="padding-top: 70px;text-align: justify;">`+model.descripcion+`</div>
            </div>`,
              showCloseButton: true,
              focusConfirm: false,
              confirmButtonText: 'Cerrar'
            });
            return false;
        },
        timeFormat: 'HH:mm:ss',
    });
});
