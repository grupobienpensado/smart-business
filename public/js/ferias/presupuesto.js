url_basico=$('#URL').val();
function format(input){
    var num = input.replace(/\./g,'');
    hallado = num.indexOf("0");
    if(hallado == 0){
        num=num.slice(1);
    }
    if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        return '$ '+num;
    }else{
        return '$ '+0;
    }
}

$('body').on('keyup','.valor',function(){
    v=$(this).val();
    v=v.replace(/\./g,'');
    v=v.replace(/\$/g,'');
    v=parseInt(v);
    v=String(v);
    v=format(v);
    $(this).val(v);
    sumatotal();
    guardar();
});

function sumatotal(){
    total=0;
    $(".valor").each(function(){
        valor=$(this).val();
        valor=valor.replace(/\./g,'');
        valor=valor.replace(/\$/g,'');
        total=total+parseInt(valor);
    });
    total=String(total);
    total=format(total);
    $('#total').html(total);
}

function guardar(){
    var jqxhr = $.post( url_basico+"/guardarferiapresupuesto", $("#formulario_basico").serialize())
    .done(function() {
        console.log("Guardado");
    })
    .fail(function(e) {
        console.error(e.msj)
    })
    .always(function(e) {
      console.log(e.msj)
    });
}

$('body').on('change','.valor',function(){
    $('.alert-success').css('display','block');
    setTimeout(function(){ $('.alert-success').css('display','none'); }, 1500);
});

/**
 * Función para crear sweet alert de no tiene permiso
 * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
 */
function no_permiso(msj){
    swal('Advertencia',msj,'warning');
}
