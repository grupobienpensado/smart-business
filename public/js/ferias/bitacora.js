'use unique';
$(function(){
    moment.locale('es');
    getJsonBitacora();

    $( "body" ).on( "change", "#selectTipo", function() {
                var tipo = $(this).val();
                if(tipo === "visitante"){
                    $('.viewVisitante').show();
                    $('.requerido').attr('required',true);
                }else{
                    $('.viewVisitante').hide();
                    $('.requerido').attr('required',false);
                }
            });

    $( "body" ).on( "change", "#filpais", function() {
        var pais = $(this).val();
        $.get( "/estados?state="+pais, function( data ) {
            html = "";
            $.each(data, function( index, value ) {
                html += `<option value="`+value.text+`">`;
            });
            $('#departamento').html(html);
        });
    });

    $( "body" ).on( "change", "#fildepartamento", function() {
        var pais = $("#filpais").val();
        var estado = $(this).val();
        $.get( "/ciudades?estado="+estado+"&pais="+pais, function( data ) {
            html = "";
            $.each(data, function( index, value ) {
                html += `<option value="`+value.text+`">`;
            });
            $('#ciudad').html(html);
        });
    });

     var i=1;
    $( "body" ).on( "click", ".agregarProductos", function() {
                i++;
                dato = `
                <div class="row animated rollIn viewVisitante" id="listAddProductos`+i+`">
                    <div class="col-6">
                        <input list="listpProducts" id="selectProducto`+i+`" class="swal2-input productos`+i+`" name="productos[`+i+`][producto]" onchange="buscarDimSelec2(`+i+`)" placeholder="Producto">
                    </div>
                    <div class="col-6">
                        <div class="input-group">
                            <select id="referencia`+i+`" name="productos[`+i+`][referencia]" class="swal2-input"></select>
                            <span class="input-group-btn">
                                <button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarProductos('#listAddProductos`+i+`')"><i class="fa fa-minus text-danger"></i></button>
                            </span>
                        </div>
                    </div>
                </div>`;
                $( "#listProductos" ).prepend(dato);
            });
});

getJsonBitacora = function() {
    $.get( "/feria/jsonbitacora/"+$("#feria_id").val(), function( data ) {
        html = "";
        console.log(data);
        recorre=0;
        $.each(data.model, function( index, value ) {
            if(value.nombre){
                html += `<li class="timeline__item" onclick="openViewBitacora(`+value.id+`)">`;
            }else{
                html += `<li class="timeline__item" style="cursor: unset !important;">`
            }
            html += `
                  <div class="timeline__step">
                    <div class="timeline__step__marker timeline__step__marker--red"></div>
                  </div>
                  <div class="timeline__time text-left" style="width: 220px;">
                    `+data.fecha[recorre]+`
                  </div>
                  <div class="timeline__content" style="width: 100%;">
                    <div class="timeline__title">`;
                    if(value.nombre){
                        html += value.nombre+' - '+value.empresa;
                    }else{
                        html += 'Comentario general:'
                    }
                  html +=`</div>
                    <ul class="timeline__points">
                      <li>`+value.comentario+`</li>
                    </ul>
                    <span class="pull-left">-- `+value.nombre_user+`</span>
                  </div>
                </li>
            `;
            recorre++;
        });
        $('.timeline').html(html);
   });
}

openViewBitacora = function(id) {
    console.log('entra con id -> ',id);

    var jqxhr = $.get( "/jsonviewbitacora/"+id)
    .always(function(model) {
        if(model.success){
            var feria = model.feria;
            var jsonProductos = model.productos;
            model = model.model;
            console.log(model);
            moment.locale('es');
            htmlProductos = '';
             $.each(jsonProductos, function (k, item) {
                 if(item.foto){
                    foto = "/images/file/productos/"+item.foto;
                }else{
                    foto = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Foto&w=100&h=100';
                }
                htmlProductos += `
                <div class="col-3 placeholder img-sedes">
                    <div class="img-thumbnail img-sedes">
                      <a href="">
                        <img src="`+foto+`" style="max-width: 50%;" class="img-fluid mx-auto d-block">
                      </a>
                    </div>
                    <div class="text-muted centrado">
                        <h6 style="margin-bottom: 0px;text-align: center;">`+item.producto+` `+item.referencia+`</h6>
                    </div>
                </div>`;
            });
            var foto = 'http://via.placeholder.com/250x250/fff/948e8e?text=Logo';
            if(feria.logo != null && feria.logo != ""){
               foto = '/storage/ferias/'+feria.logo;
            }
            html =`
                <div class="profile-empresa">
                    <img src="`+foto+`" class="img-fluid mx-auto d-block img-empresa">
                </div>
                <div class="row">
              <div class="col-8 col-md-8 centrado">
                <div  class="hijo" style="padding-left: 70px;">
                  <h2 class="titulo">`+model.empresa+`</h2>`;
            if(model.telefono){
               html += `<p class="capitalize">
                            </i><i class="fa fa-phone" aria-hidden="true"></i>
                            <span class="soloPri">`+model.telefono+`</span>
                        </p>`;
            }

            if(model.correo){
               html += `<p class="capitalize">
                            </i><i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <span class="soloPri">`+model.correo+`</span>
                        </p>`;
            }

            if(model.ciudad){
               html += `<p class="capitalize">
                            <i class="fa fa-map-marker"></i>
                            <span class="soloPri">`+model.ciudad+`, `+model.pais+`</span>
                        </p>`;
            }else{
                html += `<p class="capitalize">
                            <i class="fa fa-map-marker"></i>
                            <span class="soloPri">`+model.pais+`</span>
                        </p>`;
            }

            if(model.direccion){
               html += `<p class="capitalize">
                            <span class="soloPri">`+model.direccion+`</span>
                         </p>`;
            }


                  html += `
                  <p class="capitalize">
                      <i class="fa fa-hourglass-start"></i> Inicio `+moment(model.fecha+' '+model.hora).calendar()+`
                  </p>
                </div>
              </div>
              <div class="col-4 col-md-4 centrado" style="padding-top: 70px">
                <div class="hijo">
                  <h6 style="padding-top: 20px;">
                  <p class="subtitulo subtitulo2 pull-left">
                    <i class="fa fa-user pull-left" aria-hidden="true"></i>
                    <span>`+model.nombre_user+`</span>
                  </p>
                  </h6>
                  <h6>
                  <p class="subtitulo pull-left">
                    <i class="fa fa-star-o pull-left" aria-hidden="true"></i>
                    Creada el <span class="capitalize">`+moment(model.created_at).calendar()+`</span>
                  </p>
                  </h6>
                </div>
              </div>
              <div class="col-12"><h2 class="titulo"> Comentario general:</h2></div>
              <div class="col-12 col-md-12" style="text-align: justify;">`+model.comentario+`</div>`;
            if(htmlProductos !== ''){
               html += `<div class="col-12"><h2 class="titulo"> Equipos de interes:</h2></div>`+htmlProductos;
            }
            html += `</div>`;

            swal({
              title: model.nombre+', '+model.cargo,
              html: html,
              showCloseButton: true,
              focusConfirm: false,
              confirmButtonText: 'Cerrar',
              animation: "slide-from-top",
              showLoaderOnConfirm: true
            });
            setTimeout('cambiar_tamano()',2000);
        }else{
            swal(
              'Vaya...',
              '¡Algo salió mal!',
              'error'
            );
        }
    });

}

function cambiar_tamano(){
    $('.swal2-show').attr('width','90% !important');
}

/**
 * [[Elección de ingreso de información Visitante o comentario General]]
 * @param {number} id [[id de la feria]]
 */
newBitacora = function(id){
    swal({
          title: 'Elegir Opción',
          type: 'info',
          html:`<div class="row"><div class="col-md-2"></div><div class="col-md-8 botones-activacion"><a href="#" class="icon-button twitter2" title="Visitante" onclick="newBitacora2(`+id+`,'visitante')"><i class="fa fa-male"></i><span></span></a>
            <a href="#" class="icon-button facebook2" title="Comentario General" onclick="newBitacora2(`+id+`,'comentario')"><i class="fa fa-comments" aria-hidden="true"></i><span></span></a>
            </div><div class="col-md-2"></div></div>`,
          showCloseButton: false,
          showCancelButton: false,
          showConfirmButton: false,
          focusConfirm: false
        });
}

/**
 * [[Función para cambiar el formulario con la opción elegida]]
 * @param {string} tipo [[tipo de eleccion]]
 */
function selecttype(tipo){
    if(tipo === "visitante"){
        $('.viewVisitante').show();
        $('.requerido').attr('required',true);
    }else{
        $('.viewVisitante').hide();
        $('.requerido').attr('required',false);
    }
}

newBitacora2 = function (id,tipo) {
    if(tipo == "visitante"){
        options=`<option value="visitante" selected>Visitante</option>
                    <option value="otra">Comentario general</option>`;
    }else{
        options=`<option value="visitante">Visitante</option>
                    <option value="otra" selected>Comentario general</option>`;
    }
    swal({
        title: 'Crear bitacora',
        animation: "slide-from-top",
        showCancelButton: true,
        confirmButtonText: 'Guardar',
        cancelButtonText: 'Cerrar',
        showLoaderOnConfirm: true,
        html:`
            <form id="formGuardar" class="row">
              <div class="form-group col-md-6">
                <label><b>Fecha</b></label>
                <input type="text" class="swal2-input date" name="fecha" value="`+moment().format('YYYY-MM-DD')+`" required>
              </div>
              <div class="form-group col-md-6">
                <label><b>Hora</b></label>
                <input type="text" class="form-control swal2-input time" name="hora" value="`+moment().format('H:mm')+`" required>
              </div>
              <div class="form-group col-md-6">
                <label><b>Tipo</b></label>
                <select class="swal2-input" name="tipo" id="selectTipo" required>
                    `+options+`
                </select>
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Nombre</b></label>
                <input type="text" class="swal2-input requerido" name="nombre" placeholder="Nombre del cliente" required>
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Empresa</b></label>
                <input type="text" class="swal2-input requerido" name="empresa" placeholder="Nombre de la empresa" required>
              </div>
             <div class="form-group col-md-6 viewVisitante">
                <label><b>Cargo</b></label>
                <input type="text" class="swal2-input requerido" name="cargo" placeholder="Cargo del cliente" required>
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Telefono</b></label><br>
                <input type="number" class="swal2-input" style="max-width: 100% !important;" name="telefono" placeholder="Telefono del cliente">
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Correo</b></label>
                <input type="email" class="swal2-input" name="correo" placeholder="Correo del cliente">
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Pais</b></label>
                <input list="pais" class="requerido swal2-input" name="pais" id="filpais" placeholder="Pais de residencia" required>
                <datalist id="pais">
                    <option value="Colombia">
                </datalist>
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Departamento</b></label>
                <input list="departamento" class="swal2-input" name="departamento" id="fildepartamento" placeholder="Departamento de residencia">
                <datalist id="departamento">
                    <option value="Cesar">
                </datalist>
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Ciudad</b></label>
                <input list="ciudad" class="swal2-input" name="ciudad" id="filciudad" placeholder="Ciudad de residencia">
                <datalist id="ciudad"></datalist>
              </div>
              <div class="form-group col-md-6 viewVisitante">
                <label><b>Direccion</b></label>
                <input type="text" class="swal2-input" name="direccion" placeholder="Direccion del cliente">
              </div>

                <div class="col-6 viewVisitante">
                    <label><b>Producto/s de interes</b></label>
                </div>
                <div class="col-6 viewVisitante">
                    <label><b>Referencia</b></label>
                </div>
                <datalist id="listpProducts"></datalist>
                <div class="col-md-12 viewVisitante" id="listProductos">
                    <div class="row">
                        <div class="col-6">
                            <input list="listpProducts" id="selectProducto0" class="swal2-input productos0" name="productos[0][producto]" onchange="buscarDimSelec2(0)" placeholder="Producto">
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <select id="referencia0" name="productos[0][referencia]" class="swal2-input referencia0"></select>
                                <span class="input-group-btn">
                                    <button class="btn btn-outline-success pull-right btn-sm agregarProductos" type="button"><i class="fa fa-plus text-success"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

              <div class="form-group col-md-12">
                <label><b>Comentario general</b></label>
                <textarea class="form-control swal2-input" name="comentario" rows="6" required placeholder="Por favor ingrese un comentario general" style="margin-top: 20px; margin-bottom: 20px; height: 185px;"></textarea>
              </div>

            </form>
        `,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                if($("form").parsley().validate()){

                    var fd = new FormData(document.getElementById("formGuardar"));
                    fd.append("feria_id", id);
                    var request = $.ajax({
                      url: "/feria/bitacora/crear",
                      type: "POST",
                      data: fd,
                      processData: false,
                      contentType: false
                    });
                    request.done(function( msg ) {
                        resolve(msg)
                    });

                    request.fail(function( jqXHR, textStatus ) {
                        resolve(false)
                    });
                }else{
                    reject('Perdón!, Es necesario que complete los campos requeridos.');
                }
            });
        },
        onOpen: function () {
            var ref = $.get( "/sproducto" );
            ref.done(function( data ) {
                $.each(data, function (k, item) {
                    $('#listpProducts').append('<option label="'+item.text+'" value="'+item.text+'">');
                });
            });

            $.get( "/pais", function( data ) {
                html = "";
                $.each(data, function( index, value ) {
                    html += `<option value="`+value.text+`">`;
                });
                $('#pais').html(html);
            });

            buscarDimSelec2 = function (i) {
                console.log("entro aqui"+i);
                var ref = $.get( "/referencia?term=&dato="+$("#selectProducto"+i).val());
                ref.done(function( data ) {
                    console.log(data);
                    $('#referencia'+i).html(" ");
                    $.each(data, function (k, item) {
                        $('#referencia'+i).append($('<option>', {
                            value: item.text,
                            text : item.text
                        }));
                    });
                });
            }

            quitarProductos = function (id) {
                $(id).removeClass('rollIn').addClass('hinge');
                setTimeout(function(){
                    $(id).remove();
                }, 1000);
            }
        }
    }).then(function (result) {
        if (result.success) {
            getJsonBitacora();
            swal("Exito!","Actividad guardado con exito.","success");
        }else{
            swal("Error!","Algo ha salido mal.","warning");
        }
    }).catch(swal.noop)
    selecttype(tipo);
    $('.swal2-modal .swal2-show').attr('width','90% !important');

    $('.time').bootstrapMaterialDatePicker({
      time: true,
      date: false,
      clearButton: true,
      nowButton: true,
      setDate: moment(),
      lang: 'es',
      format : 'HH:mm',
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar',
      weekStart : 0
    });

    $('.date').bootstrapMaterialDatePicker({
      time: false,
      clearButton: true,
      nowButton: true,
      setDate: moment(),
      lang: 'es',
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar'
    })
};

 /**
 * Función para crear sweet alert de no tiene permiso
 * @param STRING     msj    MENSAJE QUE LLEGA PARA PONER EN EL SWEET ALERT (Por ejemplo: No tiene permisos para editar)
 */
function no_permiso(msj){
    swal('Advertencia',msj,'warning');
}
