var CSRF_TOKEN = $('input[name=_token]').val();
$(document).ready(function(){
    accion_menu(0);
    setTimeout(function(){ accion_menu(0); }, 2000);
});

/**
 * Función para mostrar u ocultar la barra de carga cada vez que se realiza una petición por ajax
 * @param INT hacer INDICADOR QUE HACER (0=MOSTRAR 1=OCULTRAR)
 */
function cargando(hacer){
    if(hacer==0){
        $('.loader-cylon').css('display','block');
    }else{
        $('.loader-cylon').css('display','none');
    }
}

function iniciar_elementos(){
    $('[data-toggle="tooltip"]').tooltip();
    /* Activar calendario */
    $('#fecha_inicio').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        nowButton: true,
        lang: 'es',
        maxDate : new Date(),
        cancelText : 'Cancelar',
        okText: 'Aceptar',
        nowText: 'Nuevo',
        clearText: 'Limpiar'
    }).on('close', function(e){
        $('#fecha_fin').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            nowButton: true,
            lang: 'es',
            minDate : $("#fecha_inicio").val(),
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar'
        })
    });
}

/**
 * Función para crear el formlario que va a cargar el archivo ya se PDF ó IMAGEN
 * @param INT tipo TIPO DE ARCHIVO 0=PDF 1=IMAGEN
 */
function cargar_archivo(tipo){
    var archivo = 'Imagen';
    var accept = 'image/jpg,image/png,image/jpeg';
    var icon = 'fa fa-image';
    if(tipo == 0){
        archivo = 'PDF';
        accept = 'application/pdf';
        icon = 'fa fa-file-pdf-o';
    }
    swal({
          title: 'Cargar Archivo '+archivo,
          html:`<div class="container"> <div class="row"> <div class="col-md-12"> <form id="action_frm" enctype="multipart/form-data"><input type="hidden" name="_token" value="`+CSRF_TOKEN+`"><input type="hidden" name="id_freelance_user" value="`+$('#id_user').val()+`"><input type="hidden" name="tipo" value="`+tipo+`"><div class="form-group"><label>Nombre del archivo</label><input type="text" class="form-control" name="nombre" required></div><div class="form-group"> <input type="hidden" name="action" value="1"> <input type="file" name="archivo_att" id="attached_file" class="file" accept="`+accept+`" required> <div class="input-group col-xs-12"> <span class="input-group-addon"><i class="`+icon+`"></i></span> <input type="text" class="form-control input-lg" disabled placeholder="Cargar `+archivo+`"> <span class="input-group-btn"> <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Buscar</button> </span> </div></div></form> </div><div class="col-md-12"> <div class="progress" id="upload_bar"> <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div></div><div class="col-md-12"> <a class="text-success" onclick="save_att()" style="font-size: 2rem;cursor:pointer;"><i class="fa fa-save"></i> Guardar</a></div></div></div>`,
          showCloseButton: true,
          showConfirmButton: false
        });
    $('.swal2-modal').css('width','70%');
}

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

/**
 * Función para guardar el archivo adjunto del usuario freelance ya sea un PDF ó Imagen
 */
function save_att(){
    event.preventDefault();
    if(true === $("#action_frm").parsley().validate()){
        var datos = new FormData($("#action_frm")[0]);
        if ($("#attached_file").val()) {
            $("#upload_bar").show();
        }
        $.ajax({
            url: '/freelance/ver-accion',
            method: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            data: datos,
            success: function(suss) {
                $("#upload_bar").hide();
                $('#action_modal').modal('hide');
                accion_menu(0);
                swal('Guardado','Guardado correctamente!','success');
            },
            error: function(err) {
                console.log(err);
            },
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                //Upload Progress
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                        $('#upload_bar > .progress-bar').css({
                            "width": percentComplete + "%"
                        });
                    }
                }, false);
                //Upload progress
                xhr.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            $("#upload_bar > .progress-bar").css({
                                "width": percentComplete + "%"
                            });
                        }
                    },
                    false);
                return xhr;
            }
        });
    }
}

/**
 * Función para abrir el modal
 * @param STRING ruta DIRECCIÓN A BUSCAR
 */
function abrir(ruta){
    url_basico=$('#URL').val();
    $('#documento').modal('show');
    $('#ver_pdf').attr('src',url_basico+ruta);
}

/*Capturar el click para activar el item del menu*/
$(document).on('click','.menu',function(){
    $(".active").each(function(){
        $(this).removeClass('active');
    });
    $(this).addClass('active');
});

/**
 * Función para mostrar el modal con los usuarios que tienen oportunidades y asi poder seleccionar el usuario para que despliegue las oportunidades
 * @param INT tipo ES EL TIPO DE OPORTUNIDAD QUE TIENE A LA QUE SE LE VA AÑADIR (0=VIGENTE 1=VENDIDA 2=PERDIDA)
 */
function anadir_oportunidad(tipo){
    cargando(0);
    $('#anadir_oportunidad .modal-content').html('');
    var jqxhr = $.ajax({
        url: "/freelance/ver-accion",
        data: {
            _token: CSRF_TOKEN,
            action: 3,
            tipo:tipo
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#anadir_oportunidad .modal-content').html(e.data['html']);
            cargando(1);
            $('#anadir_oportunidad').modal('show');
            reiniciar_oportunidades();
        }
    });
}

/*Funcion para capturar el click en la foto del usuario para agregar una oportunidad para modificar la clase de la imagen*/
$(document).on('click','.user-no',function(){
   $(".user-si").each(function(){
        $(this).removeClass('user-si');
       $(this).addClass('user-no');
    });
    $(this).removeClass('user-no');
    $(this).addClass('user-si');
});

/**
 * Función para buscar las oportunidades que pertenecen al usuario
 * @param INT id   IDENTIFICACIÓN DEL USUARIO
 * @param INT tipo TIPO DE OPORTUNIDAD (0=VIGENTE, 1=VENDIDA, 2=PERDIDA)
 */
function oportunidadesuser(id,tipo){
    reiniciar_oportunidades();
    var jqxhr = $.ajax({
        url: "/freelance/ver-accion",
        data: {
            _token: CSRF_TOKEN,
            action: 4,
            id_user:id,
            tipo:tipo
        },
        cache: false,
        type: 'POST',
        success: function(e){
           $('#anadir_oportunidad .modal-content .list').html(e.data['html']);
            margen_modal();
        }
    });
}

/**
 * Función para darle un margen-top a el modal anadir_oportunidad
 */
function margen_modal(){
    altura = $('#anadir_oportunidad .modal-content').height();
    margen = altura/4;
    $('#anadir_oportunidad .modal-content').css('margin-top',margen+'px');
}

/**
 * Función para seleccionar una oportunidad
 * @param INT id oportunidad
 */
var oportunidades;
function oportunidad_select(id){
    if(oportunidades[id] == 'seleccionado'){
        oportunidades[id] = '';
    }else{
        oportunidades[id] = 'seleccionado';
    }
}

/**
 * Función para reiniciar la variable oportunidades
 */
function reiniciar_oportunidades(){
    $.each(oportunidades, function(indice, valor) {
        oportunidades[indice] = '';
    });
}

/**
 * Funcion para guardar las oportunidades seleccionadas
 * @param INT id IDENTIFICACION DEL USUARIO
 */
function guardar_oportunidad(id){
    var numero_select=0;
   $.each(oportunidades, function(indice, valor) {
        if(valor != ""){
            numero_select++;
        }
    });
    if(numero_select==0){
        swal(
              'Error',
              'Debe seleccionar una oportunidad',
              'error'
            )
    }else{
        swal({
          title: 'Esta seguro?',
          text: "Desea guardar las oportunidades para el usuario Freelance",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Guardar!',
          cancelButtonText: 'No'
        }).then(function(){
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 5,
                    id:$('#id_user').val(),
                    oportunidades:oportunidades
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == 'Guardado satisfactoriamente!'){
                        swal(
                          'Guardado',
                          e.data['msj'],
                          'success'
                        ).then(function(){
                            accion_menu(1);
                            $('#anadir_oportunidad').modal('hide');
                        });
                    }else{
                       swal(
                              'Error',
                              e.data['msj'],
                              'error'
                            )
                    }
                }
            });
        });
    }
}

/**
 * Función para eliminar una oportunidad del listado del Usuario Freelance
 * @param INT id IDENTIFICACIÓN DE LA OPORTUNIDAD
 */
function eliminaroportunidad(id){
    swal({
      title: 'Esta seguro?',
          text: "Desea eliminar la oportunidad del listado del freelance",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!',
          cancelButtonText: 'No'
    }).then(function(){
        var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 6,
                    id:id
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == 'Eliminado satisfactoriamente!'){
                        swal(
                          'Eliminado',
                          e.data['msj'],
                          'success'
                        ).then(function(){
                            accion_menu(1);
                        });
                    }else{
                       swal(
                          'Error',
                          e.data['msj'],
                          'error'
                        );
                    }
                }
            })
    });
}

/**
 * Función para comentar el perfil del usuario Freelance
 */
function comentar(){
        comentario = $('#chat_box').val();
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/freelance/ver-accion",
            data: {
                _token: CSRF_TOKEN,
                action: 7,
                comentario: comentario,
                id_user_freelance: $('#id_user').val()
            },
            cache: false,
            type: 'POST',
            success: function(e){
                console.log(e.data['msj']);
                accion_menu(0);
                $('#chat_box').val('');
            }
        });
    }

/**
 * Función para traer información del menú
 * @param INT accion IDENTIFICACIÓN DEL ITEM DEL MENU
 */
function accion_menu(accion){
    switch(accion){
        /*General*/
        case 0:
            cargando(0);
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 0,
                    id:$('#id_user').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#contenido').html(e.data['html']);
                    iniciar_elementos();
                    cargando(1);
                }
            });
            break;
        /*Listado de oportunidades*/
        case 1:
            cargando(0);
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 2,
                    id:$('#id_user').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#contenido').html(e.data['html']);
                    iniciar_elementos();
                    cargando(1);
                }
            });
            break;
        /*Flujo de caja*/
        case 2:
            cargando(0);
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 8,
                    id:$('#id_user').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#contenido').html(e.data['html']);
                    iniciar_elementos();
                    cargando(1);
                }
            });
            break;
        /*Presupuesto*/
        case 3:
            cargando(0);
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 9,
                    id:$('#id_user').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#contenido').html(e.data['html']);
                    $('#total_presupuesto').html(e.data['valor_total']);
                    iniciar_elementos();
                    cargando(1);
                    calcular_suma();
                }
            });
            break;
    }
}

function filtrar_flujo(){
    fecha_inicio = $('#fecha_inicio').val();
    fecha_fin = $('#fecha_fin').val();
    if(fecha_inicio == '' || fecha_fin == ''){
       swal(
              'Alerta',
              'Debe ingresar una fecha de inicio y una fecha fin!',
              'warning'
            );
       }else{
           cargando(0);
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 8,
                    fecha_inicio:fecha_inicio,
                    fecha_fin:fecha_fin,
                    id:$('#id_user').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#contenido').html(e.data['html']);
                    iniciar_elementos();
                    cargando(1);
                }
            });
       }
}

/**
 * Función para dar formato a la moneda
 */
function formato_numero(valor,id){
    valor = replaceAll(valor, "$ ", "" );
    if(valor != ""){
       valor = replaceAll(valor, ",", "" );
       valor = parseFloat(valor);

       if(isNaN(valor)){
            $('#'+id).val('$ 0');
           total_presupuesto();
        }else{
            valor = number_format(valor,0);
            $('#'+id).val("$ "+valor);
            total_presupuesto();
        }
    }else{
       $('#'+id).val('$ 0');
        total_presupuesto();
    }
}
function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

/**
 * Funcion para calcular el total del presupuesto para el año actual
 */
function total_presupuesto(){
    var valor_total=0;
    $(".presupuesto-valor").each(function(){
        valor=$(this).val();
        if(valor != ""){
            valor = replaceAll(valor, "$ ", "" );
            if(valor != ""){
               valor = replaceAll(valor, ",", "" );
               valor = parseFloat(valor);
                if(isNaN(valor)){
                    valor_total+=0;
                }else{
                    valor_total+=valor;
                }
            }else{
                valor_total+=0;
            }
        }else{
            valor_total+=0;
        }
    });
    valor_total = number_format(valor_total,0);
    $('#total_presupuesto').html("$ "+valor_total);
}

/**
 * Funcion para guardar el presupuesto del usuario Freelance
 */
 /*function guardar_presupuesto(){
    swal({
      title: "Seguro?",
      text: "Desea guardar el presupuesto",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si, guardar",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false
    }).then(function(){
        var jqxhr = $.post("/freelance/ver-accion", $("#formulario_presupuesto").serialize())
        .done(function(e) {
            if(e.data['msj'] == "Guardado correctamente!"){
                swal("Guardado", e.data['msj'], "success");
            }else{
                swal("Error", e.data['msj'], "error");
            }
        })
        .fail(function(e) {
            console.error(e)
        })
        .always(function(e) {
          console.log(e)
        });
    });
}*/
var valor_mes = ["$ 0","$ 0","$ 0","$ 0","$ 0","$ 0","$ 0","$ 0","$ 0","$ 0","$ 0","$ 0"];
  function guardar_presupuesto(){
      var jqxhr = $.ajax({
        url: "/freelance/ver-accion",
        data: {
            _token: CSRF_TOKEN,
            action: 10,
            concepto: $('#concepto').val(),
            id_user:$('#id_user').val(),
            valor_mes: valor_mes
        },
        cache: false,
        type: 'POST',
        success: function(e){
            if(e.data['msj']=="Guardado correctamente!"){
               swal('Guardado',e.data['msj'],'success');
            }else{
                swal('Advertencia',e.data['msj'],'warning');
            }
            accion_menu(3);
        }
    });
  }

/**
 * Función para administrar todo lo relacionado de presupuesto
 * @param STRING accion ACCIÓN A REALIZAR ('crear','eliminar','guardar')
 */

function presupuesto(accion){
    switch(accion){
        case 'crear':
            var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 11
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    $('#lista_conceptos').html(e.data['html']);
                }
            });
            swal({
                  title: 'Crear Concepto',
                  type: 'info',
                  html:$('#formulario_presupuesto').html(),
                  showCloseButton: true,
                  showCancelButton: false,
                  focusConfirm: false,
                  confirmButtonColor: '#d33',
                  confirmButtonText:'<a class="text-white"><i class="fa fa-close"></i></a> Cerrar'
                })
            break;
        case 'guardar':
            var validacion = '';
            var concepto = $('#concepto').val();
            if(concepto==""){
               validacion+= $('#concepto').attr('data-nombre');
            }
            for(var i=0;i<12;i++){
                var numero = i+1;
                valor_mes[i] = $('#mes_'+numero).val();
                if(valor_mes[i] == ''){
                    validacion+=validacion.length>0?', '+$('#mes'+numero).attr('data-nombre'):$('#mes'+numero).attr('data-nombre');
                }
            }
            if(validacion==''){
               var template = $("#tem_presupuesto").html();
               var $template = $(template);
               $template.find("[data-concepto]").html(concepto);
               for(var i=0;i<12;i++){
                   valor_mes[i] = valorsinformato(valor_mes[i]);
               }
                console.log(valor_mes);
               $("#encabezado_presupuesto").after($template);
                calcular_suma();
                guardar_presupuesto();
            }else{
                swal('Error','Campos sin completar. '+validacion,'error');
            }
            break;
    }
}

function editar_presupuesto(id){
    var jqxhr = $.ajax({
        url: "/freelance/ver-accion",
        data: {
            _token: CSRF_TOKEN,
            action: 11
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#lista_conceptos').html(e.data['html']);
        }
    });
    swal({
          title: 'Editar Concepto',
          type: 'info',
          html:$('#formulario_editar_presupuesto'+id).html(),
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonColor: '#d33',
          confirmButtonText:'<a class="text-white"><i class="fa fa-close"></i></a> Cerrar'
        })
}
function editar_presupuesto2(id){
    for(var i=0;i<12;i++){
           valor_mes[i] = $('#mes_'+(i+1)+'_'+id).val();
           valor_mes[i] = valorsinformato(valor_mes[i]);
       }
    var jqxhr = $.ajax({
        url: "/freelance/ver-accion",
        data: {
            _token: CSRF_TOKEN,
            action: 12,
            concepto: $('#concepto'+id).val(),
            id_user:$('#id_user').val(),
            valor_mes: valor_mes
        },
        cache: false,
        type: 'POST',
        success: function(e){
            swal('Guardado',e.data['msj'],'success');
            accion_menu(3);
        }
    });
}

function eliminar_presupuesto(id){
     swal({
      title: 'Esta seguro?',
          text: "Desea eliminar el item del presupuesto",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!',
          cancelButtonText: 'No'
    }).then(function(){
        var jqxhr = $.ajax({
                url: "/freelance/ver-accion",
                data: {
                    _token: CSRF_TOKEN,
                    action: 13,
                    id_concepto:id,
                    id_user:$('#id_user').val()
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    if(e.data['msj'] == 'Eliminado satisfactoriamente!'){
                        swal(
                          'Eliminado',
                          e.data['msj'],
                          'success'
                        )
                    }else{
                       swal(
                          'Error',
                          e.data['msj'],
                          'error'
                        );
                    }
                     accion_menu(3);
                }
            })
    });
}

function valorsinformato(valor){
    valor = replaceAll(valor, "$ ", "" );
    valor = replaceAll(valor, ",", "" );
    valor = parseFloat(valor);
    if(isNaN(valor)){
        return 0;
    }else{
        return valor;
    }
}

function calcular_suma(){
    var valortotal = 0;
    $(".valor_presupuesto").each(function(){
        valortotal+=valorsinformato($(this).html());
    });
    valortotal = number_format(valortotal,0);
    $('#total_presupuesto').html("$ "+valortotal);
}
