function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}
cargo=$('#cargo_user').val();
$('body').on('click','.fc-time-grid-event',function(){
    setTimeout(function(){
        if(cargo == "Administrador Sistema"){
           t_m_h=$('.total_mis_horas').html();
           t_m_h=t_m_h.replace('$ ','');
           t_m_h=t_m_h.replace(/,/g,'');
           if(t_m_h==""){ t_m_h=0; }
           t_h_h=$('#total_h_hombre').html();
           t_h_h=t_h_h.replace('$ ','');
           t_h_h=t_h_h.replace(/,/g,'');
           if(t_h_h==""){ t_h_h=0; }
           tt=$('#total').val();
           tt=tt.replace('$ ','');
           tt=tt.replace(/,/g,'');
           if(tt==""){ tt=0; }
           total=parseInt(t_m_h)+parseInt(t_h_h)+parseInt(tt);
           $('.total_mis_horas').html('$ '+number_format(parseInt(t_m_h)));
           $('#total_general').val('$ '+number_format(parseInt(total)));
        }
    }, 1500);
});

