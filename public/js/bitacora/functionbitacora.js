function funTraerDatos() {
	if ($("#select_traer_datos").val() === "") {
			$("#contenedorOculto").hide();
		}else{
			$("#contenedorOculto").show();
			if ($("#select_traer_datos").val() === "P") {
				$("#divAgendaVisita").hide();
		$("#divPlanTrabajo").show();
		$("#PlanTrabajo").attr("required", true);
		$("#agendaVisita").attr("required", false);
		$("#PlanTrabajo").attr("name", "id_tipo");
		$("#agendaVisita").attr("name", "");

			}else if($("#select_traer_datos").val() === "G"){
				$("#divPlanTrabajo").hide();
		$("#divAgendaVisita").show();
		$("#PlanTrabajo").attr("required", false);
		$("#agendaVisita").attr("required", true);
		$("#PlanTrabajo").attr("name", "");
		$("#agendaVisita").attr("name", "id_tipo");
			}else{
				$("#PlanTrabajo").attr("required", false);
		$("#agendaVisita").attr("required", false);
		$("#PlanTrabajo").attr("name", "");
		$("#agendaVisita").attr("name", "");
				$("#divAgendaVisita").hide();
		$("#divPlanTrabajo").hide();
			}
		}
		$("#oportunidad").val("");
			$("#select_tipo_actividad").val("");
			$("#detalle_actividad").val("");
			$("#PlanTrabajo").val("");
			$("#agendaVisita").val("");
			$(".opprohtml").html("");
}

function funSaveCrearbitacora(start, end, resolve, reject) {
	if($("form").parsley().validate()){
		actividad = $("#detalle_actividad").val();
		resultado = $("#detalle_resultado").val();
		if (actividad.length >= 50 && resultado.length >= 50) {
			var element = $("#formAgendaSmart").parent().parent().parent();
			comparadorStart = moment(start).format('DD');
			comparadorEnd = moment(end).format('DD');
			if (comparadorStart < comparadorEnd) {
				var fd = new FormData(document.getElementById("formAgendaSmart"));
				fd.append("fecha_actividad", moment(start).format('YYYY-MM-DD'));
				fd.append("tiempo_inicio", moment(start).format('H:mm'));
				fd.append("tiempo_fin", moment(end).format('23:59'));
				if (moment(start).format('H')>12) {
					fd.append("jornada", "T");
				}else{
					fd.append("jornada", "M");
				}
				var request = $.ajax({
					url: "/bitacora",
					type: "POST",
					data: fd,
					processData: false,
					contentType: false
				});
				request.done(function( msg ) {
						resolve(msg)
				});
				request.fail(function( jqXHR, textStatus ) {
					resolve(false)
				});
			}else{
				var fd = new FormData(document.getElementById("formAgendaSmart"));
				fd.append("fecha_actividad", moment(start).format('YYYY-MM-DD'));
				fd.append("tiempo_inicio", moment(start).format('H:mm'));
				fd.append("tiempo_fin", moment(end).format('H:mm'));
				if (moment(start).format('H')>12) {
					fd.append("jornada", "T");
				}else{
					fd.append("jornada", "M");
				}
				var request = $.ajax({
					url: "/bitacora",
					type: "POST",
					data: fd,
					processData: false,
					contentType: false
				});
				request.done(function( msg ) {
						resolve(msg)
				});
				request.fail(function( jqXHR, textStatus ) {
					resolve(false)
				});
			}
		}else{
			var element = $("#formAgendaSmart").parent().parent().parent();
			$(element).addClass('animated rubberBand');
			$(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(element).removeClass('animated rubberBand');
			});
			reject('Perdón!, Es necesario que el detalle de la actividad y el detalle de resultado superen los 50 caracteres.')
		}
	}else{
			var element = $("#formAgendaSmart").parent().parent().parent();
			$(element).addClass('animated rubberBand');
			$(element).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(element).removeClass('animated rubberBand');
			});
			reject('Perdón!, Es necesario que ingrese una oportunidad.')
	}
}

funAgregarTextoPendientes = function(valorPendiente, i) {
	var val 		= valorPendiente;
	if($('#listsubtipo').find('option').filter(function(){
			if (parseInt(this.value) === parseInt(val)) {
				console.log(this.label);
				$("#tipo_pendienteTexto"+i).val(this.label).removeClass('ocultar');
				$("#tipo_pendiente"+i).addClass('ocultar');
				return 1;
			}
	}).length){}
	$("#tipo_pendienteTexto"+i).removeClass('ocultar');
	$("#tipo_pendiente"+i).addClass('ocultar');
}
