/**
 * Activar el tooltip para cada title
 */
$('[data-toggle="tooltip"]').tooltip();

$(document).ready(function() {
    url_base = $('#url_base').val();
    list_oportunidades('list_oportunidades');
});

/* Funcion para encontrar el listado de oportunidades identificadas por medio de AJAX */
function list_oportunidades(listado){
    fecha_inicial = $('#fecha_ff').val();
    fecha_final = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 0,
            dir: url_base,
            fecha_inicial: fecha_inicial,
            fecha_final: fecha_final
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-identificadas').html(e.data['cuerpo']);
            ocultar_listados('.e-identificadas',listado);
        }
    });
}

/* Modal - Plazo para cierre de oportunidades */
function list_plazoCierre(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 3,
            dir: url_base,
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.fila-plazocierre').remove();
            $('.e-plazocierre').after(e.data['cuerpo']);
        }
    });
}

/* Funcion para encontrar el listado de Negocios cerrados por medio de AJAX */
function list_negociosCerrados(listado){
    fecha_inicial = $('#fecha_ff').val();
    fecha_final = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 1,
            dir: url_base,
            fecha_inicial: fecha_inicial,
            fecha_final: fecha_final
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-cerrados').html(e.data['cuerpo']);
            ocultar_listados('.e-cerrados',listado);
        }
    });
}
/*Funcion para encontrar el listado de Negocios Perdidos*/
function list_negociosPerdidos(listado){
    fecha_inicial = $('#fecha_ff').val();
    fecha_final = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 2,
            dir: url_base,
            fecha_inicial: fecha_inicial,
            fecha_final: fecha_final
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-perdidos').html(e.data['cuerpo']);
            ocultar_listados('.e-perdidos',listado);
        }
    });
}

/*Funcion Bitacora*/
function list_Bitacora(listado){
    fecha_inicial = $('#fecha_ff').val();
    fecha_final = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 4,
            dir: url_base,
            fecha_inicial: fecha_inicial,
            fecha_final: fecha_final
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-bitacora').html(e.data['cuerpo']);
            ocultar_listados('.e-bitacora',listado);
        }
    });
}

/*Funcion Plan de Trabajo*/
function list_PlanTrabajo(listado){
    fecha_inicial = $('#fecha_ff').val();
    fecha_final = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 5,
            dir: url_base,
            fecha_inicial: fecha_inicial,
            fecha_final: fecha_final
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-planTrabajo').html(e.data['cuerpo']);
            ocultar_listados('.e-planTrabajo',listado);
        }
    });
}

/*Funcion Pendientes*/
function list_Pendientes(listado){
    fecha_ff = $('#fecha_ff').val();
    fecha_oc = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 6,
            dir: url_base,
            fecha_ff: fecha_ff,
            fecha_oc: fecha_oc
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-pendientes').html(e.data['cuerpo']);
            ocultar_listados('.e-pendientes',listado);
        }
    });
}

/*Funcion Datos*/
function list_Datos(listado){
    fecha_ff = $('#fecha_ff').val();
    fecha_oc = $('#fecha_oc').val();
    $('.loader-cylon').css('display','block');
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_base+"/administradorsistema-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 7,
            dir: url_base,
            fecha_ff: fecha_ff,
            fecha_oc: fecha_oc
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.e-datos-actualizacion').html(e.data['cuerpo_actualizacion']);
            $('.e-datos-actuales').html(e.data['cuerpo_actuales']);
            ocultar_listados('.e-datos',listado);
        }
    });
}

/*Funcion para ocultar Listados*/
function ocultar_listados(listado_ver,listado){
    /*Mostrar y ocultar listados*/
    var listados = [".e-identificadas", ".e-cerrados", ".e-perdidos", ".e-bitacora", ".e-planTrabajo", ".e-pendientes", ".e-datos"];
    $.each( listados, function( index, value ){
      $(value).addClass("no-display");
    });
    $(listado_ver).removeClass("no-display");

    /*Activar y desactivar items menu*/
    $(".menu .col-md-3").each(function(){
        $(this).removeClass('active');
    });
    $("#"+listado).addClass('active');
    $('.loader-cylon').css('display','none');
    $('[data-toggle="tooltip"]').tooltip();
}

/* Activar calendario */
$('#fecha_ff').bootstrapMaterialDatePicker({
    time: false,
    clearButton: true,
    nowButton: true,
    lang: 'es',
    maxDate : new Date(),
    cancelText : 'Cancelar',
    okText: 'Aceptar',
    nowText: 'Nuevo',
    clearText: 'Limpiar'
}).on('close', function(e){
    $('#fecha_oc').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        nowButton: true,
        lang: 'es',
        minDate : $("#fecha_ff").val(),
        cancelText : 'Cancelar',
        okText: 'Aceptar',
        nowText: 'Nuevo',
        clearText: 'Limpiar'
    })
});

/*calendario change*/
$(document).on('change','.fecha_ff', function(){
    fecha_inicial = $(this).value();
    fecha_final = $(this).value();
});

/**
 * Abrir Modal de oportunidades identificadas
 */
$(document).on('click','#ver_ciclo',function(){
    $('.bd-viewoportunidad-modal-lg').modal('show');
    list_plazoCierre();
});

/*Grafica Dona Plan de Trabajo*/
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "title": "50%",
    "value": 900
  }, {
    "title": "",
    "value": 500,

  } ],
  "titleField": "title",
  "valueField": "value",
  "labelRadius": 5,

  "radius": "46%",
  "innerRadius": "40%",
  "labelText": "[[title]]",
  "export": {
    "enabled": false
  }
} );


/*Aplicar Filtro*/
$(document).on('click','.btn-filtrar',function(){
    $('.active').click();
});
