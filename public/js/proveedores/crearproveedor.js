url_basico=$('#URL').val();
//imagen
$('.dropzone').html5imageupload();

$('body').on('change','#pais',function(){
   valor=$(this) .val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio pais!',
          'warning'
        );
        $('#pais').val('');
        $('#departamento').val('');
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultarpais/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#pais_mostrar').val(e.pais.name);
                $('.fila-pais').css('display','block');
                $('#pais').css('display','none');
                $('#lista_departamentos').html(e.html);
                $('#departamento').attr('placeholder','Seleccione un departamento (estado)');
                $('#ciudad').attr('placeholder','Seleccione un departamento (estado)');
                $('#departamento').removeClass('no-cursor');
                $('#departamento').removeAttr("readonly");
            }
        });
    }
});

$('body').on('click','.cambiar-pais',function(){
    $('.fila-pais').css('display','none');
    $('#pais').val('');
    $('#pais').css('display','block');
    $('.fila-departamento').css('display','none');
    $('#departamento').css('display','block');
    $('#lista_departamentos').html('');
    $('#departamento').val('');
    $('#departamento').addClass('no-cursor');
    $('#departamento').attr("readonly","readonly");
    $('.fila-ciudad').css('display','none');
    $('#ciudad').css('display','block');
    $('#lista_ciudades').html('');
    $('#ciudad').val('');
    $('#ciudad').addClass('no-cursor');
    $('#ciudad').attr("readonly","readonly");

    $('#departamento').attr('placeholder','Seleccione un pais');
    $('#ciudad').attr('placeholder','Seleccione un pais');
});

/*Departamento*/

$('body').on('change','#departamento',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Departamento!',
          'warning'
        );
        $('#departamento').val('');
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultardepartamento/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#departamento_mostrar').val(e.departamento.name);
                $('.fila-departamento').css('display','block');
                $('#departamento').css('display','none');
                $('#lista_ciudades').html(e.html);
                $('#ciudad').attr('placeholder','Seleccione una ciudad (municipio)');
                $('#ciudad').removeClass('no-cursor');
                $('#ciudad').removeAttr("readonly");
            }
        });
    }
});

$('body').on('click','.cambiar-departamento',function(){
    $('.fila-departamento').css('display','none');
    $('#departamento').val('');
    $('#departamento').css('display','block');
    $('.fila-ciudad').css('display','none');
    $('#ciudad').css('display','block');
    $('#lista_ciudades').html('');
    $('#ciudad').val('');
    $('#ciudad').addClass('no-cursor');
    $('#ciudad').attr("readonly","readonly");

    $('#departamento').attr('placeholder','Seleccione un departamento (estado)');
    $('#ciudad').attr('placeholder','Seleccione un departamento (estado)');
});

/*ciudad*/

$('body').on('change','#ciudad',function(){
   valor=$(this).val();
    valor=parseInt(valor);
    if(isNaN(valor)){
        swal(
          'Oops...',
          'No eligio Ciudad!',
          'warning'
        );
        $('#ciudad').val('');
    }else{
        $.ajax({
            url: url_basico+"/consultarciudad/"+valor,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#ciudad_mostrar').val(e.ciudad.name);
                $('.fila-ciudad').css('display','block');
                $('#ciudad').css('display','none');
            }
        });
    }
});

$('body').on('click','.cambiar-ciudad',function(){
    $('.fila-ciudad').css('display','none');
    $('#ciudad').val('');
    $('#ciudad').css('display','block');

    $('#ciudad').attr('placeholder','Seleccione una ciudad (municipio)');
});

$('.telefonocelular').keyup(function (){
    this.value = (this.value + '').replace(/[^0-9]/g, '');
    if((this.value).length>3){
      this.value=('('+(this.value).charAt(0)+(this.value).charAt(1)+(this.value).charAt(2)+') '+(this.value).substring(3));
      if((this.value).length>=10){
        this.value=('('+(this.value).charAt(1)+(this.value).charAt(2)+(this.value).charAt(3)+') '+(this.value).charAt(6)+(this.value).charAt(7)+(this.value).charAt(8)+' '+(this.value).substring(9));
        if((this.value).length>=13){
          this.value=('('+(this.value).charAt(1)+(this.value).charAt(2)+(this.value).charAt(3)+') '+(this.value).charAt(6)+(this.value).charAt(7)+(this.value).charAt(8)+' '+(this.value).charAt(10)+(this.value).charAt(11)+' '+(this.value).substring(12));
          if((this.value).length>15){
            this.value=((this.value).substring(0, 15));
          }
        }
      }
    }
  });

$('body').on('click','.guardar_proveedor',function(){
    event.preventDefault();
    if(true === $("#formulario_basico").parsley().validate()){
        swal({
          title: 'Esta seguro?',
          text: "Va a guardar el proveedor",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var jqxhr = $.post( url_basico+"/guardarproveedor", $("#formulario_basico").serialize())
            .done(function() {
                console.log("Guardado");
            })
            .fail(function(e) {
                console.error(e.msj)
            })
            .always(function(e) {
              console.log(e.msj);
              swal(
                  'Guardado',
                   e.msj,
                  'success'
                ).then(function () {
                    location.href = url_basico+"/proveedores";
                });
            });
        });
    }else{
        swal('Faltan campos por completar');
    }
});


$('body').on('click','.editar_proveedor',function(){
    event.preventDefault();
    if(true === $("#formulario_basico").parsley().validate()){
        swal({
          title: 'Esta seguro?',
          text: "Va a editar el proveedor",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var jqxhr = $.post( url_basico+"/editarproveedor", $("#formulario_basico").serialize())
            .done(function() {
                console.log("Guardado");
            })
            .fail(function(e) {
                console.error(e.msj)
            })
            .always(function(e) {
              console.log(e.msj);
              swal(
                  'Guardado',
                   e.msj,
                  'success'
                ).then(function () {
                    location.href = url_basico+"/proveedores";
                });
            });
        });
    }else{
        swal('Faltan campos por completar');
    }
});


/*Responsables dinámico*/
var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".ProveRespon").length;

function clone(){
    $("#reponsables_container").children(".ProveRespon").clone()
        .appendTo("#reponsables_container")
        .attr({id: "responsables-row-"+ cloneIndex, class: "ProveRespon mt-5"})
        .find("*")
        .each(function(){
            var id = this.id || "";
            var match = id.match(regex) || [];
            if (match.length == 3) {
                this.id = match[1] + (cloneIndex);
            }
        });
    cloneIndex++;
}
function remove(){
    if($(".ProveRespon").size() > 1){
        $("#reponsables_container").children(".ProveRespon:last").remove();
    }
}

$(document).on("click", '.clone', clone);

$(document).on("click", '.remove', remove);
