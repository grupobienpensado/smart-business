
    var CSRF_TOKEN = $('input[name=_token]').val();
    var fecha_inicio = '';
    var fecha_fin = '';

    /*Solo para el filtro*/
    $('#filtro_fecha_inicio').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true,
        nowButton: true,
        lang: 'es',
        cancelText : 'Cancelar',
        okText: 'Aceptar',
        nowText: 'Nuevo',
        clearText: 'Limpiar'
    }).on('close', function(e){
        $('#filtro_fecha_fin').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            nowButton: true,
            lang: 'es',
            minDate : $("#filtro_fecha_inicio").val(),
            cancelText : 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Nuevo',
            clearText: 'Limpiar'
        })
    });



    /**
     * Función para modificar los iconos del menu
     */
    function modificariconosmenu(){
        var anteriores = {menu_1:"fa fa-dashboard fa-fw", menu_3:"fa fa-id-card-o", menu_10:"fa fa-check-square-o", menu_13:"fa fa-money", menu_32:'fa fa-address-book-o'};
        var nuevos = {menu_1:"fas fa-tachometer-alt", menu_3:"far fa-id-card", menu_10:"far fa-check-square", menu_13:"far fa-money-bill-alt", menu_32:'far fa-address-book'};
        $.each( anteriores, function( key, value ) {
          $('#'+key+' i').eq(0).removeClass(value);
          $('#'+key+' i').eq(0).addClass(nuevos[key]);
        });
        $('#cabecera_menu i').addClass('fas fa-exchange-alt');
    }
    $(document).ready(function(){
       modificariconosmenu();
       cono();
       negocios_cerrados();
       ventas_proyecciones();
       empresa_clientes();
       setTimeout(function(){
           oportunidades_perdidas();
           oportunidades_identificadas();
           ciclos_venta();
       }, 2000);
    });

    /**
     * Función para filtrar el tablero
     */
    function filtrar_dashboard(){
        fecha_inicio = $('#filtro_fecha_inicio').val();
        fecha_fin = $('#filtro_fecha_fin').val();
        negocios_cerrados();
        oportunidades_perdidas();
        oportunidades_identificadas();
        ciclos_venta();
        drawChart2();
        ventas_proyecciones();
    }

    /**
     * Función para guardar el dashboard para el modulo de comparativo
     */
    function save_comparativo(){
        swal({
          title: 'Guardar',
          text: 'Esta seguro que desea guardar la información del dashboard?',
          type: "question",
          showCancelButton: true,
          confirmButtonText: 'Guardar',
          cancelButtonText: 'Cancelar',
          showLoaderOnConfirm: true,
          preConfirm: () => {
            return new Promise((resolve, reject) => {
              var fd = new FormData(document.getElementById("save-form"));
              var request = $.ajax({
                url: "/dashboard-produccion-action",
                type: "POST",
                data: fd,
                processData: false,
                contentType: false,
                cache: true,
                ifModified: true,
                async: true,
              });
              request.done(function( respuesta ) {
                if (respuesta.data.msj) {
                  resolve(respuesta)
                }
              });
              request.fail(function( jqXHR, textStatus ) {
                resolve({success:false}, jqXHR, textStatus)
              });
            })
          },
          allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
          if (result.data.msj) {
            swal({
              type: 'success',
              title: 'Guardado con éxito!'
            })
          }else {
            swal({
              type: 'error',
              title: 'Vaya parece que algo salió mal!'
            })
          }
        });
    }
    /**
     * Función para cargar la segunda parte del dashboard
     */
    function cargar_segunda_parte(){
        costoventas();
        cumplimientopresupuestal();
        usotiempo();
        gestion();
        cumplimientollenado();
    }

   /*DATOS CONO OPORTUNIDADES*/
    function cono(){
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
            url: "/dashboard-produccion-action",
            data: {
                _token: CSRF_TOKEN,
                action: 0,
                fecha_inicio: fecha_inicio,
                fecha_fin: fecha_fin
            },
            cache: false,
            type: 'POST',
            success: function(e){
                cono_data(e.data);
                datoscono(e.data);
                $('.loader-cylon').remove();
            }
        });
    }
    function cono_data(data){
        var chart1 = AmCharts.makeChart("chart_cono", {
            "type": "funnel",
            "responsive": {
                "enabled": true
            },
            "theme": "light",
            "dataProvider":[{
            "title": "LP Diferencia meta",
            "value": data['diferencia_lp']
        },
        {
             "title": "LP baja",
             "value": data['largo_plazo']['Baja']
        },
        {
             "title": "LP media",
             "value": data['largo_plazo']['Media']
        },
        {
            "title": "LP Alta",
             "value": data['largo_plazo']['Alta']
        },
        {
             "title": "MP Diferencia meta",
             "value": data['diferencia_mp']
        },
        {
            "title": "MP baja",
            "value": data['medio_plazo']['Baja']
        },
        {
            "title": "MP media",
            "value": data['medio_plazo']['Media']
        },
        {
            "title": "MP Alta",
            "value": data['medio_plazo']['Alta']
        },
        {
            "title": "CP Diferencia Meta",
            "value": data['diferencia_cp']
        },
        {
            "title": "CP Baja",
            "value": data['corto_plazo']['Baja']
        },
        {
         "title": "CP Media",
         "value": data['corto_plazo']['Media']
        },
        {
            "title": "CP alta",
            "value": data['corto_plazo']['Alta']
        }],
            "balloon": {
                "fixedPosition": true
            },
            "valueField": "value",
            "titleField": "title",
            "marginRight": 440,
            "marginLeft": 50,
            "startX": -500,
            "depth3D": 100,
            "angle": 40,
            "outlineAlpha": 1,
            "outlineColor": "#FFFFFF",
            "outlineThickness": 2,
            "colors":["#be0203","#f52b2b","#ff6155","#f9928b","#ffa901","#ffbb00","#ffd640","#ffe27e","#00690c","#268c1f","#7db85e","#5fe014"],
            "labelPosition": "right",
            "labelText":"[[title]]: [[value]] Millones",
            "balloonText": "[[title]]: [[value]] Millones",
            "export": {
                "enabled": false
            }
        });
    }
    /*FIN DATOS CONO OPORTUNIDADES*/


    /*MODAL CONO OPORTUNIDADES*/
    var largo_plazo_Baja = 0;
    var largo_plazo_Media = 0;
    var largo_plazo_Alta = 0;
    var medio_plazo_Baja = 0;
    var medio_plazo_Media = 0;
    var medio_plazo_Alta = 0;
    var corto_plazo_Baja = 0;
    var corto_plazo_Media = 0;
    var corto_plazo_Alta = 0;
    function datoscono(data){
        largo_plazo_Baja = data['largo_plazo']['Baja'];
        largo_plazo_Media = data['largo_plazo']['Media'];
        largo_plazo_Alta = data['largo_plazo']['Alta'];
        medio_plazo_Baja = data['medio_plazo']['Baja'];
        medio_plazo_Media = data['medio_plazo']['Media'];
        medio_plazo_Alta = data['medio_plazo']['Alta'];
        corto_plazo_Baja = data['corto_plazo']['Baja'];
        corto_plazo_Media = data['corto_plazo']['Media'];
        corto_plazo_Alta = data['corto_plazo']['Alta'];
    }
    abrir_cono = function(){
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        $('#exampleModal').modal('show');
        var jqxhr = $.ajax({
            url: "/dashboard-produccion-action",
            data: {
                _token: CSRF_TOKEN,
                action: 1,
                largo_plazo_Baja: largo_plazo_Baja,
                largo_plazo_Media:largo_plazo_Media,
                largo_plazo_Alta: largo_plazo_Alta,
                medio_plazo_Baja: medio_plazo_Baja,
                medio_plazo_Media: medio_plazo_Media,
                medio_plazo_Alta: medio_plazo_Alta,
                corto_plazo_Baja: corto_plazo_Baja,
                corto_plazo_Media: corto_plazo_Media,
                corto_plazo_Alta: corto_plazo_Alta
            },
            cache: false,
            type: 'POST',
            success: function(e){
                $('#script_dinamico').html(e.data['html']);
                show_info_modal('modal_cono_oportunidades');
                //9 graficas del modal
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono0);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono1);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono2);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono3);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono4);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono5);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono6);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono7);
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart_cono8);
                $('.loader-cylon').remove();
                $('#titulo_modal').html('Cono de Oportunidades');
                $('.total-open-cono:eq(0)').html('$ '+e.data['Corto']+' Millones');
                $('#total_corto_alta').html(formato_numero(corto_plazo_Alta)+' Millones');
                $('#total_corto_media').html(formato_numero(corto_plazo_Media)+' Millones');
                $('#total_corto_baja').html(formato_numero(corto_plazo_Baja)+' Millones');
                $('#total_mediano_alta').html(formato_numero(medio_plazo_Alta)+' Millones');
                $('#total_mediano_media').html(formato_numero(medio_plazo_Media)+' Millones');
                $('#total_mediano_baja').html(formato_numero(medio_plazo_Baja)+' Millones');
                $('#total_largo_alta').html(formato_numero(largo_plazo_Alta)+' Millones');
                $('#total_largo_media').html(formato_numero(largo_plazo_Media)+' Millones');
                $('#total_largo_baja').html(formato_numero(largo_plazo_Baja)+' Millones');
                $('.total-open-cono:eq(1)').html('$ '+e.data['Mediano']+' Millones');
                $('.total-open-cono:eq(2)').html('$ '+e.data['Largo']+' Millones');
                margen_modal();
             }
        });
    }
    /*FINAL MODAL CONO OPORTUNIDADES*/


    /*Margen del modal*/
    function margen_modal(){
        setTimeout(function(){
            var alto = $( window ).height();
            var alto_modal = $('.modal-content').height()
            if(alto < alto_modal){
                var margen_top = alto_modal*0.25;
                margen_top = margen_top+'px';
                $('.modal-content').css('margin-top',margen_top);
            }else{
                $('.modal-content').css('margin-top','0px');
            }
        }, 1500);
    }
    /*Fin Margen del modal*/


    /**
     * Función para mostrar la información que corresponde en el modal y cerrar los que no corresponde
     * @param STRING id IDENTIFICACIÓN DEL CONTENEDOR
     */
    function show_info_modal(id){
        $('.informacion-modal').css('display','none');
        $('#'+id).css('display','block');
    }


    /*NEGOCIOS CERRADOS*/
    function negocios_cerrados(){
      $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
      $('.loader-cylon').css('display','block');
      var jqxhr = $.ajax({
          url: "/dashboard-produccion-action",
          data: {
              _token: CSRF_TOKEN,
              action: 2,
              fecha_inicio: fecha_inicio,
              fecha_fin: fecha_fin,
          },
          cache: false,
          type: 'POST',
          success: function(e){
            $('#numero_negocios_cerrados').html(e.data['total_negocios_cerrados']);
            $('#valor_negocios_cerrados').html(e.data['valor_negocios_cerrados']);
            $('.loader-cylon').remove();
            promedio_cierre();
          }
      });
    }
    abrir_negocios_cerrados = function(){
        $('#exampleModal').modal('show');
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 4,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin,
                  cargos: $('#filtro_cargos_modal_negocios').val()
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#script_dinamico').html(e.data['html']);
                  $('#cantidad_negocios_cerrados_modal').html(e.data['html_cantidad']);
                  show_info_modal('modal_negocios_cerrados');
                  margen_modal();
                  $('#titulo_modal').html('Negocios Cerrados');
                  $('.loader-cylon').remove();
              }
        });
    }

    /**
     * Función para filtrar los cargos del modal negocios cerrados
     */
    function filtrar_cargos(){
        var cargos = $('#filtro_cargos_modal_negocios').val();
        if(cargos != null){
            abrir_negocios_cerrados();
        }else{
            swal('Advertencia','Debe seleccionar al menos un cargo para que se ejecute el filtro','warning');
        }
    }
    /*FIN NEGOCIOS CERRADOS*/


    /*PROMEDIO DE CIERRE*/
    function promedio_cierre(){
      $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
      $('.loader-cylon').css('display','block');
      var jqxhr = $.ajax({
          url: "/dashboard-produccion-action",
          data: {
              _token: CSRF_TOKEN,
              action: 3,
              fecha_inicio: fecha_inicio,
              fecha_fin: fecha_fin
          },
          cache: false,
          type: 'POST',
          success: function(e){
              $('#promedio_cierre').html(e.data['promedio']);
              $('.loader-cylon').remove();
          }
      });
    }
    /*FIN PROMEDIO DE CIERRE*/


    /*OPORTUNIDADES PERDIDAS*/
    function oportunidades_perdidas(){
      $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
      $('.loader-cylon').css('display','block');
      var jqxhr = $.ajax({
          url: "/dashboard-produccion-action",
          data: {
              _token: CSRF_TOKEN,
              action: 5,
              fecha_inicio: fecha_inicio,
              fecha_fin: fecha_fin
          },
          cache: false,
          type: 'POST',
          success: function(e){
              $('.opo-per').eq(0).html(e.data['porcentaje_respecto_total']);
              $('.opo-per').eq(1).html(e.data['numero_oportunidades']);
              $('.opo-per').eq(2).html(e.data['valor']);
              $('.loader-cylon').remove();
          }
      });
    }
    abrir_oportunidades_perdidas = function(){
        $('#exampleModal').modal('show');
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 6,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin,
                  cargos: $('#filtro_cargos_modal_oportunidadesperdidas').val()
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#modal_oportunidades_perdidas_porcentaje').html(e.data['html_oportunidad_perdida_porcentaje']);
                  $('#modal_oportunidades_perdidas_cantidad').html(e.data['html_oportunidad_perdida_cantidad']);
                  $('#script_dinamico').html(e.data['html']);
                  show_info_modal('modal_oportunidades_perdidas');
                  margen_modal();
                  $('#titulo_modal').html('Oportunidades Perdidas');
                  $('.loader-cylon').remove();
              }
        });
    }
    function filtrar_cargos_perdidas(){
        var cargos = $('#filtro_cargos_modal_oportunidadesperdidas').val();
        if(cargos != null){
            abrir_oportunidades_perdidas();
        }else{
            swal('Advertencia','Debe seleccionar al menos un cargo para que se ejecute el filtro','warning');
        }
    }
    /*FIN OPORTUNIDADES PERDIDAS*/


    /*OPORTUNIDADES IDENTIFICADAS*/
    function oportunidades_identificadas(){
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 7,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#dona_identificada').html(e.data['html']);
                  $('#total_oportunidadesidentificadas span').html(e.data['total']);
                  $('.loader-cylon').remove();
              }
        });
    }
    abrir_oportunidades_identificadas = function(){
        $('#exampleModal').modal('show');
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 8,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin,
                  largo_plazo_Baja: largo_plazo_Baja,
                  largo_plazo_Media:largo_plazo_Media,
                  largo_plazo_Alta: largo_plazo_Alta,
                  medio_plazo_Baja: medio_plazo_Baja,
                  medio_plazo_Media: medio_plazo_Media,
                  medio_plazo_Alta: medio_plazo_Alta,
                  corto_plazo_Baja: corto_plazo_Baja,
                  corto_plazo_Media: corto_plazo_Media,
                  corto_plazo_Alta: corto_plazo_Alta
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#script_dinamico').html(e.data['html']);
                  show_info_modal('modal_oportunidades_identificadas');
                  margen_modal();
                  $('#titulo_modal').html('Oportunidades Identificadas ($ '+e.data['total']+' Millones)');
                  $('.loader-cylon').remove();
              }
        });
    }
    abrir_oportunidades_identificadas2 = function(){
        $('#exampleModal').modal('show');
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 10,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#usuarios_oportunidades_identificadas2').html(e.data['html']);
                  show_info_modal('modal_oportunidades_identificadas2');
                  margen_modal();
                  $('#titulo_modal').html('Oportunidades Identificadas');
                  $('.loader-cylon').remove();
              }
        });
    }
    /*FIN OPORTUNIDADES IDENTIFICADAS*/


    /*CICLOS DE VENTA*/
    function ciclos_venta(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 11,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#content_ciclos_ventas').html(e.data['html']);
              }
        });
    }
    var ModalCiclosVentaInfo='';
    abrir_ciclos_venta = function(){
        $('#exampleModal').modal('show');
        $('#modal_ciclos_venta').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_ciclos_venta');
        margen_modal();
        if(ModalCiclosVentaInfo != ''){
             $('#modal_ciclos_venta').html(ModalCiclosVentaInfo);
             margen_modal();
            $('#titulo_modal').html('Ciclos de Venta');
            $('.loader-cylon').remove();
        }else{
            var jqxhr = $.ajax({
                  url: "/dashboard-produccion-action",
                  data: {
                      _token: CSRF_TOKEN,
                      action: 12,
                      fecha_inicio: fecha_inicio,
                      fecha_fin: fecha_fin
                  },
                  cache: false,
                  type: 'POST',
                  success: function(e){
                      $('#modal_ciclos_venta').html(e.data['html']);
                      margen_modal();
                      $('#titulo_modal').html('Ciclos de Venta');
                      ModalCiclosVentaInfo = e.data['html'];
                  }
            });
        }
    }
    /*FIN CICLOS DE VENTA*/

    /*VENTAS Y PROYECCIONES*/
    function ventas_proyecciones(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 13,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#total_equipos').html(e.data['equipos']);
                  $('#total_paises').html(e.data['paises']);
                  $('#total_equiposproyecciones').html(e.data['equiposproyecciones']);
                  $('#total_paisesproyecciones').html(e.data['paisesproyecciones']);
              }
        });
    }
    abrir_ventas = function(){
        $('#exampleModal').modal('show');
        $('#modal_ventas').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_ventas');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 14
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#modal_ventas').html(e.data['html']);
                  margen_modal();
                  $('#titulo_modal').html('Ventas');
              }
        });
    }
    abrir_proyecciones = function(){
        $('#exampleModal').modal('show');
        $('#modal_proyecciones').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_proyecciones');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 15
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#modal_proyecciones').html(e.data['html']);
                  margen_modal();
                  $('#titulo_modal').html('Proyecciones');
              }
        });
    }
    abrir_ventas_filtrado = function(){
        $('#exampleModal').modal('show');
        $('#modal_ventas').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_ventas');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 29,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#modal_ventas').html(e.data['html']);
                  margen_modal();
                  $('#titulo_modal').html('Ventas');
              }
        });
    }
    /*FIN VENTAS Y PROYECCIONES*/

    /*EMPRESAS & CLIENTES*/
    function empresa_clientes(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 16
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#total_empresa_oportunidades').html(e.data['empresasOportunidades']);
                  $('#total_empresa_identificadas').html(e.data['empresasIdentificadas']);
                  $('#total_clientes_registrados').html(e.data['clientesRegistrados']);
                  $('#total_empresas_nuevas').html(e.data['empresasIdentificadasNuevas']);
                  $('#total_clientes_nuevos').html(e.data['clientesRegistradosNuevas']);
              }
        });
    }
    abrir_empresas_clientes = function(){
        margen_modal();
        $('#exampleModal').modal('show');
        $('#modal_empresas_clientes').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_empresas_clientes');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 17
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#modal_empresas_clientes').html(e.data['html']);
                  $('#script_modal_empresa_cliente').html(e.data['html_script']);
                  $('#titulo_modal').html('Empresas & Clientes');
                  margen_modal();
              }
        });
    }
    /*FIN EMPRESAS & CLIENTES*/

    /*COSTO DE VENTAS*/
    function costoventas(){
        $('#wrapper').prepend('<div class="loader-cylon" style="display: none;"></div>');
        $('.loader-cylon').css('display','block');
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 18
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#costo_venta').html(e.data['porcentaje_gastos']+'%');
                  $('.loader-cylon').remove();
              }
        });
    }
    var InfoCostoVenta = '';
    abrircostoventa = function(){
        margen_modal();
        $('#exampleModal').modal('show');
        $('#modal_costo_venta').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_costo_venta');
        if(InfoCostoVenta!=''){
            $('#modal_costo_venta').html(InfoCostoVenta);
            $('#titulo_modal').html('Costo de Venta');
            margen_modal();
        }else{
             var jqxhr = $.ajax({
                  url: "/dashboard-produccion-action",
                  data: {
                      _token: CSRF_TOKEN,
                      action: 19
                  },
                  cache: false,
                  type: 'POST',
                  success: function(e){
                      InfoCostoVenta = e.data['html'];
                      $('#modal_costo_venta').html(e.data['html']);
                      $('#titulo_modal').html('Costo de Venta');
                      margen_modal();
                  }
            });
        }
    }
    /*FIN COSTO VENTAS*/

    /*CUMPLIMIENTO PRESUPUESTAL*/
    function cumplimientopresupuestal(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 20
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#ejecucion_presupuesto_ultimo_mes h2').html(e.data['ejecucion_presupuesto_ultimo_mes']+'%');
                  $('#ejecucion_presupuesto_ultimo_mes p').html(`Cumplimiento Presupuesto `+e.data['mes_anterior']+`<br>Segun Bitacora`);
                  $('#gasto_parcial_mes_actual h2').html(e.data['gasto_parcial_mes_actual']+'%');
                  $('#gasto_parcial_mes_actual p').html(`Gasto Parcial de presupuesto `+e.data['mes_actual']+`<br>Segun Bitacora`);
                  $('#gasto_parcial_ano_actual h2').html(e.data['gasto_parcial_ano_actual']+'%');
                  var ano_actual = moment().format('YYYY');
                  $('#gasto_parcial_ano_actual p').html(`Gasto Parcial de presupuesto `+ano_actual+`<br>Incluye datos ajuste`);
              }
        });
    }
    var InfoCumplimientoPresupuestal = '';
    abrircumplimientopresupuestal = function(){
        margen_modal();
        $('#exampleModal').modal('show');
        $('#modal_cumplimiento_presupuestal').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_cumplimiento_presupuestal');
        if(InfoCumplimientoPresupuestal!=''){
            $('#modal_cumplimiento_presupuestal').html(InfoCumplimientoPresupuestal);
            $('#titulo_modal').html('Cumplimiento Presupuestal');
            margen_modal();
        }else{
             var jqxhr = $.ajax({
                  url: "/dashboard-produccion-action",
                  data: {
                      _token: CSRF_TOKEN,
                      action: 21
                  },
                  cache: false,
                  type: 'POST',
                  success: function(e){
                      InfoCumplimientoPresupuestal = e.data['html'];
                      $('#modal_cumplimiento_presupuestal').html(e.data['html']);
                      $('#titulo_modal').html('Cumplimiento Presupuestal');
                      margen_modal();
                  }
            });
        }
    }
    /*FIN CUMPLIMIENTO PRESUPUESTAL*/

    /*USO DEL TIEMPO*/
    function usotiempo(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 22
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#first_list').html(e.data['html']);
                  $('#uso_tiempo_torta').html(e.data['html2']);
              }
        });
    }
    var InfoUsoTiempo='';
    abrirusotiempo = function(){
        margen_modal();
        $('#exampleModal').modal('show');
        $('#modal_uso_tiempo').html(`<div class="col-md-12 text-center"><div class="fa-3x"><i class="fas fa-sync fa-spin"></i><p class="h5">Cargando....<br>Por favor espere un momento hasta que se termine de realizar la consulta.</p></div></div>`);
        show_info_modal('modal_uso_tiempo');
        if(InfoUsoTiempo!=''){
            $('#modal_uso_tiempo').html(InfoUsoTiempo);
            $('#titulo_modal').html('Uso del Tiempo');
            margen_modal();
        }else{
             var jqxhr = $.ajax({
                  url: "/dashboard-produccion-action",
                  data: {
                      _token: CSRF_TOKEN,
                      action: 23
                  },
                  cache: false,
                  type: 'POST',
                  success: function(e){
                      InfoUsoTiempo = e.data['html'];
                      $('#modal_uso_tiempo').html(e.data['html']);
                      $('#titulo_modal').html('Uso del Tiempo');
                      margen_modal();
                  }
            });
        }
    }
    /*FIN USO DEL TIEMPO*/

    /*GESTÓN ACTIVIDADES PENDIENTES*/
    function gestion(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 24
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#porcentaje_hecha_tiempo').html(e.data['porcentaje_hecha_tiempo']+'%');
                  $('#pendientes_semana').html(e.data['pendientes_semana']);
                  $('#pendientes_mes').html(e.data['pendientes_mes']);
                  $('#aplazadas_mes').html(e.data['aplazadas_mes']);
              }
        });
    }
    abrirgestion = function(){
        $('#exampleModal').modal('show');
        show_info_modal('modal_gestion');
        $('#titulo_modal').html('Gestión de Actividades Pendientes');
        margen_modal();
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 25
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#graficas_gestion_actividades_pendientes').html(e.data['html']);
                  margen_modal();
              }
        });
    }
    /*FIN GESTÓN ACTIVIDADES PENDIENTES*/

    /*CUMPLIMIENTO DE LLENADO BITACORA*/
    function cumplimientollenado(){
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 26
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#cumplimiento_bitacora').html(e.data['cumplimiento_bitacora']+'%');
                  $('.cumplimiento_bitacora').css('width',e.data['cumplimiento_bitacora']+'%');
                  $('#porcentajecumplimientosemanal').html(e.data['porcentajecumplimientosemanal']+'%');
                  $('.porcentajecumplimientosemanal').css('width',e.data['porcentajecumplimientosemanal']+'%');
                  $('#porcentajecumplimientopresupuesto').html(e.data['porcentajecumplimientopresupuesto']+'%');
                  $('.porcentajecumplimientopresupuesto').css('width',e.data['porcentajecumplimientopresupuesto']+'%');
              }
        });
    }
    abrircumplimientollenado = function(){
        $('#exampleModal').modal('show');
        show_info_modal('modal_cumplimiento_llenado');
        $('#titulo_modal').html('Gestión de Actividades Pendientes');
        margen_modal();
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 27
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  $('#scriptmodal_cumplimiento_llenado').html(e.data['html']);
                  margen_modal();
              }
        });
    }
    /*FIN CUMPLIMIENTO DE LLENADO BITACORA*/

    $(window).resize(function(){
        drawChartPie();
        drawChart2();
    });
    /*Charts modales viejos*/

    /*Oportunidades perdidal modal*/
    /*var opor_per_mdl_1 = AmCharts.makeChart("opor_per_mdl_1", {
      "responsive": {
        "enabled": true
      },
      "type": "pie",
      "theme": "light",
      "dataProvider": [{
        "title": "Oscar",
        "value": 4852,
        "alpha": 1
      }, {
        "title": "Mayra",
        "value": 9899,
        "alpha": 1
      }, {
        "title": "Laura",
        "value": 9899,
        "alpha": 0.1
      }],
      "titleField": "title",
      "valueField": "value",
      "labelRadius": -130,
      "radius": "42%",
      "innerRadius": "65%",
      "labelText": ""
    });*/

    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
    /*opor_per_mdl_1.addListener("rollOverSlice", function(event) {
        var height = $("#opor_per_mdl_1").height();
        event.chart.clearLabels();
        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
    });*/
    /*remover labels al retirar el cursor*/
    /*opor_per_mdl_1.addListener( "rollOutSlice", function(event){
        event.chart.clearLabels();
    });*/

    /*var opor_per_mdl_2 = AmCharts.makeChart("opor_per_mdl_2", {
      "responsive": {
        "enabled": true
      },
      "type": "pie",
      "theme": "light",
      "dataProvider": [{
        "title": "Bajo",
        "value": 4852,
        "alpha": 1
      }, {
        "title": "Medio",
        "value": 9899,
        "alpha": 1
      }, {
        "title": "Alto",
        "value": 9899,
        "alpha": 0.1
      }],
      "titleField": "title",
      "valueField": "value",
      "labelRadius": -130,
      "radius": "42%",
      "innerRadius": "65%",
      "labelText": ""
    });

    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
    /*opor_per_mdl_2.addListener("rollOverSlice", function(event) {
        var height = $("#opor_per_mdl_2").height();
        event.chart.clearLabels();
        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
    });
    /*remover labels al retirar el cursor*/
    /*opor_per_mdl_2.addListener( "rollOutSlice", function(event){
        event.chart.clearLabels();
    });*/

    /*var opor_per_mdl_3 = AmCharts.makeChart("opor_per_mdl_3", {
      "responsive": {
        "enabled": true
      },
      "type": "pie",
      "theme": "light",
      "dataProvider": [{
        "title": "Corto Plazo",
        "value": 4852,
        "alpha": 1
      }, {
        "title": "Mediano Plazo",
        "value": 9899,
        "alpha": 1
      }, {
        "title": "Largo Plazo",
        "value": 9899,
        "alpha": 0.1
      }],
      "titleField": "title",
      "valueField": "value",
      "labelRadius": -130,
      "radius": "42%",
      "innerRadius": "65%",
      "labelText": ""
    });

    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
    /*opor_per_mdl_3.addListener("rollOverSlice", function(event) {
        var height = $("#opor_per_mdl_3").height();
        event.chart.clearLabels();
        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 20, null, 0, null, true);
        event.chart.addLabel(null, height - (height * 50 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
    });
    /*remover labels al retirar el cursor*/
    /*opor_per_mdl_3.addListener( "rollOutSlice", function(event){
        event.chart.clearLabels();
    });*/

    /*Gestión de actividades pendientes top chart*/
    /*var ges_act_pen_1 = AmCharts.makeChart("ges_act_pen_1", {
    "type": "serial",
	"theme": "light",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "dataProvider": [{
        "name": "Pedro",
        "mes": 2.5,
        "semana": 2.5,
    }, {
        "name": "Juan",
        "mes": 2.6,
        "semana": 2.7,
    }, {
        "name": "Ignacio",
        "mes": 2.8,
        "semana": 2.9,
    }],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Mes",
        "type": "column",
		"color": "#000000",
        "valueField": "mes"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Semana",
        "type": "column",
		"color": "#000000",
        "valueField": "semana"
    }],
    "categoryField": "name",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "export": {
    	"enabled": true
     }

});*/

    /*Gestión de actividades pendientes bottom left*/
    /*var ges_act_pen_2 = AmCharts.makeChart( "ges_act_pen_2", {
                "type": "radar",
                "theme": "light",
                "dataProvider": [
                                            {
                  "direction": "Oscar",
                  "value": 89
                },
                                {
                  "direction": "Mayra",
                  "value": 26
                },
                                {
                  "direction": "Silvia",
                  "value": 70
                },
                                {
                  "direction": "Johanna",
                  "value": 38
                },
                                {
                  "direction": "Diego",
                  "value": 30
                },
                                {
                  "direction": "Laura",
                  "value": 0
                },
                                {
                  "direction": "Daniela",
                  "value": 0
                },
                                {
                  "direction": "Laura",
                  "value": 0
                },
                                {
                  "direction": "Laura",
                  "value": 0
                },
                        ],
        "valueAxes": [ {
            "gridType": "circles",
            "minimum": 0,
            "autoGridCount": false,
            "axisAlpha": 0.2,
            "fillAlpha": 0.05,
            "fillColor": "#FFFFFF",
            "gridAlpha": 0.08,
            "guides": [ {
                "angle": 225,
                "fillAlpha": 0.3,
                "fillColor": "#fff",
                "tickLength": 0,
                "toAngle": 315,
                "toValue": 14,
                "value": 0,
                "lineAlpha": 0,

            },
                        {
              "angle": 45,
              "fillAlpha": 0.3,
              "fillColor": "#fff",
              "tickLength": 0,
              "toAngle": 135,
              "toValue": 14,
              "value": 0,
              "lineAlpha": 0,
        } ],
            "position": "left"
        } ],
        "startDuration": 1,
        "graphs": [ {
            "balloonText": "[[category]]: [[value]] m/s",
            "bullet": "round",
            "fillAlphas": 0.3,
            "valueField": "value"
        } ],
        "categoryField": "direction",
        "export": {
            "enabled": false
        }
    });*/

    /*Gestión de actividades pendientes bottom right*/
    /*var ges_act_pen_3 = AmCharts.makeChart("ges_act_pen_3", {
                "type": "radar",
                "theme": "light",
                "dataProvider": [
                                            {
                  "direction": "Oscar",
                  "value": 89
                },
                                {
                  "direction": "Mayra",
                  "value": 26
                },
                                {
                  "direction": "Silvia",
                  "value": 70
                },
                                {
                  "direction": "Johanna",
                  "value": 38
                },
                                {
                  "direction": "Diego",
                  "value": 30
                },
                                {
                  "direction": "Laura",
                  "value": 0
                },
                                {
                  "direction": "Daniela",
                  "value": 0
                },
                                {
                  "direction": "Laura",
                  "value": 0
                },
                                {
                  "direction": "Laura",
                  "value": 0
                },
                        ],
            "valueAxes": [{
            "gridType": "circles",
            "minimum": 0,
            "autoGridCount": false,
            "axisAlpha": 0.2,
            "fillAlpha": 0.05,
            "fillColor": "#FFFFFF",
            "gridAlpha": 0.08,
            "guides": [ {
                "angle": 225,
                "fillAlpha": 0.3,
                "fillColor": "#fff",
                "tickLength": 0,
                "toAngle": 315,
                "toValue": 14,
                "value": 0,
                "lineAlpha": 0,

            },
                        {
              "angle": 45,
              "fillAlpha": 0.3,
              "fillColor": "#fff",
              "tickLength": 0,
              "toAngle": 135,
              "toValue": 14,
              "value": 0,
              "lineAlpha": 0,
        } ],
            "position": "left"
        } ],
        "startDuration": 1,
        "graphs": [ {
            "balloonText": "[[category]]: [[value]] m/s",
            "bullet": "round",
            "fillAlphas": 0.3,
            "valueField": "value"
        } ],
        "categoryField": "direction",
        "export": {
            "enabled": false
        }
    });*/

    /*Fin charts modales viejos*/

    /*Chart Cono página inicial*/
    var chart1 = AmCharts.makeChart("chart_cono", {
        "type": "funnel",
        "responsive": {
            "enabled": true
        },
        "theme": "light",
        "dataProvider": [{
                "title": "Diferencia meta",
                "value": 200
            },
            {
                "title": "baja",
                "value": 150
            },
            {
                "title": "media",
                "value": 120
            },
            {
                "title": "Alta",
                "value": 70
            },
            {
                "title": "Diferencia meta",
                "value": 110
            },
            {
                "title": "bajas",
                "value": 130
            },
            {
                "title": "media",
                "value": 170
            },
            {
                "title": "Alta",
                "value": 100
            },
            {
                "title": "Diferencia Meta",
                "value": 50
            }, {
                "title": "Baja",
                "value": 150
            }, {
                "title": "Media",
                "value": 200
            }, {
                "title": "alta",
                "value": 100
            }
        ],
        "balloon": {
            "fixedPosition": true
        },
        "valueField": "value",
        "titleField": "title",
        "marginRight": 440,
        "marginLeft": 50,
        "startX": -500,
        "depth3D": 100,
        "angle": 40,
        "outlineAlpha": 1,
        "outlineColor": "#FFFFFF",
        "outlineThickness": 2,
        "labelPosition": "right",
        "balloonText": "[[title]]: [[value]]n[[description]]",
        "export": {
            "enabled": true
        }
    });

    /*Inician Charts de Google página inicial*/
   /* google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChartPie);

    function drawChartPie() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work', 11],
            ['Eat', 2],
            ['Commute', 2],
            ['Watch TV', 2],
            ['Sleep', 7]
        ]);

        var options = {
            width: '100%',
            height: 400,
        };

        var chart = new google.visualization.PieChart(document.getElementById('uso_tiempo_chart'));
        chart.draw(data, options);
    }*/

    /*Gestion de oportunidades*/
   /* var chartpieinicial = AmCharts.makeChart("donutchart", {
      "responsive": {
        "enabled": true
      },
      "type": "pie",
      "theme": "light",
      "dataProvider": [{
        "title": "Oscar",
        "value": 4852,
        "alpha": 1
      }, {
        "title": "Mayra",
        "value": 9899,
        "alpha": 1
      }, {
        "title": "Laura",
        "value": 9899,
        "alpha": 0.1
      }],
      "titleField": "title",
      "valueField": "value",
      "labelRadius": -130,
      "radius": "42%",
      "innerRadius": "65%",
      "labelText": ""
    });*/

    /*Set de labels en pie chart (Parte del donut chart NO ELIMINAR)*/
    /*chartpieinicial.addListener("rollOverSlice", function(event) {
        var height = $("#donutchart").height();
        event.chart.clearLabels();
        event.chart.addLabel(null, height - (height * 60 / 100), event.dataItem.dataContext.title, "center", 30, null, 0, null, true);
        event.chart.addLabel(null, height - (height * 46 / 100), event.dataItem.dataContext.value, "center", 25, null, 0, null, false);
    });*/
    /*remover labels al retirar el cursor*/
    /*chartpieinicial.addListener( "rollOutSlice", function(event){
        event.chart.clearLabels();
    });*/


    google.charts.load('current', {
        'packages': ['bar']
    });
    google.charts.setOnLoadCallback(drawChart2);

    function drawChart2() {
        var jqxhr = $.ajax({
              url: "/dashboard-produccion-action",
              data: {
                  _token: CSRF_TOKEN,
                  action: 9,
                  fecha_inicio: fecha_inicio,
                  fecha_fin: fecha_fin
              },
              cache: false,
              type: 'POST',
              success: function(e){
                  var data = google.visualization.arrayToDataTable([
                    ['Plazo', 'Alta Millones:', 'Media Millones:', 'Baja Millones:'],
                    ['Corto Plazo', e.data['cortoAlta'], e.data['cortoMedia'], e.data['cortoBaja']],
                    ['Mediano Plazo', e.data['medianoAlta'], e.data['medianoMedia'], e.data['medianoBaja']],
                    ['Largo Plazo', e.data['largoAlta'] , e.data['largoMedia'], e.data['largoBaja']]
                ]);
                var options = {
                    width: '100%',
                    height: 300,
                    chart: {
                        title: '',
                        subtitle: '',
                    },
                    vAxis: {format: 'decimal'}
                };
                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
                chart.draw(data, google.charts.Bar.convertOptions(options));
              }
        });


    }

    /*Fin Charts de Google página inicial*/

    /*Charts Comparativos con imagen*/
    /*var chart = AmCharts.makeChart("top_x_div1", {
        "rotate": true,
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
            "name": "John",
            "points": 35654,
            "color": "#58c1ce",
            "bullet": "https://www.amcharts.com/lib/images/faces/A04.png"
        }, {
            "name": "Damon",
            "points": 65456,
            "color": "#58c1ce",
            "bullet": "https://www.amcharts.com/lib/images/faces/C02.png"
        }, {
            "name": "Patrick",
            "points": 45724,
            "color": "#58c1ce",
            "bullet": "https://www.amcharts.com/lib/images/faces/D02.png"
        }, {
            "name": "Mark",
            "points": 13654,
            "color": "#58c1ce",
            "bullet": "https://www.amcharts.com/lib/images/faces/E01.png"
        }],
        "valueAxes": [{
            "maximum": 80000,
            "minimum": 0,
            "axisAlpha": 0,
            "dashLength": 4,
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
            "bulletOffset": 10,
            "bulletSize": 70,
            "colorField": "color",
            "cornerRadiusTop": 8,
            "customBulletField": "bullet",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "type": "column",
            "valueField": "points"
        }],
        "marginTop": 0,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 0,
        "autoMargins": true,
        "categoryField": "name",
        "categoryAxis": {
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "tickLength": 0
        },
        "export": {
            "enabled": false
        }
    });*/

    /*var chart = AmCharts.makeChart("top_x_div2", {
        "rotate": true,
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
            "name": "John",
            "points": 35654,
            "color": "#0792c6",
            "bullet": "https://www.amcharts.com/lib/images/faces/A04.png"
        }, {
            "name": "Damon",
            "points": 65456,
            "color": "#0792c6",
            "bullet": "https://www.amcharts.com/lib/images/faces/C02.png"
        }, {
            "name": "Patrick",
            "points": 45724,
            "color": "#0792c6",
            "bullet": "https://www.amcharts.com/lib/images/faces/D02.png"
        }, {
            "name": "Mark",
            "points": 13654,
            "color": "#0792c6",
            "bullet": "https://www.amcharts.com/lib/images/faces/E01.png"
        }],
        "valueAxes": [{
            "maximum": 80000,
            "minimum": 0,
            "axisAlpha": 0,
            "dashLength": 4,
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
            "bulletOffset": 10,
            "bulletSize": 70,
            "colorField": "color",
            "cornerRadiusTop": 8,
            "customBulletField": "bullet",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "type": "column",
            "valueField": "points"
        }],
        "marginTop": 0,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 0,
        "autoMargins": true,
        "categoryField": "name",
        "categoryAxis": {
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "tickLength": 0
        },
        "export": {
            "enabled": false
        }
    });*/

    /*var chart = AmCharts.makeChart("top_x_div3", {
        "rotate": true,
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
            "name": "John",
            "points": 35654,
            "color": "#00688f",
            "bullet": "https://www.amcharts.com/lib/images/faces/A04.png"
        }, {
            "name": "Damon",
            "points": 65456,
            "color": "#00688f",
            "bullet": "https://www.amcharts.com/lib/images/faces/C02.png"
        }, {
            "name": "Patrick",
            "points": 45724,
            "color": "#00688f",
            "bullet": "https://www.amcharts.com/lib/images/faces/D02.png"
        }, {
            "name": "Mark",
            "points": 13654,
            "color": "#00688f",
            "bullet": "https://www.amcharts.com/lib/images/faces/E01.png"
        }],
        "valueAxes": [{
            "maximum": 80000,
            "minimum": 0,
            "axisAlpha": 0,
            "dashLength": 4,
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
            "bulletOffset": 10,
            "bulletSize": 70,
            "colorField": "color",
            "cornerRadiusTop": 8,
            "customBulletField": "bullet",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "type": "column",
            "valueField": "points"
        }],
        "marginTop": 0,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 0,
        "autoMargins": true,
        "categoryField": "name",
        "categoryAxis": {
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "tickLength": 0
        },
        "export": {
            "enabled": false
        }
    });*/
    /*Oportunidades Perdidas*/
   /* var chart = AmCharts.makeChart("m_negocios_perdidos", {
        "rotate": true,
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
            "name": "John",
            "points": 35654,
            "color": "#0270c6",
            "bullet": "/images/file/clientes/595ea6fc8ec24.png"
        }, {
            "name": "Damon",
            "points": 65456,
            "color": "#0270c6",
            "bullet": "/images/file/clientes/595ea6fc8ec24.png"
        }, {
            "name": "Patrick",
            "points": 45724,
            "color": "#0270c6",
            "bullet": "/images/file/clientes/595ea6fc8ec24.png"
        }, {
            "name": "Mark",
            "points": 13654,
            "color": "#0270c6",
            "bullet": "/images/file/clientes/595ea6fc8ec24.png"
        }],
        "valueAxes": [{
            "maximum": 80000,
            "minimum": 0,
            "axisAlpha": 0,
            "dashLength": 4,
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
            "bulletOffset": 10,
            "bulletSize": 50,
            "colorField": "color",
            "cornerRadiusTop": 8,
            "customBulletField": "bullet",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "type": "column",
            "valueField": "points"
        }],
        "smoothCustomBullets": {
            "borderRadius": "auto"
        },
        "marginTop": 0,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 0,
        "autoMargins": true,
        "categoryField": "name",
        "categoryAxis": {
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "tickLength": 0
        },
        "export": {
            "enabled": false
        }
    });*/

    /**
     * Función para dar formato a la moneda
     */
    function formato_numero(valor) {
        valor = replaceAll(valor, "$ ", "");
        if (valor != "") {
            valor = replaceAll(valor, ",", "");
            valor = parseFloat(valor);

            if (isNaN(valor)) {
                return '$ 0';
            } else {
                valor = number_format(valor, 0);
                return "$ " + valor;
            }
        } else {
            return '$ 0';
        }
    }

    function number_format(amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    function replaceAll(text, busca, reemplaza) {
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca, reemplaza);
        return text;
    }
