$('body').on('change','.actividad',function(){    

    var jqxhr = $.post( url_basico+"/tipoactividades", $("#conf_actividad").serialize())
    .done(function() {
        mostrar_guardado();
    })
    .fail(function(e) {
        console.error(e)
    })
    .always(function(e) {    
      console.log(e)
    });

   /* $.ajax({
       type: "POST",
       url: url_basico+"/tipoactividades",
       data: $("#conf_actividad").serialize(), // Adjuntar los campos del formulario enviado.
       success: function(data)
       {
           console.log(e.msj); // Mostrar la respuestas del script PHP.
       }
     });*/
});

function guardar_tipo_actividad(){
    var jqxhr = $.post( url_basico+"/tipoactividades", $("#conf_actividad").serialize())
    .done(function() {
        mostrar_guardado();
    })
    .fail(function(e) {
        console.error(e)
    })
    .always(function(e) {    
      console.log(e)
    });
}

$('.porcentaje').keyup(function (){
	//aceptar solo numeros
    this.value = (this.value + '').replace(/[^0-9]/g, '');
    
    //si el valor es diferente a vacio convertirlo en entero
    if((this.value)!=''){
    	this.value = parseInt(this.value);	
    }
    
    //No dejar que pase de 100%
    if(($(this).val())>100){
    	$(this).val('100 %');
    }else if(($(this).val())==''){
    	$(this).val('0 %');
    }else{
    	$(this).val((this.value)+' %');
    }

    //No poner mas porcentaje de lo necesario
    id=$(this).attr("id");
    if(id=='porcentaje1'){
    	porcentaje2=$('#porcentaje2').val();
    	if(porcentaje2==''){
    		porcentaje2=0;
    	}
    	porcentaje2=parseInt(porcentaje2);

    	porcentaje3=$('#porcentaje3').val();
    	if(porcentaje3==''){
    		porcentaje3=0;
    	}
    	porcentaje3=parseInt(porcentaje3);

    	faltante=100-(porcentaje2+porcentaje3);

    	if((parseInt(this.value))>faltante){
    		$(this).val(faltante+' %');
    	}
    }
    if(id=='porcentaje2'){
    	porcentaje1=$('#porcentaje1').val();
    	if(porcentaje1==''){
    		porcentaje1=0;
    	}
    	porcentaje1=parseInt(porcentaje1);

    	porcentaje3=$('#porcentaje3').val();
    	if(porcentaje3==''){
    		porcentaje3=0;
    	}
    	porcentaje3=parseInt(porcentaje3);

    	faltante=100-(porcentaje1+porcentaje3);

    	if((parseInt(this.value))>faltante){
    		$(this).val(faltante+' %');
    	}
    }
    if(id=='porcentaje3'){
    	porcentaje1=$('#porcentaje1').val();
    	if(porcentaje1==''){
    		porcentaje1=0;
    	}
    	porcentaje1=parseInt(porcentaje1);

    	porcentaje2=$('#porcentaje2').val();
    	if(porcentaje2==''){
    		porcentaje2=0;
    	}
    	porcentaje2=parseInt(porcentaje2);

    	faltante=100-(porcentaje1+porcentaje2);

    	if((parseInt(this.value))>faltante){
    		$(this).val(faltante+' %');
    	}
    }
});

$('.dias').keyup(function (){
    //aceptar solo numeros
    this.value = (this.value + '').replace(/[^0-9]/g, '');

    id=$(this).attr("id");
    if(id=='cantidad1' || id=='cantidad2'){
        cantidad1=$('#cantidad1').val();
        if(cantidad1==''){
            cantidad1=0;
        }
        cantidad1=parseInt(cantidad1);

        cantidad2=$('#cantidad2').val();
        if(cantidad2==''){
            cantidad2=0;
        }
        cantidad2=parseInt(cantidad2);

        if(cantidad1>=cantidad2){
            $('#cantidad2').val((cantidad1+1));
        }
        cantidad3=$('#cantidad2').val();
        $('#cantidad3').val('Mayor a '+cantidad3);
    }
});

$(function () {
  $(".valor").maskMoney();
})

$('body').on('change','.formulariodashboard',function(){

    event.preventDefault();    
  var datos = new FormData($("#formulario_conf_dashboard")[0]);
   $.ajax({
        url: url_basico+"/save/dashboard",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(e){
          console.log(e.msj);
          mostrar_guardado();
        }
      });
    
});

$('body').on('click','.add-actividad',function(){
    numero_asignado=($('.act').length)+1;
    $.ajax({
        url: url_basico+"/nuevotipoactividades/"+numero_asignado,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            numero=$('#fila_act').val();
            html=`<div class="row act" id="actividad_insertada`+numero+`">
                    <input type="hidden" name="actividad[`+numero+`][id]" value="`+e.id+`">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="orden`+numero+`">Orden</label>
                            <input type="text" style="text-align: center;" id="orden`+numero+`" placeholder="&#xf03c; Numero" class="form-control solonumero numero valor_id_`+e.id+`" name="actividad[`+numero+`][orden]" value="`+numero_asignado+`">
                        </div>
                    </div>
                    <div class="col-md-9" style="margin-top: 2%;">
                        <div class="panel-group" id="actividades" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default colapsable">
                            <div class="panel-heading colapsable2" role="tab" id="titulo`+numero+`">
                              <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#actividades" id="titulo_nombre`+numero+`" href="#collapse`+numero+`" aria-expanded="true" aria-controls="collapse`+numero+`">
                                  Actividad 
                                </a>
                              </h4>
                            </div>
                            <div id="collapse`+numero+`" class="panel-collapse collapse" role="tabpanel" aria-labelledby="titulo`+numero+`">
                              <div class="panel-body">
                                <div class="row">                                   
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="concepto`+numero+`">Concepto</label>
                                            <input type="text" id="concepto`+numero+`" placeholder="&#xf0f6; Concepto" class="form-control actividad titulo" name="actividad[`+numero+`][concepto]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="descripcion`+numero+`">Descripcion</label>
                                            <textarea class="form-control actividad" id="descripcion`+numero+`" name="actividad[`+numero+`][descripcion]" rows="8" placeholder="&#xf0f6; Descripcion"></textarea>
                                        </div>
                                    </div>  
                                </div>  
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-1" style="margin-top: 2%;">
                        <a class="btn btn-danger remove-actividad" onclick="remove_actividad(`+e.id+`,'`+numero+`')" id='`+numero+`' data-toggle="tooltip" data-original-title="Eliminar Actividad"><i style="color:#fff;" class="fa fa-minus" aria-hidden="true"></i></a>
                    </div>
                </div>`;
            $('#conf_actividad').prepend(html);
            numero++;
            $('#fila_act').val(numero);
            $('.numero').keyup(function (){
                //aceptar solo numeros
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });
            mostrar_guardado();
        }
    });
});

function remove_actividad(id,id_fila){
    swal({
          title: 'Esta seguro?',
          text: "Va a eliminar la actividad",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
          $.ajax({
                url: url_basico+"/eliminartipoactividades/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    
                    $('#actividad_insertada'+id_fila).remove();
                    //quitar el tooltip que queda
                    $('.bs-tether-element').remove();
                    swal("Eliminado!", e.msj, "success");

                    $.ajax({
                        url: url_basico+"/cambiarnumeroorden",
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'GET',
                        success: function(e){
                              tipos=(e.tipos);
                              $.each(tipos, function (k, item) {
                              $('.valor_id_'+item.id).val(item.orden);
                               });
                        }
                    });
                }
              });
          
        });
}

$('.numero').keyup(function (){
    //aceptar solo numeros
    this.value = (this.value + '').replace(/[^0-9]/g, '');
});

$('body').on('change','.numero',function(){

    numero=$(this).val();
    numero=parseInt(numero);

    contar=$('#conf_actividad .act').length;

    error='no';
    if(numero==0){
        $(this).val('');
        error='si';
    }else if(numero>contar){
        $(this).val('');
        swal("Alerta!", "Solo hay "+contar+" actividad(es) el numero debe ser menor");
        error='si';
    }else{
        y=$('#fila_act').val();
        for(i=0;i<=y;i++){
            id1="orden"+i;
            id2=$(this).attr('id');
            if ( ($("#orden"+i).length > 0 ) && (id1!=id2) ) {
              // hacer algo aquí si el elemento existe
              if(($("#orden"+i).val())==numero){
                swal("Alerta!", "El número ya lo tiene asignado una actividad");
                $(this).val('');
                error='si';
              }
             /* if(($("#orden"+i).val())>=numero){
                valor=parseInt($("#orden"+i).val());
                $("#orden"+i).val(valor+1);
              } */

            }
        }
    }

    if(error=='no'){
        var jqxhr = $.post( url_basico+"/tipoactividades", $("#conf_actividad").serialize())
        .done(function() {
            mostrar_guardado();
        })
        .fail(function(e) {
            console.error(e)
        })
        .always(function(e) {    
          console.log(e)
        });
    }
})

$('body').on('change','.titulo',function(){
    tittle=$(this).val();
    nume=($(this).attr('id')).replace('concepto', '');
    $('#titulo_nombre'+nume).html(tittle);
});

$('body').on('change','.g_ciclos',function(){
    event.preventDefault();    
      var datos = new FormData($("#formulario_ciclos_ventas")[0]);
      
       $.ajax({
            url: url_basico+"/save/ciclosventas",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(e){
              console.log(e.msj);
              mostrar_guardado();
            }
          });
});

$('body').on('click','.btn-ok',function(){
    if( $('#editarPais').is(":visible") ){
        setTimeout(guardar_bandera, 1000);
    }else if( $('#conf_3').is(":visible") ){
        setTimeout(guardar_ciclos, 1000);
    }else{
        setTimeout(guardar_tipo_actividad, 1000);  
    }

});

function guardar_ciclos(){
     
  var datos = new FormData($("#formulario_ciclos_ventas")[0]);
  
   $.ajax({
        url: url_basico+"/save/ciclosventas2",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(e){
          console.log(e.msj);
          ciclos=(e.ciclos);
          $.each(ciclos, function (k, item) {
            if((item.imagen)!='' && (item.imagen)!=null){
                $('#imagen_ciclo'+item.id).attr('src',(url_basico+'/'+item.imagen));
            }
                
                console.log(item.imagen);
                mostrar_guardado();
            });
        }
      });
}


$('.dias_transicion').keyup(function (){
    //aceptar solo numeros
    this.value = (this.value + '').replace(/[^0-9]/g, '');
});

$('.porcen_ciclos').keyup(function (){
    //aceptar solo numeros
    this.value = (this.value + '').replace(/[^0-9]/g, '');

    if(($(this).val())>100){
        $(this).val('100');
    }
});

$('.nombre_ciclos').keyup(function(){
    tittle=$(this).val();
    nume=($(this).attr('id')).replace('nombre_ciclos', '');
    $('#titulo_nombre_ciclos'+nume).html(tittle);
});

$('.dias_transicion').keyup(function(){
    tittle=$(this).val();
    nume=($(this).attr('id')).replace('diastransicion', '');
    $('#titulo_dias_ciclos'+nume).html('Tiempo de transición: '+tittle+' dias');
});

$('.porcen_ciclos').keyup(function(){
    tittle=$(this).val();
    nume=($(this).attr('id')).replace('porcentajeciclos', '');
    $('#titulo_porcentaje_ciclos'+nume).html(tittle+'%');
});

$('body').on('click','.add-ciclo',function(){
    $.ajax({
        url: url_basico+"/nuevocicloventa",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
          
            numero=$('#fila_ciclo').val();
            html=`<div class="row" id="numero_ciclo`+numero+`">
                    <div class="col-md-11">
                        <div class="panel panel-default cabecero_colp2" style="margin-bottom: 2%">
                            <div class="panel-heading" role="tab" id="heading`+numero+`">
                              <h4 class="panel-title">
                                <div class="row collapsed" role="button" data-toggle="collapse" data-parent="#accordion-ciclo" href="#ciclo`+numero+`" aria-expanded="false" aria-controls="ciclo`+numero+`">
                                    <div class="col-md-1">
                                        <img id="imagen_ciclo`+e.id+`" src="`+url_basico+`/images/noimage.png" style="border: 0 !important;margin-bottom: 1%;width: 80%;text-align: left;">
                                    </div>
                                    <div class="col-md-9" style="text-align: left;">
                                        <span style="font-size: 21px;font-weight: bold;" id="titulo_nombre_ciclos`+numero+`">Nuevo Ciclo</span><br>
                                        <span id="titulo_dias_ciclos`+numero+`">Tiempo de transición: 0 dias</span>
                                    </div>
                                    <div class="col-md-2" style="text-align: right;">
                                        <span style="font-size: 33px;text-shadow: 3px 1px 2px #ccc;" id="titulo_porcentaje_ciclos`+numero+`">0%</span>
                                    </div>
                                </div>
                              </h4>
                            </div>
                            <div id="ciclo`+numero+`" class="panel-collapse collapse" style="background-color: #555555;" role="tabpanel" aria-labelledby="heading`+numero+`">
                              <div class="panel-body">
                                <input type="hidden" name="ciclos[`+numero+`][id]" value="`+e.id+`">
                                <div class="row"> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nombre_ciclos`+numero+`" style="color: #fff !important;">Nombre</label>
                                            <input type="text" class="form-control ciclos g_ciclos nombre_ciclos" name="ciclos[`+numero+`][nombre]" id="nombre_ciclos`+numero+`" placeholder="Nombre">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="diastransicion`+numero+`" style="color: #fff !important;">Dias de Transición</label>
                                            <input type="text" class="form-control ciclos g_ciclos dias_transicion" name="ciclos[`+numero+`][dias]" id="diastransicion`+numero+`" placeholder="Dias de Transición">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="porcentajeciclos`+numero+`" style="color: #fff !important;">Porcentaje</label>
                                            <input type="text" class="form-control ciclos g_ciclos porcen_ciclos" name="ciclos[`+numero+`][porcentaje]" id="porcentajeciclos`+numero+`" placeholder="Porcentaje" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center;">
                                          <div class="dropzone imagen-perfil" style="height: 100px; width:100px; max-height: 100px; min-height: 100px;" data-width="32" data-height="32" data-ajax="false" data-originalsave="true">
                                            <input type="file" name="ciclos[`+numero+`][imagen]" accept="image/gif, image/jpeg, image/png">
                                          </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <a class="btn btn-danger remove-ciclo" onclick="remove_ciclo(`+e.id+`,'`+numero+`')" data-toggle="tooltip" data-original-title="Eliminar Ciclo"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
                    </div>
                </div>`;
            $('#accordion-ciclo').prepend(html);
            numero++;
            mostrar_guardado();
            $('#fila_ciclo').val(numero);
            $('.dias_transicion').keyup(function (){
                //aceptar solo numeros
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });
            $('.porcen_ciclos').keyup(function (){
                //aceptar solo numeros
                this.value = (this.value + '').replace(/[^0-9]/g, '');

                if(($(this).val())>100){
                    $(this).val('100');
                }
            });

            $('.nombre_ciclos').keyup(function(){
                tittle=$(this).val();
                nume=($(this).attr('id')).replace('nombre_ciclos', '');
                $('#titulo_nombre_ciclos'+nume).html(tittle);
            });

            $('.dias_transicion').keyup(function(){
                tittle=$(this).val();
                nume=($(this).attr('id')).replace('diastransicion', '');
                $('#titulo_dias_ciclos'+nume).html('Tiempo de transición: '+tittle+' dias');
            });

            $('.porcen_ciclos').keyup(function(){
                tittle=$(this).val();
                nume=($(this).attr('id')).replace('porcentajeciclos', '');
                $('#titulo_porcentaje_ciclos'+nume).html(tittle+'%');
            });
            $('.dropzone').html5imageupload();
        }
      });
});

function remove_ciclo(id,id_fila){

    swal({
          title: 'Esta seguro?',
          text: "Va a eliminar el ciclo",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
          $.ajax({
                url: url_basico+"/eliminarciclo/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    
                    $('#numero_ciclo'+id_fila).remove();
                    //quitar el tooltip que queda
                    $('.bs-tether-element').remove();
                    swal("Eliminado!", e.msj, "success");
                }
              });
        });

}


$('.parafiscales_hh').keyup(function (){
    //aceptar solo numeros
    this.value = (this.value + '').replace(/[^0-9]/g, '');

    if(($(this).val())>100){
        $(this).val('100 %');
    }else if(($(this).val())==''){
        $(this).val('0 %');
    }else{
        porcentaje=$(this).val();
        $(this).val(porcentaje+'%');
    }
});

$('body').on('change','.hh',function(){
    event.preventDefault();    
      var datos = new FormData($("#formulario_horas_hombre")[0]);
       $.ajax({
            url: url_basico+"/save/horashombre",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(e){
              console.log(e.msj);
              mostrar_guardado();
            }
          });
});


$('body').on('change','.cargo_user',function(){
    event.preventDefault();    
      var datos = new FormData($("#formulario_usuarios")[0]);
       $.ajax({
            url: url_basico+"/save/usuarios",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(e){
              console.log(e.msj);
              mostrar_guardado();
            }
          });
});



$('body').on('click','.add-cargo',function(){
    $.ajax({
        url: url_basico+"/nuevocargo",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
          
            numero=$('#total_cargos').val();
            html=`<div class="col-md-12" id="cargo_insert`+numero+`"><input type="hidden" name="usuarios_cargo[`+numero+`][id]" value="`+e.id+`">
                    <div class="row">                                   
                        <div class="col-md-11">
                            <div class="form-group">
                                <label for="nombre_cargo`+numero+`">Nombre</label>
                                <input type="text" id="nombre_cargo`+numero+`" placeholder="&#xf007; Cargo" class="form-control cargo_user" name="usuarios_cargo[`+numero+`][nombre]" value="">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="btn btn-danger remove-cargo" onclick="remove_cargo(`+e.id+`,'`+numero+`')" data-toggle="tooltip" data-original-title="Eliminar Cargo"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
                        </div>
                    </div></div>`;
            $('#contenedor_cargos').prepend(html);
            numero++;
            $('#total_cargos').val(numero);
            mostrar_guardado();
        }
      });
});


function remove_cargo(id,id_fila){
    swal({
          title: 'Esta seguro?',
          text: "Va a eliminar el cargo",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
          $.ajax({
                url: url_basico+"/eliminarcargo/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    
                    $('#cargo_insert'+id_fila).remove();
                    //quitar el tooltip que queda
                    $('.bs-tether-element').remove();
                    swal("Eliminado!", e.msj, "success");
                }
              });
        });
}


$('body').on('change','.emergencia_user',function(){
    event.preventDefault();    
      var datos = new FormData($("#formulario_usuarios")[0]);
       $.ajax({
            url: url_basico+"/save/usuarios",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(e){
              console.log(e.msj);
              mostrar_guardado();
            }
          });
});

$('body').on('click','.add-emercencia',function(){
    $.ajax({
        url: url_basico+"/nuevaemergencia",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
          
            numero=$('#total_emergencias').val();
            html=`<div class="col-md-12" id="emergencia_insert`+numero+`">
                    <input type="hidden" name="usuarios_emeregencia[`+numero+`][id]" value="`+e.id+`">
                    <div class="row">                                   
                        <div class="col-md-11">
                            <div class="form-group">
                                <label for="nombre_emergencia`+numero+`">Nombre</label>
                                <input type="text" id="nombre_emergencia`+numero+`" placeholder="&#xf0f9; Emergencia" class="form-control emergencia_user" name="usuarios_emeregencia[`+numero+`][nombre]" value="">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="btn btn-danger remove-emergencia" onclick="remove_emergencia(`+e.id+`,'`+numero+`')" data-toggle="tooltip" data-original-title="Eliminar Emergencia"><i class="fa fa-minus" aria-hidden="true" style="color: #fff;"></i></a>
                        </div>
                    </div>
                </div>`;
            $('#contenedor_emergencias').prepend(html);
            numero++;
            $('#total_emergencias').val(numero);
            mostrar_guardado();
        }
      });
});

function remove_emergencia(id,id_fila){
    swal({
          title: 'Esta seguro?',
          text: "Va a eliminar el registro de emergencia",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
          $.ajax({
                url: url_basico+"/eliminaremergencia/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    
                    $('#emergencia_insert'+id_fila).remove();
                    //quitar el tooltip que queda
                    $('.bs-tether-element').remove();
                    swal("Eliminado!", e.msj, "success");
                }
              });
        });
}

function editar_pais(id,nombre){
    $('#editarPais').modal('show');
    $('#id_pais_editar').val(id);
    $('#nombre_pais').html(nombre);
}

function guardar_bandera(){
    var datos = new FormData($("#formulario_pais")[0]);
  
   $.ajax({
        url: url_basico+"/save/pais",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(e){
          console.log(e.msj);
          $('#bandera'+e.id).attr('src',url_basico+'/images/icons/'+e.imagen);
          $('.bandera'+e.id).css('display','block');
          mostrar_guardado();
        }
      });
}


$('body').on('click','.im_ban',function(){
    $('.btn-del').click();
});

function remove_bandera(id,clase_id){
    swal({
          title: 'Esta seguro?',
          text: "Va a eliminar una bandera",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
          $.ajax({
            url: url_basico+"/eliminarbandera/"+id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                console.log(e.msj);
                $('#'+clase_id).attr('src',e.imagen);
                $('.'+clase_id).css('display','none');
            }
          });
        });
}

mostrar_guardado = function(){
    $('#guardado-exitoso').css('display','block');
    setTimeout('ocultar_guardado()',5000)
}

ocultar_guardado = function(){
    $('#guardado-exitoso').css('display','none');
}
consulta_menu=0;
$('body').on('click','#configurar_permisos',function(){
    if(consulta_menu == 0){
       $.ajax({
        url: url_basico+"/consultar_menu",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('.permisos').html(e.content);
            $('.cargando').remove();
        }
        });
    }
});

$('body').on('click','.permisos-accesos li',function(){
    submodulo=$(this).attr('id');
    submodulo=submodulo.replace('p_','');
    submodulo=submodulo.replace('_',' ');
    $('#permiso_actual').html(submodulo);
});

$('body').on('click','.btn-subitem',function(){
   id=$(this) .attr('id');
   id=id.replace('menu_','');
    $.ajax({
        url: url_basico+"/consultar_subitem_menu/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
              title: '<i>'+e.menu.item+'</i>',
              type: 'info',
              html:e.content,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              confirmButtonText:
                '<i class="fa fa-floppy-o"></i> Guardar',
              cancelButtonText:
              '<i class="fa fa-times-circle"></i> Cancelar'
            }).then(function () {
                if($('#subitem_item').val() == "" || $('#subitem_url').val() == ""){
                   swal(
                      'Error',
                      'Debe llenar los campos',
                      'error'
                    );
                   }else{
                        var CSRF_TOKEN = $('input[name=_token]').val();
                        var jqxhr = $.ajax({
                            url: url_basico+"/guardarsubitem",
                            data: {
                                _token: CSRF_TOKEN,
                                id: $('#subitem_id').val(),
                                item: $('#subitem_item').val(),
                                url: $('#subitem_url').val(),
                                dir: url_basico
                            },
                            cache: false,
                            type: 'POST',
                            success: function(e){
                               swal(
                                  'Guardado',
                                  e.msj,
                                  'success'
                                );
                            }
                        });
                   }
            });
            $('.swal2-modal').addClass('SubItems-modal');
        }
    });
});

$('body').on('click','.btn-eliminar-subitem',function(){
   id=$(this) .attr('id');
    id=id.replace('subitemeliminar_','');
    swal(
      'Esta seguro?',
      'Desea eliminar este subitem',
      'question'
    ).then(function(){
        $.ajax({
        url: url_basico+"/eliminarsubitem/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal(
              'Eliminado',
              e.msj,
              'success'
            );
        }
        });
    });
});

function editarsubitem(id,id_padre,item_padre,item,url){
    $.ajax({
        url: url_basico+"/itemsprincipales/"+id_padre,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            items_principales=e.content;
            swal({
                  title: '<i>'+item_padre+'</i><br>',
                  type: 'info',
                  html:'<div class="row"><div class="col-md-4"><label>Item principal</label><br>'+items_principales+'</div><div class="col-md-4"><label>Nombre del subitem</label><br><input type="text" class="form-control" id="subitemeditar_item" value="'+item+'"></div><div class="col-md-4"><label>URL</label><br><input type="text" class="form-control" id="subitemeditar_url" value="'+url+'"></div></div>',
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-floppy-o"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times-circle"></i> Cancelar'
                }).then(function () {
                    if($('#subitemeditar_item').val() == "" || $('#subitemeditar_url').val() == ""){
                       swal(
                          'Error!',
                          'Hay campos vacios',
                          'error'
                        ).then(function () {
                           editarsubitem(id,id_padre,item_padre,item,url);
                       });
                       }else{
                            var CSRF_TOKEN = $('input[name=_token]').val();
                            var jqxhr = $.ajax({
                                url: url_basico+"/editarsubitem",
                                data: {
                                    _token: CSRF_TOKEN,
                                    id: id,
                                    sub_item: $('#subitemeditarsub_item').val(),
                                    item: $('#subitemeditar_item').val(),
                                    url: $('#subitemeditar_url').val()
                                },
                                cache: false,
                                type: 'POST',
                                success: function(e){
                                   swal(
                                      'Guardado',
                                      e.msj,
                                      'success'
                                    );
                                }
                            });
                       }
                });
                $('.swal2-modal').addClass('SubItems-modal');
        }
        });
}

function editarmenuitem(id){
    $.ajax({
        url: url_basico+"/consultariconos/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
              title: '<i>Editar Item</i>',
              type: 'info',
              html:e.content,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              confirmButtonText:
                    '<i class="fa fa-floppy-o"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times-circle"></i> Cancelar'
            }).then(function () {
                if($('#editarmenuitem_logo').val() == "" || $('#editarmenuitem_item').val() == ""){
                    swal(
                      'Error!',
                      'Hay campos vacios',
                      'error'
                    ).then(function () {
                       editarmenuitem(id);
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: url_basico+"/editaritem",
                        data: {
                            _token: CSRF_TOKEN,
                            id: id,
                            logo: $('#editarmenuitem_logo').val(),
                            item: $('#editarmenuitem_item').val(),
                            url: $('#editarmenuitem_url').val()
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                           swal(
                              'Guardado',
                              e.msj,
                              'success'
                            ).then(function(){
                               recargartabla();
                           });
                        }
                    });
                }
            });
        }
        });

}

$('body').on('change','#editarmenuitem_logo',function(){
    html='<i class="'+$(this).val()+'"></i>';
    $('.icono_elegido').html(html);
    $('.escoger_icono_elegido').css('display','none');
    $('.ver_icono_elegido').css('display','block');
});

$('body').on('click','.editar_icono_elegido',function(){
    $('.escoger_icono_elegido').css('display','block');
    $('.ver_icono_elegido').css('display','none');
});

function recargartabla(){
    $.ajax({
        url: url_basico+"/consultar_menu",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            $('.permisos').html(e.content);
        }
    });
}

$('body').on('click','.btn-eliminar',function(){
    id=$(this).attr('id');
    id=id.replace('eliminar_item_','');
    swal(
          'Esta Seguro?',
          'Va a eliminar un item',
          'question'
        ).then(function(){
            $.ajax({
                url: url_basico+"/eliminar_itemmenu/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    swal(
                          'Eliminado',
                          e.msj,
                          'success'
                        ).then(function(){
                            recargartabla();
                        });
                }
            });
        });
});

$('body').on('click','.crear-menu',function(){
    $.ajax({
        url: url_basico+"/nuevoitemmenu",
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
                  title: '<i>Crear Item</i>',
                  type: 'info',
                  html:e.content,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                        '<i class="fa fa-floppy-o"></i> Guardar',
                      cancelButtonText:
                      '<i class="fa fa-times-circle"></i> Cancelar'
                }).then(function(){
                    if($('#editarmenuitem_logo').val() == "" || $('#editarmenuitem_item').val() == ""){
                        swal(
                              'Error!',
                              'Hay campos vacios',
                              'error'
                            ).then(function () {
                               $('.crear-menu').click();
                           });
                       }else{
                            var CSRF_TOKEN = $('input[name=_token]').val();
                            var jqxhr = $.ajax({
                                url: url_basico+"/guardaritem",
                                data: {
                                    _token: CSRF_TOKEN,
                                    logo: $('#editarmenuitem_logo').val(),
                                    item: $('#editarmenuitem_item').val(),
                                    url: $('#editarmenuitem_url').val()
                                },
                                cache: false,
                                type: 'POST',
                                success: function(e){
                                   swal(
                                      'Guardado',
                                      e.msj,
                                      'success'
                                    ).then(function(){
                                       recargartabla();
                                   });
                                }
                            });
                       }
                });
        }
    });

});

function bajar(id){
    $.ajax({
        url: url_basico+"/bajarposicionitemmenu/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            console.log(e.msj);
            recargartabla();
        }
    });
}

function subir(id){
    $.ajax({
        url: url_basico+"/subirposicionitemmenu/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            console.log(e.msj);
            recargartabla();
        }
    });
}

function crearpermiso(id,item){
    $.ajax({
        url: url_basico+"/crearpermisoitem/"+id,
        cache: false,
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(e){
            swal({
              title: '<i>Crear Permisos '+item+'</i>',
              type: 'info',
              html:e.content,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              confirmButtonText:
                    '<i class="fa fa-floppy-o"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times-circle"></i> Cancelar'
            }).then(function(){
                var datos = new FormData($("#formulario_permisosmenu")[0]);
                $.ajax({
                    url: url_basico+'/guardarpermisoitemmenu',
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: datos,
                    success: function(e) {
                       swal(
                          'Guardado',
                          e.msj,
                          'success'
                        );
                    }
                });
            });
        }
    });

}

$('body').on('change','.p_item_menu',function(){
   id=$(this).attr('id');
    valor=$('#hidden'+id).val();
    if(valor=="Si"){
        $('#hidden'+id).val("No");
    }else{
        $('#hidden'+id).val("Si");
    }
});

