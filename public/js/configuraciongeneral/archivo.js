$('.dropzone').html5imageupload();

/*Función para realizar cualquier acción de permisos (CREAR EDITAR ELIMINAR FILTRAR LISTAR)*/
function permisos_accion(accion) {
    switch (accion) {
        case 'lista_modulos':
        	var CSRF_TOKEN = $('input[name=_token]').val();
		    var jqxhr = $.ajax({
		        url: "/configuracion-accion",
		        data: {
		            _token: CSRF_TOKEN,
		            accion: 0
		        },
		        cache: false,
		        type: 'POST',
		        success: function(e){
                    $('#listado_modulos').html(e.data['html']);
                    $('[data-toggle="tooltip"]').tooltip();
		        }
		    });
            break;
        case 'lista_cargo':
            var CSRF_TOKEN = $('input[name=_token]').val();
		    var jqxhr = $.ajax({
		        url: "/configuracion-accion",
		        data: {
		            _token: CSRF_TOKEN,
		            accion: 2
		        },
		        cache: false,
		        type: 'POST',
		        success: function(e){
                    $('#listado_cargos_permisos').html(e.data['html']);
                    $('[data-toggle="tooltip"]').tooltip();
		        }
		    });
            break;
        case 'lista_acceso':
            var CSRF_TOKEN = $('input[name=_token]').val();
		    var jqxhr = $.ajax({
		        url: "/configuracion-accion",
		        data: {
		            _token: CSRF_TOKEN,
		            accion: 4
		        },
		        cache: false,
		        type: 'POST',
		        success: function(e){
                    $('#chart4').html(e.data['html']);
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.collapse').collapse();
		        }
		    });
            break;
    }
}

/**
 * Función para realizar las acciones de los módulos como crear-editar
 * @param STRING hacer ACCION A REALIZAR
 * @param STRING nombre NOMBRE DEL MODULO A EDITAR
 * @param INT id     IDENTIFICACIÓN DE LA TABLA permiso_modulos
 */
function modulo_permisos(hacer,nombre,id){
    switch(hacer){
        case 'crear':
            swal({
                  title: 'Crear Modulo',
                  html:`<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><strong>Nombre del Modulo</strong></p>
                            <input type="text" class="form-control" id="nombre_modulo" onkeyup="max_campo(this.value,250,'#texto_nombre_modulo',this.id)">
                            <p class="text-right invisible" id="texto_nombre_modulo"><a class="text-success"><i class="fa fa-pencil"></i>... 0/250</a></p>
                        </div>
                    </div>
                </div>`,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-save"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times"></i> Cancelar'
                }).then(function(){
                if($('#nombre_modulo').val() == ''){
                   swal({
                      type: 'error',
                      title: 'Alerta',
                      text: 'Debe ingresar un nombre para el módulo',
                    }).then(function(){
                        modulo_permisos('crear');
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "/configuracion-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            accion: 1,
                            modulo:$('#nombre_modulo').val()
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            if(e.data['msj'] == 'Guardado correctamente!'){
                               swal('Guardado',e.data['msj'],'success');
                            }else{
                                swal('Error',e.data['msj'],'error');
                            }
                            permisos_accion('lista_modulos');
                        }
                    });
                }
            });
            break;
        case 'editar':
            swal({
                  title: 'Editar Modulo',
                  html:`<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><strong>Nombre del Modulo</strong></p>
                            <input type="text" class="form-control" id="nombre_modulo_editar" value="`+nombre+`" onkeyup="max_campo(this.value,250,'#texto_nombre_modulo_editar',this.id)">
                            <p class="text-right invisible" id="texto_nombre_modulo_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 0/250</a></p>
                        </div>
                    </div>
                </div>`,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-save"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times"></i> Cancelar'
                }).then(function(){
                if($('#nombre_modulo_editar').val() == ''){
                   swal({
                      type: 'error',
                      title: 'Alerta',
                      text: 'Debe ingresar un nombre para el módulo',
                    }).then(function(){
                        modulo_permisos('editar',nombre,id);
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "/configuracion-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            accion: 1,
                            modulo:$('#nombre_modulo_editar').val(),
                            id_modulo:id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            if(e.data['msj'] == 'Guardado correctamente!'){
                               swal('Guardado',e.data['msj'],'success');
                            }else{
                                swal('Error',e.data['msj'],'error');
                            }
                            permisos_accion('lista_modulos');
                        }
                    });
                }
            });
            break;
    }
}

/**
 * Función para realizar las acciones de los cargos como crear-editar
 * @param STRING hacer ACCION A REALIZAR
 * @param STRING nombre NOMBRE DEL CARGO A EDITAR
 * @param INT id     IDENTIFICACIÓN DE LA TABLA permiso_modulos
 */
function cargo_permisos(hacer,nombre,id){
    switch(hacer){
        case 'crear':
            swal({
                  title: 'Crear Cargo',
                  html:`<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><strong>Nombre del Cargo</strong></p>
                            <input type="text" class="form-control" id="nombre_cargo" onkeyup="max_campo(this.value,250,'#texto_nombre_cargo',this.id)">
                            <p class="text-right invisible" id="texto_nombre_cargo"><a class="text-success"><i class="fa fa-pencil"></i>... 0/250</a></p>
                        </div>
                    </div>
                </div>`,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-save"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times"></i> Cancelar'
                }).then(function(){
                if($('#nombre_cargo').val() == ''){
                   swal({
                      type: 'error',
                      title: 'Alerta',
                      text: 'Debe ingresar un cargo para ser guardado',
                    }).then(function(){
                        cargo_permisos('crear');
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "/configuracion-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            accion: 3,
                            cargo:$('#nombre_cargo').val()
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            if(e.data['msj'] == 'Guardado correctamente!'){
                               swal('Guardado',e.data['msj'],'success');
                            }else{
                                swal('Error',e.data['msj'],'error');
                            }
                            permisos_accion('lista_cargo');
                        }
                    });
                }
            });
            break;
        case 'editar':
            swal({
                  title: 'Editar Cargo',
                  html:`<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><strong>Nombre del Cargo</strong></p>
                            <input type="text" class="form-control" id="nombre_cargo_editar" value="`+nombre+`" onkeyup="max_campo(this.value,250,'#texto_nombre_cargo_editar',this.id)">
                            <p class="text-right invisible" id="texto_nombre_cargo_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 0/250</a></p>
                        </div>
                    </div>
                </div>`,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-save"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times"></i> Cancelar'
                }).then(function(){
                if($('#nombre_cargo_editar').val() == ''){
                   swal({
                      type: 'error',
                      title: 'Alerta',
                      text: 'Debe ingresar un nombre para el cargo',
                    }).then(function(){
                        cargo_permisos('editar',nombre,id);
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "/configuracion-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            accion: 3,
                            cargo:$('#nombre_cargo_editar').val(),
                            id_cargo:id
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            if(e.data['msj'] == 'Guardado correctamente!'){
                               swal('Guardado',e.data['msj'],'success');
                            }else{
                                swal('Error',e.data['msj'],'error');
                            }
                            permisos_accion('lista_cargo');
                        }
                    });
                }
            });
    }
}

/**
 * Función para traer el listado de accesos que estan relacionados con el módulo
 * @param INT id            IDENTIFICACIÓN DE LA TABLA permiso_modulos
 * @param STRING id_contenedor IDENTIFICACIÓN DEL CONTENEDOR
 */
function lista_accesos(id,id_contenedor){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/configuracion-accion",
        data: {
            _token: CSRF_TOKEN,
            accion: 5,
            id_modulo: id,
            id_contenedor:id_contenedor
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#'+id_contenedor+' .card-body').html(e.data['html']);
            $('[data-toggle="tooltip"]').tooltip();
            $('.collapse').collapse();
        }
    });
}

/**
 * Función para realizar las acciones de los accessos como crear-editar
 * @param STRING hacer     ACCION A REALIZAR
 * @param INT id_modulo IDENTIFICACIÓN DE LA TABLA permiso_modulos
 * @param STRING id_contenedor IDENTIFICACIÓN DEL CONTENEDOR
 * @param STRING acceso    DESCRIPCIÓN DEL ACCESO
 * @param INT id_acceso IDENTIFICACIÓN DE LA TABLA permiso_accesos
 */
function acceso_permisos(hacer,id_modulo,id_contenedor,acceso,id_acceso){
    switch(hacer){
        case 'crear':
            swal({
                  title: 'Crear Acceso',
                  html:`<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><strong>Nombre del Acceso</strong></p>
                            <input type="text" class="form-control" id="nombre_acceso" onkeyup="max_campo(this.value,250,'#texto_nombre_acceso',this.id)">
                            <p class="text-right invisible" id="texto_nombre_acceso"><a class="text-success"><i class="fa fa-pencil"></i>... 0/250</a></p>
                        </div>
                    </div>
                </div>`,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-save"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times"></i> Cancelar'
                }).then(function(){
                if($('#nombre_acceso').val() == ''){
                   swal({
                      type: 'error',
                      title: 'Alerta',
                      text: 'Debe ingresar un nombre para el acceso',
                    }).then(function(){
                        acceso_permisos('crear',id_modulo,id_contenedor);
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "/configuracion-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            accion: 6,
                            acceso:$('#nombre_acceso').val(),
                            id_modulo:id_modulo
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            if(e.data['msj'] == 'Guardado correctamente!'){
                               swal('Guardado',e.data['msj'],'success');
                            }else{
                                swal('Error',e.data['msj'],'error');
                            }
                            lista_accesos(id_modulo,id_contenedor);
                        }
                    });
                }
            });
            break;
        case 'editar':
            swal({
                  title: 'Editar Acceso',
                  html:`<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="h5"><strong>Nombre del Acceso</strong></p>
                            <input type="text" class="form-control" id="nombre_acceso_editar" value="`+acceso+`" onkeyup="max_campo(this.value,250,'#texto_nombre_acceso_editar',this.id)">
                            <p class="text-right invisible" id="texto_nombre_acceso_editar"><a class="text-success"><i class="fa fa-pencil"></i>... 0/250</a></p>
                        </div>
                    </div>
                </div>`,
                  showCloseButton: true,
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fa fa-save"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times"></i> Cancelar'
                }).then(function(){
                if($('#nombre_acceso_editar').val() == ''){
                   swal({
                      type: 'error',
                      title: 'Alerta',
                      text: 'Debe ingresar una descripción para el acceso',
                    }).then(function(){
                        acceso_permisos('editar',id_modulo,id_contenedor,acceso,id_acceso);
                   });
                }else{
                    var CSRF_TOKEN = $('input[name=_token]').val();
                    var jqxhr = $.ajax({
                        url: "/configuracion-accion",
                        data: {
                            _token: CSRF_TOKEN,
                            accion: 6,
                            acceso:$('#nombre_acceso_editar').val(),
                            id_acceso:id_acceso
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            if(e.data['msj'] == 'Guardado correctamente!'){
                               swal('Guardado',e.data['msj'],'success');
                            }else{
                                swal('Error',e.data['msj'],'error');
                            }
                            lista_accesos(id_modulo,id_contenedor);
                        }
                    });
                }
            });
            break;
    }
}

/**
 * Función para asignar permisos a los cargos sobre los accesos
 * @param INT id IDENTIFICACIÓN DE LOS CARGOS
 * @param STRING acceso DESCRIPCIÓN DEL ACCESSO
 */
function acceso_permisos_cargo(id,acceso){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/configuracion-accion",
        data: {
            _token: CSRF_TOKEN,
            accion: 7,
            id_acceso:id
        },
        cache: false,
        type: 'POST',
        success: function(e){
            swal({
              title: 'Permisos: <i>'+acceso+'</i>',
              type: 'info',
              html:e.data['html'],
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              confirmButtonText:
                    '<i class="fa fa-floppy-o"></i> Guardar',
                  cancelButtonText:
                  '<i class="fa fa-times-circle"></i> Cancelar'
            }).then(function(){
                var datos = new FormData($("#formulario_permisosacceso")[0]);
                $.ajax({
                    url: '/configuracion-accion',
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: datos,
                    success: function(e) {
                        if(e.data['msj'] == 'Guardado correctamente!'){
                            swal(
                                  'Guardado',
                                  e.data['msj'],
                                  'success'
                                );
                        }else{
                            swal(
                              'Error',
                              e.data['msj'],
                              'error'
                            );
                        }

                    }
                });
            });
        }
    });

}


/**
 * Función para crear el HTML que contiene la estructura para modificar los días de BITACORA - PLAN DE TRABAJO - FECHA PRESUPUESTO - PRESUPUESTO ANUAL
 */
function dias_asignados(){
     var CSRF_TOKEN = $('input[name=_token]').val();
     var jqxhr = $.ajax({
        url: "/configuracion-accion",
        data: {
            _token: CSRF_TOKEN,
            accion: 9
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#chart5').html(e.data['html']);
        }
    });
}

/**
 * Función para guardar los dias de configuración
 * @param STRING accion ACCION A REALIZAR (BITACORA, PLAN DE TRABAJO, PRESUPUESTO)
 * @param STRING valor  VALOR A MODIFICAR DE LA TABLA configuracions
 * @param STRING id     IDENTIFICACIÓN DEL CAMPO
 */
function dias_configuracion(accion,valor,id){
    if(id!='configuracion_presupuestoanual'){
        valor = parseInt(valor)>0?parseInt(valor):0;
        $('#'+id).val(valor);
    }
    var CSRF_TOKEN = $('input[name=_token]').val();
    switch(accion){
        case 'bitacora':
            var parametros = {
                    _token : CSRF_TOKEN,
                    accion  : 10,
                    id    : 1,
                    campo : "dias bitacora",
                    valor : valor
            }
            break;
        case 'plantrabajo':
            var parametros = {
                    _token : CSRF_TOKEN,
                    accion  : 10,
                    id    : 2,
                    campo : "dias plan trabajo",
                    valor : valor
            }
            break;
        case 'presupuesto':
            var parametros = {
                    _token : CSRF_TOKEN,
                    accion  : 10,
                    id    : 3,
                    campo : "fecha presupuesto",
                    valor : valor
            }
            break;
        case 'presupuestoAnual':
            var parametros = {
                    _token : CSRF_TOKEN,
                    accion  : 10,
                    id    : 4,
                    campo : "presupuesto anual",
                    valor : valor
            }
            break;
    }
    var jqxhr = $.ajax({
        url: "/configuracion-accion",
        data: parametros,
        cache: false,
        type: 'POST',
        success: function(e){
            if(e.data['msj'] == 'Guardado correctamente!'){
               mostrar_guardado();
            }
        }
    });
}

function validarNum(e){
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true;
    patron = /\d/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}
 /**
 * Función indicar la longitud de caracteres y el maximo permitido, y restringir el ingreso de mas caracteres
 * @param  STRING     texto  TEXTO INGRESADO EN EL CAMPO
 * @param  INT        max_lonNUMERO MAXIMO DE CARACTERES PARA EL CAMPO
 * @param  STRING     id     IDENTIFICACION DE LA ETIQUETA <P> PARA ELIMINAR LA INVISIVILIDAD
 * @param  STRING     id_campIDENTIFICACION DEL CAMPO EDITADO
 */
function max_campo(texto,max_long,id,id_campo){
    numeroCaracteres = texto.length;
    $(id).removeClass('invisible');
    if(numeroCaracteres > max_long){
        $('#'+id_campo).val(texto.substr(0,max_long));
        $(id).html(`<a class="text-danger"><i class="fa fa-pencil"></i>... `+max_long+`/`+max_long+`</a>`);
    }else{
        $(id).html(`<a class="text-success"><i class="fa fa-pencil"></i>... `+numeroCaracteres+`/`+max_long+`</a>`);
    }
}

/**
 * Función para administrar el contenido de los selects de los filtros del modulo DASHBOARD Comercial
 */
function contenido_select(){
    var CSRF_TOKEN = $('input[name=_token]').val();
     var jqxhr = $.ajax({
        url: "/configuracion-accion",
        data: {
            _token: CSRF_TOKEN,
            accion: 11
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#chart6').html(e.data['html']);
        }
    });
}

/**
 * Función para Añadir cargo al select deseado
 * @param  INT        id IDENTIFICACIÓN DEL MODULO
 */
function anadir_cargos(id){
   var CSRF_TOKEN = $('input[name=_token]').val();
   var jqxhr = $.ajax({
      url: "/configuracion-accion",
      data: {
          _token: CSRF_TOKEN,
          accion: 12,
          id: id
      },
      cache: false,
      type: 'POST',
      success: function(e){
          swal({
                title: 'Usuarios en el select',
                type: 'info',
                html:e.data['html']
              }).then(function(){
                var datos = new FormData($("#formulario_permisosselect")[0]);
                $.ajax({
                    url: '/configuracion-accion',
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: datos,
                    success: function(e) {
                        if(e.data['msj'] == 'Guardado correctamente!'){
                            contenido_select();
                            swal(
                                  'Guardado',
                                  e.data['msj'],
                                  'success'
                                );
                        }else{
                            swal(
                              'Error',
                              e.data['msj'],
                              'error'
                            );
                        }

                    }
                });
              });
      }
  });

}

/**
 * Función para modificar el usuario de entrada que se va a visualizar en el dashboard por cargo
 * @param INT id IDENTIFICACION DEL CARGO
 */
function user_dashboard(id){
    var CSRF_TOKEN = $('input[name=_token]').val();
   var jqxhr = $.ajax({
      url: "/configuracion-accion",
      data: {
          _token: CSRF_TOKEN,
          accion: 14,
          id: id
      },
      cache: false,
      type: 'POST',
      success: function(e){
          swal({
                title: 'Usuarios en el select',
                type: 'info',
                html:e.data['html']
              }).then(function(){
                var datos = new FormData($("#formulario_usuariosdashboard")[0]);
                $.ajax({
                    url: '/configuracion-accion',
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: datos,
                    success: function(e) {
                        if(e.data['msj'] == 'Guardado correctamente!'){
                            contenido_select();
                            swal(
                                  'Guardado',
                                  e.data['msj'],
                                  'success'
                                );
                        }else{
                            swal(
                              'Error',
                              e.data['msj'],
                              'error'
                            );
                        }

                    }
                });
              });
      }
  });
}
