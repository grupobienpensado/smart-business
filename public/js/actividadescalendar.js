$(function () {
		$("body").on('change', '#subtipo', function () {
				var val = this.value;
				if($('#listsubtipo').find('option').filter(function(){
						if (parseInt(this.value) === parseInt(val)) {
							console.log(this.label);
							$("#subtipotexto").val(this.label).removeClass('ocultar');
							$("#subtipo").addClass('ocultar');
							return 1;
						}
				}).length){}
		});

		$("body").on('click', '#subtipotexto', function () {
			$("#subtipo").removeClass('ocultar');
			$(this).addClass('ocultar');
		});

		$("body").on('change', '#oportunidad', function () {
				var val = this.value;
				if($('#browsers').find('option').filter(function(){
						if (parseInt(this.value) === parseInt(val)) {
							console.log(this.label);
							$("#oportunidadtexto").val(this.label).removeClass('ocultar');
							$("#oportunidad").addClass('ocultar');
							return 1;
						}
				}).length){}
		});

		$("body").on('click', '#oportunidadtexto', function () {
			$("#oportunidad").removeClass('ocultar');
			$(this).addClass('ocultar');
		});

		$("body").on('change', '#empresa_id', function () {
				var val = this.value;
				if($('#browsers').find('option').filter(function(){
						if (parseInt(this.value) === parseInt(val)) {
							console.log(this.label);
							$("#empresa_idtexto").val(this.label).removeClass('ocultar');
							$("#empresa_id").addClass('ocultar');
							return 1;
						}
				}).length){}
		});

		$("body").on('click', '#empresa_idtexto', function () {
		$("#empresa_id").removeClass('ocultar');
		$(this).addClass('ocultar');
	});

		$('body').on('click','#button-delete',function(){
			var id = $(this).attr("name");
			swal({
				title: '¿Estas seguro?',
				html: $('<div>')
					.addClass('some-class')
					.text('No podrás revertir esto!'),
				animation: false,
				customClass: 'animated tada',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si, borrarlo!',
				cancelButtonText: 'Cancelar',
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				showLoaderOnConfirm: true,
				preConfirm: function () {
					return new Promise(function (resolve) {
						$.get( "/actividad/eliminar/"+id, function( data ) {
							if (data.success) {
								$('#calendar').fullCalendar('next');
								$('#calendar').fullCalendar('prev');
								swal('¡Eliminado!','Su actividad ha sido eliminada.','success');
								resolve()
							}else{
								swal("Algo salio mal, vuelve a intentar",'warning');
								resolve()
							}
						});
					})
				},
				allowOutsideClick: false
			});
		});

		$('body').on('change','#select_traer_datos',function(){
				funTraerDatos();
		});
		$('body').on('change','#PlanTrabajo',function(){
			$(".opprohtml").html("");
				if ($(this).val() != "") {
					id = $(this).val();
					$.ajax({
							url: "/ajaxplantrabajo/"+id,
							cache: false,
							contentType: false,
							processData: false,
							type: 'GET',
							success: function(e){
								$("#tipo_actividad").val(e.tipo_actividad);

								$.ajax( "/subactividad?term="+e.tipo_actividad )
								.always(function(data) {
									if(data){
										var html = '<p>Subactividad</p><input id="subtipotexto" class="swal2-input ocultar"><input list="listsubtipo" id="subtipo" class="swal2-input" name="subtipo" placeholder="Ingrese una Subactividad" value="'+e.subtipo+'" required><datalist id="listsubtipo">';
										$.each( data, function( key, value ) {
											html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
										});
										html += '</datalist>';
										$(".addOtro").html("");
										$(".contSubTipo").html(html);

										var val = e.subtipo;
										if($('#listsubtipo').find('option').filter(function(){
												if (parseInt(this.value) === parseInt(val)) {
													console.log(this.label);
													$("#subtipotexto").val(this.label).removeClass('ocultar');
													$("#subtipo").addClass('ocultar');
													return 1;
												}
										}).length){}
									}
								});

								if (e.tipo_actividad === "Empresa") {
									$.ajax( "/empresa" )
									.always(function(data) {
										if(data){
											var html = '<p>Empresas</p><input id="empresa_idtexto" class="swal2-input ocultar"><input list="browsers" id="empresa_id" class="swal2-input" name="empresa_id" placeholder="Ingrese una empresa" value="'+e.oportunidad+'" required><datalist id="browsers">';
											$.each( data, function( key, value ) {
												html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
											});
											html += '</datalist>';
											$(".addOtro").html(html);

											var val = e.oportunidad;
											if($('#browsers').find('option').filter(function(){
													if (parseInt(this.value) === parseInt(val)) {
														console.log(this.label);
														$("#empresa_idtexto").val(this.label).removeClass('ocultar');
														$("#empresa_id").addClass('ocultar');
														return 1;
													}
											}).length){}
										}
									});
								}else if (e.tipo_actividad === "Oportunidad") {
									$.ajax( "/ssoportunidad" )
									.always(function(data) {
										if(data){
											var html = '<p>Oportunidad</p><input id="oportunidadtexto" class="swal2-input ocultar"><input list="browsers" id="oportunidad" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" value="'+e.oportunidad+'" required><datalist id="browsers">';
											$.each( data, function( key, value ) {
												html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
											});
											html += '</datalist>';
											$(".addOtro").html(html);
											funOportinidadhtml();

											var val = e.oportunidad;
											if($('#browsers').find('option').filter(function(){
													if (parseInt(this.value) === parseInt(val)) {
														console.log(this.label);
														$("#oportunidadtexto").val(this.label).removeClass('ocultar');
														$("#oportunidad").addClass('ocultar');
														return 1;
													}
											}).length){}
										}
									});
								}
								$("#detalle_actividad").val(e.observaciones);
							}
					});
				}
		});
		$('body').on('change','#agendaVisita',function(){
			$(".opprohtml").html("");
				if ($(this).val() != "") {
					id = $(this).val();
					$.ajax({
							url: "/ajaxagendavisita/"+id,
							cache: false,
							contentType: false,
							processData: false,
							type: 'GET',
							success: function(e){
								$("#oportunidad").val(e.oportunidad);
								$("#select_tipo_actividad").val("");
								$("#detalle_actividad").val(e.detalle);
							}
					});
				}
		});

		$(document).on('change', '#tipo_actividad', function() {
			$(".opprohtml").html("");
			var valor = $(this).val();
			$.ajax( "/subactividad?term="+valor )
			.always(function(data) {
				if(data){
					var html = '<p>Subactividad</p><input id="subtipotexto" class="swal2-input ocultar"><input list="listsubtipo" id="subtipo" class="swal2-input" name="subtipo" placeholder="Ingrese una Subactividad" required><datalist id="listsubtipo">';
					$.each( data, function( key, value ) {
						html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
					});
					html += '</datalist>';
					$(".addOtro").html("");
					$(".contSubTipo").html(html);
				}
			});
			if (valor === "Empresa") {
				$.ajax( "/empresa" )
				.always(function(data) {
					if(data){
						var html = '<p>Empresas</p><input id="empresa_idtexto" class="swal2-input ocultar"><input list="browsers" id="empresa_id" class="swal2-input" name="empresa_id" placeholder="Ingrese una empresa" required><datalist id="browsers">';
						$.each( data, function( key, value ) {
							html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
						});
						html += '</datalist>';
						$(".addOtro").html(html);
					}
				});
			}else if (valor === "Oportunidad") {
				$.ajax( "/ssoportunidad" )
				.always(function(data) {
					if(data){
						var html = '<p>Oportunidad</p><input id="oportunidadtexto" class="swal2-input ocultar"><input list="browsers" id="oportunidad" class="swal2-input" name="oportunidad" id="oportunidad" placeholder="Ingrese una oportunidad" required><datalist id="browsers">';
						$.each( data, function( key, value ) {
							html += '<option label="'+value.text+'" value="'+value.id+'"></option>';
						});
						html += '</datalist>';
						$(".addOtro").html(html);
					}
				});
			}
		});

		/*detalle pendientes */
		$('body').on('keyup','.detalle_pendiente',function(){
			var chars = $(this).val().length;
			id=$(this).attr('id');
			if (chars>0) {
				$("#tipo_pendiente"+id).prop('required',true);
				$("#fechPendiente"+id).prop('required',true);
			}else{
				$("#tipo_pendiente"+id).prop('required',false);
				$("#fechPendiente"+id).prop('required',false);
			}
			if(chars>=50){
					$('.num_pen_columna'+id).css('background-color','lightgreen');
					icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
			}else if(chars<=50){
					$('.num_pen_columna'+id).css('background-color','yellow');
					icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
			}
			$('#num_pen_act'+id).html(chars+' Caracteres '+icon);
		});
		/*fin detalle pendientes */

		/*detalle de la resultado */
		$('body').on('keyup','#detalle_resultado',function(){
				var chars = $(this).val().length;
				if(chars>=50){
						$('.num_car_resu_columna').css('background-color','lightgreen');
						icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
				}else if(chars<=50){
						$('.num_car_resu_columna').css('background-color','yellow');
						icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
				}
				$('#num_resu_act').html(chars+' Caracteres '+icon);
		});
		/*fin detalles de la resultado*/

		/*detalle de la actividad */
		$('body').on('keyup','#detalle_actividad',function(){
				var chars = $(this).val().length;
				if(chars>=50){
						$('.num_car_act_columna').css('background-color','lightgreen');
						icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
				}else if(chars<=50){
						$('.num_car_act_columna').css('background-color','yellow');
						icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
				}
				$('#num_car_act').html(chars+' Caracteres '+icon);
		});
		/*fin detalles de la actividad*/

		var i = 1;
		$('body').on('click','#agregarPendientes',function(){
			i++;
			dato = `<div class="row animated rollIn" id="T`+i+`">
								<div class="col-7">
									<div class="form-group">
											<textarea class="form-control detalle_pendiente" id="`+i+`" name="pendientes[`+i+`][detalle_pendiente]" rows="1" placeholder="Por favor ingrese una descripcion de la actividad pendiente"></textarea>
									</div>
								</div>
								<div class="col-2">
									<div class="form-group">
											<input id="tipo_pendienteTexto`+i+`" class="form-control cambiarValor ocultar" title="tipo_pendiente`+i+`"><input list="listsubtipo" id="tipo_pendiente`+i+`" class="form-control cambiarTexto" name="pendientes[`+i+`][tipo_pendiente]" placeholder="Ingrese una Subactividad" title="tipo_pendienteTexto`+i+`">
									</div>
								</div>
								<div class="col-2">
									<div class="form-group">
											<input class="form-control date" type="text" id="fechPendiente`+i+`" name="pendientes[`+i+`][fecha_pendiente]" placeholder="Fecha de Cierre">
									</div>
								</div>
								<div class="col-1">
									<button class="btn btn-outline-danger pull-right btn-sm" type="button" onclick="quitarPendientes('#T`+i+`')"><i class="fa fa-minus text-danger"></i></button>
								</div>
							</div>
							<div class="row">
									<div class="col-md-7 num_pen_columna`+i+`">
											<a id="num_pen_act`+i+`"></a>
									</div>
									<div class="col-md-5 num_pen_columna`+i+`">

									</div>
							</div>`;
			$( "#pendientes" ).after(dato);
			$('.date').bootstrapMaterialDatePicker({
					time: false,
					clearButton: true,
					lang: 'es',
					minDate : new Date()
			});
			/* detalle pendientes */
			$('.detalle_pendiente').keyup(function() {
					var chars = $(this).val().length;
					id=$(this).attr('id');
					if (chars>0) {
						$("#tipo_pendiente"+id).prop('required',true);
						$("#fechPendiente"+id).prop('required',true);
					}else{
						$("#tipo_pendiente"+id).prop('required',false);
						$("#fechPendiente"+id).prop('required',false);
					}
					if(chars>=50){
							$('.num_pen_columna'+id).css('background-color','lightgreen');
							icon='<i class="fa fa-check" aria-hidden="true"></i> Los caracteres son suficientes para guardar.';
					}else if(chars<=50){
							$('.num_pen_columna'+id).css('background-color','yellow');
							icon='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Faltan caracteres para guardar (minimo son 50).';
					}
					$('#num_pen_act'+id).html(chars+' Caracteres '+icon);
			});
			/*fin detalle pendientes */
		});

		$("body").on('mouseup', '.button-aprobar', function () {
			var id = $(this).attr("name");
			var text = $(this).attr("data-title");
			var detalle_aprobar = $("#detalle_aprobar").val();
			if (detalle_aprobar.length<=0) {
				swal("Por favor ingrese un comentario para gestionar la actividad");
			}else{
				swal({
					title: '¿Estas seguro?',
					html: $('<div>')
						.addClass('some-class')
						.text('Deseas cambiar el estado de esta oportunidad!'),
					animation: false,
					customClass: 'animated tada',
					type: 'success',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, continuar!',
					cancelButtonText: 'Cancelar',
					closeOnConfirm: false,
					showLoaderOnConfirm: true,
					showLoaderOnConfirm: true,
					preConfirm: function () {
						return new Promise(function (resolve) {
							$.post( "/actividad/aprobar", { id: id, text: text, detalleaprobar: detalle_aprobar }, function( data ) {
								if (data.success) {
									$('#calendar').fullCalendar('next');
						$('#calendar').fullCalendar('prev');
									swal('Â¡Exito!','Su actividad ha sido gestionada con exito.','success');
									resolve()
								}else{
									swal("Algo salio mal, vuelve a intentar",'warning');
									resolve()
								}
							});
						})
					},
					allowOutsideClick: false
				});
			}
		});




		try {
			$('#semanas').select2();
    } catch(err) {
		    console.log(err.message);
		}
	    $('#responsable').on('change', function() {
			if (this.value) {
				urlList = '/listadoactividades/'+this.value;
				$('#calendar').fullCalendar( 'removeEventSources');
				$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
				        $.ajax({
				            url: urlList,
				            dataType: 'json',
				            data: {
				                start: moment(start).format('YYYY-MM-DD'),
				                end: moment(end).format('YYYY-MM-DD')
				            },
				            success: function(doc) {
				            	respal = "";
								var push = false
								var feature = false;
								var features = [];
								doc2 = doc.sort(function (a, b) {
								  if (a.tipo_actividad > b.tipo_actividad) {
								    return 1;
								  }
								  if (a.tipo_actividad < b.tipo_actividad) {
								    return -1;
								  }
								  // a must be equal to b
								  return 0;
								});
								var comp = doc2.length - 1;
								$.each( doc2, function( key, value ) {
									var a = moment(value.start);
									var b = moment(value.end);
									c = b.diff(a);if (c<0) {c = c*(-1);}

									c = parseInt(c/3600000);

									if (feature) {
										if (value.tipo_actividad != respal) {
											push = true;
											features.push(feature);
											respal = value.tipo_actividad;
											feature = new Object();
											feature.tiempo = c;
											feature.user = value.tipo_actividad;
											feature.colorGrp = "rgb(103, 183, 220)";
										}else{
											push = false;
											feature.tiempo = c+feature.tiempo;
										}
									}else{
										push = false;
										respal = value.tipo_actividad;
										feature = new Object();
										feature.tiempo = c;
										feature.user = value.tipo_actividad;
										feature.colorGrp = "rgb(103, 183, 220)";
										respal = value.tipo_actividad;
									}

									if (comp === key) {
										features.push(feature);
									}
								});
								var charts = AmCharts.makeChart("chartdiv2", {
									"theme": "light",
									"type": "serial",
									"startDuration": 2,
									"dataProvider": features,
									"valueAxes": [{
										"position": "left",
										"title": "Uso de tiempo de las actividades",
										"gridAlpha": 0,
										"dashLength": 0
									}],
									"gridAboveGraphs": false,
									"graphs": [{
										"balloonText": "[[category]]: <b>[[value]]</b>",
										"fillColorsField": "colorGrp",
										"fillAlphas": 1,
										"lineAlpha": 0.1,
										"type": "column",
										"valueField": "tiempo"
									}],
									"depth3D": 20,
									"angle": 30,
									"chartCursor": {
										"categoryBalloonEnabled": false,
										"cursorAlpha": 0,
										"zoomable": false
									},
									"categoryField": "user",
									"categoryAxis": {
										"gridPosition": "start",
										"gridAlpha": 0,
										"tickPosition": "start",
										"tickLength": 20,
										"labelRotation": 45
									},
									"export": {
										"enabled": true
									}
								});

								var feature2 = false;
								var features2 = [];
								otrodoc = doc.sort(function (a, b) {
									return a.oportunidad.localeCompare(b.oportunidad);
								});
								$.each( otrodoc, function( key, value ) {
									var a = moment(value.start);
									var b = moment(value.end);
									c = b.diff(a);if (c<0) {c = c*(-1);}

									c = parseInt(c/3600000);

									if (feature2) {
										if (value.oportunidad != respal) {
											push = true;
											features2.push(feature2);
											respal = value.oportunidad;
											feature2 = new Object();
											feature2.tiempo = c;
											feature2.user = value.oportunidad;
											feature2.colorGrp = "rgb(103, 183, 220)";
										}else{
											push = false;
											feature2.tiempo = c+feature2.tiempo;
										}
									}else{
										push = false;
										respal = value.oportunidad;
										feature2 = new Object();
										feature2.tiempo = c;
										feature2.user = value.oportunidad;
										feature2.colorGrp = "rgb(103, 183, 220)";
										respal = value.oportunidad;
									}
									var comp = otrodoc.length - 1;
									if (comp === key) {
										features2.push(feature2);
									}
								});
								var chart = AmCharts.makeChart( "chartdiv", {
									"type": "pie",
									"theme": "light",
									"titles": [ {
										"text": "Uso de tiempo con respecto a las oportunidades",
										"size": 16
									} ],
									"dataProvider": features2,
									"valueField": "tiempo",
									"titleField": "user",
									"startEffect": "elastic",
									"startDuration": 2,
									"labelRadius": 15,
									"innerRadius": "50%",
									"depth3D": 10,
									"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
									"angle": 15,
									"export": {
										"enabled": true
									}
								});
								callback(doc);
				            },
				            error: function() {
								$('#script-warning').show();
							}
				        });
				    });
			}
		});
		$("#btnNext").click(function(){
			$('#calendar').fullCalendar('next');
			var fechadato = $('#calendar').fullCalendar('getDate');
			var dateWeek = moment(fechadato).weeks();
            var diahoy = moment().format('dddd');
            var semanahoy = dateWeek;
            if(diahoy === "domingo"){
               dateWeek = parseInt(dateWeek) - 1;
            }
			var dateWeek = moment().weeks(dateWeek);
			$("#tituloPrincipal").html("Bitacora de la semana " + semanahoy + " ( "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D") + " ) ");


			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}
							c = parseInt(c/3600000);
							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		});
		$("#btnToday").click(function(){
			$('#calendar').fullCalendar('today');
			var fechadato = $('#calendar').fullCalendar('getDate');
			var dateWeek = moment(fechadato).weeks();
			var diahoy = moment().format('dddd');
            var semanahoy = dateWeek;
            if(diahoy === "domingo"){
               dateWeek = parseInt(dateWeek) - 1;
            }
			var dateWeek = moment().weeks(dateWeek);
			$("#tituloPrincipal").html("Bitacora de la semana " + semanahoy + " ( "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D") + " ) ");
			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		});
		$("#btnPrev").click(function(){
			$('#calendar').fullCalendar('prev');
			var fechadato = $('#calendar').fullCalendar('getDate');
			var dateWeek = moment(fechadato).weeks();
			var diahoy = moment().format('dddd');
            var semanahoy = dateWeek;
            if(diahoy === "domingo"){
               dateWeek = parseInt(dateWeek) - 1;
            }
			var dateWeek = moment().weeks(dateWeek);
			$("#tituloPrincipal").html("Bitacora de la semana " + semanahoy + " ( "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D") + " ) ");
			$('#calendar').fullCalendar( 'removeEventSources');
			$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
		        $.ajax({
		            url: urlList,
		            dataType: 'json',
		            data: {
		                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
		                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
		            },
		            success: function(doc) {
		            	respal = "";
						var push = false
						var feature = false;
						var features = [];
						doc2 = doc.sort(function (a, b) {
						  if (a.tipo_actividad > b.tipo_actividad) {
						    return 1;
						  }
						  if (a.tipo_actividad < b.tipo_actividad) {
						    return -1;
						  }
						  return 0;
						});
						var comp = doc2.length - 1;
						$.each( doc2, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature) {
								if (value.tipo_actividad != respal) {
									push = true;
									features.push(feature);
									respal = value.tipo_actividad;
									feature = new Object();
									feature.tiempo = c;
									feature.user = value.tipo_actividad;
									feature.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature.tiempo = c+feature.tiempo;
								}
							}else{
								push = false;
								respal = value.tipo_actividad;
								feature = new Object();
								feature.tiempo = c;
								feature.user = value.tipo_actividad;
								feature.colorGrp = "rgb(103, 183, 220)";
								respal = value.tipo_actividad;
							}

							if (comp === key) {
								features.push(feature);
							}
						});
						var charts = AmCharts.makeChart("chartdiv2", {
							"theme": "light",
							"type": "serial",
							"startDuration": 2,
							"dataProvider": features,
							"valueAxes": [{
								"position": "left",
								"title": "Uso de tiempo de las actividades",
								"gridAlpha": 0,
								"dashLength": 0
							}],
							"gridAboveGraphs": false,
							"graphs": [{
								"balloonText": "[[category]]: <b>[[value]]</b>",
								"fillColorsField": "colorGrp",
								"fillAlphas": 1,
								"lineAlpha": 0.1,
								"type": "column",
								"valueField": "tiempo"
							}],
							"depth3D": 20,
							"angle": 30,
							"chartCursor": {
								"categoryBalloonEnabled": false,
								"cursorAlpha": 0,
								"zoomable": false
							},
							"categoryField": "user",
							"categoryAxis": {
								"gridPosition": "start",
								"gridAlpha": 0,
								"tickPosition": "start",
								"tickLength": 20,
								"labelRotation": 45
							},
							"export": {
								"enabled": true
							}
						});

						var feature2 = false;
						var features2 = [];
						otrodoc = doc.sort(function (a, b) {
							return a.oportunidad.localeCompare(b.oportunidad);
						});
						$.each( otrodoc, function( key, value ) {
							var a = moment(value.start);
							var b = moment(value.end);
							c = b.diff(a);if (c<0) {c = c*(-1);}

							c = parseInt(c/3600000);

							if (feature2) {
								if (value.oportunidad != respal) {
									push = true;
									features2.push(feature2);
									respal = value.oportunidad;
									feature2 = new Object();
									feature2.tiempo = c;
									feature2.user = value.oportunidad;
									feature2.colorGrp = "rgb(103, 183, 220)";
								}else{
									push = false;
									feature2.tiempo = c+feature2.tiempo;
								}
							}else{
								push = false;
								respal = value.oportunidad;
								feature2 = new Object();
								feature2.tiempo = c;
								feature2.user = value.oportunidad;
								feature2.colorGrp = "rgb(103, 183, 220)";
								respal = value.oportunidad;
							}
							var comp = otrodoc.length - 1;
							if (comp === key) {
								features2.push(feature2);
							}
						});
						var chart = AmCharts.makeChart( "chartdiv", {
							"type": "pie",
							"theme": "light",
							"titles": [ {
								"text": "Uso de tiempo con respecto a las oportunidades",
								"size": 16
							} ],
							"dataProvider": features2,
							"valueField": "tiempo",
							"titleField": "user",
							"startEffect": "elastic",
							"startDuration": 2,
							"labelRadius": 15,
							"innerRadius": "50%",
							"depth3D": 10,
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
							"angle": 15,
							"export": {
								"enabled": true
							}
						});
						callback(doc);
		            },
		            error: function() {
						$('#script-warning').show();
					}
		        });
		    });
		});
		$('#semanas').on('change', function() {
			if (this.value) {
					var dateWeek = moment().weeks(this.value);
					$("#tituloPrincipal").html("Bitacora de la semana " + moment(dateWeek).weeks() + " ( "+ moment(dateWeek).day(0).format("MMMM D") + " - " + moment(dateWeek).day(6).format("MMMM D") + " ) ");
					$('#calendar').fullCalendar( 'removeEventSources');
					$('#calendar').fullCalendar ('gotoDate', dateWeek);
					$('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
				        $.ajax({
				            url: urlList,
				            dataType: 'json',
				            data: {
				                start: moment(dateWeek).day(0).format('YYYY-MM-DD'),
				                end: moment(dateWeek).day(6).format('YYYY-MM-DD')
				            },
				            success: function(doc) {
				            	respal = "";
								var push = false
								var feature = false;
								var features = [];
								doc2 = doc.sort(function (a, b) {
								  if (a.tipo_actividad > b.tipo_actividad) {
								    return 1;
								  }
								  if (a.tipo_actividad < b.tipo_actividad) {
								    return -1;
								  }
								  return 0;
								});
								var comp = doc2.length - 1;
								$.each( doc2, function( key, value ) {
									var a = moment(value.start);
									var b = moment(value.end);
									c = b.diff(a);if (c<0) {c = c*(-1);}
									c = parseInt(c/3600000);


									if (feature) {
										if (value.tipo_actividad != respal) {
											push = true;
											features.push(feature);
											respal = value.tipo_actividad;
											feature = new Object();
											feature.tiempo = c;
											feature.user = value.tipo_actividad;
											feature.colorGrp = "rgb(103, 183, 220)";
										}else{
											push = false;
											feature.tiempo = c+feature.tiempo;
										}
									}else{
										push = false;
										respal = value.tipo_actividad;
										feature = new Object();
										feature.tiempo = c;
										feature.user = value.tipo_actividad;
										feature.colorGrp = "rgb(103, 183, 220)";
										respal = value.tipo_actividad;
									}

									if (comp === key) {
										features.push(feature);
									}
								});
								var charts = AmCharts.makeChart("chartdiv2", {
									"theme": "light",
									"type": "serial",
									"startDuration": 2,
									"dataProvider": features,
									"valueAxes": [{
										"position": "left",
										"title": "Uso de tiempo de las actividades",
										"gridAlpha": 0,
										"dashLength": 0
									}],
									"gridAboveGraphs": false,
									"graphs": [{
										"balloonText": "[[category]]: <b>[[value]]</b>",
										"fillColorsField": "colorGrp",
										"fillAlphas": 1,
										"lineAlpha": 0.1,
										"type": "column",
										"valueField": "tiempo"
									}],
									"depth3D": 20,
									"angle": 30,
									"chartCursor": {
										"categoryBalloonEnabled": false,
										"cursorAlpha": 0,
										"zoomable": false
									},
									"categoryField": "user",
									"categoryAxis": {
										"gridPosition": "start",
										"gridAlpha": 0,
										"tickPosition": "start",
										"tickLength": 20,
										"labelRotation": 45
									},
									"export": {
										"enabled": true
									}
								});

								var feature2 = false;
								var features2 = [];
								otrodoc = doc.sort(function (a, b) {
									return a.oportunidad.localeCompare(b.oportunidad);
								});
								$.each( otrodoc, function( key, value ) {
									var a = moment(value.start);
									var b = moment(value.end);
									c = b.diff(a);if (c<0) {c = c*(-1);}

									c = parseInt(c/3600000);

									if (feature2) {
										if (value.oportunidad != respal) {
											push = true;
											features2.push(feature2);
											respal = value.oportunidad;
											feature2 = new Object();
											feature2.tiempo = c;
											feature2.user = value.oportunidad;
											feature2.colorGrp = "rgb(103, 183, 220)";
										}else{
											push = false;
											feature2.tiempo = c+feature2.tiempo;
										}
									}else{
										push = false;
										respal = value.oportunidad;
										feature2 = new Object();
										feature2.tiempo = c;
										feature2.user = value.oportunidad;
										feature2.colorGrp = "rgb(103, 183, 220)";
										respal = value.oportunidad;
									}
									var comp = otrodoc.length - 1;
									if (comp === key) {
										features2.push(feature2);
									}
								});
								var chart = AmCharts.makeChart( "chartdiv", {
									"type": "pie",
									"theme": "light",
									"titles": [ {
										"text": "Uso de tiempo con respecto a las oportunidades",
										"size": 16
									} ],
									"dataProvider": features2,
									"valueField": "tiempo",
									"titleField": "user",
									"startEffect": "elastic",
									"startDuration": 2,
									"labelRadius": 15,
									"innerRadius": "50%",
									"depth3D": 10,
									"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
									"angle": 15,
									"export": {
										"enabled": true
									}
								});
								callback(doc);
				            },
				            error: function() {
								$('#script-warning').show();
							}
				        });
				    });
				}
		});

		setTimeout(function(){
			$('#calendar').fullCalendar('next');
			$('#calendar').fullCalendar('prev');
			$('#calendar').fullCalendar('render');
		}, 1000);
	})
	funAgregarTotales = function() {

        var valSumaGeneral = 0;
        $(".sumarSueldo").each(function() {
            var valSuma = verificar(this.id);
            valSumaGeneral = parseInt(valSumaGeneral) + parseInt(valSuma);
        });

        TotalhHombre = parseInt(valSumaGeneral);
        var TotalhHombre = TotalhHombre.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        TotalhHombre = TotalhHombre.split('').reverse().join('').replace(/^[\,]/,'');
        document.getElementById("total_h_hombre").value = TotalhHombre;

        total = verificar("total");
        valSumaGeneral = parseInt(valSumaGeneral) + parseInt(total) + parseInt($('#total_mis_horas').val());
        var valSumaGeneral = valSumaGeneral.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        valSumaGeneral = valSumaGeneral.split('').reverse().join('').replace(/^[\,]/,'');
        document.getElementById("total_general").value = valSumaGeneral;
    }

	agregarPendientes = function() {}

    quitarPendientes = function (id) {
        $(id).removeClass('rollIn').addClass('hinge');
        setTimeout(function(){ $(id).remove(); }, 1000);
    }

    quitarhHombre = function (id) {
        $(id).removeClass('rollIn').addClass('hinge');
        setTimeout(function(){
            $(id).remove();
            funAgregarTotales();
        }, 1000);

    }

    fSumar = function(){
        total = $("#s1").val()+$("#s2").val()+$("#s3").val()+$("#s4").val()+$("#s5").val()+$("#s6").val()+$("#s7").val()+$("#s8").val()+$("#s9").val()+$("#s10").val();
        alert(total);
    }

    funSumar = function(){
        var valor1=verificar("s1");
        var valor2=verificar("s2");
        var valor3=verificar("s3");
        var valor4=verificar("s4");
        var valor5=verificar("s5");
        var valor6=verificar("s6");
        var valor7=verificar("s7");
        var valor8=verificar("s8");
        var valor9=verificar("s9");
        var valor10=verificar("s10");
        // realizamos la suma de los valores y los ponemos en la casilla del
        // formulario que contiene el total
        e = parseFloat(valor1)+parseFloat(valor2)+parseFloat(valor3)+parseFloat(valor4)+parseFloat(valor5)+parseFloat(valor6)+parseFloat(valor7)+parseFloat(valor8)+parseFloat(valor9)+parseFloat(valor10);
        e = parseInt(e);
        var num = e;
        if(!isNaN(num)){
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            num = num.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("total").value = num;

            var valSumaGeneral = 0;
            $(".sumarSueldo").each(function() {
                var valSuma=verificar(this.id);
                valSumaGeneral = parseInt(valSumaGeneral) + parseInt(valSuma);
            })
            valSumaGeneral = parseInt(valSumaGeneral) + parseInt(e) + parseInt($('#total_mis_horas').val());
            var valSumaGeneral = valSumaGeneral.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            valSumaGeneral = valSumaGeneral.split('').reverse().join('').replace(/^[\,]/,'');
            document.getElementById("total_general").value = valSumaGeneral;
        }
        //document.getElementById("total").focus();
    }

	verificar = function(id){
        var obj=document.getElementById(id);
        if(obj.value==""){
            value="0";
        }else{
            value=obj.value;
            value = value.replace(/,/g, "");
        }

        try {
            if(validate_importe(value,1))
            {
                // marcamos como erroneo
                obj.style.borderColor="#808080";
                return value;
            }else{
                // marcamos como erroneo
                obj.style.borderColor="#f00";
                return 0;
            }
        }catch (e) { }

    }

    validate_importe = function(value,decimal){
        if(decimal==undefined)
            decimal=0;

        if(decimal==1)
        {
            // Permite decimales tanto por . como por ,
            var patron=new RegExp("^[0-9]+((,|\.)[0-9]{1,2})?$");
        }else{
            // Numero entero normal
            var patron=new RegExp("^([0-9])*$")
        }

        if(value && value.search(patron)==0)
        {
            return true;
        }
        return false;
    }



	funMostarDiv = function (valor) {
	    $(".active").removeClass('active');
	    $("#b"+valor).addClass('active');
	    $(".slideInDown").removeClass('slideInDown').addClass('rollOut');
	    if(valor==="grafica"){
	      $("#grafica").show();
	    }else{
	      $("#grafica").hide();
	    }
	    if(valor==="calendar"){
	      $("#calendarcontent").show();
	      $("#calendar").fullCalendar('render');
	    }else{
	      $("#calendarcontent").hide();
	    }
	    $("#"+valor).removeClass('rollOut').addClass('slideInDown');
	}

	$('body').on('change', '#oportunidad', function() {
	  	funOportinidadhtml();
	});
