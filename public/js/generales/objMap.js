var MapStreep = function(){ //crear objeto para manipular elmapa
  this._self = undefined;
  this.load =false;
  this.init = function(config){  //inicializar el mapa
    var config = config || {};
    if(!this.load){ //si no ha renderizado
      this._self = L.map(config.element, { //elemento html <div id="element"></div>
        center:config.center,
        zoom: config.zoom
      });

      L.tileLayer(config.layer,{
        maxZoom: 20,
      }).addTo( this._self );
      this.load = true;//renderizado
    }
  };
  this.requestData = function(params, callback){ //request server data markers /* variable params recibe la url de petición, callback responde con la respuesta */
    var params = params || {};
    var _self = this._self; //hace referencia al objeto mapa actual
    var reqMarkers = $.getJSON(params.url, params.data);//response server data and add markers...

    reqMarkers.then(function(res){
      callback(res, function(req){ //respondemos y a su vez esperamos la plantilla, el marker y el icono correspondiente
        if(typeof req.icon != "undefined" && typeof req.tpl != "undefined"){
            L.marker([req.marker.latitud,req.marker.longitud], {icon: req.icon}).addTo(_self).bindPopup(req.tpl.html()); //pegamos el marker al mapa
        }
        if(typeof req.icon != "undefined" && typeof req.tpl == "undefined"){
            L.marker([req.marker.latitud,req.marker.longitud], {icon: req.icon}).addTo(_self); //pegamos el marker al mapa
        }
      });
    })
    .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    })
  }
  /**
   * agregar marcador simple
   * @param  {[type]} data paramatro con icono y latitud + longitud en arreglo
   * @return {[type]}      retorna el marcador para luego modificarlo o eliminarlo
   */
  this.addSingleMarker = function(data){
    var marker = L.marker(data.latLng, {icon: data.icon}).addTo(this._self); //pegamos el marker al mapa
    return marker;
  }
  /**
   * agregar marcador simple con opcopn de arrastado
   * @param  {[type]} data paramatro con icono y latitud + longitud en arreglo
   * @return {[type]}     retorna el marcador para luego modificarlo o eliminarlo
   * @return {[type]}     dispara a una funcion getLatLng para usar las coordenadas del mapa
   */
  this.addSingleDragMarker = function(data){
    var _self = this._self;
    var marker = new L.marker(data.latLng, {icon: data.icon, draggable:'true'});
    marker.on('dragend', function(event){
      var marker = event.target;
      var position = marker.getLatLng();
      // console.log(position);
      marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
      _self.panTo(new L.LatLng(position.lat, position.lng));
      getLatLng(position.lat, position.lng);
    });
    _self.addLayer(marker);
    return marker;
  }
  /**
   * centrar el mapa
   * @param  {[type]} lat latitud
   * @param  {[type]} lng longitud
   */
  this.addCenterMap = function(lat, lng){
    this._self.panTo(new L.LatLng(lat, lng));
  }
  /**
   * Borrar un marcador
   * @param  {[type]} marker se pasa el marcador para ser borrado con la funcion remove()
   */
  this.clearMarkers = function(marker){
    if (marker) {
      console.log(marker);
      marker.remove();
    }
  }
  /**
   * Borrar todos los layers, exepto la capa del mapa
   */
  this.clearAllMarkers = function(){
    var _self = this._self;
    this._self.eachLayer(function(layer){
        if (layer._url !== 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png') {
          _self.removeLayer(layer);
        }
    });
  }
  /**
   * hacer zoom al mapa
   * @param  {[type]} val valor numerico del zoom
   */
  this.setZoom = function(val){
    this._self.setZoom(val);
  }

  this.othermethod = function(){} //.......
  this.getLatLng = function(lat, lng){
    var newLatLng = new L.LatLng(lat, lng);
    return newLatLng;
    //marker.setLatLng(newLatLng);
  }
}
