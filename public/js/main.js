html = '';
var min=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","U","V","W","X","Y","Z"];
var cont=0;
var contents = {};
var pos = 1;
$(function () {
    initrepcarp();
});

initrepcarp = function () {
    $(".breadcrumb").html('<li id="0" class="breadcrumb-item active" onclick="initrepcarp()"><a href="#" class="resaltarnav">INICIO</a></li>');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var jqxhr = $.ajax({
        url: "initdata",
        data: {
            _token: CSRF_TOKEN
        },
        cache: false,
        type: 'POST',
        success: function(e){              



            videos="";
            presentacion="";
            pdf="";
            documentos="";
            excel="";
            images='<div id="carousel">';
            countimg=0;
            folder="";

            for (var i = 1; i < cont; i++) {
                if ($("#"+min[i]+0).html() === undefined) {
                    cont = i;
                    break;
                }
            }

            for (i = 0; i < e.folder.length; i++) {
                folder += `<div class="col-3 col-sm-2 placeholder">
                  <img id="`+min[cont]+i+`" src="images/folder.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="`+ e.folder[i] +`" onclick="buscarfolderandfile('`+ e.folder[i] +`', this.id)">
                  <div id="TEXT`+min[cont]+i+`" class="text-muted">`+ e.folder[i] +`</div>
                </div>`;

            }
            for (i = 0; i < e.archivos.length; i++){
                total = e.archivos[i].length;
                c=total-1;
                b = c-1;
                a = b-1;
                extension = e.archivos[i].substr(a,b,c);
                if (extension === "BMP" || extension === "GIF" || extension === "PNG" || extension === "JPG" || extension === "TIF" || extension === "bmp" || extension === "gif" || extension === "png" || extension === "jpg" || extension === "tif") {
                    images += `<figure style="transform: rotateY(`+countimg+`deg) translateZ(388px);">
                                    <a data-gallery="" title="`+e.archivos[i]+`" href="storage/`+e.archivos[i]+`">
                                        <img src="storage/`+e.archivos[i]+`" class="rotate">
                                        <h5 style="position: absolute;top: 0px;"><span class="badge badge-primary" style="background-color: #051d60;">`+e.archivos[i]+`</span></h5>
                                    </a>
                                </figure>`;
                    countimg = countimg + 40;
                }

                if (extension === "AVI" || extension === "MOV" || extension === "WMV" || extension === "FLv" || extension === "MP4" || extension === "avi" || extension === "mov" || extension === "wmv" || extension === "flv" || extension === "mp4") {
                    videos += `<div class="col-3 col-sm-2 placeholder">
                      <img src="images/play.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openVideo('','`+e.archivos[i]+`')">
                      <div class="text-muted">`+e.archivos[i]+`</div>
                    </div>`;
                }

                if (extension === "PTX" || extension === "PSX" || extension === "PSM" || extension === "ptx" || extension === "psx" || extension === "psm") {
                    presentacion += `<div class="col-3 col-sm-2 placeholder">
                      <img src="images/ppt.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                      <div class="text-muted">`+e.archivos[i]+`</div>
                    </div>`;
                }

                if (extension === "PDF" || extension === "pdf"){
                    pdf += `<div class="col-3 col-sm-2 placeholder">
                      <img src="images/pdf.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                      <div class="text-muted">`+e.archivos[i]+`</div>
                    </div>`;
                }

                if (extension === "OCX" || extension === "ocx" || extension === "DOC" || extension === "doc"){
                    documentos += `<div class="col-3 col-sm-2 placeholder">
                                      <img src="images/doc.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                                      <div class="text-muted">`+e.archivos[i]+`</div>
                                    </div>`;
                }

                if (extension === "LSX" || extension === "lsx" || extension === "LSM" || extension === "lsm" || extension === "XML" || extension === "xml"){
                    excel += `<div class="col-3 col-sm-2 placeholder">
                                      <img src="images/stats.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                                      <div class="text-muted">`+e.archivos[i]+`</div>
                                    </div>`;
                }
                //PDF, DOC, XLS y PPT
            }
            images += "</div>";
            if (images!='<div id="carousel"></div>') {
                $("#con-img").html('<section id="section_images">'+ images +'</section>');
            }else{
                $("#section_images").remove();
            }
            $("#section_archive").html(folder);
            $("#section_archive").append(videos);
            $("#section_archive").append(presentacion);
            $("#section_archive").append(pdf);
            $("#section_archive").append(documentos);
            $("#section_archive").append(excel);
            cont++;
        }
    });
}



    var viewFullScreen = document.getElementById("view-fullscreen");
    if (viewFullScreen) {
        viewFullScreen.addEventListener("click", function () {
            var docElm = document.documentElement;
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            else if (docElm.msRequestFullscreen) {
                docElm.msRequestFullscreen();
            }
            else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
        }, false);
    }

    var cancelFullScreen = document.getElementById("cancel-fullscreen");
    if (cancelFullScreen) {
        cancelFullScreen.addEventListener("click", function () {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
            else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            }
            else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }, false);
    }


    var fullscreenState = document.getElementById("fullscreen-state");
    if (fullscreenState) {
        document.addEventListener("fullscreenchange", function () {
            fullscreenState.innerHTML = (document.fullscreenElement)? "" : "not ";
        }, false);

        document.addEventListener("msfullscreenchange", function () {
            fullscreenState.innerHTML = (document.msFullscreenElement)? "" : "not ";
        }, false);

        document.addEventListener("mozfullscreenchange", function () {
            fullscreenState.innerHTML = (document.mozFullScreen)? "" : "not ";
        }, false);

        document.addEventListener("webkitfullscreenchange", function () {
            fullscreenState.innerHTML = (document.webkitIsFullScreen)? "" : "not ";
        }, false);
    }


buscarfolderandfile = function(url, id, p) {
    for (var i = 0; i < pos; i++) {
        $( "#"+i+",a" ).removeClass( "resaltarnav" );
    }
    if (p) {
        for (var i = p+1; i < pos; i++) {
            $( "li" ).remove("#"+i);
        }
        pos=p;
        pos++;
    } else {
        titulo = $("#TEXT"+id).html();
        $(".breadcrumb").append('<li id="'+pos+'" class="breadcrumb-item" onclick="buscarfolderandfile(\''+url+'\', this.id, '+pos+')"><a href="#" class="resaltarnav">'+titulo+'</a></li>');
        pos++;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    //if ( contents[ id ] === undefined ) {
<<<<<<< HEAD
        var jqxhr = $.ajax({ 
=======
        var jqxhr = $.ajax({
>>>>>>> origin/master
            url: "/enlaces",
            data: {
                _token: CSRF_TOKEN,
                url: url
            },
            cache: false,
            type: 'POST',
            success: function(e){
                //contents[ id ] = e;
               // html = titulo+'<ul>';
                /*for (var i = 1; i < cont; i++) {
                    if ($("#"+min[i]+0).html() === undefined) {
                        cont = i;
                        break;
                    }
                } */
            /*for (i = 0; i < e.folder.length; i++) {
                html += '<li id="'+min[cont]+i+'" onclick="buscarfolderandfile(\''+url+'/'+e.folder[i]+'\', this.id)"><a href="#"><i class="fa fa-folder"></i><span style="text-transform: capitalize;overflow:hidden;">' + e.folder[i] + '</span></a></li>';
            }*/
            //html += '</ul>';
            //$("#"+id).html(html);

            videos="";
            presentacion="";
            pdf="";
            documentos="";
            excel="";
            images='<div id="carousel">';
            countimg=0;

            folder="";

            for (var i = 1; i < cont; i++) {
                if ($("#"+min[i]+0).html() === undefined) {
                    cont = i;
                    break;
                }
            }

            for (i = 0; i < e.folder.length; i++) {
                folder += `<div class="col-3 col-sm-2 placeholder">
                  <img id="`+min[cont]+i+`" src="images/folder.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="`+ e.folder[i] +`" onclick="buscarfolderandfile('`+url+`/`+ e.folder[i] +`', this.id)">
                  <div id="TEXT`+min[cont]+i+`" class="text-muted">`+ e.folder[i] +`</div>
                </div>`;

            }

            for (i = 0; i < e.archivos.length; i++){
                total = e.archivos[i].length;
                c=total-1;
                b = c-1;
                a = b-1;
                extension = e.archivos[i].substr(a,b,c);
                if (extension === "BMP" || extension === "GIF" || extension === "PNG" || extension === "JPG" || extension === "TIF" || extension === "bmp" || extension === "gif" || extension === "png" || extension === "jpg" || extension === "tif") {
                    images += `<figure style="transform: rotateY(`+countimg+`deg) translateZ(388px);">
                                    <a data-gallery="" title="`+e.archivos[i]+`" href="storage/`+url+`/`+e.archivos[i]+`">
                                        <img src="storage/`+url+`/`+e.archivos[i]+`" class="rotate">
                                        <h5 style="position: absolute;top: 0px;"><span class="badge badge-primary" style="background-color: #051d60;">`+e.archivos[i]+`</span></h5>
                                    </a>
                                </figure>`;
                    countimg = countimg + 40;
                }

                if (extension === "AVI" || extension === "MOV" || extension === "WMV" || extension === "FLv" || extension === "MP4" || extension === "avi" || extension === "mov" || extension === "wmv" || extension === "flv" || extension === "mp4") {
                    videos += `<div class="col-3 col-sm-2 placeholder">
                      <img src="images/play.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openVideo('`+url+`','`+e.archivos[i]+`')">
                      <div class="text-muted">`+e.archivos[i]+`</div>
                    </div>`;
                }

                if (extension === "PTX" || extension === "PSX" || extension === "PSM" || extension === "ptx" || extension === "psx" || extension === "psm") {
                    presentacion += `<div class="col-3 col-sm-2 placeholder">
                      <img src="images/ppt.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                      <div class="text-muted">`+e.archivos[i]+`</div>
                    </div>`;
                }

                if (extension === "PDF" || extension === "pdf"){
                    pdf += `<div class="col-3 col-sm-2 placeholder">
                      <img src="images/pdf.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                      <div class="text-muted">`+e.archivos[i]+`</div>
                    </div>`;
                }

                if (extension === "OCX" || extension === "ocx" || extension === "DOC" || extension === "doc"){
                    documentos += `<div class="col-3 col-sm-2 placeholder">
                                      <img src="images/doc.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                                      <div class="text-muted">`+e.archivos[i]+`</div>
                                    </div>`;
                }

                if (extension === "LSX" || extension === "lsx" || extension === "LSM" || extension === "lsm" || extension === "XML" || extension === "xml"){
                    excel += `<div class="col-3 col-sm-2 placeholder">
                                      <img src="images/stats.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                                      <div class="text-muted">`+e.archivos[i]+`</div>
                                    </div>`;
                }
                //PDF, DOC, XLS y PPT
            }
            images += "</div>";
            if (images!='<div id="carousel"></div>') {
                $("#con-img").html('<section id="section_images">'+ images +'</section>');
            }else{
                $("#section_images").remove();
            }
            $("#section_archive").html(folder);
            $("#section_archive").append(videos);
            $("#section_archive").append(presentacion);
            $("#section_archive").append(pdf);
            $("#section_archive").append(documentos);
            $("#section_archive").append(excel);
            cont++;
            }
        });
    //}
}

pantalla_completa = function() {
    var contenvideo = document.getElementById("play_video_modal");
    if (contenvideo) {

        if (contenvideo.requestFullscreen) {
            contenvideo.requestFullscreen();
        }
        else if (contenvideo.msRequestFullscreen) {
            contenvideo.msRequestFullscreen();
        }
        else if (contenvideo.mozRequestFullScreen) {
            contenvideo.mozRequestFullScreen();
        }
        else if (contenvideo.webkitRequestFullScreen) {
            contenvideo.webkitRequestFullScreen();
        }
    }
}



$.fn.extend({
    treed: function (o) {

      var openedClass = 'fa-folder-open';
      var closedClass = 'fa-folder';

      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').each(function () {
            var branch = $(this); //li with children ul
            branch.prepend('<i class="fa ' + closedClass + '" aria-hidden="true"></i>');
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree2').treed({openedClass:'fa-folder-open', closedClass:'fa-folder'});

openVideo = function(url, nombre) {
    dir = "storage/"+url+"/"+nombre;
    video = `<div id="play_video_modal" class="caja">
                <video id="Video1" src="`+dir+`" loop preload="auto">
                  Tu navegador no implementa el elemento <code>video</code>.
                </video>
                <div id="buttonbar">
                    <button class="video_button" id="restart" onclick="restart();"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                    <button class="video_button" id="rew" onclick="skip(-10)"><i class="fa fa-backward" aria-hidden="true"></i></button>
                    <button class="video_button" id="play" onclick="vidplay()"><i class="fa fa-play" aria-hidden="true"></i></button>
                    <button class="video_button" id="fastFwd" onclick="skip(10)"><i class="fa fa-forward" aria-hidden="true"></i></button>
                    <button class="video_button pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <button class="video_button pull-right" onclick="pantallaCompletaVideo()"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>
                </div>
            <div>`;

    $("#modal_video").html(video);
}

openDocument = function(url, nombre) {
    dir = "storage/"+url+"/"+nombre;
    contenido = `<iframe id="miframe" class="miiframeblog" src="http://docs.google.com/viewer?url=/`+dir+`&amp;embedded=true" frameborder="0" onmousedown="return false" allowfullscreen></iframe>`;
    $("#modal_video").html(contenido);
}



function vidplay() {
   var video = document.getElementById("Video1");
   var button = $("#play");

   if (video.paused) {
      video.play();
      button.html('<i class="fa fa-pause" aria-hidden="true"></i>');
   } else {
      video.pause();
      button.html('<i class="fa fa-play" aria-hidden="true"></i>');
   }
}

function restart() {
    var video = document.getElementById("Video1");
    video.currentTime = 0;
}

function skip(value) {
    var video = document.getElementById("Video1");
    video.currentTime += value;
}

$(function () {
    $('#myModal').on('hidden.bs.modal', function (e) {
        var video = document.getElementById("Video1");
        var button = $("#play");
          video.pause();
          button.html('<i class="fa fa-play" aria-hidden="true"></i>');
    });
});
