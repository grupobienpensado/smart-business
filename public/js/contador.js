setTimeout('modificar_tiermpo()',60000);
setTimeout('preguntar_porsesion()',600000);


function modificar_tiermpo(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var jqxhr = $.ajax({
             url: "/tiemponuevo",
                data: {
                    _token: CSRF_TOKEN,
                    id: id_tiempo
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    console.log(e.msg);
                    setTimeout('modificar_tiermpo()',60000);
                }
        })
}

function guardarruta(carpeta,id_tiempo){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var jqxhr = $.ajax({
             url: "/rutausada",
                data: {
                    _token: CSRF_TOKEN,
                    carpeta: carpeta,
                    id: id_tiempo
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    console.log(e.msg);
                    $('#archivo-ruta').val(carpeta);
                    setTimeout('modificar_tiempo_ruta("'+e.tiemporuta+'","'+carpeta+'")',10000);
                }
        })
}

function modificar_tiempo_ruta(tiemporuta,carpeta){
	if($('#archivo-ruta').val() === carpeta){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var jqxhr = $.ajax({
             url: "/rutausadaeditar",
                data: {
                    _token: CSRF_TOKEN,
                    id: tiemporuta
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    console.log(e.msg);
                    setTimeout('modificar_tiempo_ruta("'+tiemporuta+'","'+carpeta+'")',10000);
                }
        })
    }else{
    	carpeta=$('#archivo-ruta').val();
    	guardarruta(carpeta,id_tiempo);
    }
}

function preguntar_porsesion(){
	swal({
		  title: 'Desea seguir conectado!',
		  type: 'info',
		  text: 'Presione Ok, para continuar con la sesión',
		  timer: 10000
		}).then(
		  function () {},
		  // handling the promise rejection
		  function (dismiss) {
		    if (dismiss === 'timer') {
		      location.href='/megaarchivo';
		    }
		  })
}
