html = '';
var min=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","U","V","W","X","Y","Z"];
var cont=0;
var contents = {};
var pos = 1;

/*$(function () {
    initrepcarp();
});*/

initrepcarp = function () {
    $(".breadcrumb").html('<li id="0" class="breadcrumb-item active" onclick="initrepcarp()"><a href="#" class="resaltarnav">INICIO</a></li>');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var jqxhr = $.ajax({
        url: "../initdata",
        data: {
            _token: CSRF_TOKEN
        },
        cache: false,
        type: 'POST',
        success: function(e){              



            videos="";
            presentacion="";
            pdf="";
            documentos="";
            excel="";
            images='<div id="carousel">';
            countimg=0;
            folder="";

            for (var i = 1; i < cont; i++) {
                if ($("#"+min[i]+0).html() === undefined) {
                    cont = i;
                    break;
                }
            }

            for (i = 0; i < e.folder.length; i++) {
                folder += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                        <img id="`+min[cont]+i+`" src="../images/folder.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="`+ e.folder[i] +`" onclick="buscarfolderandfile('`+ e.folder[i] +`', this.id)">
                                        <div id="TEXT`+min[cont]+i+`" class="text-muted">`+ e.folder[i] +`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                  </div>
                                </div>
                </div>`;

            }
            for (i = 0; i < e.archivos.length; i++){
                total = e.archivos[i].length;
                c=total-1;
                b = c-1;
                a = b-1;
                extension = e.archivos[i].substr(a,b,c);
                if (extension === "BMP" || extension === "GIF" || extension === "PNG" || extension === "JPG" || extension === "TIF" || extension === "bmp" || extension === "gif" || extension === "png" || extension === "jpg" || extension === "tif") {
                    images += `<figure id="item`+countimg+`" class="carouselItem trans3d"><div class="carouselItemInner trans3d">
                                <a data-gallery="" title="`+e.archivos[i]+`" href="../storage/`+e.archivos[i]+`">
                                        <img src="../storage/`+e.archivos[i]+`" class="rotate">
                                        <h5 style="position: absolute;top: 0px;"><span class="badge badge-primary" style="background-color: #051d60;">`+e.archivos[i]+`</span></h5>
                                    </a>
                            </div></figure>`;
                   /* `<figure style="transform: rotateY(`+countimg+`deg) translateZ(388px);">
                                    <a data-gallery="" title="`+e.archivos[i]+`" href="../storage/`+e.archivos[i]+`">
                                        <img src="../storage/`+e.archivos[i]+`" class="rotate">
                                        <h5 style="position: absolute;top: 0px;"><span class="badge badge-primary" style="background-color: #051d60;">`+e.archivos[i]+`</span></h5>
                                    </a>
                                </figure>`;*/
                    countimg = countimg + 40;
                }

                if (extension === "AVI" || extension === "MOV" || extension === "WMV" || extension === "FLv" || extension === "MP4" || extension === "avi" || extension === "mov" || extension === "wmv" || extension === "flv" || extension === "mp4") {
                    videos += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                         <img src="../images/video.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openVideo('','`+e.archivos[i]+`')">
                                         <div class="text-muted">`+e.archivos[i]+`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                  </div>
                                </div>

                    </div>`;
                }

                if (extension === "PTX" || extension === "PSX" || extension === "PSM" || extension === "ptx" || extension === "psx" || extension === "psm") {
                    presentacion += `<div class="col-3 col-sm-2 placeholder">
                                        <div class="row">
                                          <div class="col-10 col-sm-10" style="text-align: right;">
                                                 <img src="../images/ppt.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                                                 <div class="text-muted">`+e.archivos[i]+`</div>
                                          </div>
                                          <div class="col-2 col-sm-2">
                                            <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                            <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                            <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                          </div>
                                        </div>
                    </div>`;
                }

                if (extension === "PDF" || extension === "pdf"){
                    pdf += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                         <img src="../images/pdf.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                                         <div class="text-muted">`+e.archivos[i]+`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                  </div>
                                </div>
                    </div>`;
                }

                if (extension === "OCX" || extension === "ocx" || extension === "DOC" || extension === "doc"){
                    documentos += `<div class="col-3 col-sm-2 placeholder">
                                       <div class="row">
                                          <div class="col-10 col-sm-10" style="text-align: right;">
                                                <img src="../images/doc.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                                                <div class="text-muted">`+e.archivos[i]+`</div>
                                          </div>
                                          <div class="col-2 col-sm-2">
                                            <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                            <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                            <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                          </div>
                                        </div>
                                    </div>`;
                }

                if (extension === "LSX" || extension === "lsx" || extension === "LSM" || extension === "lsm" || extension === "XML" || extension === "xml"){
                    excel += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                        <img src="../images/stats.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('','`+e.archivos[i]+`')">
                                        <div class="text-muted">`+e.archivos[i]+`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                  </div>
                                </div>
                             </div>`;
                }
                //PDF, DOC, XLS y PPT
            }
            images += "</div>";
            if (images!='<div id="carousel"></div>') {
                $("#con-img").html('<section id="section_images">'+ images +'</section>');

            }else{
                $("#section_images").remove();
            }
            $("#section_archive").html(folder);
            $("#section_archive").append(videos);
            $("#section_archive").append(presentacion);
            $("#section_archive").append(pdf);
            $("#section_archive").append(documentos);
            $("#section_archive").append(excel);
            cont++;
        }
    });
}



    var viewFullScreen = document.getElementById("view-fullscreen");
    if (viewFullScreen) {
        viewFullScreen.addEventListener("click", function () {
            var docElm = document.documentElement;
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            else if (docElm.msRequestFullscreen) {
                docElm.msRequestFullscreen();
            }
            else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
        }, false);
    }

    var cancelFullScreen = document.getElementById("cancel-fullscreen");
    if (cancelFullScreen) {
        cancelFullScreen.addEventListener("click", function () {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
            else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            }
            else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }, false);
    }


    var fullscreenState = document.getElementById("fullscreen-state");
    if (fullscreenState) {
        document.addEventListener("fullscreenchange", function () {
            fullscreenState.innerHTML = (document.fullscreenElement)? "" : "not ";
        }, false);

        document.addEventListener("msfullscreenchange", function () {
            fullscreenState.innerHTML = (document.msFullscreenElement)? "" : "not ";
        }, false);

        document.addEventListener("mozfullscreenchange", function () {
            fullscreenState.innerHTML = (document.mozFullScreen)? "" : "not ";
        }, false);

        document.addEventListener("webkitfullscreenchange", function () {
            fullscreenState.innerHTML = (document.webkitIsFullScreen)? "" : "not ";
        }, false);
    }

var url_general='';
buscarfolderandfile = function(url, id, p, carpeta) {
    $('#archivo-ruta').val(url);
    var num_videos=0;
    var num_imagenes=0;
    var num_documentos=0;
    url_general=url;
    $("#section_archive").html('');
    $("#section_videos").html('');
    $("#section_documentos").html('');

    for (var i = 0; i < pos; i++) {
        $( "#"+i+",a" ).removeClass( "resaltarnav" );
    }
    if (p) {
        for (var i = p+1; i < pos; i++) {
            $( "li" ).remove("#"+i);
        }
        pos=p;
        pos++;
    } else {
        titulo = $("#TEXT"+id).html();
        if(titulo===undefined){
            if(carpeta===undefined){
                mostrarr=url;
                $(".breadcrumb").append('<li id="'+pos+'" class="breadcrumb-item" onclick="buscarfolderandfile(\''+url+'\', this.id, '+pos+',\''+mostrarr+'\')"><a href="#" class="resaltarnav">'+mostrarr+'</a></li>');
            }else if(carpeta===''){
                mostrarr=url;
                $(".breadcrumb").append('<li id="'+pos+'" class="breadcrumb-item" onclick="buscarfolderandfile(\''+url+'\', this.id, '+pos+',\''+mostrarr+'\')"><a href="#" class="resaltarnav">'+mostrarr+'</a></li>');
            }else if(carpeta==='RENAME'){

            }else{
                mostrarr=carpeta;
                $(".breadcrumb").append('<li id="'+pos+'" class="breadcrumb-item" onclick="buscarfolderandfile(\''+url+'\', this.id, '+pos+',\''+mostrarr+'\')"><a href="#" class="resaltarnav">'+mostrarr+'</a></li>');
            }

        }else{
            $(".breadcrumb").append('<li id="'+pos+'" class="breadcrumb-item" onclick="buscarfolderandfile(\''+url+'\', this.id, '+pos+',\''+mostrarr+'\')"><a href="#" class="resaltarnav">'+titulo+'</a></li>');
        }

        pos++;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    //if ( contents[ id ] === undefined ) {
<<<<<<< HEAD
        var jqxhr = $.ajax({ 
=======
        var jqxhr = $.ajax({
>>>>>>> origin/master
            url: "/enlaces",
            data: {
                _token: CSRF_TOKEN,
                url: url
            },
            cache: false,
            type: 'POST',
            success: function(e){
                //contents[ id ] = e;
               // html = titulo+'<ul>';
                /*for (var i = 1; i < cont; i++) {
                    if ($("#"+min[i]+0).html() === undefined) {
                        cont = i;
                        break;
                    }
                } */
            /*for (i = 0; i < e.folder.length; i++) {
                html += '<li id="'+min[cont]+i+'" onclick="buscarfolderandfile(\''+url+'/'+e.folder[i]+'\', this.id)"><a href="#"><i class="fa fa-folder"></i><span style="text-transform: capitalize;overflow:hidden;">' + e.folder[i] + '</span></a></li>';
            }*/
            //html += '</ul>';
            //$("#"+id).html(html);

            videos="";
            presentacion="";
            pdf="";
            documentos="";
            excel="";
            images='<div id="carousel">';
            images2='';

            countimg=0;

            folder="";

            for (var i = 1; i < cont; i++) {
                if ($("#"+min[i]+0).html() === undefined) {
                    cont = i;
                    break;
                }
            }

            for (i = 0; i < e.folder.length; i++) {
                folder += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                        <img id="`+min[cont]+i+`" src="`+direccion+`/images/folder.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="`+ e.folder[i] +`" onclick="buscarfolderandfile('`+url+`/`+ e.folder[i] +`', this.id,'','`+ e.folder[i] +`')">
                                        <div id="TEXT`+min[cont]+i+`" class="text-muted">`+ e.folder[i] +`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" style="color: darkorange;cursor: pointer;" onclick="editar('/storage/`+url+`/`+ e.folder[i] +`','`+ e.folder[i] +`','`+url+`','RENAME','')"></i>
                                    <i class="fa fa-times" aria-hidden="true" style="color: red; cursor: pointer; " onclick="eliminar_carpeta('/storage/`+url+`/`+ e.folder[i] +`','`+ e.folder[i] +`','`+url+`','RENAME','')"></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" style="color: green; cursor: pointer;"></i>
                                  </div>
                                </div>
                </div>`;


            }
            imagennumero=0;
            for (i = 0; i < e.archivos.length; i++){
                total = e.archivos[i].length;
                c=total-1;
                b = c-1;
                a = b-1;
                extension = e.archivos[i].substr(a,b,c);
                if (extension === "BMP" || extension === "GIF" || extension === "PNG" || extension === "JPG" || extension === "TIF" || extension === "bmp" || extension === "gif" || extension === "png" || extension === "jpg" || extension === "tif") {
                    /*images +=`<figure style="transform: rotateY(`+countimg+`deg) translateZ(388px);">
                                    <a data-gallery="" title="`+e.archivos[i]+`" href="`+direccion+`/storage/`+url+`/`+e.archivos[i]+`">
                                        <img src="`+direccion+`/storage/`+url+`/`+e.archivos[i]+`" onclick="abrir_imagen('`+url+`','`+e.archivos[i]+`')" class="rotate">
                                        <h5 style="position: absolute;top: 0px;"><span class="badge badge-primary" style="background-color: #051d60;">`+e.archivos[i]+`</span></h5>
                                        <i class="fa fa-pencil" aria-hidden="true" style="background-color:#fff; color: darkorange;cursor: pointer; position: absolute;bottom: 0px;z-index: 9;right: 29px;" onclick="editar_imagen('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                        <i class="fa fa-times" aria-hidden="true" style="background-color:#fff; color: red; cursor: pointer; position: absolute;bottom: 0px;z-index: 9;right: 16px;" onclick="eliminar_imagen('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                        <i class="fa fa-circle-o-notch" aria-hidden="true" style="background-color:#fff; color: green; cursor: pointer; position: absolute;bottom: 0px;z-index: 9;right: 0px;" onclick="reemplazar_imagen('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    </a>

                                </figure>`;*/
                    nombre=(e.archivos[i]).replace("."+extension, "");
                    url_img=url.replace( new RegExp("[\/]","g"),"¬" );
                    images2 +=`<div class="col-md-2">
                                  <div class="row">
                                    <div class="col-md-10 sombra">
                                      <img class="img-fotos" id="imagennumero`+imagennumero+`" src="`+direccion+`/storage/`+url+`/`+e.archivos[i]+`" style="height: 100%; margin-left:-7%;" onclick="abrir_visor(\'`+url_img+`¬`+e.archivos[i]+`\','`+url+`','`+e.archivos[i]+`')">
                                    </div>
                                    <div class="col-md-2">
                                      <i class="fa fa-pencil" onclick="editar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;"></i>
                                      <i class="fa fa-times" onclick="eliminar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; "></i>
                                      <i class="fa fa-circle-o-notch" onclick="reemplazar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;"></i>
                                    </div>
                                  </div>
                                  <div class="row titulo-imagen">
                                    <div class="col-md-10">
                                      <h4 class="letra-imagen">`+nombre+`</h4>
                                    </div>
                                  </div>
                                </div>`;
                    countimg = countimg + 40;
                    num_imagenes++;
                    imagennumero++;
                }

                if (extension === "AVI" || extension === "MOV" || extension === "WMV" || extension === "FLv" || extension === "MP4" || extension === "avi" || extension === "mov" || extension === "wmv" || extension === "flv" || extension === "mp4") {
                    quitar="."+extension;
                    nombre=e.archivos[i];
                    nombre=nombre.replace(quitar, "");
                    videos += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                        <img src="`+direccion+`/images/video.png" style="max-width: 100%;" width="160" height="129" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openVideo('`+url+`','`+e.archivos[i]+`')">
                                        <div class="text-muted" style="position: absolute; top: 100px; color: #fff !important;margin-left: 25%;">`+nombre+`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;" onclick="editar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer; " onclick="eliminar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;" onclick="reemplazar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                  </div>
                                </div>
                    </div>`;
                    num_videos++;
                }

                if (extension === "PTX" || extension === "PSX" || extension === "PSM" || extension === "ptx" || extension === "psx" || extension === "psm") {
                    presentacion += `<div class="col-3 col-sm-2 placeholder">
                                        <div class="row">
                                          <div class="col-10 col-sm-10" style="text-align: right;">
                                                <img src="`+direccion+`/images/powerpoint.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                                                <div class="text-muted">`+e.archivos[i]+`</div>
                                          </div>
                                          <div class="col-2 col-sm-2">
                                            <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;" onclick="editar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                            <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer;" onclick="eliminar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                            <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;" onclick="reemplazar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                          </div>
                                        </div>
                    </div>`;
                    num_documentos++;
                }

                if (extension === "PDF" || extension === "pdf"){
                    pdf += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                        <img src="`+direccion+`/images/pdf.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                                        <div class="text-muted">`+e.archivos[i]+`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;" onclick="editar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer;" onclick="eliminar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;" onclick="reemplazar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                  </div>
                                </div>
                    </div>`;
                    num_documentos++;
                }

                if (extension === "OCX" || extension === "ocx" || extension === "DOC" || extension === "doc"){
                    documentos += `<div class="col-3 col-sm-2 placeholder">
                                    <div class="row">
                                      <div class="col-10 col-sm-10" style="text-align: right;">
                                            <img src="`+direccion+`/images/word.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                                            <div class="text-muted">`+e.archivos[i]+`</div>
                                      </div>
                                      <div class="col-2 col-sm-2">
                                        <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;" onclick="editar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                        <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer;" onclick="eliminar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                        <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;" onclick="reemplazar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                      </div>
                                    </div>
                                 </div>`;
                    num_documentos++;
                }

                if (extension === "CSV" || extension === "csv" || extension === "LSX" || extension === "lsx" || extension === "LSM" || extension === "lsm" || extension === "XML" || extension === "xml"){
                    excel += `<div class="col-3 col-sm-2 placeholder">
                                <div class="row">
                                  <div class="col-10 col-sm-10" style="text-align: right;">
                                        <img src="`+direccion+`/images/excel.png" width="100" height="100" class="img-fluid rounded-circle menu" alt="Icono Generico" data-toggle="modal" data-target="#myModal" onclick="openDocument('`+url+`','`+e.archivos[i]+`')">
                                        <div class="text-muted">`+e.archivos[i]+`</div>
                                  </div>
                                  <div class="col-2 col-sm-2">
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editar" style="color: darkorange;cursor: pointer;" onclick="editar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    <i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Eliminar" style="color: red; cursor: pointer;" onclick="eliminar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                    <i class="fa fa-circle-o-notch" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Reemplazar" style="color: green; cursor: pointer;" onclick="reemplazar('/storage/`+url+`/`+ e.archivos[i] +`','`+ e.archivos[i] +`','`+url+`','RENAME','`+extension+`')"></i>
                                  </div>
                                </div>
                            </div>`;
                    num_documentos++;
                }
                //PDF, DOC, XLS y PPT
            }
            images += "</div>";
            if (images!='<div id="carousel"></div>') {
                /*$("#con-img").html('<section id="section_images">'+ images +'</section>');*/
                $("#galeria_imagenes").html(images2);
                $( ".img-fotos" ).hover(
                  function() {
                    $( this ).addClass( "animated" ).addClass( "bounce" );
                  }, function() {
                    $( this ).removeClass( "animated" ).removeClass( "bounce" );
                  }
                );
                /*enviar a carrusel*/
                /*var rut = ($('#archivo-ruta').val()).replace( new RegExp("[\/]","g"),"¬" );
                $('#galeria_images_3d').attr('src','/carruselimagenes/'+rut);*/
            }else{
                /*$("#section_images").remove();*/
                $("#galeria_imagenes").html(images2);
                $( ".img-fotos" ).hover(
                  function() {
                    $( this ).addClass( "animated" ).addClass( "bounce" );
                  }, function() {
                    $( this ).removeClass( "animated" ).removeClass( "bounce" );
                  }
                )
                /*enviar a carrusel*/
                /*var rut = ($('#archivo-ruta').val()).replace( new RegExp("[\/]","g"),"¬" );

                $('#galeria_images_3d').attr('src','/carruselimagenes/'+rut);*/
            }
            $("#section_archive").html(folder);
            $("#section_videos").html('<div class="row">'+videos+'</div>');
            documentos_html='<div class="row">'+presentacion+pdf+documentos+excel+'</div>';
            $("#section_documentos").html(documentos_html);

            $("#numero_videos").html("Videos ("+num_videos+")");
            $("#numero_videos_seleccionado").html("Videos ("+num_videos+")");
            $("#numero_imagenes").html("Imagenes ("+num_imagenes+")");
            $("#numero_imagenes_seleccionado").html("Imagenes ("+num_imagenes+")");
            $("#numero_documentos").html("Documentos ("+num_documentos+")");
            $("#numero_documentos_seleccionado").html("Documentos ("+num_documentos+")");

            cont++;
            }
        });
    //}
    var size = $(".img-fotos").size();
    alert(size);
}

pantalla_completa = function() {
    var contenvideo = document.getElementById("play_video_modal");
    if (contenvideo) {

        if (contenvideo.requestFullscreen) {
            contenvideo.requestFullscreen();
        }
        else if (contenvideo.msRequestFullscreen) {
            contenvideo.msRequestFullscreen();
        }
        else if (contenvideo.mozRequestFullScreen) {
            contenvideo.mozRequestFullScreen();
        }
        else if (contenvideo.webkitRequestFullScreen) {
            contenvideo.webkitRequestFullScreen();
        }
    }
}



$.fn.extend({
    treed: function (o) {

      var openedClass = 'fa-folder-open';
      var closedClass = 'fa-folder';

      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').each(function () {
            var branch = $(this); //li with children ul
            branch.prepend('<i class="fa ' + closedClass + '" aria-hidden="true"></i>');
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree2').treed({openedClass:'fa-folder-open', closedClass:'fa-folder'});

openVideo = function(url, nombre) {
    dir = direccion+"/storage/"+url+"/"+nombre;
    video = `<div id="play_video_modal" class="caja">
                <video id="Video1" src="`+dir+`" loop preload="auto" style="width:0; max-height:600px !important;">
                  Tu navegador no implementa el elemento <code>video</code>.
                </video>
                <div id="buttonbar">
                    <button class="video_button" id="restart" onclick="restart();"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                    <button class="video_button" id="rew" onclick="skip(-10)"><i class="fa fa-backward" aria-hidden="true"></i></button>
                    <button class="video_button" id="play" onclick="vidplay()"><i class="fa fa-play" aria-hidden="true"></i></button>
                    <button class="video_button" id="fastFwd" onclick="skip(10)"><i class="fa fa-forward" aria-hidden="true"></i></button>
                    <button class="video_button pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <button class="video_button pull-right" onclick="pantallaCompletaVideo()"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>
                </div>
            <div>`;

    $("#modal_video").html(video);

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var jqxhr = $.ajax({
         url: "/archivousado",
            data: {
                _token: CSRF_TOKEN,
                url: url,
                nombre:nombre,
                id:id_tiempo
            },
            cache: false,
            type: 'POST',
            success: function(e){
                console.log(e.msg);
            }
    })
}

abrir_imagen = function(url, nombre){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var jqxhr = $.ajax({
         url: "/archivousado",
            data: {
                _token: CSRF_TOKEN,
                url: url,
                nombre:nombre,
                id:id_tiempo
            },
            cache: false,
            type: 'POST',
            success: function(e){
                console.log(e.msg);
            }
    })
}

openDocument = function(url, nombre) {
    dir = direccion+"/storage/"+url+"/"+nombre;
    contenido = `<iframe src="http://docs.google.com/gview?url=`+dir+`&embedded=true&toolbar=hide" style="width:100%; height:650px;" frameborder="0"></iframe>`;
    $("#modal_video").html(contenido);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var jqxhr = $.ajax({
         url: "/archivousado",
            data: {
                _token: CSRF_TOKEN,
                url: url,
                nombre:nombre,
                id:id_tiempo
            },
            cache: false,
            type: 'POST',
            success: function(e){
                console.log(e.msg);
            }
    })
}



function vidplay() {
   var video = document.getElementById("Video1");
   var button = $("#play");

   if (video.paused) {
      video.play();
      button.html('<i class="fa fa-pause" aria-hidden="true"></i>');
   } else {
      video.pause();
      button.html('<i class="fa fa-play" aria-hidden="true"></i>');
   }
}

function restart() {
    var video = document.getElementById("Video1");
    video.currentTime = 0;
}

function skip(value) {
    var video = document.getElementById("Video1");
    video.currentTime += value;
}

$(function () {
    $('#myModal').on('hidden.bs.modal', function (e) {
        var video = document.getElementById("Video1");
        var button = $("#play");
          video.pause();
          button.html('<i class="fa fa-play" aria-hidden="true"></i>');
    });
});

editar_imagen =function(ruta,nombre,recargar,carpeta,extencion){
    setTimeout('editar("'+ruta+'","'+nombre+'","'+recargar+'","'+carpeta+'","'+extencion+'")',2000);
}

editar = function(ruta,nombre,recargar,carpeta,extencion){

    swal({
          title: 'Ingrese el nuevo nombre para Reemplazar "'+nombre+'"',
          input: 'text',
          showCancelButton: true,
          confirmButtonText: "Editar",
          cancelButtonText: "Cancelar",
          showLoaderOnConfirm: true
        }).then(function (input){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            var jqxhr = $.ajax({
                 url: "/editar",
                    data: {
                        _token: CSRF_TOKEN,
                        anterior: ruta,
                        nombre_anterior:nombre,
                        nombre_nuevo:input,
                        extencion:extencion
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal({
                          title: 'Realizado',
                          text: e.msj,
                          type: 'success'
                      }).then(function () {
                            buscarfolderandfile(recargar,'A0','',carpeta);
                        })

                    }
            })
        })
}

eliminar_carpeta =function(ruta,nombre,recargar,carpeta,extencion){
    swal({
          title: 'Esta seguro?',
          text: "Desea Eliminarlo!",
          type: 'warning',

          showCancelButton: true,
          confirmButtonText: "Eliminar",
          cancelButtonText: "Cancelar",
          showLoaderOnConfirm: true
        }).then(function (){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var jqxhr = $.ajax({
                 url: "/eliminarcarpeta",
                    data: {
                        _token: CSRF_TOKEN,
                        anterior: ruta,
                        nombre_anterior:nombre,
                        recargar:recargar,
                        extencion:extencion
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        if(e==="No"){
                            swal({
                                  title: 'Error',
                                  text: 'No se puede eliminar, asegurese de que la carpeta este vacia',
                                  type: 'error'
                              })
                        }else{
                            swal({
                                  title: 'Realizado',
                                  text: e,
                                  type: 'success'
                              }).then(function () {
                                    buscarfolderandfile(recargar,'A0','',carpeta);
                                })
                        }
                    }
            })
        })
}

eliminar_imagen =function(ruta,nombre,recargar,carpeta,extencion){
    setTimeout('eliminar("'+ruta+'","'+nombre+'","'+recargar+'","'+carpeta+'","'+extencion+'")',2000);
}


eliminar = function(ruta,nombre,recargar,carpeta,extencion){

    swal({
          title: 'Esta seguro?',
          text: "Desea Eliminarlo!",
          type: 'warning',

          showCancelButton: true,
          confirmButtonText: "Eliminar",
          cancelButtonText: "Cancelar",
          showLoaderOnConfirm: true
        }).then(function (){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            var jqxhr = $.ajax({
                 url: "/eliminar",
                    data: {
                        _token: CSRF_TOKEN,
                        anterior: ruta,
                        nombre_anterior:nombre,
                        recargar:recargar,
                        extencion:extencion
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal({
                          title: 'Realizado',
                          text: e.msj,
                          type: 'success'
                      }).then(function () {
                            buscarfolderandfile(recargar,'A0','',carpeta);
                        })

                    }
            })
        })
}


cargar =function(){
    swal({
      title: 'Que de desea Hacer?',
      input: 'select',
      inputOptions: {
        'carpeta': 'Crear Carpeta',
        'archivo': 'Subir Archivo'
      },
      inputPlaceholder: 'Seleccione',
      showCancelButton: true,
      inputValidator: function (value) {
        return new Promise(function (resolve, reject) {
          if (value != '') {
            resolve()
          } else {
            reject('Debe seleccionar una opción!')
          }
        })
      }
    }).then(function (result) {
      if(result==='carpeta'){
        swal({
          title: 'Ingrese el nombre de la carpeta',
          input: 'text',
          showCancelButton: true,
          inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
              if (value) {
                resolve()
              } else {
                reject('Debe escribir un nombre!')
              }
            })
          }
        }).then(function (result) {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            var jqxhr = $.ajax({
                 url: "/crearmega",
                    data: {
                        _token: CSRF_TOKEN,
                        url: url_general,
                        nombre:result
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal({
                          title: 'Realizado',
                          text: e.msj,
                          type: e.type
                      }).then(function () {
                            buscarfolderandfile(url_general,'A0','','RENAME');
                        })

                    }
            })

        })
      }else{
        breadcrumb_html=$('.breadcrumb').html();
        breadcrumb_html=breadcrumb_html.replace(/'/g, "%");
        breadcrumb_html=breadcrumb_html.replace(/"/g, "°");
        $('#codigo_html').val(breadcrumb_html);
        $("#url").val(url_general);
        $(".abrir-modal").click();
      }
    })
}

reemplazar_imagen =function(ruta,nombre,recargar,carpeta,extencion){
    setTimeout('reemplazar("'+ruta+'","'+nombre+'","'+recargar+'","'+carpeta+'","'+extencion+'")',2000);
}

reemplazar =function(ruta,nombre,recargar,carpeta,extencion){
    $("#url2").val(ruta);
    $("#url3").val(recargar);
    $("#nombre_archivo").val(nombre);
    $("#extencion").val(extencion);
    $(".abrir-modal2").click();
}

/*
guardar_archivo =function(){
     var jqxhr = $.post("/creararchivomega", $("#formulario_archivos").serialize())

}*/
