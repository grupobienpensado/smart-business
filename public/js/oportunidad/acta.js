Ge=0;
url_basico=$('#url_basico').val();
fecha_actual=$('#fecha_actual').val();
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
$('body').on('click','.new-acta',function(){
    /*$('.crear-cita').modal('show');*/
    if(permiso_acta_crear_editar == "Si"){
       $('#listado_actas').css('display','none');
       $('#crear_acta').css('display','block');
    }else{
        swal('Advertencia','Usted no tiene permisos para crear un acta','warning');
    }
});
$('body').on('click','.img-atras',function(){
    $('.todas-ventanas').css('display','none');
    $('#listado_actas').css('display','block');
});
$('body').on('click','#añadir_invitados',function(){
   $('.añadir-invitados').modal('show');
});
$('body').on('click','.lista-usuarios',function(){
    id=$(this).attr('id');
    id=id.replace('user','');
    ids=$('#invitados').val();
    if( $(this).prop('checked') ) {
         $.ajax({
            url: url_basico+"/consultarinvitado/"+id,
            cache: false,
            contentType: false,
            processData: false,
            type: 'GET',
            success: function(e){
                $('#listado_invitados').prepend(e.html);
            }
        });
        $('#invitados').val(ids+'%%'+id);
    }else{
        $('#invitado'+id).remove();
        ids=ids.replace('%%'+id,'');
        $('#invitados').val(ids);
    }

});

function eliminar(id){
    $('#invitado'+id).remove();
    ids=$('#invitados').val();
    ids=ids.replace('%%'+id,'');
    $('#invitados').val(ids);
}
x=0;
$('body').on('click','.crear-inv',function(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/actasreunion-accion",
        data: {
            _token: CSRF_TOKEN,
            accion: 1,
            numero: x
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#contenedor-invi-exter').prepend(e.data['html_invitado_externo']);
            x++;
        }
    });
});

/**
 * Funcion para modificar el email de los invitados externos
 * @param STRING valor NOMBRE DEL INVITADO EXTERNO
 * @param INT id  IDENTIFICACION DEL ELEMENTO
 **/
function invitado_externo(valor,id){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: url_basico+"/actasreunion-accion",
        data: {
            _token: CSRF_TOKEN,
            accion: 2,
            nombre: valor
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#'+id).val(e.data['correo']);
        }
    });
}

function eliminar_invitado_adicional(id){
    $('#invitadoexterno'+id).remove();
}

$('body').on('click','.guardar',function(){
    event.preventDefault();
	if(true === $("#formulario_cita").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a guardar la citación",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                $('.btn_guardar').click();
            });
    }
});

$(function () {
$('.datatime').bootstrapMaterialDatePicker({
      time: true,
      date: true,
      clearButton: true,
      nowButton: true,
      lang: 'es',
      format : 'YYYY-MM-DD HH:mm',
      minDate : new Date(),
      cancelText : 'Cancelar',
      okText: 'Aceptar',
      nowText: 'Nuevo',
      clearText: 'Limpiar',
      weekStart : 0
  });

});

function iniciar_reunion(id){
    $('#contenedor_asistencia').html('<div class="col-md-12" style="text-aling: center;"><img src="'+url_basico+'/images/configuracion_crm/configuracion.gif" style="background: none;"></div>');
    $('#listado_actas').css('display','none');
    $('#iniciar_reunion').css('display','block');
    $('#btn_guardar_continuar').css('display','block');
    $('#id_reunion_iniciar').val(id);
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/oportunidad/acta-action",
        data: {
            _token: CSRF_TOKEN,
            id: id,
            accion: 5
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('#contenedor_asistencia').html(e.html);
            $('#select_responsable').html(e.select_responsable);
            html_responsable=e.html_responsable;
        }
    });
}
xx=1;
$('body').on('click','.crear-tema',function(){
    xx++;
    html=`<div class="row" id="tema`+xx+`">
            <div class="col-md-11">
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title titulo-tema">
                        <a class="collapsed titulo-tema`+xx+`" data-toggle="collapse" data-parent="#accordion" href="#collapse`+xx+`"><b>Tema #<span id="num_tema`+xx+`">`+xx+`</span></b> <i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                    </h4>
                </div>
                <div id="collapse`+xx+`" class="panel-collapse collapse">
                    <input type="hidden" id="id_tema`+xx+`" name="temas[`+xx+`][id]" value="">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Titulo</p>
                                    <label for="recipient-name" class="form-control-label"></label>
                                    <input class="form-control titulo-tema" id="titulo-tema`+xx+`" name="temas[`+xx+`][titulo]" maxlength="250" required="" type="text">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Descripción </p>
                                    <label for="message-text" class="form-control-label"></label>
                                    <textarea class="form-control" name="temas[`+xx+`][descripcion]" rows="5" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <a class="eliminar-tema" onclick="eliminar_tema(`+xx+`)"><i class="fa fa-trash fa-2 eliminar-inv" aria-hidden="true" title="Eliminar"></i></a>
        </div>
    </div>`;
    /*dentro=$('#accordion').html();*/
    $('#accordion').prepend(html);
    /*consecutivo=0;
    for(i=0;i<=xx;i++){
        if ( $('#num_tema'+i).length > 0 ) {
            // hacer algo aquí si el elemento existe
            consecutivo++;
            $('#num_tema'+i).html(consecutivo);
        }
    }*/
});
$('body').on('keyup','.titulo-tema',function(){
  id=$(this).attr('id');
  titulo=$(this).val();
  $('.'+id).html('<b>'+titulo+'</b><i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i>');
});

/**
 * Función para guardar los registros sin recargar la pagina
 */
function guardar_continuar(){
    event.preventDefault();
    /*Eduard: Modificar la acción para que al final del AJAX devuelva un JSON y no redireccione*/
    $('#accion_iniciar').val(6);
    var jqxhr = $.post("/oportunidad/acta-action", $("#formulario_iniciar").serialize())
    .done(function(e) {
        if(e.data['msj'] == "Guardado correctamente!"){
            if(e.data['id_temas'] != ""){
               $.each(e.data['id_temas'], function(k, dato) {
                   $('#id_tema'+k).val(dato);
               });
            }
            if(e.data['id_acciones'] != ""){
               $.each(e.data['id_acciones'], function(k, dato) {
                   $('#id_pendiente'+k).val(dato);
               });
            }
            $(".alert-success").fadeIn(1000);
            setTimeout(function(){ $(".alert-success").fadeOut(2000); }, 4000);
            var html_tiempo = `<div style="margin: 0px auto; text-align: center; width: 150px;"><img src="http://lh4.ggpht.com/_0eC4K-qZ7AM/TO1RSmqk-lI/AAAAAAAAMX0/-7OHYYDkfco/1.gif" /><img src="http://lh5.ggpht.com/_0eC4K-qZ7AM/TO1RS7xsylI/AAAAAAAAMX4/_uQfIddo6OA/2.gif" /><img src="http://lh3.ggpht.com/_0eC4K-qZ7AM/TO1RTOyFacI/AAAAAAAAMX8/i-DnYANb0kE/3.gif" /><img src="http://lh3.ggpht.com/_0eC4K-qZ7AM/TO1RTUc4nVI/AAAAAAAAMYA/bj0mqGH8xLM/4.gif" /><img src="http://lh3.ggpht.com/_0eC4K-qZ7AM/TO1RTuO79eI/AAAAAAAAMYE/-Y2w89jmaig/5.gif" /></div>`;
            $('#contador_tiempo').html(html_tiempo);
        }else{

        }
    })
    .fail(function(e) {
        console.error(e)
    })
    .always(function(e) {
      console.log(e)
    });
}

function eliminar_tema(id){
    eliminar_basedatos($('#id_tema'+id).val(),'tema');
    $('#tema'+id).remove();
    consecutivo=0;
    for(i=0;i<=xx;i++){
        if ( $('#num_tema'+i).length > 0 ) {
            // hacer algo aquí si el elemento existe
            consecutivo++;
            $('#num_tema'+i).html(consecutivo);
        }
    }
}

function eliminar_basedatos(id,tipo){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/oportunidad/acta-action",
        data: {
            _token: CSRF_TOKEN,
            accion: 8,
            tipo: tipo,
            id: id
        },
        cache: false,
        type: 'POST',
        success: function(e){
           console.log(e.data['msj']);
        }
    });
}

yy=1;
$('body').on('click','.crear-pendiente',function(){
    yy++;
    html=`<div class="row" id="pendiente`+yy+`">
            <div class="col-md-11">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title titulo-pendiente">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_pen`+yy+`"><b>Pendiente #<span id="num_pen`+yy+`">1</span></b> <i class="fa fa-angle-down pull-right"></i><i class="fa fa-angle-up pull-right"></i></a>
                        </h4>
                    </div>
                    <div id="collapse_pen`+yy+`" class="panel-collapse collapse">
                        <input type="hidden" id="id_pendiente`+yy+`" name="pendiente[`+yy+`][id]" value="">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Fecha ejecución</p>
                                        <label for="recipient-name" class="form-control-label"></label>
                                        <input type="hidden" name="pendiente[`+yy+`][fecha_creacion]" value="`+fecha_actual+`">
                                        <input class="form-control2 form-control datatime" step="1800" placeholder="Fecha y hora Ejecución" name="pendiente[`+yy+`][fecha_ejecucion]" data-dtp="dtp_jx0jc" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Responsable</p>
                                        <label for="message-text" class="form-control-label"></label>
                                        <select class="form-control responsable" name="pendiente[`+yy+`][responsable]">
                                            <option value="">Seleccione un responsable</option>`+html_responsable+`</select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                     <div class="form-group">
                                        <p>Descripción</p>
                                        <label for="message-text" class="form-control-label"></label>
                                        <textarea class="form-control" name="pendiente[`+yy+`][descripcion]" rows="5"></textarea>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <a class="eliminar_prop" onclick="eliminar_prop(`+yy+`)"><i class="fa fa-trash fa-2" aria-hidden="true" title="Eliminar"></i></a>
            </div>
        </div>`;
    /*dentro=$('#accordion0').html();*/
    $('#accordion0').prepend(html);
     consecutivo=0;
    for(i=0;i<=yy;i++){
        if ( $('#num_pen'+i).length > 0 ) {
            // hacer algo aquí si el elemento existe
            consecutivo++;
            $('#num_pen'+i).html(consecutivo);
        }
    }
    fechas();
});

function eliminar_prop(id){
    eliminar_basedatos($('#id_pendiente'+id).val(),'accion');
    $('#pendiente'+id).remove();
    consecutivo=0;
    for(i=0;i<=yy;i++){
        if ( $('#num_pen'+i).length > 0 ) {
            // hacer algo aquí si el elemento existe
            consecutivo++;
            $('#num_pen'+i).html(consecutivo);
        }
    }
}

function fechas(){
     $('.datatime').bootstrapMaterialDatePicker({
          time: true,
          date: true,
          clearButton: true,
          nowButton: true,
          lang: 'es',
          format : 'YYYY-MM-DD HH:mm',
          minDate : new Date(),
          cancelText : 'Cancelar',
          okText: 'Aceptar',
          nowText: 'Nuevo',
          clearText: 'Limpiar',
          weekStart : 0
      });
}

$('body').on('click','.lista-usuarios-asistencia',function(){
    id=$(this).attr('id');
    id=id.replace('asistio','');
    if( $(this).prop('checked') ) {
        $('#asistenciavalor'+id).val("Si");
        /*Obtener el tiempo */
        var tiempo = new Date();
        var hora = tiempo.getHours();
        var minuto = tiempo.getMinutes();
        var segundo = tiempo.getSeconds();
        $('#hora_llegada'+id).html(hora+':'+minuto+':'+segundo);
        $('#asistenciahorallegada'+id).val(hora+':'+minuto+':'+segundo);
    }else{
        $('#asistenciavalor'+id).val("No");
        $('#hora_llegada'+id).html('Hora Eliminada');
        $('#asistenciahorallegada'+id).val('00:00:00');
    }

});

$('body').on('click','.guardar2',function(){
    event.preventDefault();
  if(true === $("#formulario_iniciar").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a guardar la citación",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                $('#accion_iniciar').val(7);
                $('.btn_guardar').click();
            });
    }else{
        $('.collapsed').click();
        swal({
              type: 'warning',
              title: 'Advertencia',
              text: 'Falta campos por completar!. Revise todos los campos de temas'
            })
    }
});

function marcar(id,realizar){
  if(realizar == 'realizado'){
    swal({
        title: 'Esta seguro?',
        text: "Va a marcar la accion como realizada",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, guardar!'
      }).then(function () {
          swal({
              input: 'textarea',
              inputPlaceholder: 'Ingrese un comentario',
              showCancelButton: true
            }).then(function (text) {
              if (text) {
                var CSRF_TOKEN = $('input[name=_token]').val();
                var jqxhr = $.ajax({
                        url: "/oportunidad/acta-action",
                        data: {
                            _token: CSRF_TOKEN,
                            id: id,
                            comentario: text,
                            accion: 10
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal(
								'Guardado!',
								e.msj,
								'success'
							  ).then(function () {
								parent.window.location.reload(true);
							  });
                        }
                    });
              }else{
                  swal('No ingreso ningún comentario y no se guardó como realizada!');
              }
            });

      });
  }else if(realizar == 'aplazar'){
      $('#id_aplazar').val(id);
      $('.modificarfecha').modal('show');

  }else{
    swal({
        title: 'Esta seguro?',
        text: "Va a marcar la accion como cancelada",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, guardar!'
      }).then(function () {
        swal({
          input: 'textarea',
          inputPlaceholder: 'Ingrese un comentario',
          showCancelButton: true
        }).then(function (text) {
          if (text) {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                    url: "/oportunidad/acta-action",
                    data: {
                        _token: CSRF_TOKEN,
                        id: id,
                        comentario: text,
                        accion: 12
                    },
                    cache: false,
                    type: 'POST',
                    success: function(e){
                        swal(
                            'Guardado!',
                            e.msj,
                            'success'
                          ).then(function () {
                            parent.window.location.reload(true);
                          });
                    }
                });
          }else{
              swal('No ingreso ningún comentario y no se guardó como Cancelada!');
          }
        });
      });
  }
}

$('body').on('click','#btn_guardar_aplazar',function(){
    if($('#fecha_actual').val() > $('#nuevafecha').val()){
        swal({
          title: 'Alerta',
          type: 'warning',
          html: $('<div>')
            .addClass('some-class')
            .text('La fecha debe ser mayor a la fecha actual.'),
          animation: false,
          customClass: 'animated tada'
        });
       }else{
           swal({
              title: 'Esta seguro?',
              text: "Desea guardar la nueva fecha",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, Guardar',
              showLoaderOnConfirm: true,
              preConfirm: function () {
              return new Promise(function (resolve) {
                  var CSRF_TOKEN = $('input[name=_token]').val();
                  var jqxhr = $.ajax({
                        url: "/oportunidad/acta-action",
                        data: {
                            _token: CSRF_TOKEN,
                            id: $('#id_aplazar').val(),
                            nueva: $('#nuevafecha').val(),
                            comentario: $('#comentarioaplazado').val(),
                            accion: 11
                        },
                        cache: false,
                        type: 'POST',
                        success: function(e){
                            swal('¡Guardado!','Su Información ha sido modificado.','success').then(function (result) {
                                parent.window.location.reload(true);
                                resolve();
                            });
                        }
                    });
                })
              },
              allowOutsideClick: false
            })
       }
});

function eliminar_citada(id){
    swal({
      title: 'Esta seguro?',
      text: "Va a eliminar un registro",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar',
      showLoaderOnConfirm: true,
      preConfirm: function () {
        return new Promise(function (resolve) {
            var CSRF_TOKEN = $('input[name=_token]').val();
            var jqxhr = $.ajax({
                url: "/oportunidad/acta-action",
                data: {
                    _token: CSRF_TOKEN,
                    id: id,
                    accion: 4
                },
                cache: false,
                type: 'POST',
                success: function(e){
                    swal('¡Eliminado!','Se elimino AC-'+id,'success').then(function (result) {
                        location.href = "/oportunidad/acta/"+e.data['id'];
                        resolve();
                    });
                }
            });
        })
      },
      allowOutsideClick: false
    })
}

function editar_acta(id){
    if(permiso_acta_crear_editar == "Si"){
        $('#listado_actas').css('display','none');
        $('#editar_acta').css('display','block');
        $('#id_cita').val(id);
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/oportunidad/acta-action",
            data: {
                _token: CSRF_TOKEN,
                id: id,
                accion: 2
            },
            cache: false,
            type: 'POST',
            success: function(e){
                number_acta=$('#numero_acta_'+e.acta.id).html();
                $('#numero_Acta_editar').html(number_acta);
                $('#acta_editar_titulo').val(e.acta.nombre);
                $('#acta_editar_descripcion').val(e.acta.descripcion);
                $('#fecha_editar').val(e.acta.fecha);
                $('#lugar_editar').val(e.acta.lugar);
                invitados=(e.invitados);
                html='';
                html1='';
                con=0;
                invi="";
                $.each(invitados, function (k, item) {
                  if((item.tipo)=="usuario externo"){
                        html1+=`<div class="row" id="invitadoexternoeditar`+con+`">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Nombre </p>
                                        <label for="recipient-name" class="form-control-label"></label>
                                        <input type="text" class="form-control" name="invitadoadicional[`+con+`][nombre]" value="`+item.nombre+`" required="required">
                                    </div>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Correo </p>
                                        <label for="recipient-name" class="form-control-label"></label>
                                        <input type="email" class="form-control" name="invitadoadicional[`+con+`][correo]" value="`+item.email+`" required="required">
                                    </div>
                                  </div>
                                  <div class="col-md-1 eliminar-inv-adi">
                                      <a onclick="eliminar_invitado_adicional_editar(`+con+`)"><i class="fa fa-trash fa-2 eliminar-inv" aria-hidden="true" title="Eliminar"></i></a>
                                  </div>
                              </div>`;
                      con++;
                    }else{
                        invi+="%%"+item.id_usuario;
                        fotografia=item.foto;
                        nombre=item.nombre_funcionario;
                        cargo=item.cargo_funcionario;
                        html+=`<li id="invitado_editar`+item.id_usuario+`">
                                <img src="`+url_basico+`/images/file/clientes/`+fotografia+`" class="img-circle" alt="Avatar">
                                <p><a href="#"><strong>`+nombre+`</strong></a></p>
                                <span class="text-muted">`+cargo+` <i class="fa fa-times-circle quitar" onclick="eliminar_editar(`+item.id_usuario+`)" title="Eliminar"></i></span>
                            </li>`;
                    }
                });
                html+=`<li class="team-add">
                            <i class="icon ion-person"></i>
                            <a href="#" id="añadir_invitados_editar"><i class="fa fa-plus-circle"></i> Añadir </a>
                        </li>`;
                $('#listado_invitados_editar').html(html);
                $('#contenedor-invi-exter-editar').html(html1);
                $('#invitados_editar').val(invi);
                Ge=con;
            }
        });
    }else{
        swal('Advertencia','Usted no tiene permisos para editar el acta','warning');
    }
}

function eliminar_editar(id){
    $('#invitado_editar'+id).remove();
    ids=$('#invitados_editar').val();
    ids=ids.replace('%%'+id,'');
    $('#invitados_editar').val(ids);
}
$('body').on('click','#añadir_invitados_editar',function(){
    $('.añadir-invitados-editar').modal('show');
});
function eliminar_invitado_adicional_editar(id){
    $('#invitadoexternoeditar'+id).remove();
}


$('body').on('click','.crear-inv-editar',function(){
    Ge++;
    html=`<div class="row" id="invitadoexternoeditar`+Ge+`">
              <div class="col-md-6">
                <div class="form-group">
                    <p>Nombre </p>
                    <label for="recipient-name" class="form-control-label"></label>
                    <input type="text" class="form-control" name="invitadoadicional[`+Ge+`][nombre]">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                    <p>Correo </p>
                    <label for="recipient-name" class="form-control-label"></label>
                    <input type="email" class="form-control" name="invitadoadicional[`+Ge+`][correo]">
                </div>
              </div>
              <div class="col-md-1 eliminar-inv-adi">
                  <a onclick="eliminar_invitado_adicional_editar(`+Ge+`)"><i class="fa fa-trash fa-2 eliminar-inv" aria-hidden="true" title="Eliminar"></i></a>
              </div>
          </div>`;
    $('#contenedor-invi-exter-editar').prepend(html);
});

$('body').on('click','.lista-usuarios-editar',function(){
    id=$(this).attr('id');
    id=id.replace('user_editar','');
    ids=$('#invitados_editar').val();
    if( $(this).prop('checked') ) {
        if ( $("#invitado_editar"+id).length ) {
          // hacer algo aquí si el elemento existe
        }else{
            $.ajax({
                url: url_basico+"/consultarinvitado_editar/"+id,
                cache: false,
                contentType: false,
                processData: false,
                type: 'GET',
                success: function(e){
                    $('#listado_invitados_editar').prepend(e.html);
                }
            });
            $('#invitados_editar').val(ids+'%%'+id);
        }
    }else{
        $('#invitado_editar'+id).remove();
        ids=ids.replace('%%'+id,'');
        $('#invitados_editar').val(ids);
    }
});
$('body').on('click','.guardar_editar',function(){
    event.preventDefault();
	if(true === $("#formulario_cita_editar").parsley().validate()){
        swal({
              title: 'Esta seguro?',
              text: "Va a editar la citación",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, guardar!'
            }).then(function () {
                $('.btn_guardar_editar').click();
            });
    }
});
function comentarios(id,numero_acta){
    $('.comentarios').modal('show');
    $('#acta-comentario').html('AC-'+numero_acta);
    $('.contenido_comentarios').html('<div class="col-md-12" style="text-aling: center;"><img src="'+url_basico+'/images/configuracion_crm/configuracion.gif" style="background: none;"></div>');

    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/oportunidad/acta-action",
        data: {
            _token: CSRF_TOKEN,
            id: id,
            accion: 9
        },
        cache: false,
        type: 'POST',
        success: function(e){
            $('.contenido_comentarios').html(e.html);
        }
    });
}

/**
 * Funcion para ver el acta en un modal
 * @param INT id IDENTIFICACIÓN DEL
 */
function ver_acta(id){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var jqxhr = $.ajax({
        url: "/oportunidad/acta-action",
        data: {
            _token: CSRF_TOKEN,
            id: id,
            accion: 1
        },
        cache: false,
        type: 'POST',
        success: function(e){
            ver_info(e.data['ver']);
        }
    });
}

function ver_info(html){
    swal({
      title: 'Ver Acta',
      html:html,
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText:'<i class="fa fa-times"></i> Cerrar'
    });
    $('.swal2-modal').css('width','90%');
}


/**
 * Función para enviar correo electronico a los invitados con toda la información de la reunión
 * @param INT id [[Description]]
 */
function enviar_info(id){
    swal({
      title: 'Esta seguro?',
      text: "Desea enviar la información de la reunión por correo electronico a todos los invitados",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Enviar!'
    }).then(function () {
        var CSRF_TOKEN = $('input[name=_token]').val();
        var jqxhr = $.ajax({
            url: "/oportunidad/acta-action",
            data: {
                _token: CSRF_TOKEN,
                id: id,
                accion: 13
            },
            cache: false,
            type: 'POST',
            success: function(e){
                if(e.data['msj'] == 'Enviado correctamente!'){
                   swal(
                      'Enviado',
                      e.data['msj'],
                      'success'
                    );
                }else{
                    swal(
                      'Error',
                      e.data['msj'],
                      'error'
                    );
                }
            }
        });
    });
}
