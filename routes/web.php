<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Producto;
use App\Actividades;
use App\FreelancePaisSecundarios;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/informe-smart', function () {
    return view('InformeSmart.informe');
});
Route::post('informe-smart', 'OportunidadController@informe');
Route::get('/xx', function ()  {
    return view('xx.xx');
})->middleware('auth');

Route::post('initdata', 'FolderController@create');
Route::resource('enlaces', 'FolderController');
Route::get('/portafolio', function () {
    return view('portafolioessi');
});

Route::get('/megaarchivo', function () {
    return view('megaarchivo.login');
})->middleware('auth');

Route::get('megaarchivo/menu', function () {
    return view('megaarchivo.menu');
})->middleware('auth');

Route::get('/', 'ComercialController@index')->middleware('auth');

Route::get('informe', function () {
    return view('informe');
})->middleware('auth');

Route::get('actividad/{id}', function ($id) {
    $dato = Actividades::findOrFail($id);
    return \Response::json(['success'=>1, 'result' => $dato]);
})->middleware('auth');

Route::get('/login', [
    'uses' =>'AuthController@showLogin',
    'as' => 'login'
]);
Route::post('login', 'AuthController@postLogin');
Route::get('user/{id}', 'AuthController@showUser')->middleware('auth');
Route::get('logout', 'AuthController@logOut')->middleware('auth');

//pendientes
/*Route::get('backlog', function () {
    return view('pendientes/list');
});*/
Route::get('backlog', 'Actividades_detallesController@lista')->middleware('auth');
Route::get('jsonvigentes', 'PendientesController@jsonVigentes')->middleware('auth');
Route::get('jsonvencidas', 'PendientesController@jsonVencidas')->middleware('auth');
Route::get('jsonporaprobar', 'PendientesController@jsonPorAprobar')->middleware('auth');
Route::get('jsonfinalizadas', 'PendientesController@jsonFinalizadas')->middleware('auth');
Route::get('jsoncanceladas', 'PendientesController@jsonCanceladas')->middleware('auth');
Route::post('aplazarpendiente', 'PendientesController@aplazarPendiente')->middleware('auth');
Route::post('confirmarpendiente', 'PendientesController@confirmarPendiente')->middleware('auth');
Route::post('cancelarpendiente', 'PendientesController@cancelarPendiente')->middleware('auth');
Route::post('aprobarpendiente', 'PendientesController@aprobarPendiente')->middleware('auth');
Route::get('verpendiente/{id}', 'PendientesController@verPendiente')->middleware('auth');
Route::get('listactivpendientes', 'PendientesController@jsonTiposActividad')->middleware('auth');
Route::post('crearpendiente', 'PendientesController@savePendientes')->middleware('auth');
// Route::get('backlog/principal', 'ListaController@backlog')->middleware('auth');


// Cliente
Route::post('crearcliente', 'ClientesController@save')->middleware('auth');
Route::post('editarcliente', 'ClientesController@edit')->middleware('auth');
Route::get('crearcliente', 'ClientesController@index')->middleware('auth');
Route::get('editarcliente/{id}', 'ClientesController@indexedit')->middleware('auth');
Route::get('cliente/{id}', 'ClientesController@view')->middleware('auth');
Route::get('clientes', 'ClientesController@listAjax')->middleware('auth');
Route::get('listcomencliente/{id}', 'ClientesController@getComentarios')->middleware('auth');
Route::post('savecomencliente', 'ClientesController@saveComentario')->middleware('auth');
Route::post('savecomenoport', 'OportunidadController@saveComentario')->middleware('auth');
Route::get('registrarvisita', 'ClientesController@listaclientes')->middleware('auth');
Route::get('cliente/eliminar/{id}', 'ClientesController@eliminarcliente')->middleware('auth');

Route::get('clientesjson', 'ClientesController@listclient')->middleware('auth');
Route::get('clientesjsonfil/{id}', 'ClientesController@listaclientesfil')->middleware('auth');
Route::get('clientesfil/{id}', 'ClientesController@listfil')->middleware('auth');
// Fin Cliente
// Select2
Route::get('pais', 'selec2Controller@pais')->middleware('auth');
Route::get('empresa', 'selec2Controller@empresa')->middleware('auth');
Route::get('subactividad', 'selec2Controller@subactividad')->middleware('auth');
Route::get('estados', 'selec2Controller@estados')->middleware('auth');
Route::get('sede', 'selec2Controller@sede')->middleware('auth');
Route::get('ciudades', 'selec2Controller@ciudades')->middleware('auth');
Route::get('personal', 'selec2Controller@personal')->middleware('auth');
Route::get('cliente', 'selec2Controller@cliente')->middleware('auth');
Route::get('cliente2', 'selec2Controller@cliente2')->middleware('auth');
Route::get('cliente1', 'selec2Controller@cliente1')->middleware('auth');
Route::get('soportunidad', 'selec2Controller@soportunidad')->middleware('auth');
Route::get('ssoportunidad', 'selec2Controller@ssoportunidad')->middleware('auth');
Route::get('vendedores', 'selec2Controller@usuarios')->middleware('auth');
Route::get('vendedores2', 'selec2Controller@usuarios2')->middleware('auth');
Route::get('vendedores3', 'selec2Controller@usuariosplantrebajo')->middleware('auth');
Route::get('usuariosdashboardcomercial', 'selec2Controller@usuarios3')->middleware('auth');
Route::get('usuariosdashboardcomercialpermisos', 'selec2Controller@usuarioscomercialfiltro')->middleware('auth');
Route::get('otros', 'selec2Controller@otros')->middleware('auth');
Route::get('sproducto', 'selec2Controller@sproducto')->middleware('auth');
Route::get('referencia', 'selec2Controller@referencia')->middleware('auth');
// Fin Select2


// Storage
Route::post('archivo/crear', 'StorageController@save')->middleware('auth');
Route::get('subarchivos', 'StorageController@index')->middleware('auth');
Route::get('pdover', 'StorageController@view')->middleware('auth');
// Fin Storage

// Productos
Route::post('productos', 'ProductoController@save')->middleware('auth');
Route::post('producto/editar', 'ProductoController@editsave')->middleware('auth');
Route::get('productos', 'ProductoController@index')->middleware('auth');
Route::get('producto/ver/{id}', 'ProductoController@view')->middleware('auth');
Route::get('producto/crear', 'ProductoController@create')->middleware('auth');
Route::get('producto/editar/{id}', 'ProductoController@edit')->middleware('auth');
Route::post('validarselectproducto', 'ProductoController@validarMultiple')->middleware('auth');
Route::get('eliminarreferencia/{id}', 'ProductoController@eliminarReferencia')->middleware('auth');
// Fin Productos

// Actividades
Route::get('calendario', 'ActividadeController@calendario')->middleware('auth');
Route::get('bitacora/crear', 'ActividadeController@index')->middleware('auth');
Route::get('bitacora/ver/{id}', 'ActividadeController@view')->middleware('auth');
Route::get('bitacora/editar/{id}', 'ActividadeController@editar')->middleware('auth');
Route::get('bitacora/lista', 'ActividadeController@listAct')->middleware('auth');
Route::get('bitacora/listar', 'ActividadeController@listar')->middleware('auth');
Route::post('bitacora', 'ActividadeController@save')->middleware('auth');
Route::post('bitacora/editar', 'ActividadeController@editsave')->middleware('auth');
Route::get('prueba', 'PruebaController@gastos')->middleware('auth');
Route::get('gastos', 'ActividadeController@gastos')->middleware('auth');
Route::post('gastos-comparativo-accion', 'PresupuestoController@comparativo_accion')->middleware('auth');
Route::post('filtrargastosprueba', 'PruebaController@filtrargastos')->middleware('auth');
Route::post('filtrargastos', 'ActividadeController@filtrargastos')->middleware('auth');
Route::get('filtrogastos/{id}', 'ListaController@filtrosgraficas')->middleware('auth');
Route::get('listadogastos', 'ActividadeController@listadogastos')->middleware('auth');
Route::post('savecomentactividad','ActividadeController@saveComentario')->middleware('auth');
Route::get('listcomentactividad/{id}', 'ActividadeController@getComentarios')->middleware('auth');


Route::get('bitacora/calendario', 'ActividadeController@indexCalendar')->middleware('auth');
Route::post('bitacora/calendario', 'ActividadeController@action')->middleware('auth');
Route::get('xx3/calendario', 'ActividadeController@indexCalendarPrueba')->middleware('auth');
Route::get('bitacora/calendario2', 'ActividadeController@indexCalendar2')->middleware('auth');
Route::get('listadoactividades/{id}', 'ActividadeController@listActividades')->middleware('auth');
Route::get('actividad/eliminar/{id}', 'ActividadeController@eliminar')->middleware('auth');
Route::post('actividad/aprobar', 'ActividadeController@gesAprobar')->middleware('auth');


// Fin Actividades

// Oportunidades
Route::get('oportunidades', 'OportunidadController@listAjax')->middleware('auth');
Route::get('oportunidad/{id}', 'OportunidadController@view')->middleware('auth');
Route::get('oportunidad/acta/{id}', 'OportunidadController@acta')->middleware('auth');
Route::post('oportunidad/acta-action', 'OportunidadController@action_acta')->middleware('auth');
Route::post('oportunidad-action','OportunidadController@action_two')->middleware('auth');
Route::get('oportunidad/edit/{id}', 'OportunidadController@edit')->middleware('auth');
Route::post('registraroportunidad2', 'OportunidadController@save')->middleware('auth');
Route::post('editaroportunidad', 'OportunidadController@editsave')->middleware('auth');
Route::post('cambiarmoneda', 'OportunidadController@moneda')->middleware('auth');
Route::get('registraroportunidad', 'OportunidadController@index')->middleware('auth');
Route::get('calendario-cierre-menu', 'CalendariocierreController@index')->middleware('auth');
Route::get('calendario-cierre', 'CalendariocierreController@calendario_cierre')->middleware('auth');
Route::get('calendario-cierre-cerrados', 'CalendariocierreController@calendario_cierre_cerrado')->middleware('auth');
Route::post('calendario-action', 'CalendariocierreController@action')->middleware('auth');
Route::post('calendario-action-cerrados', 'CalendariocierreController@calendario_action_cerrados')->middleware('auth');
Route::get('listcomenoport/{id}', 'OportunidadController@getComentarios')->middleware('auth');
Route::get('acciones/{id}', 'OportunidadController@acciones')->middleware('auth');
Route::get('ver-oportunidad-flujo/{id}', 'OportunidadController@flujocaja')->middleware('auth');
Route::get('ver-oportunidadvendida-flujo/{id}', 'OportunidadController@flujocajavendida')->middleware('auth');
Route::post('ver-oportunidad-flujo-action', 'OportunidadController@action')->middleware('auth');
Route::get('acciones2/{id}', 'OportunidadController@acciones2')->middleware('auth');
Route::get('avanzaretapa', 'OportunidadController@avanzar')->middleware('auth');
Route::post('uploadfiles','OportunidadController@savefiles')->middleware('auth');
Route::post('savecomments','OportunidadController@savecomments')->middleware('auth');
Route::get('vercomentarios', 'OportunidadController@comentarios')->middleware('auth');
Route::post('ciclosave', 'OportunidadController@saveciclo')->middleware('auth');
Route::post('ventasave', 'OportunidadController@saveventa')->middleware('auth');
Route::get('oportunidadperdida/{id}', 'OportunidadController@perder')->middleware('auth');
Route::post('guardaruserotro', 'OportunidadController@saveotro')->middleware('auth');

Route::get('veroportunidadperdida/{id}', 'OportunidadController@viewperdida')->middleware('auth');
Route::get('accionesoportunidadperdida/{id}', 'OportunidadController@accionesperdidas')->middleware('auth');
Route::get('verlecciones/{id}', 'OportunidadController@leccionesaprendidas')->middleware('auth');
Route::get('calendario/oportunidades', 'OportunidadController@calendarioList')->middleware('auth');
Route::get('listOportunidades', 'OportunidadController@listOportunidades')->middleware('auth');
Route::get('gestionesoportunidad/{id}', 'OportunidadController@GestionesOp')->middleware('auth');
Route::post('gestionesoportunidad-action', 'OportunidadController@actiongestiones')->middleware('auth');
Route::get('validargestionesop/{id}', 'OportunidadController@ValidarOpGestiones')->middleware('auth');
Route::get('traerdatosop/{id}/{fecha}', 'OportunidadController@traerDatosOp')->middleware('auth');
Route::get('ver-oportunidadperdida-flujo/{id}', 'OportunidadController@flujocajaperdida')->middleware('auth');

// Fin Oportunidades

// Listados Generales
Route::get('consultarcomentarios/{id}', 'ListaController@comentarios');
Route::get('listadodiario/{id}', 'ListaController@veractividades');
Route::get('dashboardmega', 'ListaController@dashboardmega')->middleware('auth');
Route::get('dashboardmegafiltro/{fi}/{ff}/{t}', 'ListaController@dashboardmegafiltro')->middleware('auth');
Route::get('crearusuario', 'ListaController@usuarios');
// Fin Listados Generales

// Backlog

Route::get('backlog/calendario', 'ListaController@calendario')->middleware('auth');
Route::post('aprobarrechazar ', 'Actividades_detallesController@indexaprobar')->middleware('auth');
Route::post('consultarcomentarios', 'ListaController@comentarios');
//
// Mega Archivo
Route::post('megaarchivo/tipoingreso','MegaarchivoController@tipoingreso')->middleware('auth');
Route::post('tiemponuevo','MegaarchivoController@tiemponuevo')->middleware('auth');
Route::get('comercial', 'ComercialController@index')->middleware('auth');
/*DashBoard producción*/
Route::get('dashboard-produccion-filtro', 'DashboardgeneralnewController@BoardProduccionFiltro')->middleware('auth');
Route::get('dashboard-produccion', 'DashboardgeneralnewController@BoardProduccion')->middleware('auth');
Route::post('dashboard-produccion-action', 'DashboardgeneralnewController@action')->middleware('auth');
Route::get('dashboard', 'ComercialController@init')->middleware('auth');
Route::post('dashboard-action', 'ComercialController@action')->middleware('auth');
Route::get('comparacion-anos', 'ComparacionController@index')->middleware('auth');
Route::post('comparacion-action', 'ComparacionController@action_new')->middleware('auth');
Route::post('rutausada','MegaarchivoController@rutausada')->middleware('auth');
Route::post('rutausadaeditar','MegaarchivoController@rutausadaeditar')->middleware('auth');
Route::post('archivos_das_menu','MegaarchivoController@archivos_das_menu')->middleware('auth');
Route::post('archivousado','MegaarchivoController@archivousado')->middleware('auth');
Route::post('dashboardmega/funcionario','MegaarchivoController@consultar_funcionario')->middleware('auth');
Route::post('dashboardmega/funcionario1','MegaarchivoController@consultar_funcionario1')->middleware('auth');
Route::get('megaarchivo/menu', 'MegaarchivoController@menu')->middleware('auth');
Route::post('menuguardar', 'MegaarchivoController@menuguardar');
Route::post('megaarchivo/documentos', 'MegaarchivoController@documentos');
Route::post('loginmegaarchivo', 'MegaarchivoController@megaarchivo')->middleware('auth');
Route::post('dashboardmegarchivo', 'MegaarchivoController@dashboardmegarchivo')->middleware('auth');
Route::post('megaarchivo/configuracion', 'MegaarchivoController@configuracionmegarchivo')->middleware('auth');
Route::get('carruselimagenes/{nombre}', 'MegaarchivoController@carruselimagenes')->middleware('auth');
Route::get('visualizador_imagenes/{ru}', 'MegaarchivoController@visualizador_imagenes')->middleware('auth');
// Fin Mega Archivo

// Archivos Importantes
Route::get('archivosimportantes', 'ArchiImportantController@listAjax')->middleware('auth');
Route::get('listcatarchivos', 'ArchiImportantController@getCategoria')->middleware('auth');
Route::get('listarchivos/{id}', 'ArchiImportantController@getArchivo')->middleware('auth');
Route::post('savearchivosimportantes', 'ArchiImportantController@save')->middleware('auth');
Route::post('savefilecomen', 'ArchiImportantController@saveComentario')->middleware('auth');
Route::get('listcomenarchivos/{id}', 'ArchiImportantController@getComentarios')->middleware('auth');
Route::post('savefileimportantes', 'ArchiImportantController@saveArchivo')->middleware('auth');

Route::get('archivosimportantes/eliminar/{id}', 'ArchiImportantController@eliminar')->middleware('auth');
// Fin Archivos Importantes

// Archivos No Importantes
Route::get('listcatarchivos90/{id}', 'ArchiImportant90Controller@getCategoria')->middleware('auth');
Route::get('listarchivos90/{id}/{idop}', 'ArchiImportant90Controller@getArchivo')->middleware('auth');
Route::post('savearchivosimportantes90', 'ArchiImportant90Controller@save')->middleware('auth');
Route::post('savefilecomen90', 'ArchiImportant90Controller@saveComentario')->middleware('auth');
Route::get('listcomenarchivos90/{id}', 'ArchiImportant90Controller@getComentarios')->middleware('auth');
Route::post('savefileimportantes90', 'ArchiImportant90Controller@saveArchivo')->middleware('auth');
// Fin Archivos No Importantes

// Empresa
Route::post('crearempresa', 'EmpresaController@save')->middleware('auth');
Route::post('agregarsede', 'EmpresaController@savesede')->middleware('auth');
Route::post('editarsede', 'EmpresaController@editsede')->middleware('auth');
Route::get('crearempresa', 'EmpresaController@index')->middleware('auth');
Route::get('agregarsede/{id}', 'EmpresaController@indexsede')->middleware('auth');
Route::get('editarsede/{id}', 'EmpresaController@indexeditsede')->middleware('auth');
Route::get('empresa/eliminar/{id}', 'EmpresaController@eliminarEmpresa')->middleware('auth');
Route::get('sede/eliminar/{id}', 'EmpresaController@eliminarSede')->middleware('auth');
Route::get('empresas', 'EmpresaController@listmap')->middleware('auth');
Route::get('empresa/{id}', 'EmpresaController@view')->middleware('auth');
Route::get('empresasede/{id}', 'EmpresaController@viewsede')->middleware('auth');
Route::get('editarempresa/{id}', 'EmpresaController@indexedit')->middleware('auth');
Route::post('editarempresa', 'EmpresaController@editsave')->middleware('auth');

Route::get('empresasjson', 'EmpresaController@listmapjson')->middleware('auth');
Route::get('sedesjson', 'EmpresaController@listsedesmapjson')->middleware('auth');
// Fin Empresa

// Comentarios empresa y sede
Route::get('listcomenempresa/{id}', 'EmpresaComentariosController@getComentarios')->middleware('auth');
Route::post('savecomenempresa', 'EmpresaComentariosController@saveComentario')->middleware('auth');
Route::get('listcomensede/{id}', 'EmpresaComentariosController@getComentariosSede')->middleware('auth');
Route::post('savecomensede', 'EmpresaComentariosController@saveComentarioSede')->middleware('auth');
// Fin comentarios empresa y sede

// Maquinas Vendidas
Route::get('crearmaquinavendida', 'MaquinasVendidasController@viewcreate')->middleware('auth');
Route::post('crearmaquinavendida', 'MaquinasVendidasController@save')->middleware('auth');
Route::get('maquinavendida/{id}', 'MaquinasVendidasController@viewmaquina')->middleware('auth');
Route::get('listmaquinasvendidas', 'MaquinasVendidasController@listAjax')->middleware('auth');
Route::get('listmaquinasvendidas-mapa', 'MaquinasVendidasController@lista')->middleware('auth');
Route::post('listmaquinasvendidas-mapa-action', 'MaquinasVendidasController@action')->middleware('auth');
Route::get('listmaquinasvendidasJSON', 'MaquinasVendidasController@listaJSON')->middleware('auth');
Route::get('maquinasvendidas-calendario', 'MaquinasVendidasCalendarioController@index')->middleware('auth');
Route::post('maquinasvendidas-calendario-action', 'MaquinasVendidasCalendarioController@action')->middleware('auth');

Route::post('maquinavendida/editar', 'MaquinasVendidasController@editsave')->middleware('auth');
Route::get('maquinavendida/editar/{id}', 'MaquinasVendidasController@edit')->middleware('auth');
Route::get('maquinavendida/eliminar/{id}', 'MaquinasVendidasController@eliminar')->middleware('auth');
// Fin Maquinas Vendidas

// Maquinas Competencia
Route::get('crearmaquinacompetencia', 'MaquinasCompetenciaController@viewcreate')->middleware('auth');
Route::post('crearmaquinacompetencia', 'MaquinasCompetenciaController@save')->middleware('auth');
Route::get('maquinacompetencia/{id}', 'MaquinasCompetenciaController@viewmaquina')->middleware('auth');
Route::get('listmaquinascompetencia', 'MaquinasCompetenciacontroller@listAjax')->middleware('auth');
// Fin Maquinas Competencia

//CODIGO DESDE JUNIO 1
Route::post('finalizaractividad', 'Actividades_detallesController@indexactividades')->middleware('auth');
Route::post('aplazaractividad', 'Actividades_detallesController@indexaplazar')->middleware('auth');
Route::post('cancelaractividad', 'Actividades_detallesController@indexcancelar')->middleware('auth');

// Plan de trabajo
Route::get('plantrabajo', 'Plan_trabajoController@home')->middleware('auth');
Route::post('plantrabajosave', 'Plan_trabajoController@saveplan')->middleware('auth');
Route::post('plantrabajoedit', 'Plan_trabajoController@editplan')->middleware('auth');
Route::post('finalizarplan', 'Plan_trabajoController@finalizarplan')->middleware('auth');
Route::post('editarplantrabajo', 'Plan_trabajoController@editar')->middleware('auth');
Route::get('consultarplanestrabajo/{id}', 'Plan_trabajoController@listaractividades')->middleware('auth');
Route::get('listaplanesgenerales/{id}', 'Plan_trabajoController@index_listar')->middleware('auth');
Route::get('listaplangenerales/{id}', 'Plan_trabajoController@indexlistar')->middleware('auth');
Route::get('listaplanes', 'Plan_trabajoController@home2')->middleware('auth');
Route::get('listarplanestrabajoindividual/{id}', 'Plan_trabajoController@listarplanesindividual')->middleware('auth');
Route::get('buscarplantrabajo/{id}', 'Plan_trabajoController@buscarDetalleActividad')->middleware('auth');

Route::get('plantrabajo/calendario', 'Plan_trabajoController@index')->middleware('auth');
Route::post('plan-trabajo-action', 'Plan_trabajoController@action')->middleware('auth');
Route::get('xx2/calendario', 'Plan_trabajoController@pruebaindex')->middleware('auth');
Route::get('plantrabajo/grafica', function () { return view('plantrabajo.grafica'); })->middleware('auth');
Route::get('listadoplandetrabajo/{id}', 'Plan_trabajoController@listPlanesTrabajo')->middleware('auth');
Route::get('ajaxplantrabajo/{id}', 'Plan_trabajoController@AjaxPlanTrabajo')->middleware('auth');
Route::post('plantrabajo/aprobar', 'Plan_trabajoController@gesAprobar')->middleware('auth');
Route::get('plantrabajo/eliminar/{id}', 'Plan_trabajoController@eliminar')->middleware('auth');
// Fin Plan de trabajo

Route::post('ajustegastos', 'Ajuste_gastosController@ajuste_save')->middleware('auth');
Route::get('ajustes', 'Ajuste_gastosController@ajuste_lista')->middleware('auth');
Route::post('filtrarajuste', 'Ajuste_gastosController@filtrarajuste')->middleware('auth');
Route::post('ajusteguardar', 'Ajuste_gastosController@ajusteguardar')->middleware('auth');
Route::get('presupuesto', 'PresupuestoController@index')->middleware('auth');
/*Route::get('presupuestoanterior', 'PresupuestoController@indexanterior')->middleware('auth');*/
Route::get('presupuestoanterior', 'PresupuestoController@mesanterior')->middleware('auth');
Route::get('presupuestoanual', 'PresupuestoController@indexanual')->middleware('auth');
Route::get('presupuestoanualsiguiente', 'PresupuestoController@indexanualsiguiente')->middleware('auth');
Route::post('presupuestosave', 'PresupuestoController@savepresupuesto')->middleware('auth');
Route::post('presupuestoanualsave', 'PresupuestoController@savepresupuestoanual')->middleware('auth');
Route::post('filtrarpresupuesto', 'PresupuestoController@filtrarpresupuesto')->middleware('auth');
Route::post('filtrarpresupuestoanual', 'PresupuestoController@filtrarpresupuestoanual')->middleware('auth');

// Agenda Cliente
Route::get('visitasclientes', 'Agenda_clienteController@home')->middleware('auth');
Route::get('agendarvisita', 'Agenda_clienteController@agendar')->middleware('auth');
Route::post('visitasave', 'Agenda_clienteController@save')->middleware('auth');
Route::get('agendaclientes', 'Agenda_clienteController@index')->middleware('auth');
Route::post('responsablessave', 'Agenda_clienteController@saveresponsable')->middleware('auth');
Route::post('archivosagendaclientes', 'Agenda_clienteController@uploadfiles')->middleware('auth');
Route::get('agendacliente/{id}', 'Agenda_clienteController@index')->middleware('auth');
Route::get('consultaractividadesagenda/{id}', 'Agenda_clienteController@buscar_actividad')->middleware('auth');
Route::get('buscarclientes/{id}', 'Agenda_clienteController@buscar_cliente')->middleware('auth');
Route::get('asignarresponsable/{id}', 'Agenda_clienteController@asignarindex')->middleware('auth');
Route::get('enviarcorreo/{id}', 'Agenda_clienteController@sendMail')->middleware('auth');
Route::get('mostrarcorreo/{id}', 'Agenda_clienteController@viewMail')->middleware('auth');
Route::post('sendmailsave', 'Agenda_clienteController@sendMailyes')->middleware('auth');

Route::get('ajaxagendavisita/{id}', 'Agenda_clienteController@AjaxAgendaVisita')->middleware('auth');
// Fin Agenda Cliente

Route::post('eliminarcarpeta', 'FolderController@eliminarcarpeta')->middleware('auth');
Route::post('eliminarDir', 'FolderController@eliminarDir')->middleware('auth');
Route::post('reemplazararchivomega', 'FolderController@reemplazararchivomega')->middleware('auth');
Route::post('creararchivomega', 'FolderController@creararchivomega')->middleware('auth');
Route::post('eliminar', 'FolderController@eliminarimega')->middleware('auth');
Route::post('editar', 'FolderController@editar_nombre')->middleware('auth');
Route::post('crearmega', 'FolderController@crearmega')->middleware('auth');


Route::post('funcionariosave','FuncionarioController@save')->middleware('auth');
Route::get('funcionarios', 'FuncionarioController@index')->middleware('auth');
Route::get('funcionario/{id}', 'FuncionarioController@buscar_funcionario')->middleware('auth');
Route::get('editarfuncionario/{id}', 'FuncionarioController@editarfuncionario')->middleware('auth');
Route::post('editarfuncionario-action', 'FuncionarioController@action')->middleware('auth');

Route::get('ventas', 'VentaController@index')->middleware('auth');
Route::get('ventas-historico/{flag}', 'VentaController@index')->middleware('auth');
Route::get('venta1/{id}', 'VentaController@findsale')->middleware('auth');
Route::post('upload_sale', 'VentaController@upload_sale')->middleware('auth');
Route::get('ver_detalles_archivo', 'VentaController@file')->middleware('auth');
Route::post('guardar_comentario_archivo', 'VentaController@savecomments')->middleware('auth');
Route::get('guardar_visita/{id}', 'VentaController@visitors')->middleware('auth');
Route::get('apu/{id}', 'VentaController@apu')->middleware('auth');
Route::get('liquidador/{id}', 'VentaController@liquidador')->middleware('auth');
Route::post('saveliquidacion', 'VentaController@guardarliquidacion')->middleware('auth');


Route::post('saveapu', 'VentaController@saveapu')->middleware('auth');
Route::get('apulist/{id}', 'VentaController@listapu')->middleware('auth');
Route::get('ventaciclos/{id}', 'VentaController@viewciclosventa')->middleware('auth');
Route::get('venta/{id}', 'VentaController@viewsale')->middleware('auth');
Route::get('ventafileimportant/{id}', 'VentaController@viewimportanfile')->middleware('auth');
Route::post('savecomenapu', 'VentaController@saveComentario')->middleware('auth');
Route::get('listcomenapu/{id}', 'VentaController@getComentarios')->middleware('auth');
Route::get('venta/gestionesoportunidad/{id}', 'VentaController@gestion')->middleware('auth');
//Route::post('saveapu', 'ApuController@save')->middleware('auth');


Route::post('saveperdida', 'Oportunidades_perdidasController@save')->middleware('auth');
Route::get('oportunidadesperdidas', 'Oportunidades_perdidasController@listAjax')->middleware('auth');
Route::get('oportunidadesperdidas-historico/{flag}', 'Oportunidades_perdidasController@listAjax')->middleware('auth');
Route::get('perdida/{id}', 'Oportunidades_perdidasController@find')->middleware('auth');
Route::get('perdidas/gestionesoportunidad/{id}', 'Oportunidades_perdidasController@gestion')->middleware('auth');

/// CONFIGURACION ////
Route::post('paises_save', 'ListaController@savepaises')->middleware('auth');
Route::get('paises', 'ListaController@paises')->middleware('auth');
Route::get('ciclos', 'ListaController@ciclos')->middleware('auth');
Route::post('ciclo_save', 'ListaController@ciclosave')->middleware('auth');
Route::get('ciclo/{id}', 'ListaController@findciclo')->middleware('auth');
Route::post('ciclo_editar', 'ListaController@cicloedit')->middleware('auth');
Route::post('configoportunidades', 'ConfiguracionController@save')->middleware('auth');

//Configuracion General //
Route::get('configuracion', 'ConfiguraciongeneralController@configuracion_general')->middleware('auth');
Route::post('configuracion-accion', 'ConfiguraciongeneralController@accion')->middleware('auth');
Route::post('save/dashboard', 'ConfiguraciongeneralController@save_conf_dashboard')->middleware('auth');
Route::post('tipoactividades', 'ConfiguraciongeneralController@save_conf_tipoactividad')->middleware('auth');
Route::get('eliminartipoactividades/{id}', 'ConfiguraciongeneralController@eliminartipoactividades')->middleware('auth');
Route::get('nuevotipoactividades/{numero}', 'ConfiguraciongeneralController@nuevotipoactividades')->middleware('auth');
Route::post('save/ciclosventas', 'ConfiguraciongeneralController@ciclosventas')->middleware('auth');
Route::post('save/ciclosventas2', 'ConfiguraciongeneralController@ciclosventas2')->middleware('auth');
Route::get('nuevocicloventa', 'ConfiguraciongeneralController@nuevocicloventa')->middleware('auth');
Route::get('eliminarciclo/{id}', 'ConfiguraciongeneralController@eliminarciclo')->middleware('auth');
Route::post('save/horashombre', 'ConfiguraciongeneralController@horashombre')->middleware('auth');
Route::post('save/usuarios', 'ConfiguraciongeneralController@usuarios')->middleware('auth');
Route::get('nuevocargo', 'ConfiguraciongeneralController@nuevocargo')->middleware('auth');
Route::get('eliminarcargo/{id}', 'ConfiguraciongeneralController@eliminarcargo')->middleware('auth');
Route::get('nuevaemergencia', 'ConfiguraciongeneralController@nuevaemergencia')->middleware('auth');
Route::get('eliminaremergencia/{id}', 'ConfiguraciongeneralController@eliminaremergencia')->middleware('auth');
Route::post('save/pais', 'ConfiguraciongeneralController@pais')->middleware('auth');
Route::get('eliminarbandera/{id}', 'ConfiguraciongeneralController@eliminarbandera')->middleware('auth');
Route::get('cambiarnumeroorden', 'ConfiguraciongeneralController@cambiarnumeroorden')->middleware('auth');
Route::get('consultar_menu', 'ConfiguraciongeneralController@consultar_menu')->middleware('auth');
Route::get('consultar_subitem_menu/{id}', 'ConfiguraciongeneralController@consultar_subitem_menu')->middleware('auth');
Route::post('guardarsubitem', 'ConfiguraciongeneralController@guardarsubitem')->middleware('auth');
Route::get('eliminarsubitem/{id}', 'ConfiguraciongeneralController@eliminarsubitem')->middleware('auth');
Route::get('itemsprincipales/{id}', 'ConfiguraciongeneralController@itemsprincipales')->middleware('auth');
Route::post('editarsubitem', 'ConfiguraciongeneralController@editarsubitem')->middleware('auth');
Route::get('consultariconos/{id}', 'ConfiguraciongeneralController@consultariconos')->middleware('auth');
Route::post('editaritem', 'ConfiguraciongeneralController@editaritem')->middleware('auth');
Route::get('eliminar_itemmenu/{id}', 'ConfiguraciongeneralController@eliminar_itemmenu')->middleware('auth');
Route::get('nuevoitemmenu', 'ConfiguraciongeneralController@nuevoitemmenu')->middleware('auth');
Route::post('guardaritem', 'ConfiguraciongeneralController@guardaritem')->middleware('auth');
Route::get('bajarposicionitemmenu/{id}', 'ConfiguraciongeneralController@bajarposicionitemmenu')->middleware('auth');
Route::get('subirposicionitemmenu/{id}', 'ConfiguraciongeneralController@subirposicionitemmenu')->middleware('auth');
Route::get('crearpermisoitem/{id}', 'ConfiguraciongeneralController@crearpermisoitem')->middleware('auth');
Route::post('guardarpermisoitemmenu', 'ConfiguraciongeneralController@guardarpermisoitemmenu')->middleware('auth');
//fin configuracion General //

Route::get('graficasventas', 'ComercialController@graficasventas')->middleware('auth');
Route::get('graficasventas1', 'ComercialController@graficasventas1')->middleware('auth');
Route::get('graficasventas2', 'ComercialController@graficasventas2')->middleware('auth');
Route::get('graficasproyectadas', 'ComercialController@graficasproyectadas')->middleware('auth');
Route::get('graficasproyectadas1', 'ComercialController@graficasproyectadas1')->middleware('auth');
Route::get('graficasproyectadas2', 'ComercialController@graficasproyectadas2')->middleware('auth');

//crear usuario
Route::get('consultarpais/{id}', 'ConfiguraciongeneralController@consultarpais')->middleware('auth');
Route::get('ayudatipoactividad/{id}', 'ConfiguraciongeneralController@ayudatipoactividad')->middleware('auth');
//fin crear usuario

//Consulta DashboardPrincipal
Route::post('OportunidadesIdentificadas', 'Consultas_tablero_principalController@OportunidadesIdentificadas')->middleware('auth');

//fin Consulta DashboardPrincipal

//Consulta de dashboard CRM
Route::get('dashboardconsultacumplimiento', 'ComercialController@dashboardconsultacumplimiento')->middleware('auth');
Route::get('llenado_bitacora', 'ComercialController@llenado_bitacora')->middleware('auth');
Route::get('llenado_semanal', 'ComercialController@llenado_semanal')->middleware('auth');
Route::get('llenado_presupuesto', 'ComercialController@llenado_presupuesto')->middleware('auth');
//Fin Consulta de dashboard CRM

//Actas Reunion
Route::get('actasreunion/{id}', 'ActasreunionController@actasreunion')->middleware('auth');
Route::get('consultarinvitado/{id}', 'ActasreunionController@consultarinvitado')->middleware('auth');
Route::post('guardarcita', 'ActasreunionController@guardarcita')->middleware('auth');
Route::post('asistencia', 'ActasreunionController@asistencia')->middleware('auth');
Route::post('iniciarreunion', 'ActasreunionController@iniciarreunion')->middleware('auth');
Route::post('marcarrealizada', 'ActasreunionController@marcarrealizada')->middleware('auth');
Route::post('aplazar', 'ActasreunionController@aplazar')->middleware('auth');
Route::post('marcarcancelada', 'ActasreunionController@marcarcancelada')->middleware('auth');
Route::get('eliminarcitada/{id}', 'ActasreunionController@eliminarcitada')->middleware('auth');
Route::get('datos_acta/{id}', 'ActasreunionController@datos_acta')->middleware('auth');
Route::get('consultarinvitado_editar/{id}', 'ActasreunionController@consultarinvitado_editar')->middleware('auth');
Route::post('editarcita', 'ActasreunionController@editarcita')->middleware('auth');
Route::get('consultarcomentario/{id}', 'ActasreunionController@consultarcomentario')->middleware('auth');
Route::post('actasreunion-accion', 'ActasreunionController@action')->middleware('auth');
Route::post('actasreunion-savecontinuar', 'ActasreunionController@savecontinuar')->middleware('auth');

//Reporte Excel
Route::get('reporte', 'ReporteexcelController@reporte')->middleware('auth');

//Ferias
Route::post('ferias-acciones', 'FeriasmenuController@accion')->middleware('auth');
Route::get('crearferias', 'FeriasController@crear')->middleware('auth');
Route::get('editarferias/{id}', 'FeriasController@editar')->middleware('auth');
Route::post('guardarferia', 'FeriasController@guardarferia')->middleware('auth');
Route::post('editarferia', 'FeriasController@editarferia')->middleware('auth');
Route::get('cargargaleria/{id}', 'FeriasController@cargargaleria')->middleware('auth');
Route::post('cargarmultimedia', 'ArchivoController@cargarmultimedia');
Route::post('cargarmultimediacompetenciaferia', 'ArchivoController@cargarmultimediacompetenciaferia');
Route::post('renombrararchivo', 'FeriasController@renamefile');
Route::post('eliminararchivo', 'FeriasController@deletefile');
Route::get('activar_posible/{id}', 'FeriasController@activar_posible')->middleware('auth');
Route::get('buscarmaquina/{id}', 'FeriasController@buscarmaquina')->middleware('auth');
Route::get('buscarreferencia/{id}', 'FeriasController@buscarreferencia')->middleware('auth');
Route::get('imagenmaquina/{id}', 'FeriasController@imagenmaquina')->middleware('auth');
Route::post('guardarexponente', 'FeriasController@guardarexponente');
Route::post('cargarubicacion', 'ArchivoController@cargarubicacion');
Route::get('presupuesto/{id}', 'PresupuestoferiaController@crear')->middleware('auth');
Route::get('compararpresupuesto/{id}', 'PresupuestoferiaController@compararpresupuesto')->middleware('auth');
Route::post('guardarajuste', 'PresupuestoferiaController@guardarajuste');
Route::post('guardarferiapresupuesto', 'PresupuestoferiaController@guardarferiapresupuesto');
Route::get('presupuestoadministrable', 'PresupuestoferiaController@presupuestoadministrable')->middleware('auth');
Route::post('guardarferiapresupuestoadmin', 'PresupuestoferiaController@guardarferiapresupuestoadmin');
Route::get('eliminaritempresupuesto/{id}', 'PresupuestoferiaController@eliminaritempresupuesto')->middleware('auth');
Route::get('crearitempresupuesto', 'PresupuestoferiaController@crearitempresupuesto')->middleware('auth');
Route::get('feria/conferencia/{id}', 'ConferenciaController@conferencia')->middleware('auth');
Route::post('guardarconferencista', 'ConferenciaController@guardarconferencista')->middleware('auth');
Route::post('eliminarconferencista', 'ConferenciaController@eliminarconferencista')->middleware('auth');
Route::post('eliminartema', 'ConferenciaController@eliminartema')->middleware('auth');
Route::post('guardartemas', 'ConferenciaController@guardartemas')->middleware('auth');
Route::post('guardardatosconferencia', 'ConferenciaController@guardardatosconferencia')->middleware('auth');
Route::post('guardarduracionconferencia', 'ConferenciaController@guardarduracionconferencia')->middleware('auth');
Route::get('crearconferencia/{id}', 'ConferenciaController@crearconferencia')->middleware('auth');
Route::get('presupuestoconferencia/{id}', 'ConferenciaController@presupuestoconferencia')->middleware('auth');
Route::post('guardarferiaconferenciapresupuestoadmin', 'ConferenciaController@guardarferiaconferenciapresupuestoadmin')->middleware('auth');
Route::get('eliminaritempresupuestoconferencia/{id}', 'ConferenciaController@eliminaritempresupuestoconferencia')->middleware('auth');
Route::get('crearitempresupuestoconferencia/{id}', 'ConferenciaController@crearitempresupuestoconferencia')->middleware('auth');
Route::get('presupuestoconferenciacomparar/{id}', 'ConferenciaController@presupuestoconferenciacomparar')->middleware('auth');
Route::post('guardarajusteconferencia', 'ConferenciaController@guardarajusteconferencia')->middleware('auth');
Route::get('presupuestoconferenciacomentarios/{id}', 'ConferenciaController@presupuestoconferenciacomentarios')->middleware('auth');
Route::post('listarcomentariosconferencia', 'ConferenciaController@listarcomentariosconferencia')->middleware('auth');
Route::post('comentarconferencia', 'ConferenciaController@comentarconferencia')->middleware('auth');
Route::post('listadoconferenciasferias', 'ConferenciaController@listadoconferenciasferias')->middleware('auth');
Route::post('crearconferenciasferias', 'ConferenciaController@crearconferenciasferias')->middleware('auth');
Route::get('consultarubicacionconferencia/{id}', 'ConferenciaController@consultarubicacionconferencia')->middleware('auth');
Route::post('guardarubicacionconferencia', 'ConferenciaController@guardarubicacionconferencia')->middleware('auth');
Route::get('eliminarconferencia/{id}', 'ConferenciaController@eliminarconferencia')->middleware('auth');
Route::get('proveedores', 'ProveedoresController@index')->middleware('auth');
Route::get('crearproveedores', 'ProveedoresController@crearproveedores')->middleware('auth');
Route::get('consultarpais/{id}', 'ProveedoresController@consultarpais')->middleware('auth');
Route::get('consultardepartamento/{id}', 'ProveedoresController@consultardepartamento')->middleware('auth');
Route::get('consultarciudad/{id}', 'ProveedoresController@consultarciudad')->middleware('auth');
Route::post('guardarproveedor', 'ProveedoresController@guardarproveedor')->middleware('auth');
Route::get('editarproveedor/{id}', 'ProveedoresController@editarproveedores')->middleware('auth');
Route::post('editarproveedor', 'ProveedoresController@editarproveedor')->middleware('auth');
Route::get('eliminarproveedor/{id}', 'ProveedoresController@eliminarproveedor')->middleware('auth');
Route::get('verproveedor/{id}', 'ProveedoresController@verproveedor')->middleware('auth');

Route::get('jsonviewbitacora/{id}', 'FeriasController@jsonViewBitacora')->middleware('auth');
Route::get('viewferia/{id}', 'FeriasController@jsonFeria')->middleware('auth');
Route::get('calendario/feria', 'FeriasController@calendarFeria')->middleware('auth');
Route::get('feria/calendario', 'FeriasController@calendarFeriados')->middleware('auth');
Route::get('jsoncalendarferias', 'FeriasController@jsonCalendarFerias')->middleware('auth');
Route::get('feriasperdidas', 'FeriasController@getFeriasPerdidas')->middleware('auth');
Route::get('feriasposibles', 'FeriasController@getFeriasPosibles')->middleware('auth');
Route::get('feriasasistidas', 'FeriasController@getFeriasAsistidas')->middleware('auth');
Route::get('feria/ver/{id}', 'FeriasController@viewFeria')->middleware('auth');
Route::get('feria/bitacora/{id}', 'FeriasController@viewBitacora')->middleware('auth');
Route::get('feria/trasmitir/camara', 'FeriasController@pushCamara')->middleware('auth');
Route::get('feria/visualizar/camara/{id}', 'FeriasController@viewCamara')->middleware('auth');
Route::post('feria/bitacora/crear', 'FeriasController@saveBitacora')->middleware('auth');
Route::get('feria/jsonbitacora/{id}', 'FeriasController@jsonBitacora')->middleware('auth');
Route::get('feria/bitacora/dashboard/{id}', 'FeriasController@viewDashboard')->middleware('auth');
Route::get('json/visitantes/{id}','FeriasController@jsonBitDashVisitantes')->middleware('auth');
Route::get('json/horarios/{id}','FeriasController@jsonBitDashHorarios')->middleware('auth');
Route::get('json/pais/{id}','FeriasController@jsonBitDashPais')->middleware('auth');
Route::get('json/productos/{id}','FeriasController@jsonBitDashProductos')->middleware('auth');
Route::get('feriactual','FeriasController@searchFeriaLast')->middleware('auth');
Route::get('consultargaleria/{id}','FeriasController@consultargaleria')->middleware('auth');
Route::get('consultarlinks/{id}','FeriasController@consultarlinks')->middleware('auth');
Route::get('listadecategorias','FeriasController@listadecategorias')->middleware('auth');
Route::post('listadefuncionarios','FeriasController@listadefuncionarios')->middleware('auth');
Route::get('anadiruserinv/{id}','FeriasController@anadiruserinv')->middleware('auth');
Route::get('editarexponente/{id}','ExponenteController@editarexponente')->middleware('auth');
Route::post('editarexponente','ExponenteController@editarexponentedos')->middleware('auth');

Route::get('verlistpatrocinador/{id}', 'PatrocinadorController@verlistpatrocinador')->middleware('auth');
Route::get('feria/patrocinadores', 'PatrocinadorController@patrocinadores')->middleware('auth');
Route::get('feria/patrocinadores/crear', 'PatrocinadorController@patrocinadorescrear')->middleware('auth');
Route::get('feria/patrocinadores/editar/{id}', 'PatrocinadorController@patrocinadoreseditar')->middleware('auth');
Route::post('guardarpatrocinador', 'PatrocinadorController@guardar');
Route::post('editarpatrocinador', 'PatrocinadorController@editarPatrocinadorEdwin')->middleware('auth');
Route::get('eliminarpatrocinador/{id}', 'PatrocinadorController@eliminarpatrocinador')->middleware('auth');
Route::get('verpatrocinador/{id}', 'PatrocinadorController@verpatrocinador')->middleware('auth');
Route::get('clientes/GBP', 'ClientesgbpController@crearclientes');
Route::post('guardarclientesgbp', 'ClientesgbpController@guardarclientes');


Route::get('feria/galeria/{id}', 'FeriasController@vergaleria')->middleware('auth');
Route::get('invitados/ver/{id}', 'FeriasController@verinviados')->middleware('auth');
Route::get('invitado/crear/{id}', 'FeriainvitadosController@crearinviado')->middleware('auth');
Route::get('invitado/editar/{id_feria}&{id_invitado}', 'FeriainvitadosController@editarinvitado')->middleware('auth');
Route::get('consultarempresas/{id}', 'FeriainvitadosController@consultarempresas')->middleware('auth');
Route::get('consultarclientes', 'FeriainvitadosController@consultarclientes')->middleware('auth');
Route::get('consultarclientes2/{id}', 'FeriainvitadosController@consultarclientes2')->middleware('auth');
Route::post('guardarinvitado', 'FeriainvitadosController@guardarinvitado')->middleware('auth');
Route::post('guardarinvitadonuevo', 'FeriainvitadosController@guardarinvitadonuevo')->middleware('auth');
Route::post('editarinvitadonuevo', 'FeriainvitadosController@editarinvitadonuevo')->middleware('auth');
Route::post('editarinvitadocliente', 'FeriainvitadosController@editarinvitadocliente')->middleware('auth');
Route::get('eliminarinvitado/{id}', 'FeriainvitadosController@eliminarinvitado')->middleware('auth');

Route::get('ferias/menu', 'FeriasmenuController@menu')->middleware('auth');
//Gestión patrocinadores-feria
Route::get('feria/patrocinador/detalles-patrocinador/{id?}', 'PatrocinadorController@details_sponsor')->middleware('auth');
Route::get('feria/patrocinador/{id_feria}&{nom_feria}', 'PatrocinadorController@listsponsor')->middleware('auth');
Route::post('feria/savesponsorship', 'PatrocinadorController@savesponsorship')->middleware('auth');
Route::post('feria/listsponsorship', 'PatrocinadorController@listsponsorship')->middleware('auth');
Route::post('feria/accionsolicitud', 'PatrocinadorController@actionrequest')->middleware('auth');
Route::post('feria/listarcomentarios', 'PatrocinadorController@list_comments')->middleware('auth');
Route::post('feria/patrocinador/comentar-patrocinador', 'PatrocinadorController@comment')->middleware('auth');
Route::post('feria/patrocinador/comentarios-patrocinador', 'PatrocinadorController@sponsor_comments')->middleware('auth');
Route::post('feria/patrocinador/listar-responsables', 'PatrocinadorController@list_responsible')->middleware('auth');

/*Ferias Oportunidades - Clientes*/
Route::post('feria/clientes/autocomplete', 'FeriasController@ListarClientesInput')->middleware('auth');
Route::post('feria/clientes/acciones', 'FeriasController@AccionesFeriaCliente')->middleware('auth');
Route::post('feria/oportunidades/autocomplete', 'FeriasController@ListarOportunidadesInput')->middleware('auth');
Route::post('feria/oportunidades/acciones', 'FeriasController@AccionesFeriaOportunidad')->middleware('auth');
Route::post('feria/clientes-oportunidades', 'FeriasController@ListarClientesOportunidades')->middleware('auth');

/*Módulo Planeación*/
Route::get('feria/planeacion/{id}', 'PlaneacionController@index')->middleware('auth');
Route::post('feria/planeacion/activity-action', 'PlaneacionController@ActivityActions')->middleware('auth');
Route::post('feria/planeacion/timeline', 'PlaneacionController@TimeLineData')->middleware('auth');

/*Ferias Lecciones Aprendidas */
Route::get('feria/leccionesaprendidas/{id}', 'LeccionesaprendidasController@index')->middleware('auth');
Route::get('consultarlecciones/{id}', 'LeccionesaprendidasController@lecciones')->middleware('auth');
Route::get('consultarrespuesta/{id}', 'LeccionesaprendidasController@consultarrespuesta')->middleware('auth');
Route::post('guardarrespuesta', 'LeccionesaprendidasController@guardarrespuesta')->middleware('auth');
Route::get('consultardocumentoslecciones/{id}', 'LeccionesaprendidasController@consultardocumentoslecciones')->middleware('auth');
Route::post('cargararchivolecciones', 'ArchivoController@cargarlecciones')->middleware('auth');
Route::get('eliminararchivolecciones/{id}', 'LeccionesaprendidasController@eliminararchivolecciones')->middleware('auth');
Route::get('consultarcomentarios/{id}', 'LeccionesaprendidasController@consultarcomentarios')->middleware('auth');
Route::post('comentarlecciones', 'LeccionesaprendidasController@comentarlecciones')->middleware('auth');

/*Ferias necesidades*/
Route::get('feria/necesidades/{id}', 'FeriasController@necesidades')->middleware('auth');
Route::post('proveedores/todos', 'ProveedoresController@ListadoAutocomplete')->middleware('auth');
Route::post('feria/necesidades/guardar', 'FeriasController@GuardarNecesidades')->middleware('auth');
Route::post('feria/necesidades/listar', 'FeriasController@ListarNecesidades')->middleware('auth');
Route::post('feria/necesidades/guardar-seleccion', 'FeriasController@GuardarProveedorSelecionado')->middleware('auth');
Route::post('feria/necesidades/comentar', 'FeriasController@GuardarNecesidadComentario')->middleware('auth');
Route::post('feria/necesidades/comentarios', 'FeriasController@ListarNecesidadComentarios')->middleware('auth');

/*Ferias Competencias*/
Route::get('feria/competencia/{id}', 'FeriacompetenciaController@index')->middleware('auth');
Route::post('competencia-accion', 'FeriacompetenciaController@accion')->middleware('auth');

/*Modulo Administrador de sistemas*/
Route::get('administradorsistema/vista', 'AdministradosistemaController@view')->middleware('auth');
Route::post('administradorsistema-action', 'AdministradosistemaController@action')->middleware('auth');



/*Modulo Competencias*/
Route::get('competencias', 'CompetenciasController@index')->middleware('auth');
Route::get('competencias/ver/{id}', 'CompetenciasController@VerCompetencia')->middleware('auth');
Route::post('competencias/listar', 'CompetenciasController@ListarCompetencias')->middleware('auth');
Route::post('competencias/listar-individual', 'CompetenciasController@ListarCompetenciaIndividual')->middleware('auth');
Route::post('competencias/accion', 'CompetenciasController@AccionesCompetencias')->middleware('auth');
Route::post('competencias/eliminar-red-social', 'CompetenciasController@ElminarSocialIndividual')->middleware('auth');
Route::post('competencias/comentar-competencia', 'CompetenciasController@ComentarCompetencia')->middleware('auth');
Route::post('competencias/comentarios-competencia', 'CompetenciasController@ListarComentariosCompetencia')->middleware('auth');
Route::post('competencias/listar-funcionarios', 'CompetenciasController@AutocompleteFuncionarios')->middleware('auth');
Route::post('competencias/guardar-sede', 'CompetenciasController@AñadirSede')->middleware('auth');
Route::post('competencias/guardar-funcionario', 'CompetenciasController@AñadirFuncionario')->middleware('auth');
Route::post('competencias/listar-sedes', 'CompetenciasController@ListarSedes')->middleware('auth');
Route::post('competencias/listar-sedes-geo', 'CompetenciasController@SedesMapa')->middleware('auth');

/*Redes sociales*/
Route::post('redes-sociales/listar', 'RedesSocialesController@ListarTodas')->middleware('auth');

//menu Listado Ajax
Route::post('consultarlistadoferiasfuturas', 'FeriasmenuController@consultarlistadoferiasfuturas')->middleware('auth');
Route::post('consultarlistadoferiasfuturasfiltro', 'FeriasmenuController@consultarlistadoferiasfuturasfiltro')->middleware('auth');
Route::post('consultarlistadoferiasexponente', 'FeriasmenuController@consultarlistadoferiasexponente')->middleware('auth');
Route::post('consultarlistadoferiasexponentefiltro', 'FeriasmenuController@consultarlistadoferiasexponentefiltro')->middleware('auth');
Route::post('consultarlistadoferiasvisitante', 'FeriasmenuController@consultarlistadoferiasvisitante')->middleware('auth');
Route::post('consultarlistadoferiasvisitantefiltro', 'FeriasmenuController@consultarlistadoferiasvisitantefiltro')->middleware('auth');
Route::post('consultarlistadoferiasnoasistidas', 'FeriasmenuController@consultarlistadoferiasnoasistidas')->middleware('auth');
Route::post('consultarlistadoferiasnoasistidasfiltro', 'FeriasmenuController@consultarlistadoferiasnoasistidasfiltro')->middleware('auth');

//Mapa-Lechero
Route::get('mapa-lechero', 'MapalecheroController@index')->middleware('auth');
Route::get('mapa-lechero/version/{id}', 'MapalecheroController@version')->middleware('auth');
Route::post('mapa-lechero', 'MapalecheroController@action')->middleware('auth');
Route::post('mapa-lechero/version', 'MapalecheroController@action2')->middleware('auth');



/*Nuevos mercados*/
Route::get('nuevos-mercados', 'NuevosMercadosController@index')->middleware('auth');
Route::get('nuevos-mercados/ver/{id}', 'NuevosMercadosController@VerMercado')->middleware('auth');
Route::post('nuevos-mercados/crear', 'NuevosMercadosController@CrearNuevoMercado')->middleware('auth');
Route::post('nuevos-mercados/listar', 'NuevosMercadosController@ListarNuevosMercados')->middleware('auth');
Route::post('nuevos-mercados/eliminar', 'NuevosMercadosController@EliminarNuevoMercado')->middleware('auth');
Route::post('nuevos-mercados/last-update', 'NuevosMercadosController@LastUpdate')->middleware('auth');
Route::post('nuevos-mercados/cambiar-estado', 'NuevosMercadosController@GuardarJustificacion')->middleware('auth');
Route::post('nuevos-mercados/guardar-adjunto', 'NuevosMercadosController@CargarAdjuntos')->middleware('auth');
Route::post('nuevos-mercados/listar-adjuntos', 'NuevosMercadosController@ListarAdjuntos')->middleware('auth');
Route::post('nuevos-mercados-acciones', 'NuevosMercadosController@acciones')->middleware('auth');

//Freelance
Route::middleware(['auth'])->group(function () {
  // Pedidos
  Route::prefix('freelance')->group(function () {
    //usuarios
    Route::get('listado', 'FreelanceController@index');
    Route::get('crear', 'FreelanceController@crear');
    Route::get('editar/{id}', 'FreelanceController@editar');
    Route::get('ver/{id}','FreelanceController@ver');
    Route::post('crear-accion', 'FreelanceController@action_crear');
    Route::post('listar-accion', 'FreelanceController@action_listar');
    Route::post('ver-accion','FreelanceController@ver_accion');
    //Bitacora
    Route::get('bitacora', 'FreelanceController@index_bitacora');
    Route::post('bitacora-accion', 'FreelanceController@bitacora_accion');
    //Dashboard
    Route::get('red-mundial-cooperacion', 'FreelanceController@red_mundial_coop');
    Route::post('dashboard-accion', 'FreelanceController@dashboard_accion');
    //Oportunidades
    Route::get('oportunidades', 'FreelanceController@oportunidades');
    Route::post('oportunidades-accion', 'FreelanceController@oportunidad_accion');

  });
});

//Personalizado
Route::middleware(['auth'])->group(function () {
  // Pedidos
  Route::prefix('personalizado')->group(function () {
    //usuarios
    Route::get('oportunidadesvendidas', 'PersonalizadoController@index');
    });
});
//error
Route::get('error', 'ErrorController@error');
/*Route::get('error', function(){
    abort(500);
});
/*App::missing(function($exception){
    return Response::view('errors.500', array(), 404);
});*/

/*Route::get('test', function() {guardar_visitaoportunidadperdida
    Storage::disk('google')->put('test.txt', 'Hello World');
});*/

Route::get('archivos/{archivo}', function ($archivo) {
     $public_path = public_path();
     $url = $public_path.'/storage/'.$archivo;
     //verificamos si el archivo existe y lo retornamos
     if (Storage::exists($archivo))
     {
       return response()->download($url);
     }
     //si no se encuentra lanzamos un error 404.
     abort(404);

})->middleware('auth');
